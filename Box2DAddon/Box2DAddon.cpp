#include "stdafx.h"


#include <tchar.h>
#include <angelscript.h>
#include <Box2D/Box2D.h>
#include <string>
#include "../include/ScriptAddin.h"
#include "../include/RegisterVector.h"
#include "Box2DCommon.h"

extern ID_VALFUNC_MAP g_mapValFunc;
bool CALLBACK Conv_b2World( VAL_DATA* pValData, const void* pValAddress, void* pContext);
REGISTER_VECTOR(b2Vec2, b2Vec2_VECTOR);
REGISTER_VECTOR2(b2Body*, b2Body, b2BodyPtr_VECTOR);
REGISTER_VECTOR2(b2Joint*, b2Joint, b2JointPtr_VECTOR);


asIScriptEngine* g_engine = 0;





CRefCountManager* CRefCountManager::ms_pMgr = NULL;





//============================================================================


void RegisterTypeb2BodyPtr_VECTOR    (asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    RegisterTypeb2BodyPtr_VECTOR    (engine, g_mapValFunc);
}

//============================================================================

int Register_b2BodyType_Enum(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    int iRet;
    int r;
    iRet = engine->RegisterEnum("b2BodyType"); assert( iRet >= 0 );

    for (int iBody = 0;
             iBody <= 3;
             iBody++)
    {

        r = engine->RegisterEnumValue("b2BodyType", 
                                       StringToChar(b2BodyTypeToString(iBody)).c_str(),
                                      iBody);
        assert( r >= 0 );
    }
    return iRet;
}
//============================================================================
void RegisterTypeb2JointPtr_VECTOR    (asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    RegisterTypeb2JointPtr_VECTOR    (engine, g_mapValFunc);
}
//============================================================================

int Register_b2JointType_Enum(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    int r;
    int iRet;
    iRet = engine->RegisterEnum("b2JointType");
    assert( iRet >= 0 );

    for (int iJoint = e_unknownJoint;
             iJoint <= e_ropeJoint;
             iJoint++)
    {

        r = engine->RegisterEnumValue("b2JointType", 
                                       StringToChar(b2JointTypeEnumToString(iJoint)).c_str(),
                                      iJoint);
        assert( r >= 0 );
    }
    return iRet;
}


//============================================================================


//===================================================
//===================================================
//===================================================

//===================================================
//===================================================
//===================================================


//===================================================
//===================================================
//===================================================

//===================================================
//===================================================
//===================================================
void RegisterCast_b2Shape(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    int r;

    r = engine->RegisterObjectBehaviour("b2Shape", asBEHAVE_REF_CAST , 
        "b2PolygonShape@ f()", 
        asFUNCTION((ASRefCast<b2Shape, b2PolygonShape>)), 
        asCALL_CDECL_OBJLAST);
    assert( r>=0 );

    r = engine->RegisterObjectBehaviour("b2Shape", asBEHAVE_REF_CAST , 
        "b2EdgeShape@ f()", 
        asFUNCTION((ASRefCast<b2Shape, b2EdgeShape>)), 
        asCALL_CDECL_OBJLAST);
    assert( r>=0 );


    //================================
    // 逆変換
    //================================
    r = engine->RegisterObjectBehaviour("b2PolygonShape", asBEHAVE_IMPLICIT_REF_CAST, 
        "b2Shape@ f()", 
        asFUNCTION((ASRefCast< b2PolygonShape, b2Shape>)), 
        asCALL_CDECL_OBJLAST);
    assert( r>=0 );

    r = engine->RegisterObjectBehaviour("b2EdgeShape", asBEHAVE_IMPLICIT_REF_CAST, 
        "b2Shape@ f()", 
        asFUNCTION((ASRefCast< b2EdgeShape, b2Shape>)), 
        asCALL_CDECL_OBJLAST);
    assert( r>=0 );
}



//===================================================
//===================================================
//===================================================



//===================================================
//===================================================
//===================================================


//===================================================
//===================================================
//===================================================

//============================================================================
void RegisterTypeb2Vec2_VECTOR    (asIScriptEngine *engine)
{       
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    RegisterTypeb2Vec2_VECTOR    (engine, g_mapValFunc);
}

//============================================================================


//===================================================
//===================================================
//===================================================


//===================================================
//===================================================
//===================================================


//int RegisterTypeb2Vec2_VECTOR    (asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc);
//void  Registerb2Vec2_VECTOR        (asIScriptEngine *engine);

//===================================================
//===================================================
//===================================================

/*
    とりあえず以下のクラスを実装する

	b2Body
	b2World
	b2Vec2
	b2EdgeShape shape;  **
	b2PolygonShape box;
	b2CircleShape circle;

	b2FixtureDef fd;

    b2JointDef

    b2Joint

	
	b2MouseJoint* m_mouseJoint;
	b2RevoluteJointDef jd;


*/
