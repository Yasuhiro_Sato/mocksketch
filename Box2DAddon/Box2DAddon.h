
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#ifndef BOX2D_ADDON_H
#define BOX2D_ADDON_H

#include <angelscript.h>
#include "../include/ScriptAddin.h"

void Register_b2Vec2Type(asIScriptEngine *engine);
void Register_b2Vec2(asIScriptEngine *engine);

void RegisterTypeb2Vec2_VECTOR    (asIScriptEngine *engine);
void Registerb2Vec2_VECTOR        (asIScriptEngine *engine);

int Register_b2BodyType_Enum(asIScriptEngine *engine);
int Register_b2ShapeType_Enum(asIScriptEngine *engine);
int Register_b2JointType_Enum(asIScriptEngine *engine);
//-------------
void Register_b2BodyDefType(asIScriptEngine *engine);
void Register_b2BodyDef(asIScriptEngine *engine);

void Register_b2BodyType(asIScriptEngine *engine);
void Register_b2Body(asIScriptEngine *engine);
//-------------

void RegisterTypeb2BodyPtr_VECTOR    (asIScriptEngine *engine);
void Registerb2BodyPtr_VECTOR        (asIScriptEngine *engine);



//-------------
// Shape
//-------------
void Register_b2ShapeType(asIScriptEngine *engine);
void Register_b2Shape(asIScriptEngine *engine);

void Register_b2PolygonShapeType(asIScriptEngine *engine);
void Register_b2PolygonShape(asIScriptEngine *engine);

void Register_b2EdgeShapeType(asIScriptEngine *engine);
void Register_b2EdgeShape(asIScriptEngine *engine);

void RegisterCast_b2Shape(asIScriptEngine *engine);

//-------------
void Register_b2FixtureDefType(asIScriptEngine *engine);
void Register_b2FixtureDef(asIScriptEngine *engine);

void Register_b2FixtureType(asIScriptEngine *engine);
void Register_b2Fixture(asIScriptEngine *engine);

//-------------
void Register_b2WorldType(asIScriptEngine *engine);
void Register_b2World(asIScriptEngine *engine);



//-------------
// Joint
//-------------
void Register_b2JointDefType(asIScriptEngine *engine);
void Register_b2JointDef(asIScriptEngine *engine);

void Register_b2DistanceJointDefType(asIScriptEngine *engine);
void Register_b2DistanceJointDef(asIScriptEngine *engine);

void Register_b2RevoluteJointDefType(asIScriptEngine *engine);
void Register_b2RevoluteJointDef(asIScriptEngine *engine);

void Register_b2PrismaticJointDefType(asIScriptEngine *engine);
void Register_b2PrismaticJointDef(asIScriptEngine *engine);

void Register_b2MouseJointDefType(asIScriptEngine *engine);
void Register_b2MouseJointDef(asIScriptEngine *engine);

void Register_b2GearJointDefType(asIScriptEngine *engine);
void Register_b2GearJointDef(asIScriptEngine *engine);

void Register_b2FrictionJointDefType(asIScriptEngine *engine);
void Register_b2FrictionJointDef(asIScriptEngine *engine);

//-------------
void Register_b2JointType(asIScriptEngine *engine);
void Register_b2Joint(asIScriptEngine *engine);

void Register_b2RevoluteJointType(asIScriptEngine *engine);
void Register_b2RevoluteJoint(asIScriptEngine *engine);

void Register_b2DistanceJointType(asIScriptEngine *engine);
void Register_b2DistanceJoint(asIScriptEngine *engine);

void Register_b2PrismaticJointType(asIScriptEngine *engine);
void Register_b2PrismaticJoint(asIScriptEngine *engine);

void Register_b2MouseJointType(asIScriptEngine *engine);
void Register_b2MouseJoint(asIScriptEngine *engine);

void Register_b2GearJointType(asIScriptEngine *engine);
void Register_b2GearJoint(asIScriptEngine *engine);

void Register_b2FrictionJointType(asIScriptEngine *engine);
void Register_b2FrictionJoint(asIScriptEngine *engine);

void Register_b2MassDataType(asIScriptEngine *engine);
void Register_b2MassData(asIScriptEngine *engine);

void RegisterTypeb2JointPtr_VECTOR    (asIScriptEngine *engine);
void Registerb2JointPtr_VECTOR      (asIScriptEngine *engine);



#endif