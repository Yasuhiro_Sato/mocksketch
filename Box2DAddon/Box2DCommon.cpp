#include "stdafx.h"
#include <tchar.h>
#include <Box2D/Box2D.h>
#include <angelscript.h>
#include "../include/ScriptAddin.h"
#include "Box2DCommon.h"


/**
  *  @brief   StdString->char 変換.
  *  @param   [in] strString 変換元文字列 
  *  @return               変換後文字列
  */
std::string StringToChar(StdString strString)
{
#ifdef  UNICODE      
    std::string strRet;

  	char*	psToStringBuf;

    size_t iLen = strString.length();
    size_t iRet;
    errno_t iErrorNo;

    if (iLen > 0)
    {
	    psToStringBuf = (char*)malloc( iLen * 2 );

        if (psToStringBuf == 0)
        {
            return strRet;
        }

        iErrorNo = wcstombs_s(&iRet, psToStringBuf, iLen * 2, strString.c_str(), _TRUNCATE);

	    if ( iErrorNo == 0 )
	    {
		    strRet = psToStringBuf;
	    }

	    free( psToStringBuf );
    }
    return strRet;
    
#else
     return strString.c_str();
#endif
}


LPCTSTR b2BodyTypeToString(int iEnum)
{
    switch(iEnum)
    {
    case 0: {return _T("b2_staticBody");}
    case 1: {return _T("b2_kinematicBody");}
    case 2: {return _T("b2_dynamicBody");}
    case 3: {return _T("b2_bulletBody");}
    default:{return _T("b2_unknownBody");}
    }
}



LPCTSTR b2ShapeTypeToString(int iEnum)
{
    switch(iEnum)
    {
    case 0: {return _T("e_circle");}
    case 1: {return _T("e_edge");}
    case 2: {return _T("e_polygon");}
    case 3: {return _T("e_chain");}
    case 4: {return _T("e_typeCount");}
    default:{return _T("e_unknown");}
    }
}

int Register_b2ShapeType_Enum(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    int iRet;
    int r;
    iRet = engine->RegisterEnum("b2ShapeType"); assert( iRet >= 0 );

    for (int iShape = 0;
             iShape <= 4;
             iShape++)
    {

        r = engine->RegisterEnumValue("b2ShapeType", 
                                       StringToChar(b2ShapeTypeToString(iShape)).c_str(),
                                      iShape);
        assert( r >= 0 );
    }
    return iRet;
}


LPCTSTR b2JointTypeEnumToString(int iEnum)
{
    switch(iEnum)
    {
    case e_unknownJoint:    {return _T("e_unknownJoint");}
    case e_revoluteJoint:   {return _T("e_revoluteJoint");}
    case e_prismaticJoint:  {return _T("e_prismaticJoint");}
    case e_distanceJoint:   {return _T("e_distanceJoint");}
    case e_pulleyJoint:     {return _T("e_pulleyJoint");}
    case e_mouseJoint:      {return _T("e_mouseJoint");}
    case e_gearJoint:       {return _T("e_gearJoint");}
    case e_wheelJoint:      {return _T("e_wheelJoint");}
    case e_weldJoint:       {return _T("e_weldJoint");}
    case e_frictionJoint:   {return _T("e_frictionJoint");}
    case e_ropeJoint:       {return _T("e_ropeJoint");}
    default:                {return _T("e_unknownJoint");}
    }
}

