
#ifndef _BOX2D_COMMON_H
#define _BOX2D_COMMON_H



extern std::string StringToChar(StdString strString);
extern LPCTSTR b2BodyTypeToString(int iEnum);
extern LPCTSTR b2JointTypeEnumToString(int iEnum);
extern int Register_b2ShapeType_Enum(asIScriptEngine *engine);
extern LPCTSTR b2ShapeTypeToString(int iEnum);

extern ID_VALFUNC_MAP g_mapValFunc;
extern asIScriptEngine* g_engine;

template<class T>
static bool GetUserData(void* pUserData, int refTypeId, T &s)
{
    // 型チェックなしなので注意(型データを保存しておく場所がない)
    void* pStordData = s.GetUserData();
    if (!pStordData)
    {
        return false;
    }

    AsAnyVal* pAny = reinterpret_cast<AsAnyVal*>(pStordData);
 
    if( refTypeId & asTYPEID_OBJHANDLE )
	{
		// Is the handle type compatible with the stored value?

		// A handle can be retrieved if the stored type is a handle of same or compatible type
		// or if the stored type is an object that implements the interface that the handle refer to.
		if( (pAny->iType & asTYPEID_MASK_OBJECT) && 
			g_engine->IsHandleCompatibleWithObject(pAny->pVal, pAny->iType, refTypeId) )
		{
			g_engine->AddRefScriptObject(pAny->pVal, g_engine->GetObjectTypeById(pAny->iType));
			*(void**)pUserData = pAny->pVal;

			return true;
		}
	}
	else if( refTypeId & asTYPEID_MASK_OBJECT )
	{
		// Is the object type compatible with the stored value?

		// Copy the object into the given reference
		if( pAny->iType == refTypeId )
		{
			g_engine->AssignScriptObject(pUserData, pAny->pVal, g_engine->GetObjectTypeById(pAny->iType));

			return true;
		}
	}
	else
	{

		// Is the primitive type compatible with the stored value?

		if( pAny->iType == refTypeId )
		{
			int size = g_engine->GetSizeOfPrimitiveType(refTypeId);
			memcpy(pUserData,pAny->pVal, size);
			return true;
		}

		// We know all numbers are stored as either int64 or double, since we register overloaded functions for those
		if( pAny->iType == asTYPEID_INT64 && refTypeId == asTYPEID_DOUBLE )
		{
			*(double*)pUserData = double(pAny->iVal);
			return true;
		}
		else if( pAny->iType == asTYPEID_DOUBLE && refTypeId == asTYPEID_INT64 )
		{
			*(asINT64*)pUserData = asINT64(pAny->dVal);
			return true;
		}
	}
    return false;
}

template<class T>
bool GetUserData_Double(double &value, T &s)
{
    return GetUserData<T>(&value, asTYPEID_DOUBLE, s);
}

template<class T>
bool GetUserData_Int(asINT64 &value,T &s)
{
     return GetUserData<T>(&value, asTYPEID_INT64, s);
}

template<class T>
void FreeData(T &s)
{
    //型T 削除時に呼び出す必要あり

    void* pData = s.GetUserData();
    if (pData)
    {
        AsAnyVal* pAny = reinterpret_cast<AsAnyVal*>(pData);
        if (g_engine)
        {
	        if( pAny->iType & asTYPEID_MASK_OBJECT )
	        {
		        // Let the engine release the object
		        asIObjectType *ot = g_engine->GetObjectTypeById(pAny->iType);
		        g_engine->ReleaseScriptObject(pAny->pVal, ot);

		        // Release the object type info
		        if( ot )
                {
			        ot->Release();
                }

	        }
        }
        delete pData;
    }
    s.SetUserData(0);
}

template<class T>
void SetUserData(void* pUserData, int refTypeId, T &s)
{
    void* pData = *(void**)pUserData;

	// Hold on to the object type reference so it isn't destroyed too early
	if( pData && (refTypeId & asTYPEID_MASK_OBJECT) )
	{
		asIObjectType *ot = g_engine->GetObjectTypeById(refTypeId);
		if( ot )
        {
			ot->AddRef();
        }
	}

	FreeData<T>(s);

    AsAnyVal* pAny = new AsAnyVal;
    pAny->iType = refTypeId;
	if( refTypeId & asTYPEID_OBJHANDLE )
	{
		// We're receiving a reference to the handle, so we need to dereference it
		pAny->pVal = pData;
		g_engine->AddRefScriptObject(pAny->pVal, g_engine->GetObjectTypeById(pAny->iType));
	}
	else if( pAny->iType & asTYPEID_MASK_OBJECT )
	{
		// Create a copy of the object
		pAny->pVal = g_engine->CreateScriptObjectCopy(pUserData, g_engine->GetObjectTypeById(pAny->iType));
	}
	else
	{
		// Primitives can be copied directly
		pAny->pVal = 0;

		// Copy the primitive value
		// We receive a pointer to the value.
		int size = g_engine->GetSizeOfPrimitiveType(pAny->iType);
		memcpy(&pAny->pVal, pUserData, size);
	}
    s.SetUserData(pAny);
}

template<class T>
void SetUserData_Int(asINT64 &value, T &s)
{
    SetUserData<T>(&value, asTYPEID_INT64, s);
}

template<class T>
void SetUserData_Double(double &value, T &s)
{
    SetUserData<T>(&value, asTYPEID_DOUBLE, s);
}


template<class T>
void RegisterUserData(asIScriptEngine *engine, char* className)
{
   int r;
    r = engine->RegisterObjectMethod(className, 
	    "bool GetUserData(?&out) const",
        asFUNCTION(GetUserData<T>), asCALL_CDECL_OBJLAST);
        assert( r >= 0 );

    r = engine->RegisterObjectMethod(className, 
	    "bool GetUserData(int64&out) const",
        asFUNCTION(GetUserData_Int<T>), asCALL_CDECL_OBJLAST);
        assert( r >= 0 );

    r = engine->RegisterObjectMethod(className, 
	    "bool GetUserData(double&out) const",
        asFUNCTION(GetUserData_Double<T>), asCALL_CDECL_OBJLAST);
        assert( r >= 0 );
        
        r = engine->RegisterObjectMethod(className, 
        "void SetUserData(?&in)" , 
        asFUNCTION(SetUserData<T>), asCALL_CDECL_OBJLAST);
        assert( r >= 0 );
	
    r = engine->RegisterObjectMethod(className, 
        "void SetUserData(int64&in)" , 
        asFUNCTION(SetUserData_Int<T>), asCALL_CDECL_OBJLAST);
        assert( r >= 0 );

    r = engine->RegisterObjectMethod(className, 
        "void SetUserData(double&in)" , 
        asFUNCTION(SetUserData_Double<T>), asCALL_CDECL_OBJLAST);
        assert( r >= 0 );

}

struct AsAnyVal
{
    union
    {
        asINT64 iVal;
        double  dVal;
        void*   pVal;
    };
    int   iType;
};

class CRefCountManager
{
public:

    static CRefCountManager* GetInstance()
    {
        if (!ms_pMgr)
        {
            ms_pMgr = new CRefCountManager;
        }
        return ms_pMgr;
    }

    static void DeleteInstance()
    {
        if (ms_pMgr)
        {
            delete ms_pMgr;
        }
        ms_pMgr = NULL;
    }

    void AddRef(void* pInstacne)
    {
        std::map<void*, int>::iterator ite;
        ite = m_mapRefCounter.find(pInstacne); 
        if ( ite == m_mapRefCounter.end())
        {
            m_mapRefCounter[pInstacne] = 1;
        }
        else
        {
            ite->second++;
        }
    }

    int GetCount(void* pInstacne)
    {
        std::map<void*, int>::iterator ite;
        ite = m_mapRefCounter.find(pInstacne); 
        if ( ite == m_mapRefCounter.end())
        {
            return -1;
        }

        return ite->second;
    }

    void Release(void* pInstacne)
    {
        std::map<void*, int>::iterator ite;
        ite = m_mapRefCounter.find(pInstacne); 
        if ( ite == m_mapRefCounter.end())
        {
            return;
        }

        if(--(ite->second) == 0)
        {
            delete pInstacne;
            m_mapRefCounter.erase(ite);
        }
    }

protected:
    CRefCountManager(){;}
    ~CRefCountManager()
    {
        std::map<void*, int>::iterator ite;
        for (ite = m_mapRefCounter.begin();
             ite != m_mapRefCounter.end();
             ite++)
        {
            delete ite->first;
        }
    }

    std::map<void*, int> m_mapRefCounter;

    static CRefCountManager* ms_pMgr;
};

template<class A, class B> B* ASRefCast(A* a)
{
    // If the handle already is a null handle, then just return the null handle
	if (a==NULL) return NULL;
	// Now try to dynamically cast the pointer to the wanted type
	B* b = dynamic_cast<B*>(a);
	if (b!=NULL) 
    {
		// Since the cast was made, we need to increase the ref counter for the returned handle
        AddRef<B>(*b);
	}
//	printf("ASRefCast: returning %p\n", b);
	return b;
}

template<class A, class B> B* ASCast(A* a)
{
	if (a==NULL) return NULL;
	B* b = reinterpret_cast<B*>(a);
	return b;
}


template<class D>
static void Constructor(D *self)
{
    new(self) D();
}

template<class D>
static void CopyConstructor(const D &other, D *self)
{
    new(self) D(other);
}

template<class D>
static void Destructor(D *memory)
{
    memory->~D();
}


template<class D>
static void AddRef(D& cls)
{
    CRefCountManager* pMgr;
    pMgr = CRefCountManager::GetInstance();
    pMgr->AddRef(&cls);
}

template<class D>
static void Release(D& cls)
{
    CRefCountManager* pMgr;
    pMgr = CRefCountManager::GetInstance();
    pMgr->Release(&cls);
}

template<class D>
static D* Factory()
{
    D *pRet;
    pRet = new D();
    AddRef<D>(*pRet);
    return pRet;
}

template<class D>
static void DummyAddRelease(D& cls)
{
    ;//Do nothing
}

template<class D>
void RegisterC(asIScriptEngine *engine, char* className)
{
    int r;
	r = engine->RegisterObjectBehaviour(className, 
        asBEHAVE_CONSTRUCT, "void f()",
        asFUNCTION(Constructor<D>),
        asCALL_CDECL_OBJLAST); assert( r >= 0 );
}

template<class D>
void RegisterD(asIScriptEngine *engine, char* className)
{
    int r;
	r = engine->RegisterObjectBehaviour(className, 
        asBEHAVE_DESTRUCT, "void f()",
        asFUNCTION(Destructor<D>),
        asCALL_CDECL_OBJLAST); assert( r >= 0 );
}

template<class D>
void RegisterA(asIScriptEngine *engine, char* className)
{
    int r;

    std::string strFunc = "void f(const ";
    strFunc += className;
    strFunc += " &in)";

    r = engine->RegisterObjectBehaviour(className,
        asBEHAVE_CONSTRUCT, strFunc.c_str(),
        asFUNCTION(CopyConstructor<D>),
        asCALL_CDECL_OBJLAST); assert( r >= 0 );
}


template<class D>
void RegisterFactory(asIScriptEngine *engine, char* className)
{
    int r;

    std::string strFunc = className;
    strFunc += " @f()";

    r = engine->RegisterObjectBehaviour(className, asBEHAVE_FACTORY, 
        strFunc.c_str(),
        asFUNCTION( Factory<D>), asCALL_CDECL); 
        assert( r >= 0 );
}

template<class D>
void RegisterRef(asIScriptEngine *engine, char* className)
{
    int r;
    r = engine->RegisterObjectBehaviour(className, asBEHAVE_ADDREF, 
        "void f()", asFUNCTION(AddRef<D>), 
        asCALL_CDECL_OBJFIRST); assert( r >= 0 );

	r = engine->RegisterObjectBehaviour(className, asBEHAVE_RELEASE,
        "void f()", asFUNCTION(Release<D>), 
        asCALL_CDECL_OBJFIRST); assert( r >= 0 );
}

template<class D>
void RegisterRefDummy(asIScriptEngine *engine, char* className)
{
    int r;
    r = engine->RegisterObjectBehaviour(className, asBEHAVE_ADDREF, 
        "void f()", asFUNCTION(DummyAddRelease<D>), 
        asCALL_CDECL_OBJFIRST); assert( r >= 0 );

	r = engine->RegisterObjectBehaviour(className, asBEHAVE_RELEASE,
        "void f()", asFUNCTION(DummyAddRelease<D>), 
        asCALL_CDECL_OBJFIRST); assert( r >= 0 );
}


template<class D>
void RegisterTemplate_b2Shape(asIScriptEngine *engine, char* className)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    RegisterRef<D>(engine, className);

    //b2Shapeは抽象型なのでインスタンス化不可
    //RegisterFactory<D>(engine, className);

    int r;


    r = engine->RegisterObjectMethod( className, 
        "b2ShapeType GetType() const" , 
        asMETHOD( D, GetType), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod( className, 
        "int GetChildCount() const" , 
        asMETHOD( D, GetChildCount), asCALL_THISCALL); 
        assert( r >= 0 );

	//virtual bool TestPoint(const b2Transform& xf, const b2Vec2& p) const = 0;
	//virtual bool RayCast(b2RayCastOutput* output, const b2RayCastInput& input,
	//					const b2Transform& transform, int32 childIndex) const = 0;
	//virtual void ComputeAABB(b2AABB* aabb, const b2Transform& xf, int32 childIndex) const = 0;
	//virtual void ComputeMass(b2MassData* massData, float32 density) const = 0;
}
#endif
