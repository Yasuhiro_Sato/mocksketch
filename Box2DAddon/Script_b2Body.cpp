#include "stdafx.h"
#include <tchar.h>
#include <Box2D/Box2D.h>
#include <angelscript.h>
#include "../include/ScriptAddin.h"
#include "Box2DCommon.h"

extern bool CALLBACK Conv_b2Vec2( VAL_DATA* pValData, const void* pValAddress, void* pContext);
extern bool CALLBACK Conv_b2Fixture( VAL_DATA* pValData, const void* pValAddress, void* pContext);
extern bool CALLBACK Conv_b2World( VAL_DATA* pValData, const void* pValAddress, void* pContext);

bool CALLBACK Conv_b2Body( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("b2Body");
    const b2Body* pBody = reinterpret_cast<const b2Body*>(pValAddress);
    StdStringStream strmValAll; 
    StdStringStream strmVal; 

    if (!pValAddress)
    {
        pValData->bValid = false;
        return false;
    }
    pValData->bValid = true;
    pValData->bArray = false;


    strmValAll << _T("{");
    if (pBody == NULL)
    {
        strmValAll << _T("NULL");
        strmValAll << _T("}");
        pValData->strVal = strmValAll.str();
        return true;
    }


    {
    VAL_DATA val;
    val.bValid = true;
    val.bArray = false;
    val.strName     = _T("Type");
    val.strTypeName = _T("b2BodyType");
    strmVal .str(_T(""));
    strmVal << b2BodyTypeToString(pBody->GetType());
    val.strVal = strmVal.str();
    pValData->lstVal.push_back(val);
    strmValAll << val.strVal<< _T(",");
    }
	
    {
    VAL_DATA val;
    val.bValid = true;
    val.bArray = false;
    val.strName     = _T("linearVelocity");
    val.strTypeName = _T("b2Vec2");
    strmVal .str(_T(""));
    Conv_b2Vec2( &val, &pBody->GetLinearVelocity(), pContext);
    pValData->lstVal.push_back(val);
    strmValAll << val.strVal<< _T(",");
    }

    {
    VAL_DATA val;
    val.bValid = true;
    val.bArray = false;
    val.strName     = _T("angularVelocity");
    val.strTypeName = _T("float");
    strmVal .str(_T(""));
    strmVal << pBody->GetAngularVelocity();
    val.strVal = strmVal.str();
    pValData->lstVal.push_back(val);
    strmValAll << val.strVal<< _T(",");
    }

    {
    VAL_DATA val;
    val.bValid = true;
    val.bArray = false;
    val.strName     = _T("world");
    val.strTypeName = _T("b2World*");

    strmVal .str(_T(""));
    strmVal << std::hex << std::showbase << pBody->GetWorld();
    strmVal << _T(",{");
    Conv_b2World( &val, pBody->GetWorld(), pContext);
    strmVal << _T("},");
    val.strVal = strmVal.str();
    pValData->lstVal.push_back(val);
    }

    {
    VAL_DATA val;
    val.bValid = true;
    val.bArray = false;
    val.strName     = _T("fixtureList");
    val.strTypeName = _T("b2Fixture*");

    strmVal .str(_T(""));
    strmVal << std::hex << std::showbase << pBody->GetFixtureList();
    strmVal << _T(",{");
    Conv_b2Fixture( &val, pBody->GetFixtureList(), pContext);
    strmVal << _T("},");
    val.strVal = strmVal.str();
    pValData->lstVal.push_back(val);
    }
    strmValAll << _T("}");
    pValData->strVal = strmValAll.str();
    return true;
}

void Register_b2BodyType(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    int iRet;
    iRet = engine->RegisterObjectType("b2Body", 0, asOBJ_REF );
    assert( iRet >= 0 );  
    g_mapValFunc[iRet] = Conv_b2Body;
}

/*
static bool GetUserData_b2Body(void* pUserData, int refTypeId, b2Body &s)
{
    // 型チェックなしなので注意(型データを保存しておく場所がない)
    void* pStordData = s.GetUserData();
    if (!pStordData)
    {
        return false;
    }

    AsAnyVal* pAny = reinterpret_cast<AsAnyVal*>(pStordData);
 
    if( refTypeId & asTYPEID_OBJHANDLE )
	{
		// Is the handle type compatible with the stored value?

		// A handle can be retrieved if the stored type is a handle of same or compatible type
		// or if the stored type is an object that implements the interface that the handle refer to.
		if( (pAny->iType & asTYPEID_MASK_OBJECT) && 
			g_engine->IsHandleCompatibleWithObject(pAny->pVal, pAny->iType, refTypeId) )
		{
			g_engine->AddRefScriptObject(pAny->pVal, g_engine->GetObjectTypeById(pAny->iType));
			*(void**)pUserData = pAny->pVal;

			return true;
		}
	}
	else if( refTypeId & asTYPEID_MASK_OBJECT )
	{
		// Is the object type compatible with the stored value?

		// Copy the object into the given reference
		if( pAny->iType == refTypeId )
		{
			g_engine->AssignScriptObject(pUserData, pAny->pVal, g_engine->GetObjectTypeById(pAny->iType));

			return true;
		}
	}
	else
	{

		// Is the primitive type compatible with the stored value?

		if( pAny->iType == refTypeId )
		{
			int size = g_engine->GetSizeOfPrimitiveType(refTypeId);
			memcpy(pUserData,pAny->pVal, size);
			return true;
		}

		// We know all numbers are stored as either int64 or double, since we register overloaded functions for those
		if( pAny->iType == asTYPEID_INT64 && refTypeId == asTYPEID_DOUBLE )
		{
			*(double*)pUserData = double(pAny->iVal);
			return true;
		}
		else if( pAny->iType == asTYPEID_DOUBLE && refTypeId == asTYPEID_INT64 )
		{
			*(asINT64*)pUserData = asINT64(pAny->dVal);
			return true;
		}
	}
    return false;
}

bool GetUserData_b2Body_Int(asINT64 &value,b2Body &s)
{
    return GetUserData_b2Body(&value, asTYPEID_INT64, s);
}

bool GetUserData_b2Body_Double(double &value, b2Body &s)
{
    return GetUserData_b2Body(&value, asTYPEID_DOUBLE, s);
}


void FreeData(b2Body &s)
{
    //b2Body削除時に呼び出す必要あり
    //b2Bodyのデストラクタは設定していないので

    void* pData = s.GetUserData();
    if (pData)
    {
        AsAnyVal* pAny = reinterpret_cast<AsAnyVal*>(pData);
        if (g_engine)
        {
	        if( pAny->iType & asTYPEID_MASK_OBJECT )
	        {
		        // Let the engine release the object
		        asIObjectType *ot = g_engine->GetObjectTypeById(pAny->iType);
		        g_engine->ReleaseScriptObject(pAny->pVal, ot);

		        // Release the object type info
		        if( ot )
                {
			        ot->Release();
                }

	        }
        }
        delete pData;
    }
    s.SetUserData(0);
}

void SetUserData_b2Body(void* pUserData, int refTypeId, b2Body &s)
{
    void* pData = *(void**)pUserData;

	// Hold on to the object type reference so it isn't destroyed too early
	if( pData && (refTypeId & asTYPEID_MASK_OBJECT) )
	{
		asIObjectType *ot = g_engine->GetObjectTypeById(refTypeId);
		if( ot )
        {
			ot->AddRef();
        }
	}

	FreeData(s);

    AsAnyVal* pAny = new AsAnyVal;
    pAny->iType = refTypeId;
	if( refTypeId & asTYPEID_OBJHANDLE )
	{
		// We're receiving a reference to the handle, so we need to dereference it
		pAny->pVal = pData;
		g_engine->AddRefScriptObject(pAny->pVal, g_engine->GetObjectTypeById(pAny->iType));
	}
	else if( pAny->iType & asTYPEID_MASK_OBJECT )
	{
		// Create a copy of the object
		pAny->pVal = g_engine->CreateScriptObjectCopy(pUserData, g_engine->GetObjectTypeById(pAny->iType));
	}
	else
	{
		// Primitives can be copied directly
		pAny->pVal = 0;

		// Copy the primitive value
		// We receive a pointer to the value.
		int size = g_engine->GetSizeOfPrimitiveType(pAny->iType);
		memcpy(&pAny->pVal, pUserData, size);
	}
    s.SetUserData(pAny);
}

void SetUserData_b2Body_Int(asINT64 &value, b2Body &s)
{
    SetUserData_b2Body(&value, asTYPEID_INT64, s);
}

void SetUserData_b2Body_Double(double &value, b2Body &s)
{
    SetUserData_b2Body(&value, asTYPEID_DOUBLE, s);
}
*/


static void* GetUserDataAddress_b2Body(b2Body &s)
{
    return s.GetUserData();
}


void Register_b2Body(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    int r;

    //
    RegisterRefDummy<b2Body>(engine, "b2Body");

    r = engine->RegisterObjectMethod("b2Body", 
        "b2Fixture@ CreateFixture(const b2FixtureDef& in)" , 
        asMETHODPR(b2Body,CreateFixture, (const b2FixtureDef*), b2Fixture*), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "b2Fixture@ CreateFixture(const b2Shape& in, float)" , 
        asMETHODPR(b2Body,CreateFixture, (const b2Shape*, float32), b2Fixture*), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "void DestroyFixture(b2Fixture& in) " , 
        asMETHOD(b2Body,DestroyFixture), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "void SetTransform(const b2Vec2& in, float) " , 
        asMETHOD(b2Body,SetTransform), asCALL_THISCALL); 
        assert( r >= 0 );

    //const b2Transform& GetTransform() const;

    r = engine->RegisterObjectMethod("b2Body", 
        "const b2Vec2& GetPosition() const " , 
        asMETHOD(b2Body,GetPosition), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "float GetAngle() const " , 
        asMETHOD(b2Body,GetAngle), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "const b2Vec2& GetWorldCenter() const" , 
        asMETHOD(b2Body,GetWorldCenter), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "const b2Vec2& GetLocalCenter() const" , 
        asMETHOD(b2Body,GetLocalCenter), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "void SetLinearVelocity(const b2Vec2& in)" , 
        asMETHOD(b2Body,SetLinearVelocity), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "b2Vec2 GetLinearVelocity() const" , 
        asMETHOD(b2Body,GetLinearVelocity), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "void SetAngularVelocity(const float& in)" , 
        asMETHOD(b2Body,SetAngularVelocity), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "float GetAngularVelocity() const" , 
        asMETHOD(b2Body,GetAngularVelocity), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "void ApplyForce(const b2Vec2& in, const b2Vec2& in)" , 
        asMETHOD(b2Body,ApplyForce), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "void ApplyForceToCenter(const b2Vec2& in)" , 
        asMETHOD(b2Body,ApplyForceToCenter), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "void ApplyTorque(const float& in)" , 
        asMETHOD(b2Body,ApplyTorque), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "void ApplyLinearImpulse(const b2Vec2& in, const b2Vec2& in)" , 
        asMETHOD(b2Body,ApplyLinearImpulse), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "void ApplyAngularImpulse(const float& in)" , 
        asMETHOD(b2Body,ApplyAngularImpulse), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "float GetMass() const" , 
        asMETHOD(b2Body,GetMass), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "float GetInertia() const" , 
        asMETHOD(b2Body,GetInertia), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "void GetMassData(b2MassData& out) const" , 
        asMETHOD(b2Body,GetMassData), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "void SetMassData(const b2MassData& in)" , 
        asMETHOD(b2Body,SetMassData), asCALL_THISCALL); 
        assert( r >= 0 );
	
    r = engine->RegisterObjectMethod("b2Body", 
        "void ResetMassData()" , 
        asMETHOD(b2Body,ResetMassData), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "b2Vec2 GetWorldPoint(const b2Vec2 &in) const" , 
        asMETHOD(b2Body,GetWorldPoint), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "b2Vec2 GetWorldVector(const b2Vec2 &in) const" , 
        asMETHOD(b2Body,GetWorldVector), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "b2Vec2 GetLocalPoint(const b2Vec2 &in) const" , 
        asMETHOD(b2Body,GetLocalPoint), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "b2Vec2 GetLocalVector(const b2Vec2 &in) const" , 
        asMETHOD(b2Body,GetLocalVector), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "b2Vec2 GetLinearVelocityFromWorldPoint(const b2Vec2 &in) const" , 
        asMETHOD(b2Body,GetLinearVelocityFromWorldPoint), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "b2Vec2 GetLinearVelocityFromLocalPoint(const b2Vec2 &in) const" , 
        asMETHOD(b2Body,GetLinearVelocityFromLocalPoint), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "float GetLinearDamping() const" , 
        asMETHOD(b2Body,GetLinearDamping), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "void SetLinearDamping(float)" , 
        asMETHOD(b2Body,SetLinearDamping), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "float GetAngularDamping() const" , 
        asMETHOD(b2Body,GetAngularDamping), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "void SetAngularDamping(float)" , 
        asMETHOD(b2Body,SetAngularDamping), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "float GetGravityScale() const" , 
        asMETHOD(b2Body,GetGravityScale), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "void SetGravityScale(float)" , 
        asMETHOD(b2Body,SetGravityScale), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "void SetType(b2BodyType &in)" , 
        asMETHOD(b2Body,SetType), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "b2BodyType GetType() const" , 
        asMETHOD(b2Body,GetType), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "void SetBullet(bool& in)" , 
        asMETHOD(b2Body,SetBullet), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "bool IsBullet() const" , 
        asMETHOD(b2Body,IsBullet), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "void SetSleepingAllowed(bool& in)" , 
        asMETHOD(b2Body,SetSleepingAllowed), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "bool IsSleepingAllowed() const" , 
        asMETHOD(b2Body,IsSleepingAllowed), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "void SetAwake(bool& in)" , 
        asMETHOD(b2Body,SetAwake), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "bool IsAwake() const" , 
        asMETHOD(b2Body,IsAwake), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "void SetActive(bool& in)" , 
        asMETHOD(b2Body,SetActive), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "bool IsActive() const" , 
        asMETHOD(b2Body,IsActive), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "void SetFixedRotation(bool& in)" , 
        asMETHOD(b2Body,SetFixedRotation), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "bool IsFixedRotation() const" , 
        asMETHOD(b2Body,IsFixedRotation), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "b2Fixture@ GetFixtureList()" , 
        asMETHODPR(b2Body,GetFixtureList, (), b2Fixture*), asCALL_THISCALL); 
        assert( r >= 0 );

	//b2JointEdge* GetJointList();
	//const b2JointEdge* GetJointList() const;
	
    //b2ContactEdge* GetContactList();
	//const b2ContactEdge* GetContactList() const;
	
    r = engine->RegisterObjectMethod("b2Body", 
        "b2Body@ GetNext()" , 
        asMETHODPR(b2Body,GetNext, (), b2Body*), asCALL_THISCALL); 
        assert( r >= 0 );
	
    //void* GetUserData() const;
        /*
    r = engine->RegisterObjectMethod("b2Body", 
	    "bool GetUserData(?&out) const",
        asFUNCTION(GetUserData_b2Body), asCALL_CDECL_OBJLAST);
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
	    "bool GetUserData(int64&out) const",
        asFUNCTION(GetUserData_b2Body_Int), asCALL_CDECL_OBJLAST);
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
	    "bool GetUserData(double&out) const",
        asFUNCTION(GetUserData_b2Body_Double), asCALL_CDECL_OBJLAST);
        assert( r >= 0 );
        
        r = engine->RegisterObjectMethod("b2Body", 
        "void SetUserData(?&in)" , 
        asFUNCTION(SetUserData_b2Body), asCALL_CDECL_OBJLAST);
        assert( r >= 0 );
	
    r = engine->RegisterObjectMethod("b2Body", 
        "void SetUserData(int64&in)" , 
        asFUNCTION(SetUserData_b2Body_Int), asCALL_CDECL_OBJLAST);
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "void SetUserData(double&in)" , 
        asFUNCTION(SetUserData_b2Body_Double), asCALL_CDECL_OBJLAST);
        assert( r >= 0 );
    */	
        RegisterUserData<b2Body>(engine, "b2Body");

        r = engine->RegisterObjectMethod("b2Body", 
        "b2World@ GetWorld()" , 
        asMETHODPR(b2Body,GetWorld, (), b2World*), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2Body", 
        "void Dump()" , 
        asMETHOD(b2Body,Dump), asCALL_THISCALL); 
        assert( r >= 0 );
}
