#include "stdafx.h"
#include <tchar.h>
#include <Box2D/Box2D.h>
#include <angelscript.h>
#include "../include/ScriptAddin.h"
#include "Box2DCommon.h"



extern bool CALLBACK Conv_b2Vec2( VAL_DATA* pValData, const void* pValAddress, void* pContext);


bool CALLBACK Conv_b2BodyDef( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("b2BodyDef");
    const b2BodyDef* pB2BodyDef = reinterpret_cast<const b2BodyDef*>(pValAddress);
    StdStringStream strmValAll; 
    StdStringStream strmVal; 

    if (!pValAddress)
    {
        pValData->bValid = false;
        return false;
    }
    pValData->bValid = true;
    pValData->bArray = false;

    strmValAll << _T("{");
    
    {
    VAL_DATA valPositon;
    valPositon.bValid = true;
    valPositon.bArray = false;
    valPositon.strName     = _T("position");
    valPositon.strTypeName = _T("b2Vec2");
    Conv_b2Vec2( &valPositon, &pB2BodyDef->position, pContext);
    pValData->lstVal.push_back(valPositon);
    strmValAll << valPositon.strVal<< _T(",");
    }

    {
    VAL_DATA valAngle;
    valAngle.bValid = true;
    valAngle.bArray = false;
    valAngle.strName     = _T("angle");
    valAngle.strTypeName = _T("float");
    strmVal .str(_T(""));
    strmVal << pB2BodyDef->angle;
    valAngle.strVal = strmVal.str();
    pValData->lstVal.push_back(valAngle);
    strmValAll << valAngle.strVal<< _T(",");
    }

    {
    VAL_DATA val;
    val.bValid = true;
    val.bArray = false;
    val.strName     = _T("linearVelocity");
    val.strTypeName = _T("b2Vec2");
    Conv_b2Vec2( &val, &pB2BodyDef->linearVelocity, pContext);
    pValData->lstVal.push_back(val);
    strmValAll << val.strVal << _T(",");
    }

    {
    VAL_DATA val;
    val.bValid = true;
    val.bArray = false;
    val.strName     = _T("angularVelocity");
    val.strTypeName = _T("float");
    strmVal .str(_T(""));
    strmVal << pB2BodyDef->angularVelocity;
    val.strVal = strmVal.str();
    pValData->lstVal.push_back(val);
    strmValAll << val.strVal<< _T(",");
    }

    {
    VAL_DATA val;
    val.bValid = true;
    val.bArray = false;
    val.strName     = _T("linearDamping");
    val.strTypeName = _T("float");
    strmVal .str(_T(""));
    strmVal << pB2BodyDef->linearDamping;
    val.strVal = strmVal.str();
    pValData->lstVal.push_back(val);
    strmValAll << val.strVal<< _T(",");
    }

    {
    VAL_DATA val;
    val.bValid = true;
    val.bArray = false;
    val.strName     = _T("angularDamping");
    val.strTypeName = _T("float");
    strmVal .str(_T(""));
    strmVal << pB2BodyDef->angularDamping;
    val.strVal = strmVal.str();
    pValData->lstVal.push_back(val);
    strmValAll << val.strVal<< _T(",");
    }

    {
    VAL_DATA valType;
    valType.bValid = true;
    valType.bArray = false;
    valType.strName     = _T("type");
    valType.strTypeName = _T("b2BodyType");
    strmVal .str(_T(""));
    strmVal << b2BodyTypeToString(pB2BodyDef->type);
    valType.strVal = strmVal.str();
    pValData->lstVal.push_back(valType);
    strmValAll << valType.strVal<< _T(",");
    }

    {
    VAL_DATA val;
    val.bValid = true;
    val.bArray = false;
    val.strName     = _T("gravityScale");
    val.strTypeName = _T("float");
    strmVal .str(_T(""));
    strmVal << pB2BodyDef->gravityScale;
    val.strVal = strmVal.str();
    pValData->lstVal.push_back(val);
    //strmValAll << val.strVal<< _T(",");
    }

    strmValAll << _T("}");
    pValData->strVal = strmValAll.str();

    return true;
}


void Register_b2BodyDefType(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    int iRet;
    iRet = engine->RegisterObjectType("b2BodyDef", sizeof(b2BodyDef), 
        asOBJ_VALUE | asOBJ_POD | asOBJ_APP_CLASS_CA);
    assert( iRet >= 0 );  
    g_mapValFunc[iRet] = Conv_b2BodyDef;
}

void Register_b2BodyDef(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
	int r;
/*
	r = engine->RegisterObjectBehaviour("b2BodyDef", 
        asBEHAVE_DESTRUCT, "void f()",
        asFUNCTION(Constructor<D>),
        asCALL_CDECL_OBJLAST); assert( r >= 0 );
*/

	// Register the object properties
	r = engine->RegisterObjectProperty("b2BodyDef", "b2BodyType type", offsetof(b2BodyDef, type)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2BodyDef", "b2Vec2 position", offsetof(b2BodyDef, position)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2BodyDef", "float angle", offsetof(b2BodyDef, angle)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2BodyDef", "b2Vec2 linearVelocity", offsetof(b2BodyDef, linearVelocity)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2BodyDef", "float angularVelocity", offsetof(b2BodyDef, angularVelocity)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2BodyDef", "float linearDamping", offsetof(b2BodyDef, linearDamping)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2BodyDef", "float angularDamping", offsetof(b2BodyDef, angularDamping)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2BodyDef", "bool allowSleep", offsetof(b2BodyDef, allowSleep)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2BodyDef", "bool awake", offsetof(b2BodyDef, awake)); assert( r >= 0 );

    r = engine->RegisterObjectProperty("b2BodyDef", "bool fixedRotation", offsetof(b2BodyDef, fixedRotation)); assert( r >= 0 );

    r = engine->RegisterObjectProperty("b2BodyDef", "bool bullet", offsetof(b2BodyDef, bullet)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2BodyDef", "bool active", offsetof(b2BodyDef, active)); assert( r >= 0 );
	//r = engine->RegisterObjectProperty("b2BodyDef", "void* userData", offsetof(b2Vec2, userData)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2BodyDef", "float gravityScale", offsetof(b2BodyDef, gravityScale)); assert( r >= 0 );

	// Register the constructors
    RegisterC<b2BodyDef>(engine, "b2BodyDef");
    RegisterA<b2BodyDef>(engine, "b2BodyDef");
}
