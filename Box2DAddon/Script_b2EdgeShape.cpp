#include "stdafx.h"
#include <tchar.h>
#include <Box2D/Box2D.h>
#include <angelscript.h>
#include "../include/ScriptAddin.h"
#include "Box2DCommon.h"

extern bool CALLBACK Conv_b2Vec2( VAL_DATA* pValData, const void* pValAddress, void* pContext);


bool CALLBACK Conv_b2EdgeShape( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("b2EdgeShape");
    const b2EdgeShape* pShape = reinterpret_cast<const b2EdgeShape*>(pValAddress);
    StdStringStream strmValAll; 
    StdStringStream strmVal; 

    if (!pValAddress)
    {
        pValData->bValid = false;
        return false;
    }
    pValData->bValid = true;
    pValData->bArray = false;

    strmValAll << _T("{");

    {
    VAL_DATA valType;
    valType.bValid = true;
    valType.bArray = false;
    valType.strName     = _T("type");
    valType.strTypeName = _T("b2ShapeType");
    strmVal .str(_T(""));
    strmVal << b2ShapeTypeToString(pShape->GetType());
    valType.strVal = strmVal.str();
    pValData->lstVal.push_back(valType);
    strmValAll << valType.strVal<< _T(",");
    }

    {
    VAL_DATA val;
    val.bValid = true;
    val.bArray = false;
    val.strName     = _T("radius");
    val.strTypeName = _T("float");
    strmVal .str(_T(""));
    strmVal << pShape->m_radius;
    val.strVal = strmVal.str();
    pValData->lstVal.push_back(val);
    strmValAll << val.strVal<< _T(",");
    }


    {
        VAL_DATA val;
        val.bValid = true;
        val.bArray = false;
        val.strName     = _T("vertex0");
        val.strTypeName = _T("b2Vec2");
        Conv_b2Vec2( &val, &pShape->m_vertex0, pContext);
        pValData->lstVal.push_back(val);
        strmValAll << val.strVal<< _T(",");
    }

    {
        VAL_DATA val;
        val.bValid = true;
        val.bArray = false;
        val.strName     = _T("vertex3");
        val.strTypeName = _T("b2Vec2");
        Conv_b2Vec2( &val, &pShape->m_vertex3, pContext);
        pValData->lstVal.push_back(val);
        strmValAll << val.strVal<< _T(",");
    }

    {
        VAL_DATA val;
        val.bValid = true;
        val.bArray = false;
        val.strName     = _T("hasVertex0");
        val.strTypeName = _T("bool");
        strmVal .str(_T(""));
        strmVal << (pShape->m_hasVertex0 ? _T("true"): _T("false"));
        val.strVal = strmVal.str();

        pValData->lstVal.push_back(val);
        strmValAll << val.strVal<< _T(",");
    }

    {
        VAL_DATA val;
        val.bValid = true;
        val.bArray = false;
        val.strName     = _T("hasVertex3");
        val.strTypeName = _T("bool");
        strmVal .str(_T(""));
        strmVal << (pShape->m_hasVertex3 ? _T("true"): _T("false"));
        val.strVal = strmVal.str();

        pValData->lstVal.push_back(val);
        //strmValAll << val.strVal<< _T(",");
    }

    strmValAll << _T("}");
    pValData->strVal = strmValAll.str();
    return true;
}


void Register_b2EdgeShapeType(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    int iRet;
    iRet = engine->RegisterObjectType("b2EdgeShape", 0, asOBJ_REF );
    assert( iRet >= 0 );  
    g_mapValFunc[iRet] = Conv_b2EdgeShape;
}

void Register_b2EdgeShape(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    int r;

    // Register the object properties
    RegisterTemplate_b2Shape<b2EdgeShape>( engine, "b2EdgeShape");
    RegisterFactory<b2EdgeShape>(engine, "b2EdgeShape");

    r = engine->RegisterObjectProperty("b2EdgeShape", 
        "b2Vec2 m_vertex0", offsetof(b2EdgeShape, m_vertex0));
        assert( r >= 0 );

    r = engine->RegisterObjectProperty("b2EdgeShape", 
        "b2Vec2 m_vertex1", offsetof(b2EdgeShape, m_vertex1));
        assert( r >= 0 );

    r = engine->RegisterObjectProperty("b2EdgeShape", 
        "b2Vec2 m_vertex2", offsetof(b2EdgeShape, m_vertex2));
        assert( r >= 0 );

    r = engine->RegisterObjectProperty("b2EdgeShape", 
        "b2Vec2 m_vertex3", offsetof(b2EdgeShape, m_vertex3));
        assert( r >= 0 );

    r = engine->RegisterObjectProperty("b2EdgeShape", 
        "bool m_hasVertex0", offsetof(b2EdgeShape, m_hasVertex0));
        assert( r >= 0 );

    r = engine->RegisterObjectProperty("b2EdgeShape", 
        "bool m_hasVertex3", offsetof(b2EdgeShape, m_hasVertex3));
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2EdgeShape", 
        "void Set(const b2Vec2 &in, const b2Vec2 &in)" , 
        asMETHOD(b2EdgeShape,Set), asCALL_THISCALL); 
        assert( r >= 0 );

}
