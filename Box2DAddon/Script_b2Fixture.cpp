#include "stdafx.h"
#include <tchar.h>
#include <Box2D/Box2D.h>
#include <angelscript.h>
#include "../include/ScriptAddin.h"
#include "Box2DCommon.h"

extern bool CALLBACK Conv_b2FixtureDef( VAL_DATA* pValData, const void* pValAddress, void* pContext);

bool CALLBACK Conv_b2Fixture( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("b2Fixture");
    const b2Fixture* pFixture = reinterpret_cast<const b2Fixture*>(pValAddress);
    StdStringStream strmValAll; 
    StdStringStream strmVal; 

    if (!pValAddress)
    {
        pValData->bValid = false;
        return false;
    }
    pValData->bValid = true;
    pValData->bArray = false;

    strmValAll << _T("{");
    if (pFixture == NULL)
    {
        strmValAll << _T("NULL");
        strmValAll << _T("}");
        pValData->strVal = strmValAll.str();
        return true;
    }

    {
    VAL_DATA val;
    val.bValid = true;
    val.bArray = false;
    val.strName     = _T("next");
    val.strTypeName = _T("b2Fixture*");

    strmVal .str(_T(""));
    strmVal << std::hex << std::showbase << pFixture->GetNext();
    strmVal << _T(",{");
    Conv_b2Fixture( &val, pFixture->GetNext(), pContext);
    strmVal << _T("},");
    val.strVal = strmVal.str();
    pValData->lstVal.push_back(val);
    }

/*
    {
    VAL_DATA val;
    val.strName     = _T("body");
    val.strTypeName = _T("b2Body*");

    strmVal .str(_T(""));
    strmVal << std::hex << std::showbase << pFixture->GetBody();
    strmVal << _T(",{");
    Conv_b2Body( &val, pFixture->GetBody(), pContext);
    strmVal << _T("},");
    val.strVal = strmVal.str();
    pValData->lstVal.push_back(val);
    }
*/
	//b2Shape* m_shape;


    {
    VAL_DATA val;
    val.strName     = _T("density");
    val.strTypeName = _T("float");
    strmVal .str(_T(""));
    strmVal << pFixture->GetDensity();
    val.strVal = strmVal.str();
    pValData->lstVal.push_back(val);
    strmValAll << val.strVal<< _T(",");
    }
	
    {
    VAL_DATA val;
    val.strName     = _T("friction");
    val.strTypeName = _T("float");
    strmVal .str(_T(""));
    strmVal << pFixture->GetFriction();
    val.strVal = strmVal.str();
    pValData->lstVal.push_back(val);
    strmValAll << val.strVal<< _T(",");
    }

    {
    VAL_DATA val;
    val.strName     = _T("restitution");
    val.strTypeName = _T("float");
    strmVal .str(_T(""));
    strmVal << pFixture->GetRestitution();
    val.strVal = strmVal.str();
    pValData->lstVal.push_back(val);
    strmValAll << val.strVal<< _T(",");
    }


    {
    VAL_DATA val;
    val.strName     = _T("isSensor");
    val.strTypeName = _T("bool");
    strmVal .str(_T(""));
    strmVal << (pFixture->IsSensor() ? _T("true"): _T("false"));
    val.strVal = strmVal.str();

    pValData->lstVal.push_back(val);
    //strmValAll << val.strVal<< _T(",");
    }

    strmValAll << _T("}");
    pValData->strVal = strmValAll.str();
    return true;
}

void Register_b2FixtureType(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    int iRet;
    iRet = engine->RegisterObjectType("b2Fixture", 0, asOBJ_REF);
    assert( iRet >= 0 );  
    g_mapValFunc[iRet] = Conv_b2FixtureDef;
}

void Register_b2Fixture(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    int r;
    RegisterRef<b2Fixture>( engine, "b2Fixture");

    //インスタンス化不可
    //RegisterFactory<b2Fixture>(engine, "b2Fixture");
   
   r = engine->RegisterObjectMethod("b2Fixture", 
        "b2ShapeType GetType() const" , 
        asMETHOD(b2Fixture,GetType), asCALL_THISCALL); 
        assert( r >= 0 );

   r = engine->RegisterObjectMethod("b2Fixture", 
        "b2Shape@ GetShape()" , 
        asMETHODPR(b2Fixture,GetShape,(),b2Shape*), asCALL_THISCALL); 
        assert( r >= 0 );

   r = engine->RegisterObjectMethod("b2Fixture", 
        "void SetSensor(bool &in)" , 
        asMETHOD(b2Fixture,SetSensor), asCALL_THISCALL); 
        assert( r >= 0 );

   r = engine->RegisterObjectMethod("b2Fixture", 
        "bool IsSensor()" , 
        asMETHOD(b2Fixture,IsSensor), asCALL_THISCALL); 
        assert( r >= 0 );

    //void SetFilterData(const b2Filter& filter);

    //const b2Filter& GetFilterData() const;

   r = engine->RegisterObjectMethod("b2Fixture", 
        "void Refilter()" , 
        asMETHOD(b2Fixture,Refilter), asCALL_THISCALL); 
        assert( r >= 0 );

   r = engine->RegisterObjectMethod("b2Fixture", 
        "b2Body@ GetBody()" , 
        asMETHODPR(b2Fixture,GetBody, (), b2Body*), asCALL_THISCALL); 
        assert( r >= 0 );

/* TODO:原因調査
   r = engine->RegisterObjectMethod("b2Fixture", 
        "b2Fixture@ GetNext()" , 
        asMETHODPR(b2Fixture,GetNext, (), b2Fixture*), asCALL_THISCALL); 
        assert( r >= 0 );
*/
    //void* GetUserData() const;

    //void SetUserData(void* data);

   r = engine->RegisterObjectMethod("b2Fixture", 
        "bool TestPoint(const b2Vec2& in) " , 
        asMETHOD(b2Fixture,TestPoint), asCALL_THISCALL); 
        assert( r >= 0 );

    //bool RayCast(b2RayCastOutput* output, const b2RayCastInput& input, int32 childIndex) const;

   r = engine->RegisterObjectMethod("b2Fixture", 
        "void GetMassData(b2MassData& out)" , 
        asMETHOD(b2Fixture,GetMassData), asCALL_THISCALL); 
        assert( r >= 0 );

   r = engine->RegisterObjectMethod("b2Fixture", 
        "void SetDensity(float& in) " , 
        asMETHOD(b2Fixture,SetDensity), asCALL_THISCALL); 
        assert( r >= 0 );

   r = engine->RegisterObjectMethod("b2Fixture", 
        "float GetDensity() " , 
        asMETHOD(b2Fixture,GetDensity), asCALL_THISCALL); 
        assert( r >= 0 );

   r = engine->RegisterObjectMethod("b2Fixture", 
        "void SetFriction(float& in) " , 
        asMETHOD(b2Fixture,SetFriction), asCALL_THISCALL); 
        assert( r >= 0 );

   r = engine->RegisterObjectMethod("b2Fixture", 
        "float GetFriction() " , 
        asMETHOD(b2Fixture,GetFriction), asCALL_THISCALL); 
        assert( r >= 0 );

   r = engine->RegisterObjectMethod("b2Fixture", 
        "void SetRestitution(float& in) " , 
        asMETHOD(b2Fixture,SetRestitution), asCALL_THISCALL); 
        assert( r >= 0 );

   r = engine->RegisterObjectMethod("b2Fixture", 
        "float GetRestitution() " , 
        asMETHOD(b2Fixture,GetRestitution), asCALL_THISCALL); 
        assert( r >= 0 );

   //const b2AABB& GetAABB(int32 childIndex) const;
   r = engine->RegisterObjectMethod("b2Fixture", 
        "void Dump(int& in) " , 
        asMETHOD(b2Fixture,Dump), asCALL_THISCALL); 
        assert( r >= 0 );
}
