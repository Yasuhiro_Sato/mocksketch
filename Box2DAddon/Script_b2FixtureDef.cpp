#include "stdafx.h"
#include <tchar.h>
#include <Box2D/Box2D.h>
#include <angelscript.h>
#include "../include/ScriptAddin.h"
#include "Box2DCommon.h"

extern bool CALLBACK Conv_b2Vec2( VAL_DATA* pValData, const void* pValAddress, void* pContext);

bool CALLBACK Conv_b2FixtureDef( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("b2FixtureDef");
    const b2FixtureDef* pFixtureDef = reinterpret_cast<const b2FixtureDef*>(pValAddress);
    StdStringStream strmValAll; 
    StdStringStream strmVal; 

    if (!pValAddress)
    {
        pValData->bValid = false;
        return false;
    }
    pValData->bValid = true;
    pValData->bArray = false;

    strmValAll << _T("{");
    if (pFixtureDef == NULL)
    {
        strmValAll << _T("NULL");
        strmValAll << _T("}");
        pValData->strVal = strmValAll.str();
        return true;
    }


    //shape = NULL;
	//userData = NULL;
    //b2Filter filter

    {
    VAL_DATA val;
    val.bValid = true;
    val.bArray = false;
    val.strName     = _T("friction");
    val.strTypeName = _T("float");
    strmVal .str(_T(""));
    strmVal << pFixtureDef->friction;
    val.strVal = strmVal.str();
    pValData->lstVal.push_back(val);
    strmValAll << val.strVal<< _T(",");
    }
	
    {
    VAL_DATA val;
    val.bValid = true;
    val.bArray = false;
    val.strName     = _T("restitution");
    val.strTypeName = _T("float");
    strmVal .str(_T(""));
    strmVal << pFixtureDef->restitution;
    val.strVal = strmVal.str();
    pValData->lstVal.push_back(val);
    strmValAll << val.strVal<< _T(",");
    }

    {
    VAL_DATA val;
    val.bValid = true;
    val.bArray = false;
    val.strName     = _T("density");
    val.strTypeName = _T("float");
    strmVal .str(_T(""));
    strmVal << pFixtureDef->density;
    val.strVal = strmVal.str();
    pValData->lstVal.push_back(val);
    strmValAll << val.strVal<< _T(",");
    }


    {
    VAL_DATA val;
    val.bValid = true;
    val.bArray = false;
    val.strName     = _T("isSensor");
    val.strTypeName = _T("bool");
    strmVal .str(_T(""));
    strmVal << (pFixtureDef->isSensor ? _T("true"): _T("false"));
    val.strVal = strmVal.str();

    pValData->lstVal.push_back(val);
    //strmValAll << val.strVal<< _T(",");
    }

    strmValAll << _T("}");
    pValData->strVal = strmValAll.str();
    return true;
}

void Register_b2FixtureDefType(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    int iRet;
    iRet = engine->RegisterObjectType("b2FixtureDef", 
        sizeof(b2FixtureDef), 
        asOBJ_VALUE | asOBJ_POD | asOBJ_APP_CLASS_CA);
    assert( iRet >= 0 );  
    g_mapValFunc[iRet] = Conv_b2FixtureDef;
}

void Register_b2FixtureDef(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
	int r;
    RegisterC<b2FixtureDef>(engine, "b2FixtureDef");
    RegisterA<b2FixtureDef>(engine, "b2FixtureDef");

	// Register the object properties
	r = engine->RegisterObjectProperty(
        "b2FixtureDef", 
        "b2Shape@ shape", 
        offsetof(b2FixtureDef, shape)); 
        assert( r >= 0 );

    //r = engine->RegisterObjectProperty("b2FixtureDef", 
    //    "const void* userData",
    //        offsetof(b2FixtureDef, userData));
    //    assert( r >= 0 );

    r = engine->RegisterObjectProperty(
        "b2FixtureDef", 
        "float friction", 
        offsetof(b2FixtureDef, friction)); 
        assert( r >= 0 );

    r = engine->RegisterObjectProperty(
        "b2FixtureDef", 
        "float restitution", 
        offsetof(b2FixtureDef, restitution));
        assert( r >= 0 );

    r = engine->RegisterObjectProperty(
        "b2FixtureDef",
        "float density",
        offsetof(b2FixtureDef, density)); 
        assert( r >= 0 );

    r = engine->RegisterObjectProperty(
        "b2FixtureDef", 
        "bool isSensor", 
        offsetof(b2FixtureDef, isSensor));
        assert( r >= 0 );

}