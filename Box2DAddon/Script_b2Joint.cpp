#include "stdafx.h"
#include <tchar.h>
#include <Box2D/Box2D.h>
#include <angelscript.h>
#include "../include/ScriptAddin.h"
#include "Box2DCommon.h"
#include "Script_b2Joint.h"


//FOR DEBUG
void Dump_b2Joint(b2Joint *pJoint)
{
    b2DistanceJoint* pDistant;
    pDistant = dynamic_cast<b2DistanceJoint*>(pJoint);

    b2JointType eType = pJoint->GetType();
    b2Body* BodyA = pJoint->GetBodyA();
    b2Body* BodyB = pJoint->GetBodyB();

}

bool CALLBACK Conv_b2Joint( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("b2Joint");
    const b2Joint* pJoint = reinterpret_cast<const b2Joint*>(pValAddress);
    StdStringStream strmValAll; 
    StdStringStream strmVal; 

    if (!pValAddress)
    {
        pValData->bValid = false;
        return false;
    }
    pValData->bValid = true;
    pValData->bArray = false;

    strmValAll << _T("{");
    return true;
}

void Register_b2JointType(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    int iRet;
    iRet = engine->RegisterObjectType("b2Joint", 0, asOBJ_REF );
    assert( iRet >= 0 );  
    g_mapValFunc[iRet]  = Conv_b2Joint;
}


void Register_b2Joint(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    RegisterTemplate_b2Joint<b2Joint>(engine, "b2Joint");



}


////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////


bool CALLBACK Conv_b2RevoluteJoint( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("b2RevoluteJoint");
    const b2RevoluteJoint* pJoint = reinterpret_cast<const b2RevoluteJoint*>(pValAddress);
    StdStringStream strmValAll; 
    StdStringStream strmVal; 

    if (!pValAddress)
    {
        pValData->bValid = false;
        return false;
    }
    pValData->bValid = true;
    pValData->bArray = false;

    strmValAll << _T("{");
    return true;
}

void Register_b2RevoluteJointType(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    int iRet;
    iRet = engine->RegisterObjectType("b2RevoluteJoint", 0, asOBJ_REF );
    assert( iRet >= 0 );  
    g_mapValFunc[iRet]  = Conv_b2RevoluteJoint;
}


void Register_b2RevoluteJoint(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    RegisterTemplate_b2Joint<b2RevoluteJoint>(engine, "b2RevoluteJoint");

    int r;
    r = engine->RegisterObjectMethod( "b2RevoluteJoint", 
        "b2Vec2 GetLocalAnchorA() const" , 
        asMETHOD( b2RevoluteJoint, GetLocalAnchorA), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2RevoluteJoint", 
        "b2Vec2 GetLocalAnchorB() const" , 
        asMETHOD( b2RevoluteJoint, GetLocalAnchorB), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2RevoluteJoint", 
        "float GetReferenceAngle() const" , 
        asMETHOD( b2RevoluteJoint, GetReferenceAngle), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2RevoluteJoint", 
        "float GetJointAngle() const" , 
        asMETHOD( b2RevoluteJoint, GetJointAngle), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2RevoluteJoint", 
        "float GetJointSpeed() const" , 
        asMETHOD( b2RevoluteJoint, GetJointSpeed), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2RevoluteJoint", 
        "bool IsLimitEnabled() const" , 
        asMETHOD( b2RevoluteJoint, IsLimitEnabled), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2RevoluteJoint", 
        "void EnableLimit(bool)" , 
        asMETHOD( b2RevoluteJoint, EnableLimit), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2RevoluteJoint", 
        "float GetLowerLimit() const" , 
        asMETHOD( b2RevoluteJoint, GetLowerLimit), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2RevoluteJoint", 
        "float GetUpperLimit() const" , 
        asMETHOD( b2RevoluteJoint, GetUpperLimit), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2RevoluteJoint", 
        "void GetUpperLimit(float, float) const" , 
        asMETHOD( b2RevoluteJoint, SetLimits), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2RevoluteJoint", 
        "bool IsMotorEnabled() const" , 
        asMETHOD( b2RevoluteJoint, IsMotorEnabled), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2RevoluteJoint", 
        "void EnableMotor(bool)" , 
        asMETHOD( b2RevoluteJoint, EnableMotor), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2RevoluteJoint", 
        "void SetMotorSpeed(float)" , 
        asMETHOD( b2RevoluteJoint, SetMotorSpeed), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2RevoluteJoint", 
        "float GetMotorSpeed() const" , 
        asMETHOD( b2RevoluteJoint, GetMotorSpeed), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2RevoluteJoint", 
        "void SetMaxMotorTorque(float)" , 
        asMETHOD( b2RevoluteJoint, SetMaxMotorTorque), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2RevoluteJoint", 
        "float GetMaxMotorTorque() const" , 
        asMETHOD( b2RevoluteJoint, GetMaxMotorTorque), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2RevoluteJoint", 
        "float GetMotorTorque(float) const" , 
        asMETHOD( b2RevoluteJoint, GetMotorTorque), asCALL_THISCALL); 
}

////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////


bool CALLBACK Conv_b2DistanceJoint( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("b2DistanceJoint");
    const b2DistanceJoint* pJoint = reinterpret_cast<const b2DistanceJoint*>(pValAddress);
    StdStringStream strmValAll; 
    StdStringStream strmVal; 

    if (!pValAddress)
    {
        pValData->bValid = false;
        return false;
    }
    pValData->bValid = true;
    pValData->bArray = false;

    strmValAll << _T("{");
    return true;
}

void Register_b2DistanceJointType(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    int iRet;
    iRet = engine->RegisterObjectType("b2DistanceJoint", 0, asOBJ_REF );
    assert( iRet >= 0 );  
    g_mapValFunc[iRet]  = Conv_b2DistanceJoint;
}


void Register_b2DistanceJoint(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    RegisterTemplate_b2Joint<b2DistanceJoint>(engine, "b2DistanceJoint");

    int r;
    r = engine->RegisterObjectMethod( "b2DistanceJoint", 
        "b2Vec2 GetLocalAnchorA() const" , 
        asMETHOD( b2DistanceJoint, GetLocalAnchorA), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2DistanceJoint", 
        "b2Vec2 GetLocalAnchorB() const" , 
        asMETHOD( b2DistanceJoint, GetLocalAnchorB), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2DistanceJoint", 
        "void SetLength(float)" , 
        asMETHOD( b2DistanceJoint, SetLength), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2DistanceJoint", 
        "float GetLength() const" , 
        asMETHOD( b2DistanceJoint, GetLength), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2DistanceJoint", 
        "void SetFrequency(float)" , 
        asMETHOD( b2DistanceJoint, SetFrequency), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2DistanceJoint", 
        "float GetFrequency() const" , 
        asMETHOD( b2DistanceJoint, GetFrequency), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2DistanceJoint", 
        "void SetDampingRatio(float)" , 
        asMETHOD( b2DistanceJoint, SetDampingRatio), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2DistanceJoint", 
        "float GetDampingRatio() const" , 
        asMETHOD( b2DistanceJoint, GetDampingRatio), asCALL_THISCALL); 
}



////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////


bool CALLBACK Conv_b2PrismaticJoint( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("b2PrismaticJoint");
    const b2PrismaticJoint* pJoint = reinterpret_cast<const b2PrismaticJoint*>(pValAddress);
    StdStringStream strmValAll; 
    StdStringStream strmVal; 

    if (!pValAddress)
    {
        pValData->bValid = false;
        return false;
    }
    pValData->bValid = true;
    pValData->bArray = false;

    strmValAll << _T("{");
    return true;
}

void Register_b2PrismaticJointType(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    int iRet;
    iRet = engine->RegisterObjectType("b2PrismaticJoint", 0, asOBJ_REF );
    assert( iRet >= 0 );  
    g_mapValFunc[iRet]  = Conv_b2PrismaticJoint;
}


void Register_b2PrismaticJoint(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    RegisterTemplate_b2Joint<b2PrismaticJoint>(engine, "b2PrismaticJoint");

    int r;

    /*
    r = engine->RegisterObjectMethod( "b2PrismaticJoint", 
        "b2Vec2 GetAnchorA() const" , 
        asMETHOD( b2PrismaticJoint, GetAnchorA), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2PrismaticJoint", 
        "b2Vec2 GetAnchorB() const" , 
        asMETHOD( b2PrismaticJoint, GetAnchorB), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2PrismaticJoint", 
        "b2Vec2 GetReactionForce(float) const" , 
        asMETHOD( b2PrismaticJoint, GetReactionForce), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2PrismaticJoint", 
        "float GetReactionTorque(float) const" , 
        asMETHOD( b2PrismaticJoint, GetReactionTorque), asCALL_THISCALL); 
    */
    r = engine->RegisterObjectMethod( "b2PrismaticJoint", 
        "const b2Vec2 GetLocalAnchorA() const" , 
        asMETHOD( b2PrismaticJoint, GetLocalAnchorA), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2PrismaticJoint", 
        "const b2Vec2 GetLocalAnchorB() const" , 
        asMETHOD( b2PrismaticJoint, GetLocalAnchorB), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2PrismaticJoint", 
        "const b2Vec2 GetLocalAxisA() const" , 
        asMETHOD( b2PrismaticJoint, GetLocalAxisA), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2PrismaticJoint", 
        "float GetReferenceAngle() const" , 
        asMETHOD( b2PrismaticJoint, GetReferenceAngle), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2PrismaticJoint", 
        "float GetJointTranslation() const" , 
        asMETHOD( b2PrismaticJoint, GetJointTranslation), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2PrismaticJoint", 
        "float GetJointSpeed() const" , 
        asMETHOD( b2PrismaticJoint, GetJointSpeed), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2PrismaticJoint", 
        "bool IsLimitEnabled() const" , 
        asMETHOD( b2PrismaticJoint, IsLimitEnabled), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2PrismaticJoint", 
        "void EnableLimit(bool)" , 
        asMETHOD( b2PrismaticJoint, EnableLimit), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2PrismaticJoint", 
        "float GetLowerLimit() const" , 
        asMETHOD( b2PrismaticJoint, GetLowerLimit), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2PrismaticJoint", 
        "float GetUpperLimit() const" , 
        asMETHOD( b2PrismaticJoint, GetUpperLimit), asCALL_THISCALL);

    r = engine->RegisterObjectMethod( "b2PrismaticJoint", 
        "void SetLimits(float, float)" , 
        asMETHOD( b2PrismaticJoint, SetLimits), asCALL_THISCALL);

    r = engine->RegisterObjectMethod( "b2PrismaticJoint", 
        "bool IsMotorEnabled() const" , 
        asMETHOD( b2PrismaticJoint, IsMotorEnabled), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2PrismaticJoint", 
        "void EnableMotor(bool)" , 
        asMETHOD( b2PrismaticJoint, EnableMotor), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2PrismaticJoint", 
        "void SetMotorSpeed(float)" , 
        asMETHOD( b2PrismaticJoint, SetMotorSpeed), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2PrismaticJoint", 
        "float GetMotorSpeed() const" , 
        asMETHOD( b2PrismaticJoint, GetMotorSpeed), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2PrismaticJoint", 
        "float GetMaxMotorForce() const" , 
        asMETHOD( b2PrismaticJoint, GetMaxMotorForce), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2PrismaticJoint", 
        "float GetMotorForce(float) const" , 
        asMETHOD( b2PrismaticJoint, GetMotorForce), asCALL_THISCALL); 
}



////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
bool CALLBACK Conv_b2MouseJoint( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("b2MouseJoint");
    const b2MouseJoint* pJoint = reinterpret_cast<const b2MouseJoint*>(pValAddress);
    StdStringStream strmValAll; 
    StdStringStream strmVal; 

    if (!pValAddress)
    {
        pValData->bValid = false;
        return false;
    }
    pValData->bValid = true;
    pValData->bArray = false;

    strmValAll << _T("{");
    return true;
}

void Register_b2MouseJointType(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    int iRet;
    iRet = engine->RegisterObjectType("b2MouseJoint", 0, asOBJ_REF );
    assert( iRet >= 0 );  
    g_mapValFunc[iRet]  = Conv_b2MouseJoint;
}


void Register_b2MouseJoint(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    RegisterTemplate_b2Joint<b2MouseJoint>(engine, "b2MouseJoint");

    int r;
    /*
    r = engine->RegisterObjectMethod( "b2MouseJoint", 
	    "b2Vec2 GetAnchorA() const",
        asMETHOD( b2MouseJoint, GetAnchorA), asCALL_THISCALL); 

	/// Implements b2Joint.
    r = engine->RegisterObjectMethod( "b2MouseJoint", 
	    "b2Vec2 GetAnchorB() const",
        asMETHOD( b2MouseJoint, GetAnchorB), asCALL_THISCALL); 

	/// Implements b2Joint.
    r = engine->RegisterObjectMethod( "b2MouseJoint", 
	    "b2Vec2 GetReactionForce(float) const",
        asMETHOD( b2MouseJoint, GetReactionForce), asCALL_THISCALL); 

	/// Implements b2Joint.
    r = engine->RegisterObjectMethod( "b2MouseJoint", 
	    "float GetReactionTorque(float) const",
        asMETHOD( b2MouseJoint, GetReactionTorque), asCALL_THISCALL); 

    */
	/// Use this to update the target point.
    r = engine->RegisterObjectMethod( "b2MouseJoint", 
    	"void SetTarget(b2Vec2 in)",
        asMETHOD( b2MouseJoint, SetTarget), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2MouseJoint", 
        "const b2Vec2 GetTarget() const",
        asMETHOD( b2MouseJoint, GetTarget), asCALL_THISCALL); 

	/// Set/get the maximum force in Newtons.
	r = engine->RegisterObjectMethod( "b2MouseJoint", 
        "void SetMaxForce(float)",
        asMETHOD( b2MouseJoint, SetMaxForce), asCALL_THISCALL); 
	
    r = engine->RegisterObjectMethod( "b2MouseJoint", 
        "float GetMaxForce() const",
        asMETHOD( b2MouseJoint, GetMaxForce), asCALL_THISCALL); 

	/// Set/get the frequency in Hertz.
	r = engine->RegisterObjectMethod( "b2MouseJoint", 
        "void SetFrequency(float)",
        asMETHOD( b2MouseJoint, SetFrequency), asCALL_THISCALL); 
	
    r = engine->RegisterObjectMethod( "b2MouseJoint", 
        "float GetFrequency() const",
        asMETHOD( b2MouseJoint, GetFrequency), asCALL_THISCALL); 

	/// Set/get the damping ratio (dimensionless).
	r = engine->RegisterObjectMethod( "b2MouseJoint", 
        "void SetDampingRatio(float)",
        asMETHOD( b2MouseJoint, SetDampingRatio), asCALL_THISCALL); 
	
    r = engine->RegisterObjectMethod( "b2MouseJoint", 
        "float GetDampingRatio() const",
        asMETHOD( b2MouseJoint, GetDampingRatio), asCALL_THISCALL); 
}

////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
bool CALLBACK Conv_b2GearJoint( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("b2GearJoint");
    const b2GearJoint* pJoint = reinterpret_cast<const b2GearJoint*>(pValAddress);
    StdStringStream strmValAll; 
    StdStringStream strmVal; 

    if (!pValAddress)
    {
        pValData->bValid = false;
        return false;
    }
    pValData->bValid = true;
    pValData->bArray = false;

    strmValAll << _T("{");
    return true;
}

void Register_b2GearJointType(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    int iRet;
    iRet = engine->RegisterObjectType("b2GearJoint", 0, asOBJ_REF );
    assert( iRet >= 0 );  
    g_mapValFunc[iRet]  = Conv_b2GearJoint;
}


void Register_b2GearJoint(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    RegisterTemplate_b2Joint<b2GearJoint>(engine, "b2GearJoint");

    int r;

    r = engine->RegisterObjectMethod( "b2GearJoint", 
	    "b2Vec2 GetAnchorA() const",
        asMETHOD( b2GearJoint, GetAnchorA), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2GearJoint", 
	    "b2Vec2 GetAnchorB() const",
        asMETHOD( b2GearJoint, GetAnchorB), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2GearJoint", 
	    "b2Vec2 GetReactionForce(float) const",
        asMETHOD( b2GearJoint, GetReactionForce), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2GearJoint", 
	    "float GetReactionTorque(float) const",
        asMETHOD( b2GearJoint, GetReactionTorque), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2GearJoint", 
	    "b2Joint@ GetJoint1()",
        asMETHOD( b2GearJoint, GetJoint1), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2GearJoint", 
	    "b2Joint@ GetJoint2()",
        asMETHOD( b2GearJoint, GetJoint2), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2GearJoint", 
	    "void SetRatio(float) const",
        asMETHOD( b2GearJoint, SetRatio), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2GearJoint", 
	    "float GetRatio() const",
        asMETHOD( b2GearJoint, GetRatio), asCALL_THISCALL); 
}



////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
bool CALLBACK Conv_b2FrictionJoint( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("b2FrictionJoint");
    const b2FrictionJoint* pJoint = reinterpret_cast<const b2FrictionJoint*>(pValAddress);
    StdStringStream strmValAll; 
    StdStringStream strmVal; 

    if (!pValAddress)
    {
        pValData->bValid = false;
        return false;
    }
    pValData->bValid = true;
    pValData->bArray = false;

    strmValAll << _T("{");
    return true;
}

void Register_b2FrictionJointType(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    int iRet;
    iRet = engine->RegisterObjectType("b2FrictionJoint", 0, asOBJ_REF );
    assert( iRet >= 0 );  
    g_mapValFunc[iRet]  = Conv_b2FrictionJoint;
}


void Register_b2FrictionJoint(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    RegisterTemplate_b2Joint<b2FrictionJoint>(engine, "b2FrictionJoint");

    int r;

    r = engine->RegisterObjectMethod( "b2FrictionJoint", 
	    "b2Vec2 GetLocalAnchorA() const",
        asMETHOD( b2FrictionJoint, GetLocalAnchorA), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2FrictionJoint", 
	    "b2Vec2 GetLocalAnchorB() const",
        asMETHOD( b2FrictionJoint, GetLocalAnchorB), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2FrictionJoint", 
	    "void SetMaxForce(float)",
        asMETHOD( b2FrictionJoint, SetMaxForce), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2FrictionJoint", 
	    "float GetMaxForce() const",
        asMETHOD( b2FrictionJoint, GetMaxForce), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2FrictionJoint", 
	    "void SetMaxTorque(float)",
        asMETHOD( b2FrictionJoint, SetMaxTorque), asCALL_THISCALL); 

    r = engine->RegisterObjectMethod( "b2FrictionJoint", 
	    "float GetMaxTorque() const",
        asMETHOD( b2FrictionJoint, GetMaxTorque), asCALL_THISCALL); 
    
}