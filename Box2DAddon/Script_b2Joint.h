/**
 * @brief			ScriptAddin ヘッダーファイル
 * @file			ScriptAddin.h
 * @author			Yasuhiro Sato
 * @date			10-5-2013 10:42:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2011 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef _SCRIPT_B2JOINT_H_
#define _SCRIPT_B2JOINT_H_

template<class D>
void RegisterTemplate_b2Joint(asIScriptEngine *engine, char* className)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    RegisterRefDummy<D>(engine, className);

    //b2Joinはworld.CreateJointのみで生成する。
    //RegisterFactory<D>(engine, className);

    int r;
    r = engine->RegisterObjectMethod( className, 
        "b2JointType GetType() const" , 
        asMETHOD( D, GetType), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod( className, 
        "b2Body@ GetBodyA()" , 
        asMETHOD( D, GetBodyA), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod( className, 
        "b2Body@ GetBodyB()" , 
        asMETHOD( D, GetBodyB), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod( className, 
        "b2Vec2 GetAnchorA() const" , 
        asMETHOD( D, GetAnchorA), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod( className, 
        "b2Vec2 GetAnchorB() const" , 
        asMETHOD( D, GetAnchorB), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod( className, 
        "b2Vec2 GetReactionForce(float) const" , 
        asMETHOD( D, GetReactionForce), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod( className, 
        "float GetReactionTorque(float) const" , 
        asMETHOD( D, GetReactionTorque), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod( className, 
        "b2Joint@ GetNext()" , 
        asMETHODPR(D,GetNext, (), b2Joint*), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod( className, 
        "bool IsActive() const" , 
        asMETHOD( D, IsActive), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod( className, 
        "bool GetCollideConnected() const" , 
        asMETHOD( D, GetCollideConnected), asCALL_THISCALL); 
        assert( r >= 0 );

    RegisterUserData<D>(engine, className);

    //------------------
    // Cast
    //------------------
    if (strcmp( className , "b2Joint") != 0)
    {
        std::stringstream strm;
        strm << className << "@ f()";
        r = engine->RegisterObjectBehaviour("b2Joint", asBEHAVE_REF_CAST , 
            strm.str().c_str(), 
            asFUNCTION((ASRefCast<b2Joint, D>)), 
            asCALL_CDECL_OBJLAST);
        assert( r>=0 );

        r = engine->RegisterObjectBehaviour(className, asBEHAVE_IMPLICIT_REF_CAST, 
            "b2Joint@ f()", 
            asFUNCTION((ASRefCast< D, b2Joint>)), 
            asCALL_CDECL_OBJLAST);
        assert( r>=0 );
    }

    
	//FOR DEBUG
    r = engine->RegisterObjectMethod( className, 
        "void TestDump() const" , 
        asFUNCTION( Dump_b2Joint), asCALL_CDECL_OBJFIRST); 
	//FOR DEBUG
}


#endif //_SCRIPT_B2JOINT_H_
