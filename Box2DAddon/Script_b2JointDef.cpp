#include "stdafx.h"
#include <tchar.h>
#include <Box2D/Box2D.h>
#include <angelscript.h>
#include "../include/ScriptAddin.h"
#include "Box2DCommon.h"
#include "Script_b2JointDef.h"

extern bool CALLBACK Conv_b2World( VAL_DATA* pValData, const void* pValAddress, void* pContext);


bool CALLBACK Conv_b2JointDef( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("b2JointDef");
    const b2JointDef* pJointDef = reinterpret_cast<const b2JointDef*>(pValAddress);
    StdStringStream strmValAll; 
    StdStringStream strmVal; 

    if (!pValAddress)
    {
        pValData->bValid = false;
        return false;
    }
    pValData->bValid = true;
    pValData->bArray = false;

    strmValAll << _T("{");
    return true;
}

void Register_b2JointDefType(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    int iRet;
    iRet = engine->RegisterObjectType("b2JointDef", 0,  asOBJ_REF );
    assert( iRet >= 0 );  
    g_mapValFunc[iRet] = Conv_b2World;
}

void Register_b2JointDef(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}

    RegisterTemplate_b2JointDef<b2JointDef>(engine, "b2JointDef");
}

///////////////////////////////////////////////////////////////////////

bool CALLBACK Conv_b2RevoluteJointDef( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("b2RevoluteJointDef");
    const b2RevoluteJointDef* pJointDef = reinterpret_cast<const b2RevoluteJointDef*>(pValAddress);
    StdStringStream strmValAll; 
    StdStringStream strmVal; 


    if (!pValAddress)
    {
        pValData->bValid = false;
        return false;
    }
    pValData->bValid = true;
    pValData->bArray = false;

    strmValAll << _T("{");
    return true;
}

void Register_b2RevoluteJointDefType(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    int iRet;
    iRet = engine->RegisterObjectType("b2RevoluteJointDef", 0,  asOBJ_REF );
    assert( iRet >= 0 );  
    g_mapValFunc[iRet] = Conv_b2RevoluteJointDef;
}

void Register_b2RevoluteJointDef(asIScriptEngine *engine)
{
    RegisterTemplate_b2JointDef<b2RevoluteJointDef>(engine, "b2RevoluteJointDef");

    int r;
	r = engine->RegisterObjectProperty("b2RevoluteJointDef", "b2Vec2 localAnchorA"  , offsetof(b2RevoluteJointDef, localAnchorA)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2RevoluteJointDef", "b2Vec2 localAnchorB"  , offsetof(b2RevoluteJointDef, localAnchorB)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2RevoluteJointDef", "float  referenceAngle", offsetof(b2RevoluteJointDef, referenceAngle)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2RevoluteJointDef", "bool   enableLimit"   , offsetof(b2RevoluteJointDef, enableLimit)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2RevoluteJointDef", "float  lowerAngle"    , offsetof(b2RevoluteJointDef, lowerAngle)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2RevoluteJointDef", "float  upperAngle"    , offsetof(b2RevoluteJointDef, upperAngle)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2RevoluteJointDef", "bool   enableMotor"   , offsetof(b2RevoluteJointDef, enableMotor)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2RevoluteJointDef", "float  motorSpeed"    , offsetof(b2RevoluteJointDef, motorSpeed)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2RevoluteJointDef", "float  maxMotorTorque", offsetof(b2RevoluteJointDef, maxMotorTorque)); assert( r >= 0 );
}



///////////////////////////////////////////////////////////////////////

bool CALLBACK Conv_b2DistanceJointDef( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("b2DistanceJointDef");
    const b2DistanceJointDef* pJointDef = reinterpret_cast<const b2DistanceJointDef*>(pValAddress);
    StdStringStream strmValAll; 
    StdStringStream strmVal; 

    if (!pValAddress)
    {
        pValData->bValid = false;
        return false;
    }
    pValData->bValid = true;
    pValData->bArray = false;

    strmValAll << _T("{");
    return true;
}

void Register_b2DistanceJointDefType(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    int iRet;
    iRet = engine->RegisterObjectType("b2DistanceJointDef", 0,  asOBJ_REF );
    assert( iRet >= 0 );  
    g_mapValFunc[iRet] = Conv_b2DistanceJointDef;
}

void Register_b2DistanceJointDef(asIScriptEngine *engine)
{
    RegisterTemplate_b2JointDef<b2DistanceJointDef>(engine, "b2DistanceJointDef");

    int r;
	r = engine->RegisterObjectProperty("b2DistanceJointDef", "b2Vec2 localAnchorA"  , offsetof(b2DistanceJointDef, localAnchorA)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2DistanceJointDef", "b2Vec2 localAnchorB"  , offsetof(b2DistanceJointDef, localAnchorB)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2DistanceJointDef", "float length"         , offsetof(b2DistanceJointDef, length)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2DistanceJointDef", "float frequencyHz"    , offsetof(b2DistanceJointDef, frequencyHz)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2DistanceJointDef", "float dampingRatio"   , offsetof(b2DistanceJointDef, dampingRatio)); assert( r >= 0 );
}

///////////////////////////////////////////////////////////////////////

bool CALLBACK Conv_b2PrismaticJointDef( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("b2PrismaticJointDef");
    const b2PrismaticJointDef* pJointDef = reinterpret_cast<const b2PrismaticJointDef*>(pValAddress);
    StdStringStream strmValAll; 
    StdStringStream strmVal; 

    if (!pValAddress)
    {
        pValData->bValid = false;
        return false;
    }
    pValData->bValid = true;
    pValData->bArray = false;

    strmValAll << _T("{");
    return true;
}

void Register_b2PrismaticJointDefType(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    int iRet;
    iRet = engine->RegisterObjectType("b2PrismaticJointDef", 0,  asOBJ_REF );
    assert( iRet >= 0 );  
    g_mapValFunc[iRet] = Conv_b2PrismaticJointDef;
}

void Register_b2PrismaticJointDef(asIScriptEngine *engine)
{
    RegisterTemplate_b2JointDef<b2PrismaticJointDef>(engine, "b2PrismaticJointDef");

    int r;
	r = engine->RegisterObjectProperty("b2PrismaticJointDef", "b2Vec2 localAnchorA"   , offsetof(b2PrismaticJointDef, localAnchorA)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2PrismaticJointDef", "b2Vec2 localAnchorB"   , offsetof(b2PrismaticJointDef, localAnchorB)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2PrismaticJointDef", "b2Vec2 localAxisA"     , offsetof(b2PrismaticJointDef, localAxisA)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2PrismaticJointDef", "float referenceAngle"  , offsetof(b2PrismaticJointDef, referenceAngle)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2PrismaticJointDef", "bool enableLimit"      , offsetof(b2PrismaticJointDef, enableLimit)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2PrismaticJointDef", "float  lowerTranslation"  , offsetof(b2PrismaticJointDef, lowerTranslation)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2PrismaticJointDef", "float  upperTranslation"  , offsetof(b2PrismaticJointDef, upperTranslation)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2PrismaticJointDef", "bool   enableMotor"       , offsetof(b2PrismaticJointDef, enableMotor)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2PrismaticJointDef", "float  maxMotorForce"     , offsetof(b2PrismaticJointDef, maxMotorForce)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2PrismaticJointDef", "float  motorSpeed"        , offsetof(b2PrismaticJointDef, motorSpeed)); assert( r >= 0 );

}

///////////////////////////////////////////////////////////////////////

bool CALLBACK Conv_b2MouseJointDef( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("b2MouseJointDef");
    const b2MouseJointDef* pJointDef = reinterpret_cast<const b2MouseJointDef*>(pValAddress);
    StdStringStream strmValAll; 
    StdStringStream strmVal; 

    if (!pValAddress)
    {
        pValData->bValid = false;
        return false;
    }
    pValData->bValid = true;
    pValData->bArray = false;

    strmValAll << _T("{");
    return true;
}

void Register_b2MouseJointDefType(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    int iRet;
    iRet = engine->RegisterObjectType("b2MouseJointDef", 0,  asOBJ_REF );
    assert( iRet >= 0 );  
    g_mapValFunc[iRet] = Conv_b2MouseJointDef;
}

void Register_b2MouseJointDef(asIScriptEngine *engine)
{
    RegisterTemplate_b2JointDef<b2MouseJointDef>(engine, "b2MouseJointDef");

    int r;
	r = engine->RegisterObjectProperty("b2PrismaticJointDef", "b2Vec2 target"   , offsetof(b2MouseJointDef, target)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2PrismaticJointDef", "float  maxForce"  , offsetof(b2MouseJointDef, maxForce)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2PrismaticJointDef", "float  frequencyHz"  , offsetof(b2MouseJointDef, frequencyHz)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2PrismaticJointDef", "float  dampingRatio"     , offsetof(b2MouseJointDef, dampingRatio)); assert( r >= 0 );

}


///////////////////////////////////////////////////////////////////////

bool CALLBACK Conv_b2GearJointDef( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("b2GearJointDef");
    const b2GearJointDef* pJointDef = reinterpret_cast<const b2GearJointDef*>(pValAddress);
    StdStringStream strmValAll; 
    StdStringStream strmVal; 

    if (!pValAddress)
    {
        pValData->bValid = false;
        return false;
    }
    pValData->bValid = true;
    pValData->bArray = false;

    strmValAll << _T("{");
    return true;
}

void Register_b2GearJointDefType(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    int iRet;
    iRet = engine->RegisterObjectType("b2GearJointDef", 0,  asOBJ_REF );
    assert( iRet >= 0 );  
    g_mapValFunc[iRet] = Conv_b2GearJointDef;
}

void Register_b2GearJointDef(asIScriptEngine *engine)
{
    RegisterTemplate_b2JointDef<b2GearJointDef>(engine, "b2GearJointDef");

    int r;
	r = engine->RegisterObjectProperty("b2GearJointDef", "b2Joint@ joint1"   , offsetof(b2GearJointDef, joint1)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2GearJointDef", "b2Joint@  joint2"  , offsetof(b2GearJointDef, joint2)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2GearJointDef", "float  ratio"  , offsetof(b2GearJointDef, ratio)); assert( r >= 0 );

}


///////////////////////////////////////////////////////////////////////

bool CALLBACK Conv_b2FrictionJointDef( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("b2FrictionJointDef");
    const b2FrictionJointDef* pJointDef = reinterpret_cast<const b2FrictionJointDef*>(pValAddress);
    StdStringStream strmValAll; 
    StdStringStream strmVal; 

    if (!pValAddress)
    {
        pValData->bValid = false;
        return false;
    }
    pValData->bValid = true;
    pValData->bArray = false;

    strmValAll << _T("{");
    return true;
}

void Register_b2FrictionJointDefType(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    int iRet;
    iRet = engine->RegisterObjectType("b2FrictionJointDef", 0,  asOBJ_REF );
    assert( iRet >= 0 );  
    g_mapValFunc[iRet] = Conv_b2FrictionJointDef;
}

void Register_b2FrictionJointDef(asIScriptEngine *engine)
{
    RegisterTemplate_b2JointDef<b2FrictionJointDef>(engine, "b2FrictionJointDef");

    int r;
	r = engine->RegisterObjectProperty("b2FrictionJointDef", "b2Vec2 localAnchorA"   , offsetof(b2FrictionJointDef, localAnchorA)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2FrictionJointDef", "b2Vec2 localAnchorB"   , offsetof(b2FrictionJointDef, localAnchorB)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2FrictionJointDef", "float  maxForce"   , offsetof(b2FrictionJointDef, maxForce)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2FrictionJointDef", "float  maxTorque"  , offsetof(b2FrictionJointDef, maxTorque)); assert( r >= 0 );

}