#ifndef _SCRIPT_B2JOINT_DEF_H_
#define _SCRIPT_B2JOINT_DEF_H_



template<class T>
void RegisterTemplate_b2JointDef(asIScriptEngine *engine, char* className)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    int r;

    RegisterRef<T>(engine, className);
    RegisterFactory<T>(engine, className);

	r = engine->RegisterObjectProperty(className, "b2JointType type", offsetof(T, type)); assert( r >= 0 );
	r = engine->RegisterObjectProperty(className, "b2Body@ bodyA", offsetof(T, bodyA)); assert( r >= 0 );
	r = engine->RegisterObjectProperty(className, "b2Body@ bodyB", offsetof(T, bodyB)); assert( r >= 0 );
	r = engine->RegisterObjectProperty(className, "bool collideConnected", offsetof(T, collideConnected)); assert( r >= 0 );
    //r = engine->RegisterObjectProperty("b2JointDef", "void* userData", offsetof(void* type)); assert( r >= 0 );

    if ( typeid(T) != typeid(b2JointDef))
    {
        //!< ダウンキャスト
        //T -> b2JointDef へのキャストは 自動
        r = engine->RegisterObjectBehaviour(className, asBEHAVE_IMPLICIT_REF_CAST, 
            "b2JointDef@ f()", 
            asFUNCTION((ASCast<b2JointDef, T>)), 
            asCALL_CDECL_OBJLAST);
        assert( r>=0 );
    }
}





#endif //_SCRIPT_B2JOINT_DEF_H_
