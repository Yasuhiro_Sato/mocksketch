#include "stdafx.h"
#include <tchar.h>
#include <Box2D/Box2D.h>
#include <angelscript.h>
#include "../include/ScriptAddin.h"
#include "Box2DCommon.h"

extern bool CALLBACK Conv_b2Vec2( VAL_DATA* pValData, const void* pValAddress, void* pContext);

bool CALLBACK Conv_b2MassData( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("b2MassData");
    const b2MassData* p_b2MassData = reinterpret_cast<const b2MassData*>(pValAddress);
    StdStringStream strmValAll; 
    StdStringStream strmVal; 

    if (!pValAddress)
    {
        pValData->bValid = false;
        return false;
    }
    pValData->bValid = true;
    pValData->bArray = false;

    strmValAll << _T("{");

    {
    VAL_DATA val;
    val.bValid = true;
    val.bArray = false;
    val.strName     = _T("mass");
    val.strTypeName = _T("float");
    strmVal .str(_T(""));
    strmVal << p_b2MassData->mass;
    val.strVal = strmVal.str();
    pValData->lstVal.push_back(val);
    strmValAll << val.strVal<< _T(",");
    }


    {
    VAL_DATA val;
    val.bValid = true;
    val.bArray = false;
    val.strName     = _T("center");
    val.strTypeName = _T("b2Vec2");
    Conv_b2Vec2( &val, &p_b2MassData->center, pContext);
    pValData->lstVal.push_back(val);
    strmValAll << val.strVal << _T(",");
    }
   

    {
    VAL_DATA val;
    val.bValid = true;
    val.bArray = false;
    val.strName     = _T("I");
    val.strTypeName = _T("float");
    strmVal .str(_T(""));
    strmVal << p_b2MassData->I;
    val.strVal = strmVal.str();
    pValData->lstVal.push_back(val);
    strmValAll << val.strVal<< _T(",");
    }

    strmValAll << _T("}");
    pValData->strVal = strmValAll.str();
    return true;
}

void Register_b2MassDataType(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    int iRet;
    iRet = engine->RegisterObjectType("b2MassData", sizeof(b2MassData),  asOBJ_VALUE | asOBJ_POD);
    assert( iRet >= 0 );  
    g_mapValFunc[iRet] = Conv_b2MassData;
}


void Register_b2MassData(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
   int r;
    r = engine->RegisterObjectProperty("b2MassData", "float mass", offsetof(b2MassData, mass)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2MassData", "b2Vec2 center", offsetof(b2MassData, center)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2MassData", "float l", offsetof(b2MassData, I)); assert( r >= 0 );
}
