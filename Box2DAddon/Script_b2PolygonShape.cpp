#include "stdafx.h"
#include <tchar.h>
#include <Box2D/Box2D.h>
#include <angelscript.h>
#include "../include/ScriptAddin.h"
#include "Box2DCommon.h"

extern bool CALLBACK Conv_b2Vec2( VAL_DATA* pValData, const void* pValAddress, void* pContext);


bool CALLBACK Conv_b2PolygonShape( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("b2PolygonShape");
    const b2PolygonShape* pShape = reinterpret_cast< const b2PolygonShape*>(pValAddress);
    StdStringStream strmValAll; 
    StdStringStream strmVal; 

    if (!pValAddress)
    {
        pValData->bValid = false;
        return false;
    }
    pValData->bValid = true;
    pValData->bArray = false;

    strmValAll << _T("{");

    {
    VAL_DATA valType;
    valType.bValid = true;
    valType.bArray = false;
    valType.strName     = _T("type");
    valType.strTypeName = _T("b2ShapeType");
    strmVal .str(_T(""));
    strmVal << b2ShapeTypeToString(pShape->GetType());
    valType.strVal = strmVal.str();
    pValData->lstVal.push_back(valType);
    strmValAll << valType.strVal<< _T(",");
    }

    {
    VAL_DATA val;
    val.bValid = true;
    val.bArray = false;
    val.strName     = _T("radius");
    val.strTypeName = _T("float");
    strmVal .str(_T(""));
    strmVal << pShape->m_radius;
    val.strVal = strmVal.str();
    pValData->lstVal.push_back(val);
    strmValAll << val.strVal<< _T(",");
    }

    {
    VAL_DATA val;
    val.bValid = true;
    val.bArray = false;
    val.strName     = _T("count");
    val.strTypeName = _T("int");
    strmVal .str(_T(""));
    strmVal << pShape->m_count;
    val.strVal = strmVal.str();
    pValData->lstVal.push_back(val);
    strmValAll << val.strVal<< _T(",");
    }

    {
    VAL_DATA val;
    val.bValid = true;
    val.bArray = false;
    val.strName     = _T("vertices");
    val.strTypeName = _T("b2Vec2[]");
    strmVal .str(_T(""));
    
    for(int iCnt = 0; iCnt < pShape->m_count; iCnt++)
    {
        Conv_b2Vec2( &val, &pShape->m_vertices[iCnt], pContext);
        pValData->lstVal.push_back(val);
        strmValAll << val.strVal<< _T(",");
    }

    val.strVal = strmVal.str();
    pValData->lstVal.push_back(val);
    //strmValAll << val.strVal<< _T(",");
    }

    strmValAll << _T("}");
    pValData->strVal = strmValAll.str();
    return true;
}


void Register_b2PolygonShapeType(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    int iRet;
    iRet = engine->RegisterObjectType("b2PolygonShape", 0, asOBJ_REF );
    assert( iRet >= 0 );  
    g_mapValFunc[iRet] = Conv_b2PolygonShape;
}


static b2Vec2& GetPolygonShapeVertex(int iVertexNo, b2PolygonShape &s)
{
    return s.m_vertices[iVertexNo];
}

static b2Vec2& GetPolygonShapeNormal(int iVertexNo, b2PolygonShape &s)
{
    return s.m_normals[iVertexNo];
}


static void SetPolygonShape(std::vector<b2Vec2>& lstVec, b2PolygonShape &s)
{
    s.Set(&lstVec[0], lstVec.size());
}

void Register_b2PolygonShape(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    int r;

    // Register the object properties
    RegisterTemplate_b2Shape<b2PolygonShape>( engine, "b2PolygonShape");
    RegisterFactory<b2PolygonShape>(engine, "b2PolygonShape");

    r = engine->RegisterObjectProperty("b2PolygonShape", 
        "b2Vec2 m_centroid", offsetof(b2PolygonShape, m_centroid));
        assert( r >= 0 );

    r = engine->RegisterObjectProperty("b2PolygonShape", 
        "b2Vec2 m_Count", offsetof(b2PolygonShape, m_count));
        assert( r >= 0 );

    //b2Vec2 m_vertices[b2_maxPolygonVertices];
	r = engine->RegisterObjectMethod("b2PolygonShape", 
	    "b2Vec2& GetVertecex(int) const",
        asFUNCTION(GetPolygonShapeVertex), asCALL_CDECL_OBJLAST);
        assert( r >= 0 );

    //b2Vec2 m_normals[b2_maxPolygonVertices];
	r = engine->RegisterObjectMethod("b2PolygonShape", 
	    "b2Vec2& GetNormal(int) const",
        asFUNCTION(GetPolygonShapeNormal), asCALL_CDECL_OBJLAST);
        assert( r >= 0 );

    /*
    r = engine->RegisterObjectMethod("b2PolygonShape", 
        "void Set(const b2Vec2 &in, int)" , 
        asMETHOD(b2PolygonShape,Set), asCALL_THISCALL); 
        assert( r >= 0 );
    */

    r = engine->RegisterObjectMethod("b2PolygonShape", 
        "void Set(b2Vec2_VECTOR &in)" , 
        asFUNCTION(SetPolygonShape), asCALL_CDECL_OBJLAST); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2PolygonShape", 
        "void SetAsBox(float, float)" , 
        asMETHODPR(b2PolygonShape,SetAsBox,(float32, float32),void), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2PolygonShape", 
        "void SetAsBox(float, float, const b2Vec2& in, float)" , 
        asMETHODPR(b2PolygonShape,SetAsBox,(float32, float32, const b2Vec2&, float32),void), asCALL_THISCALL); 
        assert( r >= 0 );

	/// @see b2Shape::ComputeAABB
	//void ComputeAABB(b2AABB* aabb, const b2Transform& transform, int32 childIndex) const;


    r = engine->RegisterObjectMethod("b2PolygonShape", 
        "void ComputeMass(b2MassData& out, float) const" , 
        asMETHOD(b2PolygonShape,ComputeMass), asCALL_THISCALL); 
        assert( r >= 0 );


    r = engine->RegisterObjectMethod("b2PolygonShape", 
        "int GetVertexCount()" , 
        asMETHOD(b2PolygonShape,GetVertexCount), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2PolygonShape", 
        "const b2Vec2& GetVertex(int)" , 
        asMETHOD(b2PolygonShape,GetVertex), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2PolygonShape", 
        "bool Validate() const" , 
        asMETHOD(b2PolygonShape,Validate), asCALL_THISCALL); 
        assert( r >= 0 );

}