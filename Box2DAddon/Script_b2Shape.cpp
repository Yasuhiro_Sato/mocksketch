#include "stdafx.h"
#include <tchar.h>
#include <Box2D/Box2D.h>
#include <angelscript.h>
#include "../include/ScriptAddin.h"
#include "Box2DCommon.h"

bool CALLBACK Conv_b2Shape( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("b2Shape");
    const b2Shape* shape = reinterpret_cast<const b2Shape*>(pValAddress);
    StdStringStream strmValAll; 
    StdStringStream strmVal; 

    if (!pValAddress)
    {
        pValData->bValid = false;
        return false;
    }
    pValData->bValid = true;
    pValData->bArray = false;

    strmValAll << _T("{");
    return true;
}

void Register_b2ShapeType(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    int iRet;
    iRet = engine->RegisterObjectType("b2Shape", 0, asOBJ_REF );
    assert( iRet >= 0 );  
    g_mapValFunc[iRet]  = Conv_b2Shape;
}


void Register_b2Shape(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    RegisterTemplate_b2Shape<b2Shape>(engine, "b2Shape");
}
