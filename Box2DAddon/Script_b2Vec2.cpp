#include "stdafx.h"
#include <tchar.h>
#include <Box2D/Box2D.h>
#include <angelscript.h>
#include "../include/ScriptAddin.h"
#include "Box2DCommon.h"


bool CALLBACK Conv_b2Vec2( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("b2Vec2");
    const b2Vec2* pB2Vec2 = reinterpret_cast<const b2Vec2*>(pValAddress);
    StdStringStream strmValAll; 
    StdStringStream strmVal; 

    if (!pValAddress)
    {
        pValData->bValid = false;
        return false;
    }
    pValData->bValid = true;
    pValData->bArray = false;

    strmValAll << _T("{");
    
    VAL_DATA valX;
    valX.bValid = true;
    valX.bArray = false;
    valX.strName     = _T("x");
    valX.strTypeName = _T("float");
    strmVal << pB2Vec2->x;
    valX.strVal = strmVal.str();
    pValData->lstVal.push_back(valX);
    strmValAll << valX.strVal;
    strmValAll <<  _T(",");

    VAL_DATA valY;
    valY.bValid = true;
    valY.bArray = false;
    valY.strName     = _T("y");
    valY.strTypeName = _T("float");
    strmVal .str(_T(""));
    strmVal << pB2Vec2->y;
    valY.strVal = strmVal.str();
    pValData->lstVal.push_back(valY);
    strmValAll << valY.strVal;
    strmValAll <<  _T("}");

    pValData->strVal = strmValAll.str();
    return true;
}

static void b2Vec2_InitConstructor(float x, float y, b2Vec2 *self)
{
	new(self) b2Vec2(x,y);
}

//b2Vec2
void Register_b2Vec2Type(asIScriptEngine *engine)
{
    int iRet;
    g_engine = engine;

    iRet = engine->RegisterObjectType("b2Vec2", sizeof(b2Vec2), asOBJ_VALUE | asOBJ_POD | asOBJ_APP_CLASS_CA);
    assert( iRet >= 0 );  
    g_mapValFunc[iRet] = Conv_b2Vec2;
}

//b2Vec2
void Register_b2Vec2(asIScriptEngine *engine)
{
	int r;
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}

	// Register the type
	// Register the object properties
	r = engine->RegisterObjectProperty("b2Vec2", "float x", offsetof(b2Vec2, x)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("b2Vec2", "float y", offsetof(b2Vec2, y)); assert( r >= 0 );

	// Register the constructors
    RegisterC<b2Vec2>(engine, "b2Vec2");
    RegisterA<b2Vec2>(engine, "b2Vec2");
	r = engine->RegisterObjectBehaviour("b2Vec2", asBEHAVE_CONSTRUCT, "void f(float, float)",     asFUNCTION(b2Vec2_InitConstructor), asCALL_CDECL_OBJLAST); assert( r >= 0 );

	// Register the operator overloads
	r = engine->RegisterObjectMethod("b2Vec2", "void opAddAssign(const b2Vec2 &in)", asMETHODPR(b2Vec2, operator+=, (const b2Vec2 &), void), asCALL_THISCALL); assert( r >= 0 );
	r = engine->RegisterObjectMethod("b2Vec2", "void opSubAssign(const b2Vec2 &in)", asMETHODPR(b2Vec2, operator-=, (const b2Vec2 &), void), asCALL_THISCALL); assert( r >= 0 );
	r = engine->RegisterObjectMethod("b2Vec2", "void opMulAssign(float)", asMETHODPR(b2Vec2, operator*=, (float), void), asCALL_THISCALL); assert( r >= 0 );

	// Register the object methods
	r = engine->RegisterObjectMethod("b2Vec2", "void SetZero()"         , asMETHOD(b2Vec2,SetZero), asCALL_THISCALL); assert( r >= 0 );
	r = engine->RegisterObjectMethod("b2Vec2", "void Set(float, float)" , asMETHOD(b2Vec2,Set), asCALL_THISCALL); assert( r >= 0 );
	r = engine->RegisterObjectMethod("b2Vec2", "float Length() const"   , asMETHOD(b2Vec2,Length), asCALL_THISCALL); assert( r >= 0 );
	r = engine->RegisterObjectMethod("b2Vec2", "float LengthSquared() const", asMETHOD(b2Vec2,LengthSquared), asCALL_THISCALL); assert( r >= 0 );
	r = engine->RegisterObjectMethod("b2Vec2", "float Normalize()"          , asMETHOD(b2Vec2,Normalize), asCALL_THISCALL); assert( r >= 0 );
	r = engine->RegisterObjectMethod("b2Vec2", "bool IsValid() const"       , asMETHOD(b2Vec2,IsValid), asCALL_THISCALL); assert( r >= 0 );
	r = engine->RegisterObjectMethod("b2Vec2", "b2Vec2 Skew()"              , asMETHOD(b2Vec2,Skew), asCALL_THISCALL); assert( r >= 0 );
}
