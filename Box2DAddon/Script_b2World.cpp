#include "stdafx.h"
#include <tchar.h>
#include <Box2D/Box2D.h>
#include <angelscript.h>
#include "../include/ScriptAddin.h"
#include "Box2DCommon.h"

//extern void FreeData(b2Body &s);

bool CALLBACK Conv_b2World( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("b2World");
    const b2World* pWorld = reinterpret_cast<const b2World*>(pValAddress);
    StdStringStream strmValAll; 
    StdStringStream strmVal; 

    if (!pValAddress)
    {
        pValData->bValid = false;
        return false;
    }
    pValData->bValid = true;
    pValData->bArray = false;

    strmValAll << _T("{");
    return true;
}

static b2World *b2World_InitFactory(const b2Vec2& gravity)
{
    b2World *pRet;
    pRet = new b2World(gravity);
    AddRef<b2World>(*pRet);
    return pRet;
}

static b2World *b2World_InitFactoryZero()
{
    b2Vec2 gravity(0.0, 0.0);
    b2World *pRet;
    pRet = new b2World(gravity);
    AddRef<b2World>(*pRet);
    return pRet;
}

static void DestroyBody_b2World(b2Body* body, b2World &w)
{
    FreeData<b2Body>(*body);
    w.DestroyBody(body);
}

static void DestroyJoint_b2World(b2Joint* joint, b2World &w)
{
    FreeData<b2Joint>(*joint);
    w.DestroyJoint(joint);
}

void Register_b2WorldType(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    int iRet;
    iRet = engine->RegisterObjectType("b2World", 0,  asOBJ_REF);
    assert( iRet >= 0 );  
    g_mapValFunc[iRet] = Conv_b2World;
}

static void Release_b2World(b2World& wld)
{
    CRefCountManager* pMgr;
    pMgr = CRefCountManager::GetInstance();

    int iRefCount = pMgr->GetCount(&wld);

    if (iRefCount <= 1)
    {
        b2Body* pBody = wld.GetBodyList();
        while(pBody != NULL) 
        {
            if (pBody)
            {
                FreeData<b2Body>(*pBody);
            }
            pBody = pBody->GetNext();
        } 

        b2Joint* pJoint = wld.GetJointList();
        while(pJoint != NULL) 
        {
            if (pBody)
            {
                FreeData<b2Joint>(*pJoint);
            }
            pJoint = pJoint->GetNext();
        } 
    }

    pMgr->Release(&wld);
}

//===========FOR DEBUG ==================
static void DumpJointDef_b2World(b2JointDef* pJoint, b2World& wld)
{
    int a = 10;
}

static void DumpJoin_b2World(b2Joint* pJoint, b2World& wld)
{
    int a = 10;
}

static void DumpBody_b2World(b2Body* pBody, b2World& wld)
{
    int a = 10;
}

static b2Joint* CreateJoint2(b2JointDef* pDef, b2World& wld)
{
    b2Joint* pJoint;
    int a = 10;
    pJoint = wld.CreateJoint(pDef);
    return pJoint;
}

//===========FOR DEBUG ==================

void Register_b2World(asIScriptEngine *engine)
{
    if (!g_engine) { g_engine = engine;}
    else           { assert(g_engine == engine);}
    int r;


    r = engine->RegisterObjectBehaviour("b2World", asBEHAVE_FACTORY, 
        "b2World @f(const b2Vec2 &in)",
        asFUNCTION( b2World_InitFactory), asCALL_CDECL); 
        assert( r >= 0 );

    r = engine->RegisterObjectBehaviour("b2World", asBEHAVE_FACTORY, 
        "b2World @f()",
        asFUNCTION( b2World_InitFactoryZero), asCALL_CDECL); 
        assert( r >= 0 );
        

   // RegisterRef<b2World>(engine, "b2World");
   //int r;
    r = engine->RegisterObjectBehaviour("b2World", asBEHAVE_ADDREF, 
        "void f()", asFUNCTION(AddRef<b2World>), 
        asCALL_CDECL_OBJFIRST); assert( r >= 0 );

	r = engine->RegisterObjectBehaviour("b2World", asBEHAVE_RELEASE,
        "void f()", asFUNCTION(Release_b2World), 
        asCALL_CDECL_OBJFIRST); assert( r >= 0 );


    // r = engine->RegisterObjectMethod("b2World", 
    //    "void SetDestructionListener(const b2DestructionListener@ in, int)" , 
    //    asMETHOD(b2World,SetDestructionListener), asCALL_THISCALL); 
    //    assert( r >= 0 );

    //r = engine->RegisterObjectMethod("b2World", 
    //    "void SetContactFilter(b2ContactFilter@ in)" , 
    //    asMETHOD(b2World,SetContactFilter), asCALL_THISCALL); 
    //    assert( r >= 0 );

    //r = engine->RegisterObjectMethod("b2World", 
    //    "void SetContactListener(b2ContactListener& in)" , 
    //    asMETHOD(b2World,SetContactListener), asCALL_THISCALL); 
    //    assert( r >= 0 );

    //r = engine->RegisterObjectMethod("b2World", 
    //    "void SetDebugDraw(b2Draw& in)" , 
    //    asMETHOD(b2World,SetDebugDraw), asCALL_THISCALL); 
    //    assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2World", 
        "b2Body@ CreateBody(b2BodyDef& in)" , 
        asMETHOD(b2World,CreateBody), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2World", 
        "void DestroyBody(b2Body &inout)" , 
        asFUNCTION( DestroyBody_b2World), asCALL_CDECL_OBJLAST); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2World", 
        "b2Joint@ CreateJoint(b2JointDef& in)" , 
        asMETHOD(b2World,CreateJoint), asCALL_THISCALL); 
        assert( r >= 0 );
//FOR DEBUG
#if(1)
    r = engine->RegisterObjectMethod("b2World", 
        "b2Joint@ CreateJoint2(b2JointDef& in)" , 
        asFUNCTION(CreateJoint2), asCALL_CDECL_OBJLAST); 
        assert( r >= 0 );
#endif
        r = engine->RegisterObjectMethod("b2World", 
        "void DestroyJoint(b2Joint &inout)" , 
        asFUNCTION( DestroyJoint_b2World), asCALL_CDECL_OBJLAST); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2World", 
        "void Step(float, int, int)" , 
        asMETHOD(b2World,Step), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2World", 
        "void ClearForces()" , 
        asMETHOD(b2World,ClearForces), asCALL_THISCALL); 
        assert( r >= 0 );


    r = engine->RegisterObjectMethod("b2World", 
        "void SetGravity(const b2Vec2& in)" , 
        asMETHOD(b2World,SetGravity), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2World", 
        "b2Vec2 GetGravity() const" , 
        asMETHOD(b2World,GetGravity), asCALL_THISCALL); 
        assert( r >= 0 );

    //b2Body* GetBodyList();
    r = engine->RegisterObjectMethod("b2World", 
        "b2Body@ GetBodyList()" , 
        asMETHODPR(b2World,GetBodyList, (), b2Body*),
        asCALL_THISCALL); 
        assert( r >= 0 );

	//const b2Body* GetBodyList() const;
    /*
    r = engine->RegisterObjectMethod("b2World", 
        "b2Body@ GetBodyList() const" , 
        asMETHODPR(b2World,GetBodyList, (void)const,const b2Body*),
        asCALL_THISCALL); 
        assert( r >= 0 );
    */

    //b2Joint* GetJointList();
    r = engine->RegisterObjectMethod("b2World", 
        "b2Joint@ GetJointList()" , 
        asMETHODPR(b2World,GetJointList, (), b2Joint*),
        asCALL_THISCALL); 
        assert( r >= 0 );

    //const b2Joint* GetJointList() const;
    /*
    r = engine->RegisterObjectMethod("b2World", 
        "b2Joint@ GetJointList() const" , 
        asMETHODPR(b2World,GetJointList, (void)const,const b2Joint*),
        asCALL_THISCALL); 
        assert( r >= 0 );
    */

	//void SetAllowSleeping(bool flag);
    r = engine->RegisterObjectMethod("b2World", 
        "void SetAllowSleeping(bool)" , 
        asMETHOD(b2World,SetAllowSleeping), asCALL_THISCALL); 
        assert( r >= 0 );
    
    //bool GetAllowSleeping() const { return m_allowSleep; }
    r = engine->RegisterObjectMethod("b2World", 
        "bool SetAllowSleeping() const" , 
        asMETHOD(b2World,SetAllowSleeping), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2World", 
        "void SetWarmStarting(bool)" , 
        asMETHOD(b2World,SetWarmStarting), asCALL_THISCALL); 
        assert( r >= 0 );
    
    r = engine->RegisterObjectMethod("b2World", 
        "bool GetWarmStarting() const" , 
        asMETHOD(b2World,GetWarmStarting), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2World", 
        "void SetContinuousPhysics(bool)" , 
        asMETHOD(b2World,SetContinuousPhysics), asCALL_THISCALL); 
        assert( r >= 0 );
    
    r = engine->RegisterObjectMethod("b2World", 
        "bool GetContinuousPhysics() const" , 
        asMETHOD(b2World,GetContinuousPhysics), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2World", 
        "void SetSubStepping(bool)" , 
        asMETHOD(b2World,SetSubStepping), asCALL_THISCALL); 
        assert( r >= 0 );
    
    r = engine->RegisterObjectMethod("b2World", 
        "bool GetSubStepping() const" , 
        asMETHOD(b2World,GetSubStepping), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2World", 
        "int GetProxyCount() const" , 
        asMETHOD(b2World,GetProxyCount), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2World", 
        "int GetBodyCount() const" , 
        asMETHOD(b2World,GetBodyCount), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2World", 
        "int GetJointCount() const" , 
        asMETHOD(b2World,GetJointCount), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2World", 
        "int GetContactCount() const" , 
        asMETHOD(b2World,GetContactCount), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2World", 
        "int GetTreeHeight() const" , 
        asMETHOD(b2World,GetTreeHeight), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2World", 
        "int GetTreeBalance() const" , 
        asMETHOD(b2World,GetTreeBalance), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2World", 
        "float GetTreeQuality() const" , 
        asMETHOD(b2World,GetTreeQuality), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2World", 
        "bool IsLocked() const" , 
        asMETHOD(b2World,IsLocked), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2World", 
        "void SetAutoClearForces(bool)" , 
        asMETHOD(b2World,SetAutoClearForces), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2World", 
        "bool GetAutoClearForces() const" , 
        asMETHOD(b2World,GetAutoClearForces), asCALL_THISCALL); 
        assert( r >= 0 );

    //r = engine->RegisterObjectMethod("b2World", 
    //   "void QueryAABB(b2QueryCallback&in, b2AABB &in) const" , 
    //    asMETHOD(b2World,QueryAABB), asCALL_THISCALL); 
    //    assert( r >= 0 );

	//void RayCast(b2RayCastCallback* callback, const b2Vec2& point1, const b2Vec2& point2) const;
    //r = engine->RegisterObjectMethod("b2World", 
    //    "void ClearForces(b2RayCastCallback&in, b2Vec2&in, b2Vec2&in)" , 
    //    asMETHOD(b2World,ClearForces), asCALL_THISCALL); 
    //    assert( r >= 0 );

    /*

	b2Contact* GetContactList();
	const b2Contact* GetContactList() const;
	const b2ContactManager& GetContactManager() const;
	const b2Profile& GetProfile() const;
	void Dump();
    */

    //=======================FOR DEBUG===================================
    r = engine->RegisterObjectMethod("b2World", 
        "void Dump(b2JointDef &in)" , 
        asFUNCTION( DumpJointDef_b2World), asCALL_CDECL_OBJLAST); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2World", 
        "void Dump(b2Joint &in)" , 
        asFUNCTION( DumpJoin_b2World), asCALL_CDECL_OBJLAST); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("b2World", 
        "void Dump(b2Body &in)" , 
        asFUNCTION( DumpBody_b2World), asCALL_CDECL_OBJLAST); 
        assert( r >= 0 );

    //=======================FOR DEBUG===================================


}