// dllmain.cpp : DLL アプリケーションのエントリ ポイントを定義します。
#define MS_DLL_EXPORTS

#include "stdafx.h"
#include "../include/ScriptAddin.h"
#include <tchar.h>
#include <delayimp.h>
#pragma comment (lib, "delayimp.lib") 
//#pragma comment (lib, "version.lib") 

#include "Box2dAddon.h"

static HMODULE g_hModule;

ID_VALFUNC_MAP g_mapValFunc;

void DllStart();


FARPROC WINAPI DliNotifyHook(unsigned int dliNotify, PDelayLoadInfo pdli)
{
    if(dliNotify == dliNotePreLoadLibrary)
    {
        TCHAR path[MAX_PATH];
        ::GetModuleFileName(g_hModule, path, _countof(path));
        StdString strPath;

        strPath = path;
#ifdef _DEBUG
        strPath += _T("\\Box2D_D.dll");
#else
        strPath += _T("\\Box2D.dll");
#endif

        HMODULE hMod = ::LoadLibrary(strPath.c_str());
        return (FARPROC)hMod;
	}
	return NULL;
}


BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
        g_hModule = hModule;
        __pfnDliNotifyHook2 = DliNotifyHook;
        g_mapValFunc.clear();
        DllStart();
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	case DLL_PROCESS_DETACH:
        g_hModule = NULL;
		break;
	}
	return TRUE;
}

void DllStart()
{
    //::MessageBox(NULL, _T("HELLO"), _T("Yayaya"), MB_OK);
}

namespace MockScript{


LPCTSTR GetName()
{
    return _T("BOX2D");
}

int RegisterType(asIScriptEngine *engine)
{
    Register_b2Vec2Type(engine);
    RegisterTypeb2Vec2_VECTOR    (engine);

    Register_b2BodyType_Enum(engine);

    Register_b2ShapeType_Enum(engine);
    Register_b2JointType_Enum(engine);

    Register_b2BodyDefType(engine);
    Register_b2BodyType(engine);
    RegisterTypeb2BodyPtr_VECTOR    (engine);
    RegisterTypeb2JointPtr_VECTOR   (engine);

    Register_b2ShapeType(engine);
    Register_b2PolygonShapeType(engine);
    Register_b2EdgeShapeType(engine);

    Register_b2FixtureDefType(engine);
    Register_b2FixtureType(engine);

    Register_b2WorldType(engine);
    Register_b2MassDataType(engine);


    Register_b2JointDefType(engine);
    Register_b2JointType(engine);
    Register_b2DistanceJointDefType(engine);
    Register_b2RevoluteJointDefType(engine);
    Register_b2RevoluteJointType(engine);
    Register_b2DistanceJointType(engine);


    return 0;
}

int Register(asIScriptEngine *engine)
{
    Register_b2Vec2(engine);
    Registerb2Vec2_VECTOR(engine);
    RegisterCast_b2Shape(engine);
    Register_b2BodyDef(engine);
    Register_b2Body(engine);
    Registerb2BodyPtr_VECTOR    (engine);
    Registerb2JointPtr_VECTOR    (engine);
    Register_b2Shape(engine);
    Register_b2PolygonShape(engine);
    Register_b2EdgeShape(engine);
    Register_b2FixtureDef(engine);
    Register_b2Fixture(engine);
    Register_b2JointDef(engine);
    Register_b2Joint(engine);
    Register_b2DistanceJointDef(engine);
    Register_b2RevoluteJointDef(engine);
    Register_b2DistanceJoint(engine);
    Register_b2RevoluteJoint(engine);

    Register_b2World(engine);
    Register_b2MassData(engine);


    return 0;
}


DLL_API int GetValData( StdChar* pStrVal, size_t iBufSize,
                              int iTypeId,
                              void* pValAddress,
                              void* pContext)
{
    VAL_DATA valData;
    StdString strRet;
    ID_VALFUNC_MAP::iterator iteMap;
    bool bRet = false;

    iteMap = g_mapValFunc.find(iTypeId);
    if (iteMap != g_mapValFunc.end())
    {
        bRet = iteMap->second(&valData, pValAddress, pContext);
    }

    if (bRet)
    {
        strRet = VAL_DATA_CONV::ValDataToStr(&valData);

        if (iBufSize <=  strRet.size())
        {
            return strRet.size();
        }

        _tcscpy_s( pStrVal, iBufSize, strRet.c_str());
        return 0;
    }
    return -1;
}


}//MockScript