#include "stdafx.h"
#include "DeviceDialog.h"
#include "DummyContec.h"
#include "resource.h"
#include <tchar.h>
#include <process.h>
#include <commctrl.h>
#include <assert.h>
#include <math.h>


//!< コンストラクタ
DiviceDialog::DiviceDialog(HMODULE hModule, HWND hParent, DummyDivice* pDevice):
DialogBase(hModule, "IDD_DLG_AIO", hParent),
m_hLcAo   (0),
m_hAiCbRange   (0)
{
    m_pDevice = pDevice;
    memset(&m_hCbAiMode[0], 0, sizeof(m_hCbAiMode));
}

void DiviceDialog::OnPbSet(int iNo)
{
    StdChar strTxt[256];
    int iPage = GetComboBoxSel(m_hAiCbRange);

    int iCh;
    int iVal;

    iCh = iNo + (iPage * 4);
    GetDlgItemText(m_hWnd, IDC_EDIT1 + iNo, strTxt, 256);
    iVal = _tstoi(strTxt);
    m_pDevice->SetAi(iCh, iVal);
}

void DiviceDialog::OnCkDo(int iNo)
{
    //通常押せないはず
    m_bCkDo[iNo] = (IsDlgButtonChecked(m_hWnd, IDC_CK_DO1+iNo) == BST_CHECKED);

}

void DiviceDialog::OnCkDi(int iNo)
{
    m_bCkDi[iNo] = (IsDlgButtonChecked(m_hWnd, IDC_CK_DI1+iNo) == BST_CHECKED);
    m_pDevice->SetDi(iNo, m_bCkDi[iNo]);
}

void DiviceDialog::OnPbCntSet(int iNo)
{
    StdChar strNo[256];
    GetDlgItemText(m_hWnd, IDC_ED_COUNTER_1 + iNo, strNo, 256);

    int iCnt;
    iCnt = _tstoi(strNo);

    m_pDevice->SetCount(iNo, iCnt);
}

void DiviceDialog::OnCkAuto(int iNo)
{
    if(IsDlgButtonChecked(m_hWnd, IDC_CK_AUTO_CNT_1+iNo) == BST_CHECKED)
    {
        m_pDevice->StartCount(iNo);
    }
    else
    {
        m_pDevice->StopCount(iNo);
    }
}


void DiviceDialog::OnCbAiMode(int iNo, int iNotify)
{
    if (iNotify != CBN_SELCHANGE)
    {
        return;
    }

    E_AI_MODE eSel;
    int iPage;
    int iCh;
    eSel  = static_cast<E_AI_MODE>(GetComboBoxSel(m_hCbAiMode[iNo]));
    iPage = GetComboBoxSel(m_hAiCbRange);

    iCh = iNo + (iPage * 4);
    m_pDevice->SetAiMode(iCh, eSel);
}

void DiviceDialog::SetAiRange()
{
    StdChar strTxt[256];
    int iPage;
    int iMode;
    int iVal;
    iPage = GetComboBoxSel(m_hAiCbRange);

    int iCh;
    for (int iCnt = 0; iCnt < 4; iCnt++)
    {
        iCh = iCnt + (iPage * 4);
        _sntprintf(&strTxt[0] ,sizeof(strTxt), _T("%02d"), iCh);
        SetDlgItemText(m_hWnd, IDC_ST_S1 + iCnt, strTxt);
        iMode = m_pDevice->GetAiMode(iCh);
        SetComboBoxSel(m_hCbAiMode[iCnt], iMode);
        iVal = m_pDevice->GetAi(iCh);
        _sntprintf(&strTxt[0] ,sizeof(strTxt), _T("%d"), iVal);
        SetDlgItemText(m_hWnd, IDC_EDIT1 + iCnt, strTxt);
    }
}

void DiviceDialog::OnCbAiRange(int iNotify)
{
    if (iNotify != CBN_SELCHANGE)
    {
        return;
    }

    SetAiRange();
}


void DiviceDialog::OnInitDialog()
{
    assert(m_pDevice != NULL);

    AIO_PRODUCT* pProduct;
    pProduct = m_pDevice->GetProduct();
    StdString strDevice = CharToString(pProduct->strBoardName);
    StdString strId     = CharToString(pProduct->strDriver);
                           

    SetDlgItemText(m_hWnd, IDC_ST_DEVICE, strDevice.c_str());
    SetDlgItemText(m_hWnd, IDC_ST_ID, strId.c_str());

    int  iShow;
    StdChar strTxt[256];

    for (int iCnt = 0; iCnt < 8; iCnt++)
    {
        iShow = ((pProduct->iDiIn > iCnt)? SW_SHOW: SW_HIDE);
        ShowWindow(GetDlgItem(m_hWnd,IDC_CK_DI1 + iCnt), iShow);
    }

    for (int iCnt = 0; iCnt < 8; iCnt++)
    {
        iShow = ((pProduct->iDiOut > iCnt)? SW_SHOW: SW_HIDE);
        ShowWindow(GetDlgItem(m_hWnd,IDC_CK_DO1 + iCnt), iShow);
        EnableWindow(GetDlgItem(m_hWnd,IDC_CK_DO1 + iCnt), FALSE);
    }

    SetDlgItemText(m_hWnd, IDC_ED_COUNTER_1, _T("0"));
    SetDlgItemText(m_hWnd, IDC_ED_COUNTER_2, _T("0"));

    SetDlgItemText(m_hWnd, IDC_EDIT1, _T("0"));
    SetDlgItemText(m_hWnd, IDC_EDIT2, _T("0"));
    SetDlgItemText(m_hWnd, IDC_EDIT3, _T("0"));
    SetDlgItemText(m_hWnd, IDC_EDIT4, _T("0"));

    m_hLcAo = GetDlgItem(m_hWnd, IDC_LSC_AO);

    m_hAiCbRange = GetDlgItem(m_hWnd, IDC_CB_AI_RANGE);

    m_hCbAiMode[0] = GetDlgItem(m_hWnd, IDC_CB_AI_MODE_1);
    m_hCbAiMode[1] = GetDlgItem(m_hWnd, IDC_CB_AI_MODE_2);
    m_hCbAiMode[2] = GetDlgItem(m_hWnd, IDC_CB_AI_MODE_3);
    m_hCbAiMode[3] = GetDlgItem(m_hWnd, IDC_CB_AI_MODE_4);


    //AI
    int iMax = static_cast<int>(ceil(pProduct->iInChNum / 4.0));
    if (pProduct->iInChNum == 0)
    {
        ShowWindow(GetDlgItem(m_hWnd,IDC_CB_AI_RANGE), SW_HIDE);
        for(int iCnt = 0; iCnt < 4; iCnt++)
        {
            ShowWindow(GetDlgItem(m_hWnd,IDC_EDIT1 + iCnt), SW_HIDE);
            ShowWindow(GetDlgItem(m_hWnd,IDC_PB_SET_1 + iCnt), SW_HIDE);
            ShowWindow(GetDlgItem(m_hWnd,IDC_CB_AI_MODE_1 + iCnt), SW_HIDE);
        }
    }
    else
    {
        std::vector<StdString> lstAiMode;
        for(int iCnt = 0; iCnt < iMax; iCnt++)
        {
            _sntprintf(&strTxt[0] ,sizeof(strTxt), _T("%04d-%04d"), iCnt * 4, iCnt * 4 + 3);
            AddComboBox(m_hAiCbRange, strTxt);
        }
        SetComboBoxSel(m_hAiCbRange , 0);

        for(int iCnt = 0; iCnt < 4; iCnt++)
        {
            AddComboBox(m_hCbAiMode[iCnt] , _T("NONE"));
            AddComboBox(m_hCbAiMode[iCnt] , _T("SIN"));
            AddComboBox(m_hCbAiMode[iCnt] , _T("RECT"));
            AddComboBox(m_hCbAiMode[iCnt] , _T("RAND"));
            SetComboBoxSel(m_hCbAiMode[iCnt] , 0);
        }
    }

    //AO
    if (pProduct->iOutChNum == 0)
    {
        ShowWindow(GetDlgItem(m_hWnd, IDC_LST_AO), SW_HIDE);
    }
    else
    {
        InsColumn(m_hLcAo, _T("Ch"), 50, 0);
        InsColumn(m_hLcAo, _T("Val"), 100, 1);
        InsColumn(m_hLcAo, _T("Unit"), 50, 2);

        for (int iCh = 0; iCh < pProduct->iOutChNum; iCh++)
        {
            _sntprintf(&strTxt[0] ,sizeof(strTxt), _T("%02d"), iCh + 1);
            InsItem(m_hLcAo, iCh, strTxt);
            SetSubitem(m_hLcAo, iCh, 1, _T("0"));
        }
    }


    //COUNTER
    for(int iCnt = 0; iCnt < 2; iCnt++)
    {

        iShow = ((pProduct->iCntNum > iCnt)? SW_SHOW: SW_HIDE);
        ShowWindow(GetDlgItem(m_hWnd, IDC_ED_COUNTER_1 + iCnt), iShow);
        ShowWindow(GetDlgItem(m_hWnd, IDC_PB_CNT_SET_1 + iCnt), iShow);
        ShowWindow(GetDlgItem(m_hWnd, IDC_CK_AUTO_CNT_1 + iCnt), iShow);
    }

    SetAiRange();
}

void DiviceDialog::OnDlgUpdate()
{
    UpdateDo();
    UpdateAo();
    UpdateCounter();
}

void DiviceDialog::UpdateDo()
{
    int iDoMax = m_pDevice->GetProduct()->iDiOut;
    for (int iBit = 0; iBit < iDoMax; iBit++)
    {
        m_bCkDo[iBit] = m_pDevice->GetDo(iBit);
    }
    SetDo();
}

void DiviceDialog::UpdateAo()
{
    int iVal;
    StdChar strTxt[256];
    int iAoMax = m_pDevice->GetProduct()->iOutChNum;
    for (int iCh = 0; iCh < iAoMax;  iCh++)
    {
        iVal = m_pDevice->GetAo(iCh);
        _sntprintf(&strTxt[0] ,sizeof(strTxt), _T("%d"), iVal);
        SetSubitem(m_hLcAo, iCh, 1, strTxt);
    }
}

void DiviceDialog::UpdateAi()
{
    StdChar strTxt[256];
    int iPage;
    int iVal;
    iPage = GetComboBoxSel(m_hAiCbRange);

    int iCh;
    for (int iCnt = 0; iCnt < 4; iCnt++)
    {
        iCh = iCnt + (iPage * 4);
        iVal = m_pDevice->GetAi(iCh);
        if (m_pDevice->GetAiMode(iCh) != AI_NONE)
        {
            _sntprintf(&strTxt[0] ,sizeof(strTxt), _T("%d"), iVal);
            SetDlgItemText(m_hWnd, IDC_EDIT1 + iCnt, strTxt);
        }
    }
}

void DiviceDialog::UpdateCounter()
{
    int iVal;
    StdChar strTxt[256];
    int iCntMax = m_pDevice->GetProduct()->iCntNum;
    for (int iCh = 0; iCh < iCntMax;  iCh++)
    {
        iVal = m_pDevice->GetCount(iCh);
        _sntprintf(&strTxt[0] ,sizeof(strTxt), _T("%d"), iVal);
        SetDlgItemText(m_hWnd, IDC_ED_COUNTER_1 + iCh, strTxt);
    }
}

void DiviceDialog::OnPowerChange(int iPowerSts)
{
    switch(iPowerSts)
    {
    case PBT_APMPOWERSTATUSCHANGE:
        //Power status has changed.
        break;

    case PBT_APMRESUMEAUTOMATIC:
        //Operation is resuming automatically from a low-power state. This message is sent every time the system resumes.
        if(m_pDevice)
        {
            m_pDevice->SetResumed();
        }
        break;

    case PBT_APMRESUMESUSPEND:
        //Operation is resuming from a low-power state. This message is sent after PBT_APMRESUMEAUTOMATIC if the resume is triggered by user input, such as pressing a key.
        break;

    case PBT_APMSUSPEND:
        //System is suspending operation.
        break;

    case PBT_POWERSETTINGCHANGE:
        //A power setting change event has been received.
        break;
    }
}

void DiviceDialog::GetText()
{
    StdChar szBuf[1024];
    for(int iCnt = 0; iCnt < 4; iCnt++)
    {
        if(GetDlgItemText(m_hWnd, IDC_EDIT1 + iCnt, &szBuf[0], sizeof(szBuf)))
        {
            m_strEd[iCnt] = szBuf;
        }
    }

    for(int iCnt = 0; iCnt < 2; iCnt++)
    {
        if(GetDlgItemText(m_hWnd, IDC_ED_COUNTER_1 + iCnt, &szBuf[0], sizeof(szBuf)))
        {
            m_strCounter[iCnt] = szBuf;
        }
    }
}

void DiviceDialog::SetDi()
{
    for(int iCnt =0; iCnt < 8; iCnt++)
    {
        CheckDlgButton(m_hWnd, IDC_CK_DI1+ iCnt, (m_bCkDi[iCnt]?BST_CHECKED:BST_UNCHECKED));
    }
}

void DiviceDialog::SetDo()
{

    for(int iCnt =0; iCnt < 8; iCnt++)
    {
        CheckDlgButton(m_hWnd, IDC_CK_DO1+ iCnt, (m_bCkDo[iCnt]?BST_CHECKED:BST_UNCHECKED));
    }
}

void DiviceDialog::SetText()
{
    SetDlgItemText(m_hWnd, IDC_EDIT1, m_strEd[0].c_str());
    SetDlgItemText(m_hWnd, IDC_EDIT2, m_strEd[1].c_str());
    SetDlgItemText(m_hWnd, IDC_EDIT3, m_strEd[2].c_str());
    SetDlgItemText(m_hWnd, IDC_EDIT4, m_strEd[3].c_str());

    SetDlgItemText(m_hWnd, IDC_ED_COUNTER_1, m_strCounter[0].c_str());
    SetDlgItemText(m_hWnd, IDC_ED_COUNTER_2, m_strCounter[1].c_str());

    SetDlgItemText(m_hWnd, IDC_ST_DEVICE, m_strDevice.c_str());
    SetDlgItemText(m_hWnd, IDC_ST_ID, m_strId.c_str());
}



BOOL DiviceDialog::OnCommand(int iId, int iNotify, HWND hCtrl)
{
    switch(iId)
    {
    case IDC_PB_SET_1:      
    case IDC_PB_SET_2:      
    case IDC_PB_SET_3:      
    case IDC_PB_SET_4:
        OnPbSet(iId - IDC_PB_SET_1);
        break;

    case IDC_CK_DO1:      
    case IDC_CK_DO2:      
    case IDC_CK_DO3:      
    case IDC_CK_DO4:      
    case IDC_CK_DO5:      
    case IDC_CK_DO6:      
    case IDC_CK_DO7:      
    case IDC_CK_DO8:      
        OnCkDo(iId - IDC_CK_DO1);
        break;

    case IDC_CK_DI1:      
    case IDC_CK_DI2:      
    case IDC_CK_DI3:      
    case IDC_CK_DI4:      
    case IDC_CK_DI5:      
    case IDC_CK_DI6:      
    case IDC_CK_DI7:      
    case IDC_CK_DI8:      
        OnCkDi(iId - IDC_CK_DI1);
        break;

    case IDC_PB_CNT_SET_1:      
    case IDC_PB_CNT_SET_2:      
        OnPbCntSet(iId - IDC_PB_CNT_SET_1);
        break;
        
    case IDC_CK_AUTO_CNT_1:      
    case IDC_CK_AUTO_CNT_2:      
        OnCkAuto(iId - IDC_CK_AUTO_CNT_1);
        break;

    case IDC_CB_AI_MODE_1:      
    case IDC_CB_AI_MODE_2:      
    case IDC_CB_AI_MODE_3:      
    case IDC_CB_AI_MODE_4:      
        OnCbAiMode(iId - IDC_CB_AI_MODE_1, iNotify);
        break;

    case IDC_CB_AI_RANGE:      
        OnCbAiRange(iNotify);
        break;

    default:        {break;}
    }
    
    return DialogBase::OnCommand(iId, iNotify, hCtrl);
}


