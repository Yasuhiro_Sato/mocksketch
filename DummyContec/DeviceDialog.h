
#ifndef DEVICE_DIALOG_H
#define DEVICE_DIALOG_H
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DialogBase.h"
#include <string>

class DummyDivice;

class DiviceDialog : public DialogBase
{

public:

    //!< コンストラクタ
    DiviceDialog(HMODULE hModule, HWND hParent, DummyDivice* pDevice);

    virtual BOOL OnCommand(int iId, int iNotify, HWND hCtrl);

    virtual void OnInitDialog();

    virtual void OnDlgUpdate();

    virtual void OnPowerChange(int iPowerSts);

    void OnPbSet(int iNo);

    void OnCkDo(int iNo);

    void OnCkDi(int iNo);

    void OnCkAuto(int iNo);

    void OnPbCntSet(int iNo);

    void OnCbAiMode(int iNo, int iNotify);
    
    void OnCbAiRange(int iNotify);

    void GetText();

    void SetDi();
    void SetDo();

    void SetText();

protected:
    void SetAiRange();
    void UpdateDo();
    void UpdateAo();
    void UpdateAi();
    void UpdateCounter();


protected:

    HWND    m_hLcAo;

    HWND    m_hAiCbRange;

    HWND    m_hCbAiMode[4];

    StdString    m_strEd[4];

    StdString    m_strDevice;
    StdString    m_strId;

    StdString    m_strCounter[2];

    bool        m_bCkDo[8];
    bool        m_bCkDi[8];
    bool        m_bCkAuto[2];

    DummyDivice* m_pDevice;

};

#endif //DEVICE_DIALOG_H