#include "stdafx.h"
#include "DialogBase.h"
#include "resource.h"
#include <tchar.h>
#include <process.h>
#include <commctrl.h>
#include <crtdbg.h>
                            

std::map<HWND, DialogBase*> DialogBase::ms_mapDialog;



// コンストラクタ
DialogBase::DialogBase():
m_hWnd      (0),
m_hParent   (0),
m_bModal    (false)
{

}

// コンストラクタ
DialogBase::DialogBase(HMODULE hModule, char* pStr, HWND hParent):
m_hWnd  (0),
m_bModal    (false)
{
    m_hModule = hModule;
    m_hParent = hParent;

    errno_t err;
    err = strcpy_s(&m_strResource[0], sizeof(m_strResource), pStr);
}


// デストラクタ
DialogBase::~DialogBase()
{
    Destory();
}

//!< ウインドウ破棄
bool DialogBase::Destory()
{
    bool bRet = true;
    if (m_hWnd != 0)
    {
        std::map<HWND, DialogBase*>::iterator ite;
        bRet = (DestroyWindow(m_hWnd)?true:false);
        ite = ms_mapDialog.find(m_hWnd);

        _ASSERT(ite != ms_mapDialog.end());

        if (ite != ms_mapDialog.end())
        {
            ms_mapDialog.erase(ite);
        }
        else
        {
            bRet = false;
        }
        m_hWnd = 0;
    }
    return bRet;
}

//!< ウインドウ生成
bool DialogBase::Create()
{
    m_hWnd = ::CreateDialogA(m_hModule, m_strResource, NULL, StaticDialogProc);
    if (m_hWnd == NULL)
    {
        return false;
    }
    ms_mapDialog[m_hWnd] = this;
    ShowWindow(m_hWnd, SW_SHOW);
    UpdateWindow(m_hWnd);
    OnInitDialog();
    return true;
}


//ダイアログ終了
void DialogBase::EndDialog(INT_PTR nResult)
{
    _ASSERT(::IsWindow(m_hWnd));
    if (IsModal())
    {
        ::EndDialog(m_hWnd, nResult);
        Destory();
        m_hWnd = NULL;
    }
    else
    {
        //モーダレスダイアログは非表示にするだけ
        Show(false);
    }
}

//!< ウインドウ表示
bool DialogBase::Show(bool bShow) const
{
    _ASSERT(::IsWindow(m_hWnd));

    if (bShow)
    {
        ShowWindow(m_hWnd, SW_SHOW);
    }
    else
    {
        ShowWindow(m_hWnd, SW_HIDE);
    }
    return true;
}

// モーダルダイアログ表示
int DialogBase::DoModal()
{
    int iRet;
    m_bModal = TRUE;

    iRet = ::DialogBoxParamA(
            m_hModule,
            m_strResource,
            m_hParent,
            (DLGPROC)StaticDialogProc,
            (LPARAM)this

    );
    return iRet;
}


//!< コマンド処理
BOOL DialogBase::OnCommand(int iId, int iNotify, HWND hCtrl)
{
    switch(iId)
    {
    case IDOK:      {OnOk();    break;}
    case IDCANCEL:  {OnCancel();    break;}
    default:        {break;}
    }
    return TRUE;
}

//!< ダイアログプロシジャ
BOOL DialogBase::DialogProc(UINT msg, WPARAM wp, LPARAM lp)
{
	switch( msg )
    {
	case WM_INITDIALOG:  // ダイアログボックスが作成されたとき
		return TRUE;

	case WM_COMMAND:     // ダイアログボックス内の何かが選択されたとき
        {
            int  iId        = LOWORD( wp );
            int  iNotify    = HIWORD( wp );
            HWND hCtrl = reinterpret_cast<HWND>(lp);
            OnCommand(iId, iNotify, hCtrl);
			break;
		}
		return TRUE;

	case WM_DIALOG_UPDATE:
        OnDlgUpdate();
		return TRUE;

    case WM_POWERBROADCAST:
        OnPowerChange(static_cast<int>(wp));
        return TRUE;

	case WM_CLOSE:
        OnClose();
		return TRUE;
	} 
	return FALSE;

}


//!< ダイアログプロシジャ
INT_PTR CALLBACK DialogBase::StaticDialogProc(HWND hDlg, UINT msg, WPARAM wp, LPARAM lp)
{
	if ( msg == WM_INITDIALOG)
    {
        if(lp != 0)
        {
            //モーダルダイアログ(DialogBoxParamで作成)
            DialogBase* pDlg = reinterpret_cast<DialogBase*>(lp);
            pDlg->m_hWnd = hDlg;
            ms_mapDialog[hDlg] = pDlg;
            ShowWindow(hDlg, SW_SHOW);
            UpdateWindow(hDlg);
            pDlg->OnInitDialog();
            return TRUE;
        }
    }

    DialogBase* pDlg = DialogBase::GetDialog(hDlg);
    if (pDlg == NULL)
    {
        return FALSE;
    }
    return pDlg->DialogProc(msg, wp, lp);
}


//リストコントロール項目追加
void DialogBase::InsColumn(HWND hWnd, StdChar *str, int cx, int iSub)
{
    LV_COLUMN col;

    col.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
    col.fmt = LVCFMT_LEFT;
    col.cx = cx;
    col.pszText = str;
    col.iSubItem = iSub;
    ListView_InsertColumn(hWnd, iSub, &col);
    return;
}

//リストコントロールアイテム追加
void DialogBase::InsItem(HWND hWnd, int iItem, StdChar* str)
{
    LV_ITEM item;
    item.mask = LVIF_TEXT;
    item.pszText = str;
    item.iItem = iItem;
    item.iSubItem = 0;
    ListView_InsertItem(hWnd, &item);
    return;
}

//リストコントロールサブアイテム設定
void DialogBase::SetSubitem(HWND hWnd, int item, int subitem, StdChar *str)
{
    LV_ITEM itm;

    itm.mask = LVIF_TEXT;
    itm.pszText = str;
    itm.iItem = item;
    itm.iSubItem = subitem;

    ListView_SetItem(hWnd, &itm);
    return;
}

DialogBase* DialogBase::GetDialog(HWND hDlg)
{
    std::map<HWND, DialogBase*>::iterator iteMap;
    iteMap = ms_mapDialog.find(hDlg);
    if (iteMap != ms_mapDialog.end())
    {
        return iteMap->second;
    }   
    return NULL;
}
void DialogBase::AddListComboBox(HWND hWnd, const std::vector<StdString>& lstStr)
{
    SendMessage(hWnd, CB_RESETCONTENT, 0L, 0L);

    std::vector<StdString>::const_iterator ite;

    for(ite  = lstStr.begin();
        ite != lstStr.end();
        ite++)
    {
        AddComboBox( hWnd, ite->c_str());
    }
}

void DialogBase::AddComboBox(HWND hWnd, const StdChar *str)
{
    SendMessage(hWnd, CB_ADDSTRING, 0L, reinterpret_cast<LPARAM>(str));
}


int  DialogBase::GetComboBoxSel(HWND hWnd)
{
    int iIdx;
    iIdx = SendMessage(hWnd, CB_GETCURSEL, 0L, 0L);
    return iIdx;
}

void DialogBase::SetComboBoxSel(HWND hWnd, int iNo)
{
    SendMessage(hWnd, CB_SETCURSEL, static_cast<WPARAM>(iNo), 0L);
}
