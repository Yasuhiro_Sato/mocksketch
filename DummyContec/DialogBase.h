
#ifndef DIALOG_BASE_H
#define DIALOG_BASE_H

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include<map>
#include<vector>
#include<string>

class DialogBase
{
friend DialogBase;
public:
    //!< コンストラクタ
    DialogBase(HMODULE hModule, char* pStr, HWND hParent = NULL);

    //!< コンストラクタ
    ~DialogBase();

    //!< ウインドウ破棄
    bool Destory();

    //!< ウインドウ生成
    bool Create();

    void EndDialog(INT_PTR nResult);

    //!< ウインドウ表示
    bool Show(bool bShow = true) const;

    HWND GetHwnd() const {return m_hWnd;}

    static DialogBase* GetDialog(HWND hDlg);

    //!< ダイアログプロシジャ
    static INT_PTR CALLBACK StaticDialogProc(HWND hDlg, UINT msg, WPARAM wp, LPARAM lp);

    //================
    // イベント
    //================

    //!< ウインドウ生成時処理
    virtual void OnInitDialog(){;}

    virtual void OnOk(){EndDialog(IDOK);}

    virtual void OnCancel(){EndDialog(IDCANCEL);}

    virtual void OnClose(){;}

    virtual void OnDlgUpdate(){;}

    virtual void OnPowerChange(int iPowerSts){;}

    virtual BOOL OnCommand(int iId, int iNotify, HWND hCtrl);

    //!< ダイアログプロシジャ
    BOOL DialogProc(UINT msg, WPARAM wp, LPARAM lp);

    //======================
    // リストコントロール
    //======================
    //リストコントロール項目追加
    void InsColumn(HWND hWnd, StdChar *str, int cx, int iSub);

    //リストコントロールサブアイテム設定
    void InsItem(HWND hWnd, int item, StdChar* str);

    //リストコントロールサブアイテム設定
    void SetSubitem(HWND hWnd, int item, int subitem, StdChar *str);

    //======================
    // コンボボックス
    //======================
    void AddListComboBox(HWND hWnd, const std::vector<StdString>& lstStr);

    void AddComboBox(HWND hWnd, const StdChar *str);

    void InsComboBox(HWND hWnd, int iNo, const StdChar *str);

    int  GetComboBoxSel(HWND hWnd);

    void SetComboBoxSel(HWND hWnd, int iNo);

    //!< モーダルダイアログ
    int DoModal();

    //!< モーダルダイアログ問い合わせ
    bool IsModal() const { return m_bModal;}

private:
    //!< コンストラクタ
    DialogBase();

protected:

    static std::map<HWND, DialogBase*> ms_mapDialog;


    //--------------
    //  
    //--------------
    //!< ウインドウハンドル
    HWND m_hWnd;

    //!< 親ウインドウ
    HWND m_hParent;

    //!< モジュールハンドル
    HMODULE m_hModule;

    bool m_bModal;

    //!< ダイアログリソース名
    char  m_strResource[256];
};

#endif //DEVICE_DIALOG_H