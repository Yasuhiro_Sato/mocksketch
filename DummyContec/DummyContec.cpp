// DummyContec.cpp : DLL アプリケーション用にエクスポートされる関数を定義します。
//

#include "stdafx.h"
#include  "DummyContec.H"
#include  "DeviceDialog.H"
#include  "CAIO.H"
#include <process.h>
#include <math.h>
#include <cstdlib>
#include <time.h>
//
AIO_PRODUCT DummyContec::ms_aioProduct[] ={
{ "AIO-121602AH-PCI"      ,                                 //ボード名称
  "AIO0001"  ,                                              //ドライバー名
        DEV_PCI,                                            //デバイスタイプ 
        16,                                                 //入力チャンネル数
        E_PM10| E_P10| E_PM1| E_PM01| E_PM001| E_P1| E_P01| E_P001,       //入力レンジ
        2,                                                  //出力チャンネル数
        E_PM10,                                             //出力レンジ
        12,                                                 //分解能
        0,                                                  //DIO入出力点数
        4,                                                  //DI入力点数
        4,                                                  //D0出力点数
        1,                                                  //カウンターチャンネル
        32                                                   //カウンタービット数
},
/*

{ "AIO-163202FX-USB"      ,                                 //ボード名称
  "AIO0002"  ,                                              //ドライバー名
        DEV_USB,                                            //デバイスタイプ 
        32,                                                 //入力チャンネル数
        E_PM10| E_PM5| E_PM25| E_P10| E_P5| E_P25,          //入力レンジ
        2,                                                  //出力チャンネル数
        E_PM10| E_PM5| E_PM25| E_PM125| E_P10| E_P5| E_P25, //出力レンジ
        16,                                                 //分解能
        0,                                                  //DIO入出力点数
        8,                                                  //DI入力点数
        8,                                                  //D0出力点数
        2,                                                  //カウンターチャンネル
        32                                                   //カウンタービット数
},

{ "AO-1616L-LPE"      ,                                     //ボード名称
  "AIO0003"  ,                                              //ドライバー名
        DEV_PCI,                                            //デバイスタイプ 
        0,                                                  //入力チャンネル数
        0,                                                  //入力レンジ
        16,                                                 //出力チャンネル数
        E_PM10,                                             //出力レンジ
        16,                                                 //分解能
        0,                                                  //DIO入出力点数
        4,                                                  //DI入力点数
        4,                                                  //D0出力点数
        1,                                                  //カウンターチャンネル
        32                                                  //カウンタービット数
},

{ "AI-1204Z-PCI"      ,                                     //ボード名称
  "AIO0004"  ,                                              //ドライバー名
        DEV_PCI,                                            //デバイスタイプ 
        4,                                                  //入力チャンネル数
        E_PM10| E_PM1| E_PM01 | E_PM001| E_P10| E_P1| E_P01 | E_P001,         //入力レンジ
        0,                                                  //出力チャンネル数
        0,                                                  //出力レンジ
        12,                                                 //分解能
        0,                                                  //DIO入出力点数
        0,                                                  //DI入力点数
        0,                                                  //D0出力点数
        0,                                                  //カウンターチャンネル
        32                                                  //カウンタービット数
},
{
  "AO-1616L-LPE",                                          //ボード名称
  "AIO0004"  ,                                              //ドライバー名
        DEV_PCI,                                            //デバイスタイプ 
        0,                                                  //入力チャンネル数
        0,                                                  //入力レンジ
        16,                                                 //出力チャンネル数
        E_PM10,                                             //出力レンジ
        16,                                                 //分解能
        0,                                                  //DIO入出力点数
        4,                                                  //DI入力点数
        4,                                                  //D0出力点数
        1,                                                  //カウンターチャンネル
        32                                                  //カウンタービット数
},
*/
};


/*
, "SEM-AIO-CB1"
"AIO-163202FX-USB"
"DIG-100M1002-PCI"
"AIO-121601M-PCI"
"AO-1604LI-PE"
"AIO-160802LI-PE"
"AI-1616LI-PE"
"AO-1616L-LPE"
"AO-1608L-LPE"
"AI-1664LA-LPE"
"AI-1204Z-PCI"
"AI-1216AH-PCI"
"AIO-161601UE3-PE"
"AIO-161601E3-PE"
"AIO-121601UE3-PE"
"AIO-121601E3-PE"
"AIO-121602AL-PCI"
"AI-1216AL-PCI"
"AO-1604CI2-PCI"
"AI-1604CI2-PCI"
"AI-1216B-RU1-PCI"
"AI-1216B-RB1-PCI"
"AO-1604L-LPE"
"AIO-160802L-LPE"
"AI-1616L-LPE"
"AIO-160802AY-USB"
"DA16-8(LPCI)L"
"DA16-16(LPCI)L"
"AIO-163202F-PE"
"AI-1608AY-USB"
"AD16-16(PCI)EV"
"AD12-16(PCI)EV"
"AD12-16U(PCI)EV"
"AD16-64(LPCI)LA"
"AD16-16U(PCI)EV"
"DAI16-4(LPCI)L"
"ADI16-16(LPCI)L"
"ADAI16-8/2(LPCI)L"
"PTI-4(USB)"
"DAI16-4(USB)"
"ADI16-4(USB)"
"PTI-4(FIT)GY"
"ADA16-8/2(CB)L"
"DA16-4(LPCI)L"
"ADA16-8/2(LPCI)L"
"AD16-16(LPCI)L"
"DAI16-4(FIT)GY"
"ADI16-4(FIT)GY"
"ADA16-32/2(PCI)F"
"ADA16-32/2(CB)F"
"DAI12-4(FIT)GY"
"ADI12-8(FIT)GY"
"DAI12-4(USB)GY"
"ADI12-8(USB)GY"
"AD12-16U(PCI)EH"
"ADI12-8CL(PC)H"
"ADI16-4L(PCI)"
"DA12-8(PCI)"
"AD12-64(PCI)"
"AD12-16(PCI)"
"DA12-4(PCI)"
"DA12-16(PCI)"
"ADI12-16(PCI)"
"AD12-16(PCI)E"
"AD16-16U(PC)EH"
"AD16-16(PC)EH"
"AD12-16U(PC)EH
"AD12-16(PC)EH
"AD12-8(PM)
"DA12-8L(PC)
"DAI12-8C(PC)
"DAI12-4C(PC)
"DA12-4(PC)
"AD12-16(PC)
*/




DummyContec::DummyContec()
{
    m_iMaxDivce = sizeof(ms_aioProduct)/sizeof(AIO_PRODUCT);
    srand((unsigned int)time(0));
}

DummyContec::~DummyContec()
{
    DummyDivice* pDevice;
    std::vector<DummyDivice*>::iterator ite;
    for(ite  = m_lstDevice.begin();
        ite != m_lstDevice.end();
        ite++)
    {
        pDevice = *ite;
        if (pDevice)
        {
            delete pDevice;
        }
    }
}

AIO_PRODUCT* DummyContec::GetProduct(int iProductNo)
{
    if( iProductNo < 0) {return NULL;}
    if( iProductNo >= m_iMaxDivce) {return NULL;}

    return &ms_aioProduct[iProductNo];
}

int DummyContec::Init(const char* strDevName)
{

    //既に登録済み
    DummyDivice* pDevice;
    std::vector<DummyDivice*>::iterator ite;
    for(ite  = m_lstDevice.begin();
        ite != m_lstDevice.end();
        ite++)
    {
        pDevice = *ite;
        if (pDevice)
        {
            if(strcmp(pDevice->GetProduct()->strDriver, strDevName) == 0)
            {
                return pDevice->GetId();
            }
        }
    }

    for (int iCnt = 0; iCnt < m_iMaxDivce; iCnt++)
    {
        if( strcmp(ms_aioProduct[iCnt].strDriver, strDevName) == 0)
        {
            DummyDivice* pDevice = new DummyDivice(&ms_aioProduct[iCnt], m_hModule);
            m_lstDevice.push_back(pDevice);

            int iSize = static_cast<int>(m_lstDevice.size()) - 1;
            pDevice->SetId(iSize);
            return iSize;
        }
    }
    return -1;
}

int DummyContec::GetDeviceType(const char* strDevName)
{
    for (int iCnt = 0; iCnt < m_iMaxDivce; iCnt++)
    {
        if( strcmp(ms_aioProduct[iCnt].strDriver, strDevName) == 0)
        {
            return ms_aioProduct[iCnt].eDevType;
        }
    }
    return -1;
}

bool DummyContec::IsInit()
{
    return (m_lstDevice.size() != 0);
}

DummyDivice* DummyContec::GetDevice(int iNo)
{
    if (iNo >= static_cast<int>(m_lstDevice.size()))
    {
        return NULL;
    }

    if (iNo < 0)
    {
        return NULL;
    }
    return m_lstDevice[iNo];
}

bool DummyContec::ExitDevice(int iNo)
{
    if (iNo >= static_cast<int>(m_lstDevice.size())){return false;}
    if (iNo < 0)  {return false;}
    m_lstDevice[iNo]->Stop();

    delete m_lstDevice[iNo];
    m_lstDevice[iNo] = NULL;
    return true;
}



//================================
//================================
//================================

//----------------
//コンストラクタ
//----------------
DummyDivice::DummyDivice()
{
}

//----------------
//コンストラクタ
//----------------
DummyDivice::DummyDivice(AIO_PRODUCT* pProduct, HMODULE hModule):
    m_iId             (0),
    //m_iProductNo     (0),
    m_pProduct       (pProduct),
    m_iDo            (0),
    m_iDi            (0),
    m_hThread       (0),
    m_dwThreadId    (0),
    m_psDialog      (NULL),
    m_bPause        (false),
    m_bStop         (true),
    m_bResumed      (false),
    m_bExec         (false),
    m_bAoExec       (false),
    m_bAiExec       (false),
    m_dwSleepTime   (1),
    m_dwUpdateDlgTime(500)
{
    memset(&m_iAi[0], 0, sizeof(m_iAi));
    memset(&m_iAo[0], 0, sizeof(m_iAo));
    memset(&m_dwCount[0], 0, sizeof(m_dwCount));


    m_iAoRange  = static_cast<int>(double(pow(2.0, static_cast<double>(m_pProduct->iRes)))) ;
    m_iAoRange2 = static_cast<int>(m_iAoRange / 2.0);

    for (int iCnt = 0; iCnt < MAX_CH; iCnt++)
    {
        m_dAiHz[iCnt] = 1.0;
        m_modeAi[iCnt] = AI_NONE;
    }

    for (int iCnt = 0; iCnt < MAX_COUNT_CH; iCnt++)
    {
        m_bExecCount[iCnt] = false;
    }
    
    m_iAiMaxCh = 1;
    m_iAoMaxCh = 1;

    if (pProduct->iInChNum == 0)
    {
        m_iAiMaxCh = 0;
    }

    if (pProduct->iOutChNum == 0)
    {
        m_iAoMaxCh = 0;
    }

    m_psDialog = new DiviceDialog(hModule, 0, this);
    m_psDialog->Create();
    Start();
}

//----------------
//デストラクタ
//----------------
DummyDivice::~DummyDivice()
{
    Stop();
    delete m_psDialog;
}

//----------------
// DO設定
//----------------
void DummyDivice::SetDo(int iBit, bool bOn)
{
    short cI = 1;
    if (bOn)
    {
        cI = cI << iBit;
        m_iDo |= cI;
    }
    else
    {
        cI = ~(cI << iBit);
        m_iDo &= cI;
    }}

//----------------
// DO取得
//----------------
bool DummyDivice::GetDo(int iBit) const
{
    return ((m_iDo>>iBit) & 1);
}

//----------------
// DI設定
//----------------
void DummyDivice::SetDi(int iBit, bool bOn)
{
    short cI = 1;
    if (bOn)
    {
        cI = cI << iBit;
        m_iDi |= cI;
    }
    else
    {
        cI = ~(cI << iBit);
        m_iDi &= cI;
    }
}

//----------------
// DI取得
//----------------
bool DummyDivice::GetDi(int iBit) const
{
    return ((m_iDi>>iBit) & 1);
}

//----------------
// 開始
//----------------
bool DummyDivice::Start()
{
    if (m_hThread == 0)
    {
        m_bPause = false;
        m_bStop  = false;
        m_dwBeforeTime = 0;

        //m_hThread = (HANDLE)_beginthreadex( NULL, 0, 
        //    &DummyDivice::ThreadFunc, this, 0, &m_dwThreadId );

        m_hThread = CreateThread(NULL, 0, DummyDivice::ThreadFunc, (LPVOID)this, 0, &m_dwThreadId);//Thread1を開始
        return true;
    }
    return false;
}

//----------------
// リセット
//----------------
bool DummyDivice::Reset()
{
    // AioResetDevice より呼び出し
    Stop();
    m_bResumed = false;
    m_bAoExec = false;
    return true;
}


//----------------
// 一時停止
//----------------
bool DummyDivice::Pause()
{
    m_bPause = true;
    return true;
}

//----------------
// 停止
//----------------
bool DummyDivice::Stop()
{
    m_bPause = false;
    m_bStop  = true;

    if (m_hThread != 0)
    {
        DB_PRINT(_T("Stop Start %d\n"), GetThreadId());
        WaitForSingleObject(m_hThread,INFINITE);
        CloseHandle(m_hThread);
        DB_PRINT(_T("Stop End %d\n"), GetThreadId());
    }
    m_hThread = 0;
    return true;
}

//----------------
// スレッド
//----------------
void DummyDivice::ThreadCallFunc()
{
    DWORD dwCurTime = GetTickCount();//  より正確な時刻は、timeGetTime (要winmm.lib)
    for(int iCh = 0; iCh < m_pProduct->iInChNum; iCh++)
    {
        if (m_modeAi[iCh] == AI_SIN)
        {
            m_iAi[iCh] = CreateSin(iCh, dwCurTime);
        }
        else if (m_modeAi[iCh] == AI_RECT)
        {
            m_iAi[iCh] = CreateRect(iCh, dwCurTime);
        }
        else if (m_modeAi[iCh] == AI_RANDOM)
        {
            m_iAi[iCh] = CreateRandom();
        }
    }

    for(int iCh = 0; iCh < m_pProduct->iCntNum; iCh++)
    {
        if (m_bExecCount[iCh]){m_dwCount[iCh]++;}
    }

    if( dwCurTime > (m_dwBeforeTime + m_dwSleepTime))
    {
        if (m_psDialog)
        {
            ::SendMessage(m_psDialog->GetHwnd(), WM_DIALOG_UPDATE, 0, 0);
        }
        m_dwBeforeTime = dwCurTime;
    }

}

//----------------
// スレッド
//----------------
//unsigned __stdcall  DummyDivice::ThreadFunc(void*  pVoid)
DWORD __stdcall  DummyDivice::ThreadFunc(LPVOID  pVoid)
{
    DWORD dwSleep;
    DummyDivice* pDev = reinterpret_cast<DummyDivice*>(pVoid);
    
    pDev->m_bExec = true;
    dwSleep = pDev->GetSleepTime();
    while(1)
    {
        if (pDev->IsStopRequest())
        {
            DB_PRINT(_T("Stop %d\n"), pDev->GetThreadId());
            break;
        }

        if (pDev->IsPauseRequest())
        {
            Sleep(10);
            continue;
        }

        pDev->ThreadCallFunc();
        Sleep(dwSleep);
    }

    pDev->m_bExec = false;

    DB_PRINT(_T("Exit %d\n"), pDev->GetThreadId());
    return 0;
}


int DummyDivice::CreateSin(int iCh, DWORD dwTime)
{
    if (iCh < 0)        { return 0;}
    if (iCh >= MAX_CH)  { return 0;}


    //時間の正規化
    int iDev  = static_cast<int>(1000.0 / m_dAiHz[iCh]);
    int iTime = dwTime % iDev;
    
    int iVal;

    double dVal = 2.0 * M_PI * iTime * m_dAiHz[iCh] / 1000.0;
    iVal = static_cast<int>(m_iAoRange2 * sin(dVal) + m_iAoRange2);

    return iVal;
}

int DummyDivice::CreateRect(int iCh, DWORD dwTime)
{
    if (iCh < 0)        { return 0;}
    if (iCh >= MAX_CH)  { return 0;}


    //時間の正規化
    int iDev  = static_cast<int>(1000.0 / (2.0 * m_dAiHz[iCh]));
    int iTime = dwTime % (iDev * 2);

    if (iTime > iDev)
    {
        return m_iAoRange;
    }
    else
    {
        return 0;
    }
}


int DummyDivice::CreateRandom()
{
    int iVal;
    iVal = rand() % m_iAoRange;
    return iVal;
}

