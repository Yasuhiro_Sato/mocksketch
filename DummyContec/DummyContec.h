/**
 * @brief			CDummyContec ヘッダーファイル
 * @file			CDummyContec.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

#ifndef  _DUMMY_CONTEC_H__
#define  _DUMMY_CONTEC_H__
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include<map>
#include<vector>
#include "resource.h"

/*---------------------------------------------------*/
/*  Enums                                            */
/*---------------------------------------------------*/
enum E_DEVICE_TYPE
{
  DEV_PCI       = 1,
  DEV_PC_CARD   = 2,
  DEV_USB       = 3,
  DEV_DEMO      = 5,
  DEV_CARD_BUS  = 6,
};
 

enum E_RAMGE
{
    E_PM10				=	0x00000001,  //±10V
    E_PM5				=	0x00000002,  //±5V
    E_PM25				=	0x00000004,  //±2.5V
    E_PM125				=	0x00000008,  //±1.25V
    E_PM1				=	0x00000010,  //±1V
    E_PM0625			=	0x00000020,  //±0.625V
    E_PM05				=   0x00000040,  //±0.5V
    E_PM03125			=	0x00000080,	//±0.3125V
    E_PM025				=	0x00000100,	//±0.25V
    E_PM0125			=	0x00000200,	//±0.125V
    E_PM01				=	0x00000400,	//±0.1V
    E_PM005				=	0x00000800,	//±0.05V
    E_PM0025			=	0x00001000,	//±0.025V
    E_PM00125			=	0x00002000,	//±0.0125V
    E_PM001				=	0x00004000,	//±0.01V
    E_P10				=	0x00008000,	//0〜10V
    E_P5				=	0x00010000,	//0〜5V
    E_P4095				=	0x00020000,	//0〜4.095V
    E_P25				=	0x00040000,	//0〜2.5V
    E_P125				=	0x00080000,	//0〜1.25V
    E_P1				=	0x00100000,	//0〜1V
    E_P05				=	0x00200000,	//0〜0.5V
    E_P025				=	0x00400000,	//0〜0.25V
    E_P01				=	0x00800000,	//0〜0.1V
    E_P005				=	0x01000000,	//0〜0.05V
    E_P0025				=	0x02000000,	//0〜0.025V
    E_P00125			=	0x04000000,	//0〜0.0125V
    E_P001				=	0x08000000,	//0〜0.01V
    E_P20MA				=	0x10000000,	//0〜20mA
    E_P4TO20MA			=	0x20000000,	//4〜20mA
    E_P1TO5				=	0x40000000,	//1〜5V
};

enum E_EXEC_STS
{
    STS_NONE ,
    STS_STOP ,
    STS_EXEC ,
    STS_STANBY,
};



enum E_AI_MODE
{
  AI_NONE       = 0,  // 動作なし
  AI_SIN        ,     // サイン波  
  AI_RECT       ,     // 矩形波波  
  AI_RANDOM     ,     // ランダム
};


/*---------------------------------------------------*/
/*  Class                                            */
/*---------------------------------------------------*/
class DiviceDialog;

/*---------------------------------------------------*/
/*  Structs                                          */
/*---------------------------------------------------*/
struct AIO_PRODUCT
{
    char            *strBoardName;      //ボード名称
    char            *strDriver;         //ドライバー名
    E_DEVICE_TYPE   eDevType;           //デバイスタイプ 
    int             iInChNum;           //入力チャンネル数
    unsigned long   ulInRange;          //入力レンジ
    int             iOutChNum;          //出力チャンネル数
    unsigned long   ulOutRange;         //出力レンジ
    int             iRes;               //分解能
    int             iDiInOut;           //DIO入出力点数
    int             iDiIn;              //DI入力点数
    int             iDiOut;             //D0出力点数
    int             iCntNum;            //カウンターチャンネル数
    int             iCntBit;            //カウンタービット数
};

class DummyDivice
{
    static const int MAX_CH = 32;
    static const int MAX_DGITAL_CH = 8;
    static const int MAX_COUNT_CH = 2;
public:

protected:
    int m_iId;
    //int m_iProductNo;

    AIO_PRODUCT* m_pProduct;

    long m_iAi[MAX_CH];
    long m_iAo[MAX_CH];

    short m_iAiRangeUnit[MAX_CH];
    short m_iAoRangeUnit[MAX_CH];

    long m_iAiMaxCh;
    long m_iAoMaxCh;

    short m_iDo;
    short m_iDi;

    bool  m_bExecCount[MAX_COUNT_CH];
    DWORD m_dwCount[MAX_COUNT_CH];


    int       m_iAoRange;
    int       m_iAoRange2;

    double    m_dAiHz[MAX_CH];

    bool   m_bAoExec;
    bool   m_bAiExec;

    //======================
    HANDLE m_hThread;

    //unsigned  m_dwThreadId;
    DWORD  m_dwThreadId;

    bool   m_bPause;

    bool   m_bStop;

    bool   m_bExec;

    DWORD   m_dwSleepTime;

    DWORD   m_dwBeforeTime;

    DWORD   m_dwUpdateDlgTime;

    E_AI_MODE m_modeAi[MAX_CH];

    DiviceDialog* m_psDialog;

    bool   m_bResumed;   //サスペンドからの復帰


public:
    DummyDivice(AIO_PRODUCT* pProduct, HMODULE hModule);
    virtual ~DummyDivice();

    //---------------
    // プロパティ
    //---------------
    void SetId(int iId){m_iId = iId;}
    int  GetId() const {return m_iId;}

    AIO_PRODUCT* GetProduct(){return m_pProduct;}

    void SetDo(int iBit, bool bOn);
    bool GetDo(int iBit) const;

    void SetDi(int iBit, bool bOn);
    bool GetDi(int iBit) const;

    void SetDobyte(short sData){ m_iDo = sData;}
    void GetDibyte(short* pData){*pData =m_iDi;}


    void      SetAiMode(int iCh, E_AI_MODE eMode){m_modeAi[iCh] = eMode;}
    E_AI_MODE GetAiMode(int iCh){return m_modeAi[iCh];}

    void SetAi(int iCh, int iVal){m_iAi[iCh] = iVal;}
    long GetAi(int iCh) const{return m_iAi[iCh];}
    void GetAiM(long* pVal, int iChNum ) const {memcpy(pVal, &m_iAi[0], sizeof(long)* iChNum); }

    void SetAo(int iCh, int iVal){m_iAo[iCh] = iVal;}
    long GetAo(int iCh) const{return m_iAo[iCh];}
    void SetAoM(const long* pVal, int iChNum ) const {memcpy((void*)&m_iAo[0], pVal, sizeof(long)* iChNum); }

    long GetAiConvMaxCh(){return m_iAiMaxCh;}
    long GetAoConvMaxCh(){return m_iAoMaxCh;}

    void SetAiConvMaxCh(int iCh){m_iAiMaxCh = iCh;}
    void SetAoConvMaxCh(int iCh){m_iAoMaxCh = iCh;}

    void  SetAiRagne(int iCh, int iRange){m_iAiRangeUnit[iCh] = static_cast<short>(iRange);}
    short GetAiRagne(int iCh) {return m_iAiRangeUnit[iCh];}

    void  SetAoRagne(int iCh, int iRange){m_iAoRangeUnit[iCh] = static_cast<short>(iRange);}
    short GetAoRagne(int iCh) {return m_iAoRangeUnit[iCh];}

    //void SetDlg(const DialogBase* pDialog){m_pDialog = pDialog;}

    bool Reset();
    
    void SetResumed(){m_bResumed = true;}

    bool IsResumed(){return m_bResumed;}

    //---------
    //Count
    //---------
    void  StartCount(int iCh){m_bExecCount[iCh] = true;}
    void  StopCount(int iCh){m_bExecCount[iCh] = false;}
    void  SetCount(int iCh, long lVal){m_dwCount[iCh] = static_cast<DWORD>(lVal);}
    long  GetCount(int iCh){ return static_cast<long>(m_dwCount[iCh]);}

    //---------
    //Ao
    //---------
    void StartAo(){m_bAoExec = true;};

    void StopAo(){m_bAoExec = false;};

    bool IsAoExec(){return m_bAoExec;};


    //---------
    //Ai
    //---------
    void StartAi(){m_bAiExec = true;};

    void StopAi(){m_bAiExec = false;};

    bool IsAiExec(){return m_bAiExec;};

    //---------------
    // スレッド関連
    //---------------
    bool Start();
    bool Stop();
    bool Pause();

    bool IsExec(){ return m_bExec;}
    bool IsStopRequest(){ return m_bStop;}
    bool IsPauseRequest(){ return m_bPause;}
    DWORD GetThreadId(){return m_dwThreadId;}
    DWORD GetSleepTime(){return m_dwSleepTime;}
    void SetSleepTime(DWORD dwSleep){m_dwSleepTime = dwSleep;}
    void ThreadCallFunc();
    //static unsigned __stdcall ThreadFunc(void*  pVoid);
    static DWORD __stdcall  ThreadFunc(LPVOID  pVoid);

    //---------------

protected:
    DummyDivice();

    int CreateSin( int iCh, DWORD dwTime);
    int CreateRect(int iCh, DWORD dwTime);
    int CreateRandom();


};





/**
 * @class   CIoAccessBoard
 * @brief                       
 */
class DummyContec
{
protected:
    static AIO_PRODUCT ms_aioProduct[];

    std::vector<DummyDivice*>     m_lstDevice;

    int m_iMaxDivce;

    HMODULE m_hModule;


public:
    DummyContec();

    virtual ~DummyContec();

    void     SetModuleHandle(HMODULE hDll){m_hModule = hDll;}

    AIO_PRODUCT* GetProduct(int iProductNo); 

    int Init(const char* strDevName);

    int GetDeviceType(const char* strDevName);

    bool IsInit();

    DummyDivice* GetDevice(int iNo);

    bool ExitDevice(int iNo);

};






#endif //_DUMMY_CONTEC_H__