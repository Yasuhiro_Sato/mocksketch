//Ci


// dllmain.cpp : DLL アプリケーションのエントリ ポイントを定義します。
#include "stdafx.h"
#include "CAIO.H"
#include "DeviceDialog.H"
#include "DummyContec.h"

DummyContec*     g_pDummyContec;

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
        g_pDummyContec = new DummyContec;
        g_pDummyContec->SetModuleHandle(hModule);


		break;

    case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
		break;

    case DLL_PROCESS_DETACH:

        delete g_pDummyContec;
        g_pDummyContec = NULL;
		break;
	}
	return TRUE;
}


//=======================================
//  CONTEC DLL 関数
//=======================================
long WINAPI AioInit(char * DeviceName, short * Id)
{
    if (DeviceName == NULL)
    {
        return 10100;
    }

    if (Id == NULL)
    {
        return 10101;
    }
    
    int iRet;
    iRet = g_pDummyContec->Init(DeviceName);

    if (iRet < 0)
    {
        return 10000;
    }

    *Id = static_cast<short>(iRet);
    return 0;
}


long WINAPI AioExit(short Id)
{
    /*この関数は、アプリケーションの終了時に実行します。
    この関数を実行せずにアプリケーションを終了すると、以降デバイスにアクセスできなくなることがあります。

    デバイスが動作中の場合、動作は停止します。
    ドライバが使用していたメモリ、スレッドをすべて開放します。
    この関数実行後は、AioInit関数を実行するまでデバイスにアクセスすることはできません
    */

    if(!g_pDummyContec->IsInit()) {return 10002;}
    DummyDivice* pDevice = g_pDummyContec->GetDevice(Id);
    if (pDevice == NULL) {return 10001;}

    g_pDummyContec->ExitDevice(Id);
    return 1;
}

long WINAPI AioResetDevice(short Id)
{
    DummyDivice* pDevice = g_pDummyContec->GetDevice(Id);
    if (pDevice == NULL)
    {
        return 10001;
    }
    pDevice->Reset();
    return 0;
}

long WINAPI AioQueryDeviceName(short Index, char * DeviceName, char * Device)
{
    AIO_PRODUCT* pProduct;
    pProduct = g_pDummyContec->GetProduct(Index);

    if (DeviceName == NULL)
    {
        return 10007;
    }

    if (Device == NULL)
    {
        return 10008;
    }

    if (pProduct == NULL)
    {
        return 10006;
    }
    strcpy(DeviceName, pProduct->strDriver);
    strcpy(Device, pProduct->strBoardName);
    return 0;
}


long WINAPI AioGetDeviceType(char * Device, short * DeviceType)
{
    if (Device == NULL)
    {
        return 10014;
    }

    if (DeviceType == NULL)
    {
        return 10015;
    }

    int iRet;
    iRet = g_pDummyContec->GetDeviceType(Device);

    if (iRet == -1)
    {
        return 10013;
    }

    *DeviceType = iRet;
    return 0;
}


long WINAPI AioSetControlFilter(short Id, short Signal, float Value)
{
    return 0;
}


long WINAPI AioGetControlFilter(short Id, short Signal, float *Value)
{
    return 0;
}

long WINAPI AioResetProcess(short Id)
{
    /*
    アプリケーションのデバッグ中などに、デバイスが動作中の状態でアプリケーションを強制終了させると、以降AioInit関数は正常終了するものの、その他の関数で20003のエラーが返ることがあります。
    AioResetProcess関数は、この状態を回避させるために使用します。AioInit関数の実行後にこの関数を使用してください。
    デバイスが動作中の場合、この関数は実行できません。
    */


    if(!g_pDummyContec->IsInit())
    {
        //ドライバを呼び出せません
        //始めにAioInit関数を実行しください。
        return 10002;
    }

    DummyDivice* pDevice = g_pDummyContec->GetDevice(Id);
    if (pDevice == NULL)
    {
        return 10001;
    }

    if (pDevice->IsResumed())
    {
        //スタンバイモードから復帰したため、AioResetDevice関数を実行してください
        return 7;
    }
    return 0;
}




long WINAPI AioSingleAi(short Id, short AiChannel, long * AiData)
{
    if(!g_pDummyContec->IsInit()) {return 10002;}
    DummyDivice* pDevice = g_pDummyContec->GetDevice(Id);
    if (pDevice == NULL) {return 10001;}
    if (pDevice->IsResumed()){return 7;}

    if (pDevice->GetProduct()->iInChNum == 0) {return 20001;}
    if (pDevice->GetProduct()->iInChNum < AiChannel) {return 11420;}
    if (AiData == NULL) {return 11421;}
    if (pDevice->IsAiExec()){return 20002;}

    *AiData = pDevice->GetAi(AiChannel);
    return 0;
}


long WINAPI AioSingleAiEx(short Id, short AiChannel, float * AiData){return 0;}

long WINAPI AioMultiAi(short Id, short AiChannels, long * AiData)
{
    if(!g_pDummyContec->IsInit()) {return 10002;}
    DummyDivice* pDevice = g_pDummyContec->GetDevice(Id);
    if (pDevice == NULL) {return 10001;}
    if (pDevice->IsResumed()){return 7;}

    if (pDevice->GetProduct()->iInChNum == 0) {return 20001;}
    if (pDevice->GetProduct()->iInChNum < AiChannels) {return 11420;}
    if (AiData == NULL) {return 11421;}
    if (pDevice->IsAiExec()){return 20002;}
    pDevice->GetAiM(AiData, AiChannels);
    return 0;
}


long WINAPI AioMultiAiEx(short Id, short AiChannels, float * AiData){return 0;}
long WINAPI AioGetAiResolution(short Id, short * AiResolution)
{
    if(!g_pDummyContec->IsInit()) {return 10002;}
    DummyDivice* pDevice = g_pDummyContec->GetDevice(Id);
    if (pDevice == NULL) {return 10001;}
    if (pDevice->IsResumed()){return 7;}
    if (AiResolution == NULL) {return 11740;}
    if (pDevice->IsAiExec()){return 20002;}

    if (pDevice->GetProduct()->iInChNum == 0) 
    {
        *AiResolution = 0;
    }
    else 
    {
        *AiResolution = pDevice->GetProduct()->iRes;
    }
    return 0;
}

//入力方式
long WINAPI AioSetAiInputMethod(short Id, short AiInputMethod){return 0;}
long WINAPI AioGetAiInputMethod(short Id, short * AiInputMethod){return 0;}

//チャンネル
//アナログ入力チャネル数の最大値を取得します
long WINAPI AioGetAiMaxChannels(short Id, short * AiMaxChannels)
{
    if(!g_pDummyContec->IsInit()) {return 10002;}
    DummyDivice* pDevice = g_pDummyContec->GetDevice(Id);
    if (pDevice == NULL) {return 10001;}
    if (pDevice->IsResumed()){return 7;}
    if (AiMaxChannels == NULL) {return 11720;}
    if (pDevice->IsAiExec()){return 20002;}

    *AiMaxChannels = pDevice->GetProduct()->iInChNum;
    return 0;
}

//変換に使用するアナログ入力チャネル数の設定を行います
long WINAPI AioSetAiChannels(short Id, short AiChannels)
{
    if(!g_pDummyContec->IsInit()) {return 10002;}
    DummyDivice* pDevice = g_pDummyContec->GetDevice(Id);
    if (pDevice == NULL) {return 10001;}
    if (pDevice->IsResumed()){return 7;}
    if (AiChannels > pDevice->GetProduct()->iInChNum) {return 11020;}
    if (AiChannels < 1) {return 11020;}
    if (pDevice->IsAiExec()){return 20002;}

    pDevice->SetAiConvMaxCh(AiChannels);

    return 0;
}

//変換に使用するアナログ入力チャネル数を取得します。
long WINAPI AioGetAiChannels(short Id, short * AiChannels)
{
    if(!g_pDummyContec->IsInit()) {return 10002;}
    DummyDivice* pDevice = g_pDummyContec->GetDevice(Id);
    if (pDevice == NULL) {return 10001;}
    if (AiChannels == NULL) {return 11030;}
    if (pDevice->IsResumed()){return 7;}
    if (pDevice->IsAiExec()){return 20002;}
    *AiChannels = static_cast<short>(pDevice->GetAiConvMaxCh());

    return 0;
}

long WINAPI AioSetAiChannel(short Id, short AiChannel, short Enabled){return 0;}
long WINAPI AioGetAiChannel(short Id, short AiChannel, short *Enabled){return 0;}
long WINAPI AioSetAiChannelSequence(short Id, short AiSequence, short AiChannel){return 0;}
long WINAPI AioGetAiChannelSequence(short Id, short AiSequence, short * AiChannel){return 0;}


//レンジ
long WINAPI AioSetAiRange(short Id, short AiChannel, short AiRange)
{
    if(!g_pDummyContec->IsInit()) {return 10002;}
    DummyDivice* pDevice = g_pDummyContec->GetDevice(Id);
    if (pDevice == NULL) {return 10001;}
    if (pDevice->IsResumed()){return 7;}
    if (pDevice->IsAiExec()){return 20002;}
    pDevice->SetAiRagne(AiChannel, AiRange);
    return 0;
}

long WINAPI AioSetAiRangeAll(short Id, short AiRange)
{
    if(!g_pDummyContec->IsInit()) {return 10002;}
    DummyDivice* pDevice = g_pDummyContec->GetDevice(Id);
    if (pDevice == NULL) {return 10001;}
    if (pDevice->IsResumed()){return 7;}
    if (pDevice->IsAiExec()){return 20002;}

    int iChMax = pDevice->GetProduct()->iInChNum;

    for (int iCh = 0; iCh < iChMax; iCh++)
    {
        pDevice->SetAiRagne(iCh, AiRange);
    }
    return 0;
}

long WINAPI AioGetAiRange(short Id, short AiChannel, short * AiRange)
{
    if(!g_pDummyContec->IsInit()) {return 10002;}
    DummyDivice* pDevice = g_pDummyContec->GetDevice(Id);
    if (pDevice == NULL) {return 10001;}
    if (AiRange == NULL) {return 11051;}
    if (pDevice->IsResumed()){return 7;}
    if (pDevice->IsAiExec()){return 20002;}
    *AiRange = pDevice->GetAiRagne(AiChannel);
    return 0;
}

//転送方式
long WINAPI AioSetAiTransferMode(short Id, short AiTransferMode){return 0;}
long WINAPI AioGetAiTransferMode(short Id, short *AiTransferMode){return 0;}
long WINAPI AioSetAiDeviceBufferMode(short Id, short AiDeviceBufferMode){return 0;}
long WINAPI AioGetAiDeviceBufferMode(short Id, short *AiDeviceBufferMode){return 0;}

long WINAPI AioSetAiTransferData(short Id, long DataNumber, long *Buffer){return 0;}
long WINAPI AioSetAiAttachedData(short Id, long AttachedData){return 0;}
long WINAPI AioGetAiSamplingDataSize(short Id, short *DataSize){return 0;}

//メモリ
long WINAPI AioSetAiMemorySize(short Id, long AiMemorySize){return 0;}
long WINAPI AioGetAiMemorySize(short Id, long *AiMemorySize){return 0;}
long WINAPI AioSetAiMemoryType(short Id, short AiMemoryType){return 0;}
long WINAPI AioGetAiMemoryType(short Id, short * AiMemoryType){return 0;}

long WINAPI AioSetAiRepeatTimes(short Id, long AiRepeatTimes){return 0;}
long WINAPI AioGetAiRepeatTimes(short Id, long * AiRepeatTimes){return 0;}
long WINAPI AioSetAiClockType(short Id, short AiClockType){return 0;}
long WINAPI AioGetAiClockType(short Id, short * AiClockType){return 0;}
long WINAPI AioSetAiSamplingClock(short Id, float AiSamplingClock){return 0;}
long WINAPI AioGetAiSamplingClock(short Id, float * AiSamplingClock){return 0;}
long WINAPI AioSetAiScanClock(short Id, float AiScanClock){return 0;}
long WINAPI AioGetAiScanClock(short Id, float * AiScanClock){return 0;}
long WINAPI AioSetAiClockEdge(short Id, short AoClockEdge){return 0;}
long WINAPI AioGetAiClockEdge(short Id, short * AoClockEdge){return 0;}

long WINAPI AioSetAiStartTrigger(short Id, short AiStartTrigger){return 0;}
long WINAPI AioGetAiStartTrigger(short Id, short * AiStartTrigger){return 0;}
long WINAPI AioSetAiStartLevel(short Id, short AiChannel, long AiStartLevel, short AiDirection){return 0;}
long WINAPI AioSetAiStartLevelEx(short Id, short AiChannel, float AiStartLevel, short AiDirection){return 0;}
long WINAPI AioGetAiStartLevel(short Id, short AiChannel, long * AiStartLevel, short * AiDirection){return 0;}
long WINAPI AioGetAiStartLevelEx(short Id, short AiChannel, float * AiStartLevel, short * AiDirection){return 0;}
long WINAPI AioSetAiStartInRange(short Id, short AiChannel, long Level1, long Level2, long StateTimes){return 0;}
long WINAPI AioSetAiStartInRangeEx(short Id, short AiChannel, float Level1, float Level2, long StateTimes){return 0;}
long WINAPI AioGetAiStartInRange(short Id, short AiChannel, long *Level1, long *Level2, long *StateTimes){return 0;}
long WINAPI AioGetAiStartInRangeEx(short Id, short AiChannel, float *Level1, float *Level2, long *StateTimes){return 0;}
long WINAPI AioSetAiStartOutRange(short Id, short AiChannel, long Level1, long Level2, long StateTimes){return 0;}
long WINAPI AioSetAiStartOutRangeEx(short Id, short AiChannel, float Level1, float Level2, long StateTimes){return 0;}
long WINAPI AioGetAiStartOutRange(short Id, short AiChannel, long *Level1, long *Level2, long *StateTimes){return 0;}
long WINAPI AioGetAiStartOutRangeEx(short Id, short AiChannel, float *Level1, float *Level2, long *StateTimes){return 0;}
long WINAPI AioSetAiStopTrigger(short Id, short AiStopTrigger){return 0;}
long WINAPI AioGetAiStopTrigger(short Id, short * AiStopTrigger){return 0;}
long WINAPI AioSetAiStopTimes(short Id, long AiStopTimes){return 0;}
long WINAPI AioGetAiStopTimes(short Id, long * AiStopTimes){return 0;}
long WINAPI AioSetAiStopLevel(short Id, short AiChannel, long AiStopLevel, short AiDirection){return 0;}
long WINAPI AioSetAiStopLevelEx(short Id, short AiChannel, float AiStopLevel, short AiDirection){return 0;}
long WINAPI AioGetAiStopLevel(short Id, short AiChannel, long * AiStopLevel, short * AiDirection){return 0;}
long WINAPI AioGetAiStopLevelEx(short Id, short AiChannel, float * AiStopLevel, short * AiDirection){return 0;}
long WINAPI AioSetAiStopInRange(short Id, short AiChannel, long Level1, long Level2, long StateTimes){return 0;}
long WINAPI AioSetAiStopInRangeEx(short Id, short AiChannel, float Level1, float Level2, long StateTimes){return 0;}
long WINAPI AioGetAiStopInRange(short Id, short AiChannel, long *Level1, long *Level2, long *StateTimes){return 0;}
long WINAPI AioGetAiStopInRangeEx(short Id, short AiChannel, float *Level1, float *Level2, long *StateTimes){return 0;}
long WINAPI AioSetAiStopOutRange(short Id, short AiChannel, long Level1, long Level2, long StateTimes){return 0;}
long WINAPI AioSetAiStopOutRangeEx(short Id, short AiChannel, float Level1, float Level2, long StateTimes){return 0;}
long WINAPI AioGetAiStopOutRange(short Id, short AiChannel, long *Level1, long *Level2, long *StateTimes){return 0;}
long WINAPI AioGetAiStopOutRangeEx(short Id, short AiChannel, float *Level1, float *Level2, long *StateTimes){return 0;}
long WINAPI AioSetAiStopDelayTimes(short Id, long AiStopDelayTimes){return 0;}
long WINAPI AioGetAiStopDelayTimes(short Id, long * AiStopDelayTimes){return 0;}
long WINAPI AioSetAiEvent(short Id, HWND hWnd, long AiEvent){return 0;}
long WINAPI AioGetAiEvent(short Id, HWND * hWnd, long * AiEvent){return 0;}
long WINAPI AioSetAiCallBackProc(short Id,
	long (_stdcall *pProc)(short Id, short AiEvent, WPARAM wParam, LPARAM lParam, void *Param), long AiEvent, void *Param){return 0;}
long WINAPI AioSetAiEventSamplingTimes(short Id, long AiSamplingTimes){return 0;}
long WINAPI AioGetAiEventSamplingTimes(short Id, long * AiSamplingTimes){return 0;}
long WINAPI AioSetAiEventTransferTimes(short Id, long AiTransferTimes){return 0;}
long WINAPI AioGetAiEventTransferTimes(short Id, long *AiTransferTimes){return 0;}


long WINAPI AioStartAi(short Id)
{
    //ちょっと実際のボードとは意味は異なるがスレッドの開始とする
    //TODO:テスト後変更
    if(!g_pDummyContec->IsInit()) {return 10002;}
    DummyDivice* pDevice = g_pDummyContec->GetDevice(Id);
    if (pDevice == NULL) {return 10001;}
    if (pDevice->IsResumed()){return 7;}
    if (pDevice->IsAiExec()){return 20002;}
    pDevice->Start();
    return 0;
}

long WINAPI AioStartAiSync(short Id, long TimeOut)
{
    return 0;
}

long WINAPI AioStopAi(short Id)
{
    //ちょっと実際のボードとは意味は異なるがスレッドの停止とする
    //TODO:テスト後変更
    if(!g_pDummyContec->IsInit()) {return 10002;}
    DummyDivice* pDevice = g_pDummyContec->GetDevice(Id);
    if (pDevice == NULL) {return 10001;}
    if (pDevice->IsResumed()){return 7;}
    if (pDevice->IsAiExec()){return 20002;}
    pDevice->Stop();
    return 0;
}

long WINAPI AioGetAiStatus(short Id, long * AiStatus)
{
    /*
    デバイス動作中 AIS_BUSY 00000001H
    開始トリガ待ち AIS_START_TRG 00000002H
    指定個数以上データ格納 AIS_DATA_NUM 00000010H
    オーバーフロー AIS_OFERR 00010000H
    サンプリングクロックエラー AIS_SCERR 00020000H
    AD変換エラー AIS_AIERR 00040000H
    */
    return 0;
}




long WINAPI AioGetAiSamplingCount(short Id, long * AiSamplingCount){return 0;}
long WINAPI AioGetAiStopTriggerCount(short Id, long * AiStopTriggerCount){return 0;}
long WINAPI AioGetAiTransferCount(short Id, long *AiTransferCount){return 0;}
long WINAPI AioGetAiTransferLap(short Id, long *Lap){return 0;}
long WINAPI AioGetAiStopTriggerTransferCount(short Id, long *Count){return 0;}
long WINAPI AioGetAiRepeatCount(short Id, long * AiRepeatCount){return 0;}
long WINAPI AioGetAiSamplingData(short Id, long * AiSamplingTimes, long * AiData){return 0;}
long WINAPI AioGetAiSamplingDataEx(short Id, long * AiSamplingTimes, float * AiData){return 0;}
long WINAPI AioResetAiStatus(short Id){return 0;}
long WINAPI AioResetAiMemory(short Id){return 0;}

//アナログ出力関数
long WINAPI AioSingleAo(short Id, short AoChannel, long AoData)
{
    if(!g_pDummyContec->IsInit()) {return 10002;}
    DummyDivice* pDevice = g_pDummyContec->GetDevice(Id);
    if (pDevice == NULL) {return 10001;}
    if (pDevice->IsResumed()){return 7;}

    if (pDevice->GetProduct()->iOutChNum == 0) {return 20001;}
    if (pDevice->GetProduct()->iOutChNum < AoChannel) {return 13000;}
    if (pDevice->IsAoExec()){return 20002;}

    pDevice->SetAo(AoChannel, AoData);
    return 0;
}

long WINAPI AioSingleAoEx(short Id, short AoChannel, float AoData)
{
    return 0;
}


long WINAPI AioMultiAo(short Id, short AoChannels, long * AoData)
{
    if(!g_pDummyContec->IsInit()) {return 10002;}
    DummyDivice* pDevice = g_pDummyContec->GetDevice(Id);
    if (pDevice == NULL) {return 10001;}
    if (pDevice->IsResumed()){return 7;}

    if (pDevice->GetProduct()->iOutChNum == 0) {return 20001;}
    if (pDevice->GetProduct()->iOutChNum < AoChannels) {return 13000;}
    if (pDevice->IsAoExec()){return 20002;}

    pDevice->SetAoM(AoData, AoChannels);
    return 0;
}
long WINAPI AioMultiAoEx(short Id, short AoChannels, float * AoData){return 0;}


long WINAPI AioGetAoResolution(short Id, short * AoResolution)
{
    if(!g_pDummyContec->IsInit()) {return 10002;}
    DummyDivice* pDevice = g_pDummyContec->GetDevice(Id);
    if (pDevice == NULL) {return 10001;}
    if (pDevice->IsResumed()){return 7;}

    if (pDevice->GetProduct()->iOutChNum == 0) {return 20001;}
    if (pDevice->IsAoExec()){return 20002;}

    *AoResolution = pDevice->GetProduct()->iRes;
    return 0;
}


long WINAPI AioSetAoChannels(short Id, short AoChannels)
{
    if(!g_pDummyContec->IsInit()) {return 10002;}
    DummyDivice* pDevice = g_pDummyContec->GetDevice(Id);
    if (pDevice == NULL) {return 10001;}
    if (pDevice->IsResumed()){return 7;}

    if (pDevice->GetProduct()->iOutChNum == 0) {return 20001;}

    pDevice->SetAoConvMaxCh(AoChannels);
    return 0;
}


long WINAPI AioGetAoChannels(short Id, short * AoChannels)
{
    if(!g_pDummyContec->IsInit()) {return 10002;}
    DummyDivice* pDevice = g_pDummyContec->GetDevice(Id);
    if (pDevice == NULL) {return 10001;}
    if (pDevice->IsResumed()){return 7;}

    if (pDevice->GetProduct()->iOutChNum == 0) {return 20001;}

    *AoChannels = static_cast<short>(pDevice->GetAoConvMaxCh());
    return 0;
}

long WINAPI AioGetAoMaxChannels(short Id, short * AoMaxChannels)
{
    if(!g_pDummyContec->IsInit()) {return 10002;}
    DummyDivice* pDevice = g_pDummyContec->GetDevice(Id);
    if (pDevice == NULL) {return 10001;}
    if (pDevice->IsResumed()){return 7;}

    *AoMaxChannels = pDevice->GetProduct()->iOutChNum;
    return 0;
}


long WINAPI AioSetAoRange(short Id, short AoChannel, short AoRange)
{
    if(!g_pDummyContec->IsInit()) {return 10002;}
    DummyDivice* pDevice = g_pDummyContec->GetDevice(Id);
    if (pDevice == NULL) {return 10001;}
    if (pDevice->IsResumed()){return 7;}

    if (pDevice->GetProduct()->iOutChNum == 0) {return 20001;}
    if (pDevice->GetProduct()->iOutChNum < AoChannel) {return 13000;}
    if (pDevice->IsAoExec()){return 20002;}


    pDevice->SetAoRagne(AoChannel, AoRange);
    return 0;
}


long WINAPI AioSetAoRangeAll(short Id, short AoRange)
{
    if(!g_pDummyContec->IsInit()) {return 10002;}
    DummyDivice* pDevice = g_pDummyContec->GetDevice(Id);
    if (pDevice == NULL) {return 10001;}
    if (pDevice->IsResumed()){return 7;}

    if (pDevice->GetProduct()->iOutChNum == 0) {return 20001;}
    if (pDevice->IsAoExec()){return 20002;}


    int iChMax = pDevice->GetProduct()->iOutChNum;

    for (int iCh = 0; iCh < iChMax; iCh++)
    {
        pDevice->SetAoRagne(iCh, AoRange);
    }
    return 0;
}

long WINAPI AioGetAoRange(short Id, short AoChannel, short * AoRange)
{
    if(!g_pDummyContec->IsInit()) {return 10002;}
    DummyDivice* pDevice = g_pDummyContec->GetDevice(Id);
    if (pDevice == NULL) {return 10001;}
    if (pDevice->IsResumed()){return 7;}

    if (pDevice->GetProduct()->iOutChNum == 0) {return 20001;}
    if (pDevice->GetProduct()->iOutChNum < AoChannel) {return 13000;}
    if (pDevice->IsAoExec()){return 20002;}

   *AoRange = pDevice->GetAoRagne(AoChannel);

    return 0;
}



long WINAPI AioSetAoTransferMode(short Id, short AoTransferMode){return 0;}
long WINAPI AioGetAoTransferMode(short Id, short *AoTransferMode){return 0;}
long WINAPI AioSetAoDeviceBufferMode(short Id, short AoDeviceBufferMode){return 0;}
long WINAPI AioGetAoDeviceBufferMode(short Id, short *AoDeviceBufferMode){return 0;}
long WINAPI AioSetAoMemorySize(short Id, long AoMemorySize){return 0;}
long WINAPI AioGetAoMemorySize(short Id, long *AoMemorySize){return 0;}
long WINAPI AioSetAoTransferData(short Id, long DataNumber, long *Buffer){return 0;}
long WINAPI AioGetAoSamplingDataSize(short Id, short *DataSize){return 0;}
long WINAPI AioSetAoMemoryType(short Id, short AoMemoryType){return 0;}
long WINAPI AioGetAoMemoryType(short Id, short * AoMemoryType){return 0;}
long WINAPI AioSetAoRepeatTimes(short Id, long AoRepeatTimes){return 0;}
long WINAPI AioGetAoRepeatTimes(short Id, long * AoRepeatTimes){return 0;}
long WINAPI AioSetAoClockType(short Id, short AoClockType){return 0;}
long WINAPI AioGetAoClockType(short Id, short * AoClockType){return 0;}
long WINAPI AioSetAoSamplingClock(short Id, float AoSamplingClock){return 0;}
long WINAPI AioGetAoSamplingClock(short Id, float * AoSamplingClock){return 0;}
long WINAPI AioSetAoClockEdge(short Id, short AoClockEdge){return 0;}
long WINAPI AioGetAoClockEdge(short Id, short * AoClockEdge){return 0;}
long WINAPI AioSetAoSamplingData(short Id, long AoSamplingTimes, long * AoData){return 0;}
long WINAPI AioSetAoSamplingDataEx(short Id, long AoSamplingTimes, float * AoData){return 0;}
long WINAPI AioGetAoSamplingTimes(short Id, long * AoSamplingTimes){return 0;}
long WINAPI AioSetAoStartTrigger(short Id, short AoStartTrigger){return 0;}
long WINAPI AioGetAoStartTrigger(short Id, short * AoStartTrigger){return 0;}
long WINAPI AioSetAoStopTrigger(short Id, short AoStopTrigger){return 0;}
long WINAPI AioGetAoStopTrigger(short Id, short * AoStopTrigger){return 0;}
long WINAPI AioSetAoEvent(short Id, HWND hWnd, long AoEvent){return 0;}
long WINAPI AioGetAoEvent(short Id, HWND * hWnd, long * AoEvent){return 0;}
long WINAPI AioSetAoCallBackProc(short Id,
long (_stdcall *pProc)(short Id, short AiEvent, WPARAM wParam, LPARAM lParam, void *Param), long AoEvent, void *Param){return 0;}
long WINAPI AioSetAoEventSamplingTimes(short Id, long AoSamplingTimes){return 0;}
long WINAPI AioGetAoEventSamplingTimes(short Id, long * AoSamplingTimes){return 0;}
long WINAPI AioSetAoEventTransferTimes(short Id, long AoTransferTimes){return 0;}
long WINAPI AioGetAoEventTransferTimes(short Id, long *AoTransferTimes){return 0;}


long WINAPI AioStartAo(short Id)
{
    if(!g_pDummyContec->IsInit()) {return 10002;}
    DummyDivice* pDevice = g_pDummyContec->GetDevice(Id);
    if (pDevice == NULL) {return 10001;}
    if (pDevice->IsResumed()){return 7;}

    pDevice->StartAo();
    return 0;
}

long WINAPI AioStopAo(short Id)
{
    if(!g_pDummyContec->IsInit()) {return 10002;}
    DummyDivice* pDevice = g_pDummyContec->GetDevice(Id);
    if (pDevice == NULL) {return 10001;}
    if (pDevice->IsResumed()){return 7;}

    pDevice->StopAo();
    return 0;
}


long WINAPI AioEnableAo(short Id, short AoChannel){return 0;}
long WINAPI AioDisableAo(short Id, short AoChannel){return 0;}
long WINAPI AioGetAoStatus(short Id, long * AoStatus){return 0;}

long WINAPI AioGetAoSamplingCount(short Id, long * AoSamplingCount){return 0;}
long WINAPI AioGetAoTransferCount(short Id, long *AoTransferCount){return 0;}
long WINAPI AioGetAoTransferLap(short Id, long *Lap){return 0;}
long WINAPI AioGetAoRepeatCount(short Id, long * AoRepeatCount){return 0;}
long WINAPI AioResetAoStatus(short Id){return 0;}
long WINAPI AioResetAoMemory(short Id){return 0;}

//デジタル入出力関数
long WINAPI AioSetDiFilter(short Id, short Bit, float Value){return 0;}
long WINAPI AioGetDiFilter(short Id, short Bit, float *Value){return 0;}

long WINAPI AioInputDiBit(short Id, short DiBit, short * DiData)
{
    if(!g_pDummyContec->IsInit()) {return 10002;}
    DummyDivice* pDevice = g_pDummyContec->GetDevice(Id);
    if (pDevice == NULL) {return 10001;}
    if (pDevice->IsResumed()){return 7;}
    if (DiBit < 0){return 14000;}
    if (DiBit >= pDevice->GetProduct()->iDiIn) {return 14000;}
    if (DiData == NULL) {return 14001;}
    if (pDevice->GetProduct()->iDiIn == 0) {return 20001;}

    *DiData = pDevice->GetDi(DiBit);
    return 0;
}

long WINAPI AioOutputDoBit(short Id, short DoBit, short DoData)
{
    if(!g_pDummyContec->IsInit()) {return 10002;}
    DummyDivice* pDevice = g_pDummyContec->GetDevice(Id);
    if (pDevice == NULL) {return 10001;}
    if (pDevice->IsResumed()){return 7;}
    if (DoBit < 0){return 14020;}
    if (DoBit >= pDevice->GetProduct()->iDiOut) {return 14020;}
    if (pDevice->GetProduct()->iDiOut == 0) {return 20001;}


    pDevice->SetDo(DoBit, (DoData?true:false));
    return 0;
}

long WINAPI AioInputDiByte(short Id, short DiPort, short * DiData)
{
    if(!g_pDummyContec->IsInit()) {return 10002;}
    DummyDivice* pDevice = g_pDummyContec->GetDevice(Id);
    if (pDevice == NULL) {return 10001;}
    if (pDevice->IsResumed()){return 7;}
    if (DiPort != 0){return 14010;}
    if (pDevice->GetProduct()->iDiIn == 0) {return 20001;}

    pDevice->GetDibyte(DiData);
    return 0;
}

long WINAPI AioOutputDoByte(short Id, short DoPort, short DoData)
{
    DummyDivice* pDevice = g_pDummyContec->GetDevice(Id);
    if (pDevice == NULL) {return 10001;}
    if (pDevice->IsResumed()){return 7;}
    if (DoPort != 0){return 14020;}
    if (pDevice->GetProduct()->iDiOut == 0) {return 20001;}
    pDevice->SetDobyte(DoData);

    return 0;
}

//カウンタ関数
long WINAPI AioGetCntMaxChannels(short Id, short * CntMaxChannels)
{
    DummyDivice* pDevice = g_pDummyContec->GetDevice(Id);
    if (pDevice == NULL) {return 10001;}
    if (pDevice->IsResumed()){return 7;}
    if (CntMaxChannels == 0){return 14020;}

    *CntMaxChannels = static_cast<short>(pDevice->GetProduct()->iCntNum);
    return 0;
}


long WINAPI AioSetCntComparisonMode(short Id, short CntChannel, short CntMode){return 0;}
long WINAPI AioGetCntComparisonMode(short Id, short CntChannel, short *CntMode){return 0;}

long WINAPI AioSetCntPresetReg(short Id, short CntChannel, long PresetNumber, long *PresetData, short Flag){return 0;}
long WINAPI AioSetCntComparisonReg(short Id, short CntChannel, long ComparisonNumber, long *ComparisonData, short Flag){return 0;}
long WINAPI AioSetCntInputSignal(short Id, short CntChannel, short CntInputSignal){return 0;}
long WINAPI AioGetCntInputSignal(short Id, short CntChannel, short *CntInputSignal){return 0;}
long WINAPI AioSetCntEvent(short Id, short CntChannel, HWND hWnd, long CntEvent){return 0;}
long WINAPI AioGetCntEvent(short Id, short CntChannel, HWND *hWnd, long *CntEvent){return 0;}
long WINAPI AioSetCntCallBackProc(short Id, short CntChannel,
	long (_stdcall *pProc)(short Id, short CntEvent, WPARAM wParam, LPARAM lParam, void *Param), long CntEvent, void *Param){return 0;}
long WINAPI AioSetCntFilter(short Id, short CntChannel, short Signal, float Value){return 0;}
long WINAPI AioGetCntFilter(short Id, short CntChannel, short Signal, float *Value){return 0;}

long WINAPI AioStartCnt(short Id, short CntChannel)
{
    DummyDivice* pDevice = g_pDummyContec->GetDevice(Id);
    if (pDevice == NULL) {return 10001;}
    if (pDevice->IsResumed()){return 7;}
    if (CntChannel < 0){return 15140;}
    if (CntChannel >= pDevice->GetProduct()->iCntNum){return 15140;}
    pDevice->StartCount(CntChannel);
    return 0;
}


long WINAPI AioStopCnt(short Id, short CntChannel)
{
    DummyDivice* pDevice = g_pDummyContec->GetDevice(Id);
    if (pDevice == NULL) {return 10001;}
    if (pDevice->IsResumed()){return 7;}
    if (CntChannel < 0){return 15160;}
    if (CntChannel >= pDevice->GetProduct()->iCntNum){return 15160;}
    pDevice->StopCount(CntChannel);
    return 0;
}

//カウンタのプリセット（初期値）を指定します。
long WINAPI AioPresetCnt(short Id, short CntChannel, long PresetData)
{
    DummyDivice* pDevice = g_pDummyContec->GetDevice(Id);
    if (pDevice == NULL) {return 10001;}
    if (pDevice->IsResumed()){return 7;}
    if (CntChannel < 0){return 15180;}
    if (CntChannel >= pDevice->GetProduct()->iCntNum){return 15180;}
    pDevice->SetCount(CntChannel, PresetData);
    return 0;
}


long WINAPI AioGetCntStatus(short Id, short CntChannel, long *CntStatus){return 0;}

long WINAPI AioGetCntCount(short Id, short CntChannel, long *Count)
{
    DummyDivice* pDevice = g_pDummyContec->GetDevice(Id);
    if (pDevice == NULL) {return 10001;}
    if (pDevice->IsResumed()){return 7;}
    if (CntChannel < 0){return 15180;}
    if (CntChannel >= pDevice->GetProduct()->iCntNum){return 15180;}
    *Count = pDevice->GetCount(CntChannel);
    return 0;
}

long WINAPI AioResetCntStatus(short Id, short CntChannel, long CntStatus)
{
    return 0;
}

//タイマ関数
long WINAPI AioSetTmEvent(short Id, short TimerId, HWND hWnd, long TmEvent){return 0;}
long WINAPI AioGetTmEvent(short Id, short TimerId, HWND * hWnd, long * TmEvent){return 0;}
long WINAPI AioSetTmCallBackProc(short Id, short TimerId,
	long (_stdcall *pProc)(short Id, short TmEvent, WPARAM wParam, LPARAM lParam, void *Param), long TmEvent, void *Param){return 0;}
long WINAPI AioStartTmTimer(short Id, short TimerId, float Interval){return 0;}
long WINAPI AioStopTmTimer(short Id, short TimerId){return 0;}
long WINAPI AioStartTmCount(short Id, short TimerId){return 0;}
long WINAPI AioStopTmCount(short Id, short TimerId){return 0;}
long WINAPI AioLapTmCount(short Id, short TimerId, long *Lap){return 0;}
long WINAPI AioResetTmCount(short Id, short TimerId){return 0;}
long WINAPI AioTmWait(short Id, short TimerId, long Wait){return 0;}

//イベントコントローラ
long WINAPI AioSetEcuSignal(short Id, short Destination, short Source){return 0;}
long WINAPI AioGetEcuSignal(short Id, short Destination, short *Source){return 0;}













long WINAPI AioGetErrorString(long ErrorCode, char * ErrorString)
{
    if (ErrorString == NULL)
    {
        return 10180;
    }

    switch(ErrorCode)
    {
        case 0: { strcpy( ErrorString,  "正常終了"); break;}
        case 1: { strcpy( ErrorString,  "リソースの取得に失敗しました"); break;}
        case 2: { strcpy( ErrorString,  "割り込みルーチンの登録に失敗しました"); break;}
        case 3: { strcpy( ErrorString,  "メモリの割り当てに失敗しました"); break;}
        case 4: { strcpy( ErrorString,  "レジストリのアクセスに失敗しました"); break;}
        case 5: { strcpy( ErrorString,  "バスマスタ登録時に設定するアドレスの値が異常です"); break;}
        case 6: { strcpy( ErrorString,  "アダプタオブジェクトの生成に失敗しました"); break;}
        case 10000: { strcpy( ErrorString,  "デバイスマネージャに登録されていないデバイス名が指定されました"); break;}
        case 10001: { strcpy( ErrorString,  "無効なIDが指定されました"); break;}
        case 10002: { strcpy( ErrorString,  "ドライバを呼び出せません"); break;}
        case 10003: { strcpy( ErrorString,  "ファイルの作成に失敗しました"); break;}
        case 10004: { strcpy( ErrorString,  "ファイルのクローズに失敗しました"); break;}
        case 10005: { strcpy( ErrorString,  "スレッドの作成に失敗しました"); break;}
        case 10006: { strcpy( ErrorString,  "利用可能なデバイスが見つかりません"); break;}
        case 10007: { strcpy( ErrorString,  "DeviceNameのポインタがNULLです"); break;}
        case 10008: { strcpy( ErrorString,  "DeviceのポインタがNULLです"); break;}
        case 10009: { strcpy( ErrorString,  "利用可能なデバイスが見つかりません"); break;}
        case 10010: { strcpy( ErrorString,  "DeviceNameのポインタがNULLです"); break;}
        case 10011: { strcpy( ErrorString,  "DeviceのポインタがNULLです"); break;}
        case 10012: { strcpy( ErrorString,  "BoardIdのポインタがNULLです"); break;}
        case 10013: { strcpy( ErrorString,  "デバイスの種類を判別できません"); break;}
        case 10014: { strcpy( ErrorString,  "DeviceのポインタがNULLです"); break;}
        case 10015: { strcpy( ErrorString,  "DeviceTypeのポインタがNULLです"); break;}
        case 10100: { strcpy( ErrorString,  "DeviceNameに文字列が格納されていません"); break;}
        case 10101: { strcpy( ErrorString,  "IdのポインタがNULLです"); break;}
        case 10180: { strcpy( ErrorString,  "ErrorStringのポインタがNULLです"); break;}
        case 10220: { strcpy( ErrorString,  "Offsetの値がマイナスです"); break;}
        case 10221: { strcpy( ErrorString,  "DataのポインタがNULLです"); break;}
        case 10222: { strcpy( ErrorString,  "Flagの値が指定範囲外です"); break;}
        case 10240: { strcpy( ErrorString,  "Offsetの値がマイナスです"); break;}
        case 10241: { strcpy( ErrorString,  "DataのポインタがNULLです"); break;}
        case 10242: { strcpy( ErrorString,  "Flagの値が指定範囲外です"); break;}
        case 10260: { strcpy( ErrorString,  "Offsetの値がマイナスです"); break;}
        case 10261: { strcpy( ErrorString,  "DataのポインタがNULLです"); break;}
        case 10262: { strcpy( ErrorString,  "Flagの値が指定範囲外です"); break;}
        case 10280: { strcpy( ErrorString,  "Offsetの値がマイナスです"); break;}
        case 10281: { strcpy( ErrorString,  "Flagの値が指定範囲外です"); break;}
        case 10300: { strcpy( ErrorString,  "Offsetの値がマイナスです"); break;}
        case 10301: { strcpy( ErrorString,  "Flagの値が指定範囲外です"); break;}
        case 10320: { strcpy( ErrorString,  "Offsetの値がマイナスです"); break;}
        case 10321: { strcpy( ErrorString,  "Flagの値が指定範囲外です"); break;}
        case 10340: { strcpy( ErrorString,  "Signalの値が関数の指定範囲外です"); break;}
        case 10341: { strcpy( ErrorString,  "Valueの値が関数の指定範囲外です"); break;}
        case 10350: { strcpy( ErrorString,  "Signalの値が関数の指定範囲外です"); break;}
        case 10351: { strcpy( ErrorString,  "ValueのポインタがNULLです"); break;}
        case 11000: { strcpy( ErrorString,  "AiInputMethodの値が関数の指定範囲外です"); break;}
        case 11010: { strcpy( ErrorString,  "AiInputMethodのポインタがNULLです"); break;}
        case 11020: { strcpy( ErrorString,  "AiChannelsの値が関数の指定範囲外です"); break;}
        case 11030: { strcpy( ErrorString,  "AiChannelsのポインタがNULLです"); break;}
        case 11040: { strcpy( ErrorString,  "AiChannelの値が関数の指定範囲外です"); break;}
        case 11041: { strcpy( ErrorString,  "AiRangeの値が関数の指定範囲外です"); break;}
        case 11050: { strcpy( ErrorString,  "AiChannelsの値が関数の指定範囲外です"); break;}
        case 11051: { strcpy( ErrorString,  "AiRangeのポインタがNULLです"); break;}
        case 11060: { strcpy( ErrorString,  "AiRangeの値が関数の指定範囲外です"); break;}
        case 11080: { strcpy( ErrorString,  "AiMemoryTypeの値が関数の指定範囲外です"); break;}
        case 11090: { strcpy( ErrorString,  "AiMemoryTypeのポインタがNULLです"); break;}
        case 11100: { strcpy( ErrorString,  "AiRepeatTimesの値が関数の指定範囲外です"); break;}
        case 11110: { strcpy( ErrorString,  "AiRepeatTimesのポインタがNULLです"); break;}
        case 11120: { strcpy( ErrorString,  "AiClockTypeの値が関数の指定範囲外です"); break;}
        case 11130: { strcpy( ErrorString,  "AiClockTypeのポインタがNULLです"); break;}
        case 11140: { strcpy( ErrorString,  "AiSamplingClockの値が関数の指定範囲外です"); break;}
        case 11150: { strcpy( ErrorString,  "AiSamplingClockのポインタがNULLです"); break;}
        case 11160: { strcpy( ErrorString,  "AiStartTriggerの値が関数の指定範囲外です"); break;}
        case 11170: { strcpy( ErrorString,  "AiStartTriggerのポインタがNULLです"); break;}
        case 11180: { strcpy( ErrorString,  "AiChannelの値が関数の指定範囲外です"); break;}
        case 11181: { strcpy( ErrorString,  "AiDirectionの値が関数の指定範囲外です"); break;}
        case 11190: { strcpy( ErrorString,  "AiChannelの値が関数の指定範囲外です"); break;}
        case 11191: { strcpy( ErrorString,  "AiStartLevelのポインタがNULLです"); break;}
        case 11192: { strcpy( ErrorString,  "AiDirectionのポインタがNULLです"); break;}
        case 11260: { strcpy( ErrorString,  "AiStopTriggerの値が関数の指定範囲外です"); break;}
        case 11270: { strcpy( ErrorString,  "AiStopTriggerのポインタがNULLです"); break;}
        case 11280: { strcpy( ErrorString,  "AiStopTimesの値が関数の指定範囲外です"); break;}
        case 11290: { strcpy( ErrorString,  "AiStopTimesのポインタがNULLです"); break;}
        case 11300: { strcpy( ErrorString,  "AiChannelの値が関数の指定範囲外です"); break;}
        case 11301: { strcpy( ErrorString,  "AiDirectionの値が関数の指定範囲外です"); break;}
        case 11310: { strcpy( ErrorString,  "AiChannelの値が関数の指定範囲外です"); break;}
        case 11311: { strcpy( ErrorString,  "AiStopLevelのポインタがNULLです"); break;}
        case 11312: { strcpy( ErrorString,  "AiDirectionのポインタがNULLです"); break;}
        case 11360: { strcpy( ErrorString,  "AiStopDelayTimesの値が関数の指定範囲外です"); break;}
        case 11370: { strcpy( ErrorString,  "AiStopDelayTimesのポインタがNULLです"); break;}
        case 11390: { strcpy( ErrorString,  "AiEventのポインタのポインタがNULLです"); break;}
        case 11400: { strcpy( ErrorString,  "AiSamplingTimesの値が関数の指定範囲外です"); break;}
        case 11410: { strcpy( ErrorString,  "AiSamplingTimesのポインタがNULLです"); break;}
        case 11420: { strcpy( ErrorString,  "AiChannelの値が関数の指定範囲外です"); break;}
        case 11421: { strcpy( ErrorString,  "AiDataのポインタがNULLです"); break;}
        case 11440: { strcpy( ErrorString,  "AiChannelsの値が関数の指定範囲外です"); break;}
        case 11441: { strcpy( ErrorString,  "AiDataのポインタがNULLです"); break;}
        case 11460: { strcpy( ErrorString,  "スレッドを実行できません"); break;}
        case 11500: { strcpy( ErrorString,  "AiStatusのポインタがNULLです"); break;}
        case 11520: { strcpy( ErrorString,  "AiSamplingCountのポインタがNULLです"); break;}
        case 11540: { strcpy( ErrorString,  "AiRepeatCountのポインタがNULLです"); break;}
        case 11560: { strcpy( ErrorString,  "AiStopTriggerCountのポインタがNULLです"); break;}
        case 11580: { strcpy( ErrorString,  "AiSamplingTimesのポインタがNULLです"); break;}
        case 11581: { strcpy( ErrorString,  "AiDataのポインタがNULLです"); break;}
        case 11720: { strcpy( ErrorString,  "AiMaxChannelsのポインタがNULLです"); break;}
        case 11740: { strcpy( ErrorString,  "AiResolutionのポインタがNULLです"); break;}
        case 11760: { strcpy( ErrorString,  "AiChannelの値が使用しているデバイスの範囲外です"); break;}
        case 11761: { strcpy( ErrorString,  "AiSequenceの値が使用しているデバイスの範囲外です"); break;}
        case 11770: { strcpy( ErrorString,  "AiChannelの値が使用しているデバイスの範囲外です"); break;}
        case 11771: { strcpy( ErrorString,  "AiSequenceのポインタがNULLです"); break;}
        case 11820: { strcpy( ErrorString,  "AiMemorySizeの値が指定範囲外です"); break;}
        case 11830: { strcpy( ErrorString,  "AiMemorySizeのポインタがNULLです"); break;}
        case 11840: { strcpy( ErrorString,  "AiChannelの値が関数の指定範囲外です"); break;}
        case 11841: { strcpy( ErrorString,  "AiDataのポインタがNULLです"); break;}
        case 11860: { strcpy( ErrorString,  "AiChannelsの値が関数の指定範囲外です"); break;}
        case 11861: { strcpy( ErrorString,  "AiDataのポインタがNULLです"); break;}
        case 11880: { strcpy( ErrorString,  "AiSamplingTimesがNULLです"); break;}
        case 11881: { strcpy( ErrorString,  "AiDataがNULLです"); break;}
        case 11920: { strcpy( ErrorString,  "AiChannelの値が関数の指定範囲外です"); break;}
        case 11921: { strcpy( ErrorString,  "AiDirectionの値が関数の指定範囲外です"); break;}
        case 11930: { strcpy( ErrorString,  "AiChannelの値が関数の指定範囲外です"); break;}
        case 11931: { strcpy( ErrorString,  "AiStartLevelのポインタがNULLです"); break;}
        case 11932: { strcpy( ErrorString,  "AiDirectionのポインタがNULLです"); break;}
        case 11940: { strcpy( ErrorString,  "AiChannelの値が関数の指定範囲外です"); break;}
        case 11941: { strcpy( ErrorString,  "AiDirectionの値が関数の指定範囲外です"); break;}
        case 11950: { strcpy( ErrorString,  "AiChannelの値が関数の指定範囲外です"); break;}
        case 11951: { strcpy( ErrorString,  "AiStopLevelのポインタがNULLです"); break;}
        case 11952: { strcpy( ErrorString,  "AiDirectionのポインタがNULLです"); break;}
        case 11960: { strcpy( ErrorString,  "AiTransferModeの値が関数の指定範囲外です"); break;}
        case 11970: { strcpy( ErrorString,  "AiTransferModeのポインタがNULLです"); break;}
        case 11980: { strcpy( ErrorString,  "DataNumberの値が関数の指定範囲外です"); break;}
        case 11981: { strcpy( ErrorString,  "BufferのポインタがNULLです"); break;}
        case 11982: { strcpy( ErrorString,  "バスマスタ転送用のバッファ確保に失敗しました"); break;}
        case 12003: { strcpy( ErrorString,  "AiChannelの値が使用しているデバイスの範囲外です"); break;}
        case 12020: { strcpy( ErrorString,  "DataSizeのポインタがNULLです"); break;}
        case 12040: { strcpy( ErrorString,  "AiChannelの値が関数の指定範囲外です"); break;}
        case 12041: { strcpy( ErrorString,  "StateTimesの値が関数の指定範囲外です"); break;}
        case 12050: { strcpy( ErrorString,  "AiChannelの値が指定範囲外です"); break;}
        case 12051: { strcpy( ErrorString,  "Level1のポインタがNULLです"); break;}
        case 12052: { strcpy( ErrorString,  "Level2のポインタがNULLです"); break;}
        case 12053: { strcpy( ErrorString,  "StateTimesのポインタがNULLです"); break;}
        case 12060: { strcpy( ErrorString,  "AiChannelの値が関数の指定範囲外です"); break;}
        case 12061: { strcpy( ErrorString,  "StateTimesの値が関数の指定範囲外です"); break;}
        case 12070: { strcpy( ErrorString,  "AiChannelの値が指定範囲外です"); break;}
        case 12071: { strcpy( ErrorString,  "Level1のポインタがNULLです"); break;}
        case 12072: { strcpy( ErrorString,  "Level2のポインタがNULLです"); break;}
        case 12073: { strcpy( ErrorString,  "StateTimesのポインタがNULLです"); break;}
        case 12080: { strcpy( ErrorString,  "AiChannelの値が関数の指定範囲外です"); break;}
        case 12081: { strcpy( ErrorString,  "StateTimesの値が関数の指定範囲外です"); break;}
        case 12090: { strcpy( ErrorString,  "AiChannelの値が関数の指定範囲外です"); break;}
        case 12091: { strcpy( ErrorString,  "Level1のポインタがNULLです"); break;}
        case 12092: { strcpy( ErrorString,  "Level2のポインタがNULLです"); break;}
        case 12093: { strcpy( ErrorString,  "StateTimesのポインタがNULLです"); break;}
        case 12100: { strcpy( ErrorString,  "AiChannelの値が関数の指定範囲外です"); break;}
        case 12101: { strcpy( ErrorString,  "StateTimesの値が関数の指定範囲外です"); break;}
        case 12110: { strcpy( ErrorString,  "AiChannelの値が関数の指定範囲外です"); break;}
        case 12111: { strcpy( ErrorString,  "Level1のポインタがNULLです"); break;}
        case 12112: { strcpy( ErrorString,  "Level2のポインタがNULLです"); break;}
        case 12113: { strcpy( ErrorString,  "StateTimesのポインタがNULLです"); break;}
        case 12120: { strcpy( ErrorString,  "AiTransferTimesの値が関数の指定範囲外です"); break;}
        case 12130: { strcpy( ErrorString,  "AiSamplingTimesのポインタがNULLです"); break;}
        case 12140: { strcpy( ErrorString,  "AiTransferCountのポインタがNULLです"); break;}
        case 12160: { strcpy( ErrorString,  "LapのポインタがNULLです"); break;}
        case 12180: { strcpy( ErrorString,  "CountのポインタがNULLです"); break;}
        case 12200: { strcpy( ErrorString,  "Rangeの値が関数の指定範囲外です"); break;}
        case 12201: { strcpy( ErrorString,  "Typeの値が関数の指定範囲外です"); break;}
        case 12202: { strcpy( ErrorString,  "Dataの値が関数の指定範囲外です"); break;}
        case 12210: { strcpy( ErrorString,  "Rangeの値が関数の指定範囲外です"); break;}
        case 12211: { strcpy( ErrorString,  "Typeの値が関数の指定範囲外です"); break;}
        case 12212: { strcpy( ErrorString,  "DataのポインタがNULLです"); break;}
        case 12213: { strcpy( ErrorString,  "AiChannelの値が使用しているデバイスの範囲外です"); break;}
        case 12220: { strcpy( ErrorString,  "Rangeの値が関数の指定範囲外です"); break;}
        case 12221: { strcpy( ErrorString,  "Typeの値が関数の指定範囲外です"); break;}
        case 12222: { strcpy( ErrorString,  "DataのポインタがNULLです"); break;}
        case 12223: { strcpy( ErrorString,  "AiChannelの値が使用しているデバイスの範囲外です"); break;}
        case 12240: { strcpy( ErrorString,  "AiScanClockの値が関数の指定範囲外です"); break;}
        case 12250: { strcpy( ErrorString,  "AiScanClockのポインタがNULLです"); break;}
        case 12260: { strcpy( ErrorString,  "AiSamplingClockのポインタがNULLです"); break;}
        case 12310: { strcpy( ErrorString,  "AiClockEdgeの値が関数の指定範囲外です"); break;}
        case 12320: { strcpy( ErrorString,  "AiClockEdgeのポインタがNULLです"); break;}
        case 13000: { strcpy( ErrorString,  "AoChannelの値が関数の指定範囲外です"); break;}
        case 13020: { strcpy( ErrorString,  "AoChannelsの値が関数の指定範囲外です"); break;}
        case 13021: { strcpy( ErrorString,  "AoDataのポインタがNULLです"); break;}
        case 13040: { strcpy( ErrorString,  "AoResolutionのポインタがNULLです"); break;}
        case 13060: { strcpy( ErrorString,  "AoChannelsの値が関数の指定範囲外です"); break;}
        case 13070: { strcpy( ErrorString,  "AoChannelsのポインタがNULLです"); break;}
        case 13080: { strcpy( ErrorString,  "AoMaxChannelsのポインタがNULLです"); break;}
        case 13100: { strcpy( ErrorString,  "AoChannelsの値が関数の指定範囲外です"); break;}
        case 13101: { strcpy( ErrorString,  "AoRangeの値が関数の指定範囲外です"); break;}
        case 13110: { strcpy( ErrorString,  "AoChannelの値が関数の指定範囲外です"); break;}
        case 13111: { strcpy( ErrorString,  "AoRangeがNULLです"); break;}
        case 13120: { strcpy( ErrorString,  "AoRangeの値が関数の指定範囲外です"); break;}
        case 13140: { strcpy( ErrorString,  "AoMemoryTypeの値が関数の指定範囲外です"); break;}
        case 13150: { strcpy( ErrorString,  "AoMemoryTypeのポインタがNULLです"); break;}
        case 13160: { strcpy( ErrorString,  "AoRepeatTimesの値が関数の指定範囲外です"); break;}
        case 13170: { strcpy( ErrorString,  "AoRepeatTimesのポインタがNULLです"); break;}
        case 13180: { strcpy( ErrorString,  "AoClockTypeの値が関数の指定範囲外です"); break;}
        case 13190: { strcpy( ErrorString,  "AoClockTypeのポインタがNULLです"); break;}
        case 13200: { strcpy( ErrorString,  "AoSamplingClockの値が関数の指定範囲外です"); break;}
        case 13210: { strcpy( ErrorString,  "AoSamplingClockのポインタがNULLです"); break;}
        case 13220: { strcpy( ErrorString,  "AoSamplingTimesの値が関数の指定範囲外です"); break;}
        case 13221: { strcpy( ErrorString,  "AoDataのポインタがNULLです"); break;}
        case 13230: { strcpy( ErrorString,  "AoSamplingTimesのポインタがNULLです"); break;}
        case 13240: { strcpy( ErrorString,  "AoStartTriggerの値が関数の指定範囲外です"); break;}
        case 13250: { strcpy( ErrorString,  "AoStartTriggerのポインタがNULLです"); break;}
        case 13260: { strcpy( ErrorString,  "AoStopTriggerの値が関数の指定範囲外です"); break;}
        case 13270: { strcpy( ErrorString,  "AoStopTriggerのポインタがNULLです"); break;}
        case 13290: { strcpy( ErrorString,  "AoEventのポインタがNULLです"); break;}
        case 13300: { strcpy( ErrorString,  "AiSamplingTimesの値が関数の指定範囲外です"); break;}
        case 13310: { strcpy( ErrorString,  "AoSamplingTimesのポインタがNULLです"); break;}
        case 13320: { strcpy( ErrorString,  "スレッドを実行できません"); break;}
        case 13360: { strcpy( ErrorString,  "AoChannelの値が関数の指定範囲外です"); break;}
        case 13370: { strcpy( ErrorString,  "AoChannelの値が関数の指定範囲外です"); break;}
        case 13380: { strcpy( ErrorString,  "AoStatusのポインタがNULLです"); break;}
        case 13400: { strcpy( ErrorString,  "AoSamplingCountのポインタがNULLです"); break;}
        case 13420: { strcpy( ErrorString,  "AoRepeatCountのポインタがNULLです"); break;}
        case 13480: { strcpy( ErrorString,  "AoMemorySizeの値が指定範囲外です"); break;}
        case 13490: { strcpy( ErrorString,  "AoMemorySizeのポインタがNULLです"); break;}
        case 13500: { strcpy( ErrorString,  "AoChannelの値が関数の指定範囲外です"); break;}
        case 13520: { strcpy( ErrorString,  "AoChannelsの値が関数の指定範囲外です"); break;}
        case 13521: { strcpy( ErrorString,  "AoDataのポインタがNULLです"); break;}
        case 13540: { strcpy( ErrorString,  "AoSamplingTimesの値が指定範囲外です"); break;}
        case 13541: { strcpy( ErrorString,  "AoDataのポインタがNULLです"); break;}
        case 13580: { strcpy( ErrorString,  "AoTransferModeの値が関数の指定範囲外です"); break;}
        case 13590: { strcpy( ErrorString,  "AoTransferModeのポインタがNULLです"); break;}
        case 13600: { strcpy( ErrorString,  "DataNumberの値が関数の指定範囲外です"); break;}
        case 13601: { strcpy( ErrorString,  "BufferのポインタがNULLです"); break;}
        case 13602: { strcpy( ErrorString,  "バスマスタ転送用のバッファ確保に失敗しました"); break;}
        case 13640: { strcpy( ErrorString,  "DataSizeのポインタがNULLです"); break;}
        case 13660: { strcpy( ErrorString,  "AoTransferTimesの値が関数の指定範囲外です"); break;}
        case 13670: { strcpy( ErrorString,  "AoSamplingTimesのポインタがNULLです"); break;}
        case 13680: { strcpy( ErrorString,  "AoTransferCountのポインタがNULLです"); break;}
        case 13700: { strcpy( ErrorString,  "LapのポインタがNULLです"); break;}
        case 13720: { strcpy( ErrorString,  "Rangeの値が関数の指定範囲外です"); break;}
        case 13721: { strcpy( ErrorString,  "Typeの値が関数の指定範囲外です"); break;}
        case 13722: { strcpy( ErrorString,  "Dataの値が関数の指定範囲外です"); break;}
        case 13723: { strcpy( ErrorString,  "AoChannelの値が使用しているデバイスの範囲外です"); break;}
        case 13730: { strcpy( ErrorString,  "Rangeの値が関数の指定範囲外です"); break;}
        case 13731: { strcpy( ErrorString,  "Typeの値が関数の指定範囲外です"); break;}
        case 13732: { strcpy( ErrorString,  "DataのポインタがNULLです"); break;}
        case 13733: { strcpy( ErrorString,  "AoChannelの値が使用しているデバイスの範囲外です"); break;}
        case 13740: { strcpy( ErrorString,  "Rangeの値が関数の指定範囲外です"); break;}
        case 13741: { strcpy( ErrorString,  "Typeの値が関数の指定範囲外です"); break;}
        case 13742: { strcpy( ErrorString,  "DataのポインタがNULLです"); break;}
        case 13743: { strcpy( ErrorString,  "AoChannelの値が使用しているデバイスの範囲外です"); break;}
        case 13760: { strcpy( ErrorString,  "AoSamplingClockのポインタがNULLです"); break;}
        case 13770: { strcpy( ErrorString,  "AoClockEdgeの値が関数の指定範囲外です"); break;}
        case 13780: { strcpy( ErrorString,  "AoClockEdgeのポインタがNULLです"); break;}
        case 14000: { strcpy( ErrorString,  "DiBitの値が指定範囲外です"); break;}
        case 14001: { strcpy( ErrorString,  "DiDataのポインタがNULLです"); break;}
        case 14010: { strcpy( ErrorString,  "DiPortの値が指定範囲外です"); break;}
        case 14011: { strcpy( ErrorString,  "DiDataのポインタがNULLです"); break;}
        case 14020: { strcpy( ErrorString,  "DoBitの値が指定範囲外です"); break;}
        case 14021: { strcpy( ErrorString,  "DoDataの値が指定範囲外です"); break;}
        case 14030: { strcpy( ErrorString,  "DoPortの値が指定範囲外です"); break;}
        case 14031: { strcpy( ErrorString,  "DoDataの値が指定範囲外です"); break;}
        case 14040: { strcpy( ErrorString,  "Bitの値が関数の指定範囲外です"); break;}
        case 14041: { strcpy( ErrorString,  "Valueの値が関数の指定範囲外です"); break;}
        case 14050: { strcpy( ErrorString,  "Bitの値が関数の指定範囲外です"); break;}
        case 14051: { strcpy( ErrorString,  "ValueのポインタがNULLです"); break;}
        case 15000: { strcpy( ErrorString,  "CntChannelの値が関数の指定範囲外です"); break;}
        case 15010: { strcpy( ErrorString,  "CntChannelの値が関数の指定範囲外です"); break;}
        case 15011: { strcpy( ErrorString,  "CntModeのポインタがNULLです"); break;}
        case 15020: { strcpy( ErrorString,  "CntChannelの値が関数の指定範囲外です"); break;}
        case 15021: { strcpy( ErrorString,  "PresetNumberの値が関数の指定範囲外です"); break;}
        case 15022: { strcpy( ErrorString,  "Flagの値が関数の指定範囲外です"); break;}
        case 15023: { strcpy( ErrorString,  "PresetDataのポインタがNULLです"); break;}
        case 15040: { strcpy( ErrorString,  "CntChannelの値が関数の指定範囲外です"); break;}
        case 15041: { strcpy( ErrorString,  "ComparisonNumberの値が関数の指定範囲外です"); break;}
        case 15042: { strcpy( ErrorString,  "Flagの値が関数の指定範囲外です"); break;}
        case 15043: { strcpy( ErrorString,  "PresetDataのポインタがNULLです"); break;}
        case 15060: { strcpy( ErrorString,  "CntChannelの値が関数の指定範囲外です"); break;}
        case 15061: { strcpy( ErrorString,  "CntClockTypeの値が関数の指定範囲外です"); break;}
        case 15070: { strcpy( ErrorString,  "CntChannelの値が関数の指定範囲外です"); break;}
        case 15071: { strcpy( ErrorString,  "CntClockTypeのポインタがNULLです"); break;}
        case 15080: { strcpy( ErrorString,  "CntChannelの値が関数の指定範囲外です"); break;}
        case 15090: { strcpy( ErrorString,  "CntChannelの値が関数の指定範囲外です"); break;}
        case 15091: { strcpy( ErrorString,  "AiEventのポインタがNULLです"); break;}
        case 15100: { strcpy( ErrorString,  "CntChannelの値が関数の指定範囲外です"); break;}
        case 15120: { strcpy( ErrorString,  "CntChannelの値が関数の指定範囲外です"); break;}
        case 15121: { strcpy( ErrorString,  "Signalの値が関数の指定範囲外です"); break;}
        case 15122: { strcpy( ErrorString,  "Valueの値が関数の指定範囲外です"); break;}
        case 15130: { strcpy( ErrorString,  "CntChannelの値が関数の指定範囲外です"); break;}
        case 15131: { strcpy( ErrorString,  "Signalの値が関数の指定範囲外です"); break;}
        case 15132: { strcpy( ErrorString,  "ValueのポインタがNULLです"); break;}
        case 15140: { strcpy( ErrorString,  "CntChannelの値が関数の指定範囲外です"); break;}
        case 15141: { strcpy( ErrorString,  "スレッドを実行できません"); break;}
        case 15160: { strcpy( ErrorString,  "CntChannelの値が関数の指定範囲外です"); break;}
        case 15180: { strcpy( ErrorString,  "CntChannelの値が関数の指定範囲外です"); break;}
        case 15200: { strcpy( ErrorString,  "CntChannelの値が関数の指定範囲外です"); break;}
        case 15201: { strcpy( ErrorString,  "CntStatusのポインタがNULLです"); break;}
        case 15220: { strcpy( ErrorString,  "CntChannelの値が関数の指定範囲外です"); break;}
        case 15221: { strcpy( ErrorString,  "CountのポインタがNULLです"); break;}
        case 15240: { strcpy( ErrorString,  "CntChannelの値が関数の指定範囲外です"); break;}
        case 15260: { strcpy( ErrorString,  "CntMaxChannelsのポインタがNULLです"); break;}
        case 16000: { strcpy( ErrorString,  "TimerIdの値が関数の指定範囲外です"); break;}
        case 16010: { strcpy( ErrorString,  "TimerIdの値が関数の指定範囲外です"); break;}
        case 16011: { strcpy( ErrorString,  "TmEventのポインタがNULLです"); break;}
        case 16020: { strcpy( ErrorString,  "TimerIdの値が関数の指定範囲外です"); break;}
        case 16040: { strcpy( ErrorString,  "TimerIdの値が関数の指定範囲外です"); break;}
        case 16041: { strcpy( ErrorString,  "Intervalの値が関数の指定範囲外です"); break;}
        case 16042: { strcpy( ErrorString,  "スレッドを実行できません"); break;}
        case 16060: { strcpy( ErrorString,  "TimerIdの値が関数の指定範囲外です"); break;}
        case 16080: { strcpy( ErrorString,  "TimerIdの値が関数の指定範囲外です"); break;}
        case 16100: { strcpy( ErrorString,  "TimerIdの値が関数の指定範囲外です"); break;}
        case 16120: { strcpy( ErrorString,  "TimerIdの値が関数の指定範囲外です"); break;}
        case 16121: { strcpy( ErrorString,  "LapのポインタがNULLです"); break;}
        case 16140: { strcpy( ErrorString,  "TimerIdの値が関数の指定範囲外です"); break;}
        case 16160: { strcpy( ErrorString,  "TimerIdの値が関数の指定範囲外です"); break;}
        case 16161: { strcpy( ErrorString,  "Waitの値が関数の指定範囲外です"); break;}
        case 17000: { strcpy( ErrorString,  "Destinationの値が関数の指定範囲外です"); break;}
        case 17001: { strcpy( ErrorString,  "Sourceの値が関数の指定範囲外です"); break;}
        case 17010: { strcpy( ErrorString,  "Destinationの値が関数の指定範囲外です"); break;}
        case 17011: { strcpy( ErrorString,  "SourceのポインタがNULLです"); break;}
        case 18000: { strcpy( ErrorString,  "モードが設定可能範囲外です"); break;}
        case 18100: { strcpy( ErrorString,  "データバッファアドレスが不正です"); break;}
        case 18200: { strcpy( ErrorString,  "ウィンドウハンドルが指定可能範囲外です"); break;}
        case 20000: { strcpy( ErrorString,  "メモリの確保に失敗しました"); break;}
        case 20001: { strcpy( ErrorString,  "このデバイスではこの関数は使用できません"); break;}
        case 20002: { strcpy( ErrorString,  "デバイスが動作中のため実行できません"); break;}
        case 20003: { strcpy( ErrorString,  "他のプロセスがデバイスを使用しているため、実行できません"); break;}
        case 20020: { strcpy( ErrorString,  "エンドポイントから受け取った最後のデータパケットにCRCエラーが存在しています"); break;}
        case 20021: { strcpy( ErrorString,  "エンドポイントから受け取った最後のデータパケットにビット詰め違反のエラーが存在しています"); break;}
        case 20022: { strcpy( ErrorString,  "エンドポイントから受け取った最後のデータパケットに期待した値にマッチしないデータトグルパケット識別子を含んでいます"); break;}
        case 20023: { strcpy( ErrorString,  "エンドポイントがSTALLパケット識別子を返しました"); break;}
        case 20024: { strcpy( ErrorString,  "デバイスがトークン(IN)に応答していないかハンドシェークをサポートしていません"); break;}
        case 20025: { strcpy( ErrorString,  "デバイスがトークン(IN)に応答していないかハンドシェークをサポートしていません"); break;}
        case 20026: { strcpy( ErrorString,  "受け取ったパケット識別子が無効か未定義です"); break;}
        case 20027: { strcpy( ErrorString,  "エンドポイントから戻されたデータの量が許容されたデータパケットの最大長あるいはバッファの残量を超えています"); break;}
        case 20028: { strcpy( ErrorString,  "エンドポイントから戻されたデータの量が期待したデータのサイズに足りません"); break;}
        case 20029: { strcpy( ErrorString,  "IN転送において指定されたバッファが小さすぎてデバイスから受け取ったデータをすべて格納することができません"); break;}
        case 20030: { strcpy( ErrorString,  "OUT転送において指定されたバッファにはデバイスに送出するための十分なデータが格納されていません"); break;}
        case 20031: { strcpy( ErrorString,  "エンドポイントがSTALL状態のため転送に失敗しました"); break;}
        case 20032: { strcpy( ErrorString,  "情報取得でデバイスの情報が見つかりませんでした"); break;}
        case 20033: { strcpy( ErrorString,  "ハードウェアへのアクセスが拒否されました"); break;}
        case 20034: { strcpy( ErrorString,  "指定したハンドルは無効です"); break;}
        case 20035: { strcpy( ErrorString,  "ファームウェアが旧バージョンです、ファームウェアのバージョンアップが必要です"); break;}
        case 20036: { strcpy( ErrorString,  "ホストドライバが旧バージョンです、ホストドライバのバージョンアップが必要です"); break;}
        case 20037: { strcpy( ErrorString,  "USB転送でエラーが発生しました"); break;}
        case 20100: { strcpy( ErrorString,  "プロセスの数が上限に達しました"); break;}
        case 20160: { strcpy( ErrorString,  "デバイスのリセットに失敗しました"); break;}
        case 20161: { strcpy( ErrorString,  "バスマスタのリセットに失敗しました"); break;}
        case 20340: { strcpy( ErrorString,  "Signalの値が使用しているデバイスの指定範囲外です"); break;}
        case 20341: { strcpy( ErrorString,  "Valueの値が使用しているデバイスの指定範囲外です"); break;}
        case 20350: { strcpy( ErrorString,  "Signalの値が使用しているデバイスの指定範囲外です"); break;}
        case 21000: { strcpy( ErrorString,  "使用しているデバイスではアナログ入力方式の設定はできません"); break;}
        case 21001: { strcpy( ErrorString,  "AiInputMethodの値がJPで設定された値と異なります"); break;}
        case 21020: { strcpy( ErrorString,  "デバイスの最大チャネル数を超えているため、最大チャネル数で設定しました"); break;}
        case 21040: { strcpy( ErrorString,  "AiChannelの値が使用しているデバイスの指定範囲外です"); break;}
        case 21041: { strcpy( ErrorString,  "AiRangeの値が使用しているデバイスの指定範囲外です"); break;}
        case 21042: { strcpy( ErrorString,  "デバイスのJPをユニポーラに設定する必要があります"); break;}
        case 21043: { strcpy( ErrorString,  "デバイスのJPをバイポーラに設定する必要があります"); break;}
        case 21044: { strcpy( ErrorString,  "レンジの設定に失敗しました"); break;}
        case 21060: { strcpy( ErrorString,  "AiRangeの値が使用しているデバイスの指定範囲外です"); break;}
        case 21061: { strcpy( ErrorString,  "デバイスのJPをユニポーラに設定する必要があります"); break;}
        case 21062: { strcpy( ErrorString,  "デバイスのJPをバイポーラに設定する必要があります"); break;}
        case 21063: { strcpy( ErrorString,  "レンジの設定に失敗しました"); break;}
        case 21080: { strcpy( ErrorString,  "AiMemoryTypeの値が使用しているデバイスの指定範囲外です"); break;}
        case 21120: { strcpy( ErrorString,  "AiClockTypeの値が使用しているデバイスの指定範囲外です"); break;}
        case 21140: { strcpy( ErrorString,  "AiSamplingClockの値が使用しているデバイスの指定範囲外です"); break;}
        case 21160: { strcpy( ErrorString,  "AiStartTriggerの値が使用しているデバイスの指定範囲外です"); break;}
        case 21180: { strcpy( ErrorString,  "AiChannelの値が使用しているデバイスの範囲外です"); break;}
        case 21181: { strcpy( ErrorString,  "12ビットデバイスに設定するデータは0〜4095の範囲内です"); break;}
        case 21182: { strcpy( ErrorString,  "16ビットデバイスに設定するデータは0〜65535の範囲内です"); break;}
        case 21183: { strcpy( ErrorString,  "分解能の取得ができません"); break;}
        case 21184: { strcpy( ErrorString,  "AiDirectionの値が使用しているデバイスの範囲外です"); break;}
        case 21260: { strcpy( ErrorString,  "AiStopTrgの値が使用しているデバイスの指定範囲外です"); break;}
        case 21300: { strcpy( ErrorString,  "AiChannelの値が使用しているデバイスの範囲外です"); break;}
        case 21301: { strcpy( ErrorString,  "12ビットデバイスに設定するデータは0〜4095の範囲内です"); break;}
        case 21302: { strcpy( ErrorString,  "16ビットデバイスに設定するデータは0〜65535の範囲内です"); break;}
        case 21303: { strcpy( ErrorString,  "分解能の取得ができません"); break;}
        case 21304: { strcpy( ErrorString,  "AiDirectionの値が使用しているデバイスの範囲外です"); break;}
        case 21420: { strcpy( ErrorString,  "AiChannelの値が使用しているデバイスの範囲外です"); break;}
        case 21421: { strcpy( ErrorString,  "AD変換エラーが発生しました"); break;}
        case 21423: { strcpy( ErrorString,  "変換に使用されていないチャネルが指定されました"); break;}
        case 21440: { strcpy( ErrorString,  "AD変換エラーが発生しました"); break;}
        case 21441: { strcpy( ErrorString,  "変換チャネル数をオーバーしました。最大チャネルで変換します"); break;}
        case 21460: { strcpy( ErrorString,  "サンプリングクロックエラーが発生しているためデバイスをリセットする必要があります"); break;}
        case 21461: { strcpy( ErrorString,  "仮想メモリ用のメモリが確保できません"); break;}
        case 21462: { strcpy( ErrorString,  "仮想メモリを初期化することができません"); break;}
        case 21463: { strcpy( ErrorString,  "ソフトウェアスタート時はリピート回数を1に設定してください"); break;}
        case 21464: { strcpy( ErrorString,  "ドライバスレッドの作成に失敗しました"); break;}
        case 21466: { strcpy( ErrorString,  "ユーザーバッファが設定されていません"); break;}
        case 21469: { strcpy( ErrorString,  "バッファオーバーフローエラーが発生しています、メモリをリセットしてください"); break;}
        case 21480: { strcpy( ErrorString,  "このコマンドでは変換を停止できません。AioResetDeviceを使用してください"); break;}
        case 21481: { strcpy( ErrorString,  "マスタ転送の停止に失敗しました"); break;}
        case 21520: { strcpy( ErrorString,  "サンプリング回数の取得に失敗しました"); break;}
        case 21580: { strcpy( ErrorString,  "変換されたデータ数を超えるデータを取得しようとしました、AiSamplingTimesを最大サンプリング数に変更してデータを取得します"); break;}
        case 21581: { strcpy( ErrorString,  "アドレスの取得に失敗しました"); break;}
        case 21584: { strcpy( ErrorString,  "FIFOが空です"); break;}
        case 21760: { strcpy( ErrorString,  "AiChannelの値が使用しているデバイスの範囲外です"); break;}
        case 21761: { strcpy( ErrorString,  "AiSequenceの値が使用しているデバイスの範囲外です"); break;}
        case 21770: { strcpy( ErrorString,  "AiChannelの値が使用しているデバイスの範囲外です"); break;}
        case 21840: { strcpy( ErrorString,  "AiChannelの値が使用しているデバイスの範囲外です"); break;}
        case 21841: { strcpy( ErrorString,  "AD変換エラーが発生しました"); break;}
        case 21843: { strcpy( ErrorString,  "変換に使用されていないチャネルが指定されました"); break;}
        case 21860: { strcpy( ErrorString,  "AD変換エラーが発生しました"); break;}
        case 21861: { strcpy( ErrorString,  "変換チャネル数をオーバーしました、最大チャネルで変換します"); break;}
        case 21880: { strcpy( ErrorString,  "AiChannelsの値が使用しているデバイスの範囲外です"); break;}
        case 21881: { strcpy( ErrorString,  "変換されたデータ数を超えるデータを取得しようとしました、AiSamplingTimesを最大値に変更してデータを取得します"); break;}
        case 21882: { strcpy( ErrorString,  "アドレスの取得に失敗しました"); break;}
        case 21885: { strcpy( ErrorString,  "FIFOが空です"); break;}
        case 21920: { strcpy( ErrorString,  "AiChannelの値が使用しているデバイスの範囲外です"); break;}
        case 21921: { strcpy( ErrorString,  "AiStartLevelの値が使用しているデバイスの範囲外です"); break;}
        case 21922: { strcpy( ErrorString,  "分解能の取得ができません"); break;}
        case 21923: { strcpy( ErrorString,  "AiDirectionの値が使用しているデバイスの範囲外です"); break;}
        case 21930: { strcpy( ErrorString,  "AiChannelの値が使用しているデバイスの範囲外です"); break;}
        case 21940: { strcpy( ErrorString,  "AiChannelの値が使用しているデバイスの範囲外です"); break;}
        case 21941: { strcpy( ErrorString,  "AiStopLevelの値が使用しているデバイスの範囲外です"); break;}
        case 21942: { strcpy( ErrorString,  "分解能の取得ができません"); break;}
        case 21943: { strcpy( ErrorString,  "AiDirectionの値が使用しているデバイスの範囲外です"); break;}
        case 21950: { strcpy( ErrorString,  "AiChannelの値が使用しているデバイスの範囲外です"); break;}
        case 21960: { strcpy( ErrorString,  "AiTransferModeの値が使用しているデバイスの範囲外です"); break;}
        case 21980: { strcpy( ErrorString,  "DataNumberの値が使用しているデバイスの範囲外です"); break;}
        case 21981: { strcpy( ErrorString,  "メモリのロックに失敗しました"); break;}
        case 21982: { strcpy( ErrorString,  "一時的なメモリの確保に失敗しました"); break;}
        case 21984: { strcpy( ErrorString,  "バスマスタ転送中に関数が実行されました"); break;}
        case 22040: { strcpy( ErrorString,  "AiChannelの値が使用しているデバイスの範囲外です"); break;}
        case 22041: { strcpy( ErrorString,  "12ビットデバイスに設定するデータは0〜4095の範囲内です"); break;}
        case 22042: { strcpy( ErrorString,  "16ビットデバイスに設定するデータは0〜65535の範囲内です"); break;}
        case 22043: { strcpy( ErrorString,  "分解能の取得ができません"); break;}
        case 22044: { strcpy( ErrorString,  "StateTimesの値が使用しているデバイスの範囲外です"); break;}
        case 22050: { strcpy( ErrorString,  "AiChannelの値が使用しているデバイスの範囲外です"); break;}
        case 22060: { strcpy( ErrorString,  "AiChannelの値が使用しているデバイスの範囲外です"); break;}
        case 22061: { strcpy( ErrorString,  "12ビットデバイスに設定するデータは0〜4095の範囲内です"); break;}
        case 22062: { strcpy( ErrorString,  "16ビットデバイスに設定するデータは0〜65535の範囲内です"); break;}
        case 22063: { strcpy( ErrorString,  "分解能の取得ができません"); break;}
        case 22064: { strcpy( ErrorString,  "StateTimesの値が使用しているデバイスの範囲外です"); break;}
        case 22070: { strcpy( ErrorString,  "AiChannelの値が使用しているデバイスの範囲外です"); break;}
        case 22080: { strcpy( ErrorString,  "AiChannelの値が使用しているデバイスの範囲外です"); break;}
        case 22081: { strcpy( ErrorString,  "12ビットデバイスに設定するデータは0〜4095の範囲内です"); break;}
        case 22082: { strcpy( ErrorString,  "16ビットデバイスに設定するデータは0〜65535の範囲内です"); break;}
        case 22083: { strcpy( ErrorString,  "分解能の取得ができません"); break;}
        case 22084: { strcpy( ErrorString,  "StateTimesの値が使用しているデバイスの範囲外です"); break;}
        case 22085: { strcpy( ErrorString,  "ドライバ内部エラーが発生しました"); break;}
        case 22090: { strcpy( ErrorString,  "AiChannelの値が使用しているデバイスの範囲外です"); break;}
        case 22100: { strcpy( ErrorString,  "AiChannelの値が使用しているデバイスの範囲外です"); break;}
        case 22101: { strcpy( ErrorString,  "12ビットデバイスに設定するデータは0〜4095の範囲内です"); break;}
        case 22102: { strcpy( ErrorString,  "16ビットデバイスに設定するデータは0〜65535の範囲内です"); break;}
        case 22103: { strcpy( ErrorString,  "分解能の取得ができません"); break;}
        case 22104: { strcpy( ErrorString,  "StateTimesの値が使用しているデバイスの範囲外です"); break;}
        case 22105: { strcpy( ErrorString,  "ドライバ内部エラーが発生しました"); break;}
        case 22110: { strcpy( ErrorString,  "AiChannelの値が使用しているデバイスの範囲外です"); break;}
        case 22120: { strcpy( ErrorString,  "AiTransferTimesの値が使用しているデバイスの指定範囲外です"); break;}
        case 22140: { strcpy( ErrorString,  "転送回数の取得に失敗しました"); break;}
        case 22160: { strcpy( ErrorString,  "上書き回数の取得に失敗しました"); break;}
        case 22200: { strcpy( ErrorString,  "Rangeの値が使用しているデバイスの範囲外です"); break;}
        case 22201: { strcpy( ErrorString,  "Typeの値が使用しているデバイスの範囲外です"); break;}
        case 22202: { strcpy( ErrorString,  "Dataの値が使用しているデバイスの範囲外です"); break;}
        case 22220: { strcpy( ErrorString,  "Rangeの値が使用しているデバイスの範囲外です"); break;}
        case 22221: { strcpy( ErrorString,  "Typeの値が使用しているデバイスの範囲外です"); break;}
        case 22222: { strcpy( ErrorString,  "キャリブレーションの読み出しに失敗しました"); break;}
        case 22223: { strcpy( ErrorString,  "ドライバ内部エラーが発生しました"); break;}
        case 22240: { strcpy( ErrorString,  "AiScanClockの値が使用しているデバイスの指定範囲外です"); break;}
        case 22241: { strcpy( ErrorString,  "ドライバ内部エラーが発生しました"); break;}
        case 23000: { strcpy( ErrorString,  "AoChannelの値が使用しているデバイスの範囲外です"); break;}
        case 23001: { strcpy( ErrorString,  "DA変換エラーが発生しました"); break;}
        case 23002: { strcpy( ErrorString,  "AoDataに設定するデータはは0〜4095の範囲で指定してください"); break;}
        case 23003: { strcpy( ErrorString,  "AoDataに設定するデータはは0〜65535の範囲で指定してください"); break;}
        case 23020: { strcpy( ErrorString,  "DA変換エラーが発生しました"); break;}
        case 23021: { strcpy( ErrorString,  "変換チャネル数をオーバーしました.最大チャネルで変換します"); break;}
        case 23022: { strcpy( ErrorString,  "AoDataに設定するデータはは0〜4095の範囲で指定してください"); break;}
        case 23023: { strcpy( ErrorString,  "AoDataに設定するデータはは0〜65535の範囲で指定してください"); break;}
        case 23060: { strcpy( ErrorString,  "デバイスの最大チャネル数を超えているため、最大チャネル数で設定しました"); break;}
        case 23100: { strcpy( ErrorString,  "AoChannelの値が使用しているデバイスの範囲外です"); break;}
        case 23101: { strcpy( ErrorString,  "AoRangeの値が使用しているデバイスの指定範囲外です"); break;}
        case 23102: { strcpy( ErrorString,  "レンジの設定に失敗しました"); break;}
        case 23120: { strcpy( ErrorString,  "AoRangeの値が使用しているデバイスの指定範囲外です"); break;}
        case 23121: { strcpy( ErrorString,  "レンジの設定に失敗しました"); break;}
        case 23140: { strcpy( ErrorString,  "AoMemoryTypeの値が使用しているデバイスの指定範囲外です"); break;}
        case 23161: { strcpy( ErrorString,  "リピート回数はメモリ形式がRINGの時のみ設定可能です"); break;}
        case 23180: { strcpy( ErrorString,  "AoClockTypeの値が使用しているデバイスの指定範囲外です"); break;}
        case 23200: { strcpy( ErrorString,  "AoSamplingClockの値が使用しているデバイスの指定範囲外です"); break;}
        case 23220: { strcpy( ErrorString,  "出力データがメモリ容量をオーバーしています"); break;}
        case 23221: { strcpy( ErrorString,  "仮想メモリ用のメモリが確保できません"); break;}
        case 23240: { strcpy( ErrorString,  "AoStartTriggerの値が使用しているデバイスの指定範囲外です"); break;}
        case 23260: { strcpy( ErrorString,  "AoStopTriggerの値が使用しているデバイスの指定範囲外です"); break;}
        case 23320: { strcpy( ErrorString,  "データが設定されていないか、メモリの確保できません"); break;}
        case 23321: { strcpy( ErrorString,  "データが設定されていません"); break;}
        case 23326: { strcpy( ErrorString,  "ユーザーバッファが設定されていません"); break;}
        case 23500: { strcpy( ErrorString,  "AoChannelの値が使用しているデバイスの範囲外です"); break;}
        case 23501: { strcpy( ErrorString,  "DA変換エラーが発生しました"); break;}
        case 23520: { strcpy( ErrorString,  "DA変換エラーが発生しました"); break;}
        case 23521: { strcpy( ErrorString,  "変換チャネル数をオーバーしました、最大チャネルで変換します"); break;}
        case 23540: { strcpy( ErrorString,  "出力データがメモリ容量をオーバーしています"); break;}
        case 23541: { strcpy( ErrorString,  "仮想メモリの確保ができません"); break;}
        case 23580: { strcpy( ErrorString,  "AoTransferModeの値が使用しているデバイスの範囲外です"); break;}
        case 23600: { strcpy( ErrorString,  "DataNumberの値が使用しているデバイスの範囲外です"); break;}
        case 23601: { strcpy( ErrorString,  "メモリのロックに失敗しました"); break;}
        case 23602: { strcpy( ErrorString,  "一時的なメモリの確保に失敗しました"); break;}
        case 23741: { strcpy( ErrorString,  "Typeの値が使用しているデバイスの範囲外です"); break;}
        case 24000: { strcpy( ErrorString,  "DiBitの値が使用しているデバイスの範囲外です"); break;}
        case 24010: { strcpy( ErrorString,  "DiPortの値が使用しているデバイスの範囲外です"); break;}
        case 24020: { strcpy( ErrorString,  "DoBitの値が使用しているデバイスの範囲外です"); break;}
        case 24030: { strcpy( ErrorString,  "DoPortの値が使用しているデバイスの範囲外です"); break;}
        case 24040: { strcpy( ErrorString,  "Bitの値が使用しているデバイスの範囲外です"); break;}
        case 24041: { strcpy( ErrorString,  "Valueの値が使用しているデバイスの範囲外です"); break;}
        case 24050: { strcpy( ErrorString,  "Bitの値が使用しているデバイスの範囲外です"); break;}
        case 25000: { strcpy( ErrorString,  "CntChannelの値がデバイスの指定範囲外です"); break;}
        case 25002: { strcpy( ErrorString,  "プリセットカウント値のロード機能は使用できません"); break;}
        case 25003: { strcpy( ErrorString,  "比較カウント値のロード機能は使用できません"); break;}
        case 25010: { strcpy( ErrorString,  "CntChannelの値がデバイスの指定範囲外です"); break;}
        case 25020: { strcpy( ErrorString,  "CntChannelの値がデバイスの指定範囲外です"); break;}
        case 25021: { strcpy( ErrorString,  "PresetNumberの値がデバイスの指定範囲外です"); break;}
        case 25040: { strcpy( ErrorString,  "CntChannelの値がデバイスの指定範囲外です"); break;}
        case 25041: { strcpy( ErrorString,  "ComparisonNumberの値がデバイスの指定範囲外です"); break;}
        case 25060: { strcpy( ErrorString,  "CntChannelの値がデバイスの指定範囲外です"); break;}
        case 25061: { strcpy( ErrorString,  "CntClockTypeの値が使用しているデバイスの指定範囲外です"); break;}
        case 25070: { strcpy( ErrorString,  "CntChannelの値がデバイスの指定範囲外です"); break;}
        case 25080: { strcpy( ErrorString,  "CntChannelの値がデバイスの指定範囲外です"); break;}
        case 25090: { strcpy( ErrorString,  "CntChannelの値がデバイスの指定範囲外です"); break;}
        case 25100: { strcpy( ErrorString,  "CntChannelの値がデバイスの指定範囲外です"); break;}
        case 25120: { strcpy( ErrorString,  "CntChannelの値がデバイスの指定範囲外です"); break;}
        case 25121: { strcpy( ErrorString,  "Signalの値が使用しているデバイスの指定範囲外です"); break;}
        case 25122: { strcpy( ErrorString,  "Valueの値が使用しているデバイスの指定範囲外です"); break;}
        case 25123: { strcpy( ErrorString,  "Singleの値が使用しているデバイスの指定範囲外です"); break;}
        case 25130: { strcpy( ErrorString,  "CntChannelの値がデバイスの指定範囲外です"); break;}
        case 25131: { strcpy( ErrorString,  "Signalの値が関数の指定範囲外です"); break;}
        case 25140: { strcpy( ErrorString,  "CntChannelの値がデバイスの指定範囲外です"); break;}
        case 25160: { strcpy( ErrorString,  "CntChannelの値がデバイスの指定範囲外です"); break;}
        case 25180: { strcpy( ErrorString,  "CntChannelの値がデバイスの指定範囲外です"); break;}
        case 25181: { strcpy( ErrorString,  "Presetの値がデバイスの指定範囲外です"); break;}
        case 25200: { strcpy( ErrorString,  "CntChannelの値がデバイスの指定範囲外です"); break;}
        case 25220: { strcpy( ErrorString,  "CntChannelの値がデバイスの指定範囲外です"); break;}
        case 25240: { strcpy( ErrorString,  "CntChannelの値がデバイスの指定範囲外です"); break;}
        case 26000: { strcpy( ErrorString,  "TimerIdの値が使用しているデバイスの指定範囲外です"); break;}
        case 26010: { strcpy( ErrorString,  "TimerIdの値が使用しているデバイスの指定範囲外です"); break;}
        case 26020: { strcpy( ErrorString,  "TimerIdの値が使用しているデバイスの指定範囲外です"); break;}
        case 26040: { strcpy( ErrorString,  "TimerIdの値が使用しているデバイスの指定範囲外です"); break;}
        case 26060: { strcpy( ErrorString,  "TimerIdの値が使用しているデバイスの指定範囲外です"); break;}
        case 26080: { strcpy( ErrorString,  "TimerIdの値が使用しているデバイスの指定範囲外です"); break;}
        case 26100: { strcpy( ErrorString,  "TimerIdの値が使用しているデバイスの指定範囲外です"); break;}
        case 26120: { strcpy( ErrorString,  "TimerIdの値が使用しているデバイスの指定範囲外です"); break;}
        case 26140: { strcpy( ErrorString,  "TimerIdの値が使用しているデバイスの指定範囲外です"); break;}
        case 26160: { strcpy( ErrorString,  "TimerIdの値が使用しているデバイスの指定範囲外です"); break;}
        case 27000: { strcpy( ErrorString,  "Destinationの値が使用しているデバイスの指定範囲外です"); break;}
        case 27001: { strcpy( ErrorString,  "Sourceの値が使用しているデバイスの指定範囲外です"); break;}
        case 27002: { strcpy( ErrorString,  "無効な接続です"); break;}
        case 27010: { strcpy( ErrorString,  "Destinationの値が使用しているデバイスの指定範囲外です"); break;}
        case 28000: { strcpy( ErrorString,  "モードが設定可能範囲外です"); break;}
        case 28001: { strcpy( ErrorString,  "チャネル番号が設定可能範囲外です"); break;}
        case 28010: { strcpy( ErrorString,  "チャネル数が設定可能範囲外です"); break;}
        case 28011: { strcpy( ErrorString,  "カウンタ値が設定可能範囲外です"); break;}
        case 28012: { strcpy( ErrorString,  "比較レジスタ番号が設定可能範囲外です"); break;}
        case 28020: { strcpy( ErrorString,  "タイマ値が設定可能範囲外です"); break;}
        case 28021: { strcpy( ErrorString,  "制御出力信号の出力論理が設定可能範囲外です"); break;}
        case 28022: { strcpy( ErrorString,  "ハードウェアイベントの種類が設定可能範囲外です"); break;}
        case 28030: { strcpy( ErrorString,  "ワンショットパルスのパルス幅の係数が設定可能範囲外です"); break;}
        case 28031: { strcpy( ErrorString,  "制御入力信号の入力論理が設定可能範囲外です"); break;}
        case 28032: { strcpy( ErrorString,  "出力データが設定可能範囲外です"); break;}
        default: { strcpy( ErrorString,  "現在使用されていない予備のエラーコードです"); break;}
    }
    return 0;
}


