//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by DummyContec.rc
//
#define IDC_LST_AO                      1001
#define IDC_EDIT1                       1002
#define IDC_EDIT2                       1003
#define IDC_EDIT3                       1004
#define IDC_EDIT4                       1005
#define IDC_PB_SET_1                    1006
#define IDC_PB_SET_2                    1007
#define IDC_PB_SET_3                    1008
#define IDC_PB_SET_4                    1009
#define IDC_ST_DEVICE                   1010
#define IDC_ST_ID                       1011
#define IDC_CB_AI_MODE_1                1012
#define IDC_CB_AI_MODE_2                1013
#define IDC_CB_AI_MODE_3                1014
#define IDC_CB_AI_MODE_4                1015
#define IDC_LSC_AO                      1016
#define IDC_CK_DI1                      1017
#define IDC_CK_DI2                      1018
#define IDC_CK_DI3                      1019
#define IDC_CK_DI4                      1020
#define IDC_CK_DI5                      1021
#define IDC_CK_DI6                      1022
#define IDC_CK_DI7                      1023
#define IDC_CK_DI8                      1024
#define IDC_CK_DO1                      1025
#define IDC_CK_DO2                      1026
#define IDC_CK_DO3                      1027
#define IDC_CK_DO4                      1028
#define IDC_CK_DO5                      1029
#define IDC_CK_DO6                      1030
#define IDC_CK_DO7                      1031
#define IDC_CK_DO8                      1032
#define IDC_CB_AI_RANGE                 1033
#define IDC_ED_COUNTER_1                1034
#define IDC_ED_COUNTER_2                1035
#define IDC_PB_CNT_SET_1                1036
#define IDC_PB_CNT_SET_2                1037
#define IDC_CK_AUTO_CNT_1               1038
#define IDC_CK_AUTO_CNT_2               1039
#define IDC_ST_S1                       1040
#define IDC_ST_S2                       1041
#define IDC_ST_S3                       1042
#define IDC_ST_S4                       1043

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1041
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
