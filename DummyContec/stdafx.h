// stdafx.h : 標準のシステム インクルード ファイルのインクルード ファイル、または
// 参照回数が多く、かつあまり変更されない、プロジェクト専用のインクルード ファイル
// を記述します。
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Windows ヘッダーから使用されていない部分を除外します。
// Windows ヘッダー ファイル:
#include <windows.h>
#include <string>

#ifdef  UNICODE             
#define StdChar             wchar_t
#define StdString           std::wstring
#define StdStringStream     std::wstringstream
#define StdStreamBuf        std::wstreambuf
#define StdOut              std::wostream
#else
#define StdChar             char
#define StdString           std::string
#define StdStringStream     std::stringstream
#define StdStreamBuf        std::streambuf
#define StdOut              std::ostream
#endif

#include <TCHAR.h>
void DB_PRINT(const StdChar* pStr, ...);
StdString CharToString(const char* str);
std::string StringToChar(StdString strString);


// TODO: プログラムに必要な追加ヘッダーをここで参照してください。
enum E_WINDOW_MESSAGE
{
    WM_DIALOG_UPDATE = (WM_APP + 1),
};