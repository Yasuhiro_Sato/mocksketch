
// DummyContecTestbed.h : PROJECT_NAME アプリケーションのメイン ヘッダー ファイルです。
//

#pragma once

#ifndef __AFXWIN_H__
	#error "PCH に対してこのファイルをインクルードする前に 'stdafx.h' をインクルードしてください"
#endif

#include "resource.h"		// メイン シンボル


// CDummyContecTestbedApp:
// このクラスの実装については、DummyContecTestbed.cpp を参照してください。
//

class CDummyContecTestbedApp : public CWinAppEx
{
public:
	CDummyContecTestbedApp();

// オーバーライド
	public:
	virtual BOOL InitInstance();

// 実装

	DECLARE_MESSAGE_MAP()
};

extern CDummyContecTestbedApp theApp;