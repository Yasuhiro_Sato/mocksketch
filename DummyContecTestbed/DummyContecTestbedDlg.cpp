
// DummyContecTestbedDlg.cpp : 実装ファイル
//

#include "stdafx.h"
#include "DummyContecTestbed.h"
#include "DummyContecTestbedDlg.h"
#include "DummyContecTestbedDlg.h"
#include "Caio.h"
#include <math.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// アプリケーションのバージョン情報に使われる CAboutDlg ダイアログ

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// ダイアログ データ
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート

// 実装
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CDummyContecTestbedDlg ダイアログ




CDummyContecTestbedDlg::CDummyContecTestbedDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDummyContecTestbedDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

    m_hThread = 0;
    m_dwThreadId = 0;

    m_bPause = false;
    m_bStop = false;
    m_bExec = false;

    m_dwSleepTime     = 10;
    m_dwBeforeTime    = 0;
    m_dwUpdateDlgTime = 1000;

    m_sDevId = -1;

}


void CDummyContecTestbedDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_CB_DEVICE, m_cbDevice);
    DDX_Control(pDX, IDC_LSC_AO, m_lcAi);
    DDX_Control(pDX, IDC_CB_AI_RANGE, m_cbAoRange);
    DDX_Control(pDX, IDC_ST_S1, m_stAo[0]);
    DDX_Control(pDX, IDC_ST_S2, m_stAo[1]);
    DDX_Control(pDX, IDC_ST_S3, m_stAo[2]);
    DDX_Control(pDX, IDC_ST_S4, m_stAo[3]);
    DDX_Control(pDX, IDC_EDIT1, m_edAo[0]);
    DDX_Control(pDX, IDC_EDIT2, m_edAo[1]);
    DDX_Control(pDX, IDC_EDIT3, m_edAo[2]);
    DDX_Control(pDX, IDC_EDIT4, m_edAo[3]);

    DDX_Control(pDX, IDC_CB_AI_MODE_1, m_cbAoMode[0]);
    DDX_Control(pDX, IDC_CB_AI_MODE_2, m_cbAoMode[1]);
    DDX_Control(pDX, IDC_CB_AI_MODE_3, m_cbAoMode[2]);
    DDX_Control(pDX, IDC_CB_AI_MODE_4, m_cbAoMode[3]);

    DDX_Control(pDX, IDC_ED_COUNTER_1, m_edCnt[0]);
    DDX_Control(pDX, IDC_ED_COUNTER_2, m_edCnt[1]);
}

BEGIN_MESSAGE_MAP(CDummyContecTestbedDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
    ON_BN_CLICKED(IDC_PB_START, &CDummyContecTestbedDlg::OnBnClickedPbStart)
    ON_CONTROL_RANGE(BN_CLICKED, IDC_CK_DO1, IDC_CK_DO8, CDummyContecTestbedDlg::OnBnClickedCkDo)
    ON_CONTROL_RANGE(BN_CLICKED, IDC_CK_DI1, IDC_CK_DI8, CDummyContecTestbedDlg::OnBnClickedCkDi)
    ON_CONTROL_RANGE(CBN_SELCHANGE, IDC_CB_AI_MODE_1, IDC_CB_AI_MODE_4, &CDummyContecTestbedDlg::OnCbnSelchangeCbAiMode)
    ON_CONTROL_RANGE(BN_CLICKED, IDC_PB_SET_1, IDC_PB_SET_4, &CDummyContecTestbedDlg::OnBnClickedPbSet)

    ON_CBN_SELCHANGE(IDC_CB_DEVICE, &CDummyContecTestbedDlg::OnCbnSelchangeCbDevice)
    ON_WM_TIMER()
    ON_WM_DESTROY()
    ON_CBN_SELCHANGE(IDC_CB_AI_RANGE, &CDummyContecTestbedDlg::OnCbnSelchangeCbAiRange)
END_MESSAGE_MAP()


// CDummyContecTestbedDlg メッセージ ハンドラ

BOOL CDummyContecTestbedDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// "バージョン情報..." メニューをシステム メニューに追加します。

	// IDM_ABOUTBOX は、システム コマンドの範囲内になければなりません。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// このダイアログのアイコンを設定します。アプリケーションのメイン ウィンドウがダイアログでない場合、
	//  Framework は、この設定を自動的に行います。
	SetIcon(m_hIcon, TRUE);			// 大きいアイコンの設定
	SetIcon(m_hIcon, FALSE);		// 小さいアイコンの設定


    //////////////////////////////////////////////////////

    long lRet;
    long lRetInit;

    short sIndex = 0;
    short sId;
    char szDevName[1024];
    char szDevice[1024];
    
    StdString strDevName;
    StdString strDevice;
    CString strCombo;
    int iIndex;
    while(1)
    {
        lRet = AioQueryDeviceName( sIndex, szDevName, szDevice);
        if (lRet == 0)
        {
            strDevName = CharToString(szDevName);
            strDevice = CharToString(szDevice);

            strCombo.Format(_T("%s:%s"), 
                                         strDevName.c_str(),
                                         strDevice.c_str());

            iIndex = m_cbDevice.AddString(strCombo);

            lRetInit = AioInit(szDevName, &sId);

            m_cbDevice.SetItemData( iIndex, static_cast<DWORD_PTR>(sId));
            sIndex++;
        }
        else
        {
            break;
        }
    }


    m_lcAi.InsertColumn(0, _T("Ch"), LVCFMT_LEFT, 50);
    m_lcAi.InsertColumn(1, _T("Value"), LVCFMT_LEFT, 150);
    m_lcAi.InsertColumn(2, _T("Unit"), LVCFMT_LEFT, 50);

    for(int iCnt = 0; iCnt < 4; iCnt++)
    {
        m_cbAoMode[iCnt].AddString(_T("NONE"));
        m_cbAoMode[iCnt].AddString(_T("SIN"));
        m_cbAoMode[iCnt].AddString(_T("RECT"));
        m_cbAoMode[iCnt].AddString(_T("RAND"));
        m_cbAoMode[iCnt].SetCurSel(0);
    }

    Start();
	return TRUE;  // フォーカスをコントロールに設定した場合を除き、TRUE を返します。
}

void CDummyContecTestbedDlg::SetDevice(short sId)
{
    short sAiMax;
    short sAoMax;
    short sCntMax;

    ASSERT( AioGetAiMaxChannels(sId, &sAiMax) == 0);
    ASSERT( AioGetAoMaxChannels(sId, &sAoMax) == 0);
    ASSERT( AioGetCntMaxChannels(sId, &sCntMax) == 0);

    m_cbAoRange.ResetContent();
    
    //
    m_lcAi.DeleteAllItems();
    CString strCh;
    for (int iCnt = 0; iCnt < sAiMax; iCnt++)
    {
        strCh.Format(_T("%02d"), iCnt + 1);
        m_lcAi.InsertItem(iCnt, strCh);
        m_lcAi.SetItemText(iCnt, 1, _T("0"));
    }

    int iMax = static_cast<int>(ceil(sAoMax / 4.0));
    for(int iCnt = 0; iCnt < iMax; iCnt++)
    {
        strCh.Format(_T("%04d-%04d"), iCnt * 4, iCnt * 4 + 3);
        m_cbAoRange.AddString(strCh);
    }
    m_cbAoRange.SetCurSel(0);

    m_sDevId = sId;

    OnCbnSelchangeCbAiRange();
}


void CDummyContecTestbedDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// ダイアログに最小化ボタンを追加する場合、アイコンを描画するための
//  下のコードが必要です。ドキュメント/ビュー モデルを使う MFC アプリケーションの場合、
//  これは、Framework によって自動的に設定されます。

void CDummyContecTestbedDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 描画のデバイス コンテキスト

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// クライアントの四角形領域内の中央
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// アイコンの描画
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// ユーザーが最小化したウィンドウをドラッグしているときに表示するカーソルを取得するために、
//  システムがこの関数を呼び出します。
HCURSOR CDummyContecTestbedDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CDummyContecTestbedDlg::OnBnClickedPbStart()
{
    AioStartAi(m_sDevId);
}

void CDummyContecTestbedDlg::OnBnClickedCkDo(UINT nID)
{
    short sCheck;
    sCheck = ((IsDlgButtonChecked(nID) == 0)? 0: 1);
    AioOutputDoBit(m_sDevId, nID - IDC_CK_DO1, sCheck);

}

void CDummyContecTestbedDlg::OnBnClickedCkDi(UINT nID)
{
}

void CDummyContecTestbedDlg::OnCbnSelchangeCbAiMode(UINT nID)
{
    // TODO: ここにコントロール通知ハンドラ コードを追加します。
}

void CDummyContecTestbedDlg::OnCbnSelchangeCbDevice()
{
    int iIndex;
    iIndex = m_cbDevice.GetCurSel();
    short sId = static_cast<short>(m_cbDevice.GetItemData(iIndex));
    SetDevice(sId);
}


void CDummyContecTestbedDlg::UpdateInput()
{

    if (m_sDevId == -1)
    {
        return;
    }

    short sAiMax;
    short sAoMax;
    short sCntMax;


    ASSERT( AioGetAiMaxChannels(m_sDevId, &sAiMax) == 0);
    ASSERT( AioGetAoMaxChannels(m_sDevId, &sAoMax) == 0);
    ASSERT( AioGetCntMaxChannels(m_sDevId, &sCntMax) == 0);
    
    CString strIo;
    long lData;
    for (short iCh = 0; iCh < sAiMax; iCh++)
    {
        AioSingleAi(m_sDevId, iCh, &lData);
        strIo.Format(_T("%d"), lData);
        m_lcAi.SetItemText(iCh, 1, strIo);
    }

    long lCnt;
    for (short iCh = 0; iCh < sCntMax; iCh++)
    {
        AioGetCntCount(m_sDevId, iCh, &lCnt);
        strIo.Format(_T("%d"), lCnt);
        m_edCnt[iCh].SetWindowText(strIo);
    }

    short sDiData;
    for (short iCh = 0; iCh < 8; iCh++)
    {
        if (AioInputDiBit(m_sDevId, iCh, &sDiData) == 0)
        {
            CheckDlgButton( IDC_CK_DI1+ iCh, (sDiData ? BST_CHECKED: BST_UNCHECKED));
        }
    }
}

//----------------
// 開始
//----------------
bool CDummyContecTestbedDlg::Start()
{
    if (m_hThread == 0)
    {
        m_bPause = false;
        m_bStop  = false;
        m_dwBeforeTime = 0;

        m_hThread = (HANDLE)_beginthreadex( NULL, 0, 
            &CDummyContecTestbedDlg::ThreadFunc, this, 0, &m_dwThreadId );

        return true;
    }
    return false;
}

//----------------
// 一時停止
//----------------
bool CDummyContecTestbedDlg::Pause()
{
    m_bPause = true;
    return true;
}

//----------------
// 停止
//----------------
bool CDummyContecTestbedDlg::Stop()
{
    m_bPause = false;
    m_bStop  = true;

    if (m_hThread != 0)
    {
        WaitForSingleObject(m_hThread,INFINITE);
        CloseHandle(m_hThread);
    }
    m_hThread = 0;
    return true;
}


//----------------
// スレッド
//----------------
void CDummyContecTestbedDlg::ThreadCallFunc()
{
    DWORD dwCurTime = GetTickCount();//  より正確な時刻は、timeGetTime (要winmm.lib)
    /*
    for(int iCh = 0; iCh < m_pProduct->iInChNum; iCh++)
    {
        if (m_modeAi[iCh] == AI_SIN)
        {
            m_iAi[iCh] = CreateSin(iCh, dwCurTime);
        }
        else if (m_modeAi[iCh] == AI_RECT)
        {
            m_iAi[iCh] = CreateRect(iCh, dwCurTime);
        }
        else if (m_modeAi[iCh] == AI_RANDOM)
        {
            m_iAi[iCh] = CreateRandom();
        }
    }

    for(int iCh = 0; iCh < m_pProduct->iCntNum; iCh++)
    {
        if (m_bExecCount[iCh]){m_dwCount[iCh]++;}
    }
    */
    if( dwCurTime > (m_dwBeforeTime + m_dwSleepTime))
    {
        UpdateInput();
        m_dwBeforeTime = dwCurTime;
    }
}   


//----------------
// スレッド
//----------------
unsigned __stdcall  CDummyContecTestbedDlg::ThreadFunc(void*  pVoid)
{
    DWORD dwSleep;
    CDummyContecTestbedDlg* pDev = reinterpret_cast<CDummyContecTestbedDlg*>(pVoid);
    
    pDev->m_bExec = true;
    dwSleep = pDev->GetSleepTime();
    while(1)
    {
        if (pDev->IsStopRequest())
        {
            break;
        }

        if (pDev->IsPauseRequest())
        {
            Sleep(10);
            continue;
        }

        pDev->ThreadCallFunc();
        Sleep(dwSleep);
    }

    pDev->m_bExec = false;
    return 0;
}



void CDummyContecTestbedDlg::OnTimer(UINT_PTR nIDEvent)
{
    // TODO: ここにメッセージ ハンドラ コードを追加するか、既定の処理を呼び出します。

    CDialog::OnTimer(nIDEvent);
}

void CDummyContecTestbedDlg::OnDestroy()
{
    CDialog::OnDestroy();
    Stop();
}

void CDummyContecTestbedDlg::OnCbnSelchangeCbAiRange()
{

    StdChar strTxt[256];
    int iPage;
    int iMode;
    int iVal;
    iPage = m_cbAoRange.GetCurSel();

    int iCh;
    CString strOut;
    for (int iCnt = 0; iCnt < 4; iCnt++)
    {
        iCh = iCnt + (iPage * 4);
        strOut.Format(_T("%02d"), iCh);
        m_stAo[iCnt].SetWindowText(strOut);


        /*
        iMode = m_pDevice->GetAiMode(iCh);
        SetComboBoxSel(m_hCbAiMode[iCnt], iMode);
        iVal = m_pDevice->GetAi(iCh);
        _sntprintf(&strTxt[0] ,sizeof(strTxt), _T("%d"), iVal);
        SetDlgItemText(m_hWnd, IDC_EDIT1 + iCnt, strTxt);
        */
    }
}

void CDummyContecTestbedDlg::OnBnClickedPbSet(UINT nID)
{
    int iPage;
    int iMode;
    int iVal;
    iPage = m_cbAoRange.GetCurSel();

    int iCh;
    CString strOut;
    iCh = nID - IDC_PB_SET_1 + (iPage * 4);

    m_edAo[nID - IDC_PB_SET_1].GetWindowText(strOut);
    iVal = _tstoi(strOut);
    AioSingleAo( m_sDevId,iCh, iVal);
}
