
// DummyContecTestbedDlg.h : ヘッダー ファイル
//

#pragma once
#include "afxwin.h"

// CDummyContecTestbedDlg ダイアログ
class CDummyContecTestbedDlg : public CDialog
{
// コンストラクション
public:
	CDummyContecTestbedDlg(CWnd* pParent = NULL);	// 標準コンストラクタ

// ダイアログ データ
	enum { IDD = IDD_DUMMYCONTECTESTBED_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV サポート

    
// 実装
protected:
	HICON m_hIcon;

	// 生成された、メッセージ割り当て関数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

    void SetDevice(int iId);


public:
    afx_msg void OnBnClickedPbStart();

    afx_msg void OnBnClickedCkDo(UINT nID);
    afx_msg void OnBnClickedCkDi(UINT nID);
    afx_msg void OnBnClickedPbSet(UINT nID);
    afx_msg void OnCbnSelchangeCbAiMode(UINT nID);
    afx_msg void OnCbnSelchangeCbDevice();


    void SetDevice(short sId);

    //---------------
    // スレッド関連
    //---------------
    bool Start();
    bool Stop();
    bool Pause();


    bool IsExec(){ return m_bExec;}
    bool IsStopRequest(){ return m_bStop;}
    bool IsPauseRequest(){ return m_bPause;}
    DWORD GetThreadId(){return m_dwThreadId;}
    DWORD GetSleepTime(){return m_dwSleepTime;}
    void SetSleepTime(DWORD dwSleep){m_dwSleepTime = dwSleep;}

    void ThreadCallFunc();
    static unsigned __stdcall ThreadFunc(void*  pVoid);
    //---------------

    void UpdateInput();

protected:
    CComboBox m_cbDevice;
    CListCtrl m_lcAi;
    CComboBox m_cbAoRange;
    CStatic m_stAo[4];
    CEdit m_edAo[4];
    CEdit m_edCnt[2];
    CComboBox m_cbAoMode[4];

    short m_sDevId;

    //---------------
    // スレッド
    //---------------
    HANDLE m_hThread;
    unsigned  m_dwThreadId;
    bool   m_bPause;
    bool   m_bStop;
    bool   m_bExec;
    DWORD   m_dwSleepTime;
    DWORD   m_dwBeforeTime;
    DWORD   m_dwUpdateDlgTime;


public:
    afx_msg void OnTimer(UINT_PTR nIDEvent);
    afx_msg void OnDestroy();
    afx_msg void OnCbnSelchangeCbAiRange();
    afx_msg void OnBnClickedPbSet1();
};
