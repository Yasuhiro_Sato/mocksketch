b2World@	gWorld;
float g_timeStep
int32 g_velocityIterations;
int32 g_positionIterations;
array<b2PolygonShape> g_ListShape;
array<b2PolygonShape> g_ListShape;

-------------------------------------------------------
b2BodyDef bDef;
bDef.type = b2_dynamicBody;
bDef.position = b2Vec2(0, 0);
b2Body *body = world_->CreateBody(&bDef);

b2PolygonShape shape;
const float32 density = 10;

shape.SetAsBox(1, 0.1);
body->CreateFixture(&shape, density);

shape.SetAsBox(0.1, 1, b2Vec2(-1 + 0.1, 1), 0);
body->CreateFixture(&shape, density);

shape.SetAsBox(0.1, 1, b2Vec2(1 - 0.1, 1), 0);
body->CreateFixture(&shape, density);
----------------------------------------------------

shape.SetAsBox(10.0/(PTM_RATIO * CC_CONTENT_SCALE_FACTOR()), 10.0/(PTM_RATIO * CC_CONTENT_SCALE_FACTOR()));
body->CreateFixture(&shape);

b2PolygonShape

// 実行時最初に呼び出される
int Setup()
{
	DrawingGroup@ group;
	@group = GetDrawingGroup();
	/*
	DrawingObject@ obj;
	obj = group.GetObject("Text011");
	DrawingText@ txt;
	@txt = cast<DrawingText@>(obj);
	txt.SetText("OOps");
	*/
	b2Vec2 gravity;
	gravity = group.GetUserProperty("Gravity").ToPOINT2D();
	g_timeStep           = group.GetUserProperty("TimeStep").ToDouble();
	g_velocityIterations = group.GetUserProperty("VelocityIterations").ToInt();
	g_positionIterations = group.GetUserProperty("PositionIterations").ToInt();

	b2World world(gravity);
	gWorld  = @world;


	return 0;
}
// 終了時に呼び出される
int Abort()
{
	return 0;
}
// 終了時に呼び出される
int Loop()
{
	gWorld.Step(g_timeStep, g_velocityIterations, g_positionIterations);

	return 0;
}
int Draw(DrawingView@ view, int &in iMouseOver)
{
	Print("Draw\n");
	return 0;
}
//	図形登録時に呼び出される
int Initialize()
{
	Print("Initialize\n");
	return 0;
}
// プロパティ変更時に呼び出される
int OnEdit_ChangeProperty(StdString &in strPropertyName)
{
	StdString strOut;
	strOut = "OnChangeProperty:";
	strOut += strPropertyName;
	strOut += "\n";
	Print(strOut);
	return 0;
}
int OnEdit_InitNodeMarker(NodeMarker@ marker)
{
	Print("OnInitNodeMarker\n");
	return 0;
}
int OnEdit_SelectNodeMarker(NodeMarker@ marker, StdString &in strMarker)
{
	StdString strOut;
	strOut = "OnSelectNodeMarker:";
	strOut += strMarker;
	strOut += "\n";
	Print(strOut);
	return 0;
}
int OnEdit_MoveNodeMarker(NodeMarker@ marker, StdString &in strMarker, POINT2D &in ptSel, MOUSE_MOVE_POS &in mousePos)
{
	POINT2D pt;
	pt = ptSel;
	StdString strOut;
	strOut = "OnMoveNodeMarker:";
	strOut += strMarker;
	strOut += " (";
	strOut += ptSel.x;
	strOut += ",";
	strOut += ptSel.y;
	strOut += ")\n";
	Print(strOut);
	return 0;
}
int OnEdit_ReleaseNodeMarker(NodeMarker@ marker)
{
	Print("OnReleaseNodeMarker\n");
	return 0;
}