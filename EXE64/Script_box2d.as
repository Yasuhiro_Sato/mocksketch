
b2Body@ 	gGroundBody;
b2World@	gWorld;
b2PolygonShape gGroundBox;
b2Body@ 	g_DynamicBody;
b2PolygonShape g_DynamicBox;
float g_timeStep;
int32 g_velocityIterations;
int32 g_positionIterations;
// 実行時最初に呼び出される
int Setup()
{
	b2Vec2 gravity(0.0, -10.0);
	b2World world(gravity);
	@gWorld = @world;
	
	b2BodyDef groundBodyDef;
	groundBodyDef.position.Set(0.0, -10.0);
	gGroundBox.SetAsBox(50.0, 10.0);
	
	@gGroundBody = gWorld.CreateBody(groundBodyDef);
	gGroundBox.SetAsBox(50.0, 10.0);
	
	gGroundBody.CreateFixture(@gGroundBox, 0.0);
  //-----------------------------------------
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	bodyDef.position.Set(0.0, 4.0);
	
	@g_DynamicBody = gWorld.CreateBody(bodyDef);
	g_DynamicBox.SetAsBox(1.0, 1.0);
	b2FixtureDef fixtureDef;
	@fixtureDef.shape = @g_DynamicBox;
	fixtureDef.density = 1.0;
	fixtureDef.friction = 0.3;
	g_DynamicBody.CreateFixture(fixtureDef);
	g_timeStep = 1.0 / 60.0;
	g_velocityIterations = 6;
	g_positionIterations = 2;
	return 0;
}
// 終了時に呼び出される
int Abort()
{
	return 0;
}
// 終了時に呼び出される
int Loop()
{
		StdString strOut;
		gWorld.Step(g_timeStep, g_velocityIterations, g_positionIterations);
		b2Vec2 position = g_DynamicBody.GetPosition();
		float angle = g_DynamicBody.GetAngle();	
		
		strOut = "Loop ";
		strOut +=  position.x;
		strOut += " :";
		strOut +=  position.y;
		strOut += " :";
		strOut +=  angle;
		strOut += "\n";
	  Print(strOut);
	return 0;
}
int Draw(DrawingView@ view, int &in iMouseOver)
{
	Print("Draw\n");
	return 0;
}
//	図形登録時に呼び出される
int Initialize()
{
	Print("Initialize\n");
	return 0;
}
// プロパティ変更時に呼び出される
int OnChangeProperty(StdString &in strPropertyName)
{
	Print("OnChangeProperty\n");
	return 0;
}
int OnInitDragableMarker(DragableMarker@ marker)
{
	Print("OnInitDragableMarker\n");
	return 0;
}
int OnSelectDragableMarker(DragableMarker@ marker, int &in iMarkerId)
{
	Print("OnSelectDragableMarker\n");
	return 0;
}
int OnMoveDragableMarker(DragableMarker@ marker, int &in iMarkerId, POINT2D &in ptSel)
{
	Print("OnMoveDragableMarker\n");
	return 0;
}
int OnReleaseDragableMarker(DragableMarker@ marker)
{
	Print("OnReleaseDragableMarker\n");
	return 0;
}