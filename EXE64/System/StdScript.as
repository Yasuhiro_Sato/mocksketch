
class ExecClass
{
	ExecClass()
	{
		Printf("ExecClass\n");
	}

	int Setup(TCB@ tcb)
	{
		return 0;
  }


	int Loop(TCB@ tcb)
	{
		return 0;
  }

	int Draw(DrawingView@ view, int &in iMouseOver)
	{
		Print("Draw\n");
		return 0;
	}

	// プロパティ変更時に呼び出される
	int OnEdit_ChangeProperty(StdString &in strPropertyName)
	{
		Printf("OnChangeProperty: %s \n", strPropertyName);
		return 0;
	}

	int OnEdit_InitNodeMarker(NodeMarker@ marker)
	{
		Printf("OnInitNodeMarker\n");
		return 0;
	}

	int OnEdit_SelectNodeMarker(NodeMarker@ marker, StdString &in strMarker)
	{
		Printf("OnSelectNodeMarker: %s \n", strMarker);
		return 0;
	}

	int OnEdit_MoveNodeMarker(NodeMarker@ marker, StdString &in strMarker, POINT2D &in ptSel, MOUSE_MOVE_POS &in mousePos)
	{
		POINT2D pt;
		pt = ptSel;
		Printf("OnMoveNodeMarker:%s (%f,%f) \n", strMarker, ptSel.y, ptSel.y);
		return 0;
	}

	int OnEdit_ReleaseNodeMarker(NodeMarker@ marker)
	{
		Printf("OnReleaseNodeMarker\n");
		return 0;
	}

}
