
class  ExecClass
{
	//	図形登録時に呼び出される
	ExecClass()
	{
		Print("Initialize\n");
	}
	// 実行時最初に呼び出される
	int Setup(EBD@ ebd)
	{
		/*
		DrawingGroup@ group;
		@group = GetDrawingGroup();
		DrawingObject@ obj;
		obj = group.GetObject("Text011");
		DrawingText@ txt;
		@txt = cast<DrawingText@>(obj);
		txt.SetText("OOps");
		*/
		return 0;
	}
	// 終了時に呼び出される
	int Abort(EBD@ ebd)
	{
		return 0;
	}
	// 終了時に呼び出される
	int Loop(EBD@ ebd)
	{
		return 0;
	}
	int Draw(DrawingView@ view)
	{
		Print("Draw\n");
		return 0;
	}
	// プロパティ変更時に呼び出される
	int OnEdit_ChangeProperty(StdString &in strPropertyName)
	{
		StdString strOut;
		strOut = "OnChangeProperty:";
		strOut += strPropertyName;
		strOut += "\n";
		Print(strOut);
		return 0;
	}
	int OnEdit_InitNodeMarker(NodeMarker@ marker)
	{
		Print("OnInitNodeMarker\n");
		return 0;
	}
	int OnEdit_SelectNodeMarker(NodeMarker@ marker, StdString &in strMarker)
	{
		StdString strOut;
		strOut = "OnSelectNodeMarker:";
		strOut += strMarker;
		strOut += "\n";
		Print(strOut);
		return 0;
	}
	int OnEdit_MoveNodeMarker(NodeMarker@ marker, StdString &in strMarker, POINT2D &in ptSel, MOUSE_MOVE_POS &in mousePos)
	{
		POINT2D pt;
		pt = ptSel;
		StdString strOut;
		strOut = "OnMoveNodeMarker:";
		strOut += strMarker;
		strOut += " (";
		strOut += ptSel.x;
		strOut += ",";
		strOut += ptSel.y;
		strOut += ")\n";
		Print(strOut);
		return 0;
	}
	int OnEdit_ReleaseNodeMarker(NodeMarker@ marker)
	{
		Print("OnReleaseNodeMarker\n");
		return 0;
	}
}