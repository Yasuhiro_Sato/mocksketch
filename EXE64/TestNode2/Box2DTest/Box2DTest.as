// 実行時最初に呼び出される
void PrintVec(b2Vec2 vec)
{
	StdString strOut;
		strOut = "Point ";
		strOut +=  vec.x;
		strOut += " ,";
		strOut +=  vec.y;
		strOut += "\n";
	Print(strOut);
}

void PrintPOINT2D(POINT2D vec)
{
	StdString strOut;
		strOut = "Point ";
		strOut +=  vec.x;
		strOut += " ,";
		strOut +=  vec.y;
		strOut += "\n";
	Print(strOut);
}


POINT2D Vec2ToPoint(b2Vec2& in vec)
{
	POINT2D ptRet;
	ptRet.x = vec.x;
	ptRet.y = vec.y;
	return ptRet;
} 

void PrintJoint(POINT2D vec)
{


}


void PrintMatrix(MAT2D mat)
{
	StdString strOut;
		strOut = "MAT2D [[";
		strOut +=  mat.Get(0,0);
		strOut += " ,";
		strOut +=  mat.Get(0,1);
		strOut += " ,";
		strOut +=  mat.Get(0,2);
		strOut += "],[";
		strOut +=  mat.Get(1,0);
		strOut += " ,";
		strOut +=  mat.Get(1,1);
		strOut += " ,";
		strOut +=  mat.Get(1,2);
		strOut += "],[";
		strOut +=  mat.Get(2,0);
		strOut += " ,";
		strOut +=  mat.Get(2,1);
		strOut += " ,";
		strOut +=  mat.Get(2,2);
		strOut += "]]";
	Print(strOut);
}


class  ExecClass
{
	b2World 									m_world;
	b2JointPtr_VECTOR 				m_lstJoint;
	b2BodyPtr_VECTOR 					m_lstBody;

	//	コンストラクタ
	ExecClass()
	{
		Print("Box2DTest ExecClass\n");
	}

	void SetupDistanceJoint(EBD@ ebd, DrawingField@ field)
	{
		int itemNum;
		DrawingObjectPtr_VECTOR lstObj;
			//距離ジョイント
		DrawingParts@ objParts;
		field.GetObjectList(lstObj, "Box2D.b2DistanceJointDef", "", DT_PARTS);
		itemNum = lstObj.size();
	
		for (int iNo = 0; iNo < itemNum; iNo++)
		{
			@objParts = cast<DrawingParts@>(lstObj[iNo]);
	
			StdString strBodyA;
			StdString strBodyB;
			DrawingObject@ objA;
			DrawingObject@ objB;
			b2Body@        bodyA;
			b2Body@        bodyB;
			strBodyA = objParts.GetPropertySetVal("bodyA").ToString();
			strBodyB = objParts.GetPropertySetVal("bodyB").ToString();
	
			@objA = field.GetFieldObject(strBodyA);
			@objB = field.GetFieldObject(strBodyB);
	
			if (strBodyA == strBodyB)
			{
				continue;
			}
	
			objA.GetUserData(@bodyA);
			objB.GetUserData(@bodyB);

			DrawingNode@ NodeA;
			DrawingNode@ NodeB;
			@NodeA = objParts.GetObject("NodeA").AsNode();
			@NodeB = objParts.GetObject("NodeB").AsNode();
	
			POINT2D ptA;
			POINT2D ptB;
	
			ptA = NodeA.GetPoint();
			ptB = NodeB.GetPoint();
	
	
		// BOX2Dによる座標変換
		
			b2Vec2 vecNodeA;	
			b2Vec2 vecNodeB;	
		  b2Vec2 vecLocalA;
			b2Vec2 vecLocalB;
	
			vecNodeA.x =  ptA.x;
			vecNodeA.y =  ptA.y;
	
			vecNodeB.x =  ptB.x;
			vecNodeB.y =  ptB.y;
	
			vecLocalA = bodyA.GetLocalPoint(vecNodeA);
			vecLocalB = bodyB.GetLocalPoint(vecNodeB);	
	
			//POINT2D ptLocalA;
			//POINT2D ptLocalB;
			//ptLocalA = objA.ConvWorldToLocal(ptA);
			//ptLocalB = objB.ConvWorldToLocal(ptB);
	
			b2DistanceJointDef jointDef;
			jointDef.type = e_distanceJoint;
			jointDef.collideConnected = objParts.GetPropertySetVal("collideConnected").ToBool();
			jointDef.length           = objParts.GetPropertySetVal("length").ToDouble();
			jointDef.frequencyHz      = objParts.GetPropertySetVal("frequencyHz").ToDouble();
			jointDef.dampingRatio     = objParts.GetPropertySetVal("dampingRatio").ToDouble();
			jointDef.localAnchorA     = vecLocalA;
			jointDef.localAnchorB     = vecLocalB;
			@jointDef.bodyA            = @bodyA;
			@jointDef.bodyB            = @bodyB;
	
			b2Joint@ joint = m_world.CreateJoint2(jointDef);
	
			joint.TestDump();
	
//----------------------------------------
			StdString strOut;
			b2Vec2 vecA;
			b2Vec2 vecB;

			vecA = joint.GetAnchorA();
			vecB = joint.GetAnchorB();

			POINT2D ptVecA = Vec2ToPoint(vecA);
			POINT2D ptVecB = Vec2ToPoint(vecB);
			Print("SetupDistanceJoint\n");

			strOut = _T("ptA = ") + strBodyA + _T(" ") + ptA  + _T(" ") + ptVecA ;
			Print(strOut);
	
			strOut = _T("ptB = ") + strBodyB + _T(" ") + ptB  + _T(" ") + ptVecB; 
			Print(strOut);

//----------------------------------------
			joint.SetUserData(@objParts);
			m_lstJoint.push_back(joint);
			objParts.SetUserData(@joint);
		}
	}
	


	void SetupRevolutionJoint(EBD@ ebd, DrawingField@ field)
	{
		DrawingObjectPtr_VECTOR 	lstObj;
		int itemNum;
	
			//回転ジョイント
		DrawingParts@ objParts;
		field.GetObjectList(lstObj, "Box2D.b2RevoluteJointDef", "", DT_PARTS);
		itemNum = lstObj.size();
	
		for (int iNo = 0; iNo < itemNum; iNo++)
		{
			@objParts = cast<DrawingParts@>(lstObj[iNo]);
	
			StdString strBodyA;
			StdString strBodyB;
			DrawingObject@ objA;
			DrawingObject@ objB;
			b2Body@        bodyA;
			b2Body@        bodyB;
			strBodyA = objParts.GetPropertySetVal("bodyA").ToString();
			strBodyB = objParts.GetPropertySetVal("bodyB").ToString();
	
			if (strBodyA == strBodyB)
			{
				continue;
			}
	
			@objA = field.GetFieldObject(strBodyA);
			@objB = field.GetFieldObject(strBodyB);
	
	
			objA.GetUserData(@bodyA);
			objB.GetUserData(@bodyB);
	
			//==========================
			StdString strNameA;
			StdString strNameB;
	
			if (bodyA is null)
			{
				strNameA = strBodyA + _T(" is Null \n"); 
				Print(strNameA);
				continue;
			}
	
			if (bodyB is null)
			{
				strNameB = strBodyB + _T(" is Null \n"); 
				Print(strNameB);
				continue;
			}
			//==========================
	
			DrawingNode@ Node;
			@Node = objParts.GetObject("Node").AsNode();
	
			POINT2D pt;
			POINT2D ptNode;
	
			pt = Node.GetPoint();
			ptNode = objParts.ConvLocalToWorld(pt);
	
			POINT2D ptLocalA;
			POINT2D ptLocalB;
	
			ptLocalA = objA.ConvWorldToLocal(ptNode);
			ptLocalB = objB.ConvWorldToLocal(ptNode);
	
	
	
			StdString strOut;
			strOut = _T("Pt pin = ") + pt;
			Print(strOut);
	
			strOut = _T("ptNode = ") + ptNode;
			Print(strOut);
	
			strOut = _T("ptLocalA = ") + strBodyA + _T(" ") + ptLocalA;
			Print(strOut);
	
			strOut = _T("ptLocalB = ") + strBodyB + _T(" ") + ptLocalB;
			Print(strOut);
	
			// BOX2Dによる座標変換
		
			b2Vec2 vecNode;	
			b2Vec2 vecLocalA;
			b2Vec2 vecLocalB;
	
			vecNode.x =  ptNode.x;
			vecNode.y =  ptNode.y;
	
			vecLocalA = bodyA.GetLocalPoint(vecNode);
			vecLocalB = bodyB.GetLocalPoint(vecNode);
	
	
			strOut = _T("vecLocalA = ") + strBodyA + _T(" ") + Vec2ToPoint(vecLocalA);
			Print(strOut);
	
			strOut = _T("vecLocalB = ") + strBodyB + _T(" ") + Vec2ToPoint(vecLocalB);
			Print(strOut);
	
	


	
			b2RevoluteJointDef jointDef;
			//----------------Joint共通-------------------------
			jointDef.type = e_revoluteJoint;
			jointDef.collideConnected = objParts.GetPropertySetVal("collideConnected").ToBool();
			@jointDef.bodyA            = @bodyA;
			@jointDef.bodyB            = @bodyB;
			//------------------------------------------------
	
			jointDef.referenceAngle     = bodyA.GetAngle() - bodyB.GetAngle();
			jointDef.enableLimit        = objParts.GetPropertySetVal("enableLimit").ToBool();
			jointDef.lowerAngle         = objParts.GetPropertySetVal("lowerAngle").ToDouble();
			jointDef.upperAngle         = objParts.GetPropertySetVal("upperAngle").ToDouble();
			jointDef.enableMotor        = objParts.GetPropertySetVal("enableMotor").ToBool();
			jointDef.motorSpeed         = objParts.GetPropertySetVal("motorSpeed").ToDouble();
			jointDef.maxMotorTorque     = objParts.GetPropertySetVal("maxMotorTorque").ToDouble();
	
			jointDef.localAnchorA     = vecLocalA;
			jointDef.localAnchorB     = vecLocalB;
	
	
			b2Joint@ joint = m_world.CreateJoint2(jointDef);
	

//----------------------------------------
			b2Vec2 vecA;
			b2Vec2 vecB;

			vecA = joint.GetAnchorA();
			vecB = joint.GetAnchorB();

			POINT2D ptVecA = Vec2ToPoint(vecA);
			POINT2D ptVecB = Vec2ToPoint(vecB);
			Print("SetupRevolutionJoint\n");

			strOut = _T("ptA = ") + strBodyA + _T(" ") + pt  + _T(" ") + ptVecA ;
			Print(strOut);
	
			strOut = _T("ptB = ") + strBodyB + _T(" ") + pt  + _T(" ") + ptVecB; 
			Print(strOut);

//----------------------------------------

			joint.TestDump();
	
			joint.SetUserData(@objParts);
			m_lstJoint.push_back(joint);
			objParts.SetUserData(@joint);
		}
	}

	void SetupBody(EBD@ ebd, DrawingField@ field)
	{
		DrawingObjectPtr_VECTOR 	lstObj;
		
		field.GetObjectList(lstObj, "Box2D.b2BodyDef", "", DT_COMPOSITION_LINE);
		field.DispObjectListTest(lstObj);
		int itemNum;
		itemNum = lstObj.size();
	
	
		DrawingCompositionLine@ objCompo;
		DrawingObject@ obj;
		StdString strType;
		strType = "Num: " + itemNum + "\n";
		Print (strType);
	
		for (int iNo = 0; iNo < itemNum; iNo++)
		{
			@objCompo = cast<DrawingCompositionLine@>(lstObj[iNo]);
	
			field.PrintObject( lstObj[iNo]);
			field.PrintObject2(objCompo);
	
			strType = objCompo.GetPropertySetVal("type").ToString();
			b2BodyDef bodyDef;
	
			if (strType == _T("b2_staticBody"))
			{
				bodyDef.type = b2_staticBody;
			}
			else if (strType == _T("b2_kinematicBody"))
			{
				bodyDef.type = b2_kinematicBody;
			}
			else if (strType == _T("b2_dynamicBody"))
			{
				bodyDef.type = b2_dynamicBody;
			}
	
			/*
			bodyDef.userData = NULL;
			bodyDef.position.Set(0.0f, 0.0f);
			bodyDef.angle = 0.0f;
			bodyDef.linearVelocity.Set(0.0f, 0.0f);
			bodyDef.angularVelocity = 0.0f;
			*/
	
			bodyDef.linearDamping  = objCompo.GetPropertySetVal("linearDamping").ToDouble();
			bodyDef.angularDamping = objCompo.GetPropertySetVal("angularDamping").ToDouble();
			bodyDef.allowSleep     = objCompo.GetPropertySetVal("allowSleep").ToBool();
			bodyDef.awake          = objCompo.GetPropertySetVal("awake").ToBool();
			bodyDef.fixedRotation  = objCompo.GetPropertySetVal("fixedRotation").ToBool();
			bodyDef.bullet         = objCompo.GetPropertySetVal("bullet").ToBool();
			bodyDef.active         = objCompo.GetPropertySetVal("active").ToBool();
			bodyDef.gravityScale   = objCompo.GetPropertySetVal("gravityScale").ToDouble();
	
			//===================================
			POINT2D_VECTOR pt;
			pt.resize(3);
			
			b2Vec2_VECTOR vertices;
			vertices.resize(3);
	
			b2Body@ body = m_world.CreateBody(bodyDef);
			POINT2D pt1;
			POINT2D pt2;
			POINT2D pt3;
	
			//ポリゴン生成
			objCompo.CreatePolygon();
			int objNum = objCompo.GetNumberOfPolygon();
			MAT2D mat;
			b2PolygonShape triangle;
			for (int iPol = 0; iPol < objNum; iPol++)
			{
				if(!objCompo.GetPolygonVertex(iPol, pt[0], pt[1], pt[2]))
				{
					continue;
				}

				mat = objCompo.GetDrawingMatrix();
				Print(objCompo.GetName());
				PrintMatrix(mat);

			     pt[0].Matrix(mat);
			     pt[1].Matrix(mat);
			     pt[2].Matrix(mat);

				vertices[0].x = pt[0].x;
				vertices[0].y = pt[0].y;
				vertices[1].x = pt[1].x;
				vertices[1].y = pt[1].y;
				vertices[2].x = pt[2].x;
				vertices[2].y = pt[2].y;
				triangle.Set(vertices);
				body.CreateFixture(triangle, 2.0f);
			}
	
			b2MassData mass;
			b2Vec2  vecCenter;
			vecCenter = body.GetLocalCenter();
	
			b2MassData  massData;
	
	
	
			POINT2D ptCenter = Vec2ToPoint(vecCenter);
			objCompo.MoveInnerPosition(-ptCenter);
			objCompo.MoveAbs(ptCenter);
			body.SetUserData(@objCompo);
			m_lstBody.push_back(body);
			objCompo.SetUserData(@body);
	
			//----------------------------
			StdString strCenter;
			strCenter = _T("Center[") + objCompo.GetName() + _T("] ")+ ptCenter;
			Print(strCenter);
	
			//----------------------------
	
		}
	}
	
	int Setup(EBD@ ebd)
	{
		Print("Setup Box2DTest\n");

		b2Vec2 gravity(0.0, -9.8);
		
		m_world.SetGravity(gravity);

		//b2World@ tmpWorld;
		//@tmpWorld = @world
		DrawingField@ field;
		@field = GetDrawingField();
	
	
		//==========================================================
		//==========================================================
	
	
		SetupBody( ebd, field );
		SetupDistanceJoint( ebd, field );
		SetupRevolutionJoint( ebd, field );
	
		return 0;
	
	
	
	}
	// 終了時に呼び出される
	int Abort(EBD@ ebd)
	{
		return 0;
	}
	// 
	int Loop(EBD@ ebd)
	{
		b2World@ world;
	

		m_world.Step(0.02, 6, 2);
	
		//
		ebd.iCounter1++;
		//if( (ebd.iCounter1 % 10) != 0)
		//{
		//	return 0;
		//}
	
	
		
		b2Vec2 position;
		b2Vec2 posCenter;
		POINT2D posObj;
		float angle;
		int itemNum;
		
		itemNum = m_lstBody.size();
	
		DrawingCompositionLine@ obj;
		b2Body@ body;
	
		//StdString strType = "Num: " + itemNum + "\n";
		//Print (strType);
		DrawingCompositionLine@ objCompo;;
		for (int iNo = 0; iNo < itemNum; iNo++)
		{
			@body = m_lstBody[iNo];
			body.GetUserData(@objCompo);
			angle = body.GetAngle();
			position = body.GetPosition();
			posCenter = body.GetLocalCenter();
			double dC = cos(angle);
			double dS = sin(angle);
	
			posObj.x = (dC * posCenter.x  - dS *  posCenter.y) + position.x;
			posObj.y = (dS * posCenter.x  + dC *  posCenter.y) + position.y;
			objCompo.MoveAbs(posObj);
			objCompo.SetAngle(angle * 180.0 / 3.14159);	
			
			//Print ("No."+ iNo + "Pos(" + posObj.x + "," + posObj.y + "): angle "+  angle +"\n");	
		}
	
		itemNum = m_lstJoint.size();
		b2Joint@ joint;
		POINT2D ptA;
		POINT2D ptB;
		POINT2D ptALocal;
		POINT2D ptBLocal;
		b2Vec2  vecA;
		b2Vec2  vecB;
	
		DrawingNode@ NodeA;
		DrawingNode@ NodeB;
	
		DrawingParts@ objParts;

StdString strOut;	

		for (int iNo = 0; iNo < itemNum; iNo++)
		{
			@joint = m_lstJoint[iNo];
	
			if (joint is null)
			{
				continue;
			}
	
			joint.GetUserData(@objParts);
	
			if (objParts is null)
			{
				continue;
			}


			vecA = joint.GetAnchorA();
			ptA.x = vecA.x;
			ptA.y = vecA.y;
			ptALocal = objParts.ConvWorldToLocal(ptA);

			
			if (joint.GetType() ==  e_distanceJoint)
			{
				vecB = joint.GetAnchorB();
				ptB.x = vecB.x;
				ptB.y = vecB.y;
				ptBLocal = objParts.ConvWorldToLocal(ptB);
				@NodeA = objParts.GetObject("NodeA").AsNode();
				@NodeB = objParts.GetObject("NodeB").AsNode();
				NodeA.SetPoint(ptALocal);
				NodeB.SetPoint(ptBLocal);
//strOut = _T("e_distanceJoint Node ") + ptA + _T(",Node B ") + ptB;
//Print(strOut);
			}
			else if (joint.GetType() ==  e_revoluteJoint)
			{
				@NodeA = objParts.GetObject("Node").AsNode();
				NodeA.SetPoint(ptALocal);

//strOut = _T("e_revoluteJoint Node ") + ptA ;
//Print(strOut);
			}

//Print("\n");
		}
		return 0;
	}
	
	int Draw(DrawingView@ view)
	{
		return 0;
	}

	// プロパティ変更時に呼び出される
	int OnEdit_ChangeProperty(StdString &in strPropertyName)
	{
		StdString strOut;
		strOut = "OnChangeProperty:";
		strOut += strPropertyName;
		strOut += "\n";
		Print(strOut);
		return 0;
	}


	int OnEdit_InitNodeMarker(NodeMarker@ marker)
	{
		Print("OnInitNodeMarker\n");
		return 0;
	}


	int OnEdit_SelectNodeMarker(NodeMarker@ marker, StdString &in strMarker)
	{
		StdString strOut;
		strOut = "OnSelectNodeMarker:";
		strOut += strMarker;
		strOut += "\n";
		Print(strOut);
		return 0;
	}


	int OnEdit_MoveNodeMarker(NodeMarker@ marker, StdString &in strMarker, POINT2D &in ptSel, MOUSE_MOVE_POS &in mousePos)
	{
		POINT2D pt;
		pt = ptSel;
		StdString strOut;
		strOut = "OnMoveNodeMarker:";
		strOut += strMarker;
		strOut += " (";
		strOut += ptSel.x;
		strOut += ",";
		strOut += ptSel.y;
		strOut += ")\n";
		Print(strOut);
		return 0;
	}
	int OnEdit_ReleaseNodeMarker(NodeMarker@ marker)
	{
		return 0;
	}
}