DrawingObjectPtr_VECTOR 	g_lstTest;

void PrintVec(b2Vec2 vec)
{
	StdString strOut;
		strOut = "Point ";
		strOut +=  vec.x;
		strOut += " ,";
		strOut +=  vec.y;
		strOut += "\n";
	Print(strOut);
}

void PrintPOINT2D(POINT2D vec)
{
	StdString strOut;
		strOut = "Point ";
		strOut +=  vec.x;
		strOut += " ,";
		strOut +=  vec.y;
		strOut += "\n";
	Print(strOut);
}
class  ExecClass
{
	
	void Test1()
	{
		DrawingField@ field;
		@field = GetDrawingField();
	
		POINT2D_VECTOR vec1;
		POINT2D_VECTOR vec2;
		POINT2D pt1;
	
		pt1.x = 1.1;
		pt1.y = 1.2;
	
		vec1.push_back(pt1);
	
		pt1.x = 2.1;
		pt1.y = 2.2;
	
		vec1.push_back(pt1);
	
		pt1.x = 3.1;
		pt1.y = 3.2;
	
		vec1.push_back(pt1);
	
		field.SetPointList(vec1);
		field.GetPointList(vec2);
	
		POINT2D pt3;
		pt3 = vec2[0];
		PrintPOINT2D(pt3);
	
	
		int iNum = vec2.size();
	
		for (int iNo = 0; iNo < iNum; iNo++)
		{
			StdString strOut;
				strOut = "Point:";
				strOut +=  iNo;
				strOut += " ,";
				strOut +=  vec2[iNo].x;
				strOut += " ,";
				strOut +=  vec2[iNo].y;
				strOut += "\n";
		}
	}
	
	
	void Test1_1()
	{
		DrawingField@ field;
		@field = GetDrawingField();
	
		// DrawingObjectは参照型オブジェクト
	  // ただしコンストラクターはない
		DrawingObject@ obj;
		@obj = field.CreateObject(DT_LINE, "Test2"); 
	
		Print("Test1_1\n");
		field.AnalysisData( @obj );
		field.AnalysisData( obj );
		field.PrintObject( obj );
	
	
		HOBJ hObj;
		@hObj = @obj;
	
		HOBJ hObj2;
		@hObj2 = obj;
	
		field.AnalysisData( hObj );
		field.AnalysisData( hObj2 );
		Print("Test1_1 POINT2D \n");
	
		//POINT2Dは参照型オブジェクト
		POINT2D pt2D;
		pt2D.x = 12.34;
		pt2D.y = 34.56;
	
		//POINT2D@ p_pt2D;
		////@p_pt2D = @pt2D;
	
		field.AnalysisData( pt2D );
		field.PrintPoint2D(pt2D);
		field.PrintPoint2D(pt2D);
	
	
		Print("Test1_1 End\n");
	
	}
	
	
	void Test2()
	{
		DrawingField@ field;
		@field = GetDrawingField();
	
		field.CreateObjectListTest(g_lstTest);
		field.SetObjectListTest(g_lstTest);
	
	
	
	
	}
	
	
	void Test3()
	{
		DrawingObjectPtr_VECTOR  lstTest;
		DrawingField@ field;
		@field = GetDrawingField();
	
		field.GetObjectListTest(lstTest);
		field.DispObjectListTest(lstTest);
	
		DrawingObject@ obj;
		@obj = lstTest[0];
		//@obj = lstTest.GetRef(0);
		POINT pt1;
		pt1.x = 10;
		pt1.y = 20;
	
		POINT2D pt2;
		pt2.x = 12.3;
		pt2.y = 23.4;
	
		field.AnalysisData( pt1 );
		field.AnalysisData( pt2 );
	
		//field.AnalysisData( @pt1 );
		//field.AnalysisData( @pt2 );
		StdString str = obj.GetName();
		HOBJ hObj;
		@hObj = @obj;
		@hObj = obj;
		field.AnalysisData( hObj );
		field.AnalysisData( @obj );
		field.AnalysisData( obj );
	
		field.PrintObject( obj );
		field.PrintObject_in( obj );
		//field.PrintObject( @obj );
		//field.PrintObject_in( @obj );
	
	}
	
	void Test4()
	{
		DrawingField@ field;
		@field = GetDrawingField();
	
		POINT2D pt1;
		POINT2D pt2;
		pt1.x = 10.1;
		pt1.y = 12.2;
	
		//void SetData(?& in)
		field.SetData(pt1);
	
	
		//GetData(?& out)
		field.GetData(pt2);
	
		Assert(pt1 == pt2);
	
		//POINT2D@ pt4Ptr;
		//field.GetData(@pt4Ptr); これはコンパイルエラー
		
	
		//void SetData(?@ in) この定義は不可
		//void SetData(?@@ out) この定義は不可
	
		DrawingObjectPtr_VECTOR  lstTest;
	
		DrawingObject@ obj;
		@obj = field.CreateObject(DT_LINE, "Test4"); 
	
		lstTest.push_back(obj);
	
		@obj = field.CreateObject(DT_POINT, "Test4_1"); 
	
		lstTest.push_back(obj);
	
	
		DrawingObject@ obj2;
		
	
		@obj2 = lstTest[0];
	
		Assert( obj2.GetName() == "Test4");
	
	}
	
	void Test5(EBD@ ebd)
	{
		int a = 10;
		double d = 123.45;
		DrawingObjectPtr_VECTOR 	lstTest;
	
	
		ebd.SetValue("a", a);
		ebd.SetValue("d", d);
		ebd.SetValue("lstTest", lstTest);
	
	}
	
	
	void Test6(EBD@ ebd)
	{
		int a;
		double d;
		DrawingObjectPtr_VECTOR pList;
	
		ebd.GetValue("a", a);
		ebd.GetValue("d", d);
		ebd.GetValue("lstTest", pList);
	
		Assert( a == 10 );
		Assert( d == 123.45 );
	
	
	
	}
	
	// 実行時最初に呼び出される
	int Setup(EBD@ ebd)
	{
	
		Test1();
		Test1_1();
		Test2();
		Test3();
		Test4();
		Test5(ebd);
		Test6(ebd);
		return 0;
	}
	// 終了時に呼び出される
	int Abort(EBD@ ebd)
	{
		return 0;
	}
	// 終了時に呼び出される
	int Loop(EBD@ ebd)
	{
		return 0;
	}
	int Draw(DrawingView@ view)
	{
		Print("Draw\n");
		return 0;
	}
	//	図形登録時に呼び出される
	ExecClass()
	{
		Print("Initialize\n");
	}
	// プロパティ変更時に呼び出される
	int OnEdit_ChangeProperty(StdString &in strPropertyName)
	{
		StdString strOut;
		strOut = "OnChangeProperty:";
		strOut += strPropertyName;
		strOut += "\n";
		Print(strOut);
		return 0;
	}
	int OnEdit_InitNodeMarker(NodeMarker@ marker)
	{
		Print("OnInitNodeMarker\n");
		return 0;
	}
	int OnEdit_SelectNodeMarker(NodeMarker@ marker, StdString &in strMarker)
	{
		StdString strOut;
		strOut = "OnSelectNodeMarker:";
		strOut += strMarker;
		strOut += "\n";
		Print(strOut);
		return 0;
	}
	int OnEdit_MoveNodeMarker(NodeMarker@ marker, StdString &in strMarker, POINT2D &in ptSel, MOUSE_MOVE_POS &in mousePos)
	{
		POINT2D pt;
		pt = ptSel;
		StdString strOut;
		strOut = "OnMoveNodeMarker:";
		strOut += strMarker;
		strOut += " (";
		strOut += ptSel.x;
		strOut += ",";
		strOut += ptSel.y;
		strOut += ")\n";
		Print(strOut);
		return 0;
	}
	int OnEdit_ReleaseNodeMarker(NodeMarker@ marker)
	{
		Print("OnReleaseNodeMarker\n");
		return 0;
	}
}