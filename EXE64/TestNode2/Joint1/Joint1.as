class  ExecClass
{
	DrawingParts@ m_parts;
	DrawingNode@ m_NodeA;
	DrawingNode@ m_NodeB;
	DrawingLine@ m_Line;
	POINT2D m_ptAOld;
	POINT2D m_ptBOld;

	//	図形登録時に呼び出される
	ExecClass()
	{
		Print("Initialize Joint1\n");
		@m_parts = GetDrawingParts();
		@m_NodeA = m_parts.GetObject("NodeA").AsNode();
		@m_NodeB = m_parts.GetObject("NodeB").AsNode();
		@m_Line = m_parts.GetObject("Line").AsLine();
	}
	
	// 実行時最初に呼び出される
	int Setup(EBD@ ebd)
	{
		Print("Setup Joint1\n");
		return 0;
	}
	
	// 終了時に呼び出される
	int Loop(EBD@ ebd)
	{
		return 0;
	}
	
	// 終了時に呼び出される
	int Abort(EBD@ ebd)
	{
		return 0;
	}
	
	
	
	int Draw(DrawingView@ view)
	{
		POINT2D ptA = m_NodeA.GetPoint();
		POINT2D ptB = m_NodeB.GetPoint();

		if (( m_ptAOld == ptA) &&
            ( m_ptBOld == ptB))
		{
			return 0;
		}
		double dLen;
		dLen = ptA.Distance(ptB);
		m_parts.SetPropertySetVal(_T("length"), dLen);
		m_Line.SetPoint1(ptA);
		m_Line.SetPoint2(ptB);
		return 0;
	}

	// プロパティ変更時に呼び出される
	int OnEdit_ChangeProperty(StdString &in strPropertyName)
	{
		StdString strOut;
		strOut = "OnChangeProperty:";
		strOut += strPropertyName;
		strOut += "\n";
		Print(strOut);
		return 0;
	}

	int OnEdit_InitNodeMarker(NodeMarker@ marker)
	{
		Print("OnInitNodeMarker\n");
		return 0;
	}

	int OnEdit_SelectNodeMarker(NodeMarker@ marker, StdString &in strMarker)
	{
		StdString strOut;
		strOut = "OnSelectNodeMarker:";
		strOut += strMarker;
		strOut += "\n";
		Print(strOut);
		return 0;
	}

	int OnEdit_MoveNodeMarker(NodeMarker@ marker, StdString &in strMarker, POINT2D &in ptSel, MOUSE_MOVE_POS &in mousePos)
	{
		if (strMarker == "NodeA")
		{
			m_Line.SetPoint1(ptSel);
		}
		else if (strMarker == "NodeB")
		{
			m_Line.SetPoint2(ptSel);
		}
		return 0;
	}

	int OnEdit_ReleaseNodeMarker(NodeMarker@ marker)
	{

		StdString strListA;
		StdString strListB;
		strListA = m_NodeA.GetConnectabaleObjectList();
		strListB = m_NodeB.GetConnectabaleObjectList();
	
		m_parts.SetPropertySetDefMin(_T("bodyA"), strListA);
		m_parts.SetPropertySetDefMin(_T("bodyB"), strListB);
	
		double dLen;
		POINT2D ptA = m_NodeA.GetPoint();
		POINT2D ptB = m_NodeB.GetPoint();
	
		dLen = ptA.Distance(ptB);
		
		m_parts.SetPropertySetVal(_T("length"), dLen);
	
	
		StdString strConnectionObjectA;
		StdString strConnectionObjectB;
	
		strConnectionObjectA = m_NodeA.GetConnectionObjectName(0);
		strConnectionObjectB = m_NodeB.GetConnectionObjectName(0);
	
		m_parts.SetPropertySetVal(_T("bodyA"), strConnectionObjectA);
		m_parts.SetPropertySetVal(_T("bodyB"), strConnectionObjectB);
	
		return 0;
	}
	
	int OnEdit_ChangeNode(StdString &in strMarker,NODE_CHANGE_TYPE eType)
	{
		if (eType == NCT_CONNECTION)
	  	{
			StdString strConnectionObject;
			if (strMarker == "NodeA")
			{ 
				strConnectionObject = m_NodeA.GetConnectionObjectName(0);
				m_parts.SetPropertySetVal(_T("bodyA"), strConnectionObject);
			}
			else if (strMarker == "NodeB")
			{
				strConnectionObject = m_NodeB.GetConnectionObjectName(0);
				m_parts.SetPropertySetVal(_T("bodyB"), strConnectionObject);
			}
		}
		else if (eType == NCT_POSITION)
		{
			POINT2D ptA = m_NodeA.GetPoint();
			POINT2D ptB = m_NodeB.GetPoint();
	
		
			if (strMarker == "NodeA")
			{
				m_Line.SetPoint1(ptA);
			}
			else if (strMarker == "NodeB")
			{
				m_Line.SetPoint2(ptB);
			}
			double dLen = ptA.Distance(ptB);
			m_parts.SetPropertySetVal(_T("length"), dLen);
		}
		return 0;
	}
}