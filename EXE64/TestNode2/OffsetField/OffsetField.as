//OffsetField.Script
//	図形登録時に呼び出される
int Initialize(EBD@ ebd)
{
	Print("Initialize\n");
	return 0;
}
// 実行時最初に呼び出される
int Setup(EBD@ ebd)
{
	DrawingField@ field;
	@field = GetDrawingField();

	DrawingParts@ parts;
	DrawingCircle@ circle;

	DrawingParts@ parts1;
	DrawingCircle@ circle1;

	POINT2D pt(0.0, 20.0);

	@parts = field.GetFieldObject("Parts0003").AsParts();
	@circle = field.GetFieldObject("Circle_POS").AsCircle();

	@parts1 = field.GetFieldObject("Parts0002").AsParts();
	@circle1 = field.GetFieldObject("Circle_01").AsCircle();

	POINT2D ptWorld;
	POINT2D ptLocal;

	POINT2D ptWorld1;
	POINT2D ptLocal1;

	ptWorld = circle.GetCenter();
	ptLocal = parts.ConvWorldToLocal(ptWorld);

	ptWorld1 = circle1.GetCenter();
	ptLocal1 = parts1.ConvWorldToLocal(ptWorld1);

	POINT2D ptWorld2;
	POINT2D ptWorld3;

	ptWorld2 = parts.ConvLocalToWorld(pt);
	ptWorld3 = parts1.ConvLocalToWorld(pt);
                    


	return 0;
}
// 終了時に呼び出される
int Abort(EBD@ ebd)
{
	return 0;
}
// 終了時に呼び出される
int Loop(EBD@ ebd)
{
	return 0;
}
int Draw(DrawingView@ view)
{
	Print("Draw\n");
	return 0;
}
// プロパティ変更時に呼び出される
int OnEdit_ChangeProperty(StdString &in strPropertyName)
{
	StdString strOut;
	strOut = "OnChangeProperty:";
	strOut += strPropertyName;
	strOut += "\n";
	Print(strOut);
	return 0;
}
int OnEdit_InitNodeMarker(NodeMarker@ marker)
{
	Print("OnInitNodeMarker\n");
	return 0;
}
int OnEdit_SelectNodeMarker(NodeMarker@ marker, StdString &in strMarker)
{
	StdString strOut;
	strOut = "OnSelectNodeMarker:";
	strOut += strMarker;
	strOut += "\n";
	Print(strOut);
	return 0;
}
int OnEdit_MoveNodeMarker(NodeMarker@ marker, StdString &in strMarker, POINT2D &in ptSel, MOUSE_MOVE_POS &in mousePos)
{
	POINT2D pt;
	pt = ptSel;
	StdString strOut;
	strOut = "OnMoveNodeMarker:";
	strOut += strMarker;
	strOut += " (";
	strOut += ptSel.x;
	strOut += ",";
	strOut += ptSel.y;
	strOut += ")\n";
	Print(strOut);
	return 0;
}
int OnEdit_ReleaseNodeMarker(NodeMarker@ marker)
{
	Print("OnReleaseNodeMarker\n");
	return 0;
}