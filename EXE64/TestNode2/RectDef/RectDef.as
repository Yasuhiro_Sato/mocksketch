class  ExecClass
{
	//	図形登録時に呼び出される
	ExecClass()
	{
		Print("Initialize\n");
	// 実行時最初に呼び出される
	}
	int Setup(EBD@ ebd)
	{
		/*
		DrawingGroup@ parts;
		@parts = GetDrawingGroup();
		DrawingObject@ obj;
		obj = parts.GetObject("Text011");
		DrawingText@ txt;
		@txt = cast<DrawingText@>(obj);
		txt.SetText("OOps");
		*/
		return 0;
	}
	// 終了時に呼び出される
	int Abort(EBD@ ebd)
	{
		return 0;
	}
	// 終了時に呼び出される
	int Loop(EBD@ ebd)
	{
		return 0;
	}
	int Draw(DrawingView@ view)
	{
		Print("Draw\n");
		return 0;
	}
	
	void DispPos(StdString &in strName, POINT2D& in  pos)
	{
		StdString strOut;
		strOut = strName;
		strOut += ":(";
		strOut += pos.x;
		strOut += ",";
		strOut += pos.y;
		strOut += ")\n";
		Print(strOut);
	}
	
	// プロパティ変更時に呼び出される
	int OnEdit_ChangeProperty(StdString &in strPropertyName)
	{
		StdString strOut;
		strOut = "OnChangeProperty:";
		strOut += strPropertyName;
		strOut += "\n";
		Print(strOut);
		return 0;
	}
	int OnEdit_InitNodeMarker(NodeMarker@ marker)
	{
		return 0;
	}
	int OnEdit_SelectNodeMarker(NodeMarker@ marker, StdString &in strMarker)
	{
		StdString strOut;
		strOut = "OnSelectNodeMarker:";
		strOut += strMarker;
		strOut += "\n";
		Print(strOut);
		return 0;
	}
	int OnEdit_MoveNodeMarker(NodeMarker@ marker, StdString &in strMarker, POINT2D &in ptSel, MOUSE_MOVE_POS& in mousePos)
	{
		DrawingParts@ parts;
	
		@parts = GetDrawingParts();
		
		DrawingLine@ LineT;
		DrawingLine@ LineR;
		DrawingLine@ LineB;
		DrawingLine@ LineL;
		DrawingPoint@ PointCenter;
	
		@LineT = parts.GetObject("Line_T").AsLine();
		@LineR = parts.GetObject("Line_R").AsLine();
		@LineB = parts.GetObject("Line_B").AsLine();
		@LineL = parts.GetObject("Line_L").AsLine();
		@PointCenter = parts.GetObject("PT_CENTER").AsPoint();
	
		DrawingNode@ NodeT;
		DrawingNode@ NodeR;
		DrawingNode@ NodeB;
		DrawingNode@ NodeL;
		
		DrawingNode@ NodeTR;
		DrawingNode@ NodeTL;
		DrawingNode@ NodeBR;
		DrawingNode@ NodeBL;
		DrawingNode@ NodeCenter;
		DrawingNode@ NodeRot;
	
	
		@NodeT = parts.GetObject("Node_T").AsNode();
		@NodeR = parts.GetObject("Node_R").AsNode();
		@NodeB = parts.GetObject("Node_B").AsNode();
		@NodeL = parts.GetObject("Node_L").AsNode();
	
		@NodeTL = parts.GetObject("Node_TL").AsNode();
		@NodeTR = parts.GetObject("Node_TR").AsNode();
		@NodeBR = parts.GetObject("Node_BR").AsNode();
		@NodeBL = parts.GetObject("Node_BL").AsNode();
	
		@NodeRot = parts.GetObject("Node_Rot").AsNode();
	
		@NodeCenter = parts.GetObject("Node_Center").AsNode();
	
		POINT2D ptTL;
		POINT2D ptTR;
		POINT2D ptBR;
		POINT2D ptBL;
	
		POINT2D ptT;
		POINT2D ptR;
		POINT2D ptB;
		POINT2D ptL;
		POINT2D ptCenter;
		POINT2D ptRot;
	
		ptRot = NodeRot.GetPoint();
	
		bool bCenterNode = false;
	
		if (strMarker == "Node_TR")
		{
			POINT2D ptL2;
			ptL2 = LineL.GetPoint2();
	
			ptTL.x = ptL2.x;
			ptTL.y = ptSel.y;
	
			ptTR   = ptSel;
	
			ptBR.x = ptSel.x;
			ptBR.y = ptL2.y;
	
			ptBL  = ptL2;
		}
		else if (strMarker == "Node_TL")
		{
			POINT2D ptR2;
			ptR2 = LineR.GetPoint2();
	
			ptTL = ptSel;
	
			ptTR.x   = ptR2.x;
			ptTR.y   = ptSel.y;
	
			ptBR = ptR2;
	
			ptBL.x = ptSel.x;
			ptBL.y = ptR2.y;
	
		}
		else if (strMarker == "Node_BL")
		{
			POINT2D ptR1;
			ptR1 = LineR.GetPoint1();
	
			ptTL.x = ptSel.x;
			ptTL.y = ptR1.y;
	
			ptTR   = ptR1;
	
			ptBR.x = ptR1.x;
			ptBR.y = ptSel.y;
	
			ptBL = ptSel;
		}
		else if (strMarker == "Node_BR")
		{
			POINT2D ptL1;
			ptL1 = LineL.GetPoint1();
	
			ptTL = ptL1;
	
			ptTR.x   = ptSel.x;
			ptTR.y   = ptL1.y;
	
			ptBR = ptSel;
	
			ptBL.x = ptL1.x;
			ptBL.y = ptSel.y;
		}
		else if (strMarker == "Node_T")
		{
			ptBR = LineB.GetPoint1();
			ptBL = LineB.GetPoint2();
	
			ptTR.x   = ptBR.x;
			ptTR.y   = ptSel.y;
	
			ptTL.x   = ptBL.x;
			ptTL.y   = ptSel.y;
	
		}
		else if (strMarker == "Node_B")
		{
			ptTR = LineT.GetPoint1();
			ptTL = LineT.GetPoint2();
	
			ptBR.x   = ptTR.x;
			ptBR.y   = ptSel.y;
	
			ptBL.x   = ptTL.x;
			ptBL.y   = ptSel.y;
	
		}
		else if (strMarker == "Node_L")
		{
			ptTR = LineR.GetPoint1();
			ptBR = LineR.GetPoint2();
	
			ptTL.x   = ptSel.x;
			ptTL.y   = ptTR.y;
	
			ptBL.x   = ptSel.x;
			ptBL.y   = ptBR.y;
	
		}
		else if (strMarker == "Node_R")
		{
			ptTL = LineL.GetPoint1();
			ptBL = LineL.GetPoint2();
	
			ptTR.x   = ptSel.x;
			ptTR.y   = ptTL.y;
	
			ptBR.x   = ptSel.x;
			ptBR.y   = ptBL.y;
		}
		else if (strMarker == "Node_Center")
		{
			ptCenter = PointCenter.GetPoint();
			POINT2D ptDiff = ptSel - ptCenter;
		
			LineT.SetPoint1(ptTL);
		
			ptTL = LineL.GetPoint1() + ptDiff;
			ptBL = LineL.GetPoint2() + ptDiff;
			ptTR = LineR.GetPoint1() + ptDiff;
			ptBR = LineR.GetPoint2() + ptDiff;
	
			ptCenter = ptSel;
			bCenterNode = true;
		}
		else if (strMarker == "Node_Rot")
		{
			// Node_Rot はワールド座標系
			// (ローカル座標は、常に角度が０になるため）
			
			//他のマーカを非表示にする
			marker.SetVisible("",false);
			marker.SetVisible("Node_Rot",true);
	
	
			double dAngle;
	
			POINT2D globalCenter;
			globalCenter = parts.GetPoint();
	
			dAngle = globalCenter.Angle(ptSel);
			parts.RotateAbs(dAngle);
	
	
			return 0;
	
		}
	
	
		if (!bCenterNode)
		{
			ptCenter = (ptTL + ptBR) / 2.0;
		}
	
		//double dHalfWidth  = abs(ptBR.x -  ptTL.x) / 2.0;
		//double dHalfHeight = abs(ptBR.y -  ptTL.y) / 2.0;
	
		DispPos(_T("ptCenter"), ptCenter);
	  PointCenter.SetPoint(ptCenter);
		
		ptT = (ptTL + ptTR) / 2.0;
		ptR = (ptTR + ptBR) / 2.0;
		ptB = (ptBL + ptBR) / 2.0;
		ptL = (ptBL + ptTL) / 2.0;
	
	
		//ptT = ptTL.Add(ptTR);
		//ptR = ptTR.Add(ptBR);
		//ptB = ptBL.Add(ptBR);
		//ptL = ptBL.Add(ptTL);
	
		//ptT = ptT.Div(2.0);
		//ptR = ptR.Div(2.0);
		//ptB = ptB.Div(2.0);
		//ptL = ptL.Div(2.0);
	
		LineT.SetPoint1(ptTL);
		LineT.SetPoint2(ptTR);
		LineR.SetPoint1(ptTR);
		LineR.SetPoint2(ptBR);
		LineL.SetPoint1(ptTL);
		LineL.SetPoint2(ptBL);
		LineB.SetPoint1(ptBL);
		LineB.SetPoint2(ptBR);
	
		NodeTR.SetPoint(ptTR);
		NodeTL.SetPoint(ptTL);
		NodeBR.SetPoint(ptBR);
		NodeBL.SetPoint(ptBL);
	
		NodeT.SetPoint(ptT);
		NodeL.SetPoint(ptL);
		NodeB.SetPoint(ptB);
		NodeR.SetPoint(ptR);
	
		NodeCenter.SetPoint(ptCenter);
	
		marker.SetMarkerPos("Node_T", ptT);
		marker.SetMarkerPos("Node_R", ptR);
		marker.SetMarkerPos("Node_B", ptB);
		marker.SetMarkerPos("Node_L", ptL);
	
	 	marker.SetMarkerPos("Node_TL", ptTL);
	 	marker.SetMarkerPos("Node_TR", ptTR);
	 	marker.SetMarkerPos("Node_BR", ptBR);
	 	marker.SetMarkerPos("Node_BL", ptBL);
	 	marker.SetMarkerPos("Node_Center", ptCenter);
	
	
		return 0;
	}
	int OnEdit_ReleaseNodeMarker(NodeMarker@ marker)
	{
		DrawingParts@ parts;
		@parts = GetDrawingParts();
		POINT2D ptCenter;
		POINT2D ptGroupCenter;
		POINT2D ptDiff;
		double  dAngle = 0.0;
	
	
		DrawingPoint@ PointCenter;
		@PointCenter = parts.GetObject("PT_CENTER").AsPoint();
		ptCenter = PointCenter.GetPoint();
		
		ptGroupCenter = parts.GetPoint();
	
		ptDiff = ptCenter - ptGroupCenter;
	
		parts.Move(ptCenter);
		parts.MoveInnerPosition(-ptCenter);
	
	
		Print("OnReleaseNodeMarker\n");
		return 0;
	}
}