// 実行時最初に呼び出される
int Setup()
{
	return 0;
}
// 終了時に呼び出される
int Abort()
{
	return 0;
}
// 終了時に呼び出される
int Loop()
{
	return 0;
}
int Draw(DrawingView@ view)
{
	Print("Draw\n");
	return 0;
}
//	図形登録時に呼び出される
int Initialize()
{
	Print("Initialize\n");
	return 0;
}
// プロパティ変更時に呼び出される
int OnChangeProperty(StdString &in strPropertyName)
{
	Print("OnChangeProperty\n");
	return 0;
}
int OnInitNodeMarker(NodeMarker@ marker)
{
	Print("OnInitNodeMarker\n");
	return 0;
}
int OnSelectNodeMarker(NodeMarker@ marker, StdString &in strMarker)
{
	StdString strOut;
	strOut = strMarker;
	Print("OnSelectNodeMarker\n");
	return 0;
}
int OnMoveNodeMarker(NodeMarker@ marker, StdString &in strMarker, POINT2D &in ptSel)
{
	StdString strOut;
	strOut = strMarker;
	Print("OnMoveNodeMarker\n");
	return 0;
}
int OnReleaseNodeMarker(NodeMarker@ marker)
{
	Print("OnReleaseNodeMarker\n");
	return 0;
}