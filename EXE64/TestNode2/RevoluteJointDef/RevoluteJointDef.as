
class  ExecClass
{

	ExecClass(EBD@ ebd)
	{
		Print("RevoluteJointDef Initialize\n");
	}

	// 実行時最初に呼び出される
	int Setup(EBD@ ebd)
	{
		Print("RevoluteJointDef Setup\n");
		return 0;
	}

	// 終了時に呼び出される
	int Abort(EBD@ ebd)
	{
		return 0;
	}

	// 実行時に周期的に呼び出される
	int Loop(EBD@ ebd)
	{
		return 0;
	}

	int Draw(DrawingView@ view)
	{
		return 0;
	}

	// プロパティ変更時に呼び出される
	int OnEdit_ChangeProperty(StdString &in strPropertyName)
	{
		StdString strOut;
		strOut = "OnChangeProperty:";
		strOut += strPropertyName;
		strOut += "\n";
		Print(strOut);
		return 0;
	}

	int OnEdit_InitNodeMarker(NodeMarker@ marker)
	{
		Print("OnInitNodeMarker\n");
		return 0;
	}

	int OnEdit_SelectNodeMarker(NodeMarker@ marker, StdString &in strMarker)
	{
		StdString strOut;
		strOut = "OnSelectNodeMarker:";
		strOut += strMarker;
		strOut += "\n";
		Print(strOut);
		return 0;
	}

	int OnEdit_MoveNodeMarker(NodeMarker@ marker, StdString &in strMarker, POINT2D &in ptSel, MOUSE_MOVE_POS &in mousePos)
	{
		return 0;
	}

	int OnEdit_ReleaseNodeMarker(NodeMarker@ marker)
	{
			DrawingParts@ parts;
		@parts = GetDrawingParts();
	
	
		DrawingNode@ Node;
		@Node = parts.GetObject("Node").AsNode();
	
		StdString strList;
		strList = Node.GetConnectabaleObjectList();
	
		parts.SetPropertySetDefMin(_T("bodyA"), strList);
		parts.SetPropertySetDefMin(_T("bodyB"), strList);
	
		StdString strConnectionObjectA;
		StdString strConnectionObjectB;
	
		strConnectionObjectA = Node.GetConnectionObjectName(0);
		strConnectionObjectB = Node.GetConnectionObjectName(1);
	
		parts.SetPropertySetVal(_T("bodyA"), strConnectionObjectA);
		parts.SetPropertySetVal(_T("bodyB"), strConnectionObjectB);
	
		return 0;
	}

	int OnEdit_ChangeNode(StdString &in strMarker,NODE_CHANGE_TYPE eType)
	{
			DrawingParts@ parts;
			@parts = GetDrawingParts();
	
			DrawingNode@ Node;
			@Node = parts.GetObject("Node").AsNode();
	
	
		if (eType == NCT_CONNECTION)
	  {
			StdString strConnectionObject;
			if (strMarker == "Node")
			{ 
				StdString strConnectionObjectA;
				StdString strConnectionObjectB;
	
				strConnectionObjectA = Node.GetConnectionObjectName(0);
				strConnectionObjectB = Node.GetConnectionObjectName(1);
			
				parts.SetPropertySetVal(_T("bodyA"), strConnectionObjectA);
				parts.SetPropertySetVal(_T("bodyB"), strConnectionObjectB);
			}
		}
		else if (eType == NCT_POSITION)
		{
	
		}
		return 0;
	}
}