/**
 * @brief			CIoDef ヘッダーファイル
 * @file			CIoDef.h
 * @author			Yasuhiro Sato
 * @date			09-5-2011 19:36:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2011 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */


namespace IO_COMMON
{

//--------------
//<! データ種別
//--------------
enum DAT_TYPE
{
    DAT_MASK     = 0x00FF0000,  // マスク
    DAT_NONE     = 0x00000000,  // 未設定
    DAT_BIT      = 0x00010000,  // ビットデータ
    DAT_CHAR     = 0x00020000,  // 8bit
    DAT_UCHAR    = 0x00030000,  // 8bit
    DAT_USHORT   = 0x00040000,  // 16bit
    DAT_SHORT    = 0x00050000,  // 16bit
    DAT_ULONG    = 0x00060000,  // 32bit
    DAT_LONG     = 0x00070000,  // 32bit
    DAT_FLOAT    = 0x00080000,  // 浮動小数点値
    DAT_DOUBLE   = 0x00090000,  // 倍精度浮動小数点値
    DAT_STR      = 0x000A0000,  // 文字データ

    DAT_END, 
};


//--------------
//<! 入出力方向
//--------------
enum DT_IO_DIR
{
    IO_MASK =  0xFF000000,
    IO_NONE =  0x00000000,  
    IO_IN   =  0x01000000,  
    IO_OUT  =  0x02000000,
    IO_SIO  =  0x04000000,  //データ種別，ビット長無効
};


//--------------
//<! ビット長
//--------------
enum DT_IO_BIT_LEN
{
    BL_MASK =  0x0000FFFF,
    BL_NONE =  0x00000000,
    BL_01   =  0x00000001,
    BL_08   =  0x00000008,
    BL_10   =  0x0000000a,
    BL_12   =  0x0000000c,
    BL_14   =  0x0000000e,
    BL_16   =  0x00000010,
    BL_18   =  0x00000012,
    BL_20   =  0x00000014,
    BL_24   =  0x00000018,
    BL_32   =  0x00000020,
};

//--------------
//<! IO種別
//--------------
enum DT_IO_TYPE
{
    IT_NONE  = 0,
    IT_DI    = IO_IN  | DAT_BIT | BL_01,
    IT_DO    = IO_OUT | DAT_BIT | BL_01,
    IT_AI_8  = IO_IN  | DAT_UCHAR | BL_08,
    IT_AO_8  = IO_OUT | DAT_UCHAR | BL_08,
    IT_AI_10 = IO_IN  | DAT_USHORT | BL_10,
    IT_AO_10 = IO_OUT | DAT_USHORT | BL_10,
    IT_AI_12 = IO_IN  | DAT_USHORT | BL_12,
    IT_AO_12 = IO_OUT | DAT_USHORT | BL_12,
    IT_AI_14 = IO_IN  | DAT_USHORT | BL_14,
    IT_AO_14 = IO_OUT | DAT_USHORT | BL_14,
    IT_AI_16 = IO_IN  | DAT_USHORT | BL_16,
    IT_AO_16 = IO_OUT | DAT_USHORT | BL_16,
    IT_AI_18 = IO_IN  | DAT_ULONG  | BL_18,
    IT_AO_18 = IO_OUT | DAT_ULONG  | BL_18,
    IT_AI_20 = IO_IN  | DAT_ULONG  | BL_20,
    IT_AO_20 = IO_OUT | DAT_ULONG  | BL_20,
    IT_AI_24 = IO_IN  | DAT_ULONG  | BL_24,
    IT_AO_24 = IO_OUT | DAT_ULONG  | BL_24,
    IT_AI_32 = IO_IN  | DAT_ULONG  | BL_32,
    IT_AO_32 = IO_OUT | DAT_ULONG  | BL_32,
    IT_LONG_I   = IO_IN  | DAT_LONG   | BL_NONE,
    IT_LONG_O   = IO_OUT | DAT_LONG   | BL_NONE,
    IT_FLOAT_I  = IO_IN  | DAT_FLOAT  | BL_NONE,
    IT_FLOAT_O  = IO_OUT | DAT_FLOAT  | BL_NONE,
    IT_DOUBLE_I = IO_IN  | DAT_DOUBLE | BL_NONE,
    IT_DOUBLE_O = IO_OUT | DAT_DOUBLE | BL_NONE,
    IT_STR_I    = IO_IN  | DAT_STR    | BL_NONE,
    IT_STR_O    = IO_OUT | DAT_STR    | BL_NONE,
    IT_SIO      = IO_SIO | DAT_NONE   | BL_NONE,
    IT_END,
};

}//namespace IO_COMMON