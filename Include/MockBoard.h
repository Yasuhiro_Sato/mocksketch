/**
 * @brief			MockBoard ヘッダーファイル
 * @file			MockBoard.h
 * @author			Yasuhiro Sato
 * @date			09-5-2011 19:36:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2011 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef _MOCK_BOARD_H_
#define _MOCK_BOARD_H_
#include<stdlib.h>


namespace MockBoard{
#ifdef  MB_DLL_EXPORTS
#define DLL_API __declspec(dllexport)
#else
#define DLL_API __declspec(dllimport)
#endif



DLL_API  int  HeaderVersion(){return 1000;}

DLL_API  bool   GetBoardName(wchar_t* pName, int iBoardNo);
DLL_API  bool   GetBoardNoMax(int* pMax);
DLL_API  bool   UseBoard(int iBoardNo);
DLL_API  bool   ReleaseBoard(int iBoardNo);


DLL_API  bool    GetMaxPage       (int* pMax, int   iBoardNo);
DLL_API  bool    IsMassUpdate     (bool* pUpdate, int   iBoardNo);
DLL_API  bool    GetMaxUpdateSize (int* pMax, int   iBoardNo);
DLL_API  bool    GetMaxItem       (int* pMax, int   iBoardNo);
DLL_API  bool    GetMaxPageSize   (int* pMax, int   iBoardNo);

DLL_API  bool    GetItemRestriction(int* pIoTypeArray,
                                   int* pArraySize, 
                                   int   iBoardNo,
                                   int  iPageNo,
                                   int  iItemNo);


DLL_API  bool    GetItemDef        (int iIoType,
                                   int   iBoardNo,
                                   int iPageNo,
                                   int iItemNo);

DLL_API  bool   GetUserItemMax   (int* pMax, int   iBoardNo);

DLL_API  bool   GetUserItemName(wchar_t* pItemHeadName,
                               int   iBoardNo,
                               int   iItemCol);

DLL_API bool   GetUserItemDef (wchar_t* pItemDef,
                                  int   iBoardNo,
                              int      iItemCol);

DLL_API bool   SetUserItem    (wchar_t*  pUserItem,
                                  int   iBoardNo,
                                  int   iPageNo,
                                  int   iItemCol,
                                  int   iItemNo);

    //--------------
    // データ取得
    //--------------
    //データ取得
DLL_API bool    GetData(void* pVal,
                    int iBoardNo,
                    int iPageNo,
                    int iItemNo,
                    int iLength);

    //データ設定
DLL_API bool    SetData(void* pVal,     
                               int iBoardNo,
                               int PageNo,
                               int ItemNo,
                               int Length);



DLL_API bool    Reset (int iBoardNo);               //ボードリセット
DLL_API bool    Init  (int iBoardNo);               //ボードイニシャライズ
DLL_API bool    Setup (int iBoardNo);               //開始前処理
DLL_API bool    Start (int iBoardNo);               //計測開始
DLL_API bool    Stop  (int iBoardNo);               //計測停止
DLL_API bool    Finish(int iBoardNo);               //終了処理

DLL_API bool    SetLang (wchar_t* strLang);               //言語設定




}
#endif _MOCK_BOARD_H_
