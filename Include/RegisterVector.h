#ifndef REGISRER_VECTOR_H
#define REGISRER_VECTOR_H

#ifndef ANGELSCRIPT_H 
// Avoid having to inform include path if header is already include before
#include <angelscript.h>
#endif
/*
   REGISTER_VECTOR(typeName, scriptName)
   REGISTER_VECTOR2(typeName, typeNameNoOperator, scriptName) は、 
            void Register##scriptName(asIScriptEngine *engine)  
            int RegisterType##scriptName(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
            を生成する。

    REGISTER_VECTOR2 の引数    typeNameNoOperator は

    vector<CObj*> のような変数を AngelScript 側で使用する場合
    CObjのAngelScript側の定義を記入する。
    この値が atの戻り値 push_backの引数等になる。
*/

#include "VAL_DATA.h"

BEGIN_AS_NAMESPACE

//---------------------------------

union UNI_SCRIPT_VAL
{
    __int64 iVal;
    double  dVal;
    void   *pObj;
};





//---------------------------------


template <class T>class RegisterVector_Ptr;
template <class T>class RegisterVector;

//引数がポインターでない場合は RegisterVectorを使用する
template <class T>
struct IF_Pointer{
	typedef RegisterVector<T> ret;
};

//引数がポインターの場合は RegisterVector_Ptrを使用する
template <class T>
struct IF_Pointer<T*>{
	typedef RegisterVector_Ptr<T*> ret;
};


template<class T>
class StdVector :public std::vector<T>
{
public:
    std::string m_scriptName;
    std::string m_typeName;
    std::string m_typeNameNoOperator;
    static asIScriptEngine* engine;
    int m_iTypeId;
};

#define REGISTER_VECTOR(typeName, scriptName)                              \
    REGISTER_VECTOR2(typeName, typeName, scriptName)                       \


#define REGISTER_VECTOR2(typeName, typeNameNoOperator, scriptName)         \
    IF_Pointer<typeName>::ret g_##scriptName;                              \
    typedef StdVector<typeName> scriptName;                                \
    asIScriptEngine* scriptName::engine = NULL;                            \
    static void Construct##scriptName(scriptName* thisPointer    )         \
{                                                                          \
	new(thisPointer) scriptName();                                         \
    thisPointer->m_scriptName = #scriptName;                               \
    thisPointer->m_typeName = #typeName;                                   \
    thisPointer->m_typeNameNoOperator = #typeNameNoOperator;               \
    thisPointer->m_iTypeId = 0;                                            \
}                                                                          \
	static void Destruct##scriptName(scriptName *thisPointer)              \
{                                                                          \
	thisPointer->~scriptName();                                            \
}                                                                          \
bool CALLBACK Conv##scriptName( VAL_DATA* pValData, const void* pValAddress, void* pContext)\
{                                                                                    \
    return g_##scriptName.Conv( pValData, pValAddress, pContext);                    \
}                                                                                    \
int RegisterType##scriptName(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)     \
{                                                                                    \
    g_##scriptName.SetName(#scriptName);                                             \
    g_##scriptName.SetTypeName(#typeName);                                           \
    g_##scriptName.SetTypeNameNoOperator(#typeNameNoOperator);                       \
                                                                                     \
    int iRet = g_##scriptName.RegisterObjectType(engine);                            \
    mapIdFunc[iRet] = Conv##scriptName;                                              \
    return iRet;                                                                     \
}                                                                                    \
void Register##scriptName(asIScriptEngine *engine)                                   \
{                                                                                    \
    int r;                                                                           \
    g_##scriptName.SetConvFunc(Conv##scriptName);                                    \
    r = engine->RegisterObjectBehaviour(#scriptName,                                 \
        asBEHAVE_CONSTRUCT,  "void f()",                 \
        asFUNCTION(Construct##scriptName),               \
        asCALL_CDECL_OBJLAST);  assert( r >= 0 );        \
    r = engine->RegisterObjectBehaviour(#scriptName,     \
        asBEHAVE_DESTRUCT,  "void f()",                  \
        asFUNCTION(Destruct##scriptName),                \
        asCALL_CDECL_OBJLAST);  assert( r >= 0 );        \
                                                         \
    g_##scriptName.Register(engine);                     \
}                   


template <class T>
class RegisterVectorCommon
{
protected:
    bool (CALLBACK *m_pComvFunc)( VAL_DATA* pValData, const void* pValAddress, void* pContext);
    std::string m_scriptName;
    std::string m_typeName;
    std::string m_typeNameNoOperator;

public:


    void SetConvFunc( bool (CALLBACK * pConvFunc)( VAL_DATA* pValData, const void* pValAddress, void* pContext))
    {
        m_pComvFunc = pConvFunc;
    }
    void SetTypeNameNoOperator(const char* strName)
    {
        m_typeNameNoOperator = strName;
    }
    void SetTypeName(const char* strName)
    {
        m_typeName = strName;
    }

    void SetName(const char* strName)
    {
        m_scriptName = strName;
    }

    int RegisterObjectType(asIScriptEngine *engine)
    {
        int iRet = engine->RegisterObjectType(m_scriptName.c_str(), 
                    sizeof(StdVector<T>), 
                    asOBJ_VALUE | asOBJ_APP_CLASS_CD);
                    assert( iRet >= 0 );
        StdVector<T>::engine = engine;
        return iRet;
    }

    virtual const void* GetPtr(int iIndex, const StdVector<T>* pVec){return NULL;}

    bool CALLBACK Conv( VAL_DATA* pValData, const void* pValAddress, void* pContext)
    {
        pValData->strTypeName = _T("Vector");
        const StdVector<T>* pVector = reinterpret_cast<const StdVector<T>*>(pValAddress);

        asUINT iSizeMax = 20;
        asUINT iSize;

        VAL_DATA valSub;
        const void* pValSub;
        StdStringStream strm;

        strm <<_T("{");

        iSize = static_cast<asUINT>(pVector->size());
        if (iSize > iSizeMax)
        {
            //TODO: 遅延展開を考える
            //時間がかかりますよ展開しますか
        }

        for (asUINT iCnt = 0; iCnt < iSize; iCnt++)
        {
            valSub.lstVal.clear();
            StdStringStream strmSubName;
            strmSubName << pValData->strName << _T("[") << iCnt <<  _T("]");

            pValSub = GetPtr(iCnt, pVector);

            m_pComvFunc(&valSub, pValSub, pContext);
            valSub.strName     = strmSubName.str();
            pValData->lstVal.push_back(valSub);
            pValData->strVal += valSub.strVal;
            pValData->strVal += _T(",");
            pValData->lstVal.push_back(valSub);

            if (iSize < iSizeMax)
            {
                strm << valSub.strVal << _T(",");
            }

            if (iSize == iSizeMax)
            {
                strm <<_T("...");
            }
        }
        strm <<_T("}");
        pValData->strVal = strm.str();
        pValData->bValid = true;
        return true;
    }

    int RegisterType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc, const char * scriptName)
    {
        int iRet = engine->RegisterObjectType(m_scriptName, 
                   sizeof(StdVector<T>), 
                   asOBJ_VALUE | asOBJ_APP_CLASS_CD);
                   assert( iRet >= 0 );

        mapIdFunc[iRet] = ConvPOINT2D_VECTOR;
        return iRet;
    }

    void Register(asIScriptEngine *engine)
    {
        int r;
        std::stringstream strmFuncRef;
        /*
	    r = engine->RegisterObjectBehaviour(m_scriptName.c_str(), 
            asBEHAVE_CONSTRUCT,  "void f()",  
            asFUNCTION(Constructer), 
            asCALL_CDECL_OBJLAST);
            assert( r >= 0 );

	    r = engine->RegisterObjectBehaviour(m_scriptName.c_str(), 
            asBEHAVE_DESTRUCT,  "void f()",  
            asFUNCTION(Destructor), 
            asCALL_CDECL_OBJLAST); 
            assert( r >= 0 );
        */
        //"POINT2D_VECTOR &opAssign(const POINT2D_VECTOR &in)", 


        strmFuncRef.str("");
        strmFuncRef << m_scriptName.c_str()  << " &opAssign(const " << m_scriptName.c_str() <<  "&in)";
	    r = engine->RegisterObjectMethod(m_scriptName.c_str(), 
            strmFuncRef.str().c_str(), 
            asMETHODPR(StdVector<T>, operator=, (const StdVector<T> &),
            StdVector<T>&), asCALL_THISCALL); assert( r >= 0 );


            r = engine->RegisterObjectMethod(m_scriptName.c_str(), 
            "void resize(uint)", 
            asMETHODPR(StdVector<T>, 
            resize, (size_t), void), asCALL_THISCALL); 
            assert( r >= 0 );

        r = engine->RegisterObjectMethod(m_scriptName.c_str(), 
            "uint size()", 
            asMETHOD(StdVector<T>, 
            size), asCALL_THISCALL); 
            assert( r >= 0 );

	    r = engine->RegisterObjectMethod(m_scriptName.c_str(), 
            "bool empty() const", 
            asMETHOD(StdVector<T>, empty), 
            asCALL_THISCALL); 
            assert( r >= 0 );

	    r = engine->RegisterObjectMethod(m_scriptName.c_str(), 
            "void pop_back()", 
            asMETHOD(StdVector<T>, pop_back), 
            asCALL_THISCALL); 
            assert( r >= 0 );


    }

};


template <class T>
class RegisterVector_Ptr: public RegisterVectorCommon<T>
{

public:

    virtual const void* GetPtr(int iIndex, const StdVector<T>* pVec)
    {
        return pVec->at(iIndex);
    }

    static T AtRef(unsigned int iIndex, StdVector<T>* pVec)
    {
        return pVec->at(iIndex);
    }

    static void Clear(StdVector<T>* pVec)
    {
        foreach(T pData, *pVec)
        {
            RelRef( pData, pVec);
        }
        pVec->clear();
    }

    static void Erase(unsigned int iIndex, StdVector<T>* pVec)
    {
        T pData;
        StdVector<T>::iterator it = pVec->begin();
        it += iIndex;

        pData = *it;

        RelRef( pData, pVec);

        pVec->erase(it);
    }
     
    static bool AddRef(void* pRef, int refTypeId, StdVector<T>* pVec)
    {
	    //asIScriptContext *ctx    = asGetActiveContext();
        //asIScriptEngine  *engine = ctx->GetEngine();
        asIScriptEngine  *engine = StdVector<T>::engine;
        asIObjectType *ot = engine->GetObjectTypeById(refTypeId);

        if( !(ot->GetFlags() & asOBJ_NOCOUNT) )
        {
            if (refTypeId & asTYPEID_MASK_OBJECT)
            {
                 std::string strName = ot->GetName();
                 if (pVec->m_typeNameNoOperator == strName)
                 {
                    if (pVec->m_iTypeId == 0)
                    {
                        pVec->m_iTypeId = refTypeId;
                    }
                    else
                    {
                        if (pVec->m_iTypeId != refTypeId)
                        {
                            return false;
                        }
                    }

                    void* pData;

                    if (refTypeId & asTYPEID_OBJHANDLE)
                    {
                        pData = *(void**)pRef;
                    }
                    else
                    {
                        pData = pRef;
                    }

T pObj = reinterpret_cast<T>(pRef);
T pObj2 = reinterpret_cast<T>( *(void**)pRef);

                    engine->AddRefScriptObject(pData, engine->GetObjectTypeById(refTypeId));
                    return true;
                 }
                 //TODO:エラー時の例外を生成
            }
            return false;
        }
        return true;
    }

    static bool RelRef(void* pRef, StdVector<T>* pVec)
    {

        if (pVec->m_iTypeId == 0)
        {
            return false;
        }

	    //asIScriptContext *ctx    = asGetActiveContext();
        //asIScriptEngine  *engine = ctx->GetEngine();
        asIScriptEngine  *engine = StdVector<T>::engine;
        asIObjectType *ot = engine->GetObjectTypeById(pVec->m_iTypeId);

        if( !(ot->GetFlags() & asOBJ_NOCOUNT) )
        {
            if (pVec->m_iTypeId & asTYPEID_MASK_OBJECT)
            {
                 std::string strName = ot->GetName();
                 if (pVec->m_typeNameNoOperator == strName)
                 {
                    void* pData;
                    if (pVec->m_iTypeId & asTYPEID_OBJHANDLE)
                    {
                        pData = *(void**)pRef;
                    }
                    else
                    {
                        pData = pRef;
                    }

T pObj = reinterpret_cast<T>(pRef);
T pObj2 = reinterpret_cast<T>( *(void**)pRef);


                    engine->ReleaseScriptObject(pData, engine->GetObjectTypeById(pVec->m_iTypeId));
                    return true;
                 }
                 //TODO:エラー時の例外を生成
            }
            return false;
        }
        return true;
    }

    static void InsertRef(unsigned int iIndex, void* pRef, int refTypeId, StdVector<T>* pVec)
    {
        if (AddRef( pRef, refTypeId, pVec))
        {
            StdVector<T>::iterator it = pVec->begin();
            it += iIndex;
            T val;
            val = reinterpret_cast<T>(pRef);
            pVec->insert(it, val);
        }
    }

    static void PushBackRef(void* pRef, int refTypeId, StdVector<T>* pVec)
    {
        if (AddRef( pRef, refTypeId, pVec))
        {
            T val;
            val = reinterpret_cast<T>(pRef);
            pVec->push_back(val);
        }
    }

    static void PushBackRef2(T pRef, StdVector<T>* pVec)
    {
        //if (AddRef( pRef, refTypeId, pVec))
        {
            //T val;
            //val = reinterpret_cast<T>(pRef);
            pVec->push_back(pRef);
        }
    }

    void Register(asIScriptEngine *engine)
    {
        RegisterVectorCommon::Register(engine);

        int r;
        std::stringstream strmFuncRef;

        r = engine->RegisterObjectMethod(m_scriptName.c_str(), 
            "void clear()", 
            asMETHOD(StdVector<T>, 
            clear), asCALL_THISCALL); 
            assert( r >= 0 );

            strmFuncRef.str("");
        strmFuncRef << m_typeNameNoOperator.c_str()  << "@ at(uint)";
        r = engine->RegisterObjectMethod(m_scriptName.c_str(), 
            strmFuncRef.str().c_str(), 
            asFUNCTION(AtRef), 
            asCALL_CDECL_OBJLAST); 
            assert( r >= 0 );

        strmFuncRef.str("");
        strmFuncRef << m_typeNameNoOperator.c_str()  << "@opIndex(uint)";
	    r = engine->RegisterObjectMethod(m_scriptName.c_str(), 
            strmFuncRef.str().c_str(), 
            asFUNCTION(AtRef), 
            asCALL_CDECL_OBJLAST); 
            assert( r >= 0 );

        strmFuncRef.str("");
        strmFuncRef << "void push_back(const ?& in)";
	    r = engine->RegisterObjectMethod(m_scriptName.c_str(),
            strmFuncRef.str().c_str(), 
            asFUNCTION(PushBackRef), 
            asCALL_CDECL_OBJLAST); 
		    assert(r >= 0);

        strmFuncRef.str("");
        strmFuncRef << "void push_back2(const " << m_typeNameNoOperator.c_str() << "& in)";
	    r = engine->RegisterObjectMethod(m_scriptName.c_str(),
            strmFuncRef.str().c_str(), 
            asFUNCTION(PushBackRef2), 
            asCALL_CDECL_OBJLAST); 
		    assert(r >= 0);

            
        strmFuncRef.str("");
        strmFuncRef << "void insert(uint, const ?& in)";
	    r = engine->RegisterObjectMethod(m_scriptName.c_str(),
            strmFuncRef.str().c_str(), 
            asFUNCTION(InsertRef), 
            asCALL_CDECL_OBJLAST); 
            assert( r >= 0 );

	    r = engine->RegisterObjectMethod(m_scriptName.c_str(), 
            "void erase(uint)", 
            asFUNCTION(Erase), 
            asCALL_CDECL_OBJLAST); 
            assert( r >= 0 );


    }


};

//---------------------------------
template <class T>
class RegisterVector: public RegisterVectorCommon<T>
{
public:

    static T& At(unsigned int iIndex, StdVector<T>* pVec)
    {
        return pVec->at(iIndex);
    }

    virtual const void* GetPtr(int iIndex, const StdVector<T>* pVec)
    {
        return &pVec->at(iIndex);
    }

    static void Erase(unsigned int iIndex, StdVector<T>* pVec)
    {
        StdVector<T>::iterator it = pVec->begin();
        it += iIndex;
        pVec->erase(it);
    }

    static void Insert(unsigned int iIndex, const T& val, StdVector<T>* pVec)
    {
        StdVector<T>::iterator it = pVec->begin();
        it += iIndex;
        pVec->insert(it, val);
    }

    static void PushBack(const T& val, StdVector<T>* pVec)
    {
        pVec->push_back(val);
    }


    void Register(asIScriptEngine *engine)
    {
        RegisterVectorCommon::Register(engine);

        int r;
        std::stringstream strmFuncRef;

        r = engine->RegisterObjectMethod(m_scriptName.c_str(), 
            "void clear()", 
            asMETHOD(StdVector<T>, 
            clear), asCALL_THISCALL); 
            assert( r >= 0 );

        strmFuncRef.str("");
        strmFuncRef << m_typeNameNoOperator.c_str()  << "& at(uint)";
        r = engine->RegisterObjectMethod(m_scriptName.c_str(), 
            strmFuncRef.str().c_str(), 
            asFUNCTION(At), 
            asCALL_CDECL_OBJLAST); 
            assert( r >= 0 );

        strmFuncRef.str("");
        strmFuncRef << m_typeNameNoOperator.c_str()  << "&opIndex(uint)";
	    r = engine->RegisterObjectMethod(m_scriptName.c_str(), 
            strmFuncRef.str().c_str(), 
            asFUNCTION(At), 
            asCALL_CDECL_OBJLAST); 
            assert( r >= 0 );

        strmFuncRef.str("");
        strmFuncRef << "void push_back(const " << m_typeNameNoOperator.c_str() << " &in)";
	    r = engine->RegisterObjectMethod(m_scriptName.c_str(),
            strmFuncRef.str().c_str(), 
            asFUNCTION(PushBack), 
            asCALL_CDECL_OBJLAST); 
		    assert(r >= 0);

        strmFuncRef.str("");
        strmFuncRef << "void insert(uint, const " << m_typeNameNoOperator.c_str() << "&in)";
	    r = engine->RegisterObjectMethod(m_scriptName.c_str(),
            strmFuncRef.str().c_str(), 
            asFUNCTION(Insert), 
            asCALL_CDECL_OBJLAST); 
            assert( r >= 0 );

	    r = engine->RegisterObjectMethod(m_scriptName.c_str(), 
            "void erase(uint)", 
            asFUNCTION(Erase), 
            asCALL_CDECL_OBJLAST); 
            assert( r >= 0 );

    }


};

END_AS_NAMESPACE

#endif
