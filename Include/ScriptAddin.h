/**
 * @brief			ScriptAddin �w�b�_�[�t�@�C��
 * @file			ScriptAddin.h
 * @author			Yasuhiro Sato
 * @date			10-5-2013 10:42:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2011 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef _SCRIPT_ADDIN_H_
#define _SCRIPT_ADDIN_H_
#include<stdlib.h>
#include <angelscript.h>
#include <tchar.h>
#include "UtilityDef.h"
#include "VAL_DATA.h"

namespace MockScript{
#ifdef  MS_DLL_EXPORTS
#define DLL_API extern "C" __declspec(dllexport)
#else
#define DLL_API extern "C" __declspec(dllimport)
#endif

//���̎擾
DLL_API  LPCTSTR GetName();
typedef LPCTSTR (*FUNC_GetName)();

//�^�o�^
DLL_API  int RegisterType(asIScriptEngine *engine);
typedef int (*FUNC_RegisterType)(asIScriptEngine *);

//�o�^
DLL_API  int Register(asIScriptEngine *engine);
typedef int (*FUNC_Register)(asIScriptEngine *);

//�ϐ��擾
DLL_API   int GetValData( StdChar* pStrVal, size_t iBufSize,
                          int iTypeId,
                          void* pValAddress, 
                          void* pContext);
typedef  int (*FUNC_GetValData)( StdChar*, size_t, int, const void*, void*);


}
#endif _SCRIPT_ADDIN_H_