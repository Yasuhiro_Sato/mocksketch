#include <string>
#include <vector>
#include <map>
#include <sstream>

#ifdef  UNICODE
#define StdChar             wchar_t
#define StdString           std::wstring
#define StdStringStream     std::wstringstream
#define StdStreamBuf        std::wstreambuf
#else
#define StdChar             char
#define StdString           std::string
#define StdStringStream     std::stringstream
#define StdStreamBuf        std::streambuf
#endif
