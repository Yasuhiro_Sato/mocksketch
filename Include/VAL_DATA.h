/**
 * @brief			VAL_DATAインクルードファイル
 * @file			VAL_DATA.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	ウォッチで表示する変数データ
 *			
 *
 * $
 * $
 * 
 */
#ifndef _VAL_DATA_H
#define _VAL_DATA_H


//!<変数表示データ
struct VAL_DATA
{
    bool bValid = false;                //!< 有効・無効
    bool bArray = false;                //!< 配列
    StdString   strName;        //!< 名称
    StdString   strTypeName;    //!< 型名
    StdString   strVal;         //!< 値
    std::vector<VAL_DATA> lstVal;
};

class VAL_DATA_CONV
{
public:
    static StdString ValDataToStr(const VAL_DATA* pValData)
    {
        //[ true ; false ; strName ; strVal;  9;[  ];  ]
        StdStringStream strmVal; 

        strmVal << _T("[");
        strmVal << pValData->bValid;
        strmVal << _T(";");
        strmVal << pValData->bArray;
        strmVal << _T(";");
        strmVal <<  _T("\"") << pValData->strName << _T("\"");
        strmVal << _T(";");
        strmVal << _T("\"") << pValData->strTypeName << _T("\"");
        strmVal << _T(";");
        strmVal << _T("\"") << pValData->strVal << _T("\"");
        strmVal << _T(";");
        strmVal << pValData->lstVal.size();
        strmVal << _T(";");
        for(size_t iCnt = 0; iCnt < pValData->lstVal.size(); iCnt++)
        {
            strmVal <<ValDataToStr(&pValData->lstVal[iCnt]);
            strmVal << _T(";");
        }
        strmVal << _T("]");
        return strmVal.str();
    }

    static LPCTSTR ReadStr(LPCTSTR pStr, StdString* pStrOut)
    {
        pStrOut->clear();
        
        bool bStrMode = false;
        bool bEsc = false;

        while(*pStr != 0)
        {
            if (!bEsc)
            {
                if(*pStr == _T('\\'))
                {
                    bEsc = true;
                }
            }
            else
            {
                *pStrOut += *pStr++;
                bEsc = false;
                continue;
            }

            if(*pStr == _T('\"'))
            {
                bStrMode = !bStrMode;
                pStr++;
                continue;
            }


            if (!bStrMode)
            {
                if(*pStr == _T(';'))
                {
                    pStr++;
                    break;
                }
            }

            *pStrOut += *pStr++;

        }
        return pStr;
    }

    static LPCTSTR ReadInt(LPCTSTR pStr, int* pIntOut)
    {
        LPCTSTR pRet;
        StdString  strInt;
        pRet = ReadStr(pStr, &strInt);
        *pIntOut = _tstoi(strInt.c_str());
        return pRet;
    }

    static bool StrToValData(LPCTSTR pStr, LPCTSTR* ppStrNext, VAL_DATA* pValData)
    {
        if (*pStr++ != _T('['))
        {
            return false;
        }

        int iListSize;
        int iBool;
        pStr = ReadInt(pStr, &iBool); pValData->bValid = (iBool != 0);
        pStr = ReadInt(pStr, &iBool); pValData->bArray = (iBool != 0);
        pStr = ReadStr (pStr, &pValData->strName);
        pStr = ReadStr (pStr, &pValData->strTypeName);
        pStr = ReadStr (pStr, &pValData->strVal);
        pStr = ReadInt (pStr, &iListSize);

        bool bRet;
        for (int iCnt = 0; iCnt < iListSize; iCnt++)
        {
            VAL_DATA valData;
            LPCTSTR pStrNext;
            bRet = VAL_DATA_CONV::StrToValData(pStr, &pStrNext, &valData);
            if (!bRet)
            {
                return false;
            }
            pValData->lstVal.push_back(valData);
            pStr = pStrNext;
            pStr++;
        }
        
        if (*pStr++ != _T(']'))
        {
            return false;
        }

        if (ppStrNext)
        {
            *ppStrNext = pStr;
        }
        return true;
    }
};



//!<型ID->変数表示データ変換関数マップ
typedef std::map<int, bool (CALLBACK*)(VAL_DATA*, const void*, void*) > ID_VALFUNC_MAP;

//!<型名->変数表示データ変換関数マップ
typedef std::map<std::string, bool (CALLBACK*)(VAL_DATA*, const void*, void*) > NAME_VALFUNC_MAP;


#endif //_VAL_DATA_H