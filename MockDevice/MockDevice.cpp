// MockDevice.cpp : DLL アプリケーション用にエクスポートされる関数を定義します。
//

#include "stdafx.h"
#include "MockDevice.h"
#include "CAio.h"
#include <math.h>

#define CHECK_BOARD(iNo)\
    if (iNo < 0)            {m_strError = StrFormat(_T(" %s BoardNo:%d" ), DB_FUNC_NAME, iNo); return false;}\
    if (iNo > BoardMax())   {m_strError = StrFormat(_T(" %s BoardNo:%d" ), DB_FUNC_NAME, iNo); return false;}\
    

MockDevice::MockDevice():
bInit (false)
{

}


MockDevice::~MockDevice()
{

}

void MockDevice::Init()
{
    if (bInit){return;}

    char szDevName[1024];
    char szDevice[1024];
    short sIndex = 0;
    long lRet;
    BoardProperty prop;

    while(1)
    {
        lRet = AioQueryDeviceName( sIndex, szDevName, szDevice);
        if (lRet != 0 )
        {
            break;
        }
        
        prop.strDevName = CharToString(szDevName);
        prop.strReg = CharToString(szDevice);
        prop.iId = -1;
        m_lstBoardProperty.push_back(prop);
        sIndex++;
    }
    bInit = true;
}


int MockDevice::BoardMax()
{
    return static_cast<int>(m_lstBoardProperty.size());
}

const MockDevice::BoardProperty& MockDevice::GetBordProperty(int iBoardNo)  const
{
    //if (iBoardNo < 0)            {return _T("");}
    //if (iBoardNo > BoardMax())   {return _T("");}
    return m_lstBoardProperty[iBoardNo];
}


//プロパティ設定
bool  MockDevice::UseBoard(int iBoardNo)
{
    CHECK_BOARD(iBoardNo);

    BoardProperty* pProp;
    pProp = &m_lstBoardProperty[iBoardNo];
    std::string strReg;
    strReg = StringToChar(pProp->strReg);

    short sId;
    short sVal;
    long lRet;
    lRet = AioInit(const_cast<char*>(strReg.c_str()), &sId);

    if (lRet != 0)
    {
        m_strError =
        StrFormat(_T("UseBord(%d) AioInit(%s)=%d"), iBoardNo, 
                                                 pProp->strReg.c_str(),
                                                 lRet);
        return false;
    }

    pProp->iId = sId;

    lRet = AioGetAiMaxChannels(sId, &sVal);
    if (lRet != 0)
    {
        m_strError =
        StrFormat(_T("UseBord(%d) AioGetAiMaxChannels()=%d"), iBoardNo, lRet);
        return false;
    }
    pProp->iAiMax = sVal;

    lRet = AioGetAoMaxChannels(sId, &sVal);
    if (lRet != 0)
    {
        m_strError =
        StrFormat(_T("UseBord(%d) AioGetAoMaxChannels()=%d"), iBoardNo, lRet);
        return false;
    }
    pProp->iAoMax = sVal;

    lRet = AioGetCntMaxChannels(sId, &sVal);
    if (lRet != 0)
    {
        m_strError =
        StrFormat(_T("UseBord(%d) AioGetCntMaxChannels()=%d"), iBoardNo, lRet);
        return false;
    }
    pProp->iCntMax = sVal;

    lRet = AioGetAiResolution(sId, &sVal);
    if (lRet != 0)
    {
        m_strError =
        StrFormat(_T("UseBord(%d) AioGetAiResolution()=%d"), iBoardNo, lRet);
        return false;
    }
    pProp->iAiRes = sVal;

    lRet = AioGetAoResolution(sId, &sVal);
    if (lRet != 0)
    {
        m_strError =
        StrFormat(_T("UseBord(%d) AioGetAiResolution()=%d"), iBoardNo, lRet);
        return false;
    }
    pProp->iAoRes = sVal;

    return true;
}

// ボード開放
bool  MockDevice::ReleaseBoard(int iBoardNo)
{
    //
    CHECK_BOARD(iBoardNo);
    
    BoardProperty* pProp;
    pProp = &m_lstBoardProperty[iBoardNo];

    if (pProp->iId == -1)
    {
        return true;
    }
    
    long lRet;
    lRet = AioExit(static_cast<short>(pProp->iId));
    if (lRet != 0)
    {
        m_strError =
        StrFormat(_T("ReleaseBoard(%d) AioExit()=%d"), iBoardNo, lRet);
        return false;
    }

    pProp->iId = -1;
    return true;
}

//最大ペーシ数取得
bool MockDevice::GetMaxPage (int* pMax, int iBoardNo)
{
    // 8項目/pageとする。
    // また、カウンターは独立して１ページとする
    CHECK_BOARD(iBoardNo);

    BoardProperty* pProp;
    pProp = &m_lstBoardProperty[iBoardNo];

    int iAiPage  = int(ceil(pProp->iAiMax / 8.0));
    int iAoPage  = int(ceil(pProp->iAoMax / 8.0));
    int iDiPage  = int(ceil(pProp->iDiMax / 8.0));
    int iDoPage  = int(ceil(pProp->iDoMax / 8.0));
    int iCntPage = int(ceil(pProp->iCntMax / 8.0));

    *pMax = iAiPage + iAoPage + iDiPage + iDoPage + iCntPage;
    return true;
}



bool MockDevice::SetId(int iBoardNo, int iId)
{
    CHECK_BOARD(iBoardNo);

    m_lstBoardProperty[iBoardNo].iId = iId; 
    return true;
}


