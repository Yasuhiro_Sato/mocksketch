/**
 * @brief			MockDecice ヘッダーファイル
 * @file			MockDecice.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks     デバイス用DLLの基底となるクラス
 *
 *
 * $
 * $
 * 
 */

//TODO: C++インターフェイスによるDLLを作成できるようにする


#ifndef MOCK_DEVICE_H__
#define MOCK_DEVICE_H__


#include <vector>


class MockDevice
{
    
public:
    struct BoardProperty
    {
        StdString strDevName;   // デバイス名
        StdString strReg;       //  登録名
        int       iId;          //  ID

        int       iAiMax;          //  Ai最大数
        int       iAoMax;          //  Ao最大数
        int       iDiMax;          //  Di最大数(bit)
        int       iDoMax;          //  Do最大数(bit)
        int       iCntMax;         //  カウンター最大数
        int       iAiRes;          //  Ai分解能
        int       iAoRes;          //  Ao分解能
    };


public:

    MockDevice();
    ~MockDevice();

    void Init();
    int BoardMax();

    //プロパティ取得
    const BoardProperty& GetBordProperty(int iBoardNo) const;

    //プロパティ設定
    bool  SetBordProperty(int iBoardNo, BoardProperty prop);

    bool  SetId(int iBoardNo, int iId);
    bool  UseBoard(int iBoardNo);
    bool  ReleaseBoard(int iBoardNo);
    bool  GetMaxPage (int* pMax, int iBoardNo);
    //bool    IsMassUpdate     (int   iBoardNo);
    //int     GetMaxUpdateSize (int   iBoardNo);
    /*
    int     GetMaxItem       (int   iBoardNo);
    int     GetMaxPageSize   (int   iBoardNo);

    bool    GetItemRestriction(int* pIoTypeArray,
                               int* pArraySize, 
                               int   iBoardNo,
                               int  iPageNo,
                               int  iItemNo);


     bool    GetItemDef        (int iIoType,
                               int   iBoardNo,
                               int iPageNo,
                               int iItemNo);

     int    GetUserItemMax   (int   iBoardNo);

     bool   GetUserItemName(wchar_t* pItemHeadName,
                           int   iBoardNo,
                           int   iItemCol);

    bool   GetUserItemDef (wchar_t* pItemDef,
                              int   iBoardNo,
                          int      iItemCol);

    bool   SetUserItem    (wchar_t*  pUserItem,
                              int   iBoardNo,
                              int   iPageNo,
                              int   iItemCol,
                              int   iItemNo);

    bool    GetData(void* pVal,
                int iBoardNo,
                int iPageNo,
                int iItemNo,
                int iLength);

    bool    SetData(void* pVal,     
                           int iBoardNo,
                           int PageNo,
                           int ItemNo,
                           int Length);



    bool    Reset (int iBoardNo);               //ボードリセット
    bool    Init  (int iBoardNo);               //ボードイニシャライズ
    bool    Setup (int iBoardNo);               //開始前処理
    bool    Start (int iBoardNo);               //計測開始
    bool    Stop  (int iBoardNo);               //計測停止
    bool    Finish(int iBoardNo);               //終了処理

    bool    SetLang (wchar_t* strLang);               //言語設定

    */




protected:
    bool bInit;

    StdString m_strError;

    std::vector<BoardProperty> m_lstBoardProperty;
};

#endif //MOCK_DEVICE_H__
