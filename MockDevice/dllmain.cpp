// dllmain.cpp : DLL アプリケーションのエントリ ポイントを定義します。
#define MB_DLL_EXPORTS

#include "stdafx.h"
#include <string.h>
#include <stdlib.h>
#include <TCHAR.h>

#include "MockBoard.h"
#include "CAio.h"
#include "MockDevice.h"

MockDevice g_MockDevice;

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
        g_MockDevice.Init();
		break;

    case DLL_THREAD_ATTACH:

        break;

    case DLL_THREAD_DETACH:
		break;

    case DLL_PROCESS_DETACH:
		break;
	}


	return TRUE;
}



DLL_API  bool GetBoardName(wchar_t* pName, int iBoardNo)
{
    return false;
}


DLL_API  bool GetBoardNoMax(int* pMax)
{
    //return g_MockDevice.BoardMax();
    return false;
}

//ボード使用設定
DLL_API  bool  UseBoard(int iBoardNo)
{
    return g_MockDevice.UseBoard(iBoardNo);
}

//ボード開放
DLL_API  bool ReleaseBoard(int iBoardNo)
{
    return  g_MockDevice.ReleaseBoard(iBoardNo);
}

//最大ページ数取得
DLL_API  bool GetMaxPage (int* pMax, int iBoardNo)
{
    return g_MockDevice.GetMaxPage (pMax, iBoardNo);
}

//一括更新の有無
DLL_API  bool IsMassUpdate (bool* pUpdate, int iBoardNo)
{
    //一括更新はない
    *pUpdate = false;
    return true;
}

DLL_API  bool GetMaxUpdateSize (int* pMax, int iBoardNo)
{

    return false;
}


DLL_API  bool GetMaxItem (int* pMax, int iBoardNo)
{
    return false;
}

DLL_API  bool     GetMaxPageSize   (int* pMax, int iBoardNo)
{

    return false;
}

DLL_API  bool    GetItemRestriction(int* pIoTypeArray,
                                   int* pArraySize, 
                                   int  iBoardNo,
                                   int  iPageNo,
                                   int  iItemNo)
{

    return false;
}



DLL_API  bool    GetItemDef        (int iIoType,
                                   int  iBoardNo,
                                   int  iPageNo,
                                   int  iItemNo)
{

    return false;
}

DLL_API  bool    GetUserItemMax   (int* pMax, int  iBoardNo)
{

    return false;
}

DLL_API  bool   GetUserItemName(wchar_t* pItemHeadName,
                               int  iBoardNo,
                               int   iItemCol)
{


    return false;
}

DLL_API bool   GetUserItemDef (wchar_t* pItemDef,
                               int  iBoardNo,
                               int      iItemCol)
{

    return false;
}



DLL_API bool   SetUserItem    (wchar_t*  pUserItem,
                                  int   iBoardNo,
                                  int   iPageNo,
                                  int   iItemCol,
                                  int   iItemNo)
{


    return false;
}

//--------------
// データ取得
//--------------
//データ取得
DLL_API bool    GetData(void* pVal,
                        int iBoardNo,
                        int iPageNo,
                        int iItemNo,
                        int iLength)
{


    return false;
}

    //データ設定
DLL_API bool    SetData(void* pVal,     
                               int iBoardNo,
                               int PageNo,
                               int ItemNo,
                               int Length)
{


    return false;
}


//ボードリセット
DLL_API bool    Reset (int BoardNo)
{

    return false;
}

//ボードイニシャライズ
DLL_API bool    Init  (int BoardNo)
{
    return false;
}

//開始前処
DLL_API bool    Setup (int BoardNo)
{
    return false;
}

//計測開始
DLL_API bool    Start (int BoardNo)
{
    return false;
}

//計測停止
DLL_API bool    Stop  (int BoardNo)
{

    return false;
}

//終了処理
DLL_API bool    Finish(int BoardNo)
{

    return false;
}

//言語設定
DLL_API bool    SetLang (wchar_t* strLang)
{
    return false;
}
