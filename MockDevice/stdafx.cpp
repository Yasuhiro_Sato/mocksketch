// stdafx.cpp : 標準インクルード MockDevice.pch のみを
// 含むソース ファイルは、プリコンパイル済みヘッダーになります。
// stdafx.obj にはプリコンパイル済み型情報が含まれます。

#include "stdafx.h"

// TODO: このファイルではなく、STDAFX.H で必要な
// 追加ヘッダーを参照してください。
void DB_PRINT(const StdChar* pStr, ...)
{
    StdChar szBuf[4096];

    va_list arglist; 
    va_start(arglist, pStr); 
    _vstprintf( szBuf, pStr, arglist);
    va_end(arglist); 

    OutputDebugString(szBuf);

}



/**
  *  @brief   char->StdString変換.
  *  @param   [in] str 入力文字列
  *  @return  出力文字列
  */
StdString CharToString(const char* str)
{
    StdString strRet = _T("");
#ifdef  UNICODE             
  	wchar_t*	psToSemStringBuf;
	int			iToSemStringLen;
    std::string sFromString;
    sFromString = str;
    size_t iConvCnt;

    size_t iLen     =  sFromString.length() + 1 ;
    size_t iBufSize =  iLen * sizeof(wchar_t);
	psToSemStringBuf = (wchar_t*)malloc( iBufSize );
	iToSemStringLen = (int)mbstowcs_s( &iConvCnt, psToSemStringBuf , iLen,
								sFromString.c_str(),
								iLen );
	if ( iToSemStringLen >= 0 )
	{
		strRet = psToSemStringBuf;
	}
	free( psToSemStringBuf );

	return strRet;

#else
     strRet = str;
     return strRet;
#endif
}

/**
  *  @brief   StdString->char 変換.
  *  @param   [in] strString 変換元文字列 
  *  @return               変換後文字列
  */
std::string StringToChar(StdString strString)
{
#ifdef  UNICODE      
    std::string strRet;

  	char*	psToStringBuf;

    size_t iLen = strString.length();
    size_t iRet;

    if (iLen > 0)
    {
	    psToStringBuf = (char*)malloc( iLen * 2 );
        wcstombs_s(&iRet, psToStringBuf, iLen * 2, strString.c_str(), _TRUNCATE);

	    if ( iRet >= 0 )
	    {
		    strRet = psToStringBuf;
	    }
	    free( psToStringBuf );
    }
    return strRet;
    
#else
     return strString;   
#endif

}

StdString StrFormat( const StdChar *format, ... )
{
   va_list vl;
   static const size_t iBufSize = 4096;
   TCHAR buf[iBufSize];

   va_start( vl, format );
   _vsntprintf_s( buf, iBufSize, format, vl );
   StdString ret( buf );
   va_end( vl );
   return ret;
}
