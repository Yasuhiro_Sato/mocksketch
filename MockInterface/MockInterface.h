// MockInterface.h : MockInterface.DLL のメイン ヘッダー ファイル
//

#pragma once

#ifndef __AFXWIN_H__
	#error "PCH に対してこのファイルをインクルードする前に 'stdafx.h' をインクルードしてください"
#endif

#include "resource.h"		// メイン シンボル


// CMockInterfaceApp
// このクラスの実装に関しては MockInterface.cpp を参照してください。
//

class CMockInterfaceApp : public CWinApp
{
public:
	CMockInterfaceApp();

// オーバーライド
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};
