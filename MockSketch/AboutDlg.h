/**
 * @brief			CAboutDlgヘッダーファイル
 * @file			CAboutDlg.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *	アプリケーションのバージョン情報に使われる CAboutDlg ダイアログ
 *
 * $
 * $
 * 
 */

// 
#ifndef _CABOUTDLG_H__
#define _CABOUTDLG_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/

/*---------------------------------------------------*/
/*  class                                            */
/*---------------------------------------------------*/

/**
 * @class   CAboutDlg
 * @brief                        
 */
class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// ダイアログ データ
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート

// 実装
protected:
	DECLARE_MESSAGE_MAP()
};

#endif //_CABOUTDLG_H__