/**
 * @brief			COMMONヘッダーファイル
 * @file			COMMON_HEADt.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef _COMMON_HEAD_H
#define _COMMON_HEAD_H

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
enum E_EXEC
{
    E_STOP   = 0,
    E_START,
    E_PAUSE,
    E_END,
};

enum E_ON_OFF
{
    E_OFF   = 0,
    E_ON,
};

enum E_IN_OUT
{
    E_IN   = 1,
    E_OUT  = 2, 
    E_INOUT  = 3, 
};

enum E_ACTIVE_PASSIVE
{
    E_PASSIVE   = 0,
    E_ACTIVE    = 1, 
};

enum E_LOCAL_WORLD
{
    E_LOCAL   = 0,
    E_WORLD    = 1, 
};

enum COMPILE_STS
{
    CS_NOT_FIND,    //定義が見つからない
    CS_NONE,        //未コンパイル
    CS_CHANGED,     //ファイル変更のため未コンパイル

    CS_SUCCESS,     //コンパイル済み
    CS_SUCCESS_D,   //デバッグモード コンパイル済み
    CS_ERROR,       //コンパイル失敗
};




#endif //_COMMON_HEAD_H
