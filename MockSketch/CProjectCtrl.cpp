/**
 * @brief       CProjcetCtrl実装ファイル
 * @file        CProjcetCtrl.cpp
 * @author      Yasuhiro Sato
 * @date        01-2-2009 10:36:26
 * 
 * @note
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks    アプリケーションメイン
 *
 *
 * $
 * $
 * 
 */
 
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MainFrm.h"
#include "./CProjectCtrl.h"
#include "DefinitionObject/ObjectDef.h"
#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/CFieldDef.h"
#include "DefinitionObject/CElementDef.h"
#include "DefinitionObject/CReferenceDef.h"
#include "View/CUndoAction.h"
#include "View/QUERY_REF_OBJECT.h"
#include "Script/EditorView.h"
#include "Script/CScript.h"
#include "Utility/CUtility.h"
#include "Utility/CIdObj.h"
#include "Utility/CObservationFolder.h"
#include "Utility/CPropertyGroup.h"
#include "System/CSystem.h"
#include "SubWindow/DefaultSetDlg.h"
#include "Mocksketch.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/*---------------------------------------------------*/
/*  Test                                             */
/*---------------------------------------------------*/


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/


CProjectCtrl::PAIR_OBJDEF_TYPE::PAIR_OBJDEF_TYPE(const CObjectDef& def)
{
    strObject   = def.GetName();
    eType       = def.GetObjectType();
    iChgCnt     = def.GetUserPropertySet()->GetChgCnt();
}

//============================================
//============================================
//============================================
//============================================

/**
 * @brief   コンストラクタ
 * @param   なし         
 * @retval  なし
 * @note	
 */
CProjectCtrl::CProjectCtrl():
    m_bChange   (false),
    m_iModuleId (0),
    m_eProjType (VIEW_COMMON::E_PROJ_MAIN),
    m_bSingleFile   (true),
	m_eFileType     (E_BIN)
{

    m_psObsFolder = std::make_shared<CObservationFolder>();
    m_psObsFolder->SetCallbackFunction(_CallbackFileChange, this);

	CMainFrame* pFrame = THIS_APP->GetMainFrame();
	m_pDefautSetDlg = std::make_unique<DefaultSetDlg>(pFrame);

}

/**
 * @brief   コンストラクタ
 * @param   [in]         
 * @retval  なし
 * @note	
 */
CProjectCtrl::CProjectCtrl(const  VIEW_COMMON::E_PROJ_TYPE eProjType):
    m_bChange   (false),
    m_iModuleId (0),
    m_eProjType (eProjType),
    m_bSingleFile   (true),
	m_eFileType(E_BIN)

{
    m_psObsFolder = std::make_shared<CObservationFolder>();
    m_psObsFolder->SetCallbackFunction(_CallbackFileChange, this);

	CMainFrame* pFrame = THIS_APP->GetMainFrame();
	m_pDefautSetDlg = std::make_unique<DefaultSetDlg>(pFrame);
}


/**
 * @brief   デストラクタ
 * @param   なし         
 * @retval  なし
 * @note	
 */
CProjectCtrl::~CProjectCtrl()
{
    StdPath path = m_strFileDir;

    if (path.has_parent_path())
    {
        StdPath pathParent  = path.parent_path();
        SYS_PROPERTY->UnloadDir(pathParent, true);
    }
    m_psObsFolder->Stop();
}

/**
 * @brief   プロジェクト名の取得
 * @param   なし         
 * @retval  プロジェクト名
 * @note	
 */
StdString CProjectCtrl::GetName() const
{
    return m_Name;
}

/**
 * @brief   プロジェクト名の設定
 * @param   [in] strName
 * @retval  なし
 * @note
 */
bool CProjectCtrl::SetName(StdString strName)
{
     //変数名チェック
    if(!CUtil::IsIdentifer(strName))
    {
        return false;
    }

    //ファイルに使用できる名称
    m_Name = strName;


    //ツリー変更
    int iRoot = m_treeFolder.GetRootId();
    VIEW_COMMON::OBJ_TREE_DATA* pTreeData;
    pTreeData =  m_treeFolder.GetItem(iRoot);

    if (pTreeData)
    {
        m_treeFolder.SetTreeValues(m_Name, VIEW_COMMON::OBJ_ROOT, iRoot);
    }
    return true;
}

/**
 * @brief   ファイルパスの取得
 * @param   なし
 * @retval  ファイルパス(ファイル名も含む)
 * @note    
 */
StdString CProjectCtrl::GetFilePath() const
{
    StdString strFilePath;
    strFilePath = m_strFileDir;
    strFilePath += _T("\\");
    strFilePath += m_Name;

	if (m_eFileType == E_BIN)
	{
		strFilePath += _T(".msp");
	}
	else if (m_eFileType == E_XML)
	{
		strFilePath += _T(".mspx");
	}
	else if (m_eFileType == E_TXT)
	{
		strFilePath += _T(".mspt");
	}
	return strFilePath;
}

/**
 * @brief   ディレクトリの取得
 * @param   なし
 * @retval  ディレクトリ(後端に\はつかない)
 * @note    
 */
StdString CProjectCtrl::GetDir() const
{
    try
    {
        return m_strFileDir;
    }
    catch(...)
    {
        return _T("");
    }
}


/**
 * @brief   変更の有無
 * @param   なし
 * @retval  true 変更あり
 * @note    保存時からのプロジェクトの変更を検出
 */
bool CProjectCtrl::IsChange() const
{
    return m_bChange;
}

/**
 * @brief   メインオブジェクトの取得
 * @param   なし
 * @retval  メインオブジェクト
 * @note	
 */
/*
CPartsDef*  CProjectCtrl::GetMainObject()
{
    return  m_psMainPartsDef;
}
*/

/**
 * @brief   フォルダーデータ取得
 * @param   なし
 * @retval  フォルダーデータ
 * @note	
 */
TREE_DATA<VIEW_COMMON::OBJ_TREE_DATA>* CProjectCtrl::GetFolderData() 
{
    return &m_treeFolder;
}


/**
 * @brief   新プロジェクト生成
 * @param   [in] strProject    プロジェクト名
 * @param   [in] bCreteAssy    ダミーアセンブリの生成
 * @param   [in] bXml          XML形式
 * @param   [in] strDir        プロジェクトディレクトリ
 * @retval  true  成功
 * @note    strDirにはファイル名を含めない
 */
bool CProjectCtrl::CreateNewProject(StdString strProject, 
                                    bool bCreteAssy,
                                	bool bXml,
                                    StdString strDir)
{
    namespace fs = boost::filesystem;

    m_psObsFolder->Stop();

    Clear();
    //NULLファイル読み込み設定で初期化にする
    //初期化する


     //変数名チェック
    if(!CUtil::IsIdentifer(strProject))
    {
        MockException(e_no_use_id);
        return false;
    }

    m_Name = strProject;

    
    //保存パス
    if (strDir == _T(""))
    {
        strDir  = SYS_CONFIG->strPathData;
    }

    //TODO:ファイル名チェック
    m_strFileDir = strDir;
    std::shared_ptr<CObjectDef> pDef;

    //Tree生成
    using namespace VIEW_COMMON;
    OBJ_TREE_DATA  objData;

    objData.uuidDef = boost::uuids::nil_uuid();
    objData.iScriptNo = -1;

    int iRoot = m_treeFolder.AddData(-1, objData);
    m_treeFolder.SetTreeValues(m_Name, OBJ_ROOT, iRoot);

	if (bXml)
	{
		m_eFileType = E_XML;
	}
	else
	{
		//m_eFileType = E_TXT;
		m_eFileType = E_BIN;
	}

    if (bCreteAssy)
    {
        pDef = CreateParts(SYS_CONFIG->strDefaultMainName, VIEW_COMMON::E_PARTS, iRoot).lock();
        if (!pDef)
        {
            return false;
        }

        StdPath pathDef;
        pathDef = m_strFileDir;
        pathDef /= pDef->GetName();

        if (!fs::exists(pathDef))
        {
            fs::create_directories(pathDef);
        }
		if (m_eFileType == E_BIN)
		{
			pathDef /= (pDef->GetName() + _T(".msd"));
		}
		else if (m_eFileType == E_XML)
		{
			pathDef /= (pDef->GetName() + _T(".msdx"));
		}
		else if (m_eFileType == E_TXT)
		{
			pathDef /= (pDef->GetName() + _T(".msdt"));
		}

		pDef->Save(pathDef);
    }
    //!<ファイル変更監視開始
    m_psObsFolder->Start(m_strFileDir);

    m_bChange = false;
    return true;
}

/**
 * @brief   ファイル読み込み
 * @param   Path ファイルへのパス
 * @retval  true 読み込み成功
 * @note	
 */
bool CProjectCtrl::Load(StdPath Path)
{

    if (Path.filename() == _T(""))
    {
        return CreateNewProject(_T(""),     // strProject
			                      true,     // bCreteAssy
			                      false,    // bXml
			                      _T(""));  // strDir
    }

    if (!boost::filesystem::exists(Path))
    {
        return false;
    }

    //!<ファイル変更監視停止
    m_psObsFolder->Stop();

    try
    {
        m_strFileDir = GetPathStr(Path.parent_path());
        StdString strExt = GetPathStr(Path.extension());
        strExt = CUtil::ToUpper(strExt);

        if (strExt == _T(".MSP") )
        {
            boost::filesystem::ifstream inFs(Path, std::ios::binary);
            {
                boost::archive::binary_iarchive inBin(inFs);
                inBin >> *this;
            }
            inFs.close();
			SetFileType(E_BIN);
        }
		else if (strExt == _T(".MSPT"))
		{
			StdStreamIn  inFs(Path);
			{
				boost::archive::text_wiarchive txtIn(inFs);
				txtIn >> *this;
			}
			inFs.close();
			SetFileType(E_TXT);
		}
		else if (strExt == _T(".MSPX") )
        {
            StdStreamIn inFs(Path);
            {
                StdXmlArchiveIn inXml(inFs);
                inXml >> boost::serialization::make_nvp("Root", *this);
            }
            inFs.close();
			SetFileType(E_XML);
		}
        else
        {
            m_psObsFolder->Start(m_strFileDir);
            return false;
        }
    }
    catch(boost::system::system_error &e)
    {
        STD_DBG(_T("File error %s"), e.what());
        m_psObsFolder->Start(m_strFileDir);
        return false;
    }
    catch(...)
    {
        STD_DBG(_T("Unknoen error"));
        m_psObsFolder->Start(m_strFileDir);
        return false;
    }

    //m_strFilePath = GetPathStr(Path);
    m_bChange = false;
    SYS_PROPERTY->UnloadDir(Path, true);

    SYS_PROPERTY->LoadDir(Path, true);

    LoadAfter();

    //ロード完了
    ::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, 
        WM_VIEW_LOAD_END, (WPARAM)this, (LPARAM)m_eProjType);

    //!<ファイル変更監視開始
    m_psObsFolder->Start(m_strFileDir);

    return true;
}

/**
 * @brief   ファイル保存
 * @param   Path ファイルへのパス          
 * @retval  true 保存み成功
 * @note	
 */
bool CProjectCtrl::Save(StdPath Path)
{

    m_psObsFolder->Stop();

    StdString strPathOld = m_strFileDir;

    m_strFileDir = GetPathStr(Path.parent_path());

    //拡張子がXMLの時はXML形式で保存する
    try
    {
        StdString strExt = GetPathStr(Path.extension());
        strExt = CUtil::ToUpper(strExt);
        if (strExt == _T(".MSP") )
        {
			boost::filesystem::ofstream outFs(Path, std::ios::binary);
            {
                boost::archive::binary_oarchive outBin(outFs);
                outBin << *this;
            }
			outFs.close();
			SetFileType(E_BIN);
		}
		else if (strExt == _T(".MSPT"))
		{
			StdStreamOut outFs(Path);
			{
				boost::archive::text_woarchive txtOut(outFs);
				txtOut << *this;
			}
			outFs.close();
			SetFileType(E_TXT);
		}
		else if (strExt == _T(".MSPX") )
        {
            StdStreamOut outFs(Path);
            {
                StdXmlArchiveOut outXml(outFs);
                outXml << boost::serialization::make_nvp("Root", *this);
            }
            outFs.close();
			SetFileType(E_XML);
		}
        else
        {
            m_strFileDir = strPathOld;
            m_psObsFolder->Start(m_strFileDir);
            return false;
        }
    }
    catch(...)
    {
        m_strFileDir = strPathOld;
        m_psObsFolder->Start(m_strFileDir);
        return false;
    }
    m_bChange = false;
    m_psObsFolder->Start(m_strFileDir);
    return true;
}

/**
 * @brief   全オブジェクトの保存
 * @param   なし   
 * @retval  true 保存成功
 * @note	
 */
bool CProjectCtrl::SaveAllObject()
{
    std::map<boost::uuids::uuid, PAIR_OBJDEF_TYPE>::iterator iteMap;

    PAIR_OBJDEF_TYPE* pType;
    for(iteMap  = m_mapObjectDefId.begin();
        iteMap != m_mapObjectDefId.end(); 
        iteMap++)
    {
        pType = &iteMap->second;
        if(!pType->pDef)
        {
            continue;
        }

        //TODO:変更分のみを保存
        /*
        if(!pType->pDef->IsChange())
        {
            continue;
        }
        */

        _SaveObjectDef(pType->pDef.get());

    }
    return true;
}


/**
 * @brief   ロード終了後処理
 * @param   なし
 * @retval  なし
 * @note	
 */
void CProjectCtrl::LoadAfter()
{
}


/**
 * @brief   部品数の取得
 * @param   なし
 * @retval  部品数の取得
 * @note	
 */
int CProjectCtrl::GetPartsNum() const
{
    int iSize = int(m_mapObjectDefId.size());
    return iSize;
}

/**
 * @brief   部品名称チェック(使用可否)
 * @param   [in] 部品名
 * @retval  true 使用可能
 * @note	
 */
bool CProjectCtrl::IsPossiblePartsName(StdString strName)  const
{
    bool bRet;
    bRet = CUtil::IsIdentifer(strName);
    if (bRet)
    {
        //TAG:[予約語チェック]
    }
    return bRet;
}

/**
 * @brief   部品名称チェック(重複)
 * @param   [in] 部品名
 * @retval  true 重複あり
 * @note	
 */
bool CProjectCtrl::IsDupPartsName(StdString strName) const
{

    if (m_Name == strName)
    {
        return true;
    }

    std::map<StdString , boost::uuids::uuid>::const_iterator ite;
    
    ite = m_mapUuid.find(strName);
    if (ite == m_mapUuid.end())
    {
        return false;
    }
    return true;
}


/**
 * @brief   部品取得
 * @param   部品名
 * @retval  部品の取得
 * @note	
 */
std::weak_ptr<CObjectDef> CProjectCtrl::GetPartsByName(StdString strName)
{
    std::weak_ptr<CObjectDef> pRet;
    std::map<StdString , boost::uuids::uuid>::const_iterator ite;
    ite = m_mapUuid.find(strName);

    if (ite == m_mapUuid.end())
    {
        return pRet;
    }


    pRet = GetParts((*ite).second);
    return pRet;
}

/**
 * @brief   部品取得
 * @param   部品ID
 * @retval  部品の取得
 * @note    必要になった時点でファイルを読み込む
 */
std::weak_ptr<CObjectDef> CProjectCtrl::GetParts(boost::uuids::uuid uuidParts)
{
    std::weak_ptr<CObjectDef> pRet;
    std::map<boost::uuids::uuid, PAIR_OBJDEF_TYPE>::iterator ite;
    ite = m_mapObjectDefId.find(uuidParts);
    if (ite == m_mapObjectDefId.end())
    {
        return pRet;
    }

    PAIR_OBJDEF_TYPE* pPair;
    pPair = &(*ite).second;

    std::shared_ptr<CObjectDef> pDef;

    if(pPair->pDef.get() == NULL)
    {
        if(!_LoadObjectDef(&pPair->pDef, pPair))
        {
            return pRet;
        }
    }

    pRet = pPair->pDef;
    return pRet;
}


/**
 * @brief   部品読み込み済み確認
 * @param   [in] uuidParts 部品ID
 * @param   [in] strScript スクリプト名
 * @retval  true 読み込み済み
 * @note    
 */
bool CProjectCtrl::IsChange(boost::uuids::uuid uuidParts,
                                                   StdString strScript) const
{
    //TODO:バイナリSAVE機能がついたら SDS_COMPILEDの情報をPAIR_OBJDEF_TYPEに追加すること

    std::weak_ptr<CObjectDef> pRet;
    std::map<boost::uuids::uuid, PAIR_OBJDEF_TYPE>::const_iterator ite;

    ite = m_mapObjectDefId.find(uuidParts);
    if (ite == m_mapObjectDefId.end())
    {
        return false;
    }

    const PAIR_OBJDEF_TYPE* pPair;
    pPair = &(*ite).second;

    if(pPair->pDef.get() == NULL)
    {
        // 定義が読み込まれていない状態では保存済みのはず
        return false;
    }

    pRet = pPair->pDef;

    if( std::shared_ptr<CObjectDef> tmp = pRet.lock() )
    {
        if (strScript.empty())
        {
           return tmp->IsChange();
        }
        else
        {
            CScriptData* pScript;
            pScript = tmp->GetScriptInstance(strScript);
            STD_ASSERT(pScript != NULL);
            if (pScript)
            {
                return pScript->IsChange();
            }
        }
        STD_DBG(_T("Script %s is not find."), strScript.c_str());
    }
    else
    {
        STD_DBG(_T("CObjectDef can't lock."));
    }

    return false;
}

/**
 * @brief   保存の問い合わせ
 * @param   なし
 * @retval  true 保存する
 * @note    
 */
bool CProjectCtrl::InquireSave() const
{
    //TODO:実装
    // 以下のファイルが変更されています
    //     ・・・・・・・・・
    //     ・・・・・・・・・
    //     ・・・・・・・・・
    //               保存しますか？

    //_T("データが変更されています。\n保存しますか？");
    int iAns = AfxMessageBox(GET_STR(STR_MB_SAVE),MB_OKCANCEL);


    if (iAns == IDOK){return true;}
    return false;

}


/**
 * @brief   コンパイルの必要性問い合わせ
 * @param   [in] uuidParts 部品ID
 * @retval  true 読み込み済み
 * @note    
 */
COMPILE_STS CProjectCtrl::GetCompileStatus(boost::uuids::uuid uuidParts) const
{
    //TODO:バイナリSAVE機能がついたら SDS_COMPILEDの情報をPAIR_OBJDEF_TYPEに追加すること

    std::weak_ptr<CObjectDef> pRet;
    std::map<boost::uuids::uuid, PAIR_OBJDEF_TYPE>::const_iterator ite;

    ite = m_mapObjectDefId.find(uuidParts);
    if (ite == m_mapObjectDefId.end())
    {
        STD_DBG(_T("Parts isn't find."));
        return CS_NOT_FIND;
    }

    const PAIR_OBJDEF_TYPE* pPair;
    pPair = &(*ite).second;

    if(pPair->pDef.get() == NULL)
    {
        // 定義が読み込まれていない状態ではコンパイルは行われていない
        return CS_NONE;
    }

    return pPair->pDef->GetCompileStatus();

}
/**
 * @brief   部品ID取得
 * @param   [in] strName 部品名
 * @retval  部品ID
 * @note    
 */
boost::uuids::uuid CProjectCtrl::GetPartsIdByName(StdString strName) const
{

    std::weak_ptr<CObjectDef> pRet;
    std::map<StdString , boost::uuids::uuid>::const_iterator ite;
    ite = m_mapUuid.find(strName);

    if (ite == m_mapUuid.end())
    {
        return boost::uuids::nil_uuid();
    }
    return ite->second;
}

/**
 * @brief   部品変更カウント取得
 * @param   [in] uuidParts 部品ID
 * @retval  変更カウント
 * @note    
 */
int CProjectCtrl::GetPartsChangeCount(boost::uuids::uuid uuidParts) const
{
    std::map<boost::uuids::uuid, PAIR_OBJDEF_TYPE>::const_iterator ite;
    ite = m_mapObjectDefId.find(uuidParts);

    if (ite == m_mapObjectDefId.end())
    {
        return -1;
    }

    const PAIR_OBJDEF_TYPE* pPair;
    pPair = &(*ite).second;

    if (!pPair->pDef)
    {
        return pPair->iChgCnt;
    }

    return pPair->pDef->GetUserPropertySet()->GetChgCnt();
}

//!< 部品アンロード
void CProjectCtrl::UnloadParts(boost::uuids::uuid uuidParts)
{
    std::map<boost::uuids::uuid, PAIR_OBJDEF_TYPE>::iterator ite;
    ite = m_mapObjectDefId.find(uuidParts);

    if (ite == m_mapObjectDefId.end())
    {
        return;
    }

    PAIR_OBJDEF_TYPE* pPair;
    pPair = &(*ite).second;

    if (pPair->pDef)
    {
        pPair->iChgCnt = pPair->pDef->GetUserPropertySet()->GetChgCnt();
        _SaveObjectDef(pPair->pDef.get());
        pPair->pDef.reset();
    }
}

/**
 * @brief   部品登録
 * @param   [in] pDef  
 * @param   [in] iTreeId 挿入位置 (Treeに登録しない時は-1)
 *          ROOTは0,Project直下は1
 * @retval  true 登録成功 false 登録失敗
 * @note
 */
bool CProjectCtrl::AddParts( std::shared_ptr<CObjectDef> pDef,
                             int iTreeId)
{
    using namespace VIEW_COMMON;

    PAIR_OBJDEF_TYPE    pairObj;
    pairObj.eType       = pDef->GetObjectType();
    pairObj.strObject   = pDef->GetName();
    pairObj.iChgCnt     = 0;
    pairObj.pDef        = pDef;

    pDef->SetProject(this);
    m_mapObjectDefId[pDef->GetPartsId()] = pairObj;
    m_mapUuid[pairObj.strObject] = pDef->GetPartsId();

    //--------------------
    // Treeへの登録
    //--------------------

    OBJ_TREE_DATA* pTreeData = NULL;
    int iObjId = -1;
    OBJ_TREE_DATA  datObj;


    if (iTreeId == -1)
    {
        return true;
    }

    pTreeData = m_treeFolder.GetItem(iTreeId);


    StdString strSelName;
    int  iSelImage;
    bool bSelExpand;
    bool bRet;

    datObj.uuidDef = pDef->GetPartsId();
    datObj.iScriptNo = -1;

    bRet = m_treeFolder.GetTreeValues(&strSelName,
                       &iSelImage,
                       &bSelExpand,
                       iTreeId);
 

    if (!bRet)
    {
        return false;
    }

    switch(iSelImage)
    {
    case OBJ_ROOT:
    case OBJ_FOLDER:
        iObjId = m_treeFolder.AddChild(iTreeId, datObj);

        break;

    case OBJ_PARTS:
    case OBJ_ELEMENT:
    case OBJ_FIELD:
    case OBJ_REFERENCE:

        iObjId = m_treeFolder.AddData(iTreeId, datObj);
        break;

    case OBJ_SCRIPT:  //!< スクリプト  
    default:

        break;
    }


    if (iObjId != -1)
    {
        CScriptData*   pScript;

        int iScriptId;
        StdString strObj = pDef->GetName();
        E_ITEM_TYPE eItem = DefToItem(pDef->GetObjectType());
        m_treeFolder.SetTreeValues(strObj, eItem, iObjId);

        OBJ_TREE_DATA  datScript;
        datScript.uuidDef = pDef->GetPartsId();
        datScript.iScriptNo = -1;

        int iNum;
        iNum = pDef->GetScriptNum();

        for (int nPos = 0; nPos < iNum; nPos++)
        {
            pScript = pDef->GetScriptInstance(nPos);
            datScript.iScriptNo = nPos;

            if (nPos == 0)
            {
                iScriptId = m_treeFolder.AddChild(iObjId, datScript);
            }
            else
            {
                iScriptId = m_treeFolder.AddData(iScriptId, datScript);
            }
            m_treeFolder.SetTreeValues(pScript->GetName(), OBJ_SCRIPT, iScriptId);
        }
    }

    if (!m_bChange)
    {
        ::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_CHG_OBJECT_STS, 
                            (WPARAM)0, (LPARAM)0);
        m_bChange = true;
    }
    return true;
}

/**
 * @brief   部品の作成
 * @param   [in] strName 部品名
 * @param   [in] eType   部品名種別
 * @param   [in] iTreeId 挿入位置 (Treeに登録しない時は-1)
 *          ROOTは0,Project直下は1
 * @retval  true 作成成功 false 作成失敗
 * @note	通常はObjectWndから部品を生成する
 */
std::weak_ptr<CObjectDef> CProjectCtrl::CreateParts(
                     StdString strName, 
                     VIEW_COMMON::E_OBJ_DEF_TYPE eType,
                     int iTreeId)
{
    using namespace VIEW_COMMON;

    std::weak_ptr<CObjectDef> pRet;
    if (!GetPartsByName(strName).expired())
    {
        //同じ名称がある
        return pRet;
    }

    
    std::shared_ptr<CObjectDef> pDef;
    CUndoAction* pUndo = NULL;
    bool bScript = true;
    // IDは内部で付加される
    if (eType == E_PARTS)
    {
        pDef = std::make_shared<CPartsDef>(strName);
    }
    else if (eType == E_FIELD)
    {
        pDef = std::make_shared<CFieldDef>(strName);
    }
    else if (eType == E_ELEMENT)
    {
        pDef = std::make_shared<CElementDef>(strName);
    }
    else if (eType == E_REFERENCE)
    {
        pDef = std::make_shared<CReferenceDef>(strName);
        bScript = false;
    }

    if (pDef)
    {
        //!< スクリプト生成
        if (bScript)
        {
            pDef->CreateScript(strName);
        }
        AddParts( pDef, iTreeId);
    }

    pRet = pDef;

    if (!m_bChange)
    {
        ::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_CHG_OBJECT_STS, 
                            (WPARAM)0, (LPARAM)0);
        m_bChange = true;
    }

    return pRet;
}

/**
 * @brief   部品の削除
 * @param   [in]  strName        部品名
 * @param   [in]  bForceDelete   強制削除の有無
 * @retval  true 削除成功 false 削除失敗
 * @note    他で使用中であっても削除される
 */
bool CProjectCtrl::DeleteParts(boost::uuids::uuid uuidParts)
{
   //部品削除通知
    SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_VIEW_PARTS_DELETE, (WPARAM)&uuidParts, 0);

    CUndoAction* pUndo = NULL;

    std::map<boost::uuids::uuid, PAIR_OBJDEF_TYPE>::iterator iteIdMap;
    std::map<StdString , boost::uuids::uuid>::iterator iteUuidMap;

    iteIdMap = m_mapObjectDefId.find(uuidParts);
    PAIR_OBJDEF_TYPE*       pPair;
    if (iteIdMap != m_mapObjectDefId.end())
    {
        int iTreeId;
        iTreeId = _FindTreeId(uuidParts);

        pPair = &(iteIdMap->second);
        iteUuidMap = m_mapUuid.find(pPair->strObject);
        if (iteUuidMap != m_mapUuid.end())
        {
            m_mapUuid.erase(iteUuidMap);
        }
        else
        {
            STD_DBG(_T("m_mapUuid is not find (%s)"), pPair->strObject.c_str());
        }

        m_mapObjectDefId.erase(iteIdMap);
        m_treeFolder.Delete(iTreeId);

        if (!m_bChange)
        {
            ::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_CHG_OBJECT_STS, 
                                (WPARAM)0, (LPARAM)0);
            m_bChange = true;
        }
        return true;    
    }
    return false;
}


/**
 * @brief   部品名変更
 * @param   [in] 元名称
 * @param   [in] コピー先名称
 * @retval  true 成功
 * @note	
 */
bool CProjectCtrl::ChangePartsName(StdString  strOld, 
                                   StdString  strNew)
{
    CUndoAction* pUndo = NULL;

    if (!GetPartsByName(strNew).expired())
    {
        //同じ名称がある
        return false;
    }

    std::shared_ptr<CObjectDef> pDef;
    pDef = GetPartsByName(strOld).lock();
    if (!pDef)
    {
        return false;
    }

    std::map<StdString , boost::uuids::uuid>::iterator iteUuidMap;
    iteUuidMap = m_mapUuid.find(strOld);
    if (iteUuidMap == m_mapUuid.end() )
    {
        return false;
    }

    std::map<boost::uuids::uuid, PAIR_OBJDEF_TYPE>::iterator iteIdMap;
    iteIdMap = m_mapObjectDefId.find(pDef->GetPartsId());
    if (iteIdMap == m_mapObjectDefId.end() )
    {
        return false;
    }

    int iTreeId;
    iTreeId = _FindTreeId(iteUuidMap->second);

    m_mapUuid.erase(iteUuidMap);
    m_mapUuid[strNew] = pDef->GetPartsId();

    iteIdMap->second.strObject = strNew;
    pDef->SetName(strNew);
    m_treeFolder.SetName(iTreeId, strNew);

    return true;
}

/**
 * @brief   部品コピー
 * @param   [in] uuidDef コピー元uuid
 * @param   [in] iTreeId コピー先TreeId
 * @param   [in] strDst  コピー先名称
 * @retval  true 成功
 * @note    strDst 未設定時は、自動で名前をつける
 */
bool CProjectCtrl::CopyParts(boost::uuids::uuid uuidDef , 
                             int iTreeId, 
                             StdString strDst)
{
    std::shared_ptr<CObjectDef> pDefSrc;
    std::shared_ptr<CObjectDef> pDefNew;

    pDefSrc = THIS_APP->GetObjectDefByUuid(uuidDef).lock();

    if (strDst != _T(""))
    {
        if (!GetPartsByName(strDst).expired())
        {
            //同じ名称がある
            return false;
        }
    }
    else
    {
        StdString strSrc = pDefSrc->GetName();
        strDst = strSrc + _T("_Copy");

        std::vector<StdString> lstName;
        std::vector<StdString>::iterator ite;

        GetPartsNameList(&lstName);

        using boost::lambda::_1;

        ite = std::find_if(lstName.begin(), lstName.end(), _1 == strDst);

        if (ite != lstName.end())
        {
            strDst = CUtil::FindNumberingString( &lstName, strDst, 4);
        }

    }

    pDefNew = CObjectDef::Clone(pDefSrc.get(), strDst);

    if (!pDefNew)
    {
        return false;
    }

    //pDefNew->SetName(strCopy);

    AddParts( pDefNew, iTreeId);


    return true;
}


/**
 * @brief   部品の移動
 * @param   [in]  pDefSrc        移動元
 * @retval  true 移動成功 false 移動失敗
 * @note    同一プロジェクトの移動に使用
 *             
 */
bool CProjectCtrl::MoveParts(std::shared_ptr<CObjectDef> pDefSrc,
    int iTreeDst)
{
    using namespace VIEW_COMMON;
    int iTreeSrc = _FindTreeId(pDefSrc->GetPartsId());

    DBG_ASSERT(iTreeSrc != -1);
    if (iTreeSrc == -1)
    {
        return false;
    }

    bool bRet;
    StdString strSelName;
    int  iSelImage;
    bool bSelExpand;

    bRet = m_treeFolder.GetTreeValues(&strSelName,
                       &iSelImage,
                       &bSelExpand,
                       iTreeDst);
 

    DBG_ASSERT(bRet);
    if (!bRet)
    {
        return false;
    }

    switch(iSelImage)
    {
    case OBJ_ROOT:
    case OBJ_FOLDER:
        bRet = m_treeFolder.MoveChild( iTreeSrc, iTreeDst);

        break;

    case OBJ_PARTS:
    case OBJ_ELEMENT:
    case OBJ_FIELD:
    case OBJ_REFERENCE:

        bRet = m_treeFolder.Move( iTreeSrc, iTreeDst);
        break;

    case OBJ_SCRIPT:  //!< スクリプト  
    default:
        bRet = false;
        DBG_ASSERT(bRet);
        break;
    }

    DBG_ASSERT(bRet);
    return bRet;
}

/**
 * @brief   部品名リスト取得
 * @param   [out] pLstName
 * @retval  ture 成功
 * @note	
 */
bool CProjectCtrl::GetPartsNameList(std::vector<StdString>* pLstName) const
{
    if (!pLstName)
    {
        return false;
    }
    pLstName->clear();

    StdString strRet;
    std::map<StdString , boost::uuids::uuid>::const_iterator ite;

    pLstName->push_back(m_Name);

    for( ite  = m_mapUuid.begin();
         ite != m_mapUuid.end();
         ite++)
    {
        pLstName->push_back(ite->first);
    }
    return true;
}

/**
 * @brief   新部品名問い合わせ
 * @param   なし
 * @retval  新部品名
 * @note	
 */
StdString CProjectCtrl::QueryNewPartsName(VIEW_COMMON::E_OBJ_DEF_TYPE eType) const
{
    StdString strRet;
    std::vector<StdString> lstName;

    GetPartsNameList(&lstName);

    using namespace VIEW_COMMON;
    StdString strDefaultName;
    switch(eType)
    {
    case E_PARTS:       strDefaultName = SYS_CONFIG->strDefaultPartsName; break;
    case E_REFERENCE:   strDefaultName = SYS_CONFIG->strDefaultReferenceName; break;
    case E_FIELD:       strDefaultName = SYS_CONFIG->strDefaultFieldName; break;   
    default:
        break;
    }

    strRet = CUtil::FindNumberingString( &lstName, strDefaultName, 4);
    return strRet;
}

/**
 * @brief   パーツID作成
 * @param   なし
 * @retval  新ID
 * @note	
 */
/*
int CProjectCtrl::CreatePartsId()
{
    return m_psIdCtrl->CreateId();

}
*/

/**
 * @brief   IDマップ生成
 * @param   なし          
 * @retval  なし
 * @note	
 */
void CProjectCtrl::_CreateIdMap()
{
    m_mapUuid.clear();

    std::pair<boost::uuids::uuid, PAIR_OBJDEF_TYPE> pairId;
    foreach(pairId, m_mapObjectDefId)
    {
        m_mapUuid[pairId.second.strObject] = pairId.first;
    }
}

/**
 * @brief   TreeId検索
 * @param   [in] uuidObj 検索対象         
 * @retval  treeid
 * @note	
 */
 int CProjectCtrl::_FindTreeId(boost::uuids::uuid uuidObj) const
{
     std::map<int, 
         TREE_DATA<VIEW_COMMON::OBJ_TREE_DATA>::TREE_ITEM>::
         const_iterator  iteTree;
     
     for(iteTree  = m_treeFolder.mapTree.begin();
         iteTree != m_treeFolder.mapTree.end();
         iteTree++)
     {

        if(iteTree->second.data.uuidDef == uuidObj)
        {
            return iteTree->first;
        }
     }

     return -1;
}



/**
 * @brief   ファイル変更監視コールバック
 * @param   [in] str
 * @param   [in] pVoid
 * @retval  なし
 * @note	
 */
void CProjectCtrl::_CallbackFileChange(StdString strFileName, void* pVoid)
{
    CProjectCtrl* pCtrl = reinterpret_cast<CProjectCtrl*>(pVoid);
    StdPath Path(strFileName);

    bool bSimulation = true;
    if (THIS_APP->ReloadEditorFile(Path, bSimulation))
    {
        StdString strMessage;
        strMessage = CUtil::StrFormat(GET_STR(STR_DLG_CHG_FILE_s), strFileName.c_str());
        int iAns = AfxMessageBox(strMessage.c_str(), MB_YESNO);;
        if (iAns != IDYES)
        {
            return;
        }

        bSimulation = false;
        THIS_APP->ReloadEditorFile(Path, bSimulation);
    }
}

/**
 * @brief   モジュールID取得
 * @param   なし          
 * @retval  モジュールID
 * @note    CScriptModule::Compileのモジュール名
 *          設定に使用
 */
int CProjectCtrl::GetModuleId()
{
    return m_iModuleId;
}

/**
 * @brief   モジュールID加算
 * @param   なし          
 * @param   なし          
 * @note    CScriptModule::Compileのモジュール名
 *          設定に使用
 */
void CProjectCtrl::AddModuleId()
{
    m_iModuleId++;
    if (m_iModuleId == INT_MAX)
    {
        m_iModuleId = 0;
    }
}

/**
 * @brief   オブジェクト問い合わせ
 * @param   [in] pQuery 問い合わせデータ
 * @param   なし
 * @retval  オブジェクト
 * @note	
 */
CDrawingObject* CProjectCtrl::QueryObject(const QUERY_REF_OBJECT* pQuery)
{
	auto pCtrl = QueryDef(pQuery);
	int   iObject = pQuery->iObjectId;

	if (!pCtrl)
	{
		return NULL;
	}
    CDrawingObject* pRet = NULL;

    if (iObject == -1)
    {
        //実際はダウンキャスト
        pRet = reinterpret_cast<CDrawingObject*>(pCtrl->GetDrawingParts().get());
    }
    else
    {
        pRet = pCtrl->GetObjectById(iObject).get();
    }
    return pRet;
}


CPartsDef* CProjectCtrl::QueryDef(const QUERY_REF_OBJECT* pQuery)
{
	STD_ASSERT(pQuery);

	if (pQuery == NULL)
	{
		return NULL;
	}

	boost::uuids::uuid  uuidParts = pQuery->uuidParts;

	std::shared_ptr<CObjectDef>  pDef;

	pDef = GetParts(uuidParts).lock();

	if (!pDef)
	{
		return NULL;
	}

	CPartsDef* pCtrl = NULL;
	if ((pDef->GetObjectType() == VIEW_COMMON::E_PARTS) ||
		(pDef->GetObjectType() == VIEW_COMMON::E_FIELD) ||
		(pDef->GetObjectType() == VIEW_COMMON::E_REFERENCE)
		)
	{
		pCtrl = dynamic_cast<CPartsDef*>(pDef.get());
	}

	if (pCtrl == NULL)
	{
		return NULL;
	}

	return pCtrl;
}

/**
 * @brief   クリア
 * @param   なし
 * @retval  なし
 * @note	テスト用
 */
void CProjectCtrl::Clear()
{
    m_treeFolder.Clear();
    m_mapUuid.clear();
    m_mapObjectDefId.clear();
    m_Name = _T("");
}

/**
 * @brief   ファイル書き込みモード設定
 * @param   [in] bSingle 
 * @retval  なし
 * @note    書き込み、読み込み前に設定する
 */
void CProjectCtrl::SetSingleFileMode(bool bSingle)
{
    m_bSingleFile = bSingle;
}


/**
 * @brief   オブジェクトロード
 * @param   [out]  ppDef
 * @param   [in]   strObjcetDef
 * @retval  true   読み込み成功
 * @note	
 */
bool CProjectCtrl::_LoadObjectDef(std::shared_ptr<CObjectDef>* ppDef,
                                 const PAIR_OBJDEF_TYPE* pPair)
{
    using namespace VIEW_COMMON;
    namespace fs = boost::filesystem;
    StdPath  path(GetDir());
    StdPath  pathDir;
    StdString strFile;
    StdString strError;

    std::shared_ptr<CObjectDef> pDef;

    try 
    {
        pathDir = path /  pPair->strObject;
        if (!fs::exists(pathDir))
        {
            return false;
        }

		if (m_eFileType == E_BIN)
		{
			strFile = pPair->strObject + _T(".msd");
		}
		else if (m_eFileType == E_XML)
		{
			strFile = pPair->strObject + _T(".msdx");
		}
		else if (m_eFileType == E_TXT)
		{
			strFile = pPair->strObject + _T(".msdt");
		}
		pathDir =  pathDir / strFile;

        switch(pPair->eType)
        {
        case E_PARTS:    { pDef = std::make_shared<CPartsDef>(); break;}
        case E_FIELD:       { pDef = std::make_shared<CFieldDef>(); break;}
        case E_ELEMENT:     { pDef = std::make_shared<CElementDef>(); break;}
        case E_REFERENCE:   { pDef = std::make_shared<CReferenceDef>(); break;}
        default:  break;
        }

        if (!pDef)
        {
            StdStringStream strmError;
            strmError << StdFormat(_T("Type error %s: %d")) %
                     pathDir.c_str() %  pPair->eType;
            STD_DBG( strmError.str().c_str());
            return false;
        }

        CWaitCursor wait;

        pDef->SetProject(this);

        StdStringStream strm;

        strm << StdFormat(_T("ファイル%sを読込中....")) % strFile;
        PrtintOut(WIN_DEBUG, strm.str().c_str());

        if(!pDef->Load(pathDir))
        {
            PrtintOut(WIN_DEBUG, _T("NG\n"));
            return false;
        }
        PrtintOut(WIN_DEBUG, _T("OK\n"));
    }
    catch (std::exception & ex) 
    {
        strError = MockException::FileErrorMessage(true, pathDir, 
                                  CUtil::CharToString(ex.what()));

        STD_DBG( strError.c_str());
        return false;
    }
    catch(...)
    {
        strError = MockException::FileErrorMessage(true, pathDir);
        STD_DBG( strError.c_str());
        return false;
    }
    pDef->LoadAfter();
    *ppDef = pDef;
    return true;
}

/**
 * @brief   シリアル化()
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  スクリプトへのポインタ
 * @note	
 */
bool CProjectCtrl::_SaveObjectDef(CObjectDef* pDef)
{
    namespace fs = boost::filesystem;
    StdPath  path(GetDir());
    StdPath  pathDir;
    StdString strFile;

    try 
    {
        pathDir = path /  pDef->GetName();
        if (!fs::exists(pathDir))
        {
            fs::create_directories(pathDir);
        }

		if (m_eFileType == E_BIN)
		{
			strFile = pDef->GetName() + _T(".msd");
		}
		else if (m_eFileType == E_TXT)
		{
			strFile = pDef->GetName() + _T(".msdt");
		}
		else if (m_eFileType == E_XML)
		{
			strFile = pDef->GetName() + _T(".msdx");
		}

		pathDir = pathDir / strFile;
        pDef->Save(pathDir);
    }
    catch (fs::filesystem_error& ex) 
    {
        StdStringStream strmError;
        strmError << StdFormat(_T("Save error %s:%s")) %
                 pathDir.c_str() % CUtil::CharToString(ex.what());
        STD_DBG( strmError.str().c_str());
        throw MockException(e_file_write, strmError.str().c_str());
    }
    return true;
}


bool CProjectCtrl::StoreDefaultObject(const CDrawingObject* pObj, bool bPri)
{
	DRAWING_TYPE eType = pObj->GetType();
	auto cObj = CDrawingObject::CloneShared(pObj);
	cObj->InitDefault();
	m_mapDefautObjects[eType] = cObj;
	m_mapPriority[eType] = bPri;

	return true;
}

bool CProjectCtrl::ClearDefaultObject(DRAWING_TYPE eType)
{
	auto ite = m_mapDefautObjects.find(eType);
	if (ite != m_mapDefautObjects.end())
	{
		m_mapDefautObjects.erase(ite);
		auto itePri = m_mapPriority.find(eType);
		if (itePri != m_mapPriority.end())
		{
			m_mapPriority.erase(itePri);
			return true;
		}
	}
	   
	return false;
}


std::shared_ptr<CDrawingObject> CProjectCtrl::GetDefaultObject(DRAWING_TYPE eType)
{
	auto ite = m_mapDefautObjects.find(eType);

	if (ite != m_mapDefautObjects.end())
	{
		return ite->second;
	}
	return nullptr;
}

bool CProjectCtrl::GetDefaultObjectPriorty(DRAWING_TYPE eType)
{
	auto ite = m_mapPriority.find(eType);

	if (ite != m_mapPriority.end())
	{
		return ite->second;
	}
	return false;
}

DefaultSetDlg* CProjectCtrl::GetDefaultObjectDlg()
{
	return m_pDefautSetDlg.get();
}


/**
 * @brief   シリアル化(ロード)
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  スクリプトへのポインタ
 * @note	
 */
template<class Archive>
void CProjectCtrl::load(Archive& ar, const unsigned int version)
{
    using namespace VIEW_COMMON;
    SERIALIZATION_INIT
        StdStringStream strmError;
        Clear();
        SERIALIZATION_LOAD("Name", m_Name);
        SERIALIZATION_LOAD("Folder",  m_treeFolder);
        SERIALIZATION_LOAD("ModuleID", m_iModuleId);
        //SERIALIZATION_LOAD("SingleFile", m_bSingleFile);
        SERIALIZATION_LOAD("ObjectList", m_mapObjectDefId);
		SERIALIZATION_LOAD("DefaultObjects", m_mapDefautObjects);
		SERIALIZATION_LOAD("DefaultPrority", m_mapPriority);

        _CreateIdMap();
    MOCK_EXCEPTION_FILE(e_file_read);

}

/**
 * @brief   シリアル化(セーブ)
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  スクリプトへのポインタ
 * @note	
 */
template<class Archive>
void CProjectCtrl::save(Archive& ar, const unsigned int version) const
{
    //セーブ開始を通知
    SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_VIEW_SAVE_START, 0, 0);
    std::map<StdString , CObjectDef*>::const_iterator iteMap;

    SERIALIZATION_INIT
        SERIALIZATION_SAVE("Name", m_Name);
        SERIALIZATION_SAVE("Folder",  m_treeFolder);
        SERIALIZATION_SAVE("ModuleID", m_iModuleId);
        //SERIALIZATION_SAVE("SingleFile", m_bSingleFile);
        SERIALIZATION_SAVE("ObjectList", m_mapObjectDefId);
		SERIALIZATION_SAVE("DefaultObjects", m_mapDefautObjects);
		SERIALIZATION_SAVE("DefaultPrority", m_mapPriority);

    MOCK_EXCEPTION_FILE(e_file_write);
}


//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
#include "MockSketch.h"

void TEST_CProjectCtrl_01()
{
    DB_PRINT(_T("  %s Start\n"), DB_FUNC_NAME.c_str());
    //ここから:次回予定

   // CProjectCtrl* pProj;
    //pProj = THIS_APP->GetProject();


    //------------------------------
    // CProjectCtrlのテストを呼び出す
    //------------------------------





    CProjectCtrl     proj2;
/*

    namespace fs = boost::filesystem;
    StdPath  pathTest = CSystem::GetSystemPath();
    pathTest /= _T("TEST_CProjectCtrl.xml");

    //保存
    StdStreamOut outFs(pathTest);
    StdXmlArchiveOut outXml(outFs);
    outXml << boost::serialization::make_nvp("Root", ref1);
    outFs.close();


    //読込
    StdStreamIn inFs(pathTest);
    StdXmlArchiveIn inXml(inFs);
    inXml >> boost::serialization::make_nvp("Root", proj2);
    inFs.close();

*/


    DB_PRINT(_T("  %s End\n"), DB_FUNC_NAME.c_str());
}

void TEST_CProjectCtrl()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    TEST_CProjectCtrl_01();

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}

#endif //_DEBUG
