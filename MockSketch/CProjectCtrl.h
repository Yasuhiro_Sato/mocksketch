/**
 * @brief       CProjectCtrlヘッダーファイル
 * @file        CProjectCtrl.h
 * @author      Yasuhiro Sato
 * @date        01-2-2009 10:36:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks   MockSketch アプリケーションのメイン ヘッダー ファイル
 *			
 *
 * $
 * $
 * 
 */
#ifndef _CPROJECT_CTRL_H_
#define _CPROJECT_CTRL_H_


/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "Utility/TreeData.h"
#include "View/CURRENT_BREAK_CONTEXT.h"
#include "View/ViewCommon.h"
#include "View/DRAWING_TYPE.h"
#include "COMMON_HEAD.h"


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/

/*---------------------------------------------------*/
/*  classes                                          */
/*---------------------------------------------------*/
class CObjectDef;
class CUndoAction;
class CDrawingObject;
class CIdObj;
class CPartsDef;
class CObservationFolder;
class DefaultSetDlg;
struct QUERY_REF_OBJECT;


/*---------------------------------------------------*/
/*  structs                                          */
/*---------------------------------------------------*/

/** 
 * @class   CProjectCtrl
 * @brief
 */
class CProjectCtrl
{
    class PAIR_OBJDEF_TYPE
    {
    public:
        PAIR_OBJDEF_TYPE():iChgCnt(-1){;}
        PAIR_OBJDEF_TYPE(const CObjectDef& def);
        ~PAIR_OBJDEF_TYPE(){;}

        StdString                       strObject;
        VIEW_COMMON::E_OBJ_DEF_TYPE     eType = VIEW_COMMON::E_NONE;
        int                             iChgCnt;
        std::shared_ptr<CObjectDef>   pDef;

    private:
        friend class boost::serialization::access;  
        template<class Archive>
        void serialize(Archive& ar, const unsigned int version)
        {
            SERIALIZATION_INIT
                SERIALIZATION_BOTH("Name"        , strObject);
                SERIALIZATION_BOTH("DefType"     , eType);
                SERIALIZATION_BOTH("ChangeCount" , iChgCnt);
            MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
        }
    };
public:
	enum E_PROJ_FILE_TYPE
	{
		E_XML,
		E_TXT,
		E_BIN
	};

public:
    //!< コンストラクタ
    CProjectCtrl();

    //!< コンストラクタ
    CProjectCtrl(const VIEW_COMMON::E_PROJ_TYPE eProjType);

    //!< デストラクタ
    ~CProjectCtrl();

    //!< プロジェクト名の取得
   StdString GetName() const;
 
    //!< プロジェクト名の設定
    bool SetName(StdString strName);

    //!< ファイルパスの取得
    StdString  GetFilePath() const;

    //!< ファイルディレクトリの取得
    StdString  GetDir() const;

    //!<フォルダーデータ取得
    TREE_DATA<VIEW_COMMON::OBJ_TREE_DATA>* GetFolderData();

    //!< プロジェクトタイプ取得
    VIEW_COMMON::E_PROJ_TYPE GetType() const {return m_eProjType;}

    //------------------
    // 部品関連
    //------------------

    //!< 部品数の取得
    int GetPartsNum() const;

    //!< 先頭部品
    //CObjectDef* GetFirstParts() const;

    //!< 部品名称チェック(使用可否)
    bool IsPossiblePartsName(StdString strName)  const;

    //!< 部品名称チェック(重複)
    bool IsDupPartsName(StdString strName) const; 

    //!< 部品取得
    std::weak_ptr<CObjectDef> GetPartsByName(StdString strName);

    //!< 部品取得
    std::weak_ptr<CObjectDef> GetParts(boost::uuids::uuid uuidParts);

    //!< 部品ID
    boost::uuids::uuid GetPartsIdByName(StdString strName) const;

    //!< 変更の有無問い合わせ
    bool IsChange(boost::uuids::uuid uuidParts,
                                        StdString strScript) const;

    //!< コンパイルの必要性問い合わせ
    COMPILE_STS GetCompileStatus(boost::uuids::uuid uuidParts) const;

    //!< 部品変更カウント
    int GetPartsChangeCount(boost::uuids::uuid uuidParts) const;

    //!< 部品アンロード
    void UnloadParts(boost::uuids::uuid uuidParts);

    //!< 部品有無
    //bool IsValidParts(CObjectDef* pObjDef) const;

    //!< 部品の作成
    std::weak_ptr<CObjectDef> CreateParts(StdString strName, 
                                              VIEW_COMMON::E_OBJ_DEF_TYPE eType, 
                                              int iTreeId);

    //!< 部品の削除
    bool DeleteParts(boost::uuids::uuid uuidParts);

    //!< 部品名変更
    bool ChangePartsName(StdString  strOld,  StdString  strNew);

    //!< 部品コピー
    //bool CopyParts(StdString strSrc, StdString strDst);

    //!< 部品コピー
    bool CopyParts(boost::uuids::uuid uuidDef , 
                   int iTreeId, 
                   StdString strDst = _T(""));

    //!< 部品移動
    bool MoveParts(std::shared_ptr<CObjectDef> pDefSrc, int iTreeId);



    //!< 新部品名問い合わせ
    StdString QueryNewPartsName(VIEW_COMMON::E_OBJ_DEF_TYPE eType) const;

    //!< 部品名リスト取得
    bool GetPartsNameList(std::vector<StdString>* pLstName) const;

    //!< 部品追加
    bool AddParts( std::shared_ptr<CObjectDef> pDef, int iTreeId);

    //!< クリア
    void Clear();

    //----------------
    // スクリプト
    //----------------
    //!< スクリプト削除
    bool DeleteScript(std::shared_ptr<CObjectDef> pDefSrc, int iScriptNo);


    //------------------
    // 描画データ
    //------------------
    CDrawingObject* QueryObject(const QUERY_REF_OBJECT* pQuery);
	CPartsDef* QueryDef(const QUERY_REF_OBJECT* pQuery);

    //------------------
    //!< 変更の有無
    bool IsChange() const;

    //!< 保存の問い合わせ
    bool InquireSave() const;

    //!<読込
    bool Load(StdPath Path);

    //!<保存
    bool Save(StdPath Path);

    //!<全オブジェクトの保存
    bool SaveAllObject();

    //!< ロード終了後処理
    void LoadAfter();

    //!< モジュールID取得
    int GetModuleId();

    //!< モジュールID加算
    void AddModuleId();

    //!< ファイル書き込みモード問い合わせ
    bool IsSingleFileMode() const { return m_bSingleFile;}

    //!< ファイル書き込みモード設定
    void SetSingleFileMode(bool bSingle = true);

    //<! 新規プロジェクト生成
    bool CreateNewProject(StdString strProject,
                          bool bCreteAssy,
				  		  bool bXml,
                          StdString strDir);

	//ディフォルトオブジェクトの保存
	bool StoreDefaultObject(const CDrawingObject* pObj, bool bPri);

	//ディフォルトオブジェクトの削除
	bool ClearDefaultObject(DRAWING_TYPE eType);

	//ディフォルトオブジェクトの読み込み
	std::shared_ptr<CDrawingObject> GetDefaultObject(DRAWING_TYPE eType);

	//ディフォルトオブジェクト優先設定
	bool GetDefaultObjectPriorty(DRAWING_TYPE eType);

	DefaultSetDlg* GetDefaultObjectDlg();

	E_PROJ_FILE_TYPE GetFileType() { return m_eFileType; }
	void SetFileType(E_PROJ_FILE_TYPE eType) { m_eFileType = eType;}

    //!< 

protected:
    friend CProjectCtrl;

    //<! オブジェクト書き込み
    bool _SaveObjectDef(CObjectDef* pDef);

    //<! オブジェクト読み込み
    bool _LoadObjectDef(std::shared_ptr<CObjectDef>* ppDef,
                              const PAIR_OBJDEF_TYPE* pPair);

    //!< ファイル変更監視コールバック
    static void _CallbackFileChange(StdString str, void* pVoid);

    //!<  IDマップ生成
    void _CreateIdMap();

    //!< TreeId検索
    int _FindTreeId(boost::uuids::uuid uuidObj) const;

protected:
    //!< フォルダー構造保存用データ
    TREE_DATA<VIEW_COMMON::OBJ_TREE_DATA>               m_treeFolder; 

protected:
    //!< 名称(プロジェクト名)
    StdString       m_Name;

    //!< 保存パス
    //StdString      m_strFilePath;

    //!< 保存パス
    StdString      m_strFileDir;

    //!< 変更の有無
    bool           m_bChange;

    //!< モジュールID
    int             m_iModuleId;

    //!< プロジェクト種別
    VIEW_COMMON::E_PROJ_TYPE m_eProjType;

    //!< ビルドモード
    bool             m_bDebug;

    //!< オブジェクト名->uuid
    std::map<StdString , boost::uuids::uuid>         m_mapUuid;

    //!< ID検索用マップ(データ保持)
    std::map<boost::uuids::uuid, PAIR_OBJDEF_TYPE>    m_mapObjectDefId;

    //!< ブレークポイント発生中のコンテキスト
    std::deque<CURRENT_BREAK_CONTEXT>        m_lstBreakContext;
    
    //!< 単一ファイルモード
    bool             m_bSingleFile;

    //boost::thread   m_tharedObserveFileChange;

    //!< ファイル変更監視
    std::shared_ptr<CObservationFolder>   m_psObsFolder;


	//!< ディフォルトオブジェクト
	std::map< DRAWING_TYPE, std::shared_ptr<CDrawingObject>> m_mapDefautObjects;
	std::map< DRAWING_TYPE, bool> m_mapPriority;

	//!< ディフォルト設定ダイアログ
	std::unique_ptr<DefaultSetDlg> m_pDefautSetDlg;

	//ファイルタイプ
	E_PROJ_FILE_TYPE			m_eFileType;
private:
    //==============================
    // ファイル保存
    //==============================
    friend class boost::serialization::access;  
    BOOST_SERIALIZATION_SPLIT_MEMBER();

    template<class Archive>
    void load(Archive& ar, const unsigned int version);

    template<class Archive>
    void save(Archive& ar, const unsigned int version) const;

};
BOOST_CLASS_VERSION(CProjectCtrl, 0);

#endif  //_CPROJECT_CTRL_H
