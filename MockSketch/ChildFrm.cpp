/**
 * @brief			CChildFrame実装ファイル
 * @file			ChildFrm.cpp.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MockSketch.h"
#include "MainFrm.h"

#include "./ChildFrm.h"
#include "DefinitionObject/View/MockSketchView.h"

#include "Utility/CUtility.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


IMPLEMENT_DYNCREATE(CChildFrame, CMDIChildWndEx)

BEGIN_MESSAGE_MAP(CChildFrame, CMDIChildWndEx)
	ON_WM_CREATE()
    ON_BN_CLICKED(IDC_PB_PAD, &CChildFrame::OnBnClickedPbPad)
    ON_BN_CLICKED(IDC_PB_RET, &CChildFrame::OnBnClickedPbRet)
    ON_WM_INITMENUPOPUP()
    ON_WM_CLOSE()
END_MESSAGE_MAP()

/**
 * @brief   コンストラクタ
 * @param   なし         
 * @retval  なし
 * @note	
 */
CChildFrame::CChildFrame()
{
	// TODO: メンバ初期化コードをここに追加してください。
}

/**
 * @brief   デストラクタ
 * @param   なし         
 * @retval  なし
 * @note	
 */
CChildFrame::~CChildFrame()
{
}

/**
 *  クライアント領域生成.
 *  @param  [in]    lpcs   Windows の CREATESTRUCT 構造体へのポインタ
 *  @param  [in]    pContext   CCreateContext 構造体へのポインタ。
 *  @retval         正常終了した場合は 0 以外を返します。それ以外の場合は 0 を返します。
 *  @note   OnCreate の実行中に、フレームワークから呼び出されます
 *  @note   この関数を決して呼び出さないでください。
 */
/*
BOOL CChildFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext)
{
   // スプリットは使わない
   //	iRet  = m_wndSplitter.Create(this,
   //		2, 2,			// TODO: 行と列の数を調整してください。
   //		CSize(10, 10),	// TODO: 最小ペインのサイズを変更します。
   //		pContext);
   // return iRet;
   return TRUE;
}
*/

/**
 * ウインドウ生成前処理
 * @param [in]    cs	 	  
 * @return		
 * @note	オーバーライド 
 * @exception   なし
 */
BOOL CChildFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: CREATESTRUCT cs を変更して、Window クラスまたはスタイルを変更します。
	if( !CMDIChildWndEx::PreCreateWindow(cs) )
		return FALSE;

	return TRUE;
}

// CChildFrame 診断

#ifdef _DEBUG
/**
 * @brief   オブジェクト内部状態チェック
 * @param   なし
 * @return  なし
 * @note     
 */
void CChildFrame::AssertValid() const
{
	CMDIChildWndEx::AssertValid();
}

/**
 * @brief   ダンプ処理
 * @param   [in]    dc	ダンプ用の診断ダンプ コンテキスト 
 * @return  なし
 * @note    
 */
void CChildFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWndEx::Dump(dc);
}
#endif //_DEBUG

/**
 * @brief  生成時処理要求
 * @param  [in] lpCreateStruct	初期化パラメータへのポインタ	 	  
 * @return		
 * @note	 
 * @exception   なし
 */
int CChildFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWndEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	return 0;
}

/**
 * @brief   PADボタン押下
 * @param   なし
 * @return  なし
 * @note     
 */
void CChildFrame::OnBnClickedPbPad()
{
    // CNumInputDlgbar 内で処理する
    // 但しこのメソッドはボタンを有効にするために必要
}
 
/**
 * @brief   RETボタン押下
 * @param   なし
 * @return  なし
 * @note     
 */
void CChildFrame::OnBnClickedPbRet()
{
    //CNumInputDlgbar 内で処理する
    // 但しこのメソッドはボタンを有効にするために必要
}

/**
 *  @brief  メッセージフィルタ処理
 *  @param  [in] pMsg   ウインドウメッセージ
 *  @retval true これ以上の処理不要 / false 通常処理
 *  @note   
 */
BOOL CChildFrame::PreTranslateMessage(MSG* pMsg)
{
    /*
    static UINT uiOldMsg2 = 0;
    if (uiOldMsg2 != pMsg->message)
    {
        uiOldMsg2 = pMsg->message;
        DB_PRINT(_T("CMDIChildWndEx::  %s,%x \n"), CUtil::ConvWinMessage(pMsg->message),
                                                       pMsg->hwnd);
    */

    CMainFrame* pFrame = THIS_APP->GetMainFrame();
    CView* pActive = pFrame->GetActiveView();
   if (pActive)
   {
       CMockSketchView* pView = dynamic_cast<CMockSketchView*>(pActive);
       if (pView)
       {
           CRichEditCtrl* pRich = pView->GetRichEdit();
           if (pRich->IsWindowVisible())
           {
               //Deleteキーに反応してしまう(ID_MNU_DELETE)
               return FALSE;
           }

       }
   }
    return CMDIChildWndEx::PreTranslateMessage(pMsg);
}
void CChildFrame::OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu)
{
    CMDIChildWndEx::OnInitMenuPopup(pPopupMenu, nIndex, bSysMenu);

    // TODO: ここにメッセージ ハンドラ コードを追加します。
}


void CChildFrame::OnClose()
{
    // TODO: ここにメッセージ ハンドラー コードを追加するか、既定の処理を呼び出します。
DB_PRINT(_T("CChildFrame::Close %I64x:Wnd=%I64x [%s]\n"), this, m_hWnd, GetFrameText());

    CMDIFrameWnd* pFrame = GetMDIFrame();

DB_PRINT(_T("      CMDIFrameWnd %I64x:Wnd=%I64x [%s]\n"), pFrame, pFrame->m_hWnd, pFrame->GetTitle());



    CMDIChildWndEx::OnClose();
}
