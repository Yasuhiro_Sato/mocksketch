/**
 * @brief			CElementDef実装ファイル
 * @file			CElementDef.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "CElementDef.h"
#include "DefinitionObject/CPartsDef.h"
#include "DrawingObject/CDrawingElement.h"
#include "Utility/CUtility.h"
#include "DefinitionObject/ObjectDef.h"
#include "System/CSystem.h"
#include "Io/CIoAccess.h"
#include <math.h>
#include <boost/serialization/export.hpp> 
BOOST_CLASS_EXPORT(CElementDef);


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
CElementDef::CElementDef():
CObjectDef  ()
{
    m_Prop.eObjType  = VIEW_COMMON::E_ELEMENT;
}

/**
 * @brief   コンストラクタ
 * @param   [in] strName 名称
 * @retval  なし
 * @note
 */
CElementDef::CElementDef(StdString strName):
CObjectDef   (strName)
{
    m_Prop.eObjType  = VIEW_COMMON::E_ELEMENT;
}


/**
 *  @brief  コピーコンストラクタ.
 *  @param  [in] pObj    コピー元データ
 *  @param  [in] strName コピー先名称
 *  @retval なし     
 *  @note
 */
CElementDef::CElementDef(const CElementDef& pObj, 
                               StdString strName):
CObjectDef  (pObj, strName)
{
    m_Prop.eObjType  = VIEW_COMMON::E_ELEMENT;
    m_lstIcon = pObj.m_lstIcon;
}

/**
 *  @brief  デストラクタ.
 *  @param  なし 
 *  @retval なし 
 *  @note
 */
CElementDef::~CElementDef()
{
}

/**
 * @brief   インスタンス生成
 * @param   [in] pCtrl
 * @retval  インスタンス
 * @note    pCtrlに対してスクリプトオブジェクトを作成する
 */
std::shared_ptr<CDrawingScriptBase> CElementDef::CreateInstance(CObjectDef* pCtrl)
{
    if ((pCtrl->GetObjectType() != VIEW_COMMON::E_PARTS) &&
        (pCtrl->GetObjectType() != VIEW_COMMON::E_FIELD))
    {
        return NULL;
    }

    std::shared_ptr<CDrawingElement> pDrawingElement;

    try
    {
        CPartsDef* pCtrl2;
        pCtrl2 = dynamic_cast<CPartsDef*>(pCtrl);

        if (pCtrl2)
        {
            std::shared_ptr<CIoAccess> pIoAccess;
            pIoAccess = std::make_shared<CIoAccess>();

            pDrawingElement = std::make_shared<CDrawingElement>();
            //pDrawingElement->SetElementDef(this);
            pDrawingElement->SetPartsDef(pCtrl2);
            pDrawingElement->Create(pIoAccess, this);
        }
        else
        {
            return NULL;
        }
    }
    catch (MockException& e)
    {
        throw MockException(e);
        return NULL;
    }
    return pDrawingElement;
}

/**
 * @brief   アイコン初期設定
 * @param   [in] iMax
 * @param   [in] iWidth
 * @param   [in] iHeight
 * @retval
 * @note
 */
void CElementDef::InitIcon(int iMax, int iWidth, int iHeight)
{
    m_lstIcon.resize(iMax);
    foreach(CImageData& Data, m_lstIcon)
    {
        Data.Create(iWidth, iHeight, 32);
    }
}

/**
 * @brief   アイコン数取得
 * @param   なし
 * @retval  アイコン数
 * @note
 */
int CElementDef::GetMax()
{
    return SizeToInt(m_lstIcon.size());
}

/**
 * @brief   アイコン幅取得
 * @param   なし
 * @retval  アイコン幅
 * @note
 */
int CElementDef::GetWidth()
{
    if (m_lstIcon.size() == 0)
    {
        return 0;
    }
    return m_lstIcon[0].GetWidth();
}

/**
 * @brief   アイコン高さ取得
 * @param   なし
 * @retval  アイコン高さ
 * @note
 */int CElementDef::GetHeight()
{
    if (m_lstIcon.size() == 0)
    {
        return 0;
    }
    return m_lstIcon[0].GetHeight();
}


/**
 * @brief   アイコンデータ取得
 * @param   [in] iIconIndex
 * @retval  アイコンデータ
 * @note
 */
CImageData* CElementDef::GetIconData(int iIconIndex)
{
    if (static_cast<int>(m_lstIcon.size()) <= iIconIndex)
    {
        return NULL;
    }
    return &m_lstIcon[iIconIndex];
}

/**
 * @brief   ファイル読み込み
 * @param   Path ファイルへのパス
 * @retval  true 読み込み成功
 * @note	
 */
bool CElementDef::Load(StdPath Path)
{
    StdString strError;
    if (!boost::filesystem::exists(Path))
    {
        strError = GET_ERR_STR(e_file_exist);
        STD_DBG(strError.c_str()); 
        return false;
    }

    _SetCurrentPath(Path.parent_path());
    StdString strExt = GetPathStr(Path.extension());
    strExt = CUtil::ToUpper(strExt);
    bool bRet = true;

    if (strExt == _T(".MSD") )
    {
        boost::filesystem::ifstream inFs(Path, std::ios::binary);
        try
        {      
            {
                boost::archive::binary_iarchive inBin(inFs);
                inBin >> *this;
            }
            inFs.close();
        }
        catch(std::exception& e )
        {
            inFs.close();
            strError = MockException::FileErrorMessage(true, Path, 
                                      CUtil::CharToString(e.what()));
            bRet = false;
        }
        catch(...)
        {
            inFs.close();
            strError = MockException::FileErrorMessage(true, Path);
            bRet = false;
        }
    }
	if (strExt == _T(".MSDT"))
	{
		StdStreamIn  inFs(Path);
		try
		{
			{
				boost::archive::text_wiarchive txtIn(inFs);
				txtIn >> *this;
			}
			inFs.close();
		}
		catch (std::exception& e)
		{
			inFs.close();
			strError = MockException::FileErrorMessage(true, Path,
				CUtil::CharToString(e.what()));
			bRet = false;
		}
		catch (...)
		{
			inFs.close();
			strError = MockException::FileErrorMessage(true, Path);
			bRet = false;
		}
	}
	else if (strExt == _T(".MSDX") )
    {
        StdStreamIn inFs(Path);
        try
        {
            {
                StdXmlArchiveIn inXml(inFs);
                inXml >> boost::serialization::make_nvp("Root", *this);
            }
            inFs.close();
        }
        catch(std::exception& e )
        {
            inFs.close();
            strError = MockException::FileErrorMessage(true, Path, 
                                      CUtil::CharToString(e.what()));
            bRet = false;
        }
        catch(...)
        {
            inFs.close();
            strError = MockException::FileErrorMessage(true, Path);
            bRet = false;
        }
    }
    else
    {
        StdString strExtErr;
        strExtErr =  GET_ERR_STR(e_file_ext);
        strExtErr += _T("(");
        strExtErr += strExt;
        strExtErr += _T(")");
        strError = MockException::FileErrorMessage(true, Path, strExtErr);
        bRet = false;
    }

    if (!bRet)
    {
        STD_DBG(strError.c_str()); 
    }

    return bRet;
}

/**
 * @brief   ファイル保存
 * @param   Path ファイルへのパス
 * @retval  true 保存み成功
 * @note	
 */
bool CElementDef::Save(StdPath Path)
{
    StdPath pathOld = m_pathCurrent;
    _SetCurrentPath(Path.parent_path());

    StdString strExt = GetPathStr(Path.extension());
    strExt = CUtil::ToUpper(strExt);
    StdString strError;
    bool bRet = true;
    if (strExt == _T(".MSD") )
    {
        boost::filesystem::ofstream outFs(Path, std::ios::binary);
        try
        {
            SetSingleFileMode();
            {
                boost::archive::binary_oarchive outBin(outFs);
                outBin << *this;
            }
            outFs.close();
        }
        catch(std::exception& e )
        {
            //ファイル%sの書き込みに失敗しました;
            outFs.close();
            strError = MockException::FileErrorMessage(false, Path, 
                                      CUtil::CharToString(e.what()));
            bRet = false;
        }
        catch(...)
        {
            outFs.close();
            strError = MockException::FileErrorMessage(false, Path);
            bRet = false;
        }

    }
	else if (strExt == _T(".MSDT"))
	{
		StdStreamOut outFs(Path);
		try
		{
			SetSingleFileMode();
			{
				boost::archive::text_woarchive txtOut(outFs);
				txtOut << *this;
			}
			outFs.close();
		}
		catch (std::exception& e)
		{
			//ファイル%sの書き込みに失敗しました;
			outFs.close();
			strError = MockException::FileErrorMessage(false, Path,
				CUtil::CharToString(e.what()));
			bRet = false;
		}
		catch (...)
		{
			outFs.close();
			strError = MockException::FileErrorMessage(false, Path);
			bRet = false;
		}

	}
	else if (strExt == _T(".MSDX") )
    {
        StdStreamOut outFs(Path);
        try
        {
            SetSingleFileMode(false);
            {
                StdXmlArchiveOut outXml(outFs);
                outXml << boost::serialization::make_nvp("Root", *this);
            }
            outFs.close();
        }
        catch(std::exception& e )
        {
            outFs.close();
            strError = MockException::FileErrorMessage(false, Path, 
                                      CUtil::CharToString(e.what()));
            bRet = false;
        }
        catch(...)
        {
            outFs.close();
            strError = MockException::FileErrorMessage(false, Path);
            bRet = false;
        }
    }
    else
    {
        StdString strExtErr;
        strExtErr =  GET_ERR_STR(e_file_ext);
        strExtErr += _T("(");
        strExtErr += strExt;
        strExtErr += _T(")");
        strError = MockException::FileErrorMessage(false, Path, strExtErr);
        bRet = false;
    }

    if (!bRet)
    {
        STD_DBG(strError.c_str()); 
        _SetCurrentPath(pathOld);
    }
    return bRet;
}

//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void TEST_CElementDef()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}
#endif //_DEBUG