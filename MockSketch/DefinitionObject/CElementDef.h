/**
* @brief        CElementDefヘッダーファイル
* @file	        CElementDef.h
* @author           Yasuhiro Sato
* @date	        07-2-2009 20:03:09
* 
* @note	
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
* 
* @remarks	        
*			
*
* $
* $
* 
*/
#ifndef __ELEMENT_DEF_H_
#define __ELEMENT_DEF_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DefinitionObject/ObjectDef.h"
#include "Utility/CImageData.h"

/*---------------------------------------------------*/
/*  classes                                          */
/*---------------------------------------------------*/
///class CImageData;
class CDrawingElement;
/*
Element 種別
  ・BOX2D要素
  ・出力要素
  ・入力要素

Element 特性
  ・対応フィールド
  ・



*/

enum E_ELEMENT_TYPE
{
    ET_NONE,        //!< なし
    ET_PHYSICS,     //!< 物理エンジン
    ET_INPUT,       //!< 入力
    ET_OUTPUT,      //!< 出力
    ET_COM,         //!< 通信
    ET_NODE,        //!< ノード
    ET_OTHER
};



/**
 * @class   CElementDef
 * @brief  
 */
class CElementDef: public CObjectDef
{
public:
    
public:
    //!< コンストラクタ
    CElementDef();

    //!< コンストラクタ
    CElementDef(StdString strName);

    //!< コピーコンストラクタ
    CElementDef(const CElementDef& pObj, StdString strName = _T(""));

    //!< デストラクタ
    virtual ~CElementDef();

    //!< スクリプトオブジェクト生成
    virtual std::shared_ptr<CDrawingScriptBase> CreateInstance(CObjectDef* pCtrl);

    //!< アイコン初期設定
    void InitIcon(int iMax, int iWidth, int iHeight);

    //!< アイコン数取得
    int GetMax();

    //!< アイコン幅取得
    int GetWidth();

    //!< アイコン高さ取得
    int GetHeight();

    //!< アイコンデータ取得
    CImageData* GetIconData(int iIconIndex);

    //-----------------
    // ファイル
    //-----------------
    //!<読込
    virtual bool Load(StdPath Path);

    //!<保存
    virtual bool Save(StdPath Path);

protected:
    //!< 種別
    E_ELEMENT_TYPE  m_eType;

    //!< アイコン
    std::vector<CImageData>   m_lstIcon;

private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
            SERIALIZATION_BOTH("IconList" , m_lstIcon);
            ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(CObjectDef);
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }
};

#endif // 