/**
 * @brief			CFieldDef実装ファイル
 * @file			CFieldDef.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CFieldDef.h"
#include "./CPartsDef.h"
#include "DefinitionObject/View/CDrawingView.h"
#include "DrawingObject/CNodeMarker.h"
#include "DrawingObject/CDrawingParts.h"
#include "DrawingObject/CDrawingField.h"
#include "View/CLayer.h"
#include "View/CDrawMarker.h"
#include "View/CUndoAction.h"
#include "Script/CScript.h"
#include "Script/CExecCtrl.h"
#include "System/CSystem.h"
#include "System/MOCK_ERROR.h"
#include "Utility/CUtility.h"
#include "Utility/CIdObj.h"
#include "Utility/CPropertyGroup.h"
#include "MockSketch.h"
#include "CProjectCtrl.h"

#include "Io/CIoAccess.h"
#include "Io/CIoRef.h"
#include "Io/CIoDef.h"
#include "Io/CIoConnect.h"
#include "Io/CIoPropertyBlock.h"
#include "Io/CIoAccess.h"
#include "ExecCommon.h"

#include <math.h>
#include <boost/serialization/export.hpp> 
/*---------------------------------------------------*/
/*  Defines                                          */
/*---------------------------------------------------*/

BOOST_CLASS_EXPORT(CFieldDef);


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
CFieldDef::CFieldDef():
CPartsDef   ()
{
    m_Prop.eObjType  = VIEW_COMMON::E_FIELD;
    m_Prop.strThreadType = EXEC_COMMON::GetThreadType2Name(EXEC_COMMON::TH_FIELD_1);
}


/**
 * @brief   コンストラクタ
 * @param   [in] strName 名称
 * @retval  なし
 * @note
 */
CFieldDef::CFieldDef(StdString strName):
CPartsDef   (strName)
{
    m_Prop.eObjType  = VIEW_COMMON::E_FIELD;
    m_Prop.strThreadType = EXEC_COMMON::GetThreadType2Name(EXEC_COMMON::TH_FIELD_1);
}


/**
 *  @brief  コピーコンストラクタ.
 *  @param  [in] pObj    コピー元データ
 *  @param  [in] strName コピー先名称
 *  @retval なし     
 *  @note
 */
CFieldDef::CFieldDef(const CFieldDef& pObj, 
                    StdString strName):
CPartsDef  (pObj, strName)
{
    m_Prop.eObjType  = VIEW_COMMON::E_FIELD;
}

/**
 *  @brief  デストラクタ.
 *  @param  なし 
 *  @retval なし 
 *  @note
 */
CFieldDef::~CFieldDef()
{
}

/**
 * @brief   スクリプトオブジェクト生成
 * @param   なし
 * @retval  スクリプトオブジェクト
 * @note
 */
/*
CDrawingScriptBase* CFieldDef::CreateInstance(CObjectDef* pCtrl)
{
    using namespace boost;
    CDrawingField* pField = NULL;
     CDrawingParts* pParts = NULL;
    try
    {
        if (pCtrl->GetObjectType() == VIEW_COMMON::E_FIELD)
        {
            
            CFieldDef* pCtrl2;
            pCtrl2 = dynamic_cast<CFieldDef*>(pCtrl);
            if (pCtrl2)
            {
                shared_ptr<CIoAccess> pIoAccess;
                pIoAccess = make_shared<CIoAccess>();

                pField = new CDrawingField();
                pField->SetPartsDef(pCtrl2);
                pField->Create(pIoAccess, this);
            }
            else
            {
                return NULL;
            }
            return pField;
        }
        if (pCtrl->GetObjectType() == VIEW_COMMON::E_PARTS)
        {
           
            CPartsDef* pCtrl2;
            pCtrl2 = dynamic_cast<CPartsDef*>(pCtrl);
            if (pCtrl2)
            {
                shared_ptr<CIoAccess> pIoAccess;
                pIoAccess = make_shared<CIoAccess>();

                pParts = new CDrawingParts();
                pParts->SetPartsDef(pCtrl2);
                pParts->Create(pIoAccess, this);
            }
            else
            {
                return NULL;
            }
            return pParts;
        }
    }
    catch (MockException& e)
    {
        STD_DBG(_T("CreateInstance error %s"), e.what());
        if (pField)
        {
            delete pField;
        }
        if (pParts)
        {
            delete pParts;
        }
        return NULL;
    }
    return NULL;
}
*/
//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void TEST_CFieldDef()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}
#endif //_DEBUG