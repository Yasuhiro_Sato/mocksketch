/**
* @brief        CFieldDefヘッダーファイル
* @file	        CFieldDef.h
* @author           Yasuhiro Sato
* @date	        07-2-2009 20:03:09
* 
* @note	
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
* 
* @remarks	        
*			
*
* $
* $
* 
*/
#ifndef __FIELD_DEF_H_
#define __FIELD_DEF_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DefinitionObject/ObjectDef.h"
#include "DefinitionObject/CPartsDef.h"

/*---------------------------------------------------*/
/*  classes                                          */
/*---------------------------------------------------*/
class CImageData;



/**
 * @class   CFieldDef
 * @brief  
 */
class CFieldDef: public CPartsDef
{
    
public:
    //!< コンストラクタ
    CFieldDef();

    //!< コンストラクタ
    CFieldDef(StdString strName);

    //!< コピーコンストラクタ
    CFieldDef(const CFieldDef& pObj, StdString strName = _T(""));

    //!< デストラクタ
    virtual ~CFieldDef();

    //!< スクリプトオブジェクト生成
    //virtual CDrawingScriptBase* CreateInstance(CObjectDef* pCtrl);


protected:

    
private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
            ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(CPartsDef);
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }
};

#endif // __FIELD_DEF_H_