/**
 * @brief			CPartsDef実装ファイル
 * @file			CPartsDef.cpp
 * @author			Yasuhiro Sato
 * @date			03-2-2009 23:20:46
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/CFieldDef.h"
#include "DefinitionObject/View/CDrawingView.h"
#include "DrawingObject/CNodeMarker.h"
#include "DrawingObject/CDrawingParts.h"
#include "DrawingObject/CDrawingField.h"
#include "DrawingObject/CDrawingReference.h"
#include "DrawingObject/CNodeData.h"
#include "View/CLayer.h"
#include "View/CDrawMarker.h"
#include "View/CUndoAction.h"
#include "Script/CExecCtrl.h"
#include "Script/CModule.h"
#include "Script/CScriptObject.h"
#include "Script/TCB.h"
#include "System/CSystem.h"
#include "System/MOCK_ERROR.h"
#include "Utility/CUtility.h"
#include "Utility/CIdObj.h"
#include "Utility/CPropertyGroup.h"
#include "MockSketch.h"
#include "CProjectCtrl.h"
#include "SubWindow/ImageCtrlDlg.h"

#include "Io/CIoAccess.h"
#include "Io/CIoRef.h"
#include "Io/CIoDef.h"
#include "Io/CIoConnect.h"
#include "Io/CIoPropertyBlock.h"
#include "Io/CIoAccess.h"
#include "ExecCommon.h"

#include <boost/interprocess/errors.hpp>
#include <boost/serialization/export.hpp> 
/*---------------------------------------------------*/
/*  Defines                                          */
/*---------------------------------------------------*/
using namespace EXEC_COMMON;

BOOST_CLASS_EXPORT(CPartsDef);

//#define _DEBUG_SELECT
#define _DEBUG_DRAGABBLE 0

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


//!< コンストラクタ
CPartsDef::CPartsDef(StdString strName):
CObjectDef(strName)
{
    m_Prop.eObjType   =  VIEW_COMMON::E_PARTS;
    _InitLayer();
    m_curLayerId = 1;

    //m_bRunDraw = false;

    m_psObjects = std::make_shared<CDrawingParts>();
    m_psIdObject = std::make_shared<CIdObj>();
    m_psObjects->SetPartsDef(this);
    m_psObjects->SetGroup(NULL, true);

    m_Prop.strThreadType = EXEC_COMMON::GetThreadType2Name(EXEC_COMMON::TH_GROUP_1);

    //!< IO
    m_psAccess = std::make_shared<CIoAccess>(); 
    GetIoDef()->Create(this, m_psAccess);

    m_iRefCount   = 1;
    m_iCntDraw    = -1;

    m_lstIoPage.resize(6);
    m_lstIoObj.resize(6);

    std::fill(m_lstIoPage.begin(), m_lstIoPage.end(), -1);
    std::fill(m_lstIoObj.begin(), m_lstIoObj.end(), -1);

    m_bDrawComplete = false;
    m_bDrawLock = false;
    m_bDrawing = false;
    m_bPrintArea = false;
    m_bPaperArea = true;
    m_bOffsetSizeEqualPaper = true;
    m_dOffsetPageCol = DRAW_CONFIG->pDispUnit->ToThisUnit(THIS_APP->GetPaperWidth());
    m_dOffsetPageRow = DRAW_CONFIG->pDispUnit->ToThisUnit(THIS_APP->GetPaperHeight());
    m_iPrintRow  = 1;
    m_iPrintCol  = 1;

    m_pImageDlg = std::make_unique<CImageCtrlDlg>(this);
    m_bUseNodeBounds = true;
    m_bIgnoreSelPart = false;

    int iSize = ID_MNU_SHOW_POS_MAX - ID_MNU_SHOW_POS_01;
    m_lstViewPos.resize(iSize);
    m_bLockUpdateNodeData = false;
}

/**
*  @brief  コンストラクター.
*  @param  なし
*  @retval なし     
*  @note
*/
CPartsDef::CPartsDef():
CPartsDef::CPartsDef(_T("Main"))
{
}


/**
 *  @brief  コピーコンストラクタ.
 *  @param  [in] pObj    コピー元データ
 *  @param  [in] strName コピー先名称
 *  @retval なし     
 *  @note
 */
CPartsDef::CPartsDef(const CPartsDef& pObj, 
                                               StdString strName):
CObjectDef(pObj, strName)
{
    m_Prop.eObjType   =  VIEW_COMMON::E_PARTS;
    m_lstLayer      = pObj.m_lstLayer;
    m_curLayerId    = pObj.m_curLayerId;
    //m_lstDrawing   = pObj.m_lstDrawing;

    //m_bRunDraw = false;

    m_psObjects = std::make_shared<CDrawingParts>(*pObj.m_psObjects, true);

    m_psIdObject = std::make_shared< CIdObj>();
    *m_psIdObject = *pObj.m_psIdObject;

    m_psObjects->SetPartsDef(this);
    m_psObjects->SetGroup(NULL, true);
    m_psObjects->SetRefParam( pObj.m_psObjects.get(), CDrawingObject::CP_DEFAULT);
    
    //!< IO
    m_psAccess = std::make_shared<CIoAccess>(*pObj.m_psAccess.get()) ; 
    GetIoDef()->Create(this, m_psAccess);

    //ID再設定
    CreateId();

    m_iRefCount   = 1;
    m_iCntDraw    = -1;

    m_bDrawLock = false;
    m_bDrawing = false;

    //TODO: 修正
    m_lstIoPage.resize(6);
    m_lstIoObj.resize(6);

    m_bPrintArea = pObj.m_bPrintArea;

    std::fill(m_lstIoPage.begin(), m_lstIoPage.end(), -1);
    std::fill(m_lstIoObj.begin(), m_lstIoObj.end(), -1);

    m_bPrintArea = pObj.m_bPrintArea;
    m_bPaperArea = pObj.m_bPaperArea;
    m_bOffsetSizeEqualPaper = pObj.m_bOffsetSizeEqualPaper;
    m_dOffsetPageCol = pObj.m_dOffsetPageCol;
    m_dOffsetPageRow = pObj.m_dOffsetPageRow;
    m_iPrintRow  = pObj.m_iPrintRow;
    m_iPrintCol  = pObj.m_iPrintCol;

    m_pImageDlg = std::make_unique<CImageCtrlDlg>(this);
    m_bUseNodeBounds = pObj.m_bUseNodeBounds;
    m_bIgnoreSelPart = pObj.m_bIgnoreSelPart;
    m_lstViewPos = pObj.m_lstViewPos;
    m_bLockUpdateNodeData = false;

}


/**
*  @brief  デストラクタ.
*  @param  なし
*  @retval なし     
*  @note
*/
CPartsDef::~CPartsDef()
{

}

/**
 * @brief   スクリプトオブジェクト生成
 * @param   [in] pCtrl
 * @retval  スクリプトオブジェクト
 * @note    pCtrlに対してスクリプトオブジェクトを作成する
 */
std::shared_ptr<CDrawingScriptBase> CPartsDef::CreateInstance(CObjectDef* pObjectDef)
{
    //CDrawingField* pField = NULL;
    std::shared_ptr<CDrawingParts> pParts;
    try
    {
        CPartsDef* pPartsDest;
        pPartsDest = dynamic_cast<CPartsDef*>(pObjectDef);
        if (pPartsDest)
        {
            std::shared_ptr<CIoAccess> pIoAccess;
            pIoAccess = std::make_shared<CIoAccess>();

            if (GetObjectType() == VIEW_COMMON::E_FIELD)
            {
                pParts = std::make_shared<CDrawingField>();
            }
            else if(GetObjectType() == VIEW_COMMON::E_PARTS)
            {
                pParts = std::make_shared<CDrawingParts>();
            }
            else if(GetObjectType() == VIEW_COMMON::E_REFERENCE)
            {
                pParts = std::make_shared<CDrawingReference>();
            }

            pParts->SetPartsDef(pPartsDest);
            pParts->Create(pIoAccess, this);
        }
        else
        {
            return NULL;
        }
        return pParts;
    }
    catch (MockException& e)
    {
        STD_DBG(_T("CreateInstance error %s"), e.what());
    }
    return NULL;
}

/**
 * @brief   初期化スクリプト生成
 * @param   なし
 * @retval  true 成功
 * @note    
 */
bool CPartsDef::CreateInitialScript()
{
    bool bRet;
    bRet = CObjectDef::CreateInitialScript();
    return bRet;
}

/**
 * @brief   一時コピー作成
 * @param   [out] pCtrl
 * @retval  なし
 * @note    CExecCtrl::Execで使用 
 *          実行用に一時的なCPartsDefを生成する
 */
void CPartsDef::CreateTmpCopy(CObjectDef* pDef)
{
    CObjectDef::CreateTmpCopy(pDef);

    CPartsDef* pParts = dynamic_cast<CPartsDef*>(pDef);
    STD_ASSERT(pParts);


    if (pParts)
    {
        //pParts->m_lstDrawing = m_lstDrawing;
        pParts->m_lstLayer   = m_lstLayer;
        pParts->m_curLayerId = m_curLayerId;
    }
}

/**
 *  @brief  部品使用有無
 *  @param  [in] iPartsId 部品ID
 *  @retval true 使用箇所あり
 *  @note   
 */
bool CPartsDef::IsIncludeParts(boost::uuids::uuid uuidParts) const
{

    const CPartsDef* pCtrl;

    std::shared_ptr<CObjectDef> pDef;

    pDef = THIS_APP->GetObjectDefByUuid(uuidParts).lock();

    if (!pDef)
    {
        return false;
    }
    
    pCtrl = dynamic_cast<const CPartsDef*>(pDef.get());
    return IsIncludeParts(pCtrl);
}


 /**
 * @brief   部品使用有無
 * @param   [in] pRegCtrl 参照コントロール
 * @retval  true 登録あり
 * @note	
 */
bool CPartsDef::IsIncludeParts(const CPartsDef* pRefCtrl) const
{
    if (m_psObjects)
    {
        return m_psObjects->IsIncludeParts(pRefCtrl);
    }
    return false;
}

bool CPartsDef::IsIncludeParts2(const CPartsDef* pRefCtrl)
{
    if (m_psObjects)
    {
        return m_psObjects->IsIncludeParts(pRefCtrl);
    }
    return false;
}

/**
 * @brief   部品使用箇所削除
 * @param   [in] pRegCtrl 参照コントロール
 * @retval  true 削除箇所あり
 * @note	
 */
bool CPartsDef::DeleteUsingParts(const CPartsDef* pRefCtrl)
{
    if (m_psObjects)
    {
        return m_psObjects->DeleteUsingParts(pRefCtrl);
    }
    return false;
}

/**
 * @brief   名称生成
 * @param   [in] pRegCtrl 参照コントロール
 * @retval  生成した名称
 * @note	
 */
StdString CPartsDef::CreateName(StdString strBase)
{
    StdString strRet;
    std::vector<StdString> lstName;
    m_psObjects->GetNmaeList(&lstName);

    strRet = CUtil::FindNumberingString(&lstName, strBase, 4);

    return strRet;
}



/**
 *  @brief  描画リスト設定
 *  @param  [inout]    pDisplyList
 *  @param  [out]      pLstPoint
 *  @param  [out]      pLstScriptBase
 *  @retval なし
 *  @note
 */
void CPartsDef::_SetDrawData(std::vector< DRAW_OBJ_LIST >* pDisplyList,
                             std::vector< DRAW_OBJ_LIST >* pLstPoint,
                             std::vector< DRAW_OBJ_LIST >* pLstNoIndex,
                              DRAW_OBJ_LIST* pLstScriptBase)
{
    std::deque<std::shared_ptr<CDrawingObject>>* pList;

    STD_ASSERT(pDisplyList != NULL);
    STD_ASSERT(pLstPoint != NULL);

    pList = m_psObjects->GetList();
    pDisplyList->clear();
    pLstPoint->clear();
    pLstScriptBase->clear();

    int iLayerMax = GetLayerMax();
    pDisplyList->resize(iLayerMax);
     pLstPoint->resize(iLayerMax);
    int iLayer;
    CLayer* pLayer;
    int iPumpCnt = 0;

    for ( auto pObject: *pList)
    {
        iLayer = pObject->GetLayer();
        pLayer = GetLayer(iLayer);

        if (!pLayer->bVisible)
        {
            continue;
        }

        if(pObject->GetId() ==  ID_NO_ID_DRAWING)
        {
            pLstNoIndex->at(iLayer).push_back(pObject);
            continue;
        }

        if (pObject->GetType() == DT_POINT)
        {
            pLstPoint->at(iLayer).push_back(pObject);
        }
        else 
        {
            pDisplyList->at(iLayer).push_back(pObject);
        }

        if (pObject->GetType() & DT_SCRIPTBASE)
        {
            pLstScriptBase->push_back(pObject);
        }

#if 0
        iPumpCnt++;
        if ((iPumpCnt % 100) == 0)
        {
            MSG msg;
            while ( ::GetMessage(&msg,NULL,0,0) )
            {
                ::TranslateMessage(&msg);
                ::DispatchMessage(&msg);
            }
        }
#endif
    }
}

/**
 * @brief   実行グループ登録
 * @param   なし
 * @retval  true 成功
 * @note	
 */
void CPartsDef::_RegisterExecCtrl(CDrawingParts* pParts)
{

    //条件： 未登録
    //       コンパイル済み


    THIS_APP->GetExecCtrl()->AddScriptLoop(pParts);


    std::deque<std::shared_ptr<CDrawingObject>>* pList;
    pList = pParts->GetList();
    for ( auto pObject: *pList)
    {
        if(pObject->GetType() & DT_SCRIPTBASE)
        {
            auto pScriptBase = std::dynamic_pointer_cast<CDrawingScriptBase>(pObject);

            if ((pObject->GetType() ==  DT_PARTS) ||
                (pObject->GetType() ==  DT_FIELD))
            {
              auto pChildGroup = std::dynamic_pointer_cast<CDrawingParts>(pScriptBase);
              _RegisterExecCtrl(pChildGroup.get());
            }
        }
    }
}


/**
 * @brief   スクリプト生成実行
 * @param   なし
 * @retval  true 成功
 * @note	
 */
bool CPartsDef::_SetExecute()
{
    /*
    GetExecCtrl::    std::unique_ptr<CDrawingScriptBase> m_psExecScript
    CDrawingScriptBase:: CDrawingParts:: std::unique_ptr<CPartsDef>   m_psExecPartsDef
    CPartsDef  std::unique_ptr<CDrawingParts>           m_psObjects;

    */

    CExecCtrl* pCtrl;
    pCtrl = THIS_APP->GetExecCtrl();

    std::shared_ptr<CPartsDef> pPartsDefCopy;

    pPartsDefCopy = std::make_shared<CPartsDef>();
    CreateTmpCopy(pPartsDefCopy.get() );

    auto pBase = CreateInstance(pPartsDefCopy.get());
    auto pParts = std::dynamic_pointer_cast<CDrawingParts>(pBase);
    pParts->SetName(this->GetName().c_str());

#if 0
    DB_PRINT_TIME(_T("CPartsDef::_SetExecute (CDrawingParts*) %I64x  \n"), pParts);
#endif

    pCtrl->InitScript();
    _RegisterExecCtrl(pParts.get());  //Loopを登録
    pPartsDefCopy->_ChangeObjecs(pParts);
    pCtrl->SetExecDef(pPartsDefCopy);


	//画面を開く
	if (pPartsDefCopy)
	{
		THIS_APP->OpenNewExecView(pPartsDefCopy);
	}


    bool bRet;
    bRet = pCtrl->StartScript();
    return bRet;
}

/**
 *  @brief  描画オブジェクト変更
 *  @param  [in] pObjects
 *  @retval なし
 *  @note   _SetExecute内でのみ使用
 */
void CPartsDef::_ChangeObjecs(std::shared_ptr<CDrawingParts> pObjects)
{
    m_psObjects.reset();
	for (auto pSelObj : m_lstSelect)
	{
		auto pObj = pSelObj.lock();
		if (!pObj)
		{
			continue;
		}
		pObj->SetSelect(false);
	}
    m_lstSelect.clear();

	ClearConnectionToSelect();
    m_psObjects = pObjects;
}

void CPartsDef::_SetViewObject(CDrawingScriptBase* pObjects)
{
    /*
    CDrawingParts* *tmp = dynamic_cast<CDrawingParts*>(ppObjects.get());
    std::unique_ptr<CDrawingParts> pParts;
    if(tmp != nullptr)
    {
        ppObjects->release();
        m_psObjects.reset(tmp);
    }
    */
    STD_ASSERT(_T("Don't use this method."));
}

/**
 *  @brief  再描画完了待ち
 *  @param  なし
 *  @retval なし
 *  @note
 */
void CPartsDef::RedrawWait()
{
    m_tharedDraw.join();
}

void CPartsDef::SetDrawLock()
{
    m_bDrawLock = true;
    while(m_bDrawing)
    {
        Sleep(10);
    }
}

void CPartsDef::ReleaseDrawLock()
{
    m_bDrawLock = false;
}


/**
 *  @brief  スレッドによる描画
 *  @param  [in] pView 
 *  @retval なし
 *  @note   pView = NULLで全画面再描画
 */
void CPartsDef::Redraw(CDrawingView* pView, bool bUseThread)
{
#ifdef _CANVAS_DGB_
    TRACE(_T("CPartsDef::Redraw \n"));
#endif
    //m_tharedDraw.interrupt();
    //m_tharedDraw.join();
//FOR_DEBUGすべて再描画
    //if (m_iCntDraw == -1)
    {
        //初回時は強制的に再描画
        pView = NULL;
    }
/*
    if (bUseThread)
    {
        m_tharedDraw = boost::thread(  boost::bind(&_CallDrawView, this, pView));
    }
    else
*/

#ifdef _DEBUG
if(pView && (pView->GetDebugMode() != CDrawingView::DM_DISABLE))
{
DB_PRINT(_T("----------------------\n"));
}
    if (!m_bDrawLock)
    {
        m_bDrawing = true;
        _DrawView(pView);
        m_bDrawing = false;
    }

if(pView && (pView->GetDebugMode() != CDrawingView::DM_DISABLE))
{
    DB_PRINT(_T("----------------------\n"));
    DB_PRINT(_T("\n"));
}
#endif
}

/**
 *  @brief  スレッドによる選択部描画
 *  @param  [in] pView 
 *  @retval なし
 *  @note   pView = NULLで全画面描画
 *  @note
 */
void CPartsDef::RedrawSelect(CDrawingView* pView)
{
#ifdef _CANVAS_DGB_
    TRACE(_T("CPartsDef::RedrawSelect \n"));
#endif
    /*
    m_tharedDraw.interrupt();
    m_tharedDraw.join();
    m_tharedDraw = boost::thread( &_CallDrawSelect, this, pView);
    */
    _DrawSelectExt(pView, true);
}

/**
 *  @brief  スレッドによる選択部描画
 *  @param  [in] pView 
 *  @retval なし
 *  @note   pView = NULLで全画面描画
 *  @note   ドラッグ中の描画(INDEXバッファには描画しない)
 */
void CPartsDef::DrawDragging(CDrawingView* pView)
{
#ifdef _DEBUG
    if (pView && (pView->GetDebugMode() != CDrawingView::DM_DISABLE))
    {
        DB_PRINT(_T("--------DrawDragging-%d--\n"), pView->GetDebugCount());
    }
#endif
#ifdef _CANVAS_DGB_
    TRACE(_T("CPartsDef::DrawDragging \n"));
#endif
    _DrawSelectExt(pView, false);


#ifdef _DEBUG
    if (pView && (pView->GetDebugMode() != CDrawingView::DM_DISABLE))
    {
        DB_PRINT(_T("-------DrawDragging END-%d\n\n"), pView->GetDebugCount());
        pView->AddDebugCount();
        DB_PRINT(_T("\n"));
    }
#endif

}


void CPartsDef::_CallDrawView(CPartsDef* pCtrl, CDrawingView* pView)
{
    pCtrl->_DrawView(pView);
}

void CPartsDef::_CallDrawSelect(CPartsDef* pCtrl, CDrawingView* pView)
{
    pCtrl->_DrawSelectExt(pView, true);
}

void CPartsDef::_CallDrawDragableMarker(CPartsDef* pCtrl, CDrawingView* pView)
{
    pCtrl->_DrawSelectExt(pView, false);
}

/**
 *  @brief  描画処理
 *  @param  [in] pView 
 *  @retval なし
 *  @note   pView = NULLで全画面再描画
 */
void CPartsDef::_DrawView( CDrawingView* pView)
{
#ifdef _DEBUG
    if (pView)
    {
        if (pView->GetDebugMode() != CDrawingView::DM_DISABLE)
        {
            DB_PRINT(_T("_DrawViewn"));
        }
    }
#endif
    if (pView == NULL)
    {
        //------------
        //データ更新
        //------------
        if (m_psObjects)
        {
            //実行中は行わない
            if (GET_APP_MODE() == EX_EDIT)
            {
                //m_psObjects->UpdateRef();
            }
        }
        //------------
        if (!m_psObjects)
        {
            return;
        }

        if (m_iCntDraw != m_psObjects->GetChgGroupCnt())
        {
            _SetDrawData(&m_layerDraw, &m_lstPoint, &m_lstNoIndex, &m_lstDrawingScriptBase);
            m_iCntDraw = m_psObjects->GetChgGroupCnt();
        }

        for(CDrawingView* pViewOne: m_lstDrawing)
        {
            _DrawView(pViewOne);
        }
        return;
    }


    bool bDrawMarker = false;

    bool bClearFront = false;
    pView->ClearDisp(bClearFront);

    try
    {
        //レイヤー順に描画
        int iPumpCnt = 0;

        if (pView->IsPrintMode())
        {
            pView->SetDrawingMode( DRAW_FRONT );
        }
        else
        {
            pView->SetDrawingMode( DRAW_VIEW );
            if (m_bPrintArea)
            {
                pView->DrawPrintArea();
            }
			if (DRAW_CONFIG->GetSnapType() & VIEW_COMMON::SNP_GRID)
			{
				pView->DrawGrid();
			}
        }

        int iLayerId = -1;
        double dCurScl =   pView->GetCurrentLayer()->dScl;
        bool bDrawScl = false;
        bool bIndex   =  ((pView->GetDrawMode() &  DRAW_INDEX) == DRAW_INDEX);
        DRAWING_MODE dmOld = pView->GetDrawMode();
        UINT dm = (dmOld ^ DRAW_INDEX);
        DRAWING_MODE dmNoIndex = static_cast<DRAWING_MODE>(dm);

        for(DRAW_OBJ_LIST& lstObj: m_layerDraw)
        {
            //-------------------
            // カレントレイヤーと異なる倍率の場合、Indexを描画しない
            iLayerId++;
            double dDiffScl = fabs(dCurScl - pView->GetLayer(iLayerId)->dScl);
            bDrawScl = ( dDiffScl < NEAR_ZERO );
            if(bIndex && !bDrawScl)
            {
                pView->SetDrawingMode(dmNoIndex);
            }

            boost::this_thread::interruption_point();
            for(auto wObject: lstObj)
            {
                boost::this_thread::interruption_point();
                auto pObject = wObject.lock();  
                if(!pObject)
                {
                    continue;
                }

                //選択状態のオブジェクトは描画しない
                if (!pObject->IsSelect())
                {
                    pObject->Draw(pView, NULL, IDrawingObject::E_MO_IGNORE_SELECT);
#ifdef _DEBUG
if (pView->GetDebugMode() != CDrawingView::DM_DISABLE)
{
    DB_PRINT(_T("_DrawView Draw %d %I64x %s\n"), pObject->GetId(), pObject.get(), pView->GetDraingMode().c_str());
}
#endif
                }

#if 0
                iPumpCnt++;
                if ((iPumpCnt % 100) == 0)
                {
                    MSG msg;
                    while ( ::GetMessage(&msg,NULL,0,0) )
                    {
                        ::TranslateMessage(&msg);
                        ::DispatchMessage(&msg);
                    }
                }
#endif
            }

            if(bIndex && !bDrawScl)
            {
                pView->SetDrawingMode(dmOld);
            }
        }

        //------------------------------
        //インデックスを描画しないタイプ
        //------------------------------
        pView->SetDrawingMode(dmNoIndex);
        for(DRAW_OBJ_LIST& lstObj: m_lstNoIndex)
        {
            boost::this_thread::interruption_point();
            for(auto wObject: lstObj)
            {
                boost::this_thread::interruption_point();
                auto pObject = wObject.lock();  
                if(!pObject)
                {
                    continue;
                }
                pObject->Draw(pView, NULL, IDrawingObject::E_MO_IGNORE_SELECT);
            }
        }
        pView->SetDrawingMode(dmOld);
        //------------------------------

        //点を描画
        iLayerId = -1;
        if (!pView->IsPrintMode())
        {
            for(DRAW_OBJ_LIST& lstObj: m_lstPoint)
            {
                //-------------------
                // カレントレイヤーと異なる倍率の場合、Indexを描画しない
                iLayerId++;
                double dDiffScl = fabs(dCurScl - pView->GetLayer(iLayerId)->dScl);
                bDrawScl = ( dDiffScl < NEAR_ZERO );
                if(bIndex && !bDrawScl)
                {
                    UINT dm = (pView->GetDrawMode() ^ DRAW_INDEX);
                    DRAWING_MODE dmNoIndex = static_cast<DRAWING_MODE>(dm);
                    pView->SetDrawingMode(dmNoIndex);
                }

 
                boost::this_thread::interruption_point();
                for(auto wObject: lstObj)
                {
                    boost::this_thread::interruption_point();
                    auto pObject = wObject.lock();  
                    if(!pObject)
                    {
                        continue;
                    }

                    pObject->Draw(pView, NULL);
#ifdef _DEBUG
    if (pView->GetDebugMode() != CDrawingView::DM_DISABLE)
    {
        DB_PRINT(_T("_DrawView Draw1 %d %I64x\n"), pObject->GetId(), pObject.get());
    }
#endif
                }

                if(bIndex && !bDrawScl)
                {
                    pView->SetDrawingMode(dmOld);
                }
            }
        }
        m_bDrawComplete = true;

        if (!pView->IsPrintMode())
        {
            _DrawSelect(pView, true);
        }
    }
    catch(boost::thread_interrupted const&)
    {
        m_bDrawComplete = false;
    } 
}

/**
 *  @brief  選択部描画処理
 *  @param  [in] pView 
 *  @param  [in] bIndex true:インデックスあり 
 *  @retval なし
 *  @note   
 */
void CPartsDef::_DrawSelectExt(CDrawingView* pView, bool bIndex)
{
    if (!m_bDrawComplete)
    {
        _DrawView( pView);
    }

    if (m_bDrawLock)
    {
        return;
    }
    _DrawSelect( pView, bIndex);
}

/**
 *  @brief  選択部描画処理
 *  @param  [in] pView 
 *  @param  [in] bIndex true:インデックスあり 
 *  @retval なし
 *  @note   _DrawViewからのみ使用する
 */
void CPartsDef::_DrawSelect(CDrawingView* pView, bool bIndex)
{
    if (pView == NULL)
    {
        foreach(CDrawingView* pDrawView, m_lstDrawing)
        {
            _DrawSelect(pDrawView, bIndex);
        }
        return;
    }
    //選択部描画
    pView->FlipBackToSelect();
#ifdef _DEBUG_SELECT

    pView->FlipSelectToFront();

#endif


    if (bIndex)
    {
        pView->SetDrawingMode(DRAW_SEL_INDEX);
    }
    else
    {
        pView->SetDrawingMode(DRAW_SEL);
    }
    _DrawDragableMarker(pView);
}

/**
 *  @brief  選択部描画処理
 *  @param  [in] pView 
 *  @retval なし
 *  @note   
 */
void CPartsDef::_DrawDragableMarker(CDrawingView* pView)
{
    CNodeMarker* pDragableMarker;
    pDragableMarker = pView->GetNodeMarker();
    try
    {
        for(auto wObject: m_lstSelect)
        {
            auto pObject = wObject.lock();  
            if(pObject)
            {
                pObject->Draw(pView, NULL, IDrawingObject::E_MO_NO);
#if _DEBUG_DRAGABBLE
if (pView->GetDebugMode() != CDrawingView::DM_DISABLE)
{
    DB_PRINT(_T("_DrawDragableMarker1 %d %I64x\n"), pObject->GetId(), pObject.get());
}
#endif
            }
        }

#ifdef _DEBUG_SELECT

    pView->FlipSelectToFront();

#endif

        for(auto wObject: m_lstConnectedSelect)
        {
            //boost::this_thread::interruption_point();
            auto pObject = wObject.lock();  
            if (pObject)
            {
                pObject->Draw(pView, NULL, IDrawingObject::E_MO_NO);
#if _DEBUG_DRAGABBLE
    if (pView->GetDebugMode() != CDrawingView::DM_DISABLE)
    {
        DB_PRINT(_T("_DrawDragableMarker2 %d %I64x\n"), pObject->GetId(), pObject.get());
    }
#endif
            }
        }

#ifdef _DEBUG_SELECT

    pView->FlipSelectToFront();

#endif
        // ６８． グループ削除時にデッドロック発生
        //boost::mutex::scoped_lock lock(m_Mtx);
        int iPumpCnt = 0;
        if (!m_lstMouseOver.empty())
        {
            for(auto wObject: m_lstMouseOver)
            {
                auto pObject = wObject.lock();  
                if (!pObject)
                {
                    continue;
                }
                //boost::this_thread::interruption_point();
                pObject->Draw(pView, NULL, IDrawingObject::E_MO_SELECTABLE);
#if _DEBUG_DRAGABBLE
    if (pView->GetDebugMode() != CDrawingView::DM_DISABLE)
    {
        DB_PRINT(_T("_DrawDragableMarker3 %d %I64x\n"), pObject->GetId(), pObject.get());
    }
#endif
            }
        }

        if (!m_lstMouseEmphasis.empty())
        {
            for(auto wObject: m_lstMouseEmphasis)
            {
                //boost::this_thread::interruption_point();
                auto pObject = wObject.lock();  
                if (!pObject)
                {
                    continue;
                }
                pObject->Draw(pView, NULL, IDrawingObject::E_MO_EMPHASIS);
#if _DEBUG_DRAGABBLE
    if (pView->GetDebugMode() != CDrawingView::DM_DISABLE)
    {
        DB_PRINT(_T("_DrawDragableMarker4 %d %I64x\n"), pObject->GetId(), pObject.get());
    }
#endif
            }
        }

#ifdef _DEBUG_SELECT

    pView->FlipSelectToFront();

#endif
        if (!m_lstMouseConnectable.empty())
        {
            for(auto   wObject: m_lstMouseConnectable)
            {
                auto pObject = wObject.lock();  
                if (!pObject)
                {
                    continue;
                }

                pObject->Draw(pView, NULL, IDrawingObject::E_MO_CONNECTABLE);
#if _DEBUG_DRAGABBLE
    if (pView->GetDebugMode() != CDrawingView::DM_DISABLE)
    {
        DB_PRINT(_T("_DrawDragableMarker5 %d %I64x\n"), pObject->GetId(), pObject.get());
    }
#endif
            }
        }
    }
    catch(boost::thread_interrupted const&)
    {
    
    }

#ifdef _DEBUG_SELECT

    pView->FlipSelectToFront();

#endif

#if _DEBUG_DRAGABBLE
    if (pView->GetDebugMode() != CDrawingView::DM_DISABLE)
    {
        DB_PRINT(_T("_DrawDragableMarker6 \n"));
    }
#endif

    pDragableMarker->Draw();
#ifdef _DEBUG_SELECT

    pView->FlipSelectToFront();

#endif

#if _DEBUG_DRAGABBLE
    if (pView->GetDebugMode() != CDrawingView::DM_DISABLE)
    {
        DB_PRINT(_T("_DrawDragableMarker7 \n"));
    }
#endif
    pView->GetMarker()->Draw();
#ifdef _DEBUG_SELECT

    pView->FlipSelectToFront();

#endif

#if _DEBUG_DRAGABBLE
    if (pView->GetDebugMode() != CDrawingView::DM_DISABLE)
    {
        DB_PRINT(_T("_DrawDragableMarker8 \n"));
    }
#endif
    pView->RedrawAction();

    pView->FlipSelectToFront();

#if _DEBUG_DRAGABBLE
    if (pView->GetDebugMode() != CDrawingView::DM_DISABLE)
    {
        DB_PRINT(_T("_DrawDragableMarker End \n\n"));
    }
#endif

}



/**
 *  @brief ウインドウ更新
 *  @param   なし
 *  @retval  なし
 *  @note                 
 */
/*
void CPartsDef::RedrawSelectAll()
{
    foreach(CDrawingView* pView, m_lstDrawing)
    {
        pView->RedrawRequest();
    }
}
*/

/**
 *  @brief 描画
 *  @param [in] iID   描画ID
 *  @retval           なし
 *  @note  基本的には通常と順序が変わってしまうので
 *         あまり使用しないようにする
 *         HideObjectとの連携を想定
 */
void CPartsDef::Draw(int iId ) 
{
    auto pData = GetObjectById( iId);
    if (!pData)
    {
        return;
    }
	
	bool bSelect = pData->IsSelect();

    foreach(CDrawingView* pView, m_lstDrawing)
    {
		pView->SetDrawingMode(DRAW_ALL);
        pData->Draw(pView, NULL);

		if (bSelect)
		{
			pView->FlipSelectToFront();
		}
		else
		{
			pView->FlipBackToSelect();
			pView->FlipSelectToFront();
		}
    }
}

/**
 *  @brief IDガベージコレクション
 *  @param [in] pCtrl   このコントロール
 *  @retval             なし
 *  @note   
 */
void CPartsDef::_GarbageCollectionId()
{
    bool bRet;
    bRet = m_psObjects->GarbageCollection(false);
    return;
}


/**
 *  @brief  ID生成
 *  @param  なし
 *  @retval ID
 *  @note   
 */
int  CPartsDef::CreateId()
{
    int iRetId;
    try
    {
        iRetId =  m_psIdObject->CreateId();
    }
    catch (MockException& e) 
    {
        if (e.GetErr() == e_gc_request)
        {
            if (m_psObjects->GarbageCollection(true))
            {
                iRetId = m_psIdObject->CreateId();
                boost::thread thGarbage( boost::bind(&CPartsDef::_GarbageCollectionId, this));
                StdSleep(1000);
            }
            else
            {
                iRetId = -1;
                AfxMessageBox(GET_ERR_STR(e_buffer_full));
            }
        }
        else if (e.GetErr() == e_buffer_full)
        {
            iRetId = -1;
            AfxMessageBox(GET_ERR_STR(e_buffer_full));
        }
        else
        {
            iRetId = -1;
            STD_ASSERT(!_T("予定していない例外"));
        }
    }
    return iRetId;
}

/**
 *  @brief ID再使用要求
 *  @param [in] iID 再使用するID
 *  @retval             なし
 *  @note   CDrawingParts::GarbageCollectionから呼び出す
 */
void CPartsDef::ReUseId(int iId)
{
    m_psIdObject->DeleteId(iId);
}

/**
 *  @brief オブジェクト追加.
 *  @param [in] pObject オブジェクト
 *  @param [in] bCreateId ID自動生成の有無
 *  @retval             なし
 *  @note   
 */
void CPartsDef::RegisterObject(std::shared_ptr<CDrawingObject> pObject, 
                          bool bCreateId,
                          bool bKeepUnfindId)
{
    CUndoAction* pUndo = NULL;
    try
    {
        // pObject
        // 自動名称設定
        if (pObject->GetName() == _T(""))
        {
            DRAWING_TYPE eType = pObject->GetType();

            if(DRAW_CONFIG->IsAutoName(eType))
            {
                StdString strName;
                strName = VIEW_COMMON::GetObjctTypeVarBaseName(eType);
                strName = CreateName(strName);
                pObject->SetNameDirect(strName.c_str());
            }
        }
        pObject->SetSelect(false);
        m_psObjects->AddData(pObject, bCreateId, bKeepUnfindId);
    } 
    catch (MockException& e) 
    {
        STD_DBG(e.GetErrMsg().c_str());
    }
}

/**
 *  @brief オブジェクト追加.
 *  @param [in] pObject オブジェクト
 *  @param [in] bManual 手動操作(手動操作時はUndoを有効にする)
 *  @retval             なし
 *  @note   
 */
void CPartsDef::AddObject2(CDrawingObject& obj, 
                           bool bCreateId, 
                           bool bKeepUnfindId)
{
    CUndoAction* pUndo = NULL;
    try
    {
        auto  pObj = CDrawingObject::CloneShared(&obj);
        RegisterObject(pObj, bCreateId, bKeepUnfindId);
    } 
    catch (MockException& e) 
    {
        STD_DBG(e.GetErrMsg().c_str());
    }
}

/**
 *  @brief データ置き換え.
 *  @param [in] pSrc 置換元オブジェクト
 *  @param [in] pDst 置換先オブジェクト
 *  @retval             なし
 *  @note   元データを外部で削除する必要がある
 *　　　　　（ここで削除しないのはUNDOでも使用するため）
 */
void CPartsDef::ReplaceObject(std::shared_ptr<CDrawingObject> pSrc, 
                              std::shared_ptr<CDrawingObject> pDst)
{
    //要テスト:
    int iId;
    try
    {
        iId = pSrc->GetId();

        pDst->SetPartsDef(this);
        pDst->SetId(iId);

        bool bSelChg = false;

        // 選択部分の差し替え
        auto ite = std::find_if(m_lstSelect.begin(), 
                                m_lstSelect.end(),
                                [=](std::weak_ptr<CDrawingObject> wObj)
                                {
                                    return (wObj.lock() ==  pSrc);
                                } );

        if (ite != m_lstSelect.end())
        {
            pDst->SetSelect(true);
            bSelChg = true;
            *ite = pDst;
        }

        ite = std::find_if(m_lstMouseOver.begin(),
                        m_lstMouseOver.end(), 
                        [=](std::weak_ptr<CDrawingObject> wObj)
                        {
                            return (wObj.lock() ==  pSrc);
                        } );
        if (ite != m_lstMouseOver.end())
        {
            *ite = pDst;
        }

        ite = std::find_if(m_lstMouseEmphasis.begin(), 
                        m_lstMouseEmphasis.end(), 
                        [=](std::weak_ptr<CDrawingObject> wObj)
                        {
                            return (wObj.lock() ==  pSrc);
                        } );

        if (ite != m_lstMouseEmphasis.end())
        {
            *ite = pDst;
        }

        ite = std::find_if(m_lstMouseConnectable.begin(), 
                        m_lstMouseConnectable.end(), 
                        [=](std::weak_ptr<CDrawingObject> wObj)
                        {
                            return (wObj.lock() ==  pSrc);
                        } );
        if (ite != m_lstMouseConnectable.end())
        {
            *ite = pDst;
        }

        // pDstのIDは内部で自動設定
        m_psObjects->Replace(iId, pDst);

        //選択部分に変更があった
        if (bSelChg)
        {
            ::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_VIEW_SEL_CHANGEE, (WPARAM)&m_lstSelect, (LPARAM)0);
        }
        UpdateNodeDataList();
    } 
    catch (MockException& e) 
    {
        STD_DBG(e.GetErrMsg().c_str());
    }
}

void CPartsDef::RegisterPrintArea(std::shared_ptr<CDrawingObject> pObject)
{
	if (pObject)
	{
		if (pObject->GetId() == ID_PAPER_FRAME)
		{
			m_psPaperFrame = pObject;
		}
		else if (pObject->GetId() == ID_PRINT_FRAME)
		{
			m_psPrintFrame = pObject;
		}
	}
}

void CPartsDef::RegisterGridDot(std::shared_ptr<CDrawingObject> pObject)
{
	m_psGrid = pObject;
}
/**
 *  @brief ビュー追加.
 *  @param [in] pView 表示部
 *  @retval           なし
 *  @note
 */
void CPartsDef::AddView(CDrawingView* pView)
{
    m_lstDrawing.insert(pView);
}

/**
 *  @brief ビュー削除.
 *  @param [in] pView 表示部
 *  @retval           なし
 *  @note
 */
void CPartsDef::DelView(CDrawingView* pView)
{
    std::set<CDrawingView*>::iterator ite;
    
    ite = m_lstDrawing.find(pView);
    if (ite != m_lstDrawing.end())
    {
        m_lstDrawing.erase(ite);
    }
}

/**
 *  @brief  ビューリスト取得.
 *  @param  [out] pViewList ビューリスト
 *  @retval なし
 *  @note
 */
void CPartsDef::GetViewList(std::set<CDrawingView*>* pViewList)
{
    *pViewList = m_lstDrawing;
}

/**
*  @brief ドラッグ中オブジェクト取得.
*  @param [in] iId オブジェクトID
*  @param [in] bIgnoreGroup false そのオブジェクトが属しているグループを返す
*  @retval     取得オブジェクト
*  @note
*/
std::shared_ptr<CDrawingObject> CPartsDef::GetDraggingObject(int iId)
{
    //-------------------------------
    // ドラッグ用一時オブジェクト
    //-------------------------------
    if (!m_lstConnecting.empty())
    {
        auto ite = std::find_if(m_lstConnecting.begin(),
            m_lstConnecting.end(),
            [=](std::shared_ptr<CDrawingObject> pObj)
        {
            return (pObj->GetId() == iId);
        });

        if (ite != m_lstConnecting.end())
        {
            return *ite;
        }
    }

    if (!m_lstTmpGroup.empty())
    {
        for(auto ite = m_lstTmpGroup.begin();
                 ite != m_lstTmpGroup.end();
                 ite++)
        {
            if ((*ite)->GetId() == ID_TEMP_GROUP)
            {
                auto pParts = std::dynamic_pointer_cast<CDrawingParts>(*ite);
                if (pParts)
                {
                    auto pRet = pParts->Find(iId);
                    if (pRet)
                    {
                        return pRet;
                    }
                }
            }

            if ((*ite)->GetId() == iId)
            {
                return *ite;
            }
        }
    }
    return NULL;
}

/**
 *  @brief オブジェクト取得.
 *  @param [in] iId オブジェクトID
 *  @param [in] bIgnoreGroup false そのオブジェクトが属しているグループを返す 
 *  @retval     取得オブジェクト
 *  @note   
 */
std::shared_ptr<CDrawingObject> CPartsDef::GetObjectById(int iId, bool bIgnoreGroup)
{

    if (!m_psObjects)
    {
        return NULL;
    }

    if (iId == -1)
    {
        return NULL;
    }

	if (iId ==ID_PAPER_FRAME)
	{
		return m_psPaperFrame;
	}

	if (iId == ID_PRINT_FRAME)
	{
		return m_psPrintFrame;
	}

	if (iId == ID_GRID_DOT)
	{
		return m_psGrid;
	}
	
	auto pObj = m_psObjects->Find(iId);

    if (bIgnoreGroup)
    {
        return pObj;
    }
    else
    {
        if (pObj)
        {
            pObj = pObj->GetTopGroup();
        }
    }
    
    if (pObj)
    {                                                         
        CLayer*   pLayer  = GetLayer(pObj->GetLayer());
        if(!pLayer->bEnable || !pLayer->bEnableForScl) 
        {
            return NULL;
        }
    }

    return pObj;
}

/**
 *  @brief  オブジェクト取得(名称).
 *  @param [in] iId オブジェクトID
 *  @retval     取得オブジェクト
 *  @note
 */
std::shared_ptr<CDrawingObject> CPartsDef::GetObject(StdString strName)
{
    return m_psObjects->Find(strName);
}

std::shared_ptr<CDrawingObject> CPartsDef::GetObjectByNameS(std::string strName)
{
    return m_psObjects->GetObjectByNameS(strName);
}


/**
 *  @brief  オブジェクト名取得.
 *  @param [in] iId     オブジェクトID
 *  @retval   オブジェクト名
 *  @note
 */
StdString  CPartsDef::GetObjectName(int iId)
{
#ifdef _TEST
    if (m_psObjects)
    {
        StdString str  = m_psObjects->GetNameById(iId);
        StdString str1 = m_psObjects->GetNameById(iId).c_str();
    }
#endif
   return m_psObjects->GetNameById(iId);
}




/**
 *  @brief  オブジェクト名設定.
 *  @param [in] iId     オブジェクトID
 *  @param [in] strName オブジェクト名
 *  @retval   成功・失敗
 *  @note
 */
bool  CPartsDef::SetObjectName(int iId, LPCTSTR strName)
{
    return m_psObjects->SetIdName( iId, strName);
}

/**
 *  @brief  オブジェクト選択(ID).
 *  @param  [in] iId     オブジェクトID
 *  @retval なし
 *  @note
 */
void CPartsDef::SelectDrawingObject(int iId)
{
    auto pObj = GetObjectById(iId);
    SelectDrawingObject(pObj);
}


/**
 *  @brief  オブジェクト選択(ID).
 *  @param  [in] pObj     オブジェクトID
 *  @retval なし
 *  @note   通常はID選択を使用する
 *          CActionCompositionLine::SelPoint で使用
 */
void CPartsDef::SelectDrawingObject(std::shared_ptr<CDrawingObject> pObj)
{
    if (!pObj)
    {
        return;
    }

    int iLayerid = pObj->GetLayer();
    CLayer* pLayer = GetLayer(iLayerid);
    STD_ASSERT(pLayer != NULL);

    if (!pLayer->bEnable)
    {
        return;
    }

    _SetSelect(pObj);

    RedrawSelect();
}
    
/**
 *  @brief  オブジェクト選択(名称).
 *  @param  [in] strName  オブジェクト名
 *  @retval なし
 *  @note
 */
void CPartsDef::SelectDrawingObject(StdString strName)
{
    auto pObj = GetObject(strName);
    SelectDrawingObject(pObj);
}

/**
 *  @brief  オブジェクト選択(範囲).
 *  @param  [in] pt1  　一点目
 *  @param  [in] pt2  　二点目
 *  @param  [in] pView　選択したView
 *  @retval なし
 *  @note
 */
void CPartsDef::SelectDrawingObject(POINT pt1,
    POINT pt2,
    const CDrawingView* pView)
{
    // 要テスト:
    std::vector< DRAW_OBJ_LIST >layerDraw;
    int               iLayerid;
    int               iPriviousLayerid = -1;
    CLayer*           pLayer;

    layerDraw.resize(GetLayerMax());


    POINT2D Pt2D[2];
    double dCurrentScl = m_lstLayer[m_curLayerId].dScl;

    //m_lstSelect.clear();
    //m_lstConnectedSelect.clear();

    auto pList = m_psObjects->GetList();
    std::set<NODE_CONNECTION_CHANGE> lstConnection;
    RECT2D rcArea;

    for (auto pObject : *pList)
    {
        iLayerid = pObject->GetLayer();
        pLayer = GetLayer(iLayerid);
        STD_ASSERT(pLayer != NULL);

        if (!pLayer->bEnable)
        {
            continue;
        }

        if (iPriviousLayerid != iLayerid)
        {
            if (m_curLayerId != iLayerid)
            {
                double dScl = m_lstLayer[iLayerid].dScl;
                if (fabs(dScl - dCurrentScl) > NEAR_ZERO)
                {
                    continue;
                }
            }

            pView->ConvScr2World(&Pt2D[0], pt1, iLayerid);
            pView->ConvScr2World(&Pt2D[1], pt2, iLayerid);
            rcArea.SetPoint(Pt2D[0], Pt2D[1]);
            iPriviousLayerid = iLayerid;
        }

        //---------------
        // 選択状態設定
        //----------------
        CDrawingObject::RELATION relation = pObject->RelationRect(rcArea);
        if ((relation & CDrawingObject::REL_INNER) == CDrawingObject::REL_INNER)
        {
            //完全に領域内
            pObject->SetSelect(true);
            m_lstSelect.push_back(pObject);
            pObject->GetChangeObjectsList(this, &lstConnection);

        }
        else
        {
            if (DRAW_CONFIG->IsSelPart())
            {
                //----------------------------------
                //一部分でも範囲に入れば選択とする
                //----------------------------------
                if ((relation & CDrawingObject::REL_PART) == CDrawingObject::REL_PART)
                {
                    //ストレッチモード時
                    if (DRAW_CONFIG->GetStretchMode())
                    {
                        if ((relation & CDrawingObject::REL_STRATCH) == CDrawingObject::REL_STRATCH)  
                        {
                            //
                            pObject->SetSelect(true, &rcArea);
                            pObject->SetRelation(true);
                            m_lstSelect.push_back(pObject);
                        }
                        else if (((relation & CDrawingObject::REL_RELATION) == CDrawingObject::REL_RELATION))
                        {
                            /* 現状未使用
                            pObject->GetId();
                            lstConnectionId.insert(pObject->GetId());
                            */
                        }
                    }
                    else
                    {
                        pObject->SetSelect(true);
                        m_lstSelect.push_back(pObject);
                        pObject->GetChangeObjectsList(this, &lstConnection);
                    }
                }
            }
        }
    }

    for (auto mData : lstConnection)
    {
        auto pObj = GetObjectById(mData.iChangeObjectId);

        if (pObj->IsSelect())
        {
            continue;
        }

        if (mData.bMove)
        {
            pObj->SetRelation(true);
        }

        m_lstConnectedSelect.push_back(pObj);
    }

    int iId = (int)m_lstSelect.size() - 1;
    ::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_VIEW_SEL_CHANGEE, (WPARAM)&m_lstSelect, (LPARAM)iId);

    UpdateNodeDataList();
    RedrawSelect();
}

/**
 *  @brief  オブジェクト選択(種別).
 *  @param  [in] eType  種別
 *  @retval なし
 *  @note
 */
void CPartsDef::SelectDrawingObject(DRAWING_TYPE eType)
{
    //要テスト:

    std::vector< DRAW_OBJ_LIST >layerDraw;
    int               iLayerid; 
    CLayer*           pLayer;

    layerDraw.resize( GetLayerMax() );


    POINT2D Pt2D[2];
    auto pList = m_psObjects->GetList();

    for(auto pObject: *pList)
    {
        iLayerid = pObject->GetLayer();

        pLayer = GetLayer(iLayerid);
        STD_ASSERT(pLayer != NULL);

        if (!pLayer->bEnable)
        {
            continue;
        }

        if (pObject->GetType()== eType)
        {
            _SetSelect(pObject);
        }
    }

    RedrawSelect();
}

/**
 *  @brief  オブジェクト選択(レイヤー).
 *  @param  [in] iLayer   レイヤーID
 *  @param  [in] bRedraw  再描画
 *  @retval なし
 *  @note
 */
void CPartsDef::SelectDrawingObjectLayer(int iLayer, bool bRedraw)
{
    //要テスト:
    std::pair<int, CDrawingObject* > pairDraw;
    std::vector< DRAW_OBJ_LIST >layerDraw;
    int               iObjLayerid; 
    CLayer*           pLayer;


    auto pList = m_psObjects->GetList();
    for(auto pObject: *pList)
    {
        iObjLayerid = pObject->GetLayer();

        pLayer = GetLayer(iObjLayerid);
        STD_ASSERT(pLayer != NULL);

        if (!pLayer->bEnable)
        {
            continue;
        }
        
        if (iObjLayerid == iLayer)
        {
            _SetSelect(pObject);
        }
    }
    RedrawSelect();
}

/**
 *  @brief  データ削除.
 *  @param  bHandOver 現状使用しない
 *  @retval なし
 *  @note   選択したデータを削除する
 */
void CPartsDef::DeleteSelectingObject(bool bHandOver)
{
    for(auto pView: m_lstDrawing)
    {
        for(auto wpData: m_lstSelect)
        {
            //画面から一時消去
            auto pData = wpData.lock();
            if (pData)
            {
                pData->DeleteView(pView, NULL);
                pView->CancelObject(pData.get());
            }
        }
    }

    auto tmpSelect = m_lstSelect;

    //選択解除
    _ClearSelect();

    //データの削除
    int iId;
    for(auto wData: tmpSelect)
    {
        auto pData = wData.lock(); 
        if(!pData)
        {
            continue;
        }
        iId = pData->GetId();
        STD_ASSERT(m_psObjects->Delete(iId, bHandOver));
    }
    UpdateNodeDataList();
}

/**
 *  @brief  選択解除
 *  @param  [in] iId オブジェクトID
 *  @retval なし
 *  @note 
 */
void CPartsDef::ReleaseDrawingObject(int iId)
{
    auto pObj = GetObjectById(iId);

    if (pObj == NULL)
    {
        return;
    }

    int iLayerid = pObj->GetLayer();
    CLayer* pLayer = GetLayer(iLayerid);
    STD_ASSERT(pLayer != NULL);

    if (!pLayer->bEnable)
    {
        return;
    }

    _ClearSelect(pObj.get());

    RedrawSelect();
}

/**
 *  @brief  指定データ削除
 *  @param  [in] pObject
 *  @retval なし
 *  @note   
 */
void CPartsDef::DeleteDrawingObject(CDrawingObject* pObject)
{
    foreach(CDrawingView* pView, m_lstDrawing)
    {
        //画面から一時消去
        pObject->DeleteView(pView, NULL);
        pView->CancelObject(pObject);
    }

    //選択解除
    _ClearSelect();

    //データの削除
    int iId;
    iId = pObject->GetId();
    STD_ASSERT(m_psObjects->Delete(iId));
}




/**
 *  @brief  データ更新.
 *  @param   なし
 *  @retval  なし
 *  @note  
 */
void CPartsDef::Update()
{
    //選択解除
    _ClearSelect();
    m_psObjects->UpdateRef();
}

/**
 *  @brief  データ非表示.
 *  @param [in] iID データID
 *  @retval           なし
 *  @note  データ内容変更時の再描画に使用する
 *         （一旦消して新たなデータで再描画する)
  */
void CPartsDef::HideObject(int iId)
{
    auto pData = GetObjectById( iId);
    if (!pData)
    {
        return;
    }

    for(auto pView: m_lstDrawing)
    {
        pView->SetDrawingMode( DRAW_ALL );
        pData->DeleteView(pView, NULL);
    }
}


/**
 *  @brief      選択解除.
 *  @param      なし
 *  @retval     なし
 *  @note       描画あり
 */
void CPartsDef::RelSelect()
{
       // boost::mutex::scoped_lock lock(m_Mtx);

// ウインドウメッセージ起点の関数に
// ミューテックスを使っているのでデッドロックする
// TODO: ロック方式を変える
    for(auto pView: m_lstDrawing)
    {
        DRAWING_MODE eMode;
        eMode = pView->GetDrawMode();
        pView->SetDrawingMode( DRAW_ALL);

        for(auto wData: m_lstSelect)
        {
            auto pData = wData.lock();
            if(!pData)
            {
                continue;
            }
            pData->SetSelect(false);
            pData->Draw(pView, NULL);
        }

        for (auto wData : m_lstConnectedSelect)
        {
            auto pData = wData.lock();
            if (!pData)
            {
                continue;
            }
            pData->SetSelect(false);
            pData->Draw(pView, NULL);
        }
        pView->SetDrawingMode( eMode);
    }
    _ClearSelect();
}

/**
 *  @brief 選択解除.
 *  @param [in] iId 選択解除ID
 *  @retval           なし
 *  @note       描画あり
 */
void CPartsDef::RelSelectById(int iId)
{
    //std::vector<CDrawingObject*>::iterator itFind;   

    // TODO: ロック方式を変える
    //boost::mutex::scoped_lock lock(m_Mtx);


    auto itFind = std::find_if(m_lstSelect.begin(),
                               m_lstSelect.end(),
        [iId](std::weak_ptr<CDrawingObject> wObj)
        {
            auto pObj = wObj.lock();
            if(pObj)
            {
                return (pObj->GetId() == iId);
            }
            else
            {
                return false;
            }
        
        });

    if (itFind == m_lstSelect.end())
    {
        return;
    }

    auto wData = *itFind;

    auto pData = wData.lock();

    if(pData)
    {
        pData->SetSelect(false);
        for(auto pView: m_lstDrawing)
        {
            DRAWING_MODE eMode;
            eMode = pView->GetDrawMode();
            pView->SetDrawingMode( DRAW_ALL);

            pData->Draw(pView, NULL);

            pView->SetDrawingMode( eMode);
        }
    }

    //選択リストから除外
    m_lstSelect.erase(itFind);
    UpdateNodeDataList();
    ::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_VIEW_SEL_CHANGEE, (WPARAM)&m_lstSelect, (LPARAM)0);

}

/**
 *  @brief  選択図形数.
 *  @param  なし
 *  @retval 選択している図形の数
 *  @note
 */
long CPartsDef::SelectNum()
{
    return (long)m_lstSelect.size();
}

std::vector<std::weak_ptr<CDrawingObject>>*  CPartsDef::GetConnectionToSelect()
{
    return &m_lstConnectedSelect;
}

void CPartsDef::ClearConnectionToSelect()
{
	for (auto wObj : m_lstConnectedSelect)
	{
		auto pObj = wObj.lock();
		if (pObj)
		{
			pObj->ClearStatus();
		}
	}
	m_lstConnectedSelect.clear();
}

/**
 *  @brief  選択図形に接続している図形のリスト
 *  @param  なし
 *  @retval なし
 *  @note
 */
void CPartsDef::UpdateNodeDataList()
{
    //m_lstConnectedSelect に選択された図形は   DrawDragging(_DrawDragableMarkerで描画する)
    //m_lstConnectedSelect.clear();
    /*
    for(auto wObj: m_lstSelect)
    {
        auto pObj = wObj.lock();  
        if(!pObj)
        {
            continue;
        }
        pObj->GetConnectionObjectList(&m_lstConnectedSelect);
    }
    */
/*
    for(auto wSelObj: m_lstConnectedSelect)
    {
        auto ite = std::find_if(m_lstSelect.begin(),
                             m_lstSelect.end(), 
                    [=](const std::weak_ptr<CDrawingObject> wObj)
                    {
                        return (wObj.lock() ==  wSelObj.lock());
                    });

        if(ite != m_lstSelect.end())
        {
            m_lstSelect.erase(ite);
        }

        auto pSelObj = wSelObj.lock();
        if(pSelObj)
        {
            pSelObj->SetSelect(false);
        }
    }
*/
}

/**
 *  @brief  マウスオーバー設定
 *  @param  [in] pObject
 *  @retval なし
 *  @note
 */
void CPartsDef::SetMouseOver(std::weak_ptr<CDrawingObject> pObject)
{
    //boost::mutex::scoped_lock lock(m_Mtx);

    m_lstMouseOver.push_back(pObject);

}

//!< 
/**
 *  @brief  マウスオーバー解除
 *  @param  なし
 *  @retval なし
 *  @note
 */
void CPartsDef::ClearMouseOver()
{
    //グループ削除時にデッドロック発生
    boost::mutex::scoped_lock lock(m_Mtx);
    m_lstMouseOver.clear();
}

/**
 *  @brief  マウスオーバー(強調)設定
 *  @param  [in] pObject
 *  @retval なし
 *  @note
 */
void CPartsDef::SetMouseEmphasis(std::weak_ptr<CDrawingObject> pObject)
{
    //boost::mutex::scoped_lock lock(m_Mtx);
    m_lstMouseEmphasis.push_back(pObject);

}

/**
 *  @brief  マウスオーバー(強調)解除
 *  @param  なし
 *  @retval なし
 *  @note
 */
void CPartsDef::ClearMouseEmphasis()
{
    //グループ削除時にデッドロック発生
    //boost::mutex::scoped_lock lock(m_Mtx);
    m_lstMouseEmphasis.clear();
}

/**
 *  @brief  マウスオーバー(接続可能)設定
 *  @param  [in] pObject
 *  @retval なし
 *  @note
 */
void CPartsDef::SetMouseConnectable(std::weak_ptr<CDrawingObject> pObject)
{                         
    //boost::mutex::scoped_lock lock(m_Mtx);
    m_lstMouseConnectable.push_back(pObject);

}

/**
 *  @brief  マウスオーバー((接続可能)解除
 *  @param  なし
 *  @retval なし
 *  @note
 */
void CPartsDef::ClearMouseConnectable()
{
    //グループ削除時にデッドロック発生
    //boost::mutex::scoped_lock lock(m_Mtx);
    m_lstMouseConnectable.clear();
}




/**
 *  データ変更コールバック.
 *  @param [in] pCtrl 自オブジェクト
 *  @param [in] iId   データID
 *  @param [in] eType コールバックタイプ
 *  @retval     なし
 *  @note
 */
void CPartsDef::ChgCallback(CPartsDef* pCtrl, int iId, int eType)
{
    switch(eType)
    {
    case E_CHG_NAME:
        //名称変更

        break;
    
    default:
        return;
    }
}


/**
 * @brief レイヤー初期化
 * @param   なし         
 * @retval  なし
 * @note	
 */
void  CPartsDef::_InitLayer()
{
    int iNo = 0;

    m_lstLayer.resize(2);

    foreach(CLayer& Layer, m_lstLayer)
    {
        Layer.iLayerId     = iNo;
        Layer.bEnable      = true;
        Layer.bEnableForScl   = true;
        Layer.bVisible  = true;
        Layer.bUseLayerColor = false;
        Layer.crLayer   = DRAW_CONFIG->crFront;
        Layer.dScl      = 1.0;
        Layer.strName   = _T("");
        iNo++;
    }

    m_curLayerId = 1;
}

/**
 * @brief   カレントレイヤーID設定
 * @param   [in]    iLayerId     レイヤーID
 * @param   なし          
 * @retval  なし
 * @note	
 */
void CPartsDef::SetCurrentLayerId(int iLayerId, bool bManual)
{
    CUndoAction* pUndo = NULL;
    if (!bManual)
    {
        //TAG:[Undo]
    }

    STD_ASSERT(iLayerId >= 0);
    STD_ASSERT(iLayerId < GetLayerMax());

    bool bUpdate = false;

    if(m_curLayerId != iLayerId)
    {
        bUpdate = true;
    }
    m_curLayerId = iLayerId;

    double dScl = m_lstLayer[iLayerId].dScl;

    foreach(CLayer& Layer, m_lstLayer)
    {
        if (!DRAW_CONFIG->bDisableDifferentScl)
        {
            if (fabs(dScl-Layer.dScl) > NEAR_ZERO)
            {
                Layer.bEnableForScl = false;
            }
            else
            {
                Layer.bEnableForScl = true;
            }
        }
        else
        {
            Layer.bEnableForScl = true;    
        }
    }

    if(bUpdate)
    {
       ::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_UPDATE_CURRENT_LAYER_NUM, (WPARAM)m_curLayerId, (LPARAM)0);
    }
}

/**
 * @brief   カレントレイヤーID取得
 * @param   なし
 * @param   なし
 * @retval  カレントレイヤーID
 * @note	
 */
int CPartsDef::GetCurrentLayerId() const
{
    return m_curLayerId;
}

/**
 * @brief   レイヤー設定
 * @param   [in]    iLayerId  レイヤーID
 * @param   [in]    pLayer    レイヤーデータ        
 * @retval   なし
 * @note	
 */
void CPartsDef::SetLayer(int iLayerId, const CLayer* pLayer)
{
    STD_ASSERT(iLayerId >= 0);
    STD_ASSERT(iLayerId < GetLayerMax());

    m_lstLayer[iLayerId] = *pLayer;
}

/**
 * @brief   レイヤー取得
 * @param   [in]    iLayerId  レイヤーID
 * @param   なし          
 * @retval  レイヤーデータ
 * @note	
 */
CLayer* CPartsDef::GetLayer(int iLayerId)
{
    STD_ASSERT(iLayerId >= 0);
    STD_ASSERT(iLayerId < GetLayerMax());
    
    return   &m_lstLayer[iLayerId];
}

/**
 * @brief   レイヤー追加
 * @param   [in]    iLayerId  レイヤーID
 * @param   なし          
 * @retval  レイヤーデータ
 * @note	
 */
void CPartsDef::AddLayer()
{
    CLayer layer;

    layer.iLayerId  = GetLayerMax();
    layer.bEnable   = true;
    layer.bVisible  = true;
    layer.bUseLayerColor = false;
    layer.crLayer   = DRAW_CONFIG->crFront;
    layer.strName   = _T("");

    if (m_lstLayer.size() >= 2)
    {
        layer.dScl = m_lstLayer[1].dScl;
    }
    else
    {
        layer.dScl = 1.0;
    }

    m_lstLayer.push_back(layer);
}

/**
 * @brief   レイヤー削除
 * @param   [in]    iLayerId  レイヤーID
 * @param   [in]    bForace   強制削除
 * @param   なし          
 * @retval  削除可否
 * @note	
 */
bool CPartsDef::DelLayer(int iLayerId, bool bForace)
{
    STD_ASSERT(iLayerId >= 0);
    STD_ASSERT(iLayerId < GetLayerMax());

    if (m_lstLayer.size() <= 2)
    {
        return false;
    }

    RelSelect();
    SelectDrawingObjectLayer(iLayerId, false);

    if (!bForace)
    {
        if (m_lstSelect.size() != 0)
        {
            RelSelect();
            return false;
        }
    }

    DeleteSelectingObject();


    //----------------------
    //LayerIDの振りなおし
    //----------------------
    // Object   
    std::vector< DRAW_OBJ_LIST >layerDraw;
    int               iObjLayerid; 

    auto pList = m_psObjects->GetList();
    for(auto pObject: *pList)
    {
        iObjLayerid = pObject->GetLayer();

        if (iObjLayerid >= iLayerId)
        {
            pObject->SetLayer(iObjLayerid - 1);
        }
    }

    // Layer
    m_lstLayer.erase(m_lstLayer.begin() + iLayerId);

    std::vector<CLayer>::iterator iteLayer;
    for(iteLayer  = (m_lstLayer.begin() + iLayerId);
        iteLayer != m_lstLayer.end();
        iteLayer++)
    {
        iteLayer->iLayerId--; 
    }

    if( m_curLayerId >= iLayerId)
    {
        int iLayerId = m_curLayerId - 1;
        SetCurrentLayerId(iLayerId);
    }
    return  true;
}

/**
 * @brief   レイヤー最大数取得
 * @param   なし          
 * @retval  レイヤー数
 * @note	
 */
int CPartsDef::GetLayerMax() const
{
    return int(m_lstLayer.size());
}

/**
 * @brief   トリム
 * @param   [in]    iId          オブジェクトID
 * @param   [in]    ptIntersect  変更位置
 * @param   [in]    ptDir        選択位置(残しておく方）
 * @param   [in]    bRverse      true 選択した方を変更する
 * @param   なし          
 * @retval  レイヤーデータ
 * @note	
 */
bool CPartsDef::Trim(int iId, const POINT2D& ptIntersect, 
                                    const POINT2D& ptDir, bool bRverse)
{
    //一旦消去して再描画

    CUndoAction* pUndo = NULL;

    HideObject(iId);

    auto pObject = GetObjectById(iId);

    if (!pObject)
    {
        return false;
    }

    bool bRet = pObject->Trim(ptIntersect, ptDir, bRverse);
    Draw(iId);


    //選択解除
    RelSelect();

    return bRet;
}

/**
 * @brief   分割
 * @param   [in]    iId          オブジェクトID
 * @param   [in]    ptIntersect  分割位置
 * @param   [in]    bManual 手動操作(手動操作時はUndoを有効にする)
 * @param   なし          
 * @retval  true 分割完了 false 分割失敗
 * @note	
 */
bool CPartsDef::BreakPoint( int iId, const POINT2D& ptBreak, bool bManual)
{
    auto pObject = GetObjectById(iId);

    if (!pObject)
    {
        return false;
    }

    CUndoAction* pUndo = NULL;
    if (!bManual)
    {
        GetUndoAction()->AddStart(UD_CHG, this, pObject.get());
    }

    HideObject(iId);

    auto pBreakObject = pObject->Break(ptBreak);

    bool bRet = false;
    if (pBreakObject)
    {
        RegisterObject(pBreakObject, true, false);
        int iBreakId = pBreakObject->GetId();
        Draw(iBreakId);
        bRet = true;

       if (!bManual)
       {
            GetUndoAction()->AddEnd(pObject);
            GetUndoAction()->Add(UD_ADD, this, NULL, pBreakObject, true);
       }
    }
    else
    {
       if (!bManual)
       {
            GetUndoAction()->AddCancel();
       }
    }
    Draw(iId);

    //選択解除
    RelSelect();

    return bRet;
}



/**
 * @brief   オブジェクト選択
 * @param   選択オブジェクト
 * @param   なし          
 * @retval  なし
 * @note	プロパティウインドウを更新するため
 */
void CPartsDef::_SetSelect(std::weak_ptr<CDrawingObject> wObj, const RECT2D* pRect)
{
    // TODO: ロック方式を変える
    //boost::mutex::scoped_lock lock(m_Mtx);

    auto pObj = wObj.lock();

    if(!pObj)
    {
        return;
    }

    if (DRAW_CONFIG->GetStretchMode())
    {
        pObj->SetSelect(true, pRect);
    }
    else
    {
        pObj->SetSelect(true, NULL);
#ifdef _DEBUG
        pObj->DebugPrintOnSelect();
#endif
    }

    int  iId = 0;
    bool bFind = false;

    for(auto pObjFind: m_lstSelect)
    {
        if (pObjFind.lock() == pObj)
        {
            bFind = true;
            break;
        }
        iId++;
    }

    if (!bFind)
    {
        m_lstSelect.push_back(wObj);

        iId = (int)m_lstSelect.size() - 1;
    }

    //-----------------------------
    std::set<NODE_CONNECTION_CHANGE> lstConnection;
    pObj->GetChangeObjectsList(this, &lstConnection);

    for (auto mData : lstConnection)
    {
        auto pObj = GetObjectById(mData.iChangeObjectId);

        if (pObj->IsSelect())
        {
            continue;
        }

        if (mData.bMove)
        {
            pObj->SetRelation(true);
        }

        m_lstConnectedSelect.push_back(pObj);
    }

    ::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_VIEW_SEL_CHANGEE, (WPARAM)&m_lstSelect, (LPARAM)iId);
}

/**
 * @brief   オブジェクト選択解除
 * @param   なし [in] pObj 選択オブジェクト
 * @retval  なし
 * @note	プロパティウインドウを更新するため
 */
void CPartsDef::_ClearSelect(CDrawingObject* pObj)
{
    // TODO: ロック方式を変える
    //boost::mutex::scoped_lock lock(m_Mtx);
    if (pObj == NULL)
    {
        for(auto pSelObj:  m_lstSelect)
        {
            auto pObj = pSelObj.lock();
            if(!pObj)
            {
                continue;
            }
            pObj->SetSelect(false);
        }

        m_lstSelect.clear();
    }
    else
    {
        auto ite = std::find_if(m_lstSelect.begin(), 
                                m_lstSelect.end(), 
                                [=](std::weak_ptr<CDrawingObject> wObj)
                                {
                                    return (wObj.lock().get() ==  pObj);
                                } );
        if (ite != m_lstSelect.end())
        {
            m_lstSelect.erase(ite);
        }
        pObj->SetSelect(false);
    }


	ClearConnectionToSelect();
	UpdateNodeDataList();
    ::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_VIEW_SEL_CHANGEE, (WPARAM)&m_lstSelect, (LPARAM)0);
}

//デバッグ用
bool CPartsDef::_CheckSelect()
{
	std::set<int> sSel;
	auto pObjctList = m_psObjects->GetList();

	for (auto pObj : *pObjctList)
	{
		if (pObj->IsSelect())
		{
			sSel.insert(pObj->GetId());
		}
	}

	for (auto pSelObj : m_lstSelect)
	{
		auto pObj = pSelObj.lock();
		if (!pObj)
		{
			continue;
		}
		
		int iId = pObj->GetId();

		auto ite = sSel.find(iId);
		if (ite == sSel.end())
		{
			DB_PRINT(_T("_CheckSelect id:%d is not find\n"), iId);
			return false;
		}
		else
		{

			sSel.erase(ite);
		}
	}

	if (sSel.empty())
	{
		return true;
	}

	DB_PRINT(_T("_CheckSelect missmutch"));
	for (int id : sSel)
	{
		DB_PRINT(_T("%d,"), id);
	}
	DB_PRINT(_T("\n"));
	return false;
}

/**
 * @brief   選択オブジェクトリスト取得
 * @retval  選択オブジェクト
 * @note    
 */
std::vector<std::weak_ptr<CDrawingObject>>*  CPartsDef::GetSelectInstance()
{
    return &m_lstSelect;
}

//!< 
/**
 * @brief   選択確認
 * @param   [in] オブジェクト位置
 * @retval  true あり
 * @note    
 */
bool CPartsDef::IsSelected(CDrawingObject* pObject)
{
    auto ite = std::find_if(m_lstSelect.begin(), 
                            m_lstSelect.end(),
                            [=](std::weak_ptr<CDrawingObject> wObj)
                            {
                                auto pObj = wObj.lock();
                                if( pObj )
                                {
                                    return (pObj.get() ==  pObject);
                                }
                                else
                                {
                                    return false;
                                }
                            } );
    if(ite ==  m_lstSelect.end())
    {
        return false;
    }
    return true;
}

/**
 * @brief   選択オブジェクト取得
 * @param   オブジェクト位置
 * @retval  選択オブジェクト
 * @note    
 */
std::shared_ptr<CDrawingObject>  CPartsDef::GetSelect(int iIndex)
{
    STD_ASSERT(iIndex >= 0);
    STD_ASSERT(iIndex < (int)m_lstSelect.size());
    return m_lstSelect[iIndex].lock();
}


/**
 * @brief   Ioウインドウ表示ページ取得
 * @param   [in] iIndex IOウインドウインデックス
 * @retval  Ioウインドウ表示ページ
 * @note    未設定時は-1を返す
 */
int CPartsDef::GetIoPage(int iIndex) const
{
    STD_ASSERT(iIndex >= 0);
    STD_ASSERT(static_cast<int>(m_lstIoPage.size()) < iIndex);

    return m_lstIoPage[iIndex];
}

/**
 * @brief   Ioウインドウ表示ページ設定
 * @param   [in] iIndex IOウインドウインデックス
 * @param   [in] iPage  表示ページ
 * @retval  false 失敗
 * @note    
 */
bool CPartsDef::SetIoPage(int iIndex, int iPage)
{
    STD_ASSERT(iIndex >= 0);
    STD_ASSERT(static_cast<int>(m_lstIoPage.size()) < iIndex);
    if (iIndex < 0){return false;}
    if (static_cast<int>(m_lstIoPage.size()) <= iIndex){return false;}
    m_lstIoPage[iIndex] = iPage;
    return true;
}


/**
 * @brief   //Ioウインドウ表示オブジェクトID取得
 * @param   [in] iIndex IOウインドウインデックス
 * @retval  表示オブジェクトID
 * @note    未設定時は-1を返す
 */
int CPartsDef::GetIoObjId(int iIndex) const
{
    STD_ASSERT(iIndex >= 0);
    STD_ASSERT(static_cast<int>(m_lstIoObj.size()) < iIndex);

    return m_lstIoObj[iIndex];

}

/**
 * @brief   Ioウインドウ表示オブジェクトID設定
 * @param   [in] iIndex IOウインドウインデックス
 * @param   [in] iObjId  表示オブジェクトID
 * @retval  false 失敗
 * @note    
 */
bool CPartsDef::SetIoObjId(int iIndex, int iObjId)
{
    STD_ASSERT(iIndex >= 0);
    STD_ASSERT(static_cast<int>(m_lstIoObj.size()) < iIndex);
    if (iIndex < 0){return false;}
    if (static_cast<int>(m_lstIoObj.size()) <= iIndex){return false;}
    m_lstIoObj[iIndex] = iObjId;
    return true;
}

/**
 * @brief   変更の有無
 * @param   なし
 * @retval  true 変更あり
 * @note    
 */
bool CPartsDef::IsChange() const
{
    if (CObjectDef::IsChange())
    {
        return true;
    }
    //TODO:実装

    //図形の変更

    //プロパティの変更
    CPropertySet* pPropertySet = GetUserPropertySet();
    if (pPropertySet)
    {
        if (pPropertySet->GetChgCnt() != 0)
        {
            return true;
        }
    }

    //IOの変更

    //スクリプトの変更
    return false;


}

/**
 * @brief   コンパイル済み
 * @param   なし
 * @retval  false 要再コンパイル
 * @note    変更の有無とは少し異なる  
 */
bool CPartsDef::IsCompiled( std::vector<StdString>* pLstFileName) const
{
    //TODO:実装

    //図形の追加・削除

    //プロパティの追加・削除

    //IOの追加・削除

    //スクリプトの変更
    bool bRet;
    bRet = CObjectDef::IsCompiled( pLstFileName);

    DRAWING_TYPE eType;

    std::shared_ptr<CObjectDef>   pDef;
    auto pList = m_psObjects->GetList();
    for(auto pObject: *pList)
    {
        eType = pObject->GetType();

        if (eType & DT_SCRIPTBASE)
        {
            auto pScriptBase = std::dynamic_pointer_cast<CDrawingScriptBase>(pObject);
            STD_ASSERT(pScriptBase);
            if (!pScriptBase)
            {
               continue;
            }

            pDef = pScriptBase->GetObjectDef().lock();

            STD_ASSERT(pDef);
            if (!pDef)
            {
                if (!pDef->IsCompiled( pLstFileName))
                {
                    bRet = false;
                }
            }
        }
    }
    return bRet;
}

/**
 *  @brief  構築
 *  @param  [in] pScriptBase 
 *  @param  [in] bDebug    true:デバッグモード
 *  @param  [in] bRebuild  true:強制的にコンパイル
 *  @retval true 成功
 *  @note   CObjectDef::BuildThreadより呼び出し
 */
bool CPartsDef::Compile( bool bDebug, bool bRebuild)
{
    bool bCompile = true;

    if (!bRebuild)
    {
        COMPILE_STS stsCompile;
        stsCompile = m_psModule->GetCompileStatus();

        //現状では CS_SUCCESSとCS_SUCCESS_Dの判別の必要なし
        if ( (stsCompile == CS_SUCCESS) ||
             (stsCompile == CS_SUCCESS_D))
        {
            bCompile = false;
        }
    }

    bool bRet = true;

    if (bCompile)
    {
        bRet = m_psModule->Compile(bDebug);
    }

    DRAWING_TYPE eType;

    std::shared_ptr<CObjectDef>   pDef;
    auto pList = m_psObjects->GetList();
    for(auto pObject: *pList)
    {
        eType = pObject->GetType();

        if (eType & DT_SCRIPTBASE)
        {
            auto pScriptBase = std::dynamic_pointer_cast<CDrawingScriptBase>(pObject);
            STD_ASSERT(pScriptBase);
            if (!pScriptBase)
            {
               continue;
            }

            pDef = pScriptBase->GetObjectDef().lock();

            STD_ASSERT(pDef);
            if (!pDef)
            {
                bRet = false;
                continue;
            }

            if(!pDef->Compile( bDebug, bRebuild))
            {
                bRet = false;
            }
        }
    }
    return bRet;
}

void CPartsDef::_ReconnectIo()
{
    //=======================
    // IO接続を再設定する
    //=======================
    std::map< int, std::shared_ptr<CIoConnect> >* pMapConnect;
    std::map< int, std::shared_ptr<CIoConnect> >::iterator iteMap;

    pMapConnect = m_psIoDef->GetConnectMap();

    int iParentId;
    int iIoId;
    CIoDefBase*  pIo;
    CIoConnect*  pConnect;
    std::weak_ptr<CIoProperty> pIoProp;

    for(iteMap  = pMapConnect->begin();
        iteMap != pMapConnect->end();
        iteMap++)
    {

        pConnect = iteMap->second.get();
        pConnect->GetId(&iParentId, &iIoId);
        pIo = NULL;

        if (iParentId == 0)
        {
            pIo = m_psIoDef;
        }
        else
        {
            auto pObj = GetDrawingParts()->Find(iParentId);
            auto pBase = std::dynamic_pointer_cast<CDrawingScriptBase>(pObj);

            STD_ASSERT(pBase);
            if (pBase)
            {
                pIo = pBase->GetIoRef();
            }
        }

        STD_ASSERT(pIo);
        if (pIo)
        {
            bool bRet;
            CIoPropertyBlock*  pBlock;
            pBlock = pIo->GetBlock();

            if (pBlock == NULL)
            {
                pIo->LoadAfter();
                pBlock = pIo->GetBlock();
            }

            STD_ASSERT(pBlock);

            bRet = pBlock->GetPropertyWeak(&pIoProp, iIoId);
            STD_ASSERT(bRet);

            bRet = pConnect->SetConnect(pIo, iIoId);
            STD_ASSERT(bRet);
        }
    }
}


/**
 * @brief   ロード終了後処理
 * @param   なし
 * @retval  なし
 * @note	
 */
void CPartsDef::LoadAfter()
{
    CObjectDef::LoadAfter();
    _ReconnectIo();
    SetCurrentLayerId(1);
    GetDrawingParts()->LoadAfter(this);

    /*
    for (auto mapData: m_mapImage)
    {
        mapData.second->LoadAfter();
    }
    */

}



/**
 * @brief   ファイル読み込み
 * @param   Path ファイルへのパス
 * @retval  true 読み込み成功
 * @note	
 */
bool CPartsDef::Load(StdPath Path)
{
    StdString strError;
    if (!boost::filesystem::exists(Path))
    {
        strError = GET_ERR_STR(e_file_exist);
        STD_DBG(strError.c_str()); 
        return false;
    }

    _SetCurrentPath(Path.parent_path());
    StdString strExt = GetPathStr(Path.extension());
    strExt = CUtil::ToUpper(strExt);
    bool bRet = true;

    if (strExt == _T(".MSD") )
    {
        boost::filesystem::ifstream inFs(Path,std::ios::binary);
        try
        {
            {
                boost::archive::binary_iarchive inBin(inFs);
                inBin >> *this;
            }
            inFs.close();
        }
        catch(std::exception& e )
        {
            inFs.close();
            strError = MockException::FileErrorMessage(true, Path, 
                                      CUtil::CharToString(e.what()));
            bRet = false;
        }
        catch(...)
        {
            inFs.close();
            strError = MockException::FileErrorMessage(true, Path);
            bRet = false;
        }
    }
	else if (strExt == _T(".MSDT"))
	{
		StdStreamIn  inFs(Path);
		try
		{
			{
				boost::archive::text_wiarchive txtIn(inFs);
				txtIn >> *this;
			}
			inFs.close();
		}
		catch (std::exception& e)
		{
			inFs.close();
			strError = MockException::FileErrorMessage(true, Path,
				CUtil::CharToString(e.what()));
			bRet = false;
		}
		catch (...)
		{
			inFs.close();
			strError = MockException::FileErrorMessage(true, Path);
			bRet = false;
		}
	}
	else if (strExt == _T(".MSDX") )
    {
        StdStreamIn inFs(Path);
        try
        {
            StdString strFileName = CUtil::CharToString(Path.string().c_str());
            {
                StdXmlArchiveIn inXml(inFs);
//DB_PRINT(_T("CPartsDef::Load(%s) Start \n"), strFileName.c_str());
                inXml >> boost::serialization::make_nvp("Root", *this);
            }
//DB_PRINT(_T("CPartsDef::Load(%s) End \n"), strFileName.c_str());
            inFs.close();
        }
        catch(std::exception& e )
        {
            //TODO:まだ行番号は取れない
           boost::filesystem::ifstream::pos_type pos, posErr;
           posErr = inFs.tellg();
           inFs.clear();
           inFs.seekg(0, std::ios_base::beg);
           int iLineNo = 0;
           StdString  strLine;
           StdString  strLineOld;
            while( !inFs.eof() ) 
            {
                std::getline(inFs,strLine);
                pos = inFs.tellg();
                if (pos > posErr)
                {
//DB_PRINT(_T("Line(%d) %s\n"), iLineNo, strLineOld.c_str());
                    break;
                }
                strLineOld = strLine;
                iLineNo++;
            }


            inFs.close();
            strError = MockException::FileErrorMessage(true, Path, 
                                      CUtil::CharToString(e.what()));
            bRet = false;
        }
        catch(...)
        {
            inFs.close();
            strError = MockException::FileErrorMessage(true, Path);
            bRet = false;
        }
    }
    else
    {
        StdString strExtErr;
        strExtErr =  GET_ERR_STR(e_file_ext);
        strExtErr += _T("(");
        strExtErr += strExt;
        strExtErr += _T(")");
        strError = MockException::FileErrorMessage(true, Path, strExtErr.c_str());
        bRet = false;
    }

    if (!bRet)
    {
        STD_DBG(strError.c_str()); 
    }
#ifdef _DEBUG
 Print();
#endif
    return bRet;
}

/**
 * @brief   ファイル保存
 * @param   Path ファイルへのパス
 * @retval  true 保存み成功
 * @note	
 */
bool CPartsDef::Save(StdPath Path)
{
    StdPath pathOld = m_pathCurrent;
    _SetCurrentPath(Path.parent_path());

    StdString strExt = GetPathStr(Path.extension());
    strExt = CUtil::ToUpper(strExt);
    StdString strError;
    bool bRet = true;
    if (strExt == _T(".MSD") )
    {
        boost::filesystem::ofstream outFs(Path, std::ios::binary);
        try
        {
            SetSingleFileMode();
            {
                boost::archive::binary_oarchive outBin(outFs);
                outBin << *this;
            }
            outFs.close();
        }
        catch(std::exception& e )
        {
            //ファイル%sの書き込みに失敗しました;
            outFs.close();
            strError = MockException::FileErrorMessage(false, Path, 
                                      CUtil::CharToString(e.what()));
            bRet = false;
        }
        catch(...)
        {
            outFs.close();
            strError = MockException::FileErrorMessage(false, Path);
            bRet = false;
        }

    }
	else if (strExt == _T(".MSDT"))
	{
		StdStreamOut outFs(Path);
		try
		{
			SetSingleFileMode();
			{
				boost::archive::text_woarchive txtOut(outFs);
				txtOut << *this;
			}
			outFs.close();
		}
		catch (std::exception& e)
		{
			//ファイル%sの書き込みに失敗しました;
			outFs.close();
			strError = MockException::FileErrorMessage(false, Path,
				CUtil::CharToString(e.what()));
			bRet = false;
		}
		catch (...)
		{
			outFs.close();
			strError = MockException::FileErrorMessage(false, Path);
			bRet = false;
		}

	}
	else if (strExt == _T(".MSDX") )
    {
        StdStreamOut outFs(Path);
        try
        {
            SetSingleFileMode(false);
            {
                StdXmlArchiveOut outXml(outFs);
                outXml << boost::serialization::make_nvp("Root", *this);
            }
            outFs.close();
        }
        catch(std::exception& e )
        {
            outFs.close();
            strError = MockException::FileErrorMessage(false, Path, 
                                      CUtil::CharToString(e.what()));
            bRet = false;
        }
        catch(...)
        {
            outFs.close();
            strError = MockException::FileErrorMessage(false, Path);
            bRet = false;
        }
    }
    else
    {
        StdString strExtErr;
        strExtErr =  GET_ERR_STR(e_file_ext);
        strExtErr += _T("(");
        strExtErr += strExt;
        strExtErr += _T(")");
        strError = MockException::FileErrorMessage(false, Path, strExtErr);
        bRet = false;
    }

    if (!bRet)
    {
        STD_DBG(strError.c_str()); 
        _SetCurrentPath(pathOld);
    }
    return bRet;
}

/**
 * @brief   シリアル化(ロード)
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  なし
 * @note	
 */
template<class Archive>
void CPartsDef::load(Archive& ar, const unsigned int version)
{
    m_psObjects.reset();

    SERIALIZATION_INIT
        SERIALIZATION_LOAD("Objects"  , m_psObjects);

        SERIALIZATION_LOAD("Layer"    , m_lstLayer);
        SERIALIZATION_LOAD("Id"       ,  *m_psIdObject);
        SERIALIZATION_LOAD("ShowPrintArea"     ,  m_bPrintArea);
        SERIALIZATION_LOAD("PaperArea"              ,  m_bPaperArea);
        SERIALIZATION_LOAD("OffsetSizeEqualPaper"   ,  m_bOffsetSizeEqualPaper);
        SERIALIZATION_LOAD("OffsetPageCol"          ,  m_dOffsetPageCol);
        SERIALIZATION_LOAD("OffsetPageRow"          ,  m_dOffsetPageRow);
        SERIALIZATION_LOAD("PrintRow"               ,  m_iPrintRow);
        SERIALIZATION_LOAD("PrintCol"               ,  m_iPrintCol);

        int iImageNum;
        SERIALIZATION_LOAD("ImagesNumber"           ,  iImageNum);
        for (int iCnt = 0; iCnt < iImageNum; iCnt++)
        {
            StdString strName;
            std::stringstream   strmImage;
            strmImage << boost::format("ImageName_%d") % iCnt;
            SERIALIZATION_LOAD(strmImage.str().c_str()    ,  strName);

            CImageData image;
            std::stringstream   strmData;
            strmData << boost::format("ImageData_%d") % iCnt;
            SERIALIZATION_LOAD(strmData.str().c_str()    ,  image);

            std::shared_ptr<CImageData> pShare;
            pShare = std::make_shared<CImageData>(image);
            
            if(pShare->IsUseFile())
            {
                StdPath path(GetCurrentPath());
                path /= strName;
                pShare->LoadFile(path);
            }
            m_mapImage[strName] = pShare;
        }

        SERIALIZATION_LOAD("UseBoundingBox"          ,  m_bUseNodeBounds);
        SERIALIZATION_LOAD("ViewPosition"          ,  m_lstViewPos);
        SERIALIZATION_LOAD("IgnoreSlePart"          , m_bIgnoreSelPart);

        ar >> BOOST_SERIALIZATION_BASE_OBJECT_NVP(CObjectDef);

       m_psObjects->SetPartsDef(this);
       m_psObjects->ResetProperty();
    MOCK_EXCEPTION_FILE(e_file_read);
}

/**
 * @brief   シリアル化(セーブ)
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  なし
 * @note	
 */
template<class Archive>
void CPartsDef::save(Archive& ar, const unsigned int version) const
{
    //セーブ開始を通知
    SERIALIZATION_INIT;
        //SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_VIEW_SAVE_START, 0, 0);
        SERIALIZATION_SAVE("Objects"  ,  m_psObjects);
        SERIALIZATION_SAVE("Layer"    ,  m_lstLayer);
        SERIALIZATION_SAVE("Id"       ,  *m_psIdObject);
        SERIALIZATION_SAVE("ShowPrintArea"     ,  m_bPrintArea);
        SERIALIZATION_SAVE("PaperArea"              ,  m_bPaperArea);
        SERIALIZATION_SAVE("OffsetSizeEqualPaper"   ,  m_bOffsetSizeEqualPaper);
        SERIALIZATION_SAVE("OffsetPageCol"          ,  m_dOffsetPageCol);
        SERIALIZATION_SAVE("OffsetPageRow"          ,  m_dOffsetPageRow);
        SERIALIZATION_SAVE("PrintRow"               ,  m_iPrintRow);
        SERIALIZATION_SAVE("PrintCol"               ,  m_iPrintCol);

        int iNumber = static_cast<int>(m_mapImage.size());
        SERIALIZATION_SAVE("ImagesNumber"           ,  iNumber);

        int iCnt = 0;
        for (auto mapData: m_mapImage)
        {
            std::stringstream   strmImage;
            strmImage << boost::format("ImageName_%d") % iCnt;
            SERIALIZATION_SAVE(strmImage.str().c_str()    ,  mapData.first);

            std::stringstream   strmData;
            strmData << boost::format("ImageData_%d") % iCnt;

            StdPath path;
            
            mapData.second->SetFileName(mapData.first);
            SERIALIZATION_SAVE(strmData.str().c_str()    ,  *(mapData.second.get()));
        }

        SERIALIZATION_SAVE("UseBoundingBox"          ,  m_bUseNodeBounds);
        SERIALIZATION_SAVE("ViewPosition"          ,  m_lstViewPos);
        SERIALIZATION_SAVE("IgnoreSlePart", m_bIgnoreSelPart);


        ar << BOOST_SERIALIZATION_BASE_OBJECT_NVP(CObjectDef);
    MOCK_EXCEPTION_FILE(e_file_write);
}

 /**
 * @brief   保存パス
 * @param   なし
 * @retval  保存パス
 * @note	
 */
/*
 StdString CPartsDef::GetFilePath()
{
    return m_strFilePath;

}
*/

/**
 * @brief   描画オブジェクト取得
 * @param   なし
 * @retval  描画オブジェクト
 * @note	
 */
std::shared_ptr<CDrawingParts>   CPartsDef::GetDrawingParts()
{
    return m_psObjects;
}

std::shared_ptr<CDrawingScriptBase>   CPartsDef::GetViewObject()
{
    return m_psObjects;
}



/**
 * @brief   ドラッグ中オブジェクトリスト取得
 * @param   なし
 * @retval  ドラッグ中の一時オブジェクト
 * @note    ドラッグ用に生成したオブジェクトを一時的に置く場所
 *          直接このリストを用いて描画は行わない。コピーオブジェクトの置き場所として使用
 *          
 */
std::vector< std::shared_ptr<CDrawingObject>>*   CPartsDef::GetConnectingTmpObjectList()
{
    return &m_lstConnecting;
}

std::vector< std::shared_ptr<CDrawingObject>>*   CPartsDef::GetTmpGroupObjectList()
{
    return &m_lstTmpGroup;
}


/**
 * @brief   文字列取得
 * @param   なし
 * @retval  文字列
 * @note	デバッグ用
 */
StdString  CPartsDef::GetStr()
{
    StdString strRet;
    if (m_psObjects)
    {
        strRet = m_psObjects->GetStr();
    }
    return strRet;
}

/**
 * @brief   ID最大値設定
 * @param   [in] iMax ID最大値
 * @retval  なし
 * @note	デバッグ用にIDの最大値を設定する
 */
void CPartsDef::SetMaxId(int iMax)
{
    STD_ASSERT(iMax > 0);

    if (iMax < 1)
    {
        return;
    }

    m_psIdObject.reset();
    m_psIdObject = std::make_shared<CIdObj>(iMax);
}


/**
 * @brief   描画領域取得
 * @param   なし
 * @retval  なし
 * @note	描画領域を取得する
 */
RECT2D CPartsDef::GetViewBounds()  const
{

    if (m_psObjects)
    {
        return m_psObjects->GetViewBounds();
    }
    RECT2D rc;
    return rc;
}


//色による検索
void CPartsDef::SearchObjectByColor(std::vector<CDrawingObject*>* pList, 
                                             COLORREF cr,
                                             const std::vector<CDrawingObject*>* pListOrgin,
                                             bool bIgnoreGroup)
{
        SearchObject( pList,
        true  , cr,        //clolr
        false , 0,     //type
        false , _T(""),   //name,
        false , NULL,   //property
        false , _T(""),   //Tag
        false , RECT2D(), //bArea
        pListOrgin,
        bIgnoreGroup);
}
//種別による検索
void CPartsDef::SearchObjectByType(std::vector<CDrawingObject*>* pList, 
                        const std::set<DRAWING_TYPE>*  pTypeSet,
                        const std::vector<CDrawingObject*>* pListOrgin,
                        bool bIgnoreGroup)
{
        SearchObject( pList,
        false , 0,        //clolr
        true  , pTypeSet,     //type
        false , _T(""),   //name,
        false  ,NULL,   //property
        false , _T(""),   //Tag
        false , RECT2D(), //bArea
        pListOrgin,
        bIgnoreGroup);


}


//種別による検索
void CPartsDef::SearchObjectByType(std::vector<CDrawingObject*>* pList, 
                        DRAWING_TYPE type,
                        const std::vector<CDrawingObject*>* pListOrgin,
                        bool bIgnoreGroup)
{

        std::set<DRAWING_TYPE>  setType;
        setType.insert(type);

        SearchObject( pList,
        false , 0,        //clolr
        true  , &setType, //type
        false , _T(""),   //name,
        false  ,NULL,   //property
        false , _T(""),   //Tag
        false , RECT2D(), //bArea
        pListOrgin,
        bIgnoreGroup);
}

//名前による検索
void CPartsDef::SearchObjectByNmae(std::vector<CDrawingObject*>* pList,
                        StdString strNamer,
                        const std::vector<CDrawingObject*>* pListOrgin,
                        bool bIgnoreGroup)
{
        SearchObject( pList,
        false , 0,        //clolr
        false , 0,     //type
        true  , strNamer,   //name,
        false , NULL,   //property
        false , _T(""),   //Tag
        false , RECT2D(), //bArea
        pListOrgin,
        bIgnoreGroup);
}

//プロパティセットによる検索
void CPartsDef::SearchObjectByPropertyset(std::vector<CDrawingObject*>* pList, 
                        const std::vector<StdString>* pListProperty,
                        const std::vector<CDrawingObject*>* pListOrgin,
                        bool bIgnoreGroup)
{
        SearchObject( pList,
        false , 0,        //clolr
        false , NULL,     //type
        false , _T(""),   //name,
        true  ,  pListProperty,//property
        false , _T(""),   //Tag
        false , RECT2D(), //bArea
        pListOrgin,
        bIgnoreGroup);
}

//タグによる検索
void CPartsDef::SearchObjectByTag(std::vector<CDrawingObject*>* pList, 
                        StdString strTag,
                        const std::vector<CDrawingObject*>* pListOrgin,
                        bool bIgnoreGroup)
{
        SearchObject( pList,
        false , 0,        //clolr
        false , 0,     //type
        false , _T(""),   //name,
        false , NULL,   //property
        true  , strTag,   //Tag
        false , RECT2D(), //bArea
        pListOrgin,
        bIgnoreGroup);

}

//領域による検索
void CPartsDef::SearchObjectByArea(std::vector<CDrawingObject*>* pList
                        , RECT2D rc,
                          const std::vector<CDrawingObject*>* pListOrgin,
                        bool bIgnoreGroup)
{
        SearchObject( pList,
        false , 0,        //clolr
        false , 0,     //type
        false , _T(""),   //name,
        false , NULL,   //property
        false , _T(""),   //Tag
        true , rc, //bArea
        pListOrgin,
        bIgnoreGroup);
}


//プロパティセットによる検索(距離でソート)
void CPartsDef::SearchPropertysetWithSortDistance(std::vector<StdString>* pList,
                          POINT2D pt,
                          const std::vector<StdString>* pListProperty,
                          bool bNode)


{
    //ノードと同じプロパティセット（プロパティグループ)
    //のオブジェクトをリストアップ
    std::multimap<double, CDrawingObject*> mapDistanceObj;
    std::pair<double, CDrawingObject*> pairDistanceObj;
    std::vector<CDrawingObject*> lstSearch;
                            
    SearchObject( &lstSearch,
    false , 0,        //clolr
    bNode , NULL,  //type
    false , _T(""),   //name,
    true  ,  pListProperty,//property
    false , _T(""),   //Tag
    false , RECT2D(), //bArea
    NULL);

    double dDistance;
    foreach(CDrawingObject* pObj, lstSearch)
    {
		if (pObj)
		{
			dDistance = pObj->Distance(pt);
			pairDistanceObj.first = dDistance;
			pairDistanceObj.second = pObj;
            mapDistanceObj.insert(pairDistanceObj);
        }
    }

    CDrawingObject* pObj;
    for(auto ite = mapDistanceObj.begin();
        ite != mapDistanceObj.end();
        ite++)
    {
        pObj = ite->second;
            
        if (!pObj->GetName().empty())
        {
            pList->push_back(pObj->GetName());
        }
    }
}


void CPartsDef::_SearchNodeGroup(std::vector<CDrawingObject*>* pList,
                            CDrawingParts* pGroup,
                            bool bColor, COLORREF cr,
                            bool bName,  StdString strName,
                            bool bProp,  const std::vector<StdString>* pListProperty,
                            //bool bPropGroup,  StdString strPropertyGroup,
                            bool bTag,   const std::vector<StdString>*  pTagSet,
                            bool bArea,  RECT2D rc)
{

    bool bFind;
    std::set<DRAWING_TYPE> setType;
    setType.insert(DT_NODE);
    
    if(!pGroup)
    {
        pGroup = this->m_psObjects.get();
    }

    auto pObjList = pGroup->GetList();
    for(auto pObjSecond: *pObjList)
    {
        bFind = _CheckCondition(pObjSecond.get(),
                        bColor, cr,
                        true,  &setType,
                        bName,  strName,
                        bProp,  pListProperty,
                        //bPropGroup, strPropertyGroup,
                        bTag,   pTagSet,
                        bArea,  rc);
        if (bFind)
        {
            pList->push_back(pObjSecond.get());
        }
        else
        {
            if ((pObjSecond->GetType() == DT_PARTS)||
                (pObjSecond->GetType() == DT_REFERENCE)||
                (pObjSecond->GetType() == DT_FIELD))
            {
                auto pGroupSecond = std::dynamic_pointer_cast<CDrawingParts>(pObjSecond);
                _SearchNodeGroup(pList,
                    pGroupSecond.get(),
                    bColor, cr,
                    bName,  strName,
                    bProp,  pListProperty,
                    //bPropGroup, strPropertyGroup,
                    bTag,   pTagSet,
                    bArea,  rc);
            }
        }
    }
}

void CPartsDef::SearchObject(std::vector<CDrawingObject*>* pList,
                            bool bColor, COLORREF cr,
                            bool bType,  const std::set<DRAWING_TYPE>*  pTypeSet,
                            bool bName,  StdString strName,
                            bool bProp,  const std::vector<StdString>* pListProperty,
                            bool bTag,   StdString strTag,
                            bool bArea,  RECT2D rc,
                            const std::vector<CDrawingObject*>* pListOrgin,
                            bool bIgnoreGroup)
{
    std::vector<StdString> lstTag;
    StdString strPropertyGroup;
    bool  bPropGroup = false;

    if(bTag)
    {
        CUtil::TokenizeCsv(&lstTag, strTag,  _T(','));
    }

  
    bool bFind;
    if (pListOrgin)
    {
        foreach(CDrawingObject* pObj, *pListOrgin)
        {
            bFind = _CheckCondition(pObj,
                            bColor, cr,
                            bType,  pTypeSet,
                            bName,  strName,
                            bProp,  pListProperty,
                            //bPropGroup, strPropertyGroup,
                            bTag,   &lstTag,
                            bArea,  rc);
            if (bFind)
            {
                pList->push_back(pObj);
            }
            else
            {
                if (bIgnoreGroup)
                {
                    if ((pObj->GetType() == DT_PARTS) ||
                        (pObj->GetType() == DT_REFERENCE) ||
                        (pObj->GetType() == DT_FIELD))
                    {
                        CDrawingParts* pGroup;
                        pGroup = dynamic_cast<CDrawingParts*>(pObj);
                        _SearchNodeGroup(pList,
                            pGroup,
                            bColor, cr,
                            bName,  strName,
                            bProp,  pListProperty,
                            //bPropGroup, strPropertyGroup,
                            bTag,   &lstTag,
                            bArea,  rc);
                    }
                }
            }
        }
    }
    else
    {

        auto pObjctList = m_psObjects->GetList();

        for(auto pObj: *pObjctList)
        {
            bFind = _CheckCondition(pObj.get(),
                            bColor, cr,
                            bType,  pTypeSet,
                            bName,  strName,
                            bProp,  pListProperty,
                            //bPropGroup, strPropertyGroup,
                            bTag,   &lstTag,
                            bArea,  rc);
            if (bFind)
            {
                pList->push_back(pObj.get());
            }
            else
            {
                if (bIgnoreGroup)
                {
                    if ((pObj->GetType() == DT_PARTS)||
                        (pObj->GetType() == DT_REFERENCE)||
                        (pObj->GetType() == DT_FIELD))
                    {
                        auto pGroup = std::dynamic_pointer_cast<CDrawingParts>(pObj);
                        _SearchNodeGroup(pList,
                            pGroup.get(),
                            bColor, cr,
                            bName,  strName,
                            bProp,  pListProperty,
                            //bPropGroup, strPropertyGroup,
                            bTag,   &lstTag,
                            bArea,  rc);
                    }
                }
            }
        }
    }
}


bool CPartsDef::_CheckCondition(const CDrawingObject* pObj,
                            bool bColor, COLORREF cr,
                            bool bType,  const std::set<DRAWING_TYPE>*  pTypeSet,
                            bool bName,  StdString strName,
                            bool bProp,  const std::vector<StdString>* lstPropertySet,
                            //bool bPropGroup,  StdString strPropertyGroup,
                            bool bTag,   const std::vector<StdString>*  pTagSet,
                            bool bArea,  RECT2D rc)
{

    if (!pObj)
    {
        return false;
    }

    if (bColor)
    {
        if (pObj->GetColor() != cr)
        {
            return false;
        }
    }
    
    if (bType)
    {
        bool bFind = false;
        foreach(const DRAWING_TYPE type, *pTypeSet)
        {
            if (pObj->GetType() == type)
            {
                bFind = true;
                break;
            }
        }

        if (!bFind)
        {
            return false;
        }
    }

    if (bName)
    {
        if (pObj->GetName() != strName)
        {
            return false;
        }
    }


    if (bProp)
    {
        bool bRet = false;
        StdString propObj = pObj->GetPropertySetName();

        auto iter = boost::find_if(*lstPropertySet, [=](StdString prop)
        {
            return (prop == propObj);
        });

        if (iter == lstPropertySet->end())
        {
            return false;
        }
    }

    /*
    if (bPropGroup)
    {
        std::vector<StdString> lstName;
        CUtil::TokenizeCsv(&lstName, pObj->GetPropertySetName(),  _T('.'));
        if (lstName.size() != 2)
        {
            return false;
        }

        if (lstName[0] != strPropertyGroup)
        {
            return false;
        }
    }
    */

    if (bTag)
    {
        std::vector<StdString> lstTag;
        pObj->GetTagList(&lstTag);

        bool bFind = false;
        foreach(StdString strTag, lstTag)
        {
            foreach(StdString strTagSrc, *pTagSet)
            {
                if (strTagSrc == strTag)
                {
                    bFind = true;
                    break;
                }

            }
            if (bFind)
            {
                break;
            }
        }

        if (!bFind)
        {
            return false;
        }
    }

    if (bArea)
    {
        if (!pObj->IsInner( rc, DRAW_CONFIG->IsSelPart()))
        {
            return false;
        }
    }
    return true;
}

bool CPartsDef::IsImage(LPCTSTR strName)
{
     //TODO:IMAGE実装

    StdPath path(strName);

    if (path.empty())
    {
        return false;
    }

    StdString strFile;
    strFile = path.filename().c_str();

    std::map< StdString, std::shared_ptr<CImageData> >::iterator iteFind;
    iteFind = m_mapImage.find(strFile);
    if (iteFind != m_mapImage.end())
    {
        if (iteFind->second)
        {
            return true;
        }
    }
    return false;
}

bool CPartsDef::LoadImageDlg( StdString* pFileNeme)
{
    long lSize = SelectNum();

    if (lSize != 0)
    {
        //選択を解除
        RelSelect();
    }   

    static TCHAR BASED_CODE szFilter[] = 
       _T("Bitmap Files (*.bmp)|*.bmp|")
       _T("JPEG Files (*.jpg)|*.jpg|")
       _T("PNG Files (*.png)|*.png|")
       _T("All Files (*.*)|*.*||");


    CFileDialog dlgFile(
        TRUE,    //  TRUE:[ファイルを開く] FALSE:[ファイル名を付けて保存]
        NULL,   //  規定のファイル拡張子
        NULL,   //  初期ファイル名
        OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, 
        szFilter,          //  フィルターを指定する一連の文字列のペア
        NULL,              //  ファイル ダイアログ ボックスの親ウィンドウまたはオーナー ウィンドウへのポインター
        0, FALSE);


    CString strFilter;
    CString strDefault;

    CString strPath = GetCurrentPath().c_str();

    dlgFile.m_ofn.lpstrTitle = _T("画像ファイルの選択");
    //dlgFile.m_ofn.lpstrFile = strPath.GetBuffer(_MAX_PATH);
    dlgFile.m_ofn.lpstrInitialDir = strPath.GetBuffer(_MAX_PATH);

    CWaitCursor wait;
    // The dialog box will normally change the cursor to
    // the standard arrow cursor.
    INT_PTR nResult = dlgFile.DoModal();

    if (nResult != IDOK)
    {
        return false;
    }
    
    StdPath pathSrc = dlgFile.GetPathName(); 
    StdString strFineName = pathSrc.filename().c_str();
    namespace fs = boost::filesystem;

    if (strPath != pathSrc.parent_path().c_str())
    {
        //違うディレクトリのファイルはコピーする
        StdPath pathDst = GetCurrentPath();
        pathDst /= strFineName;


        if (!fs::exists(pathDst))
        {
            //ファイルをコピー
            try
            {
                fs::copy_file(pathSrc, pathDst);
            }
            catch (fs::filesystem_error& ex)
            {
                StdString strError;
                strError = MockException::FileErrorMessage(false, pathDst,
                    CUtil::CharToString(ex.what()));

                STD_DBG(strError.c_str());
                AfxMessageBox(GET_ERR_STR(e_file_system));
                return false;
            }
        }
    }

    *pFileNeme = strFineName;
    bool bRet;
    bRet = LoadImage(strFineName.c_str());

    return bRet;
}
    

bool CPartsDef::GetImageList(std::vector<StdString>* pList)
{
    pList->clear();

    for(auto dat = m_mapImage.begin();
        dat != m_mapImage.end();
        dat++)
    {
        pList->push_back(dat->first);
    }
    return true;
}


std::weak_ptr<CImageData> CPartsDef::GetImage(LPCTSTR strName)
{
    std::weak_ptr<CImageData> pRet;
     //TODO:IMAGE実装

    StdPath path(strName);
    if (path.empty())
    {
        return pRet;
    }

    StdString strFile;
    strFile = path.filename().c_str();

    std::map< StdString, std::shared_ptr<CImageData> >::iterator iteFind;
    iteFind = m_mapImage.find(strFile);

    if (iteFind != m_mapImage.end())
    {
        pRet = iteFind->second;
    }
    return pRet;
}

/**
 * @brief   イメージ読み込み
 * @param   [in] pView    表示View
 * @param   [in] flleName ファイル名(パスは含まない)
 * @retval  true 成功
 * @note	
 */
bool CPartsDef::LoadImage(LPCTSTR strName)
{
    StdPath path(strName);
    StdString strFile;
    strFile = path.filename().c_str();

    std::map< StdString, std::shared_ptr<CImageData> >::iterator iteFind;
    iteFind = m_mapImage.find(strFile);

    if (iteFind != m_mapImage.end())
    {
        //すでに読み込み済み
        return true;
    }

    auto pImage = std::make_shared<CImageData>();
    if (!pImage->LoadFile(path))
    {
        return false;
    }
    pImage->SetFileName(strName);
    m_mapImage[strFile] = pImage;
    return true;
}

/**
 * @brief   イメージ削除
 * @param   [in] pView    表示View
 * @param   [in] flleName ファイル名(パスは含まない)
 * @retval  true 成功
 * @note	
 */
bool CPartsDef::DeleteImage(LPCTSTR strName)
{
    StdPath path(strName);
    StdString strFile;
    strFile = path.filename().c_str();

    std::map< StdString, std::shared_ptr<CImageData> >::iterator iteFind;
    iteFind = m_mapImage.find(strFile);

    if (iteFind == m_mapImage.end())
    {
        return false;
    }
    m_mapImage.erase(iteFind);
    return true;
}

/**
 * @brief   デバッグ用文字出力
 * @param   なし
 * @retval  なし
 * @note	
 */
void CPartsDef::Print() const
{
    DB_PRINT(_T("CPartsDef %I64x \n"), this);
    CObjectDef::Print();
    DB_PRINT(_T("Objects %I64x \n"), m_psObjects);

	_Print(0);
}


//load save のインスタンスが生成されないため
//ダミーとして実装
void CPartsDef::Dummy()
{
    unsigned int iVersion = 0;

    StdStreamOut outFs(_T(""));
    StdStreamIn  inFs(_T(""));
    //boost::filesystem::ofstream outTxtFs(_T(""));
    //boost::filesystem::ifstream inTxtFs(_T(""));

    boost::archive::text_woarchive txtOut(outFs); 
    boost::archive::text_wiarchive txtIn(inFs);

    save<boost::archive::text_woarchive>( txtOut, iVersion);
    load<boost::archive::text_wiarchive>( txtIn,  iVersion);
}

/**
 *  @brief   プロパティ初期化.
 *  @param   [in] pCtrl オブジェクトコントロールへのポインタ
 *  @retval     なし
 *  @note
 */
TREE_GRID_ITEM*  CPartsDef::_CreateStdPropertyTree()
{
    NAME_TREE_ITEM<CStdPropertyItem>* pBeforeGroup;
    NAME_TREE_ITEM<CStdPropertyItem>* pNextItem;
    NAME_TREE_ITEM<CStdPropertyItem>* pGroupItem;
    NAME_TREE<CStdPropertyItem>* pTree;
    //int iId;
    CStdPropertyItem* pItem;
    boost::tuple< CStdPropertyItem*, void*> pairVal;


    pBeforeGroup = CObjectDef::_CreateStdPropertyTree();
    pTree = m_psProperty->GetTreeInstance();

    pGroupItem  = pTree->AddNext(pBeforeGroup, NULL, GET_STR(STR_PRO_PARTS_DEF), _T("Parts"));

    //---------------
    // 画像設定
    //---------------
    CDialog* pDlg =  m_pImageDlg.get();

    CStdPropertyItemDef DefImage(PROP_SETTING,    //表示タイプ
        GET_STR(STR_PRO_IMAGE_SET)   ,            //表示名
        _T("Images"),                             //変数名
        GET_STR(STR_PRO_INFO_IMAGE_SET),          //表示説明
        false,                                    //編集可不可
        DISP_NONE,                                //表示単位
        m_strImageList,                           //初期値
        pDlg);

    pItem = new CStdPropertyItem( DefImage, 
        PropDummy, 
        m_psProperty, 
        NULL, 
        (void*)&m_strImageList);

    pNextItem = pTree->AddChild(pGroupItem, 
                                pItem, 
                                DefImage.strDspName, 
                                pItem->GetName());


    //---------------------
    // NodeBounds使用有無
    //---------------------
    CStdPropertyItemDef DefUseNodeBounds(PROP_BOOL, 
        GET_STR(STR_PRO_USE_NODE_BOUNDS)   , 
        _T("UseNodeBounds"),
        GET_STR(STR_PRO_INFO_USE_NODE_BOUNDS), 
        true,
        DISP_NONE,
        true);

    pItem = new CStdPropertyItem( DefUseNodeBounds, PropUseNodeBounds, m_psProperty, NULL, (void*)&m_bUseNodeBounds);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefUseNodeBounds.strDspName, pItem->GetName());


    //---------------------
    // 範囲選択設定無視 
    //---------------------
    CStdPropertyItemDef DefIgnoreSelPart(PROP_BOOL, //表示タイプ
        GET_STR(STR_PRO_IGNORE_SEL_PART),           //表示名
        _T("IgnoreSelPart"),                        //変数名
        GET_STR(STR_PRO_INFO_IGNORE_SEL_PART),      //表示説明
        true,                                       //編集可不可
        DISP_NONE,                                  //表示単位
        false);                                     //初期値

    pItem = new CStdPropertyItem(DefIgnoreSelPart, PropIgnoreSelPart, m_psProperty, NULL, (void*)&m_bIgnoreSelPart);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefIgnoreSelPart.strDspName, pItem->GetName());



    //----------------------------------
    NAME_TREE_ITEM<CStdPropertyItem>* pGroupPrint;
    pGroupPrint  = pTree->AddNext(pGroupItem, NULL, GET_STR(STR_PRO_PRINT), _T("Print"));

    //---------------
    // 印刷範囲表示
    //---------------
    CStdPropertyItemDef DefShowPrintArea(PROP_BOOL, 
        GET_STR(STR_PRO_SHOW_PRINT_AREA)   , 
        _T("PreintArea"),
        GET_STR(STR_PRO_INFO_SHOW_PRINT_AREA), 
        true,
        DISP_NONE,
        false);

    pItem = new CStdPropertyItem( DefShowPrintArea, PropPrintArea, m_psProperty, NULL, (void*)&m_bPrintArea);
    //pNextItem = pTree->AddNext(pGroupPrint, pItem, DefShowPrintArea.strDspName, pItem->GetName());
    pNextItem = pTree->AddChild(pGroupPrint, pItem, DefShowPrintArea.strDspName, pItem->GetName());

    //---------------
    // 用紙枠表示
    //---------------
    CStdPropertyItemDef DefShowPaperArea(PROP_BOOL, 
        GET_STR(STR_PRO_DISP_PAPER)   , 
        _T("PaperArea"),
        GET_STR(STR_PRO_INFO_DISP_PAPER), 
        true,
        DISP_NONE,
        m_bPaperArea);

    pItem = new CStdPropertyItem( DefShowPaperArea, PropPaperArea, m_psProperty, NULL, (void*)&m_bPaperArea);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefShowPaperArea.strDspName, pItem->GetName());

    //---------------
    // 印刷行
    //---------------
    CStdPropertyItemDef DefShowPrintRow(PROP_INT_RANGE, 
        GET_STR(STR_PRO_PRINT_ROW)   , 
        _T("PreintRow"),
        GET_STR(STR_PRO_INFO_PRINT_ROW), 
        true,
        DISP_NONE,
        m_iPrintRow,
        1, 100);

    pItem = new CStdPropertyItem( DefShowPrintRow, PropPrintRow, m_psProperty, NULL, (void*)&m_iPrintRow);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefShowPrintRow.strDspName, pItem->GetName());

    //---------------
    // 印刷列
    //---------------
    CStdPropertyItemDef DefShowPrintCol(PROP_INT_RANGE, 
        GET_STR(STR_PRO_PRINT_COL)   , 
        _T("PreintCol"),
        GET_STR(STR_PRO_INFO_PRINT_COL), 
        true,
        DISP_NONE,
        m_iPrintCol,
        1, 100);

    pItem = new CStdPropertyItem( DefShowPrintCol, PropPrintCol, m_psProperty, NULL, (void*)&m_iPrintCol);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefShowPrintCol.strDspName, pItem->GetName());

    //---------------
    // 印刷オフセット
    //---------------
    CStdPropertyItemDef DefPrintOffsetEqPaper(PROP_BOOL, 
        GET_STR(STR_PRO_OFFSET_EQ_PAPER)   , 
        _T("PrintOffsetEqPaper"),
        GET_STR(STR_PRO_INFO_OFFSET_EQ_PAPER), 
        true,
        DISP_NONE,
        m_bOffsetSizeEqualPaper);

    pItem = new CStdPropertyItem( DefPrintOffsetEqPaper, 
                                  PropOffsetSizeEqualPaper, 
                                  m_psProperty, NULL, 
                                  (void*)&m_bOffsetSizeEqualPaper);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefPrintOffsetEqPaper.strDspName, pItem->GetName());


    //---------------
    // 印刷オフセット行
    //---------------
    CStdPropertyItemDef DefPrintOffsetRow(PROP_DOUBLE, 
        GET_STR(STR_PRO_PRINT_OFFSET_ROW)   , 
        _T("PrintOffsetRowr"),
        GET_STR(STR_PRO_INFO_PRINT_OFFSET_ROW), 
        true,
        DISP_UNIT,
        m_dOffsetPageRow);

    pItem = new CStdPropertyItem( DefPrintOffsetRow, 
                                  PropPrintOffetRow, 
                                  m_psProperty, NULL, 
                                  (void*)&m_dOffsetPageRow);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefPrintOffsetRow.strDspName, pItem->GetName());

    //---------------
    // 印刷オフセット行
    //---------------
    CStdPropertyItemDef DefPrintOffsetCol(PROP_DOUBLE, 
        GET_STR(STR_PRO_PRINT_OFFSET_COL)   , 
        _T("PrintOffsetCol"),
        GET_STR(STR_PRO_INFO_PRINT_OFFSET_COL), 
        true,
        DISP_UNIT,
        m_dOffsetPageCol);

    pItem = new CStdPropertyItem( DefPrintOffsetCol, 
                                  PropPrintOffetCol, 
                                  m_psProperty, NULL, 
                                  (void*)&m_dOffsetPageCol);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefPrintOffsetCol.strDspName, pItem->GetName());

    //==================
    // 用紙
    //==================
    NAME_TREE_ITEM<CStdPropertyItem>* pGroupPaper;
    pGroupPaper  = pTree->AddNext(pGroupPrint, NULL, GET_STR(STR_PRO_PAPER), _T("Paper"));

    m_dPaperWidth  = DRAW_CONFIG->pDispUnit->ToThisUnit(THIS_APP->GetPaperWidth());
    m_dPaperHeight = DRAW_CONFIG->pDispUnit->ToThisUnit(THIS_APP->GetPaperHeight());

    m_dPrintWidth  = DRAW_CONFIG->pDispUnit->ToThisUnit(THIS_APP->GetPrintWidth());
    m_dPrintHeight = DRAW_CONFIG->pDispUnit->ToThisUnit(THIS_APP->GetPrintHeight());

    m_dPrintOffsetX  = DRAW_CONFIG->pDispUnit->ToThisUnit(THIS_APP->GetPrintLeftMargin());
    m_dPrintOffsetY  = DRAW_CONFIG->pDispUnit->ToThisUnit(THIS_APP->GetPrintTopMargin());

    //------------
    // 用紙幅
    //------------
    CStdPropertyItemDef DefPaperWidth(PROP_DOUBLE, 
        GET_STR(STR_PRO_PAPER_WIDRH)   , 
        _T("PaperWidth"),
        GET_STR(STR_PRO_INFO_PAPER_WIDRH), 
        false,
        DISP_UNIT,
        m_dPaperWidth);

    pItem = new CStdPropertyItem( DefPaperWidth, 
                                  PropDummy, 
                                  m_psProperty, NULL, 
                                  (void*)&m_dPaperWidth);
    pNextItem = pTree->AddChild(pGroupPaper, pItem, DefPaperWidth.strDspName, pItem->GetName());

    //------------
    // 用紙高さ
    //------------
    CStdPropertyItemDef DefPaperHeight(PROP_DOUBLE,    
        GET_STR(STR_PRO_PAPER_HEIGHT)   ,              
        _T("PaperHeight"),                             
        GET_STR(STR_PRO_INFO_PAPER_HEIGHT),            
        false,                                         
        DISP_UNIT,                                     
        m_dPaperHeight);                               

    pItem = new CStdPropertyItem( DefPaperHeight, 
                                  PropDummy, 
                                  m_psProperty, NULL, 
                                  (void*)&m_dPaperHeight);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefPaperHeight.strDspName, pItem->GetName());



    //------------
    // 印字幅
    //------------
    CStdPropertyItemDef DefPrintWidth(PROP_DOUBLE, 
        GET_STR(STR_PRO_PRINT_WIDTH)   , 
        _T("PrintWidth"),
        GET_STR(STR_PRO_INFO_PRINT_WIDTH), 
        false,
        DISP_UNIT,
        m_dPrintWidth);

    pItem = new CStdPropertyItem( DefPrintWidth, 
                                  PropDummy, 
                                  m_psProperty, NULL, 
                                  (void*)&m_dPrintWidth);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefPrintWidth.strDspName, pItem->GetName());

    //------------
    // 印字高さ
    //------------
    CStdPropertyItemDef DefPrintHeight(PROP_DOUBLE, 
        GET_STR(STR_PRO_PRINT_HEIGHT)   , 
        _T("PrintHeight"),
        GET_STR(STR_PRO_INFO_PRINT_HEIGHT), 
        false,
        DISP_UNIT,
        m_dPrintHeight);

    pItem = new CStdPropertyItem( DefPrintHeight, 
                                  PropDummy, 
                                  m_psProperty, NULL, 
                                  (void*)&m_dPrintHeight);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefPrintHeight.strDspName, pItem->GetName());


    //------------
    // 印字 オフセットTop
    //------------
    CStdPropertyItemDef DefPrintOffsetY(PROP_DOUBLE, 
        GET_STR(STR_PRO_MARGIN_TOP)   , 
        _T("PrintWidth"),
        GET_STR(STR_PRO_INFO_MARGIN_TOP), 
        false,
        DISP_UNIT,
        m_dPrintOffsetY);

    pItem = new CStdPropertyItem( DefPrintOffsetY, 
                                  PropDummy, 
                                  m_psProperty, NULL, 
                                  (void*)&m_dPrintOffsetY);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefPrintOffsetY.strDspName, pItem->GetName());


    //------------
    // 印字 オフセットTop
    //------------
    CStdPropertyItemDef DefPrintOffsetX(PROP_DOUBLE, 
        GET_STR(STR_PRO_MARGIN_LEFT)   , 
        _T("PrintWidth"),
        GET_STR(STR_PRO_INFO_MARGIN_LEFT), 
        false,
        DISP_UNIT,
        m_dPrintOffsetX);

    pItem = new CStdPropertyItem( DefPrintOffsetX, 
                                  PropDummy, 
                                  m_psProperty, NULL, 
                                  (void*)&m_dPrintOffsetX);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefPrintOffsetX.strDspName, pItem->GetName());


    return pGroupPaper;
}

/*
 *  @brief   プロパティ変更(ダミー)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CPartsDef::PropDummy (CStdPropertyItem* pData, void* pObj)
{
   using namespace VIEW_COMMON;
   CPartsDef* pParts = reinterpret_cast<CPartsDef*>(pObj);
   return true;
}


/*
 *  @brief   プロパティ変更(印刷範囲表示)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CPartsDef::PropPrintArea (CStdPropertyItem* pData, void* pObj)
{
   using namespace VIEW_COMMON;
    CPartsDef* pParts = reinterpret_cast<CPartsDef*>(pObj);
    try
    {
        pParts->m_psObjects->GetChgGroupCnt();
        bool bShow = pData->anyData.GetBool();
        pParts->SetShowPrintArea(bShow);
        pParts->Redraw(NULL, false);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}


/*
 *  @brief   プロパティ変更(印刷行)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CPartsDef::PropPrintRow (CStdPropertyItem* pData, void* pObj)
{
   using namespace VIEW_COMMON;
    CPartsDef* pParts = reinterpret_cast<CPartsDef*>(pObj);
    try
    {
        pParts->m_psObjects->GetChgGroupCnt();
        int iRow = pData->anyData.GetInt();
        if (iRow < 1){return false;}
        pParts->SetPrintRow(iRow);
        pParts->Redraw(NULL, false);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/*
 *  @brief   プロパティ変更(印刷行)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CPartsDef::PropPrintCol (CStdPropertyItem* pData, void* pObj)
{
   using namespace VIEW_COMMON;
    CPartsDef* pParts = reinterpret_cast<CPartsDef*>(pObj);
    try
    {
        pParts->m_psObjects->GetChgGroupCnt();
        int iCol = pData->anyData.GetInt();
        if (iCol < 1){return false;}
        pParts->SetPrintCol(iCol);
        pParts->Redraw(NULL, false);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/*
 *  @brief   プロパティ変更(用紙枠を表示する)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CPartsDef::PropPaperArea  (CStdPropertyItem* pData, void* pObj)
{
   using namespace VIEW_COMMON;
    CPartsDef* pParts = reinterpret_cast<CPartsDef*>(pObj);
    try
    {
        pParts->m_psObjects->GetChgGroupCnt();
        bool bPaperArea = pData->anyData.GetBool();
        pParts->SetShowPaperArea(bPaperArea);
        pParts->Redraw(NULL, false);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/*
 *  @brief   プロパティ変更(オフセットを用紙サイズに合わせる)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CPartsDef::PropOffsetSizeEqualPaper    (CStdPropertyItem* pData, void* pObj)
{
   using namespace VIEW_COMMON;
    CPartsDef* pParts = reinterpret_cast<CPartsDef*>(pObj);
    try
    {
        pParts->m_psObjects->GetChgGroupCnt();
        bool bOffsetPaper = pData->anyData.GetBool();
        pParts->SetOffsetSizeEqualPaper(bOffsetPaper);
        if (bOffsetPaper)
        {
            pParts->SetOffsetPageRow( DRAW_CONFIG->pDispUnit->ToThisUnit(THIS_APP->GetPaperHeight()));
            pParts->SetOffsetPageCol( DRAW_CONFIG->pDispUnit->ToThisUnit(THIS_APP->GetPaperWidth()));
        }
        pParts->Redraw(NULL, false);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/*
 *  @brief   プロパティ変更(行オフセット)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CPartsDef::PropPrintOffetRow (CStdPropertyItem* pData, void* pObj)
{
   using namespace VIEW_COMMON;
    CPartsDef* pParts = reinterpret_cast<CPartsDef*>(pObj);
    try
    {
        pParts->m_psObjects->GetChgGroupCnt();
        double dOffset = pData->anyData.GetDouble();
        pParts->SetOffsetPageRow(dOffset);
        pParts->Redraw(NULL, false);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/*
 *  @brief   プロパティ変更(列オフセット)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CPartsDef::PropPrintOffetCol  (CStdPropertyItem* pData, void* pObj)
{
   using namespace VIEW_COMMON;
    CPartsDef* pParts = reinterpret_cast<CPartsDef*>(pObj);
    try
    {
        pParts->m_psObjects->GetChgGroupCnt();
        double dOffset = pData->anyData.GetDouble();
        pParts->SetOffsetPageCol(dOffset);
        pParts->Redraw(NULL, false);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}


/*
 *  @brief   プロパティ変更(列オフセット)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CPartsDef::PropUseNodeBounds  (CStdPropertyItem* pData, void* pObj)
{
   using namespace VIEW_COMMON;
    CPartsDef* pParts = reinterpret_cast<CPartsDef*>(pObj);
    try
    {
        pParts->m_psObjects->GetChgGroupCnt();
        bool bUse = pData->anyData.GetBool();
        pParts->SetUseNodeBounds(bUse);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}


/*
 *  @brief   プロパティ変更(列オフセット)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CPartsDef::PropIgnoreSelPart(CStdPropertyItem* pData, void* pObj)
{
    using namespace VIEW_COMMON;
    CPartsDef* pParts = reinterpret_cast<CPartsDef*>(pObj);
    try
    {
        pParts->m_psObjects->GetChgGroupCnt();
        bool bIgnore = pData->anyData.GetBool();
        pParts->SetIgnoreSelPart(bIgnore);
    }
    catch (...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}


//!< 分解
bool CPartsDef::Disassemble(std::shared_ptr<CDrawingParts> pParts, bool bSetUndo)
{

    bool bRet;
    bRet = pParts->Disassemble(this, bSetUndo);

    if(bSetUndo)
    {
        CUndoAction* pUndo  = GetUndoAction();
        pUndo->Add(UD_DEL, this, pParts, NULL, true);
    }

    DeleteDrawingObject(pParts.get());


    return bRet;

}

void CPartsDef::AddGroupChg()
{
    m_psObjects->AddGroupChg();
}

//描画順序
void CPartsDef::ChangeObjectOrder(VIEW_COMMON::E_DISP_ORDER order, int iId)
{
    m_psObjects->ChangeDispOrder(order, iId);
    m_psObjects->AddGroupChg();
    Redraw(NULL, false);
}


//描画位置
CPartsDef::VIEW_POS CPartsDef::GetViewPosition(int iIndex)
{
    return m_lstViewPos[iIndex];
}

void CPartsDef::SetViewPosition(int iIndex, CPartsDef::VIEW_POS& pos)
{
   m_lstViewPos[iIndex] = pos;
}


bool CPartsDef::_UpdateConnectionOne(
    CUndoAction* pUndo,
    std::map<int, int>* pMapConvId,
    std::shared_ptr<CDrawingObject> pC,
    bool bKeepUnfindId)
{
    bool bChange = false;

    int iTotal = pC->GetNodeDataNum();
    CNodeData* pConnection = NULL;
    std::vector<int> lstDel;
    for (int iNo = 0; iNo < iTotal; ++iNo)
    {
        pConnection = pC->GetNodeData(iNo);
        if (!pConnection)
        {
            continue;
        }

        auto bind = pConnection->lstSocket.begin();
        while (bind != pConnection->lstSocket.end())
        {
            auto ite = pMapConvId->find(bind->iId);
            if (ite != pMapConvId->end())
            {
                bChange = true;
                bind->iId = ite->second;
                bind++;
            }
            else
            {
                if (bKeepUnfindId)
                {
                    bind++;
                }
                else
                {
                    bChange = true;
                    bind = pConnection->lstSocket.erase(bind);
                }
            }
        }

        bind = pConnection->lstPlug.begin();
        while (bind != pConnection->lstPlug.end())
        {
            auto ite = pMapConvId->find(bind->iId);
            if (ite != pMapConvId->end())
            {
                bChange = true;
                bind->iId = ite->second;
                bind++;
            }
            else
            {
                if (bKeepUnfindId)
                {
                    bind++;
                }
                else
                {
                    bChange = true;
                    bind = pConnection->lstPlug.erase(bind);
                }
            }
        }


        if (pConnection)
        {
            if (pConnection->lstPlug.empty())
            {
                pConnection->lstPlug.push_back(CBindingPartner());
            }
        }
    }

    if (pC->GetType() == DT_CONNECTION_LINE)
    {
        //接続先名称を更新
        pC->UpdateNodeSocketData(NULL, this);
    }

    return bChange;
}

bool CPartsDef::UpdateConnection(
    std::map<int, int>* pMapConvId,
    std::vector<std::shared_ptr<CDrawingObject>>* pLstConnection,
    bool bKeepUnfindId)
{
    //------------------
    //  接続ID更新
    //-----------------
    for (auto pC : *pLstConnection)
    {
        _UpdateConnectionOne(NULL, pMapConvId, pC, bKeepUnfindId);
    }


    return true;
}

std::vector<NODE_CHANGE_DATA>* CPartsDef::GetListChangeByConnection()
{
    return &m_lstChangeByConnection; 
}

bool CPartsDef::AddListChangeByConnection(NODE_CHANGE_DATA& data)
{

    if (data.bPlug)
    {
        for (auto ite = m_lstChangeByConnection.begin();
            ite != m_lstChangeByConnection.end();
            ++ite)
        {
            if ((ite->iChangeObjectId == data.iChangeObjectId) &&
                (ite->iChangeNodeIndex == data.iChangeNodeIndex))
            {
                ite->iSrcId = data.iSrcId;
                ite->iSrcIndex = data.iSrcIndex;
                ite->pt = data.pt;
                return true;
            }
        }
    }
    m_lstChangeByConnection.push_back(data);
    return true;
}

bool CPartsDef::UpdateConnectionAndPosition
    (CUndoAction* pUndo,
    std::map<int, int>* pMapConvId,
    std::vector<int>* pLstConnection,
    bool bKeepUnfindId)
{

    for (auto id : *pLstConnection)
    {
        auto pC = GetObjectById(id);
        _UpdateConnectionOne(pUndo, pMapConvId, pC, bKeepUnfindId);
    }
    return true;
}

void CPartsDef::_Print(int iType) const
{
    if (!m_psObjects)
    {
        return;
    }
	auto pList = m_psObjects->GetList();

	DB_PRINT(_T("---m_psObjects---\n"));
	for (auto data : *pList)
	{
		DB_PRINT(_T("  %s\n"), data->Text().c_str());

        if (data->GetType() == DT_PARTS)
        {
            CDrawingParts* pParts = dynamic_cast<CDrawingParts*>(data.get());
            if(pParts)
            {
                DB_PRINT(_T("  %s\n"), pParts->GetStr().c_str());
            }
        }
	}

	if (!m_lstSelect.empty())
	{
		DB_PRINT(_T("---m_lstSelect---\n"));
		for (auto wObj : m_lstSelect)
		{
			auto pObj = wObj.lock();
			DB_PRINT(_T("  %s\n"), pObj->Text().c_str());
		}
	}

	if (!m_lstConnectedSelect.empty())
	{
		DB_PRINT(_T("---m_lstConnectedSelect---\n"));
		for (auto wObj : m_lstConnectedSelect)
		{
			auto pObj = wObj.lock();
			DB_PRINT(_T("  %s\n"), pObj->Text().c_str());
		}
	}

	if (!m_lstMouseOver.empty())
	{
		DB_PRINT(_T("---m_lstMouseOver---\n"));
		for (auto wObj : m_lstMouseOver)
		{
			auto pObj = wObj.lock();
			DB_PRINT(_T("  %s\n"), pObj->Text().c_str());
		}
	}

	if (!m_lstMouseEmphasis.empty())
	{
		DB_PRINT(_T("---m_lstMouseEmphasis---\n"));
		for (auto wObj : m_lstMouseEmphasis)
		{
			auto pObj = wObj.lock();
			DB_PRINT(_T("  %s\n"), pObj->Text().c_str());
		}
	}

	if (!m_lstMouseConnectable.empty())
	{
		DB_PRINT(_T("---m_lstMouseConnectable---\n"));
		for (auto wObj : m_lstMouseConnectable)
		{
			auto pObj = wObj.lock();
			DB_PRINT(_T("  %s\n"), pObj->Text().c_str());
		}
	}

	if (!m_lstDrawingScriptBase.empty())
	{
		DB_PRINT(_T("---m_lstDrawingScriptBase---\n"));
		for (auto wObj : m_lstDrawingScriptBase)
		{
			auto pObj = wObj.lock();
			DB_PRINT(_T("  %s\n"), pObj->Text().c_str());
		}
	}

	if (!m_lstChangeByConnection.empty())
	{
		DB_PRINT(_T("---m_lstChangeByConnection---\n"));
		for (auto data : m_lstChangeByConnection)
		{
			DB_PRINT(_T("  bPlug(%s) Chg(%d:%d) Src(%d:%d) pt(%s)\n"),
				data.bPlug ? _T("T") : _T("F"),
				data.iChangeObjectId, data.iChangeNodeIndex,
				data.iSrcId, data.iSrcIndex,
				data.pt.Str().c_str());
		}
	}

	if (!m_lstConnecting.empty())
	{
		DB_PRINT(_T("---m_lstConnecting---\n"));
		for (auto pObj : m_lstConnecting)
		{
			DB_PRINT(_T("  %s\n"), pObj->Text().c_str());
		}
	}

	if (!m_lstTmpGroup.empty())
	{
		DB_PRINT(_T("---m_lstTmpGroup---\n"));
		for (auto pObj : m_lstTmpGroup)
		{
			DB_PRINT(_T("  %s\n"), pObj->Text().c_str());
		}
	}
}



//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
#include "DrawingObject/CDrawingPoint.h"
#include "DrawingObject/CDrawingLine.h"
#include "DrawingObject/CDrawingParts.h"

//-------------------------------------------------------
void TEST_Ctrl_01(CPartsDef* pDef)
{   
    DB_PRINT(_T("  %s Start\n"), DB_FUNC_NAME.c_str());

    auto pPt_01 = std::make_shared< CDrawingPoint>();
    auto pLn_02 = std::make_shared< CDrawingLine>();
    auto pPt_03 = std::make_shared< CDrawingPoint>();

    POINT2D pt1(0.1111,0.1122);
    POINT2D pt2(0.2211,0.2222);

    pPt_01->SetPoint(0.1, 0.2);
    pLn_02->SetPoint1(pt1);
    pLn_02->SetPoint2(pt2);
    pPt_03->SetPoint(0.1, 0.2);

    pDef->RegisterObject(pPt_01, true, false);
    pDef->RegisterObject(pLn_02, true, false);
    pDef->RegisterObject(pPt_03, true, false);

    pPt_01->SetName(_T("Main_01"));
    pLn_02->SetName(_T("Main_02"));
    pPt_03->SetName(_T("Main_03"));


    auto pObj_01 = pDef->GetObject(_T("Main_01"));
    auto pObj_02 = pDef->GetObject(_T("Main_02"));
    auto pObj_03 = pDef->GetObject(_T("Main_03"));

    STD_ASSERT(pObj_01.get() == pPt_01.get());
    STD_ASSERT(pObj_02.get() == pLn_02.get());
    STD_ASSERT(pObj_03.get() == pPt_03.get());

    DB_PRINT(_T("  %s End\n"), DB_FUNC_NAME.c_str());

}

//-------------------------------------------------------
void TEST_Ctrl_02(CPartsDef* pDef,int iNo)
{   
    DB_PRINT(_T("  %s Start\n"), DB_FUNC_NAME.c_str());



    auto pPt_01 = std::make_shared< CDrawingPoint>();
    auto pLn_02 = std::make_shared< CDrawingLine >();
    auto pPt_03 = std::make_shared< CDrawingPoint>();

    POINT2D pt1(0.1111 * (double)iNo,0.1122 * (double)iNo);
    POINT2D pt2(0.2211 * (double)iNo,0.2222 * (double)iNo);

    pPt_01->SetPoint(0.1 * (double)iNo, 0.2 * (double)iNo);
    pLn_02->SetPoint1(pt1);
    pLn_02->SetPoint2(pt2);
    pPt_03->SetPoint(0.3 * (double)iNo, 0.4 * (double)iNo);

    pDef->RegisterObject(pPt_01, true, false);
    pDef->RegisterObject(pLn_02, true, false);
    pDef->RegisterObject(pPt_03, true, false);

    StdStringStream sParts01;
    StdStringStream sParts02;
    StdStringStream sParts03;

    sParts01 << StdFormat(_T("Parts%02d_01")) % iNo;
    sParts02 << StdFormat(_T("Parts%02d_02")) % iNo;
    sParts03 << StdFormat(_T("Parts%02d_03")) % iNo;

    pPt_01->SetName( sParts01.str().c_str());
    pLn_02->SetName( sParts02.str().c_str());
    pPt_03->SetName( sParts03.str().c_str());

    DB_PRINT(_T("  %s End\n"), DB_FUNC_NAME.c_str());

}

//-------------------------------------------------------
void TEST_CPartsDef_01()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    CProjectCtrl  Proj;
    CProjectCtrl* pProj = &Proj;
    CPartsDef* pMain;

    std::shared_ptr<CObjectDef> pDefMain;
    pDefMain = pProj->CreateParts(_T("main"), VIEW_COMMON::E_PARTS, -1).lock();
    STD_ASSERT(pDefMain);

    pMain = dynamic_cast<CPartsDef*>(pDefMain.get());
    TEST_Ctrl_01(pMain);

    std::shared_ptr<CObjectDef> pDefParts1;
    pDefParts1 = pProj->CreateParts(_T("Parts01"), VIEW_COMMON::E_PARTS, -1).lock();
    STD_ASSERT(pDefParts1);
    CPartsDef* pParts01;
    pParts01 = dynamic_cast<CPartsDef*>(pDefParts1.get());
    TEST_Ctrl_02(pParts01, 1);

    std::shared_ptr<CObjectDef> pDefParts2;
    pDefParts2 = pProj->CreateParts(_T("Parts02"), VIEW_COMMON::E_PARTS, -1).lock();
    STD_ASSERT(pDefParts2);
    CPartsDef* pParts02;
    pParts02 = dynamic_cast<CPartsDef*>(pDefParts2.get());
    TEST_Ctrl_02(pParts02, 2);

    //----------------------------------------------
    std::shared_ptr<CObjectDef> pDefParts3;
    pDefParts3 = pProj->CreateParts(_T("Parts03"), VIEW_COMMON::E_PARTS, -1).lock();
    STD_ASSERT(pDefParts3);
    CPartsDef* pParts03;
    pParts03 = dynamic_cast<CPartsDef*>(pDefParts3.get());
    TEST_Ctrl_02(pParts03, 3);

    auto  pPt_04 = std::make_shared< CDrawingParts>();
    pPt_04->SetName(_T("Parts03_04"));
    pParts03->RegisterObject(pPt_04, true, false);

    auto pGp_05 = std::make_shared<CDrawingParts>();

    pGp_05->SetPartsDef(pParts03);
    pGp_05->SetGroup(pParts01);
    pGp_05->SetName(_T("Parts03_05"));
    pParts03->RegisterObject(pGp_05, true, false);

    StdString strParts03;
    strParts03 = pParts03->GetStr();
    DB_PRINT(_T("%s\n"), strParts03.c_str());
    //----------------------------------------------

    //---------------------------------
    // ActionDrop

    //Parts02を追加
    auto pGp_Main_04 = std::make_shared<CDrawingParts>();
    pGp_Main_04->SetPartsDef(pMain);
    pGp_Main_04->SetGroup(pParts02);
    pGp_Main_04->SetName(_T("Main_04"));
    pMain->RegisterObject(pGp_Main_04, true, false);

    //点を追加
    auto pPt_05 = std::make_shared< CDrawingPoint>();
    pPt_05->SetPartsDef(pMain);
    pPt_05->SetName(_T("main_05"));
    pMain->RegisterObject(pPt_05, true, false);

    //Parts03を追加
    auto pGp_Main_06 = std::make_shared< CDrawingParts>();;
    pGp_Main_06->SetPartsDef(pMain);
    pGp_Main_06->SetGroup(pParts03);
    pGp_Main_06->SetName(_T("Main_06"));
    pMain->RegisterObject(pGp_Main_06, true, false);

 
    //---------------------------------

    StdString strRet;
    strRet = pMain->GetStr();
    DB_PRINT(_T("%s\n"), strRet.c_str());

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
}


//-------------------------------------------------------
void TEST_CPartsDef()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

#if 0
    TEST_CPartsDef_01();
#endif


    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}

#endif //_DEBUG
