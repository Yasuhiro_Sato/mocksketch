/**
 * @brief			CPartsDefヘッダーファイル
 * @file			CPartsDef.h
 * @author			Yasuhiro Sato
 * @date			03-2-2009 23:20:45
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#if !defined(EA_BFB0628D_80A2_46d6_92EF_DB2F8AB3E28B__INCLUDED_)
#define EA_BFB0628D_80A2_46d6_92EF_DB2F8AB3E28B__INCLUDED_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "./ObjectDef.h"
#include "DrawingObject/CDrawingObject.h"
#include "View/CLayer.h"
#include "View/DRAWING_DATA.h"
#include "Utility/TreeData.h"
#include "Utility/CImageData.h"
#include "DrawingObject/BaseObj.h"


/*---------------------------------------------------*/
/*  classes                                          */
/*---------------------------------------------------*/
class CDrawingView;
class CDrawingObject;
class CScript;
class CScriptData;
class CDrawingParts;
class CIdObj;
class CExecCtrl;
class CIoAccess;
struct CStdPropertyItemDef;
class CImageData;
class CImageCtrlDlg;

/*---------------------------------------------------*/
/*  Typedef                                          */
/*---------------------------------------------------*/
typedef  std::vector<std::weak_ptr<CDrawingObject>>  DRAW_OBJ_LIST;

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
//#define  GET_UNDO (THIS_APP->GetProject()->GetUndoAction())


/**
 * @class   CPartsDef
 * @brief   描画データコントロール                     
 */
class CPartsDef: public CObjectDef
{
public:
    enum E_CHG_OBJ_TYPE
    {
        E_CHG_NONE = 0,

        //!< オブジェクト名変更
        E_CHG_NAME = 1,
    };

    struct  VIEW_POS
    {
        POINT2D ptCenter;
        double  dScl = 1.0;

    private:
        friend class boost::serialization::access;  
        template<class Archive>
        void serialize(Archive& ar, const unsigned int version)
        {
            StdString strRead;
            try
            {
               ar & boost::serialization::make_nvp("Center" , ptCenter);
               ar & boost::serialization::make_nvp("Scl"    , dScl);
            }
            catch(...)
            {
                STD_DBG(_T("Serlization Error %s"), strRead.c_str());
            }
        }
    };


public:
    //!< コンストラクタ
    CPartsDef();

    //!< コピーコンストラクタ
    CPartsDef(const CPartsDef& pObj, StdString strName = _T(""));

    //!< コンストラクタ
    CPartsDef(StdString strName);

    //!< デストラクタ
    virtual ~CPartsDef();
 
    //!< スクリプトオブジェクト生成
    virtual std::shared_ptr<CDrawingScriptBase> CreateInstance(CObjectDef* pCtrl);

    //!< 初期化スクリプト生成
    virtual bool CreateInitialScript();

    //!< 一時コピー作成
    virtual void CreateTmpCopy(CObjectDef* pDef);

    //!< ビュー追加
    void AddView(CDrawingView* pView);

    //!< ビュー削除
    void DelView(CDrawingView* pView);

    //!< ビューリスト取得
    void GetViewList(std::set<CDrawingView*>* pViewList);

    //!< データ追加
    void RegisterObject(std::shared_ptr<CDrawingObject>,
                   bool bCreateId,
                   bool bKeepUnfindId);

    //!< データ追加(AS)
    void AddObject2(CDrawingObject& obj, 
                   bool bCreateId,
                   bool bKeepUnfindId);

    //!< データ置き換え
    void ReplaceObject(std::shared_ptr<CDrawingObject> pSrc, 
                       std::shared_ptr<CDrawingObject> pDst);

    //!< 指定データ削除
    void DeleteDrawingObject(CDrawingObject* pObject);

    //!<再描画完了待ち
    void RedrawWait();

    //!< 描画
    void Redraw(CDrawingView* pView = NULL, bool bUseThread = true);

    //!< 選択部描画
    void RedrawSelect(CDrawingView* pView = NULL);

    //!< ドラッグ中描画
    void DrawDragging(CDrawingView* pView);

    //!< データ非表示
    void HideObject(int iID);

    //!< 描画
    void Draw(int iID);

    //!< 選択解除
    void RelSelect();

    //!< 選択解除
    void RelSelectById(int iId);

    //--------------------------------
    // レイヤー
    //--------------------------------
    //! レイヤー設定
    void SetLayer(int iLayerId, const CLayer* pLayer);

    //! レイヤー取得
    CLayer* GetLayer(int iLayerId);

    //! カレントレイヤーID設定
    void SetCurrentLayerId(int iLayerId, bool bManual = true);

    //! カレントレイヤーID取得
    int GetCurrentLayerId() const;

    //! レイヤー追加
    void AddLayer();

    //! レイヤー削除
    bool DelLayer(int iLayerId, bool bForace);

    //! レイヤー最大数取得
    int GetLayerMax() const;
    //--------------------------------

    //オブジェクトマップ取得
    //const CDrawingNameIdData* GetObjectMap();

    //!< ドラッグ中オブジェクト取得(ID)
    std::shared_ptr<CDrawingObject> GetDraggingObject(int iId);

    //!< オブジェクト取得(ID)
    //CDrawingObject* GetObjectById(int iId, bool bIgnoreGroup = false);
    std::shared_ptr<CDrawingObject> GetObjectById(int iId, bool bIgnoreGroup = false);

    //!< オブジェクト取得(名称)
    //CDrawingObject* GetObject(StdString strName);
    std::shared_ptr<CDrawingObject> GetObject(StdString strName);

    //!< オブジェクト取得(名称) AS
    //CDrawingObject* GetObjectByName(StdString strName){return GetObject(strName);}
    std::shared_ptr<CDrawingObject> GetObjectByName(StdString strName){return GetObject(strName);}

    //!< オブジェクト取得(名称) AS
    //CDrawingObject* GetObjectByNameS(std::string strName);
    std::shared_ptr<CDrawingObject> GetObjectByNameS(std::string strName);

    StdString   GetObjectName(int iID);

    //!< オブジェクト名設定
    bool     SetObjectName(int iID, LPCTSTR strName);


    //!< トリミング
    bool Trim(int iId, const POINT2D& ptIntersect, const POINT2D& ptDir, bool bReverse = false);

    //!< 分割
    bool BreakPoint( int iId, const POINT2D& ptIntersect, bool bManual = true);

    //!< データ更新
    void Update();

    void AddGroupChg();

    //!< コールバック
    static void ChgCallback(CPartsDef* pCtrl, int iId, int eType);

    //--------------------------------
    // 選択
    //--------------------------------
    //!< オブジェクト選択(ID)
    void SelectDrawingObject(int iId);

    //!< オブジェクト選択(Obj)
    void SelectDrawingObject(std::shared_ptr<CDrawingObject> pObj);

    //!< オブジェクト選択(名称)
    void SelectDrawingObject(StdString strName);

    //!< オブジェクト選択(名称) AS用
    void SelectObjectByName(StdString strName){ SelectDrawingObject(strName);}

    //!< オブジェクト選択(範囲)
    void SelectDrawingObject(POINT pt1,  POINT pt2, const CDrawingView* pView);

    //!< 種別よるオブジェクト選択
    void SelectDrawingObject(DRAWING_TYPE eType);

    //!< レイヤーによるオブジェクト選択
    void SelectDrawingObjectLayer(int iLayer, bool bRedraw = true);

    //!< 選択オブジェクト削除
    void DeleteSelectingObject(bool bHandOver = false);

    //!< 選択解除
    void ReleaseDrawingObject(int iId);

    //!< オブジェクト移譲
    bool HandoverObject(CDrawingObject* pObj);

    //!< 選択図形数
    long SelectNum();

    //!< 選択図形
    std::vector<std::weak_ptr<CDrawingObject>>*  GetSelectInstance();

    //!< 選択図形
    std::shared_ptr<CDrawingObject>  GetSelect(int iIndex);

    //!< 選択図形コピー
    //void  CopySelect(std::vector<std::weak_ptr<CDrawingObject>>);

    //!< 選択図形コピー
    void  MoveSelect(int iIndex);

    //!< 選択確認
    bool IsSelected(CDrawingObject* pObject);

    //選択図形に接続している図形のリスト
    std::vector<std::weak_ptr<CDrawingObject>>*  GetConnectionToSelect();

	//選択図形に接続している図形のリストのクリア
   void ClearConnectionToSelect();


    //選択図形に接続している図形のリストの更新
    void UpdateNodeDataList();

    //!< 描画領域取得
    RECT2D GetViewBounds() const;

    //!< マウスオーバー設定
    void SetMouseOver(std::weak_ptr<CDrawingObject> pObject);

    //!< マウスオーバー解除
    void ClearMouseOver();

    //!< マウスオーバー(強調)設定
    void SetMouseEmphasis(std::weak_ptr<CDrawingObject> pObject);

    //!<マウスオーバー(強調)解除
    void ClearMouseEmphasis();

    //!< マウスオーバー(接続可能)設定
    void SetMouseConnectable(std::weak_ptr<CDrawingObject> pObject);

    //!<マウスオーバー(接続可能)解除
    void ClearMouseConnectable();

    //--------------------------------

    //--------------------------------
    // 検索
    //--------------------------------
    //色による検索
    void SearchObjectByColor(std::vector<CDrawingObject*>* pList, 
                             COLORREF cr,
                             const std::vector<CDrawingObject*>* pListOrgin = NULL,
                             bool bIgnoreGroup = false);

    void SearchObjectByColorAs(std::vector<CDrawingObject*>* pList, 
                             COLORREF cr)
    {
        SearchObjectByColor(pList, cr);
    }

    //種別による検索
    void SearchObjectByType(std::vector<CDrawingObject*>* pList, 
                            DRAWING_TYPE type,
                             const std::vector<CDrawingObject*>* pListOrgin = NULL,
                             bool bIgnoreGroup = false);

    void SearchObjectByTypeAs(std::vector<CDrawingObject*>* pList, 
                            DRAWING_TYPE type)
    {
        SearchObjectByType(pList, type);
    }
    void SearchObjectByType(std::vector<CDrawingObject*>* pList, 
                            const std::set<DRAWING_TYPE>*  pTypeSet,
                            const std::vector<CDrawingObject*>* pListOrgin = NULL,
                             bool bIgnoreGroup = false);


    //名前による検索
    void SearchObjectByNmae(std::vector<CDrawingObject*>* pList,
                            StdString strName,
                             const std::vector<CDrawingObject*>* pListOrgin = NULL,
                             bool bIgnoreGroup = false);

    void SearchObjectByNmaeAs(std::vector<CDrawingObject*>* pList, 
                            StdString strName)
    {
        SearchObjectByNmae(pList, strName);
    }

    //プロパティセットによる検索
    void SearchObjectByPropertyset(std::vector<CDrawingObject*>* pList, 
                             const std::vector<StdString>* pListProperty,
                             const std::vector<CDrawingObject*>* pListOrgin = NULL,
                             bool bIgnoreGroup = false);

    void SearchObjectByPropertysetAs(std::vector<CDrawingObject*>* pList, 
                            const std::vector<StdString>* pListProperty)
    {
        SearchObjectByPropertyset(pList, pListProperty);
    }

    //タグによる検索
    void SearchObjectByTag(std::vector<CDrawingObject*>* pList, 
                            StdString strTag,
                             const std::vector<CDrawingObject*>* pListOrgin = NULL,
                             bool bIgnoreGroup = false);

    void SearchObjectByTagAs(std::vector<CDrawingObject*>* pList, 
                            StdString strTag)
    {
        SearchObjectByTag(pList, strTag);
    }

    //領域による検索
    void SearchObjectByArea(std::vector<CDrawingObject*>* pList,
                             RECT2D rc,
                             const std::vector<CDrawingObject*>* pListOrgin = NULL,
                             bool bIgnoreGroup = false);

    void SearchObjectByAreaAs(std::vector<CDrawingObject*>* pList, 
                            RECT2D rc)
    {
        SearchObjectByArea(pList, rc);
    }

    void SearchPropertysetWithSortDistance(std::vector<StdString>* pList,
                          POINT2D pt,
                          const std::vector<StdString>* pListProperty,
                          bool bNode);


    //複合検索
    void SearchObject      (std::vector<CDrawingObject*>* pList,
                            bool bColor, COLORREF cr,
                            bool bType,  const std::set<DRAWING_TYPE>*  pTypeSet,
                            bool bName,  StdString strName,
                            bool bProp,  const std::vector<StdString>* pListProperty,
                            bool bTag,   StdString strTag,
                            bool bArea,  RECT2D rc,
                            const std::vector<CDrawingObject*>* pListOrgin = NULL,
                            bool bIgnoreGroup = false);


    bool _CheckCondition(const CDrawingObject* pObj,
                            bool bColor, COLORREF cr,
                            bool bType,  const std::set<DRAWING_TYPE>*  pTypeSet,
                            bool bName,  StdString strName,
                            bool bProp,  const std::vector<StdString>* pListProperty,
                            //bool bPropGroup,  StdString strPropertyGroup,
                            bool bTag,   const std::vector<StdString>*  pTagSet,
                            bool bArea,  RECT2D rc);

    void _SearchNodeGroup(std::vector<CDrawingObject*>* pList,
                                CDrawingParts* pGroup,
                                bool bColor, COLORREF cr,
                                bool bName,  StdString strName,
                                bool bProp,  const std::vector<StdString>* pListProperty,
                                //bool bPropGroup,  StdString strPropertyGroup,
                                bool bTag,   const std::vector<StdString>*  pTagSet,
                                bool bArea,  RECT2D rc);

    //-----------------------
    // オブジェクト関連
    //-----------------------

    //!< 部品使用有無
    bool IsIncludeParts(boost::uuids::uuid uuidParts)  const;

    //!< 部品使用有無
    bool IsIncludeParts(const CPartsDef* pRefCtrl) const;

    //!< 部品使用有無(非const)
    bool IsIncludeParts2(const CPartsDef* pRefCtrl);

    //!< 名称生成
    StdString CreateName(StdString strBase);

    //----------------
    // IO関連
    //----------------

    //Ioウインドウ表示ページ保存
    int GetIoPage(int iInde) const;

    //Ioウインドウ表示ページ取得
    bool SetIoPage(int iIndex, int iPage);

    //Ioウインドウ表示オブジェクトID取得
    int GetIoObjId(int iIndex) const;

    //Ioウインドウ表示オブジェクトID設定
    bool SetIoObjId(int iIndex, int iObjId);

    //----------------
    // 部品関連
    //----------------

    //!< 部品使用有無
    bool IsIncludeParts(const CPartsDef* pRefCtrl);

    //!< 部品使用箇所削除
    bool DeleteUsingParts(const CPartsDef* pRefCtrl);

    //!< 分解
    virtual bool Disassemble(std::shared_ptr<CDrawingParts>, bool bSetUndo) override;

    //-----------------------
    // 問い合わせ
    //-----------------------
    //!< 変更の有無
    virtual bool IsChange()const;

    //----------------
    // ファイル
    //----------------
    //!< 保存パス取得(フルパス)
    //StdString  GetFilePath();

    virtual void LoadAfter();

    //----------------

    //!< ID再使用要求
    void ReUseId(int iId);

    //!< ID生成
    int  CreateId();

    //----------------
    //!< 描画オブジェクト取得
    std::shared_ptr<CDrawingParts>   GetDrawingParts();
    virtual std::shared_ptr<CDrawingScriptBase> GetViewObject(); 

    //!< ドラッグ中オブジェクト取得
    std::vector< std::shared_ptr<CDrawingObject>>*   CPartsDef::GetConnectingTmpObjectList();
    std::vector< std::shared_ptr<CDrawingObject>>*   CPartsDef::GetTmpGroupObjectList();


    //-----------------
    // 構築
    //-----------------
    //!< コンパイル
    virtual bool Compile( bool bDebug, bool bRebuild);
    
    //!< コンパイル
    virtual bool IsCompiled( std::vector<StdString>* lstFileName) const;

    //----------------
    // デバッグ
    //----------------
    StdString GetStr();

    //ID最大値設定
    void SetMaxId(int iMax);

    //===================================
    //----------------
    // AS
    //----------------
    /*
	void AddRef() {m_iRefCount++;}
	void Release() {if( --m_iRefCount == 0 ) delete this;}
    */
    //===================================

    //-----------------
    // ファイル
    //-----------------
    //!<読込
    virtual bool Load(StdPath Path);

    //!<保存
    virtual bool Save(StdPath Path);

    //-----------------
    // 図形
    //-----------------


    //-----------------
    // 描画排他
    //-----------------
    void SetDrawLock();
    void ReleaseDrawLock();

    //-----------------
    // デバッグ用
    //-----------------
    virtual void Print() const;

    //----------------
    // プロパティ
    //----------------

    //!< プロパティ初期化
    virtual TREE_GRID_ITEM*  _CreateStdPropertyTree();


    bool IsShowPrintArea(){return  m_bPrintArea;}
    void SetShowPrintArea(bool bShow){m_bPrintArea = bShow;}

    int  GetPrintRow(){return  m_iPrintRow;}
    void SetPrintRow(int iRow){m_iPrintRow = iRow;}

    int  GetPrintCol(){return  m_iPrintCol;}
    void SetPrintCol(int iCol){m_iPrintCol = iCol;}


    //!< 用紙範囲表示
    bool IsShowPaperArea(){return  m_bPaperArea;}
    void SetShowPaperArea(bool bShowPaper){m_bPaperArea = bShowPaper;}

    //!< オフセット量を用紙サイズに合わせる
    bool IsOffsetSizeEqualPaper(){return  m_bOffsetSizeEqualPaper;}
    void SetOffsetSizeEqualPaper(bool bEqual){m_bOffsetSizeEqualPaper = bEqual;}

    //行オフセット量
    double GetOffsetPageRow(){return  m_dOffsetPageRow;}
    void SetOffsetPageRow(double bEqual){m_dOffsetPageRow = bEqual;}

    //列オフセット量
    double GetOffsetPageCol(){return  m_dOffsetPageCol;}
    void SetOffsetPageCol(double bEqual){m_dOffsetPageCol = bEqual;}

    //
    bool IsUseNodeBounds(){return  m_bUseNodeBounds;}
    void SetUseNodeBounds(bool bUsel){m_bUseNodeBounds = bUsel;}

    bool IsIgnoreSelPart() const { return  m_bIgnoreSelPart; }
    void SetIgnoreSelPart(bool bUsel) { m_bIgnoreSelPart = bUsel; }

	void RegisterPrintArea(std::shared_ptr<CDrawingObject> pObject);
	void RegisterGridDot(std::shared_ptr<CDrawingObject> pObject);


    static bool PropPrintArea    (CStdPropertyItem* pData, void* pObj);
    static bool PropPrintRow     (CStdPropertyItem* pData, void* pObj);
    static bool PropPrintCol     (CStdPropertyItem* pData, void* pObj);

    static bool PropPaperArea               (CStdPropertyItem* pData, void* pObj);
    static bool PropOffsetSizeEqualPaper    (CStdPropertyItem* pData, void* pObj);
    static bool PropPrintOffetRow           (CStdPropertyItem* pData, void* pObj);
    static bool PropPrintOffetCol           (CStdPropertyItem* pData, void* pObj);

    static bool PropUseNodeBounds           (CStdPropertyItem* pData, void* pObj);
    static bool PropIgnoreSelPart           (CStdPropertyItem* pData, void* pObj);
    static bool PropDummy                   (CStdPropertyItem* pData, void* pObj);


    //--------------
    // 画像
    //--------------
    //イメージデータ有無
    bool IsImage(LPCTSTR strName);

    //イメージデータ取得
    std::weak_ptr<CImageData> GetImage(LPCTSTR strName);

    bool LoadImageDlg( StdString* pFileNeme);

    //イメージデータ設定
    bool LoadImage(LPCTSTR strFileName);

    //イメージデータ削除
    bool DeleteImage(LPCTSTR strFileName);

    //イメージリスト取得
    bool GetImageList(std::vector<StdString>* pList);

    StdStreamIn* GetStreamIn();

    //描画順序
    void ChangeObjectOrder(VIEW_COMMON::E_DISP_ORDER order, int iId);

    //描画位置
    CPartsDef::VIEW_POS GetViewPosition(int iIndex);
    void SetViewPosition(int iIndex, CPartsDef::VIEW_POS& pos);

    //-----------
    // 接続
    //-----------
    void  SetLockUpdateNodeData(bool bLock){m_bLockUpdateNodeData = bLock;}
    bool  IsLockUpdateNodeData()const {return m_bLockUpdateNodeData;}

    //接続ID更新
    bool  _UpdateConnectionOne(CUndoAction* pUndo,
                               std::map<int, int>* pMapConvId,
                               std::shared_ptr<CDrawingObject> pC,
                               bool bKeepUnfindId);

    bool UpdateConnectionAndPosition
                                     ( CUndoAction* pUndo,
                                      std::map<int, int>* pMapConvId,
                                      std::vector<int>* pLstConnection,
                                      bool bKeepUnfindId);


    bool UpdateConnection(std::map<int, int>* pMapConvId,
        std::vector<std::shared_ptr<CDrawingObject>>* pLstConnection,
        bool bKeepUnfindId);

    //接続によって変更が生じるオブジェクトのリスト
    std::vector<NODE_CHANGE_DATA>* GetListChangeByConnection();
    

    bool AddListChangeByConnection(NODE_CHANGE_DATA& data);

	//デバッグ用
	bool _CheckSelect();
protected:
friend CPartsDef;

    //!< 実行グループ登録
    void _RegisterExecCtrl(CDrawingParts* pGroup);

    //!< オブジェクト選択(ID)
    void _SetSelect(std::weak_ptr<CDrawingObject> pObj, const RECT2D* pRect = NULL);

    //!< 選択解除
    void _ClearSelect(CDrawingObject* pObj = NULL);
    
    //!< レイヤー初期化
    void  _InitLayer();

    //!< 描画オブジェクト変更
    void _ChangeObjecs(std::shared_ptr<CDrawingParts> pObjects);
    virtual void _SetViewObject(CDrawingScriptBase* ppObjects);

    //!< 描画リスト生成
    void _SetDrawData(  std::vector< DRAW_OBJ_LIST >* pDisplyList,
                        std::vector< DRAW_OBJ_LIST >* pLstPoint,
                        std::vector< DRAW_OBJ_LIST >* pLstNoIndex,
                        DRAW_OBJ_LIST* pLstScriptBase);

    //!< IDガベージコレクション
    void _GarbageCollectionId();

    //!< 描画処理
    static void _CallDrawView(CPartsDef* pCtrl, CDrawingView* pView);

    //!< 選択部描画処理
    static void _CallDrawSelect(CPartsDef* pCtrl, CDrawingView* pView);

    //!< ドラッグ描画処理
    static void _CallDrawDragableMarker(CPartsDef* pCtrl, CDrawingView* pView);

    //!< 描画処理
    void _DrawView(CDrawingView* pView);

    //!< 選択部描画処理
    void _DrawSelect(CDrawingView* pView, bool bIndex);
    void _DrawSelectExt(CDrawingView* pView, bool bIndex);

    void _DrawDragableMarker(CDrawingView* pView);

    //!<スクリプト生成実行
    virtual bool _SetExecute();

    void _ReconnectIo();

	//デバッグ用
	void _Print(int iType) const;


protected:
    //===================================
    
    //!< 描画オブジェクト
    std::shared_ptr<CDrawingParts>              m_psObjects;

    //!< 選択
    std::vector< std::weak_ptr<CDrawingObject>> m_lstSelect;

    //!<選択図形に接続している図形
    std::vector< std::weak_ptr<CDrawingObject>> m_lstConnectedSelect;

    //!< 表示リスト
    std::set<CDrawingView*>                  m_lstDrawing;

    //!< 描画データ
    DISPLAY_LIST                             m_DisplayList;

    //!< レイヤー
    std::vector<CLayer>                      m_lstLayer;

    //!< オブジェクトID設定用
    std::shared_ptr<CIdObj>                m_psIdObject;
    //CIdObj*                                  m_psIdObject;

    //!< カレントレイヤID
    int                                      m_curLayerId;

    //!< IO定義
    std::shared_ptr<CIoAccess>              m_psAccess; 

    //!< 保存パス(フルパス)
    //StdString                               m_strFilePath;
    
    //!< 印刷範囲表示
    bool                                     m_bPrintArea;

    //!< 用紙範囲表示
    bool                                     m_bPaperArea;

    //!< オフセット量を用紙サイズに合わせる
    bool                                     m_bOffsetSizeEqualPaper;

    //列オフセット量
    double                                   m_dOffsetPageCol;

    //行オフセット量
    double                                   m_dOffsetPageRow;

    //!< 印刷行
    int                                      m_iPrintRow;

    //!< 印刷列
    int                                      m_iPrintCol;

    //!< ノードによる移動変形操作
    bool                                     m_bUseNodeBounds;

    std::vector<VIEW_POS>                    m_lstViewPos;

    //!< ストレッチモード時強制的に矩形選択が全範囲設定となる
    bool                                     m_bIgnoreSelPart;

    //---------------
    // 非保存データ
    //---------------
    //!<ミューテックス
    boost::mutex                            m_Mtx; 

    //ユーザ定義データ変更カウンタ(非保存)
    //int                                     m_iChgPropUserCnt;

    //!< 描画用データ
    std::vector< DRAW_OBJ_LIST >    m_layerDraw;

    //!< 点描画用データ
    std::vector< DRAW_OBJ_LIST >    m_lstPoint;

    //!< インデックス非描画用データ
    std::vector< DRAW_OBJ_LIST >    m_lstNoIndex;

    //!< マウスオーバ用データ
    DRAW_OBJ_LIST     m_lstMouseOver;

    //!< マウスオーバ(強調)用データ
    DRAW_OBJ_LIST     m_lstMouseEmphasis;

    //!< マウスオーバ(接続可能)用データ
    DRAW_OBJ_LIST     m_lstMouseConnectable;

    //!< IOダイアログ表示用データ
    DRAW_OBJ_LIST     m_lstDrawingScriptBase;

    //イメージデータ
    std::map<StdString,std::shared_ptr<CImageData>>    m_mapImage;

    //!< IOダイアログ表示用スクリプトデータ変更カウント
    int                                    m_iCntDraw;

    //Ioウインドウ表示ページ保存
    std::vector<int>    m_lstIoPage;

    //Ioウインドウ表示オブジェクトID
    std::vector<int>    m_lstIoObj;

    //!< 描画スレッド
    boost::thread       m_tharedDraw;

    //!< 描画完了フラグ
    bool                m_bDrawComplete;

    //!< 描画ロックフラグ
    bool m_bDrawLock;
    bool m_bDrawing;

    //用紙プロパティ
    double m_dPaperWidth;
    double m_dPaperHeight;
    double m_dPrintWidth;
    double m_dPrintHeight;
    double m_dPrintOffsetX;
    double m_dPrintOffsetY;

    std::unique_ptr<CImageCtrlDlg> m_pImageDlg;
    StdString                      m_strImageList; 


    //ストリーム
    std::unique_ptr<StdStreamIn> m_psInFs;

    //接続に追従して変更があるオブジェクト
    std::vector<NODE_CHANGE_DATA> m_lstChangeByConnection;

    //!< ドラッグ中オブジェクトリスト
    std::vector< std::shared_ptr<CDrawingObject>> m_lstConnecting;
    std::vector< std::shared_ptr<CDrawingObject>> m_lstTmpGroup;

	std::shared_ptr<CDrawingObject>             m_psPaperFrame;
	std::shared_ptr<CDrawingObject>             m_psPrintFrame;
	std::shared_ptr<CDrawingObject>             m_psGrid;

    bool m_bLockUpdateNodeData;
private:

    void Dummy();

    //==============================
    // ファイル保存
    //==============================
    friend class boost::serialization::access;  
    BOOST_SERIALIZATION_SPLIT_MEMBER();

    template<class Archive>
    void load(Archive& ar, const unsigned int version);

    template<class Archive>
    void save(Archive& ar, const unsigned int version) const;

};

BOOST_CLASS_VERSION(CPartsDef, 0);

#endif // !defined(EA_BFB0628D_80A2_46d6_92EF_DB2F8AB3E28B__INCLUDED_)
