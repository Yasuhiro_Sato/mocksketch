/**
 * @brief			CReferenceDef実装ファイル
 * @file			CReferenceDef.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CReferenceDef.h"
#include "./CPartsDef.h"
#include "DefinitionObject/View/CDrawingView.h"
#include "DrawingObject/CNodeMarker.h"
#include "DrawingObject/CDrawingParts.h"
#include "DrawingObject/CDrawingField.h"
#include "View/CLayer.h"
#include "View/CDrawMarker.h"
#include "View/CUndoAction.h"
#include "Script/CScript.h"
#include "Script/CExecCtrl.h"
#include "System/CSystem.h"
#include "System/MOCK_ERROR.h"
#include "Utility/CUtility.h"
#include "Utility/CIdObj.h"
#include "Utility/CPropertyGroup.h"
#include "MockSketch.h"
#include "CProjectCtrl.h"

#include "Io/CIoAccess.h"
#include "Io/CIoRef.h"
#include "Io/CIoDef.h"
#include "Io/CIoConnect.h"
#include "Io/CIoPropertyBlock.h"
#include "Io/CIoAccess.h"
#include "ExecCommon.h"

#include <math.h>
#include <boost/serialization/export.hpp> 
/*---------------------------------------------------*/
/*  Defines                                          */
/*---------------------------------------------------*/

BOOST_CLASS_EXPORT(CReferenceDef);


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
CReferenceDef::CReferenceDef():
CPartsDef   ()
{
    m_Prop.eObjType  = VIEW_COMMON::E_REFERENCE;
    m_Prop.strThreadType = EXEC_COMMON::GetThreadType2Name(EXEC_COMMON::TH_NONE);
}


/**
 * @brief   コンストラクタ
 * @param   [in] strName 名称
 * @retval  なし
 * @note
 */
CReferenceDef::CReferenceDef(StdString strName):
CPartsDef   (strName)
{
    m_Prop.eObjType  = VIEW_COMMON::E_REFERENCE;
    m_Prop.strThreadType = EXEC_COMMON::GetThreadType2Name(EXEC_COMMON::TH_NONE);
}


/**
 *  @brief  コピーコンストラクタ.
 *  @param  [in] pObj    コピー元データ
 *  @param  [in] strName コピー先名称
 *  @retval なし     
 *  @note
 */
CReferenceDef::CReferenceDef(const CReferenceDef& pObj, 
                    StdString strName):
CPartsDef  (pObj, strName)
{
    m_Prop.eObjType  = VIEW_COMMON::E_REFERENCE;
}

/**
 *  @brief  デストラクタ.
 *  @param  なし 
 *  @retval なし 
 *  @note
 */
CReferenceDef::~CReferenceDef()
{
}

//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void TEST_CReferenceDef()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}
#endif //_DEBUG