/**
* @brief        CReferenceDefヘッダーファイル
* @file	        CReferenceDef.h
* @author           Yasuhiro Sato
* @date	        07-2-2009 20:03:09
* 
* @note	
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
* 
* @remarks	        
*			
*
* $
* $
* 
*/
#ifndef __REFERENCE_DEF_H_
#define __REFERENCE_DEF_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DefinitionObject/ObjectDef.h"
#include "DefinitionObject/CPartsDef.h"

/*---------------------------------------------------*/
/*  classes                                          */
/*---------------------------------------------------*/
class CImageData;



/**
 * @class   CReferenceDef
 * @brief  
 */
class CReferenceDef: public CPartsDef
{
    
public:
    //!< コンストラクタ
    CReferenceDef();

    //!< コンストラクタ
    CReferenceDef(StdString strName);

    //!< コピーコンストラクタ
    CReferenceDef(const CReferenceDef& pObj, StdString strName = _T(""));

    //!< デストラクタ
    virtual ~CReferenceDef();

    bool CReferenceDef::Create(std::shared_ptr<CIoAccess> pIoAccess,
                           CObjectDef* pDef);

protected:

    
private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
            ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(CPartsDef);
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }
};

#endif // __FIELD_DEF_H_