/**
 * @brief       ObjectDef実装ファイル
 * @file        ObjectDef.cpp
 * @author      Yasuhiro Sato
 * @date        01-2-2009 10:36:26
 * 
 * @note
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks        
 *
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./ObjectDef.h"
#include "Script/CExecCtrl.h"
#include "Utility/CStdPropertyTree.h"
#include "Utility/CPropertyGrid.h"
#include "Utility/CUtility.h"
#include "Utility/CPropertyGroup.h"

#include "View/CUndoAction.h"
#include "Io/CIoDef.h"
#include "Io/CIoAccess.h"
#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/CElementDef.h"
#include "DefinitionObject/CFieldDef.h"
#include "DrawingObject/CDrawingScriptBase.h"
#include "System/CSystem.h"
#include "Script/CModule.h"
#include "Script/CScript.h"

#include "CProjectCtrl.h"
#include "Mocksketch.h"
/*---------------------------------------------------*/
/*  Defines                                          */
/*---------------------------------------------------*/
using namespace EXEC_COMMON;

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/*---------------------------------------------------*/
/*  static                                           */
/*---------------------------------------------------*/
//E_APP_MODE  CObjectDef::m_eMode = MODE_NONE;


void CProperty::SetTagStr(const StdString& strTag)
{
    CUtil::TokenizeCsv(&lstTag, strTag);
}

StdString CProperty::GetTagStr() const
{
    StdString strTag;
    size_t nSize = lstTag.size();
    for(size_t nCnt = 0; nCnt < nSize; nCnt++)
    {
        strTag += lstTag[nCnt];
        if ( (nCnt + 1) != nSize)
        {
            strTag += _T(",");
        }
    }
    return strTag;
}

void CProperty::Print() const
{
    StdString strTag;
    strTag = GetTagStr();
    DB_PRINT(_T("Name:%s Type %d \n")   , strName.c_str(), eObjType);
    DB_PRINT(_T("Tags:%s Thread %s \n") , strTag.c_str(), strThreadType.c_str());

    /*    
    SERIALIZATION_BOTH("DefaultCycleTime" , dwCycleTime);
    SERIALIZATION_BOTH("DefaultPriority"  , iTaskPriority);
    SERIALIZATION_BOTH("Debug"         , eDebug);
    SERIALIZATION_BOTH("SingleFile"    , bSingleFile);
    SERIALIZATION_BOTH("AutoCompile"   , iAutoCompile);
    SERIALIZATION_BOTH("Parmission"    , ePermission);
    SERIALIZATION_BOTH("PropertySet"   , strPropertySet);
    */
}


/**
 * @brief   コンストラクタ
 * @param   なし
 * @retval  なし
 * @note
 */
CObjectDef::CObjectDef():
m_iChgPropUserCnt   (1),
m_iRefCount         (1),
m_pProj             (NULL),
m_psViewObject      (NULL),
m_bInitProperty     (false),
m_bAbortBuild       (false),
m_bCopy             (false)

{
    //通常は使わない
    m_Prop.uuidParts = boost::uuids::random_generator()();
    m_psUndoAction = std::make_shared<CUndoAction>();

    m_psScript    = new CScript;
    m_psIoDef     = new CIoDef;
    m_psProperty  = new CStdPropertyTree;

    //m_porpUser.push_back(pDef);
    m_psPorpUser = new CPropertySet;
    m_psModule = new CModule(this);
}


/**
 * @brief   コンストラクタ
 * @param   [in] strName 名称
 * @retval  なし
 * @note
 */
CObjectDef::CObjectDef(StdString strName):
m_iChgPropUserCnt   (1),
m_iRefCount         (1),
m_Prop              (strName),
m_pProj             (NULL),
m_psViewObject      (NULL),
m_bInitProperty     (false),
m_bAbortBuild       (false),
m_bCopy             (false)

{
    m_Prop.uuidParts = boost::uuids::random_generator()();
    m_psUndoAction = std::make_shared<CUndoAction>();

    m_psScript    = new CScript;
    //m_psExecCtrl  = new CExecCtrl(this);
    m_psIoDef     = new CIoDef;
    m_psProperty  = new CStdPropertyTree;

    m_psPorpUser = new CPropertySet;
    m_psModule = new CModule(this);
}


/**
*  @brief  コピーコンストラクタ.
*  @param  pObj    コピーデータ
*  @param  strName コピーデータ
*  @retval なし     
*  @note
*/
CObjectDef::CObjectDef(const CObjectDef& pObj, 
                       StdString strName):
m_iChgPropUserCnt   (1),
m_iRefCount         (1),
m_bAbortBuild       (false),
m_bCopy             (false),
m_bInitProperty     (false),
m_Prop              (strName)
{

    m_Prop.uuidParts = boost::uuids::random_generator()();
    m_psUndoAction = std::make_shared<CUndoAction>();

    //通常 表示オブジェクトを設定しているものはコピーしない
    STD_ASSERT(m_psViewObject);
    //m_psViewObject = pObj.m_psViewObject;

    m_psScript    = new CScript(*pObj.m_psScript);
   
    
    m_psProperty  = new CStdPropertyTree;
    m_pProj       = pObj.m_pProj;
    m_Prop.strThreadType =pObj.m_Prop.strThreadType;
    
    //!< TODO:IO
    //m_psAccess = std::make_shared<CIoAccess>(*pObj.m_psAccess.get()) ; 
    //GetIoDef()->Create(this, m_psAccess);

    m_psPorpUser = new CPropertySet;
    *m_psPorpUser = *pObj.m_psPorpUser;

    m_psModule = new CModule(this);
}

/**
 * @brief   デストラクタ
 * @param   なし
 * @retval  なし
 * @note
 */
CObjectDef::~CObjectDef()
{
    _DeleteAllObject();

    if (m_psModule)
    {
        delete m_psModule;
        m_psModule = NULL;
    }


}

/**
 * @brief   全オブジェクト消去
 * @param   なし
 * @retval  なし
 * @note
 */
void CObjectDef::_DeleteAllObject()
{
    if(m_bCopy)
    {
        m_psIoDef   = NULL;
        m_psScript  = NULL;
        m_psModule  = NULL;
        m_psProperty = NULL;
        m_psPorpUser = NULL;
        return;
    }

    if (m_psScript)
    {
        delete m_psScript;
        m_psScript = NULL;
    }

    if (m_psIoDef)
    {
        delete m_psIoDef;
        m_psIoDef = NULL;
    }

    if (m_psProperty)
    {
        delete m_psProperty;
        m_psProperty = NULL;
    }

    if (m_psPorpUser)
    {
        delete m_psPorpUser;
        m_psPorpUser = NULL;
    }

    // これは、コピーしない。
    // コピーしたオブジェクトの
    if (m_psViewObject)
    {
        delete m_psViewObject;
        m_psViewObject = NULL;
    }
}

/**
 * @brief   クローン生成
 * @param   [in]  pObject
 * @param   [in]  strName
 * @retval  スクリプトオブジェクト
 * @note
 */
 std::shared_ptr<CObjectDef> CObjectDef::Clone(CObjectDef* pObject,
                                                 StdString strName)
{
    using namespace VIEW_COMMON;
    std::shared_ptr<CObjectDef> pRet;
    switch(pObject->GetObjectType())
    {
    case E_PARTS:
    {
        CPartsDef* pSrc = dynamic_cast<CPartsDef*>(pObject);
        pRet = std::make_shared<CPartsDef>(*pSrc, strName);
        break;
    }
    case E_FIELD:
    {
        CFieldDef* pSrc = dynamic_cast<CFieldDef*>(pObject);
        pRet = std::make_shared<CFieldDef>(*pSrc, strName);
        break;
    }
    case E_ELEMENT:
    {
        CElementDef* pSrc = dynamic_cast<CElementDef*>(pObject);
        pRet = std::make_shared<CElementDef>(*pSrc, strName);
        break;
    }
    default:
        break;
    }
    
    return pRet;
}

/**
 * @brief   一時コピー作成
 * @param   [out] pDef
 * @retval  なし
 * @note    CExecCtrl::Execで使用 
 *          実行用に一時的なCPartsDefを生成する
 */
void CObjectDef::CreateTmpCopy(CObjectDef* pDef)
{

    pDef->_DeleteAllObject();

    pDef->m_Prop      = m_Prop;

    pDef->m_psIoDef   = m_psIoDef;
    pDef->m_psScript  = m_psScript;
    pDef->m_psModule         = m_psModule;
    pDef->m_psProperty       = m_psProperty;
    pDef->m_psPorpUser       = m_psPorpUser;


    pDef->m_bCopy         = true;       //<< 一時コピーであることを示す
    pDef->m_pProj         = m_pProj;
    pDef->m_iChgPropUserCnt  = 0;
    pDef->m_iRefCount        = 0;
    pDef->m_bAbortBuild      = false;

    pDef->m_bInitProperty    = m_bInitProperty;

    pDef->m_eType           = m_eType;

    // 一時コピーしたものに対して m_psViewObject
    // (Viewで表示するためのオブジェクト
    // 設定していないはず
    STD_ASSERT(!pDef->m_psViewObject);
}


/**
 * @brief   初期化スクリプト生成
 * @param   なし
 * @retval  true 成功
 * @note	
 */
bool CObjectDef::CreateInitialScript()
{
    StdString strInitScript = _T("_INITILIZE_SCRIPT_");
   //!< スクリプト削除
    DeleteScript(strInitScript);

    //!< スクリプト生成
    if(!CreateScript(strInitScript))
    {
        return false;
    }


    //!< スクリプト取得(名称)
    CScriptData* pData = GetScriptInstance(strInitScript);



    std::stringstream   strmScript;

    strmScript << "void TestFunc() {";
    strmScript << " Printf(\"テスト test テステス\n \");";
    strmScript << "}";


    bool bRet;
    bRet = pData->SetScriptString(strmScript.str());

 
    return bRet;
}

/**
 * @brief   名称取得
 * @param   なし
 * @retval  名称
 * @note	
 */
StdString  CObjectDef::GetName() const
{
    return m_Prop.strName;
}



//!< タグ追加
bool CObjectDef::AddTag(StdString strName)
{
    //TODO:実装
    return false;
}

//!< タグリスト取得
bool CObjectDef::GetTagList(std::vector<StdString>* pListTag) const
{
    //TODO:実装
    return false;
}

//!< タグリスト設定
bool CObjectDef::SetTagList(const std::vector<StdString>& listTag)
{
    //TODO:実装
    return false;
}

//!< タグ判定
bool CObjectDef::IsIncludeTag(StdString strTag)
{
    //TODO:実装
    return false;
}


/**
 * @brief   アンドゥの取得
 * @param   なし
 * @retval  アンドゥ
 * @note	
 */
CUndoAction*  CObjectDef::GetUndoAction()
{
    return m_psUndoAction.get();
}

/**
 * @brief   名称設定
 * @param   [in] strName 名称
 * @param   [in] bManual 手動操作(手動操作時はUndoを有効にする)
 * @retval  
 * @note	
 */
bool CObjectDef::SetName(StdString strName, bool bManual)
{
    if(!GetProject()->IsPossiblePartsName( strName))
    {
        return false;
    }

    CUndoAction* pUndo = NULL;
    if (!bManual)
    {
        //TAG:[Undo]
    }

    m_Prop.strName = strName;
    return true;
}


/**
 *  @brief  部品ID取得
 *  @param  なし
 *  @retval 部品ID
 *  @note   
 */
boost::uuids::uuid  CObjectDef::GetPartsId() const
{
    //return m_iPartsId;
    return m_Prop.uuidParts;
}

/**
 *  @brief  プロジェクト設定
 *  @param  [in] pProject
 *  @retval なし
 *  @note   
 */
void CObjectDef::SetProject(CProjectCtrl* pProject)
{
    m_pProj = pProject;
}

/**
 *  @brief  プロジェクト取得
 *  @param  なし
 *  @retval プロジェクトへのポインタ
 *  @note   
 */
CProjectCtrl* CObjectDef::GetProject() const
{
    return m_pProj;
}

/**
 *  @brief   プロパティ初期化.
 *  @param   [in] pCtrl オブジェクトコントロールへのポインタ
 *  @retval  最終グループ
 *  @note
 */
TREE_GRID_ITEM*  CObjectDef::_CreateStdPropertyTree()
{
    boost::tuple< CStdPropertyItem*, void*> pairVal;

    //int iId;
    CStdPropertyItem* pItem;
    NAME_TREE<CStdPropertyItem>* pTree;

    NAME_TREE_ITEM<CStdPropertyItem>* pNextItem;
    NAME_TREE_ITEM<CStdPropertyItem>* pGroupItem;

    m_psProperty->SetParent(this);
    pTree = m_psProperty->GetTreeInstance();
    pTree->Clear();

    //グループ作成
    pGroupItem  = pTree->AddChild(NULL, NULL, GET_STR(STR_PRO_FIG_DEF), _T("Property"));

    //-----------
    //実行グループ
    //-----------
    StdString strTypeList;

    using namespace EXEC_COMMON;
    int iStart;
    int iEnd;
    if (GetObjectType() == VIEW_COMMON::E_FIELD)
    {
        iStart = TH_FIELD_1;
        iEnd   = TH_FIELD_MAX;
    }
    else
    {
        iStart = TH_GROUP_1;
        iEnd   = TH_GROUP_MAX;
    }

    E_THREAD_TYPE eType;
    for (int iCnt = iStart; iCnt < iEnd; iCnt++)
    {
        if (iCnt != iStart)
        {
            strTypeList += _T(",");
        }
        eType = static_cast<E_THREAD_TYPE>(iCnt);
        strTypeList += EXEC_COMMON::GetThreadType2Name(eType);
    }

    CStdPropertyItemDef DefExecThreadType
        (PROP_DROP_DOWN,                        //入力種別
        GET_STR(STR_PRO_EXEC_THREAD_TYPE)   ,   //項目名
        _T("ExecThreadType"), 
        GET_STR(STR_PRO_INFO_EXEC_THREAD_TYPE), 
        false,                                       
        DISP_NONE,
        StdString(_T("")),
        strTypeList, 
        StdString(_T("")));

    pItem = new CStdPropertyItem(
        DefExecThreadType, 
        _PropThreadType, 
        m_psProperty, 
        NULL,
        (void*)&m_Prop.strThreadType);
    
    pNextItem = pTree->AddChild(pGroupItem, 
        pItem, 
        DefExecThreadType.strDspName, 
        pItem->GetName());

    //---------------
    //実行サイクルタイム
    //---------------
    CStdPropertyItemDef DefCycleTime(PROP_INT_RANGE, 
        GET_STR(STR_PRO_EXEC_CYCKE_TIME)   , 
        _T("CycleTime"), 
        GET_STR(STR_PRO_INFO_EXEC_CYCKE_TIME), 
        true,                  //Edit許可                 
        DISP_NONE,             //UNIT
        m_Prop.dwCycleTime);   //INIT

    pItem = new CStdPropertyItem(
        DefCycleTime,           //定義
        _PropCycleTime,         //入力時コール爆
        m_psProperty,           //親グリッド
        NULL,                   //更新時コールバック
        (void*)&m_Prop.dwCycleTime); //実データ
    
    pNextItem = pTree->AddNext(pNextItem, 
        pItem, 
        DefCycleTime.strDspName, 
        pItem->GetName());

    //---------------
    //実行プライオリティ
    //---------------
    CStdPropertyItemDef DefPriority(PROP_INT_RANGE, 
        GET_STR(STR_PRO_EXEC_PRIORITY)   , 
        _T("Priority"), 
        GET_STR(STR_PRO_INFO_EXEC_PRIORITY), 
        true,                  //Edit許可                 
        DISP_NONE,             //UNIT
        m_Prop.iTaskPriority,
        1,5);   //INIT

    pItem = new CStdPropertyItem(
        DefPriority,           //定義
        _PropPriority,         //入力時コール爆
        m_psProperty,           //親グリッド
        NULL,                   //更新時コールバック
        (void*)&m_Prop.iTaskPriority); //実データ
    
    pNextItem = pTree->AddNext(pNextItem, 
        pItem, 
        DefPriority.strDspName, 
        pItem->GetName());

    //---------------
    //デバッグモード
    //---------------
    using namespace EXEC_COMMON;

    StdString strDebugList;

    strDebugList  = GetDebugMode2Name(EDB_DEBUG);
    strDebugList += _T(":");  
    strDebugList += boost::lexical_cast<StdString>(EDB_DEBUG);
    strDebugList += _T(",");  

    strDebugList += GetDebugMode2Name(EDB_RELEASE);
    strDebugList += _T(":");  
    strDebugList += boost::lexical_cast<StdString>(EDB_RELEASE);

    CStdPropertyItemDef DefDebug(PROP_DROP_DOWN_ID, 
        GET_STR(STR_PRO_DEBUG_MODE)   ,
        _T("DebugMode"),
        GET_STR(STR_PRO_INFO_DEBUG_MODE), 
        false,                                       
        DISP_NONE,
        static_cast<int>(m_Prop.eDebug),
        strDebugList, 
        0);

    pItem = new CStdPropertyItem( 
        DefDebug, 
        _PropDebugMode, 
        m_psProperty, 
        NULL,
        (void*)&m_Prop.eDebug);
    
    pNextItem = pTree->AddNext(pNextItem, 
        pItem, 
        DefDebug.strDspName, 
        pItem->GetName());

    //---------------
    //自動コンパイル
    //---------------
    CStdPropertyItemDef DefAutoCompile(PROP_BOOL, 
        GET_STR(STR_PRO_AUTO_COMPILE)   , 
        _T("AutoCompile"), 
        GET_STR(STR_PRO_INFO_AUTO_COMPILE), 
        false,                  //Edit許可(PROP_BOOLは入力不可)                     
        DISP_NONE,              //UNIT
        m_Prop.bAutoCompile);   //INIT

    pItem = new CStdPropertyItem(
        DefAutoCompile,         //定義
        _PropAutoCompie,        //入力時コール爆
        m_psProperty,           //親グリッド
        NULL,                   //更新時コールバック
        (void*)&m_Prop.bAutoCompile); //実データ
    
    pNextItem = pTree->AddNext(pNextItem, 
        pItem, 
        DefAutoCompile.strDspName, 
        pItem->GetName());


    //---------------
    //編集許可
    //---------------
    using namespace VIEW_COMMON;
    StdString strEditPermissionList;

    strEditPermissionList  = PermissionToStr(EP_NONE);
    strEditPermissionList += _T(":");  
    strEditPermissionList += boost::lexical_cast<StdString>(EP_NONE);
    strEditPermissionList += _T(",");  

    strEditPermissionList += PermissionToStr(EP_READONRY);
    strEditPermissionList += _T(":");  
    strEditPermissionList += boost::lexical_cast<StdString>(EP_READONRY);


    CStdPropertyItemDef DefEditPermission(PROP_DROP_DOWN_ID, 
        GET_STR(STR_PRO_EDIT_PARMISSION)   , 
        _T("EditPermission"), 
        GET_STR(STR_PRO_INFO_EDIT_PARMISSION), 
        false,                                       
        DISP_NONE,
        static_cast<int>(m_Prop.ePermission),
        strEditPermissionList,
        0);

    pItem = new CStdPropertyItem(
        DefEditPermission, 
        _PropEditPermission, 
        m_psProperty, 
        NULL,
        (void*)&m_Prop.ePermission);
    
    pNextItem = pTree->AddNext(pNextItem, 
        pItem, 
        DefEditPermission.strDspName, 
        pItem->GetName());


    //-----------------
    // プロパティセット
    //-----------------
    std::set<StdString> lstName;
    StdString strList;

    SYS_PROPERTY->GetPropertySetNameList(&lstName);

    strList = _T("NONE");

    bool bFirst = true;
    foreach(StdString name, lstName)
    {
        strList += _T(",");
        strList += name;
    }


    //-----------------
    // プロパティセット
    //-----------------
    CStdPropertyItemDef DefPropSet  (PROP_DROP_DOWN, //型
        GET_STR(STR_PRO_SET_NAME),            //データ表示名
        _T("PropertySetName"),         //名称
        GET_STR(STR_PRO_INFO_SET_NAME),        //データ説明
        false,                                 //編集可、不可
        DISP_NONE,                             //単位
        m_Prop.strPropertySet,                 //初期値
        strList,                               //最小値・リスト設定
        0);                                

    pItem = new CStdPropertyItem(
                                    DefPropSet,                 //定義データ
                                    _PropPropertySetName,       //入力時コールバック
                                    m_psProperty,               //親グリッド
                                    NULL,                       //更新時コールバック
                                    (void*)&m_Prop.strPropertySet);     //実データ

    pNextItem = pTree->AddNext(pNextItem, pItem, 
                                DefPropSet.strDspName, 
                                pItem->GetName());


    return pGroupItem;
}

/**
 *  @brief   プロパティ設定.
 *  @param   [in] pGrid  グリッドコントロールへのポインタ
 *  @param   [in] uiMask オブジェクトコントロールへのポインタ
 *  @retval  なし
 *  @note
 */
void CObjectDef::SetProperty(CMFCPropertyGridCtrl *pGrid, UINT uiMask)
{
    CPropertyGrid* pPropGrid = dynamic_cast<CPropertyGrid*>(pGrid);

    pPropGrid->RemoveAll();

    if (!m_bInitProperty)
    {
        _CreateStdPropertyTree();
        m_bInitProperty = true;
    }

    pPropGrid->SetGridData(m_psProperty);
    m_psProperty->UpdatePropertyGrid(pGrid, false /*bSetVisibleOnly*/);
}


/**
 * @brief   スクリプト初期化
 * @param   なし
 * @retval  true 成功
 * @note	
 */
/*
bool CObjectDef::InitScript()
{
    return true;
}
*/


//!< 表示オブジェクト設定
void CObjectDef::_SetViewObject(CDrawingScriptBase* pObjects)
{
    //所有権移動
    m_psViewObject = pObjects;
}



/**
 * @brief   スクリプト実行
 * @param   なし
 * @retval  true 成功
 * @note	
 */
bool CObjectDef::ExecScripts()
{
#ifdef _DEBUG
    DB_PRINT_TIME(_T("CObjectDef::ExecScripts \n"));
#endif

    bool bRet = false;
    //実行中のスクリプトが無いかチェック
    E_EXEC_STS eSts = GET_APP_MODE();
    if (eSts == EX_BREAK)
    {
       bRet = THIS_APP->GetExecCtrl()->Resume();
    }
    else if (eSts == EX_EDIT)
    {
        bRet = _SetExecute();
    }

    return bRet;
}

/**
 * @brief   スクリプト生成実行
 * @param   なし
 * @retval  true 成功
 * @note	
 */
bool CObjectDef::_SetExecute()
{
    // TODO:コンパイル済み確認
    // UI側でも行っておくこと


    // CPartsDefはオーバライドしているはず
    STD_ASSERT((m_Prop.eObjType  !=  VIEW_COMMON::E_PARTS) &&
               (m_Prop.eObjType  !=  VIEW_COMMON::E_FIELD));

    auto pExecBase = CreateInstance(NULL);
    pExecBase->SetName(this->GetName().c_str());

    CExecCtrl* pCtrl;
    pCtrl = THIS_APP->GetExecCtrl();

    pCtrl->InitScript();
    pCtrl->AddScriptLoop(pExecBase.get());

    bool bRet;
    bRet = pCtrl->StartScript();
    return bRet;
}

/**
 * @brief   スクリプト停止
 * @param   なし
 * @retval  true 成功
 * @note	
 */
/*
bool CObjectDef::AbortScripts()
{
    if(!CHK_APP_MODE(MODE_ABORTING, this))
    {
        return false;
    }

    bool bRet;
    STD_ASSERT(m_psExecCtrl != NULL);

    bRet = m_psExecCtrl->AbortAll();

    return bRet;
}
*/

/**
 * @brief   スクリプト一時停止
 * @param   なし
 * @retval  true 成功
 * @note	
 */
/*
bool CObjectDef::PauseScripts()
{
    bool bRet;
    STD_ASSERT(m_psExecCtrl != NULL);
    bRet = m_psExecCtrl->PauseAll();
    return bRet;
}
*/

/**
 * @brief   スクリプト数取得
 * @param   なし   
 * @retval  スクリプト数
 * @note	
 */
int CObjectDef::GetScriptNum() const
{
    if(m_psScript) 
    {
        return m_psScript->GetScriptNum();
    }
    return 0;
}

/**
 * @brief   スクリプト取得
 * @param   [in] スクリプト位置   
 * @retval  スクリプトへのポインタ
 * @note	
 */
CScriptData* CObjectDef::GetScriptInstance(int nPos) const
{
    STD_ASSERT(nPos >= 0);
    STD_ASSERT(nPos < GetScriptNum());
    if(m_psScript) 
    {
        return m_psScript->GetScript(nPos);
    }
    return NULL;
}


/**
 * @brief   スクリプト取得
 * @param   [in] スクリプト名
 * @retval  スクリプトへのポインタ
 * @note	
 */
CScriptData* CObjectDef::GetScriptInstance(StdString  strName)
{
    if(m_psScript) 
    {
        return m_psScript->GetScript(strName);
    }
    return NULL;
}


/**
 * @brief   スクリプト位置取得
 * @param   [in] スクリプト名
 * @retval  スクリプト位置
 * @note	
 */
int CObjectDef::GetScriptPos(StdString  strName)
{
    if(!m_psScript) 
    {
        return -1;
    }
    CScriptData* pData;
    int iMax = m_psScript->GetScriptNum();

    for (int iPos = 0; iPos < iMax; iPos++)
    {
        pData = m_psScript->GetScript(iPos);
        if (pData)
        {
            if(pData->GetName() == strName)
            {
                return iPos;
            }
        }
    }
    return -1;
}

/**
 * @brief   スクリプト名変更
 * @param   [in] strCurrent 現在のスクリプト名
 * @param   [in] strNew     変更するスクリプト名
 * @retval  true 成功
 * @note	
 */
bool CObjectDef::ChangeScriptName(StdString  strCurrent, StdString  strNew)
{
    CScriptData* pScript = GetScriptInstance(strCurrent);
    if (pScript == NULL)
    {
        STD_DBG(_T("GetScriptInstance %s"), strCurrent.c_str());
        return false;
    }

    if (!pScript->SetName(strNew))
    {
        STD_DBG(_T("SetName %s"), strNew.c_str());
        return false;
    }
    return true;
}

/**
 * @brief   スクリプト生成
 * @param   [in] スクリプト名
 * @retval  true 生成成功
 * @note	
 */
bool CObjectDef::CreateScript(StdString  strName)
{
    //同じ名称があるか調べる
    if (GetScriptInstance(strName) != NULL)
    {
        MockException(e_same_name);
        return false;
    }

    if (!CScript::CheckScriptName(strName) )
    {
        //使用できない文字が含まれている
        MockException(e_no_use_char);
        return false;
    }

    if(!m_psScript)
    {
        return false;
    }


    bool bRet;

    if (m_psScript->GetScriptNum() == 0)
    {
        bRet = m_psScript->SetScript(strName, SYS_CONFIG->strStdScript);
    }
    else
    {
        bRet = m_psScript->SetScript(strName, SYS_CONFIG->strExpScript);
    }

    return bRet;
}

/**
 * @brief   スクリプト削除
 * @param   [in] スクリプト名
 * @retval  true 削除成功
 * @note	
 */
bool CObjectDef::DeleteScript(StdString  strName)
{
    if(!m_psScript)
    {
        return false;
    }

    bool bRet;
    bRet = m_psScript->DelScript(strName);
    return bRet;
}

/**
 * @brief   スクリプトタブ名分割
 * @param   [out] pScriptName  スクリプト名
 * @param   [out] pPartsName   部品名
 * @param   [in]  strTabNmae スクリプトタブ名称
 * @retval  true 成功
 * @note	
 */
bool CObjectDef::DivideScriptTabName(StdString* pScriptName, 
                                     StdString* pStrParts,
                                     StdString strTabNmae)
{
    using namespace boost::xpressive;
    namespace xpr = boost::xpressive;

    //スクリプト名([LIB:]生成オブジェクト名)
    //  (.+)\((.+)\)  
    bool bRet;
    int iMatchSize;
    StdStrXMatchResult matchResult;

    StdStrXregex static_mix  =  (s1 = +_) >> _T('(') >> (s2 = +_) >> _T(')') ;

    bRet = regex_match( strTabNmae , matchResult, static_mix);
    iMatchSize =  SizeToInt(matchResult.size());

    if (!bRet)
    {
        return false;
    }

    if (iMatchSize != 3 )
    {
        return false;
    }

    StdString strTmp;
    strTmp = matchResult.str(1);
    CUtil::Trim(strTmp);
    if (strTmp.size() > 0)
    {
        if (strTmp.at(0) == _T('*'))
        {
            strTmp = strTmp.substr(1); 
        }
    }
   
    *pScriptName = strTmp;


    strTmp = matchResult.str(2);
    CUtil::Trim(strTmp);

    *pStrParts = strTmp; 

    /* LIB:XXXXXの分割はおこなわない
    CUtil::TokenizeCsv(pListPartsName, strTmp, _T(':'));

    if (pListPartsName.size() > 2)
    {
        return false;
    }

    if (pListPartsName.size() == 0)
    {
        return false;
    }
    */

    return true;
}

/**
 *  @brief   実行スレッド種別ディフォルト値設定
 *  @param   [in] crObj  実行スレッド種別ディフォルト値
 *  @retval  なし
 *  @note
 */
void CObjectDef::SetDefaultExecThreadType(EXEC_COMMON::E_THREAD_TYPE eExecThreadType)
{
    m_Prop.strThreadType = EXEC_COMMON::GetThreadType2Name(eExecThreadType);
}

/**
 *  @brief   実行スレッド種別ディフォルト値取得
 *  @param   なし
 *  @retval  実行スレッド種別ディフォルト値
 *  @note
 */
EXEC_COMMON::E_THREAD_TYPE CObjectDef::GetDefaultExecThreadType() const
{
    EXEC_COMMON::E_THREAD_TYPE eType;
    eType = EXEC_COMMON::GetThreadName2Type(m_Prop.strThreadType);
    return eType;
}

/*
 *  @brief   プロパティ変更(実行スレッド種別)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CObjectDef::_PropThreadType   (CStdPropertyItem* pData, void* pObj)
{
    using namespace EXEC_COMMON;
    CObjectDef* pDef = reinterpret_cast<CObjectDef*>(pObj);
    try
    {
        StdString strThreadType = pData->anyData.GetString();

        E_THREAD_TYPE eType = GetThreadName2Type( strThreadType);
        pDef->SetDefaultExecThreadType(eType);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/*
 *  @brief   プロパティ変更(実行スレッド種別)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CObjectDef::_PropDebugMode   (CStdPropertyItem* pData, void* pObj)
{
    using namespace EXEC_COMMON;
    CObjectDef* pDef = reinterpret_cast<CObjectDef*>(pObj);
    try
    {
        //pDrawing->AddChgCnt();
        int iType = pData->anyData.GetInt();

        DEBUG_MODE eType = static_cast<DEBUG_MODE>( iType);
        pDef->_SetDebugMode(eType);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/*
 *  @brief   プロパティ変更(実行サイクルタイム)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CObjectDef::_PropCycleTime   (CStdPropertyItem* pData, void* pObj)
{
    using namespace EXEC_COMMON;
    CObjectDef* pDef = reinterpret_cast<CObjectDef*>(pObj);
    try
    {
        int iTime = pData->anyData.GetInt();
        if (iTime < 0)
        {
            iTime = 0;
        }
        pDef->_SetCycleTime(iTime);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}


/*
 *  @brief   プロパティ変更(実行プライオリティ)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CObjectDef::_PropPriority   (CStdPropertyItem* pData, void* pObj)
{
    using namespace EXEC_COMMON;
    CObjectDef* pDef = reinterpret_cast<CObjectDef*>(pObj);
    try
    {
        int iPriority = pData->anyData.GetInt();
        if (iPriority < 1){return false;}
        if (iPriority > 5){return false;}

        pDef->_SetPriority(iPriority);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}


/*
 *  @brief   プロパティ変更(自動コンパイル)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CObjectDef::_PropAutoCompie   (CStdPropertyItem* pData, void* pObj)
{
    using namespace EXEC_COMMON;
    CObjectDef* pDef = reinterpret_cast<CObjectDef*>(pObj);
    try
    {
        bool bType = pData->anyData.GetBool();
        pDef->_SetAutoCompile(bType);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}
    
/*
 *  @brief   プロパティ変更(編集許可)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CObjectDef::_PropEditPermission (CStdPropertyItem* pData, void* pObj)
{
   using namespace VIEW_COMMON;
    CObjectDef* pDef = reinterpret_cast<CObjectDef*>(pObj);
    try
    {
        int iType = pData->anyData.GetInt();

        EDIT_PERMISSION eType = static_cast<EDIT_PERMISSION>( iType);
        pDef->_SetEditPermission(eType);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}


/**
 *  @brief   プロパティ変更(プロパティセット)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CObjectDef::_PropPropertySetName  (CStdPropertyItem* pData, void*pObj)
{
    CObjectDef* pDrawing = reinterpret_cast<CObjectDef*>(pObj);
    try
    {
        std::vector<StdString> lstStr;

        StdString strProp = pData->anyData.GetString();

        if (pDrawing->GetPropertySetName() != strProp)
        {
            pDrawing->_SetPropertySetName(strProp);
        }
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
    }
    return true;

}

/**
 * @brief   IO定義取得
 * @param   なし
 * @retval  IO定義
 * @note	
 */
CIoDef* CObjectDef::GetIoDef()
{
    return m_psIoDef;
}



/**
 * @brief   コンパイル問い合わせ
 * @param   [out] pLstFileName 
 * @retval  true 成功
 * @note	
 */
bool CObjectDef::IsCompiled( std::vector<StdString>* pLstFileName) const
{
    COMPILE_STS stsCompile;
    stsCompile = m_psModule->GetCompileStatus();

    if ( (stsCompile == CS_SUCCESS) ||
            (stsCompile == CS_SUCCESS_D))
    {
        return true;
    }

    CScriptData* pData;
    int iScriptNum = GetScriptNum();

    for (int iCnt = 0; iCnt < iScriptNum; iCnt++)
    {
        pData = GetScriptInstance(iCnt);
        if (pData->GetName() == _T("_INITILIZE_SCRIPT_"))
        {
            continue;
        }

        if (pData->IsChange())
        {
            StdPath path;
            path = pData->GetCurrentPath();
            StdString strFineName = path.filename().c_str();
            pLstFileName->push_back(strFineName);
        }
    }
    return false;
}



/**
 *  @brief  構築
 *  @param  [in] pScriptBase 
 *  @param  [in] bDebug   true:デバッグモード
 *  @retval true 成功
 *  @note
 */
bool CObjectDef::Compile( bool bDebug, bool bRebuild)
{
    bool bCompile = true;
    bool bRet = true;

    if (!bRebuild)
    {
        COMPILE_STS stsCompile;
        stsCompile = m_psModule->GetCompileStatus();

        if ( (stsCompile == CS_SUCCESS) ||
             (stsCompile == CS_SUCCESS_D))
        {
            bCompile = false;
        }
    }

    if (bCompile)
    {
        bRet = m_psModule->Compile(bDebug);
    }
    return bRet;
}
/**
 *  @brief  変更の有無
 *  @param  なし
 *  @retval true 変更のあり
 *  @note   変更ありの場合はSaveを行う
 */
bool CObjectDef::IsChange() const
{
    if(m_psUndoAction)
    {
        if(!m_psUndoAction->IsUndoEmpty())
        {
            return true;
        }
    }


    if (m_psPorpUser)
    {
        //ユーザー定義データ
        if(m_psPorpUser->GetChgCnt() != 0)
        {
            return true;
        }

        if(m_psPorpUser->IsChgEachItem())
        {
            return true;
        }
    }

    //プロパティ
    if (m_OrgProp != m_Prop)
    {
        return true;
    }

    if(m_psIoDef)
    {
        //IO
        if (m_psIoDef->IsChange())
        {
            return true;
        }
    }
    return false;
}

/**
 *  @brief  コンパイル必要性問い合わせ
 *  @param  なし
 *  @retval コンパイルステータス
 *  @note
 */
COMPILE_STS CObjectDef::GetCompileStatus() const
{
    COMPILE_STS eRet;
    if (m_Prop.eObjType  == VIEW_COMMON::E_REFERENCE)
    {
        return CS_SUCCESS;
    }

    eRet =  m_psModule->GetCompileStatus();
    return eRet;
}

/**
 *  @brief  プロパティ取得
 *  @param  なし
 *  @retval プロパティ
 *  @note
 */
CProperty* CObjectDef::GetProperty()
{
    return &m_Prop;
}

/**
 *  @brief  構築
 *  @param  [in] pDefl 
 *  @param  [in] bDebug   true:デバッグモード
 *  @param  [in] bRebuild true:強制リビルト
 *  @param  [in] bCompile true:コンパイル
 *  @retval true 成功
 *  @note   bCompileは、CPartsDefで再帰的にコンパイルを行う
 */
bool CObjectDef::Build( bool bRebuild )
{
    if (m_Prop.eObjType  == VIEW_COMMON::E_REFERENCE)
    {
        return true;
    }


    m_bAbortBuild = false;

    //TODO: コンパイルに必要なテキストのみセーブする
    THIS_APP->SaveAllTextToScript();

    //STR_MB_BUIDLD_START < ------ ビルド開始 %s-----\n
    PrtintOut(WIN_BUILD, GET_STR(STR_MB_BUIDLD_START), m_Prop.strName.c_str());
    

    bool bDebug = false;
    if ( m_Prop.eDebug == EXEC_COMMON::EDB_DEBUG)
    {
        bDebug = true;
    }

    _BuildThread(this, bDebug, bRebuild);

    /*
    boost::thread thBuild( 
        boost::bind(&_BuildThread, this, m_Prop.bDebug, bRebuild));
    */

    return true;
}

/**
 *  @brief  ビルドスレッド
 *  @param  [in] pCtrl 
 *  @param  [in] bDebug   true:デバッグモード
 *  @retval なし
 *  @note
 */
void CObjectDef::_BuildThread( CObjectDef* pObjDef, 
                             bool bDebug,
                             bool bRebuild)
{
    bool bRet;
    STD_ASSERT(pObjDef);

    bRet = pObjDef->Compile(bDebug, bRebuild);

    if (bRet)
    {
        PrtintOut(WIN_BUILD, GET_STR(STR_MB_BUIDLD_SUCCESS));
    }
    else
    {
        PrtintOut(WIN_BUILD, GET_STR(STR_MB_BUIDLD_FAILURE));

    }
     ::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_CHG_OBJECT_STS,
                            (WPARAM)0, (LPARAM)0);
}

/**
 *  @brief  ビルド停止
 *  @param  なし
 *  @retval true 成功
 *  @note
 */
void CObjectDef::AbortBuild()
{
    m_bAbortBuild = true;
}

/**
 *  @brief  モジュール取得
 *  @param  なし
 *  @retval モジュールへのポインタ
 *  @note
 */
CModule* CObjectDef::GetModule()
{
    return (m_psModule);
}


/**
 * @brief   ファイル書き込みモード設定
 * @param   [in] bSingle 
 * @retval  なし
 * @note    書き込み、読み込み前に設定する
 */
void CObjectDef::SetSingleFileMode(bool bSingle)
{
    m_Prop.bSingleFile = bSingle;
}


/**
 * @brief   ファイル読み込み
 * @param   Path ファイルへのパス
 * @retval  true 読み込み成功
 * @note	
 */
bool CObjectDef::Load(StdPath Path)
{
    StdString strError;
    if (!boost::filesystem::exists(Path))
    {
        strError = GET_ERR_STR(e_file_exist);
        STD_DBG(strError.c_str()); 
        return false;
    }

    _SetCurrentPath(Path.parent_path());
    StdString strExt = GetPathStr(Path.extension());
    strExt = CUtil::ToUpper(strExt);
    bool bRet = true;

    if (strExt == _T(".MSD") )
    {
        boost::filesystem::ifstream inFs(Path, std::ios::binary);
        try
        {
            {
                boost::archive::binary_iarchive inBin(inFs);
                inBin >> *this;
            }
            inFs.close();
        }
        catch(std::exception& e )
        {
            inFs.close();
            strError = MockException::FileErrorMessage(true, Path, 
                                      CUtil::CharToString(e.what()));
            bRet = false;
        }
        catch(...)
        {
            inFs.close();
            strError = MockException::FileErrorMessage(true, Path);
            bRet = false;
        }
    }
	if (strExt == _T(".MSDT"))
	{
		StdStreamIn  inFs(Path);
		try
		{
			{
				boost::archive::text_wiarchive txtIn(inFs);
				txtIn >> *this;
			}
			inFs.close();
		}
		catch (std::exception& e)
		{
			inFs.close();
			strError = MockException::FileErrorMessage(true, Path,
				CUtil::CharToString(e.what()));
			bRet = false;
		}
		catch (...)
		{
			inFs.close();
			strError = MockException::FileErrorMessage(true, Path);
			bRet = false;
		}
	}
	else if (strExt == _T(".MSDX") )
    {
        StdStreamIn inFs(Path);
        try
        {
            {
                StdXmlArchiveIn inXml(inFs);
                inXml >> boost::serialization::make_nvp("Root", *this);
            }
            inFs.close();
        }
        catch(std::exception& e )
        {
            inFs.close();
            strError = MockException::FileErrorMessage(true, Path, 
                                      CUtil::CharToString(e.what()));
            bRet = false;
        }
        catch(...)
        {
            inFs.close();
            strError = MockException::FileErrorMessage(true, Path);
            bRet = false;
        }
    }
    else
    {
        StdString strExtErr;
        strExtErr =  GET_ERR_STR(e_file_ext);
        strExtErr += _T("(");
        strExtErr += strExt;
        strExtErr += _T(")");
        strError = MockException::FileErrorMessage(true, Path, strExtErr);
        bRet = false;
    }

    if (!bRet)
    {
        STD_DBG(strError.c_str()); 
    }

//



//


    return bRet;
}

/**
 * @brief   ファイル保存
 * @param   Path ファイルへのパス
 * @retval  true 保存み成功
 * @note	
 */
bool CObjectDef::Save(StdPath Path)
{
    StdPath pathOld = m_pathCurrent;
    _SetCurrentPath(Path.parent_path());

    StdString strExt = GetPathStr(Path.extension());
    strExt = CUtil::ToUpper(strExt);
    StdString strError;
    bool bRet = true;
    if (strExt == _T(".MSD") )
    {
        boost::filesystem::ofstream outFs(Path, std::ios::binary);
        try
        {
            SetSingleFileMode();
            {
                boost::archive::binary_oarchive outBin(outFs);
                outBin << *this;
            }
            outFs.close();
        }
        catch(std::exception& e )
        {
            //ファイル%sの書き込みに失敗しました;
            outFs.close();
            strError = MockException::FileErrorMessage(false, Path, 
                                      CUtil::CharToString(e.what()));
            bRet = false;
        }
        catch(...)
        {
            outFs.close();
            strError = MockException::FileErrorMessage(false, Path);
            bRet = false;
        }

    }
	if (strExt == _T(".MSDT"))
	{
		StdStreamOut outFs(Path);
		try
		{
			SetSingleFileMode();
			{
				boost::archive::text_woarchive txtOut(outFs);
				txtOut << *this;
			}
			outFs.close();
		}
		catch (std::exception& e)
		{
			//ファイル%sの書き込みに失敗しました;
			outFs.close();
			strError = MockException::FileErrorMessage(false, Path,
				CUtil::CharToString(e.what()));
			bRet = false;
		}
		catch (...)
		{
			outFs.close();
			strError = MockException::FileErrorMessage(false, Path);
			bRet = false;
		}

	}
	else if (strExt == _T(".MSDX") )
    {
        StdStreamOut outFs(Path);
        try
        {
            SetSingleFileMode(false);
            {
                StdXmlArchiveOut outXml(outFs);
                outXml << boost::serialization::make_nvp("Root", *this);
            }
            outFs.close();
        }
        catch(std::exception& e )
        {
            outFs.close();
            strError = MockException::FileErrorMessage(false, Path, 
                                      CUtil::CharToString(e.what()));
            bRet = false;
        }
        catch(...)
        {
            outFs.close();
            strError = MockException::FileErrorMessage(false, Path);
            bRet = false;
        }
    }
    else
    {
        StdString strExtErr;
        strExtErr =  GET_ERR_STR(e_file_ext);
        strExtErr += _T("(");
        strExtErr += strExt;
        strExtErr += _T(")");
        strError = MockException::FileErrorMessage(false, Path, strExtErr);
        bRet = false;
    }

    if (!bRet)
    {
        STD_DBG(strError.c_str()); 
        _SetCurrentPath(pathOld);
    }
    return bRet;
}

/**
 * @brief   シリアル化(ロード)
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  なし
 * @note	
 */
template<class Archive>
void CObjectDef::load(Archive& ar, const unsigned int version)
{
    SERIALIZATION_INIT
        if(m_psScript)
        {
            delete m_psScript;
            m_psScript = NULL;
        }

        if(!m_psIoDef)
        {
			m_psIoDef = new CIoDef();
		}

        if (!m_psPorpUser)
        {
			m_psPorpUser = new CPropertySet();
		}

        SERIALIZATION_LOAD("Property" ,  m_Prop);
        SERIALIZATION_LOAD("Io"       ,  *m_psIoDef);
        SERIALIZATION_LOAD("UserData" ,  *m_psPorpUser);

        if(m_Prop.eObjType  != VIEW_COMMON::E_REFERENCE)
        {
			m_psScript = new CScript();
			if (GetProject())
            {
                StdPath pathProj(GetProject()->GetDir());
                m_pathCurrent = pathProj / m_Prop.strName;
				m_psScript->SetCurrentPath(GetCurrentPath());
            }
            SERIALIZATION_LOAD("Script"   ,   *m_psScript);
            m_psIoDef->SetParent(this);
        }

        m_iChgPropUserCnt = 0;
        m_OrgProp = m_Prop;
        m_psUndoAction->Clear();
        LoadAfter();
   MOCK_EXCEPTION_FILE(e_file_read);
}


/**
 * @brief   シリアル化(セーブ)
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  なし
 * @note	
 */
template<class Archive>
void CObjectDef::save(Archive& ar, const unsigned int version) const
{
    //セーブ開始を通知
    SERIALIZATION_INIT

        SERIALIZATION_SAVE("Property" ,  m_Prop);
        SERIALIZATION_SAVE("Io"       ,  *m_psIoDef);
        SERIALIZATION_SAVE("UserData" ,  *m_psPorpUser);


        if(m_Prop.eObjType  != VIEW_COMMON::E_REFERENCE)
        {
            if(m_psScript)
            {
                if (GetProject())
                {
                    StdPath pathProj(GetProject()->GetDir());
                    m_pathCurrent = pathProj / StdPath(m_Prop.strName);
                    m_psScript->SetCurrentPath(m_pathCurrent);
                }
                m_psScript->SetSingleFileMode(m_Prop.bSingleFile);
				SERIALIZATION_SAVE("Script"   ,  *m_psScript);
			}
		}

        m_OrgProp = m_Prop;
        m_psUndoAction->Clear();

    MOCK_EXCEPTION_FILE(e_file_write);
}

/**
 * @brief   デバッグ用文字出力
 * @param   なし
 * @retval  なし
 * @note	
 */
void CObjectDef::Print() const
{
    DB_PRINT(_T("CObjectDef %I64x \n"), this);
    m_Prop.Print();
    DB_PRINT(_T("CIoDef:%I64x CScript:%I64x \n"), m_psIoDef, m_psScript);
    DB_PRINT(_T("PorpUser:%I64x Module:%I64x \n"), m_psPorpUser, m_psModule);
    DB_PRINT(_T("bCopy:%d \n"), m_bCopy);


}


//load save のインスタンスが生成されないため
//ダミーとして実装
void CObjectDef::Dummy()
{
    unsigned int iVersion = 0;

    StdStreamOut outFs(_T(""));
    StdStreamIn  inFs(_T(""));

    boost::archive::text_woarchive txtOut(outFs); 
    boost::archive::text_wiarchive txtIn(inFs);

    save<boost::archive::text_woarchive>( txtOut, iVersion);
    load<boost::archive::text_wiarchive>( txtIn,  iVersion);


    StdXmlArchiveOut outXml(outFs);
    StdXmlArchiveIn inXml(inFs);

    save<StdXmlArchiveOut>(outXml, iVersion);
    load<StdXmlArchiveIn>(inXml, iVersion);


    boost::filesystem::ofstream outFsStd(_T(""));
    boost::filesystem::ifstream inFsStd(_T(""));

    boost::archive::binary_oarchive outBin(outFsStd);
    boost::archive::binary_iarchive inBin(inFsStd);

    save<boost::archive::binary_oarchive>(outBin, iVersion);
    load<boost::archive::binary_iarchive>(inBin, iVersion);
}

//*******************************
//             TEST
//*******************************
#ifdef _DEBUG

extern void TEST_CIoDef_SetData(CIoDef& ioDef);


void TEST_CObjectDef()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    /*
    CDrawingScriptBase* pScriptbase;
    CObjectDef objDef(_T("TestName"));


    STD_ASSERT(objDef.GetName() == _T("TestName") );


    //ユーザ定義

    


    //IO
    CIoDef  ioDef;
    CIoDef* pIoDef = objDef.GetIoDef();
    TEST_CIoDef_SetData(*pIoDef);
    TEST_CIoDef_SetData(ioDef);
    */


    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}
#endif //_DEBUG