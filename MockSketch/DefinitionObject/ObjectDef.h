/**
 * @brief			ObjectDefヘッダーファイル
 * @file			ObjectDef.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *	ObjectDef クラスのインターフェイス
 *
 * $
 * $
 * 
 */

#ifndef OBJECT_DEF_H__
#define OBJECT_DEF_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DrawingObject/BaseObj.h"
#include "Utility/CStdPropertyTree.h"
#include "ExecCommon.h"
#include "View/ViewCommon.h"
#include "View/DRAWING_TYPE.h"
#include "COMMON_HEAD.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/

/*---------------------------------------------------*/
/*  class                                            */
/*---------------------------------------------------*/
class CScript;
class CScriptData;
class CIoDef;
class CDrawingScriptBase;
class CPartsDef;
class CProjectCtrl;
class CUndoAction;
class CPropertySet;
class CModule;
class CDrawingParts;
struct CStdPropertyItemDef;
enum   E_PROPERTY_ITEM_SET;

class CProperty
{
public:
    //!< 名称
    StdString                               strName;

    //タグ
    std::vector<StdString>                  lstTag;

    //!< 定義オブジェクト種別
    VIEW_COMMON::E_OBJ_DEF_TYPE             eObjType;

    //!< 部品ID
    boost::uuids::uuid                      uuidParts;

    //!< 実行スレッド種別ディフォルト値
    StdString                               strThreadType;

    //!< 実行サイクルタイムディフォルト値
    DWORD                                   dwCycleTime;

    //!< 実行プライオリティディフォルト値
    int                                     iTaskPriority;

    //!< デバッグモード
    EXEC_COMMON::DEBUG_MODE                 eDebug;

    //自動コンパイル
    bool                                    bAutoCompile;

    //!< 単一ファイルモード
    bool                                    bSingleFile;

    //編集許可
    VIEW_COMMON::EDIT_PERMISSION            ePermission;

    //!< プロパティセット名
    StdString                               strPropertySet;

public:
    CProperty():
        eObjType(VIEW_COMMON::E_NONE),
        strThreadType(EXEC_COMMON::GetThreadType2Name(EXEC_COMMON::TH_NONE)),
        dwCycleTime(100),
        iTaskPriority(3),
        eDebug(EXEC_COMMON::EDB_DEBUG),
        bSingleFile(false),
        bAutoCompile(true),
        ePermission(VIEW_COMMON::EP_NONE),
        strPropertySet(_T("NONE")){;}

    CProperty(StdString strName):
        strName      (strName),
        eObjType     (VIEW_COMMON::E_NONE),
        strThreadType(EXEC_COMMON::GetThreadType2Name(EXEC_COMMON::TH_NONE)),
        dwCycleTime(100),
        iTaskPriority(3),
        eDebug       (EXEC_COMMON::EDB_DEBUG),
        bSingleFile(false),
        bAutoCompile(true),
        ePermission(VIEW_COMMON::EP_NONE),
        strPropertySet(_T("NONE"))
        {;}

    virtual ~CProperty(){;}


public:
    CProperty& operator = (const CProperty & m)
    {
        strName         = m.strName;
        eObjType        = m.eObjType;
        uuidParts       = m.uuidParts;
        eDebug          = m.eDebug;
        strThreadType   = m.strThreadType;
        dwCycleTime     = m.dwCycleTime;
        iTaskPriority   = m.iTaskPriority;
        bSingleFile     = m.bSingleFile;
        bAutoCompile    = m.bAutoCompile;
        lstTag          = m.lstTag;
        ePermission     = m.ePermission;
        strPropertySet  = m.strPropertySet;
        return *this;
    }


    bool operator == (const CProperty & m) const
    {
        if(strName       != m.strName)       { return false;}
        if(eObjType      != m.eObjType)      { return false;}
        if(uuidParts     != m.uuidParts)     { return false;}
        if(eDebug        != m.eDebug)        { return false;}
        if(strThreadType != m.strThreadType) { return false;}
        if(dwCycleTime   != m.dwCycleTime)   { return false;}
        if(iTaskPriority != m.iTaskPriority) { return false;}

        if(bSingleFile   != m.bSingleFile)   { return false;}
        if(bAutoCompile  != m.bAutoCompile)   { return false;}

        if(lstTag        != m.lstTag)        { return false;}
        if(ePermission   != m.ePermission)   { return false;}
        if(strPropertySet   != m.strPropertySet)   { return false;}


        return true;
    }

    bool operator != (const CProperty & m) const
    {
        return !(*this == m);
    }

    void SetTagStr(const StdString& strTag);

    StdString GetTagStr() const;

    void Print() const;

private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
            StdString strTag;
            int iAutoCompile;
            if ( !Archive::is_loading::value)
            {
                strTag = GetTagStr();
                iAutoCompile = static_cast<int>(bAutoCompile);
            }

            SERIALIZATION_BOTH("Name"   , strName);
            SERIALIZATION_BOTH("Type"   , eObjType);
            SERIALIZATION_BOTH("Tags"   , strTag  );
            SERIALIZATION_BOTH("ID"     , uuidParts);
            SERIALIZATION_BOTH("DefaultThread" , strThreadType);
            SERIALIZATION_BOTH("DefaultCycleTime" , dwCycleTime);
            SERIALIZATION_BOTH("DefaultPriority"  , iTaskPriority);
            SERIALIZATION_BOTH("Debug"         , eDebug);
            SERIALIZATION_BOTH("SingleFile"    , bSingleFile);
            SERIALIZATION_BOTH("AutoCompile"   , iAutoCompile);
            SERIALIZATION_BOTH("Parmission"    , ePermission);
            SERIALIZATION_BOTH("PropertySet"   , strPropertySet);

            if ( Archive::is_loading::value)
            {
                bAutoCompile  = ((iAutoCompile != 0) ? true : false);
                SetTagStr(strTag);
            }

        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )

    }
};


/**
 * @class   CObjectDef
 * @brief   スクリプト,描画,IO定義を保持するクラス
 */
class CObjectDef:   public CBaseObj
{
friend CObjectDef;
    

public:
    CObjectDef();

    CObjectDef(const CObjectDef& pObj);

    CObjectDef(StdString strName);

    CObjectDef(const CObjectDef& pObj, StdString strName);

    virtual ~CObjectDef();

    static std::shared_ptr<CObjectDef> Clone(CObjectDef* pObject, StdString strName);

    virtual void CreateTmpCopy(CObjectDef* pDef);

    bool Update();

    virtual void Draw(){;}

    VIEW_COMMON::E_OBJ_DEF_TYPE GetObjectType() const {return m_Prop.eObjType;}

    //!< スクリプトオブジェクトの生成
    virtual std::shared_ptr<CDrawingScriptBase> CreateInstance(CObjectDef* pCtrl){return NULL;}

    //!< 初期化スクリプト生成
    virtual bool CreateInitialScript();


    //-----------------------
    // オブジェクト関連
    //-----------------------

    //!< 名称取得
    StdString GetName() const;

    //!< 名称設定
    bool SetName(StdString strName, bool bManual = true);

    //!< タグ追加
    bool AddTag(StdString strName);

    //!< タグリスト取得
    bool GetTagList(std::vector<StdString>* pListTag) const;

    //!< タグリスト設定
    bool SetTagList(const std::vector<StdString>& listTag);

    //!< タグ判定
    bool IsIncludeTag(StdString strTag);

    //!< 部品ID取得
    boost::uuids::uuid GetPartsId() const;

    //!< プロジェクト設定
    void SetProject(CProjectCtrl* pProject);

    //!< プロジェクト取得
    CProjectCtrl* GetProject() const;

    //!< プロパティセット名
    StdString GetPropertySetName()const {return m_Prop.strPropertySet;}

    virtual bool Disassemble(std::shared_ptr<CDrawingParts> pParts, bool bSetUndo){ return false;}

    //----------------
    // プロパティ
    //----------------
    CProperty* GetProperty();

    void SetDefaultExecThreadType(EXEC_COMMON::E_THREAD_TYPE eType);

    EXEC_COMMON::E_THREAD_TYPE GetDefaultExecThreadType() const;

    //!< プロパティ設定
    void SetProperty(CMFCPropertyGridCtrl *pGrid, UINT uiMask);

    //----------------------
    // プロパティアクセッサ
    //----------------------

    //!< デバッグモード取得
    EXEC_COMMON::DEBUG_MODE GetDebugMode() const {return m_Prop.eDebug;}

    //!< デバッグモード取得
    bool IsAutoCompile() const {return m_Prop.bAutoCompile;}

    //!< 編集許可取得
    VIEW_COMMON::EDIT_PERMISSION GetEditPermession() const {return m_Prop.ePermission;}
    
    //!< 表示オブジェクト取得
    virtual std::shared_ptr<CDrawingScriptBase> GetViewObject(){ return NULL;} 

protected:
    //!< デバッグモード設定
    void _SetDebugMode(EXEC_COMMON::DEBUG_MODE eDebug){m_Prop.eDebug = eDebug;}

    //!< サイクルタイム設定
    void _SetCycleTime(DWORD bAswCycletime){m_Prop.dwCycleTime = bAswCycletime;}

    //!< プライオリティ設定
    void _SetPriority(int iPriority){m_Prop.iTaskPriority = iPriority;}

    //!< 自動コンパイル設定
    void _SetAutoCompile(bool bAuto){m_Prop.bAutoCompile = bAuto;}


    //!< 編集許可設定
    void _SetEditPermission(VIEW_COMMON::EDIT_PERMISSION ePermission){m_Prop.ePermission = ePermission;}

    //!< プロパティセット名
    void _SetPropertySetName(StdString strPropertySet){m_Prop.strPropertySet = strPropertySet;}

    //!< 表示オブジェクト設定
    //!< _SetExecute 内の
    virtual void _SetViewObject(CDrawingScriptBase* pObjects);

public:

     //-------------------
    // スクリプト処理
    //-------------------
    virtual bool ExecScripts();

    /* CExecCtrlに移行 
    virtual bool InitScript();

    virtual bool AbortScripts();

    virtual bool PauseScripts();
    */

    //----------------  
    // スクリプト関連
    //----------------

    //!< スクリプト数取得
    int GetScriptNum() const;
    
    //!< スクリプト取得(位置)
    CScriptData* GetScriptInstance(int nPos) const;

    //!< スクリプト取得(名称)
    CScriptData* GetScriptInstance(StdString  strName);

    //!< スクリプト位置取得
    int  GetScriptPos(StdString  strName);

    //!< スクリプト名変更
    bool ChangeScriptName(StdString  strOld, StdString  strNew);

    //!< スクリプト生成
    bool CreateScript(StdString  strName);

    //!< スクリプト削除
    bool DeleteScript(StdString  strName);

    //!< タブ名分割
    static bool DivideScriptTabName(StdString* pScriptName,
                                    StdString* pStrParts,
                                    StdString strTabNmae);


    //----------------
    // ユーザ定義
    //----------------

 

    //----------------
    // IO
    //----------------
    //!< IO定義取得
    CIoDef* GetIoDef();

    //----------------
    // AS
    //----------------
	void AddRef() {m_iRefCount++;}
	void Release() {if( --m_iRefCount == 0 ) delete this;}

    //----------------
    // その他
    //----------------
    //!< ロード終了後処理
    virtual void LoadAfter(){;}

    //-----------------------
    // 問い合わせ
    //-----------------------
    //!< 変更の有無
    virtual bool IsChange()const;

    //!< コンパイル必要性問い合わせ
    virtual COMPILE_STS GetCompileStatus()const;

    //!< コンパイル
    virtual bool IsCompiled( std::vector<StdString>* lstFileName) const;

    //-----------------
    // 構築
    //-----------------

    //!< 構築
    virtual bool Build( bool bRebuild );

    //!< コンパイル
    virtual bool Compile( bool bDebug, bool bRebuild);


    //!< モジュール取得
    CModule* GetModule();

    //!< ビルド停止
    void AbortBuild();

    //-----------------
    // ファイル
    //-----------------
    //!<読込
    virtual bool Load(StdPath Path);

    //!<保存
    virtual bool Save(StdPath Path);

    //!< load save のインスタンス生成用ダミー
    void Dummy();

    //!< ファイル書き込みモード問い合わせ
    bool IsSingleFileMode() const { return m_Prop.bSingleFile;}

    //!< ファイル書き込みモード設定
    void SetSingleFileMode(bool bSingle = true);

    //!< ファイルパス取得
    StdPath GetCurrentPath() const { return m_pathCurrent;}

    //-------------------
    // ユーザープロパティ
    //-------------------
    CPropertySet* GetUserPropertySet()const{return m_psPorpUser;}
   
    //-----------------
    // Undo
    //-----------------
    //!< アンドゥの取得
    virtual CUndoAction*  GetUndoAction();

    bool IsCopy() const{ return m_bCopy;};

    //-----------------
    // デバッグ用
    //-----------------
    virtual void Print() const;

protected:
    //!< ファイルパス設定
    void _SetCurrentPath(StdPath path){ m_pathCurrent = path;}

    //!< 全オブジェクト消去
    virtual void _DeleteAllObject();

    //!< スクリプト生成実行
    virtual bool _SetExecute();

    //----------------
    // プロパティ
    //----------------

    //!< プロパティ初期化
    virtual TREE_GRID_ITEM*  _CreateStdPropertyTree();

    //-------------------
    //!< プロパティ変更
    //-------------------
    static bool _PropThreadType   (CStdPropertyItem* pData, void* pObj);
    static bool _PropCycleTime    (CStdPropertyItem* pData, void* pObj);
    static bool _PropPriority     (CStdPropertyItem* pData, void* pObj);
    static bool _PropDebugMode    (CStdPropertyItem* pData, void* pObj);
    static bool _PropAutoCompie   (CStdPropertyItem* pData, void* pObj);
    static bool _PropEditPermission (CStdPropertyItem* pData, void* pObj);
    static bool _PropPropertySetName  (CStdPropertyItem* pData, void*pObj);


    //!< ビルドスレッド
    static void _BuildThread( CObjectDef* pObjDef, 
                             bool bDebug, 
                             bool bRebuild);

    
 protected:

    //---------------
    // 保存データ
    //---------------

    //プロパティ
    CProperty                                m_Prop;

    //IO定義
    CIoDef*                                 m_psIoDef;

    //スクリプト
    CScript*                                m_psScript;

    //ユーザ定義データ
    CPropertySet*                           m_psPorpUser;

    //---------------
    // 非保存データ
    //---------------
    //!< 表示用コピー
    // CreateTmpCopyで生成した
    bool                                    m_bCopy;

    //!<プロジェクト
    CProjectCtrl*                           m_pProj; 

    //ユーザ定義データ変更カウンタ(非保存)
    mutable int                             m_iChgPropUserCnt;

    // ASオブジェクト用
    int                                     m_iRefCount;

    // ビルド停止フラグ
    bool                                    m_bAbortBuild;

    //!< スクリプトモジュール
    CModule*                                m_psModule;

    //!< 現在のパス(load/save直前に設定)
    mutable StdPath                          m_pathCurrent;

    //!< 表示オブジェクト
    CDrawingScriptBase*                      m_psViewObject;

    //--------------------
    // プロパティ変更処理
    //--------------------
    //!< プロパティ
    CStdPropertyTree*      m_psProperty;

    //!< プロパティ初期化フラグ
    bool m_bInitProperty;

    //!< オブジェクト種別
    DRAWING_TYPE    m_eType;

    //保存したプロパティ
    mutable CProperty                     m_OrgProp;


    //!< 実行モード
    //static EXEC_COMMON::E_EXEC_STS  m_eMode;

    //----------------
    // UnDo
    //----------------
    //!< 操作用アンドゥ
    std::shared_ptr<CUndoAction>          m_psUndoAction;

    //==============================
    // ファイル保存
    //==============================
    friend class boost::serialization::access;  

    BOOST_SERIALIZATION_SPLIT_MEMBER();

    template<class Archive>
    void load(Archive& ar, const unsigned int version);

    template<class Archive>
    void save(Archive& ar, const unsigned int version) const;

};
#endif
