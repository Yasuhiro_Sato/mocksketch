/**
 * @brief			CDrawingView実装ファイル
 * @file			CDrawingView.cpp
 * @author			Yasuhiro Sato
 * @date			01-2-2009 10:36:26
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.3
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MockSketch.h"
#include "./CDrawingView.h"
#include "./MockSketchDoc.h"
#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/CFieldDef.h"
#include "DefinitionObject/CReferenceDef.h"
#include "DrawingObject/CDrawingParts.h"
#include "DrawingObject/CDrawingField.h"
#include "DrawingObject/CDrawingScriptBase.h"
#include "View/CCanvas.h"
#include "View/CDrawMarker.h"
#include "View/CUndoAction.h"
#include "View/CLayer.h"
#include "DrawingObject/Action/CActionPoint.h"
#include "DrawingObject/Action/CActionLine.h"
#include "DrawingObject/Action/CActionCircle.h"
#include "DrawingObject/Action/CActionEllipse.h"
#include "DrawingObject/Action/CActionText.h"
#include "DrawingObject/Action/CActionSpline.h"
#include "DrawingObject/Action/CActionDim.h"
#include "DrawingObject/Action/CActionDimL.h"
#include "DrawingObject/Action/CActionDimH.h"
#include "DrawingObject/Action/CActionDimV.h"
#include "DrawingObject/Action/CActionDimA.h"
#include "DrawingObject/Action/CActionDimD.h"
#include "DrawingObject/Action/CActionDimR.h"
#include "DrawingObject/Action/CActionDimC.h"
#include "DrawingObject/Action/CActionCorner.h"
#include "DrawingObject/Action/CActionCommand.h"
#include "DrawingObject/Action/CActionTrim.h"
#include "DrawingObject/Action/CActionBreak.h"
#include "DrawingObject/Action/CActionOffset.h"
#include "DrawingObject/Action/CActionDrop.h"
#include "DrawingObject/Action/CActionMove.h"
#include "DrawingObject/Action/CActionRotate.h"
#include "DrawingObject/Action/CActionScl.h"
#include "DrawingObject/Action/CActionMirror.h"
#include "DrawingObject/Action/CActionCompositionLine.h"
#include "DrawingObject/Action/CActionNode.h"
#include "DrawingObject/Action/CActionImage.h"
#include "DrawingObject/Action/CActionCreate.h"
#include "DrawingObject/Action/CActionNodeSelect.h"
#include "DrawingObject/Action/CActionConnectionLine.h"
#include "DrawingObject/CNodeMarker.h"
#include "DrawingObject/CNodeData.h"


#include <math.h>
#include "DrawingObject/Primitive/POINT2D.h"
#include "DrawingObject/Primitive/Rect2D.h"
#include "DrawingObject/Primitive/LINE2D.h"
#include "DrawingObject/CDrawingPoint.h"
#include "DrawingObject/CDrawingCompositionLine.h"
#include "DrawingObject/CDrawingCircle.h"
#include "DrawingObject/CDrawingCircle.h"
#include "DrawingObject/CDrawingSpline.h"
#include "DrawingObject/CDrawingEllipse.h"
#include "DrawingObject/CDrawingLine.h"
#include "DrawingObject/CDrawingNode.h"
#include "DrawingObject/CDrawingConnectionLine.h"
#include "DrawingObject/CDrawingText.h"
#include "DrawingObject/CNodeBound.h"


#include "Utility/CUtility.h"
#include "Utility/CImageData.h"
#include "Utility/ExtText/CExtText.h"
#include "Utility/ExtText/FONT_WORK_DATA.h"
#include "System/CSystem.h"
#include "System/CUnit.h"

#include "DefinitionObject/View/MockSketchView.h"
#include <ShellScalingAPI.h>
class CMockSketchDoc;

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 * @brief   コンストラクタ
 * @param   なし         
 * @retval  なし
 * @note	
 */
CDrawingView::CDrawingView():
m_pDoc(NULL)
{
    //TODO: make uniqueのに置き換え
    m_psBack = std::make_shared<CCanvas>(_T("BACK"));

    m_psSelect = std::make_shared<CCanvas>(_T("SEL"));

    m_psIndex = std::make_shared<CCanvas>(_T("INDEX"));

    m_psIndexHi = std::make_shared<CCanvas>(_T("INDEX_HI"));

    m_pSurface = std::make_shared<CCanvas>(_T("FRONT"), false);

    m_psMarker = std::make_shared<CDrawMarker>(this);

    m_psNodeMarker = std::make_shared<CNodeMarker>(this);

    m_psUnit =  std::make_shared<CUnit>();

    m_psNodeBound = std::make_unique<CNodeBound>();

    m_psIndex->SetBack(0);
    m_psIndexHi->SetBack(0);

    m_psBack->SetBack(DRAW_CONFIG->GetBackColor());
    m_psSelect->SetBack(DRAW_CONFIG->GetBackColor());
    m_pSurface->SetBack(DRAW_CONFIG->GetBackColor());

    m_eMode = DRAW_VIEW;
    m_eView = VIEW_NONE;
    m_eViewInterrupt = VIEW_NONE;
    m_eInterruptSelect =  VIEW_NONE;

    m_hDc = 0;
    m_hPen = 0;
    m_hOldPen = 0;

    // (DPI) -> n(unit) * m_dDpuX = dot
    double dConvFactor = DRAW_CONFIG->pDispUnit->ToBase(1.0);
    m_dDpu = DRAW_CONFIG->dDpi * (1000.0  * dConvFactor)/ 25.4;

    m_ptOffset.x = 0;
    m_ptOffset.y = 0;

    m_bRedraw = false;
    
    m_bInit   = false;

    m_Vv.iWidth  = 0;

    m_Vv.iHeight = 0;

    m_iInputBarHeight = 0;

    m_Vv.ptLook.x  = 0;
    m_Vv.ptLook.y  = 0;

    m_Vv.dDspScl = 1.0;

    m_brBack = CreateSolidBrush(DRAW_CONFIG->GetBackColor());

    m_pView         = NULL; 
    m_pCurAction     = NULL;
    //m_bLayerColor = false;

    //memset(&m_DeafultColor[0], 0 , sizeof(m_DeafultColor));

    /*
    for (long lCnt = 0; lCnt < DT_MAX; lCnt++)
    {
        m_DeafultColor[lCnt] = DRAW_CONFIG->GetDefaultColor();
    }
    */

    LINE2D::SetLineMax(DRAW_CONFIG->GetLineMax());

    m_bMouseMove = false;
    m_bPrint     = false;
    m_bCutMode   = true;


	//最小スナップ長(mm)
    double dSnap = DRAW_CONFIG->pDimUnit->ToBase(DRAW_CONFIG->dMinSnapLength) * 1000.0;

	//画面上の最小スナップ長に対するDot数
    m_dMinResDot = ceil((DRAW_CONFIG->dDpi / 25.4) * dSnap);

	m_iOldPrintCol = -1;
	m_iOldPrintRow = -1;

    m_bDrag = false;
    InitAction();
   //アクション設定

}

/**
 * @brief   デストラクタ
 * @param   なし         
 * @retval  なし
 * @note	
 */

CDrawingView::~CDrawingView()
{
    CPartsDef* pDef = GetPartsDef();
    if( pDef )
    {
        pDef->DelView(this);
    }

    if (m_hPen)
    {
        ::DeleteObject(m_hPen);
    }
}


/**
 * @brief   アクション初期化
 * @param   なし         
 * @retval  なし
 * @note	
 */
void  CDrawingView::InitAction()
{
    // 可能であればAction側に登録機構を付ける
    std::shared_ptr< CActionPoint > pPoint( new CActionPoint(this) );  
    m_mapViewAction[VIEW_POINT]     = pPoint;

    std::shared_ptr< CActionLine > pLine( new CActionLine(this) );  
    m_mapViewAction[VIEW_LINE]      = pLine;

    std::shared_ptr< CActionCircle> pCircle( new CActionCircle(this) );  
    m_mapViewAction[VIEW_CIRCLE]    = pCircle;
    m_mapViewAction[VIEW_CIRCLE_3P] = pCircle;

    std::shared_ptr< CActionNode >  pNode( new CActionNode(this) );  
    m_mapViewAction[VIEW_NODE]      = pNode;

    std::shared_ptr< CActionImage >  pImage( new CActionImage(this) );  
    m_mapViewAction[VIEW_IMAGE]      = pImage;

    //m_mapViewAction[VIEW_ARC]     = 

    std::shared_ptr< CActionEllipse> pEllipse( new CActionEllipse(this) );  
    m_mapViewAction[VIEW_ELLIPSE] = pEllipse;

    std::shared_ptr< CActionText> pNote( new CActionText(this) );  
    m_mapViewAction[VIEW_TEXT] =  pNote;

    std::shared_ptr< CActionSpline> pSpline( new CActionSpline(this) );  
    m_mapViewAction[VIEW_SPLINE] =  pSpline;

    std::shared_ptr< CActionDimL> pDimL( new CActionDimL(this) );  
    m_mapViewAction[VIEW_L_DIM]   =  pDimL;

    std::shared_ptr< CActionDimH> pDimH( new CActionDimH(this) );  
    m_mapViewAction[VIEW_H_DIM]   =  pDimH;

    std::shared_ptr< CActionDimV> pDimV( new CActionDimV(this) );  
    m_mapViewAction[VIEW_V_DIM]   =  pDimV;

    std::shared_ptr< CActionDimA> pDimA( new CActionDimA(this) );  
    m_mapViewAction[VIEW_A_DIM]   =  pDimA;

    std::shared_ptr< CActionDimD> pDimD( new CActionDimD(this) );  
    m_mapViewAction[VIEW_D_DIM]   =  pDimD;

    std::shared_ptr< CActionDimR> pDimR( new CActionDimR(this) );  
    m_mapViewAction[VIEW_R_DIM]   =  pDimR;

    std::shared_ptr< CActionDimC> pDimC( new CActionDimC(this) );  
    m_mapViewAction[VIEW_C_DIM]   =  pDimC;


    std::shared_ptr< CActionCommand> pCommand( new CActionCommand(this) );  
    m_mapViewAction[VIEW_SELECT]       =  pCommand;
    m_mapViewAction[VIEW_DELETE]       =  pCommand;
    m_mapViewAction[VIEW_DISASSEMBLY]  =  pCommand;

    std::shared_ptr< CActionCreate> pCreate( new CActionCreate(this) );  
    m_mapViewAction[VIEW_GROUP]        =  pCreate;
    m_mapViewAction[VIEW_PARTS]        =  pCreate;
    m_mapViewAction[VIEW_REFERENCE]    =  pCreate;

    std::shared_ptr< CActionNodeSelect> pNodeSelect( new CActionNodeSelect(this) );  
    m_mapViewAction[VIEW_NODE_SELECT]        =  pNodeSelect;


    std::shared_ptr< CActionMove> pMove( new CActionMove(this) );  
    m_mapViewAction[VIEW_MOVE]         =  pMove;
    m_mapViewAction[VIEW_COPY]         =  pMove;

    std::shared_ptr< CActionRotate> pRotate( new CActionRotate(this) );  
    m_mapViewAction[VIEW_ROTATE]       =  pRotate;

    std::shared_ptr< CActionScl> pScl( new CActionScl(this) );  
    m_mapViewAction[VIEW_SCL]          =  pScl;

    std::shared_ptr< CActionMirror> pMirror( new CActionMirror(this) );  
    m_mapViewAction[VIEW_MIRROR]       =  pMirror;

    std::shared_ptr< CActionDrop> pDrop( new CActionDrop(this) );  
    m_mapViewAction[VIEW_DROP]       =  pDrop;

    std::shared_ptr< CActionTrim> pTrim( new CActionTrim(this) );  
    m_mapViewAction[VIEW_TRIM] = pTrim;

    std::shared_ptr< CActionCorner> pCorner( new CActionCorner(this) );  
    m_mapViewAction[VIEW_CORNER] = pCorner;
    m_mapViewAction[VIEW_CHAMFER] = pCorner;

    std::shared_ptr< CActionBreak> pBreak( new CActionBreak(this) );  
    m_mapViewAction[VIEW_BREAK] = pBreak;

    std::shared_ptr< CActionOffset> pOffset( new CActionOffset(this) );  
    m_mapViewAction[VIEW_OFFSET] = pOffset;
    
    std::shared_ptr< CActionCompositionLine> pLoop( new CActionCompositionLine(this) );  
    m_mapViewAction[VIEW_COMPOSITION_LINE] = pLoop;

    std::shared_ptr< CActionConnectionLine> pConnect( new CActionConnectionLine(this) );  
    m_mapViewAction[VIEW_CONNECTION_LINE] = pConnect;

    std::shared_ptr<CViewAction> pDefault(new CActionCommand(this));  


    m_pDefaultAction = pCommand;
    m_pCurAction = m_pDefaultAction.get();
    m_bMouseMove = false;
}

/**
 * @brief   画面初期化
 * @param   なし         
 * @retval  なし
 * @note	画面のデバイスコンテキストが必要なものは
 * @note	ここで初期化する
 */
void  CDrawingView::InitDisp()
{
    //PROCESS_DPI_AWARENESS
   SetProcessDpiAwareness(PROCESS_PER_MONITOR_DPI_AWARE);
}

/**
 * @brief   ドキュメント設定
 * @param   [in]    pDoc ドキュメント
 * @retval   なし
 * @note	
 */
void CDrawingView::SetDocument( CMockSketchDoc* pDoc)
{
    STD_ASSERT(!m_pDoc);
    m_pDoc = pDoc;
}

/**
 * @brief   ウインドウ設定
 * @param   [in]    pView ウインドウ
 * @retval   なし
 * @note	
 */
void CDrawingView::SetWindow( CWnd* pView )
{
    m_pView = pView;

   CMockSketchView* pSketchView = dynamic_cast<CMockSketchView*>(m_pView);
   STD_ASSERT(pView != 0);
   SetViewMode(VIEW_SELECT);
}


/**
 * @brief   ウインドウ削除
 * @param   なし
 * @retval  なし
 * @note	
 */
void CDrawingView::DestoryWindow()
{
    m_pView = NULL;

    m_psIndex->Release();
	m_psIndexHi->Release();
	m_psBack->Release();
	m_psSelect->Release();
    m_pSurface->Release();
}

/**
 * @brief   ウインドウ取得
 * @param   なし 
 * @retval  ウインドウへのポインタ
 * @note	
 */
CWnd* CDrawingView::GetWindow()
{
    return m_pView;
}

/**
 * @brief   ウインドウ再設定設定
 * @param   なし
 * @retval  なし
 * @note    新たにオブジェクトコントロールを設定したときに

 */
void CDrawingView::ResetCtrl()
{
  
    m_bInit = false;
    OnSize(m_Vv.iWidth, m_Vv.iHeight);
}


/**
 * @brief   オブジェクト追加
 * @param   [in]    pObj 描画オブジェクト 
 * @retval   なし
 * @note	
 */
/*
void CDrawingView::AddObject( CDrawingObject* pObj)
{
    STD_ASSERT(m_pPartsDef != NULL);
    STD_ASSERT(m_pDrawingParts == NULL);

    m_pPartsDef->AddObject(pObj);
    pObj->Draw(this);
}
*/

/**
 * @brief   選択オブジェクト追加
 * @param   [in]    pObj 選択オブジェクト 
 * @retval   なし
 * @note	
 */
/*
void CDrawingView::AddSelect( CDrawingObject* pObj)
{
    STD_ASSERT(m_pPartsDef != NULL);
    STD_ASSERT(m_pDrawingParts == NULL);

    m_pPartsDef->SelectObject(pObj->GetId());
    pObj->Draw(this);
}
*/

/**
 * @brief   グループ追加
 * @param   [in]    ptClick     クリック位置 
 * @param   [in]    pPartsDef    グループオブジェクト 
 * @retval   なし
 * @note	
 */
/*
void CDrawingView::AddGroup( POINT ptClick, std::shared_ptr<CObjectDef> pDef)
{
    CActionDrop* pDrop = dynamic_cast<CActionDrop*>(m_pCurAction);

    if (pDrop)
    {   
        pDrop->AddGroup(ptClick, pDef);
    }
}
*/

/**
 * @brief   ドロップ位置検索
 * @param   [in]    ptPos   マウス位置 
 * @retval   なし
 * @note    実処理はActionDrop
 */
/*
void CDrawingView::SearchDropPos(POINT ptPos)
{
    OnMouseMove( 0, ptPos);
}
*/

/**
 * @brief   デバイスコンテキスト設定
 * @param   [in]    hDc   描画デバイスコンテキスト         
 * @retval   なし
 * @note	
 */
void  CDrawingView::SetDc (CDC* pDc)
{
    HDC hDc = pDc->GetSafeHdc();
    STD_ASSERT(hDc != 0);

    //boost::mutex::scoped_lock lk(m_Mtx);

    //dot -> 実寸法への変換値を求める。
    //DRAW_CONFIG->dDpi
    int cxInch = pDc->GetDeviceCaps(LOGPIXELSX);
    int cyInch = pDc->GetDeviceCaps(LOGPIXELSY);

    if (!m_bPrint)
    {
        double dConvFactor = DRAW_CONFIG->pDispUnit->ToBase(1.0);
        m_dDpu = cxInch * (1000.0  * dConvFactor) / 25.4;
    }


    //m_dDpuX = DRAW_CONFIG->dDpi * (1000.0  * dConvFactor)/ 25.4;
    //m_dDpuY = m_dDpuX;
		
    m_hDc = hDc;
    m_pSurface->ChangeDc(pDc, m_bPrint);
    m_psMarker->InitDisp(hDc);
    //m_pGrap = std::make_unique<Gdiplus::Graphics>(m_hDc);

    //ペン再設定
    ::DeleteObject(m_hPen);
    m_hOldPen = (HPEN)::SelectObject(m_hDc, (HGDIOBJ)m_hPen);
    //m_pPen = std::make_unique<Gdiplus::Pen>(Gdiplus::Color(0,0,0),1.0f);
}

HDC  CDrawingView::GetDc()
{
    return m_hDc;
}

/**
 * @brief   描画モード設定
 * @param   [in]    eMode 描画モード         
 * @retval   なし
 * @note	
 */
void  CDrawingView::SetDrawingMode( DRAWING_MODE eMode)
{
#ifdef _DEBUG
    if (GetDebugMode() != CDrawingView::DM_DISABLE)
    {
        DB_PRINT(_T("SetDrawingMode %s \n"), GetDraingMode().c_str());
    }
#endif
    m_eMode = eMode;
}


/**
 * @brief   ID設定.
 * @param   [in]    iID       ID  
 * @retval  なし
 * @note	DrawingElement用
 */
void CDrawingView::SetIndexBufferId( int iID)
{
    if(m_eMode & DRAW_INDEX)
    {
        int iIdHi = (iID >> 24);
        m_psIndex->SetPenColor(iID);
        m_psIndexHi->SetPenColor(iIdHi);
    }
}

/**
 * @brief   ペン設定.
 * @param   [in]    iPenStyle 線種         
 * @param   [in]    iWidth    線幅         
 * @param   [in]    crPen     線色         
 * @retval   なし
 * @note	線種 Solid以外は 線幅は１固定
 */
void CDrawingView::SetPen( int iPenStyle, int iWidth, COLORREF crPen, int iID)
{
    if(m_eMode & DRAW_FIG)
    {
    	m_psBack->SetPen( iPenStyle, iWidth, crPen);
    }
    
    if(m_eMode & DRAW_SEL)
    {
    	m_psSelect->SetPen( iPenStyle, iWidth, crPen);
    }

    if(m_eMode & DRAW_INDEX)
    {
        int iIdHi = (iID >> 24);
        m_psIndex->SetPen( iPenStyle, iWidth, iID);
        m_psIndexHi->SetPen( iPenStyle, iWidth, iIdHi);
    }
    
    if((m_eMode & DRAW_FRONT) || (m_eMode & DRAW_PRINT))
    {
        m_pSurface->SetPen( iPenStyle, iWidth, crPen);
        /*
        STD_ASSERT(m_hDc != 0);

        if( (m_iPenStyle == iPenStyle) &&
            (m_iPenWidth == iWidth)    &&
            (m_crPen     == crPen))
        {
            return;
        }

        if (m_hPen != 0)
        {
            ::DeleteObject(m_hPen);
            m_hPen = 0;
        }
    
        if (DRAW_CONFIG->bUseSystemLineType)
        {
            m_hPen = ::CreatePen(iPenStyle, iWidth, crPen);
        }
        else
        {
            m_hPen = ::CreatePen(PS_SOLID, iWidth, crPen);
        }

 //GDI+       
        m_pPen->SetColor(Gdiplus::Color(GetRValue(crPen), 
                                        GetGValue(crPen),
                                        GetBValue(crPen)));

        m_pPen->SetWidth(static_cast<Gdiplus::REAL>(iWidth));

        Gdiplus::DashStyle ds;
        ds = static_cast<Gdiplus::DashStyle>(iPenStyle);
        m_pPen->SetDashStyle(ds);

        if (m_hPen == 0)
        {
LPVOID lpMsgBuf;
DWORD  dwError;
dwError = GetLastError();
            FormatMessage(				//エラー表示文字列作成
			            FORMAT_MESSAGE_ALLOCATE_BUFFER | 
			            FORMAT_MESSAGE_FROM_SYSTEM | 
			            FORMAT_MESSAGE_IGNORE_INSERTS,
			            NULL, dwError,
			             MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), 
			             (LPWSTR) &lpMsgBuf, 0, NULL);

MessageBox(NULL, (LPCWSTR)lpMsgBuf, NULL, MB_OK);	//メッセージ表示

LocalFree(lpMsgBuf);
            STD_DBG(_T("CreatePen Error %d,%d,%x\n"), iPenStyle, iWidth, crPen);
            return;
        }
        STD_ASSERT(m_hPen != 0);

        m_hOldPen = (HPEN)::SelectObject(m_hDc, (HGDIOBJ) m_hPen);

        m_iPenStyle = iPenStyle;
        m_iPenWidth = iWidth;
        m_crPen     = crPen;
        */
    }


}


COLORREF CDrawingView::GetPenColor()
{
    if(m_eMode & DRAW_FIG)
    {
    	return m_psBack->GetColor();
    }

    if(m_eMode & DRAW_SEL)
    {
    	return m_psSelect->GetColor();
    }

    if((m_eMode & DRAW_FRONT) || (m_eMode & DRAW_PRINT))
    {
        return m_pSurface->GetColor();
    }

    return 0;
}

void CDrawingView::SetBrush(COLORREF crBrush)
{
    if(m_eMode & DRAW_FIG)
    {
    	m_psBack->SetBrush( crBrush);
    }
    
    if(m_eMode & DRAW_SEL)
    {
    	m_psSelect->SetBrush( crBrush);
    }

    if(m_eMode & DRAW_INDEX)
    {
        COLORREF crId = m_psIndex->GetColor();
        COLORREF crIdHi = m_psIndexHi->GetColor();
    	m_psIndex->SetBrush( crId);
    	m_psIndexHi->SetBrush( crIdHi);
    }
    
    if((m_eMode & DRAW_FRONT) || (m_eMode & DRAW_PRINT))
    {
        m_pSurface->SetBrush( crBrush);
    }
}


void CDrawingView::SetLineAlternateMode( bool bSet)
{
    if(m_eMode & DRAW_FIG)
    {
    	m_psBack->SetLineAlternateMode( bSet);
    }
    
    if(m_eMode & DRAW_SEL)
    {
    	m_psSelect->SetLineAlternateMode( bSet);
    }

    if((m_eMode & DRAW_FRONT) || (m_eMode & DRAW_PRINT))
    {
        m_pSurface->SetLineAlternateMode( bSet);
    }
}

/**
 * @brief   データ->ビュー変換
 * @param   [out]   pPt     画面上の座標         
 * @param   [in]    pt2D    実座標         
 * @param   [in]    dScl    スケール        
 * @retval   なし
 * @note	
 */
void CDrawingView::ConvWorld2Scr(POINT* pPt, POINT2D pt2D, int iLayerId, bool bFixLayerScl) const
{
    CLayer* pLayer = GetLayer(iLayerId);
    double dSclLayer = 1.0;
    if (!bFixLayerScl)
    {
        dSclLayer = pLayer->dScl;
    }
    double dScl =    dSclLayer * m_Vv.dDspScl;

    pPt->x =  static_cast<LONG>(  CUtil::Round((pt2D.dX  - pLayer->ptOffset.dX ) * m_dDpu *  dScl ) - m_Vv.ptLook.x + (m_Vv.iWidth / 2.0));
    pPt->y =  static_cast<LONG>(- CUtil::Round((pt2D.dY  - pLayer->ptOffset.dY ) * m_dDpu *  dScl ) - m_Vv.ptLook.y + (m_Vv.iHeight / 2.0));
}

void CDrawingView::ConvWorld2Scr(int* piLen, double dLen, int iLayerId, bool bFixLayerScl) const
{
    CLayer* pLayer = GetLayer(iLayerId);
    double dSclLayer = 1.0;
    if (!bFixLayerScl)
    {
        dSclLayer = pLayer->dScl;
    }
    double dScl =    dSclLayer * m_Vv.dDspScl;

    *piLen =  static_cast<LONG>(  CUtil::Round((dLen ) * m_dDpu *  dScl ));
}

/**
 * @brief   ビュー->データ変換
 * @param   [out]   pPt2D   実座標         
 * @param   [in]    Pt      画面上の座標         
 * @param   [in]    dScl    スケール        
 * @retval   なし
 * @note	
 */
void CDrawingView::ConvScr2World(POINT2D* pPt2D, POINT Pt, int iLayerId, bool bFixLayerScl) const
{
    /*
    STD_ASSERT( fabs(m_dDpu) > NEAR_ZERO);

    double dSclLayer = 1.0;
    CLayer* pLayer = GetLayer(iLayerId);
    if (!bFixLayerScl)
    {
        dSclLayer = pLayer->dScl;
    }

    double dScl =    dSclLayer * m_Vv.dDspScl;

    pPt2D->dX =  double(  Pt.x + m_Vv.ptLook.x  - m_Vv.iWidth / 2.0 )  / (m_dDpu  * dScl) + pLayer->ptOffset.dX;
    pPt2D->dY =  double(- Pt.y - m_Vv.ptLook.y  + m_Vv.iHeight / 2.0 ) / (m_dDpu  * dScl) - pLayer->ptOffset.dY;
    */
    ConvScr2World(pPt2D, Pt.x, Pt.y, iLayerId, bFixLayerScl);
}


void CDrawingView::ConvScr2World(POINT2D* pPt2D, double dX, double dY, int iLayerId, bool bFixLayerScl) const
{
    STD_ASSERT( fabs(m_dDpu) > NEAR_ZERO);

    double dSclLayer = 1.0;
    CLayer* pLayer = GetLayer(iLayerId);
    if (!bFixLayerScl)
    {
        dSclLayer = pLayer->dScl;
    }

    double dScl =    dSclLayer * m_Vv.dDspScl;

    pPt2D->dX =  double(  dX + m_Vv.ptLook.x  - m_Vv.iWidth / 2.0 )  / (m_dDpu  * dScl) + pLayer->ptOffset.dX;
    pPt2D->dY =  double(- dY - m_Vv.ptLook.y  + m_Vv.iHeight / 2.0 ) / (m_dDpu  * dScl) - pLayer->ptOffset.dY;
}

void CDrawingView::ConvScr2World(double* pdL, int iL, int iLayerId, bool bFixLayerScl) const
{
    STD_ASSERT( fabs(m_dDpu) > NEAR_ZERO);

    double dSclLayer = 1.0;
    CLayer* pLayer = GetLayer(iLayerId);
    if (!bFixLayerScl)
    {
        dSclLayer = pLayer->dScl;
    }

    double dScl =    dSclLayer * m_Vv.dDspScl;

    *pdL =  double( iL ) / (m_dDpu  * dScl);
}

/**
 * @brief   表示倍率
 * @param   [in]    dScl    表示倍率
 * @param   [in]    pt      拡大中心
 * @retval   なし
 * @note	
 */
void CDrawingView::ViewScale( double dScl, POINT pt)
{
    if (dScl < 0.000001)
    {
        dScl = 0.000001;
    }

    if (dScl > 100000.0)
    {
        dScl = 100000.0;
    }


#if 0
DB_PRINT(_T("----------------\n"));
DB_PRINT(_T("元注視点位置 %d, %d \n"), m_Vv.ptLook.x, m_Vv.ptLook.y);
DB_PRINT(_T("スケール %f, %f \n"), dScl);
#endif
    int iLayerId = GetCurrentLayerId();



    POINT2D  pt2D;
    ConvScr2World(&pt2D, pt, iLayerId);

    m_Vv.dDspScl = dScl;

    CLayer* pLayer = GetLayer(iLayerId);
    double dSclLayer = pLayer->dScl;

    m_Vv.ptLook.x =  static_cast<LONG>(  double(pt2D.dX - pLayer->ptOffset.dX) * m_dDpu * m_Vv.dDspScl * dSclLayer + m_Vv.iWidth  / 2.0 - pt.x);
    m_Vv.ptLook.y =  static_cast<LONG>(- double(pt2D.dY - pLayer->ptOffset.dY) * m_dDpu * m_Vv.dDspScl * dSclLayer + m_Vv.iHeight / 2.0 - pt.y);
#if 0
DB_PRINT(_T("Diff Offset %f, %f \n"), pt2D.dX - pLayer->ptOffset.dX, pt2D.dY - pLayer->ptOffset.dY);
DB_PRINT(_T("m_dDpu [%f]: m_Vv.dDspScl[%f]: dSclLayer[%f] \n"), m_dDpu, m_Vv.dDspScl, dSclLayer);


DB_PRINT(_T("クリック位置 %d, %d \n"), pt.x, pt.y);
DB_PRINT(_T("オフセット %d, %d \n"), m_Vv.ptLook.x, m_Vv.ptLook.y);
DB_PRINT(_T("----------------\n"));
DB_PRINT(_T("\n"));
#endif
    
}

/**
 * @brief   表示位置変更
 * @param   [in]    pt   クリック位置
 * @retval   なし
 * @note	
 */
void CDrawingView::SetCenter(POINT pt)
{
    //中心からの相対距離
    POINT ptMove;

    ptMove.x= static_cast<LONG>(  pt.x - (m_Vv.iWidth  / 2.0));
    ptMove.y= static_cast<LONG>(- pt.y + (m_Vv.iHeight  / 2.0));

    m_Vv.ptLook.x += ptMove.x;
    m_Vv.ptLook.y -= ptMove.y;

    CPartsDef* pDef = GetPartsDef();
    if( pDef )
    {
        pDef->Redraw(this);
    }
}


/**
 * @brief   表示位置変更
 * @param   [in]    pt2D    表示位置
 * @retval   なし
 * @note	
 */
void CDrawingView::ViewMove(const POINT2D* pt2D)
{
    int iLayerId = GetCurrentLayerId();
    CLayer* pLayer = GetLayer(iLayerId);
    double dSclLayer = pLayer->dScl;
    double dScl =    dSclLayer * m_Vv.dDspScl;

    m_Vv.ptLook.x  =   static_cast<LONG>(CUtil::Round((pt2D->dX - pLayer->ptOffset.dX) * (m_dDpu *  dScl )));
    m_Vv.ptLook.y  =   static_cast<LONG>(CUtil::Round( -(pt2D->dY - pLayer->ptOffset.dY) * (m_dDpu *  dScl )));
}

POINT2D CDrawingView::GetLookAt() const
{
    int iLayerId = GetCurrentLayerId();
    CLayer* pLayer = GetLayer(iLayerId);
    double dSclLayer = pLayer->dScl;
    double dScl =    dSclLayer * m_Vv.dDspScl;

    POINT2D ptLook;
    ptLook.dX = (m_Vv.ptLook.x  / (m_dDpu *  dScl )) + pLayer->ptOffset.dX;
    ptLook.dY = (-m_Vv.ptLook.y  / (m_dDpu *  dScl )) + pLayer->ptOffset.dY;

    return  ptLook;
}



/**
 * @brief   全体表示
 * @param   なし
 * @retval  なし
 * @note	
 */
void CDrawingView::ViewAll()
{
    RECT2D rc;

    rc = GetViewBounds();
    double dWidth  = fabs(rc.dRight - rc.dLeft);
    double dHeight = fabs(rc.dTop - rc.dBottom);

    if ( (dWidth  < NEAR_ZERO) &&
         (dHeight < NEAR_ZERO)) 
    {
        return;
    }

    // Dpm < 1

    double dDpm = m_dDpu;

    double dSclX, dSclY, dScl;


    dSclX = double(m_Vv.iWidth)  / (dWidth * dDpm);
    dSclY = double(m_Vv.iHeight - m_iInputBarHeight) / (dHeight * dDpm);

    if (dSclY > dSclX)
    {
        dScl = dSclX;
    }
    else
    {
        dScl = dSclY;
    }

    int  iLayerId  = GetCurrentLayerId();
    CLayer* pLayer = GetLayer(iLayerId);

    POINT2D ptCenter( ((rc.dRight + rc.dLeft) * 0.5) / pLayer->dScl, 
                      ((rc.dTop + rc.dBottom) * 0.5) / pLayer->dScl);

    POINT ptLook;
    m_Vv.dDspScl = dScl;

    double dDspScl =    pLayer->dScl * m_Vv.dDspScl;

   ptLook.x = static_cast<LONG>(   CUtil::Round((ptCenter.dX  - pLayer->ptOffset.dX ) * dDpm *  dDspScl ));
   ptLook.y = static_cast<LONG>( - CUtil::Round((ptCenter.dY  - pLayer->ptOffset.dY ) * dDpm *  dDspScl ) - (m_iInputBarHeight / 2.0));


    m_Vv.ptLook.x  = ptLook.x;
    m_Vv.ptLook.y  = ptLook.y;

    CPartsDef* pDef = GetPartsDef();
    if( pDef )
    {
        pDef->Redraw(this);
    }
}

/**
 * @brief   表示倍率拡大
 * @param   なし
 * @retval  なし
 * @note	
 */
void CDrawingView::ZoomUp(int iTimes, POINT ptCenter)
{
    ViewScale( m_Vv.dDspScl * 1.1 /*+ (0.1 * (double)iTimes)*/,  ptCenter);
}

/**
 * @brief   表示倍率縮小
 * @param   なし
 * @retval  なし
 * @note	
 */
void CDrawingView::ZoomDown(int iTimes, POINT ptCenter)
{
    //TAG:[拡大・縮小率について要検討]
    ViewScale( m_Vv.dDspScl * 0.9 /*- (0.1 * (double)iTimes)*/  ,  ptCenter);
}

/**
 * @brief   ウインドウサイズ変更
 * @param   [in]    iX      幅
 * @param   [in]    iY      高さ         
 * @retval   なし
 * @note	
 */
void CDrawingView::OnSize(int iX, int iY)
{
    m_psIndex->OnSize( iX, iY);
	m_psBack->OnSize( iX, iY);
    m_psIndexHi->OnSize( iX, iY);
	m_psSelect->OnSize( iX, iY);
	m_pSurface->OnSize( iX, iY);
    if (!m_bInit)
    {
        //画面初期化
        InitDisp();
        m_bInit = true;
    }

    //カレント
    m_Vv.iWidth  = iX;
    m_Vv.iHeight = iY;

    m_bRedraw = true;
}

/**
 * @brief   表示エリア取得
 * @param   [in] iLayerId
 * @retval  なし
 * @note	
 */
RECT2D CDrawingView::GetViewArea(int iLayerId) const
{
	POINT2D ptTL2D;
	POINT2D ptBR2D;
	POINT ptTL;
	POINT ptBR;
	ptTL.x = 0;
	ptTL.y = 0;
	ptBR.x = m_Vv.iWidth;
	ptBR.y = m_Vv.iHeight;

	ConvScr2World(&ptTL2D, ptTL, iLayerId);
	ConvScr2World(&ptBR2D, ptBR, iLayerId);

	RECT2D ret(ptTL2D, ptBR2D);
	return ret;
}

//!< アクション割り込み(ACT_LBUTTON_UP ACT_LBUTTON_DOWN)
bool CDrawingView::InterruptAction(VIEW_MODE eMode, MOUSE_MOVE_POS posMouse)
{
    //現状では寸法設定時、他の寸法を変更するために使用
    //接続線も追加
	//スプラインも追加
	if (m_pCurAction)
	{
		m_pCurAction->InitTempObject();
	}

    m_eViewInterrupt = m_eView;
    SetViewMode (eMode, true);

    //!< ウインドウ取得
    //CMockSketchView* pView;
    //CWnd* pWnd = GetWindow();
    //pView = dynamic_cast<CMockSketchView*>(pWnd);

    if (m_pCurAction)
    {
        m_pCurAction->SetInterrupt();
        m_pCurAction->SelPoint( posMouse, CViewAction::ACT_LBUTTON_DOWN);
        m_pCurAction->SelPoint( posMouse, CViewAction::ACT_LBUTTON_UP);
    }
    return true;
}

bool CDrawingView::RetInterrupt()
{
    SetViewMode (m_eViewInterrupt);
    m_eViewInterrupt = VIEW_NONE;
    return true;
}

bool CDrawingView::RelInterrupt()
{
   m_eViewInterrupt = VIEW_NONE;
    return true;
}

/**
 * @brief    カレントレイヤー取得
 * @param    なし
 * @retval   カレントレイヤー
 * @note	
 */
CLayer* CDrawingView::GetCurrentLayer() const
{

    int iLayerId = GetCurrentLayerId();
    return GetLayer(iLayerId);
}

/**
 * @brief    カレントレイヤーID取得
 * @param    なし
 * @retval   カレントレイヤーID
 * @note	
 */
int CDrawingView::GetCurrentLayerId() const
{
    CPartsDef* pDef = GetPartsDef();
    if( pDef )
    {
        return pDef->GetCurrentLayerId();
    }
    return -1;
}

/**
 * @brief    レイヤー取得
 * @param    なし
 * @retval   カレントレイヤー
 * @note	
 */
CLayer* CDrawingView::GetLayer(int iLayerId) const
{
    CPartsDef* pDef = GetPartsDef();
    if( pDef )
    {
        return pDef->GetLayer(iLayerId);
    }
    return NULL;
}

int CDrawingView::GetTextDatum()
{
    CMockSketchView* pView = dynamic_cast<CMockSketchView*>(m_pView);
    STD_ASSERT(pView != 0);
    return pView->GetTextDatum();
}

int CDrawingView::GetTextAlign()
{
    CMockSketchView* pView = dynamic_cast<CMockSketchView*>(m_pView);
    STD_ASSERT(pView != 0);
    return pView->GetTextAlign();

}


/**
 * @brief   領域取得
 * @param   なし
 * @retval  なし
 * @note	全体表示のため縮尺を考慮した領域を取得する
 */
RECT2D  CDrawingView::GetViewBounds() const
{
    CPartsDef* pDef = GetPartsDef();
    if( pDef )
    {
        return pDef->GetViewBounds();
    }

    RECT2D rc;
    return rc;
}

/**
 * @brief   点描画
 * @param   [in] pt
 * @retval  なし
 * @note
 */
void CDrawingView::Point(POINT pt) const
{ 
    if(m_eMode & DRAW_FIG)
    {
        ::SetPixel(m_psBack->GetDc(), pt.x, pt.y, m_crPen);
    }

    if(m_eMode & DRAW_SEL)
    {
        ::SetPixel(m_psSelect->GetDc(), pt.x, pt.y, m_crPen);
    }

    if(m_eMode & DRAW_INDEX)
    {
        ::SetPixel(m_psIndex->GetDc(), pt.x, pt.y, m_psIndex->GetColor());
        ::SetPixel(m_psIndexHi->GetDc(), pt.x, pt.y, m_psIndexHi->GetColor());
    }

    if ((m_eMode & DRAW_FRONT) ||
        (m_eMode & DRAW_PRINT))
    {
        ::SetPixel(m_hDc, pt.x, pt.y, m_crPen);
    }
}

void CDrawingView::PointMark(POINT Pt, POINT_TYPE eType, int iSize) const
{

	POINT Pt1;
	POINT Pt2;
	switch (eType)
	{
	case POINT_DOT:
		Point(Pt);
		break;

	case POINT_RECT:
	{
		RECT rc;
		rc.left = Pt.x - iSize;
		rc.top = Pt.y - iSize;
		rc.right = Pt.x + iSize;
		rc.bottom = Pt.y + iSize;
		Rect(rc);
	}
		break;

	case POINT_RECT_FILL:
	{
		RECT rc;
		rc.left = Pt.x - iSize;
		rc.top = Pt.y - iSize;
		rc.right = Pt.x + iSize;
		rc.bottom = Pt.y + iSize;
		FillRect(rc);
	}
		break;

	case POINT_PLUS:
		Pt1.x = Pt.x - iSize;
		Pt2.x = Pt.x + iSize;
		Pt1.y = Pt2.y = Pt.y;

		Line(Pt1, Pt2);

		Pt1.y = Pt.y - iSize;
		Pt2.y = Pt.y + iSize;
		Pt1.x = Pt2.x = Pt.x;

		Line(Pt1, Pt2);

		break;

	case POINT_CROSS:
		Pt1.x = Pt.x - iSize;
		Pt1.y = Pt.y - iSize;
		Pt2.x = Pt.x + iSize;
		Pt2.y = Pt.y + iSize;
		Line(Pt1, Pt2);

		Pt1.x = Pt.x - iSize;
		Pt1.y = Pt.y + iSize;
		Pt2.x = Pt.x + iSize;
		Pt2.y = Pt.y - iSize;
		Line(Pt1, Pt2);
		break;

	case  POINT_ROUND:
	{
		RECT rc;
		rc.left = Pt.x - iSize;
		rc.top = Pt.y - iSize;
		rc.right = Pt.x + iSize;
		rc.bottom = Pt.y + iSize;
		Ellipse(rc);
	}

	case  POINT_ROUND_FILL:
	{
		RECT rc;
		rc.left = Pt.x - iSize;
		rc.top = Pt.y - iSize;
		rc.right = Pt.x + iSize;
		rc.bottom = Pt.y + iSize;
		FillEllipse(rc);
	}
	}
}




int CDrawingView::_ClipPos(POINT pt)const
{
    int c = 0;
    if (pt.x < E_MIN)   {c += E_LEFT ;}
    if (pt.x > E_MAX)   {c += E_RIGHT ;} 
    if (pt.y < E_MIN)   {c += E_TOP ;}
    if (pt.y > E_MAX)   {c += E_BOTTOM ;}
    return c;
}


/* クリッピング後の座標を求める */
int CDrawingView::_CalcClippedPoint( int code, POINT pt1, POINT pt2, POINT* pPt)const
{
    double cx,cy;

    if ( ( code & E_LEFT ) != 0 ) 
    {
        cy = double( pt2.y - pt1.y ) * double( E_MIN - pt1.x ) / double( pt2.x - pt1.x ) + pt1.y;	/* ...(1) */

        if( ( cy >= E_MIN ) && ( cy <= E_MAX ) ) 
        {
            pPt->x = E_MIN;
            pPt->y = DoubleToInt(cy);
            return( 1 ); 
        }
    }

    if ( ( code & E_RIGHT ) != 0 ) 
    {
        cy = double( pt2.y - pt1.y ) * double( E_MAX - pt1.x ) / double( pt2.x - pt1.x ) + pt1.y;
        if ( ( cy >= E_MIN ) && ( cy <= E_MAX ) )
        {
            pPt->x = E_MAX;
            pPt->y = DoubleToInt(cy);
            return( 1 ); 
        }
    }

    /* ウィンドウの上端より外側にある */
    if ( ( code & E_TOP ) != 0) 
    {
        cx = double( pt2.x - pt1.x ) * double( E_MIN - pt1.y ) / double( pt2.y - pt1.y ) + pt1.x;
        if ( ( cx >= E_MIN ) && ( cx <= E_MAX ) )
        {
            pPt->x = DoubleToInt(cx);
            pPt->y = E_MIN;	
            return( 1 );  /* エリア内に収まったら終了 */
        }
    }

    /* ウィンドウの下端より外側にある */
    if ( ( code & E_BOTTOM ) != 0 ) {
        cx = double( pt2.x - pt1.x ) * double( E_MAX - pt1.y ) / double( pt2.y - pt1.y ) + pt1.x;
        if( ( cx >= E_MIN ) && ( cx <= E_MAX ) )
        {
            pPt->x = DoubleToInt(cx);
            pPt->y = E_MAX;
            return( 1 );  /* エリア内に収まったら終了 */
        }
    }

    return( -1 );  /* クリッピングされなかった場合、線分は完全に不可視 */
}

/* クリッピングメインルーチン
>0 ... クリッピングされた
0  ... クリッピングの必要なし
<0 ... 線分は完全に不可視 */
int CDrawingView::_Clipping( POINT* pPt1, POINT* pPt2) const
{
    int code0,code1; /* 端点分類コード */

    code0 = _ClipPos( *pPt1);  /* 始点の端点分類コードを求める */
    code1 = _ClipPos( *pPt2);  /* 終点の端点分類コードを求める */

                                        /* 端点分類コードがどちらも0000ならばクリッピングは必要なし */
    if ((code0 == 0) && (code1 == 0))
    {
        return(0);
    }
    /* 始点･終点の端点分類コードの論理積が非０ならば線分は完全に不可視 */
    if ((code0 & code1) != 0)
    {
        return(-1);
    }

    /* 始点のクリッピング */
    if (code0 != 0)
    {
        if (_CalcClippedPoint(code0, *pPt1, *pPt2, pPt1) < 0)
        {
            return(-1);
        }
    }
    /* 終点のクリッピング */
    if (code1 != 0)
    {
        if (_CalcClippedPoint(code1, *pPt1, *pPt2, pPt2) < 0)
        {
            return(-1);
        }
    }
    return( 1 );
}
/**
 * @brief   直線描画
 * @param   [in] pt1
 * @param   [in] pt2
 * @retval  なし
 * @note
 */
void CDrawingView::Line(POINT pt1, POINT pt2) const
{ 
    if (_Clipping(&pt1, &pt2) < 0)
    {
        return;
    }

    if(m_eMode & DRAW_FIG)
    {
        m_psBack->Line(pt1, pt2);
    }

    if(m_eMode & DRAW_SEL)
    {
        m_psSelect->Line(pt1, pt2);
    }

    if(m_eMode & DRAW_INDEX)
    {
        m_psIndex->Line(pt1, pt2);
        m_psIndexHi->Line(pt1, pt2);
    }

    if ((m_eMode & DRAW_FRONT) ||
        (m_eMode & DRAW_PRINT))
    {
        m_pSurface->Line(pt1, pt2);
    }

#ifdef _CANVAS_DGB_
TRACE(_T("LINE(%d,%d) - (%d,%d) %x\n"),pt1.x,pt1.y,
                                       pt2.x,pt2.y,
                                       m_eMode);
#endif
}

void CDrawingView::MoveTo(POINT pt1)const
{ 
    if(m_eMode & DRAW_FIG)
    {
        m_psBack->MoveTo(pt1);
#ifdef _CANVAS_ADD_
        TRACE(_T("MoveTo(%d,%d) %x\n"),pt1.x,pt1.y, m_eMode);
#endif
    }

    if(m_eMode & DRAW_SEL)
    {
        m_psSelect->MoveTo(pt1);
    }

    if(m_eMode & DRAW_INDEX)
    {
        m_psIndex->MoveTo(pt1);
        m_psIndexHi->MoveTo(pt1);
    }

    if ((m_eMode & DRAW_FRONT) ||
        (m_eMode & DRAW_PRINT))
    {
        m_pSurface->MoveTo(pt1);
    }
}

void CDrawingView::LineTo(POINT pt2)const
{ 
    if(m_eMode & DRAW_FIG)
    {
        m_psBack->LineTo(pt2);
#ifdef _CANVAS_ADD_
        TRACE(_T("LineTo(%d,%d) %x\n"),pt1.x,pt1.y, m_eMode);
#endif
    }

    if(m_eMode & DRAW_SEL)
    {
        m_psSelect->LineTo(pt2);
    }

    if(m_eMode & DRAW_INDEX)
    {
        m_psIndex->LineTo(pt2);
        m_psIndexHi->LineTo(pt2);
    }

    if ((m_eMode & DRAW_FRONT) ||
        (m_eMode & DRAW_PRINT))
    {
        m_pSurface->LineTo(pt2);
    }
}

/**
 * @brief   楕円描画
 * @param   [in] rc
 * @retval  なし
 * @note
 */
void CDrawingView::Ellipse(RECT rc) const
{
    if(m_eMode & DRAW_FIG)
    {
        m_psBack->Ellipse(rc);
    }

    if(m_eMode & DRAW_SEL)
    {
        m_psSelect->Ellipse(rc);
    }

    if(m_eMode & DRAW_INDEX)
    {
        m_psIndex->Ellipse(rc);
        m_psIndexHi->Ellipse(rc);
    }

    if ((m_eMode & DRAW_FRONT) ||
        (m_eMode & DRAW_PRINT))
    {
        m_pSurface->Ellipse(rc);
    }
}

/**
 * @brief   楕円描画
 * @param   [in] rc
 * @retval  なし
 * @note
 */
void CDrawingView::FillEllipse(RECT rc) const
{
    if(m_eMode & DRAW_FIG)
    {
        m_psBack->FillEllipse(rc);
    }

    if(m_eMode & DRAW_SEL)
    {
        m_psSelect->FillEllipse(rc);
    }

    if(m_eMode & DRAW_INDEX)
    {
        m_psIndex->FillEllipse(rc);
        m_psIndexHi->FillEllipse(rc);
    }

    if ((m_eMode & DRAW_FRONT) ||
        (m_eMode & DRAW_PRINT))
    {
        m_pSurface->FillEllipse(rc);
    }
}

/**
 * @brief   楕円描画
 * @param   [in] rc
 * @retval  なし
 * @note
 */
void CDrawingView::Rect(RECT rc) const
{
	if (m_eMode & DRAW_FIG)
	{
		m_psBack->Rect(rc);
	}

	if (m_eMode & DRAW_SEL)
	{
		m_psSelect->Rect(rc);
	}

	if (m_eMode & DRAW_INDEX)
	{
		m_psIndex->Rect(rc);
		m_psIndexHi->Rect(rc);
	}

	if ((m_eMode & DRAW_FRONT) ||
		(m_eMode & DRAW_PRINT))
	{
		m_pSurface->Rect(rc);
	}
}

/**
 * @brief   楕円描画
 * @param   [in] rc
 * @retval  なし
 * @note
 */
void CDrawingView::FillRect(RECT rc) const
{
	if (m_eMode & DRAW_FIG)
	{
		m_psBack->FillRect(rc);
	}

	if (m_eMode & DRAW_SEL)
	{
		m_psSelect->FillRect(rc);
	}

	if (m_eMode & DRAW_INDEX)
	{
		m_psIndex->FillRect(rc);
		m_psIndexHi->FillRect(rc);
	}

	if ((m_eMode & DRAW_FRONT) ||
		(m_eMode & DRAW_PRINT))
	{
		m_pSurface->FillRect(rc);
	}
}

/**
 * @brief   楕円描画
 * @param   [in] rc
 * @param   [in] pt1
 * @param   [in] pt2
 * @retval  なし
 * @note
 */
void CDrawingView::Arc(RECT rc, POINT pt1, POINT pt2, bool bClockWise) const
{
    if(m_eMode & DRAW_FIG)
    {
        m_psBack->Arc(rc, pt1, pt2, bClockWise);
    }

    if(m_eMode & DRAW_SEL)
    {
        m_psSelect->Arc(rc, pt1, pt2, bClockWise);
    }

    if(m_eMode & DRAW_INDEX)
    {
        m_psIndex->Arc(rc, pt1, pt2, bClockWise);
        m_psIndexHi->Arc(rc, pt1, pt2, bClockWise);
    }

    if ((m_eMode & DRAW_FRONT) ||
        (m_eMode & DRAW_PRINT))
    {
        m_pSurface->Arc(rc, pt1, pt2, bClockWise);
    }
}


/**
 * @brief   楕円描画
 * @param   [in] rc
 * @param   [in] pt1
 * @param   [in] pt2
 * @retval  なし
 * @note
 */
void CDrawingView::ArcTo(RECT rc, POINT pt1, POINT pt2, bool bClockWise) const
{
    if(m_eMode & DRAW_FIG)
    {
        m_psBack->Arc(rc, pt1, pt2, bClockWise, true);
    }

    if(m_eMode & DRAW_SEL)
    {
        m_psSelect->Arc(rc, pt1, pt2, bClockWise, true);
    }

    if(m_eMode & DRAW_INDEX)
    {
        m_psIndex->Arc(rc, pt1, pt2, bClockWise, true);
        m_psIndexHi->Arc(rc, pt1, pt2, bClockWise,  true);
    }

    if ((m_eMode & DRAW_FRONT) ||
        (m_eMode & DRAW_PRINT))
    {
        m_pSurface->Arc(rc, pt1, pt2, bClockWise, true);
    }
}

/**
 * @brief   パス開始
 * @param   なし
 * @retval  なし
 * @note
 */
void CDrawingView::BeginPath() const
{
    if(m_eMode & DRAW_FIG)
    {
        ::SetPolyFillMode(m_psBack->GetDc(), WINDING);
        ::SetBkMode(m_psBack->GetDc() , TRANSPARENT);
        ::BeginPath(m_psBack->GetDc());
    }

    if(m_eMode & DRAW_SEL)
    {
        ::SetPolyFillMode(m_psSelect->GetDc(), WINDING);
        ::SetBkMode(m_psSelect->GetDc() , TRANSPARENT);
        ::BeginPath(m_psSelect->GetDc());
    }

    if(m_eMode & DRAW_INDEX)
    {
        ::SetPolyFillMode(m_psIndex->GetDc(), WINDING);
        ::SetBkMode(m_psIndex->GetDc() , TRANSPARENT);
        ::BeginPath(m_psIndex->GetDc());

        ::SetPolyFillMode(m_psIndexHi->GetDc(), WINDING);
        ::SetBkMode(m_psIndexHi->GetDc() , TRANSPARENT);
        ::BeginPath(m_psIndexHi->GetDc());
    }

    if ((m_eMode & DRAW_FRONT) ||
        (m_eMode & DRAW_PRINT))
    {
        ::SetPolyFillMode(m_hDc, WINDING);
        ::SetBkMode(m_hDc , TRANSPARENT);
        ::BeginPath(m_hDc);
    }
}
/**
 * @brief   ポリライン描画
 * @param   [in] lstDot
 * @retval  なし
 * @note
 */
void CDrawingView::CloseFigure() const
{
    if(m_eMode & DRAW_FIG)
    {
        ::CloseFigure(m_psBack->GetDc());
    }

    if(m_eMode & DRAW_SEL)
    {
        ::CloseFigure(m_psSelect->GetDc());
    }

    if(m_eMode & DRAW_INDEX)
    {
        ::CloseFigure(m_psIndex->GetDc());
        ::CloseFigure(m_psIndexHi->GetDc());
    }

    if ((m_eMode & DRAW_FRONT) ||
        (m_eMode & DRAW_PRINT))
    {
        ::CloseFigure(m_hDc);
    }
}


/**
 * @brief   ポリライン描画
 * @param   [in] lstDot
 * @retval  なし
 * @note
 */
void CDrawingView::EndPath() const
{
    if(m_eMode & DRAW_FIG)
    {
        ::CloseFigure(m_psBack->GetDc());
        ::EndPath(m_psBack->GetDc());
    }

    if(m_eMode & DRAW_SEL)
    {
        ::CloseFigure(m_psSelect->GetDc());
        ::EndPath(m_psSelect->GetDc());
    }

    if(m_eMode & DRAW_INDEX)
    {
        ::CloseFigure(m_psIndex->GetDc());
        ::EndPath(m_psIndex->GetDc());

        ::CloseFigure(m_psIndexHi->GetDc());
        ::EndPath(m_psIndexHi->GetDc());
    }

    if ((m_eMode & DRAW_FRONT) ||
        (m_eMode & DRAW_PRINT))
    {
        ::CloseFigure(m_hDc);
        ::EndPath(m_hDc);
    }
}

void CDrawingView::StrokePath() const
{
    if(m_eMode & DRAW_FIG)
    {
        ::StrokePath(m_psBack->GetDc());
    }

    if(m_eMode & DRAW_SEL)
    {
        ::StrokePath(m_psSelect->GetDc());
    }

    if(m_eMode & DRAW_INDEX)
    {
        ::StrokePath(m_psIndex->GetDc());
        ::StrokePath(m_psIndexHi->GetDc());
    }

    if ((m_eMode & DRAW_FRONT) ||
        (m_eMode & DRAW_PRINT))
    {
        ::StrokePath(m_hDc);
    }
}


void CDrawingView::_FillPath(HBRUSH hBr, int    iId) const
{
    HGDIOBJ hBrOld = 0;

    if(m_eMode & DRAW_FIG)
    {
        BOOL bRet;
        hBrOld = ::SelectObject(m_psBack->GetDc(), hBr);
#ifdef _DEBUG_DISP
        //::FillPath(m_psBack->GetDc());
        std::vector<POINT> lstPt;
        std::vector<BYTE>  lstByte;
        int nCount = ::GetPath(m_psBack->GetDc(), NULL, NULL, 0);
        if (nCount > 0)
        {
            lstPt.resize(nCount);
            lstByte.resize(nCount);
            ::GetPath(m_psBack->GetDc(), &lstPt[0], &lstByte[0], nCount);

            DB_PRINT(_T("--FillPath-- \n"));
            for(int iCnt = 0; iCnt < nCount; iCnt++)
            {
                DB_PRINT(_T("(%d,%d) [%d] \n"), lstPt[iCnt].x, lstPt[iCnt].y, lstByte[iCnt]);
            }
        }
#endif

        bRet = ::FillPath(m_psBack->GetDc());
        ::SelectObject(m_psBack->GetDc(), hBrOld);
    }

    if(m_eMode & DRAW_SEL)
    {
        hBrOld = ::SelectObject(m_psSelect->GetDc(), hBr);
        //::StrokeAndFillPath(m_psSelect->GetDc());
        ::FillPath(m_psSelect->GetDc());

        ::SelectObject(m_psSelect->GetDc(), hBrOld);
    }

    if(m_eMode & DRAW_INDEX)
    {
        int iIdHi = (iId >> 24);

        HBRUSH hBrIndex = CreateSolidBrush(iId);
        hBrOld = ::SelectObject(m_psIndex->GetDc(), hBrIndex);
        //::StrokeAndFillPath(m_psIndex->GetDc());
        ::FillPath(m_psIndex->GetDc());
        ::SelectObject(m_psIndex->GetDc(), hBrOld);
        ::DeleteObject(hBrIndex);

        HBRUSH hBrIndexHi = CreateSolidBrush(iIdHi);
        hBrOld = ::SelectObject(m_psIndexHi->GetDc(), hBrIndex);
        //::StrokeAndFillPath(m_psIndexHi->GetDc());
        ::FillPath(m_psIndexHi->GetDc());
        ::SelectObject(m_psIndexHi->GetDc(), hBrOld);
        ::DeleteObject(hBrIndexHi);
    }

    if ((m_eMode & DRAW_FRONT) ||
        (m_eMode & DRAW_PRINT))
    {
        hBrOld = ::SelectObject(m_hDc, hBr);
        //::StrokeAndFillPath(m_hDc);
        ::FillPath(m_hDc);
        ::SelectObject(m_hDc, hBrOld);
    }

    ::DeleteObject(hBr);
}

void CDrawingView::FillPath(COLORREF crFill, int    iId) const
{
    HBRUSH hBr = ::CreateSolidBrush(crFill);
    _FillPath(hBr, iId);
    if (hBr)
    {
        ::DeleteObject(hBr);
    }
}

void CDrawingView::HatchingPath(int fnStyle, COLORREF color, int    iId) const
{
    HBRUSH hBr = ::CreateHatchBrush(fnStyle, color);
    _FillPath(hBr, iId);

    if (hBr)
    {
        ::DeleteObject(hBr);
    }
}


/**
 * @brief   ポリライン描画
 * @param   [in] lstDot
 * @retval  なし
 * @note
 */
void CDrawingView::Polyline(const std::vector<POINT>& lstDot, bool bSkipStartPoint) const
{
    if (bSkipStartPoint)
    {
        bool bfirst = true;
        foreach(const POINT& pt, lstDot)
        {
            if (bfirst)
            {
                bfirst = false;
                continue;
            }

            if(m_eMode & DRAW_FIG)
            {
                m_psBack->LineTo( pt);
            }

            if(m_eMode & DRAW_SEL)
            {
                m_psSelect->LineTo(pt);
            }

            if(m_eMode & DRAW_INDEX)
            {
                m_psIndex->LineTo( pt);
                m_psIndexHi->LineTo( pt);
            }

            if ((m_eMode & DRAW_FRONT) ||
                (m_eMode & DRAW_PRINT))
            {
                m_pSurface->LineTo( pt);
            }
        }
    }
    else
    {
        int iDotSize = static_cast<int>(lstDot.size());
        if(m_eMode & DRAW_FIG)
        {
            m_psBack->Polyline(lstDot);
        }

        if(m_eMode & DRAW_SEL)
        {
            m_psSelect->Polyline(lstDot);
        }

        if(m_eMode & DRAW_INDEX)
        {
            m_psIndex->Polyline(lstDot);
            m_psIndexHi->Polyline(lstDot);
        }

        if ((m_eMode & DRAW_FRONT) ||
            (m_eMode & DRAW_PRINT))
        {
            m_pSurface->Polyline(lstDot);
        }
    }
}


void CDrawingView::FillPolyline(const std::vector<POINT>& lstDot) const
{
    if(m_eMode & DRAW_FIG)
    {
        m_psBack->FillPolyline(lstDot);
    }

    if(m_eMode & DRAW_SEL)
    {
        m_psSelect->FillPolyline(lstDot);
    }

    if(m_eMode & DRAW_INDEX)
    {
        m_psIndex->FillPolyline(lstDot);
        m_psIndexHi->FillPolyline(lstDot);
    }
    
    if ((m_eMode & DRAW_FRONT) ||
        (m_eMode & DRAW_PRINT))
    {
        m_pSurface->FillPolyline(lstDot);
    }
}

/**
 * @brief   イメージ描画
 * @param   [in] pt
 * @param   [in] pImage
 * @retval  なし
 * @note
 */
void CDrawingView::Image(POINT pt, CImageData* pImage, int iNo) const
{
    if(m_eMode & DRAW_FIG)
    {
        pImage->Draw(m_psBack->GetDc(), pt, iNo);
    }

    if(m_eMode & DRAW_SEL)
    {
        pImage->Draw(m_psSelect->GetDc(), pt, iNo);
    }

    if(m_eMode & DRAW_INDEX)
    {
        pImage->DrawFill(m_psIndex->GetDc(), pt, m_psIndex->GetColor());
        pImage->DrawFill(m_psIndexHi->GetDc(), pt, m_psIndexHi->GetColor());
}
    
    if ((m_eMode & DRAW_FRONT) ||
        (m_eMode & DRAW_PRINT))
    {
        pImage->Draw(m_hDc, pt, iNo);
    }
}

void CDrawingView::Image(POINT pt, CImageData* pImage, 
              int iNo,
              double dSclX,
              double dSclY,
              double dAngle,
              DWORD dwTrans) const
{


    if (IsPrintMode())
    {
        double dY = GetDeviceCaps(m_psBack->GetDc(), LOGPIXELSY);
        double dPrintY = THIS_APP->GetPrintPpiY();
        double dY2 = GetDeviceCaps(m_hDc, LOGPIXELSY);

        dSclX = dSclX * (dPrintY / dY);
        dSclY = dSclY * (dPrintY / dY);
    }


    if(m_eMode & DRAW_FIG)
    {
        pImage->Draw(m_psBack->GetDc(), pt, 
                                       iNo, 
                                       dSclX, 
                                       dSclY, 
                                       dAngle, 
                                       dwTrans);
    }

    if(m_eMode & DRAW_SEL)
    {
        pImage->Draw(m_psSelect->GetDc(), pt,
                                       iNo, 
                                       dSclX, 
                                       dSclY, 
                                       dAngle, 
                                       dwTrans);
    }

    if(m_eMode & DRAW_INDEX)
    {
        pImage->DrawFill(m_psIndex->GetDc(), pt, 
                                            m_psIndex->GetColor(), 
                                            dSclX, 
                                            dSclY, 
                                            dAngle);
        pImage->DrawFill(m_psIndexHi->GetDc(), pt,
                                                m_psIndexHi->GetColor(), 
                                                dSclX, 
                                                dSclY, 
                                                dAngle);
    }
    
    if ((m_eMode & DRAW_FRONT) ||
        (m_eMode & DRAW_PRINT))
    {
        pImage->Draw(m_hDc, pt, 
                            iNo, 
                            dSclX, 
                            dSclY, 
                            dAngle, 
                            dwTrans);
     }
}

void CDrawingView::ImageBounds(POINT pt, CImageData* pImage, 
              double dSclX,
              double dSclY,
              double dAngle) const
{
    if(m_eMode & DRAW_FIG)
    {
        pImage->DrawRect(m_psBack->GetDc(), pt, 
                                       dSclX, 
                                       dSclY, 
                                       dAngle);
    }

    if(m_eMode & DRAW_SEL)
    {
        pImage->DrawRect(m_psSelect->GetDc(), pt,
                                       dSclX, 
                                       dSclY, 
                                       dAngle);
    }

    if(m_eMode & DRAW_INDEX)
    {
        /*
        pImage->DrawRect(m_psIndex->GetDc(), pt, 
                                            dSclX, 
                                            dSclY,
                                            dAngle);
        if (m_psIndexHi->GetColor() != 0)
        {
            pImage->DrawRect(m_psIndexHi->GetDc(), pt,
                                                    dSclX, 
                                                    dSclY, 
                                                    dAngle);
        }
        */
    }
    
    if ((m_eMode & DRAW_FRONT) ||
        (m_eMode & DRAW_PRINT))
    {
        pImage->DrawRect(m_hDc, pt, 
                            dSclX, 
                            dSclY, 
                            dAngle);
     }
}

/**
 * @brief   イメージ枠描画
 * @param   [in] pt
 * @param   [in] pImage
 * @retval  なし
 * @note
 */
void CDrawingView::ImageBounds(POINT pt, CImageData* pImage) const
{
    if(m_eMode & DRAW_FIG)
    {
        pImage->DrawRect(m_psBack->GetDc(), pt);
    }

    if(m_eMode & DRAW_SEL)
    {
        pImage->DrawRect(m_psSelect->GetDc(), pt);
    }

    if(m_eMode & DRAW_INDEX)
    {
        /*
        pImage->DrawRect(m_psIndex->GetDc(), pt);
        if (m_psIndexHi->GetColor() != 0)
        {
            pImage->DrawRect(m_psIndexHi->GetDc(), pt);
        }
        */
    }
    
    if ((m_eMode & DRAW_FRONT) ||
        (m_eMode & DRAW_PRINT))
    {
        pImage->DrawRect(m_hDc, pt);
    }
}


void CDrawingView::ImageFill(POINT pt, CImageData* pImage, 
              double dSclX,
              double dSclY,
              double dAngle,
              COLORREF cr) const
{
    if(m_eMode & DRAW_FIG)
    {
        pImage->DrawFill(m_psBack->GetDc(), pt,
                                       cr,
                                       dSclX, 
                                       dSclY, 
                                       dAngle);
    }

    if(m_eMode & DRAW_SEL)
    {
        pImage->DrawFill(m_psSelect->GetDc(), pt,
                                       cr,
                                       dSclX, 
                                       dSclY, 
                                       dAngle);
    }

    if(m_eMode & DRAW_INDEX)
    {
        //pView->SetIndexBufferId(m_nId);で設定
        COLORREF crId = m_psIndex->GetColor();
        pImage->DrawFill(m_psIndex->GetDc(), pt, 
                                            crId,
                                            dSclX, 
                                            dSclY,
                                            dAngle);
        if (m_psIndexHi->GetColor() != 0)
        {
            COLORREF crIdHi = m_psIndexHi->GetColor();

            pImage->DrawFill(m_psIndexHi->GetDc(), pt,
                                                   crIdHi,
                                                   dSclX, 
                                                   dSclY, 
                                                   dAngle);
        }
    }
    
    if ((m_eMode & DRAW_FRONT) ||
        (m_eMode & DRAW_PRINT))
    {
        pImage->DrawFill(m_hDc, pt, 
                            cr,
                            dSclX, 
                            dSclY, 
                            dAngle);
     }
}

/**
 * @brief   イメージ描画
 * @param   [in] pt
 * @param   [in] pImage
 * @retval  なし
 * @note
 */
void CDrawingView::Text(const CExtText* pText,
                        POINT pt, 
                        const TEXT_PARAM* pParam,
                        int iLayerId)
{
    FONT_WORK_DATA work;
    work.bFirstFrameCallback = true;
    
    TEXT_PARAM param = *pParam;

    param.bIndex = false;

    double dScl;
    if (pText->IsFixDispSize())
    {
        dScl = 1.0;
    }
    else
    {
        dScl = pParam->dScl;
    }

    if(m_eMode & DRAW_FIG)
    {
        work.bPrint   = false;
        work.hOldFont = pText->DrawTest(&work, m_psBack->GetDc(), 
            this,
            dScl, 
            pParam->bEmphasis);

        pText->Draw(m_psBack->GetDc(), 
            this,
            pt, 
            &work,
            &param,
            iLayerId);
        if (work.bFirstFrameCallback)
        {
            work.bFirstFrameCallback = false;
        }
    }

    if(m_eMode & DRAW_SEL)
    {
        work.Clear();
        work.hOldFont = pText->DrawTest(&work, m_psBack->GetDc(),
            this,
            dScl, 
            pParam->bEmphasis);
        pText->Draw(m_psSelect->GetDc(), 
            this,
            pt, 
            &work,
            &param,
            iLayerId);

        if (work.bFirstFrameCallback)
        {
            work.bFirstFrameCallback = false;
        }
    }

    if ((m_eMode & DRAW_FRONT) ||
        (m_eMode & DRAW_PRINT))
    {
        work.Clear();
        work.bPrint = IsPrintMode();
        work.hOldFont = pText->DrawTest(&work, m_psBack->GetDc(), 
            this,
            dScl, 
            pParam->bEmphasis);

        pText->Draw(m_hDc, 
            this,
            pt,
            &work,
            &param,
            iLayerId);

        if (work.bFirstFrameCallback)
        {
            work.bFirstFrameCallback = false;
        }
    }

    if(m_eMode & DRAW_INDEX)
    {
        param.bIndex = true;
        work.Clear();
        COLORREF crIndex = static_cast<COLORREF>(param.iId);
        COLORREF crIndexHi = 0;
        if (crIndex > 0x00FFFFFF)
        {
            crIndexHi = (0xFF000000 & crIndex);
            crIndexHi = crIndexHi >> 24;
            crIndex = (crIndex & 0x00FFFFFF);
        }

        work.hOldFont = pText->DrawTest(&work, m_psBack->GetDc(),
            this,
            dScl, 
            pParam->bEmphasis);

        param.cr     = crIndex;
        pText->Draw(m_psIndex->GetDc(), 
            this,
            pt,
            &work,
            &param,
            iLayerId);

        if (work.bFirstFrameCallback)
        {
            work.bFirstFrameCallback = false;
        }

        {
            work.Clear();
            work.hOldFont = pText->DrawTest(&work, m_psBack->GetDc(),
                this,
                dScl, 
                pParam->bEmphasis);

            param.cr     = crIndexHi;
            pText->Draw(m_psIndexHi->GetDc(),
                this,
                pt,
                &work,
                &param,
                iLayerId);

            if (work.bFirstFrameCallback)
            {
                work.bFirstFrameCallback = false;
            }
        }
    }
    
}

/**
 * @brief   インデックス取得
 * @param   [in] pt
 * @param   [in] pImage
 * @retval  なし
 * @note
 */
int  CDrawingView::GetIndex(POINT pt) const
{
    DWORD    dwRet;
    COLORREF crIndex;
    COLORREF crIndexHi;
    crIndex   = ::GetPixel(m_psIndex->GetDc(), pt.x, pt.y); 
    crIndexHi = ::GetPixel(m_psIndexHi->GetDc(), pt.x, pt.y);

    dwRet = (crIndex & 0x00FFFFFF) | ((crIndexHi << 24) & 0xFF000000);
    return dwRet;
}

/**
 *  @brief  画面転送 バックバッファ選択バッファ
 *  @param  [in]  なし
 *  @retval       なし
 *  @note
 */
void CDrawingView::FlipBackToSelect()
{
    HDC hDc = m_psSelect->GetDc();
    m_psBack->Flip(hDc);
}

/**
 *  @brief  画面転送 選択バッファ->表示画面
 *  @param  [in]  なし
 *  @retval       なし
 *  @note
 */
void CDrawingView::FlipSelectToFront()
{
    m_psSelect->Flip(m_hDc);
}


/**
 *  @brief  アクション再描画.
 *  @param  なし
 *  @retval なし
 *  @note   CPartsDefからのみ使用する
 */
void CDrawingView::RedrawAction()
{
    if (m_pCurAction)
    {
        m_pCurAction->Redraw();
    }
}

/**
 *  @brief  画面消去.
 *  @param  [in]    hDc   デバイスコンテキスト
 *  @retval         なし
 *  @note
 */
void CDrawingView::ClearDisp( bool bClearFront)
{

    if (m_pCurAction)
    {
        m_pCurAction->CancelMouseOver();
    }
    
    m_psBack->Clear();
    m_psSelect->Clear();
    m_psIndex->Clear();
    m_psIndexHi->Clear();

    RECT rcArea;
    rcArea.left  = 0;
    rcArea.top   = 0;
    rcArea.right = m_Vv.iWidth;
    rcArea.bottom = m_Vv.iHeight;

    if (bClearFront)
    {
        ::FillRect(m_hDc, &rcArea, m_brBack);
    }
}

/**
 * @brief   マウスオーバー解除
 * @param   [in]    pObj   解除オブジェクト         
 * @retval  なし
 * @note
 */
void  CDrawingView::CancelObject(CDrawingObject* pObj)
{
    if (m_pCurAction)
    {
        m_pCurAction->CancelMouseOver(pObj);
    }
}

/**
 * @brief   コンテキストメニュー表示
 * @param   [in]    point  クリック位置
 * @retval  true メニュー表示あり
 * @note
 */
bool  CDrawingView::OnContextMenu(POINT point)
{
    if (m_pCurAction)
    {
        return m_pCurAction->OnContextMenu(point);
    }
    return false;
}


/**
 * @brief   メニュー選択
 * @param   [in] nItemID   メニュー項目 ID 
 * @param   [in] nFlags    メニューのフラグの組み合わせが
 * @param   [in] hSysMenu  
 * @retval  なし
 * @note
 */
void CDrawingView::OnMenuSelect(UINT nItemID, UINT nFlags, HMENU hSysMenu)
{
    if (m_pCurAction)
    {
        return m_pCurAction->OnMenuSelect(nItemID, nFlags, hSysMenu);
    }
}

//Cut/Copyモード設定
void CDrawingView::SetCutMode(bool bCut)
{
    m_bCutMode = bCut;
}

//Cut/Copyモード取得
bool CDrawingView::GetCutMode() const
{
    return m_bCutMode;
}

/**
 * 画面操作モード
 * @param   [in]    eView   モード         
 * @param   [in]    bOveride  true: モードを割り込ませる 
 * @param   [in]    pParam       
 * @retval  なし
 * @note	CActionView派生クラスの呼び出し
 */
bool  CDrawingView::SetViewMode(VIEW_MODE eView, bool bOveride, void* pParam)
{
    std::map< VIEW_MODE, std::shared_ptr<CViewAction> >:: iterator it;
    

    CMockSketchView* pView = dynamic_cast<CMockSketchView*>(m_pView);
    STD_ASSERT(pView != 0);
    bool bRet = false;

    it = m_mapViewAction.find(eView);




    if (it != m_mapViewAction.end())
    {
        bool bInterrupt = m_pCurAction->IsInterrupt();

        CViewAction* pOldAction;
        pOldAction = m_pCurAction;


        if (m_pCurAction != it->second.get())
        {
            if (!bOveride || !bInterrupt)
            {
                m_pCurAction->Cancel(eView);
            }
        }

        m_pCurAction = it->second.get();

        bRet = m_pCurAction->SelAction(eView, pParam);

        if (bOveride)
        {
            if(bInterrupt)
            {
                m_pCurAction->SetInterrupt();
            }
        }
    }

    if (bRet)
    {
        m_eView = eView;
        pView->SetDlgBarMode(eView);
    }

    if (!bOveride)
    {
        m_eInterruptSelect = VIEW_NONE;
    }
    return bRet;
}


/**
 * 画面操作モード
 * @param   [in]    eView   モード         
 * @retval  なし
 * @note	CActionView派生クラスの呼び出し
 */
bool  CDrawingView::IntrruptSelect()
{
    
    CMockSketchView* pView = dynamic_cast<CMockSketchView*>(m_pView);
    STD_ASSERT(pView != 0);
    bool bRet = false;

    if (m_pCurAction)
    {
        m_eInterruptSelect = m_pCurAction->GetMode();
    }
    else
    {
        m_eInterruptSelect = VIEW_SELECT;
    }

    m_pCurAction = m_mapViewAction[VIEW_SELECT].get();
    if (m_pCurAction)
    {
        bRet = m_pCurAction->SelAction(VIEW_SELECT);
    }

    if (bRet)
    {
        m_eView = VIEW_SELECT;
        pView->SetDlgBarMode(m_eView);
    }

    return bRet;
}

bool  CDrawingView::RelIntrruptSelect()
{
    if (m_eInterruptSelect == VIEW_NONE)
    {
        return false;
    }

    std::map< VIEW_MODE, std::shared_ptr<CViewAction> >:: iterator it;
    
    //ダイアログバー設定

    CMockSketchView* pView = dynamic_cast<CMockSketchView*>(m_pView);
    STD_ASSERT(pView != 0);
    bool bRet = false;

    it = m_mapViewAction.find(m_eInterruptSelect);
    if (it != m_mapViewAction.end())
    {
        m_pCurAction = it->second.get();
        bRet = m_pCurAction->SelAction(m_eInterruptSelect);
    }
    else
    {
        return false;
    }

    if (bRet)
    {
        m_eView = m_eInterruptSelect;
        pView->SetDlgBarMode(m_eView);
    }

    m_eInterruptSelect = VIEW_NONE;

    return bRet;
}


/**
 * 画面操作モード取得
 * @param   なし         
 * @retval  画面操作モード
 * @note	
 */
VIEW_MODE  CDrawingView::GetViewMode() const
{
    return m_eView;
}

/**
 * 画面描画モード取得
 * @param   なし         
 * @retval  画面描画モード
 * @note	
 */
DRAWING_MODE  CDrawingView::GetDrawMode() const
{
    return m_eMode;
}

/**
 * @brief   インプットバー高さ設定
 * @param   [in] iHeight         
 * @retval  なし
 * @note	
 */
void CDrawingView::SetInputBarSize(int iHeight)
{
    m_iInputBarHeight = iHeight;
}


/**
 * @brief   マウス移動
 * @param   [in]    nFlags   仮想キー         
 * @param   [in]    point    マウス座標         
 * @retval  true:動作有り false: 動作なし
 * @note	
 */
bool CDrawingView::OnMouseMove(UINT nFlags, POINT point)
{
    MOUSE_MOVE_POS posMouse;
    posMouse.nFlags = nFlags;
    posMouse.ptSel = point;

    //MK_CONTROL   //Ctrl キーが押されている場合に設定します。
    //MK_LBUTTON   //マウスの左ボタンが押されている場合に設定します。
    //MK_MBUTTON   //マウスの中央ボタンが押されている場合に設定します。
    //MK_RBUTTON   //マウスの右ボタンが押されている場合に設定します。
    //MK_SHIFT     //Shift キーが押されている場合に設定します。

    STD_ASSERT(m_pCurAction != NULL);

    if (m_bMouseMove)
    {
        m_lstMouseMove.push_back(posMouse);
        return false;
    }

    m_bMouseMove = true;

    bool bRet;
    bRet = _MouseMove(posMouse);

    while(m_lstMouseMove.size() != 0)
    {
        if (m_lstMouseMove.size() == 1)
        {
            bRet = _MouseMove(m_lstMouseMove[0]);
        }
        m_lstMouseMove.pop_front();
    }

    m_bMouseMove = false;
    return bRet; 

}

bool CDrawingView::_MouseMove(MOUSE_MOVE_POS posMouse)
{
    bool bRet;

    m_psMarker->MouseMove(posMouse.ptSel);
	CheckPrintArea(posMouse.ptSel);
	CheckGrid(posMouse.ptSel);

    SetInputFocus();

    bRet = m_pCurAction->MovePoint(posMouse, CViewAction::ACT_MOVE);

    return bRet;
}

/**
 * @brief   マウス左ボタン押下
 * @param   [in]    nFlags   仮想キー         
 * @param   [in]    point    マウス座標         
 * @retval  true:動作有り false:なし
 * @note	
 */
bool CDrawingView::OnLButtonDown(UINT nFlags, POINT point)
{
    MOUSE_MOVE_POS posMouse;
    posMouse.nFlags = nFlags;
    posMouse.ptSel = point;
    m_bDrag = true;

    STD_ASSERT(m_pCurAction != NULL);
    m_pCurAction->CancelMouseOver();

    m_psMarker->LButtonDown( nFlags, point);
    return m_pCurAction->SelPoint(posMouse, CViewAction::ACT_LBUTTON_DOWN);
}

/**
 * @brief   マウス右ボタン押下
 * @param   [in]    nFlags   仮想キー         
 * @param   [in]    point    マウス座標         
 * @retval  true:動作有り false:なし
 * @note	
 */
bool CDrawingView::OnRButtonDown(UINT nFlags, POINT point)
{
    MOUSE_MOVE_POS posMouse;
    posMouse.nFlags = nFlags;
    posMouse.ptSel = point;

    STD_ASSERT(m_pCurAction != NULL);
    m_pCurAction->CancelMouseOver();

    return m_pCurAction->SelPoint(posMouse, CViewAction::ACT_RBUTTON_DOWN);
}

bool CDrawingView::OnSetCursor()
{
    if (m_psNodeMarker->OnSetCursor())
	{
		return true;
	}

    if (m_psMarker->OnSetCursor())
	{
		return true;
	}

	if (m_pCurAction->OnSetCursor())
	{
		return true;
	}

	return false;
}

/**
 * @brief   マウス左ボタン解放
 * @param   [in]    nFlags   仮想キー         
 * @param   [in]    point    マウス座標         
 * @retval  true:動作有り false: 動作なし
 * @note	
 */
bool CDrawingView::OnLButtonUp(UINT nFlags, POINT point)
{
    MOUSE_MOVE_POS posMouse;
    posMouse.nFlags = nFlags;
    posMouse.ptSel = point;

    m_bDrag = false;


    STD_ASSERT(m_pCurAction != NULL);
    m_psMarker->LButtonUp( nFlags, point);
    return m_pCurAction->SelPoint(posMouse, CViewAction::ACT_LBUTTON_UP);
}

/**
 * @brief   マウス右ボタン解放
 * @param   [in]    nFlags   仮想キー         
 * @param   [in]    point    マウス座標         
 * @retval  true:動作有り false: 動作なし
 * @note	
 */
bool CDrawingView::OnRButtonUp(UINT nFlags, POINT point)
{
    MOUSE_MOVE_POS posMouse;
    posMouse.nFlags = nFlags;
    posMouse.ptSel = point;

    STD_ASSERT(m_pCurAction != NULL);

    return m_pCurAction->SelPoint(posMouse, CViewAction::ACT_RBUTTON_UP);
}


/**
 * @brief   マウス左ボタンダブルクリック
 * @param   [in]    nFlags   仮想キー         
 * @param   [in]    point    マウス座標         
 * @retval  true:動作有り false: 動作なし
 * @note	
 */
bool CDrawingView::OnLButtonDblClick(UINT nFlags, POINT point)
{
    MOUSE_MOVE_POS posMouse;
    posMouse.nFlags = nFlags;
    posMouse.ptSel = point;

    //!< ビュー->データ変換
    return m_pCurAction->DblClick(posMouse);

    m_bDrag = false;
    return true;
}

bool CDrawingView::IsDrag() const
{
    return m_bDrag;
}

/**
 * @brief   アクションキャンセル
 * @param   [in]    なし         
 * @param   [in]    なし        
 * @retval  なし
 * @note	
 */
void CDrawingView::OnCancelAction()
{
    if (m_pCurAction)
    {
        m_pCurAction->Cancel(m_eView);
    }

}


/**
 * @brief   画面検索
 * @param   [in]    ptPos   画面座標         
 * @retval  オブジェクトID
 * @note	
 */
int CDrawingView::SearchPos(POINT ptPos, bool bIgnoreSpecialType) const
{
    //中心から時計回りに検索
    //
    //    20 21 22 23 24
    //    19 06 07 08 09
    //    18 05 00 01 10
    //    17 04 03 02 11
    //    16 15 14 13 12 

    //TODO:四角形になってない
    //DRAW_CONFIG->GetSnapSize() 表示単位系

    //double dSize = DRAW_CONFIG->GetSnapSize() * DRAW_CONFIG->dDpi / 25.4;
    double dSize;
    dSize = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->GetSnapSize()) * m_dDpu;
    int iSerchSize = CUtil::Round(dSize);

    int iDx[4] = { 0, -1, 0, 1};
    int iDy[4] = {-1,  0, 1, 0};

    int iX = 0;
    int iY = 0;
    POINT ptSerch;
    COLORREF crIndex;

    crIndex = GetIndex(ptPos); 
    if ((crIndex != 0) &&
        (crIndex != 0xffffffff))
    {
        return crIndex;
    }

    ptSerch.x = ptPos.x;
    ptSerch.y = ptPos.y;

    int iDir = 0;
    for (int iLoop = 1; iLoop < iSerchSize; iLoop++)
    {
        for(int iCnt = 0; iCnt < 2; iCnt++)
        {
            for(int iCnt = 0; iCnt < iLoop; iCnt++)
            {
                ptSerch.x +=  iDx[iDir];
                ptSerch.y +=  iDy[iDir]; 
                crIndex = GetIndex(ptSerch);
                if ((crIndex == 0) ||
                    (crIndex == 0xffffffff))
                {
                    continue;
                }

				if (bIgnoreSpecialType)
				{
					if ((crIndex == ID_PAPER_FRAME) ||
						(crIndex == ID_PRINT_FRAME) ||
						(crIndex == ID_GRID_DOT))
					{
						continue;
					}

				}

                return crIndex;
            }
            iDir++;
            if (iDir == 4)
            {
                iDir = 0;
            }
        }
    }
    return -1;
}

/**
 * @brief   画面検索
 * @param   [out]    pLstIndex   スナップ範囲内のオブジェクトリスト
 * @param   [in]     ptPos       画面座標
 * @retval  最も近いオブジェクトID
 * @note	
 */
int CDrawingView::SearchAllPos(std::set<int>* pLstIndex, POINT ptPos) const
{
     int iNearObj = -1;

    //double dSize = DRAW_CONFIG->GetSnapSize() * DRAW_CONFIG->dDpi / 25.4;
    double dSize;
    dSize = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->GetSnapSize()) * m_dDpu;
    int iSerchSize = CUtil::Round(dSize);



    int iDx[4] = { 0, -1, 0, 1};
    int iDy[4] = {-1,  0, 1, 0};

    int iX = 0;
    int iY = 0;
    POINT ptSerch;
    COLORREF crIndex;

    pLstIndex->clear();
    crIndex = GetIndex(ptPos); 
    if ((crIndex != 0) &&
        (crIndex != 0xffffffff))
    {
        pLstIndex->insert(crIndex);
        iNearObj = crIndex;
    }

    ptSerch.x = ptPos.x;
    ptSerch.y = ptPos.y;

    int iDir = 0;
    for (int iLoop = 1; iLoop < iSerchSize; iLoop++)
    {
        for(int iCnt = 0; iCnt < 2; iCnt++)
        {
            for(int iCnt = 0; iCnt < iLoop; iCnt++)
            {
                ptSerch.x +=  iDx[iDir];
                ptSerch.y +=  iDy[iDir]; 
                crIndex = GetIndex(ptSerch);
                if ((crIndex == 0) ||
                    (crIndex == 0xffffffff))
                {
                    continue;
                }
                if (iNearObj == -1)
                {
                    iNearObj = crIndex;
                }
                pLstIndex->insert(crIndex);
            }
            iDir++;
            if (iDir == 4)
            {
                iDir = 0;
            }
        }
    }
    return iNearObj;
}

/**
 * @brief   スナップ位置取得
 * @param   [out]    pSnapType   スナップ種別
 * @param   [out]    pPos        スナップ位置
 * @param   [out]    pObj1Id     スナップ対象1
 * @param   [out]    pObj2Id     スナップ対象2(交点の時使用)
 * @param   [in]     dwSnaptype  設定スナップ種別
 * @param   [in]     ptPos       画面座標
 * @param   [in]     bIngnoreGroup  グループ内部を検索
 * @retval  false 
 * @note	
 *          交点  特徴点()   線上 の順に調べる
 *
 */
/*
bool CDrawingView::GetSnapPoint(int* pSnapType,
                                POINT2D*  pPos,  
                                int* pObj1Id,
                                int* pObj2Id,
                                DWORD dwSnapType,
                                POINT ptPos,
                                bool bIngnoreGroup) const
*/
bool CDrawingView::GetSnapPoint(SNAP_DATA* pSnapData,
                                DWORD dwSnapType,
                                POINT ptPos,
                                bool bIngnoreGroup,
                                bool bKeepCenter,
                                int iIgnoreObjectId) const
{
    return GetSnapPointWithAddFunc(
                                NULL,
                                pSnapData,
                                dwSnapType,
                                ptPos,
                                nullptr,
								bIngnoreGroup,
                                bKeepCenter,
                                iIgnoreObjectId);

}

bool CDrawingView::GetSnapPointWithAddFunc(
                                std::list<SNAP_DATA>* pAdditonalList, //out func で追加するスナップ点出力
                                SNAP_DATA* pSnapData,
                                DWORD dwSnapType,
                                POINT ptPos,
                                std::function< bool (std::list<SNAP_DATA>*, CDrawingObject*) > func,
								bool bIngnoreGroup,
                                bool bKeepCenter,
                                int iIgnoreObjectId) const

{
    int           iNearObject  = -1;
    int           iNearObject1 = -1;
    int           iNearObject2 = -1;
    double        dMinLen = DBL_MAX;
    POINT2D       ptMin;
    POINT2D       pt2d;
    using namespace VIEW_COMMON;

    
    double dSnapSize;
    dSnapSize =  GetSnapRadius();

    ConvScr2World(&pt2d, ptPos, GetCurrentLayerId());

    E_SNAP_TYPE eSnap = SNP_NONE;
    pSnapData->ptOrg = pt2d;

    // DT_IMAGGEの扱いについて
    // 四隅と中心点の時のみスナップ対象として扱う

    CPartsDef* pParts = GetPartsDef();

    STD_ASSERT(pParts);

    if (!pParts)
    {
        return false;
    }

	bool bGrid = false;
    iNearObject =  SearchPos(ptPos);

    if (iNearObject == ID_GRID_DOT)
    {
        bGrid = true;
    }
    else
    {
        if (iNearObject == iIgnoreObjectId)
        {
            iNearObject = -1;
        }
    }

    auto pObj  = pParts->GetObjectById(iNearObject, bIngnoreGroup);

    const MAT2D* pMat = NULL;
    if (pObj)
    {
        pMat = pObj->GetParentMatrix();
    }


    //点と接点は最優先とする
    if (pObj)
    {
        bool bPoint = false;
        auto eType = pObj->GetType();
        if (eType == DT_POINT)
        {
            auto pPoint = std::dynamic_pointer_cast<CDrawingPoint>(pObj);
            STD_ASSERT(pPoint);
            if (pPoint)
            {
                ptMin = *pPoint->GetPointInstance();
                pSnapData->eSnapType = SNP_POINT;
                bPoint = true;
            }
        }
        else if (eType == DT_NODE)
        {
            auto pNode = std::dynamic_pointer_cast<CDrawingNode>(pObj);
            STD_ASSERT(pNode);
            if (pNode)
            {
                ptMin = pNode->GetPoint();
                pSnapData->eSnapType = SNP_NODE;
                bPoint = true;
            }
        }

        if (bPoint)
        {
            if (pMat)
            {
                ptMin.Matrix(*pMat);
            }

            pSnapData->pt = ptMin;
            pSnapData->iObject1 = iNearObject;
            pSnapData->iObject2 = -1;
            return true;
        }
    }

    
    if (dwSnapType & SNP_CROSS_POINT)
    {
        // 交点を調べる
        // 全交点をリストアップし一番近いものを選ぶ
        std::vector<POINT2D> lstResult;
        std::set<int> lstIndex;
        std::set<int>::iterator iteSet;
        std::set<int>::iterator iteStart;
        int iObj2;

        iNearObject = SearchAllPos(&lstIndex, ptPos);
        if (lstIndex.size() > 1)
        {
            iteStart = lstIndex.begin();
            foreach(int iObj1, lstIndex)
            {
                iteStart++;

				if (iObj1 == ID_GRID_DOT)
				{
					bGrid = true;
				}

                auto pObj1  = pParts->GetObjectById(iObj1, bIngnoreGroup);
                if (!pObj1)
                {
                    continue;
                }

                std::vector<POINT2D> lstIntersection;
                for(iteSet  = iteStart;
                    iteSet != lstIndex.end();
                    iteSet++)
                {
                    iObj2 = *iteSet;
                    if(iObj2 == iObj1)
                    {
                        continue;
                    }

                    auto pObj2  = pParts->GetObjectById(iObj2, bIngnoreGroup);
                    if (!pObj2)
                    {
                        continue;
                    }
                    pObj2->CalcIntersection (&lstIntersection, pObj1.get());



                    foreach(POINT2D ptIntersection, lstIntersection)
                    {
                        double dLen = ptIntersection.Distance(pt2d);
                        if (dMinLen > dLen)
                        {
                            dMinLen = dLen;
                            iNearObject1 = iObj1;
                            iNearObject2 = iObj2;
                            ptMin = ptIntersection;
                        }
                    }

                    if(dSnapSize > dMinLen)
                    {
                        pSnapData->eSnapType = SNP_CROSS_POINT;
                        pSnapData->pt        = ptMin;
                        pSnapData->iObject1  = iNearObject1;
                        pSnapData->iObject2  = iNearObject2;

                        return true;
                    }
                }
            }
        }
    }


	if (bGrid)
	{
		auto obj = pParts->GetObjectById(ID_GRID_DOT, false);
		auto point = std::dynamic_pointer_cast<CDrawingPoint>(obj);
		pSnapData->eSnapType = SNP_GRID;
		pSnapData->pt = point->GetPoint();
		pSnapData->iObject1 = ID_GRID_DOT;
		pSnapData->iObject2 = -1;
		return true;
	}

    std::list<SNAP_DATA> lstSnap;

    if (pObj)
    {
        const MAT2D* pMat =  pObj->GetParentMatrix();

        bool bKeep = false;
        SNAP_DATA snapKeep;
        SNAP_DATA snapMin;
        // 特徴点を調べる
        if (pObj->GetSnapList(&lstSnap))
        {
            if(func && pAdditonalList)
            {
                func(pAdditonalList, pObj.get());
                lstSnap.insert(lstSnap.end(), 
                    pAdditonalList->begin(),
                    pAdditonalList->end());
            }

            foreach(SNAP_DATA snap, lstSnap)
            {
                if(!(snap.eSnapType & dwSnapType))
                {
                    continue;
                }

                if(pMat)
                {
                    snap.pt.Matrix(*pMat);
                }

                double dLen = snap.pt.Distance(pt2d);
                if (dMinLen > dLen)
                {
                    dMinLen = dLen;
                    snapMin = snap;
                    snapMin.iObject1 = iNearObject;
                    snapMin.iObject2 = -1;
                }

                if(snap.bKeepSelect && bKeepCenter)
                {
                    bKeep = true;
                    snapKeep = snap;
                    snapKeep.iObject1 = iNearObject;
                    snapKeep.iObject2 = -1;
                }
            }
        }

        if (dSnapSize > dMinLen)
        {
#if 0
StdString strObj;
if (snapMin.iObject1 != -1)
{
    auto pSelObj = pParts->GetObjectById(snapMin.iObject1, false /*bIgnoreGroup*/);
    if (pSelObj)
    {
        auto pObjTop = pSelObj->GetTopGroup();
        if (pObjTop)
        {
            strObj = pObjTop->Text();
        }
    }
}

DB_PRINT(_T("snapMin:%s Obj;%s\n"), SnapToText(snapMin).c_str(),     
                                     strObj.c_str());
#endif
            *pSnapData = snapMin;
             pSnapData->ptOrg = pt2d;

            return true;
        }

        if(bKeep)
        {
//DB_PRINT(_T("snapKeep:%s\n"), SnapToText(snapKeep).c_str());
            *pSnapData = snapKeep;
             pSnapData->ptOrg = pt2d;
            return true;
        }
    }

    if(dSnapSize < dMinLen)
    {
        iNearObject1 = -1;
    }

    // Image上でも図形がかけるようにする
    pObj  = pParts->GetObjectById(iNearObject);
    if (pObj)
    {
        if ( pObj->GetType() == DT_IMAGE)
        {
            pObj = NULL;
            iNearObject = -1;
            std::set<int> lstIndex;
            SearchAllPos(&lstIndex, ptPos);
            for(const int& iIndex: lstIndex)
            {
                auto pNextObj  = pParts->GetObjectById(iIndex);
                if (pNextObj->GetType() != DT_IMAGE)
                {
                    iNearObject = iIndex;
                    pObj = pNextObj;
                    break;
                }
            }
        }
    }

    int iId = m_psMarker->CheckCkickAndObject( ptPos , iNearObject);

    if (iId != -1 )
    {
        CDrawMarker::MARKER_DATA* pData;
        pData = m_psMarker->GetMarker(iId);

        if ( pData->eType == CDrawMarker::MARKER_CENTER)
        {
            if (dwSnapType & SNP_CENTER)
            {
                eSnap = SNP_CENTER;
                pSnapData->eSnapType = eSnap;
                pSnapData->pt      = pData->ptObject;
                pSnapData->iObject1     = pData->iObjct;
                pSnapData->iObject2     = -1;
                return true;
            }
        }
    }

    //線上の点を知らべる
    if ((iNearObject1 == -1)&&
        ((dwSnapType) & SNP_ON_LINE) &&
        (iNearObject != -1))
    {
        if (pObj)
        {

            const MAT2D* pMat =  pObj->GetParentMatrix();

            switch(pObj->GetType())
            {
            case DT_POINT:
                {
                    auto pPoint = std::dynamic_pointer_cast<CDrawingPoint>(pObj);
                    STD_ASSERT(pPoint);
                    if (pPoint)
                    {
                        if(pMat)
                        {
                            ptMin = *pPoint->GetPointInstance();
                            ptMin.Matrix(*pMat);
                        }
                        else
                        {
                            ptMin = *pPoint->GetPointInstance();
                        }

                    }
                    eSnap = SNP_ON_LINE;
                    iNearObject1 = iNearObject;
                    break;
                }

            case DT_NODE:
                {
                    auto pNode = std::dynamic_pointer_cast<CDrawingNode>(pObj);
                    STD_ASSERT(pNode);
                    if (pNode)
                    {
                        if(pMat)
                        {
                            ptMin = *pNode->GetPointInstance();
                            ptMin.Matrix(*pMat);
                        }
                        else
                        {
                            ptMin = *pNode->GetPointInstance();
                        }

                    }
                    eSnap = SNP_ON_LINE;
                    iNearObject1 = iNearObject;
                    break;
                }

            case DT_CIRCLE:
                {
                    auto pCircle = std::dynamic_pointer_cast<CDrawingCircle>(pObj);
                    STD_ASSERT(pCircle);
                    if (pCircle)
                    {
                        POINT2D ptOn;
                        if(pMat)
                        {
                            ELLIPSE2D* pEllipse;
                            CIRCLE2D circle(*pCircle->GetCircleInstance());
                            circle.Matrix(*pMat, &pEllipse);
                            if (pEllipse)
                            {
                                ptOn = pEllipse->NearPoint(pt2d, true);
                            }
                            else
                            {
                                ptOn = circle.NearPoint(pt2d, true);
                            }
                        }
                        else
                        {
                            ptOn = pCircle->GetCircleInstance()->NearPoint(pt2d, true);
                        }
                        if (ptOn.Distance(pt2d) < dSnapSize)
                        {
                            ptMin = ptOn;
                            eSnap = SNP_ON_LINE;
                            iNearObject1 = iNearObject;
                        }
                    }
                    break;
                }
            case DT_LINE:
                {
                    auto pLine = std::dynamic_pointer_cast<CDrawingLine>(pObj);
                    STD_ASSERT(pLine);
                    if (pLine)
                    {
                        POINT2D ptOn;

                        if(pMat)
                        {
                            LINE2D line(*pLine->GetLineInstance());
                            line.Matrix(*pMat);
                            ptOn = line.NearPoint(pt2d, true);
                        }
                        else
                        {
                            ptOn = pLine->GetLineInstance()->NearPoint(pt2d, true);
                        }

                        if (ptOn.Distance(pt2d) < dSnapSize)
                        {
                            ptMin = ptOn;
                            eSnap = SNP_ON_LINE;
                            iNearObject1 = iNearObject;
                        }
                    }
                    break;
                }
            case DT_SPLINE:
                {
                    auto pSpline = std::dynamic_pointer_cast<CDrawingSpline>(pObj);
                    STD_ASSERT(pSpline);
                    if (pSpline)
                    {
                        POINT2D ptOn;
                        if(pMat)
                        {
                            SPLINE2D spline(*pSpline->GetSplineInstance());
                            spline.Matrix(*pMat);
                            ptOn = spline.NearPoint(pt2d, true);
                        }
                        else
                        {
                            ptOn = pSpline->GetSplineInstance()->NearPoint(pt2d, true);
                        }

                        if (ptOn.Distance(pt2d) < dSnapSize)
                        {
                            ptMin = ptOn;
                            eSnap = SNP_ON_LINE;
                            iNearObject1 = iNearObject;
                        }
                    }
                    break;
                }
            case DT_ELLIPSE:
                {
                    auto pEllipse = std::dynamic_pointer_cast<CDrawingEllipse>(pObj);
                    STD_ASSERT(pEllipse);
                    if (pEllipse)
                    {
                        POINT2D ptOn;
                        if(pMat)
                        {
                            ELLIPSE2D ellipse(*pEllipse->GetEllipseInstance());
                            ellipse.Matrix(*pMat);
                            ptOn = ellipse.NearPoint(pt2d, true);
                        }
                        else
                        {
                            ptOn = pEllipse->GetEllipseInstance()->NearPoint(pt2d, true);
                        }
                        if (ptOn.Distance(pt2d) < dSnapSize)
                        {
                            ptMin = ptOn;
                            eSnap = SNP_ON_LINE;
                            iNearObject1 = iNearObject;
                        }
                    }
                    break;
                }
            case DT_COMPOSITION_LINE:
                {
                    auto pCompositionLine = std::dynamic_pointer_cast<CDrawingCompositionLine>(pObj);
                    STD_ASSERT(pCompositionLine);
                    if (pCompositionLine)
                    {
                        std::vector<POINT2D> lstPoint;
                        CDrawingPoint drawingPoint;
                        drawingPoint.SetPoint(pt2d);
                        pCompositionLine->CalcIntersection(&lstPoint, &drawingPoint, true);

                        if ((lstPoint.size() > 0) &&
                            (lstPoint[0].Distance(pt2d) < dSnapSize))
                        {
                            ptMin = lstPoint[0];
                            eSnap = SNP_ON_LINE;
                            iNearObject1 = iNearObject;
                        }
                    }
                    break;
                }
            case DT_CONNECTION_LINE:
                {
                    auto pConnectionLine = std::dynamic_pointer_cast<CDrawingConnectionLine>(pObj);
                    STD_ASSERT(pConnectionLine);
                    if (pConnectionLine)
                    {
						std::vector<POINT2D> lstPoint;
						CDrawingPoint drawingPoint;
						drawingPoint.SetPoint(pt2d);
						pConnectionLine->CalcIntersection(&lstPoint, &drawingPoint, true);

						if ((lstPoint.size() > 0) &&
							(lstPoint[0].Distance(pt2d) < dSnapSize))
						{
							ptMin = lstPoint[0];
							eSnap = SNP_ON_LINE;
							iNearObject1 = iNearObject;
						}
                    }
                }

            default:
                break;
            }
        }
    }


    if (iNearObject1 != -1)
    {
        pSnapData->eSnapType = eSnap;
        pSnapData->pt        = ptMin;
        pSnapData->iObject1  = iNearObject1;
        pSnapData->iObject2  = iNearObject2;
        return true;
    }
    else
    {
        pSnapData->eSnapType = VIEW_COMMON::SNP_NONE;
        pSnapData->pt        = pt2d;
        pSnapData->iObject1  = iNearObject;
        pSnapData->iObject2  = -1;
    }
    return false;
}

double CDrawingView::DotToLength(double dDot) const
{
	double dScl = GetCurrentLayer()->dScl * m_Vv.dDspScl * m_dDpu;
	if (dScl < NEAR_ZERO)
	{
		return 0.0;
	}
	double dLen = dDot / dScl;
	return dLen;
}

double CDrawingView::GetSnapMinLength() const
{
    DWORD dwSnapType = DRAW_CONFIG->GetSnapType();

    double dMinRezLen;
    double dScl = GetCurrentLayer()->dScl * m_Vv.dDspScl * m_dDpu;


    //最小ドットの長さ（表示単位系) 
    dMinRezLen = m_dMinResDot / dScl;
    double dN = ceil(log10(dMinRezLen));

    double dMinLin;
    dMinLin = pow(10, dN);
    return  dMinLin;
}

bool CDrawingView::GetSnapLength(double* pLen, double dLen) const
{
    DWORD dwSnapType = DRAW_CONFIG->GetSnapType();
    
    double dMinRezLen;
    double dScl =    GetCurrentLayer()->dScl * m_Vv.dDspScl * m_dDpu;


    //最小ドットの長さ（表示単位系) 
    dMinRezLen = m_dMinResDot / dScl;
    double dN =  ceil(log10(dMinRezLen));

    double dMinLin;
    dMinLin = pow(10,dN);

    double dSnapLen;
    //スナップされる距離
    dSnapLen = CUtil::Round(dLen / dMinLin) 
                        * dMinLin;

    *pLen = dSnapLen;


    double dSnapSize;
    dSnapSize =  GetSnapRadius();
    bool bSnapLen = false;

    if ( fabs(dSnapLen - dLen) <  dSnapSize)
    {
        bSnapLen = true;
    }
    return bSnapLen;
}


/**
 * @brief   長さによるスナップ位置取得
 * @param   [out]    dAngle      スナップ角
 * @param   [out]    dLength     スナップ長さ   
 * @param   [out]    pPos        スナップ位置
 * @param   [in]     ptStart     開始点
 * @param   [in]     ptMouse     マウスカーソル位置
 * @param   [in]     bAngle      角度スナップの有無
 * @param   [in]     bDistance   距離スナップの有無
 * @param   [in]     dOffsetAngle  角度オフセット
 * @param   [in]     dOffsetAngle  距離オフセット (円周上の垂線時に使用)

 * @retval  true スナップあり
 * @note	
 */
bool CDrawingView::GetLengthSnapPoint(double* pAngle,
                                      double* pLength,
                                      POINT2D*  pPos,  
                                      POINT2D   ptStart,  
                                      POINT2D   ptMouse,
                                      bool      bAngle,
                                      bool      bDistance,
                                      double    dOffsetAngle,
                                      double    dLengthOffset) const
{
   //スナップ角
    double dSnapAngle = 5.0;

    //長さの最小分解能(dot)

    DWORD dwSnapType = DRAW_CONFIG->GetSnapType();
    double dSnapSize;
    dSnapSize =  GetSnapRadius();

    double dDir = 1.0;
    POINT2D ptVec = ptMouse - ptStart;
    double dLength = ptVec.Length_A() - dLengthOffset;

    if (dLength < 0)
    { 
        dLength  = (-dLength);
        dDir = -1.0;
    }

    double dSnapLen = dLength;
    bool bSnapLen = false;
    if(bDistance)
    {
        bSnapLen = GetSnapLength(&dSnapLen, dLength);
    }

    double dSnapTotalLen = ((dSnapLen * dDir) + dLengthOffset);

    double dAngle;
    dAngle = CUtil::NormAngle((atan2(ptVec.dY, ptVec.dX)* RAD2DEG) - dOffsetAngle);

    double dC;
    double dS;
    double dAngle2;
    if (bAngle)
    {
        //スナップ角を求める
        dAngle2 = CUtil::Round(dAngle / dSnapAngle) * dSnapAngle;

        CUtil::GetTriFunc(dAngle2 + dOffsetAngle, &dS, &dC);

        POINT2D ptSnapAngle;

        //スナップ角上の点を求める
        ptSnapAngle.dX = dC * dSnapTotalLen + ptStart.dX;
        ptSnapAngle.dY = dS * dSnapTotalLen + ptStart.dY;

        double  dDistance = ptSnapAngle.Distance( ptMouse);
        if (dDistance < dSnapSize)
        {
            bAngle = true;
        }
        else
        {
            bAngle = false;
        }
    }
    else
    {
        CUtil::GetTriFunc(dAngle, &dS, &dC);
    }


    POINT2D ptRet;

    if (!bAngle)
    {
        if (bSnapLen)
        {
            if (dSnapLen < NEAR_ZERO)
            {
                *pAngle   = 0.0;
                *pLength  = 0.0;
                *pPos     = ptStart;
            }
            else
            {   

                ptVec.Normalize();
                ptRet = dSnapTotalLen * ptVec + ptStart;
                *pAngle   = dAngle;
                *pLength  = dSnapLen;
                *pPos     = ptRet;
            }
        }
        else
        {
            *pAngle   = dAngle;
            *pLength  = dLength;
            *pPos     = ptMouse;
            return false;
        }
    }
    else
    {
        if (bSnapLen)
        {
            if (dSnapLen < NEAR_ZERO)
            {
                *pAngle   = 0.0;
                *pLength  = 0.0;
                *pPos     = ptStart;
            }
            else
            {
                *pAngle   = dAngle2;
                POINT2D ptId(dC, dS); 
                ptRet = dSnapTotalLen * ptId + ptStart;

                *pLength  = dSnapLen;
                *pPos     = ptRet;
            }
        }
        else
        {
            *pAngle   = dAngle2;
            POINT2D ptId(dC, dS); 
            ptRet = dSnapTotalLen * ptId + ptStart;
            *pLength  = dLength;
            *pPos     = ptRet;
        }
    }

    return true;

}

/**
 * @brief   長さ固定時の位置取得
 * @param   [out]    dAngle      スナップ角
 * @param   [out]    pPos        スナップ位置   
 * @param   [in]     dLength     固定長
 * @param   [in]     ptStart     開始点
 * @param   [in]     ptMouse     マウスカーソル位置
 * @param   [in]     bSnap       スナップの有無
 * @retval  true スナップあり
 * @note	
 */
bool CDrawingView::GetFixLengthSnapPoint(double* pAngle,
                                         POINT2D*  pPos,  
                                      double dLength,
                                      POINT2D   ptStart,  
                                      POINT2D   ptMouse,
                                      bool bSnap) const
{
    //スナップ角
    double dSnapAngle = 5.0;

    DWORD dwSnapType = DRAW_CONFIG->GetSnapType();
    
    double dSnapSize;
    double dScl =    GetCurrentLayer()->dScl * m_Vv.dDspScl * m_dDpu;

    dSnapSize =  GetSnapRadius();

    POINT2D ptVec = ptMouse - ptStart;

    double dAngle;
    dAngle = atan2(ptVec.dY, ptVec.dX)* RAD2DEG;


    bool bSnapAngle = false;
    double dAngle2;
    POINT2D ptSnapAngle;
    double dC;
    double dS;


    if (bSnap)
    {
        //スナップ角を求める
        dAngle2 = CUtil::Round(dAngle / dSnapAngle) * dSnapAngle;
    }
    else
    {
        dAngle2 = dAngle;
    }

    CUtil::GetTriFunc(dAngle2, &dS, &dC);

    //スナップ角上の点を求める
    ptSnapAngle.dX = dC * dLength + ptStart.dX; 
    ptSnapAngle.dY = dS * dLength + ptStart.dY; 

    if (bSnap)
    {
        if (ptSnapAngle.Distance( ptMouse) < dSnapSize)
        {
            bSnapAngle = true;
        }
    }

    *pAngle = dAngle2;
    *pPos = ptSnapAngle;
    return bSnapAngle;
}


/**
 * @brief   長さによるスナップ位置取得
 * @param   [out]    dLength     スナップ長さ   
 * @param   [out]    pPos        スナップ位置
 * @param   [in]     dAngle      角度
 * @param   [in]     ptStart     開始点
 * @param   [in]     ptMouse     マウスカーソル位置
 * @param   [in]     bSnap       スナップの有無
 * @retval  true スナップあり
 * @note	
 */
bool CDrawingView::GetFixAngleSnapPoint(double* pLength,
                            POINT2D*  pPos,  
                            double    dAngle,
                            POINT2D   ptStart,  
                            POINT2D   ptMouse,
                            bool      bSnap,
	                        double dOffsetLength) const
  {
    DWORD dwSnapType = DRAW_CONFIG->GetSnapType();
    
    double dSnapSize;
    double dMinRezLen;
    double dScl =    GetCurrentLayer()->dScl * m_Vv.dDspScl * m_dDpu;

    dSnapSize =  GetSnapRadius();

    dMinRezLen = m_dMinResDot / dScl;
    double dN =  ceil(log10(dMinRezLen));

    double dMinLin;
    dMinLin = pow(10,dN);
    bool bSnapLen = false;


    double dS, dC;
    CUtil::GetTriFunc(dAngle, &dS, &dC);

    POINT2D ptVec = ptMouse - ptStart;
    POINT2D ptDir(dC, dS);

    double dLength = fabs(ptDir.Dot(ptVec) - dOffsetLength);

    //スナップされる距離
    double dSnapLen = CUtil::Round(dLength / dMinLin) 
                      * dMinLin;

    if (bSnap)
    {
        if ( fabs(dSnapLen - dLength) <  dSnapSize)
        {
            bSnapLen = true;
        }
    }

    POINT2D ptRet;
    bool bRet = false;
    if (bSnap)
    {
        if (fabs(dSnapLen) < NEAR_ZERO)
        {
            *pLength  = 0.0;
            *pPos     = ptStart;
            bRet = true;
        }
        else
        {
            POINT2D ptId(dC, dS); 
            ptRet = (dSnapLen + dOffsetLength ) * ptId + ptStart;

            *pLength  = dSnapLen;
            *pPos     = ptRet;
            bRet = true;
        }
    }
    else
    {
        POINT2D ptId(dC, dS); 
        ptRet = (dLength + dOffsetLength ) * ptId + ptStart;
        *pLength  = dLength;
        *pPos     = ptRet;
    }

    return bRet;
}


/**
 * @brief   中心点と一点を通る直線へのスナップ点
 * @param   [out]    pPos        スナップ位置
 * @param   [in]     ptCenter     開始点
 * @param   [in]     ptMid       方向指定点
 * @param   [in]     ptMouse     マウスカーソル位置
 * @retval  true スナップあり
 * @note	方向指定点側のみ
 */
bool CDrawingView::GetLineSnapPoint(  POINT2D*  pPos,  
                                      POINT2D   ptCenter,  
                                      POINT2D   ptMid,  
                                      POINT2D   ptMouse) const
  {

    double dRad = GetSnapRadius();


    LINE2D l(ptCenter, ptMid);

    double dDistance;
    dDistance = l.Distance(ptMouse);

    if (dDistance > dRad)
    {
        return false;
    }

    POINT2D ptNear;
    l.NearPoint(ptNear, ptMouse);

    double dPos;

    if(!l.PosPram(&dPos, ptMid, ptNear))
    {
        return false;
    }

    if (dPos > 0)
    {
        *pPos = ptNear;
        return true;
    }

    if (dPos < -1)
    {
        return false;
    }

    double dL = l.Distance(ptNear);
        
    if (dDistance <  dL / l.Length())
    {
        *pPos = ptNear;
        return true;
    }

    return false;
}

bool CDrawingView::GetInfLineSnapPoint(  POINT2D*  pPos,  
    POINT2D   ptCenter,  
    POINT2D   ptMid,  
    POINT2D   ptMouse) const
{

    double dRad = GetSnapRadius();


    LINE2D l(ptCenter, ptMid);

    double dDistance;
    dDistance = l.Distance(ptMouse);

    if (dDistance > dRad)
    {
        return false;
    }

    POINT2D ptNear;
    l.NearPoint(ptNear, ptMouse);

    double dPos;

    if(!l.PosPram(&dPos, ptMid, ptNear))
    {
        return false;
    }

    *pPos = ptNear;
    return true;
}
 


/**
 *  @brief   特徴点取得
 *  @param   [in]  posMouse        マウス位置
 *  @param   [in]  bIgnoreGroup    グループを無視して内部のオブジェクト
                                   にアクセスする。
 *  @retval  スナップ種別
 *  @note     
 */
bool CDrawingView::GetMousePoint(
                    SNAP_DATA* pSnap,
                    MOUSE_MOVE_POS posMouse,
                    bool bIgnoreGroup,
                    bool bKeepCenter,
                    int iIgnoreObjectId)           
{
    using namespace VIEW_COMMON;

    SNAP_DATA snap;

    bool bRet = true;
    bool bIgnoreSnap = false;


    if (posMouse.nFlags & MK_SHIFT)
    {
        bIgnoreSnap = true;
    }

    bool bSnap = false;

    bool bUseMousePos = true;
    if (!bIgnoreSnap)
    {
        DWORD dwSnapType = DRAW_CONFIG->GetSnapType();

        bSnap = GetSnapPoint(&snap,
                          dwSnapType,
                          posMouse.ptSel,
                          bIgnoreGroup,
                          bKeepCenter,
                          iIgnoreObjectId
        );


        if (snap.eSnapType != SNP_NONE) 
        {
            bUseMousePos = false; 
        }
    }
    else
    {
        int  iLayerId = GetCurrentLayerId();
        ConvScr2World(&snap.ptOrg, posMouse.ptSel, iLayerId);
        snap.pt = snap.ptOrg;
    }

    if (bUseMousePos)
    {
        snap.pt = snap.ptOrg;
    }
    *pSnap = snap;
    return bSnap;
}


bool CDrawingView::GetMousePoint2(
                    SNAP_DATA* pSnap,
                    MOUSE_MOVE_POS posMouse,
                    bool bIgnoreGroup,
                    bool bKeepCenter,
                    int iIgnoreObjectId)
{
    CPartsDef*   pDef  = GetPartsDef();
    CDrawMarker* pMarker= GetMarker();
    pMarker->Clear();
    pMarker->ClearObject();

    pDef->ClearMouseOver();
    pDef->ClearMouseEmphasis();
    pDef->ClearMouseConnectable();

	bool bSnap;
    bSnap = GetMousePoint(pSnap, posMouse, bKeepCenter, bIgnoreGroup);
    auto pObj1 = pDef->GetObjectById(pSnap->iObject1);
    auto pObj2 = pDef->GetObjectById(pSnap->iObject2);

    if ((pObj1 != NULL ) &&
        (pObj2 == NULL ))
    {
        if(pSnap->iObject1 != iIgnoreObjectId)
        {
            SetMarker(pObj1.get(), false /*bIgnoreOffLineObject*/);
            pDef->SetMouseEmphasis(pObj1);
        }
        else
        {
            pSnap->eSnapType = VIEW_COMMON::SNP_NONE;
            pSnap->pt = pSnap->ptOrg;

        }
    }

    if (pObj2)
    {
        pDef->SetMouseEmphasis(pObj2);
    }

    int iMarkerId = pMarker->MouseMove(posMouse.ptSel);

	if(pSnap->eSnapType != VIEW_COMMON::SNP_NONE)
	{
		pSnap->strSnap = GetSnapName(pSnap->eSnapType);
	}

    return bSnap;
}

/**
 *  @brief   マウス移動動作 ( 点を選択 )
 *  @param   [out]    pSnap   選択位置
 *  @param   [out]    ppObj   選択オブジェクト
 *  @param   [in]     posMouse   マウス位置        
 *  @param   [in]     bIgnoreGroup true;グループ内部を検索        
 *  @param   [in]     iIgnoreId    無視する図形ID        
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CDrawingView::GetMousePointSingle(SNAP_DATA* pSnap,
                    std::shared_ptr<CDrawingObject>* ppObj,
                    MOUSE_MOVE_POS posMouse,
                    bool bIgnoreGroup,
                    bool bKeepCenter,
                    int iIgnoreId)
{
    /* 右ボタンでも移動する
    if (!(posMouse.nFlags & MK_LBUTTON))
    {
        //ドラッグ中以外は不可
        return false;
    }
    */

    SNAP_DATA snap;
    using namespace VIEW_COMMON;

    bool bIgnoreSnap = false;
    if (posMouse.nFlags & MK_SHIFT)
    {
        bIgnoreSnap = true;
    }

    CPartsDef*   pDef  = GetPartsDef();
    CDrawMarker* pMarker= GetMarker();
    pMarker->Clear();
    pMarker->ClearObject();

    pDef->ClearMouseOver();
    pDef->ClearMouseEmphasis();
    pDef->ClearMouseConnectable();

    bool bSnap;
    bSnap = GetMousePoint(&snap, posMouse, bIgnoreGroup, bKeepCenter, iIgnoreId);

    //CDrawingObject* pObj1;
    auto pObj = pDef->GetObjectById(snap.iObject1, bIgnoreGroup);
    auto pObj2 = pDef->GetObjectById(snap.iObject2, bIgnoreGroup);


    if ((!bIgnoreSnap) &&
        (snap.iObject1 !=iIgnoreId))
    {
        if (pObj &&
            !pObj2)
        {
            SetMarker(pObj.get(), false /*bIgnoreOffLineObject*/);
            pDef->SetMouseEmphasis(pObj);
        }

        if (pObj2)
        {
            pDef->SetMouseEmphasis(pObj2);
        }

        int iMarkerId = pMarker->MouseMove(posMouse.ptSel);
    }
    else
    {
        snap.eSnapType = SNP_NONE;
        snap.pt = snap.ptOrg;
    }

    *ppObj = pObj;
    *pSnap = snap;
    return true;
}


/**
 *  マーカ設定
 *  @param   [in]    pObj   選択オブジェクト
 *  @param   [in]    bIgnoreOffLineObject 線上にないオブジェクトを表示しない
 *  @retval   なし
 *  @note
 */
bool  CDrawingView::SetMarker(CDrawingObject* pObj,
                              bool bIgnoreOffLineObject)
{

    STD_ASSERT(pObj != NULL);

    bool bRet;
    bRet = SetMarkerWithAddSnapList(pObj,
                        NULL,
                        NULL,
                        bIgnoreOffLineObject); 
    return bRet;
}


bool  CDrawingView::SetMarkerWithAddSnapList(
                              CDrawingObject* pObj,
                              const std::list<SNAP_DATA>* pAddSnapList,
                              const CNodeData* pNodeData,
                              bool bIgnoreOffLineObject)
{
    using namespace VIEW_COMMON;

    CDrawMarker*pMarker = GetMarker();

    CLayer* pLayer   = GetCurrentLayer();

    POINT ptOffset;
    ptOffset.x = ptOffset.y = 0;
    DWORD dwSnap = DRAW_CONFIG->GetSnapType();

    CDrawMarker::MARKER_DATA marker;
    marker.ptOffset = ptOffset;

    DRAWING_TYPE eType;
    std::list<SNAP_DATA> lstSnap;

    bool bRet = false;
    if (pObj)
    {
        eType  = pObj->GetType();
        pObj->GetSnapList(&lstSnap);
        marker.iObjct = pObj->GetId();

        if (!lstSnap.empty())
        {
            bRet = true;
        }
    }
    else
    {
        eType = DT_NONE;
        marker.iObjct = -1;
    }

    if (pAddSnapList)
    {
        lstSnap.insert(lstSnap.end(), pAddSnapList->begin(), pAddSnapList->end());
    }

    for(const SNAP_DATA& snap: lstSnap)
    {
        if (snap.eSnapType & dwSnap)
        {
            CDrawMarker::E_MARKER_TYPE eMarkerType = CDrawMarker::MARKER_MAX;
            switch(snap.eSnapType)
            {
            case SNP_MID_POINT:     
            case SNP_CENTER:        
            case SNP_DATUME_POINT:
                eMarkerType = CDrawMarker::MARKER_CENTER; 
                break;

            case SNP_CROSS_POINT:
                eMarkerType = CDrawMarker::MARKER_CROSS;
                break;

            case SNP_ON_LINE:        
                eMarkerType = CDrawMarker::MARKER_X;
                break;

            case SNP_END_POINT:   
            case SNP_FEATURE_POINT:
            case SNP_POINT:
                    eMarkerType = CDrawMarker::MARKER_POINT;
                    break; 

            case SNP_NORM:
                    eMarkerType = CDrawMarker::MARKER_NORM;
                    break; 

            case SNP_TANGENT:
                    eMarkerType = CDrawMarker::MARKER_TANGENT;
                    break; 

            case SNP_NODE:
                {
                    bool bConnect = true;
                    if(pNodeData)
                    {
                        if(snap.pConnection)
                        {
                            if(!pNodeData->IsAbleToConnect(snap.pConnection.get()))
                            {
                                bConnect = false;
                            }

                        }
                    }

                    if(snap.pConnection)
                    {
                        if(bConnect)
                        {
                            switch(snap.pConnection->nodeProperty.m_eShape)
                            {
                            case DMS_CIRCLE_FILL:   {eMarkerType = CDrawMarker::MARKER_CIRCLE_FILL; break;}       
                            case DMS_RECT_FILL:     {eMarkerType = CDrawMarker::MARKER_RECT_FILL; break;}       
                            case DMS_RECT:          {eMarkerType = CDrawMarker::MARKER_RECT; break;}       
                            case DMS_DBL_CIRCLE:    {eMarkerType = CDrawMarker::MARKER_DBL_CIRCLE; break;}       
                            case DMS_DBL_RECT:      {eMarkerType = CDrawMarker::MARKER_DBL_RECT; break;}       
                            default:
                                eMarkerType = CDrawMarker::MARKER_CIRCLE_FILL; break;
                            }
                        }
                        else
                        {
                            eMarkerType = CDrawMarker::MARKER_X; 
                        }
                        marker.crDraw = snap.pConnection->nodeProperty.m_cr;
                    }
                    else
                    {
                        eMarkerType = CDrawMarker::MARKER_CIRCLE;
                    }
                }
                break; 

            default:
                    break; 
            }

            if (eMarkerType == CDrawMarker::MARKER_MAX)
            {
                continue;
            }

            if (bIgnoreOffLineObject)
            {
                if ((eType == DT_CIRCLE) ||
                    (eType == DT_ELLIPSE))
                {
                    if (snap.eSnapType == SNP_CENTER)
                    {
                        continue;
                    }
                }
            }

            marker.eType = eMarkerType;
			marker.eSnap = snap.eSnapType;
            marker.iVal = -1;
            marker.bKeepSelect = snap.bKeepSelect;
            pMarker->AddMarker(marker, snap.pt);
        }
    }
    return bRet;
}

/**
 * @brief   スナップ半径取得
 * @param   なし     
 * @retval  
 * @note	
 */
double CDrawingView::GetSnapRadius() const
{
    double dSnapSize;
    double dScl = GetCurrentLayer()->dScl * m_Vv.dDspScl;

    dSnapSize =  (DRAW_CONFIG->GetSnapSize() / dScl);
    return dSnapSize;
}

/**
 * @brief   オブジェクト定義取得
 * @param   なし     
 * @retval  オブジェクト定義のポインタ
 * @note
 */
CPartsDef* CDrawingView::GetPartsDef() const
{

   CPartsDef* pPartsDef = NULL;
   std::shared_ptr<CObjectDef> pDef;
    if (m_pDoc)
    {
        pDef = m_pDoc->GetObjectDef().lock();
        if( pDef )
        {
            pPartsDef = dynamic_cast<CPartsDef*>(pDef.get());
        }
    }
    return pPartsDef;
}

/**
 * @brief    デバイスコンテキスト取得
 * @param    なし
 * @retval   デバイスコンテキスト
 * @note	
 */
HDC CDrawingView::GetViewDc() const
{
    return m_hDc;
}

/**
 * @brief   裏画面デバイスコンテキスト取得
 * @param   なし
 * @retval  デバイスコンテキスト
 * @note	
 */
HDC CDrawingView::GetBackDc() const
{
    STD_ASSERT(m_psBack != NULL);
    return m_psBack->GetDc();
}

/**
 * @brief   インデックス画面デバイスコンテキスト取得
 * @param   なし
 * @retval  デバイスコンテキスト
 * @note	
 */
HDC CDrawingView::GetIndexDc() const
{
    STD_ASSERT(m_psIndex != NULL);
    return m_psIndex->GetDc();
}

HDC CDrawingView::GetIndexHiDc() const
{
    STD_ASSERT(m_psIndexHi != NULL);
    return m_psIndexHi->GetDc();
}


/**
 * @brief   選択画面デバイスコンテキスト取得
 * @param   なし
 * @retval  デバイスコンテキスト
 * @note	
 */
HDC CDrawingView::GetSelDc() const
{
    STD_ASSERT(m_psSelect != NULL);
    return m_psSelect->GetDc();
}

/**
 * @brief   表示倍率取得
 * @param   なし
 * @retval  表示倍率
 * @note	
 */
double CDrawingView::GetViewScale() const
{
    return m_Vv.dDspScl;
}

void CDrawingView::SetViewScale(double dScl)
{
    m_Vv.dDspScl = dScl;
}

/**
 * @brief    マーカインスタンス取得
 * @param    なし
 * @retval   マーカへのポインタ
 * @note	
 */
CDrawMarker* CDrawingView::GetMarker()
{
    return m_psMarker.get();
}

/**
 * @brief    ドラッグ用マーカインスタンス取得
 * @param    なし
 * @retval   ドラッグ用マーカへのポインタ
 * @note	
 */
CNodeMarker* CDrawingView::GetNodeMarker()
                              
{
    return m_psNodeMarker.get();
}

const CNodeMarker* CDrawingView::GetNodeMarker() const
                              
{
    return m_psNodeMarker.get();
}

/**
 * @brief    ドラッグ用マーカインスタンス取得
 * @param    なし
 * @retval   ドラッグ用マーカへのポインタ
 * @note	
 */
CNodeBound* CDrawingView::GetNodeBound()
                              
{
    return m_psNodeBound.get();
}

const CNodeBound* CDrawingView::GetNodeBound() const
                              
{
    return m_psNodeBound.get();
}

/**
 * @brief    値入力
 * @param    strVal  入力値
 * @retval   true 設定成功
 * @note	
 */
bool CDrawingView::InputValue(StdString strVal)
{
    STD_ASSERT(m_pCurAction != NULL);
    return m_pCurAction->InputValue(strVal);
}


/**
 * @brief    コメント入力
 * @param    strComment  入力値
 * @retval   なし
 * @note	
 */
void CDrawingView::SetComment(StdString strComment)
{
    CMockSketchView* pView = dynamic_cast<CMockSketchView*>(m_pView);
    STD_ASSERT(pView != 0);

    pView->SetComment(strComment);
}

/**
 * @brief    入力可・不可設定
 * @param    bEnable  true:入力可
 * @retval   なし
 * @note	
 */
void CDrawingView::EnableInput(bool bEnable)
{
    CMockSketchView* pView = dynamic_cast<CMockSketchView*>(m_pView);
    STD_ASSERT(pView != 0);

    pView->EnableInput(bEnable);
}


void CDrawingView::ClearInput()
{
    CMockSketchView* pView = dynamic_cast<CMockSketchView*>(m_pView);
    STD_ASSERT(pView != 0);

    pView->ClearInput();
}

/**
 * @brief    入力値取得
 * @param    なし
 * @retval   入力値
 * @note	
 */
StdString CDrawingView::GetInputValue()
{
   CMockSketchView* pView = dynamic_cast<CMockSketchView*>(m_pView);
   STD_ASSERT(pView != 0);

   return pView->GetInputValue();
}


/**
 *  @brief 入力値更新
 *  @param [in] bOk   更新する
 *  @retval           なし
 *  @note bOk==falseの時は値を選択状態にする
 */
void CDrawingView::UpdateInput(bool bOk)
{
   CMockSketchView* pView = dynamic_cast<CMockSketchView*>(m_pView);
   STD_ASSERT(pView != 0);
   pView->UpdateInput(bOk);
}



/**
 * @brief    入力値取得
 * @param    なし
 * @retval   入力値
 * @note	
 */
CUnit* CDrawingView::GetUnit()
{
    return m_psUnit.get();
}


/**
 * @brief    入力エリアフォーカス設定
 * @param    なし
 * @retval   なし
 * @note	
 */
void CDrawingView::SetInputFocus()
{
   CMockSketchView* pView = dynamic_cast<CMockSketchView*>(m_pView);
    STD_ASSERT(pView != 0);


    pView->SetInputFocus();
}

/**
 * @brief    フォーカス設定
 * @param    なし
 * @retval   なし
 * @note	
 */
void CDrawingView::SetFocus()
{
   CMockSketchView* pView = dynamic_cast<CMockSketchView*>(m_pView);
    STD_ASSERT(pView != 0);

    if (pView)
    {
        pView->SetFocus();
    }
}


/**
 * @brief    マウスキャプチャー
 * @param    なし
 * @retval   なし
 * @note	
 */
void CDrawingView::SetCapture()
{
    STD_ASSERT(m_pView != 0);
    ::SetCapture(m_pView->m_hWnd);
}


/**
* @brief    リッチエディットダイアログ
* @param    なし
* @retval   リッチエディットダイアログ
* @note	
*/
CRichEditCtrl*  CDrawingView::GetRichEdit()
{
    CMockSketchView* pView = dynamic_cast<CMockSketchView*>(m_pView);
    STD_ASSERT(pView != 0);
    return pView->GetRichEdit();

}

/**
 * @brief    リッチエディットコントロール初期化
 * @param    なし
 * @retval   リッチエディットコントロール
 * @note	
 */
void CDrawingView::InitRichEdit()
{
    CMockSketchView* pView = dynamic_cast<CMockSketchView*>(m_pView);
    STD_ASSERT(pView != 0);
    return pView->InitRichEdit();
}


 bool CDrawingView::CalcRichEditSize(SIZE* pSize)
{
    CMockSketchView* pView = dynamic_cast<CMockSketchView*>(m_pView);
    STD_ASSERT(pView != 0);
    return pView->CalcRichEditSize(pSize);
}


/**
 *  @brief  アンドゥ
 *  @param  なし
 *  @retval なし
 *  @note
 */
void CDrawingView::Undo()
{
    if (m_pCurAction)
    {
        if(m_pCurAction->Undo())
        {
            return;
        }
        return;
    }

    CPartsDef* pDef = GetPartsDef();
    if (pDef)
    {
        if(!pDef->GetUndoAction()->Undo())
        {
            //これ以上やり直しはできません
            AfxMessageBox(GET_STR(STR_ERROR_MORE_REDO));
        }
    }
    return;
}

/**
 *  @brief  リドゥ問い合わせ
 *  @param  なし
 *  @retval なし
 *  @note
 */
bool CDrawingView::IsUndoEmpty()
{
    if (m_pCurAction)
    {
		return m_pCurAction->IsUndoEmpty();
    }

    CPartsDef* pDef = GetPartsDef();
    if (pDef)
    {
        return pDef->GetUndoAction()->IsUndoEmpty();
    }

    return false;
}

/**
 *  @brief  リドゥ
 *  @param  なし
 *  @retval なし
 *  @note
 */
void CDrawingView::Redo()
{
    if (m_pCurAction)
    {
        if(m_pCurAction->Redo())
        {
            return;
        }
    }

    CPartsDef* pDef = GetPartsDef();
    if (pDef)
    {
        if(!pDef->GetUndoAction()->Redo())
        {
            //これ以上やり直しはできません
            AfxMessageBox(GET_STR(STR_ERROR_MORE_REDO));
        }
    }
}

/**
 *  @brief  リドゥ問い合わせ
 *  @param  なし
 *  @retval なし
 *  @note
 */
bool CDrawingView::IsRedoEmpty()
{

    if (m_pCurAction)
    {
        if(!m_pCurAction->IsRedoEmpty())
        {
            return false;
        }
    }

    CPartsDef* pDef = GetPartsDef();
    if (pDef)
    {
        return pDef->GetUndoAction()->IsRedoEmpty();
    }

    return false;
}

/**
 *  @brief  初期化問い合わせ
 *  @param  なし
 *  @retval なし
 *  @note
 */
bool CDrawingView::IsInit() const
{
    if (m_pView == NULL)
    {
        return false;
    }
    return true;
}


/**
 *  @brief  ツールチップ設定
 *  @param  [in] strText 表示テキスト
 *  @retval なし
 *  @note
 */
void CDrawingView::SetToolTipText(LPCTSTR strText)
{
    CMockSketchView* pView = dynamic_cast<CMockSketchView*>(m_pView);
    pView->SetToolTipText(strText);
}

/**
 *  @brief  ツールチップ消去
 *  @param  なし
 *  @retval なし
 *  @note
 */
void CDrawingView::ResetToolTipText()
{
    CMockSketchView* pView = dynamic_cast<CMockSketchView*>(m_pView);
    pView->ResetToolTipText();
}

void CDrawingView::PreparePrinting(CPrintInfo* pInfo)
{
    CPartsDef* pDef = GetPartsDef();
    int iRow = 1;
    int iCol = 1;
    if (pDef)
    {
        iRow = pDef->GetPrintRow();
        iCol = pDef->GetPrintCol();
    }

    pInfo->SetMaxPage(iRow * iCol);

    //一時退避
    m_VvOld  = m_Vv;
    m_dDpuOld = m_dDpu;
    m_hDcOld = m_hDc;

}


//!< 印刷設定
void CDrawingView::BeginPrint(CDC* pDC, CPrintInfo* pInfo)
{
    m_bPrint = true;
    PreparePrinting(pInfo);
}


void CDrawingView::EndPrinting()
{
    m_VvOld =  m_Vv;
    m_dDpu   =    m_dDpuOld;   
    m_hDc     = m_hDcOld;
    m_bPrint = false;
    
    CPartsDef* pDef = GetPartsDef();
    if( pDef )
    {
        pDef->Redraw(this);
    }
}


void CDrawingView::PrepareDC(CDC* pDC, CPrintInfo* pInfo)
{
    if (!pInfo)
    {
        return;
    }

    CPartsDef* pDef = GetPartsDef();
    int iRow = 1;
    int iCol = 1;
    int iRowMax = 1;
    int iColMax = 1;

    if (pDef)
    {
        iRowMax = pDef->GetPrintRow();
        iColMax = pDef->GetPrintCol();
    }
    

    //プリンターの情報より、縦、横のインチ当たりのドット数を求める
    int cxInch = pDC->GetDeviceCaps(LOGPIXELSX);
    int cyInch = pDC->GetDeviceCaps(LOGPIXELSY);
    
    POINT ptPaper;
    ptPaper.x = pDC->GetDeviceCaps(PHYSICALWIDTH);
    ptPaper.y = pDC->GetDeviceCaps(PHYSICALHEIGHT);

    double dConvFactor = DRAW_CONFIG->pDispUnit->ToBase(1.0);
    m_dDpu = cxInch * (1000.0  * dConvFactor) / 25.4;

    m_Vv.dDspScl = 1.0;

    int iPage = pInfo->m_nCurPage - 1;


    iRow = int(floor(iPage / iColMax));
    iCol = (iPage % iColMax);


    double iWidth2  = pDC->GetDeviceCaps( PHYSICALWIDTH);
    double iHeight2 = pDC->GetDeviceCaps( PHYSICALHEIGHT);

    double dPaperWidth  = DRAW_CONFIG->pDispUnit->ToThisUnit(THIS_APP->GetPaperWidth());
    double dPaperHeight = DRAW_CONFIG->pDispUnit->ToThisUnit(THIS_APP->GetPaperHeight());

    double dPrintWidth  = DRAW_CONFIG->pDispUnit->ToThisUnit(THIS_APP->GetPrintWidth());
    double dPrintHeight = DRAW_CONFIG->pDispUnit->ToThisUnit(THIS_APP->GetPrintHeight());

    m_Vv.iWidth  = int(CUtil::Round(m_dDpu * dPrintWidth));
    m_Vv.iHeight = int(CUtil::Round(m_dDpu * dPrintHeight));
 
    int iOffsetX;
    int iOffsetY;

    if (GetPartsDef()->IsOffsetSizeEqualPaper())
    {
        iOffsetX  = m_Vv.iWidth;
        iOffsetY  = m_Vv.iHeight;
    }
    else
    {
        iOffsetX  = int(CUtil::Round(m_dDpu * GetPartsDef()->GetOffsetPageCol()));
        iOffsetY  = int(CUtil::Round(m_dDpu * GetPartsDef()->GetOffsetPageRow()));
    }

    m_Vv.ptLook.x = iCol  * iOffsetX;  
    m_Vv.ptLook.y = iRow  * iOffsetY;

    m_hDc = pDC->GetSafeHdc();
    m_pSurface->ChangeDc(pDC, true);

    //m_pGrap = std::make_unique<Gdiplus::Graphics>(m_hDc);
}

bool CDrawingView::IsPrintMode() const
{
    return m_bPrint;
}

void CDrawingView::DrawPrintArea()
{
    if(m_eMode & DRAW_FIG)
	{
	DrawPrintArea(m_psBack->GetDc());
	}

	if ((m_eMode & DRAW_FRONT) ||
		(m_eMode & DRAW_PRINT))
	{
		DrawPrintArea(m_hDc);
	}
}

bool CDrawingView::CheckPrintArea(POINT ptMouse)
{
	CPartsDef* pDef = GetPartsDef();
	if (!pDef)
	{
		return false;
	}

	if (!pDef->IsShowPrintArea())
	{
		return false;
	}

	auto pLayer = GetCurrentLayer();
	if (pLayer->iLayerId != 0)
	{
		if (fabs(pLayer->dScl - 1.0) > NEAR_ZERO)
		{
			return false;
		}
	}

	POINT2D pt;
	ConvScr2World(&pt, ptMouse, 0);

	int iRowMax = 1;
	int iColMax = 1;
	int iRow = 1;
	int iCol = 1;

	iRowMax = pDef->GetPrintRow();
	iColMax = pDef->GetPrintCol();

	double dPaperWidth = DRAW_CONFIG->pDispUnit->ToThisUnit(THIS_APP->GetPaperWidth());
	double dPaperHeight = DRAW_CONFIG->pDispUnit->ToThisUnit(THIS_APP->GetPaperHeight());

	double dWidth = DRAW_CONFIG->pDispUnit->ToThisUnit(THIS_APP->GetPrintWidth());
	double dHeight = DRAW_CONFIG->pDispUnit->ToThisUnit(THIS_APP->GetPrintHeight());

	double dOffsetX = DRAW_CONFIG->pDispUnit->ToThisUnit(THIS_APP->GetPrintLeftMargin());
	double dOffsetY = DRAW_CONFIG->pDispUnit->ToThisUnit(THIS_APP->GetPrintTopMargin());

	double dPaperOffsetX;
	double dPaperOffsetY;
	if (pDef->IsOffsetSizeEqualPaper())
	{
		dPaperOffsetX = dPaperWidth;
		dPaperOffsetY = dPaperHeight;
	}
	else
	{
		dPaperOffsetX = pDef->GetOffsetPageCol();
		dPaperOffsetY = pDef->GetOffsetPageRow();
	}

	double dCol = (pt.dX + 0.5 * dPaperWidth) / dPaperOffsetX;
	iCol = DoubleToInt(floor(dCol));

	double dRow = (-pt.dY + 0.5 * dPaperHeight) / dPaperOffsetY;
	iRow = DoubleToInt(floor(dRow));

	if ((iRow >= iRowMax) || (iCol >= iColMax) ||
		(iRow < 0) || (iCol < 0))
	{
		SetDrawingMode(DRAW_INDEX);
		auto pOldPaper = pDef->GetObjectById(ID_PAPER_FRAME);
		if (pOldPaper)
		{
			pOldPaper->DeleteView(this, NULL);
			pOldPaper.reset();
		}

		auto pOldPrint = pDef->GetObjectById(ID_PRINT_FRAME);
		if (pOldPrint)
		{
			pOldPrint->DeleteView(this, NULL);
			pOldPrint.reset();
		}
		m_iOldPrintCol = -1;
		m_iOldPrintRow = -1;
		return false;
	}

	POINT2D ptTL2D;
	POINT2D ptBR2D;

	POINT2D ptPrintTL2D;
	POINT2D ptPrintBR2D;

	ptBR2D.dX = -(dPaperWidth / 2.0) + iCol * dPaperOffsetX;
	ptTL2D.dX = ptBR2D.dX + dPaperWidth;

	ptTL2D.dY = -(dPaperHeight / 2.0) - iRow * dPaperOffsetY;
	ptBR2D.dY = ptTL2D.dY + dPaperHeight;


	RECT2D rcPaper(ptTL2D, ptBR2D);


	POINT2D ptEdge;
	auto eEdge = rcPaper.NearEdgePoint(&ptEdge, pt);
	bool bDraw = false;
	if (eEdge != RECT2D::E_NONE)
	{
		if ((m_iOldPrintCol == iCol) &&
			(m_iOldPrintRow == iRow))
		{
			return false;
		}

		SetDrawingMode(DRAW_INDEX);
		//SetDrawingMode(DRAW_FRONT_VIEW);
		bDraw = true;
		auto paperFrame = std::make_shared<CDrawingCompositionLine>(ID_PAPER_FRAME);
		std::vector<LINE2D> lstLine;
		rcPaper.GetrLines(&lstLine);

		auto pOld = pDef->GetObjectById(ID_PAPER_FRAME);
		if (pOld)
		{
			pOld->DeleteView(this, NULL);
		}


		for (auto line : lstLine)
		{
			CDrawingLine dl(ID_PAPER_FRAME);
			dl.SetPoint1(line.GetPt1());
			dl.SetPoint2(line.GetPt2());
			paperFrame->AddObject(&dl);
		}
		paperFrame->SetPartsDef(pDef);
		paperFrame->SetLayer(0);
		paperFrame->SetLineType(PS_SOLID);
		//paperFrame->SetColor(RGB(0,0,255));
		paperFrame->SetColor(DRAW_CONFIG->crFront);
		pDef->RegisterPrintArea(paperFrame);

		paperFrame->Draw(this, NULL);
	}
	else
	{
		pDef->GetObjectById(ID_PAPER_FRAME).reset();
	}

	if (GetPartsDef()->IsShowPaperArea())
	{
		ptPrintTL2D.dX = ptBR2D.dX + dOffsetX;
		ptPrintBR2D.dX = ptPrintTL2D.dX + dWidth;

		ptPrintTL2D.dY = ptTL2D.dY + dOffsetY;
		ptPrintBR2D.dY = ptPrintTL2D.dY + dHeight;

		POINT2D ptEdge2;
		RECT2D rcPrint(ptPrintTL2D, ptPrintBR2D);
		auto eEdge2 = rcPaper.NearEdgePoint(&ptEdge2, pt);


		if (eEdge2 != RECT2D::E_NONE)
		{
			auto pOld = pDef->GetObjectById(ID_PRINT_FRAME);
			if (pOld)
			{
				pOld->DeleteView(this, NULL);
			}

			auto printFrame = std::make_shared<CDrawingCompositionLine>(ID_PRINT_FRAME);
			std::vector<LINE2D> lstLine2;
			rcPrint.GetrLines(&lstLine2);
			for (auto line : lstLine2)
			{
				CDrawingLine dl(ID_PRINT_FRAME);
				dl.SetPoint1(line.GetPt1());
				dl.SetPoint2(line.GetPt2());
				printFrame->AddObject(&dl);
			}
			printFrame->SetPartsDef(pDef);
			printFrame->SetLayer(0);
			printFrame->SetLineType(PS_DOT);
			printFrame->SetColor(DRAW_CONFIG->crFront);
			pDef->RegisterPrintArea(printFrame);

			printFrame->Draw(this, NULL);
		}
		else
		{
			pDef->GetObjectById(ID_PRINT_FRAME).reset();
		}

		m_iOldPrintCol = iCol;
		m_iOldPrintRow = iRow;
		return true;
	}
	return false;
}

void CDrawingView::DrawPrintArea(HDC  hDc)
{
    int iRowMax = 1;
    int iColMax = 1;
    int iRow = 1;
    int iCol = 1;


    CPartsDef* pDef = GetPartsDef();
    {
        iRowMax = pDef->GetPrintRow();
        iColMax = pDef->GetPrintCol();
    }

    double dPaperWidth  = DRAW_CONFIG->pDispUnit->ToThisUnit(THIS_APP->GetPaperWidth());
    double dPaperHeight = DRAW_CONFIG->pDispUnit->ToThisUnit(THIS_APP->GetPaperHeight());

    double dWidth  = DRAW_CONFIG->pDispUnit->ToThisUnit(THIS_APP->GetPrintWidth());
    double dHeight = DRAW_CONFIG->pDispUnit->ToThisUnit(THIS_APP->GetPrintHeight());

    double dOffsetX  = DRAW_CONFIG->pDispUnit->ToThisUnit(THIS_APP->GetPrintLeftMargin());
    double dOffsetY  = DRAW_CONFIG->pDispUnit->ToThisUnit(THIS_APP->GetPrintTopMargin());

    double dPaperOffsetX;
    double dPaperOffsetY;
    if (GetPartsDef()->IsOffsetSizeEqualPaper())
    {
        dPaperOffsetX = dPaperWidth;
        dPaperOffsetY = dPaperHeight;
    }
    else
    {
        dPaperOffsetX = GetPartsDef()->GetOffsetPageCol();
        dPaperOffsetY = GetPartsDef()->GetOffsetPageRow();
    }

	auto pLayer = GetCurrentLayer();
	COLORREF crPen = DRAW_CONFIG->crFront;
	if (!DRAW_CONFIG->bDisableDifferentScl)
	{
		if (fabs(pLayer->dScl - 1.0) > NEAR_ZERO)
		{
			crPen = DRAW_CONFIG->crDisable;
		}
	}

	for (int iCol = 0; iCol < iColMax; iCol++)
    {
        for (int iRow = 0; iRow < iRowMax; iRow++)
        {
            POINT2D ptTL2D;
            POINT2D ptBR2D;

            POINT2D ptPrintTL2D;
            POINT2D ptPrintBR2D;

            ptBR2D.dX      = -(dPaperWidth / 2.0) + iCol * dPaperOffsetX;
            ptTL2D.dX      = ptBR2D.dX + dPaperWidth;

            ptTL2D.dY      = -(dPaperHeight / 2.0) -  iRow * dPaperOffsetY;
            ptBR2D.dY      = ptTL2D.dY + dPaperHeight;

            ptPrintTL2D.dX = ptBR2D.dX + dOffsetX;
            ptPrintBR2D.dX = ptPrintTL2D.dX + dWidth;

            ptPrintTL2D.dY = ptTL2D.dY + dOffsetY;
            ptPrintBR2D.dY = ptPrintTL2D.dY + dHeight;


            POINT ptTL;
            POINT ptBR;

            int iLayerId = 0;
            ConvWorld2Scr(&ptTL, ptTL2D, iLayerId);
            ConvWorld2Scr(&ptBR, ptBR2D, iLayerId);

            POINT ptTR;
            POINT ptBL;

            ptTR.x = ptBR.x;
            ptTR.y = ptTL.y;

            ptBL.x = ptTL.x;
            ptBL.y = ptBR.y;

            if (GetPartsDef()->IsShowPaperArea())
            {
				SetPen(PS_SOLID, 1, crPen, 0);
				MoveTo(ptTL);
                LineTo(ptTR);
                LineTo(ptBR);
                LineTo(ptBL);
                LineTo(ptTL);
            }

            POINT ptPrintTL;
            POINT ptPrintBR;

            ConvWorld2Scr(&ptPrintTL, ptPrintTL2D, iLayerId);
            ConvWorld2Scr(&ptPrintBR, ptPrintBR2D, iLayerId);

            POINT ptPrintTR;
            POINT ptPrintBL;

            ptPrintTR.x = ptPrintBR.x;
            ptPrintTR.y = ptPrintTL.y;

            ptPrintBL.x = ptPrintTL.x;
            ptPrintBL.y = ptPrintBR.y;

			SetPen(PS_DOT, 1, crPen, 0);
			MoveTo(ptPrintTL);
            LineTo(ptPrintTR);
            LineTo(ptPrintBR);
            LineTo(ptPrintBL);
            LineTo(ptPrintTL);
        }
    }

	auto paper = pDef->GetObjectById(ID_PAPER_FRAME);
	auto frame = pDef->GetObjectById(ID_PRINT_FRAME);

	if (paper)
	{
		paper->Draw(this, NULL);
	}

	if (frame)
	{
		frame->Draw(this, NULL);
	}
}

void CDrawingView::UpdatePrinter()
{
    CPartsDef* pDef = GetPartsDef();
    if (pDef->IsOffsetSizeEqualPaper())
    {
        pDef->SetOffsetPageRow(THIS_APP->GetPaperHeight());
        pDef->SetOffsetPageCol(THIS_APP->GetPaperWidth());
    }
    pDef->Redraw(this);
}

void CDrawingView::ChangeObjectOrder(VIEW_COMMON::E_DISP_ORDER order)
{
      CPartsDef* pDef = GetPartsDef();

      long lSize = pDef->SelectNum();
        
      if( lSize != 1)
      {
        return;
      }
      auto pObj = pDef->GetSelect(0);

      if(pObj)
      {
          pDef->ChangeObjectOrder(order, pObj->GetId());
      }
}

void CDrawingView::CheckDc(HDC hDc)
{
    int iMapMode = ::GetMapMode(hDc);
    DB_PRINT(_T("CheckDc  %I64x "), hDc);

    StdString strOut;
    switch(iMapMode)
    {
    case MM_ANISOTROPIC:{DB_PRINT(_T("MM_ANISOTROPIC ")); break;}
    case MM_HIENGLISH:  {DB_PRINT(_T("MM_HIENGLISH ")); break;}
    case MM_HIMETRIC:   {DB_PRINT(_T("MM_HIMETRIC ")); break;}
    case MM_ISOTROPIC:  {DB_PRINT(_T("MM_ISOTROPIC ")); break;}
    case MM_LOENGLISH:  {DB_PRINT(_T("MM_LOENGLISH ")); break;}
    case MM_LOMETRIC:   {DB_PRINT(_T("MM_LOMETRIC ")); break;}
    case MM_TEXT:       {DB_PRINT(_T("MM_TEXT ")); break;}
    case MM_TWIPS:      {DB_PRINT(_T("MM_TWIPS ")); break;}
    default: break;
    }

    BOOL bRet;
    SIZE sz;

    bRet = GetViewportExtEx(
      hDc,      // デバイスコンテキストのハンドル
      &sz // ビューポートのサイズ
    );
    DB_PRINT(_T("ViewPort Size(%d, %d) "), sz.cx, sz.cy);

    POINT pt;
    bRet = GetViewportOrgEx(
      hDc,      // デバイスコンテキストのハンドル
      &pt // ビューポートのサイズ
    );
    DB_PRINT(_T("ViewPort Org(%d, %d) "), pt.x, pt.y);

    bRet = GetWindowOrgEx(
      hDc,      // デバイスコンテキストのハンドル
      &pt // ビューポートのサイズ
    );
    DB_PRINT(_T("WindowOrg(%d, %d) "), pt.x, pt.y);

    XFORM xForm;
    bRet = GetWorldTransform(
      hDc,      // デバイスコンテキストのハンドル
      &xForm // ビューポートのサイズ
    );
    DB_PRINT(_T("WindowOrg(%f, %f)(%f, %f)(%f, %f) "),
                                       xForm.eM11, xForm.eM12, 
                                       xForm.eM21, xForm.eM22, 
                                       xForm.eDx, xForm.eDy);

    int iGMode = GetGraphicsMode(
      hDc   // デバイスコンテキストのハンドル
    );

    if (iGMode == GM_COMPATIBLE)
    {
        DB_PRINT(_T("GM_COMPATIBLE "));
    }
    else if (iGMode == GM_ADVANCED)
    {
        DB_PRINT(_T("GM_ADVANCED "));
    }
    DB_PRINT(_T("\n"));
}

bool CDrawingView::CheckGrid(POINT ptMouse)
{
	if (!(DRAW_CONFIG->GetSnapType() & VIEW_COMMON::SNP_GRID))
	{
		return false;
	}

	CPartsDef* pDef = GetPartsDef();
	if (!pDef)
	{
		return false;
	}

	SetDrawingMode(DRAW_INDEX);
	//SetDrawingMode(DRAW_FRONT_VIEW);
	
	auto pOldDot = pDef->GetObjectById(ID_GRID_DOT);
	if (pOldDot)
	{
		pOldDot->DeleteView(this, NULL);
		pOldDot.reset();
	}

	auto pLayer = GetCurrentLayer();
	POINT2D pt;
	ConvScr2World(&pt, ptMouse, pLayer->iLayerId);

	double dCol = ceil((pt.dX / m_dUseGridSize) - 0.5);
	double dRow = ceil((pt.dY / m_dUseGridSize) - 0.5);

	POINT2D ptGrid;
	ptGrid.dX = m_dUseGridSize * dCol;
	ptGrid.dY = m_dUseGridSize * dRow;

	auto dp = std::make_shared<CDrawingPoint>(ID_GRID_DOT);

	dp->SetPartsDef(pDef);
	dp->SetPoint(ptGrid);
	dp->SetLayer(pLayer->iLayerId);
	dp->SetPointType(POINT_DOT);
	dp->SetColor(DRAW_CONFIG->crFront);
	//dp->SetColor(RGB(0,255,0));
	pDef->RegisterGridDot(dp);
	dp->Draw(this, NULL);
	return false;
}

void CDrawingView::DrawGrid()
{
	if (m_eMode & DRAW_FIG)
	{
		DrawGrid(m_psBack->GetDc());
	}

	if ((m_eMode & DRAW_FRONT) ||
		(m_eMode & DRAW_PRINT))
	{
		DrawGrid(m_hDc);
	}
}

void CDrawingView::DrawGrid(HDC  hDc)
{

	double dGridSize = DRAW_CONFIG->dGridSize; //(表示単位系) グリッドの１区間のサイズ
	double dMinLine = DRAW_CONFIG->dMinLine;  //(表示単位系) 画面上のグリッドの１区間の最小長さ


	auto pLayer = GetCurrentLayer();
	double dSclLayer = pLayer->dScl;
	double dScl =    dSclLayer * m_Vv.dDspScl;
	double dSx = m_dDpu * dScl;
	double dSy = m_dDpu * dScl;


	double dDispGridSize = dGridSize * dScl; //画面上のグリッドサイズ
	double dUseGridSize = dGridSize;
	if (dMinLine > dDispGridSize)
	{
		//Scl 1.0の場合画面上の寸法と内部の寸法は一致する

		//グリッドの画面上の最小長さを実際の寸法に変換する
		double dDispL = dMinLine / dSx;
		double dDispN = floor(log10(dDispL));
		double dDispA = dDispL / pow(10, dDispN);

		double dGridN = floor(log10(dGridSize));;
		double dGridA = dGridSize / pow(10, dGridN);
		double dGridA2 = dGridA;
		if (dGridA < dDispA)
		{
			bool bChange = false;
			if (DRAW_CONFIG->bMultiple2)
			{
				if ((dGridA * 2.0) > dDispA)
				{
					dGridA2 = dGridA * 2.0;
					bChange = true;
				}
			}

			if (DRAW_CONFIG->bMultiple5)
			{
				if (!bChange)
				{
					if ((dGridA * 5.0) > dDispA)
					{
						dGridA2 = dGridA * 5.0;
						bChange = true;
					}
				}
			}
DB_PRINT(_T("Gs:%f L:%f N:%02f A:%02f  GN:%02f GA:%02f GA2:%02f\n"),
	dDispGridSize, dDispL, dDispN, dDispA,
	dGridN, dGridA, dGridA2);


			if (!bChange)
			{
				dDispN += 1.0;
			}
		}

		dUseGridSize = dGridA2 * pow(10, dDispN);
	}

	m_dUseGridSize = dUseGridSize;

	double dHw = 0.5 * m_Vv.iWidth;
	double dHh = 0.5 * m_Vv.iHeight;

	double dL = ((m_Vv.ptLook.x - dHw) / dSx) + pLayer->ptOffset.dX;
	double dR = ((m_Vv.ptLook.x + dHw) / dSx) + pLayer->ptOffset.dX;

	double dT = ((-m_Vv.ptLook.y + dHh) / dSy) + pLayer->ptOffset.dY;
	double dB = ((-m_Vv.ptLook.y - dHh) / dSy) + pLayer->ptOffset.dY;

	double dCol = floor(dL / dUseGridSize);
	double dRow = floor(dT / dUseGridSize);

	double dColStart = dCol * dUseGridSize;
	double dRowStart = dRow * dUseGridSize;

	SetPen(PS_DOT, 1, DRAW_CONFIG->crGrid, 0);
	POINT pt;
	for (double dw = dColStart; dw < dR; dw += dUseGridSize)
	{
		for (double dh = dRowStart; dh > dB; dh -= dUseGridSize)
		{
			pt.x = static_cast<LONG>( CUtil::Round((dw - pLayer->ptOffset.dX) * dSx) - m_Vv.ptLook.x + dHw);
			pt.y = static_cast<LONG>(-CUtil::Round((dh - pLayer->ptOffset.dY) * dSy) - m_Vv.ptLook.y + dHh);
			PointMark(pt, DRAW_CONFIG->eGridType, DRAW_CONFIG->iGridMarkerSize);
		}
	}
}

void CDrawingView::ClearDraggingNodes()
{
    m_lstDraggingNode.clear();
}


void CDrawingView::ClearDraggingTmpObjects()
{
    m_lstDraggingTmpObject.clear();
}


std::vector<std::shared_ptr<CDrawingNode>>* CDrawingView::GetDraggingNodes()
{
    return &m_lstDraggingNode;
}

std::vector<std::shared_ptr<CDrawingObject>>* CDrawingView::GetDraggingTmpObjects()
{
    return &m_lstDraggingTmpObject;
}



void CDrawingView::AddConnectableNode(CDrawingNode* pNode)
{
    m_lstNode.push_back(pNode);
}

void CDrawingView::ClearConnectableNode()
{
    m_lstNode.clear();
}

std::vector<CDrawingNode*> * CDrawingView::GetConnectableNodeList()
{
    return &m_lstNode;
}

void CDrawingView::ClearCurrentAction()
{
	if (m_pCurAction)
	{
		m_pCurAction->ClearAction();
	}
}


void PrintList(CDrawingView* pView, std::deque<std::shared_ptr<CDrawingObject>>* pList, StdString strSpc)
{
    if (!pList)
    {
        return;
    }

    for(auto pObj: *pList)
    {

        DRAWING_TYPE eType = pObj->GetType();
        StdString strType = CUtil::StrFormat(_T("Id:%d Type:%s"), 
                               pObj->GetId(),
                               VIEW_COMMON::GetObjctTypeName(eType).c_str());

        DB_PRINT(_T("%s%s\n"), strSpc.c_str(), strType.c_str());

        auto pParentParts = pObj->GetParentParts();;
        if (pParentParts)
        {
            DRAWING_TYPE eParentType = pParentParts->GetType();
            strType = CUtil::StrFormat(_T("ParentParts Id:%d Type:%s"), 
                                   pParentParts->GetId(),
                                   VIEW_COMMON::GetObjctTypeName(eParentType).c_str());
            DB_PRINT(_T("%s%s\n"), strSpc.c_str(), strType.c_str());
        }

        auto pDrawingParent = pObj->GetDrawingParent();;
        if (pDrawingParent)
        {
            DRAWING_TYPE eParentType = pDrawingParent->GetType();
            strType = CUtil::StrFormat(_T("DrawingParent Id:%d Type:%s"), 
                                   pDrawingParent->GetId(),
                                   VIEW_COMMON::GetObjctTypeName(eParentType).c_str());
            DB_PRINT(_T("%s%s\n"), strSpc.c_str(), strType.c_str());

            int iDrawingId;
            COLORREF crDraw;

            int iLayerId = pDrawingParent->GetLayer();
            crDraw =  pObj->GetDrawColor(pView, iLayerId, pDrawingParent.get(), &iDrawingId, false);

            StdString strColor;
            strColor = CUtil::StrFormat(_T("Color Id:%d RGB(%02x %02x %02x)"), 
                                   iDrawingId,
                                   GetRValue(crDraw),
                                   GetRValue(crDraw),
                                   GetRValue(crDraw));
            DB_PRINT(_T("%s%s\n"), strSpc.c_str(), strColor.c_str());

            const CDrawingObject*  pObjTop = pObj->GetTopDraw(pDrawingParent.get());

            if (pObjTop)
            {
                StdString strTop;
                DRAWING_TYPE eParentType = pObjTop->GetType();
                strType = CUtil::StrFormat(_T("ObjectTop Id:%d Type:%s"), 
                                       pObjTop->GetId(),
                                       VIEW_COMMON::GetObjctTypeName(eParentType).c_str());
                DB_PRINT(_T("%s%s\n"), strSpc.c_str(), strType.c_str());
            }
        }

        const MAT2D* pParentMat = pObj->GetParentMatrix();
        if (pParentMat)
        {

            StdString strParentMat =_T("Parent: ");
            strParentMat += pParentMat->StrAffine();
            DB_PRINT(_T("%s%s\n"), strSpc.c_str(), strParentMat.c_str());

            auto pParent = pObj->GetDrawingParent();
            if (pParent)
            {
                const MAT2D* pParentMat2 = pParent->GetDrawingMatrix();
                StdString strParentMat2 =_T("Parent2: ");
                strParentMat2 += pParentMat2->StrAffine();
                DB_PRINT(_T("%s%s\n"), strSpc.c_str(), strParentMat2.c_str());
            }
        }


        MAT2D  mat = pObj->GetMatrix();
        StdString strMat =_T("Mat: ");
        strMat += mat.StrAffine();

        DB_PRINT(_T("%s%s\n"), strSpc.c_str(), strMat.c_str());
        DB_PRINT(_T("\n"));

        if ((eType == DT_PARTS)||
            (eType == DT_FIELD)||
            (eType == DT_GROUP))
        {
            auto pGroup = std::dynamic_pointer_cast<CDrawingParts>(pObj);
            if (pGroup != NULL)
            {
                strSpc += _T("  ");
                std::deque<std::shared_ptr<CDrawingObject>>* pListGroup = NULL;
                pList = pGroup->GetList();
                if (pListGroup)
                {
                    PrintList(pView, pListGroup, strSpc);
                }
            }
        }
        else if (eType == DT_REFERENCE)
        {
            auto pGroup = std::dynamic_pointer_cast<CDrawingParts>(pObj);
            if (pGroup != NULL)
            {
                std::shared_ptr<CObjectDef> pDef;
                pDef = pGroup->GetObjectDef().lock();
                CReferenceDef* pRef = dynamic_cast<CReferenceDef*>(pDef.get());
                if (pRef)
                {
                    auto pParts = pRef->GetDrawingParts();
                    strSpc += _T("  ");
                    std::deque<std::shared_ptr<CDrawingObject>>* pListGroup = NULL;
                    pListGroup = pParts->GetList();
                    PrintList(pView, pListGroup, strSpc);
                }
            }
        }
    }
}


StdString CDrawingView::GetDraingMode() const
{
    StdString strRet;
    if (m_eMode & DRAW_FIG)
    {
        strRet = _T("FIG ");
    }

    if (m_eMode & DRAW_SEL)
    {
        strRet += _T("SEL ");
    }

    if (m_eMode & DRAW_INDEX)
    {
        strRet += _T("IDX ");
    }

    if (m_eMode & DRAW_FRONT)
    {
        strRet += _T("FRT ");
    }

    if (m_eMode & DRAW_PRINT)
    {
        strRet += _T("PRN ");
    }

    return strRet;
}

int CDrawingView::GetDebugMode()
{
    return m_iDebug;
}

int CDrawingView::GetDebugCount() const
{
    return m_iDebugCount;
}

void CDrawingView::AddDebugCount()
{
    m_iDebugCount++;
}

void CDrawingView::ClearDebugCount()
{
    m_iDebugCount = 0;
}

void CDrawingView::OnDebug(UINT c)
{
    std::deque<std::shared_ptr<CDrawingObject>>* pList = NULL;

    auto pDef = m_pDoc->GetObjectDef().lock();
    if( pDef )
    {
        auto pPartsDef = std::dynamic_pointer_cast<CPartsDef>(pDef);
        auto pParts =  pPartsDef->GetDrawingParts();
        pList = pParts->GetList();
    }

    switch(c)
    {
    case 'A':
        m_iDebugCount = 0;
        m_iDebug = DM_VIEW;
        GetPartsDef()->Redraw(this);
        m_iDebug = DM_DISABLE;
        break;

    case 'B':
        PrintList(this, pList, _T(""));
        break;

    case 'C':
    case 'D':
        if (m_iDebug == DM_DISABLE)
        {
            m_iDebugCount = 0;
            if (c == 'D')
            {
                m_iDebug = DM_NO_CHILD;
            }
            else
            {
                m_iDebug = DM_VIEW;
            }
        }
        else
        {
            m_iDebug = DM_DISABLE;
        }

        DB_PRINT(_T("DebugMode %s\n"), (m_iDebug != DM_DISABLE)?_T("ON"):_T("OFF"));
        break;
	case 'E':
		DB_PRINT(_T("------\n"));
		DB_PRINT(_T("---Def---\n"));
		pDef->Print();
		DB_PRINT(_T("---Def End--\n"));

		m_psMarker->Print();;
		if (!m_lstDraggingTmpObject.empty())
		{
			DB_PRINT(_T("---m_lstDraggingTmpObject---\n"));
			for (auto pObj : m_lstDraggingTmpObject)
			{
				DB_PRINT(_T("  %s\n"), pObj->Text().c_str());
			}
		}
		DB_PRINT(_T("------\n"));
		//std::vector<std::shared_ptr<CDrawingNode>> m_lstDraggingNode;
		//::std::vector<CDrawingNode*> m_lstNode;
		break;

    case 'U':
        {
        CUndoAction* pUndo  = pDef->GetUndoAction();
        pUndo->PrintUndoBuffer();
        }
        break;

    }

}