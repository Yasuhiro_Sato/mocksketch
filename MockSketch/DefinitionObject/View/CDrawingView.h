/**
 * @brief			CDrawingViewヘッダーファイル
 * @file			CDrawingView.h
 * @author			Yasuhiro Sato
 * @date			01-2-2009 10:36:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#if !defined(EA_2BF4D5E2_4960_4500_A2C8_EB6B89A5F548__INCLUDED_)
#define EA_2BF4D5E2_4960_4500_A2C8_EB6B89A5F548__INCLUDED_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "View/DRAWING_MODE.h"
#include "View/DRAWING_TYPE.h"
#include "View/VIEW_MODE.h"
#include "View/ViewCommon.h"
#include "DrawingObject/BaseObj.h"             

class   CCanvas;
class   CPartsDef;
class   CDrawingObject;
class   CDrawingScriptBase;
class   CDrawingParts;
class   CDrawingField;
class   CLayer;
class   CViewAction;
class   CDrawMarker;
class   POINT2D;
class   LINE2D;
class   RECT2D;
class   CObjectDef;
class   CFieldDef;
class   CNodeMarker;
class   CImageData;
class   CExtText;
struct  TEXT_PARAM;
class   CUnit;
class   CMockSketchDoc;
class   CDotLine;
struct  SNAP_DATA;
class   CDrawingNode;
class   CNodeBound;
class   CNodeData;
enum    POINT_TYPE;

/**
 * @class   CDrawingView
 * @brief                        
   
 */
class CDrawingView :public CBaseObj
{
public:
    enum DEBUG_MODE
    {
        DM_DISABLE  = 0,
        DM_VIEW     = 1,
        DM_NO_CHILD = 2,
    };

    enum E_CLIP_VALUE
    {
        E_LEFT = 0x01,
        E_RIGHT = 0x02,
        E_TOP = 0x04,
        E_BOTTOM = 0x08,
        E_MIN = -32766,
        E_MAX =  32765,
    };
protected:

    //!< 描画モード
    DRAWING_MODE m_eMode;

    //!< 画面操作モード
    VIEW_MODE m_eView;

    //!< 画面操作モード
    VIEW_MODE m_eViewInterrupt;

    //!< 選択割り込み
    VIEW_MODE m_eInterruptSelect;

    //!< Cut/Copyモード
    bool m_bCutMode;

    //!< 1unitあたりのドット数
    double       m_dDpu;   

    //!< オフセット
    POINT       m_ptOffset;

    //!< 線種
	int m_iPenStyle;

	//!< 線幅
    int m_iPenWidth;

	//!< 線色
    COLORREF m_crPen;

    //!< ペン
    HPEN    m_hPen;

    //!< ペン
    HPEN    m_hOldPen;

    //!< 背景色ブラシ
    HBRUSH  m_brBack;

	//!< インデックスバッファー
	mutable std::shared_ptr<CCanvas> m_psIndex;

	//!< インデックスバッファー
	mutable std::shared_ptr<CCanvas> m_psIndexHi;

	//!< バックバッファー
	mutable std::shared_ptr<CCanvas> m_psBack;

	//!< 選択バッファー
	mutable std::shared_ptr<CCanvas> m_psSelect;

	//!< 表面
	mutable std::shared_ptr<CCanvas> m_pSurface;

    //!< マーカー
	std::shared_ptr<CDrawMarker> m_psMarker;
    
    //!< ドラッグ用マーカー
	std::shared_ptr<CNodeMarker> m_psNodeMarker;

    std::unique_ptr<CNodeBound>    m_psNodeBound;

    //
    //!< 再描画フラグ
    bool    m_bRedraw;  

    //!< 初期化
    bool    m_bInit;  

    //!< 部品定義
    //CPartsDef*   m_pPartsDef;

    //!< フィールド定義
    //CFieldDef*   m_pFieldDef;

    //!< 実行インスタンス
    //CDrawingParts*         m_pDrawingParts;

    //!< 実行インスタンス
    //CDrawingField*         m_pDrawingField;

    //!< ドキュメント
    CMockSketchDoc*          m_pDoc;

    //!< 動作設定
    std::map< VIEW_MODE, std::shared_ptr<CViewAction> > m_mapViewAction;

    //!< 現在の動作
    CViewAction* m_pCurAction;

    //!< ディフォルトの動作
    std::shared_ptr<CViewAction> m_pDefaultAction;

    //!< ビュー
    CWnd    *m_pView;

    //!< 表示デバイスコンテキスト
    HDC          m_hDc;   

    VIEW_COMMON::VIEW_VALUE   m_Vv;
    
    /*
    //ウインドウ幅
    int      m_iWidth;

    //ウインドウ高さ
    int      m_iHeight;

    //!< 表示スケール
    double   m_dDspScl;

    //!< 注視点
    POINT      m_ptLook;
    */

    //インプットバー高さ
    int      m_iInputBarHeight;


    //!<ミューテックス
    boost::mutex m_Mtx; 

    //!< マウス移動
    bool m_bMouseMove;
    std::deque<MOUSE_MOVE_POS> m_lstMouseMove;

    //!< 表示単位系
    std::shared_ptr<CUnit> m_psUnit;

    //!< 最小スナップ長にDot数
    double m_dMinResDot;

	//!< 表示中のグリッドサイズ
	double m_dUseGridSize;
	
	//!< 印刷中フラグ
	bool m_bPrint;

    //印刷用の線の太さ

    //----------------
    // 印刷時一時退避
    //---------------
    VIEW_COMMON::VIEW_VALUE   m_VvOld;
    double m_dDpuOld;
    HDC    m_hDcOld;

    //-------------------------------
    //ドラッグ時一時使用オブジェクト
    //-------------------------------
    std::vector<std::shared_ptr<CDrawingNode>> m_lstDraggingNode;
    std::vector<std::shared_ptr<CDrawingObject>> m_lstDraggingTmpObject;
    std::vector<CDrawingNode*> m_lstNode;

    bool m_bDrag;

	//----------------
	// 印刷枠
	//---------------
	int m_iOldPrintCol;
	int m_iOldPrintRow;

public:
    //!< コンストラクタ
    CDrawingView();

    //!< デストラクタ
    virtual ~CDrawingView();

    //!< 初期化問い合わせ
    bool IsInit() const;

    VIEW_COMMON::VIEW_VALUE GetViewValue() const { return m_Vv; }
    //!< 部品定義設定
    //void SetPartsDef( CPartsDef* pPartsDef);

    //!< 実行インスタンス設定
    //void SetDrawiingScript( CDrawingScriptBase* pScriptBase);

    //!< ドキュメント設定
    void SetDocument( CMockSketchDoc* pDoc);

    //!< ウインドウ設定
    void SetWindow( CWnd* pView );

    //!< ウインドウ削除
    void DestoryWindow();

    //!< ウインドウ再設定
    void ResetCtrl();

    //!< ウインドウ取得
    CWnd* GetWindow();

    //!< オブジェクト追加
    //void AddObject( CDrawingObject* pObj);

    //!< 選択オブジェクト追加
    //void AddSelect( CDrawingObject* pObj);

    //!< グループ追加
    //void AddGroup( POINT ptClick, std::shared_ptr<CObjectDef> pDef);

    //!< ドロップ位置検索
    //void SearchDropPos(POINT ptPos);

    //!< デバイスコンテキスト設定
    void SetDc( CDC* pDc);

    HDC GetDc();

    //!< 描画モード設定
    void SetDrawingMode( DRAWING_MODE eMode);

    //!< ID設定
    void SetIndexBufferId( int iID);

    //!< 点線の隙間を塗りつぶすモード
    void SetLineAlternateMode(bool bSet);

    //!< ペン設定
    void SetPen( int iPenStyle, int iWidth, COLORREF crPen, int iID);

    //!< ペン色取得
    COLORREF GetPenColor();

    //!< ブラシ設定
    void SetBrush( COLORREF crBrush);

    //!< データ->ビュー変換
    void ConvWorld2Scr(POINT* pPt, POINT2D pt2D, int iLayerID, bool bFixLayerScl = false) const;
         
    //!< データ->ビュー変換
    void ConvWorld2Scr(int* piLen, double dLen, int iLayerID, bool bFixLayerScl = false) const;

    //!< ビュー->データ変換
    void ConvScr2World(POINT2D* pPt2D, POINT Pt, int iLayerID, bool bFixLayerScl = false) const;

    //!< ビュー->データ変換
    void ConvScr2World(POINT2D* pPt2D, double dX, double dY, int iLayerID, bool bFixLayerScl = false) const;
    
    //!< ビュー->データ変換
    void ConvScr2World(double* pdL, int iL, int iLayerID, bool bFixLayerScl = false) const;


    //! ウインドウサイズ変更
    void OnSize(int iX, int iY);

    //! カレントレイヤー取得
    CLayer* GetCurrentLayer() const;

    //! レイヤー取得
    CLayer* GetLayer(int iId) const;

    //! カレントレイヤーID取得
    int GetCurrentLayerId() const;

    //!< レイヤー取得
    RECT2D  GetViewBounds() const;

    //!< マーカ取得
    CDrawMarker* GetMarker();

    //!< ドラッグ用マーカ取得
    CNodeMarker* GetNodeMarker();
    const CNodeMarker* GetNodeMarker() const;

    //!< 領域マーカ取得
    CNodeBound*  GetNodeBound();              
    const CNodeBound*  GetNodeBound() const;              

    //!< フォーカス設定
    void SetFocus();

    int GetTextDatum();
    int GetTextAlign();

    //-----------------------
    //  入力領域
    //----------------------

	//!< 数値入力
	bool InputValue(StdString strVal);

	//!< コメント入力
    void SetComment(StdString strComment);

    //!< 入力消去
    void ClearInput();

	//!< 入力可・不可設定
	void EnableInput(bool bEnable);

    //!< フォーカス設定
    void SetInputFocus();

    //!< 入力値取得
    StdString GetInputValue();

    //!< 入力値更新
    void UpdateInput(bool bOk);

    //!< 表示単位系取得
    CUnit* GetUnit();

    //-----------------------
    //  描画命令
    //----------------------
    void Point(POINT pt)const;

	void PointMark(POINT pt, POINT_TYPE eType, int iSize)const;

    void Line(POINT pt1, POINT pt2)const;

    void Ellipse(RECT rc)const;

    void FillEllipse(RECT rc)const;

	void Rect(RECT rc)const;

	void FillRect(RECT rc)const;
	
	void Arc(RECT rc, POINT pt1, POINT pt2, bool bClockWise = false)const;

    void ArcTo(RECT rc, POINT pt1, POINT pt2, bool bClockWise = false)const;

    void Polyline(const std::vector<POINT>& lstDot, bool bSkipStartPoint = false)const;

    void FillPolyline(const std::vector<POINT>& lstDot)const;

    void Image(POINT pt1, CImageData* pImage, int iNo = 0)const;

    void Image(POINT pt, CImageData* pImage, 
                  int iNo,
                  double dSclX,
                  double dSclY,
                  double dAngle,
                  DWORD dwTrans) const;

    void ImageBounds(POINT pt1, CImageData* pImage)const;

    void ImageBounds(POINT pt, CImageData* pImage, 
                  double dSclX,
                  double dSclY,
                  double dAngle) const;

    void ImageFill(POINT pt, CImageData* pImage, 
                  double dSclX,
                  double dSclY,
                  double dAngle,
                  COLORREF cr) const;

    void Text(const CExtText* pText, POINT pt, 
                    const TEXT_PARAM* pParam,
                    int iLayerId) ;

    void SimpleText(POINT pt, double dAngle, double dHeight)const;

    void MoveTo(POINT pt)const;
    void LineTo(POINT pt)const;


    int  GetIndex(POINT pt) const;

    void BeginPath() const;

    void EndPath() const;

    void StrokePath() const;

    void CloseFigure() const;

    void FillPath(COLORREF crFill, int    iId) const;

    void HatchingPath(int fnStyle, COLORREF color, int    iId) const;

    void FillPolygon(const std::vector<POINT>& lstPt) const;

     //-----------------------
    //  コマンド
    //----------------------
    // Flip以外は、CPartsDefにまとめる
    //<! 再描画 
    void FlipBackToSelect();

    void FlipSelectToFront();

    //void RedrawRequest();

    //<! 再描画
    //void FlipRequest();

    //!< 再描画
    //void Redraw( HDC hDc);

    //!< 再描画
    //void Flip( HDC hDc);

    //!< 画面消去
    void ClearDisp( bool bClearFront);

    //<! アクション再描画 
    void RedrawAction();

    //<! アンドゥ 
    void Undo();

    //<! アンドゥ問い合わせ
    bool IsUndoEmpty();

    //<! リドゥ
    void Redo();

    //<! リドゥ問い合わせ
    bool IsRedoEmpty();
    //----------------------
    //  画面操作
    //----------------------
    //!< 画面操作モード
    bool SetViewMode (VIEW_MODE eView, bool bOverrideInterrupt = false, void* pParam = NULL);

    //!< 画面操作モード取得
    VIEW_MODE  GetViewMode()  const;

    //!< 選択割り込み
    bool  IntrruptSelect();

    //!< 選択割り込み解除
    bool  RelIntrruptSelect();

    //!< 画面描画モード取得
    DRAWING_MODE  GetDrawMode() const;

    //!< カーソル内オブジェクト検索
    int SearchPos(POINT ptPos, bool bIgnoreSpecialType = false) const;
 
    //!< カーソル内オブジェクト全検索
    int SearchAllPos(std::set<int>* pLstIndex, POINT ptPos) const;

	//!< ドットに対する長さ
	double CDrawingView::DotToLength(double dDot) const;

    //!< スナップ長取得
    bool GetSnapLength(double* pLen, double dLen) const;

    //!< スナップ最小長取得
    double GetSnapMinLength() const;

    //!< スナップ点取得
    bool GetSnapPoint(  SNAP_DATA* pSnap,
                        DWORD dwSnapType,
                        POINT ptPos,
                        bool bIngnoreGroup = false,
						bool bKeepCenter   = true,
						int iIgnoreObjectId = -1) const;


    bool GetSnapPointWithAddFunc(
                                std::list<SNAP_DATA>* pAdditonalList, //out func で追加するスナップ点出力
                                SNAP_DATA* pSnapData,
                                DWORD dwSnapType,
                                POINT ptPos,
                                std::function< bool (std::list<SNAP_DATA>*, CDrawingObject*) > func,
                                bool bIngnoreGroup = false,
								bool bKeepCenter = true,
								int iIgnoreObjectId = -1) const;

    //!< 長さによるスナップ点取得
    bool GetLengthSnapPoint(double* pAngle,
                              double* pLength,
                              POINT2D*  pPos,  
                              POINT2D   ptStart,  
                              POINT2D   ptMouse,
                              bool      bAngle,
                              bool      bDistance,
                              double    dAngleOffset = 0.0,
                              double    dLengthOffset = 0.0) const;

    //!< 長さ固定スナップ点取得
    bool GetFixLengthSnapPoint(double* pAngle,
                              POINT2D*  pPos,  
                              double    dLength,
                              POINT2D   ptStart,  
                              POINT2D   ptMouse,
                              bool      bSnap) const;

    //!< 角度固定スナップ点取得
    bool GetFixAngleSnapPoint(double* pLength,
                              POINT2D*  pPos,  
                              double    dAngle,
                              POINT2D   ptStart,  
                              POINT2D   ptMouse,
                              bool      bSnap,
	                          double dOffsetLength = 0.0) const;

    //!< 中心点と一点を通る直線へのスナップ点
    bool GetLineSnapPoint(    POINT2D*  pPos,  
                              POINT2D   ptCenter,  
                              POINT2D   ptMid,  
                              POINT2D   ptMouse) const;

    //!< 中心点と一点を通る直線へのスナップ点(方向なし)
    bool GetInfLineSnapPoint(    POINT2D*  pPos,  
        POINT2D   ptCenter,  
        POINT2D   ptMid,  
        POINT2D   ptMouse) const;

    //<! 特徴点取得(交点なし)
    bool GetMousePoint(
                    SNAP_DATA* pSnap,
                    MOUSE_MOVE_POS posMouse,
                    bool bIgnoreGroup = false,
					bool bKeepCenter = true,
                    int iIgnoreObjectId = -1);
 
    //<! 特徴点取得(交点,初期化あり)
    bool GetMousePoint2(
                    SNAP_DATA* pSnap,
                    MOUSE_MOVE_POS posMouse,
                    bool bIgnoreGroup = false,
					bool bKeepCenter = true,
					int iIgnoreObjectId = -1);

    //<! 特徴点取得(交点,初期化あり)
    bool GetMousePointSingle(SNAP_DATA* pSnap,
                    std::shared_ptr<CDrawingObject>* ppObj,
                    MOUSE_MOVE_POS posMouse,
                    bool bIgnoreGroup,
					bool bKeepCenter,
					int iIgnoreId);

    //マーカ設定
    bool  SetMarker(CDrawingObject* pObj,
                    bool bIgnoreOffLineObject = false);

    bool  SetMarkerWithAddSnapList(
                     CDrawingObject* pObj,
                     const std::list<SNAP_DATA>* pAddSnapList,
                     const CNodeData* pNodeData = NULL,
                     bool bIgnoreOffLineObject = false);

    //!< 画面上のスナップ半径取得
    double GetSnapRadius() const;

    //!< オブジェクトコントロール取得
	CPartsDef* GetPartsDef() const;

    //!< デバイスコンテキスト取得
    HDC GetViewDc()  const;

     //!< 裏画面デバイスコンテキスト取得
	HDC GetBackDc()  const;

    //!< インデックス画面デバイスコンテキスト取得
	HDC GetIndexDc() const;

    //!< インデックス画面デバイスコンテキスト取得
	HDC GetIndexHiDc() const;

    //!< 選択画面デバイスコンテキスト取得
    HDC GetSelDc() const;

    //!<マウスキャプチャー
    void SetCapture();

    //!< センター位置設定
    void SetCenter(POINT pt);

    //!< 表示スケール変更
    void ViewScale(double dScl,POINT pt);

    //!< 表示倍率取得
    double GetViewScale() const;

    //!< 表示倍率設定
    void SetViewScale(double dScl);

    //!< X方向 dot/unit
    double GetDpu() const{ return m_dDpu;}

   //!< ウインドウ幅
    int GetWindowWidth() const{ return m_Vv.iWidth;}

    //!< ウインドウ高さ 
    int GetWindowHeight() const{ return m_Vv.iHeight;}

    //!< インプットバー高さ設定
    void SetInputBarSize(int iSize);

    //!< 表示スケール変更
    void ZoomUp(int iTimes, POINT ptCenter);

    //!< 表示スケール変更
    void ZoomDown(int iTimes , POINT ptCenter);

    //!< 表示位置変更
    void ViewMove(const POINT2D* pt2D);

    //!< 注視点取得
    POINT2D GetLookAt() const;

    //!< 全体表示
    void ViewAll();

    //!< リッチエディット取得
    CRichEditCtrl*  GetRichEdit();

    //!< リッチエディット初期化
    void  InitRichEdit();

    bool CalcRichEditSize(SIZE* pSize);

    //!< 表示領域取得
    RECT2D GetViewArea(int iLayerId) const;

    //!< アクション割り込み
    bool InterruptAction(VIEW_MODE eMode, MOUSE_MOVE_POS posMouse);

    //!< アクション割り込み復旧
    bool RetInterrupt();

    //!< アクション割り込み解除
    bool RelInterrupt();

    //----------------------
    //  マウス操作
    //----------------------
    //!< マウス移動
    bool OnMouseMove(UINT nFlags, POINT point);

    //!< マウス左ボタン解放
    bool OnLButtonUp(UINT nFlags, POINT point);

    //!< マウス左ボタン押下
    bool OnLButtonDown(UINT nFlags, POINT point);

    //!< マウス移動右ボタン解放
    bool OnRButtonUp(UINT nFlags, POINT point);

    //!< マウス移動右ボタン押下
    bool OnRButtonDown(UINT nFlags, POINT point);

    //!< マウス移動
    bool OnLButtonDblClick(UINT nFlags, POINT point);

    //!< マウスオーバー解除
    void  CancelObject(CDrawingObject* pObj);

    //!< コンテキストメニュー表示
    bool OnContextMenu(POINT point);

    //!< マウスカーソル設定
    bool OnSetCursor();

    //!< ドラッグ中判定
    bool IsDrag() const;

	//!< メニュー選択
    void OnMenuSelect(UINT nItemID, UINT nFlags, HMENU hSysMenu);

    //Cut/Copyモード設定
    void SetCutMode(bool bCut);

    //Cut/Copyモード取得
    bool GetCutMode() const;

    //----------------------
    //  ツールチップ
    //----------------------
    //!< ツールチップ設定
    void SetToolTipText(LPCTSTR strText);

    //!< ツールチップ消去
    void ResetToolTipText();

    //----------------------
    //  その他操作
    //----------------------
    void OnCancelAction();

    //!< 印刷設定
    void PrepareDC(CDC* pDC, CPrintInfo* pInfo);
    void BeginPrint(CDC* pDC, CPrintInfo* pInfo);
    void PreparePrinting(CPrintInfo* pInfo);
    void EndPrinting();
    void EndPrintPreview();
    bool IsPrintMode() const;
	bool CheckPrintArea(POINT ptMouse);
    void DrawPrintArea();
    void DrawPrintArea(HDC hDC);
    void UpdatePrinter();
    void ChangeObjectOrder(VIEW_COMMON::E_DISP_ORDER order);


    static void CheckDc(HDC hDc);

	bool CheckGrid(POINT ptMouse);
	void DrawGrid();
	void DrawGrid(HDC  hDc);

    //---------------------------
    // ドラッグ用一時オブジェクト
    //---------------------------
    void ClearDraggingNodes();
    void ClearDraggingTmpObjects();

    std::vector<std::shared_ptr<CDrawingNode>>* GetDraggingNodes();
    std::vector<std::shared_ptr<CDrawingObject>>* GetDraggingTmpObjects();

    //---------------------------
    // ノードドラッグ用一時オブジェクト
    //---------------------------
    void AddConnectableNode(CDrawingNode* pNode);
    void ClearConnectableNode();
    std::vector<CDrawingNode*> * GetConnectableNodeList();

	//アクションクリア
	void ClearCurrentAction();

    StdString GetDraingMode() const;
    void OnDebug(UINT c);
    int GetDebugMode();

    int GetDebugCount() const;
    void AddDebugCount();
    void ClearDebugCount();


private:
    void _FillPath(HBRUSH hBr, int    iId) const;

    //!< アクション設定初期化
    void  InitAction();

    //!<  画面関連初期化
    void  InitDisp();

    int _ClipPos(POINT pt)const;
    int _CalcClippedPoint(int code, POINT pt1, POINT pt2, POINT* pPt)const;
    int _Clipping(POINT* pPt1, POINT* pPt2)const;

    bool _MouseMove(MOUSE_MOVE_POS posMouse);

    int m_iDebug = DM_DISABLE;

    int m_iDebugCount = 0;

};
#endif // !defined(EA_2BF4D5E2_4960_4500_A2C8_EB6B89A5F548__INCLUDED_)
