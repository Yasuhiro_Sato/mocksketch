/**
 * @brief			CElementView実装ファイル
 * @file			CElementView.cpp
 * @author			Yasuhiro Sato
 * @date			01-2-2009 10:36:26
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MockSketch.h"
#include "MainFrm.h"
#include "DefinitionObject/View/ObjectDefView.h"

#include "./CElementView.h"
#include "DefinitionObject/View/MockSketchDoc.h"
#include "DefinitionObject/CElementDef.h"
#include "Utility/CUtility.h"
#include "ExecCommon.h"


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
IMPLEMENT_DYNCREATE(CElementView, CFormView)

BEGIN_MESSAGE_MAP(CElementView, CFormView)
    ON_MESSAGE(WM_MODE_CHANGE, OnModeChange)
    ON_BN_CLICKED(IDC_PB_EDIT, &CElementView::OnBnClickedPbEdit)
    ON_CBN_SELCHANGE(IDC_CB_SEL_ICON, &CElementView::OnCbnSelchangeIcon)
    ON_MESSAGE(WM_VIEW_UPDATE_NAME, OnUpdateName)

    ON_EN_KILLFOCUS(IDC_ED_ICON_NUM, &CElementView::OnEnKillfocusEdIconNum)
    ON_EN_KILLFOCUS(IDC_ED_ICON_SIZE, &CElementView::OnEnKillfocusEdIconSize)
    ON_EN_UPDATE(IDC_ED_ICON_NUM, &CElementView::OnEnUpdateEdIconNum)
    ON_EN_UPDATE(IDC_ED_ICON_SIZE, &CElementView::OnEnUpdateEdIconSize)
END_MESSAGE_MAP()


/**
 * @brief   コンストラクタ
 * @param   なし         
 * @retval  なし
 * @note	
 */
CElementView::CElementView():
CFormView(CElementView::IDD),
m_bInit         (false),
CObjectDefView  ()
{
    THIS_APP->AddView(this);
    SetWindow(this);
}

/**
 * @brief   デストラクタ
 * @param   なし         
 * @retval  なし
 * @note	
 */
CElementView::~CElementView()
{
    THIS_APP->DelView(this);
}

/**
 *  @brief  フレームワークの自動的なデータ交換
 *  @param  pDX     CDataExchange オブジェクトへのポインタ
 *  @retval なし     
 *  @note   UpdateData メンバ関数から呼び出されます。
 */
void CElementView::DoDataExchange(CDataExchange* pDX)
{
    CFormView::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_ED_ICON_NUM, m_edNum);
    DDX_Control(pDX, IDC_SPN_NUM, m_spnNum);
    DDX_Control(pDX, IDC_ST_ICON_NUM, m_stNum);

    DDX_Control(pDX, IDC_ED_ICON_SIZE, m_edSize);
    DDX_Control(pDX, IDC_SPN_SIZE, m_spnSize);
    DDX_Control(pDX, IDC_ST_SIZE, m_stSize);

    DDX_Control(pDX, IDC_ST_ICON_EDITOR, m_stIcon);
    DDX_Control(pDX, IDC_CB_SEL_ICON, m_cbSelIcon);
}

/**
 *  @brief  描画処理
 *  @param  [in] pDC 描画に使用するデバイス コンテキストへのポインタ
 *  @retval なし     
 *  @note
 */
void CElementView::OnDraw(CDC* pDC)
{
    CDocument* pDoc = GetDocument();

    int iSel;

    iSel = m_cbSelIcon.GetCurSel();

    if (iSel < 0)
    {
        return;
    }

    std::shared_ptr<CElementDef>pElementDef = GetElementDef();

    if (!pElementDef)
    {
        return;
    }

    CImageData* pImage;
    pImage = pElementDef->GetIconData(iSel);

    if (!pImage)
    {
        return;
    }

    POINT pt;
    RECT  rc;

    m_stIcon.GetWindowRect(&rc);
    ScreenToClient(&rc);

    pt.x = rc.top;
    pt.y = rc.left;
    pImage->Draw(pDC->m_hDC, pt, 0);
}


// CElementView 診断

#ifdef _DEBUG
void CElementView::AssertValid() const
{
	CView::AssertValid();
}

#ifndef _WIN32_WCE
void CElementView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif
#endif //_DEBUG


/**
 * @brief   ビュー アクティブ状態変更
 * @param   [in]    bActivate       ビューがアクティブ/非アクティブ
 * @param   [in]    pActivateView   アクティブにされているビューへのポインタ
 * @param   [in]    pDeactiveView   非アクティブになるビューへのポインタ
 * @return  なし
 * @note    フレームワークから呼び出し
 */
void CElementView::OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView)
{
    CView::OnActivateView(bActivate, pActivateView, pDeactiveView);
    if (bActivate)
    {
        //ウインドウの切り替えを通知
        StdString strTitle;
        CDocument* pDoc = GetDocument();

        if (pDoc == NULL)
        {
            STD_DBG(_T("GetDocument"));
            return;
        }

        strTitle = (LPCTSTR)pDoc->GetTitle();
        strTitle = CMockSketchDoc::SplitTitleMarker(strTitle.c_str());
        ::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_VIEW_ACTIVE, (WPARAM)&strTitle, (LPARAM)0);
        THIS_APP->SetAvtiveTabNmae(strTitle);
        THIS_APP->GetMainFrame()->SetActiveView(this, FALSE);
    }
    CFormView::OnActivateView(bActivate, pActivateView, pDeactiveView);
}


/**
 * @brief   オブジェクト定義設定
 * @param   [in] pObjectDef  オブジェクト定義
 * @return  true 設定成功
 * @note
 */
/*
bool CElementView::SetObjectDef(CObjectDef* pObjectDef)
{
    //  TODO:要検討
    //  m_pElementDefはDoc経由で設定されるはず
    if (m_pDrawingScript != NULL)
    {
        return false;
    }
    m_pObjectDef = pObjectDef;
    m_pElementDef = dynamic_cast<CElementDef*>(m_pObjectDef);
    return true;
}
*/


/**
 *  @brief モード変更
 *  @param [in] wParam   アプリケーションモード(EXEC_COMMON::E_APP_MODE)
 *  @param [in] lParam   オブジェクト定義      (CObjectDef)
 *  @retval
 *  @note
 */
LRESULT CElementView::OnModeChange(WPARAM wParam, LPARAM lParam)
{
    using namespace EXEC_COMMON;

    E_EXEC_STS  eSts;
    CObjectDef* pDef;

    eSts = static_cast<E_EXEC_STS>(wParam);
    pDef = reinterpret_cast<CObjectDef*>(lParam);

    if (eSts == EX_EDIT)
    {
    }

    return 0;
}


void CElementView::OnInitialUpdate()
{
    CFormView::OnInitialUpdate();

    // ドキュメントを更新
    CDocument* pDoc = GetDocument();
    std::shared_ptr<CObjectDef> pDef;

    if (pDoc == NULL)
    {
        STD_DBG(_T("GetDocument"));
    }



    //TODO:リソース化
    GetDlgItem(IDC_PB_EDIT)        ->SetWindowText(_T("Edit"));

    //スピンボタン
    m_spnNum.SetBuddy(&m_edNum);
    m_edNum.SetRange ( 1, 10);
    m_spnNum.SetRange32( 1, 10);

    m_spnSize.SetBuddy(&m_edSize);
    m_edSize.SetRange ( 16, 256);
    m_spnSize.SetRange32( 16, 256);

    //m_cbSelIcon.SetCurSel(0)

    std::shared_ptr<CElementDef>pElementDef = GetElementDef();
    if (pElementDef)
    {
        static TCHAR szCb[32];
        StdString strCb;
        int iMax = pElementDef->GetMax();
        if (iMax == 0)
        {
            pElementDef->InitIcon(1, 32, 32);
            iMax = 1;
        }

        m_edNum.SetVal(iMax);
        m_edSize.SetVal(pElementDef->GetWidth());
        UpdateCombo(iMax);
    }

    UpdateData(FALSE);
    RedrawWindow();
    m_bInit = true;
}

void CElementView::OnBnClickedPbEdit()
{
    int iSel;

    iSel = m_cbSelIcon.GetCurSel();

    if (iSel < 0)
    {
        return;
    }

    std::shared_ptr<CElementDef>pElementDef = GetElementDef();
    if(!pElementDef)
    {
        return;
    }

    CImageData* pImage;
    pImage = pElementDef->GetIconData(iSel);
    if (pImage)
    {
        pImage->Edit();
        RedrawWindow();
    }
}


void CElementView::OnCbnSelchangeIcon()
{
    RedrawWindow();
}

/**
 * @brief   参照元取得
 * @param   なし
 * @retval  参照元へのポインタ
 * @note    
 */
std::shared_ptr<CElementDef>  CElementView::GetElementDef() const
{
    std::shared_ptr<CElementDef> pRet;
    CDocument* pDoc = GetDocument();
    if (pDoc == NULL)
    {
        STD_DBG(_T("GetDocument"));
        return  pRet;
    }

    CMockSketchDoc* pSketchDoc;
    pSketchDoc = reinterpret_cast<CMockSketchDoc*>(pDoc);
 
    std::shared_ptr<CObjectDef> pDef;
    
    pDef = pSketchDoc->GetObjectDef().lock();

    if (!pDef)
    {
        return pRet;
    }

    pRet = std::dynamic_pointer_cast<CElementDef> (pDef);
    return pRet;
}

/**
 * @brief   名前更新
 * @param   [in] wParam 使用しない
 * @param   [in] lParam 使用しない
 * @return  なし
 * @note    CObjectWnd::OnEndLabelEditから呼び出し
 */
LRESULT CElementView::OnUpdateName(WPARAM wParam, LPARAM lParam)
{
    // ドキュメントを更新
    CDocument* pDoc = GetDocument();
    if (pDoc == NULL)
    {
        STD_DBG(_T("GetDocument"));
        return 0;
    }
    CMockSketchDoc* pSketchDoc;
    pSketchDoc = reinterpret_cast<CMockSketchDoc*>(pDoc);
    pSketchDoc->UpdateName();

    return 0;
}

void CElementView::OnEnKillfocusEdIconNum()
{
    //
}

void CElementView::OnEnKillfocusEdIconSize()
{
}

void CElementView::UpdateCombo(int iMax)
{
    m_cbSelIcon.ResetContent();
    TCHAR szCb[32];
    for(int iCnt = 0; iCnt < iMax; iCnt++)
    {
        _sntprintf_s(szCb, sizeof(szCb), _T("%d"),   iCnt + 1);
        m_cbSelIcon.AddString(szCb);
    }
    m_cbSelIcon.SetCurSel(0);
}


BOOL CElementView::PreTranslateMessage(MSG* pMsg)
{
    
    static UINT uiOldMsg = 0;
    if ((WM_PAINT == pMsg->message)||
        (WM_TIMER == pMsg->message))
    {
        return 0;
    }

    if (uiOldMsg != pMsg->message)
    {
        uiOldMsg = pMsg->message;
#if 0
        DB_PRINT(_T("DLG %s,%x,%x,%x\n"), 
            CUtil::ConvWinMessage(pMsg->message),
            pMsg->hwnd,
            pMsg->wParam, 
            pMsg->lParam);
#endif
    }

    return __super::PreTranslateMessage(pMsg);
}

LRESULT CElementView::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
    // TODO: ここに特定なコードを追加するか、もしくは基本クラスを呼び出してください。

    return __super::WindowProc(message, wParam, lParam);
}

void CElementView::OnEnUpdateEdIconNum()
{
    if (!m_bInit)
    {
        return;
    }

    int iMax;
    int iSize;
    iMax  = m_edNum.GetVal();
    iSize =m_edSize.GetVal();

    std::shared_ptr<CElementDef>pElementDef = GetElementDef();
    if (pElementDef)
    {
        pElementDef->InitIcon(iMax, iSize, iSize);
        UpdateCombo(iMax);
        RedrawWindow();
    }
}

void CElementView::OnEnUpdateEdIconSize()
{
    if (!m_bInit)
    {
        return;
    }

    int iMax;
    int iSize;
    iMax  = m_edNum.GetVal();
    iSize =m_edSize.GetVal();

    std::shared_ptr<CElementDef>pElementDef = GetElementDef();
    if (pElementDef)
    {
        pElementDef->InitIcon(iMax, iSize, iSize);
        RedrawWindow();
    }
}
