/**
 * @brief			CElementViewヘッダーファイル
 * @file			CElementView.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#pragma once


/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "Utility/IntEdit.h"

/*---------------------------------------------------*/
/*  classes                                          */
/*---------------------------------------------------*/
class CObjectDef;
class CDrawingScriptBase;
class CElementDef;

class CElementView :  public CFormView, public CObjectDefView
{
	DECLARE_DYNCREATE(CElementView)

public:
    enum{ IDD = IDD_ELEMENT_VIEW_FORM };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV サポート

protected:
    //!< コンストラクタ
    CElementView();

    //!< デストラクタ
    virtual ~CElementView();


public:
    //!< 表示更新
    virtual bool UpdateView(){return false;}


	virtual void OnDraw(CDC* pDC);      // このビューを描画するためにオーバーライドされます。
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	DECLARE_MESSAGE_MAP()
    virtual void OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView);

    //!<モード変更
    afx_msg LRESULT OnModeChange(WPARAM wParam, LPARAM lParam);

    //!<名前更新
    afx_msg LRESULT OnUpdateName(WPARAM pMessage, LPARAM lParam);


    virtual void OnInitialUpdate();

    void UpdateCombo(int iMax);

    afx_msg void OnBnClickedPbEdit();
    afx_msg void OnCbnSelchangeIcon();

    std::shared_ptr<CElementDef>  GetElementDef() const;

protected:

    //CElementDef* m_pElementDef;

    CIntEdit        m_edNum;
    CSpinButtonCtrl m_spnNum;
    CStatic m_stNum;

    CIntEdit        m_edSize;
    CSpinButtonCtrl m_spnSize;
    CStatic         m_stSize;

    CStatic         m_stIcon;

    CComboBox       m_cbSelIcon;

    bool            m_bInit;

public:
    afx_msg void OnEnKillfocusEdIconNum();
    afx_msg void OnEnKillfocusEdIconSize();
    virtual BOOL PreTranslateMessage(MSG* pMsg);
protected:
    virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
public:
    afx_msg void OnEnUpdateEdIconNum();
    afx_msg void OnEnUpdateEdIconSize();
};


