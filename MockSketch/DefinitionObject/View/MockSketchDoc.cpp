/**
 * @brief			CPartsDef実装ファイル
 * @file			CPartsDef.cpp
 * @author			Yasuhiro Sato
 * @date			03-2-2009 23:20:46
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "stdafx.h"
#include "MockSketch.h"

#include "DefinitionObject/View/MockSketchDoc.h"

#include "DefinitionObject/View/CDrawingView.h"
#include "DrawingObject/CDrawingScriptBase.h"
#include "DefinitionObject/CPartsDef.h"
#include "Utility/CUtility.h"
#include "DefinitionObject/ObjectDef.h"
#include "CProjectCtrl.h"
#include "Script/CScript.h"


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNCREATE(CMockSketchDoc, CDocument)

BEGIN_MESSAGE_MAP(CMockSketchDoc, CDocument)
END_MESSAGE_MAP()


/**
 * @brief   コンストラクター
 * @param   なし
 * @return  なし
 * @note	シリアル化からのみ作成します。 
 */
CMockSketchDoc::CMockSketchDoc():
    m_uuidObjectDef( boost::uuids::nil_uuid() ),
    m_pScriptData ( NULL ),
    m_eDocType    ( VIEW_COMMON::E_DOC_NONE)
{
}

/**
 * @brief    デストラクタ
 * @param    なし
 * @return   なし
 * @note 
 */
CMockSketchDoc::~CMockSketchDoc()
{
}


/**
 * @brief    Document生成通知
 * @param    なし
 * @retval   なし
 * @note	 フレームワークから呼び出し
 */
BOOL CMockSketchDoc::OnNewDocument()
{

    //m_pPartsDef->Load(_T(""));

    //ドキュメント生成通知
    
    return TRUE;
}

/**
 * @brief    タイトル変更通知記号除去
 * @param    [in] strTitle
 * @retval   記号除去後のタイトル
 * @note	 
 */
CString CMockSketchDoc::SplitTitleMarker(LPCTSTR strTitle)
{
    int iPos;
    CString strRet;
    strRet = strTitle;

    iPos = strRet.FindOneOf( _T("*+-=#$"));

    strRet = strTitle;
    if (iPos != 0)
    {
        return strRet;
    }

    return strRet.Mid(1);
}

/**
 * @brief    シリアル化
 * @param    [in out] ar アーカイブ
 * @retval   なし
 * @note	 フレームワークから呼び出し
 */
void CMockSketchDoc::Serialize(CArchive& ar)
{
    if (ar.IsStoring())
    {
        // TODO: 格納するコードをここに追加してください。
    }
    else
    {
        // TODO: 読み込むコードをここに追加してください。
	}
}

/**
 * @brief    描画オブジェクト取得
 * @param    なし
 * @retval   描画オブジェクトへのポインタ
 * @note	 
 */
std::weak_ptr<CObjectDef> CMockSketchDoc::GetObjectDef()
{
    using namespace VIEW_COMMON;
    std::weak_ptr<CObjectDef> pRet;

    if (m_eDocType == E_DOC_DRAWING)
    {
        pRet = m_pExecDef;

    }
    else
    {
        pRet = THIS_APP->GetObjectDefByUuid(m_uuidObjectDef);
    }
    return pRet;
}

/**
 * @brief    スクリプト取得
 * @param    なし
 * @retval   スクリプトへのポインタ
 * @note	 
 */
CScriptData* CMockSketchDoc::GetScript()
{
    return m_pScriptData;
}

/**
 * @brief    スクリプト取得
 * @param    なし
 * @retval   スクリプトへのポインタ
 * @note	 
 */
/*
CDrawingScriptBase* CMockSketchDoc::GetDrawingScriptBase()
{
    return m_pScriptBase;
}
*/

/**
 * @brief    定義オブジェクト設定
 * @param    [in] pObjDef 定義オブジェクトへのポインタ
 * @retval   なし
 * @note     Document生成時には設定せず、後から設定する
 *           pObjDefの大元は、MainFrameが持っている
 */
void CMockSketchDoc::SetObjectDef(boost::uuids::uuid uuidObjDef, CScriptData* pScript)
{
    using namespace VIEW_COMMON;
    if (pScript == NULL)
    {
        m_eDocType = E_DOC_DEF;
    }
    else
    {
        m_eDocType = E_DOC_SCRIPT;  //エディタで開く
    }

    m_uuidObjectDef = uuidObjDef;
    m_pScriptData  = pScript;
    //m_pScriptBase  = NULL;
    UpdateName();
}

/**
 * @brief    実行インスタンス設定
 * @param    [in] pScriptBase 実行インスタンスへのポインタ
 * @retval   なし
 * @note   
 */
void CMockSketchDoc::SetExecDef(std::weak_ptr<CObjectDef> pExecDef)
{
    using namespace VIEW_COMMON;
    m_eDocType = E_DOC_DRAWING;

    //m_pScriptBase = pScriptBase;
    m_uuidObjectDef = boost::uuids::nil_uuid();
    m_pScriptData  = NULL;
    m_pExecDef  = pExecDef;

    UpdateName();
}



/**
 * @brief    名称更新
 * @param    なし
 * @retval   なし
 * @note     
 */
void CMockSketchDoc::UpdateName()
{
    StdString strTabName;
    if (m_pScriptData == NULL)
    {
        if (!m_pExecDef.expired())
        {
            std::shared_ptr<CObjectDef> pDef;
            pDef = m_pExecDef.lock();
            if (pDef)
            {
                auto pBase = pDef->GetViewObject();
                strTabName = THIS_APP->GetScriptBaseName(pBase.get());
            }
        }
        else
        {

            strTabName = THIS_APP->GetObjectDefTabName(m_uuidObjectDef);
        }
    }
    else
    {
        //スクリプト
        if (m_pScriptData->IsChange())
        {
            strTabName = _T("*");
        }

        strTabName += THIS_APP->GetObjectDefTabName(m_uuidObjectDef,
                                                m_pScriptData->GetName());
    }

    SetTitle(strTabName.c_str());
}


/**
 * @brief   ファイル保存
 * @param   [in] lpszPathName ファイル名
 * @return  TRUE 成功
 * @note    ルートオブジェクトのみ保存可能
 */
BOOL CMockSketchDoc::OnSaveDocument(LPCTSTR lpszPathName)
{
    //使わない
    return FALSE;
}

/**
 * @brief   オブジェクト内部状態チェック
 * @param   
 * @return  なし
 * @note     
 */
BOOL CMockSketchDoc::OnOpenDocument(LPCTSTR lpszPathName)
{
    //使わない
    return TRUE;
}


// CMockSketchDoc 診断

#ifdef _DEBUG
/**
 * @brief   オブジェクト内部状態チェック
 * @param   なし
 * @return  なし
 * @note     
 */
void CMockSketchDoc::AssertValid() const
{
	CDocument::AssertValid();
}

/**
 * @brief   ダンプ処理
 * @param   [in]    dc	ダンプ用の診断ダンプ コンテキスト 
 * @return  なし
 * @note    
 */
void CMockSketchDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG





void CMockSketchDoc::OnFileClose()
{
	if (!SaveModified())
		return;

	// shut it down
	OnCloseDocument();
		// this should destroy the document
}

void CMockSketchDoc::OnFileSave()
{
	DoFileSave();
}

void CMockSketchDoc::OnFileSaveAs()
{
	if(!DoSave(NULL))
    {
		TRACE(traceAppMsg, 0, "Warning: File save-as failed.\n");
    }
}

/**
 *  @brief  ファイルの保存
 *  @param  なし
 *  @retval なし
 *  @note   
 */
BOOL CMockSketchDoc::DoFileSave()
{
    //	DWORD dwAttrib = GetFileAttributes(m_strPathName);
    StdString strFileName;
    using namespace VIEW_COMMON;
    if(m_eDocType & E_DOC_SCRIPT)
    {
        STD_ASSERT(m_pScriptData);
        if (m_pScriptData)
        {
            //strFileName = m_pScriptData->GetName();
            //strFileName += _T(".as");

            StdPath  path = m_pScriptData->GetCurrentPath(); // strFileName;
            if(m_pScriptData->SaveScript(path))
            {
                UpdateName();
                return TRUE;
            }
            return FALSE;
        }
    }
    else if (m_eDocType & E_DOC_DEF)
    {
        //!< オブジェクト
        std::shared_ptr<CObjectDef> pDef;
        namespace fs = boost::filesystem;
        pDef = GetObjectDef().lock();
        if (!pDef)
        {
            return FALSE;
        }
        
        CProjectCtrl* pProj;
        pProj = pDef->GetProject();

        if (!pProj)
        {
            return FALSE;
        }

        StdPath  pathDir;
        try 
        {
            StdPath  path(pProj->GetDir());
            pathDir = path /  pDef->GetName();
            if (!fs::exists(pathDir))
            {
                fs::create_directories(pathDir);
            }

			auto eType = pProj->GetFileType();
			if (eType == CProjectCtrl::E_BIN)
			{
				pathDir /= (pDef->GetName() + _T(".msd"));
			}
			else if (eType == CProjectCtrl::E_XML)
			{
				pathDir /= (pDef->GetName() + _T(".msdx"));
			}
			else if (eType == CProjectCtrl::E_TXT)
			{
				pathDir /= (pDef->GetName() + _T(".msdt"));
			}

			pDef->Save(pathDir);
            return TRUE;
        }
        catch (fs::filesystem_error& ex) 
        {
            StdStringStream strmError;
            strmError << StdFormat(_T("Save error %s:%s")) %
                     pathDir.c_str() % CUtil::CharToString(ex.what());
            STD_DBG( strmError.str().c_str());
            throw MockException(e_file_write, strmError.str().c_str());
        }
    }
    return FALSE;
}

BOOL CMockSketchDoc::DoSave(LPCTSTR lpszPathName, BOOL bReplace)
	// Save the document data to a file
	// lpszPathName = path name where to save document file
	// if lpszPathName is NULL then the user will be prompted (SaveAs)
	// note: lpszPathName can be different than 'm_strPathName'
	// if 'bReplace' is TRUE will change file name if successful (SaveAs)
	// if 'bReplace' is FALSE will not change path name (SaveCopyAs)
{
#if 0
	CString newName = lpszPathName;
	if (newName.IsEmpty())
	{
		CDocTemplate* pTemplate = GetDocTemplate();
		ASSERT(pTemplate != NULL);

		newName = m_strPathName;
		if (bReplace && newName.IsEmpty())
		{
			newName = m_strTitle;
			// check for dubious filename
			int iBad = newName.FindOneOf(_T(":/\\"));
			if (iBad != -1)
				newName.ReleaseBuffer(iBad);

			// append the default suffix if there is one
			CString strExt;
			if (pTemplate->GetDocString(strExt, CDocTemplate::filterExt) &&
			  !strExt.IsEmpty())
			{
				ASSERT(strExt[0] == '.');
				int iStart = 0;
				newName += strExt.Tokenize(_T(";"), iStart);
			}
		}

		if (!AfxGetApp()->DoPromptFileName(newName,
		  bReplace ? AFX_IDS_SAVEFILE : AFX_IDS_SAVEFILECOPY,
		  OFN_HIDEREADONLY | OFN_PATHMUSTEXIST, FALSE, pTemplate))
			return FALSE;       // don't even attempt to save
	}

	CWaitCursor wait;

	if (!OnSaveDocument(newName))
	{
		if (lpszPathName == NULL)
		{
			// be sure to delete the file
			TRY
			{
				CFile::Remove(newName);
			}
			CATCH_ALL(e)
			{
				DB_PRINT(traceAppMsg, 0, "Warning: failed to delete file after failed SaveAs.\n");
				//DELETE_EXCEPTION(e);
			}
			END_CATCH_ALL
		}
		return FALSE;
	}

	// reset the title and change the document name
	if (bReplace)
		SetPathName(newName);
#endif
	return TRUE;        // success
}

BOOL CMockSketchDoc::SaveModified()
{
#if 0
	if (!IsModified())
    {
		return TRUE;        // ok to continue
    }

	// get name/title of document
	CString name;
	if (m_strPathName.IsEmpty())
	{
		// get name based on caption
		name = m_strTitle;
		if (name.IsEmpty())
			ENSURE(name.LoadString(AFX_IDS_UNTITLED));
	}
	else
	{
		// get name based on file title of path name
		name = m_strPathName;
        //
		//AfxGetFileTitle(m_strPathName, name.GetBuffer(_MAX_PATH), _MAX_PATH);
		name.ReleaseBuffer();
	}

	CString prompt;
	AfxFormatString1(prompt, AFX_IDP_ASK_TO_SAVE, name);
	switch (AfxMessageBox(prompt, MB_YESNOCANCEL, AFX_IDP_ASK_TO_SAVE))
	{
	case IDCANCEL:
		return FALSE;       // don't continue

	case IDYES:
		// If so, either Save or Update, as appropriate
		if (!DoFileSave())
			return FALSE;       // don't continue
		break;

	case IDNO:
		// If not saving changes, revert the document
		break;

	default:
		ASSERT(FALSE);
		break;
	}
#endif
	return TRUE;    // keep going
}

//!< フレームウインドウ破棄前呼び出し
void CMockSketchDoc::PreCloseFrame(CFrameWnd* pFrame)
{
   // DB_PRINT(_T("ウインドウを閉じます\n"));
}


// CMockSketchDoc コマンド
