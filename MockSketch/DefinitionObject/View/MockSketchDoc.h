/**
 * @brief			MockSketchDocヘッダーファイル
 * @file			MockSketchDoc.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *	CMockSketchDoc クラスのインターフェイス
 *  すべてのデータの大元は CMainFrame::m_pMainPartsDefにある
 *  Documentクラスはそれを参照するのみ
 * $
 * $
 * 
 */

#pragma once

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "View/ViewCommon.h"

/*---------------------------------------------------*/
/*  class                                            */
/*---------------------------------------------------*/
class CObjectDef;
class CScriptData;
class CDrawingScriptBase;


/**
 * @class   CMockSketchDoc
 * @brief                        
 */
class CMockSketchDoc : public CDocument
{
protected:
     
    //!< 表示オブジェクト
    boost::uuids::uuid        m_uuidObjectDef;

    //!< 実行オブジェクト
    std::weak_ptr<CObjectDef> m_pExecDef;

    //!< スクリプト
    CScriptData*            m_pScriptData;

    //!< ドキュメント種別
    VIEW_COMMON::E_DOCUMENT_TYPE     m_eDocType;

protected: // シリアル化からのみ作成します。
    CMockSketchDoc();
    DECLARE_DYNCREATE(CMockSketchDoc)

    // 属性
public:

    // 操作
public:

    // オーバーライド
public:
    //!< Document生成通知
    virtual BOOL OnNewDocument();

    virtual void Serialize(CArchive& ar);

    //!< タイトル変更通知記号除去
    static CString SplitTitleMarker(LPCTSTR strTitle);

    //!< フレームウインドウ破棄前呼び出し
    virtual void PreCloseFrame(CFrameWnd* pFrame);

    //!< 描画オブジェクト設定
    void SetObjectDef(boost::uuids::uuid uuidObjDef, CScriptData*   pScript = NULL);

    //!< 実行インスタンス設定
    void SetExecDef(std::weak_ptr<CObjectDef> pExecDef);

    //!< 名称更新
    void UpdateName();

    //!< ドキュメント種別
    VIEW_COMMON::E_DOCUMENT_TYPE GetDocType() const{ return m_eDocType;}


    //!< オブジェクト
    std::weak_ptr<CObjectDef> GetObjectDef();

    //!< オブジェクト
    boost::uuids::uuid GetPartsId(){return m_uuidObjectDef;}

    //!< スクリプト取得
    CScriptData* GetScript();

    //!< DrawingScriptBase取得
    //CDrawingScriptBase* GetDrawingScriptBase();

    //!< 保存
    virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);

    //!< 読み込み
    virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);

    //-------------------
    //  オーバーライド
    //-------------------
    BOOL SaveModified();
    
    void OnFileClose();

    void OnFileSave();

    void OnFileSaveAs();

    BOOL DoFileSave();

    BOOL DoSave(LPCTSTR lpszPathName, BOOL bReplace = TRUE);
    //-------------------

    //===============================


// 実装
public:
	virtual ~CMockSketchDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 生成された、メッセージ割り当て関数
protected:
	DECLARE_MESSAGE_MAP()
};


