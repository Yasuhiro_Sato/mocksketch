﻿
/**
 * @brief			CMockSketchView実装ファイル
 * @file			CMockSketchView.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MockSketch.h"
#include "MainFrm.h"

#include "DefinitionObject/View/MockSketchDoc.h"

#include "./MockSketchView.h"

#include "View/NumInputDlgbar.h"
#include "View/CLayer.h"
#include "View/CUndoAction.h"

#include "DefinitionObject/View/CDrawingView.h"
#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/CFieldDef.h"
#include "DefinitionObject/CReferenceDef.h"
#include "DrawingObject/CDrawingParts.h"
#include "DrawingObject/CDrawingText.h"
#include "DrawingObject/CDrawingScriptBase.h"
#include "DrawingObject/CDrawingGroup.h"
#include "DrawingObject/CDrawingCompositionLine.h"
#include "System/CSystem.h"
#include "Utility/CUtility.h"
#include "Utility/ExtText/TEXT_TYPE.H"
#include "DrawingObject\Primitive/SPLINE2D.H"
#include "ToolBar/CToolBarFontSizeCombo.h"
#include "ToolBar/CLineStyleMenuItem.h"
#include "ToolBar/CLineWidthMenuItem.h"

#include "SubWindow/ImageCtrlDlg.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


//とりあえずここに
#define WPD_CHARFORMAT CHARFORMAT2
#define WPD_PARAFORMAT PARAFORMAT2
#define DEBUG_RICH_EDIT 0

struct CCharFormat : public WPD_CHARFORMAT
{
	CCharFormat() {cbSize = sizeof(WPD_CHARFORMAT);}
	BOOL operator==(CCharFormat& cf);
};


// CMockSketchView

IMPLEMENT_DYNCREATE(CMockSketchView, CView)

BEGIN_MESSAGE_MAP(CMockSketchView, CView)
	// 標準印刷コマンド
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CMockSketchView::OnFilePrintPreview)
    ON_WM_SIZE()
    ON_WM_LBUTTONUP()
    ON_WM_RBUTTONUP()
    ON_WM_LBUTTONDOWN()
    ON_WM_RBUTTONDOWN()
    ON_WM_CREATE()

    ON_WM_MOUSEMOVE()
    ON_WM_LBUTTONDBLCLK()
    ON_WM_MOUSEWHEEL()


    ON_COMMAND(ID_MNU_POINT     , &CMockSketchView::OnMnuPoint    )
    ON_COMMAND(ID_MNU_LINE      , &CMockSketchView::OnMnuLine     )
    ON_COMMAND(ID_MNU_CIRCLE    , &CMockSketchView::OnMnuCircle   )
    ON_COMMAND(ID_MNU_ARC       , &CMockSketchView::OnMnuArc      )
    ON_COMMAND(ID_MNU_NODE      , &CMockSketchView::OnMnuNode     )
    ON_COMMAND(ID_MNU_IMAGE     , &CMockSketchView::OnMnuImage    )

    ON_COMMAND(ID_MNU_OFFSET    , &CMockSketchView::OnMnuOffset   )
    ON_COMMAND(ID_MNU_CORNER    , &CMockSketchView::OnMnuCorner   )
    ON_COMMAND(ID_MNU_CHAMFER   , &CMockSketchView::OnMnuChamfer  )
    ON_COMMAND(ID_MNU_TRIM      , &CMockSketchView::OnMnuTrim     )
    ON_COMMAND(ID_MNU_MOVE      , &CMockSketchView::OnMnuMove     )
    ON_COMMAND(ID_MNU_STRETCH_MODE, &CMockSketchView::OnMnuStretchMode)
    ON_COMMAND(ID_MNU_COPY      , &CMockSketchView::OnMnuCopy     )
    ON_COMMAND(ID_MNU_ROTATE    , &CMockSketchView::OnMnuRotate   )
    ON_COMMAND(ID_MNU_SCL       , &CMockSketchView::OnMnuScl      )
    ON_COMMAND(ID_MNU_MIRROR    , &CMockSketchView::OnMnuMirror   )
    ON_COMMAND(ID_MNU_SELECT    , &CMockSketchView::OnMnuSelect   ) 
    ON_COMMAND(ID_MNU_CUT_MODE  , &CMockSketchView::OnMnuCutMode  ) 
    ON_COMMAND(ID_MNU_COPY_MODE , &CMockSketchView::OnMnuCopyMode ) 

    ON_COMMAND(ID_MNU_DELETE    , &CMockSketchView::OnMnuDelete   )
    ON_COMMAND(ID_MNU_DEVIDE    , &CMockSketchView::OnMnuDevide   )

    ON_COMMAND(ID_MNU_3P_CIRCLE    , &CMockSketchView::OnMnu3PCircle )
    ON_COMMAND(ID_MNU_ELLIPSE      , &CMockSketchView::OnMnuEllipse  )
    ON_COMMAND(ID_MNU_SPLINE       , &CMockSketchView::OnMnuSpline   )
    ON_COMMAND(ID_MNU_COMPOSITION_LINE   , &CMockSketchView::OnMnuCompositionLine)
    ON_COMMAND(ID_MNU_CONNECTION_LINE   , &CMockSketchView::OnMnuConnectionLine)
    ON_COMMAND(ID_MNU_FILL         , &CMockSketchView::OnMnuFill     )
    ON_COMMAND(ID_MNU_GROUP        , &CMockSketchView::OnMnuGroup    )
    ON_COMMAND(ID_MNU_PARTS        , &CMockSketchView::OnMnuParts    )
    ON_COMMAND(ID_MNU_REFERENCE    , &CMockSketchView::OnMnuReference)

    ON_COMMAND(ID_MNU_DISASSEMBLY  , &CMockSketchView::OnMnuDisassembly )
    ON_COMMAND(ID_MNU_DRAW_TEXT    , &CMockSketchView::OnMnuDrawText )
    ON_COMMAND(ID_MNU_CONVERT_SPLINE, &CMockSketchView::OnMnuConvertSpline )

    

    ON_COMMAND(ID_MNU_CANCEL       , &CMockSketchView::OnMnuCancel   )
    ON_COMMAND(ID_MNU_END          , &CMockSketchView::OnMnuEnd      )

    ON_COMMAND(ID_MNU_IMAGE_CTRL   , &CMockSketchView::OnMnuImageCtrl )
    

    ON_UPDATE_COMMAND_UI(ID_MNU_POINT   , &CMockSketchView::OnUpdateMnuPoint    )
    ON_UPDATE_COMMAND_UI(ID_MNU_LINE    , &CMockSketchView::OnUpdateMnuLine     )
    ON_UPDATE_COMMAND_UI(ID_MNU_CIRCLE  , &CMockSketchView::OnUpdateMnuCircle   )
    ON_UPDATE_COMMAND_UI(ID_MNU_ARC     , &CMockSketchView::OnUpdateMnuArc      )
    ON_UPDATE_COMMAND_UI(ID_MNU_NODE    , &CMockSketchView::OnUpdateMnuNode     )
    ON_UPDATE_COMMAND_UI(ID_MNU_IMAGE   , &CMockSketchView::OnUpdateMnuImage    )
    ON_UPDATE_COMMAND_UI(ID_MNU_OFFSET  , &CMockSketchView::OnUpdateMnuOffset   )
    ON_UPDATE_COMMAND_UI(ID_MNU_CORNER  , &CMockSketchView::OnUpdateMnuCorner   )
    ON_UPDATE_COMMAND_UI(ID_MNU_CHAMFER , &CMockSketchView::OnUpdateMnuChamfer  )
    ON_UPDATE_COMMAND_UI(ID_MNU_TRIM    , &CMockSketchView::OnUpdateMnuTrim     )
    ON_UPDATE_COMMAND_UI(ID_MNU_MOVE    , &CMockSketchView::OnUpdateMnuMove     )
    ON_UPDATE_COMMAND_UI(ID_MNU_STRETCH_MODE    , &CMockSketchView::OnUpdateMnuStretchMode )

    ON_UPDATE_COMMAND_UI(ID_MNU_COPY    , &CMockSketchView::OnUpdateMnuCopy     )
    ON_UPDATE_COMMAND_UI(ID_MNU_ROTATE  , &CMockSketchView::OnUpdateMnuRotate   )
    ON_UPDATE_COMMAND_UI(ID_MNU_SELECT  , &CMockSketchView::OnUpdateMnuSelect   ) 
    ON_UPDATE_COMMAND_UI(ID_MNU_CUT_MODE  , &CMockSketchView::OnUpdateMnuCutMode  ) 
    ON_UPDATE_COMMAND_UI(ID_MNU_COPY_MODE , &CMockSketchView::OnUpdateMnuCopyMode ) 


    ON_UPDATE_COMMAND_UI(ID_MNU_DELETE  , &CMockSketchView::OnUpdateMnuDelete   )
    ON_UPDATE_COMMAND_UI(ID_MNU_DEVIDE  , &CMockSketchView::OnUpdateMnuDevide   )

    ON_UPDATE_COMMAND_UI(ID_MNU_3P_CIRCLE    , &CMockSketchView::OnUpdateMnu3PCircle )
    ON_UPDATE_COMMAND_UI(ID_MNU_ELLIPSE      , &CMockSketchView::OnUpdateMnuEllipse  )
    ON_UPDATE_COMMAND_UI(ID_MNU_SPLINE       , &CMockSketchView::OnUpdateMnuSpline   )
    ON_UPDATE_COMMAND_UI(ID_MNU_COMPOSITION_LINE   , &CMockSketchView::OnUpdateMnuCompositionLine)
    ON_UPDATE_COMMAND_UI(ID_MNU_CONNECTION_LINE    , &CMockSketchView::OnUpdateMnuConnectionLine)

    ON_UPDATE_COMMAND_UI(ID_MNU_FILL         , &CMockSketchView::OnUpdateMnuFill     )

    ON_UPDATE_COMMAND_UI(ID_MNU_GROUP        , &CMockSketchView::OnUpdateMnuGroup    )
    ON_UPDATE_COMMAND_UI(ID_MNU_PARTS        , &CMockSketchView::OnUpdateMnuParts    )
    ON_UPDATE_COMMAND_UI(ID_MNU_REFERENCE     , &CMockSketchView::OnUpdateMnuReference    )
    ON_UPDATE_COMMAND_UI(ID_MNU_DISASSEMBLY  , &CMockSketchView::OnUpdateMnuDisassembly   )
    ON_UPDATE_COMMAND_UI(ID_MNU_DRAW_TEXT    , &CMockSketchView::OnUpdateMnuDrawText )
    ON_UPDATE_COMMAND_UI(ID_MNU_CONVERT_SPLINE, &CMockSketchView::OnUpdateMnuConvertSpline )


    ON_UPDATE_COMMAND_UI(ID_MNU_IMAGE_CTRL    , &CMockSketchView::OnUpdateMnuImageCtrl )

    /*
    ON_COMMAND(ID_MNU_L_DIM        , &CMockSketchView::OnMnuDim      )
    ON_COMMAND(ID_MNU_H_DIM        , &CMockSketchView::OnMnuHDim     )
    ON_COMMAND(ID_MNU_V_DIM        , &CMockSketchView::OnMnuVDim     )
    ON_COMMAND(ID_MNU_D_DIM        , &CMockSketchView::OnMnuDDim     )
    ON_COMMAND(ID_MNU_A_DIM        , &CMockSketchView::OnMnuADim     )
    ON_COMMAND(ID_MNU_R_DIM        , &CMockSketchView::OnMnuRDim     )
    ON_COMMAND(ID_MNU_C_DIM        , &CMockSketchView::OnMnuCDim     )

    ON_UPDATE_COMMAND_UI(ID_MNU_L_DIM        , &CMockSketchView::OnUpdateMnuDim      )
    ON_UPDATE_COMMAND_UI(ID_MNU_H_DIM        , &CMockSketchView::OnUpdateMnuHDim     )
    ON_UPDATE_COMMAND_UI(ID_MNU_V_DIM        , &CMockSketchView::OnUpdateMnuVDim     )
    ON_UPDATE_COMMAND_UI(ID_MNU_D_DIM        , &CMockSketchView::OnUpdateMnuDDim     )
    ON_UPDATE_COMMAND_UI(ID_MNU_A_DIM        , &CMockSketchView::OnUpdateMnuADim     )
    ON_UPDATE_COMMAND_UI(ID_MNU_R_DIM        , &CMockSketchView::OnUpdateMnuRDim     )
    ON_UPDATE_COMMAND_UI(ID_MNU_C_DIM        , &CMockSketchView::OnUpdateMnuCDim     )
    */



    //フォーマットバー
	ON_COMMAND(ID_CHAR_COLOR, OnCharColor)
	
    ON_COMMAND(IDC_FONTNAME, OnFontname)
	ON_COMMAND(IDC_FONTSIZE, OnFontsize)
	ON_CBN_SELENDOK(IDC_FONTNAME, OnFontname)
	ON_CBN_SELENDOK(IDC_FONTSIZE, OnFontsize)


	ON_COMMAND(ID_CHAR_BOLD, OnCharBold)
	ON_COMMAND(ID_CHAR_ITALIC, OnCharItalic)
	ON_COMMAND(ID_CHAR_UNDERLINE, OnCharUnderline)
	ON_COMMAND(ID_CHAR_STRIKE_OUT, OnCharStrikeOut)
	ON_COMMAND(ID_CHAR_UPPER, OnCharUpper)
	ON_COMMAND(ID_CHAR_LOWER, OnCharLower)
	ON_COMMAND_RANGE(ID_CHAR_DATUM_TL, ID_CHAR_DATUM_BR, OnCharDatum)
    ON_COMMAND_RANGE(ID_CHAR_ALIGN_L, ID_CHAR_ALIGN_R, OnCharAlign)

	ON_UPDATE_COMMAND_UI(ID_CHAR_BOLD, OnUpdateCharBold)
	ON_UPDATE_COMMAND_UI(ID_CHAR_ITALIC, OnUpdateCharItalic)
	ON_UPDATE_COMMAND_UI(ID_CHAR_UNDERLINE, OnUpdateCharUnderline)
	ON_UPDATE_COMMAND_UI(ID_CHAR_STRIKE_OUT, OnUpdateCharStrikeOut)
	ON_UPDATE_COMMAND_UI(ID_CHAR_UPPER, OnUpdateCharUpper)
	ON_UPDATE_COMMAND_UI(ID_CHAR_LOWER, OnUpdateCharLower)
	ON_UPDATE_COMMAND_UI_RANGE(ID_CHAR_DATUM_TL, ID_CHAR_DATUM_BR, OnUpdateCharDatum)
    ON_UPDATE_COMMAND_UI_RANGE(ID_CHAR_ALIGN_L, ID_CHAR_ALIGN_R, OnUpdateCharAlign)

    //描画バー
	ON_COMMAND(ID_MNU_LINE_COLOR,  OnLineColor)
	ON_COMMAND(ID_MNU_BRUSH_COLOR, OnBrushColor)

	ON_COMMAND(ID_MNU_LINE_STYLE,  OnLineStyleSet)
	ON_COMMAND(ID_MNU_LINE_WIDTH,  OnLineWidthSet)

	ON_COMMAND_RANGE(ID_LINE_SOLID, ID_LINE_DASHDOTDOT, OnLineStyle)
	ON_UPDATE_COMMAND_UI_RANGE(ID_LINE_SOLID, ID_LINE_DASHDOTDOT, OnUpdateLineStyle)

	ON_COMMAND_RANGE(ID_LINE_WIDTH_1, ID_LINE_WIDTH_10, OnLineWidth)
	ON_UPDATE_COMMAND_UI_RANGE(ID_LINE_WIDTH_1, ID_LINE_WIDTH_10, OnUpdateLineWidth)

	ON_COMMAND_RANGE(ID_MNU_L_DIM, ID_MNU_C_DIM, OnDimType)
	ON_UPDATE_COMMAND_UI_RANGE(ID_MNU_L_DIM, ID_MNU_C_DIM, OnUpdateDimType)


	//ON_COMMAND(ID_MORE_LINES, OnMoreLines)

    //リッチエディット
    ON_NOTIFY(EN_SELCHANGE, IDC_RICH_CTRL, &CMockSketchView::OnEnSelchangeRichedit)

    ON_MESSAGE(WM_UPDATE_CURRENT_LAYER_NUM, OnLayerChange)

    ON_MESSAGE(WM_VAL_INPUT, OnValInput)

    ON_MESSAGE(WM_VIEW_UPDATE_NAME, OnUpdateName)

    ON_MESSAGE(WM_VIEW_CLEAR_ALL, OnViewClear)

    //IOダイアログ
    /*
    ON_MESSAGE(WM_DRAW_IO_DLG, OnDrawIoDialog)
    
    ON_MESSAGE(WM_MOVE_IO_DLG, OnMoveIoDialog)

    ON_MESSAGE(WM_SIZE_IO_DLG, OnSizeIoDialog)
    */

    //ドロップ処理
    ON_MESSAGE(WM_DROP_ENTER,  OnDropEnter)
    ON_MESSAGE(WM_DROP_END,    OnDropEnd)
    ON_MESSAGE(WM_DROP_OVER,   OnDropOver)
    ON_MESSAGE(WM_DROP_LEAVE,  OnDropLeave)


    //!< IOダイアログ表示
	//ON_COMMAND(ID_MNU_DSP_IO_DLG,  OnDspIoDlg)
    //ON_UPDATE_COMMAND_UI(ID_MNU_DSP_IO_DLG        , &CMockSketchView::OnUpdateDspIoDlg)

    //順序
    ON_COMMAND_RANGE(ID_MNU_DSP_ORDER_TOPMOST, ID_MNU_DSP_ORDER_BACK, OnMnuOrder)
  	ON_UPDATE_COMMAND_UI_RANGE(ID_MNU_DSP_ORDER_TOPMOST, ID_MNU_DSP_ORDER_BACK, OnUpdateMnuOrder)

    //スナップ
    ON_COMMAND_RANGE(ID_MNU_SNAP_NONE, ID_MNU_SNAP_NODE, OnMnuSnap)
  	ON_UPDATE_COMMAND_UI_RANGE(ID_MNU_SNAP_NONE, ID_MNU_SNAP_NODE, OnUpdateMnuSnap)

    //表示位置
    ON_COMMAND_RANGE(ID_MNU_SHOW_POS_01, ID_MNU_SHOW_POS_09, OnMnuShowPos)
  	ON_UPDATE_COMMAND_UI_RANGE(ID_MNU_SHOW_POS_01, ID_MNU_SHOW_POS_09, OnUpdateMnuShowPos)

    //表示位置設定
    ON_COMMAND(ID_MNU_SHOW_POS_SET, &CMockSketchView::OnMnuShowPosSet)
  	ON_UPDATE_COMMAND_UI(ID_MNU_SHOW_POS_SET, &CMockSketchView::OnUpdateMnuShowPosSet)

    //表示位置コンボボックス
    ON_COMMAND(ID_SHOW_POS, OnShowPos)
	ON_CBN_SELENDOK(ID_SHOW_POS, OnShowPos)


    ON_WM_CLOSE()
    ON_WM_DESTROY()
    ON_COMMAND(ID_MNU_REDRAW, &CMockSketchView::OnMnuRedraw)
    ON_UPDATE_COMMAND_UI(ID_MNU_REDRAW, &CMockSketchView::OnUpdateMnuRedraw)

    ON_COMMAND(ID_MNU_VIEW_ALL, &CMockSketchView::OnMnuViewAll)
    ON_UPDATE_COMMAND_UI(ID_MNU_REDRAW, &CMockSketchView::OnUpdateMnuViewAll)


    ON_WM_MBUTTONUP()
    ON_COMMAND(ID_EDIT_UNDO, &CMockSketchView::OnEditUndo)
    ON_COMMAND(ID_EDIT_REDO, &CMockSketchView::OnEditRedo)
    ON_UPDATE_COMMAND_UI(ID_EDIT_REDO, &CMockSketchView::OnUpdateEditRedo)
    ON_UPDATE_COMMAND_UI(ID_EDIT_UNDO, &CMockSketchView::OnUpdateEditUndo)

    ON_MESSAGE(WM_MODE_CHANGE, OnModeChange)

    ON_MESSAGE(WM_UPDATE_PRINTER, OnUpdatePrinter)
    
    ON_WM_MENUSELECT()
	ON_WM_SETCURSOR()
    ON_WM_KEYUP()


   //ON_EN_CHANGE(IDC_RICH_CTRL, &CMockSketchView::OnEnChangeRichedit)
   ON_EN_UPDATE(IDC_RICH_CTRL, &CMockSketchView::OnEnChangeRichedit)


    //IME関連
    ON_MESSAGE(WM_IME_CHAR,  &CMockSketchView::OnImeChar)
    ON_MESSAGE(WM_IME_COMPOSITION,  &CMockSketchView::OnImeComposition)


    END_MESSAGE_MAP()

/**
 * コンストラクタ
 * @return		
 * @note	シリアル化からのみ作成します。 
 * @exception   なし
 */

CMockSketchView::CMockSketchView():
CView(),
CObjectDefView()
{
    THIS_APP->AddView(this);
    SetWindow(this);

    m_pView = std::make_shared<CDrawingView>();

    m_pDlgbarNumInput = std::make_shared<CNumInputDlgbar>();

    m_bInit = false;
    m_hDc = 0;
    m_bDblClick = false;
    m_bPopTooltip = false;
    m_iViewPosition = 0;
    m_bViewSet      = false;
}

//----------------------
// デストラクター
//----------------------
CMockSketchView::~CMockSketchView()
{
    if (m_hDc)
    {
        ::ReleaseDC(m_hWnd, m_hDc);
    }
}

/**
 * @
 * @param [in]    cs	 	  
 * @return		
 * @note	このビューを描画するためにオーバーライドされます。 
 * @exception   なし
 */
BOOL CMockSketchView::PreCreateWindow(CREATESTRUCT& cs)
{

	return CView::PreCreateWindow(cs);
}

//-------------------------
// 初期化処理
//-------------------------
void CMockSketchView::Init()
{
    //文字編集用のリッチエディットを作成する
    RECT rect;
    memset(&rect, 0, sizeof(rect));

    DWORD dwStyle	= WS_CHILD | WS_VISIBLE |WS_BORDER | ES_MULTILINE | ES_WANTRETURN;
    DWORD dwExStyle = ES_EX_ZOOMABLE;



    m_Rich.CreateEx(dwExStyle, dwStyle,
        rect,                  
        this,                          //コントロールの親ペインへのポインター。
        IDC_RICH_CTRL                  //コントロールの子ウィンドウ ID
    );                  
    m_Rich.ShowWindow(SW_HIDE);


    CString strFontName;
    strFontName = DRAW_CONFIG->GetDefaultFont();

    CMFCToolBarFontComboBox* pFontCombo = 
    (CMFCToolBarFontComboBox*) CMFCToolBarComboBoxButton::GetByCmd (IDC_FONTNAME);

    if(pFontCombo)
    {
        pFontCombo->SetFont( strFontName );
    }

	CToolBarFontSizeCombo* pSizeCombo =
           (CToolBarFontSizeCombo*)CMFCToolBarComboBoxButton::GetByCmd (IDC_FONTSIZE);
	if (pSizeCombo != NULL)
	{
		pSizeCombo->RebuildFontSizes (strFontName);
		pSizeCombo->SetTwipSize (DRAW_CONFIG->GetFontSize() * 20);
	}

    //--------------------------
    //ドロップターゲットの登録
    //--------------------------
    m_DropTarget.Register(this);
    m_DropTarget.SetDropWnd(this);
    //m_DropTarget.SetTreeWnd(&m_tree);

    //--------------------------

    //OnEnSelchangeRichedit を呼ぶために イベントマスクの設定を変更する
    //m_Rich.SetEventMask(ENM_SELCHANGE|ENM_UPDATE|ENM_CHANGE);
    //m_Rich.SetEventMask(ENM_SELCHANGE|ENM_UPDATE|ENM_CHANGE);

    DWORD dwEventMask;
    dwEventMask = m_Rich.GetEventMask();
    dwEventMask |= ENM_CHANGE;
    m_Rich.SetEventMask(dwEventMask);

    ::SendMessage(m_Rich.m_hWnd, EM_SETTARGETDEVICE, 0, 1);  //折り返しなし
    
    // ウインドウ生成を通知
    // 
    ::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, 
        WM_VIEW_CREATE, 
        (WPARAM)VIEW_COMMON::E_PARTS, 
        (LPARAM)m_pView->GetPartsDef());

}

bool CMockSketchView::UpdateView()
{
    if (m_pView)
    {
        CPartsDef* pDef = m_pView->GetPartsDef();
        if (pDef)
        {
            pDef->Redraw(m_pView.get(), false);
            return true;
        }
    }
    return false;
}

//-------------------------
// CMockSketchView 描画
//-------------------------
void CMockSketchView::OnDraw(CDC* pDC)
{
    if (m_pView)
    {
        CPartsDef* pDef = m_pView->GetPartsDef();
        if (pDef)
        {
            if (m_pView->IsPrintMode())
            {
                m_pView->SetDc(pDC);
            }
            pDef->Redraw(m_pView.get());
        }

        EnableScrollBarCtrl(SB_HORZ, FALSE);
        ShowScrollBar(SB_HORZ, FALSE);
    }
}


// CMockSketchView 印刷


//---------------------------------------
// CViewTestView 印刷
//---------------------------------------
void CMockSketchView::OnFilePrintPreview()
{
	AFXPrintPreview(this);
}

//---------------------------------------
// 印刷準備
//---------------------------------------
BOOL CMockSketchView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 既定の印刷準備
    try
    {
        BOOL bRet;
        bRet = DoPreparePrinting(pInfo);
        return bRet;
    }
    catch(...)
    {
        STD_DBG(_T("DoPreparePrinting Error")); 
        return false;
    }

}

//---------------------------------------
// 印刷開始
//---------------------------------------
void CMockSketchView::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	// TODO: 印刷前の特別な初期化処理を追加してください。
    m_pView->BeginPrint(pDC, pInfo);

    OnDraw(pDC);
}

//---------------------------------------
// 印刷終了
//---------------------------------------
void CMockSketchView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
    m_pView->EndPrinting();
	// TODO: 印刷後の後処理を追加してください。
}

void CMockSketchView::OnEndPrintPreview(CDC* pDC, CPrintInfo* pInfo, POINT point, CPreviewView* pView)
{
    // TODO: ここに特定なコードを追加するか、もしくは基底クラスを呼び出してください。
    m_pView->EndPrinting();
    __super::OnEndPrintPreview(pDC, pInfo, point, pView);
}


//---------------------------------------
// マウス移動
//---------------------------------------
void CMockSketchView::OnMouseMove(UINT nFlags, CPoint point)
{

    if ((nFlags != nOldMouseFlags) ||
        (point != ptOldMoucePos))
    {
        CView::OnMouseMove(nFlags, point);
        m_pView->OnMouseMove(nFlags, point);

        ptOldMoucePos = point;
        nOldMouseFlags = nFlags;
    }
}

//---------------------------------------
// 左マウスボタン押下
//---------------------------------------
void CMockSketchView::OnLButtonDown(UINT nFlags, CPoint point)
{
    CView::OnLButtonDown(nFlags, point);
    m_pView->OnLButtonDown(nFlags, point);
        SetFocus();
}

/**
 * マウス左ボタン解放
 * @param   [in]    nFlags   仮想キー         
 * @param   [in]    point    マウス座標         
 * @retval  なし
 * @note	
 */
void CMockSketchView::OnLButtonUp(UINT nFlags, CPoint point)
{
    if (m_bDblClick)
    {
        m_bDblClick = false;
        return;
    }
    CView::OnLButtonUp(nFlags, point);
    m_pView->OnLButtonUp(nFlags, point);
}

/**
 * マウス左ボタン解放
 * @param   [in]    nFlags   仮想キー         
 * @param   [in]    point    マウス座標         
 * @retval  なし
 * @note	
 */
void CMockSketchView::OnRButtonDown(UINT nFlags, CPoint point)
{
    if (!m_pView->OnRButtonDown(nFlags, point))
    {
        CView::OnRButtonDown(nFlags, point);
    }
}

/**
 * マウス右ボタン解放
 * @param   [in]    nFlags   仮想キー         
 * @param   [in]    point    マウス座標         
 * @retval  なし
 * @note	
 */
void CMockSketchView::OnRButtonUp(UINT nFlags, CPoint point)
{
    bool bRet;
    bRet = m_pView->OnRButtonUp(nFlags, point);
    if (!bRet)
    {
        ClientToScreen(&point);
	    OnContextMenu(this, point);
    }
}

/**
*  @brief コンテキストメニュー選択
*  @param [in] pWnd   マウスの右ボタンがクリックされたウィンドウのハンドル
*  @param [in] point  クリックされたときの、カーソルの画面座標位置 
*  @retval           なし
*  @note
*/
void CMockSketchView::OnContextMenu(CWnd* pWnd, CPoint point)
{
    //選択状態
    if (!m_pView->OnContextMenu(point))
    {
	    theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_VIEW, point.x, point.y, this, TRUE);
    }
}


/**
 * マウス中央ボタン解放
 * @param   [in]    nFlags   仮想キー         
 * @param   [in]    point    マウス座標         
 * @retval  なし
 * @note	
 */
void CMockSketchView::OnMButtonUp(UINT nFlags, CPoint point)
{
    m_pView->SetCenter(point);
}

/**
 *  @brief ウインドウサイズ変更
 *  @param [in] nType   要求されるサイズ変更のタイプ  (SIZE_MAXIMIZED....)
 *  @param [in] cx      クライアント領域の新しい幅 
 *  @param [in] cy      クライアント領域の新しい高さ
 *  @retval           なし
 *  @note
 */
void CMockSketchView::OnSize(UINT nType, int cx, int cy)
{
    CView::OnSize(nType, cx, cy);
    //========================

    if (!m_bInit)
    {
        CMockSketchDoc* pDoc = GetDocument();
        ASSERT_VALID(pDoc);

        /*
        CDrawingScriptBase* pScriptBase = NULL;
        pScriptBase = pDoc->GetDrawingScriptBase();

        std::shared_ptr<CObjectDef> pDef;
        pDef = pDoc->GetObjectDef().lock();
        */



        //---------------------
        std::shared_ptr<CObjectDef> pDef;
        pDef = pDoc->GetObjectDef().lock();

        CPartsDef* pParts = NULL;
        CFieldDef* pField = NULL;
        CReferenceDef* pRef = NULL;
        if (pDef)
        {
            if (pDef->GetObjectType() == VIEW_COMMON::E_PARTS)
            {
                pParts = dynamic_cast<CPartsDef*>(pDef.get());
                pParts->AddView(m_pView.get());
                m_bInit = true;

            }
            else if(pDef->GetObjectType() == VIEW_COMMON::E_FIELD)
            {
                pField = dynamic_cast<CFieldDef*>(pDef.get());
                pField->AddView(m_pView.get());
                m_bInit = true;
            }
            else if(pDef->GetObjectType() == VIEW_COMMON::E_REFERENCE)
            {
                pRef = dynamic_cast<CReferenceDef*>(pDef.get());
                pRef->AddView(m_pView.get());
                m_bInit = true;
            }
        }

        if (m_bInit)
        {
            m_pView->SetDocument(pDoc);
            SetView();
            m_pView->SetWindow( this );
            m_hDc = ::GetDC(m_hWnd);
            m_pView->SetDc (GetDC());
            m_pDlgbarNumInput->SetParent(m_hWnd);
            Init();
        }

    }
    //スクロールバーはなし
    ShowScrollBar(SB_BOTH, FALSE);

    m_pView->OnSize( cx, cy);
    AdjustLayout();
    //========================
}

/**
 *  @brief 左ボタンダブルクリック
 *  @param [in] nFlags   仮想キー
 *  @param [in] point    クリック位置         
 *  @retval           なし
 *  @note
 */
void CMockSketchView::OnLButtonDblClk(UINT nFlags, CPoint point)
{
    m_bDblClick = true;

    m_pView->OnLButtonDblClick( nFlags, point);
    CView::OnLButtonDblClk(nFlags, point);
}

/**
 *  @brief マウスホイール
 *  @param [in] nFlags   仮想キー
 *  @param [in] zDelta   回転距離 
 *  @param [in] point    マウス位置         
 *  @retval           なし
 *  @note
 */
BOOL CMockSketchView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
    int nCnt = abs(zDelta / 120);
    ScreenToClient(&pt);
    CPartsDef* pDef = m_pView->GetPartsDef();
    if (pDef)
    {
        if (zDelta < 0)
        {
            m_pView->ZoomDown(nCnt, pt);
            pDef->Redraw(m_pView.get());
        }
        else
        {
            m_pView->ZoomUp(nCnt, pt);
            pDef->Redraw(m_pView.get());
        }
    }
    return CView::OnMouseWheel(nFlags, zDelta, pt);
}

// CMockSketchView 診断

#ifdef _DEBUG
/**
 * @brief   オブジェクト内部状態チェック
 * @param   なし
 * @return  なし
 * @note     
 */
void CMockSketchView::AssertValid() const
{
	CView::AssertValid();
}

/**
 * @brief   ダンプ処理
 * @param   [in]    dc	ダンプ用の診断ダンプ コンテキスト 
 * @return  なし
 * @note    
 */
void CMockSketchView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMockSketchDoc* CMockSketchView::GetDocument() const // デバッグ以外のバージョンはインラインです。
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMockSketchDoc)));
	return (CMockSketchDoc*)m_pDocument;
}
#endif //_DEBUG


// CMockSketchView メッセージ ハンドラ
void CMockSketchView::OnMnuPoint    (){   SetViewMode(VIEW_POINT );}
void CMockSketchView::OnMnuLine     (){   SetViewMode(VIEW_LINE  );}
void CMockSketchView::OnMnuCircle   (){   SetViewMode(VIEW_CIRCLE);}
void CMockSketchView::OnMnuArc      (){   SetViewMode(VIEW_ARC   );}
void CMockSketchView::OnMnuNode     (){   SetViewMode(VIEW_NODE  );}
void CMockSketchView::OnMnuImage    (){   SetViewMode(VIEW_IMAGE );}
void CMockSketchView::OnMnuOffset   (){   SetViewMode(VIEW_OFFSET);}
void CMockSketchView::OnMnuCorner   (){   SetViewMode(VIEW_CORNER);}
void CMockSketchView::OnMnuChamfer  (){   SetViewMode(VIEW_CHAMFER);}
void CMockSketchView::OnMnuTrim     (){   SetViewMode(VIEW_TRIM  );}
void CMockSketchView::OnMnuMove     (){   SetViewMode(VIEW_MOVE  );}
void CMockSketchView::OnMnuStretchMode ()
{
    if (m_pView)
    {
        CPartsDef* pDef = m_pView->GetPartsDef();
        if (pDef)
        {
            pDef->RelSelect();
            pDef->Redraw();
        }
    }
    bool bStretch = !DRAW_CONFIG->GetStretchMode();
    DRAW_CONFIG->SetStretchMode(bStretch);
}

void CMockSketchView::OnMnuCopy     (){   SetViewMode(VIEW_COPY  );}
void CMockSketchView::OnMnuRotate   (){   SetViewMode(VIEW_ROTATE);}
void CMockSketchView::OnMnuScl      (){   SetViewMode(VIEW_SCL   );}
void CMockSketchView::OnMnuMirror   (){   SetViewMode(VIEW_MIRROR);}

void CMockSketchView::OnMnuSelect   (){   SetViewMode(VIEW_SELECT);}
void CMockSketchView::OnMnuDelete   ()
{   
    if (!m_Rich.IsWindowVisible())
    {
        SetViewMode(VIEW_DELETE);
    }
}
void CMockSketchView::OnMnuDevide   (){   SetViewMode(VIEW_BREAK );}

void CMockSketchView::OnMnuCutMode  (){   m_pView->SetCutMode(true);}
void CMockSketchView::OnMnuCopyMode (){   m_pView->SetCutMode(false);}


void CMockSketchView::OnMnu3PCircle (){   SetViewMode(VIEW_CIRCLE_3P);}
void CMockSketchView::OnMnuEllipse  (){   SetViewMode(VIEW_ELLIPSE  );}
void CMockSketchView::OnMnuSpline   (){   SetViewMode(VIEW_SPLINE   );}
void CMockSketchView::OnMnuCompositionLine(){   SetViewMode(VIEW_COMPOSITION_LINE);}
void CMockSketchView::OnMnuConnectionLine(){   SetViewMode(VIEW_CONNECTION_LINE);}
void CMockSketchView::OnMnuFill     (){   SetViewMode(VIEW_FILL     );}

void CMockSketchView::OnMnuGroup        (){   SetViewMode(VIEW_GROUP    );}
void CMockSketchView::OnMnuParts        (){   SetViewMode(VIEW_PARTS    );}
void CMockSketchView::OnMnuReference    (){   SetViewMode(VIEW_REFERENCE);}
void CMockSketchView::OnMnuDisassembly  (){   SetViewMode(VIEW_DISASSEMBLY);}
void CMockSketchView::OnMnuDrawText     (){   SetViewMode(VIEW_TEXT     );}

void CMockSketchView::OnMnuConvertSpline()
{
    bool bEnable = false;
    CPartsDef* pDef = NULL;
    if (!m_pView)
    {
        return;
    }
    pDef = m_pView->GetPartsDef();
    if (!pDef)
    {
        return;
    }

    if (pDef->SelectNum() != 1)
    {
        return;
    }

    auto pObj = pDef->GetSelect(0);
    if (!pObj)
    {
        return;
    }

    if (pObj->GetType() != DT_TEXT)
    {
        return;
    }

    auto pTxt = std::dynamic_pointer_cast<CDrawingText>(pObj);
    auto Group = std::vector<std::shared_ptr<CDrawingCompositionLine>>();

    pTxt->Convert(m_pView.get(), &Group);

    if(CUndoAction* pUndo  = pDef->GetUndoAction())
    {
        for(auto pCompo: Group)
        {
            pCompo->SetPartsDef(pDef);
            pCompo->SetLayer(m_pView->GetCurrentLayerId());
            pDef->RegisterObject(pCompo, true, false);
            pUndo->Add(UD_ADD, pDef, NULL, pCompo, false);
        }
        pUndo->Push();


        int iAns = AfxMessageBox(GET_STR(STR_DLG_DELETE_ORGIN_TEXT), MB_YESNO);
        if (iAns == IDYES)
        {
            pUndo->Add(UD_DEL, pDef, pTxt, NULL, true);
            pDef->DeleteSelectingObject();
            pDef->ClearMouseOver();
            pDef->ClearMouseEmphasis();
            pDef->RelSelect();
            pDef->Redraw();
        }
    }
}

bool CMockSketchView::SetViewMode    (VIEW_MODE eMode)
{
    //描画停止
    return m_pView->SetViewMode(eMode);
}

VIEW_MODE CMockSketchView::GetViewMode()
{
    //描画停止
    if (m_pView)
    {
        return m_pView->GetViewMode();
    }
    return VIEW_NONE;
}

void CMockSketchView::SetDlgBarMode    (VIEW_MODE eMode)
{
    m_pDlgbarNumInput->SetMode(eMode);
}


void CMockSketchView::OnUpdateMnuPoint    (CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck (m_pView->GetViewMode() == VIEW_POINT);
}

void CMockSketchView::OnUpdateMnuLine     (CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck (m_pView->GetViewMode() == VIEW_LINE);
}

void CMockSketchView::OnUpdateMnuCircle   (CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck (m_pView->GetViewMode() == VIEW_CIRCLE);
}

void CMockSketchView::OnUpdateMnuArc      (CCmdUI *pCmdUI)
{
}

void CMockSketchView::OnUpdateMnuNode     (CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck (m_pView->GetViewMode() == VIEW_NODE);
}

void CMockSketchView::OnUpdateMnuImage     (CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck (m_pView->GetViewMode() == VIEW_IMAGE);
}

void CMockSketchView::OnUpdateMnuOffset   (CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck (m_pView->GetViewMode() == VIEW_OFFSET);
}

void CMockSketchView::OnUpdateMnuCorner   (CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck (m_pView->GetViewMode() == VIEW_CORNER);
}

void CMockSketchView::OnUpdateMnuChamfer   (CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck (m_pView->GetViewMode() == VIEW_CHAMFER);
}

void CMockSketchView::OnUpdateMnuTrim     (CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck (m_pView->GetViewMode() == VIEW_TRIM);
}

void CMockSketchView::OnUpdateMnuMove     (CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck (m_pView->GetViewMode() == VIEW_MOVE);
}

void CMockSketchView::OnUpdateMnuStretchMode     (CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck (DRAW_CONFIG->GetStretchMode());
}

void CMockSketchView::OnUpdateMnuCopy     (CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck (m_pView->GetViewMode() == VIEW_COPY);
}

void CMockSketchView::OnUpdateMnuScl     (CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck (m_pView->GetViewMode() == VIEW_SCL);
}

void CMockSketchView::OnUpdateMnuMirror   (CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck (m_pView->GetViewMode() == VIEW_MIRROR);
}

void CMockSketchView::OnUpdateMnuRotate   (CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck (m_pView->GetViewMode() == VIEW_ROTATE);
}

void CMockSketchView::OnUpdateMnuSelect   (CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck (m_pView->GetViewMode() == VIEW_SELECT);
}

void CMockSketchView::OnUpdateMnuDelete   (CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck (m_pView->GetViewMode() == VIEW_DELETE);
}

void CMockSketchView::OnUpdateMnuDevide   (CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck (m_pView->GetViewMode() == VIEW_BREAK);
}

void CMockSketchView::OnUpdateMnuCutMode   (CCmdUI *pCmdUI)
{
    pCmdUI->SetRadio (m_pView->GetCutMode());
}

void CMockSketchView::OnUpdateMnuCopyMode   (CCmdUI *pCmdUI)
{
    pCmdUI->SetRadio (!m_pView->GetCutMode());
}




void CMockSketchView::OnUpdateMnu3PCircle (CCmdUI *pCmdUI){ pCmdUI->SetCheck (m_pView->GetViewMode() == VIEW_CIRCLE_3P);}
void CMockSketchView::OnUpdateMnuEllipse  (CCmdUI *pCmdUI){ pCmdUI->SetCheck (m_pView->GetViewMode() == VIEW_ELLIPSE  );}
void CMockSketchView::OnUpdateMnuSpline   (CCmdUI *pCmdUI){ pCmdUI->SetCheck (m_pView->GetViewMode() == VIEW_SPLINE   );}
void CMockSketchView::OnUpdateMnuCompositionLine(CCmdUI *pCmdUI){ pCmdUI->SetCheck (m_pView->GetViewMode() == VIEW_COMPOSITION_LINE     );}
void CMockSketchView::OnUpdateMnuConnectionLine(CCmdUI *pCmdUI){ pCmdUI->SetCheck (m_pView->GetViewMode() == VIEW_CONNECTION_LINE     );}
void CMockSketchView::OnUpdateMnuFill     (CCmdUI *pCmdUI){ pCmdUI->SetCheck (m_pView->GetViewMode() == VIEW_FILL     );}
void CMockSketchView::OnUpdateMnuDrawText   (CCmdUI *pCmdUI){ pCmdUI->SetCheck (m_pView->GetViewMode() == VIEW_TEXT     );}

bool CMockSketchView::_IsSelect() const
{
    CPartsDef* pDef = NULL;
    if (!m_pView)
    {
        return false;
    }


    pDef = m_pView->GetPartsDef();

    if (!pDef)
    {
        return false;
    }

    if (pDef->SelectNum() == 0)
    {
        return false;
    }

    return true;
}

void CMockSketchView::OnUpdateMnuGroup      (CCmdUI *pCmdUI)
{ 
    pCmdUI->Enable(_IsSelect());
    pCmdUI->SetCheck (m_pView->GetViewMode() == VIEW_GROUP    );
}

void CMockSketchView::OnUpdateMnuParts      (CCmdUI *pCmdUI)
{ 
    pCmdUI->Enable(_IsSelect());
    pCmdUI->SetCheck (m_pView->GetViewMode() == VIEW_PARTS    );
}

void CMockSketchView::OnUpdateMnuReference  (CCmdUI *pCmdUI)
{ 
    pCmdUI->Enable(_IsSelect());
    pCmdUI->SetCheck (m_pView->GetViewMode() == VIEW_REFERENCE    );
}

void CMockSketchView::OnUpdateMnuDisassembly(CCmdUI *pCmdUI)
{
    pCmdUI->Enable(_IsSelect());
    pCmdUI->SetCheck (m_pView->GetViewMode() == VIEW_DISASSEMBLY);
}

void CMockSketchView::OnUpdateMnuConvertSpline(CCmdUI *pCmdUI)
{
    bool bEnable = false;
    CPartsDef* pDef = NULL;
    if (m_pView)
    {
        pDef = m_pView->GetPartsDef();
        if (pDef)
        {
            if (pDef->SelectNum() == 1)
            {
                if (pDef->GetSelect(0)->GetType() == DT_TEXT)
                {
                    bEnable = true;
                }
            }
        }
    }
    pCmdUI->Enable(bEnable);
}


/*
void CMockSketchView::OnUpdateMnuDim      (CCmdUI *pCmdUI){ pCmdUI->SetCheck (m_pView->GetViewMode() == VIEW_DIM      );}
void CMockSketchView::OnUpdateMnuHDim     (CCmdUI *pCmdUI){ pCmdUI->SetCheck (m_pView->GetViewMode() == VIEW_H_DIM    );}
void CMockSketchView::OnUpdateMnuVDim     (CCmdUI *pCmdUI){ pCmdUI->SetCheck (m_pView->GetViewMode() == VIEW_V_DIM    );}
void CMockSketchView::OnUpdateMnuDDim     (CCmdUI *pCmdUI){ pCmdUI->SetCheck (m_pView->GetViewMode() == VIEW_D_DIM    );}
void CMockSketchView::OnUpdateMnuADim     (CCmdUI *pCmdUI){ pCmdUI->SetCheck (m_pView->GetViewMode() == VIEW_A_DIM    );}
void CMockSketchView::OnUpdateMnuRDim     (CCmdUI *pCmdUI){ pCmdUI->SetCheck (m_pView->GetViewMode() == VIEW_R_DIM    );}
void CMockSketchView::OnUpdateMnuCDim     (CCmdUI *pCmdUI){ pCmdUI->SetCheck (m_pView->GetViewMode() == VIEW_C_DIM    );}
*/

/**
 * @brief   文字色設定
 * @param   なし
 * @return	なし
 * @note	 
 */
void CMockSketchView::OnCharColor()
{
	COLORREF color = CMFCColorMenuButton::GetColorByCmdID (ID_CHAR_COLOR);
	if (color == -1)
	{
		color = DRAW_CONFIG->GetDefautTextColor();
    }
 
    if (m_Rich.IsWindowVisible())
    {
        long nStart;
        long nEnd;
        m_Rich.GetSel(nStart, nEnd);

        CHARFORMAT cf;
        cf.dwMask = CFM_COLOR ;
        cf.crTextColor = color; 
        cf.dwEffects   = 0;

        if (nStart != nEnd)
        {
            m_Rich.SetSelectionCharFormat (cf);
        }
        else
        {
            m_Rich.SetDefaultCharFormat   (cf);

        }
        m_Rich.SetFocus ();
    }
    else
    {
        DRAW_CONFIG->crText = color;
    }
}

/**
 * @brief   フォント名変更
 * @param   なし
 * @return	なし
 * @note	 
 */
void CMockSketchView::OnFontname()
{
	USES_CONVERSION;

	CMFCToolBarFontComboBox* pSrcCombo = 
		(CMFCToolBarFontComboBox*) CMFCToolBarComboBoxButton::GetByCmd (IDC_FONTNAME);
	if (pSrcCombo == NULL)
	{
		return;
	}

    CCharFormat cf;
    cf.szFaceName[0] = NULL;
    cf.dwMask = CFM_FACE | CFM_CHARSET;
    cf.cbSize = sizeof(cf);

    const CMFCFontInfo* pDesc = pSrcCombo->GetFontDesc();
    ASSERT_VALID (pDesc);
    ASSERT(pDesc->m_strName.GetLength() < LF_FACESIZE);

    if (lstrcpyn(cf.szFaceName, pDesc->m_strName, LF_FACESIZE) == NULL)
    {
        STD_DBG(_T("OnFontname lstrcpyn fail"));
        return;
    }

    cf.bCharSet = pDesc->m_nCharSet;
    cf.bPitchAndFamily = pDesc->m_nPitchAndFamily;

    CToolBarFontSizeCombo* pSizeCombo =
        (CToolBarFontSizeCombo*)CMFCToolBarComboBoxButton::GetByCmd (IDC_FONTSIZE);
    if (pSizeCombo != NULL)
    {
        int nSize = pSizeCombo->GetTwipSize();
        pSizeCombo->RebuildFontSizes (pDesc->m_strName);
        pSizeCombo->SetTwipSize (nSize);
    }

    if (m_Rich.IsWindowVisible())
    {
        long nStart;
        long nEnd;
        m_Rich.GetSel(nStart, nEnd);

        if (nStart == nEnd)
        {
            m_Rich.SetDefaultCharFormat   (cf);
        }
        else
        {
            m_Rich.SetSelectionCharFormat (cf);

        }
        m_Rich.SetFocus ();
    }
    else
    {
        DRAW_CONFIG->strFont = pDesc->m_strName;
    }
}

/**
 * @brief   フォントサイズ変更
 * @param   なし
 * @return	なし
 * @note	 
 */
void CMockSketchView::OnFontsize()
{
	CToolBarFontSizeCombo* pSrcCombo = 
		(CToolBarFontSizeCombo*) CToolBarFontSizeCombo::GetByCmd (IDC_FONTSIZE);
	if (pSrcCombo == NULL)
	{
		return;
	}

	int nSize = pSrcCombo->GetTwipSize();
	if (nSize == -2)
	{
		AfxMessageBox(GET_SYS_STR(STR_ERROR_INVALID_NUMBER).c_str(), 
                                               MB_OK|MB_ICONINFORMATION);
        return;
    }
	else if ((nSize >= 0 && nSize < 20) || nSize > 32760)
	{
		AfxMessageBox(GET_SYS_STR(STR_ERROR_INVALID_FONTSIZE).c_str(), 
                                              MB_OK|MB_ICONINFORMATION);
        return;
	}

    if (m_Rich.IsWindowVisible())
    {
        CCharFormat cf;
        cf.dwMask = CFM_SIZE;
        cf.yHeight = nSize;

        long nStart;
        long nEnd;
        m_Rich.GetSel(nStart, nEnd);
        if (nStart == nEnd)
        {
            m_Rich.SetDefaultCharFormat   (cf);
        }
        else
        {
            m_Rich.SetSelectionCharFormat (cf);
        }
        m_Rich.SetFocus ();
    }
    else
    {
        DRAW_CONFIG->iFontSize = nSize;
    }
}

/**
 * @brief   太字設定
 * @param   なし
 * @return	なし
 * @note	 
 */
void CMockSketchView::OnCharBold()
{
   OnFomatToolBar(CFM_BOLD, CFE_BOLD);
}

/**
 *  @brief  太字設定ボタン表示更新.
 *  @param  [in] pCmdUI  コマンドUI
 *  @retval なし
 *  @note
 */
void CMockSketchView::OnUpdateCharBold(CCmdUI* pCmdUI)
{
    OnUpdateFomatToolBar(pCmdUI, CFM_BOLD, CFE_BOLD);
}

/**
 *  @brief  斜体設定.
 *  @param  なし
 *  @retval なし
 *  @note
 */
void CMockSketchView::OnCharItalic()
{
   OnFomatToolBar(CFM_ITALIC, CFE_ITALIC);
}

/**
 *  @brief  斜体設定ボタン表示更新.
 *  @param  [in] pCmdUI  コマンドUI
 *  @retval なし
 *  @note
 */
void CMockSketchView::OnUpdateCharItalic(CCmdUI* pCmdUI)
{
    OnUpdateFomatToolBar(pCmdUI, CFM_ITALIC, CFE_ITALIC);
}

/**
 *  @brief  下線設定.
 *  @param  なし
 *  @retval なし
 *  @note
 */
void CMockSketchView::OnCharUnderline()
{
    OnFomatToolBar(CFM_UNDERLINE, CFE_UNDERLINE);
}

/**
 *  @brief  下線設定ボタン表示更新.
 *  @param  [in] pCmdUI  コマンドUI
 *  @retval なし
 *  @note
 */
void CMockSketchView::OnUpdateCharUnderline(CCmdUI* pCmdUI)
{
    OnUpdateFomatToolBar(pCmdUI, CFM_UNDERLINE, CFE_UNDERLINE);
}

/**
 *  @brief  取消線線設定.
 *  @param  なし
 *  @retval なし
 *  @note
 */
void CMockSketchView::OnCharStrikeOut()
{
    OnFomatToolBar(CFM_STRIKEOUT, CFE_STRIKEOUT);
}

/**
 *  @brief  取消線設定ボタン表示更新.
 *  @param  [in] pCmdUI  コマンドUI
 *  @retval なし
 *  @note
 */
void CMockSketchView::OnUpdateCharStrikeOut(CCmdUI* pCmdUI)
{
    OnUpdateFomatToolBar(pCmdUI, CFM_STRIKEOUT, CFE_STRIKEOUT);
}

/**
 *  @brief  下付文字設定.
 *  @param  なし
 *  @retval なし
 *  @note
 */
void CMockSketchView::OnCharLower()
{
    OnFomatToolBar(CFM_SUBSCRIPT, CFE_SUBSCRIPT);
}

/**
 *  @brief  下付文字設定ボタン表示更新.
 *  @param  [in] pCmdUI  コマンドUI
 *  @retval なし
 *  @note
 */
void CMockSketchView::OnUpdateCharLower(CCmdUI* pCmdUI)
{
    OnUpdateFomatToolBar(pCmdUI, CFM_SUBSCRIPT, CFE_SUBSCRIPT);
}

/**
 *  @brief  上付文字設定.
 *  @param  なし
 *  @retval なし
 *  @note
 */
void CMockSketchView::OnCharUpper()
{
    OnFomatToolBar(CFM_SUPERSCRIPT, CFE_SUPERSCRIPT);
}

/**
 *  @brief  上付文字ボタン表示更新.
 *  @param  [in] pCmdUI  コマンドUI
 *  @retval なし
 *  @note
 */
void CMockSketchView::OnUpdateCharUpper(CCmdUI* pCmdUI)
{
    OnUpdateFomatToolBar(pCmdUI, CFM_SUPERSCRIPT, CFE_SUPERSCRIPT);
}


/**
 *  @brief  文字位置設定
 *  @param  [in] id  選択ID
 *  @retval なし
 *  @note
 */
afx_msg void CMockSketchView::OnCharAlign (UINT iId)
{
    auto eType = CExtText::ConvIdToTextAlign(iId);
    auto pText = _GetSelectedText();
    if (pText)
    {
        auto pExtTxt = pText->GetTextInstance();
        pExtTxt->SetAlign(eType);
        CPartsDef* pDef = m_pView->GetPartsDef();
        UpdateView();
    }
    m_iTextAlign = iId;
}      

/**
*  @brief  テキストアライメント取得
*  @param  なし
*  @retval テキストアライメントID(UI)
*  @note
*/
int CMockSketchView::GetTextAlign()
{
    return m_iTextAlign;
}
/**
*  @brief  文字位置設定
*  @param  [in] id  選択ID
*  @retval なし
*  @note
*/
afx_msg void CMockSketchView::OnCharDatum (UINT iId)
{
    auto eType = CExtText::ConvIdToDatum(iId);
    auto pText = _GetSelectedText();
    if (pText)
    {
        auto pExtTxt = pText->GetTextInstance();
        pExtTxt->SetDatum(eType);
        UpdateView();
    }
    m_iTextDatum = iId;
}

int CMockSketchView::GetTextDatum()
{
    //基本的には文字を選択しないとDatumは設定できないが
    //Richeditを開いている最中にToolBarでDatumを変更した場合
    //一時的に選択したDatumの値を保存する必要がある

    return m_iTextDatum;
}


CMFCToolBarComboBoxButton* CMockSketchView::GetToolBarComboBox(UINT uiCmd) const
{
    CMFCToolBarComboBoxButton* pSrcCombo = NULL;
    CObList listButtons;
    if(CMFCToolBar::GetCommandButtons(uiCmd, listButtons) > 0)
    {
        for(POSITION posCombo = listButtons.GetHeadPosition();
            pSrcCombo == NULL && posCombo != NULL;)
        {
            CMFCToolBarComboBoxButton* pCombo =
                DYNAMIC_DOWNCAST(CMFCToolBarComboBoxButton, listButtons.GetNext(posCombo));
            if(pCombo != NULL)
            {
                pSrcCombo = pCombo;
            }
        }
    }
    return pSrcCombo;
}


CMFCToolBarButton* CMockSketchView::GetToolBarButton(UINT uiCmd) const
{
    CMFCToolBarButton* pSrc = NULL;

    CObList listButtons;
    if(CMFCToolBar::GetCommandButtons(uiCmd, listButtons) > 0)
    {
        for(POSITION pos = listButtons.GetHeadPosition();
            pSrc == NULL && pos != NULL;)
        {
            CMFCToolBarButton* pCombo =
                DYNAMIC_DOWNCAST(CMFCToolBarButton, listButtons.GetNext(pos));
            if(pCombo != NULL)
            {
                pSrc = pCombo;
            }
        }
    }
    return pSrc;
}

std::shared_ptr<CDrawingText> CMockSketchView::_GetSelectedText()
{
    std::shared_ptr<CDrawingText> pRet;
    auto pDef = m_pView->GetPartsDef();
    if (!pDef)
    {
        return pRet;
    }

    long lSize = pDef->SelectNum();
    if( lSize != 1)
    {
        return pRet;
    }

    auto pObj = pDef->GetSelect(0);
    if (!pObj)
    {
        return pRet;
    }

    if (pObj->GetType() != DT_TEXT)
    {
        return pRet;
    }

    pRet = std::dynamic_pointer_cast<CDrawingText>(pObj);

    return  pRet;

}

/**
 *  @brief  文字位置ボタン表示更新.
 *  @param  [in] pCmdUI  コマンドUI
 *  @retval なし
 *  @note
 */
afx_msg void CMockSketchView::OnUpdateCharAlign(CCmdUI* pCmdUI)
{
    auto pText = _GetSelectedText();
    if (!pText)
    {
        pCmdUI->Enable(false);
        return;
    }

    if (!m_Rich.IsWindowVisible())
    {
        if (pText->GetTextInstance()->IsEnableFrame())
        {
            pCmdUI->Enable(true);

            ALIGNMENT_TYPE eType = pText->GetTextInstance()->GetAlign();
            int iId = CExtText::ConvTextAlignToId(eType);
            pCmdUI->SetCheck(pCmdUI->m_nID == iId);
        }
    }
}


/**
*  @brief  文字位置ボタン表示更新.
*  @param  [in] pCmdUI  コマンドUI
*  @retval なし
*  @note
*/
afx_msg void CMockSketchView::OnUpdateCharDatum(CCmdUI* pCmdUI)
{
    auto pText = _GetSelectedText();
    if (!pText)
    {
        pCmdUI->Enable(false);
        return;
    }

    pCmdUI->Enable(true);

    if (!m_Rich.IsWindowVisible())
    {
        POS_TYPE eType = pText->GetTextInstance()->GetDatum();
        int iId = CExtText::ConvDatumToId(eType);
        pCmdUI->SetCheck (pCmdUI->m_nID == iId);
    }
}

/**
 *  @brief  フォーマットバーボタン処理.
 *  @param  [in] iMask    マスク
 *  @param  [in] iEffect  処理 
 *  @retval なし
 *  @note
 */
void CMockSketchView::OnFomatToolBar(int iMask, int iEffect)
{
	CHARFORMAT2 cf;
    cf.cbSize = sizeof(CHARFORMAT2);
	cf.dwMask = iMask;  
    m_Rich.GetSelectionCharFormat(cf);
    cf.cbSize = sizeof(CHARFORMAT2);
	cf.dwMask = iMask;  

    cf.dwEffects = ~(cf.dwEffects & iEffect);
    m_Rich.SetSelectionCharFormat(cf);
    m_Rich.SetFocus();
}

/**
 *  @brief  フォーマットバーボタン更新処理.
 *  @param  [in] pCmdUI 
 *  @param  [in] iMask    マスク
 *  @param  [in] iEffect  処理 
 *  @retval なし
 *  @note
 */
void CMockSketchView::OnUpdateFomatToolBar(CCmdUI* pCmdUI, 
                                           int iMask, int Effect)
{
    if (m_Rich)
    {
        if(m_Rich.IsWindowVisible())
        {
            pCmdUI->Enable(true);

            CHARFORMAT2 cf;
            memset(&cf, 0, sizeof(cf));
            cf.cbSize = sizeof(CHARFORMAT2);
            cf.dwMask = iMask;

            m_Rich.GetSelectionCharFormat(cf);
            pCmdUI->SetCheck (cf.dwEffects & Effect);
            return;
        }
    }

    auto pText = _GetSelectedText();
    if (pText)
    {
        CHARFORMAT2 cf;
        memset(&cf, 0, sizeof(cf));
        cf.cbSize = sizeof(CHARFORMAT2);
        cf.dwMask = iMask;



        pCmdUI->Enable(false);
        return;
    }




}

/**
 *  @brief  線色変更.
 *  @param  なし
 *  @retval なし
 *  @note
 */
void CMockSketchView::OnLineColor()
{
	COLORREF color = CMFCColorMenuButton::GetColorByCmdID (ID_MNU_LINE_COLOR);
    m_pView->GetCurrentLayer()->crDefault  = color;
}

/**
 *  @brief  塗りつぶし色変更.
 *  @param  なし
 *  @retval なし
 *  @note
 */
void CMockSketchView::OnBrushColor()
{
	COLORREF color = CMFCColorMenuButton::GetColorByCmdID (ID_MNU_BRUSH_COLOR);
    m_pView->GetCurrentLayer()->crBrush = color;
}

afx_msg void CMockSketchView::OnLineStyleSet ()
{
    //ボタンを置き換えているため機能しないが Enable状態にするため必要
}

afx_msg void CMockSketchView::OnLineWidthSet ()
{
    //ボタンを置き換えているため機能しないが Enable状態にするため必要
}

/**
 *  @brief  線種変更.
 *  @param  なし
 *  @retval なし
 *  @note
 */
void CMockSketchView::OnLineStyle (UINT id)
{
    CMFCToolBarComboBoxButton*  pCb = GetToolBarComboBox(ID_MNU_LINE_STYLE);
    auto pLc = dynamic_cast<CToolBarLineComboBox*>(pCb);
    if(pLc)
    {
        pLc->SetLineStyle(id);
    }

    int iLineType = CLineStyleMenuItem::GetIdToLineStyle (id);
    m_pView->GetCurrentLayer()->iLineType = iLineType; 
}

void CMockSketchView::OnUpdateLineStyle (CCmdUI* pCmdUI)
{
    int iLineType = m_pView->GetCurrentLayer()->iLineType;
    UINT iId = CLineStyleMenuItem::GetLineStyleToId (iLineType);
	pCmdUI->SetCheck (pCmdUI->m_nID == iId);
}

/**
 *  @brief  線幅変更.
 *  @param  なし
 *  @retval なし
 *  @note
 */
void CMockSketchView::OnLineWidth (UINT id)
{
    int iLineWidth = id - ID_LINE_WIDTH_1 + 1;

    CMFCToolBarComboBoxButton*  pCb = GetToolBarComboBox(ID_MNU_LINE_WIDTH);
    auto pLc = dynamic_cast<CToolBarLineWidthComboBox*>(pCb);
    if(pLc)
    {
        pLc->SetWidth(iLineWidth);
    }
    m_pView->GetCurrentLayer()->iLineWidth  = iLineWidth;

}

void CMockSketchView::OnUpdateLineWidth (CCmdUI* pCmdUI)
{
    int iLineWidthId = m_pView->GetCurrentLayer()->iLineWidth + ID_LINE_WIDTH_1 -1;

	pCmdUI->SetCheck (pCmdUI->m_nID == iLineWidthId);
}

/**
 *  @brief  線種変更.
 *  @param  なし
 *  @retval なし
 *  @note
 */
void CMockSketchView::OnDimType (UINT id)
{
    VIEW_MODE eViewMode;

    switch(id)
    {
    case ID_MNU_L_DIM      :{eViewMode = VIEW_L_DIM; break;}
    case ID_MNU_H_DIM      :{eViewMode = VIEW_H_DIM; break;}
    case ID_MNU_V_DIM      :{eViewMode = VIEW_V_DIM; break;}
    case ID_MNU_D_DIM      :{eViewMode = VIEW_D_DIM; break;}
    case ID_MNU_A_DIM      :{eViewMode = VIEW_A_DIM; break;}
    case ID_MNU_R_DIM      :{eViewMode = VIEW_R_DIM; break;}
    case ID_MNU_C_DIM      :{eViewMode = VIEW_C_DIM; break;}
    default:{eViewMode = VIEW_L_DIM;}
    }

    SetViewMode(eViewMode);

}

void CMockSketchView::OnUpdateDimType (CCmdUI* pCmdUI)
{
    VIEW_MODE eViewMode;
    eViewMode = m_pView->GetViewMode();

    int id;
    switch(eViewMode)
    {
    case VIEW_L_DIM      :{id = ID_MNU_L_DIM; break;}
    case VIEW_H_DIM      :{id = ID_MNU_H_DIM; break;}
    case VIEW_V_DIM      :{id = ID_MNU_V_DIM; break;}
    case VIEW_D_DIM      :{id = ID_MNU_D_DIM; break;}
    case VIEW_A_DIM      :{id = ID_MNU_A_DIM; break;}
    case VIEW_R_DIM      :{id = ID_MNU_R_DIM; break;}
    case VIEW_C_DIM      :{id = ID_MNU_C_DIM; break;}
    default:{id = ID_MNU_DIM;}
    }
    pCmdUI->SetCheck(pCmdUI->m_nID == id);
}

/**
 *  @brief  再描画メニュー項目.
 *  @param  なし
 *  @retval なし
 *  @note
 */
void CMockSketchView::OnMnuRedraw()
{
    m_pView->GetPartsDef()->Redraw(m_pView.get());
}

/**
 *  @brief  再描画メニュー項目更新.
 *  @param  [in] pCmdUI  コマンドUI
 *  @retval なし
 *  @note
 */
void CMockSketchView::OnUpdateMnuRedraw(CCmdUI *pCmdUI)
{
}

/**
 *  @brief  全体表示メニュー項目.
 *  @param  なし
 *  @retval なし
 *  @note
 */
void CMockSketchView::OnMnuViewAll()
{
    m_pView->ViewAll();
}

/**
 *  @brief  全体表示メニュー項目更新.
 *  @param  [in] pCmdUI  コマンドUI
 *  @retval なし
 *  @note
 */
void CMockSketchView::OnUpdateMnuViewAll(CCmdUI *pCmdUI)
{
}

/**
 *  @brief  アンドゥメニュー項目.
 *  @param  なし
 *  @retval なし
 *  @note
 */
void CMockSketchView::OnEditUndo()
{
    m_pView->Undo();
}

/**
 *  @brief  アンドゥメニュー項目更新.
 *  @param  [in] pCmdUI  コマンドUI
 *  @retval なし
 *  @note
 */
void CMockSketchView::OnUpdateEditUndo(CCmdUI *pCmdUI)
{
    pCmdUI->Enable(!m_pView->IsUndoEmpty());
}

/**
 *  @brief  アンドゥメニュー項目.
 *  @param  なし
 *  @retval なし
 *  @note
 */
void CMockSketchView::OnEditRedo()
{
    m_pView->Redo();
}

/**
 *  @brief  リドゥメニュー項目更新.
 *  @param  [in] pCmdUI  コマンドUI
 *  @retval なし
 *  @note
 */
void CMockSketchView::OnUpdateEditRedo(CCmdUI *pCmdUI)
{
    pCmdUI->Enable(!m_pView->IsRedoEmpty());
}


/**
 * 生成時処理要求
 * @param [in]    lpCreateStruct	初期化パラメータへのポインタ	 	  
 * @return		
 * @note	 
 * @exception   なし
 */
int CMockSketchView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (CView::OnCreate(lpCreateStruct) == -1)
        return -1;

    // ダイアログバーの生成
	if(!m_pDlgbarNumInput->Create(this, IDD_DLG_NUM_INPUT, WS_VISIBLE | CBRS_TOP,
		IDD_DLG_NUM_INPUT))
	{
		//DB_PRINT(_T("Failed to create toolbar\n"));
		return FALSE;      // 作成に失敗
	}

    //スクロールバーはなし
    
    //-------------------------
    
    SetScrollRange(SB_HORZ, 0, 0);
    SetScrollPos(SB_HORZ, 0, TRUE);
    ShowScrollBar(SB_HORZ, FALSE);

    //SetScrollSizes(MM_TEXT, CSize(0, 0));

    AdjustLayout();
    return TRUE;
    //m_dlgbarNumInput.EnableDocking(CBRS_ALIGN_ANY);
	////EnableDocking(CBRS_ALIGN_ANY);
	////DockControlBar(&m_wndDlgBar);
	////m_wndDlgBar.SetWindowText("文字列の検索");
    //return 0;
}

/**
 * @brief   レイアウト調整
 * @param   なし
 * @return	なし
 * @note	 
 */
void CMockSketchView::AdjustLayout()
{
	if (GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect rectClient;
	GetClientRect(rectClient);

	int cyTlb = m_pDlgbarNumInput->CalcFixedLayout(FALSE, TRUE).cy;

    if (m_pView)
    {
        m_pView->SetInputBarSize(cyTlb);
    }

	m_pDlgbarNumInput->SetWindowPos(NULL, rectClient.left, rectClient.top,
        rectClient.Width(), cyTlb, SWP_NOACTIVATE | SWP_NOZORDER);
}

/**
 *  @brief 値入力
 *  @param [in] wParam   使用しない
 *  @param [in] lParam   使用しない 
 *  @retval           な
 *  @note
 */
LRESULT CMockSketchView::OnLayerChange(WPARAM wParam, LPARAM lParam)
{
    STD_ASSERT(m_pView != NULL);
    STD_ASSERT(m_pDlgbarNumInput != NULL);

    int iLineType = m_pView->GetCurrentLayer()->iLineType; 
    int iLineTypeId = CLineStyleMenuItem::GetLineStyleToId (iLineType);

    auto*  pCb = GetToolBarButton(ID_MNU_LINE_STYLE);
    auto pLc = dynamic_cast<CToolBarLineComboBox*>(pCb);
    if(pLc)
    {
        pLc->SetLineStyle(iLineTypeId);
    }

    pCb = GetToolBarButton(ID_MNU_LINE_WIDTH);
    auto pLw = dynamic_cast<CToolBarLineWidthComboBox*>(pCb);
    if(pLw)
    {
        pLw->SetWidth(m_pView->GetCurrentLayer()->iLineWidth);
    }

    pCb = GetToolBarButton(ID_MNU_LINE_COLOR);
    auto pC = dynamic_cast<CMFCColorMenuButton*>(pCb);
    if(pC)
    {
        pC->SetColor(m_pView->GetCurrentLayer()->crDefault);
    }

    pCb = GetToolBarButton(ID_MNU_BRUSH_COLOR);
    auto pB = dynamic_cast<CMFCColorMenuButton*>(pCb);
    if(pB)
    {
        pB->SetColor(m_pView->GetCurrentLayer()->crBrush);
    }

    return 0;
}


/**
 *  @brief 値入力
 *  @param [in] wParam   使用しない
 *  @param [in] lParam   使用しない 
 *  @retval           な
 *  @note
 */
LRESULT CMockSketchView::OnValInput(WPARAM wParam, LPARAM lParam)
{
    //
    STD_ASSERT(m_pView != NULL);
    STD_ASSERT(m_pDlgbarNumInput != NULL);
    StdString strVal = m_pDlgbarNumInput->GetString();
    
    bool bRet = m_pView->InputValue(strVal);
    m_pDlgbarNumInput->UpdateInput(bRet);
    return 0;
}

/**
 *  @brief 入力値更新
 *  @param [in] bOk   更新する
 *  @retval           なし
 *  @note bOk==falseの時は値を選択状態にする
 */
void CMockSketchView::UpdateInput(bool bOk)
{
    m_pDlgbarNumInput->UpdateInput(bOk);
}

/**
 * @brief    コメント入力
 * @param    strComment  入力値
 * @retval   なし
 * @note	
 */
void CMockSketchView::SetComment(StdString strComment)
{
    STD_ASSERT(m_pDlgbarNumInput != NULL);
    m_pDlgbarNumInput->SetComment(strComment);
}


void CMockSketchView::ClearInput()
{
    STD_ASSERT(m_pDlgbarNumInput != NULL);
    m_pDlgbarNumInput->ClearInput();
}

/**
 * @brief    入力可・不可設定
 * @param    bEnable  true:入力可
 * @retval   なし
 * @note	
 */
void CMockSketchView::EnableInput(bool bEnable)
{
    STD_ASSERT(m_pDlgbarNumInput != NULL);
    m_pDlgbarNumInput->EnableInput(bEnable);
}

/**
 * @brief    入力エリアフォーカス設定
 * @param    なし
 * @retval   なし
 * @note	
 */
void CMockSketchView::SetInputFocus()
{
    STD_ASSERT(m_pDlgbarNumInput != NULL);
 
    if (m_Rich.IsWindowVisible())
    {
        CWnd* pFocusWindow = m_Rich.GetFocus();
        if(pFocusWindow)
        {
            if (pFocusWindow->m_hWnd != m_Rich.m_hWnd)
            {
                m_Rich.SetFocus();
            }
        }
        else
        {
            m_Rich.SetFocus();
        }
    }
    else
    {
        if (!m_pDlgbarNumInput->HasFocus())
        {
            m_pDlgbarNumInput->SetFocus();
        }
    }
}

/**
 * @brief    入力値取得
 * @param    なし
 * @retval   入力値
 * @note	
 */
StdString CMockSketchView::GetInputValue()
{
    STD_ASSERT(m_pDlgbarNumInput != NULL);
    return m_pDlgbarNumInput->GetString();
}

/**
 * @brief    リッチエディット変更イベント
 * @param    なし
 * @retval   なし
 * @note	
 */
void CMockSketchView::OnEnSelchangeRichedit(NMHDR *pNMHDR, LRESULT *pResult)
{
    SELCHANGE *pSelChange = reinterpret_cast<SELCHANGE *>(pNMHDR);

    /*
 
    CHARFORMAT2 cf;
    memset(&cf, 0, sizeof(cf));
    cf.cbSize = sizeof(CHARFORMAT2);

    cf.dwMask = CFM_BOLD | CFM_ITALIC | CFM_UNDERLINE |CFM_STRIKEOUT |
            CFM_SUBSCRIPT| CFM_SUPERSCRIPT|
            CFM_SIZE | CFM_COLOR  | CFM_FACE | CFM_OFFSET | CFM_CHARSET;

    cf.szFaceName[0] = NULL;

    long lStart, lEnd;

    m_Rich.GetSel(lStart, lEnd);
    m_Rich.GetSelectionCharFormat(cf);

    DB_PRINT( _T("Rich sel (%03d, %03d) - (%03d, %03d) %x \n"), lStart, lEnd, pSelChange->chrg.cpMin,
                                                                           pSelChange->chrg.cpMax,
                                                                           pSelChange->seltyp);
    */

}
  
void CMockSketchView::InitRichEdit()
{
    CCharFormat cf;


    cf.dwMask = 0;

 
    //フォント名
    cf.dwMask |= (CFM_FACE | CFM_CHARSET);
    if (lstrcpyn(cf.szFaceName,
        DRAW_CONFIG->GetDefaultFont(),
        LF_FACESIZE) == NULL)
    {
        cf.szFaceName[0] = 0;
        STD_DBG(_T("InitRichEdit lstrcpyn fail 1"));
    }

    cf.bCharSet = DEFAULT_CHARSET;
    cf.bPitchAndFamily = 0;

    COLORREF color = DRAW_CONFIG->GetDefautTextColor();
    cf.dwMask |= CFM_COLOR;
    cf.crTextColor = color; 

    cf.dwMask |= CFM_SIZE;
    cf.yHeight = DRAW_CONFIG->GetFontSize() * 20;

    cf.dwEffects   = 0;
    cf.cbSize = sizeof(cf);

    m_Rich.SetDefaultCharFormat(cf);
    m_Rich.SetWindowText(_T(""));

    m_Rich.SetBackgroundColor(FALSE, DRAW_CONFIG->crBack);


    //----------------------------------------------------
    PARAFORMAT2 pf;
    memset(&pf, 0, sizeof(pf));
    pf.cbSize = sizeof(pf);
    pf.dwMask = (PFM_LINESPACING | PFM_LINESPACING);

    m_Rich.SetParaFormat(pf);
    pf.bLineSpacingRule = 5;
    pf.dyLineSpacing = 00;
    m_Rich.SetParaFormat(pf);
    /*
    PARAFORMAT2 pf;
    m_Rich.GetParaFormat(pf);

    DB_PRINT(_T("m_Rich.GetParaFormat B:%d A:%d S:%d R:%d \n"),
        pf.dySpaceBefore, //Twipで段落上部の間隔のサイズ   PFM_SPACEBEFORE
        pf.dySpaceAfter,  //Twipで段落下部の間隔のサイズ   PFM_SPACEAFTER
        pf.dyLineSpacing, //行間の間隔 解釈は、bLineSpacingRuleに依る       PFM_LINESPACING
        pf.bLineSpacingRule   //  行間隔のタイプ    PFM_LINESPACING
    );
    */
    /*
    int iSpace =

    int ls = pf->dyLineSpacing;
    switch (pf->bLineSpacingRule)
    {
    case 0:       iSpace = ls; break;
    case 1:       sp = (3 * ls) / 2; break;
    case 2:       sp = 2 * ls; break;
    case 3:       sp = ME_twips2pointsY(c, para->pFmt->dyLineSpacing); 
                  if (sp < ls) sp = ls;
                  break;
    case 4:       sp = ME_twips2pointsY(c, para->pFmt->dyLineSpacing); 
                  break;
    case 5:       sp = para->pFmt->dyLineSpacing / 20; 
                  break;

        }

    int ME_twips2pointsY(const ME_Context *c, int y)
    {
        if (c->editor->nZoomNumerator == 0)
            return y * c->dpi.cy / 1440;
        else
            return y * c->dpi.cy * c->editor->nZoomNumerator / 1440 / c->editor->nZoomDenominator;
    }
    */

    /*
    pf.dySpaceBefore; //Twipで段落上部の間隔のサイズ   PFM_SPACEBEFORE
    pf.dySpaceAfter;  //Twipで段落下部の間隔のサイズ   PFM_SPACEAFTER
    pf.dyLineSpacing; //行間の間隔 解釈は、bLineSpacingRuleに依る       PFM_LINESPACING
    pf.sStyle;        //テキストのスタイル    TOMインターフェイス用 不使用
    pf.bLineSpacingRule;   //  行間隔のタイプ    PFM_LINESPACING
    ////
    ////                        値	意味
    //                          0   シングルスペース。 dyLineSpacingのメンバーは無視されます。

    //                          1   1つ半の間隔。 dyLineSpacingのメンバーは無視されます。

    //                          2   ダブルスペーシング。 dyLineSpacingのメンバーは無視されます。

    //                          3   dyLineSpacingメンバーは、1つのラインから次のラインまでの 
    //                                 間隔をTwipトゥウィップで指定します。ただし、 
    //                                 dyLineSpacingがシングルスペースより小さい値を指定した場合、
    //                                 コントロールはシングルスペースのテキストを表示します。

    //                          4   dyLineSpacingのメンバーはtwip単位で、次の1行から間隔を指定します。
    //                              コントロールは、dyLineSpacingが単一の間隔よりも小さい値を指定した場合でも、
    //                              指定された正確な間隔を使用します 。
    //                          
    //                          5   dyLineSpacing / 20 の値 は、ある行から次の行への行単位の間隔です。
    //                              したがって、dyLineSpacingを20 に設定 すると、
    //                              シングルスペーステキスト、40はダブルスペース、60はトリプルスペースなどになります。

    pf.bOutlineLevel;       //予約済み
    pf.wShadingWeight;      //不使用
    pf.wShadingStyle;       //不使用
    pf.wNumberingStart;     //不使用
    pf.wNumberingStyle;        //不使用
    pf.wNumberingTab;          //不使用
    pf.wBorderSpace;           //不使用
    pf.wBorderWidth;           //境界線の幅（twip単位）   PFM_BORDER
    pf.wBorders;               //ボーダーの位置、スタイル、色      ビット0〜7は境界線 8〜11は境界線のスタイル
                               //ビット12〜15は境界線のカラーインデックス PFM_BORDER
    */

    //Zoom
    double dZoom = m_pView->GetViewScale();


    LPARAM  lDenominator = 0; //分母
    WPARAM  wNumerator = 0; //分子

    static const double div = (1.0 / 64.0) /2.0;

    if (dZoom >= 64)
    {
        lDenominator = 64;
        wNumerator = 1;
    }
    else if (dZoom <= div)
    {
        lDenominator = 1;
        wNumerator = 64;
    }
    else if (dZoom >= (1+div))
    {
        //TODO:良い方法があったら書き直す
        lDenominator = 1;
        wNumerator = CUtil::Round(dZoom);
    }
    else if (dZoom < (1 - div))
    {
        lDenominator = CUtil::Round(1 / dZoom);
        wNumerator = 1;
    }
    m_Rich.SendMessage(EM_SETZOOM, wNumerator, lDenominator);

    //半角文字のフォントを指定したフォントセットする
    DWORD dwLangOptions;
    dwLangOptions = m_Rich.SendMessage(EM_GETLANGOPTIONS, 0, 0); 
    dwLangOptions &= ~IMF_DUALFONT;
    m_Rich.SendMessage(EM_SETLANGOPTIONS, 0, (LPARAM)dwLangOptions);
}



CRichEditCtrl* CMockSketchView::GetRichEdit() 
{ 
    return &m_Rich;
}
/**
 * @brief    Viewデータ変更
 * @param    [in] pPartsCtrl 
 * @retval   なし
 * @note     部品作成時に使用する
 */
void CMockSketchView::SetView()
{

    DBG_ASSERT(m_pView != NULL);

    m_pView->SetWindow( this );
    m_hDc = ::GetDC(m_hWnd);
    m_pView->SetDc(GetDC());
    m_pDlgbarNumInput->SetParent(m_hWnd);
    m_pView->ResetCtrl();
}


/**
 * @brief    描画インスタンス設定
 * @param    [in] pPartsCtrl 
 * @retval   なし
 * @note
 */
/*
bool CMockSketchView::SetDrawiingScript(CDrawingScriptBase* pDrawingScript)
{

    bool bRet;
    DBG_ASSERT(m_pView != NULL);

    bRet = CObjectDefView::SetDrawiingScript(pDrawingScript);
    if (!bRet)
    {
        return false;
    }

    m_pView->SetDrawiingScript(pDrawingScript);
    m_pView->SetWindow( this );

    m_hDc = ::GetDC(m_hWnd);
    m_pView->SetDc(m_hDc);
    m_pDlgbarNumInput->SetParent(m_hWnd);

    m_pView->ResetCtrl();
    return false;
}
/

/**
 * @brief   ビュー アクティブ状態変更
 * @param   [in]    bActivate       ビューがアクティブ/非アクティブ
 * @param   [in]    pActivateView   アクティブにされているビューへのポインタ
 * @param   [in]    pDeactiveView   非アクティブになるビューへのポインタ
 * @return  なし
 * @note    フレームワークから呼び出し
 */
void CMockSketchView::OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView)
{

#if 1
    {
        StdString strTitle;
        CDocument* pDoc = GetDocument();
        if (pDoc != NULL)
        {
            strTitle = (LPCTSTR)pDoc->GetTitle();
            //DB_PRINT(_T("OnActivate bActive(%d) %s\n"), bActivate, strTitle.c_str());
        }
        else
        {
            //DB_PRINT(_T("OnActivate bActive(%d) NULL\n"), bActivate);
        }
    }
#endif

    CView::OnActivateView(bActivate, pActivateView, pDeactiveView);
    if (bActivate)
    {
        //ウインドウの切り替えを通知
        StdString strTitle;
        CDocument* pDoc = GetDocument();

        if (pDoc == NULL)
        {
            STD_DBG(_T("GetDocument"));
            return;
        }

        strTitle = (LPCTSTR)pDoc->GetTitle();
        strTitle = CMockSketchDoc::SplitTitleMarker(strTitle.c_str());
        ::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_VIEW_ACTIVE, (WPARAM)&strTitle, (LPARAM)0);
        THIS_APP->SetAvtiveTabNmae(strTitle);
        THIS_APP->GetMainFrame()->SetActiveView(this, FALSE);
        CPartsDef* pDef = m_pView->GetPartsDef();
        if (pDef)
        {
            int iLayerId = pDef->GetCurrentLayerId();
            ::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_UPDATE_CURRENT_LAYER_NUM, (WPARAM)iLayerId, (LPARAM)0);
        }
    }

    if (bActivate)
    {
        if (!m_pView->IsPrintMode())
        {
            CPartsDef* pDef = m_pView->GetPartsDef();
            if (pDef)
            {
                auto pGroup = pDef->GetDrawingParts();
                STD_ASSERT(pGroup);
                pGroup->UpdateAllUserPropertyValue();
                pDef->Redraw(m_pView.get());
            }
        }
    }
}


/**
 * @brief   名前更新
 * @param   [in] wParam 使用しない
 * @param   [in] lParam 使用しない
 * @return  なし
 * @note    CObjectWnd::OnEndLabelEditから呼び出し
 */
LRESULT CMockSketchView::OnUpdateName(WPARAM wParam, LPARAM lParam)
{
    // ドキュメントを更新
    CDocument* pDoc = GetDocument();
    if (pDoc == NULL)
    {
        STD_DBG(_T("GetDocument"));
        return 0;
    }
    CMockSketchDoc* pSketchDoc;
    pSketchDoc = reinterpret_cast<CMockSketchDoc*>(pDoc);
    pSketchDoc->UpdateName();

    return 0;
}


/**
 * @brief   ビュークリア
 * @param   [in] wParam 使用しない
 * @param   [in] lParam 使用しない
 * @return  なし
 * @note    
 */
LRESULT CMockSketchView::OnViewClear(WPARAM pMessage, LPARAM lParam)
{
    return 0;
}

/**
 * @brief   ウインドウ終了処理
 * @param   なし
 * @return  なし
 * @note    
 */
void CMockSketchView::OnClose()
{
    //DB_PRINT(_T("CMockSketchView::OnClose %x hWnd=%x\n"), this, m_hWnd);
    CView::OnClose();
}

/**
 * @brief   ウインドウ破棄
 * @param   なし
 * @return  なし
 * @note    
 */
void CMockSketchView::OnDestroy()
{
    //DB_PRINT(_T("CMockSketchView::OnDestroy %x hWnd=%x\n"), this, m_hWnd);
    m_pView->DestoryWindow();
    CView::OnDestroy();
}



 /**
 * @brief   ドロップ完了通知
 * @param   [in] type   ドロップ種別 
 * @param   [in] lParam ドロップデータ (DROP_PARTS_DATA) 
 * @retval  
 * @note	
 */
LRESULT CMockSketchView::OnDropEnd(WPARAM type, LPARAM lParam)
{
    DROP_TARGET_DATA*  pTargetData;
    pTargetData = reinterpret_cast<DROP_TARGET_DATA*>(lParam);
    LRESULT lRet = S_FALSE;

    if (CF_MOCK_PARTS == type)
    {
       m_pView->OnLButtonUp(pTargetData->dwKeyState, pTargetData->ptDrop);
       m_pView->SetViewMode(VIEW_SELECT);
    }
    return lRet;
}

/**
 * @brief   ドロップ通過開始通知
 * @param   [in] type    ドロップ種別 
 * @param   [in] lParam  dropTarget
 * @retval  
 * @note	
 */
LRESULT CMockSketchView::OnDropEnter(WPARAM type, LPARAM lParam)
{
    CPoint point;
    LRESULT lRet = S_FALSE;
    bool bRet = false;
    if (CF_MOCK_PARTS == type)
    {
        DROP_TARGET_DATA*  pTargetData;
        pTargetData = reinterpret_cast<DROP_TARGET_DATA*>(lParam);
        bRet = m_pView->SetViewMode(VIEW_DROP, false, pTargetData);
        if (!bRet)
        {
            pTargetData->dropEffect = DROPEFFECT_NONE;
        }
        else
        {
            pTargetData->dropEffect = DROPEFFECT_COPY;
            lRet = S_OK;
        }
    }
    else if (type == CF_TEXT)
    {

    }
    else if (type == CF_UNICODETEXT)
    {

    }
    else if (type == CF_BITMAP)
    {

    }
    else if (type == CF_METAFILEPICT)
    {


    }
    else if (type ==CF_ENHMETAFILE)
    {

    }


    return lRet;
}


/**
 * @brief   ドロップ通過通知
 * @param   [in] type    ドロップ種別 
 * @param   [in] lParam  マウス位置
 * @retval  
 * @note	
 */
LRESULT CMockSketchView::OnDropOver(WPARAM type, LPARAM lParam)
{
    CPoint point;
    if (type == CF_MOCK_PARTS)
    {
        DROP_TARGET_DATA*  pTargetData;
        pTargetData = reinterpret_cast<DROP_TARGET_DATA*>(lParam);
        if (pTargetData->dropEffect == DROPEFFECT_COPY)
        {
            m_pView->OnMouseMove(pTargetData->dwKeyState, pTargetData->ptDrop);
            return DROPEFFECT_COPY;
        }
    }
    else if (type == CF_TEXT)
    {

    }
    else if (type == CF_UNICODETEXT)
    {

    }
    else if (type == CF_BITMAP)
    {

    }
    else if (type == CF_METAFILEPICT)
    {


    }
    else if (type ==CF_ENHMETAFILE)
    {

    }
    
    

    return DROPEFFECT_NONE;
}

/**
 * @brief   ドロップ通過完了
 * @param   [in] type    ドロップ種別 
 * @param   [in] lParam  使用しない
 * @retval  
 * @note	
 */
LRESULT CMockSketchView::OnDropLeave(WPARAM type, LPARAM lParam)
{
    CPoint point;
    if (CF_MOCK_PARTS == type)
    {
        m_pView->SetViewMode(VIEW_SELECT);
    }
    return 0;
}


//!<  
/**
 * @brief   アクティブ・非アクティブ切替
 * @param   [in] type    ドロップ種別 
 * @param   [in] lParam  使用しない
 * @retval  
 * @note    
 */
void CMockSketchView::OnActivateFrame( UINT nState,  CFrameWnd* pFrameWnd)
{

    CView::OnActivateFrame( nState, pFrameWnd);
}


void CMockSketchView::OnMnuCancel   ()
{
    m_pView->OnCancelAction();
}

void CMockSketchView::OnMnuEnd      ()
{
    DB_PRINT(_T("OnMnuEnd\n"));
    OnMenuSelect(ID_MNU_END, 0, 0);
}

void CMockSketchView::OnMnuImageCtrl()
{
    CMockSketchDoc* pDoc = GetDocument();
    ASSERT_VALID(pDoc);
    std::shared_ptr<CObjectDef> pDef;
    pDef = pDoc->GetObjectDef().lock();

    CPartsDef* pParts = NULL;
    pParts = dynamic_cast<CPartsDef*>(pDef.get());

    if (pParts)
    {
        CImageCtrlDlg dlg(pParts);
        dlg.DoModal();
    }
}


void CMockSketchView::OnUpdateMnuImageCtrl(CCmdUI *pCmdUI)
{

}


bool CMockSketchView::CalcRichEditSize(SIZE* pSize)
{
    CDC* pDc = m_Rich.GetDC();
    HDC hDc =  pDc->GetSafeHdc();

    TEXTMETRIC tm;
    ::GetTextMetrics(hDc, &tm);
    int iLastType = -1;

    BOOL bChange;
    bChange = m_Rich.GetModify();
    if (!bChange)
    {
        return false;
    }
    /*
    GetCharPosで文字位置を調べRichEditの大きさを決める
    但し、GetCharPosは文字の左上を返すため最終行の高さがわからない
    そのため、末尾に"\n "を追加し文字位置を調べたあと削除する。
    */
    CHARRANGE cr;
    m_Rich.GetSel(cr);
    m_Rich.SetSel(0,0);
    const int Buffsize = 4096;
    TCHAR buff[Buffsize];
    TCHAR buff2[Buffsize];


    /*
    auto GetLine = [=](int iLine)
    {
        int iLastLineLength = m_Rich.LineLength(iLine);
        int iLastIndex = m_Rich.LineIndex(iLine) + m_Rich.LineLength(iLine);
        memset(&buff[0], 0, sizeof(buff)); 
        int iLineLength2 = m_Rich.GetLine(iLine, &buff[0], Buffsize);
    };
    */

    //終端文字の位置を取得する
    int iLast = -1;
    {
        int iLastLine = m_Rich.GetLineCount() - 1;
        if(iLastLine >= 0)
        {
            int iLastLineLength = m_Rich.LineLength(iLastLine);
            iLast = m_Rich.LineIndex(iLastLine) + m_Rich.LineLength(iLastLine);
            memset(&buff[0], 0, sizeof(buff)); 
            int iLineLength2 = m_Rich.GetLine(iLastLine, &buff[0], Buffsize);

            if (iLineLength2 > 0)
            {

                bool bAddLast = true;
                if (buff[iLineLength2 - 1] == 0x0d)
                {
                    if(iLineLength2 == 1)
                    {
                        bAddLast = false;
                    }
                }

                if(bAddLast)
                {
                    m_Rich.SetSel(-1, -1);
                    m_Rich.ReplaceSel(_T("\r"));
                    iLastType = 1;
                }
            }
        }
    }

    {
        int iLastLine = m_Rich.GetLineCount() - 1;
        if(iLastLine >= 0)
        {
            int iLastLineLength = m_Rich.LineLength(iLastLine);
            iLast = m_Rich.LineIndex(iLastLine) + m_Rich.LineLength(iLastLine);
            memset(&buff2[0], 0, sizeof(buff2)); 
            int iLineLength2 = m_Rich.GetLine(iLastLine, &buff2[0], Buffsize);
        }
    }



    //
    int iLinePitch;
    int iChar;

    int iAddWidth = tm.tmMaxCharWidth + 5;
    int iAddHeight =tm.tmHeight * 1.5;

    int iWidthMax = DRAW_CONFIG->DimSettingToDot(DRAW_CONFIG->dTextBoxMinWidth);
    int iHeightMax = DRAW_CONFIG->DimSettingToDot(DRAW_CONFIG->dTextBoxMinHeight);
    int iHeightMin = 0;
    int iLineCount = m_Rich.GetLineCount();
    int iLineTopIndex = 0;
    int iIndex = 0;
    int iLineLength = 0;
    int iLineLength2 = 0;


#if DEBUG_RICH_EDIT
    DB_PRINT(_T("CHAR SIZE (%d,%d) \n"), tm.tmMaxCharWidth, tm.tmHeight);
#endif



    for (int iLine = 0; iLine < iLineCount; ++iLine)
    {
        iLineTopIndex = m_Rich.LineIndex(iLine);
        iLineLength = m_Rich.LineLength(iLine);

        memset(&buff[0], 0, sizeof(buff)); 

        iLineLength2 = m_Rich.GetLine(iLine, &buff[0], Buffsize);

        if (iLineLength2 > 0)
        {
            if (buff[iLineLength2 - 1] == 0x0d)
            {
                iLineLength2--;
            }
        }

        iIndex = iLineTopIndex + iLineLength2;

        //最終行の位置を取得する(左上)
        CPoint pt = m_Rich.GetCharPos(iIndex);

#if DEBUG_RICH_EDIT
        DB_PRINT(_T("LINE %d [%d, %d, %d ] (%d,%d) %s\n"), iLine, iIndex, iLineTopIndex, iLineLength2, pt.x, pt.y, buff);
#endif

        if (iHeightMin > pt.y)
        {
            iHeightMin = pt.y;
        }

        if ((pt.x + iAddWidth) > (iWidthMax))
        {
            iWidthMax = pt.x + iAddWidth;
        }

        if ((pt.y+iAddHeight) > iHeightMax)
        {
            iHeightMax = pt.y + iAddHeight;
        }
    }

#if DEBUG_RICH_EDIT
    DB_PRINT(_T("W;%d h:%d \n"), iWidthMax, iHeightMax);
#endif

    if(iLastType != -1)
    {
        m_Rich.SetSel(iLast, -1/*iLast + iLastType*/);
        m_Rich.ReplaceSel(_T(""));
    }

#if DEBUG_RICH_EDIT
    DB_PRINT(_T("CR min:%d max:%d\n"), cr.cpMin, cr.cpMax);
#endif


    m_Rich.SetSel(cr);



    SIZE sz;
    sz.cx = iWidthMax;
    sz.cy = iHeightMax - iHeightMin;

    *pSize = sz; 

    BOOL bModifyed = FALSE;
    m_Rich.SetModify(bModifyed);
    return true;

}

void CMockSketchView::OnEnChangeRichedit()
{
    /*
    入力に合わせてエディットエリアのサイズを変更する。
    CRichEditCtrl::LineLength(iLine); は機能しないようなので
    CRichEditCtrl::GetLineを使用する。
    IME使用時に位置がずれる場合があるので  iAddWidth，iAddHeight 分サイズを大きくする
    */

    SIZE sz;
    bool  bChg = CalcRichEditSize(&sz);
#if DEBUG_RICH_EDIT
    CRect rc;
    m_Rich.GetRect(rc);
    DB_PRINT(_T("Rect  (L:%d, T:%d, R:%d, B:%d)[W:%d H:%d]\n"), rc.left, rc.top, rc.right, rc.bottom, sz.cx, sz.cy);
#endif


    if (bChg)
    {
        m_Rich.SetWindowPos(&CWnd::wndBottom, 0,0, sz.cx, sz.cy,SWP_NOMOVE);
        //m_Rich.SetWindowPos(&CWnd::wndBottom, 0,0, rc.Width(), rc.Height(),SWP_NOMOVE);
        m_Rich.RequestResize();
    }

#if DEBUG_RICH_EDIT
    m_Rich.GetRect(rc);
    DB_PRINT(_T("Rect2 (L:%d, T:%d, R:%d, B:%d)\n"), rc.left, rc.top, rc.right, rc.bottom);
#endif
}



LRESULT CMockSketchView::OnImeChar(WPARAM iType, LPARAM pMessage)
{
    OnEnChangeRichedit();
    return 0;
}

LRESULT CMockSketchView::OnImeComposition(WPARAM iType, LPARAM pMessage)
{
    OnEnChangeRichedit();
    return 0;
}



/**
 *  @brief  メッセージフィルタ処理
 *  @param  [in] pMsg   ウインドウメッセージ
 *  @retval true これ以上の処理不要 / false 通常処理
 *  @note   
 */
BOOL CMockSketchView::PreTranslateMessage(MSG* pMsg)
{
#if 0
    static UINT uiOldMsg2 = 0;
    if (uiOldMsg2 != pMsg->message)
    {
        uiOldMsg2 = pMsg->message;
        DB_PRINT(_T("CMockSketchView::  %s,%x \n"), CUtil::ConvWinMessage(pMsg->message),
                                                       pMsg->hwnd);
    }
#endif

    if (pMsg->hwnd == m_Rich.m_hWnd)
    {
        if (pMsg->message == WM_IME_CHAR)
        {
            OnImeChar(pMsg->wParam, pMsg->lParam);
        }
        else if (pMsg->message == WM_IME_COMPOSITION)
        {
            /*
            未確定文字列がやってきたとき
            未確定文字列に変更があったとき
            未確定文字列が確定されたとき
            */
            OnImeComposition(pMsg->wParam, pMsg->lParam);
        }

        /*
        WM_IME_STARTCOMPOSITION      これから未確定文字列がやってくるという予告。
        WM_IME_NOTIFY, WM_IME_REQUEST    IMEからの通知です
        ImmSetCandidateWindow(hImc, &form)で変換候補ウィンドウの位置を教える
        */

        if (m_Rich.GetFocus() == &m_Rich)
        {
            bool bFocus = false;
            switch (pMsg->message)
            {
                case WM_KEYDOWN:

                    if (::GetKeyState(VK_CONTROL) & 0x8000)
                    {
                        if ((pMsg->wParam) == _T('V'))     {m_Rich.Paste();}
                        else if ((pMsg->wParam) == _T('C')){m_Rich.Copy();}
                        else if ((pMsg->wParam) == _T('X')){m_Rich.Cut();}
                        else if ((pMsg->wParam) == _T('Z')){m_Rich.Undo();}
                        else if ((pMsg->wParam) == VK_RETURN)
                        {
                            //確定する
                            OnLButtonUp(0, CPoint(0, 0));
                        }
                        else if ((pMsg->wParam) == VK_DELETE)
                        {
                            //DB_PRINT(_T("Delete \n"));
                        }
                    }
                    else if ((pMsg->wParam) == VK_ESCAPE)
                    {
                        //キャンセル
                        m_pView->OnCancelAction();
                    }
                    else
                    {
                        //m_Rich.PreTranslateMessage(pMsg);
                    }
                    break;

                case WM_KEYUP:   
                    if (pMsg->wParam == VK_DELETE)
                    {
                       //int a = 10;
                    }
                    break;
                case WM_CHAR:    
                    break;
            }
        }
    }

    switch(pMsg->message)
    {
        case WM_LBUTTONDOWN: 
        case WM_LBUTTONUP: 
        case WM_MOUSEMOVE: 
            m_toolTip.RelayEvent(pMsg);
            break;

        case WM_KEYDOWN:
            //DB_PRINT(_T("WM_KEYDOWN %s\n"), CUtil::ConvVK(pMsg->wParam));
            break;
        case WM_KEYUP:   
            //DB_PRINT(_T("WM_KEYUP %s\n"), CUtil::ConvVK(pMsg->wParam));
            break;
        case WM_CHAR:    
            //DB_PRINT(_T("WM_CHAR %c\n"), pMsg->wParam);
            break;
        default:
            break;
    }
    return CView::PreTranslateMessage(pMsg);
}

/**
 *  @brief モード変更
 *  @param [in] wParam   アプリケーションモード(EXEC_COMMON::E_APP_MODE)
 *  @param [in] lParam   オブジェクト定義      (CObjectDef)
 *  @retval
 *  @note
 */
LRESULT CMockSketchView::OnModeChange(WPARAM wParam, LPARAM lParam)
{
    using namespace EXEC_COMMON;

    E_EXEC_STS  eSts;
    CObjectDef* pDef;

    eSts = static_cast<E_EXEC_STS>(wParam);
    pDef = reinterpret_cast<CObjectDef*>(lParam);

    if (eSts == EX_EDIT)
    {
    }

    return 0;
}

void CMockSketchView::OnInitialUpdate()
{
    __super::OnInitialUpdate();

    m_toolTip.Create(this);
    m_toolTip.AddTool(this);

    // ツールチップの改行を有効にするため
    // 最大幅を設定する

    m_toolTip.SetMaxTipWidth(1024);
}

void CMockSketchView::SetToolTipText(LPCTSTR strText)
{
    if (!m_bPopTooltip)
    {
        m_toolTip.Activate(false);
        m_toolTip.Activate(true);
    }

    {
#if 0
if (!m_toolTip.IsWindowVisible())
{
TRACE(_T("SetToolTipText Invisible \n"));
}
#endif
        m_toolTip.Activate(true);
        m_toolTip.UpdateTipText(  strText, this);
        m_toolTip.Popup();
    }
    m_bPopTooltip = true;
}

void CMockSketchView::ResetToolTipText()
{
    if(m_bPopTooltip)
    {
        m_toolTip.Activate(false);
        m_toolTip.UpdateTipText(  _T(""), this);
        m_toolTip.Pop( );
        m_bPopTooltip = false;
    }
}


void CMockSketchView::OnMenuSelect(UINT nItemID, UINT nFlags, HMENU hSysMenu)
{
    __super::OnMenuSelect(nItemID, nFlags, hSysMenu);

    // TODO: ここにメッセージ ハンドラー コードを追加します。

    m_pView->OnMenuSelect( nItemID, nFlags, hSysMenu);

}


void CMockSketchView::OnPrepareDC(CDC* pDC, CPrintInfo* pInfo)
{
    if (pInfo)
    {
        m_pView->PrepareDC( pDC, pInfo);
    }
    __super::OnPrepareDC(pDC, pInfo);
}


LRESULT CMockSketchView::OnUpdatePrinter(WPARAM wParam, LPARAM lParam)
{
    m_pView->UpdatePrinter();
    return 0;
}

void CMockSketchView::OnMnuOrder (UINT id)
{
    VIEW_COMMON::E_DISP_ORDER order;
    switch(id)
    {
    case ID_MNU_DSP_ORDER_TOPMOST:{order = VIEW_COMMON::E_TOPMOST; break;}
    case ID_MNU_DSP_ORDER_TOPBACK:{order = VIEW_COMMON::E_TOPBACK; break;}
    case ID_MNU_DSP_ORDER_FRONT: {order = VIEW_COMMON::E_FRONT; break;}
    case ID_MNU_DSP_ORDER_BACK:  {order = VIEW_COMMON::E_BACK; break;}
    default: return;
    }
    m_pView->ChangeObjectOrder(order);
}

void CMockSketchView::OnUpdateMnuOrder(CCmdUI* pCmdUI)
{

}

DWORD CMockSketchView::MenuIdToSnap (UINT id, bool bRedraw)
{
    DWORD dwSnap;
	bool bRedraw2 = false;
	switch(id)
    {    
        case ID_MNU_SNAP_NONE         :{dwSnap = VIEW_COMMON::SNP_NONE        ; break;}          
        case ID_MNU_SNAP_END_POINT    :{dwSnap = VIEW_COMMON::SNP_END_POINT   ; break;}      
        case ID_MNU_SNAP_MID_POINT    :{dwSnap = VIEW_COMMON::SNP_MID_POINT   ; break;}   
        case ID_MNU_SNAP_CROSS_POINT  :{dwSnap = VIEW_COMMON::SNP_CROSS_POINT ; break;}   
        case ID_MNU_SNAP_ON_LINE      :{dwSnap = VIEW_COMMON::SNP_ON_LINE     ; break;}   
        case ID_MNU_SNAP_NEAR_POINT   :{dwSnap = VIEW_COMMON::SNP_NEAR_POINT  ; break;}   
        case ID_MNU_SNAP_CENTER       :{dwSnap = VIEW_COMMON::SNP_CENTER      ; break;}   
        case ID_MNU_SNAP_TANGENT      :{dwSnap = VIEW_COMMON::SNP_TANGENT     ; break;}   
        case ID_MNU_SNAP_FEATURE_POINT:{dwSnap = VIEW_COMMON::SNP_FEATURE_POINT; break;}  
        case ID_MNU_SNAP_DATUM_POINT  :{dwSnap = VIEW_COMMON::SNP_DATUME_POINT; break;}   
		case ID_MNU_SNAP_GRID: {dwSnap = VIEW_COMMON::SNP_GRID; bRedraw2 = true; break; }
        case ID_MNU_SNAP_POINT        :{dwSnap = VIEW_COMMON::SNP_POINT       ; break;}   
        case ID_MNU_SNAP_NORM         :{dwSnap = VIEW_COMMON::SNP_NORM        ; break;}   
        case ID_MNU_SNAP_NODE         :{dwSnap = VIEW_COMMON::SNP_NODE        ; break;}  
        default:
            dwSnap = VIEW_COMMON::SNP_NONE;
            break;         
   }

	if (bRedraw && bRedraw2)
	{
		auto pDef = m_pView->GetPartsDef();
		pDef->Redraw(m_pView.get());
	}
   return dwSnap;
}

//!<スナップ
void CMockSketchView::OnMnuSnap (UINT id)
{
   DWORD dwSnap = MenuIdToSnap (id, false);
   DWORD dwSnapConf = DRAW_CONFIG->GetSnapType();
   DWORD dwSnapNew =   (dwSnapConf ^ dwSnap);

   DRAW_CONFIG->dwSnapType = dwSnapNew; 
   MenuIdToSnap(id, true);
}

void CMockSketchView::OnUpdateMnuSnap(CCmdUI* pCmdUI)
{
   DWORD dwSnap = MenuIdToSnap (pCmdUI->m_nID, false);
   bool bCheck = ((DRAW_CONFIG->GetSnapType() & dwSnap) == dwSnap);
   pCmdUI->SetCheck (bCheck);
}


void CMockSketchView::OnMnuShowPos (UINT id)
{
    int iIndex = id - ID_MNU_SHOW_POS_01;
    m_iViewPosition = iIndex;

    auto pDef = m_pView->GetPartsDef();
    auto pos = pDef->GetViewPosition(iIndex);

    if (m_bViewSet)
    {
        pos.dScl = m_pView->GetViewScale();
        pos.ptCenter = m_pView->GetLookAt();
        pDef->SetViewPosition(m_iViewPosition, pos);
        m_bViewSet = false;
    }
    else
    {
        m_pView->SetViewScale(pos.dScl);
        m_pView->ViewMove(&pos.ptCenter);
        pDef->Redraw(m_pView.get());
    }

    auto pWnd = theApp.GetSubWindow(VIEW_COMMON::SHOW_BAR);

    auto pShow = dynamic_cast<CShowBar*>(pWnd);
    if(pShow)
    {
        pShow->SetShowPos(m_iViewPosition);
    }
}                                                

void CMockSketchView::OnUpdateMnuShowPos(CCmdUI* pCmdUI)
{
    int iIndex = pCmdUI->m_nID - ID_MNU_SHOW_POS_01;

    bool bCheck = (m_iViewPosition ==  iIndex);

    pCmdUI->SetCheck (bCheck);
}

void CMockSketchView::OnMnuShowPosSet()
{
    if (m_bViewSet)
    {
        m_bViewSet = false;
        return;
    }

    //設定する位置番号を選択してください
    AfxMessageBox(GET_STR(STR_DIAG_SHOW_POS_SET));
    m_bViewSet = true;
}

void CMockSketchView::OnUpdateMnuShowPosSet(CCmdUI* pCmdUI)
{
    pCmdUI->SetCheck (m_bViewSet);
}

void CMockSketchView::OnShowPos()
{
   auto pWnd = theApp.GetSubWindow(VIEW_COMMON::SHOW_BAR);
   if(!pWnd)
   {
       return;
   }

   auto pShow = dynamic_cast<CShowBar*>(pWnd);
   if(!pShow)
   {
       return;
   }
   //最終的に OnMnuShowPos (UINT id) に送信する
   pShow->SendCurSel(ID_SHOW_POS);
}




void CMockSketchView::DrawTo(HDC hDc, DRAWING_MODE eMode, int iWidth, int iHeight)
{
    HDC hdcSrc = 0;
    if (eMode == DRAW_FIG)
    {
        hdcSrc = m_pView->GetBackDc();
    }
    else if (eMode == DRAW_INDEX)
    {
        hdcSrc = m_pView->GetIndexDc();
    }
    else if (eMode == DRAW_SEL)
    {
        hdcSrc = m_pView->GetSelDc();
    }
    else if (eMode == DRAW_FRONT)
    {
        hdcSrc = m_pView->GetViewDc();
    }
    else if (eMode == DRAW_PRINT)
    {
        HFONT hFont;
        hFont = ::CreateFont(
        -10,                               // フォントの高さ
        0,                // 平均文字幅
        0,                                // 文字送り方向の角度
        0,                                // ベースラインの角度
        0,              // フォントの太さ
        FALSE,           // 斜体にするかどうか
        FALSE,        // 下線を付けるかどうか
        FALSE,        // 取り消し線を付けるかどうか
        DEFAULT_CHARSET,          // 文字セットの識別子
        OUT_CHARACTER_PRECIS,      // 出力精度
        CLIP_DEFAULT_PRECIS,    // クリッピング精度
        DEFAULT_QUALITY,          // 出力品質
        FIXED_PITCH,   // ピッチとファミリ
        NULL            // フォント名
    );

        HFONT hBeforFont = (HFONT)::SelectObject(hDc, hFont);


        UINT oldTA;
        oldTA = ::SetTextAlign (hDc, TA_TOP|TA_LEFT);

        SIZE czText;
        GetTextExtentPoint32(hDc, _T("00 ") , 3, &czText);

        POINT ptMouse;
        ::GetCursorPos( &ptMouse );
        ScreenToClient(&ptMouse);

        POINT pt;
        RECT rc = {0};

        int iHeight = 0;
        int iWidth= 0;
        int iSize = 10;

        COLORREF crBk  =   RGB(255,50,50);
        COLORREF crOrg = ::GetBkColor(hDc);
        for(int iY = -iSize;  iY < iSize; iY++)
        {
            iWidth= 0;
            int iIndex;
            for(int iX = -iSize;  iX < iSize; iX++)
            {
                StdStringStream strmLine;
                pt.x = ptMouse.x + iX;
                pt.y = ptMouse.y + iY;
                iIndex = m_pView->GetIndex(pt);

                if (iIndex != 0)
                {
                    ::SetBkColor(hDc, crBk);
                }
                else
                {
                    ::SetBkColor(hDc, crOrg);
                }

                strmLine << StdFormat(_T("%02x ")) % iIndex;
                ::TextOut(hDc, iWidth, iHeight, strmLine.str().c_str(),
                                           SizeToInt(strmLine.str().length()));
                iWidth  += (czText.cx + 1);
            }
            iHeight += (czText.cy + 1);
        }

        (HFONT)::SelectObject(hDc, hBeforFont);
        DeleteObject(hFont);
        ::SetBkColor(hDc, crOrg);
        return;
    }
	else
	{
		return;
	}

	BitBlt(hDc, 0, 0, iWidth, iHeight, hdcSrc, 0, 0, SRCCOPY);
}


BOOL CMockSketchView::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
{
	if(m_pView->OnSetCursor())
	{
		return TRUE;
	}

	return __super::OnSetCursor(pWnd, nHitTest, message);
}


void CMockSketchView::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags)
{
    
    m_pView->OnDebug(nChar);

    __super::OnKeyUp(nChar, nRepCnt, nFlags);
}


