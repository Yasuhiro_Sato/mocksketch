
/**
 * @brief			CMockSketchViewヘッダーファイル
 * @file			CMockSketchView.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#pragma once

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "View/VIEW_MODE.h"
#include "View/DRAWING_MODE.h"
#include "Utility/DropUtil.h"
#include "Utility/DropTarget.h"
#include "SubWindow/RichDlg.h"
#include "DefinitionObject/View/ObjectDefView.h"



/*---------------------------------------------------*/
/*  classes                                          */
/*---------------------------------------------------*/
class CDrawingView;
class CNumInputDlgbar;
class CMockSketchDoc;
class CDrawingText;

class CMockSketchView : public CView, public CObjectDefView
{
	DECLARE_DYNCREATE(CMockSketchView)

protected: // シリアル化からのみ作成します。
    //-------------------------
	std::shared_ptr<CDrawingView> m_pView;

    //!< 数値入力用ダイアログ
	std::shared_ptr<CNumInputDlgbar> m_pDlgbarNumInput;

    bool m_bInit;
    HDC  m_hDc;

    //!< 文字入力用リッチエディット
    CRichEditCtrl   m_Rich;     //テスト用

    boost::mutex   m_mtxGuard;

    //!< ドロップターゲット
    CDropTarget m_DropTarget;

    //!< ダブルクリック後マウスUP阻止
    bool  m_bDblClick;


    //!< ツールチップ
    mutable CToolTipCtrl m_toolTip;

    //!< ツールチップ表示フラグ
    mutable bool m_bPopTooltip;


    CPoint ptOldMoucePos;
    UINT   nOldMouseFlags;

    int    m_iViewPosition;
    bool   m_bViewSet;

    int   m_iTextAlign;
    int   m_iTextDatum;

    //-------------------------
protected: // シリアル化からのみ作成します。
	CMockSketchView();


public:
    CMockSketchDoc* GetDocument() const;

    //!< Viewデータ変更
    void SetView();

    //!< 描画インスタンス設定
    //virtual bool SetDrawiingScript(CDrawingScriptBase* pDrawingScript);

    //--------------------------------
    void SetComment(StdString strComment);

    void ClearInput();
    
    void EnableInput(bool bEnable);

    void SetDlgBarMode  (VIEW_MODE eMode);
    
    VIEW_MODE GetViewMode  ();

    void SetInputFocus();

    //!< 入力値取得
    StdString GetInputValue();

    //!< 入力値更新
    void UpdateInput(bool bOk);

    //!< リッチエディット初期化
    void InitRichEdit();

    //!< リッチエディット取得
    CRichEditCtrl* GetRichEdit();

    bool CalcRichEditSize(SIZE* pSize);


    int GetTextAlign();
    int GetTextDatum();



    //!< 表示更新
    virtual bool UpdateView();

	virtual void OnDraw(CDC* pDC);  // このビューを描画するためにオーバーライドされます。

    void DrawTo(HDC hDc, DRAWING_MODE eMode, int iWidth, int iHeight);

    //!< ツールチップ設定
    void SetToolTipText(LPCTSTR strText);
    void ResetToolTipText();

	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
    
protected:
    void   Init();
    bool   SetViewMode    (VIEW_MODE eMode);

	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

    void AdjustLayout();
    std::shared_ptr<CDrawingText> _GetSelectedText();


// 実装
public:
	virtual ~CMockSketchView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
    
    //!<  アクティブ・非アクティブ切替
    virtual void OnActivateFrame( UINT nState,  CFrameWnd* pFrameWnd);


// 生成された、メッセージ割り当て関数
protected:
	afx_msg void OnFilePrintPreview();
    afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	DECLARE_MESSAGE_MAP()
public:
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
    afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
    afx_msg void OnMouseMove(UINT nFlags, CPoint point);
    afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
    afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);


    afx_msg void OnMnuPoint    ();
    afx_msg void OnMnuLine     ();
    afx_msg void OnMnuCircle   ();
    afx_msg void OnMnuArc      ();
    afx_msg void OnMnuOffset   ();
    afx_msg void OnMnuCorner   ();
    afx_msg void OnMnuChamfer  ();
    afx_msg void OnMnuTrim     ();
    afx_msg void OnMnuMove     ();
    afx_msg void OnMnuStretchMode();
    afx_msg void OnMnuCopy     ();
    afx_msg void OnMnuRotate   ();
    afx_msg void OnMnuScl      ();
    afx_msg void OnMnuMirror   ();
    afx_msg void OnMnuSelect   (); 
    afx_msg void OnMnuDelete   ();
    afx_msg void OnMnuDevide   ();
    afx_msg void OnMnuCutMode  ();
    afx_msg void OnMnuCopyMode ();

    afx_msg void OnMnu3PCircle ();
    afx_msg void OnMnuEllipse  ();
    afx_msg void OnMnuSpline   ();
    afx_msg void OnMnuCompositionLine();
    afx_msg void OnMnuConnectionLine();
    afx_msg void OnMnuFill     ();
    afx_msg void OnMnuGroup    ();
    afx_msg void OnMnuParts    ();
    afx_msg void OnMnuReference();

    afx_msg void OnMnuDisassembly ();
    afx_msg void OnMnuDrawText ();
    afx_msg void OnMnuNode     ();
    afx_msg void OnMnuImage    ();

    afx_msg void OnMnuConvertSpline();


    afx_msg void OnMnuCancel   ();
    afx_msg void OnMnuEnd      ();
    afx_msg void OnMnuImageCtrl      ();
    

    afx_msg void OnUpdateMnuPoint    (CCmdUI *pCmdUI);
    afx_msg void OnUpdateMnuLine     (CCmdUI *pCmdUI);
    afx_msg void OnUpdateMnuCircle   (CCmdUI *pCmdUI);
    afx_msg void OnUpdateMnuArc      (CCmdUI *pCmdUI);
    afx_msg void OnUpdateMnuOffset   (CCmdUI *pCmdUI);
    afx_msg void OnUpdateMnuCorner   (CCmdUI *pCmdUI);
    afx_msg void OnUpdateMnuChamfer  (CCmdUI *pCmdUI);
    afx_msg void OnUpdateMnuTrim     (CCmdUI *pCmdUI);
    afx_msg void OnUpdateMnuMove     (CCmdUI *pCmdUI);
    afx_msg void OnUpdateMnuStretchMode (CCmdUI *pCmdUI);
    afx_msg void OnUpdateMnuCopy     (CCmdUI *pCmdUI);
    afx_msg void OnUpdateMnuRotate   (CCmdUI *pCmdUI);
    afx_msg void OnUpdateMnuScl      (CCmdUI *pCmdUI);
    afx_msg void OnUpdateMnuMirror   (CCmdUI *pCmdUI);
    afx_msg void OnUpdateMnuSelect   (CCmdUI *pCmdUI);   
    afx_msg void OnUpdateMnuDelete   (CCmdUI *pCmdUI);
    afx_msg void OnUpdateMnuDevide   (CCmdUI *pCmdUI);
    afx_msg void OnUpdateMnuNode     (CCmdUI *pCmdUI);
    afx_msg void OnUpdateMnuImage    (CCmdUI *pCmdUI);
    afx_msg void OnUpdateMnuCutMode  (CCmdUI *pCmdUI);
    afx_msg void OnUpdateMnuCopyMode (CCmdUI *pCmdUI);


    afx_msg void OnUpdateMnu3PCircle (CCmdUI *pCmdUI);
    afx_msg void OnUpdateMnuEllipse  (CCmdUI *pCmdUI);
    afx_msg void OnUpdateMnuSpline   (CCmdUI *pCmdUI);
    afx_msg void OnUpdateMnuCompositionLine(CCmdUI *pCmdUI);
    afx_msg void OnUpdateMnuConnectionLine(CCmdUI *pCmdUI);
    afx_msg void OnUpdateMnuFill     (CCmdUI *pCmdUI);
    afx_msg void OnUpdateMnuGroup       (CCmdUI *pCmdUI);
    afx_msg void OnUpdateMnuParts       (CCmdUI *pCmdUI);
    afx_msg void OnUpdateMnuReference   (CCmdUI *pCmdUI);

    afx_msg void OnUpdateMnuDisassembly (CCmdUI *pCmdUI);
    afx_msg void OnUpdateMnuConvertSpline(CCmdUI *pCmdUI);
    afx_msg void OnUpdateMnuDrawText (CCmdUI *pCmdUI);
    afx_msg void OnUpdateMnuImageCtrl(CCmdUI *pCmdUI);

    
    /*
    afx_msg void OnUpdateMnuDim      (CCmdUI *pCmdUI);
    afx_msg void OnUpdateMnuHDim     (CCmdUI *pCmdUI);
    afx_msg void OnUpdateMnuVDim     (CCmdUI *pCmdUI);
    afx_msg void OnUpdateMnuDDim     (CCmdUI *pCmdUI);
    afx_msg void OnUpdateMnuADim     (CCmdUI *pCmdUI);
    afx_msg void OnUpdateMnuRDim     (CCmdUI *pCmdUI);
    afx_msg void OnUpdateMnuCDim     (CCmdUI *pCmdUI);
    afx_msg void OnMnuDim      ();
    afx_msg void OnMnuHDim     ();
    afx_msg void OnMnuVDim     ();
    afx_msg void OnMnuDDim     ();
    afx_msg void OnMnuADim     ();
    afx_msg void OnMnuCDim     ();
    afx_msg void OnMnuRDim     ();
    */

	afx_msg void OnLineColor();
	afx_msg void OnBrushColor();
	afx_msg void OnLineStyleSet ();
	afx_msg void OnLineWidthSet ();
	afx_msg void OnLineStyle (UINT id);
	afx_msg void OnLineWidth (UINT id);
	afx_msg void OnDimType   (UINT id);

	afx_msg void OnUpdateLineStyle (CCmdUI* pCmdUI);
	afx_msg void OnUpdateLineWidth (CCmdUI* pCmdUI);
	afx_msg void OnUpdateDimType   (CCmdUI* pCmdUI);

    //!< リッチエディット
    afx_msg void OnEnSelchangeRichedit(NMHDR *pNMHDR, LRESULT *pResult);

	//afx_msg void OnDspIoDlg();
    //afx_msg void OnUpdateDspIoDlg(CCmdUI *pCmdUI);


    //--------------------------
    //!< フォーマットツールバー
    //--------------------------
    afx_msg void OnCharColor();
	afx_msg void OnFontname();
	afx_msg void OnFontsize();
	afx_msg void OnCharBold();
	afx_msg void OnCharItalic();
	afx_msg void OnCharUnderline();
	afx_msg void OnCharStrikeOut();
	afx_msg void OnCharLower();
	afx_msg void OnCharUpper();
   
    afx_msg void OnUpdateCharBold(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCharItalic(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCharUnderline(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCharStrikeOut(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCharUpper(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCharLower(CCmdUI* pCmdUI);
	afx_msg void OnCharAlign (UINT id);
	afx_msg void OnUpdateCharAlign(CCmdUI* pCmdUI);
    afx_msg void OnCharDatum (UINT id);
    afx_msg void OnUpdateCharDatum(CCmdUI* pCmdUI);

    void OnUpdateFomatToolBar(CCmdUI* pCmdUI, int iMask, int iEffect);
    void OnFomatToolBar(int iMask, int iEffect);
    //--------------------------


    //!< 入力
    afx_msg LRESULT OnValInput(WPARAM wParam, LPARAM lParam);

    //!<名前更新
    afx_msg LRESULT OnUpdateName(WPARAM pMessage, LPARAM lParam);

    //!<ビュークリア
    afx_msg LRESULT OnViewClear(WPARAM pMessage, LPARAM lParam);

    //!<ドロップ開始
    afx_msg  LRESULT OnDropEnter(WPARAM type, LPARAM lParam);

    //!<ドロップ完了
    afx_msg LRESULT OnDropEnd(WPARAM type, LPARAM lParam);

    //!< ドロップ通過
    afx_msg LRESULT OnDropOver(WPARAM pMessage, LPARAM lParam);

    //!< ドロップ通過完了
    afx_msg LRESULT OnDropLeave(WPARAM pMessage, LPARAM lParam);

    //!<プリンタ更新通知
    afx_msg LRESULT OnUpdatePrinter(WPARAM wParam, LPARAM lParam);

    //!<カレントレイヤ変更
    afx_msg LRESULT OnLayerChange    (WPARAM wParam, LPARAM lParam);

    //!<順序
 	afx_msg void OnMnuOrder (UINT id);
	afx_msg void OnUpdateMnuOrder(CCmdUI* pCmdUI);

    //!<スナップ
 	afx_msg void OnMnuSnap (UINT id);
	afx_msg void OnUpdateMnuSnap(CCmdUI* pCmdUI);
    DWORD MenuIdToSnap (UINT id, bool bRedraw);


    //!<表示位置
 	afx_msg void OnMnuShowPos (UINT id);
	afx_msg void OnUpdateMnuShowPos(CCmdUI* pCmdUI);

	afx_msg void OnMnuShowPosSet();
	afx_msg void OnUpdateMnuShowPosSet(CCmdUI* pCmdUI);
  	afx_msg void OnShowPos();



    CMFCToolBarComboBoxButton* GetToolBarComboBox(UINT uiCmd) const;
    CMFCToolBarButton* GetToolBarButton(UINT uiCmd) const;



protected:
    bool _IsSelect() const;

    virtual void OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView);
public:
    afx_msg void OnClose();
    afx_msg void OnDestroy();
    afx_msg void OnMnuRedraw();
    afx_msg void OnUpdateMnuRedraw(CCmdUI *pCmdUI);

    afx_msg void OnMnuViewAll();
    afx_msg void OnUpdateMnuViewAll(CCmdUI *pCmdUI);

    afx_msg void OnMButtonUp(UINT nFlags, CPoint point);

    afx_msg void OnEditUndo();
    afx_msg void OnEditRedo();
    afx_msg void OnUpdateEditRedo(CCmdUI *pCmdUI);
    afx_msg void OnUpdateEditUndo(CCmdUI *pCmdUI);
    virtual BOOL PreTranslateMessage(MSG* pMsg);
    //!<モード変更
    afx_msg LRESULT OnModeChange(WPARAM wParam, LPARAM lParam);

    virtual void OnInitialUpdate();
    afx_msg void OnMenuSelect(UINT nItemID, UINT nFlags, HMENU hSysMenu);
    virtual void OnPrepareDC(CDC* pDC, CPrintInfo* pInfo = NULL);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
    afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
    virtual void OnEndPrintPreview(CDC* pDC, CPrintInfo* pInfo, POINT point, CPreviewView* pView);
    afx_msg LRESULT OnImeChar(WPARAM iType, LPARAM pMessage);
    afx_msg LRESULT OnImeComposition(WPARAM iType, LPARAM pMessage);
    afx_msg void OnEnChangeRichedit();
};

#ifndef _DEBUG  // MockSketchView.cpp のデバッグ バージョン
inline CMockSketchDoc* CMockSketchView::GetDocument() const
   { return reinterpret_cast<CMockSketchDoc*>(m_pDocument); }
#endif

