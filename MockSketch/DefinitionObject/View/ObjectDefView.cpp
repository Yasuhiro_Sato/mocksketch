/**
 * @brief			ObjectDefView実装ファイル
 * @file			CDrawingView.cpp
 * @author			Yasuhiro Sato
 * @date			01-2-2009 10:36:26
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MockSketch.h"
#include "ObjectDefView.h"
#include "DefinitionObject/View/MockSketchDoc.h"
//#include "DefinitionObject/ObjectDef.h"
//#include "ExecCommon.h"


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/

/**
 * @brief   コンストラクタ
 * @param   なし         
 * @retval  なし
 * @note	
 */
CObjectDefView::CObjectDefView():
m_pWnd          (NULL)
{
    THIS_APP->AddView(this);
}

/**
 * @brief   デストラクタ
 * @param   なし         
 * @retval  なし
 * @note	
 */
CObjectDefView::~CObjectDefView()
{
    THIS_APP->DelView(this);
}

/**
 * @brief   インスタンス定義設定
 * @param   [in] pDrawingScript  描画インスタンス
 * @return  true 設定成功
 * @note
 */
CMockSketchDoc*  CObjectDefView::_GetDoc() const
{
    CMockSketchDoc* pMockDoc = NULL;
    if (!m_pWnd)
    {
        return pMockDoc;
    }
    CView* pView = dynamic_cast<CView*>(m_pWnd);

    if (!pView)
    {
        return pMockDoc;
    }

    CDocument* pDoc = pView->GetDocument();

    pMockDoc = dynamic_cast<CMockSketchDoc*>(pDoc);

    return pMockDoc;
}

/**
 * @brief   オブジェクト定義問い合わせ
 * @param   なし
 * @return  true 
 * @note
 */
inline
bool CObjectDefView::IsObjectDef() const
{
    using namespace VIEW_COMMON;
    CMockSketchDoc* pMockDoc = _GetDoc();
    if (pMockDoc)
    {
        if (E_DOC_DEF == pMockDoc->GetDocType())
        {
            return true;
        }
    }
    return false;
}

/**
 * @brief   描画インスタンス問い合わせ
 * @param   なし
 * @return  true 
 * @note
 */
inline
bool CObjectDefView::IsDrawiingScript() const
{
    using namespace VIEW_COMMON;
    CMockSketchDoc* pMockDoc = _GetDoc();
    if (pMockDoc)
    {
        if (E_DOC_DRAWING == pMockDoc->GetDocType())
        {
            return true;
        }
    }
    return false;
}

