/**
 * @brief			CObjectDefViewヘッダーファイル
 * @file			CObjectDefView.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#pragma once


/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/

/*---------------------------------------------------*/
/*  classes                                          */
/*---------------------------------------------------*/
class CMockSketchDoc;

class CObjectDefView
{

protected:
    //!< コンストラクタ
    CObjectDefView();

    //!< デストラクタ
    virtual ~CObjectDefView();


public:
    CWnd* GetWindow(){ return m_pWnd;}

    void SetWindow(CWnd* pWnd){ m_pWnd = pWnd;}

    //!< 表示更新
    virtual bool UpdateView(){return false;}

    //!< オブジェクト定義問い合わせ
    virtual bool IsObjectDef() const;

    //!< 描画インスタンス問い合わせ
    virtual bool IsDrawiingScript() const;

protected:

    CMockSketchDoc*  _GetDoc() const;

    //!< 描画ウインドウ
    CWnd* m_pWnd;

};


