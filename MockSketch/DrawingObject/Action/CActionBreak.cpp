/**
 * @brief			CActionBreak実装ファイル
 * @file			CActionBreak.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CActionBreak.h"
#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/View/CDrawingView.h"
#include "DrawingObject/CDrawingPoint.h"
#include "DrawingObject/CDrawingObject.h"
#include "DrawingObject/CDrawingLine.h"
#include "DrawingObject/CDrawingCircle.h"
#include "View/CLayer.h"
#include "View/CDrawMarker.h"
#include "View/CUndoAction.h"
#include "Utility/CUtility.h"
#include "System/CSystem.h"
#include <math.h>

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 * コンストラクタ
 */
CActionBreak::CActionBreak():
CViewAction()
{
    m_lMouseOver =  SEL_PRI_NOPT;
}

/**
 * コンストラクタ
 */
CActionBreak::CActionBreak(CDrawingView* pView):
CViewAction(pView)
{
    m_lMouseOver =  SEL_PRI_NOPT;
}
    
/**
 * デストラクタ
 */
CActionBreak::~CActionBreak()
{
    
}

/**
 *  @brief   解放時動作
 *  @param   [in]    eMode 解放時点でのモード
 *  @retval   なし
 *  @note
 */
void CActionBreak::Cancel(VIEW_MODE eMode) 
{
    //選択解除
    ClearAction();
}

/**
 *  @brief   選択時動作
 *  @param   [in]    eMode 選択時点でのモード
 *  @retval   なし
 *  @note
 */
bool CActionBreak::SelAction(VIEW_MODE eMode, void* pParam) 
{
    //既に選択中の図形はあるか？
    CPartsDef* pDef = m_pView->GetPartsDef();
    if (pDef)
    {
        long lSize = pDef->SelectNum();

        if (lSize != 0)
        {
            //選択を解除
            pDef->RelSelect();
        }   
    }

    m_pView->SetComment(GET_SYS_STR(STR_SEL_BREAK));
    m_pFirstObj = NULL;
    m_eViewMode = eMode;
    return true;    
}

/**
 *  @brief   マウスクリック動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionBreak::SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    POINT ptSel = posMouse.ptSel;
    UINT nFlags = posMouse.nFlags;

    CPartsDef*   pDef  = m_pView->GetPartsDef();
    int                  iId  = m_pView->SearchPos(ptSel);
    if (!pDef)
    {
        return false;
    }

    auto     pObj  = pDef->GetObjectById(iId);

    if (eAction != CViewAction::ACT_LBUTTON_UP)
    {
        //左クリックのみ
        return false;
    }

    //!< ビュー->データ変換
    POINT2D pt2D;
    int iLayerId = m_pView->GetCurrentLayerId();
    m_pView->ConvScr2World(&pt2D, ptSel, iLayerId);


    if ( !m_pFirstObj )
    {
        if (!pObj)
        {
            return false;
        }

        //何かをクリック
        m_pFirstObj = pObj;



        //選択状態にする
        pDef->SelectDrawingObject(iId);
        pObj->Draw(m_pView, NULL);

        //点も選択
        m_lMouseOver =   SEL_FIG;
        //分割位置を選択してください
        m_pView->SetComment(GET_SYS_STR(STR_SEL_BREAK_OBJ));
        return false;
    }

    //-----------------
    // ２回目クリック
    //-----------------
    std::vector<POINT2D> lstPoint;
    DRAWING_TYPE eSecondType;
    int                 iSelID;
    if ((pObj == NULL) || 
        (m_pFirstObj == pObj ))
    {
        auto pDrawPoint = std::make_shared<CDrawingPoint>();
        eSecondType = DT_NONE;
        pDrawPoint->GetPointInstance()->dX = pt2D.dX;
        pDrawPoint->GetPointInstance()->dY = pt2D.dY;
        pObj = pDrawPoint;
    }
    else
    {
        eSecondType = pObj->GetType();
    }

    CWaitCursor wait;
    //交点計算
    m_pFirstObj->CalcIntersection(&lstPoint, pObj.get(), true);
    iSelID = m_pFirstObj->GetId();

    size_t lSize = lstPoint.size();
    if (lSize == 0)
    {
        //分割できない位置を指定しています
        AfxMessageBox(GET_STR(STR_ERROR_BREAK));
        ClearAction();
        return false;
    }
    else if (lSize == 1)
    {
        //交点を設定して終わり
        pDef->BreakPoint( iSelID,  lstPoint[0] );
        ClearAction();
        pDef->Redraw();
        return true;
    }
    else
    {
        //クリック位置に近い交点を分割点とする
        POINT2D ptNear;
        if (!pt2D.NearPoint_P(&ptNear, lstPoint))
        {
            AfxMessageBox(GET_STR(STR_ERROR_BREAK));
            ClearAction();
            pDef->Redraw();
            return false;
        }

        pDef->BreakPoint( iSelID,  ptNear, true);
        ClearAction();
        pDef->Redraw();
        return true;
    }
    return false;
}


/**
 *  @brief   マウス移動動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionBreak::MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    POINT ptSel = posMouse.ptSel;
    UINT nFlags = posMouse.nFlags;

    //!< スナップ点取得
    bool bIgnoreSnap = false;

    if (posMouse.nFlags & MK_SHIFT)
    {
        bIgnoreSnap = true;
    }

    CPartsDef*   pDef  = m_pView->GetPartsDef();
    CDrawMarker* pMarker= m_pView->GetMarker();
    bool bOnLine = false;
    bool bTrimClickSide = true; //最初にクリックした側を調整する

    pMarker->Clear();
    pMarker->ClearObject();
    pDef->ClearMouseOver();
    pDef->ClearMouseEmphasis();
    pDef->ClearMouseConnectable();

    using namespace VIEW_COMMON;

    SNAP_DATA snap;
    m_pView->GetMousePoint(&snap, posMouse);
    int iLayerId = m_pView->GetCurrentLayerId();

    //bool bOnLine = false;
    bool bReverse = false;

    auto pObj1 = pDef->GetObjectById(snap.iObject1);
    auto pObj2 = pDef->GetObjectById(snap.iObject2);

    
    if (!m_pFirstObj)
    {
        //点以外の基本図形をマウスオーバ
        bool bStart = false;
        bStart = IsActionType(SEL_PRI_NOPT, pObj1.get());
        if (bStart)
        {
            pObj1->DrawOverStart(m_pView);
            pDef->SetMouseOver(pObj1);
            pDef->DrawDragging(m_pView);
        }
        return false;
    }

 
    CDrawMarker::MARKER_DATA marker;
    if ((pObj1 != NULL) && (pObj2 != NULL))
    {
        //交点
        pDef->SetMouseEmphasis(pObj1);
        pDef->SetMouseEmphasis(pObj2);

        marker.eType = CDrawMarker::MARKER_CROSS;
        marker.eSnap = VIEW_COMMON::SNP_CROSS_POINT;
        pMarker->AddMarker(marker, snap.pt);

        auto  pt = std::make_shared<CDrawingPoint>();
        pt->SetPoint(snap.pt);
        pObj1 = pt;
    }

    else if ((pObj1 != NULL ) &&
             (pObj2 == NULL ))
    {
        pDef->SetMouseEmphasis(pObj1);
    }


    //１点目未選択
    if (snap.eSnapType == VIEW_COMMON::SNP_NONE)
    {
        //任意点
        marker.eType = CDrawMarker::MARKER_CROSS;
        marker.eSnap = VIEW_COMMON::SNP_CROSS_POINT;
        pMarker->AddMarker(marker, snap.ptOrg);

        auto  pt = std::make_shared<CDrawingPoint>();
        pt->SetPoint(snap.ptOrg);
        pObj1 = pt;
    }


    //交点計算
    std::vector<POINT2D> lstPoint;
    m_pFirstObj->CalcIntersection(&lstPoint, pObj1.get(), true);

    m_psAdditionalLine.reset();
    if (pObj1->GetType() == DT_POINT)
    {
        //補助線を作成
        m_psAdditionalLine = std::make_shared<CDrawingLine>();
        m_psAdditionalLine->SetColor(DRAW_CONFIG->crAdditional);
        m_psAdditionalLine->SetLineType(PS_DOT);
        m_psAdditionalLine->SetLayer(iLayerId);

        auto  pt = std::dynamic_pointer_cast<CDrawingPoint>(pObj1);
        if(pt)
        {
            m_psAdditionalLine->SetPoint1(pt->GetPoint());
        }
    }

    auto pTmpList = m_pView->GetDraggingTmpObjects();
    pTmpList->clear();
    auto pTmpObj = CDrawingObject::CloneShared(m_pFirstObj.get(), false);
    pTmpObj->SetColor(DRAW_CONFIG->crImaginary);
    pTmpList->push_back(pTmpObj);

    POINT2D ptBreak;
    size_t lSize = lstPoint.size();
    if (lSize == 0)
    {
        //分割できない
        pDef->DrawDragging(m_pView);
        return false;
    }
    else if (lSize == 1)
    {
        ptBreak = lstPoint[0];
    }
    else
    {
        //クリック位置に近い交点を分割点とする
        if (!snap.ptOrg.NearPoint_P(&ptBreak, lstPoint))
        {
            pDef->DrawDragging(m_pView);
            return false;
        }
    }

    if(m_psAdditionalLine)
    {
        m_psAdditionalLine->SetPoint2(ptBreak);
        pMarker->AddObject(m_psAdditionalLine);
    }

    auto pBreakObject = pTmpList->at(0)->Break(ptBreak);
    pBreakObject->SetColor(DRAW_CONFIG->crEmphasis);
    pTmpList->push_back(pBreakObject);

    pDef->SetMouseOver(pTmpList->at(0));
    pDef->SetMouseOver(pTmpList->at(1));
    pMarker->AddObject(pTmpList->at(0));
    pMarker->AddObject(pTmpList->at(1));

    //pDef->SetMouseEmphasis(pTmpList->at(0));
    //pDef->SetMouseEmphasis(pTmpList->at(1));

    pDef->DrawDragging(m_pView);
    return true;
}

/**
 *  動作クリア
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CActionBreak::ClearAction()
{
    m_pFirstObj = NULL;

    CPartsDef*    pDef   = m_pView->GetPartsDef();

    if (pDef)
    {
        pDef->RelSelect();
    }
    m_lMouseOver =   SEL_PRI_NOPT;
}
