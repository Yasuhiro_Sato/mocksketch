/**
 * @brief			CActionBreakヘッダーファイル
 * @file			CActionBreak.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef _ACTION_BREAK_H__
#define _ACTION_BREAK_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CViewAction.h"
class     CDrawingView;
class     POINT2D;

/**
 * @class   CActionBreak
 * @brief                        
 */
class CActionBreak: public CViewAction
{
public:
	//!< コンストラクタ
    CActionBreak();

    //!< コンストラクタ
    CActionBreak(CDrawingView* pView);
    
	//!< デストラクタ
    virtual ~CActionBreak();

	//!< 選択解放時動作
    virtual void Cancel(VIEW_MODE eMode);

	//!< 選択解放時動作
	virtual bool SelAction(VIEW_MODE eMode, void* pParam = NULL);

	//!< マウスクリック動作
	virtual bool SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

	//!< マウス移動動作
    virtual bool MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

protected:

    //!< 動作クリア
    void ClearAction();


protected:

    std::shared_ptr<CDrawingObject> m_pFirstObj;

    std::shared_ptr<CDrawingLine> m_psAdditionalLine;
    

};
#endif // _ACTION_BREAK_H__
