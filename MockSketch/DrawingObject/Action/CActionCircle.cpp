/**
 * @brief			CViewAction実装ファイル
 * @file			CViewAction.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CActionCircle.h"

#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/View/CDrawingView.h"

#include "DrawingObject/CDrawingObject.h"
#include "DrawingObject/CDrawingPoint.h"
#include "DrawingObject/CDrawingLine.h"
#include "DrawingObject/CDrawingCircle.h"
#include "DrawingObject/CDrawingSpline.h"
#include "DrawingObject/CDrawingEllipse.h"
#include "DrawingObject/CDrawingCompositionLine.h"
#include "DrawingObject/Primitive/HYPERBOLA2D.h"
#include "DrawingObject/Primitive/PARABOLA2D.h"

#include "View/CLayer.h"
#include "View/CDrawMarker.h"
#include "View/CUndoAction.h"
#include "Utility/CUtility.h"
#include "System/CSystem.h"
#include "MockSketch.h"
#include <math.h>

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define DIR_CIRCLE_TOLERANCE_MAX 1e-7
//#define _DEBUG_LLL

//!< コンストラクタ
CActionCircle::CActionCircle():
CActionCircle(NULL)
{
}

//!< コンストラクタ
CActionCircle::CActionCircle(CDrawingView* pView):
CViewAction(pView)
{
    m_lMouseOver        = SEL_PRI;
    for (auto p : m_pSelObj)
    {
        p.reset();
    }
    m_psVvHyp = std::make_unique<VIEW_COMMON::VIEW_VALUE>();
}

/**
 *  解放時動作
 *  @param   [in]    eMode 解放時点でのモード
 *  @retval   なし
 *  @note
 */
void CActionCircle::Cancel(VIEW_MODE eMode)
{
    _ClearAction();
}

/**
 *  選択時動作
 *  @param   [in]    eMode 選択時点でのモード
 *  @retval   なし
 *  @note
 */
bool CActionCircle::SelAction(VIEW_MODE eMode, void* pParam)
{
    //既に選択中の図形はあるか？

    CPartsDef*    pDef   = m_pView->GetPartsDef();
    if (!pDef)
    {
        return false;
    }

    long lSize = pDef->SelectNum();

    if (lSize != 0)
    {
        //選択を解除
        pDef->RelSelect();
    }   

    if (eMode == VIEW_CIRCLE   )
    {
        m_pView->SetComment(GET_SYS_STR(STR_SEL_CIRCLE_CENTER));
    }
    else if (eMode == VIEW_CIRCLE_3P)
    {
        m_pView->SetComment(GET_SYS_STR(STR_SEL_3P_1));
    }
    m_eViewMode = eMode;
    return true;    

}

/**
 *  マウスクリック動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval   なし
 *  @note
 */
bool CActionCircle::SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    CPartsDef*   pDef = m_pView->GetPartsDef();
    if (!pDef)
    {
        return false;
    }

    int      iId  = m_pView->SearchPos(posMouse.ptSel);
    auto     pObj  = pDef->GetObjectById(iId);
    VIEW_MODE      eViewMode  = m_pView->GetViewMode();

    POINT ptSel = posMouse.ptSel;
    UINT nFlags = posMouse.nFlags;

    if (eAction != CViewAction::ACT_LBUTTON_UP)
    {
        //左クリックのみ
        return false;
    }

    //!< ビュー->データ変換
    POINT2D pt2D;
    int iLayerId = m_pView->GetCurrentLayerId();
    m_pView->ConvScr2World(&pt2D, ptSel, iLayerId);

    //!< マーカークリック確認
    POINT2D ptMarker;
    bool    bMarker = false;
    if(_CheckMarkerObject( &ptMarker, iId, ptSel,  eAction))
    {
        pt2D = ptMarker;
        bMarker = true;
    }
    else
    {
       if (pObj)
        {
            if(pObj->GetType() == DT_POINT)
            {
                auto pt = std::dynamic_pointer_cast<CDrawingPoint>(pObj);
                pt2D = pt->GetPoint();
                bMarker = true;
                pObj = NULL;
            }
        }
    }

    if      (eViewMode == VIEW_CIRCLE   ){return _SelCircle  (posMouse, eAction, pt2D, pObj, bMarker);}
    else if (eViewMode == VIEW_CIRCLE_3P){return _SelCircle3P(posMouse, eAction, pt2D, pObj, bMarker);}
    else
    {
        STD_ASSERT(eViewMode);
    }

    return true;
}

/**
 *  @brief   マウス移動動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionCircle::MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
	bool bRet = false;

    VIEW_MODE      eViewMode  = m_pView->GetViewMode();

    if(eViewMode == VIEW_CIRCLE_3P)
    {
        return _MovePoint3P( posMouse,  eAction);
    }


    if (m_iClickCnt == 0)
    {
        bRet = _MovePoint1( posMouse,  eAction);
    }
    else if(m_iClickCnt == 1)
    {
        bRet = _MovePoint2( posMouse,  eAction);
    }
    else if(m_iClickCnt == 2)
    {
        bRet = _MovePoint3( posMouse,  eAction);
    }
    else if(m_iClickCnt == 3)
    {
        bRet = _MovePoint4( posMouse,  eAction);
    }

    return bRet;
}

bool CActionCircle::_MovePoint3P(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    POINT ptSel = posMouse.ptSel;
    UINT nFlags = posMouse.nFlags;

    //!< スナップ点取得
    bool bSnap;
    bool bIgnoreSnap = false;


    CPartsDef*   pDef = m_pView->GetPartsDef();
    if (!pDef)
    {
        return false;
    }

    pDef->RedrawWait();  //TODO:どうする？


    CDrawMarker* pMarker= m_pView->GetMarker();
    pMarker->Clear();
    pMarker->ClearObject();
    pDef->ClearMouseOver();
    pDef->ClearMouseEmphasis();
    pDef->ClearMouseConnectable();

    DWORD dwSnapType = DRAW_CONFIG->GetSnapType();

    SNAP_DATA snap;
    static const bool bIngnoreGroup = false;
    static const bool bKeepCenter = true;

    bSnap = m_pView->GetSnapPoint(&snap,
        dwSnapType,
        ptSel ,
        bIngnoreGroup,
        bKeepCenter               
    );

    POINT ptOffset;
    ptOffset.x = ptOffset.y = 0;
    CDrawMarker::MARKER_DATA marker;
    marker.ptOffset = ptOffset;

    if (nFlags & MK_SHIFT)
    {
        //スナップ無視
        bIgnoreSnap = true;
        bSnap = false;
    }

    VIEW_MODE      eViewMode  = m_pView->GetViewMode();
    m_pView->SetDrawingMode(DRAW_SEL);


    //==================================
    auto pObj1 = pDef->GetObjectById(snap.iObject1);
    auto pObj2 = pDef->GetObjectById(snap.iObject2);

    marker.eType = CDrawMarker::MARKER_CROSS;
    marker.eSnap = VIEW_COMMON::SNP_CROSS_POINT;

    int iMarkerId = pMarker->MouseMove(posMouse.ptSel);

    if (pObj1 != NULL)
    {
        pDef->SetMouseEmphasis(pObj1);
        //===========================================
        // 円をクリックして中心を選択した場合
        // 確定時(SelPoint)マウスがマーカー上にないため
        // マーカにオブジェクトIDをセットする
        // クリック時は_CheckMarkerObjectを使用する
        //===========================================
        if (snap.eSnapType == VIEW_COMMON::SNP_CENTER)
        {
            marker.bKeepSelect = true;
            marker.iObjct = snap.iObject1;
        }
    }

    if(pObj2 != NULL)
    {
        //交点
        pDef->SetMouseEmphasis(pObj2);
    }
 
    if (m_iClickCnt == 0)
    {
        m_psAddObj.reset();
        StdStringStream   strmLen;
        strmLen << GET_STR(STR_SNAP_3POINT_THROUGH);
        strmLen << _T("(") << GET_STR(STR_SNAP_FIRST_POINT) << _T(")-");
        strmLen << GetSnapName(snap.eSnapType, true)<< _T("\n");
        snap.strSnap = strmLen.str();

    }
    else if(m_iClickCnt == 1)
    {
        m_psAddObj.reset();
        
        auto pLine = CreateLine(&m_psAddtionalObj[0]);
        pLine->SetPoint1(m_ptSel[0]);
        pLine->SetPoint2(snap.pt);
        pMarker->AddObject(pLine);
        pMarker->AddMarker(marker, m_ptSel[0]);

        StdStringStream   strmLen;
        strmLen << GET_STR(STR_SNAP_3POINT_THROUGH);
        strmLen << _T("(") << GET_STR(STR_SNAP_SECOND_POINT) << _T(")-");
        strmLen << GetSnapName(snap.eSnapType, true)<< _T("\n");
        snap.strSnap = strmLen.str();
    }
    else if(m_iClickCnt == 2)
    {
        CIRCLE2D  c;
        if (_CreatePPP(&c, m_ptSel[0],  m_ptSel[1],  snap.pt))
        {
            if (!m_psAddObj)
            {
                auto pCreateObj = THIS_APP->CreateDefaultObject(DT_CIRCLE);
                auto defObj = std::dynamic_pointer_cast<CDrawingCircle>(pCreateObj);
                auto pCircle = std::make_shared<CDrawingCircle>(*(defObj.get()));

                pCircle->SetColor(DRAW_CONFIG->crImaginary);

                if (!THIS_APP->GetDefaultObjectPriorty(DT_CIRCLE))
                {
                    pCircle->SetLineType(m_pView->GetCurrentLayer()->iLineType);
                    pCircle->SetLineWidth(m_pView->GetCurrentLayer()->iLineWidth);
                }


                pCircle->SetLayer(m_pView->GetCurrentLayerId());
                m_psAddObj = pCircle;
            }
        
            auto pDrawCircle = std::dynamic_pointer_cast<CDrawingCircle>(m_psAddObj);
            pDrawCircle->SetCenter(c.GetCenter());
            pDrawCircle->SetRadius(c.GetRadius());


            pDrawCircle->SetVisible(true);
            pDef->SetMouseOver(pDrawCircle);
            pMarker->AddObject(pDrawCircle);

            StdStringStream   strmLen;
            strmLen << GET_STR(STR_SNAP_3POINT_THROUGH) << _T("-");
            strmLen << GetSnapName(snap.eSnapType, true)<< _T("\n");
            strmLen << GET_STR(STR_PRO_RAD) << _T(":")
                << CDrawingObject::GetValText(c.GetRadius(), false, DRAW_CONFIG->iDimLengthPrecision);
            snap.strSnap = strmLen.str();
        }
        pMarker->AddMarker(marker, m_ptSel[0]);
        pMarker->AddMarker(marker, m_ptSel[1]);
    }
    pMarker->AddMarker(marker, snap.pt);

    if (snap.strSnap.empty()) 
    {
        m_pView->ResetToolTipText();
    }
    else
    {
        m_pView->SetToolTipText(snap.strSnap.c_str());
    }
    pDef->DrawDragging(m_pView);
    return true;
}


bool CActionCircle::_MovePoint1(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    POINT ptSel = posMouse.ptSel;
    UINT nFlags = posMouse.nFlags;

    //!< スナップ点取得
    bool bSnap;
    bool bIgnoreSnap = false;


    CPartsDef*   pDef = m_pView->GetPartsDef();
    if (!pDef)
    {
        return false;
    }

    pDef->RedrawWait();  //TODO:どうする？


    if (!m_psAddObj)
    {
        auto pCreateObj = THIS_APP->CreateDefaultObject(DT_CIRCLE);
        auto defObj = std::dynamic_pointer_cast<CDrawingCircle>(pCreateObj);
        auto pCircle = std::make_shared<CDrawingCircle>(*(defObj.get()));

        pCircle->SetColor(DRAW_CONFIG->crImaginary);

        if (!THIS_APP->GetDefaultObjectPriorty(DT_CIRCLE))
        {
            pCircle->SetLineType(m_pView->GetCurrentLayer()->iLineType);
            pCircle->SetLineWidth(m_pView->GetCurrentLayer()->iLineWidth);
        }

        pCircle->SetLayer(m_pView->GetCurrentLayerId());
        m_psAddObj = pCircle;
    }



    CDrawMarker* pMarker= m_pView->GetMarker();
    pMarker->Clear();
    pMarker->ClearObject();
    pDef->ClearMouseOver();
    pDef->ClearMouseEmphasis();
    pDef->ClearMouseConnectable();

    DWORD dwSnapType = DRAW_CONFIG->GetSnapType();


    SNAP_DATA snap;
    static const bool bIngnoreGroup = false;
    static const bool bKeepCenter = true;

    //------------------------------------
    // 円そののものを選択するため、中心点を選択し続ける
    // KeepSnapCenter処理が邪魔になる。そのため 
    // 一時的に円のKeepSnapCenter処理を停止する
    // (円以外は、KeepSnapで問題ない) 
    // TODO:もう少し良いやり方を検討する
    //------------------------------------
    DWORD dwSnapNew =   (dwSnapType & (~VIEW_COMMON::SNP_CIRCLE));
          dwSnapNew =   (dwSnapNew  & (~VIEW_COMMON::SNP_ELLIPSE));
    DRAW_CONFIG->SetSnapType(dwSnapNew); 
    //--------------------

    bool bValInput = false;
    StdString sVal = m_pView->GetInputValue();
    if (sVal != _T(""))
    {
        bool bRet = false;
        if (m_strOldVal != sVal)
        {
            bRet = InputValue(sVal);
            m_pView->UpdateInput(bRet);
            if (bRet)
            {
                m_strOldVal = sVal;
            }
        }
        bValInput = true;
    }
    else
    {
        if (m_strOldVal != _T(""))
        {

        }
    }

    bSnap = m_pView->GetSnapPoint(&snap,
                        dwSnapNew,
                        ptSel ,
                        bIngnoreGroup,
                        bKeepCenter               
    );


    auto pObj1 = pDef->GetObjectById(snap.iObject1);
    auto pObj2 = pDef->GetObjectById(snap.iObject2);
 

    POINT ptOffset;
    ptOffset.x = ptOffset.y = 0;
    CDrawMarker::MARKER_DATA marker;
    marker.ptOffset = ptOffset;

    if (nFlags & MK_SHIFT)
    {
        //スナップ無視
        bIgnoreSnap = true;
        bSnap = false;
    }

    VIEW_MODE      eViewMode  = m_pView->GetViewMode();
    m_pView->SetDrawingMode(DRAW_SEL);

    //==================================

    if ((pObj1 != NULL) &&
        (pObj2 != NULL))
    {
        //交点
        pDef->SetMouseEmphasis(pObj1);
        pDef->SetMouseEmphasis(pObj2);
        marker.eType = CDrawMarker::MARKER_CROSS;
        marker.eSnap = VIEW_COMMON::SNP_CROSS_POINT;
        pMarker->AddMarker(marker, snap.pt);
    }

    if ((pObj1 != NULL ) &&
        (pObj2 == NULL ))
    {
        m_pView->SetMarker(pObj1.get(), false /*bIgnoreOffLineObject*/);
        pDef->SetMouseEmphasis(pObj1);
    }

    //==================================
    // 円のKeepSnapCenter処理を戻す
    // m_pView->SetMarker内でもGetSnapListを使用するため
    // ここで解除する
    DRAW_CONFIG->SetSnapType(dwSnapType);
    //==================================

    int iMarkerId = pMarker->MouseMove(posMouse.ptSel);
    if ((pObj1 != NULL ) &&
        (pObj2 == NULL ))
    {
        bool bSelectable = false;
        if ((iMarkerId == -1))
        {
            //オブジェクト上にあって、マーカー以外の位置の場合は
            //選択可能なのでSetMouseOverを設定する
            switch(pObj1->GetType())
            {
            case DT_LINE:
                snap.eSnapType = VIEW_COMMON::SNP_LINE;
                bSelectable = true;
                break;

            case DT_CIRCLE:
                snap.eSnapType = VIEW_COMMON::SNP_CIRCLE;
                bSelectable = true;
                break;
            }
        }

        if (bSelectable)
        {
            pDef->ClearMouseEmphasis();
            pDef->SetMouseOver(pObj1);
        }
    }


    //１点目未選択
    if (snap.eSnapType == VIEW_COMMON::SNP_NONE)
    {
        //任意点
        CDrawMarker*pMarker = m_pView->GetMarker();
        marker.eType = CDrawMarker::MARKER_CROSS;
        marker.eSnap = VIEW_COMMON::SNP_CROSS_POINT;
        pMarker->AddMarker(marker, snap.ptOrg);
    }


    if (bValInput)
    {
        auto pDrawCircle = std::dynamic_pointer_cast<CDrawingCircle>(m_psAddObj);
        if (pDrawCircle)
        {
            pDrawCircle->SetVisible(true);
            pDrawCircle->SetCenter(snap.pt);
            pMarker->AddObject(pDrawCircle);
        }
        m_selSts |= SS_CREATE_CIRCLE;
    }
    else
    {
        m_selSts  &= ~SS_CREATE_CIRCLE;
    }


    SetSnapTooltip(snap.eSnapType, 0, 0);
    pDef->DrawDragging(m_pView);
    return true;
}

bool CActionCircle::_MovePoint2AddSnap(std::list<SNAP_DATA>* pSnapList, CDrawingObject*   pObj)
{
    //初回に点を選択した場合、オブジェクト上の接点となる位置にマーカーを表示する
    if (!pObj)
    {
        return false;
    }

    DWORD dwSnapType = DRAW_CONFIG->GetSnapType();

    if (!(dwSnapType & VIEW_COMMON::SNP_TANGENT))
    {
        return false;
    }

    SNAP_DATA snp;

    snp.eSnapType = VIEW_COMMON::SNP_TANGENT;
    snp.bKeepSelect = false;
    snp.iObject1 = pObj->GetId();

    DRAWING_TYPE eType = pObj->GetType();
    

    if (eType == DT_LINE)
    {
        auto pLine = dynamic_cast<CDrawingLine*>(pObj);
        pLine->NearPoint(&snp.pt, m_ptSel[0], false);
        pSnapList->push_back(snp);

    }
    else if (eType == DT_CIRCLE)
    {
        auto pCircle = dynamic_cast<CDrawingCircle*>(pObj);
        auto pC = pCircle->GetCircleInstance();


        snp.pt =  pC->NearPoint(m_ptSel[0]);
        pSnapList->push_back(snp);

        snp.pt =  pC->FarPoint(m_ptSel[0]);
        pSnapList->push_back(snp);
    }
    else if (eType == DT_ELLIPSE)
    {
        auto pEllipse = dynamic_cast<CDrawingEllipse*>(pObj);
        auto pE = pEllipse->GetEllipseInstance();

        std::vector<POINT2D> lst;

        pE->NormPoints(&lst, m_ptSel[0]);
        for (POINT2D pt : lst)
        {
            snp.pt = pt;
            pSnapList->push_back(snp);
        }
    }
    else if (eType == DT_SPLINE)
    {
        auto pSpline = dynamic_cast<CDrawingSpline*>(pObj);
        auto pS = pSpline->GetSplineInstance();

        std::vector<POINT2D> lst;

        pS->NormPoints(&lst, m_ptSel[0]);
        for (POINT2D pt : lst)
        {
            snp.pt = pt;
            pSnapList->push_back(snp);
        }
    }
    else
    {
        return false;
    }

    return true;
}

bool CActionCircle::_MovePoint2_C_C(
    SNAP_DATA* pSnap,
    std::shared_ptr<CDrawingCircle> c1,
    std::shared_ptr<CDrawingCircle> c2)
{
    return _MovePoint3_C_C(
        pSnap->ptOrg,
        false,
        true, //bTmp
        pSnap, c1, c2);
}

bool CActionCircle::_MovePoint2_L_C(
    SNAP_DATA* pSnap,
    std::shared_ptr<CDrawingLine> l,
    std::shared_ptr<CDrawingCircle> c)
{
    return _MovePoint3_L_C(
        pSnap->ptOrg,
        false,
        true, //bTmp
        pSnap, l, c);
 
    return true;
}


bool CActionCircle::_MovePoint2_L_L(
    SNAP_DATA* pSnap,
    std::shared_ptr<CDrawingLine> l1,
    std::shared_ptr<CDrawingLine> l2)
{
    CDrawMarker* pMarker= m_pView->GetMarker();

    pMarker->Clear();
    pMarker->ClearObject();


    POINT2D ptNear;

    if (!_CreateAux_L_L(l1, l2))
    {
        return false;
    }

    POINT2D ptCenter;
    bool bUseLine0 = true;
    POINT2D ptOnLine;
    const bool bOnlineFalse = false;

    POINT2D pt1 = m_pLine[0]->NearPoint(pSnap->pt);
    if (m_pLine[1])
    {
        POINT2D pt2 = m_pLine[1]->NearPoint(pSnap->pt);

        if (pt1.Distance(pSnap->pt) > pt2.Distance(pSnap->pt))
        {
            ptCenter = m_pLine[1]->NearPoint(pSnap->pt);
            bUseLine0 = false;
            l1->NearPoint(&ptOnLine, ptCenter, bOnlineFalse);
        }
    }

    if(bUseLine0)
    {
        ptCenter = m_pLine[0]->NearPoint(pSnap->pt);
        l1->NearPoint(&ptOnLine, ptCenter, bOnlineFalse);
    }

    double dR = ptOnLine.Distance(ptCenter);

    auto pDrawCircle = std::dynamic_pointer_cast<CDrawingCircle>(m_psAddObj);
    pDrawCircle->SetVisible(false);
    m_psSelectCircles.clear();
    auto dc = std::make_shared<CDrawingCircle>();
    dc->SetCenter(ptCenter);
    dc->SetRadius(dR);
    dc->SetColor(DRAW_CONFIG->crAdditional);
    dc->SetLineType(PS_DOT);
    m_psSelectCircles.push_back(dc);
    pMarker->AddObject(dc);

    pSnap->eSnapType = VIEW_COMMON::SNP_CIRCLE;
    pSnap->strSnap = GET_STR(STR_SNAP_2LINE_CONTACT);

    return true;
}

bool CActionCircle::_MovePoint2(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    POINT ptSel = posMouse.ptSel;
    UINT nFlags = posMouse.nFlags;

 
    //!< スナップ点取得
    bool bIgnoreSnap = false;


    CPartsDef*   pDef = m_pView->GetPartsDef();
    if (!pDef)
    {
        return false;
    }

    pDef->RedrawWait();  //TODO:どうする？
    int iLayerId = m_pView->GetCurrentLayerId();

 
    //==================================


    CDrawMarker* pMarker= m_pView->GetMarker();
    pMarker->Clear();
    pMarker->ClearObject();
    pDef->ClearMouseOver();
    pDef->ClearMouseEmphasis();
    pDef->ClearMouseConnectable();


    DWORD dwSnapType = DRAW_CONFIG->GetSnapType();

    CDrawMarker::MARKER_DATA marker;
    SNAP_DATA snap;
    bool bValInput = false;
    StdString sVal = m_pView->GetInputValue();
    if (sVal != _T(""))
    {
        bValInput = true;;
        bool bRet = false;
        if (m_strOldVal != sVal)
        {
            bRet = InputValue(sVal);
            m_pView->UpdateInput(bRet);
            if (bRet)
            {
                m_strOldVal = sVal;
            }
            else
            {
                bValInput = false;
            }
        }
    }


    DWORD dwSnapNew = (dwSnapType & (~VIEW_COMMON::SNP_ON_LINE));

    //------------------------------------
    //  一時的に円のKeepSnapCenter処理を停止
    //------------------------------------
    dwSnapNew &= (~VIEW_COMMON::SNP_CIRCLE);
    dwSnapNew &= (~VIEW_COMMON::SNP_ELLIPSE);
    DRAW_CONFIG->SetSnapType(dwSnapNew);
    //--------------------

    namespace  PLC = std::placeholders;
    bool bSnap;
    std::list<SNAP_DATA> lstAddSnap;
    bSnap = m_pView->GetSnapPointWithAddFunc(
        &lstAddSnap,
        &snap,
        dwSnapNew,
        ptSel,
        std::bind(&CActionCircle::_MovePoint2AddSnap, this, PLC::_1, PLC::_2)
    );


    auto pObj1 = pDef->GetObjectById(snap.iObject1);
    auto pObj2 = pDef->GetObjectById(snap.iObject2);

    POINT ptOffset;
    ptOffset.x = ptOffset.y = 0;
    marker.ptOffset = ptOffset;
    if (nFlags & MK_SHIFT)
    {
        //スナップ無視
        bIgnoreSnap = true;
        bSnap = false;
    }

    if ((pObj1 != NULL) &&
        (pObj2 != NULL))
    {
        //交点
        pDef->SetMouseEmphasis(pObj1);
        pDef->SetMouseEmphasis(pObj2);

        marker.eType = CDrawMarker::MARKER_CROSS;
        marker.eSnap = VIEW_COMMON::SNP_CROSS_POINT;
        pMarker->AddMarker(marker, snap.pt);
    }


    if ((pObj1 != NULL) &&
        (pObj2 == NULL))
    {
        m_pView->SetMarkerWithAddSnapList(pObj1.get(), &lstAddSnap, NULL, false /*bIgnoreOffLineObject*/);
        pDef->SetMouseEmphasis(pObj1);
    }

    //==================================
    // 円のKeepSnapCenter処理を戻す
    // m_pView->SetMarker内でもGetSnapListを使用するため
    // ここで解除する
    DRAW_CONFIG->SetSnapType(dwSnapType);
    //==================================

    auto pDrawCircle = std::dynamic_pointer_cast<CDrawingCircle>(m_psAddObj);

    DRAWING_TYPE eFirstObjectType = DT_NONE;
    if (m_pSelObj[0] != NULL)
    {
        eFirstObjectType = m_pSelObj[0]->GetType();
    }


    if(!bValInput)
    {
        m_selSts &= ~ SS_CREATE_CIRCLE;

        int iMarkerId = pMarker->MouseMove(posMouse.ptSel);
        if ((pObj1 != NULL ) &&
            (pObj2 == NULL ))
        {
            bool bSelectable = false;
            if ((iMarkerId == -1))
            {
                //オブジェクト上にあって、マーカー以外の位置の場合は
                //選択可能なのでSetMouseOverを設定する
                switch(pObj1->GetType())
                {
                case DT_LINE:
                    snap.eSnapType = VIEW_COMMON::SNP_LINE;
                    bSelectable = true;
                    break;

                case DT_CIRCLE:
                    snap.eSnapType = VIEW_COMMON::SNP_CIRCLE;
                    bSelectable = true;
                    break;
                }
            }

            if (bSelectable)
            {
                if ((eFirstObjectType == DT_LINE) ||
                    (eFirstObjectType == DT_CIRCLE))
                {
                    pDef->ClearMouseEmphasis();
                    pDef->SetMouseOver(pObj1);
                }
            }
        }
        if (!m_psAddObj)
        {
            return false;
        }



        VIEW_MODE      eViewMode  = m_pView->GetViewMode();
        m_pView->SetDrawingMode(DRAW_SEL);

        bool bSnap2 = false;
        double dLength;

        dLength = m_ptSel[0].Distance(snap.pt);

        bool bMoveCenter = false;
        //------------------------
        //最初に線と円を選択した場合
        //------------------------

        CDrawMarker::MARKER_DATA marker;
        marker.eType = CDrawMarker::MARKER_CROSS;

        bool bSelObject = false;

        if (eFirstObjectType == DT_LINE)
        {
            auto firstLine = std::dynamic_pointer_cast<CDrawingLine>(m_pSelObj[0]);
            if (pObj1 && pObj2 == NULL && iMarkerId == -1)
            {
                if ((pObj1->GetType() == DT_CIRCLE))
                {
                    auto SecondCircle = std::dynamic_pointer_cast<CDrawingCircle>(pObj1);

                    _MovePoint2_L_C(&snap, firstLine, SecondCircle);
                    bSelObject = true;
                    bSnap = true;
                }
                else if (pObj1->GetType() == DT_LINE)
                {
                    auto SecondLine = std::dynamic_pointer_cast<CDrawingLine>(pObj1);

                    _MovePoint2_L_L(&snap, firstLine, SecondLine);
                    bSelObject = true;
                    bSnap = true;
                }
            }

            LINE2D* fastLineData = firstLine->GetLineInstance();
            if (!bSelObject)
            {
                double dLen = firstLine->Distance(snap.pt);
                double dSnapLen;

                if (!bIgnoreSnap && !bSnap)
                {
                    m_pView->GetSnapLength(&dSnapLen, dLen);
                }
                else
                {
                    dSnapLen = dLen;
                }

                LINE2D lineOffset = fastLineData->Offset(dSnapLen, snap.pt);
                LINE2D lineNorm = fastLineData->NormalLine(snap.pt);
                POINT2D ptCross = lineNorm.Intersect(lineOffset);
                POINT2D ptFirst = lineNorm.Intersect(*fastLineData);

                if(dSnapLen > 0)
                {
                    //-----------
                    //補助線
                    //-----------
                    auto pLine = CreateLine(&m_psAddtionalObj[0]);
                    pLine->SetPoint(ptFirst, ptCross);
                    pMarker->AddObject(m_psAddtionalObj[0]);
                    //-----------

                    dLength = dSnapLen;
                    pDrawCircle->SetVisible(true);
                    pDrawCircle->SetCenter(ptCross);

                    snap.pt = ptCross;
                    marker.eSnap = snap.eSnapType;

                    StdStringStream   strmLen;
                    strmLen << GET_STR(STR_SNAP_CIRCLE_CONTACT) << _T("-");
                    strmLen << GetSnapName(snap.eSnapType, true) << _T("\n");
                    strmLen << GET_STR(STR_PRO_RAD) << _T(":")
                        << CDrawingObject::GetValText(dLength, false, DRAW_CONFIG->iDimLengthPrecision);
                    snap.strSnap = strmLen.str();

                    pDrawCircle->SetRadius(dLength);
                    pMarker->AddMarker(marker, snap.pt);
                    bSnap = true;
                }
            }
        }
        else if (eFirstObjectType == DT_CIRCLE)
        {

            auto firstCircle = std::dynamic_pointer_cast<CDrawingCircle>(m_pSelObj[0]);
            auto firstCircleData  = firstCircle->GetCircleInstance();
            POINT2D ptCenter = firstCircle->GetCenter();

            double dCircleSerfaceToSnapPoint = firstCircleData->Distance(snap.pt);
            double dCenterDistance = ptCenter.Distance(snap.pt);
 
            double dSnapLen;

            if (!bIgnoreSnap)
            {
                m_pView->GetSnapLength(&dSnapLen, dCircleSerfaceToSnapPoint);
            }
            else
            {
                dSnapLen = dCircleSerfaceToSnapPoint;
            }

            double dOffsetRadious;
            if(dCenterDistance >= firstCircle->GetRadius())
            {
                dOffsetRadious = firstCircleData->GetRadius() + dSnapLen;
            }
            else
            {
                dOffsetRadious = firstCircleData->GetRadius() - dSnapLen;
            }


            bool bSelObject = false;
            if (pObj1)
            {
                if ((pObj1->GetType() == DT_CIRCLE) && pObj2 == NULL)
                {
                    //============================
                    //  円ー円
                    //============================
                    auto secondCircle = std::dynamic_pointer_cast<CDrawingCircle>(pObj1);
                    
                    /*

                    POINT2D p1Near;
                    POINT2D p2Near;
                    POINT2D p1Far;
                    POINT2D p2Far;
                    POINT2D pCenter;

                    _GetCrossPoint(&p1Near, &p1Far,
                    &p2Near, &p2Far,
                    firstCircle, secondCircle);

                    if (p2Near.Distance(snap.pt) < p2Far.Distance(snap.pt))
                    {
                        dLength = p2Near.Distance(p1Near)/2.0;
                        pCenter = (p2Near + p1Near) / 2.0;
                    }
                    else
                    {
                        dLength = p2Far.Distance(p1Far)/2.0;
                        pCenter = (p2Far + p1Far) / 2.0;
                    }

                  pDrawCircle->SetRadius(dLength);
                  pDrawCircle->SetCenter(pCenter);

                  pMarker->Clear();
                  pMarker->ClearObject();
                  snap.eSnapType = VIEW_COMMON::SNP_CIRCLE;
                  snap.strSnap = GET_STR(STR_SNAP_2CIRCLE_CONTACT);
                  bSelObject = true;
                  */
                    _MovePoint2_C_C(&snap, firstCircle, secondCircle);
                    bSelObject = true;

                }                
                else if ((pObj1->GetType() == DT_LINE) && pObj2 == NULL)
                {
                    //============================
                    //  円-直線
                    //============================
                    auto secondLine = std::dynamic_pointer_cast<CDrawingLine>(pObj1);
                    _MovePoint2_L_C(&snap, secondLine, firstCircle);
                    bSelObject = true;
                }
            }



            if(!bSnap && !bSelObject)
            {
                //!< 長さによるスナップ点取得
                double dAngle;
                POINT2D ptSnap;
                m_pView->GetFixLengthSnapPoint(&dAngle,
                    &ptSnap,
                    dOffsetRadious,
                    ptCenter,
                    snap.pt,
                    !bIgnoreSnap);

                //補助線
                auto pLine = CreateLine(&m_psAddtionalObj[0]);
                pLine->SetPoint(ptCenter, ptSnap);
                pMarker->AddObject(m_psAddtionalObj[0]);

                if (dSnapLen > 0)
                {
                    m_psAddObj->SetVisible(true);
                    dLength = dSnapLen;
                    pDrawCircle->SetCenter(ptSnap);

                    snap.eSnapType = VIEW_COMMON::SNP_ANGLE;
                    snap.pt = ptSnap;
                    bSnap = true;

                    //角度と長さを表示する
                    StdStringStream   strmLen;
                    strmLen << GET_STR(STR_SNAP_CIRCLE_CONTACT) << _T(" ");
                    strmLen << GET_STR(STR_PRO_RAD) << _T(":")
                        << CDrawingObject::GetValText(dLength, false, DRAW_CONFIG->iDimLengthPrecision);

                    strmLen << _T(" ") << GET_STR(STR_ANGLE) << _T(":")
                        << CDrawingObject::GetValText(dAngle, true, DRAW_CONFIG->iDimLengthPrecision);
                    snap.strSnap = strmLen.str();

                    marker.eSnap = snap.eSnapType;
                    pMarker->AddMarker(marker, snap.pt);
                    pDrawCircle->SetVisible(true);
                    pDrawCircle->SetRadius(dLength);

                }
            }
            else if(!bSelObject)
            {
                //============================
                //  円-点/特徴点
                //============================
                pDrawCircle->SetCenter(snap.pt);
                dLength = dCircleSerfaceToSnapPoint;

                //補助線
                auto pLine = CreateLine(&m_psAddtionalObj[0]);
                pLine->SetPoint(ptCenter, snap.pt);
                pMarker->AddObject(m_psAddtionalObj[0]);

                StdStringStream   strmLen;
                strmLen << GET_STR(STR_SNAP_CIRCLE_CONTACT) << _T("-");
                strmLen << GetSnapName(snap.eSnapType, true)<< _T("\n");
                strmLen << GET_STR(STR_PRO_RAD) << _T(":")
                    << CDrawingObject::GetValText(dLength, false, DRAW_CONFIG->iDimLengthPrecision);
                snap.strSnap = strmLen.str();

                pDrawCircle->SetVisible(true);
                pDrawCircle->SetRadius(dLength);

#               //marker.eType = CDrawMarker::MARKER_CROSS;
                //marker.eSnap = snap.eSnapType;
                //pMarker->AddMarker(marker, snap.pt);
                bSnap = true;
            }
            else
            {
                bSnap = true;
            }
        }


        double dAngle;
        if(!bSnap)
        {
            if (!bIgnoreSnap)
            {
                bSnap2 = m_pView->GetLengthSnapPoint(&dAngle,    //スナップ角
                    &dLength,           //スナップ長さ 
                    &snap.pt,           //スナップ位置
                    m_ptSel[0],         //開始点
                    snap.ptOrg,         //マウスカーソル位置
                    false,              //角度スナップの有無
                    true);
            }
            marker.eType = CDrawMarker::MARKER_CROSS;
            marker.eSnap = VIEW_COMMON::SNP_CROSS_POINT;
            pMarker->AddMarker(marker, snap.pt);
            pDrawCircle->SetVisible(true);
            pDrawCircle->SetRadius(dLength);
        }


        if (snap.strSnap.empty())
        {
            StdStringStream   strmLen;
            strmLen << GetSnapName(snap.eSnapType, true);
            strmLen << _T(" ") << GET_STR(STR_PRO_RAD) << _T(":")
            << CDrawingObject::GetValText(dLength, true, DRAW_CONFIG->iDimLengthPrecision);
            snap.strSnap = strmLen.str();
            pDrawCircle->SetVisible(true);
            pDrawCircle->SetRadius(dLength);
            /*
            snap.strSnap = GetSnapName(snap.eSnapType, true);
            snap.strSnap +=  _T("\n");
            snap.strSnap +=  GET_STR(STR_PRO_RAD);
            snap.strSnap +=  _T(":");
            snap.strSnap += CDrawingObject::GetValText(dLength, false, DRAW_CONFIG->iDimLengthPrecision);
            */
        }
    }
    else
    {
        //数値入力あり
        if (snap.eSnapType == VIEW_COMMON::SNP_NONE)
        {
            //任意点
            CDrawMarker*pMarker = m_pView->GetMarker();
            marker.eType = CDrawMarker::MARKER_CROSS;
            marker.eSnap = VIEW_COMMON::SNP_CROSS_POINT;
            pMarker->AddMarker(marker, snap.ptOrg);
        }

        if (pDrawCircle)
        {
            pDrawCircle->SetVisible(true);
            pDrawCircle->SetCenter(snap.pt);
        }
        m_selSts |= SS_CREATE_CIRCLE;
    }


    pMarker->AddObject(m_psAddObj);
    pMarker->MouseMove(posMouse.ptSel);

    if (snap.strSnap.empty()) 
    {
        m_pView->ResetToolTipText();
    }
    else
    {
        m_pView->SetToolTipText(snap.strSnap.c_str());
    }

    pDef->DrawDragging(m_pView);
    return true;
}

CIRCLE2D CActionCircle::_CalcGoodStopPoint_L_C(POINT2D ptOnLine,
    int iMethod,
    bool bSnap,
    const LINE2D* l, 
    const CIRCLE2D* c, 
    const std::vector<POINT2D>& lstPoint)
{
    
    POINT2D pt1;

    double dR = c->GetRadius();

    double l1;
    pt1 = c->GetCenter();

    l1 = ptOnLine.Distance(pt1);

    double dLC = l->Distance(pt1);

     //-----------------
    //-----------------
    /*
    最初に指定位置での接円の半径を求める
    */
    POINT2D ptL = l->NearPoint(ptOnLine);
    double dRNewCircle = ptL.Distance(ptOnLine);

    //-----------------

    CIRCLE2D crRet(ptOnLine, dRNewCircle);

    if (!bSnap)
    {
        return   crRet;
    }

    double dSnapR = 0.0;
    double dDiff;
    if (lstPoint.size() > 1)
    {
        double R1_0 = l->Distance(lstPoint[0]);
        double R1_1 = l->Distance(lstPoint[1]);

        dDiff = fabs(R1_0 - R1_1);
    }
    else
    {
        dDiff = m_pView->GetSnapMinLength();
    }

    double dN = ceil(log10(dDiff));
    double dMinLin;
    dMinLin = pow(10, dN) / 10;;
    dSnapR = fabs(CUtil::Round(dRNewCircle / dMinLin) * dMinLin);



    CIRCLE2D cr1;
    LINE2D ln1;
    ln1 = l->Offset(dSnapR, ptOnLine);



    POINT2D crc = cr1.GetCenter();


    std::vector<POINT2D> lst;

    std::shared_ptr<PARABOLA2D> para = m_pParabola[iMethod];
    if (para)
    {
        para->Intersect(ln1, &lst, false, DIR_CIRCLE_TOLERANCE_MAX);
    }
    else if(m_pLine[0])
    {
        POINT2D pt;
        if (m_pLine[0]->IntersectInfnityLine(&pt, ln1))
        {
            lst.push_back(pt);
        }
    }

    double lMin = DBL_MAX;
    for (auto pI : lst)
    {
        double dL = pI.Distance(ptOnLine);
        if (lMin > dL)
        {
            crRet.SetRadius(dSnapR);
            crRet.SetCenter(pI);
            lMin = dL;
        }
    }

    return  crRet;
}

bool CActionCircle::_CreateLC(std::vector<POINT2D>* pLstCenter,
    double dR,
    std::shared_ptr<CDrawingLine> dl,
    std::shared_ptr<CDrawingCircle> c)
{

    CDrawMarker* pMarker= m_pView->GetMarker();
    CDrawMarker::MARKER_DATA marker;

    POINT2D ptCenter;

    double  dCRad = c->GetRadius();
    POINT2D ptCCenter = c->GetCenter();

    LINE2D l = *dl->GetLineInstance();
    std::vector<POINT2D> lst;


    POINT2D ptFocus = m_pParabola[0]->GetFocus();
    LINE2D ln1 = l.Offset(dR, ptFocus);

    m_pParabola[0]->Intersect(ln1, &lst, false, DIR_CIRCLE_TOLERANCE_MAX);
    pLstCenter->insert(pLstCenter->end(), lst.begin(), lst.end());

    POINT2D ptOppsitFocus = ptFocus ;
    ptOppsitFocus.Mirror(l);
    LINE2D ln2 = l.Offset(dR, ptOppsitFocus);

    bool bBothSide = false;
    if (l.Distance(ptCCenter) < dCRad)
    {
        bBothSide = true;
        m_pParabola[0]->Intersect(ln2, &lst, false, DIR_CIRCLE_TOLERANCE_MAX);
        pLstCenter->insert(pLstCenter->end(), lst.begin(), lst.end());
    }


    if(m_pLine[0])
    {
        POINT2D pt;
        if (m_pLine[0]->IntersectInfnityLine(&pt, ln1))
        {
            pLstCenter->push_back(pt);
        }

        if (m_pLine[0]->IntersectInfnityLine(&pt, ln2))
        {
            pLstCenter->push_back(pt);
        }
    }
    else
    {

        ptFocus = m_pParabola[1]->GetFocus();
        ln1 = l.Offset(dR, ptFocus);

        m_pParabola[1]->Intersect(ln1, &lst, false, DIR_CIRCLE_TOLERANCE_MAX);
        pLstCenter->insert(pLstCenter->end(), lst.begin(), lst.end());

        POINT2D ptOppsitFocus = ptFocus ;
        ptOppsitFocus.Mirror(l);
        LINE2D ln2 = l.Offset(dR, ptOppsitFocus);

        if (bBothSide)
        {
            m_pParabola[1]->Intersect(ln2, &lst, false, DIR_CIRCLE_TOLERANCE_MAX);
            pLstCenter->insert(pLstCenter->end(), lst.begin(), lst.end());
        }
    }

    if (pLstCenter->empty())
    {
        return false;
    }
    return true;

}

bool CActionCircle::_MovePoint3_L_C(
    POINT2D pt2d,
    bool bSnap,
    bool bTmp,
    SNAP_DATA* pSnap,
    std::shared_ptr<CDrawingLine> l,
    std::shared_ptr<CDrawingCircle> c)
{
    auto vv = m_pView->GetViewValue();
    if (vv != *m_psVvHyp)
    {
        *m_psVvHyp = vv;
        m_psHypabora.clear();
        _CreateAux_L_C(l, c);
    }


    CDrawMarker* pMarker= m_pView->GetMarker();
    CDrawMarker::MARKER_DATA marker;

    double dR;
    POINT2D ptCenter;

    double  dCRad = c->GetRadius();
    POINT2D ptCCenter = c->GetCenter();
    double dT1, dT2;

    POINT2D p1 = m_pParabola[0]->NearPoint2(pt2d, &dT1);
    
    POINT2D p2;
    if (m_pParabola[1])
    {
        p2 = m_pParabola[1]->NearPoint2(pt2d, &dT2);
    }
    else if (m_pLine[0])
    {
        p2 = m_pLine[0]->NearPoint(pt2d, &dT2);
    }

    double dMin = m_pView->GetSnapMinLength()/2.0;

    CIRCLE2D cr;
    cr.SetRadius(dMin);
    int iMethod;
    std::vector<POINT2D>  lstPoint;
    if (pt2d.Distance(p1) < pt2d.Distance(p2))
    {
        ptCenter = p1;
        dR = ptCenter.Distance(ptCCenter)-dCRad;
        iMethod = 0;

        m_pParabola[0]->Intersect(cr, &lstPoint, DIR_CIRCLE_TOLERANCE_MAX);

    }
    else
    {
        ptCenter = p2;
        dR = ptCenter.Distance(ptCCenter)+dCRad;
        iMethod = 1;

        if (m_pParabola[1])
        {
            m_pParabola[1]->Intersect(cr, &lstPoint, DIR_CIRCLE_TOLERANCE_MAX);
        }
        else if (m_pLine[0])
        {
            cr.Intersect(*m_pLine[0].get(), &lstPoint, false, DIR_CIRCLE_TOLERANCE_MAX);
        }
        
    }

    CIRCLE2D crRet;
    crRet = _CalcGoodStopPoint_L_C( ptCenter, 
        iMethod,
        bSnap,
        l->GetLineInstance(),
        c->GetCircleInstance(), lstPoint);

    ptCenter = crRet.GetCenter();
    dR = crRet.GetRadius();


    //半径を表示する
    StdStringStream   strmLen;
    strmLen << GET_STR(STR_SNAP_LINE_CIRCLE_CONTACT) << _T("\n");
    if (!bTmp)
    {
        strmLen << _T(" ") << GET_STR(STR_PRO_RAD) << _T(":")
            << CDrawingObject::GetValText(dR, true, DRAW_CONFIG->iDimLengthPrecision);
    }
    pSnap->strSnap = strmLen.str();

    auto pDrawCircle = std::dynamic_pointer_cast<CDrawingCircle>(m_psAddObj);
                                                   
    if (bTmp)
    {
        pDrawCircle->SetVisible(false);
        m_psSelectCircles.clear();
        auto dc = std::make_shared<CDrawingCircle>();
        dc->SetCenter(ptCenter);
        dc->SetRadius(dR);
        dc->SetColor(DRAW_CONFIG->crAdditional);
        dc->SetLineType(PS_DOT);
        m_psSelectCircles.push_back(dc);
        pMarker->AddObject(dc);
    }
    else
    {
        pDrawCircle->SetVisible(true);
        pDrawCircle->SetRadius(dR);
        pDrawCircle->SetCenter(ptCenter);

        marker.eType = CDrawMarker::MARKER_CROSS;
        marker.eSnap = VIEW_COMMON::SNP_CROSS_POINT;
        pMarker->AddMarker(marker, ptCenter);
        _AddAux(pMarker);
    }
    return true;
}


CIRCLE2D CActionCircle::_CalcGoodStopPoint_C_C(POINT2D ptOnLine,
    int iMethod1,
    int iMethod2,
    bool bSnap,
    const CIRCLE2D* c1, 
    const CIRCLE2D* c2, 
    const std::vector<POINT2D>& lstPoint)
{
    POINT2D pt1;
    POINT2D pt2;

    double dR1 = c1->GetRadius();
    double dR2 = c2->GetRadius();

    double l1;
    double l2;
    pt1 = c1->GetCenter();
    pt2 = c2->GetCenter();
    l1 = ptOnLine.Distance(pt1);
    l2 = ptOnLine.Distance(pt2);

    //-----------------
    //-----------------
    /*
    最初に指定位置での接円の半径を求める
    iMethodは 円中心から接円中心までの距離を計算するときの符号を表したものなので
    以下のように変形する。
    L1 =  (+-)R + (+-)R1
    (-+)R = (+-)R1 - L1                       
    R =   (-+)((+-)R1 - L1)
    */
    double dRNewCircle;
    if (iMethod1 & R1_PLUS)
    {
        dRNewCircle = dR1-l1;
    }
    else
    {
        dRNewCircle = -dR1-l1;
    }

    if (iMethod1 & SNAPC1_PLUS)
    {
        dRNewCircle = -dRNewCircle;
    }

    //-----------------


    CIRCLE2D crRet(ptOnLine, dRNewCircle);

    if (!bSnap)
    {
        return   crRet;
    }

    //--------------------
    //
    //--------------------

    double dSnapR = 0.0;
    double dDiff;
    if (lstPoint.size() > 1)
    {
        double R1_0 = pt1.Distance(lstPoint[0]);
        double R1_1 = pt1.Distance(lstPoint[1]);

        dDiff = fabs((R1_0 - R1_1));
#ifdef _DEBUG
DB_PRINT(_T("diff1 %f \n"), dDiff);
#endif
    }
    else
    {
        dDiff = m_pView->GetSnapMinLength();
#ifdef _DEBUG
        DB_PRINT(_T("diff2 %f \n"), dDiff);
#endif
    }

    double dN = ceil(log10(dDiff));
    double dMinLin;
    dMinLin = pow(10, dN) / 10;;
    dSnapR = CUtil::Round(dRNewCircle / dMinLin) * dMinLin;

#ifdef _DEBUG
    DB_PRINT(_T("dSnapR %f dSnapR:%f\n"), dSnapR, dMinLin);
#endif

    double lNewL1;
    double lNewL2;

    std::vector<int > lstMethod;

    lstMethod.push_back(iMethod1);
    if (iMethod1 != iMethod2)
    {
        lstMethod.push_back(iMethod2);
    }


    CIRCLE2D c[2];
    c[0].SetCenter(pt1);
    c[1].SetCenter(pt2);

    double lMin = DBL_MAX;         
    int iMethod = 0;
    int iMethodRet = 0;
    for (int iMethod: lstMethod)
    {
        if (iMethod & R1_PLUS)     {lNewL1 = dR1;}
        else                        {lNewL1 = -dR1;}

        if (iMethod & SNAPC1_PLUS) {lNewL1 += dSnapR;}
        else                        {lNewL1 -= dSnapR;}

        if (iMethod & R2_PLUS)     {lNewL2 = dR2;}
        else                        {lNewL2 = -dR2;}

        if (iMethod & SNAPC2_PLUS) {lNewL2 += dSnapR;}
        else                        {lNewL2 -= dSnapR;}

        c[0].SetRadius(lNewL1);
        c[1].SetRadius(lNewL2);

        std::vector<POINT2D> lst;
        c[1].Intersect(c[0], &lst, false, DIR_CIRCLE_TOLERANCE_MAX);


        for (auto pI : lst)
        {
            double dL = pI.Distance(ptOnLine);
            if (lMin > dL)
            {
                crRet.SetRadius(dSnapR);
                crRet.SetCenter(pI);
                lMin = dL;
                iMethodRet = iMethod;
            }
        }
        iMethod++;
    }

#if 0  
    POINT2D pCNew = crRet.GetCenter();
    if (fabs(lMin - DBL_MAX) < NEAR_ZERO)
    {
        DB_PRINT(_T(" Not Find \n"));
    }
    DB_PRINT(_T(" pO:%s PN%s R:%f, SnapR:%f, lMin:%f M:%d\n"),
        ptOnLine.Str().c_str() ,
        pCNew.Str().c_str(),
        dRNewCircle,
        dSnapR,
        lMin,
        iMethodRet);
#endif
    return  crRet;
}

bool CActionCircle::_CreateCC(std::vector<POINT2D>* pLstCenter,
                              double dR, 
                              std::shared_ptr<CDrawingCircle> c1,
                              std::shared_ptr<CDrawingCircle> c2)
{

    if (c1->GetRadius() > c2->GetRadius())
    if (c1->GetRadius() > c2->GetRadius())
    {
        std::swap(c1, c2);
    }

    POINT2D pContact;
    auto eType = CActionCircle::_GetContactType(&pContact, c1, c2);

    CIRCLE2D cr1;
    CIRCLE2D cr2;

    pLstCenter->clear();
    std::vector<POINT2D> lstRet;

    double dR1 = c1->GetRadius();
    double dR2 = c2->GetRadius();
    cr1.SetCenter(c1->GetCenter());
    cr2.SetCenter(c2->GetCenter());

    if ((eType & E_OUTSIDE) || (eType & E_CORSS))
    {
        cr1.SetRadius(dR1+dR);
        cr2.SetRadius(dR2+dR);

        cr1.Intersect(cr2, &lstRet);
        pLstCenter->insert(pLstCenter->end(), lstRet.begin(), lstRet.end());


        if (dR > dR1)
        {
            cr1.SetRadius(dR - dR1);
            cr2.SetRadius(dR - dR2);
            cr1.Intersect(cr2, &lstRet);
            pLstCenter->insert(pLstCenter->end(), lstRet.begin(), lstRet.end());
        }

        if (dR1 > dR)
        {
            cr1.SetRadius(dR1 - dR);
            cr2.SetRadius(dR2 + dR);
            cr1.Intersect(cr2, &lstRet);
            pLstCenter->insert(pLstCenter->end(), lstRet.begin(), lstRet.end());
        }

        if (dR2 > dR)
        {
            cr1.SetRadius(dR1 + dR);
            cr2.SetRadius(dR2 - dR);
            cr1.Intersect(cr2, &lstRet);
            pLstCenter->insert(pLstCenter->end(), lstRet.begin(), lstRet.end());
        }
    }
    
    if (eType & E_INSIDE)
    {
        if (dR2 > dR)
        {
            cr1.SetRadius(dR1 + dR);
            cr2.SetRadius(dR2 - dR);
            cr1.Intersect(cr2, &lstRet);
            pLstCenter->insert(pLstCenter->end(), lstRet.begin(), lstRet.end());


            if (2 * dR1 > dR)
            {
                cr1.SetRadius(dR - 2 * dR1);
                cr2.SetRadius(dR2 - dR);
                cr1.Intersect(cr2, &lstRet);
                pLstCenter->insert(pLstCenter->end(), lstRet.begin(), lstRet.end());
            }
        }
    }

    if (pLstCenter->empty())
    {
        return false;
    }
    return true;

}

bool CActionCircle::_MovePoint3_C_C(
                                    POINT2D pt2d,
                                    bool bSnap,
                                    bool bTmp,
                                    SNAP_DATA* pSnap,
                                    std::shared_ptr<CDrawingCircle> c1, 
                                    std::shared_ptr<CDrawingCircle> c2)
{

    if (c1 == c2)
    {
        return false;
    }

    if (c1->GetRadius() > c2->GetRadius())
    {
        std::swap(c1, c2);
    }

    POINT2D pContact;
    auto eType = CActionCircle::_GetContactType(&pContact, c1, c2);

    auto vv = m_pView->GetViewValue();
    if (vv != *m_psVvHyp)
    {
        *m_psVvHyp = vv;
        m_psHypabora.clear();
        _CreateAux_C_C(c1, c2);
    }

    /*
    ２円に接する円の関係は大別すると３とおりとなる

    L1：円１と接円の中心距離
    L2：円２と接円の中心距離
    R1：円１の半径
    R2：円２の半径
    R ：接円の半径

    ・両円が交わらない
    Hyp:1
    L1=R+R1   R = R1-L1
    L2=R+R2   R = R2-L2

    L1=R-R1   R = R1+L1
    L2=R-R2   R = R2+L2

    Hyp:2
    L1=R+R1   R = R1-L1
    L2=R-R2   R = R2+L2

    L1=R-R1  R = R1+L1
    L2=R+R2  R = R1-L1

    ・両円が交差する

    Hyp:1
    L1=R+R1
    L2=R+R2

    L1=R-R1
    L2=R-R2

    Ellp:1
    L1=  R + R1
    L2= -R + R2

    L1= -R + R1
    L2=  R + R2


    ・円を内包する

    Ellp:1
    L1 =  R + R1
    L2 = -R + R2

    Ellp:2
    L1=  R - R1
    L2= -R + R2
    */

    int iMethod1,iMethod2;
    POINT2D pt;
    POINT2D pt1;
    POINT2D pt2;
    POINT2D ptC1 = c1->GetCenter();
    POINT2D ptC2 = c2->GetCenter();

    double dR1 = c1->GetRadius();
    double dR2 = c2->GetRadius();
    double l1 ;
    double l2 ;
    std::vector<POINT2D> lstPoint;

    double dMin = m_pView->GetSnapMinLength()/2.0;
    CIRCLE2D cr;
    cr.SetRadius(dMin);

    double dR;
    POINT2D ptCenter;
    std::shared_ptr<LINE2D> pContactLine;

    if (eType & E_OUTSIDE)
    {
        //円と円が離れている場合
        bool bHyp = false;

        if (eType & E_SAME)
        {
            //円のサイズが同じ
            pt1 = m_pLine[0]->NearPoint(pt2d);

            if (eType & E_CONTACT)
            {
                pt2 = m_pLine[1]->NearPoint(pt2d);
                pContactLine = m_pLine[1];
            }
            else
            {
                //外接 内接
                pt2 = m_pHyperbola[0]->NearPoint(pt2d);
            }

            if (pt1.Distance(pt2d) < pt2.Distance(pt2d))
            {
                l1 = pt1.Distance(ptC1);
                l2 = pt1.Distance(ptC2);
                ptCenter = pt1;
                cr.SetCenter(pt1);

                /*
                L1=R+R1   R = R1-L1
                L2=R+R2   R = R2-L2
                or   
                L1=R-R1   R = R1+L1
                L2=R-R2   R = R2+L2
                */

                //直線の右側か左側かで判別を行う
                int iSide = m_pLine[0]->Side(pt2d);

                if (iSide == 1)
                {
                    iMethod1  =  SNAPC1_PLUS  | R1_PLUS;
                    iMethod1 |=  SNAPC2_PLUS  | R2_PLUS;
                    iMethod2 = iMethod1;
                }
                else
                {
                    iMethod1  =  SNAPC1_PLUS  | R1_MINUS;
                    iMethod1 |=  SNAPC2_PLUS  | R2_MINUS;
                    iMethod2 = iMethod1;
                }
#ifdef _DEBUG
DB_PRINT(_T("lstPoint 1\n"));
#endif
                cr.Intersect(*(m_pLine[0].get()), &lstPoint);

            }
            else
            {
                bHyp = true;
            }
        }
        else
        {
            if (eType & E_CONTACT)
            {
                pt2 = m_pLine[0]->NearPoint(pt2d);
                pContactLine = m_pLine[0];
            }
            else
            {
                pt2 = m_pHyperbola[0]->NearPoint(pt2d);
            }

            pt1 = m_pHyperbola[1]->NearPoint(pt2d);
            //m_pHyperbola[1] は２つの円に外接する円 (outside時)
            //m_pHyperbola[0] は円に外接と内接する (outside時)
            if (pt1.Distance(pt2d) < pt2.Distance(pt2d))
            {

                l1 = pt1.Distance(ptC1);
                l2 = pt1.Distance(ptC2);
                ptCenter = pt1;
                cr.SetCenter(pt1);


                /*
                R:外接円
                L1:外接円中心から円１中心までの距離
                L1=R+R1   R = R1-L1
                L2=R+R2   R = R2-L2
                or   
                L1=R-R1   R = R1+L1
                L2=R-R2   R = R2+L2
                */

                double dROrg1 = dR1 - l1;
                double dROrg2 = dR2 - l2;
#ifdef _DEBUG
                DB_PRINT(_T("dROrg1:%f, dROrg2:%f\n"), dROrg1, dROrg2);
#endif

                if (fabs(dROrg1 - dROrg2) < NEAR_ZERO)
                {
                    //外接 外接
                    iMethod1  =  SNAPC1_PLUS  | R1_PLUS;
                    iMethod1 |=  SNAPC2_PLUS  | R2_PLUS;
                    iMethod2 = iMethod1;
#ifdef _DEBUG
                    DB_PRINT(_T("lstPoint 2-1\n"));
#endif
                }
                else
                {
                    //外接 内接
                    iMethod1  =  SNAPC1_PLUS  | R1_MINUS;
                    iMethod1 |=  SNAPC2_PLUS  | R2_MINUS;
                    iMethod2 = iMethod1;
#ifdef _DEBUG
                    DB_PRINT(_T("lstPoint 2-2\n"));
#endif
                }

                m_pHyperbola[1]->Intersect(cr, &lstPoint, false, DIR_CIRCLE_TOLERANCE_MAX);
            }
            else
            {
                bHyp = true;
            }
        }

        if (bHyp)
        {
            l1 = pt2.Distance(ptC1);
            l2 = pt2.Distance(ptC2);
            ptCenter = pt2;
            cr.SetCenter(pt2);

            /*
            L1=R-R1   R = L1+R1
            L2=R+R2   R = L2-R2

            L1=R+R1  R = L1-R1
            L2=R-R2  R = L2+R2
            */
            double dROrg1 =  l1 + dR1;
            double dROrg2 =  l2 - dR2;
            if (fabs(dROrg1 - dROrg2) < NEAR_ZERO)
            {
                iMethod1  =  SNAPC1_PLUS  |R1_MINUS;
                iMethod1 |=  SNAPC2_PLUS  |R2_PLUS;
                iMethod2 =  iMethod1;
            }
            else
            {
                iMethod1  =  SNAPC1_PLUS  |R1_PLUS;
                iMethod1 |=  SNAPC2_PLUS  |R2_MINUS;
                iMethod2 =  iMethod1;
            }

            if (eType & E_CONTACT)
            {
#ifdef _DEBUG
                DB_PRINT(_T("lstPoint 3\n"));
#endif

                cr.Intersect(*pContactLine.get(), &lstPoint);
            }
            else
            {
#ifdef _DEBUG
                DB_PRINT(_T("lstPoint 4\n"));
#endif
                m_pHyperbola[0]->Intersect(cr, &lstPoint, false, DIR_CIRCLE_TOLERANCE_MAX);
            }
        }

        CIRCLE2D crRet = _CalcGoodStopPoint_C_C( ptCenter, 
            iMethod1, iMethod2,
            bSnap,
            c1->GetCircleInstance(),
            c2->GetCircleInstance(), lstPoint);

        dR = crRet.GetRadius();
        ptCenter = crRet.GetCenter();

    }
    else if (eType & E_CORSS)
    {

        double  dT;
        int     iSide;
        POINT2D pt;

        int iType = 0;

        if (eType & E_SAME)
        {
            //円のサイズが同じ
            pt1 = m_pLine[0]->NearPoint(pt2d);
            pt2 = m_pEllipse[0]->NearPoint(pt2d);

            if (pt1.Distance(pt2d) < pt2.Distance(pt2d))
            {
                ptCenter = pt1;
                cr.SetCenter(pt1);
                cr.Intersect(*(m_pLine[0].get()), &lstPoint);

                //直線の右側か左側かで判別を行う
                int iSide = m_pLine[0]->Side(pt2d);

                if (iSide == 1)
                {
                    iType = 1;
                }
                else
                {
                    iType = 2;
                }
            }
            else
            {
                iType = 3;
            }
        }
        else
        {
            pt1 = m_pHyperbola[0]->NearPoint(pt2d, &dT, &iSide);
            pt2 = m_pEllipse[0]->NearPoint(pt2d);

            double snap1 = pt1.Distance(pt2d);
            double snap2 = pt2.Distance(pt2d);

            if (snap1 < snap2)
            {
                cr.SetCenter(pt1);
                m_pHyperbola[0]->Intersect(cr, &lstPoint, false, DIR_CIRCLE_TOLERANCE_MAX);
                ptCenter = pt1;

                if (lstPoint.size() < 2)
                {
                    m_pHyperbola[0]->Intersect(cr, &lstPoint);
                }
                /*
                L1=R-R1  R = L1 + R1
                L2=R-R2  R = L2 + R2
                --------------------------
                L1=R+R1  R = L1 - R1
                L2=R+R2  R = L2 - R2

                L1=R1-R  R = R1 - L1
                L2=R2-R  R = R2 - L2
                */
                l1 = pt1.Distance(ptC1);
                l2 = pt1.Distance(ptC2);

                double dROrg1 = l1 + dR1;
                double dROrg2 = l2 + dR2;

                if (fabs(dROrg1 - dROrg2) < NEAR_ZERO)
                {
                    /*
                    L1=R-R1  R = L1 + R1
                    L2=R-R2  R = L2 + R2
                    */
                    iType = 1;
                }
                else
                {
                    iType = 2;
                }
            }
            else
            {
                iType = 3;
            }
        }

        if (iType == 1)
        {
            iMethod1 = SNAPC1_PLUS | R1_MINUS;
            iMethod1 |= SNAPC2_PLUS | R2_MINUS;
            iMethod2 = iMethod1;

        }
        else if (iType == 2)
        {
            iMethod1 = SNAPC1_PLUS | R1_PLUS;
            iMethod1 |= SNAPC2_PLUS | R2_PLUS;

            iMethod2 = R1_PLUS | SNAPC1_NINUS;
            iMethod2 |= R2_PLUS | SNAPC2_NINUS;
        }
        else if (iType == 3)
        {
            cr.SetCenter(pt2);
            m_pEllipse[0]->Intersect(cr, &lstPoint, false, DIR_CIRCLE_TOLERANCE_MAX);
            ptCenter = pt2;

            /*
            L1=  R + R1    R = -R1 + L1 
            L2= -R + R2    R =  R2 - L2

            L1= -R + R1    R =  R1 - L1
            L2=  R + R2    R = -R2 + L2
            */

            l1 = pt2.Distance(ptC1);
            l2 = pt2.Distance(ptC2);

            double dROrg1 = - dR1 + l1;
            double dROrg2   = dR2 - l2;
            if (fabs(dROrg1 - dROrg2) < NEAR_ZERO)
            {
                iMethod1 =   SNAPC1_PLUS  | R1_PLUS;
                iMethod1 |=  SNAPC2_NINUS | R2_PLUS;
                iMethod2 =  iMethod1;
            }
            else
            {
                iMethod1 =   SNAPC1_NINUS | R1_PLUS;
                iMethod1 |=  SNAPC2_PLUS  | R2_PLUS;
                iMethod2 =  iMethod1;
            }
        }

        CIRCLE2D crRet = _CalcGoodStopPoint_C_C( ptCenter, 
            iMethod1, iMethod2,
            bSnap,
            c1->GetCircleInstance(),
            c2->GetCircleInstance(), lstPoint);

        dR = crRet.GetRadius();
        ptCenter = crRet.GetCenter();
    }
    else if(eType & E_INSIDE)
    {
        double dRSecond = 0.0;
        pt1 = m_pEllipse[0]->NearPoint(pt2d);

        if (eType & E_CONTACT)
        {
            pt2 = m_pLine[0]->NearPoint(pt2d);
        }
        else
        {
            pt2 = m_pEllipse[1]->NearPoint(pt2d);
        }

        if (pt1.Distance(pt2d) < pt2.Distance(pt2d))
        {

            /*
            L1 =  R + R1
            L2 = -R + R2
            */

            l1 = pt1.Distance(ptC1);
            l2 = pt1.Distance(ptC2);

            cr.SetCenter(pt1);
            m_pEllipse[0]->Intersect(cr, &lstPoint, false, DIR_CIRCLE_TOLERANCE_MAX);
            pt = pt1;

            iMethod1  = SNAPC1_PLUS  | R1_PLUS ;
            iMethod1 |= SNAPC2_NINUS | R2_PLUS;
            iMethod2 = iMethod1;
        }
        else
        {
            /*
            L1=  R - R1
            L2= -R + R2
            */

            pt = pt2;
            cr.SetCenter(pt2);
            bool bType1 = true;

            if (eType & E_CONTACT)
            {
                cr.Intersect(*(m_pLine[0].get()), &lstPoint);
                l1 = pt2.Distance(ptC1);
                l2 = pt2.Distance(ptC2);

                double dROrg1   =  l1 - dR1;
                double dROrg2   =  l2 - dR2;
                if (fabs(dROrg1 - dROrg2) < NEAR_ZERO)
                {
                    bType1 = false;
                    /*
                    L1=  R + R1
                    L2=  R + R2
                    */

                    iMethod1 = SNAPC1_PLUS  | R1_PLUS;
                    iMethod1 |= SNAPC2_PLUS | R2_PLUS;
                    iMethod2 = iMethod1;

                }
            }
            else
            {
                m_pEllipse[1]->Intersect(cr, &lstPoint, false, DIR_CIRCLE_TOLERANCE_MAX);
            }

            if (bType1)
            {
                iMethod1 = SNAPC1_PLUS | R1_MINUS;
                iMethod1 |= SNAPC2_NINUS | R2_PLUS;
                iMethod2 = iMethod1;
            }
        }

        CIRCLE2D crRet = _CalcGoodStopPoint_C_C( pt, 
            iMethod1,
            iMethod2,
            bSnap,
            c1->GetCircleInstance(),
            c2->GetCircleInstance(), lstPoint);

        dR = crRet.GetRadius();
        ptCenter = crRet.GetCenter();
    }
    CDrawMarker* pMarker= m_pView->GetMarker();

    CDrawMarker::MARKER_DATA marker;

    //半径を表示する
    StdStringStream   strmLen;
    strmLen << GET_STR(STR_SNAP_2CIRCLE_CONTACT) << _T("\n");
    if (!bTmp)
    {
        strmLen << _T(" ") << GET_STR(STR_PRO_RAD) << _T(":")
            << CDrawingObject::GetValText(dR, true, DRAW_CONFIG->iDimLengthPrecision);
    }
    pSnap->strSnap = strmLen.str();

    auto pDrawCircle = std::dynamic_pointer_cast<CDrawingCircle>(m_psAddObj);
    if (bTmp)
    {
        pDrawCircle->SetVisible(false);
        m_psSelectCircles.clear();
        auto dc = std::make_shared<CDrawingCircle>();
        dc->SetCenter(ptCenter);
        dc->SetRadius(dR);
        dc->SetColor(DRAW_CONFIG->crAdditional);
        dc->SetLineType(PS_DOT);
        m_psSelectCircles.push_back(dc);
        pMarker->AddObject(dc);
    }
    else
    {
        pDrawCircle->SetVisible(true);
        pDrawCircle->SetRadius(dR);
        pDrawCircle->SetCenter(ptCenter);

        marker.eType = CDrawMarker::MARKER_CROSS;
        marker.eSnap = VIEW_COMMON::SNP_CROSS_POINT;
        pMarker->AddMarker(marker, ptCenter);
        _AddAux(pMarker);
    }


    return true;
}


CIRCLE2D CActionCircle::_CalcGoodStopPoint_L_L(POINT2D ptOnLine,
    int iMethod,
    bool bSnap,
    const LINE2D* l1, 
    const LINE2D* l2, 
    const std::vector<POINT2D>& lstPoint)
{
    /*
    最初に指定位置での接円の半径を求める
    */

    POINT2D ptL = l1->NearPoint(ptOnLine);
    double dRNewCircle = ptL.Distance(ptOnLine);

    CIRCLE2D crRet(ptOnLine, dRNewCircle);

    if (!bSnap || iMethod == 2/*平行*/)
    {
        return   crRet;
    }
    double dSnapR = 0.0;
    double dDiff;

    if (lstPoint.size() > 1)
    {
        double R1_0 = l1->Distance(lstPoint[0]);
        double R1_1 = l1->Distance(lstPoint[1]);

        dDiff = fabs(R1_0 - R1_1);
#ifdef _DEBUG
        DB_PRINT(_T("Diff %f\n")  ,dDiff);
#endif 
    }
    else
    {
        dDiff = m_pView->GetSnapMinLength();
    }

    double dN = ceil(log10(dDiff));
    double dMinLin;
    dMinLin = pow(10, dN) / 10;;
    dSnapR = fabs(CUtil::Round(dRNewCircle / dMinLin) * dMinLin);

    LINE2D ln1 = l1->Offset(dSnapR, ptOnLine);
    POINT2D ptC = ln1.Intersect(*m_pLine[iMethod].get());

    crRet.SetRadius(dSnapR);
    crRet.SetCenter(ptC);

    return  crRet;
}

bool CActionCircle::_CreateLL(std::vector<POINT2D>* pLstCenter,
    double dR,
    std::shared_ptr<CDrawingLine> dl1,
    std::shared_ptr<CDrawingLine> dl2)
{
    CDrawMarker* pMarker= m_pView->GetMarker();
    CDrawMarker::MARKER_DATA marker;

    if (!m_pLine[1])
    {
        return false;
    }                         

    LINE2D* pll = dl1->GetLineInstance();
    LINE2D* pl2 = dl2->GetLineInstance();

    POINT2D ptCross;
    if (!pll->IntersectInfnityLine(&ptCross, *pl2))
    {
        return false;
    }

    std::vector<POINT2D> lstDir;
    CIRCLE2D cr(ptCross, 1.0);
    LINE2D lstOffset[4];

    LINE2D pn1 = pll->NormalLine(ptCross);
    LINE2D pn2 = pl2->NormalLine(ptCross);

    cr.Intersect(pn1, &lstDir);
    lstOffset[0] = pll->Offset(dR, lstDir[0]);
    lstOffset[1] = pll->Offset(dR, lstDir[1]);

    cr.Intersect(pn2, &lstDir);
    lstOffset[2] = pl2->Offset(dR, lstDir[0]);
    lstOffset[3] = pl2->Offset(dR, lstDir[1]);


    POINT2D ptCenter;
    lstOffset[0].IntersectInfnityLine(&ptCenter, lstOffset[2]);
    pLstCenter->push_back(ptCenter);
    lstOffset[0].IntersectInfnityLine(&ptCenter, lstOffset[3]);
    pLstCenter->push_back(ptCenter);
    lstOffset[1].IntersectInfnityLine(&ptCenter, lstOffset[2]);
    pLstCenter->push_back(ptCenter);
    lstOffset[1].IntersectInfnityLine(&ptCenter, lstOffset[3]);
    pLstCenter->push_back(ptCenter);

    return true;
}


bool CActionCircle::_CreateLLL(std::vector<CIRCLE2D>* pCircls,
    std::shared_ptr<CDrawingLine> l1,
    std::shared_ptr<CDrawingLine> l2,
    std::shared_ptr<CDrawingLine> l3)
{
    if (l1->GetId() == l2->GetId())
    {
        return false;
    }

    if (l2->GetId() == l3->GetId())
    {
        return false;
    }

    if (l3->GetId() == l1->GetId())
    {
        return false;
    }

    //CActionCircle::_CreateAux_L_L実行が前提

    auto pL1 = l1->GetLineInstance();
    auto pL2 = l2->GetLineInstance();
    auto pL3 = l3->GetLineInstance();

    POINT2D ptCross1;
    POINT2D ptCross2;
    POINT2D ptCross3;

    bool bRet1 = pL1->IntersectInfnityLine(&ptCross1, *pL2);
    bool bRet2 = pL2->IntersectInfnityLine(&ptCross2, *pL3);
    bool bRet3 = pL3->IntersectInfnityLine(&ptCross3, *pL1);


    if (!m_pLine[1])
    {
        //２直線が平行
        if (!bRet2)
        {
            //3直線が平行な場合は解がない
            STD_ASSERT(!bRet3);
#ifdef _DEBUG_LLL
            DB_PRINT(_T("NoAns\n"));
#endif
            return false;
        }

        //平行直線の距離が円の直径となる
        POINT2D ptTh = m_pLine[0]->GetThrouchPoint();
        double dR = pL1->Distance(ptTh);

        double dAngle1 = pL1->Angle();
        double dAngle3 = pL3->Angle();


        POINT2D pt3;
        bool bPt3 = pL3->IntersectInfnityLine(&pt3, *pL1);
        STD_ASSERT(bPt3);

        if (bPt3)
        {
            double midAngle = (dAngle1 + dAngle3) / 2.0;

            LINE2D m1(ptCross3, midAngle);
            LINE2D m2(ptCross3, midAngle+90.0);

            POINT2D ptCenter;
            if (m1.IntersectInfnityLine(&ptCenter, *m_pLine[0].get()))
            {
                CIRCLE2D cr(ptCenter, dR);
                pCircls->push_back(cr);
            }

            if (m2.IntersectInfnityLine(&ptCenter, *m_pLine[0].get()))
            {
                CIRCLE2D cr(ptCenter, dR);
                pCircls->push_back(cr);
            }
            return true;
        }
#ifdef _DEBUG_LLL
        DB_PRINT(_T("Paealle NoAns\n"));
#endif

        return false;
    }
    else
    {
        if (bRet1 & bRet2 & bRet3)
        {
            double dAngle1 = pL1->Angle();
            double dAngle2 = pL2->Angle();
            double dAngle3 = pL3->Angle();

            double midAngle1 = (dAngle1 + dAngle2) / 2.0;
            LINE2D m1_1(ptCross1, midAngle1);
            LINE2D m1_2(ptCross1, midAngle1+90.0);

            double midAngle2 = (dAngle2 + dAngle3) / 2.0;
            LINE2D m2_1(ptCross2, midAngle2);
            LINE2D m2_2(ptCross2, midAngle2+90.0);

            double midAngle3 = (dAngle1 + dAngle3) / 2.0;
            LINE2D m3_1(ptCross3, midAngle3);
            LINE2D m3_2(ptCross3, midAngle3+90.0);

            /*
             L1,L2,L3でできる三角形の頂点と内心を通る直線ををm1,m2,m3,
             それに直行する直線をt1,t2,t3とする,
          */
            std::shared_ptr<LINE2D> m[3];
            std::shared_ptr<LINE2D> t[3];

 
            POINT2D pt12;
            POINT2D ptInCenter;
            bool bEndCalc = false;
            if (m1_1.IntersectInfnityLine(&pt12, m2_1))
            {
                if (CUtil::IsInnerTriangle2(pt12, ptCross1, ptCross2, ptCross3))
                {
                    m[0] = std::make_shared<LINE2D>(m1_1);
                    m[1] = std::make_shared<LINE2D>(m2_1);
                    t[0] = std::make_shared<LINE2D>(m1_2);
                    t[1] = std::make_shared<LINE2D>(m2_2);
                    ptInCenter = pt12;
                    bEndCalc = true;
                }
            }
            
            
            if (!bEndCalc && m1_2.IntersectInfnityLine(&pt12, m2_1))
            {
                if (CUtil::IsInnerTriangle2(pt12, ptCross1, ptCross2, ptCross3))
                {
                    m[0] = std::make_shared<LINE2D>(m1_2);
                    m[1] = std::make_shared<LINE2D>(m2_1);
                    t[0] = std::make_shared<LINE2D>(m1_1);
                    t[1] = std::make_shared<LINE2D>(m2_2);
                    ptInCenter = pt12;
                    bEndCalc = true;
                }
            }
            
            if (!bEndCalc && m1_1.IntersectInfnityLine(&pt12, m2_2))
            {
                if (CUtil::IsInnerTriangle2(pt12, ptCross1, ptCross2, ptCross3))
                {
                    m[0] = std::make_shared<LINE2D>(m1_1);
                    m[1] = std::make_shared<LINE2D>(m2_2);
                    t[0] = std::make_shared<LINE2D>(m1_2);
                    t[1] = std::make_shared<LINE2D>(m2_1);
                    ptInCenter = pt12;
                    bEndCalc = true;
                }
            }
            
            if (!bEndCalc && m1_2.IntersectInfnityLine(&pt12, m2_2))
            {
                if (CUtil::IsInnerTriangle2(pt12, ptCross1, ptCross2, ptCross3))
                {
                    m[0] = std::make_shared<LINE2D>(m1_2);
                    m[1] = std::make_shared<LINE2D>(m2_2);
                    t[0] = std::make_shared<LINE2D>(m1_1);
                    t[1] = std::make_shared<LINE2D>(m2_1);
                    ptInCenter = pt12;
                    bEndCalc = true;
                }
            }
 
            
            if(!bEndCalc)
            {
#ifdef _DEBUG_LLL
                DB_PRINT(_T("ptInCenter NoAns\n"));
#endif
                return false;
            }

            bEndCalc = false;
            if (m[0]->IntersectInfnityLine(&pt12, m3_1))
            {
                if (CUtil::IsInnerTriangle2(pt12, ptCross1, ptCross2, ptCross3))
                {
                    m[2] = std::make_shared<LINE2D>(m3_1);
                    t[2] = std::make_shared<LINE2D>(m3_2);
                    bEndCalc = true;
                }
                else
                {
                    if (m[0]->IntersectInfnityLine(&pt12, m3_2))
                    {
                        if (CUtil::IsInnerTriangle2(pt12, ptCross1, ptCross2, ptCross3))
                        {
                            m[2] = std::make_shared<LINE2D>(m3_2);
                            t[2] = std::make_shared<LINE2D>(m3_1);
                            bEndCalc = true;
                        }
                    }
                }
            }

            if(!bEndCalc)
            {
#ifdef _DEBUG_LLL
                DB_PRINT(_T("ptInCenter 1-2 NoAns\n"));
#endif
                return false;
            }


            

#ifdef _DEBUG_LLL
            //m_pLine[2] = m[1];
            //m_pLine[3] = m[2];
#endif

            //内接円
            POINT2D ptCinT = pL3->NearPoint(ptInCenter);
            double dR = ptCinT.Distance(ptInCenter);
            CIRCLE2D cIn(ptInCenter, dR);
            pCircls->push_back(cIn);

#ifdef _DEBUG_LLL
            POINT2D ptCinT1 = pL1->NearPoint(ptInCenter);
            POINT2D ptCinT2 = pL2->NearPoint(ptInCenter);
            double dR1 = ptCinT1.Distance(ptInCenter);
            double dR2 = ptCinT2.Distance(ptInCenter);

            //半径は等しくなるはず
            DB_PRINT(_T("ptCinT R1:%f, R2:%f, R3:%f\n"), dR1, dR2, dR);
#endif

            //外接円1    L2-L1-L3
            POINT2D ptOutCenter1;
            bool bRetOutC1 = m[1]->IntersectInfnityLine(&ptOutCenter1, *t[0].get());
            STD_ASSERT(bRetOutC1);
            if (bRetOutC1)
            {
                POINT2D ptCinT = pL1->NearPoint(ptOutCenter1);
                double dR1 = ptCinT.Distance(ptOutCenter1);
                CIRCLE2D cOut1(ptOutCenter1, dR1);
                pCircls->push_back(cOut1);

#ifdef _DEBUG_LLL
                POINT2D ptCinT2 = pL2->NearPoint(ptOutCenter1);
                POINT2D ptCinT3 = pL3->NearPoint(ptOutCenter1);
                double dR2 = ptCinT2.Distance(ptOutCenter1);
                double dR3 = ptCinT2.Distance(ptOutCenter1);

                //半径は等しくなるはず
                DB_PRINT(_T("C1(%s) R1:%f, R2:%f, R3:%f\n"), ptOutCenter1.Str().c_str(), dR1, dR2, dR3);
#endif
            }

            //外接円2    L1-L2-L3
            POINT2D ptOutCenter2;
            bool bRetOutC2 = m[2]->IntersectInfnityLine(&ptOutCenter2, *t[1].get());
            STD_ASSERT(bRetOutC2);
            if (bRetOutC2)
            {
                POINT2D ptCinT = pL2->NearPoint(ptOutCenter2);
                double dR2 = ptCinT.Distance(ptOutCenter2);
                CIRCLE2D cIn2(ptOutCenter2, dR2);
                pCircls->push_back(cIn2);

#ifdef _DEBUG_LLL
                POINT2D ptCinT1 = pL1->NearPoint(ptOutCenter2);
                POINT2D ptCinT3 = pL3->NearPoint(ptOutCenter2);
                double dR1 = ptCinT1.Distance(ptOutCenter2);
                double dR3 = ptCinT2.Distance(ptOutCenter2);

                //半径は等しくなるはず
                DB_PRINT(_T("C2(%s) R1:%f, R2:%f, R3:%f\n"), ptOutCenter2.Str().c_str(), dR1, dR2, dR3);
#endif
            }

            //外接円2    L1-L3-L2
            POINT2D ptOutCenter3;

            bool bRetOutC3 = m[0]->IntersectInfnityLine(&ptOutCenter3, *t[2].get());
            STD_ASSERT(bRetOutC3);
            if (bRetOutC3)
            {
                POINT2D ptCinT = pL3->NearPoint(ptOutCenter3);
                double dR3 = ptCinT.Distance(ptOutCenter3);
                CIRCLE2D cIn3(ptOutCenter3, dR3);
                pCircls->push_back(cIn3);

#ifdef _DEBUG_LLL
                POINT2D ptCinT1 = pL1->NearPoint(ptOutCenter3);
                POINT2D ptCinT2 = pL2->NearPoint(ptOutCenter3);
                double dR1 = ptCinT1.Distance(ptOutCenter3);
                double dR2 = ptCinT2.Distance(ptOutCenter3);

                //半径は等しくなるはず
                DB_PRINT(_T("C3 R1:%f, R2:%f, R3:%f\n"), dR1, dR2, dR3);
#endif
            }
        }
        else
        {
            //L1とL3 または  L2とL3 が平行
            LINE2D lc;
            POINT2D ptT = pL3->GetThrouchPoint();
            LINE2D lineNorm = pL3->NormalLine(ptT);

            LINE2D* pParallel;
            LINE2D* pOther;
            if (!bRet3)
            {
                //L1,L3が平行
                pParallel = pL1;
                pOther = pL2;
            }
            else if (!bRet2)
            {
                //L2,L3が平行
                pParallel = pL2;
                pOther = pL1;
            }
            else
            {
#ifdef _DEBUG_LLL
        DB_PRINT(_T("unknown parallel NoAns\n"));
#endif
                return false;
            }

            POINT2D ptIntersect;
            bool bRet;

            bRet = lineNorm.IntersectInfnityLine(&ptIntersect, *pParallel);
            POINT2D ptCross = (ptIntersect + ptT) / 2.0;

            double dR = pL3->Distance(ptCross);
            
            //平行線の中間を通る線
            LINE2D lineMid(ptCross, pL3->Angle());


            POINT2D pt3;
            bool bPt3 = pOther->IntersectInfnityLine(&pt3, *pL3);
            STD_ASSERT(bPt3);

            if (bPt3)
            {
                double dAngleOther = pOther->Angle();
                double dAngle3 = pL3->Angle();

                double midAngle = (dAngleOther + dAngleOther) / 2.0;

                LINE2D m1(ptCross3, midAngle);
                LINE2D m2(ptCross3, midAngle+90.0);

                POINT2D ptCenter;
                if (m1.IntersectInfnityLine(&ptCenter, lineMid))
                {
                    CIRCLE2D cr(ptCenter, dR);
                    pCircls->push_back(cr);
                }

                if (m2.IntersectInfnityLine(&ptCenter, lineMid))
                {
                    CIRCLE2D cr(ptCenter, dR);
                    pCircls->push_back(cr);
                }
                return true;
            }
#ifdef _DEBUG_LLL
            DB_PRINT(_T("unknown parallel NoAns\n"));
#endif
            return false;
        }
    }
    return true;
}


//http://claus-jo.dk/Apollonius_Uk.html
//http://mathworld.wolfram.com/ApolloniusProblem.html


//http://claus-jo.dk/Apollonius_Uk.htmlでうまく解が算出られなかったので
//別に計算する

/*
poly1:(x-xa)^2+y^2-(r+ra)^2
poly2:(x-xb)^2+y^2-(r+rb)^2
poly3:x^2+(y-yc)^2-(r+rc)^2

load(grobner);
gb:poly_buchberger([poly1, poly2, poly3], [x,y,r]);


(gb)	[y^2+xa^2\-2*x*xa+x^2\-ra^2\-2*r*ra\-r^2,
y^2+xb^2\-2*x*xb+x^2\-rb^2\-2*r*rb\-r^2,
yc^2\-2*y*yc+y^2+x^2\-rc^2\-2*r*rc\-r^2,\-xb^2+x*(2*xb\-2*xa)+xa^2+rb^2+r*(2*rb\-2*ra)\-ra^2,
xa*(yc^2\-rc^2+rb^2)+xb*(\-yc^2+xa^2+rc^2\-ra^2)+y*(2*xb*yc\-2*xa*yc)\-xa*xb^2+r*((2*rc\-2*ra)*xb+(2*rb\-2*rc)*xa),
xb^2*(yc^4+xa^2*(2*yc^2+4*rc^2\-2*rb^2\-2*ra^2)\-2*rc^2*yc^2\-2*rb^2*yc^2+xa^4+rc^4\-2*ra^2*rc^2+ra^4)+xa^2*(yc^4\-2*rc^2*yc^2\-2*ra^2*yc^2+rc^4\-2*rb^2*rc^2+rb^4)+xb*(xa*(\-2*yc^4+rb^2*(2*yc^2+2*rc^2\-2*ra^2)+ra^2*(2*yc^2+2*rc^2)+4*rc^2*yc^2\-2*rc^4)+xa^3*(\-2*yc^2\-2*rc^2+2*rb^2))+r*(xb*(xa*(rb*(4*yc^2+4*rc^2\-4*ra^2)+ra*(4*yc^2+4*rc^2)+8*rc*yc^2\-8*rc^3+rb^2*(4*rc\-4*ra)+4*ra^2*rc)+(4*rb\-4*rc)*xa^3)+xb^2*(\-4*rc*yc^2\-4*rb*yc^2+(8*rc\-4*rb\-4*ra)*xa^2+4*rc^3\-4*ra*rc^2\-4*ra^2*rc+4*ra^3)+xa^2*(\-4*rc*yc^2\-4*ra*yc^2+4*rc^3\-4*rb*rc^2\-4*rb^2*rc+4*rb^3)+4*rb^3*yc^2\-4*ra*rb^2*yc^2\-4*ra^2*rb*yc^2+4*ra^3*yc^2+(4*ra\-4*rc)*xa*xb^3)+r^2*(xa*xb*(8*yc^2\-8*rc^2+rb*(8*rc\-8*ra)+8*ra*rc)+4*rb^2*yc^2\-8*ra*rb*yc^2+4*ra^2*yc^2+xa^2*(\-4*yc^2+4*rc^2\-8*rb*rc+4*rb^2)+xb^2*(\-4*yc^2+4*rc^2\-8*ra*rc+4*ra^2))+xb^4*(yc^2+xa^2)+xa^4*yc^2+rb^4*yc^2\-2*ra^2*rb^2*yc^2+ra^4*yc^2+xb^3*(xa*(\-2*yc^2\-2*rc^2+2*ra^2)\-2*xa^3)]



+xb*(xa*(-2*yc^4+rb^2*(2*yc^2+2*rc^2\-2*ra^2)+ra^2*(2*yc^2+2*rc^2)+4*rc^2*yc^2\-2*rc^4)+xa^3*(\-2*yc^2\-2*rc^2+2*rb^2))
+xb^2*(yc^4+xa^2*(2*yc^2+4*rc^2-2*rb^2-2*ra^2)-2*rc^2*yc^2-2*rb^2*yc^2+xa^4+rc^4-2*ra^2*rc^2+ra^4)
+xb^3*(xa*(-2*yc^2\-2*rc^2+2*ra^2)-2*xa^3)
+xb^4*(yc^2+xa^2)
+xa^4*yc^2
+xa^2*(yc^4-2*rc^2*yc^2-2*ra^2*yc^2+rc^4-2*rb^2*rc^2+rb^4)
+rb^4*yc^2
-2*ra^2*rb^2*yc^2
+ra^4*yc^2

+r*(xb*
(xa*
(rb*
(4*yc^2+4*rc^2-4*ra^2)
+ra*(4*yc^2+4*rc^2)
+8*rc*yc^2\-8*rc^3
+rb^2*(4*rc\-4*ra)
+4*ra^2*rc)
+(4*rb-4*rc)*xa^3)
+xb^2*(-4*rc*yc^2-4*rb*yc^2+(8*rc\-4*rb\-4*ra)*xa^2+4*rc^3\-4*ra*rc^2\-4*ra^2*rc+4*ra^3)
+xa^2*(-4*rc*yc^2-4*ra*yc^2+4*rc^3\-4*rb*rc^2\-4*rb^2*rc+4*rb^3)
+4*rb^3*yc^2-4*ra*rb^2*yc^2-4*ra^2*rb*yc^2+4*ra^3*yc^2+(4*ra-4*rc)*xa*xb^3)

+r^2*(xa*xb*(8*yc^2-8*rc^2+rb*(8*rc-8*ra)+8*ra*rc)
+4*rb^2*yc^2-8*ra*rb*yc^2+4*ra^2*yc^2+xa^2*(\-4*yc^2+4*rc^2\-8*rb*rc+4*rb^2)
+xb^2*(-4*yc^2+4*rc^2-8*ra*rc+4*ra^2))


xをもとめる
(x-xa)^2+y^2-(r+ra)^2 = 0
(x-xb)^2+y^2-(r+rb)^2 = 0

(x-xa)^2-(r+ra)^2 -(x-xb)^2+(r+rb)^2=0 ;

solve([(x-xa)^2-(r+ra)^2 -(x-xb)^2+(r+rb)^2=0],[x])

x=(xb^2\-xa^2\-rb^2\-2*r*rb+ra^2+2*r*ra)/(2*xb\-2*xa)

y = ±((r+ra)^2-(x-xa)^2)





*/


bool SolveTheApollonius(std::vector<CIRCLE2D> * pLstAns,
        int calcCounter,
        const CIRCLE2D& c1,
        const CIRCLE2D& c2,
        const CIRCLE2D& c3)
{
    pLstAns->clear();
    POINT2D p[3];
    POINT2D p2[3];

    double s1, s2, s3;
    if (calcCounter == 0)      {  s1 =  1; s2 =  1; s3 =  1;    }
    else if (calcCounter == 1) {  s1 = -1; s2 = -1; s3 = -1;    }
    else if (calcCounter == 2) {  s1 =  1; s2 = -1; s3 = -1;    }
    else if (calcCounter == 3) {  s1 = -1; s2 =  1; s3 = -1;    }
    else if (calcCounter == 4) {  s1 = -1; s2 = -1; s3 =  1;    }
    else if (calcCounter == 5) {  s1 =  1; s2 =  1; s3 = -1;    }
    else if (calcCounter == 6) {  s1 = -1; s2 =  1; s3 =  1;    }
    else if (calcCounter == 7) {  s1 =  1; s2 = -1; s3 =  1;    }
    else
    {
#ifdef _DEBUG_LLL
        DB_PRINT(_T("SolveTheApollonius fail(%d) Error1\n"), calcCounter);
#endif

        return false;
    }

    double ra, rb, rc;
    if (c1.GetCenter().Distance(c2.GetCenter()) > NEAR_ZERO)
    {
        p[0] = c1.GetCenter();
        p[1] = c2.GetCenter();
        p[2] = c3.GetCenter();

        ra = c1.GetRadius() * s1;
        rb = c2.GetRadius() * s2;
        rc = c3.GetRadius() * s3;
    }
    else
    {
        p[0] = c3.GetCenter();
        p[1] = c2.GetCenter();
        p[2] = c1.GetCenter();

        ra = c3.GetRadius() * s1;
        rb = c2.GetRadius() * s2;
        rc = c1.GetRadius() * s3;
    }


    /*
    x軸を2つの円の中心（ここではAとB）に合わせるための回転。
    x軸が2つの円（ここではAとB）の中心を通過し、
    y軸が3番目の円（ここではC）の中心を通過するように組み合わせたx変換とy変換。
    5.1）回転
    中心座標距離（AおよびB）を見つけます
    */
    double dS = p[0].dX - p[1].dX;
    double dT = p[0].dY - p[1].dY;
    double dAlpha = atan2(dT, dS);
    double dL = sqrt(dS * dS + dT * dT);
    double dCos = dS/dL;
    double dSin = dT/dL;

    for (int cnt = 0; cnt < 3; cnt++ )
    {
        p2[cnt].dX = p[cnt].dX * dCos + p[cnt].dY * dSin;
        p2[cnt].dY = -p[cnt].dX * dSin + p[cnt].dY * dCos;
    }

    double du = p2[2].dX;
    double dv = p2[0].dY;
    for (int cnt = 0; cnt < 3; cnt++ )
    {
        p2[cnt].dX = p2[cnt].dX - du;
        p2[cnt].dY = p2[cnt].dY - dv;

#ifdef _DEBUG_LLL
       // DB_PRINT(_T("p2[%d]:%s\n"), cnt, p2[cnt].Str().c_str());
#endif
    }


    double xa = p2[0].dX;
    double xb = p2[1].dX;
    double yc = p2[2].dY;

    double xa2 = xa * xa;
    double xa3 = xa2 * xa;
    double xa4 = xa3 * xa;
    double xb2 = xb * xb;
    double xb3 = xb2 * xb;
    double xb4 = xb3 * xb;
    double yc2 = yc * yc;
    double yc3 = yc2 * yc;
    double yc4 = yc3 * yc;
    double ra2 = ra * ra;
    double ra3 = ra2 * ra;
    double ra4 = ra2 * ra2;
    double rb2 = rb * rb;
    double rb3 = rb2 * rb;
    double rb4 = rb2 * rb2;
    double rc2 = rc * rc;
    double rc3 = rc2 * rc;
    double rc4 = rc2 * rc2;



    double C= xb2*(yc4+xa2*
                (2*yc2+4*rc2-2*rb2-2*ra2)
                -2*rc2*yc2-2*rb2*yc2
                +xa4+rc4-2*ra2*rc2+ra4)
            +xa2*
            (yc4-2*rc2*yc2-2*ra2*yc2+rc4-2*rb2*rc2+rb4)
            +xb*(xa*
            (-2*yc4+rb2*
                (2*yc2+2*rc2-2*ra2)
                +ra2*(2*yc2+2*rc2)
                +4*rc2*yc2-2*rc4)
                +xa3*(-2*yc2-2*rc2+2*rb2))
             +xb4*(yc2+xa2)
             +xa4*yc2+rb4*yc2
             -2*ra2*rb2*yc2+ra4*yc2
             +xb3*(xa*(-2*yc2-2*rc2+2*ra2)-2*xa3);


    double B = (xb*(xa*(rb*(4*yc2+4*rc2-4*ra2)+ra*(4*yc2+4*rc2)
            +8*rc*yc2-8*rc3+rb2*(4*rc-4*ra)+4*ra2*rc)
            +(4*rb-4*rc)*xa3)+xb2*(-4*rc*yc2-4*rb*yc2
                +(8*rc-4*rb-4*ra)*xa2+4*rc3-4*ra*rc2-4*ra2*rc+4*ra3)
            +xa2*(-4*rc*yc2-4*ra*yc2+4*rc3-4*rb*rc2-4*rb2*rc+4*rb3)
            +4*rb3*yc2-4*ra*rb2*yc2-4*ra2*rb*yc2+4*ra3*yc2+(4*ra-4*rc)*xa*xb3);

    double A = (xa*xb*(8*yc2-8*rc2+rb*(8*rc-8*ra)+8*ra*rc)
        +4*rb2*yc2-8*ra*rb*yc2+4*ra2*yc2+xa2
        *(-4*yc2+4*rc2-8*rb*rc+4*rb2)+xb2
        *(-4*yc2+4*rc2-8*ra*rc+4*ra2));

    double D = B * B - 4 * A * C;


    if (D < 0)
    {
#ifdef _DEBUG_LLL
        DB_PRINT(_T("SolveTheApollonius fail(%d) (D < 0)\n"), calcCounter);
#endif
        return false;
    }


    auto checkCircle = [&](POINT2D pt, double dR)
    {
        double x = pt.dX;
        double y = pt.dY;

        double ans1 = fabs((x-xa)*(x-xa)+y*y-(dR+ra)*(dR+ra));
        double ans2 = fabs((x-xb)*(x-xb)+y*y-(dR+rb)*(dR+rb));
        double ans3 = fabs(x*x+(y-yc)*(y-yc)-(dR+rc)*(dR+rc));

#ifdef _DEBUG_LLL
        //DB_PRINT(_T("ans %f,%f,%f,\n"), ans1,ans2,ans3);
#endif

        if ((ans1 < NEAR_ZERO) &&
            (ans2 < NEAR_ZERO) &&
            (ans3 < NEAR_ZERO))
        {
            return true;
        }
        return false;
    };

    double K =  2*xb-2*xa;
    if (fabs(K) < NEAR_ZERO)
    {
#ifdef _DEBUG_LLL
        DB_PRINT(_T("SolveTheApollonius fail(%d) (|K| == 0)\n"), calcCounter);
#endif
        return false;
    }


    std::vector<CIRCLE2D> lstCircle;
    double r0 = (-B + sqrt(D)) / (2.0 * A);
    if (r0 > 0)
    {
        double x0=(xb2-xa2-rb2-2*r0*rb+ra2+2*r0*ra)/K;
        double dS = (r0+ra)*(r0+ra)-(x0-xa)*(x0-xa);

        if (fabs(dS) < NEAR_ZERO)
        {
            double y0 = 0;
            POINT2D pC1(x0, y0);
            if (checkCircle(pC1, r0))
            {
                CIRCLE2D c(pC1, r0);
                lstCircle.push_back(c);
            }
        }
        else if (dS > 0)
        {
            double y0 = sqrt(dS);
            double y1 = -y0;
            POINT2D pC1(x0, y0);
            POINT2D pC2(x0, y1);

            if (checkCircle(pC1, r0))
            {
                CIRCLE2D c(pC1, r0);
                lstCircle.push_back(c);
            }
            if (checkCircle(pC2, r0))
            {
                CIRCLE2D c(pC2, r0);
                lstCircle.push_back(c);
            }
        }
    }

    if (D > NEAR_ZERO)
    {
        double r1 = (-B - sqrt(D)) / (2 * A);

        if (r1 > 0)
        {
            double x1=(xb2-xa2-rb2-2*r1*rb+ra2+2*r1*ra)/K;

            double dS = (r1+ra)*(r1+ra)-(x1-xa)*(x1-xa);

            if (fabs(dS) < NEAR_ZERO)
            {
                double y2 = 0;
                POINT2D pC3(x1, y2);
                if (checkCircle(pC3, r1))
                {
                    CIRCLE2D c(pC3, r1);
                    lstCircle.push_back(c);
                }
            }
            else if (dS > 0)
            {
                double y2 = sqrt(dS);
                double y3 = -y2;

                POINT2D pC3(x1, y2);
                POINT2D pC4(x1, y3);
                if (checkCircle(pC3, r1))
                {
                    CIRCLE2D c(pC3, r1);
                    lstCircle.push_back(c);
                }

                if (checkCircle(pC4, r1))
                {
                    CIRCLE2D c(pC4, r1);
                    lstCircle.push_back(c);
                }
            }
        }
    }

    if (lstCircle.empty())
    {
#ifdef _DEBUG_LLL
        DB_PRINT(_T("SolveTheApollonius fail(%d) No Ans\n"), calcCounter);

#endif
        return false;
    }

    for (CIRCLE2D& c : lstCircle)
    {
        POINT2D aP = c.GetCenter();
        double  dR = c.GetRadius();

        aP.dX = aP.dX + du;
        aP.dY = aP.dY + dv;

        POINT2D pRet;
        pRet.dX = aP.dX * dCos - aP.dY * dSin;
        pRet.dY = aP.dX * dSin + aP.dY * dCos;

        CIRCLE2D ans(pRet, dR);
        pLstAns->push_back(ans);
    }

    return true;
}


bool CActionCircle::_CreatePPP(CIRCLE2D* pC,
    const POINT2D& p1,
    const POINT2D& p2,
    const POINT2D& p3)
{

    //同一点、一直線上は不可
    /*
    solve((xa-x)^2+(ya-y)^2-r^2=0, (xb-x)^2+(yb-y)^2-r^2=0,(xc-x)^2+(yc-y)^2-r^2=0],[x,y,r])

    (%o1)	[[x=\-((yb\-ya)*yc^2+(\-yb^2+ya^2\-xb^2+xa^2)*yc+ya*yb^2+(\-ya^2+xc^2\-xa^2)*yb+(xb^2\-xc^2)*ya)/
             ((2*xb\-2*xa)*yc+(2*xa\-2*xc)*yb+(2*xc\-2*xb)*ya),
             
             y=((xb\-xa)*yc^2+(xa\-xc)*yb^2+(xc\-xb)*ya^2+(xb\-xa)*xc^2+(xa^2\-xb^2)*xc+xa*xb^2\-xa^2*xb)/
             ((2*xb\-2*xa)*yc+(2*xa\-2*xc)*yb+(2*xc\-2*xb)*ya)                 ....


    */

    double xa = p1.dX;
    double xb = p2.dX;
    double xc = p3.dX;
    double ya = p1.dY;
    double yb = p2.dY;
    double yc = p3.dY;

    double xa2 = xa*xa;
    double xb2 = xb*xb;
    double xc2 = xc*xc;
    double ya2 = ya*ya;
    double yb2 = yb*yb;
    double yc2 = yc*yc;

    double k= 2*((xb-xa)*yc+(xa-xc)*yb+(xc-xb)*ya);

    if (fabs(k) < NEAR_ZERO)
    {
        return false;
    }

    POINT2D ptC;
    ptC.dX = -((yb-ya)*yc2+(-yb2+ya2-xb2+xa2)*yc+ya*yb2+(-ya2+xc2-xa2)*yb+(xb2-xc2)*ya)/k;
    ptC.dY = ((xb-xa)*yc2+(xa-xc)*yb2+(xc-xb)*ya2+(xb-xa)*xc2+(xa2-xb2)*xc+xa*xb2-xa2*xb)/k;

    double r1 = ptC.Distance(p1);
    double r2 = ptC.Distance(p2);
    double r3 = ptC.Distance(p3);

    double dR  = (r1 + r2 + r3) / 3.0 ;

#ifdef _DEBUG_LLL
    double da1 = (xa-ptC.dX)*(xa-ptC.dX)+(ya-ptC.dY)*(ya-ptC.dY)-dR*dR;
    double da2 = (xb-ptC.dX)*(xb-ptC.dX)+(yb-ptC.dY)*(yb-ptC.dY)-dR*dR;
    double da3 = (xc-ptC.dX)*(xc-ptC.dX)+(yc-ptC.dY)*(yc-ptC.dY)-dR*dR;
   // STD_ASSERT(fabs(da1)<NEAR_ZERO);
   // STD_ASSERT(fabs(da2)<NEAR_ZERO);
   // STD_ASSERT(fabs(da3)<NEAR_ZERO);
    DB_PRINT(_T(" AnsCheck %f, %f, %f,\n"), da1, da2,da3);
#endif

    pC->SetCenter(ptC);
    pC->SetRadius(dR);
    return true;
}


bool CActionCircle::_CreateCCC(std::vector<CIRCLE2D>* pLstCircle,
    std::shared_ptr<CDrawingCircle> c1,
    std::shared_ptr<CDrawingCircle> c2,
    std::shared_ptr<CDrawingCircle> c3)
{

    pLstCircle->clear();
    for (int cnt = 0; cnt < 8; cnt++)
    {
        std::vector<CIRCLE2D> lstAns;
        bool bRet = SolveTheApollonius(&lstAns,
            cnt,
            *c1->GetCircleInstance(),
            *c2->GetCircleInstance(),
            *c3->GetCircleInstance());

        if (bRet)
        {
            for (CIRCLE2D c : lstAns)
            {
                pLstCircle->push_back(c);
            }
        }
    }
    return  (!pLstCircle->empty());
}


bool CActionCircle::_MovePoint3_L_L(
    POINT2D pt2d,
    bool bSnap,
    SNAP_DATA* pSnap,
    std::shared_ptr<CDrawingLine> l1,
    std::shared_ptr<CDrawingLine> l2)
{
    auto vv = m_pView->GetViewValue();
    if (vv != *m_psVvHyp)
    {
        *m_psVvHyp = vv;
        m_psHypabora.clear();
        _CreateAux_L_L(l1, l2);
    }

    CDrawMarker* pMarker= m_pView->GetMarker();
    CDrawMarker::MARKER_DATA marker;

    POINT2D pt1 = m_pLine[0]->NearPoint(pt2d);

    double dMin = m_pView->GetSnapMinLength()/2.0;
    CIRCLE2D cr;
    cr.SetRadius(dMin);
    std::vector<POINT2D> lstPoint;

    POINT2D ptCenter;
    bool bUseLine0 = true;
    POINT2D ptOnLine;
    const bool bOnlineFalse = false;
    int iMethod = 0;
    if (m_pLine[1])
    {
        POINT2D pt2 = m_pLine[1]->NearPoint(pt2d);

        if (pt1.Distance(pt2d) > pt2.Distance(pt2d))
        {
            ptCenter = m_pLine[1]->NearPoint(pt2d);
            bUseLine0 = false;
            l1->NearPoint(&ptOnLine, ptCenter, bOnlineFalse);
            cr.SetCenter(ptCenter);
            cr.Intersect(*m_pLine[1].get(), &lstPoint, bOnlineFalse, DIR_CIRCLE_TOLERANCE_MAX);
            iMethod = 1;
        }
    }
    else
    {
        iMethod = 2; //平行
    }

    if(bUseLine0)
    {
        ptCenter = m_pLine[0]->NearPoint(pt2d);
        cr.SetCenter(ptCenter);
        l1->NearPoint(&ptOnLine, ptCenter, bOnlineFalse);
        cr.Intersect(*m_pLine[0].get(), &lstPoint, bOnlineFalse, DIR_CIRCLE_TOLERANCE_MAX);
    }

    CIRCLE2D crRet;
    bool bSnapOrg = bSnap;
    if (pSnap->eSnapType != VIEW_COMMON::SNP_NONE)
    {
        bSnap = false;
    }

    crRet = _CalcGoodStopPoint_L_L( ptCenter, 
        iMethod,
        bSnap,
        l1->GetLineInstance(),
        l2->GetLineInstance(), lstPoint);

    ptCenter = crRet.GetCenter();
    double dR = crRet.GetRadius();


    //半径を表示する
    StdStringStream   strmLen;
    strmLen << GET_STR(STR_SNAP_2LINE_CONTACT);
    if ((pSnap->eSnapType != VIEW_COMMON::SNP_NONE) && bSnapOrg)
    {
        strmLen << _T("-")<< GetSnapName(pSnap->eSnapType, false) <<  _T("\n");
    }
    else
    {
        strmLen <<  _T("\n");
    }
    strmLen << _T(" ") << GET_STR(STR_PRO_RAD) << _T(":")
        << CDrawingObject::GetValText(dR, true, DRAW_CONFIG->iDimLengthPrecision);
    pSnap->strSnap = strmLen.str();

    auto pDrawCircle = std::dynamic_pointer_cast<CDrawingCircle>(m_psAddObj);
    pDrawCircle->SetVisible(true);
    pDrawCircle->SetRadius(dR);
    pDrawCircle->SetCenter(ptCenter);
    marker.eType = CDrawMarker::MARKER_CROSS;
    marker.eSnap = VIEW_COMMON::SNP_CROSS_POINT;
    pMarker->AddMarker(marker, ptCenter);

    _AddAux(pMarker);

    //-----------
    //補助線
    //-----------
    auto pLine = CreateLine(&m_psAddtionalObj[2]);
    pLine->SetPoint(ptCenter, pt2d);
    pMarker->AddObject(m_psAddtionalObj[2]);
    //-----------

    return true;
}

bool CActionCircle::_MovePoint3AddSnap(std::list<SNAP_DATA>* pSnapList, CDrawingObject*   pObj)
{
    return false;
}


bool CActionCircle::_MovePoint3(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{

    POINT ptSel = posMouse.ptSel;
    UINT nFlags = posMouse.nFlags;


    //!< スナップ点取得
    bool bIgnoreSnap = false;


    CPartsDef* pDef = m_pView->GetPartsDef();
    if (!pDef)
    {
        return false;
    }

    pDef->RedrawWait();  //TODO:どうする？
    int iLayerId = m_pView->GetCurrentLayerId();

    CDrawMarker* pMarker= m_pView->GetMarker();
    pMarker->Clear();
    pMarker->ClearObject();
    pDef->ClearMouseOver();
    pDef->ClearMouseEmphasis();
    pDef->ClearMouseConnectable();


    DWORD dwSnapType = DRAW_CONFIG->GetSnapType();
    dwSnapType &= (~VIEW_COMMON::SNP_ON_LINE);


    SNAP_DATA snap;
    StdString sVal;
    bool bSnap = true;
    bool bValInput = false;

    sVal = m_pView->GetInputValue();
    if (sVal !=_T(""))
    {
        bool bRet = false;
        if (m_strOldVal != sVal)
        {
            bRet = InputValue(sVal);
            m_pView->UpdateInput(bRet);
            if (bRet)
            {
                m_strOldVal = sVal;
            }
        }

        double dR;
        if (!m_psSelectCircles.empty())
        {
            dR = m_psSelectCircles[0]->GetRadius();
            for (auto pObj : m_psSelectCircles)
            {
                pMarker->AddObject(pObj);
            }
            bValInput = true;
        }

        if (bValInput)
        {
            //半径を表示する
            StdStringStream   strmLen;
            
            if (m_selSts & SS_SELECT_CIRCLE_CIRCLE)
            {
                strmLen << GET_STR(STR_SNAP_2CIRCLE_CONTACT) << _T("\n");
            }
            else if (m_selSts & SS_SELECT_CIRCLE_LINE)
            {
                strmLen << GET_STR(STR_SNAP_LINE_CIRCLE_CONTACT) << _T("\n");
            }
            else if (m_selSts & SS_SELECT_LINE_LINE)
            {
                strmLen << GET_STR(STR_SNAP_2LINE_CONTACT) << _T("\n");
            }
            
            strmLen << _T(" ") << GET_STR(STR_PRO_RAD) << _T(":")
                << CDrawingObject::GetValText(dR, true, DRAW_CONFIG->iDimLengthPrecision);
            snap.strSnap = strmLen.str();
        }
    }


    if (nFlags & MK_SHIFT)
    {
        //スナップ無視
        bIgnoreSnap = true;
        bSnap = false;
    }




    POINT2D  pt2d;
    m_pView->ConvScr2World(&pt2d, ptSel, iLayerId);


    std::shared_ptr<CDrawingObject>  pObj1;
    std::shared_ptr<CDrawingObject>  pObj2;
    //==================================
    DRAWING_TYPE eFirstObjectType = DT_NONE;
    DRAWING_TYPE eSecondObjectType = DT_NONE;
    if (!bValInput)
    {
        if (!bIgnoreSnap)
        {
            DWORD dwSnapNew = (dwSnapType & (~VIEW_COMMON::SNP_ON_LINE));
            //------------------------------------
            // 円そののものを選択するため、中心点を選択し続ける
            // KeepSnapCenter処理が邪魔になる。そのため 
            // 一時的に円のKeepSnapCenter処理を停止する
            // (円以外は、KeepSnapで問題ない) 
            // TODO:もう少し良いやり方を検討する
            //------------------------------------
            dwSnapNew =   (dwSnapType & (~VIEW_COMMON::SNP_CIRCLE));
            dwSnapNew =   (dwSnapNew  & (~VIEW_COMMON::SNP_ELLIPSE));
            DRAW_CONFIG->SetSnapType(dwSnapNew); 
            //--------------------


            namespace  PLC = std::placeholders;
            bool bSnap;
            std::list<SNAP_DATA> lstAddSnap;
            bSnap = m_pView->GetSnapPointWithAddFunc(
                &lstAddSnap,
                &snap,
                dwSnapNew,
                ptSel,
                std::bind(&CActionCircle::_MovePoint3AddSnap, this, PLC::_1, PLC::_2)
            );

            pObj1 = pDef->GetObjectById(snap.iObject1);
            pObj2 = pDef->GetObjectById(snap.iObject2);

            pt2d = snap.pt;

            if ((pObj1 != NULL) &&
                (pObj2 != NULL))
            {
                //交点
                pDef->SetMouseEmphasis(pObj1);
                pDef->SetMouseEmphasis(pObj2);

                CDrawMarker::MARKER_DATA marker;
                marker.eType = CDrawMarker::MARKER_CROSS;
                marker.eSnap = VIEW_COMMON::SNP_CROSS_POINT;
                pMarker->AddMarker(marker, snap.pt);
            }


            if ((pObj1 != NULL) &&
                (pObj2 == NULL))
            {
                m_pView->SetMarkerWithAddSnapList(pObj1.get(), &lstAddSnap, NULL, false /*bIgnoreOffLineObject*/);
                pDef->SetMouseEmphasis(pObj1);
            }
        }

        auto pDrawCircle = std::dynamic_pointer_cast<CDrawingCircle>(m_psAddObj);
        pMarker->AddObject(m_psAddObj);


        if (m_pSelObj[0] != NULL)
        {
            eFirstObjectType = m_pSelObj[0]->GetType();
        }
        if (m_pSelObj[1] != NULL)
        {
            eSecondObjectType = m_pSelObj[1]->GetType();
        }


        if (eFirstObjectType == DT_LINE)
        {
            auto l = std::dynamic_pointer_cast<CDrawingLine>(m_pSelObj[0]);

            if (eSecondObjectType == DT_LINE)
            {
                auto l2 = std::dynamic_pointer_cast<CDrawingLine>(m_pSelObj[1]);

                bool b3Line = false;
                if ((pObj1 != NULL) &&
                    (pObj2 == NULL))
                {
                    int iMarkerId = pMarker->MouseMove(posMouse.ptSel);
                    if ((iMarkerId == -1))
                    {
                        if (pObj1->GetType() == DT_LINE)
                        {
                            //-------------------
                            //３直線に接する円
                            //-------------------
                            auto l3 = std::dynamic_pointer_cast<CDrawingLine>(pObj1);

                            m_psSelectCircles.clear();

                            std::vector<CIRCLE2D> lstCircle;
                            bool bRet = _CreateLLL(&lstCircle, l, l2, l3);
                            CIRCLE2D  crSel;
                            if (bRet)
                            {
                                snap.eSnapType = VIEW_COMMON::SNP_LINE;
                                pDef->ClearMouseEmphasis();
                                pDef->SetMouseOver(pObj1);
                                b3Line = true;

                                pDrawCircle->SetVisible(false);
                                for (CIRCLE2D cr : lstCircle)
                                {
                                    auto dc = std::make_shared<CDrawingCircle>();
                                    dc->SetCenter(cr.GetCenter());
                                    dc->SetRadius(cr.GetRadius());
                                    dc->SetColor(DRAW_CONFIG->crAdditional);
                                    dc->SetLineType(PS_DOT);
                                    m_psSelectCircles.push_back(dc);
                                    pMarker->AddObject(dc);
                                }

                                StdStringStream   strmLen;
                                strmLen << GET_STR(STR_SNAP_3LINE_CONTACT) << _T("\n");
                                snap.strSnap = strmLen.str();
                            }
                            //-------------------
                        }
                    }
                }

                if (!b3Line)
                {
                    _MovePoint3_L_L(pt2d, bSnap, &snap, l, l2);
                }
            }
            else if (eSecondObjectType == DT_CIRCLE)
            {
                auto c = std::dynamic_pointer_cast<CDrawingCircle>(m_pSelObj[1]);
                _MovePoint3_L_C(pt2d, bSnap, false, &snap, l, c);

            }
        }
        if (eFirstObjectType == DT_CIRCLE)
        {
            auto c1 = std::dynamic_pointer_cast<CDrawingCircle>(m_pSelObj[0]);
            if (eSecondObjectType == DT_LINE)
            {
                auto l = std::dynamic_pointer_cast<CDrawingLine>(m_pSelObj[1]);
                _MovePoint3_L_C(pt2d, bSnap, false, &snap, l, c1);

            }
            else if (eSecondObjectType == DT_CIRCLE)
            {
                auto c2 = std::dynamic_pointer_cast<CDrawingCircle>(m_pSelObj[1]);


                bool b3Circle = false;
                if ((pObj1 != NULL) &&
                    (pObj2 == NULL))
                {
                    int iMarkerId = pMarker->MouseMove(posMouse.ptSel);
                    if ((iMarkerId == -1))
                    {
                        if (pObj1->GetType() == DT_CIRCLE)
                        {
                            //------------------
                            //３円に接する円
                            //------------------
                            auto c3 = std::dynamic_pointer_cast<CDrawingCircle>(pObj1);


                            m_psSelectCircles.clear();

                            std::vector<CIRCLE2D> lstCircle;
                            bool bRet = _CreateCCC(&lstCircle, c1, c2, c3);
                            if (bRet)
                            {
                                snap.eSnapType = VIEW_COMMON::SNP_CIRCLE;
                                pDef->ClearMouseEmphasis();
                                pDef->SetMouseOver(pObj1);
                                pDrawCircle->SetVisible(false);
                                b3Circle = true;

                                double dMin = DBL_MAX;
                                for (CIRCLE2D cr : lstCircle)
                                {
                                    auto dc = std::make_shared<CDrawingCircle>();
                                    dc->SetCenter(cr.GetCenter());
                                    dc->SetRadius(cr.GetRadius());
                                    dc->SetColor(DRAW_CONFIG->crAdditional);
                                    dc->SetLineType(PS_DOT);
                                    m_psSelectCircles.push_back(dc);
                                    pMarker->AddObject(dc);
                                }

                                StdStringStream   strmLen;
                                strmLen << GET_STR(STR_SNAP_3CIRCLE_CONTACT) << _T("\n");
                                snap.strSnap = strmLen.str();
                            }
                        }
                    }
                }

                if (!b3Circle)
                {
                    _MovePoint3_C_C(pt2d, bSnap, false, &snap, c1, c2);
                }

            }
        }
    }

    pMarker->MouseMove(posMouse.ptSel);

    //==================================
    // 円のKeepSnapCenter処理を戻す
    // m_pView->SetMarker内でもGetSnapListを使用するため
    // ここで解除する
    DRAW_CONFIG->SetSnapType(dwSnapType);
    //==================================


    if (bValInput)
    {
        double dMin = DBL_MAX;
        std::shared_ptr<CDrawingCircle> pSelCircle;
        for (auto pObj : m_psSelectCircles)
        {
            double d;
            d = pObj->GetCircleInstance()->NearPoint(pt2d).Distance(pt2d);
            if (dMin > d)
            {
                pSelCircle = pObj;
                dMin = d;
            }
        }

        if (pSelCircle)
        {
            auto pDrawCircle = std::dynamic_pointer_cast<CDrawingCircle>(m_psAddObj);
            pDrawCircle->SetVisible(true);
            pDrawCircle->SetCenter(pSelCircle->GetCenter());
            pDrawCircle->SetRadius(pSelCircle->GetRadius());
            pDef->SetMouseOver(pDrawCircle);
        }
    }


    if (snap.strSnap.empty()) 
    {
        m_pView->ResetToolTipText();
    }
    else
    {
        m_pView->SetToolTipText(snap.strSnap.c_str());
    }

    pDef->DrawDragging(m_pView);

    return true;
}



bool CActionCircle::_MovePoint4(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{

    POINT ptSel = posMouse.ptSel;
    UINT nFlags = posMouse.nFlags;


    //!< スナップ点取得
    bool bIgnoreSnap = false;


    CPartsDef* pDef = m_pView->GetPartsDef();
    if (!pDef)
    {
        return false;
    }

    pDef->RedrawWait();  //TODO:どうする？
    int iLayerId = m_pView->GetCurrentLayerId();

    CDrawMarker* pMarker= m_pView->GetMarker();
    pMarker->Clear();
    pMarker->ClearObject();
    pDef->ClearMouseOver();
    pDef->ClearMouseEmphasis();
    pDef->ClearMouseConnectable();


    DWORD dwSnapType = DRAW_CONFIG->GetSnapType();
    dwSnapType &= (~VIEW_COMMON::SNP_ON_LINE);


    CDrawMarker::MARKER_DATA marker;
    SNAP_DATA snap;
    StdString sVal;

    m_psAddObj->SetVisible(true);
    auto pDrawCircle = std::dynamic_pointer_cast<CDrawingCircle>(m_psAddObj);
    pMarker->AddObject(m_psAddObj);


    DRAWING_TYPE eObj1 = DT_NONE;
    DRAWING_TYPE eObj2 = DT_NONE;
    DRAWING_TYPE eObj3 = DT_NONE;

    if (m_pSelObj[0] != NULL)
    {
        eObj1 = m_pSelObj[0]->GetType();
    }

    if (m_pSelObj[1] != NULL)
    {
        eObj2 = m_pSelObj[1]->GetType();
    }

    if (m_pSelObj[2] != NULL)
    {
        eObj3 = m_pSelObj[2]->GetType();
    }

    POINT2D  pt2d;
    m_pView->ConvScr2World(&pt2d, ptSel, iLayerId);

    pMarker->MouseMove(posMouse.ptSel);

    if (!m_psSelectCircles.empty())
    {
        for (auto pObj : m_psSelectCircles)
        {
            pMarker->AddObject(pObj);
        }
    }


    double dMin = DBL_MAX;
    std::shared_ptr<CDrawingCircle> pSelCircle;
    for (auto pObj : m_psSelectCircles)
    {
        double d;
        d = pObj->GetCircleInstance()->NearPoint(pt2d).Distance(pt2d);
        if (dMin > d)
        {
            pSelCircle = pObj;
            dMin = d;
        }
    }

    if (pSelCircle)
    {
        pDrawCircle->SetVisible(true);
        auto pDrawCircle = std::dynamic_pointer_cast<CDrawingCircle>(m_psAddObj);
        pDrawCircle->SetCenter(pSelCircle->GetCenter());
        pDrawCircle->SetRadius(pSelCircle->GetRadius());
        pDef->SetMouseOver(pDrawCircle);
    }


    StdStringStream   strmLen;
    if ((eObj1 == DT_LINE) && (eObj2 == DT_LINE) && (eObj3 == DT_LINE))
    {
        strmLen << GET_STR(STR_SNAP_3LINE_CONTACT) << _T("\n");
    }

    if ((eObj1 == DT_CIRCLE) && (eObj2 == DT_CIRCLE) && (eObj3 == DT_CIRCLE))
    {
        strmLen << GET_STR(STR_SNAP_3CIRCLE_CONTACT) << _T("\n");
    }


    snap.strSnap = strmLen.str();

    if (snap.strSnap.empty()) 
    {
        m_pView->ResetToolTipText();
    }
    else
    {
        m_pView->SetToolTipText(snap.strSnap.c_str());
    }

    pDef->DrawDragging(m_pView);

    return true;
}
/**
 * @brief   マウスクリック（円選択時)
 * @param   [in]     pt2D    マウスカーソル位置
 * @param   [in]     pObj    カーソル位置のオブジェクト
 * @param   [in]     bMarker  マーカー選択
 * @retval  true  
 * @note	
 */
bool CActionCircle::_SelCircle(MOUSE_MOVE_POS posMouse, 
                               SEL_ACTION eAction, 
                               POINT2D pt2D, 
                               std::shared_ptr<CDrawingObject> pObj,
                               bool bMarker)
{
    bool bRet = false;
    if (m_iClickCnt == 0)
    {
        bRet = _SelCircle1(posMouse, eAction, pt2D, pObj, bMarker);
    }
    else if(m_iClickCnt == 1)
    {
        bRet = _SelCircle2(posMouse, eAction, pt2D, pObj, bMarker);
    }
    else if(m_iClickCnt == 2)
    {
        bRet = _SelCircle3(posMouse, eAction, pt2D, pObj, bMarker);
    }
    else if(m_iClickCnt == 3)
    {
        bRet = _SelCircle4(posMouse, eAction, pt2D, pObj, bMarker);
    }
    return bRet;

}


bool CActionCircle::_SelCircle1(MOUSE_MOVE_POS posMouse, 
                                SEL_ACTION eAction,
                                POINT2D pt2D,
                                std::shared_ptr<CDrawingObject> pObj,
                                bool bMarker)
{
 
    POINT ptSel = posMouse.ptSel;
    UINT nFlags = posMouse.nFlags;


    //!< スナップ点取得
    bool bIgnoreSnap = false;


    CPartsDef* pDef = m_pView->GetPartsDef();


    m_pSelObj[0] = NULL;
    bool bSelPoint = false;

    POINT2D ptObj;
    if (!pObj)
    {
        m_pSelObj[0].reset();
        m_ptSel[0] = pt2D;
        bSelPoint = true;
    }
    else
    {
        DRAWING_TYPE eType = pObj->GetType();
        bool bSelObject = false;
        switch (eType)
        {
        case DT_LINE:
        case DT_CIRCLE:
            if (!bMarker)
            {
                m_pSelObj[0] = pObj;
                pDef->SelectDrawingObject(pObj->GetId());
                bSelObject = true;

                if (eType == DT_LINE) { m_selSts = SS_SELECT_LINE; }
                else if (eType == DT_CIRCLE) { m_selSts = SS_SELECT_CIRCLE; }
            }
            break;
        }

        if (!bSelObject)
        {
            m_pSelObj[0].reset();
            bSelPoint = true;
            if (!(m_selSts & SS_CREATE_CIRCLE))
            {
                m_selSts = SS_SELECT_POINT;
                m_pView->SetMarker(pObj.get());
            }
        }
    }

    if (m_selSts & SS_CREATE_CIRCLE)
    {
        //円の追加
        auto pCircle = std::dynamic_pointer_cast<CDrawingCircle>(m_psAddObj);
        _AddCircle(pCircle);
        m_pView->SetComment(GET_SYS_STR(STR_SEL_CIRCLE_CENTER));

    }
    else
    {
        //-----------------------------------------
        m_ptSel[0] = pt2D;

        if (m_psAddObj)
        {
            auto pDrawCircle = std::dynamic_pointer_cast<CDrawingCircle>(m_psAddObj);
            pDrawCircle->SetVisible(true);
            pDrawCircle->SetCenter(pt2D);

            if ((m_selSts & SS_SELECT_LINE) ||
                (m_selSts & SS_SELECT_CIRCLE))
            {
                m_pView->SetComment(GET_SYS_STR(STR_SEL_CIRCLE_OBJ));
            }
            else
            {
                m_pView->SetComment(GET_SYS_STR(STR_SEL_CIRCLE_CENTER1));
            }
        }
        m_iClickCnt = 1;
    }
    return true;   
}


bool CActionCircle::CreateHyperboraLine(
    std::vector<std::shared_ptr<CDrawingCompositionLine>>* pDraw,
    const std::shared_ptr<HYPERBOLA2D> pHyp)
{
    int iLayerId = m_pView->GetCurrentLayerId();
    RECT2D viewRect = m_pView->GetViewArea(iLayerId);

    std::vector< std::vector<POINT2D>> lstPoints;

    pHyp->DivideLine(&lstPoints, viewRect, NEAR_ZERO);

    for (auto lst : lstPoints)
    {
        auto pComp = std::make_shared<CDrawingCompositionLine>();
        pComp->CreateMultiLine(lst);
        pComp->SetColor(DRAW_CONFIG->crAdditional);
        pComp->SetLineType(PS_DOT);
        pDraw->push_back(pComp);
    }
    return true;
}


bool CActionCircle::CreateParaboraLine(
    std::vector<std::shared_ptr<CDrawingCompositionLine>>* pDraw,
    const std::shared_ptr<PARABOLA2D> pPara)
{
    int iLayerId = m_pView->GetCurrentLayerId();
    RECT2D viewRect = m_pView->GetViewArea(iLayerId);

    std::vector< std::vector<POINT2D>> lstPoints;

    pPara->DivideLine(&lstPoints, viewRect, NEAR_ZERO);

    for (auto lst : lstPoints)
    {
        auto pComp = std::make_shared<CDrawingCompositionLine>();
        pComp->CreateMultiLine(lst);
        pComp->SetColor(DRAW_CONFIG->crAdditional);
        pComp->SetLineType(PS_DOT);
        pDraw->push_back(pComp);
    }
    return true;
}

//２円の中心を通る直線と円の交点を求め相手側の中心に
//近い点と遠い点を求める
bool CActionCircle::_GetCrossPoint(POINT2D* p1Near,
    POINT2D* p1Far,
    POINT2D* p2Near,
    POINT2D* p2Far,
    std::shared_ptr<CDrawingCircle> c1,
    std::shared_ptr<CDrawingCircle> c2)
{

    POINT2D p = c1->GetCenter();
    POINT2D q = c2->GetCenter();
    LINE2D l(p, q);

    std::vector<POINT2D> lp;
    std::vector<POINT2D> lq;
    c1->GetCircleInstance()->Intersect(l, &lp);
    c2->GetCircleInstance()->Intersect(l, &lq);

    if ((lp.size() != 2) || (lq.size() != 2))
    {
        return false;
    }

    POINT2D ptCenter = (p + q) / 2.0;
    double p0Min;
    double p0Max;
    double p1Min;
    double p1Max;


    l.PosPram(&p0Min, p, lp[0]);
    l.PosPram(&p0Max, p, lp[1]);
    l.PosPram(&p1Min, p, lq[0]);
    l.PosPram(&p1Max, p, lq[1]);


    if (p0Max < p0Min)
    {
        std::swap(p0Max, p0Min);
        std::swap(lp[0], lp[1]);
    }

    if (p1Max < p1Min)
    {
        std::swap(p1Max, p1Min);
        std::swap(lq[0], lq[1]);
    }

    if (p0Max <= p1Min)
    {
       //  P0min(p0)   P0Max  P1Min(q0)    P1Max
       //   |            |      |            |
       //   +------------+      +------------+
        *p1Far  = lp[0];
        *p1Near = lp[1];
        *p2Near = lq[0];
        *p2Far  = lq[1];
        return true;
    }
    
    if (p1Max <= p0Min)
    {
        //  P1Min(q0)   P1Max   P0min(p0)     P0Max
        //   |            |      |            |
        //   +------------+      +------------+


        *p1Far  = lp[1];
        *p1Near = lp[0];
        *p2Near = lq[1];
        *p2Far  = lq[0];
        return true;
    }


    if ((p1Max >= p0Max) &&
        (p1Min <= p0Max))
    {   //  P0min       P1Min  P0Max        P1Max
        //   |            |      |            |
        //   +------------+------+------------+
        if ((p1Max > p0Min) &&
            (p1Min <= p0Min))
        {
            double d0 = lq[0].Distance(lp[0]);
            double d1 = lq[1].Distance(lp[1]);

            if (d0 <= d1)
            {
                *p1Far  = lp[1];
                *p1Near = lp[0];
                *p2Near = lq[0];
                *p2Far  = lq[1];
                return true;
            }
            else
            {
                *p1Far  = lp[0];
                *p1Near = lp[1];
                *p2Near = lq[1];
                *p2Far  = lq[0];
                return true;
            }
        }
        else
        {
            *p1Far  = lp[0];
            *p1Near = lp[1];
            *p2Near = lq[0];
            *p2Far  = lq[1];
            return true;
        }
    }
    return false;
}

bool CActionCircle::_SelCircle2(MOUSE_MOVE_POS posMouse,
    SEL_ACTION eAction,
    POINT2D pt2D,
    std::shared_ptr<CDrawingObject> pObj,
    bool bMarker)
{
    CPartsDef*   pDef = m_pView->GetPartsDef();
    CDrawMarker*  pMarker = m_pView->GetMarker();

    if(!bMarker)
    {
        if ((m_pSelObj[0] == pObj) && pObj)
        {
            //同一Objectを選択
            return false;
        }
        else if(m_pSelObj[0])
        {
            DRAWING_TYPE eTypeFirst = m_pSelObj[0]->GetType();
            if(pObj)
            {
                DRAWING_TYPE eTypeSecond = pObj->GetType();
                switch (eTypeFirst)
                {
                case DT_LINE:
                case DT_CIRCLE:
                    m_pSelObj[1] = pObj;
                    pDef->SelectDrawingObject(pObj->GetId());
                    m_iClickCnt++;
                    *m_psVvHyp = m_pView->GetViewValue();

                    if ((pObj->GetType() == DT_CIRCLE) &&
                        (eTypeFirst == DT_CIRCLE))
                    {
                        //2円に接する円の中心を通る双曲線を設定する
                        //https://www.geogebra.org/m/UA3BwjAb#material/AmrNDbcu 参照
                        //交差する円と内包する円の場合も考慮する

                        auto c1 = std::dynamic_pointer_cast<CDrawingCircle>(m_pSelObj[0]);
                        auto c2 = std::dynamic_pointer_cast<CDrawingCircle>(m_pSelObj[1]);

                        m_selSts = SS_SELECT_CIRCLE_CIRCLE;


                        bool bRet;
                        bRet = _CreateAux_C_C(c1, c2);
                        if (!bRet)
                        {
                            return false;
                        }
                    }

                    else if ((pObj->GetType() == DT_CIRCLE) && (eTypeFirst == DT_LINE))
                    {
                        //円と直線に接する円の中心を通る放物線を設定する
                        auto l = std::dynamic_pointer_cast<CDrawingLine>(m_pSelObj[0]);
                        auto c = std::dynamic_pointer_cast<CDrawingCircle>(m_pSelObj[1]);

                        m_selSts = SS_SELECT_CIRCLE_LINE;

                        bool bRet;
                        bRet = _CreateAux_L_C(l, c);
                        if (!bRet)
                        {
                            return false;
                        }
                    }
                    else if ((pObj->GetType() == DT_LINE) && (eTypeFirst == DT_CIRCLE))
                    {
                        auto c = std::dynamic_pointer_cast<CDrawingCircle>(m_pSelObj[0]);
                        auto l = std::dynamic_pointer_cast<CDrawingLine>(m_pSelObj[1]);

                        m_selSts = SS_SELECT_CIRCLE_LINE;

                        bool bRet;
                        bRet = _CreateAux_L_C(l, c);
                        if (!bRet)
                        {
                            return false;
                        }
                    }
                    else if ((pObj->GetType() == DT_LINE) && (eTypeFirst == DT_LINE))
                    {
                        auto l1 = std::dynamic_pointer_cast<CDrawingLine>(m_pSelObj[0]);
                        auto l2 = std::dynamic_pointer_cast<CDrawingLine>(m_pSelObj[1]);

                        m_selSts = SS_SELECT_LINE_LINE;

                        bool bRet;
                        bRet = _CreateAux_L_L(l1, l2);
                        if (!bRet)
                        {
                            return false;
                        }
                    }

                    _AddAux(pMarker);
                    return true;

                default:
                    //最初に選択できるObjectは線か円のみ
                    STD_ASSERT(eTypeFirst);
                    return false;
                }
            }
        }
    }


    if((bMarker) || (m_selSts & SS_CREATE_CIRCLE))
    {
        //円の追加
        auto pCircle = std::dynamic_pointer_cast<CDrawingCircle>(m_psAddObj);
        _AddCircle(pCircle);
        return true;
    }

    return false;   

}

bool CActionCircle::_SelCircle3(MOUSE_MOVE_POS posMouse,
    SEL_ACTION eAction,
    POINT2D pt2D,
    std::shared_ptr<CDrawingObject> pObj,
    bool bMarker)
{

    CPartsDef*   pDef = m_pView->GetPartsDef();
    CDrawMarker*  pMarker = m_pView->GetMarker();

    if (!bMarker && pObj)
    {
        if (((m_pSelObj[0] == pObj) && pObj) ||
            ((m_pSelObj[1] == pObj) && pObj))
        {
            //同一Objectを選択
            return false;
        }
        DRAWING_TYPE eObj1 = DT_NONE;
        DRAWING_TYPE eObj2 = DT_NONE;


        if (m_pSelObj[0] != NULL)
        {
            eObj1 = m_pSelObj[0]->GetType();
        }
      
        if (m_pSelObj[1] != NULL)
        {
            eObj2 = m_pSelObj[1]->GetType();
        }

        if ((eObj1 == DT_LINE) && (eObj2 == DT_LINE))
        {
            if(pObj->GetType() ==  DT_LINE)
            {
                auto l1 = std::dynamic_pointer_cast<CDrawingLine>(m_pSelObj[0]);
                auto l2 = std::dynamic_pointer_cast<CDrawingLine>(m_pSelObj[1]);
                auto l3 = std::dynamic_pointer_cast<CDrawingLine>(pObj);

                std::vector<CIRCLE2D> lstCircle;
                bool bRet = _CreateLLL(&lstCircle, l1, l2, l3);
                if (bRet)
                {
                    //3直線に接する円
                    m_pSelObj[2] = pObj;
                    pDef->SelectDrawingObject(pObj->GetId());
                    m_iClickCnt++;
                    m_psSelectCircles.clear();

                    for (CIRCLE2D cr : lstCircle)
                    {
                        auto dc = std::make_shared<CDrawingCircle>();
                        dc->SetCenter(cr.GetCenter());
                        dc->SetRadius(cr.GetRadius());
                        dc->SetColor(DRAW_CONFIG->crAdditional);
                        dc->SetLineType(PS_DOT);
                        m_psSelectCircles.push_back(dc);
                    }
                    m_pView->SetComment(GET_SYS_STR(STR_SEL_CIRCLE_LLL));
                    return true;   
                }
            }
        }

        else if ((eObj1 == DT_CIRCLE) && (eObj2 == DT_CIRCLE))
        {
            if(pObj->GetType() ==  DT_CIRCLE)
            {
                auto c1 = std::dynamic_pointer_cast<CDrawingCircle>(m_pSelObj[0]);
                auto c2 = std::dynamic_pointer_cast<CDrawingCircle>(m_pSelObj[1]);
                auto c3 = std::dynamic_pointer_cast<CDrawingCircle>(pObj);

                std::vector<CIRCLE2D> lstCircle;
                bool bRet = _CreateCCC(&lstCircle, c1, c2, c3);
                if (bRet)
                {
                    //3円に接する円
                    m_pSelObj[2] = pObj;
                    pDef->SelectDrawingObject(pObj->GetId());
                    m_iClickCnt++;
                    m_psSelectCircles.clear();

                    for (CIRCLE2D cr : lstCircle)
                    {
                        auto dc = std::make_shared<CDrawingCircle>();
                        dc->SetCenter(cr.GetCenter());
                        dc->SetRadius(cr.GetRadius());
                        dc->SetColor(DRAW_CONFIG->crAdditional);
                        dc->SetLineType(PS_DOT);
                        m_psSelectCircles.push_back(dc);
                    }
                    m_pView->SetComment(GET_SYS_STR(STR_SEL_CIRCLE_CCC));
                    return true;   
                }
            }
        }
    }

    //円の追加
    auto pCircle = std::dynamic_pointer_cast<CDrawingCircle>(m_psAddObj);

    if (pCircle)
    {
        _AddCircle(pCircle);
        m_pView->ClearInput();
        m_pView->SetComment(GET_SYS_STR(STR_SEL_CIRCLE_CENTER));
        return true;
    }

    return false;   
}


bool CActionCircle::_SelCircle4(MOUSE_MOVE_POS posMouse,
    SEL_ACTION eAction,
    POINT2D pt2D,
    std::shared_ptr<CDrawingObject> pObj,
    bool bMarker)
{
    //円の追加
    auto pCircle = std::dynamic_pointer_cast<CDrawingCircle>(m_psAddObj);

    if (pCircle)
    {
        _AddCircle(pCircle);
        m_pView->ClearInput();
        m_pView->SetComment(GET_SYS_STR(STR_SEL_CIRCLE_CENTER));
        return true;
    }
    return false;   
}

//!< ３点を通る円
bool CActionCircle::_SelCircle3P(MOUSE_MOVE_POS posMouse,
    SEL_ACTION eAction,
    POINT2D pt2D,
    std::shared_ptr<CDrawingObject> pObj,
    bool bMarker)

{

    POINT ptSel = posMouse.ptSel;
    UINT nFlags = posMouse.nFlags;


    //!< スナップ点取得
    bool bIgnoreSnap = false;
    CPartsDef* pDef = m_pView->GetPartsDef();
    CDrawMarker* pMarker= m_pView->GetMarker();

    bool bRet = false;

    if (m_iClickCnt == 0)
    {
        if (bMarker)
        {
            m_ptSel[0] = pt2D;
            m_iClickCnt++;
            m_pView->SetComment(GET_SYS_STR(STR_SEL_3P_2));

        }
    }
    else if(m_iClickCnt == 1)
    {
        if (bMarker)
        {
            m_ptSel[1] = pt2D;
            m_iClickCnt++;
            m_pView->SetComment(GET_SYS_STR(STR_SEL_3P_3));
        }
    }
    else if(m_iClickCnt == 2)
    {
        auto pCircle = std::dynamic_pointer_cast<CDrawingCircle>(m_psAddObj);
        if (pCircle)
        {
            _AddCircle(pCircle);
            m_pView->SetComment(GET_SYS_STR(STR_SEL_3P_1));

        }
    }
    return true;
}


bool CActionCircle::_AddCircle(std::shared_ptr<CDrawingCircle> pCircle)
{
    if (!THIS_APP->GetDefaultObjectPriorty(DT_CIRCLE))
    {
        pCircle->SetColor(m_pView->GetCurrentLayer()->crDefault);
    }
    else
    {
        COLORREF cr = THIS_APP->GetDefaultObject(DT_CIRCLE)->GetColor();
        pCircle->SetColor(cr);
    }

    if (!THIS_APP->GetDefaultObjectPriorty(DT_CIRCLE))
    {
        pCircle->SetLineType(m_pView->GetCurrentLayer()->iLineType);
        pCircle->SetLineWidth(m_pView->GetCurrentLayer()->iLineWidth);
    }

    CPartsDef* pDef = m_pView->GetPartsDef();
    CUndoAction* pUndo = pDef->GetUndoAction();
    pCircle->SetVisible(true);
    pDef->RegisterObject(pCircle, true, false);
    pUndo->Add(UD_ADD, pDef, NULL, pCircle, true);
    pDef->Draw(pCircle->GetId());

    _ClearAction();

    m_iClickCnt = 0;
    return true;
}



/**
 *  @brief   入力値設定
 *  @param   [in]    strVal 入力値
 *  @retval  false エラー発生
 *  @note    必要であればエラー表示を行う
 *  @note    エラー発生で入力コンボボックスは選択状態になる      
 *  @note    エラーがない場合はコンボボックスに値が登録される
 */
bool CActionCircle::InputValue(StdString strVal)
{

    std::vector<StdString> lstInput;
    boost::algorithm::split( lstInput, strVal, boost::is_any_of(_T(",")));

    /*
      x,y,r
      r
    */

    if (m_selSts & SS_NO_SELECT)
    {
        if (lstInput.size() == 3)
        {
            POINT2D pt2D;
            double dR;
            if(!ExecuteString(&pt2D.dX, lstInput[0])){return false;}
            if(!ExecuteString(&pt2D.dY, lstInput[1])){return false;}
            if(!ExecuteString(&dR, lstInput[2])){return false;}

            auto pCreateObj = THIS_APP->CreateDefaultObject(DT_CIRCLE);
            auto defObj = std::dynamic_pointer_cast<CDrawingCircle>(pCreateObj);
            auto pCircle = std::make_shared<CDrawingCircle>(*(defObj.get()));


            pCircle->SetCenter(pt2D);
            pCircle->SetRadius(dR);

            _AddCircle(pCircle);

            return true;
        }
        else if (lstInput.size() == 1)
        {
            double dR;
            if(!ExecuteString(&dR, lstInput[0])){return false;}

            if (!m_psAddObj)
            {
                return false;
            }
            auto pCircle = std::dynamic_pointer_cast<CDrawingCircle>(m_psAddObj);
            if (pCircle)
            {
                pCircle->SetRadius(dR);
                return true;
            }
        }
    }
    else if (m_selSts & SS_SELECT_POINT)
    {
        if (lstInput.size() == 1)
        {
            double dR;
            if(!ExecuteString(&dR, lstInput[0])){return false;}

            if (!m_psAddObj)
            {
                return false;
            }
            auto pCircle = std::dynamic_pointer_cast<CDrawingCircle>(m_psAddObj);
            if (pCircle)
            {
                pCircle->SetCenter(m_ptSel[0]);
                pCircle->SetRadius(dR);
                _AddCircle(pCircle);
                return true;
            }
        }
    }
    else if (m_selSts & SS_SELECT_CIRCLE_LINE)
    {
        double dR;
        if (!ExecuteString(&dR, lstInput[0]))
        {
            return false; 
        }

        m_psSelectCircles.clear();

        std::shared_ptr<CDrawingLine> l;
        std::shared_ptr<CDrawingCircle> c;

        if (m_pSelObj[0]->GetType() == DT_LINE)
        {
            l = std::dynamic_pointer_cast<CDrawingLine>(m_pSelObj[0]);
            c = std::dynamic_pointer_cast<CDrawingCircle>(m_pSelObj[1]);
        }
        else
        {
            c = std::dynamic_pointer_cast<CDrawingCircle>(m_pSelObj[0]);
            l = std::dynamic_pointer_cast<CDrawingLine>(m_pSelObj[1]);
        }

        std::vector<POINT2D> lstCenter;

        if (!_CreateLC(&lstCenter, dR, l, c))
        {
            return false; 
        }

        for (POINT2D ptCenter : lstCenter)
        {
            auto dc = std::make_shared<CDrawingCircle>();
            dc->SetCenter(ptCenter);
            dc->SetRadius(dR);
            dc->SetColor(DRAW_CONFIG->crAdditional);
            dc->SetLineType(PS_DOT);
            m_psSelectCircles.push_back(dc);
        }
        return true;
    }
    else if (m_selSts & SS_SELECT_CIRCLE_CIRCLE)
    {
        if (lstInput.size() == 1)
        {
            double dR;
            if (!ExecuteString(&dR, lstInput[0]))
            {
                return false; 
            }

            m_psSelectCircles.clear();

            auto c1 = std::dynamic_pointer_cast<CDrawingCircle>(m_pSelObj[0]);
            auto c2 = std::dynamic_pointer_cast<CDrawingCircle>(m_pSelObj[1]);
            std::vector<POINT2D> lstCenter;

            if (!_CreateCC(&lstCenter, dR, c1, c2))
            {
                return false; 
            }

            for (POINT2D ptCenter : lstCenter)
            {
                auto dc = std::make_shared<CDrawingCircle>();
                dc->SetCenter(ptCenter);
                dc->SetRadius(dR);
                dc->SetColor(DRAW_CONFIG->crAdditional);
                dc->SetLineType(PS_DOT);
                m_psSelectCircles.push_back(dc);
            }
            return true;
        }
    }
    else if (m_selSts & SS_SELECT_LINE_LINE)
    {
    if (lstInput.size() == 1)
    {
        double dR;
        if (!ExecuteString(&dR, lstInput[0]))
        {
            return false; 
        }

        m_psSelectCircles.clear();

        auto l1 = std::dynamic_pointer_cast<CDrawingLine>(m_pSelObj[0]);
        auto l2 = std::dynamic_pointer_cast<CDrawingLine>(m_pSelObj[1]);
        std::vector<POINT2D> lstCenter;

        if (!_CreateLL(&lstCenter, dR, l1, l2))
        {
            return false; 
        }

        for (POINT2D ptCenter : lstCenter)
        {
            auto dc = std::make_shared<CDrawingCircle>();
            dc->SetCenter(ptCenter);
            dc->SetRadius(dR);
            dc->SetColor(DRAW_CONFIG->crAdditional);
            dc->SetLineType(PS_DOT);
            m_psSelectCircles.push_back(dc);
        }
        return true;
    }
    }
    return false;
}

/**
 *  動作クリア
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CActionCircle::_ClearAction()
{
    m_bMark = false;

    m_iClickCnt = 0;

    CDrawMarker*        pMarker = m_pView->GetMarker();
    CPartsDef*   pDef = m_pView->GetPartsDef();
    if (!pDef)
    {
        return;
    }

    pMarker->Clear();
    pMarker->ClearObject();
    pDef->RelSelect();

    pDef->Redraw();

    if (m_psAddObj)
    {
        m_psAddObj.reset();
    }
    m_psVvHyp->Clear();

    _ClearAux();

    m_selSts = SS_NO_SELECT;
    m_strOldVal = _T("");


    VIEW_MODE      eViewMode  = m_pView->GetViewMode();

    for (auto p : m_pSelObj)
    {
        p.reset();
    }

    pDef->ClearMouseOver();
    pDef->ClearMouseEmphasis();

    SelAction(eViewMode);

    if (eViewMode == VIEW_CIRCLE   )
    {
        m_pView->SetComment(GET_SYS_STR(STR_SEL_CIRCLE_CENTER));
    }
    else if (eViewMode == VIEW_CIRCLE_3P)
    {
        m_pView->SetComment(GET_SYS_STR(STR_SEL_3P_1));
    }
}


CActionCircle::E_TWO_CIRCLE_CONTACT CActionCircle::_GetContactType(
    POINT2D* pContact,
    std::shared_ptr<CDrawingCircle> c1,
    std::shared_ptr<CDrawingCircle> c2)
{
    POINT2D p1 = c1->GetCenter();
    POINT2D p2 = c2->GetCenter();

    double r1 = c1->GetRadius();
    double r2 = c2->GetRadius();

    double dL = p1.Distance(p2);

    int iRet = 0;
    if (fabs(r1 - r2) < NEAR_ZERO)
    {
        iRet = E_SAME;
    }

    if (r1 > dL)
    {
        if ((dL + r2) < r1)
        {
            iRet |= E_INSIDE;
        }
        else if (fabs((dL + r2) - r1) < NEAR_ZERO)
        {
            iRet |= E_INSIDE;
            iRet |= E_CONTACT;

        }
        else
        {
            iRet |= E_CORSS;
        }
    }
    else if (r2 > dL)
    {
        if ((dL + r1) < r2)
        {
            iRet |= E_INSIDE;
        }
        else if (fabs((dL + r1) - r2) < NEAR_ZERO)
        {
            iRet |= E_INSIDE;
            iRet |= E_CONTACT;
        }
        else
        {
            iRet |= E_CORSS;
        }
    }
    else if (fabs(dL-r1-r2)<NEAR_ZERO)
    {
        iRet |= E_OUTSIDE;
        iRet |= E_CONTACT;
    }
    else if ((dL - r1 - r2) < 0)
    {
        iRet |= E_CORSS;
    }
    else
    {
        iRet |= E_OUTSIDE;
    }


    if (iRet & E_CONTACT)
    {
        std::vector<POINT2D> lstPoint;
        CIRCLE2D* pC2 = c2->GetCircleInstance();

        LINE2D l(p1, p2);
        bool bOnline = true;
        c1->GetCircleInstance()->Intersect(l, &lstPoint, bOnline);

        STD_ASSERT(lstPoint.size() == 1);
        if (lstPoint.size() > 0)
        {
            *pContact = lstPoint[0];
        }
    }


    CActionCircle::E_TWO_CIRCLE_CONTACT eRet;
    eRet = static_cast<CActionCircle::E_TWO_CIRCLE_CONTACT>(iRet);

    return  eRet;
}

void  CActionCircle::_ClearAux()
{
    m_pLine[0].reset();
    m_pLine[1].reset();
    m_pHyperbola[0].reset();
    m_pHyperbola[1].reset();
    m_pEllipse[0].reset() ;
    m_pEllipse[1].reset() ;
    m_pParabola[0].reset() ;
    m_pParabola[1].reset() ;

    m_psHypabora.clear();
    m_psParabora.clear();
}


bool  CActionCircle::_CreateAux_C_C(
    std::shared_ptr<CDrawingCircle> c1,
    std::shared_ptr<CDrawingCircle> c2)
{
    POINT2D ptC1Near;
    POINT2D ptC2Near;
    POINT2D ptC1Far;
    POINT2D ptC2Far;

    if (c1->GetRadius() > c2->GetRadius())
    {
        std::swap(c1, c2);
    }

    POINT2D p = c1->GetCenter();
    POINT2D q = c2->GetCenter();

    m_psHypabora.clear();

    POINT2D ptContact;
    auto eType = CActionCircle::_GetContactType(&ptContact, c1, c2);

    _ClearAux();

    CActionCircle::_GetCrossPoint(&ptC1Near, &ptC1Far,
        &ptC2Near, &ptC2Far,
        c1, c2);

#ifdef _DEBUG
    DB_PRINT(_T("1N(%f,%f) 1F(%f,%f) 2N(%f,%f) 2F(%f,%f) \n"),ptC1Near.dX, ptC1Near.dY, 
                                                              ptC1Far.dX, ptC1Far.dY,
                                                              ptC2Near.dX, ptC2Near.dY,
                                                              ptC2Far.dX, ptC2Far.dY);
#endif


    POINT2D pass1 = (ptC1Near + ptC2Near) / 2.0;     //外接 外接
    POINT2D pass2 = (ptC1Near + ptC2Far) / 2.0;      //外接 内接

    std::shared_ptr<LINE2D>  lContact;
    if (eType & E_CONTACT)
    {
        //c1,c2の中心点p,qを通る直線
        double A = q.dY - p.dY;
        double B = p.dX - q.dX ;
        double C = p.dY * q.dX - q.dY * p.dX;
        lContact = std::make_shared<LINE2D>(A, B, C);
    }

    if (eType & E_OUTSIDE)
    {
        std::shared_ptr<HYPERBOLA2D>  hp2;
        hp2 = std::make_shared<HYPERBOLA2D>(p, q, pass2);

        if (eType & E_SAME)
        {
            //p,qを通る直線に直交しpass1を通る直線
            double A = q.dX - p.dX;
            double B = q.dY - p.dY;
            double C = -A * pass1.dX - B * pass1.dY;
            m_pLine[0] = std::make_shared<LINE2D>(A, B, C);

            if (eType & E_CONTACT)
            {
                m_pLine[1] = lContact;
            }
            else
            {
                m_pHyperbola[0] = hp2;      //外接 内接
            }
        }
        else
        {
            if (eType & E_CONTACT)
            {
                m_pLine[0] = lContact;
#ifdef _DEBUG
                DB_PRINT(_T("m_pLine[0] = lContact \n"));
#endif

            }
            else
            {
                m_pHyperbola[0] = hp2;
            }

            auto hp1 = std::make_shared<HYPERBOLA2D>(p, q, pass1);
            m_pHyperbola[1] = hp1;      //外接 外接
#ifdef _DEBUG
            DB_PRINT(_T("m_pHyperbola[1] = hp1 \n"));
#endif
        }
    }
    else if (eType & E_CORSS)
    {
        if (eType & E_SAME)
        {
            //p,qを通る直線に直交しpass1を通る直線
            double A = q.dX - p.dX;
            double B = q.dY - p.dY;
            double C = -A * pass1.dX - B * pass1.dY;

            m_pLine[0] = std::make_shared<LINE2D>(A, B, C);
            m_pEllipse[0] = std::make_shared<ELLIPSE2D>(p, q, pass2);
        }
        else
        {
            m_pHyperbola[0] = std::make_shared<HYPERBOLA2D>(p, q, pass1);    //外接 外接
            m_pEllipse[0] = std::make_shared<ELLIPSE2D>(p, q, pass2);
        }
    }
    else if (eType & E_INSIDE)
    {
        if (eType & E_SAME)
        {
            return false;
        }

        m_pEllipse[0] = std::make_shared<ELLIPSE2D>(p,q, pass1);

        if (eType & E_CONTACT)
        {
            m_pLine[0] = lContact;
        }
        else
        {
            m_pEllipse[1] = std::make_shared<ELLIPSE2D>(p, q, pass2);
        }
    }
    return true;
}


bool  CActionCircle::_CreateAux_L_C(
    std::shared_ptr<CDrawingLine>   l,
    std::shared_ptr<CDrawingCircle> c)
{


    /*
    POINT2D p1(0,0);
    POINT2D p2(1,0);
    POINT2D p3(0,1);

    POINT2D p4 = p1 + p2;
    */

    _ClearAux();

    POINT2D ptV[2];

    POINT2D ptF = c->GetCenter();
    double  dR  = c->GetRadius();
    POINT2D ptNear;

    l->NearPoint(&ptNear, ptF, false);
    POINT2D   vV = ptNear - ptF;

    if (vV.Length() < NEAR_ZERO)
    {
        vV = l->GetLineInstance()->Normal();
    }

    vV.Normalize();

    double dL = ptNear.Distance(ptF);
    double dL2 = (dL + dR) / 2.0;


    ptV[0] = (((dL + dR) / 2.0) * vV) + ptF;
    ptV[1] = (((dL - dR) / 2.0) * vV) + ptF;

    m_pParabola[0] = std::make_shared<PARABOLA2D>(ptF, ptV[0]);

    if (fabs(dL - dR) < NEAR_ZERO)
    {
        m_pLine[0] = std::make_shared<LINE2D>( l->GetLineInstance()->NormalLine(ptF));
    }
    else
    {
        m_pParabola[1] = std::make_shared<PARABOLA2D>(ptF, ptV[1]);
    }

    return true;
}


bool  CActionCircle::_CreateAux_L_L(
    std::shared_ptr<CDrawingLine> l1,
    std::shared_ptr<CDrawingLine> l2)
{
    _ClearAux();

    auto pL1 = l1->GetLineInstance();
    auto pL2 = l2->GetLineInstance();


    bool bRet;
    POINT2D ptCross;
    bRet = pL1->IntersectInfnityLine(&ptCross, *pL2);


    if (!bRet)
    {
        //２直線が平行
        POINT2D ptT = pL1->GetThrouchPoint();
        LINE2D lineNorm = pL1->NormalLine(ptT);
        POINT2D ptIntersect;
        bRet = lineNorm.IntersectInfnityLine(&ptIntersect, *pL2);
        ptCross = (ptIntersect + ptT) / 2.0;
        m_pLine[0] = std::make_shared<LINE2D>(ptCross, pL1->Angle());
    }
    else
    {

        double dAngle1 = pL1->Angle();
        double dAngle2 = pL2->Angle();


        double midAngle1 = (dAngle1 + dAngle2) / 2.0;
        m_pLine[0] = std::make_shared<LINE2D>(ptCross, midAngle1);
        m_pLine[1] = std::make_shared<LINE2D>(ptCross, midAngle1 + 90.0);
    }
    return true;
}

bool  CActionCircle::_AddAux(CDrawMarker * pMarker)
{
    //------------
    for (auto hp : m_pHyperbola)
    {
        if (hp)
        {
            CreateHyperboraLine(&m_psHypabora, hp);
        }
    }
    for (auto pObj : m_psHypabora)
    {
        pMarker->AddObject(pObj);
    }

    int iCnt = 0;
    for (auto l : m_pLine)
    {
        if (l)
        {
            auto pLine = CreateLine(&m_psAddtionalObj[iCnt]);
            *(pLine->GetLineInstance()) = *l;
#ifdef _DEBUG_LLL
#if 0
            pLine->SetLineWidth(iCnt * 4 + 1);
#endif
#endif
            pMarker->AddObject(pLine);
        }
        iCnt++;
    }

    iCnt = 0;
    for (auto e : m_pEllipse)
    {
        if (e)
        {
            auto pE = CreateEllipse(&m_pAuxEllipse[iCnt]);
            *(pE->GetEllipseInstance()) = *e;
            pMarker->AddObject(pE);
        }
        iCnt++;
    }

    for (auto pb : m_pParabola)
    {
        if (pb)
        {
            CreateParaboraLine(&m_psParabora, pb);
        }
    }
    for (auto pObj : m_psParabora)
    {
        pMarker->AddObject(pObj);
    }


    return true;
}
//*******************************
//             TEST
//*******************************
#ifdef _DEBUG




void TEST_CActionCircle()
{

    CActionCircle ac;

    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());


    auto c1 = std::make_shared<CDrawingCircle>();
    auto c2 = std::make_shared<CDrawingCircle>();

    POINT2D  p1(1, 0);
    POINT2D  p2(2, 0);


    c1->SetCenter(p1);
    c1->SetRadius(1);

    c2->SetCenter(p2);
    c2->SetRadius(2);


    POINT2D ptContact;
    auto eType = CActionCircle::_GetContactType(&ptContact, c1, c2);

    STD_ASSERT((eType & CActionCircle::E_INSIDE));
    STD_ASSERT(!(eType & CActionCircle::E_OUTSIDE));
    STD_ASSERT((eType & CActionCircle::E_CONTACT));
    STD_ASSERT(!(eType & CActionCircle::E_SAME));


    c1->SetCenter(POINT2D(1.5, 0));
    eType = CActionCircle::_GetContactType(&ptContact, c1, c2);
    STD_ASSERT((eType & CActionCircle::E_INSIDE));
    STD_ASSERT(!(eType & CActionCircle::E_OUTSIDE));
    STD_ASSERT(!(eType & CActionCircle::E_CONTACT));
    STD_ASSERT(!(eType & CActionCircle::E_SAME));


    c1->SetCenter(POINT2D(-1.0, 0));
    eType = CActionCircle::_GetContactType(&ptContact, c1, c2);
    STD_ASSERT(!(eType & CActionCircle::E_INSIDE));
    STD_ASSERT((eType & CActionCircle::E_OUTSIDE));
    STD_ASSERT((eType & CActionCircle::E_CONTACT));
    STD_ASSERT(!(eType & CActionCircle::E_SAME));

    c1->SetCenter(POINT2D(-1.0, 0));
    eType = CActionCircle::_GetContactType(&ptContact, c1, c2);
    STD_ASSERT(!(eType & CActionCircle::E_INSIDE));
    STD_ASSERT((eType & CActionCircle::E_OUTSIDE));
    STD_ASSERT((eType & CActionCircle::E_CONTACT));
    STD_ASSERT(!(eType & CActionCircle::E_SAME));





    std::vector<CIRCLE2D>  lstCircle;
    auto Calc3circles = [&](CIRCLE2D c1, CIRCLE2D c2, CIRCLE2D c3)
    {
        int iCounter = 0;
        for (int cnt = 0; cnt < 8; cnt++)
        {
            lstCircle.clear();
            bool bRet = SolveTheApollonius(&lstCircle,
                cnt,
                c1,
                c2,
                c3);

            if (bRet)
            {
                for (auto cAns : lstCircle)
                {
                    DB_PRINT(_T("%d: %s\n"), iCounter++, cAns.Str().c_str());
                }
            }
        }
    };

    CIRCLE2D c3_1(POINT2D(0, 0), 1);
    CIRCLE2D c3_2(POINT2D(2, 4), 2);
    CIRCLE2D c3_3(POINT2D(4, 0), 1);
    Calc3circles(c3_1, c3_2, c3_3);

    DB_PRINT(_T("-----------\n"));

    CIRCLE2D c4_1(POINT2D(0, 0), 5);
    CIRCLE2D c4_2(POINT2D(8, 0), 3);
    CIRCLE2D c4_3(POINT2D(13, 0), 2);
    Calc3circles(c4_1, c4_2, c4_3);



    {
        CIRCLE2D  cAnsPPP;
        POINT2D  p1(2, 1);
        POINT2D  p2(4, -7);
        POINT2D  p3(-1, 3);

        bool bRet = ac._CreatePPP(&cAnsPPP, p1, p2, p3);


        POINT2D pC = cAnsPPP.GetCenter();
        double dR  = cAnsPPP.GetRadius();

        auto CheckAns = [&](POINT2D pt)
        {
            double dAns;
            dAns = (pC.dX - pt.dX)* (pC.dX - pt.dX)
                 + (pC.dY - pt.dY) * (pC.dY - pt.dY) - dR * dR;
            return dAns;
        };

        DB_PRINT(_T("check ppp %f, %f, %f,\n"), CheckAns(p1), CheckAns(p2), CheckAns(p3));
    }

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}
#endif //_DEBUG
