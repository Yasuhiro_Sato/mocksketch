/**
 * @brief			CActionCircleヘッダーファイル
 * @file			CActionCircle.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#if !defined(_ACTION_CIRCLE_H_)
#define _ACTION_CIRCLE_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "View/VIEW_MODE.h"
#include "CViewAction.h"
#include "DrawingObject/Primitive/POINT2D.h"
class     CDrawingView;
class     CDrawingCompositionLine;
class     HYPERBOLA2D;
class     PARABOLA2D;
//extern void TEST_CActionCircle(void);

/**
 * @class   CActionCircle
 * @brief                        
   
 */
class CActionCircle: public CViewAction
{
    enum E_TWO_CIRCLE_CONTACT
    {
        E_OUTSIDE         = 0x01,   //2つの円は離れているか交差している
        E_INSIDE          = 0x02,   //一方の円が他方の円の内側にある,
        E_CONTACT         = 0x04,   //接触あり
        E_SAME            = 0x08,   //同じサイズの円
        E_CORSS           = 0x10,
    };

    enum E_CALC_RADUS_METHODS
    {
        R1_PLUS          =0x0001,
        R1_MINUS         =0x0002,          
        R2_PLUS          =0x0010,
        R2_MINUS         =0x0020,
        SNAPC1_PLUS      =0x0100,
        SNAPC1_NINUS     =0x0200,
        SNAPC2_PLUS      =0x1000,
        SNAPC2_NINUS     =0x2000 ,
    };

  
    enum E_SELECT_STS
    {
        SS_NO_SELECT             = 0x00001,
        SS_SELECT_POINT          = 0x00002,
        SS_SELECT_LINE           = 0x00004,
        SS_SELECT_CIRCLE         = 0x00008,
        SS_SELECT_LINE_LINE      = 0x00010,
        SS_SELECT_CIRCLE_LINE    = 0x00020,
        SS_SELECT_CIRCLE_CIRCLE  = 0x00040,

        SS_CREATE_CIRCLE          = 0x10000,

    };

public:
	//!< コンストラクタ
    CActionCircle();

    //!< コンストラクタ
    CActionCircle(CDrawingView* pView);
    
	//!< デストラクタ
    virtual ~CActionCircle(){;}


	//!< 選択解放時動作
	virtual void Cancel(VIEW_MODE eMode);

	//!< 選択解放時動作
	virtual bool SelAction(VIEW_MODE eMode, void* pParam = NULL);

	//!< マウスクリック動作
	virtual bool SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

private:
    friend void TEST_CActionCircle();

	//!< マウスクリック動作
	virtual bool _SelCircle(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction, POINT2D pt2D, std::shared_ptr<CDrawingObject> pObj, bool bMarker);
	virtual bool _SelCircle1(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction, POINT2D pt2D, std::shared_ptr<CDrawingObject> pObj, bool bMarker);
	virtual bool _SelCircle2(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction, POINT2D pt2D, std::shared_ptr<CDrawingObject> pObj, bool bMarker);
	virtual bool _SelCircle3(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction, POINT2D pt2D, std::shared_ptr<CDrawingObject> pObj, bool bMarker);
    virtual bool _SelCircle4(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction, POINT2D pt2D, std::shared_ptr<CDrawingObject> pObj, bool bMarker);



	//!< ３点を通る円
	virtual bool _SelCircle3P(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction, POINT2D pt2D, std::shared_ptr<CDrawingObject> pObj, bool bMarker);


    //!< 動作クリア
    void _ClearAction();

    //!<  入力値設定
    virtual bool  InputValue(StdString strVal);

    //!< マウス移動動作
    virtual bool MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

    bool CreateHyperboraLine(std::vector<std::shared_ptr<CDrawingCompositionLine>>* pDraw,
       const std::shared_ptr<HYPERBOLA2D> pHyp);

    bool CreateParaboraLine(std::vector<std::shared_ptr<CDrawingCompositionLine>>* pDraw,
        const std::shared_ptr<PARABOLA2D> pPara);

    static bool _GetCrossPoint(POINT2D* p1Near,
        POINT2D* p1Far,
        POINT2D* p2Near,
        POINT2D* p2Far,
        std::shared_ptr<CDrawingCircle> c1,
        std::shared_ptr<CDrawingCircle> c2);

    static E_TWO_CIRCLE_CONTACT _GetContactType(
        POINT2D* pContact, 
        std::shared_ptr<CDrawingCircle> c1,
        std::shared_ptr<CDrawingCircle> c2);

    bool  _CreateAux_C_C(std::shared_ptr<CDrawingCircle> c1,
                     std::shared_ptr<CDrawingCircle> c2);

    bool  _CreateAux_L_C(std::shared_ptr<CDrawingLine> l,
        std::shared_ptr<CDrawingCircle> c);
 
    bool  _CreateAux_L_L(std::shared_ptr<CDrawingLine> l1,
        std::shared_ptr<CDrawingLine> l2);

    bool  _AddAux(CDrawMarker* pMarker);


private:
    void _ClearAux();

    bool _MovePoint1(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);
    bool _MovePoint2(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);
    bool _MovePoint3(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);
    bool _MovePoint4(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

    bool _MovePoint3P(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);


    bool _MovePoint2_C_C(
        SNAP_DATA* pSnap,
        std::shared_ptr<CDrawingCircle> c1,
        std::shared_ptr<CDrawingCircle> c2);

    bool _MovePoint2_L_C(
        SNAP_DATA* pSnap,
        std::shared_ptr<CDrawingLine> l,
        std::shared_ptr<CDrawingCircle> c);

    bool _MovePoint2_L_L(
        SNAP_DATA* pSnap,
        std::shared_ptr<CDrawingLine> l1,
        std::shared_ptr<CDrawingLine> l2);

    bool _MovePoint2AddSnap(std::list<SNAP_DATA>* pSnapList, CDrawingObject*   pObj);
    bool _MovePoint3AddSnap(std::list<SNAP_DATA>* pSnapList, CDrawingObject*   pObj);


    CIRCLE2D _CalcGoodStopPoint_C_C(POINT2D ptOnLine, 
                                int iMethod1,
                                int iMethod2,
                                bool bSnap,
                                const CIRCLE2D* c1,
                                const CIRCLE2D* c2,
                                const std::vector<POINT2D>& lstPoint);

    CIRCLE2D _CalcGoodStopPoint_L_C(POINT2D ptOnLine,
        int iMethod,
        bool bSnap,
        const LINE2D* l,
        const CIRCLE2D* c,
        const std::vector<POINT2D>& lstPoint);

    CIRCLE2D _CalcGoodStopPoint_L_L(POINT2D ptOnLine,
        int iMethod,
        bool bSnap,
        const LINE2D* l1,
        const LINE2D* l2,
        const std::vector<POINT2D>& lstPoint);

    bool _MovePoint3_C_C(POINT2D pt2d,
                         bool bSnap,
                         bool bTmp,
                         SNAP_DATA* pSnap,
                         std::shared_ptr<CDrawingCircle> c1, 
                         std::shared_ptr<CDrawingCircle> c2);

    bool _MovePoint3_L_C(
        POINT2D pt2d,
        bool bSnap,
        bool bTmp,
        SNAP_DATA* pSnap,
        std::shared_ptr<CDrawingLine> l,
        std::shared_ptr<CDrawingCircle> c);

    bool _MovePoint3_L_L(
        POINT2D pt2d,
        bool bSnap,
        SNAP_DATA* pSnap,
        std::shared_ptr<CDrawingLine> l1,
        std::shared_ptr<CDrawingLine> l2);

    bool _CreateCC(std::vector<POINT2D>* pLstCenter,
        double dR,
        std::shared_ptr<CDrawingCircle> c1,
        std::shared_ptr<CDrawingCircle> c2);

    bool _CreateLC(std::vector<POINT2D>* pLstCenter,
        double dR,
        std::shared_ptr<CDrawingLine> l,
        std::shared_ptr<CDrawingCircle> c);

    bool _CreateLL(std::vector<POINT2D>* pLstCenter,
        double dR,
        std::shared_ptr<CDrawingLine> l1,
        std::shared_ptr<CDrawingLine> l2);

    bool _CreateLLL(std::vector<CIRCLE2D>* pLstCircle,
        std::shared_ptr<CDrawingLine> l1,
        std::shared_ptr<CDrawingLine> l2,
        std::shared_ptr<CDrawingLine> l3);

    bool _CreateCCC(std::vector<CIRCLE2D>* pLstCircle,
        std::shared_ptr<CDrawingCircle> c1,
        std::shared_ptr<CDrawingCircle> c2,
        std::shared_ptr<CDrawingCircle> c3);


    bool _CreatePPP(CIRCLE2D* pC,
        const POINT2D& p1,
        const POINT2D& p2,
        const POINT2D& p3);

    bool _AddCircle(std::shared_ptr<CDrawingCircle> pCircle);

private:
    //!< 選択位置
    POINT2D              m_ptSel[2];

    //!< 初回選択オブジェクト
    std::shared_ptr<CDrawingObject>      m_pSelObj[4];

    // 選択可能円
    std::vector<std::shared_ptr<CDrawingCircle>>  m_psSelectCircles;

    int                                           m_selSts;
    StdString                                     m_strOldVal;


    //----------------
    // 2円に接する円
    //----------------
    E_TWO_CIRCLE_CONTACT              m_eContactType;

    //!< 双曲線
    std::shared_ptr<HYPERBOLA2D>      m_pHyperbola[2];
    std::shared_ptr<PARABOLA2D>       m_pParabola[2];
    std::shared_ptr<LINE2D>           m_pLine[5];
    std::shared_ptr<ELLIPSE2D>        m_pEllipse[2];
    std::shared_ptr<CDrawingObject>   m_pAuxEllipse[2];
    std::shared_ptr<POINT2D>          m_pContact;

    // 双曲線(描画用)
    std::vector<std::shared_ptr<CDrawingCompositionLine>>  m_psHypabora;
    std::vector<std::shared_ptr<CDrawingCompositionLine>>  m_psParabora;

    std::unique_ptr<VIEW_COMMON::VIEW_VALUE>    m_psVvHyp;

    //!< 現在作成中の円
    //std::unique_ptr<CDrawingCircle>     m_psEditingCircle;
   
};
#endif // !defined(_ACTION_POINT_H_)
