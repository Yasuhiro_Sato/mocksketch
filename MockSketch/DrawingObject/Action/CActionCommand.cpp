/**
 * @brief			CActionCommand実装ファイル
 * @file			CActionCommand.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CActionCommand.h"

#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/View/CDrawingView.h"

#include "DrawingObject/CDrawingObject.h"
#include "DrawingObject/CDrawingPoint.h"
#include "DrawingObject/CDrawingLine.h"
#include "DrawingObject/CDrawingCircle.h"
#include "DrawingObject/CDrawingScriptBase.h"
#include "DrawingObject/CDrawingParts.h"
#include "DrawingObject/CDrawingEllipse.h"
#include "DrawingObject/CDrawingCompositionLine.h"
#include "DrawingObject/CDrawingGroup.h"
#include "DrawingObject/CNodeMarker.h"
#include "DrawingObject/CDrawingNode.h"
#include "DrawingObject/CNodeBound.h"

#include "View/CLayer.h"
#include "View/CDrawMarker.h"
#include "View/CUndoAction.h"
#include "MockSketch.h"
#include "CProjectCtrl.h"
#include "Utility/CUtility.h"
#include "System/CSystem.h"
#include "SubWindow/TextInputDlg.h"
#include <math.h>

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 * コンストラクタ
 */
CActionCommand::CActionCommand(CDrawingView* pView):
CViewAction(pView),
m_eDrag         (E_DS_NONE)
{
    m_lMouseOver  = SEL_ALL;
}

/**
 * コンストラクタ
 */
CActionCommand::CActionCommand():
CActionCommand(NULL)
{
}

    
/**
 * デストラクタ
 */
CActionCommand::~CActionCommand()
{
    
}

/**
 *  @brief   解放時動作
 *  @param   [in]    eMode 解放時点でのモード
 *  @retval   なし
 *  @note
 */
void CActionCommand::Cancel(VIEW_MODE eMode) 
{
    CancelSelectRect();    

    switch(eMode)
    {
    case VIEW_MOVE:
    case VIEW_COPY:
    case VIEW_ROTATE:
    case VIEW_SCL:
    case VIEW_DELETE:
    case VIEW_GROUP:
    case VIEW_PARTS:
    case VIEW_REFERENCE:
    case VIEW_NODE_SELECT:

    case VIEW_MIRROR:
        return;
    }
    ClearAction();    
}

/**
 *  @brief   選択時動作
 *  @param   [in]    eMode 選択時点でのモード
 *  @retval   なし
 *  @note
 */
bool CActionCommand::SelAction(VIEW_MODE eViewMode, void* pParam) 
{
    STD_ASSERT(m_pView != NULL);

    //既に選択中の図形はあるか？
    auto pDef = m_pView->GetPartsDef();
    long lSize = pDef->SelectNum();

    m_eViewMode = eViewMode;
    if (lSize == 0)
    {
        //図形を選択してください
        m_pView->SetComment(GET_SYS_STR(STR_SEL_OBJECT));
        if (m_eViewMode != VIEW_SELECT)
        {
            m_ePreViewMode = m_eViewMode;
            m_pView->SetViewMode(VIEW_SELECT, true);
        }
        return true;    
    }

    if (m_eViewMode != VIEW_SELECT)
    {
        m_ePreViewMode = VIEW_NONE;
    }

    m_iClickCnt = 0;
    switch (eViewMode)
    {
        case VIEW_SELECT:
            m_lMouseOver =   SEL_ALL;
            m_pView->SetComment(GET_SYS_STR(STR_SEL_OBJECT));
            break;

        case VIEW_DELETE:
            //TODO:UNDO対応
            pDef->SetDrawLock();
            Delete();
            pDef->ReleaseDrawLock();
            m_pView->SetViewMode(VIEW_SELECT, false);
            pDef->Redraw();
            break;

        case VIEW_DISASSEMBLY:

            auto pObj = pDef->GetSelectInstance()->at(0).lock();
            if (pObj)
            {
                if ((pObj->GetType() == DT_GROUP) ||
                    (pObj->GetType() == DT_PARTS) ||
                    (pObj->GetType() == DT_REFERENCE))
                {
                    auto pGroup = std::dynamic_pointer_cast<CDrawingGroup>(pObj);
                    bool  bSetUndo = true; 
                    pDef->Disassemble(pGroup, bSetUndo);
                    pDef->Redraw();
                    m_pView->SetViewMode(VIEW_SELECT, false);
                }
                else if(pObj->GetType() == DT_COMPOSITION_LINE)
                {
                    auto pCompo = std::dynamic_pointer_cast<CDrawingCompositionLine>(pObj);
                    bool  bSetUndo = true; 
                    pCompo->Disassemble(pDef, bSetUndo);
                    Delete();
                    pDef->Redraw();
                    m_pView->SetViewMode(VIEW_SELECT, false);
                }
            }

            break;
    }
    return true;    
}

/**
 *  @brief   マウスクリック動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionCommand::SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    STD_ASSERT(m_pView != NULL);

    POINT ptSel = posMouse.ptSel;
    UINT nFlags = posMouse.nFlags;

    //既に選択中の図形はあるか？
    long lSize = m_pView->GetPartsDef()->SelectNum();

    POINT2D ptBase;
    bool bRet = false;
    try
    {
        switch (m_eViewMode)
        {
            case VIEW_SELECT:
                //選択動作
                bRet = SelectAction( posMouse, eAction);
                break;

            case VIEW_DELETE:
                break;

            case VIEW_DISASSEMBLY:
                break;
        }
    }
    catch(MockException &e)
    {
        e.DispMessageBox();
    }

    return bRet;
}

/**
 *  動作クリア
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CActionCommand::ClearAction(bool bReleaseSelect)
{
    //範囲選択時(E_DS_AREA)は選択状態を解除しない

    CPartsDef*   pDef  = m_pView->GetPartsDef();
	CViewAction::ClearAction(bReleaseSelect);

    m_iClickCnt = 0;

    pDef->Redraw();

    ::ReleaseCapture();
    m_eDrag = E_DS_NONE;

    _DeleteTmpObjects();
}

/**
 *  @brief   選択動作
 *  @param   [in]    posMouse   選択位置
 *  @param   [in]    eAction 選択動作
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionCommand::SelectAction(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    bool bRet = false;
DB_PRINT(_T("CActionCommand::SelectAction %d)\n"), eAction);

    switch (eAction)
    {
    case ACT_RBUTTON_DOWN:
        bRet = OnRButtonDown(posMouse);
        break;

    case ACT_RBUTTON_UP:
        bRet = OnRButtonUp(posMouse);
        break;

    case ACT_LBUTTON_DOWN:
        bRet = OnLButtonDown(posMouse);
        break;

    case ACT_LBUTTON_UP:
        bRet = OnLButtonUp(posMouse);
        break;
    }

    return bRet;
}

bool CActionCommand::OnRButtonUp(MOUSE_MOVE_POS posMouse)
{
    return false;
}


bool CActionCommand::OnClickLButton(MOUSE_MOVE_POS posMouse)
{
    CPartsDef*pDef = m_pView->GetPartsDef();
	bool const bIgnoreSpecialType = true;
    int iId = m_pView->SearchPos(posMouse.ptSel, bIgnoreSpecialType);
    auto pObject = pDef->GetObjectById( iId );
    bool bCtrl = ((posMouse.nFlags & MK_CONTROL) == MK_CONTROL);

    m_eDrag = E_DS_NONE;

    if (iId == -1)
    {
        pDef->RelSelect();
		if (pDef->SelectNum() == 0)
		{
			ReturnInterrupt();
		}
		pDef->DrawDragging(m_pView);
		return false;
    }

    //1オブジェクト選択
    bool bSelNewObject = true;
    if (pObject)
    {
        if (pObject->IsSelect())
        {
            if (bCtrl)
            {
                pDef->RelSelectById(iId);
            }
            else
            {
                //オブジェクトが選択された状態でもう一度選択した場合
                //NodeSelect状態へ移行する
                m_pView->SetViewMode(VIEW_NODE_SELECT, true);
            }
        }
        else
        {
            if (!bCtrl)
            {
                pDef->RelSelect();
            }
            pDef->SelectDrawingObject(iId);
        }
    }
	else
	{
		if (pDef->SelectNum() == 0)
		{
			ReturnInterrupt();
		}
	}

    pDef->DrawDragging(m_pView);
    return true;
}

bool CActionCommand::OnLButtonUp(MOUSE_MOVE_POS posMouse)
{
    using namespace VIEW_COMMON;
    CPartsDef*pCtrl = m_pView->GetPartsDef();
    CNodeMarker* pNodeMarker;

    if (m_eDrag == E_DS_NONE)
    {
        return false;
    }

    STD_ASSERT(pCtrl != NULL);

    pNodeMarker = m_pView->GetNodeMarker();

    //Mouse Down時に変更
    //Ctrlキーを押しているときは解除しない
    //if (posMouse.nFlags != MK_CONTROL)
    //{
    //    pCtrl->RelSelect();
    //}

	bool bRelSelect = true;
    int diffX = abs(m_ptSelStart.x - posMouse.ptSel.x);
    int diffY = abs(m_ptSelStart.y - posMouse.ptSel.y);

    if ((diffX < DRAW_CONFIG->iAllowClickMoveCount) &&
        (diffY < DRAW_CONFIG->iAllowClickMoveCount))
    {       
        OnClickLButton(posMouse);
		return true;
    }
    else
    {
        if (m_eDrag == E_DS_AREA)
        {
			bool bKeepSelected = ((posMouse.nFlags & MK_CONTROL) == MK_CONTROL);
			if (!bKeepSelected)
			{
				pCtrl->RelSelect();
			}

			//範囲オブジェクト選択
            pCtrl->SelectDrawingObject(m_ptSelStart, posMouse.ptSel, m_pView);
			CancelSelectRect();
            bRelSelect = false;
        }
    }

	ClearAction(bRelSelect);

    //割り込み解除
    if(pCtrl->SelectNum() == 0)
    {
        ReturnInterrupt();
    }

    if (!bRelSelect)
    {
         m_pView->RelInterrupt();
    }
	return true;
}

bool CActionCommand::OnRButtonDown(MOUSE_MOVE_POS posMouse)
{
    // NodeBoundを右ボタンでもドラッグできるようにする
    // この場合ボタンアップ時にコンテキストメニューを表示して
    // 移動、複写、キャンセルを選べるようにする

    return false;
}


bool CActionCommand::OnLButtonDown(MOUSE_MOVE_POS posMouse)
{
    using namespace VIEW_COMMON;
    CPartsDef*pCtrl = m_pView->GetPartsDef();
    CNodeMarker* pNodeMarker;
    pNodeMarker = m_pView->GetNodeMarker();
    int iId = m_pView->SearchPos(posMouse.ptSel);
    auto pObject = pCtrl->GetObjectById( iId );

	//----------------------
    bool bSelectedMousedown = false;
    if ((iId != -1) &&
        (pCtrl->SelectNum() == 1))
    {
        auto pList = pCtrl->GetSelectInstance();
        auto wObj =  pList->at(0);
        auto pObj =  wObj.lock();
        if(pObj)
        {
            if(pObj->GetId() == iId)
            {
                pObject->SelectedMouseDown(m_pView, posMouse);
            }
        }
    }

    if (pObject)
    {
        bSelectedMousedown = pObject->IsSelect();
    }

    // Ctrlキーを押しているとき
    // 選択状態を更に選択した場合
    // ノードマーカを選択した場合 
    // は解除しない

    if ((posMouse.nFlags & MK_CONTROL) 
        && bSelectedMousedown
        )
    {
        pCtrl->RelSelect();
    }

    m_eDrag = E_DS_AREA;
    m_ptSelStart = posMouse.ptSel;
    m_ptSelLast  = posMouse.ptSel;
    CRect rect(posMouse.ptSel.x, posMouse.ptSel.y, posMouse.ptSel.x, posMouse.ptSel.y);
	rect.NormalizeRect();
	DrawFocusRect(m_pView->GetViewDc(), rect);
	return true;
}


bool CActionCommand::_MovePointNone(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    CPartsDef* pDef = m_pView->GetPartsDef();
    int iId = -1;
	SNAP_DATA snap;

    CViewAction::MovePoint( posMouse, eAction);
    iId  = m_pView->SearchPos(posMouse.ptSel);
    // 選択中のオブジェクトが１の場合
    // ノードマーカを優先する

    bool bCtrl = false;

    if ((posMouse.nFlags & MK_CONTROL) &&
        (iId != -1))
    {
        //Ctrlキーを押している場合
        bCtrl = true;
        auto pObj = pDef->GetObjectById(iId);
        pDef->SetMouseOver(pObj);
        pDef->DrawDragging(m_pView);
        return true;
    }
            
    int iSelNum = pDef->SelectNum();
	if ( pDef->SelectNum() > 1) 
	{
		if(iId != -1)
		{
            //　次にクリックした場合NodeBoundを表示する
            //その為、マウスオーバーでk強調表示する
			auto pList = pDef->GetSelectInstance();
			for(auto pObj: *pList)
			{
				pDef->SetMouseOver(pObj);
			}
		}
		else
		{
			pDef->ClearMouseOver();
		}
    }
    else if (iSelNum == 1)
    {
		auto pList = pDef->GetSelectInstance();
        auto wObj = pList->at(0);
        auto pObj = wObj.lock();

        std::shared_ptr<CDrawingObject> pSelObj;
        if(pObj)
        {
            int iId = pObj->GetId();
	        pSelObj = pDef->GetObjectById(iId);
        }

        if (pSelObj)
        {
            pSelObj->PreSelectedMouseMove(m_pView, posMouse);
        }
    }

    pDef->DrawDragging(m_pView);
    return true;
}

bool CActionCommand::_MovePointArea(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    CPartsDef* pDef = m_pView->GetPartsDef();

    SelectAction( posMouse, eAction);

    CRect rect(m_ptSelStart.x, m_ptSelStart.y, m_ptSelLast.x, m_ptSelLast.y);
    rect.NormalizeRect();
    DrawFocusRect(m_pView->GetViewDc(), rect);

    rect.SetRect(m_ptSelStart.x, m_ptSelStart.y, posMouse.ptSel.x, posMouse.ptSel.y);
    rect.NormalizeRect();
    DrawFocusRect(m_pView->GetViewDc(), rect);
    m_ptSelLast = posMouse.ptSel;
#if 0
 DB_PRINT(_T("OnMouseMove(%d,%d)-(%d,%d)\n"), m_ptSelStart.x, m_ptSelStart.y,
                                          m_ptSelLast.x, m_ptSelLast.y);
#endif
    pDef->DrawDragging(m_pView);

    return true;
}
/*
bool CActionCommand::_MovePointNode(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
	SNAP_DATA snap;

    CPartsDef* pDef = m_pView->GetPartsDef();
    CDrawMarker* pMarker= m_pView->GetMarker();
    pMarker->Clear();
    pMarker->ClearObject();

    StdString strToolTip;
    DWORD dwSnapType = 0;

    _GetMousePointLengthAngle(&snap, m_strNodeMarkerId, posMouse);
    if (m_pMarkerPearentObject)
    {
        //親オブジェクトにノードの移動を通知
		CNodeMarker* pNodeMarker = m_pView->GetNodeMarker();
        m_pMarkerPearentObject->MoveNodeMarker(pNodeMarker,
                                &snap,
                                m_strNodeMarkerId,
                                posMouse);

        pDef->HideObject(m_pMarkerPearentObject->GetId());
        pNodeMarker->SetMarkerPosView(m_strNodeMarkerId, snap.pt);
        pMarker->AddObject(m_pMarkerPearentObject);
    }

    pDef->DrawDragging(m_pView);

    if(m_pMarkerPearentObject)
	{
		m_pView->SetToolTipText(snap.strSnap.c_str());
    }


    return true;
}
*/

/**
 *  @brief   マウス移動動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionCommand::MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    CPartsDef* pDef = m_pView->GetPartsDef();
    if (!pDef)
    {
        return false;
    }

	SNAP_DATA snap;

    if (m_eDrag == E_DS_NONE)
    {
        return _MovePointNone(posMouse, eAction);
    }
	else if(m_eDrag == E_DS_AREA)
	{
        return _MovePointArea(posMouse, eAction);
	}

    if (pDef)
    {
        pDef->DrawDragging(m_pView);
    }

    return true;
}

/**
 * @brief   コンテキストメニュー表示
 * @param   [in]    point  クリック位置
 * @retval  true メニュー表示あり
 * @note
 */
bool  CActionCommand::OnContextMenu(POINT point)
{
    STD_ASSERT(m_pView != NULL);

    //既に選択中の図形はあるか？
    CPartsDef* pDef = m_pView->GetPartsDef();
    if (!pDef)
    {
        return false;
    }

    long lSize = pDef->SelectNum();

    if (m_eViewMode == VIEW_SELECT)
    {
        auto AddMove = [](CMenu* pNmeu)
        {
            pNmeu->AppendMenu(MF_STRING, ID_MNU_MOVE, GET_STR(STR_MNU_ITEM_MOVE));
            pNmeu->AppendMenu(MF_STRING, ID_MNU_COPY, GET_STR(STR_MNU_ITEM_COPY));
            pNmeu->AppendMenu(MF_STRING, ID_MNU_ROTATE, GET_STR(STR_MNU_ITEM_ROTATE));
            pNmeu->AppendMenu(MF_STRING, ID_MNU_SCL, GET_STR(STR_MNU_SCL));
            pNmeu->AppendMenu(MF_STRING, ID_MNU_MIRROR, GET_STR(STR_MNU_MIRROR));
            pNmeu->AppendMenu(MF_STRING, ID_MNU_DELETE, GET_STR(STR_MNU_DELETE));
            //CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_STRETCH_MODE), ID_MNU_STRETCH_MODE , NULL);

        };

        CMenu menu;
        menu.CreatePopupMenu();


        if(lSize == 1)
        {

            auto pObj = pDef->GetSelectInstance()->at(0).lock();
            if (pObj)
            {
                CMenu mOrder;
                mOrder.CreatePopupMenu();
                DRAWING_TYPE eType = pObj->GetType();

                mOrder.AppendMenu(MF_STRING, ID_MNU_DSP_ORDER_TOPMOST, GET_STR(STR_MNU_DSP_ORDER_TOPMOST));
                mOrder.AppendMenu(MF_STRING, ID_MNU_DSP_ORDER_TOPBACK, GET_STR(STR_MNU_DSP_ORDER_TOPBACK));
                mOrder.AppendMenu(MF_STRING, ID_MNU_DSP_ORDER_FRONT, GET_STR(STR_MNU_DSP_ORDER_FRONT));
                mOrder.AppendMenu(MF_STRING, ID_MNU_DSP_ORDER_BACK, GET_STR(STR_MNU_DSP_ORDER_TBACK));
                menu.AppendMenu(MF_POPUP, (UINT)mOrder.GetSafeHmenu(), GET_STR(STR_MNU_DSP_ORDER));
                menu.AppendMenu(MF_SEPARATOR);
                AddMove(&menu);
                menu.AppendMenu(MF_SEPARATOR);

                if((eType == DT_CIRCLE) ||
                    (eType == DT_LINE)  ||
                    (eType == DT_SPLINE)  ||
                    (eType == DT_ELLIPSE)  ||
                    (eType == DT_COMPOSITION_LINE))
                {
                    menu.AppendMenu(MF_STRING, ID_MNU_OFFSET, GET_STR(STR_MNU_OFFSET));
                    menu.AppendMenu(MF_STRING, ID_MNU_TRIM, GET_STR(STR_MNU_TRIM));
                    menu.AppendMenu(MF_STRING, ID_MNU_DEVIDE, GET_STR(STR_MNU_DEVIDE));
                    menu.AppendMenu(MF_SEPARATOR);
                }
                if (eType == DT_GROUP)
                {
                    menu.AppendMenu(MF_STRING, ID_MNU_DISASSEMBLY, GET_STR(STR_MNU_REL_GROUP));
                }
                else if ((eType == DT_COMPOSITION_LINE) ||
                         (eType == DT_PARTS) ||
                         (eType == DT_REFERENCE))
                {
                    menu.AppendMenu(MF_STRING, ID_MNU_DISASSEMBLY, GET_STR(STR_MNU_DISASSEMBLY));
                }
                else if (eType == DT_TEXT)
                {
                    menu.AppendMenu(MF_STRING, ID_MNU_CONVERT_SPLINE, GET_STR(STR_MNU_CONVERT_SPLINE));
                }

            }
        }
        else if(lSize > 1)
        {
            AddMove(&menu);
            menu.AppendMenu(MF_SEPARATOR);
            menu.AppendMenu(MF_STRING, ID_MNU_GROUP, GET_STR(STR_MNU_GROUP));
        }
        else
        {
            return false;
        }

        CWnd* pWnd = const_cast<CWnd*>(m_pView->GetWindow());
        UINT uiMenu = menu.TrackPopupMenu(
            TPM_LEFTALIGN  |    //クリック時のX座標をメニューの左辺にする
            TPM_NONOTIFY   |
            TPM_RIGHTBUTTON,    //右クリックでメニュー選択可能とする
            point.x,point.y,    //メニューの表示位置
            pWnd                //このメニューを所有するウィンドウ
        );

        menu.DestroyMenu();
        return true;
    }

    return false;
}



/**
 *  @brief   選択矩形解除
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CActionCommand::CancelSelectRect()
{
    if (m_eDrag != E_DS_AREA)
    {
        return;
    }
    //領域を選択
    CRect rect(m_ptSelStart.x, m_ptSelStart.y, m_ptSelLast.x, m_ptSelLast.y);
    rect.NormalizeRect();
	DrawFocusRect(m_pView->GetViewDc(), rect);
    ::ReleaseCapture();
}


/**
 *  @brief   削除
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CActionCommand::Delete()
{
    //
    CNodeMarker* pNodeMarker;
    pNodeMarker = m_pView->GetNodeMarker();
    if (pNodeMarker)
    {
        pNodeMarker->Clear();
    }

    long lSize = m_pView->GetPartsDef()->SelectNum();
    if (lSize == 0){return;}
    if (lSize > 1)
    {
        int iAns = AfxMessageBox(GET_SYS_STR(STR_MB_DELETE).c_str(), MB_YESNO);

        if (iAns != IDYES)
        {
            return;
        }
    }
    m_pView->GetPartsDef()->DeleteSelectingObject();
    m_pView->GetPartsDef()->ClearMouseOver();
    m_pView->GetPartsDef()->ClearMouseEmphasis();

}


StdString CActionCommand::_GetPropertyGroup(StdString strPropertySet) const
{
    std::vector<StdString> lstProp;
    CUtil::TokenizeCsv(&lstProp, strPropertySet,  _T('.'));
    if(lstProp.size() > 1)
    {
        return lstProp[0];
    }
    return strPropertySet;
}


void CActionCommand::Redraw() const
{
    if(m_eDrag == E_DS_AREA)
    {
        CRect rect(m_ptSelStart.x, m_ptSelStart.y, m_ptSelLast.x, m_ptSelLast.y);
        rect.NormalizeRect();
        DrawFocusRect(m_pView->GetViewDc(), rect);
        DrawFocusRect(m_pView->GetSelDc(), rect);
    }
}

void CActionCommand::_DeleteTmpObjects()
{
    m_lstTmpObjects.clear();
}

