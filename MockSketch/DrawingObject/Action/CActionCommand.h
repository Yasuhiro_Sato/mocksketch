/**
 * @brief			CAActionヘッダーファイル
 * @file			CActionCommand.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef _ACTION_COMMAND_H__
#define _ACTION_COMMAND_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CViewAction.h"
#include "DrawingObject/Primitive/POINT2D.h"
#include "DrawingObject/Primitive/RECT2D.h"

class     CDrawingView;
class     CNodeBound;
class     CPartsDef;
class     CUndoAction;
struct    SNAP_DATA;

/**
 * @class   CActionCommand
 * @brief                        
   
 */
class CActionCommand: public CViewAction
{
public:
    enum E_MENU_TYPE
    {
        E_MNUT_NONE,
        E_MNUT_CONNECTION,
        E_MNUT_OTHER,
    };

    enum E_DRAG_STATE
    {
        E_DS_NONE,
        E_DS_AREA,
        E_DS_BOUND,
        E_DS_NODE,
    };

protected:

    E_DRAG_STATE m_eDrag;

    //bool m_bDrag;
    
    //bool m_bNodeMarker;

    //StdString  m_strNodeMarkerId;

    //POINT2D m_ptStartNode;

    POINT m_ptSelStart;
    
    POINT m_ptSelLast;

    int m_iClick; //継承用

    //再選択用
    std::vector<CDrawingObject*> m_lstReSelect;
    //======================================
    // NodeBound用
    //======================================
    double m_dXScl;

    double m_dYScl;

    double m_dAngle;

    POINT2D m_ptMove;

    std::vector< std::vector<CDrawingObject* > > m_lstTmpObjects;

    std::vector<std::unique_ptr<MAT2D>> m_lstOffset;

    RECT2D m_rcBound;

    //======================================

public:
	//!< コンストラクタ
    CActionCommand();

    //!< コンストラクタ
    CActionCommand(CDrawingView* pView);
    
	//!< デストラクタ
    virtual ~CActionCommand();

	//!< 選択解放時動作
    virtual void Cancel(VIEW_MODE eMode);

	//!< 選択解放時動作
	virtual bool SelAction(VIEW_MODE eMode, void* pParam = NULL);

	//!< マウスクリック動作
	virtual bool SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

	//!< マウス移動動作
	virtual bool MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

    //!<コンテキストメニュー表示
    virtual bool OnContextMenu(POINT point);


     //!< 再描画
    virtual void Redraw() const;

    bool OnLButtonUp(MOUSE_MOVE_POS posMouse);
    bool OnLButtonDown(MOUSE_MOVE_POS posMouse);
    bool OnMouseMove(MOUSE_MOVE_POS posMouse);
    bool OnRButtonUp(MOUSE_MOVE_POS posMouse);
    bool OnRButtonDown(MOUSE_MOVE_POS posMouse);
    bool OnClickLButton(MOUSE_MOVE_POS posMouse);

protected:
    //!< 選択動作
    bool SelectAction(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

    //!< 動作クリア
    virtual void ClearAction(bool bReleaseSelect = true);

    //!< 選択矩形解除
    void CancelSelectRect();

    //!< 削除
    void Delete();



    StdString _GetPropertyGroup(StdString strPropertySet) const;

    bool _GetMousePointLengthAngle(SNAP_DATA* pSnapData, 
                                              const StdString& strNodeMarkerId,
                                              MOUSE_MOVE_POS posMouse);

	//bool _InitNodeBound();

    void _DeleteTmpObjects();

    bool _MovePointCreateParts(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);
    bool _MovePointNone(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);
    bool _MovePointArea(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);
    //bool _MovePointNode(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

protected:

};
#endif // _ACTION_COMMAND_H__
