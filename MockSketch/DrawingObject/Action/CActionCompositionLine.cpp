/**
 * @brief			CActionCompositionLine実装ファイル
 * @file			CActionCompositionLine.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MockSketch.h"

#include "./CActionCompositionLine.h"

#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/View/CDrawingView.h"

#include "DrawingObject/CDrawingObject.h"
#include "DrawingObject/CDrawingPoint.h"
#include "DrawingObject/CDrawingLine.h"
#include "DrawingObject/CDrawingCircle.h"
#include "DrawingObject/CDrawingSpline.h"
#include "DrawingObject/CDrawingCompositionLine.h"

#include "View/CLayer.h"
#include "View/CDrawMarker.h"
#include "View/CUndoAction.h"
#include "Utility/CUtility.h"
#include "System/CSystem.h"
#include "System/MOCK_ERROR.h"

#include <math.h>

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 * コンストラクタ
 */
CActionCompositionLine::CActionCompositionLine():
CViewAction(),
m_pCompositionLine(0)   ,
m_iReplaceId(-1)
{
    m_lMouseOver =  SEL_LINE| SEL_CIRCLE | SEL_ELLIPSE | SEL_SPLINE | SEL_COMPOSITION_LINE;
}

/**
 * コンストラクタ
 */
CActionCompositionLine::CActionCompositionLine(CDrawingView* pView):
CViewAction(pView),
m_pCompositionLine(0),
m_iReplaceId(-1)
{
    m_lMouseOver =  SEL_LINE| SEL_CIRCLE | SEL_ELLIPSE | SEL_SPLINE | SEL_COMPOSITION_LINE;
}
    
/**
 * デストラクタ
 */
CActionCompositionLine::~CActionCompositionLine()
{
    
}

/**
 *  @brief   解放時動作
 *  @param   [in]    eMode 解放時点でのモード
 *  @retval   なし
 *  @note
 */
void CActionCompositionLine::Cancel(VIEW_MODE eMode) 
{
    if (m_pCompositionLine)
    {
        int iAns = AfxMessageBox(GET_STR(STR_DLG_LOOP_REG), MB_YESNO);
        if (iAns == IDYES)
        {
            AddLoop();
        }
    }
    ClearAction(false);    
}


bool  CActionCompositionLine::OnContextMenu(POINT point)
{
    STD_ASSERT(m_pView != NULL);
    CPartsDef* pDef = m_pView->GetPartsDef();
    if (!pDef)
    {
        return false;
    }


    CMenu menu;
    menu.CreatePopupMenu();

     CWnd* pWnd = const_cast<CWnd*>(m_pView->GetWindow());

    if (m_pCompositionLine)
    {
        menu.AppendMenu(MF_STRING, ID_MNU_END, GET_STR(STR_MNU_END));
    }
    menu.AppendMenu(MF_STRING, ID_MNU_CANCEL, GET_STR(STR_MNU_CANCEL));

    if (m_pCompositionLine)
    {
        if(m_pCompositionLine->GetNumberOfObjects() != 0)
        {
            menu.AppendMenu(MF_SEPARATOR);
            menu.AppendMenu(MF_STRING, ID_MNU_DELETE, GET_STR(STR_MNU_DEL_LAST));
        }
    }

    UINT uiMenu = menu.TrackPopupMenu(
        TPM_LEFTALIGN  |    //クリック時のX座標をメニューの左辺にする
        TPM_RIGHTBUTTON|    //右クリックでメニュー選択可能とする
        TPM_RETURNCMD,      //関数の戻り値として、ユーザーが選択したメニュー項目の識別子を返します。
        point.x,point.y,    //メニューの表示位置
        pWnd                //このメニューを所有するウィンドウ
    );

    if(uiMenu == ID_MNU_END)
    {
        if (m_pCompositionLine->GetNumberOfObjects() != 0)
        {
            AddLoop();
        }
    }
    else if(uiMenu == ID_MNU_CANCEL)
    {
        if (m_pCompositionLine)
        {
            ClearAction(false);
        }
        m_pView->SetViewMode(VIEW_SELECT);
    }
    else if(uiMenu ==ID_MNU_DELETE)
    {
        m_pCompositionLine->DelLast();
    }

    pDef->Redraw();

    return true;
}

/**
 *  @brief   選択時動作
 *  @param   [in]    eMode 選択時点でのモード
 *  @retval   なし
 *  @note
 */
bool CActionCompositionLine::SelAction(VIEW_MODE eMode, void* pParam) 
{
    //既に選択中の図形はあるか？
    long lSize = m_pView->GetPartsDef()->SelectNum();

    if (lSize != 0)
    {
        //選択を解除
        m_pView->GetPartsDef()->RelSelect();
    }   

    m_pView->SetComment(GET_SYS_STR(STR_SEL_COMPOSITION_LINE));
    if (m_pCompositionLine)
    {
        m_pCompositionLine.reset();
    }

    m_lstId.clear();

    m_eViewMode = eMode;
    return true;
}
 
/**
 *  @brief   元図形描画
 *  @param   [in] pView 表示View
 *  @retval  なし
 *  @note
 */
void CActionCompositionLine::DrawOrg(CDrawingView* pView ) const
{
    /*
    CPartsDef*   pCtrl  = m_pView->GetPartsDef();
    CDrawingObject*        pObj;

    if (pCtrl)
    {
        foreach(int iId, m_lstId)
        {
            pObj  = pCtrl->GetObjectById(iId);
            pObj->Draw(pView);
        }
    }
    */
}

/**
 *  @brief   再描画
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CActionCompositionLine::Redraw() const
{

}


/**
 *  @brief   マウスクリック動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionCompositionLine::SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    CPartsDef*   pCtrl  = m_pView->GetPartsDef();
    int                  iId  = m_pView->SearchPos(posMouse.ptSel);

    std::shared_ptr<CDrawingObject>     pObj;
    CUndoAction*       pUndo;

    pObj  = pCtrl->GetObjectById(iId);
    pUndo  = pCtrl->GetUndoAction();

    if (eAction != CViewAction::ACT_LBUTTON_UP)
    {
        //左クリックのみ
        return false;
    }

    if (!pObj)
    {
        //何もないところをクリック
        if(m_pCompositionLine)
        {
            if(iId == ID_DRAWING)
            {
                AddLoop();
                return true;
            }
        }
        return false;
    }

    if (m_pCompositionLine == NULL)
    {

        CDrawMarker* pMarker= m_pView->GetMarker();
        pMarker->ClearObject();

        m_pCompositionLine = std::make_shared<CDrawingCompositionLine>(ID_DRAWING);
        m_pCompositionLine->SetSelect(true);
        pMarker->AddObject(m_pCompositionLine);
    }

    POINT2D pt2D;
    int iLayerId = m_pView->GetCurrentLayerId();
    m_pView->ConvScr2World(&pt2D, posMouse.ptSel, iLayerId);

    if (m_iReplaceId == -1)
    {
        double dSnapSize;
        double dLen;
        dLen = m_pCompositionLine->Distance(pt2D);
        dSnapSize =  m_pView->GetSnapRadius() / 2.0;
        if (dLen < dSnapSize)
        {
            pObj = m_pCompositionLine;
        }
    }

    bool bInckude = m_pCompositionLine->CheckIncludeObject(iId);

    bool  bSimulation = false;
    EXCEPTION_TYPE eError = m_pCompositionLine->Add(pt2D, pObj.get(), bSimulation);
    if (eError == e_no_error)
    {
        m_lstId.push_back(iId);
    }
    else
    {
        AfxMessageBox( GET_ERR_STR(eError) );
        return false;
    }

    if (bInckude)
    {
        //すでに選択済みの図形を選択したら終了
        AddLoop();
        return true;
    }
    return false;
}

/**
 *  @brief   マウス移動動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionCompositionLine::MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    CPartsDef*   pDef  = m_pView->GetPartsDef();
    int          iId  = m_pView->SearchPos(posMouse.ptSel);

    if (!pDef)
    {
        return false;
    }
    pDef->ClearMouseOver();
    pDef->ClearMouseEmphasis();

    CDrawMarker* pMarker= m_pView->GetMarker();
    pMarker->ClearObject();

    int iLayerId = m_pView->GetCurrentLayerId();

    std::shared_ptr<CDrawingObject> pSelObj;
    auto    pObj  = pDef->GetObjectById(iId);


    pSelObj = pObj;

    if (!m_pCompositionLine)
    {
        bool bStart = false;
        bStart = IsActionType(m_lMouseOver, pSelObj.get());
        if (bStart)
        {
            pSelObj->DrawOverStart(m_pView);
            pDef->SetMouseOver(pSelObj);
        }
    }
    else
    {
        if(!pObj)
        {
            m_psAddObj = CDrawingObject::CloneShared(m_pCompositionLine.get());
            m_psAddObj->SetSelect(true);
            m_psAddObj->SetVisible(true);
            m_psAddObj->SetLayer(iLayerId);
            pMarker->AddObject(m_psAddObj);
            pDef->DrawDragging(m_pView);
            return true;
        }

        EXCEPTION_TYPE eType;
        POINT2D pt2D;

        m_pView->ConvScr2World(&pt2D, posMouse.ptSel, iLayerId);

        if (m_iReplaceId == -1)
        {
            //自分自身との接続を再優先とする
            double dSnapSize;
            double dLen;
            dLen = m_pCompositionLine->Distance(pt2D);
            dSnapSize =  m_pView->GetSnapRadius() / 2.0;
            if (dLen < dSnapSize)
            {
                pSelObj = m_pCompositionLine;
            }
        }

        bool bSimulation = true; 
        bool bInclude = true;


        eType = m_pCompositionLine->Add(pt2D, pSelObj.get(), bSimulation, &m_psAddObj);
        if (eType != e_no_error)
        {
            m_psAddObj = CDrawingObject::CloneShared(m_pCompositionLine.get());
        }

        if (m_psAddObj)
        {
            // m_pViewのマーカに表示する、目的は以下の２つ
            // 1．インデックスバッファーに書き込まない
            // 2. 最上位に描画される
            pMarker->HideObject(false);
            m_psAddObj->SetSelect(false);
            m_psAddObj->SetColor(DRAW_CONFIG->crImaginary);
            m_psAddObj->SetVisible(true);
            m_psAddObj->SetLayer(iLayerId);
            pDef->ClearMouseOver();

            int iWidth;
            if(pObj)
            {
                iWidth = pObj->GetLineWidth();
            }
            else
            {
                iWidth = m_psAddObj->GetLineWidth();
            }
            m_psAddObj->SetLineWidth(iWidth + 2);

            pDef->SetMouseOver(m_psAddObj);
            pMarker->AddObject(m_psAddObj);
            pDef->SetMouseOver(m_psAddObj);
        }
    }

    pDef->DrawDragging(m_pView);
    return true;
}

/**
 *  @brief   ループ登録
 *  @param   なし
 *  @retval  なし
 *  @note    
 */
void CActionCompositionLine::AddLoop()
{
    CPartsDef*   pDef  = m_pView->GetPartsDef();

    if (m_pCompositionLine == NULL)
    {
        return;
    }
    m_pCompositionLine->SetLayer(m_pView->GetCurrentLayerId());

    CUndoAction* pUndo  = pDef->GetUndoAction();
    bool bEnd = true;
    bool bDelOlg = false;
    if (m_iReplaceId == -1)
    {
        pDef->RegisterObject(m_pCompositionLine, 
            true,     //bCreateId,
            false);   // bKeepUnfindId
        pUndo->Add(UD_ADD, pDef, NULL, m_pCompositionLine, bEnd);

        //元図形を削除しますか？
        int iAns = AfxMessageBox(GET_STR(STR_DLG_DEL_ORG), MB_YESNO);
        if (iAns == IDYES)
        {
            bDelOlg = true;
        }
    }
    else
    {
        auto pLoop = std::dynamic_pointer_cast<CDrawingCompositionLine>(pDef->GetObjectById(m_iReplaceId));
        pUndo->AddStart(UD_CHG, pDef, pLoop.get());
        pLoop->GetSegmetList()->clear();
        *(pLoop->GetSegmetList()) = *(m_pCompositionLine->GetSegmetList());
        pUndo->AddEnd(pLoop);
        bDelOlg = true;
    }
    ClearAction(bDelOlg);

    m_pCompositionLine= NULL;
    m_pView->SetViewMode(VIEW_SELECT);
}

/**
 *  動作クリア
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CActionCompositionLine::ClearAction(bool bDeleteOrg)
{
    m_bMark = false;

    CDrawMarker*        pMarker = m_pView->GetMarker();
    CPartsDef*    pCtrl   = m_pView->GetPartsDef();

    pMarker->Clear();
    pMarker->ClearObject();

    pCtrl->RelSelect();
    pCtrl->ClearMouseOver();
    pCtrl->ClearMouseEmphasis();
    pCtrl->ClearMouseConnectable();
    pCtrl->RelSelect();
    m_pView->ResetToolTipText();

    if (bDeleteOrg)
    {
        //元データを削除
        CUndoAction*       pUndo  = pCtrl->GetUndoAction();
        for(int iId: m_lstId)
        {
            if (m_iReplaceId !=  iId)
            {
                pCtrl->SelectDrawingObject(iId);
                auto pObj = pCtrl->GetObjectById(iId);
                pUndo->Add(UD_DEL, pCtrl, pObj, NULL, false);
            }
        }
        pUndo->Push();
        pCtrl->DeleteSelectingObject();
    }

    m_lstId.clear();
    m_pCompositionLine.reset();

    for (int iCnt = 0; iCnt < 2; iCnt++)
    {
        if (m_psAddtionalObj[iCnt])
        {
            m_psAddtionalObj[iCnt].reset();
        }
    }

    m_iReplaceId = -1;
    m_psAddObj.reset();
    m_pCloseLine.reset();
    pCtrl->Redraw(m_pView);
}
