/**
 * @brief			CActionCompositionLineヘッダーファイル
 * @file			CActionCompositionLine.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef _ACTION_LOOP_H__
#define _ACTION_LOOP_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CViewAction.h"
class     CDrawingView;
class     POINT2D;
class     CDrawingCompositionLine;
enum      EXCEPTION_TYPE;


/**
 * @class   CActionCompositionLine
 * @brief                        
 */
class CActionCompositionLine: public CViewAction
{
public:
	//!< コンストラクタ
    CActionCompositionLine();

    //!< コンストラクタ
    CActionCompositionLine(CDrawingView* pView);
    
	//!< デストラクタ
    virtual ~CActionCompositionLine();

	//!< 選択解放時動作
    virtual void Cancel(VIEW_MODE eMode);

	//!< 選択解放時動作
	virtual bool SelAction(VIEW_MODE eMode, void* pParam = NULL);

	//!< マウスクリック動作
	virtual bool SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

	//!< マウス移動動作
	virtual bool MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

    //!< 元図形描画
    void DrawOrg(CDrawingView* pView ) const;

    //!< 再描画
    virtual void Redraw() const;

    //!<コンテキストメニュー表示
    virtual bool OnContextMenu(POINT point);

protected:
    void ClearAction(bool bDeleteOrg);

    void AddLoop();

protected:

    //!< 選択したオブジェクトを記録する
    std::vector<int> m_lstId;

    // 失敗時に削除する
    std::shared_ptr<CDrawingCompositionLine> m_pCompositionLine;

    // 閉じるときに表示する複合線
    std::shared_ptr<CDrawingCompositionLine> m_pCloseLine;

    int m_iReplaceId;

};
#endif // _ACTION_LOOP_H__
