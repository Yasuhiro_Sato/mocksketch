/**
 * @brief			CViewAction実装ファイル
 * @file			CViewAction.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CActionConnectionLine.h"

#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/View/CDrawingView.h"

#include "DrawingObject/CDrawingObject.h"
#include "DrawingObject/CDrawingPoint.h"
#include "DrawingObject/CDrawingLine.h"
#include "DrawingObject/CDrawingCircle.h"
#include "DrawingObject/CDrawingSpline.h"
#include "DrawingObject/CDrawingEllipse.h"
#include "DrawingObject/CDrawingNode.h"
#include "DrawingObject/CDrawingConnectionLine.h"
#include "DrawingObject/CNodeData.h"

#include "View/CLayer.h"
#include "View/CDrawMarker.h"
#include "MockSketch.h"
#include "View/CUndoAction.h"
#include "Utility/CUtility.h"
#include "System/CSystem.h"
#include <math.h>

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//!< コンストラクタ
CActionConnectionLine::CActionConnectionLine():
CActionConnectionLine(NULL)
{
}

//!< コンストラクタ
CActionConnectionLine::CActionConnectionLine(CDrawingView* pView):
CViewAction(pView),
m_bConnect(true)
{
    m_lMouseOver        = SEL_PRI;
    for (auto p : m_pSelObj)
    {
        p.reset();
    }
}

/**
 *  解放時動作
 *  @param   [in]    eMode 解放時点でのモード
 *  @retval   なし
 *  @note
 */
void CActionConnectionLine::Cancel(VIEW_MODE eMode)
{
    _ClearAction();
}

/**
 *  選択時動作
 *  @param   [in]    eMode 選択時点でのモード
 *  @retval   なし
 *  @note
 */
bool CActionConnectionLine::SelAction(VIEW_MODE eMode, void* pParam)
{
    //既に選択中の図形はあるか？

    CPartsDef*    pDef   = m_pView->GetPartsDef();
    if (!pDef)
    {
        return false;
    }

    long lSize = pDef->SelectNum();

    if (lSize != 0)
    {
        //選択を解除
        pDef->RelSelect();
    }   

    m_pView->SetComment(GET_SYS_STR(STR_SEL_POINT1));
    m_eViewMode = eMode;
    return true;    

}

/**
 *  マウスクリック動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval   なし
 *  @note
 */
bool CActionConnectionLine::SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    CPartsDef*   pDef = m_pView->GetPartsDef();
    if (!pDef)
    {
        return false;
    }

    int                  iId  = m_pView->SearchPos(posMouse.ptSel);
    auto     pObj  = pDef->GetObjectById(iId);
    VIEW_MODE      eViewMode  = m_pView->GetViewMode();

    POINT ptSel = posMouse.ptSel;
    UINT nFlags = posMouse.nFlags;

    if (eAction != CViewAction::ACT_LBUTTON_UP)
    {
        //左クリックのみ
        return false;
    }

    //!< ビュー->データ変換
    POINT2D pt2D;
    int iLayerId = m_pView->GetCurrentLayerId();
    m_pView->ConvScr2World(&pt2D, ptSel, iLayerId);

    //!< マーカークリック確認
    POINT2D ptMarker;
    bool    bMarker = false;
    if(_CheckMarker( &ptMarker, ptSel, eAction))
    {
        pt2D = ptMarker;
        bMarker = true;
    }

    if (m_iClickCnt == 0)
    {
        if (pObj != NULL)
        {
            if ((!pObj->IsSelect()) &&
                (pObj->GetType() == DT_CONNECTION_LINE))
            {
                //選択を割り込み
                m_pView->InterruptAction(VIEW_SELECT, posMouse);
                return true;
            }
        }
    }

    return _SelConnect  (pt2D, pObj, bMarker);

}

/**
 *  @brief   マウス移動動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */

bool CActionConnectionLine::MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    POINT ptSel = posMouse.ptSel;
    UINT nFlags = posMouse.nFlags;

    //!< スナップ点取得
    bool bSnap;
    bool bIgnoreSnap = false;

    CPartsDef*   pDef = m_pView->GetPartsDef();
    if (!pDef)
    {
        return false;
    }

    pDef->RedrawWait();  //TODO:どうする？


    CDrawMarker* pMarker= m_pView->GetMarker();
    pMarker->Clear();
    pMarker->ClearObject();
    pDef->ClearMouseOver();
    pDef->ClearMouseEmphasis();
    pDef->ClearMouseConnectable();




    DWORD dwSnapType = DRAW_CONFIG->GetSnapType();
    dwSnapType |= VIEW_COMMON::SNP_NODE;

    SNAP_DATA snap;
    bSnap = m_pView->GetSnapPoint(&snap,
                        dwSnapType,
                        ptSel);
    auto pObj1 = pDef->GetObjectById(snap.iObject1);
    auto pObj2 = pDef->GetObjectById(snap.iObject2);
 
    //==================================

    POINT ptOffset;
    ptOffset.x = ptOffset.y = 0;
    CDrawMarker::MARKER_DATA marker;
    marker.ptOffset = ptOffset;

    if (nFlags & MK_SHIFT)
    {
        //スナップ無視
        bIgnoreSnap = true;
        bSnap = false;
    }
    m_pView->SetDrawingMode(DRAW_SEL);

    //==================================

    if ((pObj1 != NULL) &&
        (pObj2 != NULL))
    {
        //交点
        pDef->SetMouseEmphasis(pObj1);
        pDef->SetMouseEmphasis(pObj2);
        marker.eType = CDrawMarker::MARKER_CROSS;
        marker.eSnap = VIEW_COMMON::SNP_CROSS_POINT;
        pMarker->AddMarker(marker, snap.pt);
    }

    auto pConnection = std::dynamic_pointer_cast<CDrawingConnectionLine>(m_psAddObj);

    if ((pObj1 != NULL ) &&
        (pObj2 == NULL ))
    {

        DRAWING_TYPE eType = pObj1->GetType();
        if (eType & DT_CONNECTION_LINE)
        {
            //割り込み選択が可能なのでマウスオーバー表示を行う
            pDef->SetMouseOver(pObj1);
        }


        CNodeData* pNodeData = NULL;

        if(pConnection)
        {
            pNodeData = pConnection->GetNodeData(0);
        }

        m_pView->SetMarkerWithAddSnapList(pObj1.get(), NULL, pNodeData, false /*bIgnoreOffLineObject*/);
        pDef->SetMouseEmphasis(pObj1);
    }

    //１点目未選択
    if (snap.eSnapType == VIEW_COMMON::SNP_NONE)
    {
        //任意点
        CDrawMarker*pMarker = m_pView->GetMarker();
        marker.eType = CDrawMarker::MARKER_CROSS;
        marker.eSnap = VIEW_COMMON::SNP_CROSS_POINT;
        pMarker->AddMarker(marker, snap.ptOrg);
    }

    int iMarkerId = pMarker->MouseMove(posMouse.ptSel);
    m_iConnectionIndex[m_iClickCnt] = snap.iConnectionIndex;


     bool bTmpConnect = true;

    if (m_iClickCnt == 1)
    {
        pMarker->AddObject(m_psAddObj);
        m_pSelObj[1] = NULL;
        m_iConnectionIndex[1] = -1;
        m_pConnectionData[1] = NULL;

        bool bConnect = false;
        if (snap.iConnectionIndex != -1)
        {
            CNodeData* pCdEnd = NULL;
            if(pObj1)
            {
                // 接続の可不可を判定する
                // 接続対象がNODEの場合
                pCdEnd = pObj1->GetNodeData(snap.iConnectionIndex);
                m_pSelObj[1] = pObj1;

            }

            if(m_pConnectionData[0])
            {
                //始点と接続可能か
                if(!m_pConnectionData[0]->IsAbleToConnectEndToEnd(pCdEnd))
                {
                    bTmpConnect = false; 
                }
            }

            if (bTmpConnect)
            {
                m_iConnectionIndex[1] = snap.iConnectionIndex;
                m_pConnectionData[1] = pCdEnd;
                bConnect = true;
            }
        }

        if(!bConnect)
        {
            m_iConnectionIndex[1] = -1;
            m_pConnectionData[1] = NULL;
        }

        pConnection->SetConnectionEnd(m_pSelObj[1].get(), m_iConnectionIndex[1]);
        pConnection->SetEnd(snap.pt);
        pConnection->RecalcMidPoints(NULL, 1, false);
    }

    m_bConnect = bTmpConnect;  //マウスカーソル用
    SetSnapTooltip(snap.eSnapType, 0, 0);
    pDef->DrawDragging(m_pView);
    return true;
}

/**
 * @brief   マウスクリック
 * @param   [in]     pt2D    マウスカーソル位置
 * @param   [in]     pObj    カーソル位置のオブジェクト
 * @param   [in]     bMarker  マーカー選択
 * @retval  true  
 * @note	
 */
bool CActionConnectionLine::_SelConnect(POINT2D pt2D, std::shared_ptr<CDrawingObject>  pObj, bool bMarker)
{
    bool bRet = false;
    if (m_iClickCnt == 0)
    {
        bRet = _SelConnect1( pt2D,  pObj, bMarker);
    }
    else if(m_iClickCnt == 1)
    {
        bRet = _SelConnect2( pt2D,  pObj, bMarker);
    }
    return bRet;

}


bool CActionConnectionLine::_SelConnect1(POINT2D pt2D, std::shared_ptr<CDrawingObject>  pObj, bool bMarker)
{
    m_pSelObj[0] = NULL;
    if (pObj != NULL)
    {
           
        if (pObj->GetType() != DT_IMAGE)
        {
            m_pSelObj[0] = pObj;
        }
    }
    //-----------------------------------------
    m_ptSel[0]   = pt2D;

    m_pConnectionData[0] = NULL;
    if (bMarker)
    {
        if(m_iConnectionIndex[0] != -1)
        {
            m_pConnectionData[0] = pObj->GetNodeData(m_iConnectionIndex[0]);
        }
    }

	auto pCreateObj = THIS_APP->CreateDefaultObject(DT_CONNECTION_LINE);
	auto defObj = std::dynamic_pointer_cast<CDrawingConnectionLine>(pCreateObj);
	auto pConnect = std::make_shared<CDrawingConnectionLine>(*(defObj.get()));

    pConnect->SetPartsDef(m_pView->GetPartsDef());
    pConnect->SetColor(DRAW_CONFIG->crImaginary);
	if (!THIS_APP->GetDefaultObjectPriorty(DT_CONNECTION_LINE))
	{
		pConnect->SetLineType(m_pView->GetCurrentLayer()->iLineType);
		pConnect->SetLineWidth(m_pView->GetCurrentLayer()->iLineWidth);
	}
    pConnect->SetStart(pt2D);
    pConnect->SetLayer(m_pView->GetCurrentLayerId());

    if(m_pConnectionData[0] != NULL)
    {
        pConnect->SetConnectionStart(m_pSelObj[0].get(), m_iConnectionIndex[0]);
    }

    m_psAddObj = pConnect;
    m_iClickCnt = 1;
    return true;   
}

bool CActionConnectionLine::_SelConnect2(POINT2D pt2D, std::shared_ptr<CDrawingObject>  pObj, bool bMarker)
{
    CPartsDef*   pDef = m_pView->GetPartsDef();

    if ((m_pSelObj[0] == pObj) && pObj)
    {
    }
    else
    {
        auto pConnectionLine = std::dynamic_pointer_cast<CDrawingConnectionLine>(m_psAddObj);
        pConnectionLine->SetEnd(pt2D);

		if (!THIS_APP->GetDefaultObjectPriorty(DT_CONNECTION_LINE))
		{
			pConnectionLine->SetColor(m_pView->GetCurrentLayer()->crDefault);
		}
		else
		{
			COLORREF cr = THIS_APP->GetDefaultObject(DT_CONNECTION_LINE)->GetColor();
			pConnectionLine->SetColor(cr);
		}

        auto pNew = AddObject(pConnectionLine.get());
        m_psAddObj.reset();

        auto pConnectionLineNew = std::dynamic_pointer_cast<CDrawingConnectionLine>(pNew);

        if(m_pConnectionData[0] != NULL)
        {
            pConnectionLineNew->SetConnectionStart(m_pSelObj[0].get(), m_iConnectionIndex[0]);
        }

        if(m_pConnectionData[1] != NULL)
        {
            pConnectionLineNew->SetConnectionEnd(m_pSelObj[1].get(), m_iConnectionIndex[1]);
        }

        m_iClickCnt = 0;
        return true;
    }
    return false;   
}


//!<マウスカーソル設定
bool CActionConnectionLine::OnSetCursor()
{
    if(!m_bConnect)
    {
        ::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_NO));
        return true;
    }
    return false;
}

/**
 *  @brief   入力値設定
 *  @param   [in]    strVal 入力値
 *  @retval  false エラー発生
 *  @note    必要であればエラー表示を行う
 *  @note    エラー発生で入力コンボボックスは選択状態になる      
 *  @note    エラーがない場合はコンボボックスに値が登録される
 */
bool CActionConnectionLine::InputValue(StdString strVal)
{

    return false;
}

/**
 *  動作クリア
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CActionConnectionLine::_ClearAction()
{
    m_bMark = false;

    m_iClickCnt = 0;

    CDrawMarker*        pMarker = m_pView->GetMarker();
    CPartsDef*   pDef = m_pView->GetPartsDef();
    if (!pDef)
    {
        return;
    }

    pMarker->Clear();
    pDef->RelSelect();

    pDef->Redraw();

    if (m_psAddObj)
    {
        pMarker->ClearObject();
    }

    VIEW_MODE      eViewMode  = m_pView->GetViewMode();


    for (auto p : m_pSelObj)
    {
        p.reset();
    }
    pDef->ClearMouseOver();
    pDef->ClearMouseEmphasis();

    SelAction(eViewMode);
    m_bConnect = true;
    m_pView->SetComment(GET_SYS_STR(STR_SEL_POINT1));
}

