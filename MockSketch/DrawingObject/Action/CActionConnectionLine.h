/**
 * @brief			CActionConnectionLineヘッダーファイル
 * @file			CActionConnectionLine.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#if !defined(_ACTION_CONNECTION_LINE_H_)
#define _ACTION_CONNECTION_LINE_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "View/VIEW_MODE.h"
#include "CViewAction.h"
#include "DrawingObject/Primitive/POINT2D.h"
class     CDrawingView;
class     CNodeData;
/**
 * @class   CActionConnectionLine
 * @brief                        
   
 */
class CActionConnectionLine: public CViewAction
{

public:
	//!< コンストラクタ
    CActionConnectionLine();

    //!< コンストラクタ
    CActionConnectionLine(CDrawingView* pView);
    
	//!< デストラクタ
    virtual ~CActionConnectionLine(){;}


	//!< 選択解放時動作
	virtual void Cancel(VIEW_MODE eMode);

	//!< 選択解放時動作
	virtual bool SelAction(VIEW_MODE eMode, void* pParam = NULL) override;

	//!< マウスクリック動作
	virtual bool SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction) override;

    virtual bool OnSetCursor() override; 
private:

	//!< マウスクリック動作
	virtual bool _SelConnect(POINT2D pt2D,  std::shared_ptr<CDrawingObject>  pObj, bool bMarker);
	virtual bool _SelConnect1(POINT2D pt2D, std::shared_ptr<CDrawingObject>  pObj, bool bMarker);
	virtual bool _SelConnect2(POINT2D pt2D, std::shared_ptr<CDrawingObject>  pObj, bool bMarker);



    //!< 動作クリア
    void _ClearAction();

    //!<  入力値設定
    virtual bool  InputValue(StdString strVal);

    //!< マウス移動動作
    virtual bool MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

private:

private:
    //!< 選択位置
    POINT2D              m_ptSel[2];

    //!< 初回選択オブジェクト
    std::shared_ptr<CDrawingObject>      m_pSelObj[2];

    //!<接続相手のNodeDataのインデックス 0:始点 1:終点
    int           m_iConnectionIndex[2];

    //!<接続相手のNodeData 0:始点 1:終点 
    CNodeData*    m_pConnectionData[2];

private:
    bool                m_bConnect;   
};
#endif // !defined(_ACTION_POINT_H_)
