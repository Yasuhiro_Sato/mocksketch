/**
 * @brief			CActionCorner実装ファイル
 * @file			CActionCorner.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CActionCorner.h"

#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/View/CDrawingView.h"

#include "DrawingObject/CDrawingObject.h"
#include "DrawingObject/CDrawingPoint.h"
#include "DrawingObject/CDrawingLine.h"
#include "DrawingObject/CDrawingCircle.h"
#include "DrawingObject/CDrawingSpline.h"


#include "View/CLayer.h"
#include "View/CDrawMarker.h"
#include "View/CUndoAction.h"
#include "Utility/CUtility.h"
#include "System/CSystem.h"
#include <math.h>

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 * コンストラクタ
 */
CActionCorner::CActionCorner():
CViewAction()
{
    m_lMouseOver =  SEL_SEGMENT;
    m_dLen = 0.0;
    m_Select[0].pObject = NULL;
    m_Select[1].pObject = NULL;
    m_iSelCnt = 0;
}

/**
 * コンストラクタ
 */
CActionCorner::CActionCorner(CDrawingView* pView):
CViewAction(pView)
{
    m_lMouseOver =  SEL_SEGMENT;
    m_dLen = 0.0;
    m_Select[0].pObject = NULL;
    m_Select[1].pObject = NULL;
    m_iSelCnt = 0;


}
    
/**
 * デストラクタ
 */
CActionCorner::~CActionCorner()
{
    
}

/**
 *  @brief   解放時動作
 *  @param   [in]    eMode 解放時点でのモード
 *  @retval   なし
 *  @note
 */
void CActionCorner::Cancel(VIEW_MODE eMode) 
{
    //選択解除
    ClearAction();

}

/**
 *  @brief   選択時動作
 *  @param   [in]    eMode 選択時点でのモード
 *  @retval   なし
 *  @note
 */
bool CActionCorner::SelAction(VIEW_MODE eMode, void* pParam) 
{
    
    //既に選択中の図形はあるか？
    long lSize = m_pView->GetPartsDef()->SelectNum();

    if (lSize != 0)
    {
        //選択を解除
        m_pView->GetPartsDef()->RelSelect();
    }   

    m_pView->SetComment(GET_SYS_STR(STR_SEL_CORNER1));
    m_Select[0].pObject = NULL;
    m_Select[1].pObject = NULL;
    m_iSelCnt = 0;

    // 入力エリアにフォーカスを設定する
    m_pView->SetInputFocus();
    //m_pView->SetComment(GET_SYS_STR(STR_SEL_CORNER_RAD));
    m_eViewMode = eMode;


    if (m_eViewMode == VIEW_CORNER)
    {
        m_lMouseOver =   SEL_SEGMENT;
        m_bChamfer   = false;
    }
    else
    {
        m_lMouseOver =   SEL_LINE;
        m_bChamfer   = true;
    }
    return true;
}

/**
 *  @brief   マウス移動動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionCorner::MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    POINT ptSel = posMouse.ptSel;
    UINT nFlags = posMouse.nFlags;

    //!< スナップ点取得
    bool bIgnoreSnap = false;

    if (posMouse.nFlags & MK_SHIFT)
    {
        bIgnoreSnap = true;
    }

    CPartsDef*   pDef  = m_pView->GetPartsDef();
    CDrawMarker* pMarker= m_pView->GetMarker();

    if (!pDef)
    {
        return false;
    }

    bool bOnLine = false;
    bool bTrimClickSide = true; //最初にクリックした側を調整する

    pMarker->Clear();
    pMarker->ClearObject();
    pDef->ClearMouseOver();
    pDef->ClearMouseEmphasis();
    pDef->ClearMouseConnectable();

    using namespace VIEW_COMMON;

    SNAP_DATA snap;
    m_pView->GetMousePoint( &snap, posMouse);

    //bool bOnLine = false;
    bool bReverse = false;
    auto pObj1 = pDef->GetObjectById(snap.iObject1);
    auto pObj2 = pDef->GetObjectById(snap.iObject2);

    if (!m_Select[0].pObject)
    {
        bool bStart = false;
        bStart = IsActionType(m_lMouseOver, pObj1.get());
        if (bStart)
        {
            pObj1->DrawOverStart(m_pView);
            pDef->SetMouseOver(pObj1);
        }
    }
    else
    {
        bool bIntersect = false;

        POINT2D ptCreate;

        auto pFireatObj = m_Select[0].pObject;

        if (pFireatObj == pObj1)
        {
            return false;
        }


        m_Select[1].pObject = pObj1;
        m_Select[1].ptClick = snap.ptOrg;



        StdString sVal;
        sVal = m_pView->GetInputValue();
        if (sVal ==_T(""))
        {
        }
        else
        {
            bool bRet;
            bRet = InputValue(sVal);
            if (m_strOldVal != sVal)
            {
                m_pView->UpdateInput(bRet);
                if (bRet)
                {
                    m_strOldVal = sVal;
                }
            }
        }
 
        
        int iLayer = m_pView->GetCurrentLayerId();
        m_psEditingObject[0] = CDrawingObject::CloneShared(m_Select[0].pObject.get());
        m_psEditingObject[0]->SetLayer(iLayer);
        m_psEditingObject[0]->SetSelect(true);



        bool bRet = false;
        if(pObj1)
        {
            m_psEditingObject[1] = CDrawingObject::CloneShared(m_Select[1].pObject.get());

            if(m_bChamfer)
            {
                bRet = _CreateChamfer(&m_psAddObj,
                    m_dLen,
                    m_psEditingObject[0].get(),     
                    m_Select[1].ptClick,            
                    m_psEditingObject[1].get(),
                    m_Select[0].ptClick
                );
            }
            else
            {
                bRet = _CreateCorner(&m_psAddObj,
                    m_dLen,
                    m_psEditingObject[0].get(),     
                    m_Select[1].ptClick,            
                    m_psEditingObject[1].get(),
                    m_Select[0].ptClick
                    );
            }
        }

        if (bRet)
        {
            m_psEditingObject[0]->SetSelect(false);
            m_psEditingObject[0]->SetColor(DRAW_CONFIG->crImaginary);
            pDef->SetMouseOver(m_psEditingObject[0]);

            m_psEditingObject[1]->SetColor(DRAW_CONFIG->crImaginary);
            m_psEditingObject[1]->SetLayer(iLayer);

            pDef->SetMouseOver(m_psEditingObject[1]);
            pMarker->AddObject(m_psEditingObject[1]);

            if (m_psAddObj)
            {
                m_psAddObj->SetLineType(pObj1->GetLineType());
                m_psAddObj->SetLineWidth(pObj1->GetLineWidth());
                m_psAddObj->SetColor(DRAW_CONFIG->crImaginary);
                m_psAddObj->SetLayer(iLayer);
                pMarker->AddObject(m_psAddObj);
                pDef->SetMouseOver(m_psAddObj);
            }
            pMarker->HideObject(false);
        }
        pMarker->AddObject(m_psEditingObject[0]);

        //最初にクリックした場所を表示する
        CDrawMarker::MARKER_DATA marker;
        marker.crDraw = DRAW_CONFIG->crSelect;
        marker.eType = CDrawMarker::MARKER_DBL_CIRCLE;
        pMarker->AddMarker(marker, m_Select[0].ptClick);

        if (m_psAddObj)
        {
            
            pMarker->AddMarker(marker, m_Select[0].ptClick);
        }


    }
    pDef->DrawDragging(m_pView);
    return true;
}

/**
 *  @brief   マウスクリック動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionCorner::SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    POINT ptSel = posMouse.ptSel;
    UINT nFlags = posMouse.nFlags;

    CPartsDef*   pDef  = m_pView->GetPartsDef();
    int                  iId  = m_pView->SearchPos(ptSel);
    auto    pObj  = pDef->GetObjectById(iId);

    if (eAction != CViewAction::ACT_LBUTTON_UP)
    {
        //左クリックのみ
        return false;
    }

    //!< ビュー->データ変換
    POINT2D pt2D;
    int iLayerId = m_pView->GetCurrentLayerId();
    m_pView->ConvScr2World(&pt2D, ptSel, iLayerId);

    if (pObj == NULL)
    {
        if(m_iSelCnt == 1)
        {
            ClearAction();
        }
        return false;
    }


    StdString sVal;
    if (m_iSelCnt == 0)    
    {
        m_Select[0].ptClick = pt2D;
        m_Select[0].pObject = pObj;

        // 選択状態にする
        pDef->SelectDrawingObject(iId);
        m_iSelCnt++;

        sVal = m_pView->GetInputValue();
        if (sVal ==_T(""))
        {
            m_pView->SetComment(GET_SYS_STR(STR_SEL_CORNER_RAD));
            m_pView->SetInputFocus();
        }

    }

    if (m_iSelCnt == 1)
    {
        StdString sVal;
        sVal = m_pView->GetInputValue();
         InputValue(sVal);

        bool bRet;
 
        if(m_Select[0].pObject ==   pObj)
        {
            return false;
        }
        m_Select[1].ptClick = pt2D;
        m_Select[1].pObject = pObj;

        m_Select[0].pObject->SetSelect(false);
        m_Select[1].pObject->SetSelect(false);
        m_psEditingObject[0] = CDrawingObject::CloneShared(m_Select[0].pObject.get());
        m_psEditingObject[1] = CDrawingObject::CloneShared(m_Select[1].pObject.get());

        if(m_bChamfer)
        {
            bRet = _CreateChamfer(&m_psAddObj,
                m_dLen,
                m_psEditingObject[0].get(),     
                m_Select[1].ptClick,            
                m_psEditingObject[1].get(),
                m_Select[0].ptClick
            );
        }
        else
        {
            bRet = _CreateCorner(&m_psAddObj,
                m_dLen,
                m_psEditingObject[0].get(),     
                m_Select[1].ptClick,            
                m_psEditingObject[1].get(),
                m_Select[0].ptClick
            );
        }

        CLICK_POINT cp;
        cp.ptClick =  m_Select[1].ptClick;

        if (bRet)
        {

            pDef->SelectDrawingObject(m_Select[1].pObject);

            CUndoAction* pUndo  = pDef->GetUndoAction();


            pUndo->AddStart(UD_CHG, pDef, m_Select[0].pObject.get());
            m_psEditingObject[0]->SetId(m_Select[0].pObject->GetId());
            pDef->SelectDrawingObject(m_Select[0].pObject);
            pDef->DeleteSelectingObject();

            pDef->RegisterObject(m_psEditingObject[0], false/*bCreateId*/,false /*bKeepUnfindId*/);
            pUndo->AddEnd(m_psEditingObject[0]);
  
            int iObjId_1 = m_Select[1].pObject->GetId();
            pUndo->AddStart(UD_CHG, pDef, m_Select[1].pObject.get());
            m_psEditingObject[1]->SetId(iObjId_1);
            pDef->SelectDrawingObject(m_Select[1].pObject);
            pDef->DeleteSelectingObject();

            pDef->RegisterObject(m_psEditingObject[1], false/*bCreateId*/,false /*bKeepUnfindId*/);
            pUndo->AddEnd(m_psEditingObject[1]);

            cp.pObject = pDef->GetObjectById(iObjId_1);

            if (m_psAddObj)
            {
                auto eType = m_psAddObj->GetType();
                if(eType == DT_LINE)
                {
                    auto pLine = std::dynamic_pointer_cast<CDrawingLine>(m_psAddObj);
                    if (pLine)
                    {
                        pLine->CopyDrawParm(*m_psEditingObject[0].get());
                        pDef->RegisterObject(pLine, true/*bCreateId*/,false /*bKeepUnfindId*/);
                        pUndo->Add(UD_ADD, pDef, NULL, pLine);
                    }
                }
                else if(eType == DT_CIRCLE)
                {
                    auto pCircle = std::dynamic_pointer_cast<CDrawingCircle>(m_psAddObj);

                    if(pCircle)
                    {
                        pCircle->CopyDrawParm(*m_psEditingObject[0].get());
                        pDef->RegisterObject(pCircle, true/*bCreateId*/,false /*bKeepUnfindId*/);
                        pUndo->Add(UD_ADD, pDef, NULL, pCircle);
                    }
                }
            }
            pUndo->Push();
            _ClearContinue(&cp);
        }
    }
    return true;
}

/**
 *  @brief   入力値設定
 *  @param   [in]    strVal 入力値
 *  @retval  false エラー発生
 *  @note    必要であればエラー表示を行う
 *  @note    エラー発生で入力コンボボックスは選択状態になる      
 *  @note    エラーがない場合はコンボボックスに値が登録される
 */
bool CActionCorner::InputValue(StdString strVal)
{
    //後で計算可能にする
    if (strVal == _T(""))
    {
        return false;
    }

    double dLen;
    if(!ExecuteString(&dLen, strVal))
    {
        return false;
    }

    m_dLen =  dLen;
    return true;
}

bool CActionCorner::_CreateCorner(std::shared_ptr<CDrawingObject>* pCorner,
                                     double dRad,
                                     CDrawingObject* pObj1, 
                                     POINT2D&        ptDirObj1,
                                     CDrawingObject* pObj2,
                                     POINT2D&        ptDirObj2)
{


    std::vector<POINT2D> lstCornerCenter;
    const bool bOnlne = false;

    if( fabs(dRad) > NEAR_ZERO)
    {
        
        if(pObj1->GetType()== DT_SPLINE) 
        {
           auto pSpline = dynamic_cast<CDrawingSpline*>(pObj1);
           pSpline->CalcOffsetIntersection(&lstCornerCenter, dRad, ptDirObj1, 
                                              pObj2, dRad, ptDirObj2, bOnlne, TOLERANCE_MAX);
        }
        else if(pObj2->GetType()== DT_SPLINE)
        {
            auto pSpline = dynamic_cast<CDrawingSpline*>(pObj2);
            pSpline->CalcOffsetIntersection(&lstCornerCenter, dRad, ptDirObj2, 
                pObj1, dRad, ptDirObj1, bOnlne, TOLERANCE_MAX);

        }
        else
        {
            auto  offset1 = CDrawingObject::CloneShared(pObj1);
            auto  offset2 = CDrawingObject::CloneShared(pObj2);
            offset1->Offset(dRad, ptDirObj1);
            offset2->Offset(dRad, ptDirObj2);
            offset1->CalcIntersection(&lstCornerCenter, offset2.get(), bOnlne);
        }



    }
    else
    {
        //Rが０の場合
        pObj1->CalcIntersection(&lstCornerCenter, pObj2, bOnlne);

        POINT2D ptIntersection;

        if(ptDirObj2.NearPoint_P(&ptIntersection, lstCornerCenter))
        {
            pObj1->TrimCorner(ptIntersection, m_Select[0].ptClick);
            pObj2->TrimCorner(ptIntersection, m_Select[1].ptClick);
            return true;
        }
        return false;
    }
   

    if(lstCornerCenter.empty())
    {
        return false;         
    }


    POINT2D ptIntersection1;
    POINT2D ptIntersection2;
    bool bInterSect = false;

    std::shared_ptr<CDrawingCircle> crRet;
    crRet = std::dynamic_pointer_cast<CDrawingCircle>(*pCorner);
    if(!crRet)
    {
        *pCorner = std::make_shared<CDrawingCircle>();
        crRet =  std::dynamic_pointer_cast<CDrawingCircle>(*pCorner);
    }

    POINT2D ptCornerCenterNear;
    double dDistanceMin = DBL_MAX;
    for(POINT2D& pt : lstCornerCenter)
    {
        CDrawingCircle cr;
        cr.SetRadius(dRad);
        cr.SetCenter(pt);
        std::vector<POINT2D> lstPoint1;

        double dMin = TOLERANCE_MAX;
        cr.CalcIntersection(&lstPoint1, pObj1, bOnlne, dMin);
        if(lstPoint1.empty())
        {
            continue;
        }


        std::vector<POINT2D> lstPoint2;
        cr.CalcIntersection(&lstPoint2, pObj2, bOnlne, dMin);

        if(lstPoint2.empty())
        {
            continue;
        }

        double dDistance = m_Select[1].ptClick.Distance(pt);

        if (dDistanceMin > dDistance)
        {
            crRet->SetRadius(dRad);
            crRet->SetCenter(pt);
            dDistanceMin = dDistance;
            ptCornerCenterNear = pt;
            ptIntersection1 =  lstPoint1[0];
            ptIntersection2 =  lstPoint2[0];
            bInterSect = true;

#ifdef DEBUG
            if(lstPoint1.size() > 1)
            {

            }


            if(lstPoint2.size() > 1)
            {

            }

#endif

        }
    }

    if(bInterSect)
    {
        int iSide = CUtil::Side(ptCornerCenterNear, ptIntersection1, ptIntersection2);
        POINT2D p1 = ptIntersection1 - ptCornerCenterNear;
        double dA1 = p1.Angle();
        POINT2D p2 = ptIntersection2 - ptCornerCenterNear;
        double dA2 = p2.Angle();

        if(iSide == 0)
        {
            return false;
        }
        else if (iSide < 0)
        {
            //pt2 が pt->pt1 の右側にある
            crRet->SetStart(dA2);
            crRet->SetEnd(dA1);
            pObj1->TrimCorner(ptIntersection1, m_Select[0].ptClick);
            pObj2->TrimCorner(ptIntersection2, m_Select[1].ptClick);
        }
        else if (iSide > 0)
        {
            //pt2 が pt->pt1 の左側にある
            crRet->SetStart(dA1);
            crRet->SetEnd(dA2);
            pObj1->TrimCorner(ptIntersection1, m_Select[0].ptClick);
            pObj2->TrimCorner(ptIntersection2, m_Select[1].ptClick);
        }

        return true;
    }

    return false;
}


/**
 *  @brief   コーナー追加
 *  @param   [in]    bChamfer  true:面取り , false コーナー
 *  @retval   なし
 *  @note
 */
bool CActionCorner::_CreateChamfer(std::shared_ptr<CDrawingObject>* pCorner,
    double dLen,
    CDrawingObject* pObj1, 
    POINT2D&        ptDirObj1,
    CDrawingObject* pObj2,
    POINT2D&        ptDirObj2)
{

    auto pDrawLine1 = std::dynamic_pointer_cast<CDrawingLine>(CDrawingObject::CloneShared(pObj1));
    auto pDrawLine2 = std::dynamic_pointer_cast<CDrawingLine>(CDrawingObject::CloneShared(pObj2));


    if ( !pDrawLine1 || !pDrawLine2 )
    {
        return false;
    }

    m_psEditingObject[0] = pDrawLine1;
    m_psEditingObject[1] = pDrawLine2;
  

    LINE2D* pLine1 = pDrawLine1->GetLineInstance();
    LINE2D* pLine2 = pDrawLine2->GetLineInstance();

    CPartsDef*    pDef   = m_pView->GetPartsDef();
    CUndoAction* pUndo  = pDef->GetUndoAction();

    POINT2D ptCross;

    if (!pLine1)
    {
        return false;
    }

    if (!pLine2)
    {
        return false;
    }

    try
    {
        if(!pLine1->IntersectInfnityLine(&ptCross, *pLine2))
        {
            return false;
        }
    }
    catch(...)
    {
        return false;
    }

    double dA1, dA2;
    double dB1, dB2;
    double dC1, dC2;

    double dE1, dE2;
    double dF1, dF2;
    double dG1, dG2;

    if(!pLine1->GetParam( &dA1, &dB1, &dC1))
    {
        return false;
    }
    
    if(!pLine2->GetParam( &dA2, &dB2, &dC2))
    {
        return false;
    }

    double dH = sqrt(dA2 * dA2 + dB2 * dB2) - sqrt(dA1 * dA1 + dB1 * dB1);
    dE1 = dA1 - dA2;   
    dF1 = dB1 - dB2;   
    dG1 = dC1 - dC2 + dH;   

    dE2 = dA1 + dA2;   
    dF2 = dB1 + dB2;   
    dG2 = dC1 + dC2 + dH;   
   

    //角を２等分する直線を求める
    double dDirClick1 = dE1 * m_Select[0].ptClick.dX + dF1 * m_Select[0].ptClick.dY + dG1;
    double dDirClick2 = dE1 * m_Select[1].ptClick.dX + dF1 * m_Select[1].ptClick.dY + dG1;

    if ((dDirClick1 * dDirClick2) > 0.0)
    {
        dE1 = dE2;
        dF1 = dF2;
        dG1 = dG2;
    }


    int  iId1  = m_psEditingObject[0]->GetId();
    int  iId2  = m_psEditingObject[1]->GetId();


    if (m_dLen < NEAR_ZERO)
    {
        //長さ０のとき

        bool bRet1 = m_psEditingObject[0]->TrimCorner(ptCross, m_Select[0].ptClick);
        bool bRet2 = m_psEditingObject[1]->TrimCorner(ptCross, m_Select[1].ptClick);

        return true;
    }

    double dDiv = dB1 * dE1 - dA1 * dF1;
    if (fabs (dDiv)  < NEAR_ZERO)
    {
        return false;
    }

    CIRCLE2D cr(ptCross, m_dLen);

    std::vector<POINT2D> listL[2];
    POINT2D ptRet[2];
    POINT2D ptClick[2];
    cr.Intersect(*pLine1, &listL[0], false);

    cr.Intersect(*pLine2, &listL[1], false);

    int iCnt = 0;
    for ( std::vector<POINT2D>& lst:listL)
    {
        if (lst.size() != 2)
        {
            return false;
        }


        if (lst[0].Distance( m_Select[iCnt].ptClick) < 
            lst[1].Distance( m_Select[iCnt].ptClick))
        {
            ptRet[iCnt] = lst[0];
        }
        else
        {
            ptRet[iCnt] = lst[1];
        }

        ptClick[iCnt] = ptRet[iCnt] + (ptRet[iCnt] - ptCross);
        iCnt++;
    }

       
    bool bRet1 = m_psEditingObject[0]->TrimCorner( ptRet[0], ptClick[0]);
    bool bRet2 = m_psEditingObject[1]->TrimCorner( ptRet[1], ptClick[1]);

    LINE2D lineChamfer(ptRet[0], ptRet[1]);


    std::shared_ptr<CDrawingLine> pLine;
    if (m_psAddObj)
    {
        pLine = std::dynamic_pointer_cast<CDrawingLine>(m_psAddObj);
        if (!pLine)
        {
            m_psAddObj.reset();
        }
    }

    if (!pLine)
    {
        pLine = std::make_shared<CDrawingLine>();
        m_psAddObj = pLine;
    }

    *pLine->GetLineInstance() = lineChamfer;

    return true;
}


/**
 *  @brief   アクション解除
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CActionCorner::ClearAction()
{
    CViewAction::ClearAction();
    m_Select[0].pObject = NULL;
    m_Select[1].pObject = NULL;

    m_iSelCnt = 0;

    CPartsDef*    pCtrl   = m_pView->GetPartsDef();

    pCtrl->RelSelect();
    pCtrl->Redraw();

    m_pView->SetComment(GET_SYS_STR(STR_SEL_OBJECT));
}


/**
 *  @brief   連続アクション
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CActionCorner::_ClearContinue(CLICK_POINT* pLastObject )
{
    CViewAction::ClearAction();
    m_Select[0].pObject = pLastObject->pObject;
    m_Select[0].ptClick = pLastObject->ptClick;


    CPartsDef*    pDef   = m_pView->GetPartsDef();
    m_iSelCnt = 0;

    pDef->RelSelect();
    pDef->Redraw();


    if (m_Select[0].pObject)
    {
        pDef->SelectDrawingObject(m_Select[0].pObject->GetId());
        m_iSelCnt = 1;
        StdString sVal;
        sVal = m_pView->GetInputValue();
        if (sVal ==_T(""))
        {
            m_pView->SetInputFocus();
        }
        pDef->DrawDragging(m_pView);
    }
}



//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void TEST_CActionOffset()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());
    
 
    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}
#endif //_DEBUG