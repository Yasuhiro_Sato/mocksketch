/**
 * @brief			CActionCornerヘッダーファイル
 * @file			CActionCorner.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef _ACTION_CORNER_H__
#define _ACTION_CORNER_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CViewAction.h"
#include "CViewAction.h"
#include "DrawingObject/Primitive/POINT2D.h"
class     CDrawingView;

/**
 * @class   CActionCorner
 * @brief                        
 */
class CActionCorner: public CViewAction
{
    struct CLICK_POINT
    {
        POINT2D         ptClick;    //クリック位置
        std::shared_ptr<CDrawingObject>   pObject;    //選択オブジェクト
    };

public:
	//!< コンストラクタ
    CActionCorner();

    //!< コンストラクタ
    CActionCorner(CDrawingView* pView);
    
	//!< デストラクタ
    virtual ~CActionCorner();

	//!< 選択解放時動作
    virtual void Cancel(VIEW_MODE eMode);

	//!< 選択解放時動作
	virtual bool SelAction(VIEW_MODE eMode, void* pParam = NULL);

	virtual bool CActionCorner::MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

    //!< マウスクリック動作
	virtual bool SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

    virtual bool InputValue(StdString strVal);

protected:

    //!< アクション解除
    void ClearAction();

    //!< 連続アクション
    void _ClearContinue(CLICK_POINT* pLastObject);

    //!< コーナー追加
    bool _CreateChamfer(std::shared_ptr<CDrawingObject>* pCorner,
        double dLen,
        CDrawingObject* pObj1,
        POINT2D&        ptDirObj1,
        CDrawingObject* pObj2,
        POINT2D&        ptDirObj2
    );


    bool _CreateCorner(std::shared_ptr<CDrawingObject>* pCorner,
        double dRad,
        CDrawingObject* pObj1,
        POINT2D&        ptDirObj1,
        CDrawingObject* pObj2,
        POINT2D&        ptDirObj2
        );


protected:
    CLICK_POINT   m_Select[2];

    // 現在作成中のオブジェクト
    std::shared_ptr<CDrawingObject>   m_psEditingObject[2];


    int           m_iSelCnt;
    double        m_dLen;
    bool          m_bChamfer = false;
    StdString     m_strOldVal;

};
#endif // _ACTION_CORNER_H__
