/**
 * @brief			CViewAction実装ファイル
 * @file			CViewAction.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "CProjectCtrl.h"

#include "./CActionCreate.h"

#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/View/CDrawingView.h"

#include "DrawingObject/CDrawingObject.h"
#include "DrawingObject/CDrawingPoint.h"
#include "DrawingObject/CDrawingLine.h"
#include "DrawingObject/CDrawingCircle.h"
#include "DrawingObject/CDrawingNode.h"
#include "DrawingObject/CDrawingGroup.h"

#include "View/CLayer.h"
#include "View/CUndoAction.h"
#include "View/CDrawMarker.h"
#include "System/CSystem.h"
#include "SubWindow/TextInputDlg.h"

#include <math.h>

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//!< コンストラクタ
CActionCreate::CActionCreate():
CViewAction()
{
    m_lMouseOver =   SEL_ALL;
}

//!< コンストラクタ
CActionCreate::CActionCreate(CDrawingView* pView):
CViewAction(pView)
{
    m_lMouseOver =   SEL_ALL;
}

//!< デストラクタ
CActionCreate::~CActionCreate()
{
    _DeleteObject();
}

/**
 *  解放時動作
 *  @param   [in]    eMode 解放時点でのモード
 *  @retval   なし
 *  @note
 */
void CActionCreate::Cancel(VIEW_MODE eMode)
{
    ClearAction();
    _DeleteObject();
}

/**
 *  選択時動作
 *  @param   [in]    eMode 選択時点でのモード
 *  @retval   なし
 *  @note
 */
bool CActionCreate::SelAction(VIEW_MODE eViewMode, void* pParam)
{

    STD_ASSERT(m_pView != NULL);

    //既に選択中の図形はあるか？
    long lSize = m_pView->GetPartsDef()->SelectNum();

    m_eViewMode = eViewMode;
    if (lSize == 0)
    {
        //図形を選択してください
        m_pView->SetComment(GET_SYS_STR(STR_SEL_OBJECT));
        if (m_eViewMode != VIEW_SELECT)
        {
            m_ePreViewMode = m_eViewMode;
            m_pView->SetViewMode(VIEW_SELECT);
        }
        return true;    
    }

    m_lMouseOver =   SEL_ALL;
    m_pView->SetComment(GET_SYS_STR(STR_SEL_GROUP_CENTER));
    return true;    
}

/**
 *  マウスクリック動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval   なし
 *  @note
 */
bool CActionCreate::SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    STD_ASSERT(m_pView != NULL);

    POINT ptSel = posMouse.ptSel;
    UINT nFlags = posMouse.nFlags;

    //既に選択中の図形はあるか？
    long lSize = m_pView->GetPartsDef()->SelectNum();


    if (eAction != ACT_LBUTTON_UP)
    {
        return false;
    }

    POINT2D ptBase;
    bool bRet = false;
    try
    {
        switch (m_eViewMode)
        {
            case VIEW_GROUP:
                //基準点を選択
                if ( SelObjectPoint(&ptBase, posMouse, eAction ))
                {
                    SetGroup(&ptBase);
                }
                break;

            case VIEW_PARTS:
                //基準点を選択
                if ( SelObjectPoint(&ptBase, posMouse, eAction ))
                {
                    SetParts(&ptBase, false);
                }
                break;


            case VIEW_REFERENCE:
                //基準点を選択
                if ( SelObjectPoint(&ptBase, posMouse, eAction ))
                {
                    SetParts(&ptBase, true);
                }
                break;

            default:
                break;

        }
    }

    catch(MockException &e)
    {
        e.DispMessageBox();
    } 

    m_pView->SetViewMode(VIEW_SELECT);
    return false;
}


/**
 *  @brief   点の選択
 *  @param   [out]   pObj    選択位置
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionCreate::SelObjectPoint(POINT2D* p_pt2D, MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    CPartsDef*   pCtrl  = m_pView->GetPartsDef();
    int                  iId  = m_pView->SearchPos(posMouse.ptSel);
    auto     pObj  = pCtrl->GetObjectById(iId);


    VIEW_MODE      eViewMode  = m_pView->GetViewMode();
    POINT ptSel = posMouse.ptSel;
    UINT nFlags = posMouse.nFlags;

    STD_ASSERT(p_pt2D != NULL);

    bool bObj = false;
    if (pObj)
    {
        bObj = true;
    }

    if (eAction != CViewAction::ACT_LBUTTON_UP)
    {
        //左クリックのみ
        return false;
    }

    //!< マーカークリック確認
    if (CheckMarker( p_pt2D, ptSel, eAction))
    {

        //マーカをクリック
        return true;
    }


    if (!bObj)
    {
        //!< ビュー->データ変換
        int iLayerId = m_pView->GetCurrentLayerId();
        m_pView->ConvScr2World(p_pt2D, ptSel, iLayerId);
        return true;
    }

    if (pObj->GetType() == DT_POINT)
    {
        //点を選択
        auto pPoint = std::dynamic_pointer_cast<CDrawingPoint>(pObj);
        if(pPoint)
        {
            *p_pt2D = *pPoint->GetPointInstance();
        }
        return true;
    }
    else
    {
        m_pView->SetMarker(pObj.get());
    }
    return false;
}

/**
 *  @brief   部品作成
 *  @param   [in]   p_pt2D    選択位置
 *  @param   [in]   bRef  　　true　参照として作成
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionCreate::SetParts(POINT2D* p_pt2D, bool bRef)
{
    CPartsDef*   pDef  = m_pView->GetPartsDef();

    auto pList = pDef->GetSelectInstance();

    VIEW_COMMON::E_OBJ_DEF_TYPE eObjType;

    if (bRef)
    {
        eObjType = VIEW_COMMON::E_REFERENCE;
    }
    else
    {
        eObjType = VIEW_COMMON::E_PARTS;
    }


    //---------------------------------------------------
    // CObjectWnd::OnCreatePartsと同一
    CTextInputDlg dlgInput;

    dlgInput.SetInfo(GET_STR(STR_DLG_PARTS_TITLE), 
                     GET_STR(STR_DLG_PARTS_INFO));
    
    //グループ名入力
    CProjectCtrl* pProj = pDef->GetProject();

    StdString  strGroupName = pProj->QueryNewPartsName(eObjType);
    dlgInput.SetText(strGroupName.c_str());
    dlgInput.SelectAll();

    INT_PTR iRet = dlgInput.DoModal();

    if (iRet != IDOK)
    {
        return false;
    }
    
    strGroupName = dlgInput.GetText();

    if (!pProj->IsPossiblePartsName(strGroupName))
    {
        
        //使用不可能な文字が含まれています
        AfxMessageBox(GET_ERR_STR(e_no_use_char));
        return false;
    }

    if (pProj->IsDupPartsName(strGroupName))
    {
        //すでに同じ名称が使用されています
        AfxMessageBox(GET_ERR_STR(e_same_name));
        return false;
    }

    //!< 部品の作成
    std::shared_ptr<CObjectDef> pDefNew;

    pDefNew = pProj->CreateParts(strGroupName, eObjType, 1).lock();
    if(!pDefNew)
    {
        //バーツが生成できませんでした
        AfxMessageBox(GET_ERR_STR(e_parts_create));
        return false;
    }
    //---------------------------------------------------

    CPartsDef* pPartsNew;

    pPartsNew = dynamic_cast<CPartsDef*>(pDefNew.get());

    STD_ASSERT(pPartsNew);

    //---------------------
    //Undo Redp設定時に移動

    // 選択したオブジェクトの位置を移動して部品にコピーする
    // トップレベルの参照設定は行わない

    int iLayerNum = pPartsNew->GetLayerMax();

    int iLayerOld = -1;
    std::map<int,int> mapLayer;
    double dBaseScl = -1.0;
    
    for(auto wObj: *pList)
    {
        POINT2D ptDiff;
        ptDiff.Set(-(*p_pt2D));
        auto pObj = wObj.lock(); 
        auto pNewObj = CDrawingObject::CloneShared(pObj.get(), false);

        //-------------------
        //---------------------
        int iLayerOld = pNewObj->GetLayer();
        auto ite = mapLayer.find(iLayerOld);
        int iLayerNew = -1;
        if (ite != mapLayer.end())
        {
            iLayerNew = ite->second;
            pNewObj->SetLayer(iLayerNew);
        }
        else
        {
            CLayer* pOldLayer = pDef->GetLayer(iLayerOld);

            if (dBaseScl < 0)
            {
                dBaseScl = pOldLayer->dScl;
                iLayerNew = 1;
                mapLayer[iLayerOld] = iLayerNew;
            }
            else 
            {
                pPartsNew->AddLayer();
                iLayerNew = pPartsNew->GetLayerMax() - 1;
                CLayer* pNewLayer = pPartsNew->GetLayer(iLayerNew);
                *pNewLayer = *pOldLayer;

                double dNewScl = 1.0;
                //最初のオブジェクトとと異なる倍率のレイヤーが設定されている場合
                if ( fabs(dBaseScl - pOldLayer->dScl) > NEAR_ZERO)
                {
                    dNewScl = pOldLayer->dScl / dBaseScl;
                }
                pNewLayer->dScl = dNewScl;
                mapLayer[iLayerOld] = iLayerNew;
            }
        }

        pNewObj->SetSelect(false);
        pNewObj->Move( ptDiff );
        pPartsNew->RegisterObject(pNewObj, true, false);
    }

    try
    {
        //選択データ削除
        pDef->DeleteSelectingObject();

        //!< グループ設定
        
        auto pBase = pPartsNew->CreateInstance(pDef);
        auto pGroup = std::dynamic_pointer_cast<CDrawingParts>(pBase);

        if (!pGroup)
        {
            throw MockException(e_object_create); 
        }

        pGroup->SetPoint(*p_pt2D);
        pDef->RegisterObject(pGroup, true, false);
    }
    catch(MockException &e)
    {
        //新規作成のデータではエラーは発生しないはず
        STD_ASSERT(_T("SetGroup"));
        e.DispMessageBox();
    }


    //!< 変更を通知
    ::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_VIEW_ADD_OBJECT, (WPARAM)pPartsNew, 0);

    //!< 表示を更新
    
    return true;
}


/**
 *  @brief   グループ作成
 *  @param   [in]   pObj    選択位置
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionCreate::SetGroup(POINT2D* p_pt2D)
{
    CPartsDef*   pDef  = m_pView->GetPartsDef();

    auto pList = pDef->GetSelectInstance();

    auto pGroup = std::make_shared<CDrawingGroup>();

    pGroup->SetPartsDef(pDef);
    CUndoAction* pUndo  = pDef->GetUndoAction();

    //最初にグループを登録する


    pDef->SetLockUpdateNodeData(true);
    try
    {
        pGroup->SetPoint(*p_pt2D);
        pDef->RegisterObject(pGroup, true, false);
    }
    catch (MockException &e)
    {
        //新規作成のデータではエラーは発生しないはず
        STD_ASSERT(_T("SetGroup"));
        e.DispMessageBox();
    }

    for(auto wObj: *pList)
    {
        POINT2D ptDiff;
        ptDiff.Set(-(*p_pt2D));
        auto pObj = wObj.lock();
        if(pObj)
        {
            pUndo->Add(UD_DEL, pDef, pObj, NULL, false);
            pObj->Move( ptDiff );
            static const bool bCreateId = false;
            static const bool bKeepUnfindId = true;

            pGroup->AddData(pObj, bCreateId, bKeepUnfindId);
        }
    }

    pUndo->Add(UD_ADD, pDef, NULL, pGroup, true);


    //選択部をGroupに移譲する
    bool bHandOver = true;
    pDef->DeleteSelectingObject(bHandOver);


    pDef->SetLockUpdateNodeData(false);

    ClearAction(true);
    pDef->Redraw();

    //選択状態へ移行
    m_pView->SetViewMode(VIEW_SELECT);
    return true;
}


/**
 *  @brief   マウス移動動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionCreate::MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    CPartsDef* pDef = m_pView->GetPartsDef();
    if (!pDef)
    {
        return false;
    }

	SNAP_DATA snap;


    if (( m_eViewMode == VIEW_GROUP) ||
        ( m_eViewMode == VIEW_PARTS) ||
        ( m_eViewMode == VIEW_REFERENCE))
    {
        //基準点を選択
        return _MovePointCreateParts(posMouse, eAction);
    }

    return false;
}


bool CActionCreate::_MovePointCreateParts(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
	SNAP_DATA snap;

    CPartsDef* pDef = m_pView->GetPartsDef();

    bool bSnap = true;

    DWORD dwSnapType = DRAW_CONFIG->GetSnapType();
    POINT ptSel = posMouse.ptSel;
    UINT nFlags = posMouse.nFlags;

    //基準点を設定
    bool bIgnoreSnap = false;
    if (nFlags & MK_SHIFT)
    {
        //スナップ無視
        bIgnoreSnap = true;
        bSnap = false;
    }

    if (!bIgnoreSnap)
    {
        bSnap = m_pView->GetSnapPoint(&snap,
                            dwSnapType,
                            ptSel);
    }
    else
    {
        m_pView->ConvScr2World(&snap.pt, ptSel, m_pView->GetCurrentLayerId());
        snap.ptOrg = snap.pt;
    }

    auto pObj1 = pDef->GetObjectById(snap.iObject1);
    auto pObj2 = pDef->GetObjectById(snap.iObject2);


    CDrawMarker* pMarker= m_pView->GetMarker();
    m_pView->SetDrawingMode(DRAW_SEL);
    pMarker->Clear();
    pMarker->ClearObject();
    pDef->ClearMouseOver();
    pDef->ClearMouseEmphasis();
    pDef->ClearMouseConnectable();


    CDrawMarker::MARKER_DATA marker;
    if ((pObj1 != NULL) &&
        (pObj2 != NULL))
    {
        //交点
        pDef->SetMouseEmphasis(pObj1);
        pDef->SetMouseEmphasis(pObj2);
        marker.eType = CDrawMarker::MARKER_CROSS;
        marker.eSnap = VIEW_COMMON::SNP_CROSS_POINT;
        pMarker->AddMarker(marker, snap.pt);
    }


    if ((pObj1 != NULL ) &&
        (pObj2 == NULL ))
    {

        m_pView->SetMarker(pObj1.get(), false /*bIgnoreOffLineObject*/);
        pDef->SetMouseEmphasis(pObj1);
    }


    if (snap.eSnapType == VIEW_COMMON::SNP_NONE)
    {
        //任意点
        CDrawMarker*pMarker = m_pView->GetMarker();
        marker.eType = CDrawMarker::MARKER_CROSS;
        marker.eSnap = VIEW_COMMON::SNP_CROSS_POINT;
        pMarker->AddMarker(marker, snap.ptOrg);
    }


    if (snap.strSnap.empty()) 
    {
        m_pView->ResetToolTipText();
    }
    else
    {
        m_pView->SetToolTipText(snap.strSnap.c_str());
    }

    pMarker->HideObject(false);
    pDef->DrawDragging(m_pView);

    return true;
}

/**
 *  @brief   再描画
 *  @param   なし
 *  @retval  なし
 *  @note    SELECTバッファーに描画
 *           pCtrl->DrawDraggingの最中に呼び出し
 */
void CActionCreate::Redraw() const
{

}

/**
 *  動作クリア
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CActionCreate::ClearAction(bool bReleaseSelect)
{
    CViewAction::ClearAction(bReleaseSelect);

    m_bMark = false;
    CPartsDef*    pCtrl   = m_pView->GetPartsDef();

    pCtrl->Redraw();
}


/**
 *  マーカーチェック
 *  @param   [out] p_pt2D   選択位置
 *  @param   [in ] ptSel    クリック位置
 *  @param   [in ] eAction  選択動作
 *  @retval  true 選択あり
 *  @note
 */
bool CActionCreate::CheckMarker( POINT2D* p_pt2D, POINT ptSel, SEL_ACTION eAction)
{
    //マーカをクリック
    CDrawMarker*pMarker = m_pView->GetMarker();

    int iId = pMarker->CheckCkick( ptSel );

    if (iId == -1 )
    {
        return false;
    }

    CDrawMarker::MARKER_DATA* pData;
    pData = pMarker->GetMarker(iId);

    STD_ASSERT(pData != NULL);

    *p_pt2D = pData->ptObject;

    if ( (eAction == ACT_RBUTTON_UP) ||
            (eAction == ACT_LBUTTON_UP))
    {
        pMarker->Clear();
    }
    else if (eAction == ACT_MOVE)
    {
        pMarker->MouseMove(ptSel);
    }
    return true;
}


/**
 *  @brief   入力値設定
 *  @param   [in]    strVal 入力値
 *  @retval  false エラー発生
 *  @note    必要であればエラー表示を行う
 */
bool CActionCreate::InputValue(StdString strVal)
{
    return false;
}
