/**
 * @brief			CActionCreateヘッダーファイル
 * @file			CActionCreate.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#if !defined(_ACTION_CREATE_H_)
#define _ACTION_CREATE_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "View/VIEW_MODE.h"
#include "CViewAction.h"
class     CDrawingView;
class     CDrawingObject;
class     CDrawingNode;
class     CDrawingCircle;
class     CDrawingLine;
class     CDrawingEllipse;
class     POINT2D;
/**
 * @class   CActionCreate
 * @brief                        
   
 */
class CActionCreate: public CViewAction
{

protected:


public:
	//!< コンストラクタ
    CActionCreate();

    //!< コンストラクタ
    CActionCreate(CDrawingView* pView);
    
	//!< デストラクタ
    virtual ~CActionCreate();


	//!< 選択解放時動作
	virtual void Cancel(VIEW_MODE eMode);

	//!< 選択解放時動作
	virtual bool SelAction(VIEW_MODE eMode, void* pParam = NULL);

	//!< マウスクリック動作
	virtual bool SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

    //!< マウス移動
	virtual bool MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

    //!< 数値入力
    virtual bool InputValue(StdString strVal);

    //! 再描画
    virtual void Redraw() const;

    //!< グループ化
    bool SetGroup(POINT2D* p_pt2D);

    //!< 部品化
    bool SetParts(POINT2D* p_pt2D, bool bRef);


private:
    //!< 点の選択
    bool SelObjectPoint(POINT2D* p_pt2D, MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

    //!< 動作クリア
    virtual void ClearAction(bool bReleaseSelect = true);

   //!< マーカチェック
    bool CheckMarker( POINT2D* p_pt2D, POINT ptSel, SEL_ACTION eAction);

    bool _MovePointCreateParts(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

private:
};
#endif // !defined(_ACTION_POINT_H_)
