/**
 * @brief			CActionDim実装ファイル
 * @file			CActionDim.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CActionDim.h"

#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/View/CDrawingView.h"

#include "DrawingObject/CDrawingObject.h"
#include "DrawingObject/CDrawingPoint.h"
#include "DrawingObject/CDrawingLine.h"
#include "DrawingObject/CDrawingCircle.h"
#include "DrawingObject/CDrawingDim.h"
#include "DrawingObject/CDrawingDimL.h"
#include "DrawingObject/CDrawingDimH.h"
#include "DrawingObject/CDrawingDimV.h"

#include "View/CLayer.h"
#include "View/CDrawMarker.h"
#include "View/CUndoAction.h"
#include "Utility/CUtility.h"
#include "System/CSystem.h"
#include "MockSketch.h"
#include <math.h>

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 * コンストラクタ
 */
CActionDim::CActionDim(CDrawingView* pView):
CViewAction(pView)

{
    m_lMouseOver =   SEL_PRI ;
    m_pFirst = NULL;
    //m_pDim->SetColor(DRAW_CONFIG->crImaginary);

    m_psTmpLine = std::make_shared<CDrawingLine>();
    m_psTmpLine->SetColor(DRAW_CONFIG->crAdditional);
    m_psTmpLine->SetLineType(PS_DOT);
}

/**
 * コンストラクタ
 */
CActionDim::CActionDim():
CActionDim(NULL)
{
}

    
/**
 * デストラクタ
 */
CActionDim::~CActionDim()
{
    if (m_pView)
    {
        CDrawMarker* pMarker= m_pView->GetMarker();
        if (pMarker)
        {
            pMarker->ClearObject();
        }
    }
}

/**
 *  @brief   解放時動作
 *  @param   [in]    eMode 解放時点でのモード
 *  @retval   なし
 *  @note
 */
void CActionDim::Cancel(VIEW_MODE eMode) 
{
    _ClearAction();
}

/**
 *  @brief   選択時動作
 *  @param   [in]    eMode 選択時点でのモード
 *  @retval   なし
 *  @note
 */
bool CActionDim::SelAction(VIEW_MODE eMode, void* pParam) 
{
    m_iClickCnt = 0;

    if (eMode == VIEW_L_DIM)  
    {
    }
     m_eViewMode = eMode;
     m_eSelMode = SM_FAST_POINT;
    //VIEW_DIM,        //!< 寸法
    //VIEW_H_DIM,      //!< 水平寸法
    //VIEW_V_DIM,      //!< 垂直寸法
    //VIEW_A_DIM,      //!< 角度
    //VIEW_D_DIM,      //!< 直径
    return true;    
  
}

//LHV用
std::shared_ptr<CDrawingDim> CActionDim::CreateNewDim()
{
    return NULL;
}

//LHV用
bool CActionDim::_SetHeight(const POINT2D& pt2D, CDrawingDim* pDim)
{
    if (m_eViewMode == VIEW_L_DIM)
    {
        auto pDimL = dynamic_cast<CDrawingDimL*>(pDim);
        STD_ASSERT(pDimL);
        if (pDimL)
        {
            pDimL->SetTextCenter(pt2D);
        }
    }
    else if (m_eViewMode == VIEW_H_DIM)
    {
        auto pDimH = dynamic_cast<CDrawingDimH*>(pDim);
        STD_ASSERT(pDimH);
        if (pDimH)
        {
            pDimH->SetTextCenter(pt2D);
        }
    }
    else if (m_eViewMode == VIEW_V_DIM)
    {
        auto pDimV = dynamic_cast<CDrawingDimV*>(pDim);
        STD_ASSERT(pDimV);
        if (pDimV)
        {
            pDimV->SetTextCenter(pt2D);
        }
    }

    DBG_ASSERT(m_eViewMode);
    return false;
}

//LHV用
bool CActionDim::_AddObject(const POINT2D& pt2D)
{
    CPartsDef*   pDef  = m_pView->GetPartsDef();
    auto pDim= CreateNewDim();

    _SetHeight(pt2D, pDim.get());

    pDim->SetLayer(m_pView->GetCurrentLayerId());

	if (!THIS_APP->GetDefaultObjectPriorty(pDim->GetType()))
	{
		pDim->SetColor(m_pView->GetCurrentLayer()->crDefault);
	}
	else
	{
		COLORREF cr = THIS_APP->GetDefaultObject(pDim->GetType())->GetColor();
		pDim->SetColor(cr);
	}

    pDim->SetColor(m_pView->GetCurrentLayer()->crDefault);
    pDef->RegisterObject(pDim, true, false);
    m_eSelMode = SM_FAST_POINT;
    _ClearAction();

    m_pView->SetDrawingMode(DRAW_FRONT);
    pDef->Draw(pDim->GetId());


    return true;
}

/**
 *  @brief   マウスクリック動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionDim::_SelPointDim(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction, DRAWING_TYPE eType,
	std::shared_ptr<CDrawingDim>* pDim)
{
    CPartsDef*   pCtrl  = m_pView->GetPartsDef();
    int                  iId  = m_pView->SearchPos(posMouse.ptSel);
    auto     pObj  = pCtrl->GetObjectById(iId);

    VIEW_MODE      eViewMode  = m_pView->GetViewMode();

   //!< ビュー->データ変換
    POINT2D pt2D;
    int iLayerId = m_pView->GetCurrentLayerId();
    m_pView->ConvScr2World(&pt2D, posMouse.ptSel, iLayerId);


    POINT2D ptMarker;
    if(_CheckMarker( &ptMarker, posMouse.ptSel, eAction))
    {
        pObj = NULL;
        pt2D = ptMarker;
    }
    else
    {
        if (!pObj && (m_eSelMode != SM_TEXT_POSITION))
        {
            return false;
        }
    }

    if (m_eSelMode == SM_FAST_POINT)
    {
		auto pCreateObj = THIS_APP->CreateDefaultObject(eType);
        pCreateObj->SetLayer(iLayerId);
		if (eType == DT_DIM_L)
		{
			auto defObj = std::dynamic_pointer_cast<CDrawingDimL>(pCreateObj);
			*pDim = std::make_shared<CDrawingDimL>(*(defObj.get()));
		}
		else if (eType == DT_DIM_H)
		{
			auto defObj = std::dynamic_pointer_cast<CDrawingDimH>(pCreateObj);
			*pDim = std::make_shared<CDrawingDimH>(*(defObj.get()));
		}
		else if (eType == DT_DIM_V)
		{
			auto defObj = std::dynamic_pointer_cast<CDrawingDimV>(pCreateObj);
			*pDim = std::make_shared<CDrawingDimV>(*(defObj.get()));
		}
		(*pDim)->SetColor(DRAW_CONFIG->crImaginary);
        SetDim(*pDim);

        if (pObj)
        {
            DRAWING_TYPE eType = pObj->GetType();
            if (eType & DT_DIM)
            {
                eType = DT_DIM;
            }

            switch(eType)
            {
            case DT_LINE:
            case DT_CIRCLE:
                CreateObject(pObj.get(), NULL);

                m_eSelMode = SM_TEXT_POSITION;
                //pObj->SetSelect(true);
                m_pFirst = NULL;
                pCtrl->SelectDrawingObject(pObj->GetId());
                break;

            case DT_POINT:
            case DT_TEXT:
            case DT_NODE:
                m_pFirst = pObj;
                m_eSelMode = SM_SECOND_POINT;
                // pObj->SetSelect(true);
                pCtrl->SelectDrawingObject(pObj->GetId());
                break;

            case DT_DIM:
                //選択を割り込み
                m_pView->InterruptAction(VIEW_SELECT, posMouse);
                break;

            }
        }
        else
        {
            m_pTmpPoint1 = std::make_unique<CDrawingPoint>(-1);
            m_pTmpPoint1->SetPoint(pt2D);
            m_pFirst = m_pTmpPoint1;
            m_eSelMode = SM_SECOND_POINT;
        }

    }
    else if (m_eSelMode == SM_SECOND_POINT)
    {
        bool bSet = false;
        if (pObj)
        {
            DRAWING_TYPE eType = pObj->GetType();
            switch(eType)
            {
            case DT_LINE:
            case DT_CIRCLE:
            case DT_POINT:
            case DT_TEXT:
            case DT_NODE:
                if (CreateObject(m_pFirst.get(), pObj.get()))
                {
                    m_eSelMode = SM_TEXT_POSITION;
                    bSet = true;
                }
                break;
            }
        }

        if (!bSet)
        {
            m_pTmpPoint2 = std::make_unique<CDrawingPoint>(-1);
            m_pTmpPoint2->SetPoint(pt2D);
            if (CreateObject(m_pFirst.get(), m_pTmpPoint2.get()))
            {
#if 0
DB_PRINT( _T("First:(%f,%f), Second(%f,%f)\n"),
             m_pTmpPoint1->GetPoint().dX,
             m_pTmpPoint1->GetPoint().dY,
             pt2D.dX, pt2D.dY);
#endif

                m_eSelMode = SM_TEXT_POSITION;
            }
        }
    }
    else if (m_eSelMode == SM_TEXT_POSITION)
    {
        _AddObject(pt2D);
        /*
        CDrawingDim* pDim = new CDrawingDim(*(m_pDim.get()));
        m_pDim->SetHeight(pt2D);
        pDim->SetLayer(m_pView->GetCurrentLayerId());
        pDim->SetColor(DRAW_CONFIG->crDefault);
        pCtrl->AddObject(pDim, true, false);
        m_eSelMode = SM_FAST_POINT;
        _ClearAction();

        m_pView->SetDrawingMode(DRAW_FRONT);
        pCtrl->Draw(pDim->GetId());
        */

    }
    return true;
}

/**
 *  動作クリア
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CActionDim::_ClearAction()
{
    m_bMark = false;
    m_pFirst = NULL;


    CDrawMarker*    pMarker = m_pView->GetMarker();
    CPartsDef*      pCtrl  = m_pView->GetPartsDef();

    pMarker->Clear();
    pCtrl->RelSelect();

    pCtrl->Redraw();


    pCtrl->ClearMouseOver();
    pCtrl->ClearMouseEmphasis();
    VIEW_MODE      eViewMode  = m_pView->GetViewMode();

    m_pView->ResetToolTipText();
    SelAction(eViewMode);
}

bool CActionDim::_MovePointDim(MOUSE_MOVE_POS posMouse,
                                SEL_ACTION eAction,
                                std::weak_ptr<CDrawingDim> wDim)
{
    POINT ptSel = posMouse.ptSel;
    UINT nFlags = posMouse.nFlags;

    //!< スナップ点取得
    bool bSnap = true;

    SNAP_DATA snap;
    
    CPartsDef*   pDef  = m_pView->GetPartsDef();
    if (!pDef)
    {
        return false;
    }
    pDef->RedrawWait();   //TODO:どうする？

    DWORD dwSnapType = DRAW_CONFIG->GetSnapType();

    CDrawMarker* pMarker= m_pView->GetMarker();

    bSnap = m_pView->GetSnapPoint(&snap, 
                      dwSnapType,
                      ptSel);

    auto pObj1 = pDef->GetObjectById(snap.iObject1);
    auto pObj2 = pDef->GetObjectById(snap.iObject2);

    POINT ptOffset;
    ptOffset.x = ptOffset.y = 0;
    CDrawMarker::MARKER_DATA marker;
    marker.ptOffset = ptOffset;


    bool bIgnoreSnap = false;
    if (nFlags & MK_SHIFT)
    {
        //スナップ無視
        bIgnoreSnap = true;
        bSnap = false;
    }

    VIEW_MODE      eViewMode  = m_pView->GetViewMode();
    m_pView->SetDrawingMode(DRAW_SEL);
    pMarker->Clear();
    pMarker->ClearObject();
    pDef->ClearMouseOver();
    pDef->ClearMouseEmphasis();
    pDef->ClearMouseConnectable();

    if (pObj1 &&
        pObj2)
    {
        //交点
        pDef->SetMouseEmphasis(pObj1);
        pDef->SetMouseEmphasis(pObj2);
        marker.eType = CDrawMarker::MARKER_CROSS;
        marker.eSnap = VIEW_COMMON::SNP_CROSS_POINT;
        pMarker->AddMarker(marker, snap.pt);
    }

    if (m_eSelMode == SM_FAST_POINT)
    {
        if ((pObj1 != NULL ) &&
            (pObj2 == NULL ))
        {
            DRAWING_TYPE eType = pObj1->GetType();
            //SetMarker(pObj1, false /*bIgnoreOffLineObject*/);
            if (eType == DT_LINE)
            {
                //直線は選択可能 
                m_pView->SetMarker(pObj1.get(), false /*bIgnoreOffLineObject*/);
                pDef->SetMouseOver(pObj1);
            }
            else if((eType == DT_POINT) ||
                    (eType == DT_TEXT)||
                    (eType == DT_NODE))
            {
                //オブジェクトを選択し登録するので
                //マーカの表示はなし
                pDef->SetMouseOver(pObj1);
            }
            else
            {
                //それ以外はマーカを表示
                m_pView->SetMarker(pObj1.get(), false /*bIgnoreOffLineObject*/);
                pDef->SetMouseEmphasis(pObj1);

                if (eType & DT_DIM)
                {
                    //割り込み選択が可能なのでマウスオーバー表示を行う
                    pDef->SetMouseOver(pObj1);
                }
            }
        }
        int iMarkerId = pMarker->MouseMove(posMouse.ptSel);
    }
    else if (m_eSelMode == SM_SECOND_POINT)
    {
        bool bSet = false;
        bool bPoint = false;

        if (m_pFirst)
        {
            if ((pObj1 != NULL ) &&
                (pObj2 == NULL ))
            {
                DRAWING_TYPE eType = pObj1->GetType();
                switch(eType)
                {
                //case DT_LINE:
                //case DT_CIRCLE:
                case DT_POINT:
                case DT_TEXT:
                case DT_NODE:
                    bPoint = true;
                    if (CreateObject(m_pFirst.get(), pObj1.get()))
                    {
                        bSet = true;
                    }
                    break;


                default:
                    m_pView->SetMarker(pObj1.get(), false /*bIgnoreOffLineObject*/);
                    pDef->SetMouseEmphasis(pObj1);
                    break;
                }
            }

            if (!bSet && !bPoint)
            {
                m_pTmpPoint2 = std::make_unique<CDrawingPoint>(-1);
                m_pTmpPoint2->SetPoint(snap.pt);
                CreateObject(m_pFirst.get(), m_pTmpPoint2.get());
            }
            pDef->SetMouseOver(wDim);
        }
        int iMarkerId = pMarker->MouseMove(posMouse.ptSel);

    }
    else if (m_eSelMode == SM_TEXT_POSITION)
    {
        if ((pObj1 != NULL ) &&
            (pObj2 == NULL ))
        {
            m_pView->SetMarker(pObj1.get(), false /*bIgnoreOffLineObject*/);
            int iMarkerId = pMarker->MouseMove(posMouse.ptSel);
        }

        if(auto pDim = wDim.lock())
        {
            _SetHeight(snap.pt, pDim.get());
        }
        pDef->SetMouseOver(wDim);
    }

    //SetSnapTooltip(eSnap, 0, 0);
    pMarker->HideObject(false);

    pDef->DrawDragging(m_pView);

    return true;
}



/**
 *  @brief   マウスオーバー解除
 *  @param   なし
 *  @retval  true 成功
 *  @note
 */
bool CActionDim::CancelMouseOver()
{
   return false;

}

/**
 *  @brief   入力値設定
 *  @param   [in]    strVal 入力値
 *  @retval  false エラー発生
 *  @note    必要であればエラー表示を行う
 *  @note    エラー発生で入力コンボボックスは選択状態になる      
 *  @note    エラーがない場合はコンボボックスに値が登録される
 */
bool CActionDim::InputValue(StdString strVal)
{
    return false;
}
