/**
 * @brief			CActionDimヘッダーファイル
 * @file			CActionDim.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef _ACTION_DIM_H__
#define _ACTION_DIM_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CViewAction.h"
#include "DrawingObject/Primitive/POINT2D.h"
#include "View/DRAWING_TYPE.h"

class     CDrawingView;
class     CDrawingDim;

class     CDrawingDimL;
class     CDrawingDimH;
class     CDrawingDimV;

/**
 * @class   CActionDim
 * @brief                        
 */
class CActionDim: public CViewAction
{
public:
    enum SEL_MODE
    {
        SM_FAST_POINT,
        SM_SECOND_POINT,
        SM_TEXT_ANGLE,
        SM_TEXT_POSITION,
        SM_TEXT_ANGLE_POSITION,
    };

public:
	//!< コンストラクタ
    CActionDim();

    //!< コンストラクタ
    CActionDim(CDrawingView* pView);
    
	//!< デストラクタ
    virtual ~CActionDim();

	//!< 選択解放時動作
    virtual void Cancel(VIEW_MODE eMode);

	//!< 選択解放時動作
	virtual bool SelAction(VIEW_MODE eMode, void* pParam = NULL);

	//!< マウスクリック動作
	virtual bool SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction) = 0;

	//!< マウス移動動作
	virtual bool MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction) = 0;

    //!<マウスオーバー解除
    virtual bool CancelMouseOver();

    //!<入力値設定
    virtual bool InputValue(StdString strVal);

    //LHV用
    virtual bool SetDim(std::shared_ptr<CDrawingDim> pDim){return false;}
    virtual bool CreateObject(CDrawingObject* pObj1, CDrawingObject* pObj2){return false;}

    virtual std::shared_ptr<CDrawingDim> CreateNewDim();

protected:
    VIEW_MODE  m_eViewMode;
    SEL_MODE   m_eSelMode;
    POINT2D    m_ptFirst;
    bool      m_bDrag; 

    std::shared_ptr<CDrawingObject>  m_pFirst;
    std::shared_ptr<CDrawingPoint> m_pTmpPoint1;
    std::unique_ptr<CDrawingPoint> m_pTmpPoint2;

    // 補助直線
    std::shared_ptr<CDrawingLine>   m_psTmpLine;
     
protected:

    bool _SelPointDim(MOUSE_MOVE_POS posMouse, 
									SEL_ACTION eAction, 
									DRAWING_TYPE eType,
		                            std::shared_ptr<CDrawingDim>* pDim);
    bool _MovePointDim(MOUSE_MOVE_POS posMouse, 
                       SEL_ACTION eAction,
                       std::weak_ptr<CDrawingDim> pDim);
    bool _AddObject(const POINT2D& pt2D );
    bool _SetHeight(const POINT2D& pt2D, CDrawingDim* pDim);

    void _ClearAction();


};
#endif // _ACTION_DIM_H__
