/**
 * @brief			CViewAction実装ファイル
 * @file			CViewAction.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CActionDimA.h"

#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/View/CDrawingView.h"

#include "DrawingObject/CDrawingObject.h"
#include "DrawingObject/CDrawingPoint.h"
#include "DrawingObject/CDrawingLine.h"
#include "DrawingObject/CDrawingCircle.h"
#include "DrawingObject/CDrawingDimA.h"

#include "View/CLayer.h"
#include "View/CDrawMarker.h"
#include "View/CUndoAction.h"
#include "Utility/CUtility.h"
#include "System/CSystem.h"
#include "MockSketch.h"
#include <math.h>

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CActionDimA::CActionDimA(CDrawingView* pView):
CActionDim(pView)
{
}

//!< コンストラクタ
CActionDimA::CActionDimA():
CActionDimA(NULL)
{
}

/**
 * デストラクタ
 */
CActionDimA::~CActionDimA()
{
    if (m_pView)
    {
        CDrawMarker* pMarker= m_pView->GetMarker();
        if (pMarker)
        {
            pMarker->ClearObject();
        }
    }
}

/**
 *  解放時動作
 *  @param   [in]    eMode 解放時点でのモード
 *  @retval   なし
 *  @note
 */
void CActionDimA::Cancel(VIEW_MODE eMode)
{
    CActionDim::Cancel(eMode); 
}

/**
 *  選択時動作
 *  @param   [in]    eMode 選択時点でのモード
 *  @retval   なし
 *  @note
 */
bool CActionDimA::SelAction(VIEW_MODE eMode, void* pParam)
{
    m_pView->SetComment(GET_SYS_STR(STR_SEL_CREATE_POINT));
    m_eViewMode = eMode;
    m_eSelMode = SM_FAST_POINT;

    return true;
}

/**
 *  マウスクリック動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval   なし
 *  @note
 */
bool CActionDimA::SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    if (eAction != CViewAction::ACT_LBUTTON_UP) 
    {
        //左クリックと移動
        return false;
    }

    //--------------------------

    CPartsDef*   pDef = m_pView->GetPartsDef();
    int                 iId  = m_pView->SearchPos(posMouse.ptSel);
    auto     pObj  = pDef->GetObjectById(iId);

    VIEW_MODE      eViewMode  = m_pView->GetViewMode();

   //!< ビュー->データ変換
    POINT2D pt2D;
    int iLayerId = m_pView->GetCurrentLayerId();
    m_pView->ConvScr2World(&pt2D, posMouse.ptSel, iLayerId);





    POINT2D ptMarker;
    if(_CheckMarker( &ptMarker, posMouse.ptSel, eAction))
    {
        pObj = NULL;
        pt2D = ptMarker;
    }
    else
    {
        if (!pObj && (m_eSelMode != SM_TEXT_POSITION))
        {
            return false;
        }
    }


    STD_ASSERT(eViewMode == VIEW_A_DIM);
    if (m_eSelMode == SM_FAST_POINT)
    {
        m_pDim = std::dynamic_pointer_cast<CDrawingDimA>(THIS_APP->CreateDefaultObject(DT_DIM_A));
		m_pDim->SetColor(DRAW_CONFIG->crImaginary);

        if (pObj)
        {
            DRAWING_TYPE eType = pObj->GetType();

            if (eType & DT_DIM)
            {
                eType = DT_DIM;
            }

            if(eType == DT_LINE)
            {
                m_eSelMode = SM_SECOND_POINT;
                m_ptFirst = pt2D;
                m_pFirst = pObj;
                pDef->SelectDrawingObject(pObj->GetId());
            }
            else if(eType & DT_DIM)
            {
                //選択を割り込み
                m_pView->InterruptAction(VIEW_SELECT, posMouse);
            }
            else
            {
                m_pFirst = NULL;
            }
        }
    }
    else if (m_eSelMode == SM_SECOND_POINT)
    {
        bool bSet = false;
        if (pObj)
        {
            DRAWING_TYPE eType = pObj->GetType();
            switch(eType)
            {
            case DT_LINE:
                m_pDim->Create(m_pFirst.get(),
                                           pObj.get(),
                                           m_ptFirst,
                                           pt2D,
                                           false);
                m_pDim->Show(CDrawingDim::SM_SHOW);
                m_eSelMode = SM_TEXT_POSITION;
                bSet = true;
                break;
            }
        }
        /*
        if (!bSet)
        {
            m_pTmpPoint2 = std::make_unique<CDrawingPoint>(-1);
            m_pTmpPoint2->SetPoint(pt2D);
            m_pDim->SetObject(m_pFirst, m_pTmpPoint2.get());
            m_eSelMode = SM_TEXT_POSITION;
        }
        */
    }
    else if (m_eSelMode == SM_TEXT_POSITION)
    {

        m_pDim->SetAngleHeight(pt2D, false);


        auto pDim = std::make_shared<CDrawingDimA>(*(m_pDim.get()));

        pDim->SetLayer(m_pView->GetCurrentLayerId());
		if (!THIS_APP->GetDefaultObjectPriorty(DT_DIM_A))
		{
			pDim->SetColor(m_pView->GetCurrentLayer()->crDefault);
		}
		else
		{
			COLORREF cr = THIS_APP->GetDefaultObject(DT_DIM_A)->GetColor();
			pDim->SetColor(cr);
		}

        pDef->RegisterObject(pDim, true, false);
        m_eSelMode = SM_FAST_POINT;
        _ClearAction();

        m_pView->SetDrawingMode(DRAW_FRONT);
        pDef->Draw(pDim->GetId());

    }
    return true;
}

/**
 *  @brief   マウス移動動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionDimA::MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    POINT ptSel = posMouse.ptSel;
    UINT nFlags = posMouse.nFlags;

    //!< スナップ点取得
    bool bSnap = true;

    
    CPartsDef*   pDef  = m_pView->GetPartsDef();
    if (!pDef)
    {
        return false;
    }

    pDef->RedrawWait();   //TODO:どうする？

    DWORD dwSnapType = DRAW_CONFIG->GetSnapType();

    CDrawMarker* pMarker= m_pView->GetMarker();

    SNAP_DATA snap;
    bSnap = m_pView->GetSnapPoint(&snap,
                      dwSnapType,
                      ptSel);

    auto pObj1 = pDef->GetObjectById(snap.iObject1);
    auto pObj2 = pDef->GetObjectById(snap.iObject2);

    POINT ptOffset;
    ptOffset.x = ptOffset.y = 0;
    CDrawMarker::MARKER_DATA marker;
    marker.ptOffset = ptOffset;


    bool bIgnoreSnap = false;
    if (nFlags & MK_SHIFT)
    {
        //スナップ無視
        bIgnoreSnap = true;
        bSnap = false;
    }

    m_pView->SetDrawingMode(DRAW_SEL);
    pMarker->Clear();
    pMarker->ClearObject();
    pDef->ClearMouseOver();
    pDef->ClearMouseEmphasis();
    pDef->ClearMouseConnectable();

    if (m_eSelMode == SM_FAST_POINT)
    {
        if ((pObj1 != NULL ) &&
            (pObj2 == NULL ))
        {
            DRAWING_TYPE eType = pObj1->GetType();
            if ((eType != DT_LINE) &&
                !(eType & DT_DIM))
            {
                return false;
            }
            pDef->SetMouseOver(pObj1);
        }
    }
    else if (m_eSelMode == SM_SECOND_POINT)
    {
        bool bSet = false;

        if (m_pFirst)
        {
            if ((pObj1 != NULL ) &&
                (pObj2 == NULL ))
            {
                DRAWING_TYPE eType = pObj1->GetType();
                if (eType == DT_LINE)
                {
                    if(m_pDim->Create(m_pFirst.get(), pObj1.get(),
                                          m_ptFirst, snap.pt, true))
                    {
                        pDef->SetMouseEmphasis(pObj1);
                        bSet = true;
                    }
                }
            }

            if (!bSet)
            {
                m_pDim->Show(CDrawingDim::SM_HIDE);
            }
            else
            {
                m_pDim->Show(CDrawingDim::SM_DOT);
                pDef->SetMouseOver(m_pDim);
            }

        }
        else
        {
            m_eSelMode = SM_FAST_POINT;
        }
    }
    else if (m_eSelMode == SM_TEXT_POSITION)
    {

        if ((pObj1 != NULL ) &&
            (pObj2 == NULL ))
        {
            m_pView->SetMarker(pObj1.get(), false /*bIgnoreOffLineObject*/);
        }
        m_pDim->SetAngleHeight(snap.pt, false);
        pDef->SetMouseOver(m_pDim);
    }

    pMarker->HideObject(false);
    pDef->DrawDragging(m_pView);

    return true;
}