/**
 * @brief			CActionDimC実装ファイル
 * @file			CActionDimC.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CActionDimC.h"

#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/View/CDrawingView.h"

#include "DrawingObject/CDrawingObject.h"
#include "DrawingObject/CDrawingPoint.h"
#include "DrawingObject/CDrawingLine.h"
#include "DrawingObject/CDrawingCircle.h"
#include "DrawingObject/CDrawingDimC.h"

#include "View/CLayer.h"
#include "View/CDrawMarker.h"
#include "View/CUndoAction.h"
#include "Utility/CUtility.h"
#include "System/CSystem.h"
#include "MockSketch.h"
#include <math.h>

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CActionDimC::CActionDimC(CDrawingView* pView):
CActionDim(pView)
{
}

//!< コンストラクタ
CActionDimC::CActionDimC():
CActionDimC(NULL)
{
}

/**
 * デストラクタ
 */
CActionDimC::~CActionDimC()
{
    if (m_pView)
    {
        CDrawMarker* pMarker= m_pView->GetMarker();
        if (pMarker)
        {
            pMarker->ClearObject();
        }
    }
}

/**
 *  解放時動作
 *  @param   [in]    eMode 解放時点でのモード
 *  @retval   なし
 *  @note
 */
void CActionDimC::Cancel(VIEW_MODE eMode)
{
    CActionDim::Cancel(eMode); 
}

/**
 *  選択時動作
 *  @param   [in]    eMode 選択時点でのモード
 *  @retval   なし
 *  @note
 */
bool CActionDimC::SelAction(VIEW_MODE eMode, void* pParam)
{
    m_pView->SetComment(GET_SYS_STR(STR_SEL_CREATE_POINT));
    m_eViewMode = eMode;
    m_eSelMode = SM_FAST_POINT;

    return true;
}

/**
 *  マウスクリック動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval   なし
 *  @note
 */
bool CActionDimC::SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    if (eAction != CViewAction::ACT_LBUTTON_UP) 
    {
        //左クリックと移動
        return false;
    }

    //--------------------------
    CPartsDef*   pDef  = m_pView->GetPartsDef();
    int          iId  = m_pView->SearchPos(posMouse.ptSel);
    auto         pObj  = pDef->GetObjectById(iId);

    VIEW_MODE      eViewMode  = m_pView->GetViewMode();

    //!< ビュー->データ変換
    POINT2D pt2D;
    int iLayerId = m_pView->GetCurrentLayerId();
    m_pView->ConvScr2World(&pt2D, posMouse.ptSel, iLayerId);


    POINT2D ptMarker;
    if(_CheckMarker( &ptMarker, posMouse.ptSel, eAction))
    {
        pObj = NULL;
        pt2D = ptMarker;
    }

    //マウス位置を設定
    m_pTmpPoint1 = std::make_unique<CDrawingPoint>(-1);
    m_pTmpPoint1->SetPoint(pt2D);

    if (m_eSelMode == SM_FAST_POINT)
    {
        bool bSet = false;       

        m_pDim = std::dynamic_pointer_cast<CDrawingDimC>(THIS_APP->CreateDefaultObject(DT_DIM_C));
		m_pDim->SetColor(DRAW_CONFIG->crImaginary);

        if (pObj)
        {
            DRAWING_TYPE eType = pObj->GetType();

            if (eType & DT_DIM)
            {
                eType = DT_DIM;
            }

            if(eType == DT_LINE)
            {
                    bSet = true;
            }
            else if(eType & DT_DIM)
            {
                //選択を割り込み
                m_pView->InterruptAction(VIEW_SELECT, posMouse);
            }

        }

        if (bSet)
        {
            m_pFirst = pObj;
            auto pL = std::dynamic_pointer_cast<CDrawingLine>(m_pFirst);

            POINT2D ptMidMousePos;
            ptMidMousePos =  _GetMidLineMousePoint(pL.get(),  pt2D);

            if (m_pDim->Create(pL.get(), ptMidMousePos))
            {
                m_eSelMode = SM_TEXT_POSITION;
                pDef->SelectDrawingObject(pObj->GetId());
                STD_ASSERT(m_pFirst->GetType() ==  DT_LINE);
            }
        }
        else
        {
            return false;
        }
    }
    else if (m_eSelMode == SM_TEXT_POSITION)
    {
        auto pDim = std::make_shared<CDrawingDimC>(*(m_pDim.get()));

        POINT2D ptMidMousePos;
        auto pL = std::dynamic_pointer_cast<CDrawingLine>(m_pFirst);
        ptMidMousePos =  _GetMidLineMousePoint(pL.get(),  pt2D);

        m_pDim->SetTextCenter(ptMidMousePos);
        pDim->SetLayer(m_pView->GetCurrentLayerId());

		if (!THIS_APP->GetDefaultObjectPriorty(DT_DIM_C))
		{
			pDim->SetColor(m_pView->GetCurrentLayer()->crDefault);
		}
		else
		{
			COLORREF cr = THIS_APP->GetDefaultObject(DT_DIM_C)->GetColor();
			pDim->SetColor(cr);
		}

		pDef->RegisterObject(pDim, true, false);

        m_eSelMode = SM_FAST_POINT;
        _ClearAction();

        m_pView->SetDrawingMode(DRAW_FRONT);
        pDef->Draw(pDim->GetId());

    }
    return true;
}


POINT2D CActionDimC::_GetMidLineMousePoint(const CDrawingLine* pL,  const POINT2D& ptP) const
{
    //ある直線Lと点Pがある時
    //直線Lの中点を通りLに垂直な線をNとする
    //PからNに対して垂線を引いた時の交点のQを求める
    //チャンファー用

    auto pLine = pL->GetLineInstance();

    POINT2D ptMid  = (pLine->Pt(0) + pLine->Pt(1)) / 2.0;
    POINT2D ptNorm = pLine->Normal() + ptMid;
    LINE2D lN(ptMid, ptNorm);
    POINT2D ptQ = lN.NearPoint(ptP);
    return ptQ;
}



/**
 *  @brief   マウス移動動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionDimC::MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
   POINT ptSel = posMouse.ptSel;
    UINT nFlags = posMouse.nFlags;

    //!< スナップ点取得
    bool bSnap = true;

   
    CPartsDef*   pDef  = m_pView->GetPartsDef();
    if (!pDef)
    {
        return false;
    }

    pDef->RedrawWait();   //TODO:どうする？

    DWORD dwSnapType = DRAW_CONFIG->GetSnapType();
    VIEW_MODE      eViewMode  = m_pView->GetViewMode();

    CDrawMarker* pMarker= m_pView->GetMarker();

    LINE2D* pL = NULL;

    bool bIgnoreSnap = false;
    if (nFlags & MK_SHIFT)
    {
        //スナップ無視
        bIgnoreSnap = true;
        bSnap = false;
    }

    if (m_pFirst)
    {
        auto pDraingLine = std::dynamic_pointer_cast<CDrawingLine>(m_pFirst);
        STD_ASSERT(pDraingLine);
        if (pDraingLine)
        {
            pL = pDraingLine->GetLineInstance();
        }
    }


    SNAP_DATA snap;
    if (!bIgnoreSnap)
    {
        bSnap = m_pView->GetSnapPoint(&snap,
                          dwSnapType,
                          ptSel);
        if (m_pFirst)
        {
            if(m_pFirst->GetId() == snap.iObject1)
            {
                bSnap = false;
                snap.iObject1 = -1;
                snap.pt = snap.ptOrg;
            }
        }
    }
    else
    {
        m_pView->ConvScr2World(&snap.pt, ptSel, m_pView->GetCurrentLayerId());
        snap.ptOrg = snap.pt;
    }

    auto pObj1 = pDef->GetObjectById(snap.iObject1);
    auto pObj2 = pDef->GetObjectById(snap.iObject2);

    POINT ptOffset;
    ptOffset.x = ptOffset.y = 0;
    CDrawMarker::MARKER_DATA marker;
    marker.ptOffset = ptOffset;


    m_pView->SetDrawingMode(DRAW_SEL);
    pMarker->Clear();
    pMarker->ClearObject();
    pDef->ClearMouseOver();
    pDef->ClearMouseEmphasis();
    pDef->ClearMouseConnectable();

    if (m_eSelMode == SM_FAST_POINT)
    {
        if ((pObj1 != NULL ) &&
            (pObj2 == NULL ))
        {
            DRAWING_TYPE eType = pObj1->GetType();
            //SetMarker(pObj1, false /*bIgnoreOffLineObject*/);
            if( (eType == DT_LINE) ||
                (eType & DT_DIM))
            {   
                //SetMarker(pObj1, false /*bIgnoreOffLineObject*/);
                pDef->SetMouseOver(pObj1);
            }
        }
        int iMarkerId = pMarker->MouseMove(posMouse.ptSel);
    }
    else if (m_eSelMode == SM_TEXT_POSITION)
    {
        POINT2D ptPos;


        POINT2D ptMidMousePos;
        auto pL = std::dynamic_pointer_cast<CDrawingLine>(m_pFirst);
        ptMidMousePos =  _GetMidLineMousePoint(pL.get(),  snap.pt);

        m_psTmpLine->SetPoint1(snap.pt);
        m_psTmpLine->SetPoint2(ptMidMousePos);

        m_pView->GetMarker()->AddObject(m_psTmpLine);

        m_pDim->SetTextCenter(snap.pt);

        pDef->SetMouseOver(m_pDim);

    }

    //SetSnapTooltip(eSnap, 0, 0);
    pMarker->HideObject(false);
    pDef->DrawDragging(m_pView);
    return true;
}