/**
 * @brief			CActionDimD実装ファイル
 * @file			CActionDimD.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CActionDimD.h"

#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/View/CDrawingView.h"

#include "DrawingObject/CDrawingObject.h"
#include "DrawingObject/CDrawingPoint.h"
#include "DrawingObject/CDrawingLine.h"
#include "DrawingObject/CDrawingCircle.h"
#include "DrawingObject/CDrawingDimD.h"

#include "View/CLayer.h"
#include "View/CDrawMarker.h"
#include "View/CUndoAction.h"
#include "Utility/CUtility.h"
#include "System/CSystem.h"
#include "MockSketch.h"
#include <math.h>

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CActionDimD::CActionDimD(CDrawingView* pView):
CActionDim(pView)
{
}

//!< コンストラクタ
CActionDimD::CActionDimD():
CActionDimD(NULL)
{
}

/**
 * デストラクタ
 */
CActionDimD::~CActionDimD()
{
    if (m_pView)
    {
        CDrawMarker* pMarker= m_pView->GetMarker();
        if (pMarker)
        {
            pMarker->ClearObject();
        }
    }
}

/**
 *  解放時動作
 *  @param   [in]    eMode 解放時点でのモード
 *  @retval   なし
 *  @note
 */
void CActionDimD::Cancel(VIEW_MODE eMode)
{
    CActionDim::Cancel(eMode); 
}

/**
 *  選択時動作
 *  @param   [in]    eMode 選択時点でのモード
 *  @retval   なし
 *  @note
 */
bool CActionDimD::SelAction(VIEW_MODE eMode, void* pParam)
{
    m_pView->SetComment(GET_SYS_STR(STR_SEL_CREATE_POINT));
    m_eViewMode = eMode;
    m_eSelMode = SM_FAST_POINT;

    return true;
}

/**
 *  マウスクリック動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval   なし
 *  @note
 */
bool CActionDimD::SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    if (eAction != CViewAction::ACT_LBUTTON_UP) 
    {
        //左クリックと移動
        return false;
    }

    //--------------------------
    CPartsDef*   pDef  = m_pView->GetPartsDef();
    int                  iId  = m_pView->SearchPos(posMouse.ptSel);
    auto     pObj  = pDef->GetObjectById(iId);

    VIEW_MODE      eViewMode  = m_pView->GetViewMode();

    //!< ビュー->データ変換
    POINT2D pt2D;
    int iLayerId = m_pView->GetCurrentLayerId();
    m_pView->ConvScr2World(&pt2D, posMouse.ptSel, iLayerId);


    POINT2D ptMarker;
    if(_CheckMarker( &ptMarker, posMouse.ptSel, eAction))
    {
        pObj = NULL;
        pt2D = ptMarker;
    }

    m_pTmpPoint1 = std::make_unique<CDrawingPoint>(-1);
    m_pTmpPoint1->SetPoint(pt2D);


    if (m_eSelMode == SM_FAST_POINT)
    {
        bool bSet = false;       

        m_pDim = std::dynamic_pointer_cast<CDrawingDimD>(THIS_APP->CreateDefaultObject(DT_DIM_D));
		m_pDim->SetColor(DRAW_CONFIG->crImaginary);


        if (pObj)
        {
            DRAWING_TYPE eType = pObj->GetType();
            if (eType & DT_DIM)
            {
                eType = DT_DIM;
            }

            switch(eType)
            {
            case DT_LINE:
                break;

            case DT_CIRCLE:
                bSet = true;
                break;

            case DT_DIM:
                //選択を割り込み
                m_pView->InterruptAction(VIEW_SELECT, posMouse);
                break;
            }

        }
        else
        {
            return false;
        }

        if (bSet)
        {
            m_pFirst = pObj;
            STD_ASSERT(m_pFirst->GetType() ==  DT_CIRCLE);
            auto pC = std::dynamic_pointer_cast<CDrawingCircle>(m_pFirst);
            if(m_pDim->Create(pC.get(), *m_pTmpPoint1->GetPointInstance()))
            {
                m_eSelMode = SM_TEXT_ANGLE;
                pDef->SelectDrawingObject(pObj->GetId());
            }
        }
    }
    else if (m_eSelMode == SM_TEXT_ANGLE)
    {
        m_eSelMode = SM_TEXT_POSITION;
    }
    else if (m_eSelMode == SM_TEXT_POSITION)
    {
        auto pDim = std::make_shared<CDrawingDimD>(*(m_pDim.get()));
        m_pDim->SetTextOffset(pt2D);

		pDim->SetLayer(m_pView->GetCurrentLayerId());
		if (!THIS_APP->GetDefaultObjectPriorty(DT_DIM_D))
		{
			pDim->SetColor(m_pView->GetCurrentLayer()->crDefault);
		}
		else
		{
			COLORREF cr = THIS_APP->GetDefaultObject(DT_DIM_D)->GetColor();
			pDim->SetColor(cr);
		}

        pDef->RegisterObject(pDim, true, false);

        m_eSelMode = SM_FAST_POINT;
        _ClearAction();

        m_pView->SetDrawingMode(DRAW_FRONT);
        pDef->Draw(pDim->GetId());

    }
    return true;
}

/**
 *  @brief   マウス移動動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionDimD::MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    POINT ptSel = posMouse.ptSel;
    UINT nFlags = posMouse.nFlags;

    //!< スナップ点取得
    bool bSnap = true;

    
    CPartsDef*   pDef  = m_pView->GetPartsDef();
    if (!pDef)
    {
        return false;
    }
    pDef->RedrawWait();   //TODO:どうする？
 
    DWORD dwSnapType = DRAW_CONFIG->GetSnapType();
    VIEW_MODE      eViewMode  = m_pView->GetViewMode();

    CDrawMarker* pMarker= m_pView->GetMarker();

    SNAP_DATA snap;
    bSnap = m_pView->GetSnapPoint(&snap,
                      dwSnapType,
                      ptSel);


    if ( (m_eSelMode == SM_TEXT_ANGLE) &&
         (eViewMode  == VIEW_D_DIM) &&
         !bSnap)
    {
        auto pC = std::dynamic_pointer_cast<CDrawingCircle>(m_pFirst);

        POINT2D   ptMouse = snap.pt;
        
        //!< 長さ固定スナップ点取得
        double dAngle;
        double    dLength;
        POINT2D   ptCenter;
        ptCenter = pC->GetCenter();
        bSnap = m_pView->GetLengthSnapPoint(&dAngle,
                                  &dLength,
                                  &snap.pt,  
                                  ptCenter,  
                                  ptMouse,
                                  true,         //bAngle
                                  false     );  //bDistance
    }


    auto pObj1 = pDef->GetObjectById(snap.iObject1);
    auto pObj2 = pDef->GetObjectById(snap.iObject2);

    POINT ptOffset;
    ptOffset.x = ptOffset.y = 0;
    CDrawMarker::MARKER_DATA marker;
    marker.ptOffset = ptOffset;


    bool bIgnoreSnap = false;
    if (nFlags & MK_SHIFT)
    {
        //スナップ無視
        bIgnoreSnap = true;
        bSnap = false;
    }

    m_pView->SetDrawingMode(DRAW_SEL);
    pMarker->Clear();
    pMarker->ClearObject();
    pDef->ClearMouseOver();
    pDef->ClearMouseEmphasis();
    pDef->ClearMouseConnectable();

    if ((pObj1 != NULL) &&
        (pObj2 != NULL))
    {
        //交点
        pDef->SetMouseEmphasis(pObj1);
        pDef->SetMouseEmphasis(pObj2);
        marker.eType = CDrawMarker::MARKER_CROSS;
        marker.eSnap = VIEW_COMMON::SNP_CROSS_POINT;
        pMarker->AddMarker(marker, snap.pt);
    }

    if (m_eSelMode == SM_FAST_POINT)
    {
        if ((pObj1 != NULL ) &&
            (pObj2 == NULL ))
        {
            DRAWING_TYPE eType = pObj1->GetType();
            //SetMarker(pObj1, false /*bIgnoreOffLineObject*/);
            if( (eType == DT_LINE) &&
                (eViewMode  == VIEW_C_DIM))
            {   
                //SetMarker(pObj1, false /*bIgnoreOffLineObject*/);
                pDef->SetMouseOver(pObj1);
            }
            else if( (eType == DT_CIRCLE) &&
                    ((eViewMode  == VIEW_D_DIM)||
                     (eViewMode  == VIEW_R_DIM)))
            {
                pDef->SetMouseOver(pObj1);
            }
            else if (eType & DT_DIM)
            {
                //割り込み選択が可能なのでマウスオーバー表示を行う
                pDef->SetMouseOver(pObj1);
            }
 
        }
        int iMarkerId = pMarker->MouseMove(posMouse.ptSel);
    }
    else if (m_eSelMode == SM_TEXT_ANGLE)
    {
        int iMarkerId = pMarker->MouseMove(posMouse.ptSel);

        m_pDim->SetAngleHeight(snap.pt, true);

        if (snap.pt.Distance(m_pDim->GetPoint1()) >
            snap.pt.Distance(m_pDim->GetPoint2()) )
        {
            m_psTmpLine->SetPoint2(m_pDim->GetPoint2());
        }
        else
        {
            m_psTmpLine->SetPoint2(m_pDim->GetPoint1());
        }

        m_psTmpLine->SetPoint1(snap.pt);
        m_pView->GetMarker()->AddObject(m_psTmpLine);
        pDef->SetMouseOver(m_pDim);
    }
    else if (m_eSelMode == SM_TEXT_POSITION)
    {
        if ((pObj1 != NULL ) &&
            (pObj2 == NULL ))
        {
            m_pView->SetMarker(pObj1.get(), false /*bIgnoreOffLineObject*/);
            int iMarkerId = pMarker->MouseMove(posMouse.ptSel);
        }
        m_pDim->SetAngleHeight(snap.pt, false);
        pDef->SetMouseOver(m_pDim);
    }

    //SetSnapTooltip(eSnap, 0, 0);
    pMarker->HideObject(false);
    pDef->DrawDragging(m_pView);

    return true;}