/**
 * @brief			CActionDimDヘッダーファイル
 * @file			CActionDimD.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#if !defined(_ACTION_DIM_D_H__)
#define _ACTION_DIM_D_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CViewAction.h"
#include "CActionDim.h"
class     CDrawingView;
class     CDrawingDimD;

/**
 * @class   CActionDimD
 * @brief                        
   
 */
class CActionDimD: public CActionDim
{


public:
	//!< コンストラクタ
    CActionDimD();

    //!< コンストラクタ
    CActionDimD(CDrawingView* pView);
    
	//!< デストラクタ
    virtual ~CActionDimD();

	//!< 選択解放時動作
    virtual void Cancel(VIEW_MODE eMode);

	//!< 選択解放時動作
	virtual bool SelAction(VIEW_MODE eMode, void* pParam = NULL);

	//!< マウスクリック動作
	virtual bool SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

	//!< マウス移動動作
	virtual bool MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

private:
    std::shared_ptr<CDrawingDimD> m_pDim;

private:

};
#endif // !defined(_ACTION_DIM_D_H__)
