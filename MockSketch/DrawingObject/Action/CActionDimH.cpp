/**
 * @brief			CActionDimH実装ファイル
 * @file			CActionDimH.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CActionDimH.h"

#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/View/CDrawingView.h"

#include "DrawingObject/CDrawingObject.h"
#include "DrawingObject/CDrawingPoint.h"
#include "DrawingObject/CDrawingLine.h"
#include "DrawingObject/CDrawingCircle.h"
#include "DrawingObject/CDrawingDimH.h"

#include "View/CLayer.h"
#include "View/CDrawMarker.h"
#include "View/CUndoAction.h"
#include "Utility/CUtility.h"
#include "System/CSystem.h"
#include "MockSketch.h"

#include <math.h>

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CActionDimH::CActionDimH(CDrawingView* pView):
CActionDim(pView)
{
}

//!< コンストラクタ
CActionDimH::CActionDimH():
CActionDimH(NULL)
{
}

/**
 * デストラクタ
 */
CActionDimH::~CActionDimH()
{
    if (m_pView)
    {
        CDrawMarker* pMarker= m_pView->GetMarker();
        if (pMarker)
        {
            pMarker->ClearObject();
        }
    }
}

/**
 *  解放時動作
 *  @param   [in]    eMode 解放時点でのモード
 *  @retval   なし
 *  @note
 */
void CActionDimH::Cancel(VIEW_MODE eMode)
{
    CActionDim::Cancel(eMode); 
}

//LHV用
bool CActionDimH::CreateObject(CDrawingObject* pObj1, CDrawingObject* pObj2)
{
    bool bRet;
    bRet = m_pDim->Create(pObj1, pObj2);
    return bRet;
}



/**
 *  選択時動作
 *  @param   [in]    eMode 選択時点でのモード
 *  @retval   なし
 *  @note
 */
bool CActionDimH::SelAction(VIEW_MODE eMode, void* pParam)
{
    m_pView->SetComment(GET_SYS_STR(STR_SEL_CREATE_POINT));
    m_eViewMode = eMode;
    m_eSelMode = SM_FAST_POINT;

    return true;
}

//LHV用
std::shared_ptr<CDrawingDim> CActionDimH::CreateNewDim()
{
    auto pDim = std::make_shared<CDrawingDimH>(*(m_pDim.get()));
    return pDim;
}



bool CActionDimH::SetDim(std::shared_ptr<CDrawingDim> pDim)
{
    m_pDim = std::dynamic_pointer_cast<CDrawingDimH>(pDim);
    return true;
}


/**
 *  マウスクリック動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval   なし
 *  @note
 */
bool CActionDimH::SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    if (eAction != CViewAction::ACT_LBUTTON_UP) 
    {
        //左クリックと移動
        return false;
    }

	std::shared_ptr<CDrawingDim> pDim;
    pDim = std::dynamic_pointer_cast<CDrawingDim>(m_pDim);

    bool bRet; 
    bRet = CActionDim::_SelPointDim(posMouse, eAction, DT_DIM_H, &pDim);
    return bRet;
}

/**
 *  @brief   マウス移動動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionDimH::MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    return CActionDim::_MovePointDim( posMouse, eAction, m_pDim);
}