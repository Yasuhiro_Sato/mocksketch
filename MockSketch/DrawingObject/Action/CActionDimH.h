/**
 * @brief			CActionDimHヘッダーファイル
 * @file			CActionDimH.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#if !defined(_ACTION_DIM_H_H__)
#define _ACTION_DIM_H_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CViewAction.h"
#include "CActionDim.h"
class     CDrawingView;
class     CDrawingDimH;

/**
 * @class   CActionDimH
 * @brief                        
   
 */
class CActionDimH: public CActionDim
{


public:
	//!< コンストラクタ
    CActionDimH();

    //!< コンストラクタ
    CActionDimH(CDrawingView* pView);
    
	//!< デストラクタ
    virtual ~CActionDimH();

	//!< 選択解放時動作
    virtual void Cancel(VIEW_MODE eMode);

	//!< 選択解放時動作
	virtual bool SelAction(VIEW_MODE eMode, void* pParam = NULL);

	//!< マウスクリック動作
	virtual bool SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

	//!< マウス移動動作
	virtual bool MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

    //LHV用
    virtual bool CreateObject(CDrawingObject* pObj1, CDrawingObject* pObj2) override;

    virtual bool SetDim(std::shared_ptr<CDrawingDim> pDim) override;


    //LHV用
    virtual std::shared_ptr<CDrawingDim> CreateNewDim();

private:
    std::shared_ptr<CDrawingDimH> m_pDim;




private:

};
#endif // !defined(_ACTION_DIM_A_H__)
