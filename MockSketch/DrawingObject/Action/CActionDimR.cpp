/**
 * @brief			CActionDimR実装ファイル
 * @file			CActionDimR.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CActionDimR.h"

#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/View/CDrawingView.h"

#include "DrawingObject/CDrawingObject.h"
#include "DrawingObject/CDrawingPoint.h"
#include "DrawingObject/CDrawingLine.h"
#include "DrawingObject/CDrawingCircle.h"
#include "DrawingObject/CDrawingDimR.h"

#include "View/CLayer.h"
#include "View/CDrawMarker.h"
#include "View/CUndoAction.h"
#include "Utility/CUtility.h"
#include "System/CSystem.h"
#include <math.h>

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CActionDimR::CActionDimR(CDrawingView* pView):
CActionDim(pView)
{
    m_pDim = std::make_shared<CDrawingDimR>();
    m_pDim->SetColor(DRAW_CONFIG->crImaginary);

    m_psTmpLine2 = std::make_shared<CDrawingLine>();
    m_psTmpLine2->SetColor(DRAW_CONFIG->crAdditional);
    m_psTmpLine2->SetLineType(PS_DOT);
    
}

//!< コンストラクタ
CActionDimR::CActionDimR():
CActionDimR(NULL)
{
}

/**
 * デストラクタ
 */
CActionDimR::~CActionDimR()
{
    if (m_pView)
    {
        CDrawMarker* pMarker= m_pView->GetMarker();
        if (pMarker)
        {
            pMarker->ClearObject();
        }
    }
}

/**
 *  解放時動作
 *  @param   [in]    eMode 解放時点でのモード
 *  @retval   なし
 *  @note
 */
void CActionDimR::Cancel(VIEW_MODE eMode)
{
    CActionDim::Cancel(eMode); 
}

/**
 *  選択時動作
 *  @param   [in]    eMode 選択時点でのモード
 *  @retval   なし
 *  @note
 */
bool CActionDimR::SelAction(VIEW_MODE eMode, void* pParam)
{
    m_pView->SetComment(GET_SYS_STR(STR_SEL_CREATE_POINT));
    m_eViewMode = eMode;
    m_eSelMode = SM_FAST_POINT;

    return true;
}

/**
 *  マウスクリック動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval   なし
 *  @note
 */
bool CActionDimR::SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    if (eAction != CViewAction::ACT_LBUTTON_UP) 
    {
        //左クリックと移動
        return false;
    }
    CPartsDef*   pDef = m_pView->GetPartsDef();
    int                  iId  = m_pView->SearchPos(posMouse.ptSel);
    auto     pObj  = pDef->GetObjectById(iId);

    VIEW_MODE      eViewMode  = m_pView->GetViewMode();

    //!< ビュー->データ変換
    POINT2D pt2D;
    int iLayerId = m_pView->GetCurrentLayerId();
    m_pView->ConvScr2World(&pt2D, posMouse.ptSel, iLayerId);


    POINT2D ptMarker;
    if(_CheckMarker( &ptMarker, posMouse.ptSel, eAction))
    {
        pObj = NULL;
        pt2D = ptMarker;
    }

    m_pTmpPoint1 = std::make_unique<CDrawingPoint>(-1);
    m_pTmpPoint1->SetPoint(pt2D);

    if (m_eSelMode == SM_FAST_POINT)
    {
        bool bSet = false;       
        if (pObj)
        {
            DRAWING_TYPE eType = pObj->GetType();

            if (eType & DT_DIM)
            {
                eType = DT_DIM;
            }

            if(eType == DT_CIRCLE)
            {
                    bSet = true;
            }
            else if(eType == DT_DIM)
            {
                //選択を割り込み
                m_pView->InterruptAction(VIEW_SELECT, posMouse);
            }

        }

        if (bSet)
        {
            m_pFirst = pObj;
            STD_ASSERT(m_pFirst->GetType() ==  DT_CIRCLE);

            auto pC = std::dynamic_pointer_cast<CDrawingCircle>(m_pFirst);
            m_pDim->Create(pC.get(), *m_pTmpPoint1->GetPointInstance());


            double dS = pC->GetStart();
            double dE = pC->GetEnd();
            //補助線
            if (!pC->GetCircleInstance()->IsClose())
            {

                if (dS > dE)
                {
                    dS += 360.0;
                }
                double dM;
                dM = (dE - dS) / 2.0;
                dM += dS;

                POINT2D pt1 = pC->GetCenter();

                m_ptMidPoint.dX = 2.0 * pC->GetRadius() * cos(dM * DEG2RAD) + pt1.dX;
                m_ptMidPoint.dY = 2.0 * pC->GetRadius() * sin(dM * DEG2RAD) + pt1.dY;

                m_psTmpLine2->SetPoint1(pt1);
                m_psTmpLine2->SetPoint2(m_ptMidPoint);

                m_bArc = true;
            }
            else
            {
                m_bArc = false;
            }

            pDef->SelectDrawingObject(pObj->GetId());

            m_eSelMode = SM_TEXT_ANGLE_POSITION;
        }
        else
        {
            return false;
        }
    }
    else if (m_eSelMode == SM_TEXT_ANGLE_POSITION)
    {
        auto pDim = std::make_shared<CDrawingDimR>(*(m_pDim.get()));
        m_pDim->SetTextCenter(pt2D);
        pDim->SetLayer(m_pView->GetCurrentLayerId());
        pDim->SetColor(m_pView->GetCurrentLayer()->crDefault);
        pDef->RegisterObject(pDim, true, false);

        m_eSelMode = SM_FAST_POINT;
        _ClearAction();

        m_pView->SetDrawingMode(DRAW_FRONT);
        pDef->Draw(pDim->GetId());


    }

    return true;

}

/**
 *  @brief   マウス移動動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionDimR::MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    using namespace VIEW_COMMON;
    POINT ptSel = posMouse.ptSel;
    UINT nFlags = posMouse.nFlags;

    //!< スナップ点取得
    bool bSnap = true;

    
    CPartsDef*   pDef  = m_pView->GetPartsDef();
    pDef->RedrawWait();   //TODO:どうする？


    DWORD dwSnapType = DRAW_CONFIG->GetSnapType();



    VIEW_MODE      eViewMode  = m_pView->GetViewMode();
    CDrawMarker* pMarker= m_pView->GetMarker();

    CIRCLE2D* pC = NULL;

    bool bIgnoreSnap = false;
    if (nFlags & MK_SHIFT)
    {
        //スナップ無視
        bIgnoreSnap = true;
        bSnap = false;
    }

    if (m_pFirst)
    {
        auto pDraingCircle = std::dynamic_pointer_cast<CDrawingCircle>(m_pFirst);
        STD_ASSERT(pDraingCircle);
        if (pDraingCircle)
        {
            pC = pDraingCircle->GetCircleInstance();
        }
    }

	if (!pC)
	{
		return false;
	}

	SNAP_DATA snap;
    if (!bIgnoreSnap)
    {
        bSnap = m_pView->GetSnapPoint(&snap,
                          dwSnapType,
                          ptSel);

        if (m_pFirst)
        {
            if(m_pFirst->GetId() == snap.iObject1)
            {
                bSnap = false;
                snap.iObject1 = -1;
                snap.pt = snap.ptOrg;
            }
        }


        if (bSnap)
        {
            snap.strSnap += GetSnapName(snap.eSnapType);
        }


        if ( (m_eSelMode == SM_TEXT_ANGLE_POSITION) &&
             !bSnap)
        {

       
            //!< 長さ固定スナップ点取得
            double dAngle;
            double    dLength;
            POINT2D   ptCenter;
            ptCenter = pC->GetCenter();


            if (m_bArc)
            {

                bSnap =  m_pView->GetLineSnapPoint(    &snap.pt,  
                                          ptCenter,  
                                          m_ptMidPoint,  
                                          snap.ptOrg);

                if (bSnap)
                {
                    snap.strSnap += GetSnapName(SNP_MID_POINT);
                }
            }


            if (!bSnap)
            {
                bSnap = m_pView->GetLengthSnapPoint(&dAngle,
                                          &dLength,
                                          &snap.pt,  
                                          ptCenter,  
                                          snap.ptOrg,
                                          true,         //bAngle
                                          false     );  //bDistance
                if (bSnap)
                {
                    snap.strSnap += GetSnapName(SNP_ANGLE);
                    snap.strSnap += CUtil::StrFormat(_T(":%03f"), dAngle);
                }
            }
        }


    }
    else
    {
        m_pView->ConvScr2World(&snap.pt, ptSel, m_pView->GetCurrentLayerId());
    }

    auto pObj1 = pDef->GetObjectById(snap.iObject1);
    auto pObj2 = pDef->GetObjectById(snap.iObject2);

    POINT ptOffset;
    ptOffset.x = ptOffset.y = 0;
    CDrawMarker::MARKER_DATA marker;
    marker.ptOffset = ptOffset;


    m_pView->SetDrawingMode(DRAW_SEL);
    pMarker->Clear();
    pMarker->ClearObject();
    pDef->ClearMouseOver();
    pDef->ClearMouseEmphasis();
    pDef->ClearMouseConnectable();




    if (m_eSelMode == SM_FAST_POINT)
    {
        if (pObj1 &&
            !pObj2)
        {
            DRAWING_TYPE eType = pObj1->GetType();
            if( (eType == DT_CIRCLE) ||
                (eType & DT_DIM))
            {
                pDef->SetMouseOver(pObj1);
            }
        }
        int iMarkerId = pMarker->MouseMove(posMouse.ptSel);
    }
    else if (m_eSelMode == SM_TEXT_ANGLE_POSITION)
    {
        if (!bIgnoreSnap && m_bArc)
        {
            m_pView->GetMarker()->AddObject(m_psTmpLine2);
        }

        POINT2D ptPos;

        ptPos = pC->NearPoint(snap.pt);

        m_psTmpLine->SetPoint1(snap.pt);
        m_psTmpLine->SetPoint2(ptPos);
        m_pView->GetMarker()->AddObject(m_psTmpLine);

        m_pDim->SetAngleHeight(snap.pt);

        pDef->SetMouseOver(m_pDim);

    }
    pMarker->HideObject(false);
    pDef->DrawDragging(m_pView);

    if (bSnap && 
        m_eSelMode == SM_TEXT_ANGLE_POSITION)
    {
        m_pView->SetToolTipText(snap.strSnap.c_str());
    }


    return true;
}