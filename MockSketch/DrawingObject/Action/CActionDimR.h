/**
 * @brief			CActionDimRヘッダーファイル
 * @file			CActionDimR.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#if !defined(_ACTION_DIM_R_H__)
#define _ACTION_DIM_R_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CViewAction.h"
#include "CActionDim.h"
class     CDrawingView;
class     CDrawingDimR;

/**
 * @class   CActionDimR
 * @brief                        
   
 */
class CActionDimR: public CActionDim
{


public:
	//!< コンストラクタ
    CActionDimR();

    //!< コンストラクタ
    CActionDimR(CDrawingView* pView);
    
	//!< デストラクタ
    virtual ~CActionDimR();

	//!< 選択解放時動作
    virtual void Cancel(VIEW_MODE eMode);

	//!< 選択解放時動作
	virtual bool SelAction(VIEW_MODE eMode, void* pParam = NULL);

	//!< マウスクリック動作
	virtual bool SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

	//!< マウス移動動作
	virtual bool MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

private:
    std::shared_ptr<CDrawingDimR> m_pDim;

    // 補助直線
    std::shared_ptr<CDrawingLine>   m_psTmpLine2;

    // 円弧上の中点
    POINT2D m_ptMidPoint;

    bool m_bArc = false;

private:

};
#endif // !defined(_ACTION_DIM_A_H__)
