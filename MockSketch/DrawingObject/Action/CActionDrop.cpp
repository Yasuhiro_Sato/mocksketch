/**
 * @brief			CActionDrop実装ファイル
 * @file			CActionDrop.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MockSketch.h"
#include "./CActionDrop.h"
#include "DrawingObject/CDrawingObject.h"

#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/CFieldDef.h"
#include "DefinitionObject/CElementDef.h"
#include "DefinitionObject/View/CDrawingView.h"

#include "DrawingObject/CDrawingPoint.h"
#include "DrawingObject/CDrawingLine.h"
#include "DrawingObject/CDrawingCircle.h"
#include "DrawingObject/CDrawingParts.h"
#include "DrawingObject/CDrawingElement.h"
#include "DrawingObject/CDrawingField.h"
#include "DrawingObject/CDrawingReference.h"
#include "DrawingObject/CDrawingGroup.h"

#include "View/CLayer.h"
#include "View/CDrawMarker.h"
#include "View/CUndoAction.h"
#include "Utility/CUtility.h"
#include "Utility/DropUtil.h"
#include "Utility/DropTarget.h"
#include "System/CSystem.h"
#include <math.h>

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 * コンストラクタ
 */
CActionDrop::CActionDrop():
CViewAction()
{
    m_psAddObj = NULL;
    m_lMouseOver =  SEL_POINT;
    m_pView = NULL;
    m_uuidDef = boost::uuids::nil_uuid();
}

/**
 * コンストラクタ
 */
CActionDrop::CActionDrop(CDrawingView* pView):
CViewAction(pView)
{
    m_psAddObj = NULL;
    m_lMouseOver =  SEL_POINT;
    m_pView = pView;
    m_uuidDef = boost::uuids::nil_uuid();
}
    
/**
 * デストラクタ
 */
CActionDrop::~CActionDrop()
{
}

/**
 *  @brief   解放時動作
 *  @param   [in]    eMode 解放時点でのモード
 *  @retval   なし
 *  @note
 */
void CActionDrop::Cancel(VIEW_MODE eMode) 
{
    //選択解除
    ClearAction();
}

/**
 *  @brief   選択時動作
 *  @param   [in]    eMode 選択時点でのモード
 *  @retval   なし
 *  @note
 */
bool CActionDrop::SelAction(VIEW_MODE eMode, void* pParam) 
{
    m_lMouseOver =   SEL_POINT;

    //既に選択中の図形はあるか？
    long lSize = m_pView->GetPartsDef()->SelectNum();

    if (lSize != 0)
    {
        //選択を解除
        m_pView->GetPartsDef()->RelSelect();
    }   

    DROP_TARGET_DATA*  pTargetData;
    pTargetData = reinterpret_cast<DROP_TARGET_DATA*>(pParam);
    DROP_PARTS_DATA* pPartsData;

    HGLOBAL hData = pTargetData->pDataObject->GetGlobalData(CF_MOCK_PARTS);
    bool bRet = false;
    pPartsData = reinterpret_cast<DROP_PARTS_DATA*>(::GlobalLock(hData));
    if (pPartsData)
    {
        m_uuidDef = pPartsData->uuidDef;
        bRet = _CreateDraggingObject( m_uuidDef);
        GlobalUnlock(hData);
    }
    m_eViewMode = eMode;
    return bRet;    
}


/**
 *  @brief   マウスクリック動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionDrop::SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    //DB_PRINT(_T("CActionDrop::SelPoint\n"));
    if (eAction == ACT_LBUTTON_UP)
    {
        using namespace VIEW_COMMON;

        SNAP_DATA snap;

        m_pView->GetMousePoint(&snap, posMouse);

        std::shared_ptr<CObjectDef> pDef;
        pDef = THIS_APP->GetObjectDefByUuid(m_uuidDef ).lock();

        STD_ASSERT(pDef);
        if (pDef)
        {
            AddGroup(snap.pt, pDef);
        }
    }
    return false;
}

/**
 *  @brief   マウス移動動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note    CDropTarget::OnDragOverから
 *
 *           CMockSketchView::OnDropEnd
 */
bool CActionDrop::MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    if (!m_psAddObj)
    {
        return false;
    }

    if (!(posMouse.nFlags & MK_LBUTTON))
    {
        //ドラッグ中以外は不可
        return false;
    }

    using namespace VIEW_COMMON;

    bool bIgnoreSnap = false;
    if (posMouse.nFlags & MK_SHIFT)
    {
        bIgnoreSnap = true;
    }

    CPartsDef*   pDef  = m_pView->GetPartsDef();
    if (!pDef)
    {
        return false;
    }

    CDrawMarker* pMarker= m_pView->GetMarker();
    pMarker->Clear();
    pMarker->ClearObject();

    pDef->ClearMouseOver();
    pDef->ClearMouseEmphasis();
    pDef->ClearMouseConnectable();

    SNAP_DATA snap;

    m_pView->GetMousePoint(&snap, posMouse);
    auto pObj1 = pDef->GetObjectById(snap.iObject1);
    auto pObj2 = pDef->GetObjectById(snap.iObject2);

    if (!bIgnoreSnap)
    {
        if ((pObj1 != NULL ) &&
            (pObj2 == NULL ))
        {
            m_pView->SetMarker(pObj1.get(), false /*bIgnoreOffLineObject*/);
            pDef->SetMouseEmphasis(pObj1);
        }

        if (pObj2)
        {
            pDef->SetMouseEmphasis(pObj2);
        }

        int iMarkerId = pMarker->MouseMove(posMouse.ptSel);
    }

    pMarker->HideObject(false);
    STD_ASSERT(SetPosition(m_psAddObj.get(), snap.pt));
    m_psAddObj->SetVisible(true);
    pMarker->AddObject(m_psAddObj);
  
    pDef->DrawDragging(m_pView);

    StdString strSnap;
    if (snap.eSnapType == SNP_NONE)
    {
        //strSnap = GET_STR(STR_SNAP_NONE);
    }
    else
    {
        strSnap = GetSnapName(snap.eSnapType);
    }

    if (!strSnap.empty())
    {
        if((m_ptTooltip.x != posMouse.ptSel.x) ||
            (m_ptTooltip.y != posMouse.ptSel.y))
        {
            m_pView->SetToolTipText(strSnap.c_str());
        }
    }
    else
    {
        m_pView->ResetToolTipText();
    }
    m_ptTooltip = posMouse.ptSel;

    return  false;
}

/**
 *  動作クリア
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CActionDrop::ClearAction()
{
    CViewAction::ClearAction();
    CPartsDef*    pCtrl   = m_pView->GetPartsDef();

    pCtrl->RelSelect();
    
    pCtrl->Redraw();
}

/**
 *  @brief   グループ追加
 *  @param   [in]    ptClick クリック位置 
 *  @param   [in]    pDef    定義オブジェクト 
 *  @retval  なし
 *  @note
 */
void CActionDrop::AddGroup(POINT2D pt2D, std::shared_ptr<CObjectDef> pDef)
{
    //!< ビュー->データ変換
    int iLayerId = m_pView->GetCurrentLayerId();

    CUndoAction* pUndo  = m_pView->GetPartsDef()->GetUndoAction();

    CPartsDef*    pViewDef   = m_pView->GetPartsDef();

    std::shared_ptr<CDrawingScriptBase> pBase; 
    try
    {
        pBase = pDef->CreateInstance(pViewDef);
        pBase->SetLayer(iLayerId);

        if (!pBase)
        {
            return;
        }

        switch (pBase->GetType())
        {       
            case DT_PARTS:
            {
                auto pGroup = 
                    std::dynamic_pointer_cast<CDrawingParts>(pBase);
                pGroup->SetPoint( pt2D );
                break;
            }

            case DT_FIELD:
            {
                auto pField = 
                    std::dynamic_pointer_cast<CDrawingField>(pBase);
                pField->SetPoint( pt2D );
                break;
            }

            case DT_REFERENCE:
            {
                auto pRef = 
                    std::dynamic_pointer_cast<CDrawingReference>(pBase);
                pRef->SetPoint( pt2D );
                break;
            }

            case DT_ELEMENT:
            {
                auto pElement = 
                    std::dynamic_pointer_cast<CDrawingElement>(pBase);
                pElement->SetPoint( pt2D );
                break;
            }

            default:
            {
                break;
            }
        }
    }
    catch(MockException &e)
    {
        e.DispMessageBox();
       return; 
    }

    CDrawMarker* pMarker= m_pView->GetMarker();
    pMarker->Clear();
    pMarker->ClearObject();

    
    pViewDef->RegisterObject(pBase, true, false);

    m_pView->SetDrawingMode(DRAW_FRONT_VIEW);
    pBase->Draw(m_pView, NULL);

    pUndo->Add(UD_ADD, pViewDef, NULL, pBase);
}


bool CActionDrop::_CreateDraggingObject( boost::uuids::uuid uuidDef)
{
    std::shared_ptr<CObjectDef> pDef;
    pDef = THIS_APP->GetObjectDefByUuid(uuidDef ).lock();
    STD_ASSERT(pDef);
    if (pDef)
    {
        m_psAddObj.reset();

        CPartsDef*    pParts   = m_pView->GetPartsDef();

        //TODO:とりあえずはCreateInstance
        //      重いデータを扱うときは別途処理が必要

        m_psAddObj = pDef->CreateInstance(pParts);
        if (m_psAddObj)
        {
            m_psAddObj->SetLayer(m_pView->GetCurrentLayerId());
            CDrawMarker* pMarker= m_pView->GetMarker();
            return true;
        }
    }
    return false;
}





void CActionDrop::ClearDraggingObject( )
{



}
