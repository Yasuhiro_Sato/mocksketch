    /**
 * @brief			CActionDropヘッダーファイル
 * @file			CActionDrop.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef _ACTION_DROP_H__
#define _ACTION_DROP_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CViewAction.h"
class     CDrawingView;
class     CObjectDef;
class     POINT2D;

/**
 * @class   CActionDrop
 * @brief                        
 */
class CActionDrop: public CViewAction
{
public:
	//!< コンストラクタ
    CActionDrop();

    //!< コンストラクタ
    CActionDrop(CDrawingView* pView);
    
	//!< デストラクタ
    virtual ~CActionDrop();

	//!< 選択解放時動作
    virtual void Cancel(VIEW_MODE eMode);

	//!< 選択解放時動作
	virtual bool SelAction(VIEW_MODE eMode, void* pParam = NULL);

	//!< マウスクリック動作
	virtual bool SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

	//!< マウス移動動作
	virtual bool MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);
    
    void AddGroup(POINT2D pt2D, std::shared_ptr<CObjectDef> pDef);


    //ドラッグ中オブジェクトクリア
    void CActionDrop::ClearDraggingObject( );

protected:

    //!< 動作クリア
    void ClearAction();

    //ドラッグ中オブジェクト作成
    bool _CreateDraggingObject( boost::uuids::uuid uuidDef);

protected:

    //!< ドラッグ中オブジェクト
    CDrawingObject* m_psDraggingObject;

    //!< ドラッグ元オブジェクトUUID
    boost::uuids::uuid m_uuidDef;

};
#endif // _ACTION_DROP_H__
