/**
 * @brief			CActionEllipse実装ファイル
 * @file			CActionEllipse.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CActionEllipse.h"


#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/View/CDrawingView.h"

#include "DrawingObject/CDrawingObject.h"
#include "DrawingObject/CDrawingPoint.h"
#include "DrawingObject/CDrawingLine.h"
#include "DrawingObject/CDrawingCircle.h"
#include "DrawingObject/CDrawingEllipse.h"
#include "DrawingObject/Primitive/ELLIPSE2D.h"

#include "View/CLayer.h"
#include "View/CDrawMarker.h"
#include "View/CUndoAction.h"
#include "Utility/CUtility.h"
#include "System/CSystem.h"
#include "MockSketch.h"
#include <math.h>


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 * コンストラクタ
 */
CActionEllipse::CActionEllipse(CDrawingView* pView):
CViewAction(pView),
m_eSelMode(SM_CENTER_POINT)
{
    m_psTmpMouseLine = std::make_shared<CDrawingLine>();
    m_psTmpMouseLine->SetColor(DRAW_CONFIG->crAdditional);
    m_psTmpMouseLine->SetLineType(PS_DOT);

    m_lMouseOver =   SEL_FIG;
}

/**
 * コンストラクタ
 */
CActionEllipse::CActionEllipse():
CActionEllipse(NULL)
{
}

    
/**
 * デストラクタ
 */
CActionEllipse::~CActionEllipse()
{
    
}

/**
 *  @brief   解放時動作
 *  @param   [in]    eMode 解放時点でのモード
 *  @retval   なし
 *  @note
 */
void CActionEllipse::Cancel(VIEW_MODE eMode) 
{
    ClearAction();
}

/**
 *  @brief   選択時動作
 *  @param   [in]    eMode 選択時点でのモード
 *  @retval   なし
 *  @note
 */
bool CActionEllipse::SelAction(VIEW_MODE eMode, void* pParam) 
{
    //既に選択中の図形はあるか？
    long lSize = m_pView->GetPartsDef()->SelectNum();

    if (lSize != 0)
    {
        //選択を解除
        m_pView->GetPartsDef()->RelSelect();
    }   
    m_pView->SetComment(GET_SYS_STR(STR_SEL_ELLIPSE_CENTER));
    m_eViewMode = eMode;
    m_eSelMode = SM_CENTER_POINT;
    return true;    
   
}

/**
 *  @brief   マウスクリック動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionEllipse::SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    POINT ptSel = posMouse.ptSel;
    UINT nFlags = posMouse.nFlags;

    CPartsDef*   pDef  = m_pView->GetPartsDef();
    int                  iId  = m_pView->SearchPos(ptSel);
    auto        pObj  = pDef->GetObjectById(iId);
    VIEW_MODE      eViewMode  = m_pView->GetViewMode();

    if (eAction != CViewAction::ACT_LBUTTON_UP)
    {
        //左クリックのみ
        return false;
    }


    if (m_eSelMode == SM_CENTER_POINT)
    {

        POINT2D ptRet;
        if (! _CheckMarker( &ptRet, ptSel, eAction))
        {
            return false;
        }

        /*
        else
        {
            bRet = _SelCenter( &ptRet, ptSel, pObj);
        }
        */
        m_ptCenter = ptRet;
        m_eSelMode = SM_FIRST_AXIS;
        m_pView->SetComment(GET_SYS_STR(STR_SEL_ELLIPSE_VAL));

        //補助線生成
        CreateLine(&m_psAddtionalObj[0]);

		auto pCreateObj = THIS_APP->CreateDefaultObject(DT_ELLIPSE);
		auto defObj = std::dynamic_pointer_cast<CDrawingEllipse>(pCreateObj);
		auto pEllipse = std::make_shared<CDrawingEllipse>(*(defObj.get()));

		pEllipse->SetColor(DRAW_CONFIG->crImaginary);
		if (!THIS_APP->GetDefaultObjectPriorty(DT_ELLIPSE))
		{
			pEllipse->SetLineType(m_pView->GetCurrentLayer()->iLineType);
			pEllipse->SetLineWidth(m_pView->GetCurrentLayer()->iLineWidth);
		}
		pEllipse->SetCenter(m_ptCenter);
		pEllipse->SetLayer(m_pView->GetCurrentLayerId());

		m_psAddObj = pEllipse;
        m_psAddObj->SetSelect(false);
        m_psAddObj->SetVisible(true);

    }
    else if (m_eSelMode == SM_FIRST_AXIS)
    {
        auto pEllipse = std::dynamic_pointer_cast<CDrawingEllipse>(m_psAddObj);
        pEllipse->SetAngle(m_dTmpAngle);
        pEllipse->SetRadius1(m_dTmpRad1);

        CDrawMarker* pMarker= m_pView->GetMarker();
        pMarker->Clear();
        pMarker->ClearObject();
        pMarker->AddObject(m_psAddObj);

        auto pLine = std::dynamic_pointer_cast<CDrawingLine>(m_psAddtionalObj[0]);
        if (!pLine)
        {
            pLine = CreateLine(&m_psAddtionalObj[0]);
        }

        if (pLine)
        {
            double dAngle = m_dTmpAngle * DEG2RAD;
            POINT2D pt1;
            pt1.dX = m_ptCenter.dX + m_dTmpRad1 * 1.1 * cos( dAngle);
            pt1.dY = m_ptCenter.dY + m_dTmpRad1 * 1.1 * sin( dAngle);
            pLine->SetPoint1( pt1 );

            dAngle = (m_dTmpAngle + 180.0) * DEG2RAD;

            pt1.dX = m_ptCenter.dX + m_dTmpRad1 * 1.1 * cos( dAngle);
            pt1.dY = m_ptCenter.dY + m_dTmpRad1 * 1.1 * sin( dAngle);
            pLine->SetPoint2( pt1 );
        }

        CreateLine(&m_psAddtionalObj[1]);

        m_eSelMode = SM_SECOND_RADIUS;
        pDef->DrawDragging(m_pView);

    }
    else if (m_eSelMode == SM_SECOND_RADIUS)
    {
        auto pEllipse = std::dynamic_pointer_cast<CDrawingEllipse>(m_psAddObj);
        pEllipse->SetRadius2(m_dTmpRad2);

        pEllipse->SetLayer(m_pView->GetCurrentLayerId());

		if (!THIS_APP->GetDefaultObjectPriorty(DT_ELLIPSE))
		{
			pEllipse->SetColor(m_pView->GetCurrentLayer()->crDefault);
		}
		else
		{
			COLORREF cr = THIS_APP->GetDefaultObject(DT_ELLIPSE)->GetColor();
			pEllipse->SetColor(cr);
		}

        auto pEllipseAdd = std::make_shared<CDrawingEllipse>(*pEllipse.get());
        pDef->RegisterObject(pEllipseAdd, true, false);

        if (pEllipse)
        {
            CUndoAction* pUndo  = pDef->GetUndoAction();
            pUndo->Add(UD_ADD, pDef, NULL, pEllipse, true);
        }

        m_pView->SetDrawingMode(DRAW_FRONT);
        pDef->Draw(pEllipse->GetId());
        m_eSelMode = SM_CENTER_POINT;
        ClearAction();

    }

    return true;
}

/**
 *  @brief   マウス移動動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionEllipse::MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    using namespace VIEW_COMMON;

     POINT ptSel = posMouse.ptSel;
    UINT nFlags = posMouse.nFlags;

    //!< スナップ点取得
    bool bSnap = true;

    SNAP_DATA snap;
    
    CPartsDef*   pDef  = m_pView->GetPartsDef();
    if (!pDef)
    {
        return false;
    }
    pDef->RedrawWait();   //TODO:どうする？

    DWORD dwSnapType = DRAW_CONFIG->GetSnapType();

    CDrawMarker* pMarker= m_pView->GetMarker();

    bSnap = m_pView->GetSnapPoint(&snap, 
                      dwSnapType,
                      ptSel);

    auto pObj1 = pDef->GetObjectById(snap.iObject1);
    auto pObj2 = pDef->GetObjectById(snap.iObject2);

    POINT ptOffset;
    ptOffset.x = ptOffset.y = 0;
    CDrawMarker::MARKER_DATA marker;
    marker.ptOffset = ptOffset;


    bool bIgnoreSnap = false;
    if (nFlags & MK_SHIFT)
    {
        //スナップ無視
        bIgnoreSnap = true;
        bSnap = false;
        pObj1 = NULL;
        snap.eSnapType = SNP_NONE;
    }

    VIEW_MODE      eViewMode  = m_pView->GetViewMode();
    m_pView->SetDrawingMode(DRAW_SEL);
    pMarker->Clear();
    pMarker->ClearObject();
    pDef->ClearMouseOver();
    pDef->ClearMouseEmphasis();
    pDef->ClearMouseConnectable();

    if (pObj1 &&
        pObj2)
    {
        //交点
        pDef->SetMouseEmphasis(pObj1);
        pDef->SetMouseEmphasis(pObj2);
        marker.eType = CDrawMarker::MARKER_CROSS;
        marker.eSnap = VIEW_COMMON::SNP_CROSS_POINT;
        pMarker->AddMarker(marker, snap.pt);
    }


    if (m_eSelMode == SM_CENTER_POINT)
    {
        if (pObj1 &&
            !pObj2)
        {

            m_pView->SetMarker(pObj1.get(), false /*bIgnoreOffLineObject*/);
            pDef->SetMouseEmphasis(pObj1);
        }

        int iMarkerId = pMarker->MouseMove(posMouse.ptSel);

        snap.strSnap = GetSnapName(snap.eSnapType, true);

        if (snap.eSnapType == SNP_NONE)
        {
            //任意点
            CDrawMarker*pMarker = m_pView->GetMarker();
            marker.eType = CDrawMarker::MARKER_CROSS;
            marker.eSnap = VIEW_COMMON::SNP_CROSS_POINT;
            pMarker->AddMarker(marker, snap.ptOrg);
        }

    }
    else if (m_eSelMode == SM_FIRST_AXIS)
    {
        E_SNAP_TYPE eSnap = SNP_NONE;
        StdString strToolTip;

        POINT2D ptSelectingPos;
        bool bSnapLength = false;  
        double dAngle;
        double dLength;

        if (pObj1 &&
            !pObj2)
        {

            m_pView->SetMarker(pObj1.get(), false /*bIgnoreOffLineObject*/);
            pDef->SetMouseEmphasis(pObj1);
        }


        if (!bSnap)
        {
            bSnapLength = m_pView->GetLengthSnapPoint(&dAngle,
                                        &dLength,
                                        &ptSelectingPos,  
                                        m_ptCenter,  
                                        snap.pt,
                                        true,     //Angle
                                        true);   
        }

        if(!bSnapLength)
        {
            dLength  = m_ptCenter.Distance(snap.pt);
            dAngle   = atan2(snap.pt.dY-m_ptCenter.dY,
                             snap.pt.dX-m_ptCenter.dX) * RAD2DEG;
            ptSelectingPos = snap.pt;
        }

        {
            int iDecimalPlace = 3;

            StdStringStream   strmLen;
            if (snap.eSnapType != SNP_NONE)
            {
                snap.strSnap = GetSnapName(snap.eSnapType, false);
                snap.strSnap += _T("\n");
            }

            strmLen << GET_STR(STR_PRO_RAD1) << _T(":")
			    << CDrawingObject::GetValText(dLength, false, iDecimalPlace);

            strmLen << _T(" ") << GET_STR(STR_ANGLE) << _T(":")
			    << CDrawingObject::GetValText(dAngle, true, 1);
            snap.strSnap += strmLen.str();

        }


        m_dTmpAngle = dAngle;
        m_dTmpRad1 = dLength;

        int iLayerId =  m_pView->GetCurrentLayerId();
        auto pExtLine = std::dynamic_pointer_cast<CDrawingLine>(m_psAddtionalObj[0]);
        if (!pExtLine)
        {
            pExtLine = CreateLine(&m_psAddtionalObj[0]);
        }
        pExtLine->SetPoint1(m_ptCenter);
        pExtLine->SetPoint2(ptSelectingPos);
        pExtLine->SetLayer(iLayerId);
        pMarker->AddObject(pExtLine);
        

        auto pEllipse = std::dynamic_pointer_cast<CDrawingEllipse>(m_psAddObj);
        if(pEllipse)
        {
            pEllipse->SetAngle(m_dTmpAngle);
            pEllipse->SetRadius1(m_dTmpRad1);
            pEllipse->SetRadius2(m_dTmpRad1/2.0);
            pEllipse->SetLayer(iLayerId);
            pMarker->AddObject(pEllipse);
        }

    }
    else if (m_eSelMode == SM_SECOND_RADIUS)
    {
        double dLength;
        POINT2D ptRet;

        bool bSnapLength = false;  
        bSnapLength = m_pView->GetFixAngleSnapPoint(&dLength,
                                    &ptRet,  
                                    m_dTmpAngle + 90.0,
                                    m_ptCenter,  
                                    snap.pt,
                                    true); //bSnap

        int iLayerId =  m_pView->GetCurrentLayerId();
        if (bSnapLength)
        {
            auto pLine = std::dynamic_pointer_cast<CDrawingLine>(m_psAddtionalObj[1]);
            if (!pLine)
            {
                pLine = CreateLine(&m_psAddtionalObj[1]);
            }

            double dAngle = (m_dTmpAngle + 90.0) * DEG2RAD;
            POINT2D pt1;
            pt1.dX = m_ptCenter.dX + dLength * 1.1 * cos( dAngle);
            pt1.dY = m_ptCenter.dY + dLength * 1.1 * sin( dAngle);
            pLine->SetPoint1( pt1 );

            dAngle = (m_dTmpAngle + 270.0) * DEG2RAD;

            pt1.dX = m_ptCenter.dX + dLength * 1.1 * cos( dAngle);
            pt1.dY = m_ptCenter.dY + dLength * 1.1 * sin( dAngle);
            pLine->SetPoint2( pt1 );


            pLine->SetLayer(iLayerId);
            pMarker->AddObject(pLine);


            m_dTmpRad2 = fabs(dLength);

            {
            int iDecimalPlace = 3;

            StdStringStream   strmLen;
            if (snap.eSnapType != SNP_NONE)
            {
                snap.strSnap = GetSnapName(snap.eSnapType, false);
                snap.strSnap += _T("\n");
            }

            strmLen << GET_STR(STR_PRO_RAD2) << _T(":")
			    << CDrawingObject::GetValText(m_dTmpRad2, false, iDecimalPlace);

            snap.strSnap += strmLen.str();
            }

            auto pEllipse = std::dynamic_pointer_cast<CDrawingEllipse>(m_psAddObj);
            pEllipse->SetRadius2(m_dTmpRad2);
            pEllipse->SetLayer(iLayerId);
            pMarker->AddObject(pEllipse);
        }

        auto pExtLine = std::dynamic_pointer_cast<CDrawingLine>(m_psAddtionalObj[0]);
        if (!pExtLine)
        {
            pExtLine = CreateLine(&m_psAddtionalObj[0]);
        }
        pExtLine->SetLayer(iLayerId);
        pMarker->AddObject(pExtLine);

        m_psTmpMouseLine->SetLayer(iLayerId);
        m_psTmpMouseLine->SetPoint1(snap.pt);
        m_psTmpMouseLine->SetPoint2(ptRet);
        pMarker->AddObject(m_psTmpMouseLine);

        StdString strToolTip;
        if (bSnapLength)
        {
        }
    }

    if (snap.strSnap.empty()) 
    {
        m_pView->ResetToolTipText();
    }
    else
    {
        m_pView->SetToolTipText(snap.strSnap.c_str());
    }
    pMarker->HideObject(false);
    pDef->DrawDragging(m_pView);

    return true;
}

/**
 *  @brief   入力値設定
 *  @param   [in]    strVal 入力値
 *  @retval  false エラー発生
 *  @note    必要であればエラー表示を行う
 *  @note    エラー発生で入力コンボボックスは選択状態になる      
 *  @note    エラーがない場合はコンボボックスに値が登録される
 */
bool CActionEllipse::InputValue(StdString strVal)
{
    // TAG: [数値解析]
    if (m_eSelMode != SM_CENTER_POINT)
    {
        //!< 先に中心点を選択してください
        AfxMessageBox(GET_STR(STR_ERROR_SELECT_BEFORE_CENTER));
        return false;
    }


    //長径,短径,角度
    std::vector<StdString> lstInput;
    boost::algorithm::split( lstInput, strVal, boost::is_any_of(_T(",")));

    double dRad1 = 1.0;
    double dRad2 = 1.0;
    double dAngle = 0.0;

    size_t iSize = lstInput.size();
    if (iSize <= 0)
    {
        //!<入力値が誤っています
        return false;
    }

    if (iSize >= 1)
    {
        dRad1 = _tstof(lstInput[0].c_str());
        dRad2 = dRad1;
    }
    
    if (iSize >= 2)
    {
        dRad2 = _tstof(lstInput[1].c_str());
    }

    if (iSize >= 3)
    {
        dAngle = _tstof(lstInput[2].c_str());
    }

    CPartsDef*    pCtrl   = m_pView->GetPartsDef();
    CUndoAction* pUndo  = pCtrl->GetUndoAction();

    ELLIPSE2D ellipse(m_ptCenter, dRad1, dRad2, dAngle);

    auto pEllipse = RegisterEllipse(&ellipse);
    if (pEllipse)
    {
        pUndo->Add(UD_ADD, pCtrl, NULL, pEllipse, true);
    }
    ClearAction();
    return true;
}



//!< マーカーチェック
bool CActionEllipse::_CheckMarker( POINT2D* ptOut, POINT ptSel, SEL_ACTION eAction )
{
    // マーカをクリック
    CPartsDef*   pCtrl  = m_pView->GetPartsDef();
    CDrawMarker*pMarker = m_pView->GetMarker();

    int iId = pMarker->CheckCkick( ptSel );

    if (iId == -1 )
    {
        //マーカ以外をクリックした場合はエラー
        AfxMessageBox(GET_STR(STR_ERROR_SELECT_BEFORE_CENTER));
        ClearAction();
        return true;
    }


    CDrawMarker::MARKER_DATA* pData;
    pData = pMarker->GetMarker(iId);

    //選択オブジェクトは解除
    pCtrl->RelSelect();


    *ptOut = pData->ptObject;

    pMarker->Clear();
    pCtrl->Redraw();

    // 入力エリアにフォーカスを設定する
    m_pView->SetInputFocus();
    m_pView->SetComment(GET_SYS_STR(STR_SEL_ELLIPSE_VAL));
    return true;
}


/**
 *  動作クリア
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CActionEllipse::ClearAction()
{
    CViewAction::ClearAction();
    m_lstSelObj.clear();
    
    CDrawMarker*        pMarker = m_pView->GetMarker();
    CPartsDef*    pCtrl   = m_pView->GetPartsDef();

    pMarker->Clear();
    pCtrl->RelSelect();

    //一時的に作成したオブジェクトを削除する
    pCtrl->DeleteSelectingObject();
    pCtrl->Redraw();

    m_pView->SetComment(GET_SYS_STR(STR_SEL_ELLIPSE_CENTER));
}

//!< 点設定
bool CActionEllipse::_SelCenter(POINT2D* ptOut, POINT ptSel, CDrawingObject* pObj)
{
    //!< ビュー->データ変換
    POINT2D pt2D;
    POINT2D ptObj;
    CDrawingPoint* pPoint;
    int iId;
    CPartsDef*   pCtrl  = m_pView->GetPartsDef();
    m_pView->ConvScr2World(&pt2D, ptSel, m_pView->GetCurrentLayerId());

    bool bSel = false;
    //----------------------------------------------
    // 点選択
    if (pObj == NULL)
    {
        return false;
    }
    else if (pObj->GetType() == DT_POINT)
    {
        ClearAction();

        pPoint = dynamic_cast<CDrawingPoint*>(pObj);
        *ptOut = *pPoint->GetPointInstance();

        iId = pPoint->GetId();
        pCtrl->SelectDrawingObject(iId);

        // 入力エリアにフォーカスを設定する
        m_pView->SetInputFocus();
        m_pView->SetComment(GET_SYS_STR(STR_SEL_ELLIPSE_VAL));
        bSel = true;
    }
    else
    {
        //中心点を設定するためマーカを表示
        if (m_pView->SetMarker(pObj))        
        {
            //一時的に作成したオブジェクトを削除する
            pCtrl->DeleteSelectingObject();

            //マーカーを表示し選択状態にする
            iId = pObj->GetId();
            pCtrl->SelectDrawingObject(iId);

            m_lstSelObj.push_back(pObj);

            iId = pObj->GetId();
            pCtrl->SelectDrawingObject(iId);
        }
    }
    //----------------------------------------------
    return bSel;
}
