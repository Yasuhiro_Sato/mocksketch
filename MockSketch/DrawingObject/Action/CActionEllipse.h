/**
 * @brief			CActionEllipseヘッダーファイル
 * @file			CActionEllipse.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef _ACTION_ELLIPSE_H__
#define _ACTION_ELLIPSE_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CViewAction.h"
#include "DrawingObject/Primitive/POINT2D.h"
class     CDrawingView;

/**
 * @class   CActionEllipse
 * @brief                        
 */
class CActionEllipse: public CViewAction
{

    enum SEL_MODE
    {
        SM_CENTER_POINT,
        SM_FIRST_AXIS,
        SM_SECOND_RADIUS
    };

public:
	//!< コンストラクタ
    CActionEllipse();

    //!< コンストラクタ
    CActionEllipse(CDrawingView* pView);
    
	//!< デストラクタ
    virtual ~CActionEllipse();

	//!< 選択解放時動作
    virtual void Cancel(VIEW_MODE eMode);

	//!< 選択解放時動作
	virtual bool SelAction(VIEW_MODE eMode, void* pParam = NULL);

	//!< マウスクリック動作
	virtual bool SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

	//!< マウス移動動作
	virtual bool MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);


    //!<入力値設定
    virtual bool InputValue(StdString strVal);

protected:
    bool _SelCenter(POINT2D* ptOut, POINT ptSel, CDrawingObject* pObj);

    //!< マーカーチェック
    bool _CheckMarker( POINT2D* ptOut, POINT ptSel, SEL_ACTION eAction );

    //!< 動作クリア
    void ClearAction();

protected:
    //選択オブジェクト
    std::vector<CDrawingObject*> m_lstSelObj;

    // 選択位置
    std::vector<POINT2D> m_lstSelPoint;

protected:
    SEL_MODE   m_eSelMode;

    // 補助直線
    std::shared_ptr<CDrawingLine>   m_psTmpMouseLine;

    //角度
    double m_dTmpAngle;

    double m_dTmpRad1;

    double m_dTmpRad2;

    POINT2D m_ptCenter;
};
#endif // _ACTION_ELLIPSE_H__
