/**
 * @brief			CActionImage実装ファイル
 * @file			CActionImage.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CActionImage.h"
#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/View/CDrawingView.h"
#include "DrawingObject/CDrawingPoint.h"
#include "DrawingObject/CDrawingObject.h"
#include "DrawingObject/CDrawingLine.h"
#include "DrawingObject/CDrawingCircle.h"
#include "DrawingObject/CDrawingImage.h"
#include "View/CLayer.h"
#include "View/CDrawMarker.h"
#include "View/CUndoAction.h"
#include "Utility/CUtility.h"
#include "System/CSystem.h"
#include "SubWindow/ImageSelDlg.h"
#include "SubWindow/ImageCtrlDlg.h"

#include <math.h>

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 * コンストラクタ
 */
CActionImage::CActionImage():
CViewAction(),
m_eSelMode(SM_NONE)
{

    m_psEditingImage = std::make_shared<CDrawingImage>();
    m_lMouseOver =  SEL_FIG;
}

/**
 * コンストラクタ
 */
CActionImage::CActionImage(CDrawingView* pView):
CViewAction(pView),
m_eSelMode(SM_NONE)
{
    m_lMouseOver =  SEL_FIG;
    m_psEditingImage = std::make_shared<CDrawingImage>();
}
    
/**
 * デストラクタ
 */
CActionImage::~CActionImage()
{
    
}

/**
 *  @brief   解放時動作
 *  @param   [in]    eMode 解放時点でのモード
 *  @retval   なし
 *  @note
 */
void CActionImage::Cancel(VIEW_MODE eMode) 
{
    //選択解除
    ClearAction();
}

/**
 *  @brief   選択時動作
 *  @param   [in]    eMode 選択時点でのモード
 *  @retval   なし
 *  @note
 */
bool CActionImage::SelAction(VIEW_MODE eMode, void* pParam) 
{
    m_lMouseOver =   SEL_FIG ;
    m_eSelMode = SM_NONE;

    //既に選択中の図形はあるか？

    CPartsDef* pDef = m_pView->GetPartsDef();
    if (pDef)
    {
        long lSize = pDef->SelectNum();

        if (lSize != 0)
        {
            //選択を解除
            pDef->RelSelect();
        }   
    }
    std::vector<StdString> lst;
    pDef->GetImageList(&lst);
    

    bool bRet = false;
    bool bFile = true;
    StdString strFileNeme;
    if (lst.size() != 0)
    {
        CImageSelDlg dlg;
        if (!dlg.DoModal() == IDOK)
        {
            bRet = false;
        }

        int iSel = dlg.GetSelect();

        if (iSel == 1)  //IDC_RD_STOREDを選択 読込済みデータ使用
        {
            bFile = false;
        }
    }


    if (bFile)
    {
        //ファイル読み込み
        if (pDef->LoadImageDlg(&strFileNeme))
        {
            bRet = true;
        }
    }
    else
    {
        CImageCtrlDlg dlg(pDef);
        dlg.SetSelectMode();
        if(dlg.DoModal() == IDOK)
        {
            strFileNeme = dlg.GetSeletFileName();
            if (!strFileNeme.empty())
            {
                bRet = true;
            }
        }
    }

    if (!bRet)
    {
        return false;
    }

    CWaitCursor wait;
    m_psEditingImage->SetPartsDef(pDef);
    //!< イメージ読み込み
    if (!m_psEditingImage->SetImage(strFileNeme.c_str(), true))
    {
        wait.Restore();
        StdStringStream  strmError;
        StdString        strFormat;
        strFormat = GET_ERR_STR(e_file_read_s);
        strmError << StdFormat(strFormat) % strFileNeme.c_str();

        //ファイル
        AfxMessageBox(strmError.str().c_str());
        return false;
    }
    m_psEditingImage->SetMoveMode();

    /*
    if(!pDef->IsImage(pathSel.c_str()))
    {
        if(!pDef->LoadImage(pathSel.c_str()))
        {
            StdStringStream  strmError;
            StdString        strFormat;
            strFormat = GET_ERR_STR(e_file_read_s);
            strmError << StdFormat(strFormat) % pathSel.c_str();

            //ファイル
            AfxMessageBox(strmError.str().c_str());
            return false;
        }
    }
    */


    m_pView->SetComment(GET_SYS_STR(STR_SEL_BREAK));
    return true;    
}



/**
 *  @brief   マウス移動動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionImage::MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    bool bIgnoreGroup = false;
    POINT2D ptSnap;
    POINT2D ptMouse;


    using namespace VIEW_COMMON;

    bool bIgnoreSnap = false;
    if (posMouse.nFlags & MK_SHIFT)
    {
        bIgnoreSnap = true;
    }

    CPartsDef*   pDef  = m_pView->GetPartsDef();
    if (!pDef)
    {
        return false;
    }
    CDrawMarker* pMarker= m_pView->GetMarker();

    pMarker->Clear();
    pMarker->ClearObject();
    pDef->ClearMouseOver();
    pDef->ClearMouseEmphasis();
    pDef->ClearMouseConnectable();

    SNAP_DATA snap;
    m_pView->GetMousePoint(&snap, posMouse);
    auto pObj1 = pDef->GetObjectById(snap.iObject1, bIgnoreGroup);
    auto pObj2 = pDef->GetObjectById(snap.iObject2, bIgnoreGroup);


    if (!bIgnoreSnap)
    {
        if (pObj1  &&
            !pObj2)
        {
            m_pView->SetMarker(pObj1.get(), false /*bIgnoreOffLineObject*/);
            pDef->SetMouseEmphasis(pObj1);
        }

        if (pObj2)
        {
            //交点
            pDef->SetMouseEmphasis(pObj1);
            pDef->SetMouseEmphasis(pObj2);

            CDrawMarker::MARKER_DATA marker;
            marker.ptOffset.x = 0;
            marker.ptOffset.y = 0;
            marker.eType = CDrawMarker::MARKER_CROSS;
            marker.eSnap = VIEW_COMMON::SNP_CROSS_POINT;
            pMarker->AddMarker(marker, ptSnap);

        }

        int iMarkerId = pMarker->MouseMove(posMouse.ptSel);
    }
    else
    {
        snap.eSnapType = SNP_NONE;
        ptSnap = snap.ptOrg;
    }


//TRACE(_T("Action %f,%f \n"),ptSnap.dX,ptSnap.dY);
    m_psEditingImage->AbsMove(ptSnap);
    pMarker->AddObjectLast(m_psEditingImage); //図形の描画順序を最後にする

    pDef->DrawDragging(m_pView);

    m_eSelMode = SM_FIRST_POINT;
    return true;
}
/**
 *  @brief   マウスクリック動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionImage::SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    POINT ptSel = posMouse.ptSel;
    UINT nFlags = posMouse.nFlags;

    CPartsDef*   pDef  = m_pView->GetPartsDef();
    int                  iId  = m_pView->SearchPos(ptSel);
    if (!pDef)
    {
        return false;
    }

    auto     pObj  = pDef->GetObjectById(iId);

    if (eAction != CViewAction::ACT_LBUTTON_UP)
    {
        //左クリックのみ
        return false;
    }

    if (m_eSelMode == SM_NONE)
    {
        m_eSelMode = SM_FIRST_POINT;
        return false;
    }

    POINT2D pt2D;
    POINT2D ptMarker;

    //!< ビュー->データ変換
    int iLayerId = m_pView->GetCurrentLayerId();
    m_pView->ConvScr2World(&pt2D, ptSel, iLayerId);

    if(_CheckMarker( &ptMarker, posMouse.ptSel, eAction))
    {
        pt2D = ptMarker;
    }


    CDrawMarker*            pMarker = m_pView->GetMarker();
    auto pImage = std::make_shared<CDrawingImage>(*(m_psEditingImage.get()));
    pImage->SetLayer(m_pView->GetCurrentLayerId());
    pDef->RegisterObject(pImage, true, false);
    pMarker->ClearObject();
    m_pView->SetDrawingMode(DRAW_FRONT);
    pDef->Draw(pImage->GetId());


    ClearAction();

    m_pView->SetViewMode(VIEW_SELECT);
   return true;
}

/**
 *  動作クリア
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CActionImage::ClearAction()
{
    CPartsDef*    pDef   = m_pView->GetPartsDef();
    if (pDef)
    {
        pDef->RelSelect();
    }

    //終了したら選択状態へ移行する
    //m_pView->SetViewMode(VIEW_SELECT);

}
