/**
 * @brief			CViewAction実装ファイル
 * @file			CViewAction.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MockSketch.h"
#include "./CActionLine.h"

#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/View/CDrawingView.h"

#include "DrawingObject/CDrawingObject.h"
#include "DrawingObject/CDrawingPoint.h"
#include "DrawingObject/CDrawingLine.h"
#include "DrawingObject/CDrawingCircle.h"
#include "DrawingObject/CDrawingEllipse.h"
#include "DrawingObject/CDrawingSpline.h"
#include "DrawingObject/CDrawingNode.h"

#include "View/CLayer.h"
#include "View/CDrawMarker.h"
#include "View/CUndoAction.h"
#include "Utility/CUtility.h"
#include "System/CSystem.h"
#include "Script/CScriptEngine.h"
#include <math.h>

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//!< コンストラクタ
CActionLine::CActionLine():
CViewAction(),
m_psEditingLine(NULL),
m_psEditingLine2(NULL),
m_pObjFirst    (NULL)
{
    m_lMouseOver =   SEL_PRI ;
}

//!< コンストラクタ
CActionLine::CActionLine(CDrawingView* pView):
CViewAction(pView),
m_psEditingLine(NULL),
m_psEditingLine2(NULL),
m_pObjFirst    (NULL)
{
    m_lMouseOver =   SEL_PRI;
    m_psTmpEndpoint = std::make_shared<CDrawingPoint>();
    m_psTmpEndpoint->SetColor(DRAW_CONFIG->crImaginary);

    m_psTmpLine = std::make_shared<CDrawingLine>();
    m_psTmpLine->SetColor(DRAW_CONFIG->crAdditional);
    m_psTmpLine->SetLineType(PS_DOT);
}


//!< デストラクタ
CActionLine::~CActionLine()
{
    if (m_pView)
    {
        CDrawMarker* pMarker= m_pView->GetMarker();
        if (pMarker)
        {
            pMarker->ClearObject();
        }
    }
}


/**
 *  解放時動作
 *  @param   [in]    eMode 解放時点でのモード
 *  @retval   なし
 *  @note
 */
void CActionLine::Cancel(VIEW_MODE eMode)
{
    _ClearAction();
}

/**
 *  選択時動作
 *  @param   [in]    eMode 選択時点でのモード
 *  @retval   なし
 *  @note
 */
bool CActionLine::SelAction(VIEW_MODE eMode, void* pParam)
{
    m_iClickCnt = 0;

    if (eMode == VIEW_LINE)  
    {
        m_pView->SetComment(GET_SYS_STR(STR_SEL_POINT1));
    }
    m_eViewMode = eMode;
    return true;    
}


bool CActionLine::_SetFixLengthPos(POINT2D* pRet, POINT2D ptPos)
{

    if (!m_bFixLength)
    {
        return false;
    }

    double dLength = ptPos.Distance(m_ptFirst);
    POINT2D ptRet = m_ptFirst;
    if (fabs(dLength) > NEAR_ZERO)
    {
        double dScl = m_dFixdLength / dLength;
        ptRet.dX = m_ptFirst.dX + dScl * (ptPos.dX - m_ptFirst.dX);
        ptRet.dY = m_ptFirst.dY + dScl * (ptPos.dY - m_ptFirst.dY);

        if (m_dFixdLength < dLength)
        {
            int iLayerId = m_pView->GetCurrentLayerId();
            m_psTmpLine->SetPoint1(ptRet);
            m_psTmpLine->SetPoint2(ptPos);
            m_psTmpLine->SetLayer(iLayerId);
            m_pView->GetMarker()->AddObject(m_psTmpLine);
        }
        *pRet = ptRet;
    }
    return true;
}

bool CActionLine::_GetSnapAngleDistance(POINT2D* pEnfPoint, 
                            SNAP_DATA* pSnap,
                            POINT2D ptFirst,
                            double dFixdLength,
                            double dFixdAngle,
                            bool bFixLength,
                            bool bFixAngle,
							double dOffsetLength,
                            bool bIgnoreSnap )
{
    //入力値がある場合
    double dAngle = 0.0;
    double dLength = 0.0;
    POINT2D ptEndPos; // 追加する直線の終端
    bool bRet = false;
    bool bInputVal = false;
    if (bFixLength && !bFixAngle)
    {
        bRet = m_pView->GetFixLengthSnapPoint(&dAngle,
            pEnfPoint,
            dFixdLength + dOffsetLength,
            ptFirst,
            pSnap->pt,
            !bIgnoreSnap);

        dLength = dFixdLength;
    }
    else if (bFixAngle && !bFixLength)
    {
        bRet = m_pView->GetFixAngleSnapPoint(&dLength,
            pEnfPoint,
            dFixdAngle,
            ptFirst,
            pSnap->pt,
            !bIgnoreSnap,
			dOffsetLength);
    }
    else if (bFixAngle && bFixLength)
    {
        //角度、距離固定
        pEnfPoint->dX = ptFirst.dX + (dFixdLength + dOffsetLength )* cos(dFixdAngle * DEG2RAD);
        pEnfPoint->dY = ptFirst.dY + (dFixdLength  + dOffsetLength )* sin(dFixdAngle * DEG2RAD);
        bRet = true;
    } 
    else
    {
        double    dOffsetAngle = 0.0;
        bRet = m_pView->GetLengthSnapPoint(&dAngle,
            &dLength,
            pEnfPoint,
            ptFirst,
            pSnap->pt,
            !bIgnoreSnap,
            !bIgnoreSnap,
            dOffsetAngle,
			dOffsetLength);
    }

    SetSnapTooltip(VIEW_COMMON::SNP_ANGLE,
        dLength,
        dAngle);
    return bRet;
}

/**
 *  @brief   マウス移動動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionLine::MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    POINT ptSel = posMouse.ptSel;
    UINT nFlags = posMouse.nFlags;

    //!< スナップ点取得
    bool bSnap = true;

    CPartsDef*   pDef  = m_pView->GetPartsDef();
    if (!pDef)
    {
        return false;
    }

    pDef->RedrawWait();   //TODO:どうする？

    DWORD dwSnapType = DRAW_CONFIG->GetSnapType();

    CDrawMarker* pMarker= m_pView->GetMarker();

    SNAP_DATA snap;
    bool bKeepCenter = false;
    bool bIngnoreGroup = false;
    bSnap = m_pView->GetSnapPoint(&snap,
                        dwSnapType,
                        ptSel,
                        bIngnoreGroup,
                        bKeepCenter);

    auto pObj1 = pDef->GetObjectById(snap.iObject1);
    auto pObj2 = pDef->GetObjectById(snap.iObject2);

    POINT ptOffset;
    ptOffset.x = ptOffset.y = 0;
    CDrawMarker::MARKER_DATA marker;
    marker.ptOffset = ptOffset;


    bool bIgnoreSnap = false;
    if (nFlags & MK_SHIFT)
    {
        //スナップ無視
        bIgnoreSnap = true;
        bSnap = false;
        pObj1 = nullptr;
        pObj2 = nullptr;
        snap.pt = snap.ptOrg;
    }

    bool bBothSide = false;
    if (nFlags & MK_CONTROL)
    {
        //両側
        bBothSide = true;
    }


    m_pView->SetDrawingMode(DRAW_SEL);
    pMarker->Clear();
    pMarker->ClearObject();
    pDef->ClearMouseOver();
    pDef->ClearMouseEmphasis();
    pDef->ClearMouseConnectable();

    if (!m_psEditingLine)
    {
        //１点目未選択
        POINT ptOffset;
        ptOffset.x = ptOffset.y = 0;

        if (bSnap)
        {
            if (pObj1 &&
                !pObj2)
            {
                m_pView->SetMarker(pObj1.get(), false /*bIgnoreOffLineObject*/);
                if (pObj1->GetType() == DT_LINE)
                {
                    pDef->SetMouseOver(pObj1);
                }
                else
                {
                    pDef->SetMouseEmphasis(pObj1);
                }
            }

            if (pObj2)
            {
                //交点
                pDef->SetMouseEmphasis(pObj1);
                pDef->SetMouseEmphasis(pObj2);
                marker.eType = CDrawMarker::MARKER_CROSS;
                marker.eSnap = VIEW_COMMON::SNP_CROSS_POINT;
                pMarker->AddMarker(marker, snap.pt);
            }

        }
        else
        {
            m_pView->ConvScr2World(&snap.pt, ptSel,m_pView->GetCurrentLayerId());

            if (pObj1)
            {
                if(m_pView->SetMarker(pObj1.get(), true /*bIgnoreOffLineObject*/))
                {
                    pDef->SetMouseEmphasis(pObj1);
                }
            }
        }


        int iLayerId =  m_pView->GetCurrentLayerId();
        m_psTmpEndpoint->SetPoint(snap.pt);
        m_psTmpEndpoint->SetLayer(iLayerId);
        pMarker->AddObject(m_psTmpEndpoint);

        int iMarkerId = pMarker->MouseMove(posMouse.ptSel);
        if(iMarkerId == -1)
        {
            CDrawMarker* pMarker = m_pView->GetMarker();
            marker.eType = CDrawMarker::MARKER_CROSS;
            if (snap.eSnapType == VIEW_COMMON::SNP_NONE)
            {           
                bool bNone = true;
                if (pObj1)
                {
                    //オブジェクト上にあって、マーカー以外の位置の場合は
                    //選択可能
                    switch (pObj1->GetType())
                    {
                    case DT_LINE:
                    case DT_CIRCLE:
                        pDef->SetMouseOver(pObj1);
                        bNone = false;
                        break;
                    }
                }

                if (bNone)
                {
                    //任意点
                    marker.eSnap = VIEW_COMMON::SNP_NONE;
                    pMarker->AddMarker(marker, snap.pt);
                }

            }
            else if (snap.eSnapType == VIEW_COMMON::SNP_ON_LINE)
            {
                marker.eSnap = VIEW_COMMON::SNP_ON_LINE;
                pMarker->AddMarker(marker, snap.pt);
            }

        }


        SetSnapTooltip(snap.eSnapType, 0, 0);

        if (m_pObjFirst)
        {
            //選択したオブジェクトがある場合はここでマーカを追加する
            m_pView->SetMarker(m_pObjFirst.get());
        }
    }
    else
    {
        //------------------
        //１点目選択済み
        //------------------
        int iLayerId =  m_pView->GetCurrentLayerId();
        m_psEditingLine->SetLayer(iLayerId);

        pMarker->AddObject(m_psEditingLine);
        pDef->SetMouseOver(m_psEditingLine);


        //入力値の取得
        StdString sVal;
        sVal = m_pView->GetInputValue();
        if(sVal != m_strOldInput)
        {
            bool bRet;
            bRet = _CalcValue(sVal);
        }
 

        if (m_pObjFirst)
        {
            //最初にオブジェクトを選択した場合
            if (pObj1)
            {
                m_pView->SetMarker(pObj1.get());
            }
            bool bUseKeep = false;
            int iMarkerId = pMarker->MouseMove(posMouse.ptSel, bUseKeep);
            if (iMarkerId != -1)
            {
                pObj1 = nullptr;
            }

            switch(m_pObjFirst->GetType())
            {
            case DT_LINE:   
                _MovePointSecond_Line(&snap, posMouse, pObj1, bBothSide, bIgnoreSnap);
                break;

            case DT_CIRCLE: 
                _MovePointSecond_Circle(&snap, posMouse, pObj1, bBothSide, bIgnoreSnap);
                break;

            default:
                break;
            }
        }
        else
        {
            //入力値がある場合
            double dAngle = 0.0;
            double dLength = 0.0;
            POINT2D ptEndPos; // 追加する直線の終端
            
            bool bInputVal = false;
            if (m_bFixLength && !m_bFixAngle)
            {
                m_pView->GetFixLengthSnapPoint(&dAngle,
                                            &ptEndPos,  
                                        m_dFixdLength,
                                        m_ptFirst,  
                                        snap.pt,
                                        !bIgnoreSnap);

                dLength = m_dFixdLength;
                bInputVal = true;
            }
            else if (m_bFixAngle && !m_bFixLength)
            {
                m_pView->GetFixAngleSnapPoint(&dLength,
                                            &ptEndPos,  
                                        m_dFixdAngle,
                                        m_ptFirst,  
                                        snap.pt,
                                        !bIgnoreSnap);

                dAngle = m_dFixdAngle;
                bInputVal = true;
            }
            else if (m_bFixAngle && m_bFixLength)
            {
                //角度、距離固定
                dAngle =  m_dFixdAngle;
                dLength = m_dFixdLength;
                ptEndPos.dX = m_ptFirst.dX + m_dFixdLength * cos(  dAngle * DEG2RAD);
                ptEndPos.dY = m_ptFirst.dY + m_dFixdLength * sin(  dAngle * DEG2RAD);
                bInputVal = true;
                //クリックで確定とする
                pObj1 = NULL;
            }

            //角度固定の場合は交点のみ
            POINT2D ptAngle;
            if (m_bFixAngle)
            {
                ptAngle.dX = m_ptFirst.dX + cos(  m_dFixdAngle * DEG2RAD);
                ptAngle.dY = m_ptFirst.dY + sin(  m_dFixdAngle * DEG2RAD);
            }

            //---------------------------

            if (bIgnoreSnap)
            {
                pObj1 = NULL;
            }

            //最初は点(orなにも無い)
            if (pObj1)
            {
                //スナップ無視
                DRAWING_TYPE typeOnMouse = pObj1->GetType();

                pDef->SetMouseEmphasis(pObj1);
                marker.iObjct = pObj1->GetId();

                bool bIgnoreOffLineObject = true;


                if (m_bFixAngle)
                {
                    bool bRet = false;
                    POINT2D ptCross;
                    POINT2D pt2D;
                    LINE2D line(m_ptFirst, ptAngle);
                    std::vector<POINT2D> lstCross;

                    if (typeOnMouse == DT_LINE)
                    {
                        auto pLine = std::dynamic_pointer_cast<CDrawingLine>(pObj1);
                        LINE2D* pSelctedLine;
                        pSelctedLine = pLine->GetLineInstance();

                        bool bRetIntersect = pSelctedLine->IntersectInfnityLine(&ptCross, line);

                        if (bRetIntersect && pSelctedLine->IsOnLine(ptCross))
                        {
                            POINT2D ptEnd;
                            double l1 = pSelctedLine->GetPt1().Distance(ptCross);
                            double l2 = pSelctedLine->GetPt2().Distance(ptCross);

                            if (l1 > l2)
                            {
                                ptEnd = pSelctedLine->GetPt2();
                            }
                            else
                            {
                                ptEnd = pSelctedLine->GetPt1();
                            }

                            int iLayerId = m_pView->GetCurrentLayerId();
                            m_psTmpLine->SetPoint1(ptEnd);
                            m_psTmpLine->SetPoint2(ptCross);
                            m_psTmpLine->SetLayer(iLayerId);
                            pMarker->AddObject(m_psTmpLine);
                            bRet = true;
                        }
                    }
                    else if (typeOnMouse == DT_CIRCLE)
                    {

                        auto pCircle = std::dynamic_pointer_cast<CDrawingCircle>(pObj1);
                        CIRCLE2D* pCircle2D;
                        pCircle2D = pCircle->GetCircleInstance();

                        pCircle2D->Intersect(line, &lstCross, false);

                        if (lstCross.size() > 0)
                        {
                            if (pt2D.Distance(lstCross[0]) < m_pView->GetSnapRadius())
                            {
                                ptCross = lstCross[0];
                                bRet = true;
                            }
                        }

                        if (lstCross.size() > 1)
                        {
                            if (pt2D.Distance(lstCross[1]) < m_pView->GetSnapRadius())
                            {
                                ptCross = lstCross[1];
                                bRet = true;
                            }
                        }
                    }
                    else if (typeOnMouse == DT_ELLIPSE)
                    {
                        auto pEllipse = std::dynamic_pointer_cast<CDrawingEllipse>(pObj1);
                        ELLIPSE2D* pEllipse2D;
                        pEllipse2D = pEllipse->GetEllipseInstance();

                        pEllipse2D->Intersect(line, &lstCross, false);

                        if (lstCross.size() > 0)
                        {
                            if (pt2D.Distance(lstCross[0]) < m_pView->GetSnapRadius())
                            {
                                ptCross = lstCross[0];
                                bRet = true;
                            }
                        }

                        if (lstCross.size() > 1)
                        {
                            if (pt2D.Distance(lstCross[1]) < m_pView->GetSnapRadius())
                            {
                                ptCross = lstCross[1];
                                bRet = true;
                            }
                        }
                    }
                    else if (typeOnMouse == DT_SPLINE)
                    {
                        auto pSpline = std::dynamic_pointer_cast<CDrawingSpline>(pObj1);
                        SPLINE2D* pSpline2D;
                        pSpline2D = pSpline->GetSplineInstance();

                        POINT2D ptCross;
                        pSpline2D->Intersect(line, &lstCross, false);

                        if (lstCross.size() > 0)
                        {
                            if (pt2D.Distance(lstCross[0]) < m_pView->GetSnapRadius())
                            {
                                ptCross = lstCross[0];
                                bRet = true;
                            }
                        }

                        if (lstCross.size() > 1)
                        {
                            if (pt2D.Distance(lstCross[1]) < m_pView->GetSnapRadius())
                            {
                                ptCross = lstCross[1];
                                bRet = true;
                            }
                        }
                    }



                    if (bRet)
                    {
                        marker.eType = CDrawMarker::MARKER_CROSS;
                        marker.eSnap = VIEW_COMMON::SNP_CROSS_POINT;
                        pMarker->AddMarker(marker, ptCross);
                    }
                }
                else
                {
                    std::vector<CDrawMarker::MARKER_DATA> lstMarker;
                    //角度自由時の交点設定
                    m_pView->SetMarker(pObj1.get(), bIgnoreOffLineObject);
                    if (pObj2)
                    {
                        pDef->SetMouseEmphasis(pObj2);
                        marker.eType = CDrawMarker::MARKER_CROSS;
                        marker.eSnap = VIEW_COMMON::SNP_CROSS_POINT;
                        marker.ptObject = snap.pt;
                        lstMarker.push_back(marker);
                    }


                    if(typeOnMouse == DT_LINE)
                    {
                        //点-線
                        //端点＋中点+垂線でスナップ
                        //MovePointSecond_PointLine(); 

                        auto pLine = std::dynamic_pointer_cast<CDrawingLine>(pObj1);
                        LINE2D* pSelctedLine;
                        pSelctedLine = pLine->GetLineInstance();

                        POINT2D ptNear;
                        ptNear = pSelctedLine->NearPoint(m_ptFirst);
                        // 近接点
                        if (pSelctedLine->IsOnLine(ptNear))
                        {
                            marker.eType = CDrawMarker::MARKER_NORM;
                            marker.eSnap = VIEW_COMMON::SNP_NORMAL;
                            marker.ptObject = ptNear;
                            lstMarker.push_back(marker);
                        }
                    }
                    else if (typeOnMouse == DT_CIRCLE) 
                    {           
                        //点-円
                        //端点＋中点+垂線+接線でスナップ
                        POINT2D pt2D;
                        int iLayerId = m_pView->GetCurrentLayerId();
                        m_pView->ConvScr2World(&pt2D, ptSel, iLayerId);

                        auto pCircle = std::dynamic_pointer_cast<CDrawingCircle>(pObj1);
                        CIRCLE2D* pCircle2D;
                        pCircle2D = pCircle->GetCircleInstance();

                        //近接点(垂線)
                        POINT2D ptNear = pCircle2D->NearPoint(m_ptFirst);
                        if (pCircle2D->IsInsideAngle(ptNear))
                        {
                            marker.eType = CDrawMarker::MARKER_NORM;
                            marker.eSnap = VIEW_COMMON::SNP_NORMAL;
                            marker.ptObject = ptNear;
                            lstMarker.push_back(marker);
                        }

                        //接線
                        std::vector<POINT2D> lstTangent;
                        if (pCircle2D->TangentPoint(&lstTangent, m_ptFirst))
                        {
                            if ((lstTangent.size() > 0)&&
                                (pCircle2D->IsInsideAngle(lstTangent[0])))
                            {
                                marker.eType = CDrawMarker::MARKER_TANGENT;
                                marker.eSnap = VIEW_COMMON::SNP_TANGENT;
                                marker.ptObject = lstTangent[0];
                                lstMarker.push_back(marker);
                            }

                            if ((lstTangent.size() > 1)&&
                                (pCircle2D->IsInsideAngle(lstTangent[1])))
                            {
                                marker.eType = CDrawMarker::MARKER_TANGENT;
                                marker.eSnap = VIEW_COMMON::SNP_TANGENT;
                                marker.ptObject = lstTangent[1];
                                lstMarker.push_back(marker);
                            }
                        }
                    }
                    else if (typeOnMouse == DT_ELLIPSE) 
                    {
                        //点-円
                        //端点＋中点+垂線+接線でスナップ
                        POINT2D pt2D;
                        int iLayerId = m_pView->GetCurrentLayerId();
                        m_pView->ConvScr2World(&pt2D, ptSel, iLayerId);

                        auto pEllipse = std::dynamic_pointer_cast<CDrawingEllipse>(pObj1);
                        ELLIPSE2D* pEllipse2D;
                        pEllipse2D = pEllipse->GetEllipseInstance();

                        //近接点(垂線)
                        POINT2D ptNear = pEllipse2D->NearPoint(m_ptFirst);
                        if (pEllipse2D->IsInsideAngle(ptNear))
                        {
                            marker.eType = CDrawMarker::MARKER_NORM;
                            marker.eSnap = VIEW_COMMON::SNP_NORMAL;
                            marker.ptObject = ptNear;
                            lstMarker.push_back(marker);
                        }

                        //接線
                        std::vector<POINT2D> lstTangent;
                        if (pEllipse2D->TangentPoint(&lstTangent, m_ptFirst))
                        {
                            if ((lstTangent.size() > 0) &&
                                (pEllipse2D->IsInsideAngle(lstTangent[0])))
                            {
                                marker.eType = CDrawMarker::MARKER_TANGENT;
                                marker.eSnap = VIEW_COMMON::SNP_TANGENT;
                                marker.ptObject = lstTangent[0];
                                lstMarker.push_back(marker);
                            }

                            if ((lstTangent.size() > 1) &&
                                (pEllipse2D->IsInsideAngle(lstTangent[1])))
                            {
                                marker.eType = CDrawMarker::MARKER_TANGENT;
                                marker.eSnap = VIEW_COMMON::SNP_TANGENT;
                                marker.ptObject = lstTangent[1];
                                lstMarker.push_back(marker);
                            }
                        }
                    }
                    else if (typeOnMouse == DT_SPLINE)
                    {
                        //点-スプライン
                        //端点＋特徴点+垂線+接線スナップ

                        POINT2D pt2D;
                        int iLayerId = m_pView->GetCurrentLayerId();
                        m_pView->ConvScr2World(&pt2D, ptSel, iLayerId);

                        auto pSpline = std::dynamic_pointer_cast<CDrawingSpline>(pObj1);
                        SPLINE2D* pSpline2D;
                        pSpline2D = pSpline->GetSplineInstance();


                        //垂線
                        std::vector<POINT2D> lstNorm;
                        if(pSpline2D->NormPoints(&lstNorm, m_ptFirst))
                        {
                            marker.eType = CDrawMarker::MARKER_NORM;
                            marker.eSnap = VIEW_COMMON::SNP_NORMAL;
                            for (auto pt : lstNorm)
                            {
                                marker.ptObject = pt;
                                lstMarker.push_back(marker);
                            }
                        }

                        //接線
                        std::vector<POINT2D> lstTangent;
                        if (pSpline2D->TangentPoint(&lstTangent, m_ptFirst))
                        {
                            marker.eType = CDrawMarker::MARKER_TANGENT;
                            marker.eSnap = VIEW_COMMON::SNP_TANGENT;
                            for (auto pt: lstTangent)
                            {
                                marker.ptObject = pt;
                                lstMarker.push_back(marker);
                            }
                        }
                    }
                    else if (typeOnMouse == DT_COMPOSITION_LINE)
                    {
                        //特になし
                    }
                    else if (typeOnMouse ==  DT_CONNECTION_LINE)
                    {
                        //特になし
                    }
                    else if (typeOnMouse ==  DT_POINT)
                    {
                        POINT2D pt1, pt2;
                        auto pPoint = std::dynamic_pointer_cast<CDrawingPoint>(pObj1);
                        marker.eType = CDrawMarker::MARKER_POINT;
                        marker.eSnap = VIEW_COMMON::SNP_CENTER;

                        pMarker->AddMarker(marker, pPoint->GetPoint());
                    }


                    marker.eSnap = VIEW_COMMON::SNP_TANGENT;
                    marker.eType = CDrawMarker::MARKER_TANGENT;

                    int iVal = 0;
                    for (auto marker : lstMarker)
                    {
                        marker.iVal = iVal;
                        pMarker->AddMarker(marker, marker.ptObject);
                        auto drawLine = std::make_shared<CDrawingLine>();
                        drawLine->SetPoint(m_ptFirst, marker.ptObject);
                        drawLine->SetColor(DRAW_CONFIG->crAdditional);
                        drawLine->SetLineType(PS_DASH);
                        drawLine->SetLayer(m_pView->GetCurrentLayerId());

                        m_psSelectLines.push_back(drawLine);
                        pMarker->AddObject(drawLine);
                        iVal++;
                    }
                }
            }

          int iMarkerId;
            iMarkerId = pMarker->MouseMove(posMouse.ptSel);


            //通常のスナップ以外の処理
            //通常のスナップは m_pView->GetSnapPoint で取得済み
            if (iMarkerId != -1)
            {
                CDrawMarker::MARKER_DATA* pData;
                pData = pMarker->GetMarker( iMarkerId );
                ptEndPos = pData->ptObject;
                snap.eSnapType = pData->eSnap;
                bSnap = true;
            }
            else
            {
                snap.eSnapType = VIEW_COMMON::SNP_ANGLE;
                if (!bInputVal)
                {
                    m_pView->GetLengthSnapPoint(&dAngle,
                                                &dLength,
                                                &ptEndPos,  
                                                m_ptFirst,  
                                                snap.pt,
                                                true,
                                                true);
                    snap.pt = ptEndPos;
                }
            }


            _SetFixLengthPos(&ptEndPos, snap.pt);

            POINT2D pt1 = m_ptFirst;

            m_psEditingLine->SetPoint2(ptEndPos);

            if (bBothSide)
            {
                POINT2D pt3;
                pt3 = pt1 + pt1 - ptEndPos;
                m_psEditingLine->SetPoint1(ptEndPos);
                m_psEditingLine->SetPoint2(pt3);
            }
            else
            {
                m_psEditingLine->SetPoint1(ptEndPos);
                m_psEditingLine->SetPoint2(pt1);
            }


            if (snap.eSnapType == VIEW_COMMON::SNP_ANGLE)
            {

                SetSnapTooltip(VIEW_COMMON::SNP_ANGLE, 
                                            dLength,
                                            dAngle);
            }
            else
            {
                SetSnapTooltip(snap.eSnapType, 0, 0);
            }

            pMarker->HideObject(false);
        }
    }


    //============================================
    pDef->DrawDragging(m_pView);
    return true;

}

/**
 *  @brief   マウス移動動作(直線を最初に選択した場合)
 *  @param   [in] eSnapType   選択位置
 *  @param   [in] pt2D 選択動作
 *  @param   [in] pObj 
 *  @retval  なし
 *  @note
 */
void CActionLine::_MovePointSecond_Line
                            (SNAP_DATA* pSnap,
                             MOUSE_MOVE_POS posMouse,
                             std::shared_ptr<CDrawingObject> pObj,
                             bool bBoth,
                             bool bIgnoreSnap)
{
    auto pLine1 = std::dynamic_pointer_cast<CDrawingLine>(m_pObjFirst)->GetLineInstance();

    CDrawMarker* pMarker = m_pView->GetMarker();

    if (pObj == m_pObjFirst)
    {
        //自分自身のマーカを選択
        m_psEditingLine->SetPoint1(pSnap->pt);
        m_psEditingLine->SetPoint2(pSnap->pt);


        if (pSnap->eSnapType != VIEW_COMMON::SNP_ON_LINE)
        {
            SetSnapTooltip(pSnap->eSnapType, 0.0 , 0.0);
        }
        else
        {
            SetSnapTooltip(VIEW_COMMON::SNP_NONE, 0.0 , 0.0);
        }
        return;
    }
	CPartsDef* pDef = m_pView->GetPartsDef();

    //任意点
    CDrawMarker::MARKER_DATA marker;
	marker.eType = CDrawMarker::MARKER_CROSS;

	bool bEmphasis = false;
    bool bSnapLength = false;
    if (pSnap->eSnapType == VIEW_COMMON::SNP_NONE)
    {
        marker.eSnap = VIEW_COMMON::SNP_NONE;
        pMarker->AddMarker(marker, pSnap->pt);
        bSnapLength = true;
    }
    else if (pSnap->eSnapType == VIEW_COMMON::SNP_ON_LINE)
    {
        marker.eSnap = VIEW_COMMON::SNP_ON_LINE;
        pMarker->AddMarker(marker, pSnap->pt);
		bEmphasis = true;
    }

    if (pObj)
    {
		if (bEmphasis)
		{
			pDef->SetMouseEmphasis(pObj);
		}

        if(pObj->GetType() == DT_LINE)
        {
            if (pSnap->eSnapType == VIEW_COMMON::SNP_NONE)
            {
                //===============
                //角の２等分線
                //===============
				pDef->SetMouseEmphasis(pObj);
				auto pLine2 = std::dynamic_pointer_cast<CDrawingLine>(pObj)->GetLineInstance();
                POINT2D ptCross;

                // 交点を求める
                bool bRet;
                bRet = pLine1->IntersectInfnityLine(&ptCross, *pLine2);
 
                if (!bRet)
                {
                    //２直線が平行

                    //pLine1と直交しpLine1->GetPoint1()を通る直線と
                    //pLine2の交点を求める

                    LINE2D lineNorm;
                    POINT2D ptMove;

                    lineNorm = pLine1->NormalLine(pLine1->Pt(0));
                    bRet = lineNorm.IntersectInfnityLine(&ptCross, *pLine2);

                    bool bLine = false;

                    if (bRet)
                    {
                        ptMove = (ptCross - pLine1->Pt(0)) / 2.0;
                        if (ptMove.Length() > NEAR_ZERO)
                        {
                            m_psEditingLine->SetPoint1(pLine1->Pt(0) + ptMove);
                            m_psEditingLine->SetPoint2(pLine1->Pt(1) + ptMove);
                            m_pView->SetToolTipText(GET_STR(STR_BISECTRIX));
                            bLine = true;
                        }
                    }

                    if (!bLine)
                    {
                        m_psEditingLine->SetPoint1(m_ptFirst);
                        m_psEditingLine->SetPoint2(pSnap->pt);
                    }

                    if (bBoth)
                    {   //初回に選択した直線を中心に対象位置に
                        if (bLine)
                        {
                            if (!m_psEditingLine2)
                            {
								auto pObj = THIS_APP->CreateDefaultObject(DT_LINE);
								auto defObj = std::dynamic_pointer_cast<CDrawingLine>(pObj);
								m_psEditingLine2 = defObj;
								m_psEditingLine2->SetColor(DRAW_CONFIG->crImaginary);

                            }
							int iLayerId = m_pView->GetCurrentLayerId();
							m_psEditingLine2->SetPoint1(pLine1->Pt(0) - ptMove);
                            m_psEditingLine2->SetPoint2(pLine1->Pt(1) - ptMove);
							m_psEditingLine2->SetLayer(iLayerId);
							pMarker->AddObject(m_psEditingLine2);
							pDef->SetMouseOver(m_psEditingLine2);
						}
                    }
					else
					{
						m_psEditingLine2.reset();
					}
                    marker.eSnap = VIEW_COMMON::SNP_BISECTRIX;
                    SetSnapTooltip(pSnap->eSnapType, 0.0, 0.0);
                    return;
                }
                else
                {
                    //２直線に交点あり
                    // 
                    POINT2D ptClickFirstOnLine; 
                    POINT2D ptClickSecondOnLine; 
                    LINE2D  vecClickFirst;
                    LINE2D  vecCurMouse;       
                
                    ptClickFirstOnLine  = pLine1->NearPoint(m_ptFirst);
                    ptClickSecondOnLine = pLine2->NearPoint(pSnap->pt);

                    vecClickFirst.SetPt(ptCross, ptClickFirstOnLine);
                    vecCurMouse.SetPt(ptCross, ptClickSecondOnLine);

                    double dDiffAngle1;
                    double dDiffAngle2;
                
                    double dAngle1;
                    double dAngle2;
                    double dAngleMid;
                
                    dAngle1 = pLine1->Angle();
                    dAngle2 = pLine2->Angle();

                    dDiffAngle1 = CUtil::NormAngle(dAngle1 - vecClickFirst.Angle());
                    dDiffAngle2 = CUtil::NormAngle(dAngle2 - vecCurMouse.Angle());

                    bool bRev = false;
                    if ( (fabs(dDiffAngle1) - 180.0) < NEAR_ZERO)
                    {
                        dAngle1 = CUtil::NormAngle(pLine1->Angle() + 180.0);
                        bRev = true;
                    }

                    dAngleMid = (vecClickFirst.Angle() + vecCurMouse.Angle()) / 2.0;

                    double dRotete = dAngleMid - dAngle1;
                    POINT2D p0 = pLine1->Pt(0);
                    POINT2D p1 = pLine1->Pt(1);

                    if (bBoth)
                    {
                        if (p0.Distance(ptCross) > p1.Distance(ptCross))
                        {
                            p1 = p0;
                            p1.Rotate(ptCross, 180.0);
                        }
                        else
                        {
                            p0 = p1;
                            p0.Rotate(ptCross, 180.0);
                        }
                    }

                    p0.Rotate(ptCross, dRotete);
                    p1.Rotate(ptCross, dRotete);

                    m_psEditingLine->SetPoint1(p0);
                    m_psEditingLine->SetPoint2(p1);

                    marker.eSnap = VIEW_COMMON::SNP_BISECTRIX;
                    SetSnapTooltip(pSnap->eSnapType, 0.0, 0.0);
                    return;
                }
            }
        }
        else if(pObj->GetType() == DT_POINT)
        {
            //===============
            // 線-点
            //===============
            //垂線を引く
            POINT2D ptSel2D;
            auto pPoint = std::dynamic_pointer_cast<CDrawingPoint>(pObj);
            ptSel2D = *pPoint->GetPointInstance();

            pSnap->pt = ptSel2D;
            pSnap->eSnapType = VIEW_COMMON::SNP_POINT;
        }
        else if(pObj->GetType() == DT_CIRCLE)
        {
            //なし
        }
    }

    bool bUseKeep = false;
    int iMarkerId = pMarker->MouseMove(posMouse.ptSel, bUseKeep);

    _DrawNormalSegment(*pLine1, pSnap->pt, bBoth, pSnap->eSnapType);
	pMarker->HideObject(false);

}

void CActionLine::_MovePointSecond_Circle
                            (SNAP_DATA* pSnap,
                             MOUSE_MOVE_POS posMouse,
                             std::shared_ptr<CDrawingObject> pObj,
                             bool bBoth,
                             bool bIgnoreSnap)
{
    auto pCircle = std::dynamic_pointer_cast<CDrawingCircle>(m_pObjFirst)->GetCircleInstance();

    CDrawMarker* pMarker = m_pView->GetMarker();

    if (pObj == m_pObjFirst)
    {
        //自分自身のマーカを選択
        m_psEditingLine->SetPoint1(pSnap->pt);
        m_psEditingLine->SetPoint2(pSnap->pt);


        if (pSnap->eSnapType != VIEW_COMMON::SNP_ON_LINE)
        {
            SetSnapTooltip(pSnap->eSnapType, 0.0, 0.0);
        }
        else
        {
            SetSnapTooltip(VIEW_COMMON::SNP_NONE, 0.0, 0.0);
        }
        return;
    }
    CPartsDef* pDef = m_pView->GetPartsDef();

    //任意点
    CDrawMarker::MARKER_DATA marker;
    marker.eType = CDrawMarker::MARKER_CROSS;

    bool bEmphasis = false;
    bool bSnapLength = false;

    StdString strToolTipText;

    bool bNearPoint = false;
    bool bSnapTangent = false;
    POINT2D ptNear;

    m_psSelectLines.clear();
    std::vector<LINE2D> lstLine;

    if (pObj)
    {
        if (bEmphasis)
        {
            pDef->SetMouseEmphasis(pObj);
        }

        if (pObj->GetType() == DT_POINT)
        {
            bNearPoint = true;
            auto pPoint = std::dynamic_pointer_cast<CDrawingPoint>(pObj);
            ptNear = *pPoint->GetPointInstance();
            pSnap->eSnapType = VIEW_COMMON::SNP_POINT;
            pDef->SetMouseEmphasis(pObj);
        }
        else if (pObj->GetType() == DT_LINE)
        {
            //なし
        }
        else if (pObj->GetType() == DT_CIRCLE)
        {
            pDef->SetMouseEmphasis(pObj);
            auto pDrawCircle2 = std::dynamic_pointer_cast<CDrawingCircle>(pObj);
            CIRCLE2D* pCircle2 = pDrawCircle2->GetCircleInstance();
			pCircle->TangentLine(&lstLine, *pCircle2);
        }
        else if (pObj->GetType() == DT_ELLIPSE)
        {
            pDef->SetMouseEmphasis(pObj);
            auto pDrawEllipse = std::dynamic_pointer_cast<CDrawingEllipse>(pObj);
            ELLIPSE2D* pEllipse = pDrawEllipse->GetEllipseInstance();
            pCircle->TangentLine(&lstLine, *pEllipse);
        }
        else if (pObj->GetType() == DT_SPLINE)
        {
            /* 計算量的に無理
            pDef->SetMouseEmphasis(pObj);
            auto pDrawSpline = std::dynamic_pointer_cast<CDrawingSpline>(pObj);
            SPLINE2D* pSpline = pDrawSpline->GetSplineInstance();
            pCircle->TangentLine(&lstLine, *pSpline);
            */
        }
    }

    if (!lstLine.empty())
    {
        marker.eSnap = VIEW_COMMON::SNP_TANGENT;
        marker.eType = CDrawMarker::MARKER_TANGENT;

        int iVal = 0;
        for (auto line : lstLine)
        {
            marker.iVal = iVal;
            pMarker->AddMarker(marker, line.GetPt2());
            auto drawLine = std::make_shared<CDrawingLine>();
            drawLine->SetLine(line);
            drawLine->SetColor(DRAW_CONFIG->crAdditional);
            drawLine->SetLineType(PS_DASH);
            drawLine->SetLayer(m_pView->GetCurrentLayerId());

            m_psSelectLines.push_back(drawLine);
            pMarker->AddObject(drawLine);
            iVal++;
        }
        pMarker->HideObject(false);
        bool bUseKeep = false;
        int iMarkerId = pMarker->MouseMove(posMouse.ptSel, bUseKeep);
        if (iMarkerId != -1)
        {
            CDrawMarker::MARKER_DATA* pData;
            pData = pMarker->GetMarker(iMarkerId);
            if (pData->iVal >= 0)
            {
                pSnap->eSnapType = pData->eSnap;
                pSnap->pt = pData->ptObject;
                bSnapTangent = true;

                if (bBoth)
                {
                    POINT2D pt3 = lstLine[pData->iVal].GetPt1();
                    pt3.Rotate(lstLine[pData->iVal].GetPt2(), 180.0);
                    m_psEditingLine->SetPoint1(pt3);
                    m_psEditingLine->SetPoint2(lstLine[pData->iVal].GetPt2());
                }
                else
                {
                    m_psEditingLine->SetLine(lstLine[pData->iVal]);
                }
            }
        }
    }

    if (!bSnapTangent)
    {
        if (pSnap->eSnapType == VIEW_COMMON::SNP_NONE)
        {
            marker.eSnap = VIEW_COMMON::SNP_NONE;
            pMarker->AddMarker(marker, pSnap->pt);
            ptNear = pSnap->pt;
            bNearPoint = true;

            POINT2D ptEndPos; // 追加する直線の終端

            double    dOffsetAngle = 0.0;
            double    dOffsetLength = 0.0;
            _GetSnapAngleDistance(&ptEndPos,
                pSnap,
                pCircle->ptCenter,
                m_dFixdLength,
                m_dFixdAngle,
                m_bFixLength,
                m_bFixAngle,
                pCircle->GetRadius(),
                bIgnoreSnap);

			LINE2D l(pCircle->ptCenter, ptEndPos);

			std::vector<POINT2D> lstPoint;
			pCircle->Intersect(l, &lstPoint, false);

			if (lstPoint.size() == 2)
			{
                POINT2D ptOnCircle;
                if (ptEndPos.Distance(lstPoint[0]) > ptEndPos.Distance(lstPoint[1]))
                {
                    ptOnCircle = lstPoint[1];
                }
                else
                {
                    ptOnCircle = lstPoint[0];
                }

                if (l.Length() < pCircle->GetRadius())
                {
                    m_psTmpLine->SetPoint1(pCircle->ptCenter);
                    m_psTmpLine->SetPoint2(ptEndPos);
                }
                else
                {
				    m_psTmpLine->SetPoint1(pCircle->ptCenter);
                    m_psTmpLine->SetPoint2(ptOnCircle);
                }
                int iLayerId = m_pView->GetCurrentLayerId();
                m_psTmpLine->SetLayer(iLayerId);
				m_pView->GetMarker()->AddObject(m_psTmpLine);
                pMarker->HideObject(false);

                if (bBoth)
                {
                    POINT2D pt3 = ptEndPos  ;
                    pt3.Rotate(ptOnCircle, 180.0);
                    m_psEditingLine->SetPoint1(pt3);
                    m_psEditingLine->SetPoint2(ptEndPos);
                }
                else
                {
                    m_psEditingLine->SetPoint1(ptOnCircle);
                    m_psEditingLine->SetPoint2(ptEndPos);
                }
			}
            else
            {
            }


			if (!bIgnoreSnap)
			{
			   marker.eSnap = VIEW_COMMON::SNP_ANGLE;
			   marker.eType = CDrawMarker::MARKER_POINT;
			   pMarker->AddMarker(marker, pSnap->pt);
			}
            return;
        }
        else if (pSnap->eSnapType == VIEW_COMMON::SNP_ON_LINE)
        {
            marker.eSnap = VIEW_COMMON::SNP_ON_LINE;
            pMarker->AddMarker(marker, pSnap->pt);
            ptNear = pSnap->pt;
            if(pObj)
            {
                pDef->SetMouseEmphasis(pObj);
            }
            bNearPoint = true;
        }
        else 
        {
            ptNear = pSnap->pt;
            if (pObj)
            {
                pDef->SetMouseEmphasis(pObj);
            }
            bNearPoint = true;
        }
    }

    if (bNearPoint)
    {
        auto pPoint = std::dynamic_pointer_cast<CDrawingPoint>(pObj);
        POINT2D* pSel2D = pPoint->GetPointInstance();

        POINT2D p2 = pCircle->NearPoint(ptNear);

        m_psEditingLine->SetPoint1(p2);
        m_psEditingLine->SetPoint2(ptNear);
       
        bool bEnableNone = true;

        strToolTipText = VIEW_COMMON::GetSnapName(VIEW_COMMON::SNP_NORMAL);
        strToolTipText += _T("-");
        strToolTipText += VIEW_COMMON::GetSnapName(pSnap->eSnapType, bEnableNone);
        m_pView->SetToolTipText(strToolTipText.c_str());
    }
}

/**
 *  1点を通る直線への垂線
 *  @param   [in]    line   垂線を引く線分
 *  @param   [in]    ptSel  マウスカーソルの位置
 *  @param   [in]    bBoth  true:両端
 *  @retval   なし
 *  @note
 */
void CActionLine::_DrawNormalSegment(const LINE2D& line, 
                                     const POINT2D& ptSel, bool bBoth, 
                                     VIEW_COMMON::E_SNAP_TYPE eType)
{
    using namespace VIEW_COMMON;
    LINE2D lineNorm;
    POINT2D pt1;
    POINT2D pt2;
    StdString strToolTipText;

    strToolTipText = GetSnapName(SNP_NORMAL);
    pt1 = line.NearPoint(ptSel, false);
    pt2 = ptSel;

    if (eType == VIEW_COMMON::SNP_NONE)
    {
        //中点、両端でスナップさせる
        bool bSnap = false;
        std::list<SNAP_DATA> lstSnap;
        SNAP_DATA snapData;

        snapData.eSnapType = SNP_END_POINT;
        snapData.pt = line.Pt(0);
        lstSnap.push_back(snapData);

        snapData.pt = line.Pt(1);
        lstSnap.push_back(snapData);

        snapData.eSnapType = SNP_MID_POINT;
        snapData.pt = (line.Pt(0) + line.Pt(1)) /2.0;
        lstSnap.push_back(snapData);


        VIEW_COMMON::E_SNAP_TYPE  eLine1SnapType;
        foreach(snapData, lstSnap)
        {
            if (snapData.pt.Distance(pt1) < m_pView->GetSnapRadius())
            {
                pt1         = snapData.pt;
                eLine1SnapType = snapData.eSnapType;
                lineNorm = line.NormalLine(pt1);
                pt2 = lineNorm.NearPoint(ptSel);
                bSnap = true;
                break;
            }
        }
    
        if (bSnap)
        {
            strToolTipText += _T("-");
            strToolTipText += GetSnapName(eLine1SnapType);
        }

        double dAngle;
        double dLength;
        POINT2D ptLength;


        bool bSnapAngle = false;

        //!< 長さによるスナップ点取得
        if (m_pView->GetLengthSnapPoint(&dAngle, &dLength,
                                        &ptLength,  
                                        pt1, pt2, bSnapAngle, true /*bSnapLength*/))
        {

            strToolTipText += _T("-");
            strToolTipText += GetSnapName(SNP_DISTANCE);
            strToolTipText += CUtil::StrFormat(_T(":%03f"), dLength);
            pt2 = ptLength;
        }
    }
    else
    {
        strToolTipText += _T("-");
        strToolTipText += GetSnapName(eType);
    }

    if (bBoth)
    {
        POINT2D pt3;
        pt3 = pt1 + pt1 - pt2;
        m_psEditingLine->SetPoint1(pt2);
        m_psEditingLine->SetPoint2(pt3);
    }
    else
    {
        m_psEditingLine->SetPoint1(pt1);
        m_psEditingLine->SetPoint2(pt2);
    }

    m_pView->SetToolTipText(strToolTipText.c_str());

}

/**
 *  マウスクリック動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval   なし
 *  @note
 */
bool CActionLine::SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    CPartsDef*   pCtrl  = m_pView->GetPartsDef();
    int                  iId  = m_pView->SearchPos(posMouse.ptSel);
    auto     pObj  = pCtrl->GetObjectById(iId);

    VIEW_MODE      eViewMode  = m_pView->GetViewMode();

    //!< ビュー->データ変換
    POINT2D pt2D;
    int iLayerId = m_pView->GetCurrentLayerId();
    m_pView->ConvScr2World(&pt2D, posMouse.ptSel, iLayerId);


    if (eAction != CViewAction::ACT_LBUTTON_UP) 
    {
        //左クリックと移動
        return false;
    }

    POINT2D ptMarker;
    if(_CheckMarker( &ptMarker, posMouse.ptSel, eAction))
    {
        pObj.reset();
        pt2D = ptMarker;
    }

    if (!m_psEditingLine)
    {
        //１点め未設定
        _SelFirstPoint(pt2D, pObj);
    }
    else
    {
        _SelSecondPoint(pt2D, pObj);
    }
    return false;
}


/**
 * @brief   一点目設定
 * @param   [in]     pt2D       マウスカーソル位置
 * @param   [in]     pObj        マウスの動作
 * @retval  true  
 * @note	
 */
bool CActionLine::_SelFirstPoint(POINT2D pt2D, std::shared_ptr<CDrawingObject> pObj)
{
    //選択オブジェクトリストには点を含めない
    CPartsDef*    pCtrl = m_pView->GetPartsDef();
    CUndoAction*            pUndo  = pCtrl->GetUndoAction();
    CDrawMarker*            pMarker= m_pView->GetMarker();
    DRAWING_TYPE            eType;


    bool bSelPoint = false;
    m_ptFirst = pt2D;
    POINT2D ptObj;
    if (!pObj)
    {
        m_pObjFirst.reset();
        bSelPoint = true;
    }
    else
    {   eType = pObj->GetType();
        switch(eType)
        {
        case DT_LINE:
        case DT_CIRCLE:
            m_pObjFirst = pObj;
            pCtrl->SelectDrawingObject(pObj->GetId());
            break;
        default:
            m_pObjFirst.reset();
            bSelPoint = true;
            break;
        }
        m_pView->SetMarker(pObj.get());
    }

    STD_ASSERT(m_psEditingLine == NULL);

    //if (bSelPoint)
    {
        //表示コメント変更
        m_pView->SetComment(GET_SYS_STR(STR_SEL_POINT2));

        //入力値初期化
        m_bFixAngle = false;
        m_bFixLength = false;
        m_dFixdAngle = 0.0;
        m_dFixdLength = 0.0;

        // 入力エリアにフォーカスを設定する
        m_pView->SetInputFocus();
        int iLayerId =  m_pView->GetCurrentLayerId();


        //m_psEditingLineの有無で2点めが選択できるか判定する
		auto pObj = THIS_APP->CreateDefaultObject(DT_LINE);
		auto defObj = std::dynamic_pointer_cast<CDrawingLine>(pObj);
		m_psEditingLine = defObj;
        m_psEditingLine->SetColor(DRAW_CONFIG->crImaginary);

		if (!THIS_APP->GetDefaultObjectPriorty(DT_LINE))
		{
			m_psEditingLine->SetLineType(m_pView->GetCurrentLayer()->iLineType);
			m_psEditingLine->SetLineWidth(m_pView->GetCurrentLayer()->iLineWidth);
		}
        m_psEditingLine->SetLayer(iLayerId);

        m_psEditingLine->SetPoint1(m_ptFirst);
        m_psEditingLine->SetPoint2(m_ptFirst);
        pCtrl->SetMouseOver(m_psEditingLine);
        pMarker->AddObject(m_psEditingLine);
        pMarker->HideObject(true);
        return true;
    }
    return false;
}

/**
 * @brief   二点目設定
 * @param   [in]     pt2D    マウスカーソル位置
 * @param   [in]     pObj    マウスが指している
 * @retval  true  
 * @note	
 */
void CActionLine::_SelSecondPoint(POINT2D pt2D, std::shared_ptr<CDrawingObject> pObj)
{
    CPartsDef*    pDef = m_pView->GetPartsDef();
    CDrawMarker*            pMarker= m_pView->GetMarker();

    if  ((m_pObjFirst)&&
         (pObj == m_pObjFirst))
    {
        m_pObjFirst = NULL;
        m_psEditingLine->SetPoint1(pt2D);
        m_psEditingLine->SetPoint2(pt2D);
        pDef->RelSelect();
        return;
    }


    m_psEditingLine->SetLayer(m_pView->GetCurrentLayerId());
	if (!THIS_APP->GetDefaultObjectPriorty(DT_LINE))
	{
		m_psEditingLine->SetColor(m_pView->GetCurrentLayer()->crDefault);
	}
	else
	{
		COLORREF cr = THIS_APP->GetDefaultObject(DT_LINE)->GetColor();
		m_psEditingLine->SetColor(cr);
	}


    pDef->RegisterObject(m_psEditingLine, true, false);
    m_psEditingLine = NULL; //所有権をpCtrlに移行

	if (m_psEditingLine2)
	{
		m_psEditingLine2->SetLayer(m_pView->GetCurrentLayerId());
		if (!THIS_APP->GetDefaultObjectPriorty(DT_LINE))
		{
			m_psEditingLine2->SetColor(m_pView->GetCurrentLayer()->crDefault);
		}
		else
		{
			COLORREF cr = THIS_APP->GetDefaultObject(DT_LINE)->GetColor();
			m_psEditingLine2->SetColor(cr);
		}


		pDef->RegisterObject(m_psEditingLine2, true, false);
		m_psEditingLine2 = NULL; //所有権をpCtrlに移行
	}

	pMarker->ClearObject();
    _ClearAction();
}

/**
 *  動作クリア
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CActionLine::_ClearAction()
{
    m_bMark = false;

    m_iClickCnt = 0;

    CDrawMarker*        pMarker = m_pView->GetMarker();
    CPartsDef*    pCtrl   = m_pView->GetPartsDef();

    pMarker->Clear();
    pCtrl->RelSelect();

    //pCtrl->DeleteSelectingObject();
    pCtrl->Redraw();

    m_psEditingLine.reset();
	m_psEditingLine2.reset();


    m_pObjFirst.reset();
    pCtrl->ClearMouseOver();
    pCtrl->ClearMouseEmphasis();
    pCtrl->ClearMouseConnectable();

    VIEW_MODE      eViewMode  = m_pView->GetViewMode();

    m_bFixAngle = false;
    m_bFixLength = false;

    SelAction(eViewMode);
}

//!< 入力値設定
bool CActionLine::InputValue(StdString strVal)
{

    //double dVal;
    //StdString strVal;
    //THIS_APP->GetScriptEngine()->ExecuteString(&dVal, strVal);

    bool bRet = false;

    bRet = _CalcValue( strVal);
    if (bRet)
    {
        _SetFixLengthAngle();
    }
    return bRet;
}


bool CActionLine::_CalcValue(StdString strVal)
{

    //入力モード確認
    //角度入力が出来るのは１点選択後 若しくは、 １点選択後に直線を選択

    // 入力値は 長さ,角度 
    m_bFixAngle = false;
    m_bFixLength = false;

    std::vector<StdString> lstInput;
    boost::algorithm::split( lstInput, strVal, boost::is_any_of(_T(",")));

    if (lstInput.size() == 0)
    {
        return false;
    }

    if (lstInput.size() >= 1)
    {
        if (lstInput[0].size() != 0)
        {
            double dLen;
            bool bRet;
            bRet = ExecuteString(&dLen, lstInput[0]);

            if (bRet)
            {
                m_bFixLength = true;
                m_dFixdLength = dLen;
            }
            else
            {
                return false;
            }
        }
    }

    if  (lstInput.size() >= 2)
    {
        if (lstInput[1].size() != 0)
        {
            double dAngle;
            bool bRet;
            bRet = ExecuteString(&dAngle, lstInput[1]);

            if (bRet)
            {
                m_bFixAngle = true;
                m_dFixdAngle = dAngle;
            }
            else
            {
                return false;
            }
        }
    }
    m_strOldInput = strVal;
    return true;
}


void CActionLine::_SetFixLengthAngle()
{
    if (m_bFixLength && m_bFixAngle && m_pView)
    {
        //TODO:UNDO
        CPartsDef*     pDef = m_pView->GetPartsDef();
        CUndoAction*   pUndo  = pDef->GetUndoAction();
        CDrawMarker*   pMarker= m_pView->GetMarker();

        if(m_psEditingLine)
        {
            POINT2D pt1 = m_psEditingLine->GetPoint1();
            POINT2D pt2;
            double  dAngle = DEG2RAD * m_dFixdAngle;
            pt2.dX = pt1.dX + m_dFixdLength * cos(  dAngle );
            pt2.dY = pt1.dY + m_dFixdLength * sin(  dAngle );

            m_psEditingLine->SetPoint2(pt2);

            m_psEditingLine->SetLayer(m_pView->GetCurrentLayerId());
            m_psEditingLine->SetColor(m_pView->GetCurrentLayer()->crDefault);

            pDef->RegisterObject(m_psEditingLine, true, false);
        }
        m_psEditingLine = NULL; //所有権をpCtrlに移行
        pMarker->ClearObject();
        _ClearAction();
    }
}

//*******************************
//             TEST
//*******************************
#ifdef _DEBUG


void TEST_CActionLine()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    CActionLine action;

    action.InputValue(_T("100"));
    STD_ASSERT(action.m_bFixLength);
    STD_ASSERT(!action.m_bFixAngle);
    STD_ASSERT_DBL_EQ(action.m_bFixLength, 100.0);

    action.InputValue(_T("110,20"));
    STD_ASSERT(action.m_bFixLength);
    STD_ASSERT(action.m_bFixAngle);
    STD_ASSERT_DBL_EQ(action.m_bFixLength, 110.0);
    STD_ASSERT_DBL_EQ(action.m_dFixdAngle, 20.0);

    action.InputValue(_T(",25"));
    STD_ASSERT(!action.m_bFixLength);
    STD_ASSERT(action.m_bFixAngle);
    STD_ASSERT_DBL_EQ(action.m_dFixdAngle, 25.0);

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));

}

#endif //_DEBUG



