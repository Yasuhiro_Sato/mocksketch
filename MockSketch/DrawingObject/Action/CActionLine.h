/**
 * @brief			CActionLineヘッダーファイル
 * @file			CActionLine.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#if !defined(_ACTION_LINE_H_)
#define _ACTION_LINE_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "View/VIEW_MODE.h"
#include "CViewAction.h"
#include "DrawingObject/Primitive/POINT2D.h"

class     CDrawingView;
class     CDrawingLine;

/**
 * @class   CActionLine
 * @brief                        
   
 */
class CActionLine: public CViewAction
{

public:
	//!< コンストラクタ
    CActionLine();

    //!< コンストラクタ
    CActionLine(CDrawingView* pView);
    
	//!< デストラクタ
    virtual ~CActionLine();


	//!< 選択解放時動作
	virtual void Cancel(VIEW_MODE eMode);

	//!< 選択解放時動作
	virtual bool SelAction(VIEW_MODE eMode, void* pParam = NULL);

	//!< マウスクリック動作
	virtual bool SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

    //!< マウス移動
	virtual bool MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

    //!< 入力値設定
    virtual bool InputValue(StdString strVal);

private:
    friend CActionLine;

    //!< 一点目設定
    bool _SelFirstPoint(POINT2D pt2D, std::shared_ptr<CDrawingObject> pObj);

    //!< 線分
    void _SelSecondPoint(POINT2D pt2D, std::shared_ptr<CDrawingObject> pObj);

    //!< 動作クリア
    void _ClearAction();

    bool _CalcValue(StdString strVal);

    void _SetFixLengthAngle();

 
    //!< マウス移動(直線を最初に選択した場合)
    void _MovePointSecond_Line( 
                                SNAP_DATA* pSnap,
                                MOUSE_MOVE_POS posMouse,
                                std::shared_ptr<CDrawingObject> pObj,
                                bool bBoth,
                                bool bIgnoreSnap);

    //!< マウス移動(円を最初に選択した場合)
    void _MovePointSecond_Circle(SNAP_DATA* pSnap,
                                MOUSE_MOVE_POS posMouse,
                                std::shared_ptr<CDrawingObject> pObj,
                                bool bBoth,
                                bool bIgnoreSnap);

    //!<  一点を始点とする直線への線分への垂線
    void _DrawNormalSegment(const LINE2D& line, 
                            const POINT2D& ptSel, 
                            bool bBoth,
                            VIEW_COMMON::E_SNAP_TYPE eType);

    bool _GetSnapAngleDistance(POINT2D* pEnfPoint,
        SNAP_DATA* pSnap,
        POINT2D ptFirst,
        double dFixdLength,
        double dFixdAngle,
        bool bFixLength,
        bool bFixAngle,
		double dOffsetLength,
        bool bIgnoreSnap);

    bool _SetFixLengthPos(POINT2D* pRet, POINT2D ptPos);

#ifdef _DEBUG
friend void TEST_CActionLine();
#endif //_DEBUG

private:
    // 初回選択位置
    POINT2D              m_ptFirst;

    // 初回選択オブジェクト
    std::shared_ptr<CDrawingObject>      m_pObjFirst;

    // 現在作成中の直線
    std::shared_ptr<CDrawingLine>    m_psEditingLine;
    std::shared_ptr<CDrawingLine>    m_psEditingLine2;

    // 補助点
    std::shared_ptr<CDrawingPoint>   m_psTmpEndpoint;

    // 補助直線
    std::shared_ptr<CDrawingLine>   m_psTmpLine;

    // 選択可能直線
    std::vector<std::shared_ptr<CDrawingLine>>  m_psSelectLines;

    //!< 前回入力文字
    StdString m_strOldInput;

    //!< 角度固定
    bool m_bFixAngle;

    //!< 固定角度
    double m_dFixdAngle;

    //!< 距離固定
    bool m_bFixLength;

    //!< 固定距離
    double m_dFixdLength;
};
#endif // !defined(_ACTION_POINT_H_)
