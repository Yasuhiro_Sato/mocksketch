/**
 * @brief			CActionMirror実装ファイル
 * @file			CActionMirror.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CActionMirror.h"

#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/View/CDrawingView.h"

#include "DrawingObject/CDrawingObject.h"
#include "DrawingObject/CDrawingPoint.h"
#include "DrawingObject/CDrawingLine.h"
#include "DrawingObject/CDrawingCircle.h"
#include "DrawingObject/CDrawingEllipse.h"
#include "DrawingObject/CDrawingParts.h"

#include "View/CLayer.h"
#include "View/CDrawMarker.h"
#include "View/CUndoAction.h"
#include "MockSketch.h"
#include "CProjectCtrl.h"
#include "Utility/CUtility.h"
#include "System/CSystem.h"
#include "SubWindow/TextInputDlg.h"
#include <math.h>

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 * コンストラクタ
 */
CActionMirror::CActionMirror():
CActionCommand()
{
    m_lMouseOver =   SEL_FIG;
    m_eViewMode = VIEW_NONE;
}

/**
 * コンストラクタ
 */
CActionMirror::CActionMirror(CDrawingView* pView):
CActionCommand(pView)
{
    m_lMouseOver =   SEL_FIG;
    m_pView = pView;
    m_eViewMode = VIEW_NONE;
}
    
/**
 * デストラクタ
 */
CActionMirror::~CActionMirror()
{
    
}

/**
 *  @brief   解放時動作
 *  @param   [in]    eMode 解放時点でのモード
 *  @retval   なし
 *  @note
 */
void CActionMirror::Cancel(VIEW_MODE eMode) 
{
    CancelSelectRect();    
}

/**
 *  @brief   選択時動作
 *  @param   [in]    eMode 選択時点でのモード
 *  @retval   なし
 *  @note
 */
bool CActionMirror::SelAction(VIEW_MODE eViewMode, void* pParam) 
{
    STD_ASSERT(m_pView != NULL);

    m_eViewMode = eViewMode;
    long lSize = m_pView->GetPartsDef()->SelectNum();
    if (lSize == 0)
    {
        //図形を選択してください
        m_pView->SetComment(GET_SYS_STR(STR_SEL_OBJECT));
        if (m_eViewMode != VIEW_SELECT)
        {
            //内部選択モード
            m_eViewMode = VIEW_SELECT;
        }
        return  true;
    }



    m_pView->SetComment(GET_SYS_STR(STR_SEL_MIRROR_LINE));
    return true;    

}

/**
 *  @brief   マウスクリック動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionMirror::SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    STD_ASSERT(m_pView != NULL);

    POINT2D ptBase;
    long lSize = 0;
    try
    {
        switch (m_eViewMode)
        {
            case VIEW_SELECT:
                //選択動作
                SelectAction( posMouse, eAction);

                //既に選択中の図形はあるか？
                lSize = m_pView->GetPartsDef()->SelectNum();
                if (lSize != 0)
                {
                    m_iClickCnt = 0;
                    m_lMouseOver =   SEL_LINE;
                    m_pView->SetComment(GET_SYS_STR(STR_SEL_MIRROR_LINE));
                    m_eViewMode = VIEW_MIRROR;
                }
                break;

            case VIEW_MIRROR:
                SelLineMirror( posMouse, eAction, !m_pView->GetCutMode());

                break;

        }
    }
    catch(MockException &e)
    {
        e.DispMessageBox();
    }

    return false;
}


/**
 *  @brief   鏡像時 マウスクリック動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags  仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionMirror::SelLineMirror(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction, bool bCopy)
{
    bool bRet;
    POINT2D ptClick;
    CPartsDef*   pCtrl  = m_pView->GetPartsDef();
    int                  iId  = m_pView->SearchPos(posMouse.ptSel);
    auto     pObj  = pCtrl->GetObjectById(iId);
    int iLayerId = m_pView->GetCurrentLayerId();

    if (pObj == NULL)
    {
        return false;
    }


    auto pLine = std::dynamic_pointer_cast<CDrawingLine>(pObj);

    if (pLine == NULL)
    {
        return false;
    }

    bRet = SetMirror(*pLine->GetLineInstance(), bCopy);

    ClearAction();

    //選択状態へ移行
    m_pView->SetViewMode(VIEW_SELECT);

    //m_eViewMode = VIEW_SELECT;
    //m_pView->SetComment(GET_SYS_STR(STR_SEL_OBJECT));

    return bRet;
}


/**
 *  動作クリア
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CActionMirror::ClearAction(bool bReleaseSelect)
{
    CViewAction::ClearAction(bReleaseSelect);
    CPartsDef*    pDef   = m_pView->GetPartsDef();

    m_lstOffset.clear();

    VIEW_MODE      eViewMode  = m_pView->GetViewMode();

    _DeleteTmpObjects();

    pDef->Redraw(m_pView);
    SelAction(eViewMode);
}


void CActionMirror::_DeleteTmpObjects()
{
    m_lstTmpObjects.clear();
}

/**
 *  マウス移動
 *  @param   [in]    pObj   選択オブジェクト
 *  @param   [in]    eAction   選択オブジェクト
 *  @param   [in]    nFlags   仮想キー 
 *  @retval   なし
 *  @note
 */
bool CActionMirror::MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction) 
{ 

    CPartsDef*   pDef  = m_pView->GetPartsDef();
    if (!pDef)
    {
        return false;
    }

   if (m_eViewMode == VIEW_SELECT)
   {
        if (m_eDrag == E_DS_AREA)
        {
           CActionCommand:: SelectAction( posMouse, eAction);
        }
        else
        {
            CViewAction::MovePoint( posMouse, eAction);
        }

        CPartsDef* pDef = m_pView->GetPartsDef();

        if (pDef)
        {
            pDef->DrawDragging(m_pView);
        }
        return false;
   }

    CDrawMarker* pMarker= m_pView->GetMarker();

    if(m_lstTmpObjects.empty())
    {
        auto pList = pDef->GetSelectInstance();

        if (!pList->empty())
        {
            m_iClickCnt = 0;
            m_lMouseOver =   SEL_LINE;
            _DeleteTmpObjects();
            pMarker->Clear();

            //一時表示用の図形を作成
            CreateTempMovingObjects(&m_lstOffset,
                &m_lstTmpObjects,
                pDef,
                1);
         }
    }

    pDef->ClearMouseOver();
    pDef->ClearMouseEmphasis();
    pDef->ClearMouseConnectable();

    auto pObj = SearchMouseOver(posMouse);
    auto pLine = std::dynamic_pointer_cast<CDrawingLine>(pObj);

    if (!pLine)
    {
        pDef->DrawDragging(m_pView);
        return false;
    }

    pMarker->HideObject(false);

    int iCnt = 0;
    for(auto pObj : m_lstTmpObjects)
    {
        m_lstOffset[iCnt]->Mirror(*pLine->GetLineInstance()) ;
        pObj->OffsetMatrix(m_lstOffset[iCnt].get());
        pDef->SetMouseOver(pObj);
        iCnt++;
    }

    pDef->DrawDragging(m_pView);
    return true;
}
        
/**
 *  鏡像
 *  @param   [in] line     対称線
 *  @param   [in] bCopy    true:Copy false:Move
 *  @retval  true 成功
 *  @note    
 */
bool CActionMirror::SetMirror(LINE2D& line, bool bCopy)
{
    CPartsDef*   pDef  = m_pView->GetPartsDef();
    auto pList = pDef->GetSelectInstance();
    CUndoAction* pUndo  = pDef->GetUndoAction();
    // グループ化した際、UNDO、移動のタイミングの問題が発生する場合があるため
    // 変更の影響があるオブジェクトを覚えておいてあとでまとめて処理を行う
    // UpdateConnection→
    //      CDrawingObject::UpdateNodePlugData
    //      CDrawingObject::UpdateNodeSocketDataで
    //pDef->GetListChangeByConnectionの内容が更新される
    //
    pDef->GetListChangeByConnection()->clear();

    pDef->SetLockUpdateNodeData(true);
    //---------------------
    if (!bCopy)
    {
        for(auto wObj: *pList)
        {
            auto pObj = wObj.lock();
            if (!pObj)
            {
                continue;
            }
            pUndo->AddStart(UD_CHG, pDef, pObj.get());
            pObj->Mirror( line);
            pUndo->AddEnd(pObj);
        }
        pDef->SetLockUpdateNodeData(false);
    }
    else
    {
        std::map<int, int> mapConvId;
        std::vector<int> lstConnection;
        mapConvId[-1] = -1;

        for(auto wObj: *pList)
        {
            auto pObj = wObj.lock();
            if (!pObj)
            {
                continue;
            }
            auto pNewObj = CDrawingObject::CloneShared(pObj.get());

            pNewObj->SetSelect(false);
            pNewObj->Mirror( line);
            pDef->RegisterObject(pNewObj, true, true);

            mapConvId[pObj->GetId()] = pNewObj->GetId();
            if (pObj->GetNodeDataNum() > 0)
            {
                lstConnection.push_back(pNewObj->GetId());
            }
            else
            {
                pUndo->Add(UD_ADD, pDef, NULL, pNewObj);
            }
        }

        //  接続ID更新
        bool bKeepUnfindId = false;
        pDef->SetLockUpdateNodeData(false);
        pDef->UpdateConnectionAndPosition( pUndo, &mapConvId, &lstConnection, bKeepUnfindId);
    }

    //---------------------
    // 接続しているオブジェクトの変更
    //---------------------
    std::set<int> lstObj;
    UpdateAllNodePositionAndRelation(
        pDef,
        pDef->GetListChangeByConnection(),
        &lstObj, pUndo);

    pUndo->Push();
    pDef->Redraw(m_pView);

    //!< 変更を通知
    //::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_VIEW_UPDATE_OBJECTS, (WPARAM)pParts, 0);
    return true;
}

