/**
 * @brief			CActionMirrorヘッダーファイル
 * @file			CActionMirror.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef _ACTION_MIRROR_H__
#define _ACTION_MIRROR_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CActionCommand.h"
#include "DrawingObject/Primitive/POINT2D.h"

class     CDrawingView;

/**
 * @class   CActionMirror
 * @brief                        
   
 */
class CActionMirror: public CActionCommand
{
protected:
    std::vector< std::shared_ptr<CDrawingObject> > m_lstTmpObjects;

    std::vector<std::unique_ptr<MAT2D>> m_lstOffset;

public:
	//!< コンストラクタ
    CActionMirror();

    //!< コンストラクタ
    CActionMirror(CDrawingView* pView);
    
	//!< デストラクタ
    virtual ~CActionMirror();

	//!< 選択解放時動作
    virtual void Cancel(VIEW_MODE eMode);

	//!< 選択解放時動作
	virtual bool SelAction(VIEW_MODE eMode, void* pParam = NULL);

	//!< マウスク移動動作
    bool MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

    //!< マウスクリック動作
	virtual bool SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

    //!<鏡像時 マウスクリック動作
    bool SelLineMirror(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction, bool bCopy);


protected:
    //!< 動作クリア
    void ClearAction(bool bReleaseSelect = true) override;

    void _DeleteTmpObjects();

    //!< 鏡像
    bool SetMirror(LINE2D& line, bool bCopy);

protected:

};
#endif // _ACTION_COMMAND_H__
