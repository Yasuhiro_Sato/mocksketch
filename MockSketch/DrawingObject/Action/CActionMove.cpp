/**
 * @brief			CActionMove実装ファイル
 * @file			CActionMove.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MockSketch.h"
#include "./CActionMove.h"

#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/View/CDrawingView.h"

#include "DrawingObject/CDrawingObject.h"
#include "DrawingObject/CDrawingPoint.h"
#include "DrawingObject/CDrawingLine.h"
#include "DrawingObject/CDrawingCircle.h"
#include "DrawingObject/CDrawingEllipse.h"
#include "DrawingObject/CDrawingParts.h"
#include "DrawingObject/CNodeData.h"
#include "DrawingObject/CDrawingGroup.h"

#include "View/CLayer.h"
#include "View/CDrawMarker.h"
#include "View/CUndoAction.h"
#include "MockSketch.h"
#include "CProjectCtrl.h"
#include "Utility/CUtility.h"
#include "System/CSystem.h"
#include "SubWindow/TextInputDlg.h"
#include "Script/CScriptEngine.h"
#include <math.h>

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 * コンストラクタ
 */
CActionMove::CActionMove():
CActionCommand()
{
    m_bFixAngle = false;
    m_bFixLength = false;
    m_dFixdAngle = 0.0;
    m_dFixdLength = 0.0;


    m_lMouseOver =   SEL_ALL;
    m_eViewMode = VIEW_NONE;

    m_psTmpLine = std::make_shared<CDrawingLine>();
    m_psTmpLine->SetColor(DRAW_CONFIG->crAdditional);
    m_psTmpLine->SetLineType(PS_DOT);

}

/**
 * コンストラクタ
 */
CActionMove::CActionMove(CDrawingView* pView):
CActionCommand(pView)
{
    m_bFixAngle = false;
    m_bFixLength = false;
    m_dFixdAngle = 0.0;
    m_dFixdLength = 0.0;

    m_lMouseOver =   SEL_ALL;
    m_eViewMode = VIEW_NONE;

    m_psTmpLine = std::make_shared<CDrawingLine>();
    m_psTmpLine->SetColor(DRAW_CONFIG->crAdditional);
    m_psTmpLine->SetLineType(PS_DOT);
}
    
/**
 * デストラクタ
 */
CActionMove::~CActionMove()
{
    _DeleteTmpObjects();
}

/**
 *  @brief   解放時動作
 *  @param   [in]    eMode 解放時点でのモード
 *  @retval   なし
 *  @note
 */
void CActionMove::Cancel(VIEW_MODE eMode) 
{
    _ClearAction();    
}


/**
 *  動作クリア
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CActionMove::_ClearAction()
{
    CPartsDef*   pDef  = m_pView->GetPartsDef();
    CDrawMarker* pMarker= m_pView->GetMarker();
 
    pMarker->Clear();
    pMarker->ClearObject();
    pDef->RelSelect();
    pDef->ClearMouseOver();
    pDef->ClearMouseEmphasis();
    pDef->ClearMouseConnectable();
    _DeleteTmpObjects();
    m_lstOffset.clear();

    VIEW_MODE      eViewMode  = m_pView->GetViewMode();

    m_bFixAngle = false;
    m_bFixLength = false;
    m_iTimes = 1;

    ClearAction();

    pDef->Redraw(m_pView);
    SelAction(eViewMode);
}

/**
 *  @brief   選択時動作
 *  @param   [in]    eMode 選択時点でのモード
 *  @retval   なし
 *  @note
 */
bool CActionMove::SelAction(VIEW_MODE eViewMode, void* pParam) 
{
    STD_ASSERT(m_pView != NULL);

    //既に選択中の図形はあるか？
    long lSize = m_pView->GetPartsDef()->SelectNum();
    m_eViewModeOrg = eViewMode;
    m_eViewMode    = eViewMode;
    if (lSize == 0)
    {
        //図形を選択してください
        m_pView->SetComment(GET_SYS_STR(STR_SEL_OBJECT));
        if (m_eViewMode != VIEW_SELECT)
        {
            m_eViewMode = VIEW_SELECT;
        }
        return true;
    }

    m_bFixAngle = false;
    m_bFixLength = false;
    m_dFixdAngle = 0.0;
    m_dFixdLength = 0.0;

    m_iClickCnt = 0;

    m_lMouseOver =   SEL_ALL;


    m_pView->SetComment(GET_SYS_STR(STR_SEL_MOVE_POINT1));
    return true;
}

/**
 *  @brief   マウスクリック動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionMove::SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    STD_ASSERT(m_pView != NULL);

    //既に選択中の図形はあるか？
    POINT2D ptBase;

    try
    {
        switch (m_eViewMode)
        {
            case VIEW_SELECT:
                //TODO: 1つのみ または 矩形選択のみになるため、削除するべきか検討する。
                //選択動作
                CActionCommand::SelectAction( posMouse, eAction);

                if (eAction == ACT_LBUTTON_UP)
                {
                    size_t lSize = m_pView->GetPartsDef()->SelectNum();
                    if (lSize != 0)
                    {
                        m_pView->SetComment(GET_SYS_STR(STR_SEL_MOVE_POINT1));
                        m_eViewMode = m_eViewModeOrg;
                    }
                }
                break;

            case VIEW_MOVE:
            {
                bool bCopy = !m_pView->GetCutMode();
                SelPointMove( posMouse, eAction, bCopy);

            }
                break;

            case VIEW_COPY:
                SelPointMove( posMouse, eAction, true );

                break;
        }
    }
    catch(MockException &e)
    {
        e.DispMessageBox();
    }

    return false;
}

bool CActionMove::_IsCopyMode() const
{
    if (m_eViewMode == VIEW_COPY)
    {
        return true;
    }
    else if(m_eViewMode == VIEW_COPY)
    {
         if(!m_pView->GetCutMode())
         {
            return true;
         }
    }
    return false; 
}

/**
 *  マウス移動
 *  @param   [in]    pObj   選択オブジェクト
 *  @param   [in]    eAction   選択オブジェクト
 *  @param   [in]    nFlags   仮想キー 
 *  @retval   なし
 *  @note
 */
bool CActionMove::MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction) 
{ 

    CPartsDef*   pDef  = m_pView->GetPartsDef();

   if (m_eViewMode == VIEW_SELECT)
   {
       if (m_eDrag == E_DS_AREA)
       {
           CActionCommand::_MovePointArea(posMouse, eAction);
       }
        else
        {
            CViewAction::MovePoint( posMouse, eAction);
        }

        CPartsDef* pDef = m_pView->GetPartsDef();

        if (pDef)
        {
            pDef->DrawDragging(m_pView);
        }
        return false;
   }

    POINT ptSel = posMouse.ptSel;
    UINT nFlags = posMouse.nFlags;

    //!< スナップ点取得
    bool bSnap = true;

   
    DWORD dwSnapType = DRAW_CONFIG->GetSnapType();

    CDrawMarker* pMarker= m_pView->GetMarker();

    SNAP_DATA snap;
    bSnap = m_pView->GetSnapPoint(&snap,
                      dwSnapType,
                      ptSel);

    auto pObj1 = pDef->GetObjectById(snap.iObject1);
    auto pObj2 = pDef->GetObjectById(snap.iObject2);

    POINT ptOffset;
    ptOffset.x = ptOffset.y = 0;
    CDrawMarker::MARKER_DATA marker;
    marker.ptOffset = ptOffset;


    bool bIgnoreSnap = false;
    if (nFlags & MK_SHIFT)
    {
        //スナップ無視
        bIgnoreSnap = true;
        bSnap = false;
    }

    m_pView->SetDrawingMode(DRAW_SEL);
    pMarker->Clear();
    pDef->ClearMouseOver();
    pDef->ClearMouseEmphasis();
    pDef->ClearMouseConnectable();

    int iMarkerId = -1;

    //一点目
    if (m_iClickCnt == 0)
    {
        if ((pObj1 != NULL ) &&
            (pObj2 != NULL ))
        {
            //交点
            pDef->SetMouseEmphasis(pObj1);
            pDef->SetMouseEmphasis(pObj2);
            marker.eType = CDrawMarker::MARKER_CROSS;
            marker.eSnap = VIEW_COMMON::SNP_CROSS_POINT;
            pMarker->AddMarker(marker, snap.pt);
        }
        else if ((pObj1 != NULL ) &&
            (pObj2 == NULL ))
        {
            m_pView->SetMarker(pObj1.get(), false /*bIgnoreOffLineObject*/);
            pDef->SetMouseEmphasis(pObj1);
        }
        
        if (snap.eSnapType == VIEW_COMMON::SNP_NONE)
        {
            //任意点
            CDrawMarker*pMarker = m_pView->GetMarker();
            marker.eType = CDrawMarker::MARKER_CROSS;
            marker.eSnap = VIEW_COMMON::SNP_CROSS_POINT;
            pMarker->AddMarker(marker, snap.ptOrg);
        }

        iMarkerId = pMarker->MouseMove(posMouse.ptSel);


        SetSnapTooltip(snap.eSnapType, 0, 0);
    }
    else
    {
        //------------------
        //１点目選択済み
        //------------------

        //入力値の取得
        StdString sVal;
        sVal = m_pView->GetInputValue();
        if(sVal != m_strOldInput)
        {
            bool bRet;
            bRet = _CalcValue(sVal);
        }

        //入力値がある場合
        double dAngle = 0.0;
        double dLength = 0.0;
        POINT2D ptEndPos; // 追加する直線の終端


            
        bool bInputVal = false;
        if (m_bFixLength && !m_bFixAngle)
        {
            m_pView->GetFixLengthSnapPoint(&dAngle,
                                        &ptEndPos,  
                                    m_dFixdLength,
                                    m_ptFirst,  
                                    snap.pt,
                                    !bIgnoreSnap);

            dLength = m_dFixdLength;
            bInputVal = true;
        }
        else if (m_bFixAngle && !m_bFixLength)
        {
            m_pView->GetFixAngleSnapPoint(&dLength,
                                        &ptEndPos,  
                                    m_dFixdAngle,
                                    m_ptFirst,  
                                    snap.pt,
                                    !bIgnoreSnap);

            dAngle = m_dFixdAngle;
            bInputVal = true;
        }
        else if (m_bFixAngle && m_bFixLength)
        {
            //角度、距離固定
            dAngle =  m_dFixdAngle;
            dLength = m_dFixdLength;
            ptEndPos.dX = m_ptFirst.dX + m_dFixdLength * cos(  dAngle * DEG2RAD);
            ptEndPos.dY = m_ptFirst.dY + m_dFixdLength * sin(  dAngle * DEG2RAD);
            bInputVal = true;
            //クリックで確定とする
            pObj1 = NULL;
        }

 
        //角度固定の場合は交点のみ
        POINT2D ptAngle;
        if (m_bFixAngle)
        {
            ptAngle.dX = m_ptFirst.dX + cos(  m_dFixdAngle * DEG2RAD);
            ptAngle.dY = m_ptFirst.dY + sin(  m_dFixdAngle * DEG2RAD);
        }

        if (bIgnoreSnap)
        {
            pObj1 = NULL;
        }
 
        if (pObj1)
        {
            DRAWING_TYPE typeOnMouse = pObj1->GetType();
            pDef->SetMouseEmphasis(pObj1);


            if (!m_bFixAngle)
            {
                //角度固定の場合は交点のみ
                bool bIgnoreOffLineObject = true;
                m_pView->SetMarker(pObj1.get(), bIgnoreOffLineObject);
                if (pObj2)
                {
                    pDef->SetMouseEmphasis(pObj2);
                    marker.eType = CDrawMarker::MARKER_CROSS;
                    marker.eSnap = VIEW_COMMON::SNP_CROSS_POINT;
                    pMarker->AddMarker(marker, snap.pt);
                }
            }

        }

        int iMarkerId;
        iMarkerId = pMarker->MouseMove(posMouse.ptSel);

        if (iMarkerId != -1)
        {
            CDrawMarker::MARKER_DATA* pData;
            pData = pMarker->GetMarker( iMarkerId );
            snap.pt = pData->ptObject;
            snap.eSnapType = marker.eSnap;
            bSnap = true;
        }
        else
        {
            snap.eSnapType = VIEW_COMMON::SNP_ANGLE;
            if (!bInputVal)
            {
                m_pView->GetLengthSnapPoint(&dAngle,
                                            &dLength,
                                            &ptEndPos,  
                                            m_ptFirst,  
                                            snap.pt,
                                            true,
                                            true);
                snap.pt = ptEndPos;

            }
        }


        if (m_bFixLength)
        {
            double dLength = snap.pt.Distance(m_ptFirst);
            POINT2D ptRet = m_ptFirst;
            if (fabs(dLength) > NEAR_ZERO)
            {
                double dScl = m_dFixdLength / dLength;
                ptRet.dX = m_ptFirst.dX + dScl * (snap.pt.dX - m_ptFirst.dX);
                ptRet.dY = m_ptFirst.dY + dScl * (snap.pt.dY - m_ptFirst.dY);
                snap.pt = ptRet;
            }
        }

        POINT2D ptDiff = snap.pt - m_ptFirst;

        m_psTmpLine->SetPoint1(m_ptFirst);
        m_psTmpLine->SetPoint2(snap.pt);
        m_pView->GetMarker()->AddObject(m_psTmpLine);
        m_ptLast = snap.pt;
 
        if (snap.eSnapType == VIEW_COMMON::SNP_ANGLE)
        {

            SetSnapTooltip(VIEW_COMMON::SNP_ANGLE, 
                                        dLength,
                                        dAngle);
        }
        else
        {
            SetSnapTooltip(snap.eSnapType, 0, 0);
        }

        pMarker->HideObject(false);


		//----------
		// 移動処理
		//----------
		int iCnt = 0;
		pDef->SetLockUpdateNodeData(true);
		pDef->GetListChangeByConnection()->clear();

		if (m_iTimes != m_lstTmpObjects.size())
		{
DB_PRINT(_T("CActionMove::MovePoint m_iTimes:%d, m_lstTmpObjects:%d \n"), m_iTimes, m_lstTmpObjects.size());
		}

        for(auto& pObj: m_lstTmpObjects)
        {
            POINT2D ptDiff;
            ptDiff = (snap.pt - m_ptFirst) * double(iCnt + 1);
            m_lstOffset[iCnt]->SetAffinePos(ptDiff);
            pObj->OffsetMatrix(m_lstOffset[iCnt].get());
            pDef->SetMouseOver(pObj);
            iCnt++;
        }

		if (m_eViewMode != VIEW_COPY)
		{
			pDef->SetLockUpdateNodeData(false);

			//全ノード位置の更新と関連オブジェクト、その関係の更新
			std::set<int> lstObj;
			CViewAction::UpdateAllNodePositionAndRelation(pDef,
				pDef->GetListChangeByConnection(),
				&lstObj,
				NULL);
#ifdef _DEBUG
			auto pConnection = pDef->GetListChangeByConnection();
			DB_PRINT(_T("CActionMove::MovePoint %d--\n"), pConnection->size());

#endif

			auto pList = pDef->GetConnectingTmpObjectList();
			for (auto pRefObj : *pList)
			{
				pMarker->AddObject(pRefObj);
				pDef->SetMouseEmphasis(pRefObj);
			}
		}
		//----------
	}

    pDef->DrawDragging(m_pView);
    return true;
}

void CActionMove::_DeleteTmpObjects()
{
    m_lstTmpObjects.clear();
	CPartsDef* pDef = m_pView->GetPartsDef();

	pDef->GetTmpGroupObjectList()->clear();


	pDef->GetListChangeByConnection()->clear();
	pDef->GetConnectingTmpObjectList()->clear();

}

/**
 *  @brief   移動・コピー時 マウスクリック動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags  仮想キー         
 *  @param   [in]    bCopy   true コピー false 移動 
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionMove::SelPointMove(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction, bool bCopy)
{

    //ここに入ってきた時点で図形は選択済み
    if (eAction != CViewAction::ACT_LBUTTON_UP)
    {
        return false;
    }

    CPartsDef*   pDef  = m_pView->GetPartsDef();
    CDrawMarker* pMarker= m_pView->GetMarker();

    POINT2D pt2D;
    int iLayerId = m_pView->GetCurrentLayerId();
    m_pView->ConvScr2World(&pt2D, posMouse.ptSel, iLayerId);
    
    POINT2D ptMarker;
    if(_CheckMarker( &ptMarker, posMouse.ptSel, eAction))
    {
        pt2D = ptMarker;
    }
    
    pMarker->Clear();
    pMarker->ClearObject();
    pDef->ClearMouseOver();
    pDef->ClearMouseEmphasis();
    pDef->ClearMouseConnectable();

    //1点目を選択
    if(m_iClickCnt == 0)
    {
		_DeleteTmpObjects();
		//入力値初期化
        m_bFixAngle = false;
        m_bFixLength = false;
        m_dFixdAngle = 0.0;
        m_dFixdLength = 0.0;

        // 入力エリアにフォーカスを設定する
        m_pView->SetInputFocus();

        //1点目を選択
        m_ptFirst = pt2D;
        m_iClickCnt++;
        //移動先の点を選択してください
        m_pView->SetComment(GET_SYS_STR(STR_SEL_MOVE_POINT2));

        auto pList = pDef->GetSelectInstance();


        m_bStretch = DRAW_CONFIG->GetStretchMode();


        if (bCopy)
        {
            pDef->SetLockUpdateNodeData(true);
			pDef->ClearConnectionToSelect();
			auto pTmpConnectList = pDef->GetConnectingTmpObjectList();
			pTmpConnectList->clear();
		}

        bool bCopyNodeConnection = !bCopy;

		//一時移動オブジェクトの作成
		// 接続によって影響を受けるものは GetConnectingTmpObjectListにコピーする（移動時のみ）
		CViewAction::CreateTempMovingObjects(&m_lstOffset,
                                &m_lstTmpObjects,
                                pDef,
                                m_iTimes,
                                bCopyNodeConnection   
                                );

		if (!m_lstTmpObjects.empty())
		{
			pDef->GetTmpGroupObjectList()->push_back(m_lstTmpObjects[0]);
		}

    }
    else
    {
        //2点目を選択
        SetMove(m_ptFirst, m_ptLast, m_iTimes, bCopy);
        //選択へ移行
        ClearAction();

        //選択状態へ移行
        m_pView->SetViewMode(VIEW_SELECT);
    }
    return true;
}

/**
 *  @brief   移動、コピー数値入力
 *  @param   [in] lstVal  数値列
 *  @param   [in] bCopy   true 数値列
 *  @retval  false エラー発生
 *  @note
 */
bool CActionMove::SelInputMove( std::vector<double>& lstVal, bool bCopy )
{
    bool bRet = false;

    int iTimes = 1;
    if (lstVal.size() < 2)
    {
        return false;
    }

    if (lstVal.size() >= 3)
    {
        iTimes = int(lstVal[2]);
        if (iTimes < 1)
        {
            iTimes = 1;
        }
    }

    POINT2D ptFirst ( 0.0, 0.0);
    POINT2D ptSecond(lstVal[0], lstVal[1]);

    bRet = SetMove(ptFirst, ptSecond, iTimes, bCopy);

    ClearAction();
    m_eViewMode =  VIEW_SELECT;
    return bRet;
}

/**
 *  移動
 *  @param   [in] ptFirst  移動元座標
 *  @param   [in] ptSecond 移動先座標
 *  @param   [in] iTimes   複写回数
 *  @param   [in] bCopy    true:Copy false:Move
 *  @retval  true 成功
 *  @note
 */
bool CActionMove::SetMove(POINT2D ptFirst, POINT2D ptSecond, int iTimes, bool bCopy)
{
    CPartsDef*   pDef = m_pView->GetPartsDef();
    CUndoAction* pUndo  = pDef->GetUndoAction();
    /*
        接続線について
        接続線を含めた場合以下の３つの場合について考える必要がある
        ・被接続オブジェクトのみ
        ・被接続オブジェクト片側と接続線
        ・被接続オブジェクト両側と接続線

        １．移動時
            ・すべてのオブジェクトを移動する。  この時点でのUpdateNodeDataはSetLockUpdateNodeData
               で動作させない（すべてのオブジェクトの移動が済むまでは接続位置の関係が正常に判断できないため）
            ・接続点に対して接続調査を行う

        ２．複写時
            ・すべてのオブジェクトを複写する。
                   コビー元のオブジェクトID と 新オブジェクトのIDのMapを作成する



    */
    //---------------------
    POINT2D ptDiff= (ptSecond - ptFirst);

#ifdef _DEBUG
	auto pConnection = pDef->GetListChangeByConnection();
	DB_PRINT(_T("CActionMove::SetMove 1 %d--\n"), pConnection->size());

#endif

	MAT2D mat;
	mat.Move(ptDiff);


	if (m_lstTmpObjects.empty())
	{
		return false;
	}

	//コピー用に複数オブジェクトがあるがここではIDのみを使用すため先頭のみで良い
	for (auto& pObj : m_lstTmpObjects)
	{
		pObj->OffsetMatrix(NULL);
	}
	m_lstTmpObjects.resize(1);

	auto pObj = m_lstTmpObjects[0];

	bool bRet;
	bRet = CViewAction::ApplyMatrix(pDef, pObj, mat, iTimes, bCopy);

	_DeleteTmpObjects();
	pDef->Redraw(m_pView);
    return bRet;
}


/**
 *  @brief   入力値設定
 *  @param   [in]    strVal 入力値
 *  @retval  false エラー発生
 *  @note    必要であればエラー表示を行う
 *  @note    エラー発生で入力コンボボックスは選択状態になる      
 *  @note    エラーがない場合はコンボボックスに値が登録される
 */
bool CActionMove::InputValue(StdString strVal)
{
   bool bRet = false;

    bRet = _CalcValue( strVal);

    if (m_bFixLength && m_bFixAngle && m_pView)
    {
        POINT2D ptFirst ( 0.0, 0.0);
        POINT2D ptSecond;
        double  dAngle = DEG2RAD * m_dFixdAngle;
        ptSecond.dX = m_dFixdLength * cos(  dAngle );
        ptSecond.dY = m_dFixdLength * sin(  dAngle );

        bool bCopy = false;
        int iTimes = m_iTimes;
        switch (m_eViewMode)
        {
        case VIEW_COPY: {bCopy = true; break;}
        case VIEW_MOVE: {bCopy = false; iTimes = 1;break;}
        }

        bRet = SetMove(ptFirst, ptSecond, iTimes, bCopy);

        ClearAction();
        m_eViewMode =  VIEW_SELECT;

    }
    return bRet;
}

bool CActionMove::_CalcValue(const StdString& sVal)
{

    //入力モード確認
    //角度入力が出来るのは１点選択後 若しくは、 １点選択後に直線を選択

    // 入力値は 長さ[,角度[,回数]] 
    m_bFixAngle = false;
    m_bFixLength = false;

    std::vector<StdString> lstInput;
    boost::algorithm::split( lstInput, sVal, boost::is_any_of(_T(",")));
    m_iTimes = 1;

    if (lstInput.size() == 0)
    {
        return false;
    }

    if (lstInput.size() >= 1)
    {
        if (lstInput[0].size() != 0)
        {
            double dLen;
            bool bRet;
            bRet = ExecuteString(&dLen, lstInput[0]);

            if (bRet)
            {
                m_bFixLength = true;
                m_dFixdLength = dLen;
            }
            else
            {
                return false;
            }
        }
    }

    if  (lstInput.size() >= 2)
    {
        if (lstInput[1].size() != 0)
        {
            double dAngle;
            bool bRet;
            bRet = ExecuteString(&dAngle, lstInput[1]);

            if (bRet)
            {
                m_bFixAngle = true;
                m_dFixdAngle = dAngle;
            }
            else
            {
                return false;
            }
        }
    }

    if  (lstInput.size() >= 3)
    {
        if (lstInput[2].size() != 0)
        {
            double dTimes;
            bool bRet;
            bRet = ExecuteString(&dTimes, lstInput[2]);

            if (bRet)
            {
                m_iTimes = int (dTimes);
                if (m_iTimes < 1)
                {
                    m_iTimes = 1;
                }
            }
            else
            {
                return false;
            }
        }
    }
    m_strOldInput = sVal;
    return true;
}

void CActionMove::Redraw() const
{
    if( (m_eDrag == E_DS_AREA) && 
        (m_eViewMode == VIEW_SELECT))
    {
        CRect rect(m_ptSelStart.x, m_ptSelStart.y, m_ptSelLast.x, m_ptSelLast.y);
        rect.NormalizeRect();
        DrawFocusRect(m_pView->GetViewDc(), rect);
        DrawFocusRect(m_pView->GetSelDc(), rect);
 //DB_PRINT(_T("Redraw(%d,%d)-(%d,%d)\n"), m_ptSelStart.x, m_ptSelStart.y,
 //                                         m_ptSelLast.x, m_ptSelLast.y);
   }

}
