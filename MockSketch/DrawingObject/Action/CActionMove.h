/**
 * @brief			CActionMoveヘッダーファイル
 * @file			CActionMove.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef _ACTION_MOVE_H__
#define _ACTION_MOVE_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CActionCommand.h"
#include "DrawingObject/Primitive/POINT2D.h"

class     CDrawingView;

/**
 * @class   CActionMove
 * @brief                        
   
 */
class CActionMove: public CActionCommand
{
     
protected:
    POINT2D m_ptFirst;

    POINT2D m_ptLast;

    VIEW_MODE m_eViewModeOrg;

    int         m_iTimes = 1;

    POINT2D     m_ptMove;

    StdString   m_strOldInput;

    //!< 角度固定
    bool m_bFixAngle;

    //!< 固定角度
    double m_dFixdAngle;

    //!< 距離固定
    bool m_bFixLength;

    //!< 固定距離
    double m_dFixdLength;

    //!< ストレッチモード
    bool m_bStretch;

    // 補助直線
    std::shared_ptr<CDrawingLine>   m_psTmpLine;

    std::vector<std::unique_ptr<MAT2D>> m_lstOffset;


public:
	//!< コンストラクタ
    CActionMove();

    //!< コンストラクタ
    CActionMove(CDrawingView* pView);
    
	//!< デストラクタ
    virtual ~CActionMove();

	//!< 選択解放時動作
    virtual void Cancel(VIEW_MODE eMode);

	//!< 選択解放時動作
	virtual bool SelAction(VIEW_MODE eMode, void* pParam = NULL);

	//!< マウスクリック動作
	virtual bool SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

    //!< マウス移動動作
	virtual bool MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

    //!<移動・コピー時 マウスクリック動作
    bool SelPointMove(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction, bool bCopy);

    //!<入力値設定
    virtual bool InputValue(StdString strVal);

    //移動数値入力
    bool SelInputMove( std::vector<double>& lstVal, bool bCopy );

    //!< 再描画
    virtual void Redraw() const;

protected:

    //!< 移動
    bool SetMove(POINT2D ptFirst, POINT2D ptSecond, int iTimes, bool bCopy);

    void _DeleteTmpObjects();

    bool _CalcValue(const StdString& sVal );

    void _ClearAction();

    bool _IsCopyMode() const;

protected:

    std::vector< std::shared_ptr<CDrawingObject> > m_lstTmpObjects;

};
#endif // _ACTION_MOVE_H__
