/**
 * @brief			CViewAction実装ファイル
 * @file			CViewAction.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CActionNode.h"

#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/View/CDrawingView.h"

#include "DrawingObject/CDrawingObject.h"
#include "DrawingObject/CDrawingPoint.h"
#include "DrawingObject/CDrawingLine.h"
#include "DrawingObject/CDrawingCircle.h"
#include "DrawingObject/CDrawingNode.h"

#include "View/CLayer.h"
#include "View/CUndoAction.h"
#include "View/CDrawMarker.h"
#include "System/CSystem.h"
#include "MockSketch.h"
#include <math.h>

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//!< コンストラクタ
CActionNode::CActionNode():
CViewAction()
{
    m_lMouseOver =   SEL_ALL;
}

//!< コンストラクタ
CActionNode::CActionNode(CDrawingView* pView):
CViewAction(pView)
{
    m_lMouseOver =   SEL_ALL;
}

//!< デストラクタ
CActionNode::~CActionNode()
{
    _DeleteObject();
}

/**
 *  解放時動作
 *  @param   [in]    eMode 解放時点でのモード
 *  @retval   なし
 *  @note
 */
void CActionNode::Cancel(VIEW_MODE eMode)
{
    ClearAction();
    _DeleteObject();
}

/**
 *  選択時動作
 *  @param   [in]    eMode 選択時点でのモード
 *  @retval   なし
 *  @note
 */
bool CActionNode::SelAction(VIEW_MODE eMode, void* pParam)
{
    m_iClickCnt = 0;
    m_pView->SetComment(GET_SYS_STR(STR_SEL_CREATE_POINT));

    CDrawMarker* pMarker= m_pView->GetMarker();
    pMarker->HideObject();

    m_eViewMode = eMode;
    return true;
}

/**
 *  マウスクリック動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval   なし
 *  @note
 */
bool CActionNode::SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{

	auto pNode = std::dynamic_pointer_cast<CDrawingNode>(m_psAddObj);
	CDrawingObject* pObj = NULL;
	if (pNode->IsVisible())
	{
		pObj = pNode.get();
	}

	bool bRet;
    bRet = CViewAction::AddObjByPoint(pObj, posMouse, eAction, 1);

	
	if (!bRet)
	{
		return false;
	}

	if (!pObj)
	{
		return bRet;
	}

	auto pDefNode = THIS_APP->GetDefaultObject(DT_NODE);
	COLORREF cr;
	if (pDefNode)
	{
		cr = pDefNode->GetColor();
	}
	else
	{
		cr = DRAW_CONFIG->crFront;
	}

	pNode->SetColor(cr);

	CPartsDef* pDef = m_pView->GetPartsDef();
	CUndoAction* pUndo = pDef->GetUndoAction();

	pDef->RegisterObject(pNode, true, false);
	pUndo->Add(UD_ADD, pDef, NULL, pNode, true);
	pNode->SetColor(DRAW_CONFIG->crImaginary);


	return bRet;
}


/**
 *  @brief   マウス移動動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionNode::MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
	if (!m_psAddObj)
	{
		auto pCreateObj = THIS_APP->CreateDefaultObject(DT_NODE);
		auto pNode = std::dynamic_pointer_cast<CDrawingNode>(pCreateObj);
		pNode->SetLayer(m_pView->GetCurrentLayerId());
		pNode->SetColor(DRAW_CONFIG->crImaginary);
		m_psAddObj = pNode;
	}
	
	bool bRet;
    POINT2D ptRet;
    POINT2D ptMouse;
    bRet =  CViewAction::GetMousePointWithSpalateIntersection(&ptRet,
                         &ptMouse, posMouse);

    return bRet;
}
/**
 *  @brief   再描画
 *  @param   なし
 *  @retval  なし
 *  @note    SELECTバッファーに描画
 *           pCtrl->DrawDraggingの最中に呼び出し
 */
void CActionNode::Redraw() const
{

}

/**
 *  動作クリア
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CActionNode::ClearAction(bool bReleaseSelect)
{
    CViewAction::ClearAction(bReleaseSelect);

    m_bMark = false;
    CPartsDef*    pCtrl   = m_pView->GetPartsDef();

    pCtrl->Redraw();
}


/**
 *  マーカ動作チェック
 *  @param   [in]    ptSel          選択位置
 *  @param   [in]    eActionpObj    選択位置
 *  @retval  true 選択・false 未選択
 *  @note
 */
bool CActionNode::CheckMarker(POINT ptSel, SEL_ACTION eActionpObj)
{
    return false;
}

/**
 *  @brief   入力値設定
 *  @param   [in]    strVal 入力値
 *  @retval  false エラー発生
 *  @note    必要であればエラー表示を行う
 */
bool CActionNode::InputValue(StdString strVal)
{
    // TAG: [数値解析]

    //X座標,Y座標
    std::vector<StdString> lstInput;
    boost::algorithm::split( lstInput, strVal, boost::is_any_of(_T(",")));

    double dX = 0.0;
    double dY = 0.0;
 
    size_t iSize = lstInput.size();
    if (iSize < 2)
    {
        //!< X座標,Y座標  の形式で入力してください
        AfxMessageBox(GET_STR(STR_SEL_TMP));
        return false;
    }

    POINT2D ptInput;

    ptInput.dX = _tstof(lstInput[0].c_str());
    ptInput.dY = _tstof(lstInput[1].c_str());

    CPartsDef*   pCtrl  = m_pView->GetPartsDef();
    CUndoAction* pUndo  = pCtrl->GetUndoAction();

    auto pNewNode = CViewAction::RegisterNode(ptInput);
    pUndo->Add(UD_ADD, pCtrl, NULL, pNewNode, true);

    ClearAction();
    return true;
}
