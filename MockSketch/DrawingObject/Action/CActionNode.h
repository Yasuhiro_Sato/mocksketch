/**
 * @brief			CActionNodeヘッダーファイル
 * @file			CActionNode.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#if !defined(_ACTION_NODE_H_)
#define _ACTION_NODE_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "View/VIEW_MODE.h"
#include "CViewAction.h"
class     CDrawingView;
class     CDrawingObject;
class     CDrawingNode;
class     CDrawingCircle;
class     CDrawingLine;
class     CDrawingEllipse;
class     POINT2D;
/**
 * @class   CActionNode
 * @brief                        
   
 */
class CActionNode: public CViewAction
{

protected:


public:
	//!< コンストラクタ
    CActionNode();

    //!< コンストラクタ
    CActionNode(CDrawingView* pView);
    
	//!< デストラクタ
    virtual ~CActionNode();


	//!< 選択解放時動作
	virtual void Cancel(VIEW_MODE eMode);

	//!< 選択解放時動作
	virtual bool SelAction(VIEW_MODE eMode, void* pParam = NULL);

	//!< マウスクリック動作
	virtual bool SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

    //!< マウス移動
	virtual bool MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

    //!< 数値入力
    virtual bool InputValue(StdString strVal);

    //! 再描画
    virtual void Redraw() const;


private:

    //!< 動作クリア
    virtual void ClearAction(bool bReleaseSelect = true);

    //!< マーカチェック
    bool  CheckMarker(POINT ptSel, SEL_ACTION eActionpObj);

private:
};
#endif // !defined(_ACTION_POINT_H_)
