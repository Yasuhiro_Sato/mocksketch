/**
 * @brief			CViewAction実装ファイル
 * @file			CViewAction.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MockDef.h"
#include "./CActionNodeSelect.h"

#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/View/CDrawingView.h"

#include "DrawingObject/CDrawingObject.h"
#include "DrawingObject/CDrawingPoint.h"
#include "DrawingObject/CDrawingLine.h"
#include "DrawingObject/CDrawingCircle.h"
#include "DrawingObject/CDrawingNode.h"
#include "DrawingObject/CNodeMarker.h"
#include "DrawingObject/CDrawingGroup.h"

#include "View/CLayer.h"
#include "View/CUndoAction.h"
#include "View/CDrawMarker.h"
#include "System/CSystem.h"
#include <math.h>

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define _DEBUG_DRUG 0

#if _DEBUG_DRUG
#include "DrawingObject/CDrawingText.h"
#endif

//!< コンストラクタ
CActionNodeSelect::CActionNodeSelect():
CActionNodeSelect(NULL)
{
}

//!< コンストラクタ
CActionNodeSelect::CActionNodeSelect(CDrawingView* pView):
CViewAction(pView),
m_bDrag (false),
m_strNodeMarkerId   (_T(""))
{
    m_lMouseOver =   SEL_ALL;
}

//!< デストラクタ
CActionNodeSelect::~CActionNodeSelect()
{
    _DeleteObject();
}

/**
 *  解放時動作
 *  @param   [in]    eMode 解放時点でのモード
 *  @retval   なし
 *  @note
 */
void CActionNodeSelect::Cancel(VIEW_MODE eMode)
{
    ClearAction();
    _DeleteObject();
}



void CActionNodeSelect::ClearAction(bool bReleaseSelect)
{
    CPartsDef*   pDef  = m_pView->GetPartsDef();
    //CViewAction::ClearAction(bReleaseSelect);

    CNodeMarker* pNodeMarker;
    pNodeMarker = m_pView->GetNodeMarker();

    if (pNodeMarker)
    {
        CMarkerData* pNode = pNodeMarker->GetMarker(m_strNodeMarkerId);
        if (pNode)
        {
            CDrawingNode* pDrawingNode = pNode->GetDrawingNode();
            if (pDrawingNode)
            {
                pDrawingNode->ClearNodeMenu();
            }
        }
    }

    m_bDrag = false;
    m_strNodeMarkerId   = _T("");
    m_pMarkerPearentObject.reset();

    m_pView->ClearConnectableNode();
    ::ReleaseCapture();
}


/**
 *  選択時動作
 *  @param   [in]    eMode 選択時点でのモード
 *  @retval   なし
 *  @note
 */
bool CActionNodeSelect::SelAction(VIEW_MODE eViewMode, void* pParam)
{
    //既に選択中の図形はあるか？
    CPartsDef*pDef = m_pView->GetPartsDef();
    long lSize = pDef->SelectNum();

    m_eViewMode = eViewMode;
    if (lSize == 0)
    {
        //図形を選択してください
        m_pView->SetComment(GET_SYS_STR(STR_SEL_OBJECT));
        if (m_eViewMode != VIEW_SELECT)
        {
            m_ePreViewMode = m_eViewMode;
            m_pView->SetViewMode(VIEW_SELECT);
        }
        return true;    
    }

    m_strNodeMarkerId = _T("");

    m_pMarkerPearentObject.reset();
    pDef->GetConnectingTmpObjectList()->clear();
    pDef->GetTmpGroupObjectList()->clear();

    //選択したオブジェクトが複数の場合はの場合は一時グループにまとめる
	static const bool bTmpCopy = true;
    auto pObject = CViewAction::GroupSelectObject(pDef, bTmpCopy);

    m_psGroup.reset();
    if (pObject->GetId() == ID_TEMP_GROUP)
    {
        m_psGroup = std::dynamic_pointer_cast<CDrawingGroup>(pObject);
    }

    //接続によって影響を受ける部分をコピーしConnectingTmpObjectListに登録する
    //pDef->GetConnectionToSelect->GetConnectingTmpObjectList
    CViewAction::CopyConnectionToSelectTmp(pDef);

    CNodeMarker* pNodeMarker;
    pNodeMarker = m_pView->GetNodeMarker();
    bool bNodeMarker = pObject->InitNodeMarker(pNodeMarker);
    if (bNodeMarker)
    {
        m_pMarkerPearentObject = pObject;
    }

    pDef->Redraw(m_pView);
    return true;
}

/**
 *  マウスクリック動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval   なし
 *  @note
 */
bool CActionNodeSelect::SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    CNodeMarker* pNodeMarker;
    pNodeMarker = m_pView->GetNodeMarker();
    StdString strNodeMarkerId;
    CPartsDef*   pDef  = m_pView->GetPartsDef();

    bool bNoUseShowDefautlContextMenu = true;

    bool bRet = false;
    switch (eAction)
    {
    case ACT_LBUTTON_DOWN:
    case ACT_RBUTTON_DOWN:
        m_strNodeMarkerId = _T("");

        strNodeMarkerId = pNodeMarker->GetMarkerId(posMouse.ptSel);
        m_bDrag = true;
        SelectNode(strNodeMarkerId,  posMouse);
        if (m_strNodeMarkerId == _T(""))
        {
            m_bDrag = false;
        }
        return true;
        break;

    case ACT_LBUTTON_UP:
    case ACT_RBUTTON_UP:

        pDef->ClearMouseOver();
        pDef->ClearMouseEmphasis();
        pDef->ClearMouseConnectable();
        bool bEnd = true; 

        if (m_bDrag)
        {
            m_bDrag = false;
            bEnd = ReleaseNode(posMouse);
        }
        else
        {
            if (m_psGroup)
            {
                //複数選択の場合
                m_psGroup.reset();
            }
            else
            {
                if (m_pMarkerPearentObject)
                {
                    m_pMarkerPearentObject->OffsetMatrix(NULL);

                    m_pMarkerPearentObject->SetSelect(false);
                }
                pDef->RelSelect();
            }
            pNodeMarker->Clear();
        }

        if (eAction == ACT_RBUTTON_UP)
        {
            CMarkerData* pNode = pNodeMarker->GetMarker(m_strNodeMarkerId);
            if (pNode)
            {
                CDrawingNode* pDrawingNode = pNode->GetDrawingNode();
                if (pDrawingNode)
                {
                    pDrawingNode->OnContextMenu( m_pView, posMouse, m_strNodeMarkerId);
                }
            }
        }

        if (bEnd)
        {
            if (m_psGroup)
            {
                auto pObjList = m_psGroup->GetList();
                for(auto pObj: *pObjList)
                {
                    pObj->OffsetMatrix(NULL);
                }
            }
            else
            {
				/*
                if (m_pMarkerPearentObject)
                {
                    m_pMarkerPearentObject->OffsetMatrix(NULL);
                }
				*/
            }

            CDrawMarker* pMarker= m_pView->GetMarker();
            pMarker->Clear();
            pMarker->ClearObject();

           // pDef->Redraw(m_pView);

            if (IsInterrupt())
            {
               ReturnInterrupt();
            }
            else
            {
                m_pView->SetViewMode(VIEW_SELECT);
            }
        }

        //割り込み解除
        if(pDef->SelectNum() == 0)
        {
            ReturnInterrupt();
        }

        pDef->RelSelect();
        pNodeMarker->Clear();
        pDef->Redraw();
        break;
    }
    return bNoUseShowDefautlContextMenu;
}

bool CActionNodeSelect::SelectNode(const StdString& strNodeMarkerId,
                                 MOUSE_MOVE_POS posMouse)
{

    CPartsDef*pDef = m_pView->GetPartsDef();
    CNodeMarker* pNodeMarker;
    pNodeMarker = m_pView->GetNodeMarker();

    std::shared_ptr<CDrawingObject> pObject;
    if (m_psGroup)
    {
        //複数選択の場合
        pObject = m_psGroup;
    }
    else
    {
	    pObject = m_pMarkerPearentObject;
    }

    MOUSE_BUTTON eButton;
    if(posMouse.nFlags & MK_LBUTTON)
    {
        eButton = SEL_LBUTTON;
    }
    else if (posMouse.nFlags & MK_RBUTTON)
    {
        eButton = SEL_RBUTTON;
    }
    else if (posMouse.nFlags & MK_MBUTTON)
    {
        eButton = SEL_MBUTTON;
    }
    else
    {
        eButton = SEL_NOUSE_NONE;
    }

    pNodeMarker->Select(strNodeMarkerId, eButton);

    if (pObject)
    {
        pObject->SelectNodeMarker(pNodeMarker,
                                    strNodeMarkerId);
    }

    //DrawingConnectionLineの場合 SelectNodeMarker中にマーカを増やす場合がある
    //その結果名称が途中で変更される
    m_strNodeMarkerId = pNodeMarker->GetMarkerId(posMouse.ptSel);

    CMarkerData* pNode = pNodeMarker->GetMarker(m_strNodeMarkerId);
    if (pNode)
    {
        CDrawingNode* pDrawingNode = pNode->GetDrawingNode();
        pDrawingNode->NodeSelect(m_pView);
    }
    return true;
}

bool CActionNodeSelect::ReleaseNode(MOUSE_MOVE_POS posMouse)
{
    CPartsDef*pDef = m_pView->GetPartsDef();

    CNodeMarker* pNodeMarker;
    pNodeMarker = m_pView->GetNodeMarker();

    bool bRet = true;
    if (!m_pMarkerPearentObject)
    {
        return false;
    }
    
    //ノードの移動
    CMarkerData* pNode = pNodeMarker->GetMarker(m_strNodeMarkerId);

    if (pNode)
    {
        pNode->UpdateNodePosition();

        CDrawingNode* pDrawingNode = pNode->GetDrawingNode();
        if (pDrawingNode)
        {
            pDrawingNode->ReleaseMouseOnNode(m_pView, posMouse);
        }
    }

    //-------ASに通知
    bRet = m_pMarkerPearentObject->ReleaseNodeMarker(pNodeMarker, m_strNodeMarkerId, posMouse);
    //--------

    if (m_psGroup)
    {
        //複数選択の場合
        //ReleaseNodeMarker       ->  
        //    CDrawingParts::ReleaseNodeMarker
        //        CNodeBound::OnMouseRButtonUp
        //           CNodeBound::ApplyMatrix 内で登録を行う    
        
        //m_psGroup->Disassemble(pDef);


        m_psGroup.reset();
    }
    else
    {
        m_pMarkerPearentObject->SetSelect(false);
        pDef->RelSelect();
    }
    m_pView->SetDrawingMode(DRAW_FRONT);


    pNodeMarker->Clear();


    if (!bRet)
    {
        //再選択
        if(m_pMarkerPearentObject)
        {
            //再選択は単独オブジェクトのみ
            int iId = m_pMarkerPearentObject->GetId();
            pDef->SelectDrawingObject(iId);
            m_pMarkerPearentObject->InitNodeMarker(pNodeMarker);
        }
    }
    return bRet;
}


/**
 *  @brief   マウス移動動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionNodeSelect::MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
	SNAP_DATA snap;

    CPartsDef* pDef = m_pView->GetPartsDef();
    if (!pDef)
    {
        return false;
    }

    CDrawMarker* pMarker= m_pView->GetMarker();
    pMarker->Clear();
    pMarker->ClearObject();

    StdString strToolTip;
    DWORD dwSnapType = 0;

    CNodeMarker* pNodeMarker = m_pView->GetNodeMarker();
#if _DEBUG_DRUG
    DB_PRINT(_T("CActionNodeSelect::MovePoint Drag %s--\n"), m_bDrag?_T("ON"):_T("OFF"));
#endif

    if (m_bDrag)
    {
        _GetMousePointLengthAngle(&snap, m_strNodeMarkerId, posMouse);
        if (m_pMarkerPearentObject)
        {
#if _DEBUG_DRUG
            DB_PRINT(_T("CActionNodeSelect::MovePoint Drag 1 --\n"));
#endif
            //親オブジェクトにノードの移動を通知
            m_pMarkerPearentObject->MoveNodeMarker(pNodeMarker,
                                    &snap,
                                    m_strNodeMarkerId,
                                    posMouse);

#if _DEBUG_DRUG
            DB_PRINT(_T("CActionNodeSelect::MovePoint Drag 2 --\n"));
#endif
            //pDef->HideObject(m_pMarkerPearentObject->GetId());
            pNodeMarker->SetMarkerPosView(m_strNodeMarkerId, snap.pt);
#if _DEBUG_DRUG
            DB_PRINT(_T("CActionNodeSelect::MovePoint Drag 3 --\n"));
#endif		//ここでpMarker->AddObjectを行うと選択状態が上書きされれしまう場合がある
			//m_pMarkerPearentObjectは一時グループ
            //pMarker->AddObject(m_pMarkerPearentObject); 一時削除
        }

        /*
        for (auto pTmpObj : *pDef->GetConnectingTmpObjectList())
        {
            pDef->SetMouseEmphasis(pCopy);
        }
        */
    }
    else
    {
        std::shared_ptr<CDrawingObject> pObj;
        if (m_psGroup)
        {
            //複数選択の場合
            pObj = m_psGroup;
        }
        else
        {
		    auto pList = pDef->GetSelectInstance();
            if (!pList->empty())
            {
                auto pSelObj = pList->at(0).lock();
                if(pSelObj)
                {
	                pObj = pDef->GetObjectById(pSelObj->GetId());
                }
            }
        }

        if (pObj)
        {
            posMouse.nFlags |=  MK_ON_MARKER;
            pObj->SelectedMouseMove(m_pView, posMouse);
        }
        pNodeMarker->MouseOver(posMouse.ptSel);
    }

    pDef->DrawDragging(m_pView);
#if _DEBUG_DRUG
    DB_PRINT(_T("CActionNodeSelect::MovePoint Drag 4 --\n"));
#endif
    m_pView->SetToolTipText(snap.strSnap.c_str());


#if _DEBUG_DRUG
    DB_PRINT(_T("CActionNodeSelect::MovePoint Drag END--\n\n"));
#endif

    return true;
}

bool CActionNodeSelect::_GetMousePointLengthAngle(SNAP_DATA* pSnapData, 
                                              const StdString& strNodeMarkerId,
                                              MOUSE_MOVE_POS posMouse)
{
    using namespace VIEW_COMMON;

    //点に対してスナップ
    E_SNAP_TYPE eSnap = SNP_NONE;
    bool bRet = false;
    POINT2D pt2dMousePos;
    double dAngle = 0.0;
    double dLength = 0.0;
    bool  bAngle = false;
    bool  bDistance = false;
    StdString strToolTip;
    CNodeMarker* pNodeMarker;
    pNodeMarker = m_pView->GetNodeMarker();
    int iLayerId = m_pView->GetCurrentLayerId();
    DWORD dwSnapType;

    CDrawingNode* pDrawingNode = pNodeMarker->GetDrawingNode(strNodeMarkerId);


    CNodeData::E_CONNECTION_OBJECT eConnectionObject = CNodeData::NONE;
    NODE_MARKER_MOVE eMoveMode = DMT_NOMOVE;
    POINT2D ptDir;
    E_LOCAL_WORLD eLocalWarld = E_LOCAL;
    LINE2D lineDir;
    POINT2D ptOrgin;

    POINT2D ptStartNode;

    std::shared_ptr<CDrawingObject> pObjSnap;
    POINT2D* pPt2dMove;
    pPt2dMove = &pSnapData->pt;

    int iConnectionIndex  = -1;
    int iObject1 = -1;

    if (pDrawingNode)
    {
        StdString strOrginNode;
        eMoveMode = pDrawingNode->GetMarkerMove();
        eConnectionObject =  pDrawingNode->GetConnectionObjectType();
        dwSnapType = pDrawingNode->GetSnapType();
        strOrginNode = pDrawingNode->GetSnapOrginNodeName();
        CMarkerData* pNodeOrgin = pNodeMarker->GetMarker(strOrginNode);
        dwSnapType = pDrawingNode->GetSnapType();

        //基準点の設定
        if (pNodeOrgin)
        {
            ptOrgin = pNodeOrgin->GetPoint();
        }
        else
        {
            ptOrgin = pDrawingNode->GetStartNodePosition();
        }

        ptDir = pDrawingNode->GetMarkerDir();
        eLocalWarld = pDrawingNode->GetMarkerCordinateSystem();
        LINE2D lineTmp(0.0, 0.0, ptDir.dX,  ptDir.dY);
        if (eLocalWarld == E_LOCAL)
        {
            const MAT2D* pMat =  pDrawingNode->GetParentMatrix();
            if (pMat)
            {
                lineTmp.Matrix(*pMat);
            }
            ptDir = lineTmp.Normalization();
        }
    }
    else
    {
        return false;
    }

    if (eMoveMode == DMT_NOMOVE)
    {
        m_pView->ConvScr2World(&pt2dMousePos, posMouse.ptSel, iLayerId);
        *pPt2dMove = ptStartNode;
    }
    else
    {
        if (eMoveMode == DMT_LIMITED_DIRECTION)
        {
            POINT2D ptTmp;
            POINT2D pt2dMouseOnLine;
            POINT   ptMouseOnLine;

            //マウスの位置を直線上の位置に補正する
            ptStartNode = pDrawingNode->GetStartNodePosition();
            lineDir.SetPt( ptStartNode, ptStartNode + ptDir);

            m_pView->ConvScr2World(&pt2dMousePos, posMouse.ptSel, iLayerId);

            pt2dMouseOnLine = lineDir.NearPoint(pt2dMousePos);

            m_pView->ConvWorld2Scr(&ptMouseOnLine, pt2dMouseOnLine, iLayerId);
            posMouse.ptSel = ptMouseOnLine;

            //方向指定時のスナップタイプは、 SNP_NONE か SNP_DISTANCE のみ
            if (dwSnapType != SNP_NONE)
            {
                dwSnapType = SNP_DISTANCE;
            }
        }
        else  if (eMoveMode == DMT_LIMITED_DISTANCE)
        {
            //TODO:実装
        }

        if(dwSnapType & SNP_ANGLE)      { bAngle = true;    }
        if(dwSnapType & SNP_DISTANCE)   { bDistance = true; }

        bool bIgnoreGroup = false;
		bool bKeepCenter   = true;
		if (eConnectionObject == CNodeData::NODE)
        {
            //Node接続時は,Grour内部も検索対象とする
            bIgnoreGroup = true;
        }

        if(dwSnapType & SNP_ALL_POINT)
        {
            int iMarker = -1;
            if (m_pMarkerPearentObject)
            {
                iMarker = m_pMarkerPearentObject->GetId();
            }

            SNAP_DATA snap;

            bRet = m_pView->GetMousePointSingle(&snap, 
                                                &pObjSnap, 
                                                posMouse, 
                                                bIgnoreGroup,
                                     			bKeepCenter,
                                                iMarker);
            *pPt2dMove = snap.pt;
            pt2dMousePos = snap.ptOrg;

            if (pObjSnap && m_pMarkerPearentObject)
            {
                int iSnap = pObjSnap->GetId();

                if (iSnap == iMarker)
                {
                    bRet = false;
                    eSnap = SNP_NONE;
                }
            }

            if (bRet)
            {
                eSnap =    snap.eSnapType;
                iConnectionIndex = snap.iConnectionIndex;
                iObject1      = snap.iObject1;
            }
        }
        else
        {
            m_pView->ConvScr2World(&pt2dMousePos, posMouse.ptSel, iLayerId);

        }

        if (((eSnap == SNP_NONE) || !bRet) &&
            ( bDistance || bAngle))
        {
            bool bRet;
            bRet = m_pView->GetLengthSnapPoint(&dAngle,
                                        &dLength,
                                        pPt2dMove,  
                                        ptOrgin,  
                                        pt2dMousePos,
                                        bAngle,
                                        bDistance);

            if (bAngle)
            {
                eSnap = SNP_ANGLE;
            }
            else if (bDistance)
            {
                eSnap = SNP_DISTANCE;
            }

            if(eMoveMode == DMT_LIMITED_DIRECTION)
            {
                //もう一度線上に補正をかける
                POINT2D pt2dMouseOnLine = *pPt2dMove;
                if (bRet)
                {
                    //
                    std::vector<POINT2D> lstPoint;
                    if (fabs(dLength) < NEAR_ZERO)
                    {
                        pt2dMouseOnLine = ptOrgin;
                    }
                    else
                    {
                        CIRCLE2D cr(ptOrgin, dLength);
                        if (cr.Intersect(lineDir, &lstPoint))
                        {
                            if (lstPoint.size() == 2)
                            {
                                if ( lstPoint[0].Distance(*pPt2dMove) > lstPoint[1].Distance(*pPt2dMove))
                                {
                                    pt2dMouseOnLine = lstPoint[1];
                                }
                                else
                                {
                                    pt2dMouseOnLine = lstPoint[0];
                                }
                            }
                        }
                    }
                }
                else
                {
                    pt2dMouseOnLine = lineDir.NearPoint(*pPt2dMove);
                }

                *pPt2dMove = pt2dMouseOnLine;
            }
        }
    }
                    
    bool bConnectNode = false;
    CPartsDef*pCtrl = m_pView->GetPartsDef();
    switch(eConnectionObject)
    {
    case CNodeData::NONE:
        break;

    case CNodeData::NODE:
        {
            for(auto pObj: m_lstMouseSearch)
            {
                pCtrl->SetMouseConnectable(pObj);

                if (pObjSnap == pObj)
                {
                    bConnectNode = true;
                }
            }

            CMarkerData* pNode = pNodeMarker->GetMarker(strNodeMarkerId);

            if (pNode)
            {
                pNode->m_bConnect = bConnectNode;
            }
        }
  

        break;

    case CNodeData::FIELD:

        break;
    case CNodeData::OBJECT:
        {
            double dMinDistance = DBL_MAX;
            double dDistance;
            std::shared_ptr<CDrawingObject> pMinDistanceObj;
            for(auto pObj: m_lstMouseSearch)
            {
                dDistance = pObj->Distance(*pPt2dMove);
                if (dDistance < dMinDistance)
                {
                    dMinDistance = dDistance;
                    pMinDistanceObj = pObj;
                }
            }

            bool bConnect = false;
            if (pMinDistanceObj)
            {
                pCtrl->SetMouseEmphasis(pMinDistanceObj);
                bConnect = true;
            }

            CMarkerData* pNode = pNodeMarker->GetMarker(strNodeMarkerId);

            if (pNode)
            {
                pNode->m_bConnect = bConnect;
            }

        }
        break;

    default:
        break;
    }

    if (bConnectNode)
    {
        strToolTip = GET_STR(STR_ACT_CONNECTION);
    }
    else
    {
        strToolTip = GetSnapText(eSnap, dLength, dAngle);
    }

    pSnapData->eSnapType = eSnap;
    pSnapData->ptOrg = pt2dMousePos;
    pSnapData->strSnap = strToolTip;
    pSnapData->iConnectionIndex   =  iConnectionIndex;
    pSnapData->iObject1        =  iObject1;
    return true;
}

bool  CActionNodeSelect::OnContextMenu(POINT point)
{
    STD_ASSERT(m_pView != NULL);

    //既に選択中の図形はあるか？
    CPartsDef* pDef = m_pView->GetPartsDef();
    if (!pDef)
    {
        return false;
    }

    long lSize = pDef->SelectNum();

    if(lSize == 0)
    {
        return false;
    }

    auto pObj = pDef->GetSelectInstance()->at(0);

    CWnd* pWnd = const_cast<CWnd*>(m_pView->GetWindow());

    CNodeMarker* pNodeMarker;
    pNodeMarker = m_pView->GetNodeMarker();
    POINT pointView = point;
    pWnd->ScreenToClient( &pointView );
    m_strNodeMarkerId = pNodeMarker->GetMarkerId(pointView);
    if (m_strNodeMarkerId != _T(""))
    {
        pNodeMarker->OnContextMenu(pWnd, point, m_strNodeMarkerId);
    }
    return true;
}

/**
 * @brief   メニュー選択
 * @param   [in] nItemID   メニュー項目 ID 
 * @param   [in] nFlags    メニューのフラグの組み合わせが
 * @param   [in] hSysMenu  
 * @retval  なし
 * @note
 */
void CActionNodeSelect::OnMenuSelect(UINT nItemID, UINT nFlags, HMENU hSysMenu)
{
    CNodeMarker* pNodeMarker;
    pNodeMarker = m_pView->GetNodeMarker();

    if (pNodeMarker)
    {
        CMarkerData* pNode = pNodeMarker->GetMarker(m_strNodeMarkerId);
        if (pNode)
        {
            CDrawingNode* pDrawingNode = pNode->GetDrawingNode();

            if (pDrawingNode)
            {
                pDrawingNode->OnMenuSelect(m_pView, nItemID, nFlags, hSysMenu);
            }
        }
    }
}


/**
 *  @brief   再描画
 *  @param   なし
 *  @retval  なし
 *  @note    SELECTバッファーに描画
 *           pCtrl->DrawDraggingの最中に呼び出し
 */
void CActionNodeSelect::Redraw() const
{

}

/**
 *  動作クリア
 *  @param   なし
 *  @retval  なし
 *  @note
 */
/*
void CActionNodeSelect::ClearAction(bool bReleaseSelect)
{
    CViewAction::ClearAction(bReleaseSelect);

    m_bMark = false;
    CPartsDef*    pCtrl   = m_pView->GetPartsDef();

    pCtrl->Redraw();
}
*/

/**
 *  マーカ動作チェック
 *  @param   [in]    ptSel          選択位置
 *  @param   [in]    eActionpObj    選択位置
 *  @retval  true 選択・false 未選択
 *  @note
 */
bool CActionNodeSelect::CheckMarker(POINT ptSel, SEL_ACTION eActionpObj)
{
    return false;
}

/**
 *  @brief   入力値設定
 *  @param   [in]    strVal 入力値
 *  @retval  false エラー発生
 *  @note    必要であればエラー表示を行う
 */
bool CActionNodeSelect::InputValue(StdString strVal)
{
    // TAG: [数値解析]

    //X座標,Y座標
    std::vector<StdString> lstInput;
    boost::algorithm::split( lstInput, strVal, boost::is_any_of(_T(",")));

    double dX = 0.0;
    double dY = 0.0;
 
    size_t iSize = lstInput.size();
    if (iSize < 2)
    {
        //!< X座標,Y座標  の形式で入力してください
        AfxMessageBox(GET_STR(STR_SEL_TMP));
        return false;
    }

    POINT2D ptInput;

    ptInput.dX = _tstof(lstInput[0].c_str());
    ptInput.dY = _tstof(lstInput[1].c_str());

    CPartsDef*   pCtrl  = m_pView->GetPartsDef();
    CUndoAction* pUndo  = pCtrl->GetUndoAction();

    auto pNewNode = CViewAction::RegisterNode(ptInput);
    pUndo->Add(UD_ADD, pCtrl, NULL, pNewNode, true);

    ClearAction();
    return true;
}
