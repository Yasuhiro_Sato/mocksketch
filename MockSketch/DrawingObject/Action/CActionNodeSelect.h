/**
 * @brief			CActionNodeSelectヘッダーファイル
 * @file			CActionNodeSelect.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#if !defined(_ACTION_NODE_SELECT_H_)
#define _ACTION_NODE_SELECT_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "View/VIEW_MODE.h"
#include "CViewAction.h"
#include "DrawingObject/Primitive/POINT2D.h"

class     CDrawingView;
class     CDrawingObject;
class     CDrawingNode;
class     CDrawingCircle;
class     CDrawingLine;
class     CDrawingEllipse;
class     CDrawingGroup;

/**
 * @class   CActionNodeSelect
 * @brief                        
   
 */
class CActionNodeSelect: public CViewAction
{

protected:
    bool m_bDrag;

    StdString m_strNodeMarkerId;

    POINT2D m_ptStartNode;

    std::vector<std::shared_ptr<CDrawingObject>> m_lstMouseSearch;

public:
	//!< コンストラクタ
    CActionNodeSelect();

    //!< コンストラクタ
    CActionNodeSelect(CDrawingView* pView);
    
	//!< デストラクタ
    virtual ~CActionNodeSelect();


	//!< 選択解放時動作
	virtual void Cancel(VIEW_MODE eMode);

	//!< 選択解放時動作
	virtual bool SelAction(VIEW_MODE eMode, void* pParam = NULL) override;

	//!< マウスクリック動作
	virtual bool SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction) override;

    //!< マウス移動
	virtual bool MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction) override;

    //!< 数値入力
    virtual bool InputValue(StdString strVal);

    //! 再描画
    virtual void Redraw() const;

    //!<コンテキストメニュー表示
    virtual bool OnContextMenu(POINT point);

    virtual void OnMenuSelect(UINT nItemID, UINT nFlags, HMENU hSysMenu);

private:

    bool SelectNode(const StdString& strNodeMarkerId, 
                    MOUSE_MOVE_POS posMouse);

    bool ReleaseNode(MOUSE_MOVE_POS posMouse);

    //!< 動作クリア
    virtual void ClearAction(bool bReleaseSelect = true);

    //!< マーカチェック
    bool  CheckMarker(POINT ptSel, SEL_ACTION eActionpObj);

    bool _GetMousePointLengthAngle(SNAP_DATA* pSnapData, 
                                   const StdString& strNodeMarkerId,
                                   MOUSE_MOVE_POS posMouse);


private:
};
#endif // !defined(_ACTION_POINT_H_)
