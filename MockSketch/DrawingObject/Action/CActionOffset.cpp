/**
 * @brief			CActionOffset実装ファイル
 * @file			CActionOffset.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CActionOffset.h"

#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/View/CDrawingView.h"

#include "DrawingObject/CDrawingObject.h"
#include "DrawingObject/CDrawingPoint.h"
#include "DrawingObject/CDrawingLine.h"
#include "DrawingObject/CDrawingCircle.h"

#include "View/CLayer.h"
#include "View/CDrawMarker.h"
#include "View/CUndoAction.h"
#include "View/ViewCommon.h"
#include "Utility/CUtility.h"
#include "System/CSystem.h"
#include <math.h>


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 * コンストラクタ
 */
CActionOffset::CActionOffset():
CViewAction(),
m_dOffsetInput      (0.0),
m_iTimes       (1)
{
    m_lMouseOver =  SEL_LINE | SEL_CIRCLE | SEL_ELLIPSE | SEL_COMPOSITION_LINE;
    m_pView        = NULL;
    _SetSnapType();
}

/**
 * コンストラクタ
 */
CActionOffset::CActionOffset(CDrawingView* pView):
CViewAction(pView),
m_dOffsetInput      (0.0),
m_iTimes       (1)

{
    m_lMouseOver =  SEL_LINE | SEL_CIRCLE | SEL_ELLIPSE;
    _SetSnapType();
}

/**
 * デストラクタ
 */
CActionOffset::~CActionOffset()
{
}

//スナップタイプ設定
void CActionOffset::_SetSnapType()
{
    using namespace VIEW_COMMON;
    m_dwSnapType = DRAW_CONFIG->GetSnapType();
    DWORD dwSnapOffset;
    dwSnapOffset = SNP_END_POINT &
                    SNP_MID_POINT &
                    SNP_CROSS_POINT &
                    SNP_CENTER &
                    SNP_TANGENT &
                    SNP_FEATURE_POINT &
                    SNP_DATUME_POINT &
                    SNP_DISTANCE;
    m_dwSnapType &= dwSnapOffset;
}

/**
 *  @brief   解放時動作
 *  @param   [in]    eMode 解放時点でのモード
 *  @retval   なし
 *  @note
 */
void CActionOffset::Cancel(VIEW_MODE eMode) 
{
    //選択解除
    ClearAction();
    m_dOffsetInput = 0.0;
}

/**
 *  @brief   選択時動作
 *  @param   [in]    eMode 選択時点でのモード
 *  @retval   なし
 *  @note
 */
bool CActionOffset::SelAction(VIEW_MODE eMode, void* pParam) 
{
    if (!m_pView){return false;}
    //点はOFFSET要素としては選択不可
    m_lMouseOver =   SEL_PRI_NOPT ;

    //既に選択中の図形はあるか？
    long lSize = m_pView->GetPartsDef()->SelectNum();

    if (lSize != 0)
    {
        //選択を解除
        m_pView->GetPartsDef()->RelSelect();
    }   
    
    m_pView->SetComment(GET_SYS_STR(STR_SEL_OBJECT));
    m_eViewMode = eMode;
    return true;
}

/**
 *  @brief   マウスクリック動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionOffset::SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    CPartsDef*   pCtrl;
    int iId;

    pCtrl = m_pView->GetPartsDef();
    iId   = m_pView->SearchPos(posMouse.ptSel);
    auto pObj  = pCtrl->GetObjectById(iId);
    CUndoAction* pUndo  = pCtrl->GetUndoAction();

    if (eAction != CViewAction::ACT_LBUTTON_UP)
    {
        //左クリックのみ
        return false;
    }
    
    POINT2D pt2D;
    if (m_lstSelectObj.size() > 0)
    {
        _DrawOrSetCurrentPositionData(false /*bDraw*/);
        return true;
    }



    //何かをクリック
    if (m_iClickCnt == 0)
    {
        if (IsActionType(m_lMouseOver, pObj.get()))
        {
            m_lstSelectObj.push_back(pObj);
            pCtrl->SelectDrawingObject(iId);
            m_iClickCnt++;

            // 入力エリアにフォーカスを設定する
            m_pView->SetInputFocus();
            m_pView->SetComment(GET_SYS_STR(STR_SEL_OFFSET));

            return true;
        }
    }
    else
    {
        //
    }
    return false;
}


/**
 *  @brief   マウス移動動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionOffset::MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    bool bRet = true;
    std::shared_ptr<CDrawingObject> pFirstObject;

    using namespace VIEW_COMMON;

    if (m_lstSelectObj.size() > 0)
    {
        pFirstObject = m_lstSelectObj[0].lock();
    }

    bool bIgnoreSnap = false;
    if (posMouse.nFlags & MK_SHIFT)
    {
        bIgnoreSnap = true;
    }
    if (posMouse.nFlags & MK_CONTROL)
    {
        m_bBothSide = true;
    }
    else
    {
        m_bBothSide = false;
    }
    
    CPartsDef*   pDef  = m_pView->GetPartsDef();
    if (!pDef)
    {
        return false;
    }

    CDrawMarker* pMarker= m_pView->GetMarker();
    m_dOffset = m_dOffsetInput;

    pMarker->Clear();
    pMarker->ClearObject();
    pDef->ClearMouseOver();
    pDef->ClearMouseEmphasis();
    pDef->ClearMouseConnectable();

    SNAP_DATA snap;
    m_pView->GetMousePoint(&snap, posMouse);

    auto pObj1 = pDef->GetObjectById(snap.iObject1);

    if (!pFirstObject)
    {
        if (pObj1)
        {
            if (IsActionType(m_lMouseOver, pObj1.get()))
            {
                pDef->SetMouseOver(pObj1);
            }
        }
    }
    else
    {
        double dAngle;

        bool bOnLine = false;
        if ((pFirstObject->GetType() == DT_COMPOSITION_LINE) ||
            (pFirstObject->GetType() == DT_SPLINE))
        {
            bOnLine = true;
        }

        pFirstObject->NearPoint (&m_ptCreate, snap.pt, bOnLine /*bOnLine*/);  

        //距離によるスナップ
        if (fabs(m_dOffsetInput) < NEAR_ZERO)
        {
            if ( (snap.eSnapType == SNP_NONE) && (!bIgnoreSnap))
            {
                bool bRet;
                bRet =m_pView->GetLengthSnapPoint(&dAngle,
                                                &m_dOffset,
                                                &m_ptSnap,  
                                                m_ptCreate,  
                                                snap.pt,
                                                false,
                                                true);
                snap.eSnapType = SNP_DISTANCE;
            }
            else
            {
                m_dOffset = m_ptCreate.Distance(snap.pt);
                m_ptSnap = snap.pt;
            }
        }
        else
        {
            //垂線

            LINE2D lineNorm(m_ptCreate, snap.pt);
            POINT2D ptNorm = lineNorm.Normalization();
            m_ptSnap.dX = ptNorm.dX * m_dOffsetInput + m_ptCreate.dX;
            m_ptSnap.dY = ptNorm.dY * m_dOffsetInput + m_ptCreate.dY;
            m_dOffset = m_dOffsetInput;
            snap.eSnapType = SNP_INPUT_VAL;
        }

        if( (snap.eSnapType != SNP_NONE) &&
            (pObj1 != NULL))
        {
            if (snap.eSnapType != SNP_INPUT_VAL)
            {
                pDef->SetMouseEmphasis(pObj1);

                if (snap.iObject2 == -1)
                {
                    m_pView->SetMarker(pObj1.get(), true /*bIgnoreOffLineObject*/);
                    pMarker->MouseMove(posMouse.ptSel);
                }
                else
                {
                    auto pObj2 = pDef->GetObjectById(snap.iObject2);
                    pDef->SetMouseEmphasis(pObj2);
                }
            }
        }


        auto pExtLine = CreateLine(&m_psAddtionalObj[0]);
        pExtLine->SetPoint1(m_ptCreate);
        pExtLine->SetPoint2(m_ptSnap);
        pExtLine->SetLayer(m_pView->GetCurrentLayerId());
        pMarker->AddObject(pExtLine);


        _DrawOrSetCurrentPositionData(true /*bDraw*/);

    }

    pDef->DrawDragging(m_pView);

    if(pFirstObject)
    {
        StdString strToolTipText;
        StdStringStream sToolTip;
        strToolTipText = GetSnapName(snap.eSnapType);
        sToolTip << StdFormat(_T("%s:%f")) % strToolTipText % m_dOffset;
    

        if (!sToolTip.str().empty())
        {
            if((m_ptTooltip.x != posMouse.ptSel.x) ||
                (m_ptTooltip.y != posMouse.ptSel.y))
            {
                m_pView->SetToolTipText(sToolTip.str().c_str());
            }
        }
        else
        {
            m_pView->ResetToolTipText();
        }
        m_ptTooltip = posMouse.ptSel;
    }
    return bRet;
}

/**
 *  @brief   再描画
 *  @param   なし
 *  @retval  なし
 *  @note    SELECTバッファーに描画
 *           pCtrl->DrawDraggingの最中に呼び出し
 */
void CActionOffset::Redraw() const
{
}

/**
 *  @brief   現在位置データの設定及び描画
 *  @param   [in] bDraw   true:描画 false:設定
 *  @retval  なし
 *  @note    
 *           
 */
void CActionOffset::_DrawOrSetCurrentPositionData(bool bDraw)
{
    CPartsDef*   pDef  = m_pView->GetPartsDef();
    std::shared_ptr<CDrawingObject> pFirstObject;
    CUndoAction* pUndo  = pDef->GetUndoAction();

    m_lstOffsetObj.clear();
    if (m_lstSelectObj.size() > 0)
    {
        pFirstObject = m_lstSelectObj[0].lock();
    }
    else
    {
        return;
    }

    int iTimes;
    if (m_iTimes == 0)
    {
        iTimes = 1;
    }
    else
    {
        iTimes = m_iTimes;
    }

    CDrawMarker* pMarker= m_pView->GetMarker();
    
    for(int iCnt = 0; iCnt < iTimes; iCnt++)
    {
        bool bOffset;
        auto pOffset =CDrawingObject::CloneShared(pFirstObject.get());
        bOffset = pOffset->Offset(m_dOffset * (iCnt + 1), m_ptSnap); 

        if(!bOffset)
        {
            continue;
        }

        if (bDraw)
        {
            pOffset->SetColor(DRAW_CONFIG->crImaginary);
            m_lstOffsetObj.push_back(pOffset);
            pDef->SetMouseOver(pOffset);
        }
        else
        {
            pOffset->SetSelect(false);
            pDef->RegisterObject(pOffset, true, true);
            pUndo->Add(UD_ADD, pDef, NULL, pOffset);
        }

        if (m_bBothSide)
        {
            POINT2D ptMirror = m_ptSnap.GetSynnetry(m_ptCreate);
            auto pOffsetBoth =CDrawingObject::CloneShared(pFirstObject.get());
            bOffset = pOffsetBoth->Offset(m_dOffset * (iCnt + 1), ptMirror); 

            if(!bOffset)
            {
                continue;
            }

                    
            if (bDraw)
            {
               pOffsetBoth->SetColor(DRAW_CONFIG->crImaginary);
               m_lstOffsetObj.push_back(pOffsetBoth);
               pDef->SetMouseOver(pOffsetBoth);
            }
            else

            {         
                pOffsetBoth->SetSelect(false);
                pDef->RegisterObject(pOffsetBoth, true, true);
                pUndo->Add(UD_ADD, pDef, NULL, pOffsetBoth);
            }
        }
    }
 
    if (!bDraw)
    {
        pUndo->Push();
        const_cast<CActionOffset*>(this)-> ClearAction();
    }

}


/**
 *  @brief   入力値設定
 *  @param   [in]    strVal 入力値
 *  @retval  false エラー発生
 *  @note    必要であればエラー表示を行う
 *  @note    エラー発生で入力コンボボックスは選択状態になる      
 *  @note    エラーがない場合はコンボボックスに値が登録される
 */
bool CActionOffset::InputValue(StdString strVal)
{
    // TAG: [数値解析]
    if (strVal == _T(""))
    {
        return false;
    }

    std::vector<StdString> lstInput;
    double dVal;
    std::vector<double>    lstVal;
    boost::algorithm::split( lstInput, strVal, boost::is_any_of(_T(",")));
    
    foreach(StdString sVal, lstInput)
    {
        dVal = _tstof(sVal.c_str());
        lstVal.push_back(dVal);
    }

    m_dOffsetInput = lstVal[0];

    m_iTimes = 1;
    if (lstVal.size() >= 2)
    {
        m_iTimes = int(lstVal[1]);
        if (m_iTimes < 1)
        {
            m_iTimes = 1;
        }
        else if (m_iTimes > 1000)
        {
            m_iTimes = 1000;
        }
    }


    return true;
}


void CActionOffset::ClearAction()
{
    CViewAction::ClearAction();

    m_bMark = false;

    CDrawMarker*        pMarker = m_pView->GetMarker();
    CPartsDef*    pCtrl   = m_pView->GetPartsDef();


    m_psAddObj.reset();
    m_lstOffsetObj.clear();
    pMarker->Clear();
    pCtrl->RelSelect();
    pCtrl->Redraw();
    m_pView->SetComment(GET_SYS_STR(STR_SEL_OBJECT));
}


//!< ２等分線作成
bool CActionOffset::DivideLine (const LINE2D* line1, const POINT2D* ptClick1,
                                const LINE2D* line2, const POINT2D* ptClick2 )
{
    double dA1, dA2;
    double dB1, dB2;
    double dC1, dC2;

    double dE1, dE2;
    double dF1, dF2;
    double dG1, dG2;
    CPartsDef*   pCtrl  = m_pView->GetPartsDef();
    CUndoAction* pUndo  = pCtrl->GetUndoAction();

    if(!line1->GetParam( &dA1, &dB1, &dC1))
    {
        return false;
    }

    if(!line2->GetParam( &dA2, &dB2, &dC2))
    {
        return false;
    }

    double dH = sqrt(dA2 * dA2 + dB2 * dB2) - sqrt(dA1 * dA1 + dB1 * dB1);
    dE1 = dA1 - dA2;   
    dF1 = dB1 - dB2;   
    dG1 = dC1 - dC2 + dH;   

    dE2 = dA1 + dA2;   
    dF2 = dB1 + dB2;   
    dG2 = dC1 + dC2 + dH;   
   

    double dDirClick1 = dE1 * ptClick1->dX + dF1 * ptClick1->dY + dG1;
    double dDirClick2 = dE1 * ptClick2->dX + dF1 * ptClick2->dY + dG1;

    double dTheta;
    if ((dDirClick1 * dDirClick2) > 0.0)
    {
        dTheta = atan2(-dE2, dF2) * RAD2DEG;
    }
    else
    {
        dTheta = atan2(-dE1, dF1) * RAD2DEG;
    }

    try
    {
        LINE2D lineNorm(*ptClick1, dTheta + 90.0);
        POINT2D ptCross = lineNorm.Intersect(*line2);
        POINT2D ptThrough = (ptCross + (*ptClick1)) / 2.0;
        LINE2D lineRet(ptThrough, dTheta);

        auto pLine = RegisterLine(&lineRet );
        pUndo->Add(UD_ADD, pCtrl, NULL, pLine, true);
    }
    catch(...)
    {

    }

    //終わり
    ClearAction();

    return true;
}
