/**
 * @brief			CActionOffsetヘッダーファイル
 * @file			CActionOffset.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef _ACTION_OFFSET_H__
#define _ACTION_OFFSET_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CViewAction.h"
#include "DrawingObject/Primitive/POINT2D.h"

class     CDrawingView;

/**
 * @class   CActionOffset
 * @brief                        
 */
class CActionOffset: public CViewAction
{
    struct CLICK_POINT
    {
        POINT2D         ptClick;    //クリック位置
        CDrawingObject*   pObject;    //選択オブジェクト
    };


public:
	//!< コンストラクタ
    CActionOffset();

    //!< コンストラクタ
    CActionOffset(CDrawingView* pView);
    
	//!< デストラクタ
    virtual ~CActionOffset();

	//!< 選択解放時動作
    virtual void Cancel(VIEW_MODE eMode);

	//!< 選択解放時動作
	virtual bool SelAction(VIEW_MODE eMode, void* pParam = NULL);

	//!< マウスクリック動作
	virtual bool SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

	//!< マウス移動動作
	virtual bool MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

    //!<入力値設定
    virtual bool InputValue(StdString strVal);

    //! 再描画
    virtual void Redraw() const;

protected:

    //!< マーカーチェック
    //bool CheckMarker( POINT ptSel, SEL_ACTION eAction);

    //アクション解除
    void ClearAction();

    //!< ２等分線作成
    bool DivideLine (const LINE2D* line1, const POINT2D* ptClick1,
                     const LINE2D* line2, const POINT2D* ptClick2 );

    //スナップタイプ設定
    void _SetSnapType();

    //現在位置データの設定及び描画
    void _DrawOrSetCurrentPositionData(bool bDraw);



protected:
    double m_dOffsetInput;

    double m_dOffset;

    int    m_iTimes;

    POINT2D m_ptSnap;

    POINT2D m_ptCreate;

    bool   m_bBothSide;

    //以下削除予定
    CLICK_POINT   m_FirstSelect;

    DWORD   m_dwSnapType;

   std::vector<std::shared_ptr<CDrawingObject>> m_lstOffsetObj;

};
#endif // _ACTION_OFFSET_H__
