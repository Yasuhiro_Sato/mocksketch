/**
 * @brief			CViewAction実装ファイル
 * @file			CViewAction.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CActionPoint.h"

#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/View/CDrawingView.h"

#include "DrawingObject/CDrawingObject.h"
#include "DrawingObject/CDrawingPoint.h"
#include "DrawingObject/CDrawingLine.h"
#include "DrawingObject/CDrawingCircle.h"

#include "View/CLayer.h"
#include "View/CUndoAction.h"
#include "View/CDrawMarker.h"
#include "System/CSystem.h"
#include "MockSketch.h"
#include <math.h>

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//!< コンストラクタ
CActionPoint::CActionPoint():
CViewAction()
{
    m_lMouseOver =   SEL_ALL;
}

//!< コンストラクタ
CActionPoint::CActionPoint(CDrawingView* pView):
CViewAction(pView)
{
    m_lMouseOver =   SEL_FIG;
}

/**
 *  解放時動作
 *  @param   [in]    eMode 解放時点でのモード
 *  @retval   なし
 *  @note
 */
void CActionPoint::Cancel(VIEW_MODE eMode)
{
    ClearAction();
    CDrawMarker* pMarker= m_pView->GetMarker();
    pMarker->ClearObject();

    m_psAddObj.reset();

}

/**
 *  選択時動作
 *  @param   [in]    eMode 選択時点でのモード
 *  @retval   なし
 *  @note
 */
bool CActionPoint::SelAction(VIEW_MODE eMode, void* pParam)
{
    m_iClickCnt = 0;
    CDrawMarker* pMarker= m_pView->GetMarker();
    pMarker->HideObject();
    m_eViewMode = eMode;
    return true;
}

/**
 *  マウスクリック動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval   なし
 *  @note
 */
bool CActionPoint::SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
	auto pPoint = std::dynamic_pointer_cast<CDrawingPoint>(m_psAddObj);

	CDrawingObject* pObj = NULL;
	if (pPoint->IsVisible())
	{
		pObj = pPoint.get();
	}

	bool bRet;
	bRet = CViewAction::AddObjByPoint(pObj, posMouse, eAction, 1);
    return bRet;
}

/**
 *  @brief   マウス移動動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionPoint::MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{

	if (!m_psAddObj)
	{
		auto pCreateObj = THIS_APP->CreateDefaultObject(DT_POINT);
		pCreateObj->SetLayer(m_pView->GetCurrentLayerId());
		pCreateObj->SetColor(DRAW_CONFIG->crImaginary);
		m_psAddObj = pCreateObj;
	}
	auto pPoint = std::dynamic_pointer_cast<CDrawingPoint>(m_psAddObj);

    bool bRet;
    POINT2D ptRet;
    POINT2D ptMouse;
	bRet =  CViewAction::GetMousePointWithSpalateIntersection(&ptRet,
                         &ptMouse, posMouse);

    return bRet;
}

bool  CActionPoint::OnContextMenu(POINT point)
{
	STD_ASSERT(m_pView != NULL);
	CPartsDef* pDef = m_pView->GetPartsDef();
	if (!pDef)
	{
		return false;
	}

	CWnd* pWnd = const_cast<CWnd*>(m_pView->GetWindow());
	POINT pointView = point;
	pWnd->ScreenToClient(&pointView);

	int iId = m_pView->SearchPos(pointView);
	auto     pObj = pDef->GetObjectById(iId);

	if (!pObj)
	{
		return false;
	}

	std::list<SNAP_DATA> lstSnap;
	pObj->GetSnapList(&lstSnap);

	if (lstSnap.empty())
	{
		return false;
	}

	CMenu menu;
	menu.CreatePopupMenu();

	menu.AppendMenu(MF_STRING, ID_MNU_ADD_ALL_POINT, GET_STR(STR_MNU_ADD_ALL_POINT));


	UINT uiMenu = menu.TrackPopupMenu(
		TPM_LEFTALIGN |    //クリック時のX座標をメニューの左辺にする
		TPM_RIGHTBUTTON |    //右クリックでメニュー選択可能とする
		TPM_RETURNCMD,      //関数の戻り値として、ユーザーが選択したメニュー項目の識別子を返します。
		point.x, point.y,    //メニューの表示位置
		pWnd                //このメニューを所有するウィンドウ
	);

	if (uiMenu == ID_MNU_ADD_ALL_POINT)
	{
		CUndoAction* pUndo = pDef->GetUndoAction();

		auto pCreateObj = THIS_APP->CreateDefaultObject(DT_POINT);
		auto defObj = std::dynamic_pointer_cast<CDrawingPoint>(pCreateObj);
		auto pt = std::make_shared<CDrawingPoint>(*(defObj.get()));

		for (auto snap : lstSnap)
		{
			pt->SetPoint(snap.pt);
			auto pNewObject = AddObject(pt.get());
			pUndo->Add(UD_ADD, pDef, NULL, pNewObject, false);
		}

		pUndo->Push();
	}

	pDef->Redraw();
	return true;
}


/**
 *  動作クリア
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CActionPoint::ClearAction()
{
    CViewAction::ClearAction();
    m_bMark = false;
    CPartsDef*    pCtrl   = m_pView->GetPartsDef();
    pCtrl->Redraw();
}

/**
 *  @brief   入力値設定
 *  @param   [in]    strVal 入力値
 *  @retval  false エラー発生
 *  @note    必要であればエラー表示を行う
 */
bool CActionPoint::InputValue(StdString strVal)
{
    //X座標,Y座標
    std::vector<StdString> lstInput;
    boost::algorithm::split( lstInput, strVal, boost::is_any_of(_T(",")));

    double dX = 0.0;
    double dY = 0.0;
 
    size_t iSize = lstInput.size();
    if (iSize < 2)
    {
        //!< X座標,Y座標  の形式で入力してください
        AfxMessageBox(GET_STR(STR_SEL_TMP));
        return false;
    }

    POINT2D ptInput;

    if(!ExecuteString(&dX, lstInput[0]))
    {
        return false;
    }

    if(!ExecuteString(&dY, lstInput[1]))
    {
        return false;
    }

    ptInput.dX = dX;
    ptInput.dY = dY;


    CPartsDef*   pCtrl  = m_pView->GetPartsDef();
    CUndoAction* pUndo  = pCtrl->GetUndoAction();

    auto pNewPoint = RegisterPoint(ptInput);
    pUndo->Add(UD_ADD, pCtrl, NULL, pNewPoint, true);

    ClearAction();
    return true;
}
