/**
 * @brief			CActionPointヘッダーファイル
 * @file			CActionPoint.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#if !defined(_ACTION_POINT_H_)
#define _ACTION_POINT_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "View/VIEW_MODE.h"
#include "CViewAction.h"
class     CDrawingView;
class     CDrawingObject;
class     CDrawingPoint;
class     POINT2D;
/**
 * @class   CActionPoint
 * @brief                        
   
 */
class CActionPoint: public CViewAction
{

protected:


public:
	//!< コンストラクタ
    CActionPoint();

    //!< コンストラクタ
    CActionPoint(CDrawingView* pView);
    
	//!< デストラクタ
    virtual ~CActionPoint(){;}


	//!< 選択解放時動作
	virtual void Cancel(VIEW_MODE eMode) override;

	//!< 選択解放時動作
	virtual bool SelAction(VIEW_MODE eMode, void* pParam = NULL) override;

	//!< マウスクリック動作
	virtual bool SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction) override;

    //!< マウス移動
	virtual bool MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction) override;

	virtual bool OnContextMenu(POINT point) override;

    //!< 数値入力
    virtual bool InputValue(StdString strVal);

private:

    //!< 動作クリア
    void ClearAction();

private:

};
#endif // !defined(_ACTION_POINT_H_)
