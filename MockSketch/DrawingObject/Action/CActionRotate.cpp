    /**
 * @brief			CActionRotate実装ファイル
 * @file			CActionRotate.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CActionRotate.h"

#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/View/CDrawingView.h"

#include "DrawingObject/CDrawingObject.h"
#include "DrawingObject/CDrawingPoint.h"
#include "DrawingObject/CDrawingLine.h"
#include "DrawingObject/CDrawingCircle.h"
#include "DrawingObject/CDrawingEllipse.h"
#include "DrawingObject/CDrawingParts.h"

#include "View/CLayer.h"
#include "View/CDrawMarker.h"
#include "View/CUndoAction.h"
#include "MockSketch.h"
#include "CProjectCtrl.h"
#include "Utility/CUtility.h"
#include "System/CSystem.h"
#include "SubWindow/TextInputDlg.h"
#include <math.h>

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 * コンストラクタ
 */
CActionRotate::CActionRotate(CDrawingView* pView):
CActionCommand(pView),
m_bFixAngle(false),
m_dFixdAngle(0.0),
m_iTimes(1)
{
    m_lMouseOver =   SEL_ALL;
    m_eViewMode = VIEW_NONE;
    m_psTmpLine = std::make_shared<CDrawingLine>();
    m_psTmpLine->SetColor(DRAW_CONFIG->crAdditional);
    m_psTmpLine->SetLineType(PS_DOT);
}
    

/**
 * コンストラクタ
 */
CActionRotate::CActionRotate():
CActionRotate(NULL)
{
    m_lMouseOver =   SEL_ALL;
    m_eViewMode = VIEW_NONE;

}

/**
 * デストラクタ
 */
CActionRotate::~CActionRotate()
{
    
}

/**
 *  @brief   解放時動作
 *  @param   [in]    eMode 解放時点でのモード
 *  @retval   なし
 *  @note
 */
void CActionRotate::Cancel(VIEW_MODE eMode) 
{
    CancelSelectRect();    
    ClearAction();    
}

/*
      動作
      1 図形を選択
         回転中心を選択
           （今のところ、直線-直線で回転角を決める機能はなし）
             

      2-1   回転中心を選択
        回転角を入力するかマウスを移動して角度を設定してください
        1 回転角を入力
        2 回転角をマウスドラッグ
            1 決定（マウスクリック or Return)

    
     割り込み選択モード




*/

/**
 *  @brief   選択時動作
 *  @param   [in]    eMode 選択時点でのモード
 *  @retval   なし
 *  @note
 */
bool CActionRotate::SelAction(VIEW_MODE eViewMode, void* pParam) 
{
    STD_ASSERT(m_pView != NULL);

    //既に選択中の図形はあるか？
    long lSize = m_pView->GetPartsDef()->SelectNum();

    m_eViewMode = eViewMode;
    if (lSize == 0)
    {
        //図形を選択してください
        m_pView->SetComment(GET_SYS_STR(STR_SEL_OBJECT));
        m_pView->IntrruptSelect();
        return true ;
    }

    m_iClickCnt = 0;
    m_lMouseOver =   SEL_FIG;
    m_pView->SetComment(GET_SYS_STR(STR_SEL_ROTATE_CENTER));

    return true;
}


/**
 *  マウス移動
 *  @param   [in]    pObj   選択オブジェクト
 *  @param   [in]    eAction   選択オブジェクト
 *  @param   [in]    nFlags   仮想キー 
 *  @retval   なし
 *  @note
 */
bool CActionRotate::MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction) 
{ 

    CPartsDef*   pDef  = m_pView->GetPartsDef();
    if (!pDef)
    {
        return false;
    }

    POINT ptSel = posMouse.ptSel;
    UINT nFlags = posMouse.nFlags;

    //!< スナップ点取得
    bool bSnap = true;

    DWORD dwSnapType = DRAW_CONFIG->GetSnapType();

    CDrawMarker* pMarker= m_pView->GetMarker();

    SNAP_DATA snap;
    bSnap = m_pView->GetSnapPoint(&snap,
                      dwSnapType,
                      ptSel);

    auto pObj1 = pDef->GetObjectById(snap.iObject1);
    auto pObj2 = pDef->GetObjectById(snap.iObject2);

    POINT ptOffset;
    ptOffset.x = ptOffset.y = 0;
    CDrawMarker::MARKER_DATA marker;
    marker.ptOffset = ptOffset;


    bool bIgnoreSnap = false;
    if (nFlags & MK_SHIFT)
    {
        //スナップ無視
        bIgnoreSnap = true;
        bSnap = false;
    }

    m_pView->SetDrawingMode(DRAW_SEL);
    pMarker->Clear();
    pDef->ClearMouseOver();
    pDef->ClearMouseEmphasis();
    pDef->ClearMouseConnectable();

    int iMarkerId = -1;
    //入力値の取得
    StdString sVal;
    sVal = m_pView->GetInputValue();

    if(sVal != m_strOldInput)
    {
        bool bRet ;
        bRet = _CalcValue(sVal);
    }


    //一点目
    if (m_iClickCnt == 0)
    {
        if (bSnap)
        {
            if (pObj1 &&
                !pObj2)
            {
                m_pView->SetMarker(pObj1.get(), false /*bIgnoreOffLineObject*/);
            }

            if (pObj2)
            {
                //交点
                pDef->SetMouseEmphasis(pObj1);
                pDef->SetMouseEmphasis(pObj2);
                marker.eType = CDrawMarker::MARKER_CROSS;
                marker.eSnap = VIEW_COMMON::SNP_CROSS_POINT;
                pMarker->AddMarker(marker, snap.pt);
            }
        }
        else
        {
            m_pView->ConvScr2World(&snap.pt, ptSel,m_pView->GetCurrentLayerId());
        }

        iMarkerId = pMarker->MouseMove(posMouse.ptSel);
        SetSnapTooltip(snap.eSnapType, 0, 0);
    }
    else
    {
        //------------------
        //１点目選択済み
        //------------------

  
        double dAngle = 0.0;
        double dLength = 0.0;
        POINT2D ptMousrPos = snap.pt; 

        bool bInputVal = false;
        if (!m_bFixAngle  )
        {
            if(!bSnap)
            {
                bool bAngle;
                bAngle = m_pView->GetLengthSnapPoint(&dAngle,
                                                     &dLength,
                                                     &snap.pt,  
                                                     m_ptFirst,  
                                            ptMousrPos,
                                            !bIgnoreSnap,
                                            false);

            }
            else
            {
                dAngle = m_ptFirst.Angle(snap.pt);
            }
            bInputVal = true;
            m_dFixdAngle = dAngle;
        }

        m_psTmpLine->SetPoint1(snap.pt);
        m_psTmpLine->SetPoint2(m_ptFirst);
        m_pView->GetMarker()->AddObject(m_psTmpLine);


        {
            using namespace VIEW_COMMON;
            int iDecimalPlace = 3;

            StdString strString;

            StdStringStream   strmLen;
            if (snap.eSnapType != SNP_NONE)
            {
                strString = GetSnapName(snap.eSnapType, false);
                strString += _T("\n");
            }
            strmLen << GET_STR(STR_ANGLE) << _T(":%0") << iDecimalPlace << _T("f");
            strString += CUtil::StrFormat(strmLen.str().c_str(), dAngle);
            m_pView->SetToolTipText(strString.c_str());
        }

        pMarker->HideObject(false);

        int iCnt = 0;
        for(auto pObj : m_lstTmpObjects)
        {
            double dAngle = m_dFixdAngle * (iCnt + 1);

            m_lstOffset[iCnt]->Rotate(m_ptFirst, dAngle) ;

            pObj->OffsetMatrix(m_lstOffset[iCnt].get());
            pDef->SetMouseOver(pObj);
            iCnt++;
        }
    }

    pDef->DrawDragging(m_pView);
    return true;
}


/**
 *  @brief   マウスクリック動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionRotate::SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    STD_ASSERT(m_pView != NULL);

    POINT2D ptBase;

    try
    {
        SelPointRotate( posMouse, eAction, !m_pView->GetCutMode()); 
    }
    catch(MockException &e)
    {
        e.DispMessageBox();
    }

    return false;
}


/**
 *  @brief   回転時 マウスクリック動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags  仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionRotate::SelPointRotate(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction, bool bCopy)
{
    //ここに入ってきた時点で図形は選択済み
    if (eAction != CViewAction::ACT_LBUTTON_UP)
    {
        return false;
    }

    POINT2D ptClick;
    POINT2D ptBase;

    CPartsDef*   pDef  = m_pView->GetPartsDef();
    CDrawMarker* pMarker= m_pView->GetMarker();

    POINT2D pt2D;
    int iLayerId = m_pView->GetCurrentLayerId();
    m_pView->ConvScr2World(&ptClick, posMouse.ptSel, iLayerId);

    POINT2D ptMarker;
    if(_CheckMarker( &ptMarker, posMouse.ptSel, eAction))
    {
        pt2D = ptMarker;
    }
   
    pMarker->Clear();
    pMarker->ClearObject();
    pDef->ClearMouseOver();
    pDef->ClearMouseEmphasis();
    pDef->ClearMouseConnectable();
    _DeleteTmpObjects();

    if(m_iClickCnt == 0)
    {
        //1点目を選択
        //回転の中心・回転の基準となる直線


        //入力値初期化
        m_bFixAngle = false;
        m_dFixdAngle = 0.0;

        // 入力エリアにフォーカスを設定する
        m_pView->SetInputFocus();


        //中心点を選択
        //回転角を入力するか、回転角を選択する
        //m_pView->SetComment(GET_SYS_STR(STR_SEL_ROTATE_ANGLE));
        m_ptFirst = pt2D;
        m_iClickCnt++;

        //回転表示用の図形を作成
        CreateTempMovingObjects(&m_lstOffset,
                                &m_lstTmpObjects,
                                pDef,
                                m_iTimes);


        return true;

    }
    else if(m_iClickCnt == 1)
    {
        SetRotate(m_ptFirst, m_dFixdAngle, m_iTimes, bCopy);
        ClearAction();

        //選択状態へ移行
        m_pView->SetViewMode(VIEW_SELECT);
        //m_pView->SetViewMode(VIEW_SELECT);
        return true;
    }

    return false;
}

/**
 *  @brief   回転数値入力
 *  @param   [in] lstVal  数値列
 *  @param   [in] bCopy   true 数値列
 *  @retval  false エラー発生
 *  @note
 */
bool CActionRotate::SelInputRotate( std::vector<double>& lstVal)
{
    if(m_iClickCnt == 0)
    {
        //先に中心点を選択してください
        AfxMessageBox(GET_STR(STR_ERROR_SELECT_BEFORE_CENTER));
        return false;
    }
    else
    {
        if (lstVal.size() < 1)
        {
            //角度[, 複写回数 ]を入力してください
            AfxMessageBox(GET_STR(STR_ERROR_ANGLE_VAL));
            return false;
        }

        int     iTimes = 1;
        bool    bCopy = false;
        if (lstVal.size() >= 2)
        {
            iTimes = int(lstVal[1]);
            bCopy = true;
        }

        double dAngle = lstVal[0];

        SetRotate(m_ptFirst, dAngle, iTimes, bCopy);


        ClearAction();

        //選択状態へ移行
        m_pView->SetViewMode(VIEW_SELECT);

        //m_eViewMode = VIEW_SELECT;
        //m_pView->SetComment(GET_SYS_STR(STR_SEL_OBJECT));
        return true;
    }
}


/**
 *  回転
 *  @param   [in] ptCenter 回転中心
 *  @param   [in] dAngle   角度(deg)
 *  @param   [in] iTimes   複写回数
 *  @param   [in] bCopy    true:Copy false:Move
 *  @retval  true 成功
 *  @note    
 */
bool CActionRotate::SetRotate(POINT2D ptCenter, double dAngle,int iTimes, bool bCopy)
{
    CPartsDef*   pDef = m_pView->GetPartsDef();
    auto pList = pDef->GetSelectInstance();
    CUndoAction* pUndo  = pDef->GetUndoAction();
    pDef->GetListChangeByConnection()->clear();

    //---------------------
    if (iTimes < 1){iTimes = 1;}
    double dTotalAngle;

    for(int iCnt = 0; iCnt < iTimes; iCnt++)
    {
        pDef->SetLockUpdateNodeData(true);
        std::map<int, int> mapConvId;
        std::vector<int> lstConnection;
        mapConvId[-1] = -1;

         for(auto wObj: *pList)
         {
            auto pObj = wObj.lock();
            if (!pObj)
            {
                continue;
            }
            dTotalAngle = dAngle * double(iCnt);
            if ((iCnt == 0) && !bCopy)
            {
                pUndo->AddStart(UD_CHG, pDef, pObj.get());
                pObj->Rotate( ptCenter, dAngle);
                pUndo->AddEnd(pObj);
            }
            else
            {
                auto pNewObj = CDrawingObject::CloneShared(pObj.get());
                pNewObj->SetSelect(false);
                pNewObj->Rotate( ptCenter, dTotalAngle);
                pDef->RegisterObject(pNewObj, true, true);

                mapConvId[pObj->GetId()] = pNewObj->GetId();
                if (pObj->GetNodeDataNum() > 0)
                {
                    lstConnection.push_back(pNewObj->GetId());
                }
                else
                {
                    pUndo->Add(UD_ADD, pDef, NULL, pNewObj);
                }
            }

        }
         //  接続ID更新
         bool bKeepUnfindId = false;
         pDef->UpdateConnectionAndPosition(pUndo, &mapConvId, &lstConnection, bKeepUnfindId);
         pDef->SetLockUpdateNodeData(false);
    }

    //---------------------
    // 接続しているオブジェクトの変更
    //---------------------
    std::set<int> lstObj;
    UpdateAllNodePositionAndRelation(pDef,
                pDef->GetListChangeByConnection(),
                &lstObj, pUndo);



    pUndo->Push();
    pDef->Redraw(m_pView);
    return true;
}


bool CActionRotate::_CalcValue(const StdString& sVal)
{

    //入力モード確認

    // 入力値は 角度(,回数)

    m_bFixAngle = false;

    std::vector<StdString> lstInput;
    boost::algorithm::split( lstInput, sVal, boost::is_any_of(_T(",")));
    m_iTimes = 1;

    if (lstInput.size() == 0)
    {
        return false;
    }

    if (lstInput.size() >= 1)
    {
        if (lstInput[0].size() != 0)
        {
            double dAngle;
            bool bRet;
            bRet = ExecuteString(&dAngle, lstInput[0]);

            if (bRet)
            {
                m_bFixAngle = true;
                m_dFixdAngle = dAngle;
            }
            else
            {
                return false;
            }
        }
    }

    if  (lstInput.size() >= 2)
    {
        if (lstInput[1].size() != 0)
        {
            double dTimes;
            bool bRet;
            bRet = ExecuteString(&dTimes, lstInput[1]);

            if (bRet)
            {
                m_iTimes = int (dTimes);
                if (m_iTimes < 1)
                {
                    m_iTimes = 1;
                }
            }
            else
            {
                return false;
            }
        }
    }
    m_strOldInput = sVal;
    return true;
}

/**
 *  動作クリア
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CActionRotate::ClearAction(bool bReleaseSelect)
{
    CViewAction::ClearAction(bReleaseSelect);
    CPartsDef*    pDef   = m_pView->GetPartsDef();

    m_lstOffset.clear();
    _DeleteTmpObjects();

    m_bFixAngle = false;
    m_iTimes = 1;

    pDef->Redraw(m_pView);
}


void CActionRotate::_DeleteTmpObjects()
{
    m_lstTmpObjects.clear();
}


/**
 *  @brief   入力値設定
 *  @param   [in]    strVal 入力値
 *  @retval  false エラー発生
 *  @note    必要であればエラー表示を行う
 *  @note    エラー発生で入力コンボボックスは選択状態になる      
 *  @note    エラーがない場合はコンボボックスに値が登録される
 */
bool CActionRotate::InputValue(StdString strVal)
{
    if (strVal == _T(""))
    {
        return false;
    }

    std::vector<StdString> lstInput;
    double dVal;
    std::vector<double>    lstVal;
    boost::algorithm::split( lstInput, strVal, boost::is_any_of(_T(",")));
    
    foreach(StdString sVal, lstInput)
    {
        dVal = _tstof(sVal.c_str());
        lstVal.push_back(dVal);
    }

    bool bRet;
    bRet = SelInputRotate( lstVal);
    return bRet;
}

