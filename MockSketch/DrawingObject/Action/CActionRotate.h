/**
 * @brief			CActionRotateヘッダーファイル
 * @file			CActionRotate.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef _ACTION_ROTATE_H__
#define _ACTION_ROTATE_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CActionCommand.h"
#include "DrawingObject/Primitive/POINT2D.h"

class     CDrawingView;

/**
 * @class   CActionRotate
 * @brief                        
   
 */
class CActionRotate: public CActionCommand
{
    struct OBJ_POINT
    {
        POINT2D          pt;
        CDrawingObject*  obj;
    };

protected:

    POINT2D m_ptFirst;

    StdString   m_strOldInput;

    bool m_bFixAngle;

    double m_dFixdAngle;

    int m_iTimes;

    // 補助直線
    std::shared_ptr<CDrawingLine>   m_psTmpLine;

    std::vector<OBJ_POINT> m_lstObjPoint;

    std::vector< std::shared_ptr<CDrawingObject> > m_lstTmpObjects;

    std::vector<std::unique_ptr<MAT2D>> m_lstOffset;

public:
	//!< コンストラクタ
    CActionRotate();

    //!< コンストラクタ
    CActionRotate(CDrawingView* pView);
    
	//!< デストラクタ
    virtual ~CActionRotate();

	//!< 選択解放時動作
    virtual void Cancel(VIEW_MODE eMode);

	//!< 選択解放時動作
	virtual bool SelAction(VIEW_MODE eMode, void* pParam = NULL);

	//!< マウスク移動動作
    bool MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

	//!< マウスクリック動作
	virtual bool SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

    //!<回転時 マウスクリック動作
    bool SelPointRotate(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction, bool bCopy);

    //!<入力値設定
    virtual bool InputValue(StdString strVal);

    //回転数値入力
    bool SelInputRotate( std::vector<double>& lstVal);

protected:

    bool _CalcValue(const StdString& sVal);

    void _DeleteTmpObjects();

    //!< 動作クリア
    void ClearAction(bool bReleaseSelect = true) override;

    //!< 回転
    bool SetRotate(POINT2D ptCenter, double dAngle,int iTimes, bool bCopy);

};
#endif // _ACTION_ROTATE_H__
