/**
 * @brief			CActionScl実装ファイル
 * @file			CActionScl.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CActionScl.h"

#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/View/CDrawingView.h"

#include "DrawingObject/CDrawingObject.h"
#include "DrawingObject/CDrawingPoint.h"
#include "DrawingObject/CDrawingLine.h"
#include "DrawingObject/CDrawingCircle.h"
#include "DrawingObject/CDrawingEllipse.h"
#include "DrawingObject/CDrawingParts.h"
#include "DrawingObject/CNodeBound.h"

#include "View/CLayer.h"
#include "View/CDrawMarker.h"
#include "View/CUndoAction.h"
#include "MockSketch.h"
#include "CProjectCtrl.h"
#include "Utility/CUtility.h"
#include "System/CSystem.h"
#include "SubWindow/TextInputDlg.h"
#include <math.h>

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 * コンストラクタ
 */
CActionScl::CActionScl():
CActionScl(NULL)
{
}

/**
 * コンストラクタ
 */
CActionScl::CActionScl(CDrawingView* pView):
CActionCommand(pView),
m_bFixScl(false),
m_dXScl(1.0),
m_dYScl(1.0)
{
    m_lMouseOver =   SEL_FIG;
}
    
/**
 * デストラクタ
 */
CActionScl::~CActionScl()
{
    
}

/**
 *  @brief   解放時動作
 *  @param   [in]    eMode 解放時点でのモード
 *  @retval   なし
 *  @note
 */
void CActionScl::Cancel(VIEW_MODE eMode) 
{
    CancelSelectRect();    
}

/**
 *  @brief   選択時動作
 *  @param   [in]    eMode 選択時点でのモード
 *  @retval   なし
 *  @note
 */
bool CActionScl::SelAction(VIEW_MODE eViewMode, void* pParam) 
{
    STD_ASSERT(m_pView != NULL);

    //既に選択中の図形はあるか？
    long lSize = m_pView->GetPartsDef()->SelectNum();

    m_eViewMode = eViewMode;
    if (lSize == 0)
    {
        //図形を選択してください
        m_pView->SetComment(GET_SYS_STR(STR_SEL_OBJECT));
        if (m_eViewMode != VIEW_SELECT)
        {
            m_eViewMode = VIEW_SELECT;
        }
        return true;
    }

    _Init();
    m_iClickCnt = 0;

    m_lMouseOver =   SEL_FIG;
    m_pView->SetComment(GET_SYS_STR(STR_SEL_SCL));
    return true;
}

/**
 *  マウス移動
 *  @param   [in]    pObj   選択オブジェクト
 *  @param   [in]    eAction   選択オブジェクト
 *  @param   [in]    nFlags   仮想キー 
 *  @retval   なし
 *  @note
 */
bool CActionScl::MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction) 
{ 
    CPartsDef*   pDef  = m_pView->GetPartsDef();
    if (!pDef)
    {
        return false;
    }

   if (m_eViewMode == VIEW_SELECT)
   {
        if (m_eDrag == E_DS_AREA)
        {
           CActionCommand:: SelectAction( posMouse, eAction);
        }
        else
        {
            CViewAction::MovePoint( posMouse, eAction);
        }

        CPartsDef* pDef = m_pView->GetPartsDef();

        if (pDef)
        {
            pDef->DrawDragging(m_pView);
        }
        return false;
   }

    //_Init();

    CDrawMarker* pMarker= m_pView->GetMarker();
    CNodeMarker* pNodeMarker = m_pView->GetNodeMarker();


    m_pView->SetDrawingMode(DRAW_SEL);
    pMarker->Clear();


    bool bRet ;
    SNAP_DATA snap;

	bRet = m_pView->GetMousePoint2(&snap, posMouse);

    //入力値の取得
    StdString sVal;
    sVal = m_pView->GetInputValue();

    if(sVal != m_strOldInput)
    {
        bRet = _CalcValue(sVal);
    }

    if (m_iClickCnt == 0)
    {
        m_psNodeBound->OnMouseMove(&snap, posMouse);
    }

    pDef->DrawDragging(m_pView);
    m_pView->SetToolTipText(snap.strSnap.c_str());
    return true;
}

bool CActionScl::_Init()
{

    if(m_psNodeBound)
    {
        return false;
    }
/*
    m_psNodeBound = std::make_unique<CNodeBound>(false,
                                               false);

*/
    m_psNodeBound = std::make_unique<CNodeBound>(true,  //Rotate
                                                 true,  //Move
												 true); //Scale 

	//入力値初期化
    m_bFixScl = false;
    m_dXScl = 1.0;
    m_dYScl = 1.0;

    CPartsDef*   pDef  = m_pView->GetPartsDef();
    CDrawMarker* pMarker= m_pView->GetMarker();

    pMarker->Clear();
    pMarker->ClearObject();
    pDef->ClearMouseOver();
    pDef->ClearMouseEmphasis();
    pDef->ClearMouseConnectable();
    _DeleteTmpObjects();

    // 入力エリアにフォーカスを設定する
    m_pView->SetInputFocus();

    auto pList = pDef->GetSelectInstance();

    //拡大表示用の図形を作成,

    auto pSelectedObject = CViewAction::GroupSelectObject(pDef, true);

    if (pSelectedObject)
    {
        m_rcBound = pSelectedObject->GetBounds();
        //マーカーを用意
        m_psNodeBound->SetAngle(0.0);
        m_psNodeBound->EnableMove(false);
        m_psNodeBound->EnableRotate(false);
        m_psNodeBound->Init(m_pView,
                            m_rcBound,
                            pSelectedObject);
        m_ptCenter = m_psNodeBound->GetC();
    }
    
    return true;
}


/**
 *  @brief   マウスクリック動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionScl::SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    STD_ASSERT(m_pView != NULL);

    long lSize;
    POINT2D ptBase;

    try
    {
        switch (m_eViewMode)
        {
            case VIEW_SELECT:
                //選択動作
                SelectAction( posMouse, eAction);
                lSize = m_pView->GetPartsDef()->SelectNum();
                if (lSize != 0)
                {
                    m_pView->SetComment(GET_SYS_STR(STR_SEL_ROTATE_CENTER));
                    m_eViewMode = VIEW_SCL;
                }
                break;

            case VIEW_SCL:
                {
                    bool bCopy = !m_pView->GetCutMode();
                    SelPointScl( posMouse, eAction, bCopy);
                }
                break;
        }
    }
    catch(MockException &e)
    {
        e.DispMessageBox();
    }

    return false;
}


/**
 *  @brief   倍率変更 マウスクリック動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags  仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionScl::SelPointScl(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction, bool bCopy)
{

    if (eAction == CViewAction::ACT_LBUTTON_DOWN)
    {
        m_psNodeBound->OnMouseLButtonDown(posMouse);
        return true;
    }

    //ここに入ってきた時点で図形は選択済み
    if (eAction != CViewAction::ACT_LBUTTON_UP)
    {
        return false;
    }

    m_psNodeBound->OnMouseLButtonUp(posMouse);

    POINT2D ptClick;
    POINT2D ptBase;

    CPartsDef*   pDef  = m_pView->GetPartsDef();
    CDrawMarker* pMarker= m_pView->GetMarker();

    POINT2D pt2D;
    int iLayerId = m_pView->GetCurrentLayerId();
    m_pView->ConvScr2World(&ptClick, posMouse.ptSel, iLayerId);

    POINT2D ptMarker;
    if(_CheckMarker( &ptMarker, posMouse.ptSel, eAction))
    {
        pt2D = ptMarker;
    }
   

    if(m_iClickCnt == 0)
    {
        m_psNodeBound->ApplyMatrix(false);
        m_dXScl = m_psNodeBound->GetXScl();
        m_dYScl = m_psNodeBound->GetYScl();
        m_ptCenter = m_psNodeBound->GetC();

        SetScl(m_ptCenter, m_dXScl, m_dYScl, bCopy);

        ClearAction();

        //選択状態へ移行
        m_pView->SetViewMode(VIEW_SELECT);
        //m_eViewMode = VIEW_SELECT;
        //m_pView->SetComment(GET_SYS_STR(STR_SEL_OBJECT));
        return true;
    }

    return false;
}

/**
 *  @brief   倍率数値入力
 *  @param   [in] lstVal  数値列
 *  @param   [in] bCopy   true 数値列
 *  @retval  false エラー発生
 *  @note
 */
bool CActionScl::SelInputScl( std::vector<double>& lstVal)
{
    bool bRet;


    if (lstVal.size() < 1)
    {
        AfxMessageBox(GET_STR(STR_SEL_SCL));
        return false;
    }

    double dSclX = lstVal[0];
    double dSclY = dSclX;

    if (lstVal.size() > 1)
    {
        dSclY = lstVal[1];
    }

    bRet = SetScl(m_ptCenter, dSclX, dSclY, false);

    ClearAction();
    m_iClickCnt = 0;

    //選択状態へ移行
    m_pView->SetViewMode(VIEW_SELECT);

    //m_eViewMode = VIEW_SELECT;
    //m_pView->SetComment(GET_SYS_STR(STR_SEL_OBJECT));

    return bRet;
}

/**
 *  倍率
 *  @param   [in] ptCenter 中心座標
 *  @param   [in] dXScl    X軸倍率
 *  @param   [in] dYScl    Y軸倍率
 *  @retval  true 成功
 *  @note    
 */
bool CActionScl::SetScl(POINT2D ptCenter, double dXScl, double dYScl, bool bCopy)
{
    CPartsDef*   pDef = m_pView->GetPartsDef();
    auto pList = pDef->GetSelectInstance();
    CUndoAction* pUndo  = pDef->GetUndoAction();

    pDef->SetLockUpdateNodeData(true);
    std::map<int, int> mapConvId;
    std::vector<int> lstConnection;
    mapConvId[-1] = -1;
    pDef->GetListChangeByConnection()->clear();

    //---------------------
    for(auto wObj: *pList)
    {
        auto pObj = wObj.lock();
        if (!pObj)
        {
            continue;
        }
        if (!bCopy)
        {
            pUndo->AddStart(UD_CHG, pDef, pObj.get());
            pObj->Scl( ptCenter, dXScl, dYScl);
            pUndo->AddEnd(pObj);
        }
        else
        {
            auto pNewObj = CDrawingObject::CloneShared(pObj.get());
            pNewObj->SetSelect(false);
            pNewObj->Scl( ptCenter, dXScl, dYScl);
            pDef->RegisterObject(pNewObj, true, true);

            mapConvId[pObj->GetId()] = pNewObj->GetId();
            if (pObj->GetNodeDataNum() > 0)
            {
                lstConnection.push_back(pNewObj->GetId());
            }
            else
            {
                pUndo->Add(UD_ADD, pDef, NULL, pNewObj);
            }
        }
        //  接続ID更新
        bool bKeepUnfindId = false;
        pDef->UpdateConnectionAndPosition(pUndo, &mapConvId, &lstConnection, bKeepUnfindId);
        pDef->SetLockUpdateNodeData(false);

    }

    //---------------------
    // 接続しているオブジェクトの変更
    //---------------------
    std::set<int> lstObj;
    UpdateAllNodePositionAndRelation(pDef,
        pDef->GetListChangeByConnection(),
        &lstObj, pUndo);



    pUndo->Push();
    pDef->Redraw(m_pView);

    //!< 変更を通知
    //::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_VIEW_UPDATE_OBJECTS, (WPARAM)pParts, 0);
    return true;
}



bool CActionScl::_CalcValue(const StdString& sVal)
{

    //入力モード確認
    //角度入力が出来るのは１点選択後 若しくは、 １点選択後に直線を選択

    // 入力値は  倍率 若しくは X倍率,Y倍率

    std::vector<StdString> lstInput;
    boost::algorithm::split( lstInput, sVal, boost::is_any_of(_T(",")));

    if (lstInput.size() == 0)
    {
        return false;
    }

    m_bFixScl = false;

    if (lstInput.size() >= 1)
    {
        if (lstInput[0].size() != 0)
        {
            double dScl;
            bool bRet;
            bRet = ExecuteString(&dScl, lstInput[0]);

            if (bRet)
            {
                m_bFixScl = true;
                m_dXScl = dScl;
                m_dYScl = dScl;

            }
            else
            {
                return false;
            }
        }
    }

    if  (lstInput.size() >= 2)
    {
        if (lstInput[1].size() != 0)
        {
            double dSclY;
            bool bRet;
            bRet = ExecuteString(&dSclY, lstInput[1]);

            if (bRet)
            {
                m_bFixScl = true;
                m_dYScl = dSclY;
            }
            else
            {
                return false;
            }
        }
    }

    m_strOldInput = sVal;
    return true;
}


/**
 *  動作クリア
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CActionScl::ClearAction(bool bReleaseSelect)
{
    CViewAction::ClearAction(bReleaseSelect);

    m_lstOffset.clear();
    _DeleteTmpObjects();

    VIEW_MODE      eViewMode  = m_pView->GetViewMode();

    m_bFixScl = false;
    m_psNodeBound->Release();
    m_psNodeBound.reset();
    m_eDrag = E_DS_NONE;

    auto pDragableMarker = m_pView->GetNodeMarker();
    pDragableMarker->Clear();

    CPartsDef*    pDef   = m_pView->GetPartsDef();
    pDef->Redraw(m_pView);
    SelAction(eViewMode);
}


void CActionScl::_DeleteTmpObjects()
{
    m_lstTmpObjects.clear();
}

/**
 *  @brief   入力値設定
 *  @param   [in]    strVal 入力値
 *  @retval  false エラー発生
 *  @note    必要であればエラー表示を行う
 *  @note    エラー発生で入力コンボボックスは選択状態になる      
 *  @note    エラーがない場合はコンボボックスに値が登録される
 */
bool CActionScl::InputValue(StdString strVal)
{
    if (strVal == _T(""))
    {
        return false;
    }

    std::vector<StdString> lstInput;
    double dVal;
    std::vector<double>    lstVal;
    boost::algorithm::split( lstInput, strVal, boost::is_any_of(_T(",")));
    
    foreach(StdString sVal, lstInput)
    {
        dVal = _tstof(sVal.c_str());
        lstVal.push_back(dVal);
    }

    bool bRet = false;
    bRet = SelInputScl( lstVal);
    return bRet;
}
