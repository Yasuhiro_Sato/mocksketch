/**
 * @brief			CActionSclヘッダーファイル
 * @file			CActionScl.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef _ACTION_SCL_H__
#define _ACTION_SCL_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CActionCommand.h"
#include "DrawingObject/Primitive/POINT2D.h"
#include "DrawingObject/Primitive/RECT2D.h"

class     CDrawingView;
class     CNodeBound;
/**
 * @class   CActionScl
 * @brief                        
   
 */
class CActionScl: public CActionCommand
{
protected:
    struct OBJ_POINT
    {
        POINT2D          pt;
        CDrawingObject*  obj;
    };


    POINT2D m_ptCenter;

    StdString   m_strOldInput;

    bool    m_bFixScl;

    double m_dXScl;

    double m_dYScl;

    std::vector<OBJ_POINT> m_lstObjPoint;

    std::vector< std::shared_ptr<CDrawingObject> > m_lstTmpObjects;

    std::vector<std::unique_ptr<MAT2D>> m_lstOffset;

    std::unique_ptr<CNodeBound>         m_psNodeBound;

    RECT2D m_rcBound;

public:
	//!< コンストラクタ
    CActionScl();

    //!< コンストラクタ
    CActionScl(CDrawingView* pView);
    
	//!< デストラクタ
    virtual ~CActionScl();

	//!< 選択解放時動作
    virtual void Cancel(VIEW_MODE eMode);

	//!< 選択解放時動作
	virtual bool SelAction(VIEW_MODE eMode, void* pParam = NULL);

	//!< マウスク移動動作
    bool MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

    //!< マウスクリック動作
	virtual bool SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

    //!<倍率変更 マウスクリック動作
    bool SelPointScl(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction, bool bCopy);

    //!<入力値設定
    virtual bool InputValue(StdString strVal);

    //移動、コピー数値入力
    bool SelInputMove( std::vector<double>& lstVal, bool bCopy );

    //倍率数値入力
    bool SelInputScl( std::vector<double>& lstVal);

protected:
    bool _Init();

    //!< 動作クリア
    void ClearAction(bool bReleaseSelect = true) override;

    void _DeleteTmpObjects();

    bool _CalcValue(const StdString& sVal);


    //!< 倍率
    bool SetScl(POINT2D ptCenter, double dXScl, double dYScl, bool bCopy);

protected:

};
#endif // _ACTION_SCL_H__
