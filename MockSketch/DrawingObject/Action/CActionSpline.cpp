/**
/**
 * @brief			CActionSpline実装ファイル
 * @file			CActionSpline.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CActionSpline.h"

#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/View/CDrawingView.h"

#include "DrawingObject/CDrawingObject.h"
#include "DrawingObject/CDrawingPoint.h"
#include "DrawingObject/CDrawingLine.h"
#include "DrawingObject/CDrawingCircle.h"
#include "DrawingObject/CDrawingSpline.h"
#include "DrawingObject/CDrawingCompositionLine.h"


#include "View/CLayer.h"
#include "View/CUndoAction.h"
#include "View/CDrawMarker.h"
#include "MockSketch.h"
#include "Utility/CUtility.h"
#include "System/CSystem.h"
#include <math.h>

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 * コンストラクタ
 */
CActionSpline::CActionSpline():
CViewAction()
{
    m_lMouseOver =   SEL_POINT;
}

/**
 * コンストラクタ
 */
CActionSpline::CActionSpline(CDrawingView* pView):
CViewAction(pView)
{
    m_lMouseOver =   SEL_POINT;
}
    
/**
 * デストラクタ
 */
CActionSpline::~CActionSpline()
{
    
}

/**
 *  @brief   解放時動作
 *  @param   [in]    eMode 解放時点でのモード
 *  @retval   なし
 *  @note
 */
void CActionSpline::Cancel(VIEW_MODE eMode) 
{
    if (m_pSpline)
    {
        int iAns = AfxMessageBox(GET_STR(STR_DLG_SPLINE_REG), MB_YESNO);
        if (iAns == IDYES)
        {
            AddSpline();
        }
    }
    ClearAction();    
}

/**
 *  @brief   選択時動作
 *  @param   [in]    eMode 選択時点でのモード
 *  @retval   なし
 *  @note
 */
bool CActionSpline::SelAction(VIEW_MODE eMode, void* pParam) 
{
    //既に選択中の図形はあるか？
    long lSize = m_pView->GetPartsDef()->SelectNum();

    if (lSize != 0)
    {
        //選択を解除
        m_pView->GetPartsDef()->RelSelect();
    }   
    m_pView->SetComment(GET_SYS_STR(STR_SEL_SPLINE_POINT));
    m_eViewMode = eMode;
    m_iDrag = -1;
    return true;
}

bool CActionSpline::_AddMarker(CDrawingSpline* pSpline)
{
    CDrawMarker* pMarker= m_pView->GetMarker();

    CDrawMarker::MARKER_DATA marker;
    marker.eType = CDrawMarker::MARKER_CIRCLE_FILL;

    marker.crDraw = DRAW_CONFIG->crAdditional;
    auto pS = pSpline->GetSplineInstance();
	int iMax;

	if (pSpline->GetEditType() == SPLINE2D::EDT_PASS_POINT)
	{
		iMax = pSpline->GetPointNum();
	}
	else
	{
		iMax = pSpline->GetControlPointNum();
	}

    for  (int iIndex = 0; iIndex < iMax; ++iIndex)
    {
        if ((iIndex == (iMax - 1)) ||
            (iIndex == 0))
        {
            //始点、終点はクリックして終了するため、マウスカーソルを変更しない
            marker.SetCursor((UINT)0);
        }
        else
        {
            marker.SetCursor(IDC_SIZEALL);
        }
        marker.iVal = iIndex;

		if (pSpline->GetEditType() == SPLINE2D::EDT_PASS_POINT)
		{
			if (pSpline->GetPointNum() <= iIndex)
			{
				return false;
			}
			pMarker->AddMarker(marker, pSpline->GetPoint(iIndex));
		}
		else
		{
			if (pSpline->GetControlPointNum() <= iIndex)
			{
				return false;
			}
			pMarker->AddMarker(marker, pSpline->GetControlPoint(iIndex));
		}
    }
    return true;
}

/**
 *  @brief   マウスクリック動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionSpline::SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    CPartsDef*   pDef  = m_pView->GetPartsDef();
    int                  iId  = m_pView->SearchPos(posMouse.ptSel);
    auto     pObj  = pDef->GetObjectById(iId);
    VIEW_MODE      eViewMode  = m_pView->GetViewMode();

    CDrawMarker* pMarker= m_pView->GetMarker();

	int iMax = 0;
	if(m_pSpline)
	{
		if (m_pSpline->GetEditType() == SPLINE2D::EDT_PASS_POINT)
		{
			iMax = m_pSpline->GetPointNum() - 1;
		}
		else
		{
			iMax = m_pSpline->GetControlPointNum() - 1;
		}
	}

    if (eAction == CViewAction::ACT_LBUTTON_DOWN)
    {
        //int iCtrlId;
        auto pPointObj = std::dynamic_pointer_cast<CDrawingPoint>(pObj);
        if(pPointObj)
        {
            if (m_pSpline)
            {
                return false;
            }
        }

        POINT2D pt2D;
        int iMarkerId = pMarker->CheckCkick( posMouse.ptSel , CDrawMarker::MARKER_CIRCLE_FILL);
        if (iMarkerId != -1)
        {
             auto pM = pMarker->GetMarker(iMarkerId);
             if(pM->eType ==  CDrawMarker::MARKER_CIRCLE_FILL)
             {
                if ( pM->iVal == 0)
                {
                    return false;
                }
                else if( pM->iVal == iMax)
                {
                    return false;
                }
                else
                {
                    m_iDrag = pM->iVal;
                }
             }
        }
    }

    if (eAction != CViewAction::ACT_LBUTTON_UP)
    {
        //左クリックのみ
        return false;
    }

    if(m_iDrag != -1)
    {
        m_iDrag = -1;
        return false;
    }

    if (m_pSpline == NULL)
    {
		auto pCreateObj = THIS_APP->CreateDefaultObject(DT_SPLINE);
		auto defObj = std::dynamic_pointer_cast<CDrawingSpline>(pCreateObj);
		m_pSpline = std::make_shared<CDrawingSpline>(*(defObj.get()));

        m_pSpline->SetColor(DRAW_CONFIG->crImaginary);

		if (!THIS_APP->GetDefaultObjectPriorty(DT_SPLINE))
		{
			m_pSpline->SetLineType(m_pView->GetCurrentLayer()->iLineType);
			m_pSpline->SetLineWidth(m_pView->GetCurrentLayer()->iLineWidth);
		}

        m_pSpline->SetLayer(m_pView->GetCurrentLayerId());

		m_pNextSpline = std::make_shared<CDrawingSpline>(*(defObj.get()));
		m_pNextSpline->SetColor(DRAW_CONFIG->crImaginary);
		m_pNextSpline->SetLineType(PS_DOT);
		if (!THIS_APP->GetDefaultObjectPriorty(DT_SPLINE))
		{
			m_pNextSpline->SetLineType(m_pView->GetCurrentLayer()->iLineType);
			m_pNextSpline->SetLineWidth(m_pView->GetCurrentLayer()->iLineWidth);
		}
		pMarker->AddObject(m_pNextSpline);
	}

	if (m_psAddObj == NULL)
	{
		m_psAddObj = std::make_shared<CDrawingCompositionLine>();
		m_psAddObj->SetColor(DRAW_CONFIG->crImaginary);
		m_psAddObj->SetLineType(PS_DOT);
		m_psAddObj->SetLineWidth(1);
		m_psAddObj->SetLayer(m_pView->GetCurrentLayerId());
	}

    pMarker->ClearObject();
    pMarker->AddObject(m_pSpline);

   
    bool bIgnoreSnap = false;
    if (posMouse.nFlags & MK_SHIFT)
    {
        //スナップ無視
        bIgnoreSnap = true;
    }
	else
	{
		if (m_pSpline)
		{
			_AddMarker(m_pSpline.get());
		}
	}

    //!< ビュー->データ変換
    POINT2D pt2D;
    int iMarkerId = pMarker->CheckCkick( posMouse.ptSel , CDrawMarker::MARKER_CIRCLE_FILL);
    if (iMarkerId != -1)
    {
         auto pM = pMarker->GetMarker(iMarkerId);
         if(pM->eType ==  CDrawMarker::MARKER_CIRCLE_FILL)
         {
            if ( pM->iVal == 0)
            {
				if (!m_pSpline->IsClose())
				{
					m_pSpline->SetClose();
					AddSpline();
					ClearAction();
					return false;
				}
            }
            else if( pM->iVal == iMax)
            {
                AddSpline();
                ClearAction();
                return false;
            }
			pObj.reset();
		 }

        pt2D = pM->ptObject;
    }
    else
    {
        int iLayerId = m_pView->GetCurrentLayerId();
        m_pView->ConvScr2World(&pt2D, posMouse.ptSel, iLayerId);
    }

	//何も選択していない状態でSplineを選択したときは、そのSplineを選択する
	//(選択割り込み)
	if (m_pSpline->GetControlPointNum() == 0)
	{
		if (pObj)
		{
			DRAWING_TYPE eType = pObj->GetType();
			if (eType == DT_SPLINE)
			{
				//選択を割り込み
				m_pView->InterruptAction(VIEW_SELECT, posMouse);
				return true;
			}
		}
	}


	if (m_pNextSpline->GetEditType() == SPLINE2D::EDT_PASS_POINT)
	{
		m_pSpline->AddPoint(pt2D);
		m_pNextSpline->AddPoint(pt2D);
		if (m_pNextSpline->GetPointNum() == 1)
		{
			m_pNextSpline->AddPoint(pt2D);
		}
		else
		{
			int iIndex = m_pNextSpline->GetPointNum() - 2;
			m_pNextSpline->SetPoint(iIndex, pt2D);
		}
	}
	else
	{
		m_pSpline->AddControlPoint(pt2D);
		m_pNextSpline->AddControlPoint(pt2D);
		if (m_pNextSpline->GetControlPointNum() == 1)
		{
			m_pNextSpline->AddControlPoint(pt2D);
		}
		else
		{
			int iIndex = m_pNextSpline->GetControlPointNum() - 2;
			m_pNextSpline->SetControlPoint(iIndex, pt2D);
		}

	}

    if(!bIgnoreSnap)
    {
        _AddMarker(m_pSpline.get());
    }

    pDef->DrawDragging(m_pView);
    return false;
}

/**
 *  @brief   マウス移動動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionSpline::MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    //!< スナップ点取得
    POINT ptSel = posMouse.ptSel;
    UINT nFlags = posMouse.nFlags;

    bool bSnap = true;
	bool bIgnoreSnap = false;

	CPartsDef* pDef = m_pView->GetPartsDef();
	if (!pDef)
	{
		return false;
	}
	pDef->RedrawWait();  //TODO:どうする？

    POINT2D ptSnap;


    CDrawMarker* pMarker= m_pView->GetMarker();
	pMarker->Clear();
	pMarker->ClearObject();
	pDef->ClearMouseOver();
	pDef->ClearMouseEmphasis();
	pDef->ClearMouseConnectable();

	DWORD dwSnapType = DRAW_CONFIG->GetSnapType();
	dwSnapType |= VIEW_COMMON::SNP_NODE;


    SNAP_DATA snap;
    bSnap = m_pView->GetSnapPoint(&snap,
                      dwSnapType,
                      ptSel);

	auto pObj1 = pDef->GetObjectById(snap.iObject1);
	auto pObj2 = pDef->GetObjectById(snap.iObject2);
	ptSnap = snap.pt;

	//==================================

	POINT ptOffset;
	ptOffset.x = ptOffset.y = 0;
	CDrawMarker::MARKER_DATA marker;
	marker.ptOffset = ptOffset;


    if (nFlags & MK_SHIFT)
    {
        //スナップ無視
        bIgnoreSnap = true;
        bSnap = false;
        snap.iObject1 = -1;
        snap.iObject2 = -1;
    }

	m_pView->SetDrawingMode(DRAW_SEL);

    POINT2D ptNew;
       
    if ((pObj1 != NULL ) &&
        (pObj2 == NULL ))
    {
		if ((snap.eSnapType == VIEW_COMMON::SNP_ON_LINE) &&
			(pObj1->GetType() == DT_SPLINE))
		{
			//選択割り込みが可能
			pDef->SetMouseOver(pObj1);
			snap.eSnapType = VIEW_COMMON::SNP_NONE;
		}
		else
		{
			m_pView->SetMarker(pObj1.get(), false /*bIgnoreOffLineObject*/);
			pDef->SetMouseEmphasis(pObj1);
		}
    }

	if ((pObj1 != NULL) &&
		(pObj2 != NULL))
	{
        //交点
        pDef->SetMouseEmphasis(pObj1);
        pDef->SetMouseEmphasis(pObj2);
        marker.eType = CDrawMarker::MARKER_CROSS;
        marker.eSnap = VIEW_COMMON::SNP_CROSS_POINT;
		pMarker->AddMarker(marker, snap.pt);
    }

    if (bIgnoreSnap)
    {
        int iLayerId = m_pView->GetCurrentLayerId();
        m_pView->ConvScr2World(&ptSnap, posMouse.ptSel, iLayerId);
    }
	bool bTmpClose = false;

	if (snap.eSnapType == VIEW_COMMON::SNP_NONE)
	{
		//任意点
		CDrawMarker* pMarker = m_pView->GetMarker();
		marker.eType = CDrawMarker::MARKER_CROSS;
		marker.eSnap = VIEW_COMMON::SNP_CROSS_POINT;
		pMarker->AddMarker(marker, snap.ptOrg);
	}

    if(m_pNextSpline)
    {
		bTmpClose = (m_pNextSpline->IsClose() && !m_pSpline->IsClose());

        if(m_iDrag != -1)
        {
			//マーカーをドラッグ
			if (m_pNextSpline->GetEditType() == SPLINE2D::EDT_PASS_POINT)
			{
				m_pSpline->SetPoint(m_iDrag, ptSnap);
				m_pNextSpline->SetPoint(m_iDrag, ptSnap);
			}
			else
			{
			m_pSpline->SetControlPoint(m_iDrag, ptSnap);
			m_pNextSpline->SetControlPoint(m_iDrag, ptSnap);
			}
		}
		else if (!bTmpClose)
		{
		if (m_pNextSpline->GetEditType() == SPLINE2D::EDT_PASS_POINT)
		{
			int iIndex = m_pNextSpline->GetPointNum() - 1;
			if (iIndex > 0)
			{
				m_pNextSpline->SetPoint(iIndex, ptSnap);
			}
		}
		else
		{
			int iIndex = m_pNextSpline->GetControlPointNum() - 1;
			if (iIndex > 0)
			{
				m_pNextSpline->SetControlPoint(iIndex, ptSnap);
			}
		}
		}
	}

	int iMarkerId = -1;
	if (bIgnoreSnap)
	{
		pMarker->Clear();
	}
	else
	{
		//ここでマーカーを再描画
		if (m_pSpline)
		{
			_AddMarker(m_pSpline.get());
			iMarkerId = pMarker->MouseMove(posMouse.ptSel);
		}
	}


	bool bDrawNextSpline = true;

	if (m_pNextSpline)
	{
		bool bSelFirst = false;
		if (iMarkerId != -1)
		{
			auto pM = pMarker->GetMarker(iMarkerId);
			if (pM->eType == CDrawMarker::MARKER_CIRCLE_FILL)
			{
				if (pM->iVal == 0)
				{
					if (!m_pNextSpline->IsClose())
					{
						if (m_pNextSpline->GetEditType() == SPLINE2D::EDT_PASS_POINT)
						{
							m_pNextSpline->DelLastPoint();
						}
						else
						{
							m_pNextSpline->DelLastControlPoint();
						}
						m_pNextSpline->SetClose();
					}
					bSelFirst = true;
				}
				else
				{
					bDrawNextSpline = false;
				}
			}
		}

		if (!bSelFirst)
		{
			if (bTmpClose)
			{
				m_pNextSpline->SetClose(false);
				if (m_pNextSpline->GetEditType() == SPLINE2D::EDT_PASS_POINT)
				{
					m_pNextSpline->AddPoint(ptSnap);
				}
				else
				{
					m_pNextSpline->AddControlPoint(ptSnap);
				}
			}
		}
	}

	if (m_psAddObj && 
		m_pNextSpline &&
		m_pNextSpline->GetEditType() == SPLINE2D::EDT_CTRL)
	{
		auto pCtrlLine = std::dynamic_pointer_cast<CDrawingCompositionLine>(m_psAddObj);
		std::vector<POINT2D> lstPoint;
		m_pNextSpline->GetSplineInstance()->GetControlPoints(&lstPoint);

		if (lstPoint.size() >= 2)
		{
			pCtrlLine->CreateMultiLine(lstPoint);
			pDef->SetMouseEmphasis(pCtrlLine);
		}
	}


    /* SetMouseOverで描画するので登録の必要なし
    if (m_pSpline != NULL)
    {
        pMarker->AddObject(m_pSpline);
    }
    */

    if(bDrawNextSpline)
    {
        if (m_pNextSpline != NULL)
        {
            pMarker->AddObject(m_pNextSpline);
        }
    }

   if(m_pSpline)
    {
        pDef->SetMouseOver(m_pSpline);
    }

    SetSnapTooltip(snap.eSnapType, 0, 0);

    pDef->DrawDragging(m_pView);

    return true;
}


bool  CActionSpline::OnContextMenu(POINT point)
{
    STD_ASSERT(m_pView != NULL);
    CPartsDef* pDef = m_pView->GetPartsDef();
    if (!pDef)
    {
        return false;
    }

	if (!m_pSpline)
	{
		return false;
	}

    CDrawMarker*        pMarker = m_pView->GetMarker();
    POINT2D pt2D;
    int iDel = -1;

    CWnd* pWnd = const_cast<CWnd*>(m_pView->GetWindow());
    POINT pointView = point;
    pWnd->ScreenToClient( &pointView );
 
    int iMarkerId = pMarker->CheckCkick( pointView , CDrawMarker::MARKER_CIRCLE_FILL);
    if (iMarkerId != -1)
    {
        auto pM = pMarker->GetMarker(iMarkerId);
        if(pM->eType ==  CDrawMarker::MARKER_CIRCLE_FILL)
        {
            iDel = pM->iVal;
        }
    }
    
    CMenu menu;
    menu.CreatePopupMenu();

	//
    menu.AppendMenu(MF_STRING, ID_MNU_END, GET_STR(STR_MNU_END));
    menu.AppendMenu(MF_STRING, ID_MNU_CANCEL, GET_STR(STR_MNU_CANCEL));
	menu.AppendMenu(MF_STRING, ID_MNU_SPLINE_CLOSED_CURVE, GET_STR(STR_MNU_CLOSED_CURVE));
	UINT nCheck;
	if (m_pNextSpline->IsClose())
	{
		nCheck = MF_CHECKED | MF_BYCOMMAND;
	}
	else
	{
		nCheck = MF_UNCHECKED | MF_BYCOMMAND;
	}
	menu.CheckMenuItem(ID_MNU_SPLINE_CLOSED_CURVE, nCheck);


	CMenu mSplineTypoe;
	mSplineTypoe.CreatePopupMenu();

	mSplineTypoe.AppendMenu(MF_STRING, ID_MNU_SPLINE_TYPE_BSPLINE2, GET_STR(STR_BSPLINE2));
	mSplineTypoe.AppendMenu(MF_STRING, ID_MNU_SPLINE_TYPE_BSPLINE3, GET_STR(STR_BSPLINE3));
	
	UINT idCheckItem;
	if (m_pSpline->GetSplineInstance()->GetType() == SPLINE2D::SPT_BSPLINE2)
	{
		idCheckItem = ID_MNU_SPLINE_TYPE_BSPLINE2;
	}
	else
	{
		idCheckItem = ID_MNU_SPLINE_TYPE_BSPLINE3;
	}
	mSplineTypoe.CheckMenuRadioItem(ID_MNU_SPLINE_TYPE_BSPLINE2, ID_MNU_SPLINE_TYPE_BSPLINE3, idCheckItem, MF_BYCOMMAND);
	menu.AppendMenu(MF_POPUP, (UINT)mSplineTypoe.GetSafeHmenu(), GET_STR(STR_MNU_SPLINE_TYPE));

	CMenu mEditTypoe;
	mEditTypoe.CreatePopupMenu();
	mEditTypoe.AppendMenu(MF_STRING, ID_MNU_SPLINE_EDIT_TYPE_CTRL, GET_STR(STR_CONTROL_POINT));
	mEditTypoe.AppendMenu(MF_STRING, ID_MNU_SPLINE_EDIT_TYPE_PASS, GET_STR(STR_PASS_POINT));
	if (m_pSpline->GetSplineInstance()->GetEditType() == SPLINE2D::EDT_CTRL)
	{
		idCheckItem = ID_MNU_SPLINE_EDIT_TYPE_CTRL;
	}
	else
	{
		idCheckItem = ID_MNU_SPLINE_EDIT_TYPE_PASS;
	}
	mEditTypoe.CheckMenuRadioItem(ID_MNU_SPLINE_EDIT_TYPE_CTRL, ID_MNU_SPLINE_EDIT_TYPE_PASS, idCheckItem, MF_BYCOMMAND);
	menu.AppendMenu(MF_POPUP, (UINT)mEditTypoe.GetSafeHmenu(), GET_STR(STR_MNU_SPLINE_EDIT_TYPE));

    if(iDel != -1)
    {                                    
        menu.AppendMenu(MF_SEPARATOR);
        menu.AppendMenu(MF_STRING, ID_MNU_DELETE, GET_STR(STR_MNU_PROP_EDIT_DEL));
	}



    UINT uiMenu = menu.TrackPopupMenu(
        TPM_LEFTALIGN  |    //クリック時のX座標をメニューの左辺にする
        TPM_RIGHTBUTTON|    //右クリックでメニュー選択可能とする
        TPM_RETURNCMD,      //関数の戻り値として、ユーザーが選択したメニュー項目の識別子を返します。
        point.x,point.y,    //メニューの表示位置
        pWnd                //このメニューを所有するウィンドウ
    );

	switch (uiMenu)
	{
	case ID_MNU_END:
		if (m_pSpline)
		{
			AddSpline();
		}
		ClearAction();
		break;

	case ID_MNU_CANCEL:
		ClearAction();
		break;

	case ID_MNU_DELETE:
		if (m_pNextSpline->GetEditType() == SPLINE2D::EDT_PASS_POINT)
		{
			m_pSpline->DelPoint(iDel);
			m_pNextSpline->DelPoint(iDel);
		}
		else
		{
			m_pSpline->DelControlPoint(iDel);
			m_pNextSpline->DelControlPoint(iDel);
		}
		break;

	case ID_MNU_SPLINE_CLOSED_CURVE:
	{
		bool bClose = true;
		if (m_pSpline->IsClose())
		{
			bClose = false;
		}

		m_pSpline->SetClose(bClose);
		m_pNextSpline->SetClose(bClose);
	}
	break;

	case ID_MNU_SPLINE_TYPE_BSPLINE2:
		m_pSpline->GetSplineInstance()->SetType(SPLINE2D::SPT_BSPLINE2);
		m_pNextSpline->GetSplineInstance()->SetType(SPLINE2D::SPT_BSPLINE2);
		break;

	case ID_MNU_SPLINE_TYPE_BSPLINE3:
		m_pSpline->GetSplineInstance()->SetType(SPLINE2D::SPT_BSPLINE3);
		m_pNextSpline->GetSplineInstance()->SetType(SPLINE2D::SPT_BSPLINE3);
		break;

	case ID_MNU_SPLINE_EDIT_TYPE_CTRL:
	{
		if (m_pSpline->GetControlPointNum() == 0)
		{
			if (m_pSpline->GetPointNum() != 0)
			{
				POINT2D ptStart = m_pSpline->GetPoint(0);
				m_pSpline->AddControlPoint(ptStart);
				m_pNextSpline->Clear();
				m_pNextSpline->AddControlPoint(ptStart);
				m_pNextSpline->AddControlPoint(ptStart);
			}
		}
		else
		{
			m_pSpline->Clear();
			int iControlNum = m_pNextSpline->GetControlPointNum();

			iControlNum--;
			for (int iIndex = 0; iIndex < iControlNum; iIndex++)
			{
				m_pSpline->AddControlPoint(m_pNextSpline->GetControlPoint(iIndex));
			}
		}
		m_lstRedo.clear();
		m_pSpline->GetSplineInstance()->SetEditType(SPLINE2D::EDT_CTRL);
		m_pNextSpline->GetSplineInstance()->SetEditType(SPLINE2D::EDT_CTRL);
		break;

	}

	case ID_MNU_SPLINE_EDIT_TYPE_PASS:
	{
		if (m_pSpline->GetPointNum() == 0)
		{
			if (m_pSpline->GetControlPointNum() != 0)
			{
				POINT2D ptStart = m_pSpline->GetControlPoint(0);
				m_pSpline->AddControlPoint(ptStart);
				m_pNextSpline->Clear();
				m_pNextSpline->AddControlPoint(ptStart);
				m_pNextSpline->AddControlPoint(ptStart);
			}
		}
		m_pSpline->GetSplineInstance()->SetEditType(SPLINE2D::EDT_PASS_POINT);
		m_pNextSpline->GetSplineInstance()->SetEditType(SPLINE2D::EDT_PASS_POINT);
		break;
	}

	default:
		break;

	}


    pDef->Redraw();
    return true;
}

/**
 *  @brief   マウス右クリック
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    nFlags   仮想キー 
 *  @retval   なし
 *  @note
 */
bool CActionSpline::RButtonClick(POINT ptSel, UINT nFlags)
{/*
    CPartsDef*   pCtrl  = m_pView->GetPartsDef();
    int                  iId  = m_pView->SearchPos(ptSel);
    auto     pObj  = pCtrl->GetObjectById(iId);
    VIEW_MODE      eViewMode  = m_pView->GetViewMode();

    if (m_pSpline == NULL)
    {
        return false;
    }

    if( m_pSpline->GetPointNum() < 2)
    {
        ClearAction();
        return false;
    }

    if (pObj)
    {
        auto pPointObj = std::dynamic_pointer_cast<CDrawingPoint>(pObj);
        if (pPointObj)
        {
            if (m_pSpline->GetPointNum() >= 3 )
            {
                POINT2D   pt2D = *pPointObj->GetPointInstance();
                POINT2D ptStart = m_pSpline->GetPoint(0);
                if (ptStart == pt2D)
                {
                    m_pSpline->SetClose();
                }
            }
        }
    }
    
    AddSpline();
    ClearAction();
 */
	return true;
}

/**
 *  @brief   スプライン登録
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CActionSpline::AddSpline()
{
    if(!m_pSpline)
    {
        return;
    }

    auto pSpline = std::make_shared<CDrawingSpline>(*m_pSpline);
	if (!THIS_APP->GetDefaultObjectPriorty(DT_SPLINE))
	{
		pSpline->SetColor(m_pView->GetCurrentLayer()->crDefault);
	}
	else
	{
		COLORREF cr = THIS_APP->GetDefaultObject(DT_SPLINE)->GetColor();
		pSpline->SetColor(cr);
	}

    CPartsDef*   pDef = m_pView->GetPartsDef();
    CUndoAction*       pUndo  = pDef->GetUndoAction();

    pSpline->SetLayer(m_pView->GetCurrentLayerId());
    pDef->RegisterObject(pSpline, true, false);
    pUndo->Add(UD_ADD, pDef, NULL, pSpline, true);
}
//!< アンドゥ
bool CActionSpline::Undo()
{
    if (!m_pSpline)
    {
        return false;
    }


	POINT2D ptLast;
	int iLastNum;

	if (m_pSpline->GetSplineInstance()->GetEditType() == SPLINE2D::EDT_PASS_POINT)
	{
		if (m_pSpline->GetPointNum() == 0)
		{
			return false;
		}
		iLastNum = m_pSpline->GetPointNum() - 1;
		ptLast = m_pSpline->GetPoint(iLastNum);
		m_pSpline->DelLastPoint();
		m_pNextSpline->DelLastPoint();
	}
	else
	{
		if (m_pSpline->GetControlPointNum() == 0)
		{
			return false;
		}
		iLastNum = m_pSpline->GetControlPointNum() - 1;
		ptLast = m_pSpline->GetControlPoint(iLastNum);
		m_pSpline->DelLastControlPoint();
		m_pNextSpline->DelLastControlPoint();
	}

    m_lstRedo.push_front(std::make_unique<POINT2D>(ptLast));

    CPartsDef*   pDef  = m_pView->GetPartsDef();
    pDef->DrawDragging(m_pView);

    return true;
}

//!< リドゥ
bool CActionSpline::Redo()
{
    if (!m_pSpline)
    {
        return false;
    }

    if (m_lstRedo.size() == 0)
    {
        return false;
    }

    POINT2D ptRedo;
    ptRedo = *m_lstRedo[0];

	if (m_pSpline->GetSplineInstance()->GetEditType() == SPLINE2D::EDT_PASS_POINT)
	{
		m_pSpline->AddPoint(ptRedo);

		m_pNextSpline->AddPoint(ptRedo);
		if (m_pNextSpline->GetPointNum() == 1)
		{
			m_pNextSpline->AddPoint(ptRedo);
		}
		else
		{
			int iIndex = m_pNextSpline->GetPointNum() - 2;
			m_pNextSpline->SetPoint(iIndex, ptRedo);
		}
	}
	else
	{
		m_pSpline->AddControlPoint(ptRedo);

		m_pNextSpline->AddControlPoint(ptRedo);
		if (m_pNextSpline->GetControlPointNum() == 1)
		{
			m_pNextSpline->AddControlPoint(ptRedo);
		}
		else
		{
			int iIndex = m_pNextSpline->GetPointNum() - 2;
			m_pNextSpline->SetControlPoint(iIndex, ptRedo);
		}
	}

    m_lstRedo.pop_back();

    CPartsDef*   pDef  = m_pView->GetPartsDef();
    pDef->DrawDragging(m_pView);
    return true;
}

bool CActionSpline::IsUndoEmpty()
{
    if (!m_pSpline)
    {
        return true;
    }

	if (m_pSpline->GetSplineInstance()->GetEditType() == SPLINE2D::EDT_PASS_POINT)
	{
		if (m_pSpline->GetPointNum() == 0)
		{
			return true;
		}
	}
	else
	{
		if (m_pSpline->GetControlPointNum() == 0)
		{
			return true;
		}
	}
    return false;
}


bool CActionSpline::IsRedoEmpty()
{
    return (m_lstRedo.size() == 0);
}


/**
 *  @brief   再描画
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CActionSpline::Redraw() const
{
    //MouseMov内で描画するのでこでは再描画しない
    /*
    if (m_pNextSpline)
    {
        m_pNextSpline->DrawControl(m_pView, true);
    }

    if (m_pSpline)
    {
        m_pSpline->DrawControl(m_pView, true);
    }
    */
}

void CActionSpline::InitTempObject()
{
	if (m_pSpline)
	{
		m_pSpline.reset();
	};

	if (m_pNextSpline)
	{
		m_pNextSpline.reset();
	};
}

/**
 *  動作クリア
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CActionSpline::ClearAction()
{
    CViewAction::ClearAction();
    m_bMark = false;
    m_iDrag = -1;

    CDrawMarker*        pMarker = m_pView->GetMarker();
    CPartsDef*    pCtrl   = m_pView->GetPartsDef();

    pMarker->Clear();
    pCtrl->RelSelect();

	InitTempObject();

    m_lstRedo.clear();

    pCtrl->Redraw();
}

