/**
 * @brief			CActionSplineヘッダーファイル
 * @file			CActionSpline.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef _ACTION_SPLINE_H__
#define _ACTION_SPLINE_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CViewAction.h"
class     CDrawingView;
class     POINT2D;
class     CDrawingSpline;

/**
 * @class   CActionSpline
 * @brief                        
 */
class CActionSpline: public CViewAction
{
public:
	//!< コンストラクタ
    CActionSpline();

    //!< コンストラクタ
    CActionSpline(CDrawingView* pView);
    
	//!< デストラクタ
    virtual ~CActionSpline();

	//!< 選択解放時動作
    virtual void Cancel(VIEW_MODE eMode);

	//!< 選択解放時動作
	virtual bool SelAction(VIEW_MODE eMode, void* pParam = NULL);

	//!< マウスクリック動作
	virtual bool SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

    //!< マウス移動動作
	virtual bool MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

    //!< マウス右ボタンクリック
    virtual bool RButtonClick(POINT ptSel, UINT nFlags);

    //!<コンテキストメニュー表示
    virtual bool OnContextMenu(POINT point);


    //!< 再描画
    virtual void Redraw() const;

    //!< アンドゥ
    virtual bool Undo();

    virtual bool IsUndoEmpty();

    //!< リドゥ
    virtual bool Redo();

    virtual bool IsRedoEmpty();

	virtual void InitTempObject();


protected:
    void ClearAction();

    void AddSpline();

    bool _AddMarker(CDrawingSpline* pSpline);

protected:
    // 作図用に一時生成した点を記憶する
    std::vector<POINT2D> m_lstPoint;

    // 失敗時に削除する
    std::shared_ptr<CDrawingSpline> m_pSpline;

    std::shared_ptr<CDrawingSpline> m_pNextSpline;

    std::deque<std::unique_ptr<POINT2D>> m_lstRedo;
    
    int m_iDrag;

protected:
};
#endif // _ACTION_SPLINE_H__
