/**
 * @brief			CActionText実装ファイル
 * @file			CActionText.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CActionText.h"

#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/View/CDrawingView.h"

#include "DrawingObject/CDrawingObject.h"
#include "DrawingObject/CDrawingPoint.h"
#include "DrawingObject/CDrawingLine.h"
#include "DrawingObject/CDrawingCircle.h"
#include "DrawingObject/CDrawingText.h"

#include "View/CLayer.h"
#include "View/CDrawMarker.h"
#include "View/CUndoAction.h"
#include "Utility/CUtility.h"
#include "Utility/ExtText/CExtText.h"
#include "System/CSystem.h"
#include "MockSketch.h"
#include <math.h>

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 * コンストラクタ
 */
CActionText::CActionText():
CViewAction()
{
    m_lMouseOver =  SEL_POINT | SEL_RICH_TEXT;
    mp_ptSel = std::make_unique<POINT2D>();
} 

/**
 * コンストラクタ
 */
CActionText::CActionText(CDrawingView* pView):
CViewAction(pView)
{
    m_lMouseOver =  SEL_POINT| SEL_RICH_TEXT;
    mp_ptSel = std::make_unique<POINT2D>();
}
    
/**
 * デストラクタ
 */
CActionText::~CActionText()
{
    
}

/**
 *  @brief   解放時動作
 *  @param   [in]    eMode 解放時点でのモード
 *  @retval   なし
 *  @note
 */
void CActionText::Cancel(VIEW_MODE eMode) 
{
    CPartsDef*   pCtrl  = m_pView->GetPartsDef();
    STD_ASSERT(pCtrl !=NULL);

    //選択解除
    pCtrl->RelSelect();

    static const bool bCancel = true;
    CloseText(*mp_ptSel, bCancel);
}

/**
 *  @brief   選択時動作
 *  @param   [in]    eMode 選択時点でのモード
 *  @retval   なし
 *  @note
 */
bool CActionText::SelAction(VIEW_MODE eMode, void* pParam) 
{
    //文字を選択した場合は再編集
    m_lMouseOver =   SEL_POINT | SEL_RICH_TEXT;

    //既に選択中の図形はあるか？
    long lSize = m_pView->GetPartsDef()->SelectNum();

    if (lSize != 0)
    {
        //選択を解除
        m_pView->GetPartsDef()->RelSelect();
    }   
   
    m_pView->SetComment(GET_SYS_STR(STR_SEL_TEXT_POINT));
    m_pText = NULL;
    m_eViewMode = eMode;

    return true;
}


/**
 *  @brief   マウス移動動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionText::MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    using namespace VIEW_COMMON;

    POINT ptSel = posMouse.ptSel;
    UINT nFlags = posMouse.nFlags;

    auto pRichEdit = m_pView->GetRichEdit();
    if (pRichEdit->IsWindowVisible())
    {
        return false;
    }

    //!< スナップ点取得
    bool bSnap = true;

    SNAP_DATA snap;
    
    CPartsDef*   pDef  = m_pView->GetPartsDef();
    if (!pDef)
    {
        return false;
    }
    pDef->RedrawWait();   //TODO:どうする？

    DWORD dwSnapType = DRAW_CONFIG->GetSnapType();

    CDrawMarker* pMarker= m_pView->GetMarker();

    bSnap = m_pView->GetSnapPoint(&snap, 
                      dwSnapType,
                      ptSel);

    auto pObj1 = pDef->GetObjectById(snap.iObject1);
    auto pObj2 = pDef->GetObjectById(snap.iObject2);

    POINT ptOffset;
    ptOffset.x = ptOffset.y = 0;
    CDrawMarker::MARKER_DATA marker;
    marker.ptOffset = ptOffset;


    bool bIgnoreSnap = false;
    if (nFlags & MK_SHIFT)
    {
        //スナップ無視
        bIgnoreSnap = true;
        bSnap = false;
        pObj1 = NULL;
        snap.eSnapType = SNP_NONE;
    }

    VIEW_MODE      eViewMode  = m_pView->GetViewMode();
    m_pView->SetDrawingMode(DRAW_SEL);
    pMarker->Clear();
    pMarker->ClearObject();
    pDef->ClearMouseOver();
    pDef->ClearMouseEmphasis();
    pDef->ClearMouseConnectable();

    if ((pObj1) &&
        (pObj2))
    {
        //交点
        pDef->SetMouseEmphasis(pObj1);
        pDef->SetMouseEmphasis(pObj2);
        marker.eType = CDrawMarker::MARKER_CROSS;
        marker.eSnap = VIEW_COMMON::SNP_CROSS_POINT;
        pMarker->AddMarker(marker, snap.pt);
    }

    if ((pObj1) &&
        (!pObj2))
    {

        m_pView->SetMarker(pObj1.get(), false /*bIgnoreOffLineObject*/);
        pDef->SetMouseEmphasis(pObj1);
    }

    int iMarkerId = pMarker->MouseMove(posMouse.ptSel);

    snap.strSnap = GetSnapName(snap.eSnapType, true);

    if (snap.eSnapType == SNP_NONE)
    {
        //任意点
        CDrawMarker*pMarker = m_pView->GetMarker();
        marker.eType = CDrawMarker::MARKER_CROSS;
        marker.eSnap = VIEW_COMMON::SNP_CROSS_POINT;
        pMarker->AddMarker(marker, snap.ptOrg);
    }

    if (snap.strSnap.empty()) 
    {
        m_pView->ResetToolTipText();
    }
    else
    {
        m_pView->SetToolTipText(snap.strSnap.c_str());
    }
    pMarker->HideObject(false);
    pDef->DrawDragging(m_pView);
    return true;
}

/**
 *  @brief   マウスクリック動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionText::SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{

    POINT ptSel = posMouse.ptSel;
    UINT nFlags = posMouse.nFlags;
    CPartsDef*   pCtrl  = m_pView->GetPartsDef();
    int                  iId  = m_pView->SearchPos(posMouse.ptSel);
    auto     pObj  = pCtrl->GetObjectById(iId);

    if (eAction != CViewAction::ACT_LBUTTON_UP)
    {
        //左クリックのみ
        return false;
    }

    POINT2D pt2D;

    //!< ビュー->データ変換
    auto pRichEdit = m_pView->GetRichEdit();
    if (!pRichEdit->IsWindowVisible())
    {
        POINT2D ptRet;
        if (!_CheckMarker( &ptRet, ptSel, eAction))
        {
            int iCurrentLayer = m_pView->GetCurrentLayer()->iLayerId;
            m_pView->ConvScr2World(&ptRet, posMouse.ptSel, iCurrentLayer);
        }
        else
        {
            if(pObj)
            {
                DRAWING_TYPE eDwType = pObj->GetType();
                if (eDwType == DT_TEXT)
                {
                    auto pText = std::dynamic_pointer_cast<CDrawingText>(pObj);
                    *mp_ptSel = pText->GetPoint();
                    OpenText(pText);
                }
                else
                {
                    pObj = NULL;
                }
            }
        }

        if (pObj == NULL)
        {
            *mp_ptSel = ptRet;
            OpenText(ptRet);
        }
    }
    else
    {
        //入力確定とする
        CloseText(*mp_ptSel);
    }
    return false;
}

/**
 *  @brief   テキスト入力
 *  @param   [in]    ptSel 表示位置
 *  @retval  false エラー発生
 *  @note    
 */
bool CActionText::OpenText(POINT2D ptSel)
{
    CPartsDef*   pCtrl  = m_pView->GetPartsDef();
    CLayer*           pLayer  = m_pView->GetCurrentLayer();

    //!< リッチエディット取得
    m_pView->InitRichEdit();
    auto pRichEdit = m_pView->GetRichEdit();

    //適当な大きさでリッチエディットを表示する
    //int iWidth  = 150;
    //int iHeight = 100;

    int iWidth  = DRAW_CONFIG->DimSettingToDot(DRAW_CONFIG->dTextBoxMinWidth);
    int iHeight = DRAW_CONFIG->DimSettingToDot(DRAW_CONFIG->dTextBoxMinHeight);

     POINT ptDspPos;

    int iLayerId = m_pView->GetCurrentLayerId();
    m_pView->ConvWorld2Scr(&ptDspPos, ptSel, iLayerId);

    int iWW = m_pView->GetWindowWidth();
    int iWH = m_pView->GetWindowHeight();


    //端部の処理
    if ((ptDspPos.x + iWidth) > iWW)
    {
        ptDspPos.x = iWW - iWidth;
        if (ptDspPos.x < 0)
        {
            ptDspPos.x = 0;
        }
    }

    if ((ptDspPos.y + iHeight) > iWH)
    {
        ptDspPos.y = iWH - iHeight;
        if (ptDspPos.y < 0)
        {
            ptDspPos.y = 0;
        }
    }

    SIZE sz;
    if(m_pView->CalcRichEditSize(&sz))
    {
        pRichEdit->MoveWindow(ptDspPos.x, ptDspPos.y, sz.cx, sz.cy);
    }
    BOOL bRet;
    bRet = pRichEdit->ShowWindow(SW_SHOW);

    pRichEdit->SetFocus();
    m_pText = NULL;
    return true;
}

//!< テキスト入力ボックス表示
bool CActionText::OpenText(std::shared_ptr<CDrawingText> pText)
{
    /*  OnUpdateで行うはず
    CExtText* pExtText = m_pText->GetTextInstance();
    auto eDatum = pExtText->GetDatum();
    int iId = CExtText::ConvDatumToId(eType);
    m_pView->SetTextDatum(iId);
    */

    m_pText = pText;
    pText->OpenText(m_pView);
    return true;
}

/**
 *  @brief   テキスト入力終了
 *  @param   [in]    strVal 入力値
 *  @retval  false エラー発生
 *  @note    必要であればエラー表示を行う
 *  @note    エラー発生で入力コンボボックスは選択状態になる      
 *  @note    エラーがない場合はコンボボックスに値が登録される
 */
bool CActionText::CloseText(POINT2D pt, bool bCancel)
{
    CPartsDef*   pDef = m_pView->GetPartsDef();
    CLayer*           pLayer  = m_pView->GetCurrentLayer();
    CUndoAction*       pUndo  = pDef->GetUndoAction();


    auto pRichEdit = m_pView->GetRichEdit();
 
    if (!pRichEdit->IsWindowVisible())
    {
        m_pView->SetComment(GET_SYS_STR(STR_SEL_TEXT_POINT));
        return false;
    }

    //!< リッチエディット取得

    if ((pRichEdit->GetTextLength()==0) || bCancel)
    {
        pRichEdit->ShowWindow(SW_HIDE);
        m_pView->SetComment(GET_SYS_STR(STR_SEL_TEXT_POINT));
        m_pText .reset();
        return false;
    }

    //文字を作成

    if (!m_pText)
    {
		auto pCreateObj = THIS_APP->CreateDefaultObject(DT_TEXT);
		auto defObj = std::dynamic_pointer_cast<CDrawingText>(pCreateObj);
		auto pText = std::make_shared<CDrawingText>(*(defObj.get()));

        pText->SetPoint(pt);
        pText->SetLayer(m_pView->GetCurrentLayerId());
        CExtText* pExtText = pText->GetTextInstance();

        // リッチエディットからテキストを設定
        pRichEdit->ShowWindow(SW_HIDE);
        pExtText->SetFromRich(pRichEdit);
        pRichEdit->SetWindowText(_T(""));

        // 文字角度設定
        pExtText->SetAngle(0.0);
        

        CUndoAction* pUndo  = pDef->GetUndoAction();
        pDef->RegisterObject(pText, true, false);
        pUndo->Add(UD_ADD, pDef, NULL, pText, true);
        m_pText = pText; 

    }
    else
    {   
        //削除
        pUndo->AddStart(UD_CHG, pDef, m_pText.get());
        m_pText->DeleteView(m_pView, NULL);

        pRichEdit->ShowWindow(SW_HIDE);
        CExtText* pExtText = m_pText->GetTextInstance();

        pExtText->SetFromRich(pRichEdit);
        pRichEdit->SetWindowText(_T(""));


        // 文字表示位置設定
        int iId = m_pView->GetTextDatum();
        auto eType = CExtText::ConvIdToDatum(iId);
        pExtText->SetDatum(eType);

        //再描画(Redrawが良いか？）
        m_pText->Draw(m_pView, NULL);
        pUndo->AddEnd(m_pText, true);
    }

    m_pView->SetComment(GET_SYS_STR(STR_SEL_TEXT_POINT));
    m_pText.reset();

    m_pView->SetFocus();
    return true;
}

/**
 *  @brief   入力値設定
 *  @param   [in]    strVal 入力値
 *  @retval  false エラー発生
 *  @note    必要であればエラー表示を行う
 *  @note    エラー発生で入力コンボボックスは選択状態になる      
 *  @note    エラーがない場合はコンボボックスに値が登録される
 */
bool CActionText::InputValue(StdString strVal)
{
    

    return false;
}


/**
 *  動作クリア
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CActionText::ClearAction(bool bReleaseSelect)
{
    CloseText(*mp_ptSel, true);

    CViewAction::ClearAction(bReleaseSelect);
}