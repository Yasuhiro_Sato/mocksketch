/**
 * @brief			CActionTextヘッダーファイル
 * @file			CActionText.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef _ACTION_NOTE_H__
#define _ACTION_NOTE_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CViewAction.h"
class     CDrawingView;
class     CDrawingText;

/**
 * @class   CActionText
 * @brief                        
 */
class CActionText: public CViewAction
{
public:
	//!< コンストラクタ
    CActionText();

    //!< コンストラクタ
    CActionText(CDrawingView* pView);
    
	//!< デストラクタ
    virtual ~CActionText();

	//!< 選択解放時動作
    virtual void Cancel(VIEW_MODE eMode);

	//!< 選択解放時動作
	virtual bool SelAction(VIEW_MODE eMode, void* pParam = NULL);

	//!< マウスクリック動作
	virtual bool SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

	//!< マウス移動動作
	virtual bool MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

    //!<入力値設定
    virtual bool InputValue(StdString strVal);

    virtual void ClearAction(bool bReleaseSelect = true);

protected:
    //!< テキスト入力ボックス表示
    bool OpenText(POINT2D ptPos);

    //!< テキスト入力ボックス表示
    bool OpenText(std::shared_ptr<CDrawingText> pText);

    //!< テキスト入力終了
    bool CloseText(POINT2D ptPos, bool bCancel = false);

protected:

    //表示テキスト
    std::shared_ptr<CDrawingText>   m_pText;

    //選択位置
    std::unique_ptr<POINT2D>        mp_ptSel;
};
#endif // _ACTION_NOTE_H__
