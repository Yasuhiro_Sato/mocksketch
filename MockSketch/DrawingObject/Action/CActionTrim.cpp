/**
 * @brief			CActionTrim実装ファイル
 * @file			CActionTrim.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CActionTrim.h"

#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/View/CDrawingView.h"

#include "DrawingObject/CDrawingObject.h"
#include "DrawingObject/CDrawingPoint.h"
#include "DrawingObject/CDrawingLine.h"
#include "DrawingObject/CDrawingCircle.h"
#include "DrawingObject/CDrawingSpline.h"

#include "View/CLayer.h"
#include "View/CDrawMarker.h"
#include "View/CUndoAction.h"
#include "Utility/CUtility.h"
#include "System/CSystem.h"
#include <math.h>

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 * コンストラクタ
 */
CActionTrim::CActionTrim():
CViewAction()
{
    m_lMouseOver =  SEL_PRI_NOPT;

}

/**
 * コンストラクタ
 */
CActionTrim::CActionTrim(CDrawingView* pView):
CViewAction(pView)
{
    m_lMouseOver =  SEL_PRI_NOPT;
}
    
/**
 * デストラクタ
 */
CActionTrim::~CActionTrim()
{
    
}

/**
 *  @brief   解放時動作
 *  @param   [in]    eMode 解放時点でのモード
 *  @retval   なし
 *  @note
 */
void CActionTrim::Cancel(VIEW_MODE eMode) 
{
    CPartsDef*   pCtrl  = m_pView->GetPartsDef();
    STD_ASSERT(pCtrl !=NULL);

    //選択解除
    pCtrl->RelSelect();
    m_FirstSelect.pObject = NULL;

}

/**
 *  @brief   選択時動作
 *  @param   [in]    eMode 選択時点でのモード
 *  @retval   なし
 *  @note
 */
bool CActionTrim::SelAction(VIEW_MODE eMode, void* pParam) 
{
    //点はトリム要素としては選択不可
    m_lMouseOver =   /*SEL_POINT |*/ SEL_PRI_NOPT;

    //既に選択中の図形はあるか？
    long lSize = m_pView->GetPartsDef()->SelectNum();

    if (lSize != 0)
    {
        //選択を解除
        m_pView->GetPartsDef()->RelSelect();
    }   
    
    m_pView->SetComment(GET_SYS_STR(STR_SEL_TRIM));
    m_FirstSelect.pObject = NULL;
    m_eViewMode = eMode;
    return true;
}

/**
 *  @brief   マウスクリック動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionTrim::SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{

    CPartsDef*   pCtrl  = m_pView->GetPartsDef();
    int                  iId  = m_pView->SearchPos(posMouse.ptSel);
    auto     pObj  = pCtrl->GetObjectById(iId);
    CUndoAction*       pUndo  = pCtrl->GetUndoAction();

    if (eAction != CViewAction::ACT_LBUTTON_UP)
    {
        //左クリックのみ
        return false;
    }

    //!< ビュー->データ変換
    POINT2D pt2D;
    int iLayerId = m_pView->GetCurrentLayerId();
    m_pView->ConvScr2World(&pt2D, posMouse.ptSel, iLayerId);


    if (m_FirstSelect.pObject == NULL)
    {
        //==============
        // 一点目を選択
        //==============

        if (pObj == NULL)
        {
            return false;
        }

        DRAWING_TYPE eDwType = pObj->GetType();

        //点は選択不可
        if (!IsActionType(m_lMouseOver, pObj.get()))
        {
            return false;
        }


        m_FirstSelect.ptClick = pt2D;
        m_FirstSelect.pObject = pObj;

        //選択状態にする
        pCtrl->SelectDrawingObject(iId);


        m_pView->SetComment(GET_SYS_STR(STR_SEL_TRIM_OBJ));

        //点も選択可能にする
        m_lMouseOver =  SEL_FIG;
        return true;
    }
    else
    {
        //==============
        // 二点目を選択
        //==============
        if (pObj == m_FirstSelect.pObject)
        {
            //同じオブジェクトを選択したときは無視する
            return false;
        }

        bool bReverse = false;
        pUndo->AddStart(UD_CHG, pCtrl, m_FirstSelect.pObject.get());
        /*
        if (m_FirstSelect.pObject->GetType() == DT_CIRCLE)
        {
            reinterpret_cast<CDrawingCircle*>(m_FirstSelect.pObject)->GetCircleInstance()->SetArc();
        }
        */
        m_FirstSelect.pObject->Trim(m_ptSel, m_FirstSelect.ptClick, bReverse);
        pUndo->AddEnd( m_FirstSelect.pObject, true);

        m_FirstSelect.pObject = NULL;
        m_lMouseOver  = SEL_PRI_NOPT;
        ClearAction();
        m_pView->SetComment(GET_SYS_STR(STR_SEL_TRIM));
        pCtrl->Redraw(m_pView);
        return true;
    }
    return false;
}

/**
 *  @brief   マウス移動動作
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CActionTrim::MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction)
{
    POINT ptSel = posMouse.ptSel;
    UINT nFlags = posMouse.nFlags;

    //!< スナップ点取得
    bool bIgnoreSnap = false;

    if (posMouse.nFlags & MK_SHIFT)
    {
        bIgnoreSnap = true;
    }

    CPartsDef*   pDef  = m_pView->GetPartsDef();
    if (!pDef)
    {
        return false;
    }
    CDrawMarker* pMarker= m_pView->GetMarker();
    bool bOnLine = false;
    bool bTrimClickSide = true; //最初にクリックした側を調整する

    pMarker->Clear();
    pMarker->ClearObject();
    pDef->ClearMouseOver();
    pDef->ClearMouseEmphasis();
    pDef->ClearMouseConnectable();

    using namespace VIEW_COMMON;

    SNAP_DATA snap;
    m_pView->GetMousePoint(&snap, posMouse);

    //bool bOnLine = false;
    bool bReverse = false;

    auto pObj1 = pDef->GetObjectById(snap.iObject1);
    auto pObj2 = pDef->GetObjectById(snap.iObject2);

    if (!m_FirstSelect.pObject)
    {
        bool bStart = false;
        bStart = IsActionType(m_lMouseOver, pObj1.get());
        if (bStart)
        {
            pObj1->DrawOverStart(m_pView);
            pDef->SetMouseOver(pObj1);
        }
    }
    else
    {
        bool bIntersect = false;
        //補助線の作成
        bool bExt1 = false;
        POINT2D ptCreate;

        int iLayerId = m_pView->GetCurrentLayerId();
 
        DRAWING_TYPE eType =  m_FirstSelect.pObject->GetType();
        auto pPoint = CreatePoint(&m_psAddtionalObj[1]);
        m_psAddtionalObj[1]->SetLayer(iLayerId);

        std::vector<POINT2D> lstPoint;
        if ( (snap.eSnapType == SNP_ON_LINE) &&
                (m_FirstSelect.pObject != pObj1))
        {
            //Trimする図形と交点がある場合
            m_FirstSelect.pObject->CalcIntersection(&lstPoint, pObj1.get(), bOnLine);
            if (lstPoint.size() > 0)
            {
                snap.pt.NearPoint_P(&ptCreate, lstPoint);
                //CreateExtendは、すでにLineが生成されている時は再利用するので
                //m_psAddtionalObj[0]をDeleteする必要なし
                bExt1 = CreateExtend(&m_psAddtionalObj[0],
                                    ptCreate,
                                    snap.iObject1);
                m_psAddtionalObj[0]->SetLayer(iLayerId);
                snap.eSnapType = SNP_CROSS_POINT;
                pMarker->Clear();
                bIntersect = true;
            }
        }

        if (!bIntersect)
        {
            //CreateLineは、すでにLineが生成されている時は再利用するので
            //m_psAddtionalObj[0]をDeleteする必要なし
            auto pLine = CreateLine(&m_psAddtionalObj[0]);
            pLine->SetLayer(iLayerId);
            if (m_FirstSelect.pObject->NearPoint(&ptCreate, snap.pt, bOnLine))
            {
                pMarker->Clear();
                if (pObj1)
                {
                    m_pView->SetMarker(pObj1.get(), false /*bIgnoreOffLineObject*/);
                    int iMarkerId = pMarker->MouseMove(posMouse.ptSel);
                }
                pLine->SetPoint1(ptCreate);
                pLine->SetPoint2(snap.pt);
                bExt1 = true;
            }
        }

        m_psAddObj = CDrawingObject::CloneShared(m_FirstSelect.pObject.get());

 DB_PRINT(_T("Near %s \n"),ptCreate.Str().c_str() );
        //------------
        // 補助線表示
        //------------
        if (m_psAddtionalObj[0])
        {
            m_psAddtionalObj[0]->SetVisible(bExt1);
        }
        pPoint->SetPoint(snap.pt);
        if ((snap.eSnapType == SNP_NONE) || (bIntersect))
        {
            pPoint->SetVisible(false);
        }
        else
        {
            pPoint->SetVisible(true);
        }
        //------------

        m_psAddObj->Trim(ptCreate, m_FirstSelect.ptClick, bReverse);
        m_psAddObj->SetSelect(false);
        m_psAddObj->SetColor(DRAW_CONFIG->crImaginary);
        m_psAddObj->SetVisible(true);

        m_ptSel = ptCreate;
        pMarker->AddObject(m_psAddObj);
        pDef->ClearMouseOver();
        pDef->ClearMouseEmphasis();
        pDef->SetMouseOver(m_psAddObj);

        pMarker->AddObject(m_psAddtionalObj[0]);
        pMarker->AddObject(m_psAddtionalObj[1]);

        if (pObj1)
        {
            pDef->SetMouseEmphasis(pObj1);
        }
        if (pObj2)
        {
            pDef->SetMouseEmphasis(pObj2);
        }

    }

    pDef->DrawDragging(m_pView);


    if (m_FirstSelect.pObject)
    {

        StdString strToolTipText;
        StdStringStream sToolTip;
        strToolTipText = GetSnapName(snap.eSnapType);

        sToolTip << StdFormat(_T("%s")) % strToolTipText;
    

        if (!sToolTip.str().empty())
        {
            if((m_ptTooltip.x != posMouse.ptSel.x) ||
                (m_ptTooltip.y != posMouse.ptSel.y))
            {
                m_pView->SetToolTipText(sToolTip.str().c_str());
            }
        }
        else
        {
            m_pView->ResetToolTipText();
        }
        m_ptTooltip = posMouse.ptSel;
    }
    return true;
}




/**
 *  @brief   入力値設定
 *  @param   [in]    strVal 入力値
 *  @retval  false エラー発生
 *  @note    必要であればエラー表示を行う
 *  @note    エラー発生で入力コンボボックスは選択状態になる      
 *  @note    エラーがない場合はコンボボックスに値が登録される
 */
bool CActionTrim::InputValue(StdString strVal)
{
    return false;
}

