/**
 * @brief			CActionTrimヘッダーファイル
 * @file			CActionTrim.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef _ACTION_TRIM_H__
#define _ACTION_TRIM_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DrawingObject/Action/CViewAction.h"
#include "DrawingObject/Primitive/POINT2D.h"

/*---------------------------------------------------*/
/*  Classess                                         */
/*---------------------------------------------------*/
class     CDrawingView;

/**
 * @class   CActionTrim
 * @brief                        
 */
class CActionTrim: public CViewAction
{
    struct CLICK_POINT
    {
        POINT2D         ptClick;    //クリック位置
        std::shared_ptr<CDrawingObject>   pObject;    //選択オブジェクト
    };


public:
	//!< コンストラクタ
    CActionTrim();

    //!< コンストラクタ
    CActionTrim(CDrawingView* pView);
    
	//!< デストラクタ
    virtual ~CActionTrim();

	//!< 選択解放時動作
    virtual void Cancel(VIEW_MODE eMode);

	//!< 選択時動作
	virtual bool SelAction(VIEW_MODE eMode, void* pParam = NULL);

	//!< マウスクリック動作
	virtual bool SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

	//!< マウス移動動作
    virtual bool MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);



    //!<入力値設定
    virtual bool InputValue(StdString strVal);

protected:


protected:
    CLICK_POINT   m_FirstSelect;

    POINT2D       m_ptSel;
};
#endif // _ACTION_TRIM_H__
