/**
 * @brief			CViewAction実装ファイル
 * @file			CViewAction.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MockSketch.h"
#include "./CViewAction.h"

#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/CElementDef.h"
#include "DefinitionObject/View/CDrawingView.h"

#include "DrawingObject/CDrawingObject.h"
#include "DrawingObject/CDrawingPoint.h"
#include "DrawingObject/CDrawingLine.h"
#include "DrawingObject/CDrawingCircle.h"
#include "DrawingObject/CDrawingParts.h"
#include "DrawingObject/CDrawingEllipse.h"
#include "DrawingObject/CDrawingSpline.h"
#include "DrawingObject/CDrawingCompositionLine.h"
#include "DrawingObject/CDrawingDim.h"
#include "DrawingObject/CDrawingNode.h"
#include "DrawingObject/CDrawingText.h"
#include "DrawingObject/CDrawingElement.h"
#include "DrawingObject/CDrawingConnectionLine.h"
#include "DrawingObject/CDrawingImage.h"
#include "DrawingObject/CDrawingReference.h"
#include "DrawingObject/CDrawingGroup.h"

#include "View/CLayer.h"
#include "View/CUndoAction.h"
#include "View/CDrawMarker.h"
#include "System/CSystem.h"
#include "Utility/CUtility.h"
#include "View/ViewCommon.h"
#include "Script/CScriptEngine.h"


#include <math.h>

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define _DEBUG_PRINT 0



//!< デストラクタ
CViewAction::~CViewAction()
{
    _DeleteObject();
}

/**
 *  解放時動作
 *  @param   [in]    eMode 解放時点でのモード
 *  @retval   なし
 *  @note
 */
void CViewAction::_DeleteObject()
{
    if (m_psAddObj)
    {
        CDrawMarker* pMarker= m_pView->GetMarker();
        pMarker->ClearObject();
    }

    for (int iCnt = 0; iCnt < 2; iCnt++)
    {
        m_psAddtionalObj[iCnt].reset();
    }
}



/**
 *  @brief   点選択によるオブジェクト追加
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @param   [in]    iMaxSel  最大選択数         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CViewAction::AddObjByPoint(CDrawingObject* pAddObject, MOUSE_MOVE_POS posMouse, SEL_ACTION eAction, int iMaxSel)
{
	if (eAction != CViewAction::ACT_LBUTTON_UP)
	{
		//左クリックのみ
		return false;
	}
	
	/*
	    これを使用するのは点かノード
		マーカをクリック マーカー位置に点を設定
		何もないところをクリック 任意点を設定
		ObjectをクリックそのObjectのマーカーを表示して選択状態にする
		pAddObject が 設定してあればそれを登録する
	*/


	CPartsDef*   pDef;
	pDef = m_pView->GetPartsDef();

	CUndoAction* pUndo = pDef->GetUndoAction();
	std::shared_ptr<CDrawingObject> pNewObject;


	POINT2D pt2D;
	if (pAddObject)
	{
		pNewObject = AddObject(pAddObject);
		pUndo->Add(UD_ADD, pDef, NULL, pNewObject, true);

		//モードは変更しない
		ClearAction();
		return true;
	}

	int iId = m_pView->SearchPos(posMouse.ptSel);

	if ((iId == ID_PAPER_FRAME) ||
	    (iId == ID_PRINT_FRAME) ||
	    (iId == ID_GRID_DOT))
	{
		return false;
	}

	auto pObj = pDef->GetObjectById(iId);

	if (!pObj)
	{
		return false;
	}

	if (m_iClickCnt < iMaxSel)
	{
		if (pObj->IsSelect())
		{
			pObj->SetSelect(false);
			auto iteEraseObj = std::find_if(m_lstSelectObj.begin(), 
				                       m_lstSelectObj.end(), 
										[=](std::weak_ptr<CDrawingObject> wObj)
										{
   											auto sObj = wObj.lock();
											if (!sObj)
											{
												return false;
											}
											int iId1 = pObj->GetId();
											int iId2 = sObj->GetId();
											return (iId1 == iId2);
										});

			m_lstSelectObj.erase(iteEraseObj);
			return false;
		}

		m_lstSelectObj.push_back(pObj);

		//選択状態にする
		m_iClickCnt++;
		pDef->SelectDrawingObject(iId);
		pObj->Draw(m_pView, NULL);

		//マーカを設定する
		m_pView->SetMarker(pObj.get());
		return true;
	}

    return false;
}

/**
 *  マウス移動
 *  @param   [in]    pObj   選択オブジェクト
 *  @param   [in]    eAction   選択オブジェクト
 *  @param   [in]    nFlags   仮想キー 
 *  @retval   なし
 *  @note
 */
bool CViewAction::MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction) 
{ 
    POINT ptSel = posMouse.ptSel;
    UINT nFlags = posMouse.nFlags;

    CPartsDef*   pDef  = m_pView->GetPartsDef();
    int                  iId  = m_pView->SearchPos(ptSel);

    if (!pDef)
    {
        return false;
    }
    pDef->ClearMouseOver();


    m_pView->SetDrawingMode(DRAW_FRONT);
    if (m_pCurMouseOver != NULL)
    {
        if (m_pCurMouseOver->GetId() != iId)
        {
            m_pCurMouseOver->DrawOverEnd(m_pView);
            m_pCurMouseOver = NULL;
        }
    }

    if (iId == -1)
    {
        return false;
    }


    auto    pObj  = pDef->GetObjectById(iId);


    if(!pObj)
    {
        return false;
    }

    STD_ASSERT(pObj != NULL);
    DRAWING_TYPE       eType = pObj->GetType();

    auto pSelObj = pObj;
    
    bool bStart;
    bStart = IsActionType(m_lMouseOver, pSelObj.get());

    if (bStart)
    {
        pSelObj->DrawOverStart(m_pView);
        m_pCurMouseOver = pSelObj;
        pDef->SetMouseOver(pSelObj);
    }
    return true;
}

bool CViewAction::CreateBox(std::vector<std::shared_ptr<CDrawingObject>>* pLst,
                            const RECT2D& rcBound)
{
    POINT2D  ptTL(rcBound.dLeft,    rcBound.dTop);
    POINT2D  ptTR(rcBound.dRight,   rcBound.dTop);
    POINT2D  ptBL(rcBound.dLeft,    rcBound.dBottom);
    POINT2D  ptBR(rcBound.dRight,   rcBound.dBottom);

    auto pLineT = std::make_shared< CDrawingLine>();
    auto pLineL = std::make_shared< CDrawingLine>();
    auto pLineR = std::make_shared< CDrawingLine>();
    auto pLineB = std::make_shared< CDrawingLine>();
    auto pLineTLBR = std::make_shared< CDrawingLine>();
    auto pLineTRBL = std::make_shared< CDrawingLine>();
                
    pLineT->SetPoint1(ptTL);
    pLineT->SetPoint2(ptTR);

    pLineL->SetPoint1(ptTL);
    pLineL->SetPoint2(ptBL);

    pLineR->SetPoint1(ptTR);
    pLineR->SetPoint2(ptBR);

    pLineB->SetPoint1(ptBL);
    pLineB->SetPoint2(ptBR);

    pLineTLBR->SetPoint1(ptTL);
    pLineTLBR->SetPoint2(ptBR);

    pLineTRBL->SetPoint1(ptTR);
    pLineTRBL->SetPoint2(ptBL);

    pLst->push_back(pLineT);
    pLst->push_back(pLineL);
    pLst->push_back(pLineR);
    pLst->push_back(pLineB);
    pLst->push_back(pLineTLBR);
    pLst->push_back(pLineTRBL);

    return true;
}

/**
 *  @brief   移動用一時表示図形作成
 *  @param   [out]   pLstOffstMatrix    移動用マトリクス
 *  @param   [out]   pTempMoveObjects   選択オブジェクト
 *  @param   [out]   pBound             選択領域
 *  @param   [in]    pMarker   
 *  @param   [in]    iCopyTimes   
 *  @param   [in]    selectItems   
 *  @retval  
 *  @note   接続によって影響を受けるものは GetConnectingTmpObjectListにコピーする
 */
bool CViewAction::CreateTempMovingObjects(std::vector<std::unique_ptr<MAT2D>>* pLstOffstMatrix,
                                            std::vector< std::shared_ptr<CDrawingObject > >* pTempMoveObjects,
                                            CPartsDef*   pDef,
                                            int iCopyTimes,  
                                            bool bCopyConnection)
{
    if (iCopyTimes < 1){iCopyTimes = 1;}


    pLstOffstMatrix->clear();
    pTempMoveObjects->clear();

	//コピーする場合は接続線を移動させないため一時的なオブジェクト
	//を明示し、UpdateAllNodePositionAndRelationで接続線を移動させないようにする
	bool bTmp = !bCopyConnection;
	auto pSelectedObject = CViewAction::GroupSelectObject(pDef, true);

    for(int iCnt = 0; iCnt < iCopyTimes; iCnt++)
    {
        pLstOffstMatrix->push_back( std::make_unique<MAT2D>());
		auto pNewObj = CDrawingObject::CloneShared(pSelectedObject.get());
        pTempMoveObjects->push_back(pNewObj);
    }

    if (bCopyConnection)
    {
        CopyConnectionToSelectTmp(pDef);
    }

    return true;
}

/**
 *  オブジェクト判定
 *  @param    [in] uiActionFlag 動作フラグ
 *  @param    [in] pObj         表示オブジェクト
 *  @retval   なし
 *  @note     true 動作あり
 */
bool CViewAction::IsActionType(UINT uiActionFlag, CDrawingObject* pObj)
{
    if(!pObj)
    {
        return false;
    }
    bool bRet = false;
    DRAWING_TYPE       eType = pObj->GetType();

    if      ((eType == DT_POINT)  && (uiActionFlag & SEL_POINT))    {bRet = true;}
    else if ((eType == DT_LINE)   && (uiActionFlag & SEL_LINE))     {bRet = true;}
    else if ((eType == DT_CIRCLE) && (uiActionFlag & SEL_CIRCLE))   {bRet = true;}
    else if ((eType == DT_TEXT)   && (uiActionFlag & SEL_RICH_TEXT)){bRet = true;}
    else if ((eType == DT_ELLIPSE)&& (uiActionFlag & SEL_ELLIPSE))  {bRet = true;}
    else if (((eType & DT_DIM) ==  DT_DIM)  && (uiActionFlag & SEL_DIM))       {bRet = true;}
    else if ((eType == DT_IMAGE)  && (uiActionFlag & SEL_IMAGE))     {bRet = true;}

    else if ((eType == DT_SPLINE) && (uiActionFlag & SEL_SPLINE))       {bRet = true;}
    else if ((eType == DT_PARTS)  && (uiActionFlag & SEL_PARTS))        {bRet = true;}
    else if ((eType == DT_GROUP)  && (uiActionFlag & SEL_GROUP))        {bRet = true;}
    else if ((eType == DT_REFERENCE)  && (uiActionFlag & SEL_REFRENCE)) {bRet = true;}

    else if ((eType == DT_COMPOSITION_LINE)   && (uiActionFlag & SEL_COMPOSITION_LINE))     {bRet = true;}
    else if ((eType == DT_CONNECTION_LINE)   && (uiActionFlag & SEL_CONNECTION_LINE))     {bRet = true;}
    else if ((eType == DT_FIELD)   && (uiActionFlag & SEL_FIELD))   {bRet = true;}
    else if ((eType == DT_ELEMENT) && (uiActionFlag & SEL_ELEMENT)) {bRet = true;}
    else if ((eType == DT_NODE)    && (uiActionFlag & SEL_NODE))    {bRet = true;}


    return bRet;
}


/**
 *  @brief   ドラッグ中オブジェクト設定
 *  @param   [in]    ptet2d   描画位置
 *  @retval  なし
 *  @not     m_psAddObjにドラッグするオブジェクトを設定しておく
 *           pCtrl->DrawDragging(m_pView) で描画を行う
 */
void CViewAction::_SetDraggingObject(POINT2D ptet2d)
{
    CDrawMarker* pMarker= m_pView->GetMarker();
    CPartsDef*   pDef  = m_pView->GetPartsDef();

    pMarker->HideObject(false);
    STD_ASSERT(SetPosition(m_psAddObj.get(), ptet2d));

    //m_psAddObj->SetColor(DRAW_CONFIG->crImaginary);
	m_psAddObj->SetImaginary(true);
	m_psAddObj->SetVisible(true);
    pMarker->AddObject(m_psAddObj);
    pDef->SetMouseOver(m_psAddObj);
    //pCtrl->DrawDragging(m_pView);
}


/**
 *  @brief   マウス移動動作 ( 点を選択 )
 *  @param   [in]    pSnap   スナップ対称
 *  @param   [in]    posMouse   マウス位置        
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
void CViewAction::_DrawTooltip(VIEW_COMMON::E_SNAP_TYPE eSnap, MOUSE_MOVE_POS posMouse)
{
    StdString strSnap;
    if (eSnap == VIEW_COMMON::SNP_NONE)
    {
        //strSnap = GET_STR(STR_SNAP_NONE);
    }
    else
    {
        strSnap = VIEW_COMMON::GetSnapName(eSnap);
    }

    if (!strSnap.empty())
    {
        if((m_ptTooltip.x != posMouse.ptSel.x) ||
            (m_ptTooltip.y != posMouse.ptSel.y))
        {
            m_pView->SetToolTipText(strSnap.c_str());
        }
    }
    else
    {
        m_pView->ResetToolTipText();
    }
    m_ptTooltip = posMouse.ptSel;

}


/**
 *  @brief   マウス移動動作 (2オブジェクト選択による交点,線上の点を選択)
 *  @param   [in]    ptSel   選択位置
 *  @param   [in]    eAction 選択動作
 *  @param   [in]    nFlags   仮想キー         
 *  @retval  true 動作有り false 動作無し
 *  @note
 */
bool CViewAction::GetMousePointWithSpalateIntersection(POINT2D* pRet2d,
                    POINT2D* pMousePos2d,
                    MOUSE_MOVE_POS posMouse)
{


    //このメソッドでは、選択オブジェクトは１つまで

    using namespace VIEW_COMMON;
    StdString strToolTipText;

	bool bSelPoint = false;

    E_SNAP_TYPE eSnap2 = SNP_NONE;

    CPartsDef*   pDef  = m_pView->GetPartsDef();
    if (!pDef)
    {
        return false;
    }

    CDrawMarker* pMarker= m_pView->GetMarker();
    pMarker->Clear();
    pMarker->ClearObject();
    pDef->ClearMouseOver();
    pDef->ClearMouseEmphasis();
    pDef->ClearMouseConnectable();


    bool bMarkCenter = false;

    SNAP_DATA snap;
	bool bIngnoreGroup = false;
	bool bKeepCenter = false;
	int  iIgnoreObjectId = -1;
	m_pView->GetMousePoint(&snap, posMouse, 
		                          bIngnoreGroup, 
		                          bKeepCenter, 
		                          iIgnoreObjectId);

	int iObj1 = snap.iObject1;
	if ((snap.iObject1 == ID_PAPER_FRAME) ||
		(snap.iObject1 == ID_PRINT_FRAME) ||
		(snap.iObject1 == ID_GRID_DOT))
	{
		iObj1 = -1;
	}

    auto pObj1 = pDef->GetObjectById(iObj1);
    auto pObj2 = pDef->GetObjectById(snap.iObject2);

	//----------------
	*pRet2d = snap.pt;
	//----------------

	/*
    int iMarkerId = pMarker->MouseMove(posMouse.ptSel);
	DWORD dwSnapType = DRAW_CONFIG->GetSnapType();
    if (iMarkerId != -1)
    {
		//マーカ上を優先
        CDrawMarker::MARKER_DATA* pMarkerData;
		pMarkerData = pMarker->GetMarker( iMarkerId );
		if (dwSnapType & pMarkerData->eSnap)
		{
			snap.eSnapType = pMarkerData->eSnap;
			snap.pt = pMarkerData->ptObject;
			bSelPoint = true;
		}
    }
	*/
	std::shared_ptr<CDrawingObject> pFirstObject;
	if (m_lstSelectObj.size() > 0)
	{
		pFirstObject = m_lstSelectObj[0].lock();
	}
	
	if (!pFirstObject)
    {
		//--------------
        //初回
		//--------------
		bool bMidPoint = false;
        if (pObj1 && !pObj2)
        {
            DRAWING_TYPE eType = DT_NONE;
            eType = pObj1->GetType();

			pDef->SetMouseOver(pObj1);
			m_pView->SetMarker(pObj1.get(), true /*bIgnoreOffLineObject*/);

            if (eType != DT_POINT)
            {
			    if (snap.eSnapType != SNP_NONE)
			    {
				    bSelPoint = true;
			    }
            }
        }
		else if (!pObj1 && !pObj2)
		{
			//任意点
			bSelPoint = true;
		}
        else 
        {
			pDef->SetMouseEmphasis(pObj1);
			pDef->SetMouseEmphasis(pObj2);
			bSelPoint = true;
		}


		if (m_psAddObj)
		{
			if (bSelPoint)
			{
				pMarker->HideObject(false);
				STD_ASSERT(SetPosition(m_psAddObj.get(), snap.pt));
				m_psAddObj->SetVisible(true);
				pMarker->AddObject(m_psAddObj);
			}
			else
			{
				m_psAddObj->SetVisible(false);
			}
		}
	}
    else
    {
		//--------------
		// １つ目のオブジェクトを選択済み
		//--------------
		DWORD dwSnapType = DRAW_CONFIG->GetSnapType();

        //補助線の作成
        bool bExt1 = false;
        bool bExt2 = false;
        bool bMidPoint = false;

        POINT2D ptCreate;
        std::vector<POINT2D> lstPoint;

        if (pObj1)
        {
            pDef->SetMouseEmphasis(pObj1);
        }
        if (pObj2)
        {
            pDef->SetMouseEmphasis(pObj2);
        }

        for (int iCnt = 0; iCnt < 2; iCnt++)
        {
            m_psAddtionalObj[iCnt].reset();
        }

        DRAWING_TYPE eType = DT_NONE;
        if (m_psAddObj)
        {
            eType = m_psAddObj->GetType();
        }

		bool bOnLine = true;

        m_pView->SetMarker(pFirstObject.get(), false /*bIgnoreOffLineObject*/);

        if ( pFirstObject ->GetType() == DT_POINT) 
        {
            //最初に点を選択した場合
            POINT2D ptFirst;
			GetPosition(pFirstObject.get(), &ptFirst);
			//
            auto pLine = CreateLine(&m_psAddtionalObj[0]);
            pLine->SetPoint1(snap.pt);
            pLine->SetPoint2(ptFirst);
            ptCreate = (snap.pt + ptFirst) / 2.0;
            bExt1 = true;
            bMidPoint = true;
			bOnLine = false;
			bSelPoint = true;
			*pRet2d = ptCreate;
            snap.eSnapType = SNP_MID_POINT;
        }
        else if ( (pFirstObject != pObj1) &&
                   pObj1)
        {
			//交点
            pFirstObject->CalcIntersection (&lstPoint, pObj1.get(), false /*bOnLine*/);  

            if (lstPoint.size() > 0)
            {
                //最初に選択したオブジェクトと交点がある場合
                snap.pt.NearPoint_P(&ptCreate, lstPoint);
                bExt2 = CreateExtend(&m_psAddtionalObj[1],
                                    ptCreate,
                                    snap.iObject1);
				bOnLine = false;
                snap.eSnapType = SNP_CROSS_POINT;

                //マーカは削除する
				bSelPoint = true;
				*pRet2d = ptCreate;
				pMarker->Clear();
            }
        }
		else if (pObj1)
		{
			//マーカを表示
			m_pView->SetMarker(pObj1.get(), true /*bIgnoreOffLineObject*/);
		}

		bool bUseKeep = false;
		int iMarkerId = pMarker->MouseMove(posMouse.ptSel, bUseKeep);

		if (!bSelPoint)
		{
			auto eFirstType = pFirstObject->GetType();
			if ((eFirstType != DT_CIRCLE) &&
				(eFirstType != DT_LINE) &&
				(eFirstType != DT_SPLINE) &&
				(eFirstType != DT_ELLIPSE) &&
				(eFirstType != DT_COMPOSITION_LINE) &&
				(eFirstType != DT_CONNECTION_LINE))
			{
				bOnLine = false;
			}
		}

		if (iMarkerId != -1)
		{
			//マーカ上を優先
			CDrawMarker::MARKER_DATA* pMarkerData;
			pMarkerData = pMarker->GetMarker(iMarkerId);
			if (pMarkerData->iObjct == pFirstObject->GetId())
			{
				ptCreate = pMarkerData->ptObject;
				snap.eSnapType = pMarkerData->eSnap;
				bOnLine = false;
			}
		}

        if(bOnLine)
        {
            //線上の近接点を作成
            pFirstObject->NearPoint (&ptCreate, snap.pt, false /*bOnLine*/);  
            auto pLine = CreateLine(&m_psAddtionalObj[1]);
            pLine->SetPoint1(snap.pt);
            pLine->SetPoint2(ptCreate);
			*pRet2d = ptCreate;
            eSnap2 = SNP_ON_LINE;
            bExt2 = true;
        }

        if (!bMidPoint)
        {
            bExt1 = CreateExtend(&m_psAddtionalObj[0],
                                    ptCreate,
                                    pFirstObject->GetId());
        }


        if (bExt1)
        {
            pMarker->AddObject(m_psAddtionalObj[0]);
        }
        if (bExt2)
        {
            pMarker->AddObject(m_psAddtionalObj[1]);
        }
        snap.pt = ptCreate;

        if (m_psAddObj)
        {
            STD_ASSERT(SetPosition(m_psAddObj.get(), snap.pt));
            m_psAddObj->SetVisible(true);
            pMarker->AddObject(m_psAddObj);
        }
    }

    pDef->DrawDragging(m_pView);

    if (eSnap2 == SNP_NONE)
    {
        strToolTipText = GetSnapName(snap.eSnapType);
    }
    else
    {
        StdString strSnap;
        if (snap.eSnapType == SNP_NONE)
        {
            strSnap = GET_STR(STR_SNAP_NONE);
        }
        else
        {
            strSnap = GetSnapName(snap.eSnapType);
        }

        strToolTipText = 
            CUtil::StrFormat(_T("%s-%s"),
                GetSnapName(eSnap2).c_str(),
                strSnap.c_str());
    }

    if (!strToolTipText.empty())
    {
        if((m_ptTooltip.x != posMouse.ptSel.x) ||
            (m_ptTooltip.y != posMouse.ptSel.y))
        {
            m_pView->SetToolTipText(strToolTipText.c_str());
        }
    }
    else
    {
        m_pView->ResetToolTipText();
    }
    m_ptTooltip = posMouse.ptSel;
    return true;
}

bool CViewAction::CreateExtend(std::shared_ptr<CDrawingObject>* ppExtObj,
                            POINT2D pt2D,
                            int iObjId)
{
    CPartsDef*   pCtrl  = m_pView->GetPartsDef();
    auto pObj = pCtrl->GetObjectById(iObjId);

    if(!pObj)
    {
        return false;
    }

    bool bRet = false;
    switch(pObj->GetType())
    {
    case DT_LINE:
    {
        auto pLine = std::dynamic_pointer_cast<CDrawingLine>(pObj);
        if (pLine)
        {
            bRet = CreateExtendLine(ppExtObj, pt2D, pLine.get());
        }
        break;
    }

    case DT_CIRCLE:
    {
        auto pCircle = std::dynamic_pointer_cast<CDrawingCircle>(pObj);
        if (pCircle)
        {
            bRet = CreateExtendCircle(ppExtObj, pt2D, pCircle.get());
        }
        break;
    }

    case DT_ELLIPSE:
    {
        auto pEllipse = std::dynamic_pointer_cast<CDrawingEllipse>(pObj);
        if (pEllipse)
        {
            bRet = CreateExtendEllipse(ppExtObj, pt2D, pEllipse.get());
        }
        break;
    }
    default:
        break;
    }

    return bRet;
}

bool CViewAction::CreateExtendLine(std::shared_ptr<CDrawingObject>* ppExtObj1,
                                   POINT2D pt2D,
                                   const CDrawingLine* pLine)
{

    const LINE2D* pLine2D;
    pLine2D = pLine->GetLineInstance();

    if (pLine2D->IsOnLine(pt2D))
    {
        return false;
    }

    auto pExtLine = CreateLine(ppExtObj1);

    pExtLine->SetPoint1(pt2D);

    double d1 = pLine2D->GetPt1().Distance(pt2D);
    double d2 = pLine2D->GetPt2().Distance(pt2D);

    if (d1 < d2)
    {
        pExtLine->SetPoint2(pLine2D->GetPt1());
    }
    else
    {
        pExtLine->SetPoint2(pLine2D->GetPt2());
    }

    return true;
}

bool CViewAction::CreateExtendCircle(std::shared_ptr<CDrawingObject>* ppExtObj1,
                            POINT2D pt2D,
                            const CDrawingCircle* pCircle)
{
    const CIRCLE2D* pCircle2D;

    pCircle2D = pCircle->GetCircleInstance();
   
    if (pCircle2D->IsInsideAngle(pt2D))
    {
        return false;
    }

    auto pExtCircle = CreateCircle(ppExtObj1);
    pExtCircle->SetRadius(pCircle->GetRadius());
    pExtCircle->SetCenter(pCircle->GetCenter());

    double dS = pCircle->GetStartPoint().Distance(pt2D);
    double dE = pCircle->GetEndPoint().Distance(pt2D);


    double dAngle = pCircle->GetCenter().Angle(pt2D);

    if (dS < dE)
    {
        pExtCircle->SetStart(dAngle);
        pExtCircle->SetEnd(pCircle->GetStart());
    }
    else
    {
        pExtCircle->SetStart(pCircle->GetEnd());
        pExtCircle->SetEnd(dAngle);
    }
    return true;
}

bool CViewAction::CreateExtendEllipse(std::shared_ptr<CDrawingObject>* ppExtObj1,
                            POINT2D pt2D,
                            const CDrawingEllipse* pEllipse)
{
    //TODO:実装

    return false;
}

/**
 *  @brief   直線生成(補助線用)
 *  @param   [in] pObj  生成元
 *  @retval  直線へのポインタ
 *  @note
 */
std::shared_ptr<CDrawingLine> CViewAction::CreateLine(std::shared_ptr<CDrawingObject>* ppObj)
{
    std::shared_ptr<CDrawingLine> pRet;

    if (*ppObj)
    {
        if ((*ppObj)->GetType() != DT_LINE)
        {
            *ppObj = std::make_shared< CDrawingLine>();
        }
    }
    else
    {
        *ppObj= std::make_shared< CDrawingLine>();
    }

    pRet = std::dynamic_pointer_cast<CDrawingLine>(*ppObj);
    pRet->SetColor(DRAW_CONFIG->crAdditional);
    pRet->SetLineType(PS_DOT);
    return pRet;
}

/**
 *  @brief   円生成(補助線用)
 *  @param   [in] pObj  生成元
 *  @retval  円へのポインタ
 *  @note
 */
std::shared_ptr<CDrawingCircle> CViewAction::CreateCircle(std::shared_ptr<CDrawingObject>* ppObj)
{
    std::shared_ptr<CDrawingCircle> pRet;

    if (*ppObj)
    {
        if ((*ppObj)->GetType() != DT_CIRCLE)
        {
            *ppObj = std::make_shared< CDrawingCircle>();
        }
    }
    else
    {
        *ppObj= std::make_shared< CDrawingCircle>();
    }

    pRet = std::dynamic_pointer_cast<CDrawingCircle>(*ppObj);
    pRet->SetColor(DRAW_CONFIG->crAdditional);
    pRet->SetLineType(PS_DOT);
    return pRet;
}

/**
 *  @brief   楕円生成(補助線用)
 *  @param   [in] pObj  生成元
 *  @retval  楕円へのポインタ
 *  @note
 */
std::shared_ptr<CDrawingEllipse> CViewAction::CreateEllipse(std::shared_ptr<CDrawingObject>* ppObj)
{
    std::shared_ptr<CDrawingEllipse> pRet;

    if (*ppObj)
    {
        if ((*ppObj)->GetType() != DT_ELLIPSE)
        {
            *ppObj = std::make_shared< CDrawingEllipse>();
        }
    }
    else
    {
        *ppObj= std::make_shared< CDrawingEllipse>();
    }

    pRet = std::dynamic_pointer_cast<CDrawingEllipse>(*ppObj);
    pRet->SetColor(DRAW_CONFIG->crAdditional);
    pRet->SetLineType(PS_DOT);
    return pRet;
}

/**
 *  @brief   楕円生成(補助線用)
 *  @param   [in] pObj  生成元
 *  @retval  楕円へのポインタ
 *  @note
 */
std::shared_ptr<CDrawingPoint> CViewAction::CreatePoint(std::shared_ptr<CDrawingObject>* ppObj)
{
    std::shared_ptr<CDrawingPoint> pRet;

    if (*ppObj)
    {
        if ((*ppObj)->GetType() != DT_POINT)
        {
            *ppObj = std::make_shared<CDrawingPoint>();
        }
    }
    else
    {
        *ppObj= std::make_shared<CDrawingPoint>();
    }

    pRet = std::dynamic_pointer_cast<CDrawingPoint>(*ppObj);
    pRet->SetColor(DRAW_CONFIG->crAdditional);
    return pRet;
}

/**
 *  マウスオーバー解除
 *  @param    なし
 *  @retval   なし
 *  @note     Flip、Redrawで画面が強制的に書き換えられるときにクリアする
 */
bool CViewAction::CancelMouseOver() 
{
    if (m_pCurMouseOver)
    {
        m_pCurMouseOver->DrawOverEnd(m_pView);
        m_pCurMouseOver = NULL;
    }

    if (m_pCurMouseOverSecond)
    {
        m_pCurMouseOverSecond->DrawOverEnd(m_pView);
        m_pCurMouseOverSecond = NULL;
    }
    
    return true;
}

/**
 *  @brief  マウスオーバー解除
 *  @param  なし
 *  @retval なし
 *  @note   Flip、Redrawで画面が強制的に書き換えられるときにクリアする
 */
bool CViewAction::CancelMouseOver(CDrawingObject* pObj) 
{
    if (m_pCurMouseOver)
    {
        if (pObj != m_pCurMouseOver.get())
        {
            return false;
        }
        m_pCurMouseOver->DrawOverEnd(m_pView);
        m_pCurMouseOver = NULL;
    }


    if (m_pCurMouseOverSecond)
    {
        if (pObj != m_pCurMouseOverSecond)
        {
            return false;
        }
        m_pCurMouseOverSecond->DrawOverEnd(m_pView);
        m_pCurMouseOverSecond = NULL;
    }

    return true;
}

/**
 * @brief   コンテキストメニュー表示
 * @param   [in]    point  クリック位置
 * @retval  なし
 * @note
 */
bool  CViewAction::OnContextMenu(POINT point)
{
    return false;
}

/**
 * @brief   メニュー選択
 * @param   [in] nItemID   メニュー項目 ID 
 * @param   [in] nFlags    メニューのフラグの組み合わせ
 * @param   [in] hSysMenu  
 * @retval  なし
 * @note
 */
void CViewAction::OnMenuSelect(UINT nItemID, UINT nFlags, HMENU hSysMenu)
{
    switch(nItemID)
    {
    case ID_MNU_END:
        OnEnd();
        break;

    case ID_MNU_CANCEL:
        OnCancel();
        break;
    }
}



/**
 *  @brief   オブジェクト追加
 *  @param   [in]    pObject  追加オブジェクト
 *  @retval  追加した直線
 *  @note
 */
std::shared_ptr<CDrawingObject> CViewAction::AddObject(CDrawingObject* pObject)
{
    CPartsDef*   pDef  = m_pView->GetPartsDef();

    auto pClone = CDrawingObject::CloneShared(pObject);

	auto pDefault = THIS_APP->GetDefaultObject(pObject->GetType());

	COLORREF cr;
	if (pDefault)
	{
		cr = pDefault->GetColor();
	}
	else
	{
		cr = m_pView->GetCurrentLayer()->crDefault;
	}

    pClone->SetColor(cr);
    pClone->SetLayer(m_pView->GetCurrentLayerId());
    pDef->RegisterObject(pClone, true, false);
    pDef->Draw(pClone->GetId());
    return pClone;
}

/**
 *  @brief   線分追加
 *  @param   [in]    pt1  端点１
 *  @param   [in]    pt2  端点２
 *  @retval  追加した直線
 *  @note
 */
std::shared_ptr<CDrawingLine> CViewAction::RegisterLine(POINT2D pt1, POINT2D pt2)
{
    LINE2D  line2D(pt1, pt2);
    return RegisterLine(&line2D);
}

/**
 *  @brief   線分追加
 *  @param   [in]    pLin2D  線分成分
 *  @retval  追加した直線
 *  @note
 */
std::shared_ptr<CDrawingLine> CViewAction::RegisterLine(const LINE2D* pLin2D)
{
    if (pLin2D == NULL)
    {
        return false;
    }

    auto pLine = std::make_shared<CDrawingLine>(pLin2D);
    CPartsDef*   pDef = m_pView->GetPartsDef();

    pLine->SetLayer(m_pView->GetCurrentLayerId());
    pLine->SetColor(m_pView->GetCurrentLayer()->crBrush);
    pLine->SetLineType(m_pView->GetCurrentLayer()->iLineType);        
    pLine->SetLineWidth(m_pView->GetCurrentLayer()->iLineWidth);
    pDef->RegisterObject(pLine, true, false);

    return pLine;
}

/**
 *  @brief   無限直線追加
 *  @param   [in]    pt      追加点
 *  @param   [in]    dAngle  角度
 *  @retval  追加した直線
 *  @note
 */
std::shared_ptr<CDrawingLine> CViewAction::RegisterInfLine(POINT2D pt, double dAngle)
{
    LINE2D LineData( pt,  dAngle);
    return RegisterLine(&LineData);
}

/**
 *  @brief   円追加
 *  @param   [in]    pt2D    中心点
 *  @param   [in]    dAngle  半径
 *  @retval  追加した円
 *  @note
 */
std::shared_ptr<CDrawingCircle> CViewAction::RegisterCircle(POINT2D pt2D, double dRad)
{
    //円データを作成
    auto pCircle = std::make_shared<CDrawingCircle>();
    CPartsDef*   pDef = m_pView->GetPartsDef();

    pCircle->SetCenter(pt2D);
    pCircle->SetRadius(dRad);
    pCircle->SetLayer(m_pView->GetCurrentLayerId());
    pCircle->SetColor(m_pView->GetCurrentLayer()->crBrush);
    pCircle->SetLineType(m_pView->GetCurrentLayer()->iLineType);        
    pCircle->SetLineWidth(m_pView->GetCurrentLayer()->iLineWidth);
    pDef->RegisterObject(pCircle, true, false);

    return pCircle;
}

/**
 *  @brief   円追加
 *  @param   [in]    pCircle2D   円成分
 *  @retval  追加した円
 *  @note
 */
std::shared_ptr<CDrawingCircle> CViewAction::RegisterCircle(const CIRCLE2D* pCircle2D)
{
    //円データを作成
    auto pCircle = std::make_shared<CDrawingCircle>();
    CPartsDef*   pDef = m_pView->GetPartsDef();

    *pCircle->GetCircleInstance() = *pCircle2D;        

    pCircle->SetLayer(m_pView->GetCurrentLayerId());
    pCircle->SetColor(m_pView->GetCurrentLayer()->crBrush);
    pCircle->SetLineType(m_pView->GetCurrentLayer()->iLineType);        
    pCircle->SetLineWidth(m_pView->GetCurrentLayer()->iLineWidth);
    pDef->RegisterObject(pCircle, true, false);

    return pCircle;
}

/**
 *  点データ追加
 *  @param   [in]    pt2D    座標
 *  @retval   なし
 *  @note
 */
std::shared_ptr<CDrawingPoint> CViewAction::RegisterPoint(const POINT2D& pt2D)
{
    //点データを作成
    auto pPoint = std::make_shared<CDrawingPoint>();
    CPartsDef*   pDef = m_pView->GetPartsDef();

    pPoint->SetPoint(pt2D);
    pPoint->SetLayer(m_pView->GetCurrentLayerId());
    pPoint->SetColor(DRAW_CONFIG->crPoint);
    pPoint->SetLineType(m_pView->GetCurrentLayer()->iLineType);        
    pPoint->SetLineWidth(m_pView->GetCurrentLayer()->iLineWidth);

    pDef->RegisterObject(pPoint, true, false);

    return pPoint;
}

/**
 *  ノードデータ追加
 *  @param   [in]    pt2D    座標
 *  @retval   なし
 *  @note
 */
std::shared_ptr<CDrawingNode> CViewAction::RegisterNode(const POINT2D& pt2D)
{
    //点データを作成
    auto pNode = std::make_shared<CDrawingNode>();
    CPartsDef*   pDef = m_pView->GetPartsDef();

    pNode->SetPoint(pt2D);
    pNode->SetLayer(m_pView->GetCurrentLayerId());


    pDef->RegisterObject(pNode, true, false);

    return pNode;
}

/**
 *  @brief   楕円追加
 *  @param   [in]    pEllipse2D   楕円成分
 *  @retval  追加した楕円
 *  @note
 */
std::shared_ptr<CDrawingEllipse> CViewAction::RegisterEllipse(const ELLIPSE2D* pEllipse2D)
{
    //円データを作成
    auto pEllipse = std::make_shared<CDrawingEllipse>();
    CPartsDef*   pDef = m_pView->GetPartsDef();

    *pEllipse->GetEllipseInstance() = *pEllipse2D;        

    pEllipse->SetLayer(m_pView->GetCurrentLayerId());
    pEllipse->SetColor(m_pView->GetCurrentLayer()->crBrush);
    pEllipse->SetLineType(m_pView->GetCurrentLayer()->iLineType);        
    pEllipse->SetLineWidth(m_pView->GetCurrentLayer()->iLineWidth);

    pDef->RegisterObject(pEllipse, true, false);

    return pEllipse;
}

/**
 *  @brief   スナップツールチップ表示
 *  @param   [in]    eSnap    スナップ種別
 *  @param   [in]    dLength  長さ
 *  @param   [in]    dAngle   角度
 *  @retval  
 *  @note
 */
StdString CViewAction::GetSnapText(VIEW_COMMON::E_SNAP_TYPE eSnap, 
                                       double dLength,
                                       double dAngle,
                                       bool bEnabeNone)
{
    //表示小数点 3
    using namespace VIEW_COMMON;
    int iDecimalPlace = 3;

    StdString strString;
    strString = GetSnapName(eSnap, bEnabeNone);

    if(eSnap == SNP_DISTANCE)
    {
        StdStringStream   strmLen;
        strmLen << GET_STR(STR_LENGTH) << _T(":") 
			<< CDrawingObject::GetValText(dLength, false, iDecimalPlace);
        strString = strmLen.str();
    }
    else if (eSnap == SNP_ANGLE)
    {
        //角度と長さを表示する
        StdStringStream   strmLen;
        strmLen << GET_STR(STR_LENGTH) << _T(":")
			<< CDrawingObject::GetValText(dLength, false, iDecimalPlace);

        strmLen << _T(" ") << GET_STR(STR_ANGLE) << _T(":")
			<< CDrawingObject::GetValText(dAngle, true, iDecimalPlace);
        strString = strmLen.str();
    }

    return strString;

}

/**
 *  @brief   スナップツールチップ表示
 *  @param   [in]    eSnap    スナップ種別
 *  @param   [in]    dLength  長さ
 *  @param   [in]    dAngle   角度
 *  @param   [in]    bEnabeNone   任意点表示の有無
 *  @retval  
 *  @note
 */

void CViewAction::SetSnapTooltip(VIEW_COMMON::E_SNAP_TYPE eSnap, 
                                       double dLength,
                                       double dAngle,
                                       bool bEnabeNone)
{

    StdString strString;
    strString = GetSnapName(eSnap, bEnabeNone);
    if (strString == _T(""))
    {
        m_pView->ResetToolTipText();
        return;
    }
    strString = GetSnapText(eSnap,dLength, dAngle, bEnabeNone);
    m_pView->SetToolTipText(strString.c_str());
}


/**
 *  @brief   位置設定
 *  @param   [in]    pObj 設定オブジェクト
 *  @param   [in]    pt2D 設定位置
 *  @retval  true 成功
 *  @note
 */
bool CViewAction::SetPosition(CDrawingObject* pObj, POINT2D pt2D) const
{
    switch(pObj->GetType())
    {
    case DT_POINT:
    {
        CDrawingPoint* pPoint = reinterpret_cast<CDrawingPoint*>(pObj);
        pPoint->SetPoint(pt2D);
        break;
    }
    case DT_CIRCLE:
    {
        CDrawingCircle* pCirclet = reinterpret_cast<CDrawingCircle*>(pObj);
        pCirclet->SetCenter(pt2D);
        break;
    }
    case DT_TEXT:
    {
        CDrawingText* pText = reinterpret_cast<CDrawingText*>(pObj);
        pText->SetPoint(pt2D);
        break;
    }
    case DT_ELLIPSE:
    {
        CDrawingEllipse* pEllipse = reinterpret_cast<CDrawingEllipse*>(pObj);
        pEllipse->SetCenter(pt2D);
        break;
    }
    case DT_NODE:
    {
        CDrawingNode* pNode = reinterpret_cast<CDrawingNode*>(pObj);
        pNode->SetPoint(pt2D);
        break;
    }
    case DT_FIELD:
    case DT_PARTS:
    {
        CDrawingParts* pGroup = reinterpret_cast<CDrawingParts*>(pObj);
        pGroup->SetPoint(pt2D);
        break;
    }
    case DT_REFERENCE:
    {
        CDrawingReference* pRef = reinterpret_cast<CDrawingReference*>(pObj);
        pRef->SetPoint(pt2D);
        break;
    }
    case DT_ELEMENT:
    {
        CDrawingElement* pElement = reinterpret_cast<CDrawingElement*>(pObj);
        pElement->SetPoint(pt2D);
        break;
    }
    case DT_IMAGE:
    {
        CDrawingImage* pImage = reinterpret_cast<CDrawingImage*>(pObj);
        pImage->SetPoint(pt2D);
        break;
    }

    default:
        return false;
    }
    return true;
}

/**
 *  @brief   位置取得
 *  @param   [in]    pObj 設定オブジェクト
 *  @param   [out]   pPt2D 取得位置
 *  @retval  true 成功
 *  @note
 */
bool CViewAction::GetPosition(CDrawingObject* pObj, POINT2D* pPt2D) const
{
    switch(pObj->GetType())
    {
    case DT_POINT:
    {
        CDrawingPoint* pPoint = reinterpret_cast<CDrawingPoint*>(pObj);
        *pPt2D = pPoint->GetPoint();
        break;
    }
    case DT_CIRCLE:
    {
        CDrawingCircle* pCirclet = reinterpret_cast<CDrawingCircle*>(pObj);
        *pPt2D = pCirclet->GetCenter();
        break;
    }
    case DT_TEXT:
    {
        CDrawingText* pText = reinterpret_cast<CDrawingText*>(pObj);
        *pPt2D = pText->GetPoint();
        break;
    }
    case DT_ELLIPSE:
    {
        CDrawingEllipse* pEllipse = reinterpret_cast<CDrawingEllipse*>(pObj);
        *pPt2D = pEllipse->GetCenter();
        break;
    }
    case DT_NODE:
    {
        CDrawingNode* pNode = reinterpret_cast<CDrawingNode*>(pObj);
        *pPt2D = pNode->GetPoint();
        break;
    }
    case DT_FIELD:
    case DT_PARTS:
    {
        CDrawingParts* pGroup = reinterpret_cast<CDrawingParts*>(pObj);
        *pPt2D = pGroup->GetPoint();
        break;
    }
    case DT_REFERENCE:
    {
        CDrawingReference* pRef = reinterpret_cast<CDrawingReference*>(pObj);
        *pPt2D = pRef->GetPoint();
        break;
    }
    case DT_CONNECTION_LINE:
    {
        CDrawingConnectionLine* pConnectionLine = reinterpret_cast<CDrawingConnectionLine*>(pObj);
        
        COMMENT_ASSERT(false, _T("要実装"));
        break;
    }
    case DT_ELEMENT:
    {
        CDrawingElement* pElemet = reinterpret_cast<CDrawingElement*>(pObj);
        *pPt2D = pElemet->GetPoint();
        break;

    }

        

    default:
        return false;
    }
    return true;
}


/**
 * @brief   マーカクリック確認
 * @param   [out]    pMarkerPos  マーカー位置
 * @param   [in]     ptSel       マウスカーソル位置
 * @param   [in]     eAction     マウスの動作
 * @retval  true マーカクリック
 * @note	
 */
bool CViewAction::_CheckMarker( POINT2D* pMarkerPos, 
                                POINT ptSel, 
                                SEL_ACTION eAction) const
{
    //マーカをクリック
    if (eAction == ACT_LBUTTON_UP)
    {
        CDrawMarker*pMarker = m_pView->GetMarker();
        int iId = pMarker->CheckCkick( ptSel );
#if 1 //_DEBUG_PRINT
int iId2 = pMarker->MouseMove(ptSel);
DB_PRINT(_T("_CheckMarker %d,%d\n"), iId, iId2);
#endif
        if (iId == -1)
        {
            //マーカー以外をクリック
            return false;
        }

        CDrawMarker::MARKER_DATA* pData;
        pData = pMarker->GetMarker(iId);
        *pMarkerPos = pData->ptObject;

        pMarker->Clear();
        return true;
    }
    return false;
}

bool CViewAction::_CheckMarkerObject( POINT2D* pMarkerPos, 
    int iObjctId,
    POINT ptSel, 

    SEL_ACTION eAction) const
{
    //マーカをクリック
    if (eAction == ACT_LBUTTON_UP)
    {
        CDrawMarker*pMarker = m_pView->GetMarker();
        int iId = pMarker->CheckCkickAndObject( ptSel , iObjctId);

        if (iId == -1 )
        {
            //マーカー以外をクリック
            return false;
        }

        CDrawMarker::MARKER_DATA* pData;
        pData = pMarker->GetMarker(iId);
        *pMarkerPos = pData->ptObject;

        pMarker->Clear();
        return true;
    }
    return false;
}

/**
 *  動作クリア
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CViewAction::ClearAction(bool bReleaseSelect)
{
    m_iClickCnt = 0;
    m_lstSelectObj.clear();

    if (m_pView)
    {
        CPartsDef*   pDef  = m_pView->GetPartsDef();
		CDrawMarker* pMarker= m_pView->GetMarker();

        pMarker->Clear();
        pMarker->ClearObject();

        pDef->ClearMouseOver();
        pDef->ClearMouseEmphasis();
        pDef->ClearMouseConnectable();
        pDef->GetConnectingTmpObjectList()->clear();
        pDef->GetTmpGroupObjectList()->clear();

        if (bReleaseSelect)
        {
            pDef->RelSelect();
        }
        m_pView->ResetToolTipText();
    }

    for (int iCnt = 0; iCnt < 2; iCnt++)
    {
        m_psAddtionalObj[iCnt].reset();
    }

    if (m_bInterrupt)
    {
        m_bInterrupt = false;
        m_pView->RetInterrupt();
    }
}

//割り込み復帰
void CViewAction::SetInterrupt()
{ 
    m_bInterrupt = true;
}

//割り込み復帰
bool CViewAction::IsInterrupt()
{ 
    return m_bInterrupt;
}


//割り込み復帰
void CViewAction::ReturnInterrupt()
{ 
    if (m_pView && m_bInterrupt)
    {
        m_pView->RetInterrupt();
        m_bInterrupt = false;
    }
}

//割り込み解除
void CViewAction::ReleaseInterrupt()
{ 
    m_bInterrupt = false;
}

bool CViewAction::ExecuteString(double* pVal, const StdString& code)
{
    return THIS_APP->GetScriptEngine()->ExecuteString(pVal, code);
}

/*
 *  @brief   マウスオーバー選択
 *  @param   [in] posMouse  マウス位置
 *  @retval  選択オブジェクト
 *  @note    マウスオーバーが可能なオブジェクトがマウス上にあれば
 *            マウスオーバーを設定しそのオブジェクトを返す
 */
std::shared_ptr<CDrawingObject> CViewAction::SearchMouseOver(MOUSE_MOVE_POS posMouse) 
{ 
    POINT ptSel = posMouse.ptSel;
    UINT nFlags = posMouse.nFlags;

    CPartsDef*   pDef  = m_pView->GetPartsDef();
    int                  iId  = m_pView->SearchPos(ptSel);

    if (!pDef)
    {
        return NULL;
    }
    pDef->ClearMouseOver();


    m_pView->SetDrawingMode(DRAW_FRONT);
    if (m_pCurMouseOver != NULL)
    {
        if (m_pCurMouseOver->GetId() != iId)
        {
            m_pCurMouseOver->DrawOverEnd(m_pView);
            m_pCurMouseOver = NULL;
        }
    }

    if (iId == -1)
    {
        return NULL;
    }


    auto  pObj  = pDef->GetObjectById(iId);

    if(!pObj)
    {
        return NULL;
    }

    DRAWING_TYPE       eType = pObj->GetType();

    bool bStart;
    bStart = IsActionType(m_lMouseOver, pObj.get());

    if (!bStart)
    {
        return NULL;
    }
    
    pObj->DrawOverStart(m_pView);
    m_pCurMouseOver = pObj;
    pDef->SetMouseOver(pObj);

    return pObj;
}

//全ノード位置の更新と関連オブジェクト、その関係の更新
bool CViewAction::UpdateAllNodePositionAndRelation(
    CPartsDef*   pDef,
    std::vector<NODE_CHANGE_DATA>* pList,
    std::set<int>* pLstUsedObject,
    CUndoAction* pUndo)
{
    if (!pDef)
    {
        return false;
    }
#if _DEBUG_PRINT

DB_PRINT(_T("CViewAction::UpdateAllNodePositionAndRelation\n"));
for (auto data : *pList)
{
    DB_PRINT(_T("  NODE_CHANGE bPlug(%s) Chg(%d:%d) Src(%d:%d) pt(%s)\n"),
        data.bPlug?_T("T"): _T("F"),
        data.iChangeObjectId, data.iChangeNodeIndex,
        data.iSrcId,data.iSrcIndex,
        data.pt.Str().c_str()
    );
}

#endif
    //=========================================
    auto Check = [=](NODE_CHANGE_DATA& data,
        std::shared_ptr<CDrawingObject>* pS, //
        std::shared_ptr<CDrawingObject>* pD,
        bool *pDrS,
        bool *pDrD)
    {
        CNodeData* pConnection = NULL;

        *pDrS = false;
        auto pSrc = pDef->GetDraggingObject(data.iSrcId);
//For Debug
        auto pSrcOrg = pDef->GetObjectById(data.iSrcId);
//
        if (!pSrc)
        {
            pSrc = pDef->GetObjectById(data.iSrcId);
        }
        else
        {
            *pDrS = true;
        }

        if (!pSrc)
        {
            return false;
        }

        pConnection = pSrc->GetNodeData(data.iSrcIndex);

        if (!pConnection)
        {
            return false;
        }

        *pDrD = false;
        auto pDst = pDef->GetDraggingObject(data.iChangeObjectId);
        if (!pDst)
        {
            pDst = pDef->GetObjectById(data.iChangeObjectId);
        }
        else
        {
            *pDrD = true;
        }

        if (!pDst)
        {
            return false;
        }

        *pS = pSrc;
        *pD = pDst;
        return true;
    };
    //=========================================

    CUndoAction* pTmpUndo;
    //ノードの移動があるオブジェクト
    for (int iCnt = 0; iCnt < 2; iCnt++)
    {
        for (auto data : *pList)
        {
            std::shared_ptr<CDrawingObject> pSrcObj;
            std::shared_ptr<CDrawingObject> pDstObj;
            bool bDraggingSrc;
            bool bDraggingDst;

            if (!Check(data, &pSrcObj, &pDstObj, 
                       &bDraggingSrc, &bDraggingDst))
            {
                continue;
            }

            pTmpUndo = pUndo;

            if ( !data.bPlug && iCnt == 0)
            {
                //--------------------
                //  ソケット側の変更  
                //     pSrcObjがソケット側
                //     pDstObjはプラグ側になる
                //--------------------
                auto pPlug = pDef->GetDraggingObject(data.iChangeObjectId);
                auto pPlugOrg = pDef->GetObjectById(data.iChangeObjectId);    //ドラッグ前の図形
#if _DEBUG_PRINT
auto pSocket = pDef->GetDraggingObject(data.iSrcId);
auto pSocketOrg = pDef->GetObjectById(data.iSrcId);    //ドラッグ前の図形
                //pDstObj == pPlug
				if (pDstObj)
				{
					if (pDstObj->GetTmpCopy())
					{
DB_PRINT(_T("UANPAR TmpCopy Plug%d %I64x\n"), pDstObj->GetId(), pDstObj.get());
						continue;
					}
				}

				
DB_PRINT(_T("UANPAR Plug1%d %I64x\n"), pDstObj->GetId(), pDstObj.get());
if (pPlug)
{
	DB_PRINT(_T("UANPAR Plug2%d %I64x\n"), pPlug->GetId(), pPlug.get());
}
DB_PRINT(_T("UANPAR PlugOrg%d %I64x\n"), pPlugOrg->GetId(), pPlugOrg.get());

pDstObj->AddTag(_T("Test"));
#endif



                POINT2D ptDest;
                pDstObj->GetNodeDataPos(&ptDest, data.iChangeNodeIndex);

                bool bMove = true;
                if (ptDest.Distance(data.pt) < NEAR_ZERO)
                {
                    bMove = false;
                    if (!pTmpUndo)
                    {
#if _DEBUG_PRINT
                        DB_PRINT(_T("UANPAR  NoMove %s\n"), ptDest.Str().c_str());
#endif
                        continue;
                    }
                    else
                    {
                        if (pPlugOrg)
                        {
                            POINT2D ptOrg;
                            pPlugOrg->GetNodeDataPos(&ptOrg, data.iChangeNodeIndex);
                            if (ptOrg.Distance(data.pt) < NEAR_ZERO)
                            {
#if _DEBUG_PRINT
    DB_PRINT(_T("UANPAR  NoMove2 %s\n"), ptDest.Str().c_str());
#endif
                                continue;
                            }
                        }
                        else
                        {
#if _DEBUG_PRINT
    DB_PRINT(_T("UANPAR  NoMove3 %s\n"), ptDest.Str().c_str());
#endif
                            continue;
                        }
                    }
                }

#if _DEBUG_PRINT
    DB_PRINT(_T("UANPAR  Move %s \n"), ptDest.Str().c_str());
#endif
                if (pTmpUndo &&  pPlug )
                {
#if _DEBUG_PRINT
    DB_PRINT(_T("UANPAR  UNDO \n"));
#endif
                    //pPlug ＝ pDstObj
                    STD_ASSERT(pDstObj.get() == pPlug.get());
                    pUndo->AddStart(UD_CHG, pDef, pPlugOrg.get());
					pPlug->ClearStatus();
					pDef->ReplaceObject(pPlugOrg, pPlug);
                }

                if (bMove)
                {
                    pDstObj->OnUpdateNodeSocketData(&data.pt,
                        pSrcObj.get(),
                        data.iSrcIndex,
                        data.iChangeNodeIndex,
                        pLstUsedObject,
                        pTmpUndo);
                }


                if (pTmpUndo && pPlug)
                {
                    pUndo->AddEnd(pPlug, false);
                }


            }
            else if (data.bPlug && iCnt == 1)
            {
                //--------------------
                //  プラグ側の変更  
                //     pDstObjはプラグになる
                //--------------------
                POINT2D ptDest;
                pDstObj->GetNodeDataPos(&ptDest, data.iChangeNodeIndex);
                if (ptDest.Distance(data.pt) > NEAR_ZERO)
                {
                    //距離が離れたら削除
                    auto pConnectionDest = pDstObj->GetNodeData(data.iChangeNodeIndex);
                    auto pConnectionSrc  = pSrcObj->GetNodeData(data.iSrcIndex);

                    //相手側のデータを削除
                    if (pUndo)
                    {
                        pUndo->AddStart(UD_CHG, pDef, pDstObj.get());
                    }

                    if (!bDraggingDst)
                    {
                        pConnectionDest->DeletePlug(pSrcObj.get(), data.iSrcIndex);
                    }

                    if (pUndo)
                    {
                        pUndo->AddEnd(pDstObj, false);
                    }


                    if (pUndo)
                    {
                        pUndo->AddStart(UD_CHG, pDef, pSrcObj.get());
                    }

                    if (!bDraggingSrc)
                    {
                        pConnectionSrc->DeletePlug(pDstObj.get(), data.iChangeNodeIndex);
                    }

                    if (pUndo)
                    {
                        pUndo->AddEnd(pDstObj, false);
                    }

                }
            }
        }
    }
#if _DEBUG_PRINT
    DB_PRINT(_T("CViewAction::UpdateAllNodePositionAndRelation -- END\n\n"));
#endif
    return true;
}

bool CViewAction::ApplyMatrix(CPartsDef* pDef,
							 std::shared_ptr<CDrawingObject> pObj, 
	                          MAT2D matApply, int iTimes, bool bCopy)
{
#ifdef DEBUG
	auto pConnectionListInDef = pDef->GetListChangeByConnection();
#endif // DEBUG

	if (!pObj)
	{
		return false;
	}

	CUndoAction* pUndo = pDef->GetUndoAction();
	bool bTmp = false;
	if (!bCopy)
	{
		if (pObj->GetType() == DT_GROUP)
		{
			auto pGroup = std::dynamic_pointer_cast<CDrawingGroup>(pObj);
			pDef->SetLockUpdateNodeData(true);
			if (pGroup->GetId() == ID_TEMP_GROUP)
			{
				//一時オブジェクトの場合(CActionNodeSelect::SelActionで設定)
				bTmp = true;
				auto pParent = pGroup->GetParentParts();
				auto oldList = pGroup->GetList();
				std::vector<CDrawingObject*> lstPlug;
				for (auto pGroupObj : *oldList)
				{
					//元のオブジェクトを取得する
					int iId = pGroupObj->GetId();
					auto pOrg = pDef->GetObjectById(iId);
					if (pOrg)
					{
						pUndo->AddStart(UD_CHG, pDef, pOrg.get());
						pOrg->Matrix(matApply);
						pOrg->ClearStatus();
						pOrg->SetSelect(false);
						pUndo->AddEnd(pOrg);
					}
				}
			}
		}

		if (!bTmp)
		{
			//元のオブジェクトを取得する
			int iId = pObj->GetId();
			auto pOrg = pDef->GetObjectById(iId);

			pUndo->AddStart(UD_CHG, pDef, pOrg.get());
			pOrg->Matrix(matApply);
			pUndo->AddEnd(pOrg);
		}
	}
	else
	{
		pDef->SetLockUpdateNodeData(true);
		if (iTimes < 1) { iTimes = 1; }
		POINT2D ptMove;
		for (int iCnt = 0; iCnt < iTimes; iCnt++)
		{
			//接続リストに従って更新しない
			pDef->SetLockUpdateNodeData(true);
			std::map<int, int> mapConvId;
			std::vector<int> lstConnection;

			mapConvId[-1] = -1;
			MAT2D m2 = matApply;

			m2.MultiAffine(double(iCnt) + 1.0);

			if (pObj->GetType() == DT_GROUP)
			{
				pDef->SetLockUpdateNodeData(true);
				auto pGroup = std::dynamic_pointer_cast<CDrawingGroup>(pObj);
				if (pGroup->GetId() == ID_TEMP_GROUP)
				{
					//一時オブジェクトの場合(CActionNodeSelect::SelActionで設定)
					bTmp = true;

					auto pParent = pGroup->GetParentParts();
					auto oldList = pGroup->GetList();

					std::vector<CDrawingObject*> lstPlug;
					for (auto pGroupObj : *oldList)
					{
						//元のオブジェクトを取得する
						int iId = pGroupObj->GetId();
						auto pOrg = pDef->GetObjectById(iId);
						auto pNewObj = CDrawingObject::CloneShared(pOrg.get());

						if (pOrg)
						{
							pNewObj->Matrix(m2);
							pNewObj->ClearStatus();
							pNewObj->SetSelect(false);
							pDef->RegisterObject(pNewObj, true, true);

							mapConvId[pOrg->GetId()] = pNewObj->GetId();
							if (pOrg->GetNodeDataNum() > 0)
							{
								lstConnection.push_back(pNewObj->GetId());
							}
							else
							{
								pUndo->Add(UD_ADD, pDef, NULL, pNewObj);
							}
						}
					}
				}
			}
			else
			{
				auto pNewObj = CDrawingObject::CloneShared(pObj.get());
				pNewObj->ClearStatus();
				pNewObj->SetSelect(false);
				pNewObj->Matrix(m2);
				pDef->RegisterObject(pNewObj, true, true);
				mapConvId[pObj->GetId()] = pNewObj->GetId();
				if (pObj->GetNodeDataNum() > 0)
				{
					lstConnection.push_back(pNewObj->GetId());
				}
				else
				{
					pUndo->Add(UD_ADD, pDef, NULL, pNewObj);
				}
			}

			//  接続ID更新
			bool bKeepUnfindId = false;
			pDef->UpdateConnectionAndPosition(pUndo, &mapConvId, &lstConnection, bKeepUnfindId);

			//接続による変更はない	
			pDef->GetListChangeByConnection()->clear();
		}
		pDef->SetLockUpdateNodeData(false);
	}

	//---------------------
	// 接続しているオブジェクトの変更
	//---------------------
	if (!bCopy)
	{
		std::set<int> lstObj;
		UpdateAllNodePositionAndRelation(pDef,
			pDef->GetListChangeByConnection(),
			&lstObj,
			pUndo);
	}

	pDef->SetLockUpdateNodeData(false);

	pUndo->Push();
	return true;
}


/**
 *  選択時動作
 *  @param   [in]    eMode 選択時点でのモード
 *  @retval   なし
 *  @note
 */
std::shared_ptr<CDrawingObject> CViewAction::GroupSelectObject(CPartsDef* pDef, bool bSetTmp)
{

    std::set<int> lstChg;
    std::set<int> lstSelect;


    std::shared_ptr<CDrawingObject>  pObject;


    if (pDef->SelectNum() == 1)
    {
        pObject = pDef->GetSelect(0);
		pObject->SetTmpCopy(false);
	}
    else
    {

        //仮のグループを作ってそこに登録する。
        /*
            ストレッチ部（ノード移動により変更を受けるものは除く）
                グループに追加（ストレッチ状態の選択済み）

            ノード移動により変更を受けるものは
                GetTmpGroupObjectListにコピーを追加する
                これは、IDで検索を行った場合優先でTmpGroupObjectListを
                見に行くため
        */

        std::shared_ptr<CDrawingGroup> psGroup;

        psGroup = std::make_shared<CDrawingGroup>();
        psGroup->SetPartsDef(pDef);
        psGroup->SetLayer(m_pView->GetCurrentLayerId());
        psGroup->SetId(ID_TEMP_GROUP); //一時グループ用ID
        pDef->SetLockUpdateNodeData(true);

        auto pList = pDef->GetSelectInstance();
        for (auto wObj : *pList)
        {
            auto pObj = wObj.lock();
            if (!pObj)
            {
                continue;
            }
            static const bool bCreateId = false;
            static const bool bKeepUnfindId = true;
            static const bool bKeepSelect = true;
			pObj->SetTmpCopy(bSetTmp);

            psGroup->AddData(pObj, bCreateId, bKeepUnfindId, bKeepSelect);
        }

        pDef->SetLockUpdateNodeData(false);
        pObject = psGroup;
    }
    return pObject;
}

void CViewAction::CopyConnectionToSelectTmp(CPartsDef* pDef)
{
    //------------------------------------------
    // 接続によって影響を受ける部分をコピーする
    //------------------------------------------
    // CPartsDef::SelectDrawingObjectで設定済み
    auto pLstConnect = pDef->GetConnectionToSelect();
    for (auto wSrc : *pLstConnect)
    {
        auto pSrc = wSrc.lock();
        if (!pSrc)
        {
            continue;
        }

        if (pSrc->IsSelect())
        {
            continue;
        }

        std::shared_ptr<CDrawingObject> pCopy = CDrawingObject::CloneShared(pSrc.get());

        if (pSrc->GetRelation())
        {
            pCopy->SetImaginary(true);
            pDef->SetMouseEmphasis(pCopy);
        }

        pDef->GetConnectingTmpObjectList()->push_back(pCopy);
    }
}

bool CViewAction::Undo()
{ 
    CPartsDef*   pDef;
    if (!m_pView)
    {
        return false;
    }

    pDef = m_pView->GetPartsDef();

    if (!pDef->GetUndoAction()->Undo())
    {
        //これ以上やり直しはできません
        AfxMessageBox(GET_STR(STR_ERROR_MORE_REDO));
        return false;
    }

    return true;
}

bool CViewAction::IsUndoEmpty() 
{
    CPartsDef*   pDef;
    if (!m_pView)
    {
        return false;
    }

    pDef = m_pView->GetPartsDef();
    if (pDef)
    {
        return pDef->GetUndoAction()->IsUndoEmpty();
    }

    return false;
}


bool CViewAction::Redo() 
{ 
    return false; 
}

bool CViewAction::IsRedoEmpty()
{ 
    return false; 
}
