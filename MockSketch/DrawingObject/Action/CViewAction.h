/**
 * @brief			CViewActionヘッダーファイル
 * @file			CViewAction.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#if !defined(EA_2FB152AB_15EA_47cf_9AAD_3FE7099CD620__INCLUDED_)
#define EA_2FB152AB_15EA_47cf_9AAD_3FE7099CD620__INCLUDED_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "View/VIEW_MODE.h"
#include "View/ViewCommon.h"
class     CDrawingView;
class     CDrawingObject;
class     POINT2D;
class     LINE2D;
class     RECT2D;
class     MAT2D;
class     CIRCLE2D;
class     ELLIPSE2D;
class     CDrawingLine;
class     CDrawingCircle;
class     CDrawingPoint;
class     CDrawingEllipse;
class     CDrawingNode;
class     CDrawMarker;
class     CUndoAction;
class     CPartsDef;
class     CDrawingGroup;
struct    SNAP_DATA;
struct    NODE_CHANGE_DATA;
#pragma warning(disable:4351)
/**
 * @class   CViewAction
 * @brief                        
   
 */
class CViewAction
{
public:
    enum SEL_ACTION
    {
        ACT_NONE,
        ACT_RBUTTON_UP,
        ACT_RBUTTON_DOWN,
        ACT_LBUTTON_UP,
        ACT_LBUTTON_DOWN,
        ACT_MOVE,
    };

    //---------------------
    //  選択可能フラグ
    //---------------------
    enum SEL_ACTION_FLAG
    {
        SEL_NONE       = 0x00001,
        SEL_POINT      = 0x00002,
        SEL_LINE       = 0x00004,
        SEL_CIRCLE     = 0x00008,
        SEL_ELLIPSE    = 0x00010,
        SEL_SPLINE     = 0x00020,
        SEL_COMPOSITION_LINE       = 0x00080,
        SEL_SEGMENT     = SEL_LINE|SEL_CIRCLE|SEL_ELLIPSE|SEL_SPLINE|SEL_COMPOSITION_LINE,

        SEL_GROUP      = 0x00100,

        SEL_ELEMENT    = 0x00200,
        SEL_NODE       = 0x00400,
        SEL_CONNECTION_LINE  = 0x00800,
        SEL_FIELD      = 0x01000,
        SEL_IMAGE      = 0x02000,
        SEL_REFRENCE   = 0x04000,
        SEL_PARTS      = 0x08000,

        SEL_RICH_TEXT  = 0x10000,
        SEL_DIM        = 0x20000,

        SEL_ALL        = 0xFFFFF,   //全図形
        SEL_PRI        = 0x00FFF,   //基本図形
        SEL_FIG        = 0x0FFFF,   //全形状
        SEL_PRI_NOPT   = 0x000FD,   //点以外の基本図形
        SEL_FIG_NOPT   = 0x0FFFD,   //点以外の全形状
    };

    /*
    //---------------------
    //  選択動作
    //---------------------
    enum SEL_TYPE
    {
        TYPE_ANY        = 0x00001,  //任意点
        TYPE_POINT1     = 0x00002,  //選択点(点選択を除く)
        TYPE_POINT2     = 0x00004,  //選択点(点選択を含む)
        TYPE_OBJECT     = 0x00008,  //オブジェクト
        TYPE_LOOP       = 0x00010,
    };

    //---------------------
    //  選択動作
    //---------------------
    struct SEL_SEQ_DATA
    {
        int         iActionType;
        StdString   strMsgStart;
        StdString   strMsgEnd;
        StdString   strMsgError;
        //boost::function< bool (void*,  CDrawingObject* pObj, MOUSE_MOVE_POS posMouse, SEL_ACTION eAction) > func;
    };

    struct SEL_SEQ
    {
        //実行シーケンス位置
        int iSeqPos;

        //!< 選択動作
        std::vector<SEL_SEQ_DATA>   lstSelSeq;
    }
    */



public:
    //!< コンストラクタ
    CViewAction(CDrawingView* pView):m_iClickCnt(0), 
        m_bInterrupt(false),
        m_pCurMouseOver(0), 
        m_pCurMouseOverSecond(0),
        m_psAddObj(0),
        m_psAddtionalObj{},
        m_bMark(false), 
        m_lMouseOver(0),
        m_eViewMode(VIEW_NONE){ m_pView = pView;}

	//!< コンストラクタ
    CViewAction():CViewAction(NULL){;}


	//!< デストラクタ
    virtual ~CViewAction();

    //!< 選択解放時動作
    VIEW_MODE GetMode() {return m_eViewMode;}

	//!< 選択解放時動作
    virtual void Cancel(VIEW_MODE eMode) = 0;

	//!< 選択時動作
    virtual bool SelAction(VIEW_MODE eMode, void* pParam = NULL) = 0;

    //!< 選択完了
	virtual void SelDone() {;}

    //!< 再描画
    virtual void Redraw() const {;}

	//!< 割り込み設定
    virtual void SetInterrupt();

	//!< 割り込み問い合わせ
    virtual bool IsInterrupt();

    //!< 割り込み解除
    virtual void ReleaseInterrupt();

    //!< 割り込み復帰
    virtual void ReturnInterrupt();

	//!< マウスクリック動作
	virtual bool SelPoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction) { return false;}

	//!< マウス移動動作
	virtual bool MovePoint(MOUSE_MOVE_POS posMouse, SEL_ACTION eAction);

	//!< マウスダブルクリック
    virtual bool DblClick(MOUSE_MOVE_POS posMouse){ return false;}

    //!<マウスオーバー解除
    virtual bool CancelMouseOver();

    //!<マウスオーバー解除
    virtual bool CancelMouseOver(CDrawingObject* pObj);

    //!<コンテキストメニュー表示
    virtual bool OnContextMenu(POINT point);

    //!< メニュー選択
    virtual void OnMenuSelect(UINT nItemID, UINT nFlags, HMENU hSysMenu);

    //!<入力値設定
    virtual bool InputValue(StdString strVal){return false;}

    //!< 点選択によるオブジェクト追加
    bool AddObjByPoint(CDrawingObject* pAddObject, MOUSE_MOVE_POS posMouse, SEL_ACTION eAction, int iMaxSel);
    
    //!< オブジェクト追加
	virtual std::shared_ptr<CDrawingObject> AddObject(CDrawingObject* pObject);

    //!< 線分追加
	virtual std::shared_ptr<CDrawingLine> RegisterLine(POINT2D pt1, POINT2D pt2);

    //!< 線分追加
	virtual std::shared_ptr<CDrawingLine> RegisterLine(const LINE2D* pLine);

    //!< 直線追加
    virtual std::shared_ptr<CDrawingLine> RegisterInfLine(POINT2D pt, double dAngle);

    //!< 円追加
    virtual std::shared_ptr<CDrawingCircle> RegisterCircle(POINT2D pt2D, double dRad);

    //!< 円追加
    virtual std::shared_ptr<CDrawingCircle> RegisterCircle(const CIRCLE2D* pCircle);

    //!< 点データ追加
    virtual std::shared_ptr<CDrawingPoint> RegisterPoint(const POINT2D& pt2D);

    //!< 節点データ追加
    virtual std::shared_ptr<CDrawingNode> RegisterNode(const POINT2D& pt2D);

    //!< 楕円追加
    virtual std::shared_ptr<CDrawingEllipse> RegisterEllipse(const ELLIPSE2D* pEllipse2D);

    //!< オブジェクト判定
    bool IsActionType(UINT uiActionFlag, CDrawingObject* pObj);

    //!< アンドゥ
    virtual bool Undo();

    virtual bool IsUndoEmpty();

    //!< りドゥ
    virtual bool Redo();

    virtual bool IsRedoEmpty();


    //!< スナップツールチップ表示
    static StdString GetSnapText(VIEW_COMMON::E_SNAP_TYPE eSnap, 
                                    double dLength,
                                    double dAngle,
                                    bool bEnableNone = false);

    //!< スナップツールチップ表示
    void SetSnapTooltip(VIEW_COMMON::E_SNAP_TYPE eSnap, 
                                    double dLength,
                                    double dAngle,
                                    bool bEnableNone = false);

    //!<マウス移動動作
    bool GetMousePointWithSpalateIntersection(POINT2D* pRet2d,
                    POINT2D* pMousePos2d,
                    MOUSE_MOVE_POS posMouse);
    

    //!<メニュー  キャンセル
    virtual void OnCancel(){;}

    //!<メニュー  終了
    virtual void OnEnd(){;}

	//!<マウスカーソル設定
	virtual bool OnSetCursor(){return false;}

    //直線生成(補助線用)
    std::shared_ptr<CDrawingLine> CreateLine(std::shared_ptr<CDrawingObject>* ppObj);

    //円生成(補助線用)
    std::shared_ptr<CDrawingCircle> CreateCircle(std::shared_ptr<CDrawingObject>* ppObj);

    //楕円生成(補助線用)
    std::shared_ptr<CDrawingEllipse> CreateEllipse(std::shared_ptr<CDrawingObject>* ppObj);

    //点生成(補助線用)
    std::shared_ptr<CDrawingPoint> CreatePoint(std::shared_ptr<CDrawingObject>* ppObj);


    bool CreateExtend(std::shared_ptr<CDrawingObject>* ppExtObj,
                               POINT2D pt2D,
                               int iObjId1);

    bool CreateExtendLine(std::shared_ptr<CDrawingObject>* ppExtObj1,
                               POINT2D pt2D,
                              const CDrawingLine* pLine);

    bool CreateExtendCircle(std::shared_ptr<CDrawingObject>* ppExtObj1,
                               POINT2D pt2D,
                              const CDrawingCircle* pCircle);

    bool CreateExtendEllipse(std::shared_ptr<CDrawingObject>* ppExtObj1,
                            POINT2D pt2D,
                            const CDrawingEllipse* pEllipse);

    virtual void ClearAction(bool bReleaseSelect = true);


    bool ExecuteString(double* pVal, const StdString& code);

    //移動用一時表示図形作成
    bool CreateTempMovingObjects(std::vector<std::unique_ptr<MAT2D>>* pLstOffstMatrix,
                                    std::vector< std::shared_ptr<CDrawingObject > >* pTempMoveObjects,
                                    CPartsDef*   pDef,
                                    int iCopyTimes,
                                    bool bCopyConnection = false);

    static bool CreateBox(std::vector<std::shared_ptr<CDrawingObject>>* pLst,
                            const RECT2D& rcBound);

    //マウスオーバー選択
    std::shared_ptr<CDrawingObject> SearchMouseOver(MOUSE_MOVE_POS posMouse);

    static bool UpdateAllNodePositionAndRelation(
        CPartsDef*   pDef,
        std::vector<NODE_CHANGE_DATA>* pList,
        std::set<int>* pLstUsedObject,
        CUndoAction* pUndo);

    //選択オブジェクトグループ化
    std::shared_ptr<CDrawingObject> GroupSelectObject(CPartsDef* pDef, bool bSetTmp);
	
	//行列を反映
	static bool ApplyMatrix(CPartsDef* pDef,
					std::shared_ptr<CDrawingObject> pObj, 
					MAT2D matApply, int iTimes, bool bCopy);

    //pDef->GetConnectionToSelectからGetConnectingTmpObjectListへの複写
    void CopyConnectionToSelectTmp(CPartsDef* pDef);


	virtual void InitTempObject() { ; }

	//位置設定
	bool SetPosition(CDrawingObject* pObj, POINT2D pt2D) const;

	//位置取得
	bool GetPosition(CDrawingObject* pObj, POINT2D* pPt2D) const;

protected:


    virtual bool _CheckMarker( POINT2D* pMarkerPos, 
                                POINT ptSel, 
                                SEL_ACTION eAction) const;

    virtual bool _CheckMarkerObject( POINT2D* pMarkerPos, 
        int iObjctId,
        POINT ptSel, 
        SEL_ACTION eAction) const;


    //!< オブジェクト削除
    void _DeleteObject();


    void _SetDraggingObject(POINT2D ptet2d);

    void _DrawTooltip(VIEW_COMMON::E_SNAP_TYPE eSnap, MOUSE_MOVE_POS posMouse);


protected:

    CDrawingView* m_pView;

    int           m_iClickCnt;

    bool          m_bInterrupt;

    //!< 表示モード
    VIEW_MODE     m_eViewMode;

    //!< 前回の表示モード
    // VIEW_DELETE, VIEW_GROUP, VIEW_PARTS, VIEW_REFERENCE は事前に
    // オブジェクトを選択しておく必要がある
    // 事前に選択していない場合は選択モードに移行する
    // 範囲選択ををした場合は、選択後元のモードを起動する
    VIEW_MODE     m_ePreViewMode;

    std::shared_ptr<CDrawingObject> m_pCurMouseOver;

    CDrawingObject* m_pCurMouseOverSecond;

    //マーカ表示フラグ
    bool m_bMark;

    // マウスオーバー動作設定
    long  m_lMouseOver;

    //!< ツールチップ表示位置
    POINT m_ptTooltip;

    //!< 選択オブジェクトリスト
    std::vector<std::weak_ptr<CDrawingObject>> m_lstSelectObj;
    
    //追加オブジェクト
    std::shared_ptr<CDrawingObject> m_psAddObj;

    //補助線
    std::shared_ptr<CDrawingObject> m_psAddtionalObj[4];

    //一時グループ化用
    std::shared_ptr<CDrawingGroup> m_psGroup;

    std::shared_ptr<CDrawingObject>     m_pMarkerPearentObject;


};
#endif // !defined(EA_2FB152AB_15EA_47cf_9AAD_3FE7099CD620__INCLUDED_)
