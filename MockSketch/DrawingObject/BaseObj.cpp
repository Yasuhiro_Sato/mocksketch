/**
 * @brief			BaseObjヘッダーファイル
 * @file		    BaseObj.h
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./BaseObj.h"
#include <boost/serialization/export.hpp> 
BOOST_CLASS_EXPORT(CBaseObj);

/*---------------------------------------------------*/
/*  class                                            */
/*---------------------------------------------------*/

//!< コンストラクタ
CBaseObj::CBaseObj()
{

}

    //!< デストラクタ
CBaseObj::~CBaseObj()
{
}

StdString CBaseObj::ToString()const
{
    StdStringStream strmRet;
    boost::archive::text_woarchive txtOut(strmRet);
    txtOut << *this;
    return strmRet.str();
}

bool CBaseObj::FromString(const StdString& strAny)
{
    try
    {
        StdStringStream strmIn;
        strmIn << strAny;
        boost::archive::text_wiarchive txtIn(strmIn);
        txtIn >> *this;
    }
    catch(...)
    {
         return false;
    }
    return true;
}


