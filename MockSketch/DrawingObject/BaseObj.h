/**
 * @brief			BaseObjヘッダーファイル
 * @file		    BaseObj.h
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

#ifndef BASE_OBJ_H
#define BASE_OBJ_H
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DrawingObject/RefBase.h"
#include "MockDef.h"


/*---------------------------------------------------*/
/*  class                                            */
/*---------------------------------------------------*/


/**
 * @class   CBaseObj
 * @brief   
 *
 *
 */
class CBaseObj:public CAsRefObj
{
public:
    //!< コンストラクタ
    CBaseObj();

    //!< デストラクタ
    virtual ~CBaseObj();

    virtual StdString ToString()const;

    virtual bool FromString(const StdString& strAny);

private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        ;
    }


};

BOOST_CLASS_VERSION(CBaseObj, 0);
#endif