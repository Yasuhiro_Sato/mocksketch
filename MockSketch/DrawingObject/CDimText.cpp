/**
* @brief        CDimTextヘッダーファイル
* @file	        CDimText.cpp
* @author           Yasuhiro Sato
* @date	        07-2-2009 20:03:09
* 
* @note	
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
* 
* @remarks	        
*			
*
* $
* $
* 
*/

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"

#include "./CDimText.h"
#include "Utility/CUtility.h"
#include "Utility/ExtText/CExtText.h"
#include "Utility/ExtText/TEXT_TYPE.H"
#include "Utility/ExtText/FONT_DATA.H"
#include "Utility/ExtText/TEXT_FRAME.H"
#include "Utility/ExtText/TEXT_MARGIN.H"
#include "Utility/ExtText/LINE_PROPERTY.H"
#include "System/CSystem.h"
#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/View/CDrawingView.h"
#include "View/ViewCommon.h"

CDimText::CDimText():
m_bAuto(true),
m_bTextTop(true),
m_bReverse(false),
m_iPrecision(3),
m_dFontHeight(0.0),
m_dGap(0.0),
m_pDim(NULL),
m_dAngle(0.0),
m_dTextHeightPoint(0.0),
m_dVText(0.0),
m_bCaleTextRect(false)
{


}

CDimText::CDimText(CDrawingDim* pDim):CDimText()
{
    m_pDim = pDim;
}


CDimText::~CDimText()
{
    ;
}

void CDimText::SetDefault()
{
    m_bAuto = true;
    m_bTextTop = true;
    m_bReverse = false;

    SetGap(DRAW_CONFIG->dDimTextSpace);
    SetFontHeight(DRAW_CONFIG->dDimTextHeight);

    m_strDim = _T("");
    m_strPrifix = _T("");
    m_strSuffix = _T("");
}

void CDimText::SetDim(CDrawingDim* pDim)
{
    m_pDim = pDim;
}

/*
 *  @brief   プロパティ変更(表示文字)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDimText::PropText       (CStdPropertyItem* pData, void* pObj)
{
   using namespace VIEW_COMMON;
    CDrawingDim* pDim = reinterpret_cast<CDrawingDim*>(pObj);
    try
    {
        StdString str;
        str  = pData->anyData.GetString();

        if (str == pDim->GetDimText()->GetText())
        {
            return true;
        }

        pDim->AddChgCnt();
        pDim->GetDimText()->SetAuto(false);
        pDim->GetDimText()->SetText(str.c_str());
        pDim->_UpdateText();
        pDim->GetPartsDef()->Redraw();
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}


/*
 *  @brief   プロパティ変更()
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDimText::PropPrefix     (CStdPropertyItem* pData, void* pObj)
{
   using namespace VIEW_COMMON;
    CDrawingDim* pDim = reinterpret_cast<CDrawingDim*>(pObj);
    try
    {
        StdString str;
        str  = pData->anyData.GetString();

        if (str == pDim->GetDimText()->GetPrifix())
        {
            return true;
        }

        pDim->AddChgCnt();
        pDim->GetDimText()->SetAuto(false);
        pDim->GetDimText()->SetPrifix(str.c_str());
        pDim->_UpdateText();
        pDim->GetPartsDef()->Redraw();
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}


/*
 *  @brief   プロパティ変更()
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDimText::PropSuffix     (CStdPropertyItem* pData, void* pObj)
{
   using namespace VIEW_COMMON;
    CDrawingDim* pDim = reinterpret_cast<CDrawingDim*>(pObj);
    try
    {
        StdString str;
        str  = pData->anyData.GetString();

        if (str == pDim->GetDimText()->GetSuffix())
        {
            return true;
        }

        pDim->AddChgCnt();
        pDim->GetDimText()->SetAuto(false);
        pDim->GetDimText()->SetSuffix(str.c_str());
        pDim->_UpdateText();
        pDim->GetPartsDef()->Redraw();
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/*
 *  @brief   プロパティ変更(テキスト自動設定)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDimText::PropAuto       (CStdPropertyItem* pData, void* pObj)
{
   using namespace VIEW_COMMON;
    CDrawingDim* pDim = reinterpret_cast<CDrawingDim*>(pObj);
    try
    {
        bool bAuto;
        bAuto  = pData->anyData.GetBool();

        if (pDim->GetDimText()->IsAuto() == bAuto)
        {
            return true;
        }
        pDim->GetDimText()->SetAuto(bAuto);
        pDim->AddChgCnt();
        pDim->_UpdateText();
        pDim->GetPartsDef()->Redraw();

    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/*
 *  @brief   プロパティ変更(テキスト位置)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDimText::PropTextTop    (CStdPropertyItem* pData, void* pObj)
{
   using namespace VIEW_COMMON;
    CDrawingDim* pDim = reinterpret_cast<CDrawingDim*>(pObj);
    try
    {
        bool bPos;
        bPos  = pData->anyData.GetBool();

        if (pDim->GetDimText()->GetTextTop() == bPos)
        {
            return true;
        }

        pDim->GetDimText()->SetTextTop(bPos);
        pDim->AddChgCnt();
        pDim->GetPartsDef()->Redraw();
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/*
 *  @brief   プロパティ変更(テキスト向き)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDimText::PropTextDir    (CStdPropertyItem* pData, void* pObj)
{
   using namespace VIEW_COMMON;
    CDrawingDim* pDim = reinterpret_cast<CDrawingDim*>(pObj);
    try
    {
        bool bDIr;
        bDIr  = pData->anyData.GetBool();

        if (pDim->GetDimText()->GetReverse() == bDIr)
        {
            return true;
        }

        pDim->GetDimText()->SetReverse(bDIr);
        pDim->AddChgCnt();
        pDim->GetPartsDef()->Redraw();
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}


/*
 *  @brief   プロパティ変更(小数点以下の桁数)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDimText::PropPresision   (CStdPropertyItem* pData, void* pObj)
{
   using namespace VIEW_COMMON;
    CDrawingDim* pDim = reinterpret_cast<CDrawingDim*>(pObj);
    try
    {
        int iPrecision;
        iPrecision  = pData->anyData.GetInt();

        if (pDim->GetDimText()->GetPrecision() == iPrecision)
        {
            return true;
        }

        pDim->GetDimText()->SetPrecision(iPrecision);
        pDim->_UpdateText();
        pDim->AddChgCnt();
        pDim->GetPartsDef()->Redraw();
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}


/*
 *  @brief   プロパティ変更(テキストサイズ)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDimText::PropFontHeight   (CStdPropertyItem* pData, void* pObj)
{
   using namespace VIEW_COMMON;
    CDrawingDim* pDim = reinterpret_cast<CDrawingDim*>(pObj);
    try
    {
        double dSize;
        dSize  = pData->anyData.GetDouble();

        if (pDim->GetDimText()->GetFontHeight() == dSize)
        {
            return true;
        }

        pDim->GetDimText()->SetFontHeight(dSize);
        pDim->AddChgCnt();
        pDim->GetPartsDef()->Redraw();
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/*
 *  @brief   プロパティ変更(文字間隔)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDimText::PropTextGap    (CStdPropertyItem* pData, void* pObj)
{
   using namespace VIEW_COMMON;
    CDrawingDim* pDim = reinterpret_cast<CDrawingDim*>(pObj);
    try
    {
        double dGap;
        dGap  = pData->anyData.GetDouble();

        if (pDim->GetDimText()->GetGap() == dGap)
        {
            return true;
        }

        pDim->GetDimText()->SetGap(dGap);
        pDim->AddChgCnt();
        pDim->GetPartsDef()->Redraw();
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}


TREE_GRID_ITEM*  CDimText::CreateStdPropertyTree( CStdPropertyTree* pProperty,
                                                  NAME_TREE<CStdPropertyItem>* pTree,
                                                  NAME_TREE_ITEM<CStdPropertyItem>* pNextItem)
{
    CStdPropertyItem* pItem;

    //------------
    // テキスト
    //------------
    CStdPropertyItemDef DefText(PROP_STR,              //表示タイプ
        GET_STR(STR_PRO_DIM_TEXT)   ,                   //表示名
        _T("Text"),                                     //変数名
        GET_STR(STR_PRO_INFO_DIM_TEXT),                 //表示説明
        true,                                           //編集可不可
        DISP_NONE,                                      //表示単位
        m_strDim                                    //初期値
        );

    pItem = new CStdPropertyItem(
        DefText, 
        PropText, 
        pProperty, 
        NULL,
        (void*)&m_strDim);
    
    pNextItem = pTree->AddNext(pNextItem, 
        pItem, 
        DefText.strDspName, 
        pItem->GetName());


    //------------
    // 接頭語
    //------------
    CStdPropertyItemDef DefPrifix(PROP_STR,            //表示タイプ
        GET_STR(STR_PRO_DIM_PRIFIX)   ,                //表示名
        _T("Prifix"),                                  //変数名
        GET_STR(STR_PRO_INFO_DIM_PRIFIX),              //表示説明
        true,                                          //編集可不可
        DISP_NONE,                                     //表示単位
        m_strPrifix                                    //初期値
        );

    pItem = new CStdPropertyItem(
        DefPrifix, 
        PropPrefix, 
        pProperty, 
        NULL,
        (void*)&m_strPrifix);
    
    pNextItem = pTree->AddNext(pNextItem, 
        pItem, 
        DefPrifix.strDspName, 
        pItem->GetName());

    //------------
    // 接尾語
    //------------
    CStdPropertyItemDef DefSuffix(PROP_STR,            //表示タイプ
        GET_STR(STR_PRO_DIM_SUFFIX)   ,                //表示名
        _T("Suffix"),                                  //変数名
        GET_STR(STR_PRO_INFO_DIM_SUFFIX),              //表示説明
        true,                                          //編集可不可
        DISP_NONE,                                     //表示単位
        m_strSuffix                                    //初期値
        );

    pItem = new CStdPropertyItem(
        DefSuffix, 
        PropSuffix, 
        pProperty, 
        NULL,
        (void*)&m_strSuffix);
    
    pNextItem = pTree->AddNext(pNextItem, 
        pItem, 
        DefSuffix.strDspName, 
        pItem->GetName());

    //--------------------
    // テキスト自動設定
    //--------------------
    CStdPropertyItemDef DefAuto(PROP_BOOL,              //表示タイプ
        GET_STR(STR_PRO_DIM_AUTO)   ,                   //表示名
        _T("Auto"),                                     //変数名
        GET_STR(STR_PRO_INFO_DIM_AUTO),                 //表示説明
        true,                                           //編集可不可
        DISP_NONE,                                      //表示単位
        m_bAuto                                         //初期値
        );

    pItem = new CStdPropertyItem(
        DefAuto, 
        PropAuto, 
        pProperty, 
        NULL,
        (void*)&m_bAuto);
    
    pNextItem = pTree->AddNext(pNextItem, 
        pItem, 
        DefAuto.strDspName, 
        pItem->GetName());

    //------------------------------
    // 寸法線に対するテキスト位置
    //------------------------------
    CStdPropertyItemDef DefTextPos(PROP_BOOL,         //表示タイプ
        GET_STR(STR_PRO_DIM_TEXT_POS)   ,             //表示名
        _T("TextTop"),                                //変数名
        GET_STR(STR_PRO_INFO_DIM_TEXT_POS),           //表示説明
        true,                                         //編集可不可
        DISP_NONE,                                    //表示単位
        m_bTextTop                       //初期値
        );

    pItem = new CStdPropertyItem(
        DefTextPos, 
        PropTextTop, 
        pProperty, 
        NULL,
        (void*)&m_bTextTop);
    
    pNextItem = pTree->AddNext(pNextItem, 
        pItem, 
        DefTextPos.strDspName, 
        pItem->GetName());

    //--------------------
    // テキスト方向
    //--------------------
    CStdPropertyItemDef DefTextDir(PROP_BOOL,         //表示タイプ
        GET_STR(STR_PRO_DIM_TEXT_DIR)   ,             //表示名
        _T("TextDir"),                                //変数名
        GET_STR(STR_PRO_INFO_DIM_TEXT_DIR),           //表示説明
        true,                                         //編集可不可
        DISP_NONE,                                    //表示単位
        m_bReverse                                    //初期値
        );

    pItem = new CStdPropertyItem(
        DefTextDir, 
        PropTextDir, 
        pProperty, 
        NULL,
        (void*)&m_bReverse);
    
    pNextItem = pTree->AddNext(pNextItem, 
        pItem, 
        DefTextDir.strDspName, 
        pItem->GetName());

    //--------------------
    // 小数点以下の桁数
    //--------------------
    CStdPropertyItemDef DefPrecision(PROP_INT_RANGE, //表示タイプ
        GET_STR(STR_PRO_DIM_PRECISION)   ,            //表示名
        _T("Pricision"),                              //変数名
        GET_STR(STR_PRO_INFO_DIM_PRECISION),          //表示説明
        true,                                         //編集可不可
        DISP_NONE,                                    //表示単位
        m_iPrecision,                                 //初期値
        0,
        15
        );

    pItem = new CStdPropertyItem(
        DefPrecision, 
        PropPresision, 
        pProperty, 
        NULL,
        (void*)&m_iPrecision);
    
    pNextItem = pTree->AddNext(pNextItem, 
        pItem, 
        DefPrecision.strDspName, 
        pItem->GetName());

    //--------------------
    // フォントサイズ
    //--------------------
    CStdPropertyItemDef DefTextHeight(PROP_DOUBLE,    //表示タイプ
        GET_STR(STR_PRO_DIM_TEXT_SIZE)   ,            //表示名
        _T("FontHeight"),                             //変数名
        GET_STR(STR_PRO_INFO_DIM_TEXT_SIZE),          //表示説明
        true,                                         //編集可不可
        DISP_UNIT,                                    //表示単位
        m_dFontHeight                                  //初期値
        );

    pItem = new CStdPropertyItem(
        DefTextHeight, 
        PropFontHeight, 
        pProperty, 
        NULL,
        (void*)&m_dFontHeight);
    
    pNextItem = pTree->AddNext(pNextItem, 
        pItem, 
        DefTextHeight.strDspName, 
        pItem->GetName());

    //--------------------
    // テキストと寸法線の間隔
    //--------------------
    CStdPropertyItemDef DefTextGap(PROP_DOUBLE,       //表示タイプ
        GET_STR(STR_PRO_DIM_TEXT_GAP)   ,             //表示名
        _T("TextGap"),                                //変数名
        GET_STR(STR_PRO_INFO_DIM_TEXT_GAP),           //表示説明
        true,                                         //編集可不可
        DISP_UNIT,                                    //表示単位
        m_dGap                                        //初期値
        );

    pItem = new CStdPropertyItem(
        DefTextGap, 
        PropTextGap, 
        pProperty, 
        NULL,
        (void*)&m_dGap);
    
    pNextItem = pTree->AddNext(pNextItem, 
        pItem, 
        DefTextGap.strDspName, 
        pItem->GetName());

    return pNextItem;
}

void CDimText::SetAngle(double dAngle)
{
    m_dAngle = dAngle;
}

void CDimText::SetPos(POINT2D ptPos)
{
    m_ptCenter = ptPos;
}

void CDimText::SetText(LPCTSTR str)
{
    m_strDim = str;
}

void CDimText::SetPrifix(LPCTSTR str)
{
    m_strPrifix = str;
}

void CDimText::SetSuffix(LPCTSTR str)
{
    m_strSuffix = str;
}

void CDimText::SetAuto(bool bAuto)
{
    m_bAuto = bAuto;
}

void CDimText::SetTextTop(bool bTop)
{
    m_bTextTop = bTop;
}

void CDimText::FlipTop()
{
    m_bTextTop = (!m_bTextTop);
}


void CDimText::SetReverse(bool bRev)
{
    m_bReverse = bRev;
}

void CDimText::SetPrecision(int iPrecision)
{
    m_iPrecision = iPrecision;
}

void CDimText::SetFontHeight(double dHeight)
{
    //フォントの高さ指定にはポイントを使用する
    m_dFontHeight = dHeight;
    double dHeightPoint = DRAW_CONFIG->pDimUnit->ToBase(m_dFontHeight) * 1000.0;

    //1point = 1inch / 72 = 0.352777...mm

    m_dTextHeightPoint = dHeightPoint * 72.0 /25.4;
}

void CDimText::SetGap(double dGap)
{
    m_dGap = dGap;
    m_dVText = DRAW_CONFIG->DimSettingToDispUnit(dGap);
}


void CDimText::CalcTextRect(const CDrawingView* pView, const CExtText& extText) const
{
   if (m_bCaleTextRect)
   {
   //    return;
   }
    double dDspScl = pView->GetViewScale();

    //!< 表示領域取得
    SIZE size;
    HDC hDc = pView->GetViewDc();
    POINT2D pTmp;

    extText.GetDispSize(&size, hDc, pView, 1.0);
    POINT pt1;        
    POINT pt2;     
    POINT2D pt2D1;        
    POINT2D pt2D2;     
    pt2.x = 0;
    pt2.y = 0;
    pt1.x = size.cx;
    pt1.y = size.cy;

    pView->ConvScr2World(&pt2D1, pt1, 0);
    pView->ConvScr2World(&pt2D2, pt2, 0);
    m_ptTextSize.dX = (pt2D1.dX - pt2D2.dX) * dDspScl;
    m_ptTextSize.dY = (pt2D2.dY - pt2D1.dY ) * dDspScl;
    m_bCaleTextRect = true; 
}

POINT2D CDimText::_CalcOffset() const
{
    POINT2D ptNorm;
    double dNormAngle   = CUtil::NormAngle(m_dAngle + 90.0);
    ptNorm.dX = cos((dNormAngle) * DEG2RAD);
    ptNorm.dY = sin((dNormAngle) * DEG2RAD);
    POINT2D ptOffet = ptNorm * m_dVText;
    return ptOffet;
}

bool CDimText::Draw(CDrawingView* pView,
        COLORREF crPen,
        int iId,
        int iLayerId,
        CDrawingObject::E_MOUSE_OVER_TYPE eMouseOver)
{
    double dDspScl = pView->GetViewScale();
    CExtText   extText;

    
    double dAngle = m_dAngle;

    extText.SetText(GetDimText().c_str());
    extText.SetHeight(-1,-1, m_dTextHeightPoint);
    extText.SetColor(-1,-1, crPen);
    CalcTextRect(pView, extText);


    double  dDir = 1.0;

    bool bDirTop = true;
    if (!m_bTextTop)
    {
        dDir = -1.0; 
        bDirTop = false;
    }

    POINT pt;

    if (m_bReverse)
    {
        dAngle   = CUtil::NormAngle(m_dAngle + 180.0);
        bDirTop = !bDirTop;
    }

    if (bDirTop)
    {
        extText.SetDatum(BOTTTOM_MID);

    }
    else
    {
        extText.SetDatum(TOP_MID);
    }

    POINT2D ptPos = m_ptCenter + dDir * _CalcOffset();

    extText.SetAngle(dAngle);
    pView->ConvWorld2Scr(&pt, ptPos, iLayerId);

#if 0
    DrawTextArea(pView, crPen, iId, iLayerId);
#endif
    TEXT_PARAM param;
    param.dScl      = dDspScl;
    param.bIndex    = false;         //インデックス使用有無
    param.iId       = iId;            //インデックス
    //-------------------------
    //param.ptTL     = 0;
    //-------------------------
    param.dOffsetAngle  = 0.0;      //追加角度
    param.bForceColor        = false;    //強制色設定の有無
    param.cr            = 0;        //設定色
    param.bEmphasis         = false ;   //太字
    param.bFrameColor   = false;    //フレーム強制色設定の有無
    param.crFrame       = 0;        //フレーム強制設定色

    pView->Text(&extText, pt, &param, iLayerId);

    return true;
}

void CDimText::DrawTextArea(CDrawingView* pView,
                            COLORREF crPen,
                            int iId,
                            int iLayerId) const
{
    //デバッグ用
    std::vector<POINT2D> rc;
    _GetTextRect(&rc, false);


    LINE2D lTx1(rc[0], rc[1]);
    LINE2D lTx2(rc[1], rc[2]);
    LINE2D lTx3(rc[2], rc[3]);
    LINE2D lTx4(rc[3], rc[0]);

   m_pDim->Line(pView, iLayerId, &lTx1, PS_SOLID, 1, crPen, iId);
   m_pDim->Line(pView, iLayerId, &lTx2, PS_SOLID, 1, crPen, iId);
   m_pDim->Line(pView, iLayerId, &lTx3, PS_SOLID, 1, crPen, iId);
   m_pDim->Line(pView, iLayerId, &lTx4, PS_SOLID, 1, crPen, iId);


    std::vector<POINT2D> rc1;
    _GetTextRect(&rc1, true);

    LINE2D lTx1R(rc1[0], rc1[1]);
    LINE2D lTx2R(rc1[1], rc1[2]);
    LINE2D lTx3R(rc1[2], rc1[3]);
    LINE2D lTx4R(rc1[3], rc1[0]);

   m_pDim->Line(pView, iLayerId, &lTx1R, PS_SOLID, 1, crPen, iId);
   m_pDim->Line(pView, iLayerId, &lTx2R, PS_SOLID, 1, crPen, iId);
   m_pDim->Line(pView, iLayerId, &lTx3R, PS_SOLID, 1, crPen, iId);
   m_pDim->Line(pView, iLayerId, &lTx4R, PS_SOLID, 1, crPen, iId);




}

StdString CDimText::GetDimText() const
{
    StdStringStream strmDim;
    strmDim << m_strPrifix  
            << m_strDim
            << m_strSuffix;
    return strmDim.str();
}

//文字描画
/*
void CDimText::_DrawText(CDrawingView* pView, 
                             COLORREF crPen,
                             int iId,
                             POINT2D ptCenter,
                             double dAngle)const
{ 
    CExtText   extText;
    StdString strDim = GetDimText();
    extText.SetText(strDim.c_str());
    extText.SetHeight(-1,-1, m_dTextHeightPoint);
    extText.SetAlign(CExtText::BOTTTOM_MID);
    extText.SetAngle(dAngle);
    extText.SetColor(-1, -1, crPen);
    POINT pt;

    double dDspScl = pView->GetViewScale();
    pView->ConvWorld2Scr(&pt, ptCenter, m_iLayerId);
    pView->Text(&extText, pt, 
                        0,
                        dDspScl,
                        false,  iId);
}
*/

/*
 *  @brief   値設定
 *  @param   [in] dVal    設定値
 *  @param   [in] bAngle  true:角度, false:長さ
 *  @retval  なし
 *  @note
 */
void CDimText::SetVal(double dVal, bool bAngle)
{
    if (!m_bAuto)
    {
        return;
    }

    m_strDim =   GetValDimText(dVal, bAngle);
}

StdString CDimText::GetValDimText(double dVal, bool bAngle) const
{
    return CDrawingObject::GetValText(dVal, bAngle, m_iPrecision);
}

void CDimText::_GetTextRect(std::vector<POINT2D>* pRect, bool bTop) const
{
    pRect->resize(4);

    double dDir = (bTop ? 1.0: -1.0);

    double dX = m_ptTextSize.dX / 2.0;
    double dY = m_ptTextSize.dY * dDir;

    POINT2D ptOffset;
    POINT2D ptCenter;

    ptOffset = _CalcOffset();
    ptCenter = m_ptCenter + dDir * ptOffset;

    pRect->at(0).dX = ptCenter.dX + dX;
    pRect->at(0).dY = ptCenter.dY ;

    pRect->at(1).dX = ptCenter.dX + dX;
    pRect->at(1).dY = ptCenter.dY + dY;

    pRect->at(2).dX = ptCenter.dX - dX; 
    pRect->at(2).dY = ptCenter.dY + dY;

    pRect->at(3).dX = ptCenter.dX - dX;
    pRect->at(3).dY = ptCenter.dY ;

    pRect->at(0).Rotate(ptCenter, m_dAngle);
    pRect->at(1).Rotate(ptCenter, m_dAngle);
    pRect->at(2).Rotate(ptCenter, m_dAngle);
    pRect->at(3).Rotate(ptCenter, m_dAngle);

}


/*
 *  @brief   テキスト上位置確認
 *  @param   [in] pt     変更アイテム
 *  @param   [in] bRev   反対位置
 *  @retval  true テキスト上
 *  @note
 */
bool CDimText::IsOnText(const POINT2D& pt , bool bTop) const
{
    std::vector<POINT2D> rc;
    _GetTextRect(&rc, bTop);

    if(CUtil::IsInnerTriangle2(pt, rc[3],rc[2],rc[0]))
    {
        return true;
    }

    if(CUtil::IsInnerTriangle2(pt, rc[2],rc[1],rc[0]))
    {
        return true;
    }
    return false;
}


POINT2D& CDimText::GetTextSize()const
{
    return m_ptTextSize;
}

POINT2D& CDimText::GetTextCenter()const
{
    return m_ptPos;
}

POINT2D CDimText::GetTextStart()const
{

    double dX = m_ptTextSize.dX / 2.0;
    POINT2D ptText;
    ptText.dX = m_ptPos.dX - dX;
    ptText.dY = m_ptPos.dY;
    ptText.Rotate(m_ptPos, m_dAngle);

    return ptText;
}

POINT2D CDimText::GetTextEnd()const
{
    double dX = m_ptTextSize.dX / 2.0;
    POINT2D ptText;
    ptText.dX = m_ptPos.dX + dX;
    ptText.dY = m_ptPos.dY;
    ptText.Rotate(m_ptPos, m_dAngle);

    return ptText;
}
