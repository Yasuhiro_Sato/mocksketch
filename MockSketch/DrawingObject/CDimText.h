/**
* @brief        CDimTextヘッダーファイル
* @file	        CDimText.h
* @author           Yasuhiro Sato
* @date	        07-2-2009 20:03:09
* 
* @note	
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
* 
* @remarks	        
*			
*
* $
* $
* 
*/
#ifndef __DIM_TEXT_H_
#define __DIM_TEXT_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CDrawingDim.h"


class CDimText
{
    struct POSITION
    {
        POINT2D ptPos;
        POINT2D ptTextSize;
        bool    bCaleTextRect;
    };

public:
    CDimText();

    CDimText(CDrawingDim* pDim);

    virtual ~CDimText();


    TREE_GRID_ITEM*  CreateStdPropertyTree(CStdPropertyTree* pProperty,
                                           NAME_TREE<CStdPropertyItem>* pTree,
                                           NAME_TREE_ITEM<CStdPropertyItem>* pNextItem);
    void SetDefault();

    void SetDim(CDrawingDim* pDim);

    void SetVal(double dVal, bool bAngle);

    StdString GetValDimText(double dVal, bool bAngle) const;

    void DrawTextArea(CDrawingView* pView,
                       
                      COLORREF crPen,
                      int iId,
                      int iLayerId) const;

    bool IsOnText(const POINT2D& pt, bool bRev = false)const;

    POINT2D& GetTextSize()const;

    POINT2D& GetTextCenter()const;

    POINT2D GetTextStart()const;

    POINT2D GetTextEnd()const;

    void SetText(LPCTSTR str);
    LPCTSTR GetText(){return m_strDim.c_str();}

    void SetPrifix(LPCTSTR str);
    LPCTSTR GetPrifix(){return m_strPrifix.c_str();}

    void SetSuffix(LPCTSTR str);
    LPCTSTR GetSuffix(){return m_strSuffix.c_str();}

    void SetAuto(bool bAuto);
    bool IsAuto(){return m_bAuto;}

    void SetTextTop(bool bTop);
    bool GetTextTop(){return m_bTextTop;}
    void FlipTop();

    void SetReverse(bool bRev);
    bool GetReverse(){return m_bReverse;}

    void SetPrecision(int iPrecision);
    int GetPrecision(){return m_iPrecision;}

    void SetFontHeight(double dHeight);
    double GetFontHeight(){return m_dFontHeight;}
    double GetFontHeightPoint(){return m_dTextHeightPoint;}

    void SetGap(double dGap);
    double GetGap(){return m_dGap;}
    double GetTextGap(){return m_dVText;}
    //------------
    void SetAngle(double dAngle);

    void SetPos(POINT2D ptPos);


    bool Draw(CDrawingView* pView,
                COLORREF crPen,
                int iId,
                int iLayerId,
                CDrawingObject::E_MOUSE_OVER_TYPE eMouseOver);


    StdString GetDimText() const;

    void CalcTextRect(const CDrawingView* pView, const CExtText& extText) const;

    static bool PropText       (CStdPropertyItem* pData, void* pObj);
    static bool PropPrefix     (CStdPropertyItem* pData, void* pObj);
    static bool PropSuffix     (CStdPropertyItem* pData, void* pObj);
    static bool PropAuto       (CStdPropertyItem* pData, void* pObj);
    static bool PropTextTop    (CStdPropertyItem* pData, void* pObj);
    static bool PropTextDir    (CStdPropertyItem* pData, void* pObj);
    static bool PropPresision  (CStdPropertyItem* pData, void* pObj);
    static bool PropFontHeight (CStdPropertyItem* pData, void* pObj);
    static bool PropTextGap    (CStdPropertyItem* pData, void* pObj);



protected:
    POINT2D _CalcOffset() const;


    void _GetTextRect(std::vector<POINT2D>* rcText, bool bTop) const;

protected:
friend CDimText;

    // 要保存データ
    StdString m_strDim;    //寸法値
   
    StdString m_strPrifix; //接頭語

    StdString m_strSuffix; //接尾語

    bool    m_bAuto;       //自動文字変更

    bool    m_bTextTop;    //文字が寸法線の上か下（左か右）
            
    bool    m_bReverse;    //文字列上下反転                  

    int     m_iPrecision;  //小数点以下の桁数

    double  m_dFontHeight; //文字高さ(設定単位系)

    double  m_dGap;        //寸法線と文字の間隔(設定単位系)




    // 要保存不要
    CDrawingDim*    m_pDim;         //寸法線

    POINT2D         m_ptCenter;     //寸法線上の中心位置

    mutable POINT2D m_ptPos;        //文字下限中心位置

    double          m_dAngle;       //角度

    double     m_dTextHeightPoint;  //m_dTextSizeより生成(単位：ポイント)

    double     m_dVText;            //m_dGap より生成 (表示単位系)

    mutable POINT2D m_ptTextSize;   //テキスト表示範囲

    mutable bool    m_bCaleTextRect; //テキスト表示範囲再計算


private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
            SERIALIZATION_BOTH("Auto"         , m_bAuto);
            SERIALIZATION_BOTH("TopDirection" , m_bTextTop);
            SERIALIZATION_BOTH("Reverse"      , m_bReverse);
            SERIALIZATION_BOTH("Precision"    , m_iPrecision);
            SERIALIZATION_BOTH("FontHeight"   , m_dFontHeight);
            SERIALIZATION_BOTH("TextGap"      , m_dGap);

            SERIALIZATION_BOTH("Text"         , m_strDim);
            SERIALIZATION_BOTH("Prifix"       , m_strPrifix);
            SERIALIZATION_BOTH("Sufix"        , m_strSuffix);
        
            if(Archive::is_loading::value)
            {
                SetFontHeight(m_dFontHeight);
                SetGap(m_dGap);
            }
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }
};
BOOST_CLASS_VERSION(CDimText, 0);


#endif