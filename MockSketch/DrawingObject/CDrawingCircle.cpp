/**
 * @brief			CDrawingCircle実装ファイル
 * @file			CDrawingCircle.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CDrawingPoint.h"
#include "./CDrawingCircle.h"
#include "./CDrawingLine.h"
#include "./CDrawingSpline.h"
#include "./CDrawingEllipse.h"
#include "./CDrawingCompositionLine.h"
#include "./CDrawingDim.h"
#include "./CDrawingParts.h"
#include "./CDrawingNode.h"
#include "./CNodeMarker.h"

#include "DefinitionObject/View/CDrawingView.h"
#include "DefinitionObject/CPartsDef.h"
#include "System/CSystem.h"
#include "Utility/CUtility.h"
#include "View/CUndoAction.h"
#include <math.h>
#include <boost/serialization/export.hpp> 
BOOST_CLASS_EXPORT(CDrawingCircle);

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 * コンストラクタ
 */

CDrawingCircle::CDrawingCircle():
CDrawingEllipse()
{
    //STD_ASSERT (_T("これは普通使わない") == 0);

    m_eType  = DT_CIRCLE;
    m_iPropFeatures = P_BASE | P_LINE; 
}

/**
 * コンストラクタ
 */
CDrawingCircle::CDrawingCircle(int nID):
CDrawingEllipse( nID )
{
    m_eType  = DT_CIRCLE;
    m_iPropFeatures = P_BASE | P_LINE; 
}

/**
 * コピーコンストラクタ
 */
CDrawingCircle::CDrawingCircle(const CDrawingCircle& Obj):
CDrawingEllipse( Obj)
{
    m_eType  = DT_CIRCLE;
    m_Circle    = Obj.m_Circle;
    m_iPropFeatures = Obj.m_iPropFeatures; 
}

/**
 * デストラクタ
 */

CDrawingCircle::~CDrawingCircle()
{

}


/**
 *  @brief  回転.
 *  @param  [in]    pPt2D    回転中心
 *  @param  [in]    dAngle   回転角
 *  @retval     なし
 *  @note
 */
void CDrawingCircle::Rotate(const POINT2D& pt2D, double dAngle)
{
    AddChgCnt();
    if (m_eType == DT_ELLIPSE)
    {
        m_Ellipse.Rotate(pt2D, dAngle);
    }
    else
    {
        m_Circle.Rotate(pt2D, dAngle);
    }
}

/**
 *  @brief  相対移動.
 *  @param  [in]    pPt2D   移動量
 *  @retval         なし
 *  @note
 */
void CDrawingCircle::Move(const POINT2D& pt2D) 
{
    if (m_eType == DT_ELLIPSE)
    {
        return CDrawingEllipse::Move(pt2D);
    }
    else
    {
        AddChgCnt();
        m_Circle.Move(pt2D);
    }
}

/**
 *  @brief  鏡像.
 *  @param  [in]    line   対称軸
 *  @retval         なし
 *  @note
 */
void CDrawingCircle::Mirror(const LINE2D& line)
{
    if (m_eType == DT_ELLIPSE)
    {
        return CDrawingEllipse::Mirror(line);
    }
    else
    {
        AddChgCnt();
        m_Circle.Mirror(line);
    }
}

/**
 *  @brief  倍率.
 *  @param  [in]    pPt2D   拡大中心
 *  @param  [in]    dSclX   X倍率
 *  @param  [in]    dSclY   Y倍率
 *  @retval         なし
 *  @note
 */
void CDrawingCircle::Scl2(const POINT2D& pt2D, double dXScl, double dYScl, ELLIPSE2D** ppEllipse)
{
    AddChgCnt();
    m_Circle.Scl(pt2D, dXScl, dYScl, ppEllipse);
}

void CDrawingCircle::Scl(const POINT2D& pt2D, double dXScl, double dYScl)
{
    if (m_eType == DT_ELLIPSE)
    {
        return CDrawingEllipse::Scl(pt2D, dXScl, dYScl);
    }
    else
    {
        AddChgCnt();
        ELLIPSE2D* pEllipse = NULL;
        m_Circle.Scl(pt2D, dXScl, dYScl, &pEllipse);
        if (pEllipse)
        {
            m_Ellipse = *pEllipse;
            m_eType = DT_ELLIPSE;
            delete pEllipse;
        }
    }
}


/**
 *  @brief  行列.
 *  @param  [in]    mat2D   適用する行列
 *  @retval         なし
 *  @note
 */
void CDrawingCircle::Matrix2(const MAT2D& mat2D, ELLIPSE2D** ppEllipse)
{
    AddChgCnt();
    m_Circle.Matrix(mat2D, ppEllipse);
}

void CDrawingCircle::Matrix(const MAT2D& mat2D)
{
    if (m_eType == DT_ELLIPSE)
    {
        return CDrawingEllipse::Matrix(mat2D);
    }
    else
    {
        AddChgCnt();
        ELLIPSE2D* pEllipse = NULL;
        m_Circle.Matrix(mat2D, &pEllipse);

        if (pEllipse != NULL)
        {
            m_Ellipse = *pEllipse;
            m_eType = DT_ELLIPSE;
            delete pEllipse;
        }
    }
}

bool CDrawingCircle::IsEllipse() const
{
    return (m_eType == DT_ELLIPSE);
}

/**
 *  @brief  交点計算.
 *  @param  [out]   pList   交点のリスト
 *  @param  [in]    pObj    交差するオブジェクト
 *  @retval         なし
 *  @note
 */
void CDrawingCircle::CalcIntersection ( std::vector<POINT2D>* pList,   
                                const CDrawingObject* pObj,
                                bool bOnline,
                                double dMin) const
{
    if (m_eType == DT_ELLIPSE)
    {
        CDrawingEllipse::CalcIntersection(pList, pObj, bOnline, dMin);
        return;
    }

    STD_ASSERT(pList != NULL);

    POINT2D ptTmp;
    const CIRCLE2D* pCircle = NULL;
    CIRCLE2D* pMatCircle = NULL;
    ELLIPSE2D*  pEllipse = NULL;

    pList->clear();

    //レイヤーの確認
    if (!IsSameScale(pObj))
    {
        return;
    }

    const MAT2D* pMat =  GetParentMatrix();
    const MAT2D* pMat2 =  pObj->GetParentMatrix();
    if (pMat == NULL)
    {
        pCircle = &m_Circle;
    }
    else
    {
        pMatCircle = new CIRCLE2D(m_Circle);
        pMatCircle->Matrix(*pMat, &pEllipse);
        pCircle = pMatCircle;
    }

    if (pEllipse)
    {
        CDrawingEllipse::CalcIntersectionStatic(pEllipse, pList, pObj, bOnline, dMin);
        delete pEllipse;

        if(pMatCircle)
        {
            delete pMatCircle;
        }
        return;
    }

    try
    {
        switch (pObj->GetType())
        {
        case DT_POINT:
            {
            //近接点を算出
            const CDrawingPoint* ptObj = dynamic_cast<const CDrawingPoint*>(pObj);

            STD_ASSERT(ptObj != NULL);

            const POINT2D* pt2D = ptObj->GetPointInstance();
            if (pMat2 != NULL)
            {
                POINT2D ptMat(*pt2D);
                ptMat.Matrix(*pMat2);
                ptTmp = pCircle->NearPoint(ptMat);
            }
            else
            {
                ptTmp = pCircle->NearPoint(*pt2D);
            }

            pList->push_back(ptTmp);
            }
            break;

        case DT_NODE:
            {
            //近接点を算出
            const CDrawingNode* ptObj = dynamic_cast<const CDrawingNode*>(pObj);

            STD_ASSERT(ptObj != NULL);

            const POINT2D* pt2D = ptObj->GetPointInstance();
            if (pMat2 != NULL)
            {
                POINT2D ptMat(*pt2D);
                ptMat.Matrix(*pMat2);
                ptTmp = pCircle->NearPoint(ptMat);
            }
            else
            {
                ptTmp = pCircle->NearPoint(*pt2D);
            }


            pList->push_back(ptTmp);
            }
            break;

        case DT_LINE:
            {

            //交点を算出
            const CDrawingLine* pLine = dynamic_cast<const CDrawingLine*>(pObj);

            STD_ASSERT(pLine != NULL);
            const LINE2D* pLine2D = pLine->GetLineInstance();

            if (pMat2 != NULL)
            {
                LINE2D line2(*pLine2D);
                line2.Matrix(*pMat2);
                pCircle->Intersect( line2, pList, bOnline, dMin);
            }
            else
            {
                pCircle->Intersect( *pLine2D, pList, bOnline, dMin);
            }

            }
            break;

        case DT_CIRCLE:
            {
            //交点を算出
            const CDrawingCircle* pCircle2 = dynamic_cast<const CDrawingCircle*>(pObj);
            STD_ASSERT(pCircle2 != NULL);

            const CIRCLE2D* pCircle2Instance = pCircle2->GetCircleInstance();

            if (pMat2 != NULL)
            {
                CIRCLE2D circle(*pCircle2Instance);
                circle.Matrix(*pMat2, &pEllipse);

                if (!pEllipse)
                {
                    pCircle->Intersect( circle, pList, bOnline, dMin);
                }
                else
                {
                    pEllipse->Intersect( circle, pList, bOnline, dMin);
                    delete pEllipse;
                }
            }
            else
            {
                pCircle->Intersect( *pCircle2Instance, pList, bOnline, dMin);
            }

            
            }
            break;

        case DT_SPLINE:
            {
            //交点を算出
            const CDrawingSpline* pSpline = dynamic_cast<const CDrawingSpline*>(pObj);
            STD_ASSERT(pSpline != NULL);

            if (pMat2 != NULL)
            {
                SPLINE2D spline(*pSpline->GetSplineInstance());
                
                spline.Matrix(*pMat2);
                spline.Intersect( *pCircle, pList, bOnline, dMin);

            }
            else
            {
                pSpline->GetSplineInstance()->Intersect( *pCircle, pList, bOnline, dMin);
            }


            }
            break;

        case DT_ELLIPSE:
            {
            //交点を算出
            const CDrawingEllipse* pEllipse = dynamic_cast<const CDrawingEllipse*>(pObj);
            STD_ASSERT(pEllipse != NULL);

            if (pMat2 != NULL)
            {
                ELLIPSE2D ellipse(*pEllipse->GetEllipseInstance());
                
                ellipse.Matrix(*pMat2);
                ellipse.Intersect( *pCircle, pList, bOnline, dMin);
            }
            else
            {
                pEllipse->GetEllipseInstance()->Intersect( *pCircle, pList, bOnline, dMin);
            }

            }
            break;

        case DT_COMPOSITION_LINE:
        {
            //交点を算出
            const CDrawingCompositionLine* pCom = dynamic_cast<const CDrawingCompositionLine*>(pObj);
            STD_ASSERT(pCom != NULL);
            if (pMat2 == NULL)
            {
                pCom->CalcIntersection(pList, this, bOnline, dMin);
            }
            else
            {
                CDrawingCompositionLine com(*pCom);
                com.Matrix(*pMat2);
                com.CalcIntersection(pList, this, bOnline, dMin);
            }
        }
        break;


        default:
            break;
        }
    }
    catch(...)
    {

    }

    if (pMatCircle)
    {
        delete pMatCircle;
    }
}

/**
 *  @brief  描画.
 *  @param  [in]  pView       表示View
 *  @param  [in]  bMouseOver  true オブジェクト上にマウスあり
 *  @retval         なし
 *  @note
 */
void CDrawingCircle::Draw(CDrawingView* pView, 
                          const std::shared_ptr<CDrawingObject> pParentObject,
                         E_MOUSE_OVER_TYPE eMouseOver,
                        int iLayerId)
{
    if (m_eType == DT_ELLIPSE)
    {
        CDrawingEllipse::Draw(pView, pParentObject, eMouseOver, iLayerId);
        return;
    }

    bool bIgnoreSelect = false;
    if (eMouseOver == E_MO_IGNORE_SELECT)
    {
        bIgnoreSelect = true;
    }

    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }

    COLORREF crObj;
    int      iId;
    crObj = GetDrawColor(pView, iLayerId, pParentObject.get(), &iId, bIgnoreSelect);

    int iWidth = m_iWidth;

    if (eMouseOver == E_MO_SELECTABLE)
    {
        pView->SetLineAlternateMode(true);
        iWidth += 2;
    }
    else if (eMouseOver == E_MO_EMPHASIS)
    {
        crObj = DRAW_CONFIG->crEmphasis;
    }
	else if (eMouseOver == E_MO_CONNECTABLE)
	{
		crObj = DRAW_CONFIG->crConnectable;
	}


    //------------------
    //  塗りつぶし
    //------------------
    if (m_Fill.eFillType != FIT_NONE)
    {
        COLORREF crFill;
        pView->BeginPath();

        CPartsDef* pDef = pView->GetPartsDef();
        CLayer* pLayer = pDef->GetLayer(iLayerId);

        if (pLayer->bUseLayerColor)
        {
            crFill = pLayer->crLayer;
        }
        else
        {
            crFill = m_Fill.cr1;
        }

        pView->SetPen(m_iLineType, iWidth, crObj, m_nId);

        POINT   ptViewStart;
        pView->ConvWorld2Scr(&ptViewStart, GetStartSide(), iLayerId);

        pView->MoveTo(ptViewStart);

        MAT2D matDraw;
        if (pParentObject)
        {
            const MAT2D* pMatParent = pParentObject->GetDrawingMatrix();
            if (pMatParent)
            {
                matDraw = (*pMatParent);
            }
        }

        if (m_pMatOffset)
        {
            matDraw = matDraw * (*m_pMatOffset);
        }

        CIRCLE2D tmpCircle(*this->GetCircleInstance());
        ELLIPSE2D* pEllipse = NULL;

        tmpCircle.Matrix(matDraw, &pEllipse);
        if (pEllipse != NULL)
        {
            //楕円を描画
            std::vector<POINT2D> lstPoint;
            RECT2D viewRect = pView->GetViewArea(iLayerId);
            double dDotLength = pView->DotToLength(1);
            pEllipse->DivideLine(&lstPoint, viewRect, dDotLength);
            LineMulti(pView, iLayerId, &lstPoint, true);
            delete pEllipse;
        }
        else
        {
            RECT   rcEllipse;
            POINT  PtStart;
            POINT  PtEnd;

            GetCircleRect(&rcEllipse, iLayerId, &PtStart, &PtEnd, pView, tmpCircle);
            {
                pView->ArcTo(rcEllipse, PtStart, PtEnd, tmpCircle.IsClockwise());
            }
        }

        pView->CloseFigure();
        pView->EndPath();

        if (m_Fill.eFillType == FIT_SOLID)
        {
            pView->FillPath(crFill, m_nId);
        }
        else if (m_Fill.eFillType == FIT_HATCHING)
        {
            pView->HatchingPath(m_Fill.cr1, crObj, m_nId);
        }
    }
    //------------------


    Circle( pView, iLayerId, &m_Circle,pParentObject.get(), m_iLineType, iWidth, crObj, iId);

    if (eMouseOver == E_MO_SELECTABLE)
    {
        pView->SetLineAlternateMode(false);
    }

}

/**
 *  @brief  描画削除.
 *  @param  [in]    pView   表示View
 *  @retval         なし
 *  @note
 */
void CDrawingCircle::DeleteView(CDrawingView* pView, 
                                 const std::shared_ptr<CDrawingObject> pParentObject,
                                 int iLayerId)
{
    if (m_eType == DT_ELLIPSE)
    {
        CDrawingEllipse::DeleteView(pView, pParentObject, iLayerId);
        return;
    }

    //背景色
    COLORREF crObj;
    crObj = DRAW_CONFIG->GetBackColor();

    if (m_iWidth != 1)
    {
        pView->SetLineAlternateMode(true);
    }

    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }

    Circle( pView, iLayerId, &m_Circle, pParentObject.get(), m_iLineType, m_iWidth, crObj, -1);

    if (m_iWidth != 1)
    {
        pView->SetLineAlternateMode(false);
    }

}

/**
 *  @brief  リフレクション設定.
 *  @param      なし
 *  @retval     なし
 *  @note
 */
void CDrawingCircle::RegisterReflection()
{
    /*
	//RegisterProperty<POINT2D>       ( _T("Point1")   , &GetPoint1     , &SetPoint1 );
	//RegisterProperty<POINT2D>       ( _T("Point2")   , &GetPoint2     , &SetPoint2 );
	//RegisterProperty<int>           ( _T("Width")       , &GetWidth      , &SetWidth );
	//RegisterProperty<int>           ( _T("LineType")    , &GetLineType   , &SetLineType );

	RegisterProperty<POINT2D>       ( "Point1"   , &GetPoint1     , &SetPoint1 );
	RegisterProperty<POINT2D>       ( "Point2"   , &GetPoint2     , &SetPoint2 );
	RegisterProperty<int>           ( "Width"       , &GetWidth      , &SetWidth );
	RegisterProperty<int>           ( "LineType"    , &GetLineType   , &SetLineType );
    */
}

//!< 範囲
CDrawingObject::RELATION CDrawingCircle::RelationRect(const RECT2D& rcArea) const
{
    if (m_eType == DT_ELLIPSE)
    {
        //auto pEllipse = dynamic_cast<const CDrawingEllipse*>(this);
        //return pEllipse->RelationRect(rcArea);
        return CDrawingEllipse::RelationRect(rcArea);
    }

    std::vector<POINT2D> lstPoint;

    LINE2D line1(rcArea.dLeft, rcArea.dTop, rcArea.dLeft, rcArea.dBottom);
    LINE2D line2(rcArea.dRight, rcArea.dTop, rcArea.dRight, rcArea.dBottom);
    LINE2D line3(rcArea.dLeft, rcArea.dTop, rcArea.dRight, rcArea.dTop);
    LINE2D line4(rcArea.dLeft, rcArea.dBottom, rcArea.dRight, rcArea.dBottom);

    //--------------------
    //  MATRIX
    //--------------------
    const MAT2D* pMat = GetParentMatrix();
    CIRCLE2D tmpCircle(m_Circle);
    ELLIPSE2D* pEllipse = NULL;

    if (pMat != NULL)
    {
        tmpCircle.Matrix(*pMat, &pEllipse);
    }
    //--------------------

    auto pCneter = &(tmpCircle.GetCenter());
    double dR = tmpCircle.GetRadius();
    if (!pEllipse)
    {
        if ((pCneter->GetX() + dR < rcArea.dRight) &&
            (pCneter->GetX() - dR > rcArea.dLeft) &&
            (pCneter->GetY() - dR < rcArea.dTop) &&
            (pCneter->GetY() + dR > rcArea.dBottom))
        {   
            return REL_INNER;
        }

        if (tmpCircle.Intersect(line1, &lstPoint, true)) { return REL_PART; }
        if (tmpCircle.Intersect(line2, &lstPoint, true)) { return REL_PART; }
        if (tmpCircle.Intersect(line3, &lstPoint, true)) { return REL_PART; }
        if (tmpCircle.Intersect(line4, &lstPoint, true)) { return REL_PART; }
    }
    else
    {
        if (pEllipse->Intersect(line1, &lstPoint, true)) { return REL_PART; }
        if (pEllipse->Intersect(line2, &lstPoint, true)) { return REL_PART; }
        if (pEllipse->Intersect(line3, &lstPoint, true)) { return REL_PART; }
        if (pEllipse->Intersect(line4, &lstPoint, true)) { return REL_PART; }

        RECT2D rcEllipse = pEllipse->GetBounds();

        if (rcArea.IsInside(rcEllipse))
        {
            return REL_INNER;
        }
    }
    return REL_NONE;
}


/**
 *  @brief   矩形内にあるか
 *  @param   [in] rcArea 範囲内を調べる矩形
 *  @param   [in] bPart 一部が含まれている  
 *  @retval  true:2点で示される矩形内に点が存在する 
 *  @note
 */
bool CDrawingCircle::IsInner( const RECT2D& rcArea, bool bPart) const
{
    if (m_eType == DT_ELLIPSE)
    {
        return CDrawingEllipse::IsInner(rcArea, bPart);
    }

    std::vector<POINT2D> lstPoint;

    LINE2D line1(rcArea.dLeft , rcArea.dTop,    rcArea.dLeft , rcArea.dBottom);
    LINE2D line2(rcArea.dRight, rcArea.dTop,    rcArea.dRight, rcArea.dBottom);
    LINE2D line3(rcArea.dLeft , rcArea.dTop,    rcArea.dRight, rcArea.dTop);
    LINE2D line4(rcArea.dLeft , rcArea.dBottom, rcArea.dRight, rcArea.dBottom);

    //--------------------
    //  MATRIX
    //--------------------
    const MAT2D* pMat =  GetParentMatrix();
    CIRCLE2D tmpCircle(m_Circle);
    ELLIPSE2D* pEllipse = NULL;

    if (pMat != NULL)
    {
        tmpCircle.Matrix(*pMat, &pEllipse);
    }
    //--------------------

    if(bPart)
    {
        if (pEllipse == NULL)
        {
            if(tmpCircle.Intersect(line1, &lstPoint, true)){return true;}
            if(tmpCircle.Intersect(line2, &lstPoint, true)){return true;}
            if(tmpCircle.Intersect(line3, &lstPoint, true)){return true;}
            if(tmpCircle.Intersect(line4, &lstPoint, true)){return true;}
        }
        else
        {
            if(pEllipse->Intersect(line1, &lstPoint, true)){return true;}
            if(pEllipse->Intersect(line2, &lstPoint, true)){return true;}
            if(pEllipse->Intersect(line3, &lstPoint, true)){return true;}
            if(pEllipse->Intersect(line4, &lstPoint, true)){return true;}
        }
    }

    if (pEllipse == NULL)
    {
        RECT2D rcCircle = tmpCircle.GetBounds();
        if (rcArea.IsInside(rcCircle))
        {
            return true;
        }
    }
    else
    {

        RECT2D rcEllipse = pEllipse->GetBounds();
        if (rcArea.IsInside(rcEllipse))
        {
            return true;
        }
    }
    return false;
}

/**
 *  @brief  位置調整.
 *  @param  [in]    pPtIntersect   調整位置
 *  @param  [in]    pPtClick       調整方向指定点
 *  @retval         なし
 *  @note
 */
bool CDrawingCircle::Trim (const POINT2D& ptIntersect, const POINT2D& ptClick, bool bReverse)
{
    if (m_eType == DT_ELLIPSE)
    {
        return CDrawingEllipse::Trim(ptIntersect, ptClick, bReverse);
    }

    AddChgCnt();
    m_Circle.Trim(ptIntersect, ptClick, bReverse);
    return true;
}

/**
 *  @brief  位置調整.
 *  @param  [in]    pPtIntersect   調整位置
 *  @param  [in]    pPtClick       調整方向指定点
 *  @retval         なし
 *  @note
 */
bool CDrawingCircle::TrimCorner (const POINT2D& ptIntersect, const POINT2D& ptClick)
{
    AddChgCnt();
    m_Circle.TrimCorner(ptIntersect, ptClick);
    return true;
}

/**
 *  @brief  分割.
 *  @param  [in]    pPtBreak   分割位置
 *  @retval         分割オブジェクト
 *  @note           オブジェクトは使用側で破棄すること  
 */
std::shared_ptr<CDrawingObject>  CDrawingCircle::Break (const POINT2D& ptBreak)
{
    if (m_eType == DT_ELLIPSE)
    {
        return CDrawingEllipse::Break( ptBreak);
    }


    //!<  念のため近接点を取得
    POINT2D ptNear = m_Circle.NearPoint(ptBreak);

    
    double dAngle = atan2( ptNear.dY -GetCenter().dY, 
                           ptNear.dX -GetCenter().dX) * RAD2DEG;

    //分割位置は円弧上？
    if (!m_Circle.IsInsideAngle(dAngle))
    {
        return NULL;   
    }

    auto circleBreak = std::make_shared<CDrawingCircle>(*this);

    //選択を解除しておく
    circleBreak->SetSelect(false);

    SetEnd(dAngle);
    circleBreak->SetStart(dAngle);

    AddChgCnt();
    return circleBreak;
}


/**
 *  @brief   描画(マウスオーバー時).
 *  @param   [in] pView 表示View
 *  @param   [in] bOver true:開始 false:終了
 *  @retval  なし
 *  @note
 */
void CDrawingCircle::DrawOver( CDrawingView* pView, bool bOver)
{
}


/**
 *  @brief   プロパティ初期化.
 *  @param   [in] pCtrl オブジェクトコントロールへのポインタ
 *  @retval     なし
 *  @note
 */
TREE_GRID_ITEM*  CDrawingCircle::_CreateStdPropertyTree()
{
    if (m_eType == DT_ELLIPSE)
    {
        return CDrawingEllipse::_CreateStdPropertyTree();
    }

    NAME_TREE_ITEM<CStdPropertyItem>* pBeforeGroup;
    NAME_TREE_ITEM<CStdPropertyItem>* pNextItem;
    NAME_TREE_ITEM<CStdPropertyItem>* pGroupItem;
    NAME_TREE<CStdPropertyItem>* pTree;
    //int iId;
    CStdPropertyItem* pItem;
    boost::tuple< CStdPropertyItem*, void*> pairVal;


    pBeforeGroup = CDrawingObject::_CreateStdPropertyTree();
    pTree = m_psProperty->GetTreeInstance();

    pGroupItem  = pTree->AddNext(pBeforeGroup, NULL, GET_STR(STR_PRO_LINE), _T("Line"));

#ifdef _DEBUG //Clockwise
    DB_PRINT(_T(" Clockwise %s \n"), (m_Circle.IsClockwise()? _T("true"): _T("false"))); 
#endif
    //------------
    //中心点
    //------------
    CStdPropertyItemDef DefCenter(PROP_POINT2D, 
        GET_STR(STR_PRO_CENTER)   , 
        _T("Center"),
        GET_STR(STR_PRO_INFO_CENTER), 
        true,
        DISP_UNIT,
        POINT2D());

    pItem = new CStdPropertyItem( DefCenter, PropCenter, m_psProperty, NULL, &m_Circle.ptCenter);
    pNextItem = pTree->AddChild(pGroupItem, pItem, DefCenter.strDspName, pItem->GetName(), 2);

    //------------
    //半径
    //------------
    CStdPropertyItemDef DefRad(PROP_DOUBLE, 
        GET_STR(STR_PRO_RAD)   , 
        _T("Rad"),
        GET_STR(STR_PRO_INFO_RAD), 
        true,
        DISP_UNIT,
        double(0.0));

    pItem = new CStdPropertyItem( DefRad, PropRad, m_psProperty, NULL, &m_Circle.dRad);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefRad.strDspName, pItem->GetName());

    //------------
    //開始角(deg)
    //------------
    CStdPropertyItemDef DefStart(PROP_DOUBLE, 
        GET_STR(STR_PRO_START_ANGLE)   , 
        _T("Start"),
        GET_STR(STR_PRO_INFO_START_ANGLE), 
        true,
        DISP_ANGLE,
        double(0.0));

    pItem = new CStdPropertyItem( DefStart, PropStart, m_psProperty, NULL, &m_Circle.dStart);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefStart.strDspName, pItem->GetName());

    //------------
    //終了角(deg)
    //------------
    CStdPropertyItemDef DefEnd(PROP_DOUBLE, 
        GET_STR(STR_PRO_END_ANGLE)   , 
        _T("End"),
        GET_STR(STR_PRO_INFO_END_ANGLE), 
        true,
        DISP_ANGLE,
        double(0.0));

    pItem = new CStdPropertyItem( DefEnd, PropEnd, m_psProperty, NULL, &m_Circle.dEnd);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefEnd.strDspName, pItem->GetName());


    //------------
    //線幅
    //------------
    CStdPropertyItemDef DefLineWidth(PROP_LINE_WIDTH, 
        GET_STR(STR_PRO_LINE_WIDTH)   , 
        _T("Width"),
        GET_STR(STR_PRO_INFO_LINE_WIDTH), 
        true,
        DISP_NONE,
        0);

    pItem = new CStdPropertyItem( DefLineWidth, PropLineWidth, m_psProperty, NULL, &m_iWidth);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefLineWidth.strDspName, pItem->GetName());

    //------------
    //線種
    //------------
    CStdPropertyItemDef DefLineType(PROP_LINE_TYPE, 
        GET_STR(STR_PRO_LINE_TYPE)   , 
        _T("Type"),
        GET_STR(STR_PRO_INFO_LINE_TYPE), 
        false,
        DISP_NONE,
        0);

    pItem = new CStdPropertyItem( DefLineType, PropLineType, m_psProperty, NULL, &m_iLineType);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefLineType.strDspName, pItem->GetName());

    //--------------------
    // 塗りつぶし
    //--------------------
    pNextItem = m_Fill.CreateStdPropertyTree(m_psProperty, pTree, pNextItem);

    return pGroupItem;
}

/**
 *  @brief   プロパティ変更(中心点)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval  false 変更失敗
 *  @note
 */
bool CDrawingCircle::PropCenter   (CStdPropertyItem* pData, void* pObj)
{
    CDrawingCircle* pDrawing = reinterpret_cast<CDrawingCircle*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->SetCenter(pData->anyData.GetPoint());
        pDrawing->m_pCtrl->Draw(pDrawing->m_nId);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(半径)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingCircle::PropRad (CStdPropertyItem* pData, void* pObj)
{
    CDrawingCircle* pDrawing = reinterpret_cast<CDrawingCircle*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->m_pCtrl->HideObject(pDrawing->m_nId);
        pDrawing->SetRadius(pData->anyData.GetDouble());
        pDrawing->m_pCtrl->Draw(pDrawing->m_nId);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(開始角)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingCircle::PropStart    (CStdPropertyItem* pData, void* pObj)
{
    CDrawingCircle* pDrawing = reinterpret_cast<CDrawingCircle*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->m_pCtrl->HideObject(pDrawing->m_nId);
        pDrawing->SetStart(pData->anyData.GetDouble());
        pDrawing->m_pCtrl->Draw(pDrawing->m_nId);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(開始角)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingCircle::PropEnd      (CStdPropertyItem* pData, void* pObj)
{
    CDrawingCircle* pDrawing = reinterpret_cast<CDrawingCircle*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->m_pCtrl->HideObject(pDrawing->m_nId);
        pDrawing->SetEnd(pData->anyData.GetDouble());
        pDrawing->m_pCtrl->Draw(pDrawing->m_nId);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(線幅)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingCircle::PropLineWidth    (CStdPropertyItem* pData, void* pObj)
{
    CDrawingCircle* pDrawing = reinterpret_cast<CDrawingCircle*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->m_pCtrl->HideObject(pDrawing->m_nId);
        pDrawing->SetLineWidth(pData->anyData.GetInt());
        pDrawing->m_pCtrl->Draw(pDrawing->m_nId);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(線種)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingCircle::PropLineType (CStdPropertyItem* pData, void* pObj)
{
    CDrawingCircle* pDrawing = reinterpret_cast<CDrawingCircle*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->m_pCtrl->HideObject(pDrawing->m_nId);
        pDrawing->SetLineType(pData->anyData.GetInt());
        pDrawing->m_pCtrl->Draw(pDrawing->m_nId);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}


/**
 *  @brief   参照データに基づいて更新.
 *  @param   
 *  @retval  
 *  @note
 */
CDrawingObject* CDrawingCircle::UpdateRef()
{
    if (m_eType == DT_ELLIPSE)
    {
        return CDrawingEllipse::UpdateRef();
    }

    CDrawingObject*      pRef;

    pRef = CDrawingObject::UpdateRef();

    if (pRef == NULL)
    {
        return NULL;
    }

    CDrawingCircle* pCircle = dynamic_cast<CDrawingCircle*>(pRef);

    STD_ASSERT(pCircle != NULL);

    if (pCircle == NULL)
    {
        return false;
    }


    m_Circle    = pCircle->m_Circle;
    m_iWidth    = pCircle->m_iWidth;
    m_iLineType = pCircle->m_iLineType;

    m_iChgCnt = pRef->GetChgCnt();
    return pRef;
}

/**
 *  @brief   始点終点交換.
 *  @param   なし
 *  @retval  なし
 *  @note    Line,Circle,Splie,CompositLine
 */
bool  CDrawingCircle::SwapStartEnd()
{
    if (m_eType == DT_ELLIPSE)
    {
        return CDrawingEllipse::SwapStartEnd();
    }

    m_Circle.SwapStartEnd();
    return true;
}

/**
 *  @brief   始点取得.
 *  @param   なし
 *  @retval  始点
 *  @note    Line,Circle,Splie,CompositLine
 */
POINT2D  CDrawingCircle::GetStartSide() const
{
    if (m_eType == DT_ELLIPSE)
    {
        return CDrawingEllipse::GetStartSide();
    }

    if(m_Circle.IsClockwise())
    {
        return m_Circle.GetEndPointVal();
    }
    else
    {
        return m_Circle.GetStartPointVal();
    }
}

/**
 *  @brief   終点取得.
 *  @param   なし
 *  @retval  終点
 *  @note    Line,Circle,Splie,CompositLine
 */
POINT2D  CDrawingCircle::GetEndSide() const
{
    if (m_eType == DT_ELLIPSE)
    {
        return CDrawingEllipse::GetEndSide();
    }

    if(m_Circle.IsClockwise())
    {
        return m_Circle.GetStartPointVal();
    }
    else
    {
        return m_Circle.GetEndPointVal();
    }
}

/**
*  @brief   中点取得.
*  @param   なし
*  @retval  中点
*  @note    Line,Circle,Splie,CompositLine
*/
POINT2D  CDrawingCircle::GetMidPoint() const
{
    return GetPointByRate(0.5);
}


//!<  任意点取得(Line,Circle,Splie,CompositLine用)
POINT2D  CDrawingCircle::GetPointByRate(double d) const
{
    if (m_eType == DT_ELLIPSE)
    {
        return CDrawingEllipse::GetMidPoint();
    }

    double dAngle;
    double dStartOrg = m_Circle.GetStart();
    double dEndOrg   = m_Circle.GetEnd();
    double dS = dStartOrg;
    double dE = dEndOrg;

    if(fabs( dStartOrg - dEndOrg) < NEAR_ZERO)
    {
        dAngle =  dStartOrg + 180.0;
    }
    else
    {
        if(m_Circle.IsClockwise())
        {
            if(dEndOrg > dStartOrg)
            {
                dS = dStartOrg + 180.0;    
            }
        }
        else
        {
            if(dEndOrg < dStartOrg)
            {
                dE = dEndOrg + 180.0;    
            }
        }
        dAngle = dS * (1.0 - d) + dE * d;
    }
    return m_Circle.GetPoint(dAngle);
}


//!<  近接点取得(Line,Circle,Splie,CompositLine用)
bool  CDrawingCircle::GetNearPoints(POINT2D*  ptNear, const POINT2D& pt, bool bOnline) const
{
    POINT2D ptRet = m_Circle.NearPoint(pt, bOnline);
    *ptNear = ptRet; 
    return true;
}

//!<  法線角取得(Line,Circle,Splie,CompositLine用)
bool  CDrawingCircle::GetNormAngle(double* pAngle, const POINT2D& pt, bool bOnline) const
{
    m_Circle.NearPoint(pt, bOnline);
    POINT2D ptC = m_Circle.GetCenter();
    double dAngle = atan2(ptC.dY - pt.dY,  ptC.dX - pt.dX) * RAD2DEG;
    *pAngle = dAngle;
    return true;
}



//円弧上の点から始点(終点）までの円弧の長さを求める。
double CDrawingCircle::Length(const POINT2D& ptIntersect, bool bStart) const
{
    POINT2D pt = m_Circle.NearPoint(ptIntersect, true);
    POINT2D ptAngle;
    ptAngle.dX = pt.dX - m_Circle.GetCenter().GetX();
    ptAngle.dY = pt.dY - m_Circle.GetCenter().GetY();

    double dIntersect = atan2(ptAngle.dY, ptAngle.dX);

    double dRet;
    double dAngle = 0.0;
    if(bStart)
    {
        if(!m_Circle.IsClockwise())
        {
            dAngle = dIntersect - m_Circle.GetStart()* DEG2RAD;
        }
        else
        {
            dAngle = m_Circle.GetStart()* DEG2RAD - dIntersect;
        }
    }
    else
    {
        if(!m_Circle.IsClockwise())
        {
            dAngle = m_Circle.GetEnd()* DEG2RAD - dIntersect;
        }
        else
        {
            dAngle = dIntersect - m_Circle.GetEnd()* DEG2RAD;
        }
    }
    dRet =  m_Circle.GetRadius() * 2.0 *  dAngle;
    return dRet;
}

//!< オフセット(線,円弧,スプライン上)
bool CDrawingCircle::Offset(double dLen, POINT2D ptDir) 
{
    if (m_eType == DT_ELLIPSE)
    {
        return CDrawingEllipse::Offset(dLen, ptDir);
    }

    m_Circle = m_Circle.Offset(dLen, ptDir);
    return true;
}

//!< オフセット(線,円弧,スプライン上)
bool CDrawingCircle::Offset(double dLen)
{
    if (m_eType == DT_ELLIPSE)
    {
        return CDrawingEllipse::Offset(dLen);
    }

    //進行方向右側を正とする
    POINT2D ptDir;
    
    ptDir = m_Circle.GetCenter();
    ptDir.dX +=  m_Circle.GetRadius();

    
    if(m_Circle.IsClockwise())
    {
        ptDir.dX -= dLen; 
    }
    else
    {
        ptDir.dX += dLen; 
    }

    m_Circle = m_Circle.Offset(fabs(dLen), ptDir);
    return true;

}

//!< 点が進行方向の右側にあるか
bool CDrawingCircle::IsRightSide(POINT2D ptDir) const
{
    if (m_eType == DT_ELLIPSE)
    {
        return CDrawingEllipse::IsRightSide(ptDir);
    }
    
    bool bRet = false;
    if(m_Circle.GetRadius() <= m_Circle.GetCenter().Distance(ptDir))
    {
        bRet = true;
    }

    if(m_Circle.IsClockwise())
    {
        bRet = (!bRet);
    }
DB_PRINT(_T("CDrawingCircle::IsRightSide %d R(%d)\n"), bRet, m_Circle.IsClockwise());
    return bRet;
}


/**
 *  @brief   スナップ点取得.
 *  @param   [out] pLstSnap スナップ点リスト
 *  @retval  true: スナップ点あり
 *  @note
 */
bool CDrawingCircle::GetSnapList(std::list<SNAP_DATA>* pLstSnap)const 
{
    if (m_eType == DT_ELLIPSE)
    {
        return CDrawingEllipse::GetSnapList(pLstSnap);
    }
    
    using namespace VIEW_COMMON;

    DWORD dwSnap = DRAW_CONFIG->GetSnapType();
    bool bRet = false;

    STD_ASSERT(pLstSnap);

    if (!pLstSnap)
    {
        return false;
    }

    pLstSnap->clear();
    SNAP_DATA snapData;

    if (dwSnap & SNP_CENTER)
    {
        snapData.eSnapType = SNP_CENTER;
        snapData.pt = GetCenter();


        if (dwSnap & SNP_CIRCLE)
        {
            snapData.bKeepSelect = true;
        }
        else
        {
            snapData.bKeepSelect = false;
        }

        pLstSnap->push_back(snapData);
        bRet = true;
    }

    if ((dwSnap & SNP_END_POINT)&&
        (!m_Circle.IsClose()))

    {
        snapData.eSnapType = SNP_END_POINT;
        snapData.pt = GetPoint1();
        snapData.bKeepSelect = false;
        pLstSnap->push_back(snapData);

        snapData.pt = GetPoint2();
        pLstSnap->push_back(snapData);
        bRet = true;
    }

    return bRet;
}

/**
 * @brief   領域取得
 * @param   なし
 * @retval  なし
 * @note	描画領域を取得する
 */
RECT2D CDrawingCircle::GetBounds() const
{
    if (m_eType == DT_ELLIPSE)
    {
        return CDrawingEllipse::GetBounds();
    }

    return m_Circle.GetBounds();
}

/**
 * @brief   距離
 * @param   [in] pt 
 * @retval  近接点からの距離
 * @note
 */
double CDrawingCircle::Distance(const POINT2D& pt) const
{
    if (m_eType == DT_ELLIPSE)
    {
        return CDrawingEllipse::Distance(pt);
    }

    POINT2D ptNear = m_Circle.NearPoint( pt, true /*bOnline*/);
    return pt.Distance(ptNear);
}


//!< 位置取得（AS)
POINT2D&  CDrawingCircle::GetCenter() const   
{
    return const_cast<POINT2D&>(m_Circle.ptCenter);
}


bool CDrawingCircle::OnSetCursor(CNodeMarker* pNodeMarker, CDrawingView* pView)
{
    if (m_eType == DT_ELLIPSE)
    {
        return CDrawingEllipse::OnSetCursor(pNodeMarker, pView);
    }
    //AfxGetApp()->LoadCursor, AfxGetApp()->LoadStandardCursorを使用すること

	if (pView->IsDrag())
	{
		return false;
	}

    StdString strId = pNodeMarker->GetMouseOver();
    E_NODE eNode = GetNode(strId);


    //右ボタンドラッグ(移動・コピー選択)できるのはセンターのみ
    if (pNodeMarker->GetMouseButton() == SEL_RBUTTON)
    {
        if( eNode != ND_C)
        {
            return false;
        }
    }

	HCURSOR hCursor;
	switch(eNode)
	{
	case ND_R1:
		::SetCursor(AfxGetApp()->LoadCursor(IDC_RADIUS));
		break;

	case ND_S:
		::SetCursor(AfxGetApp()->LoadCursor(IDC_START));
		break;

	case ND_E:
		::SetCursor(AfxGetApp()->LoadCursor(IDC_END));
		break;

	case ND_C:
		hCursor =(HCURSOR)LoadImage(NULL, IDC_SIZEALL,IMAGE_CURSOR,
                                    NULL,NULL,LR_DEFAULTCOLOR | LR_SHARED);
		::SetCursor(hCursor);
		break;

	default:
		//::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
        //break;
		return false;
	}
	
	return true;

}

//!< マーカ初期化 true:マーカあり
bool CDrawingCircle::InitNodeMarker(CNodeMarker* pMarker)
{
    if (m_eType == DT_ELLIPSE)
    {
        return CDrawingEllipse::InitNodeMarker(pMarker);
    }

    CDrawingView* pView = pMarker->GetView();
    pView->ClearDraggingNodes();
    pView->ClearDraggingTmpObjects();

    auto lstNode = pView->GetDraggingNodes();
    lstNode->resize(ND_R2);
    for(auto& pNode: *lstNode)
    {
        pNode = std::make_shared<CDrawingNode>();
        pNode->SetVisible(true);
        pNode->SetMarkerShape(DMS_CIRCLE_FILL);
    }

    lstNode->at(ND_C)->SetName(_T("C"));
    lstNode->at(ND_R1)->SetName(_T("R1"));
    lstNode->at(ND_S)->SetName(_T("S"));
    lstNode->at(ND_E)->SetName(_T("E"));

    STD_ASSERT(pMarker);

    double dLen = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->GetMarkerSize());  
    int iR =  CUtil::Round(dLen * pView->GetDpu());

    pMarker->SetRadius(iR);
    pMarker->SetObject(shared_from_this());


	//マウスカーソル設定コールバック
    namespace  PLC = std::placeholders;
	pMarker->SetOnSetcursorCallback(std::bind(&CDrawingCircle::OnSetCursor, this, PLC::_1, PLC::_2));


    //ドラッグ表示用の図形を用意する
    auto lstTmpObject = pView->GetDraggingTmpObjects();
    lstTmpObject->push_back(std::make_shared<CDrawingCircle>(*this)); //表示用
    auto pCircleTmp = std::dynamic_pointer_cast<CDrawingCircle>(lstTmpObject->at(0));
    pCircleTmp->SetColor(DRAW_CONFIG->crImaginary);
    
    //　開始角　終了角
    lstTmpObject->push_back(std::make_shared<CDrawingCircle>(*this)); //表示用
    auto pCircleOffest = std::dynamic_pointer_cast<CDrawingCircle>(lstTmpObject->at(1));


    pCircleOffest->SetRadius(GetRadius() * 1.1);

    //補助線

	lstNode->at(ND_C)->SetPoint(GetCenter());
	lstNode->at(ND_R1)->SetPoint(m_Circle.GetPoint( 0.0));
	lstNode->at(ND_S)->SetPoint(pCircleOffest->GetStartSide());
	lstNode->at(ND_E)->SetPoint(pCircleOffest->GetEndSide());


    for(auto& pNode: *lstNode)
    {
        pMarker->Create(pNode.get(), pNode->GetPoint());
    }

    //補助線
    lstTmpObject->push_back(std::make_shared<CDrawingLine>());
    auto pLine = std::dynamic_pointer_cast<CDrawingLine>(lstTmpObject->at(2));

    pLine->SetColor(DRAW_CONFIG->crAdditional);
    pLine->SetLineType(PS_DOT);

    lstTmpObject->push_back(std::make_shared<CDrawingCircle>(*this)); //円弧の欠けた部分を描画
    auto pCircleMissing = std::dynamic_pointer_cast<CDrawingCircle>(lstTmpObject->at(3));
    pCircleMissing->SetColor(DRAW_CONFIG->crAdditional);
    pCircleMissing->SetLineType(PS_DOT);
    pCircleMissing->SwapStartEnd();

    CPartsDef*   pDef  = pView->GetPartsDef();
    if (!pCircleTmp->GetCircleInstance()->IsClose())
    {
        pDef->SetMouseEmphasis(pCircleMissing);
    }

    return true;
}

//!< マーカ選択
 void CDrawingCircle::SelectNodeMarker(CNodeMarker* pMarker,
                       StdString strMarkerId)
{
     if (m_eType == DT_ELLIPSE)
     {
         return CDrawingEllipse::SelectNodeMarker(pMarker, strMarkerId);
     }
     
     pMarker->SetVisibleOnly( strMarkerId, true);
    E_NODE eNode = CDrawingEllipse::GetNode(strMarkerId);

    if (eNode == ND_R1)
    {
        //補助線2
        CDrawingView* pView = pMarker->GetView();
        auto lstTmpObject = pView->GetDraggingTmpObjects();
        lstTmpObject->push_back(std::make_unique<CDrawingLine>());

        STD_ASSERT(lstTmpObject->size() == 5);

        CDrawingLine* pLine = dynamic_cast<CDrawingLine*>(lstTmpObject->at(4).get());

        pLine->SetColor(DRAW_CONFIG->crAdditional);
        pLine->SetLineType(PS_DOT);
    }
}

//!< マーカ移動
void CDrawingCircle::MoveNodeMarker(CNodeMarker* pMarker,
                                 SNAP_DATA* pSnap,
                                 StdString strMarkerId,
                                 MOUSE_MOVE_POS posMouse)
{
    if (m_eType == DT_ELLIPSE)
    {
        return CDrawingEllipse::MoveNodeMarker(pMarker, pSnap,
                                 strMarkerId, posMouse);
    }
    
    using namespace VIEW_COMMON;
    CDrawingView* pView = pMarker->GetView();

    bool bIgnoreSnap = false;
	bool bAngle = true;
	bool bLength = true;


    bool bSnap = true;

    SNAP_DATA snap;
    DWORD dwSnapType = DRAW_CONFIG->GetSnapType();

    DWORD dwSnapMask = ~SNP_ON_LINE;
    dwSnapType   &=  dwSnapMask;

    bSnap = pView->GetSnapPoint(&snap, 
                      dwSnapType,
                      posMouse.ptSel);

    CPartsDef*   pDef  = pView->GetPartsDef();
    auto lstTmpObject = pView->GetDraggingTmpObjects();
    auto pObj1 = pDef->GetObjectById(snap.iObject1);
    auto pObj2 = pDef->GetObjectById(snap.iObject2);

    auto pCircleTmp = std::dynamic_pointer_cast<CDrawingCircle>(lstTmpObject->at(0));
    auto pLine = std::dynamic_pointer_cast<CDrawingLine>(lstTmpObject->at(2));

    pCircleTmp->SetColor(DRAW_CONFIG->crImaginary);
    auto pCircleMissing = std::dynamic_pointer_cast<CDrawingCircle>(lstTmpObject->at(3));
    auto pNode = pMarker->GetDrawingNode(strMarkerId);

    if (posMouse.nFlags & MK_SHIFT)
    {
        //スナップ無視
        bIgnoreSnap = true;
		bAngle = false;
        pObj1 = NULL;
		bLength = false;
        pSnap->eSnapType = SNP_NONE;
        pSnap->pt = pSnap->ptOrg;
    }

    E_NODE eNode = GetNode(strMarkerId);

    //右ボタンドラッグ(移動・コピー選択)できるのはセンターのみ
    if (pMarker->GetMouseButton() == SEL_RBUTTON)
    {
        if( eNode != ND_C)
        {
            return;
        }
    }

    if (eNode == ND_C)
    {
		bool bRet = false;
        POINT2D ptNewCenter;
        StdString strToolTip;

		double dAngle;
		double dLength;

        if ((!bSnap) &&  
            (!bIgnoreSnap))
        {
		    bRet =pView->GetLengthSnapPoint(&dAngle,
										    &dLength,
										    &ptNewCenter,  
										    GetCenter(),  
										    pSnap->ptOrg,
										    bAngle,
										    bLength);
        }
        else
        {
            ptNewCenter = pSnap->pt;
            snap.strSnap = GetSnapName(snap.eSnapType, false);
            snap.strSnap += _T("\n");

            POINT2D ptCenter = GetCenter();
            dLength  = ptCenter.Distance(snap.pt);
            dAngle   = atan2(snap.pt.dY-ptCenter.dY,
                             snap.pt.dX-ptCenter.dX) * RAD2DEG;
        }

        
		snap.strSnap += CViewAction::GetSnapText(SNP_ANGLE, 
													dLength,
													dAngle,
													false);

        pLine->SetPoint1(GetCenter());
        pLine->SetPoint2(ptNewCenter);
        pDef->SetMouseEmphasis(pLine);
        pCircleTmp->SetCenter(ptNewCenter);
        pDef->SetMouseOver(pCircleTmp);
        pNode->SetPoint(ptNewCenter);

    }
    else if( eNode == ND_R1)
    {
		bool bRet = false;
		double dLength;
        POINT2D ptSnap;
        double dAngleOffset = 0.0;

        StdStringStream   strmLen;
        strmLen << GET_STR(STR_PRO_RAD) << _T(":");


        bRet = pView->GetFixAngleSnapPoint(&dLength,
                                    &ptSnap,  
                                    0,
                                    GetCenter(),  
                                    pSnap->pt,
                                    bLength); //bSnap



        //ツールチップ
        if (bSnap)
        {
            snap.strSnap = GetSnapName(snap.eSnapType, false);
            snap.strSnap += _T("\n");
        }

        dLength = fabs(dLength);

        int iDecimalPlace = 3;
        strmLen << CDrawingObject::GetValText(dLength, false, iDecimalPlace);
	    snap.strSnap += strmLen.str();

        auto pLineSub = std::dynamic_pointer_cast<CDrawingLine>(lstTmpObject->at(4));
        pLineSub->SetPoint1(GetCenter());
        pLineSub->SetPoint2(ptSnap);

        pCircleTmp->SetRadius(dLength);

        pMarker->SetMarkerPos(strMarkerId, ptSnap);
        pDef->SetMouseOver(pCircleTmp);
        pDef->SetMouseEmphasis(pLine);
        pDef->SetMouseEmphasis(pLineSub);

        pNode->SetPoint(ptSnap);

    }
    


    else if((eNode ==ND_S) ||
            (eNode ==ND_E))
    {       

        auto pCircleOffest = std::dynamic_pointer_cast<CDrawingCircle>(lstTmpObject->at(1));

		bool bRet = false;
		double dAngle;
		double dLength;
        POINT2D ptSnap;

        if ((!bSnap) &&  
            (!bIgnoreSnap))
        {
		    bRet = pView->GetLengthSnapPoint(&dAngle,
								 		     &dLength,
										     &ptSnap,  
										     GetCenter(),  
										     pSnap->ptOrg,
										     true,
										     false,
                                             GetAngle());

        }
        else
        {
            ptSnap = snap.pt;
            snap.strSnap = GetSnapName(snap.eSnapType, false);
            snap.strSnap += _T("\n");

            POINT2D ptCenter = GetCenter();
            dLength  = ptCenter.Distance(snap.pt);
            dAngle   = atan2(snap.pt.dY-ptCenter.dY,
                             snap.pt.dX-ptCenter.dX) * RAD2DEG;

            dAngle = CUtil::NormAngle( dAngle - GetAngle());

        }


        StdStringStream   strmAngle;


        //ツールチップ
        if (bSnap)
        {
            snap.strSnap = GetSnapName(snap.eSnapType, false);
            snap.strSnap += _T("\n");
        }

        if (eNode ==ND_S)
        {
            strmAngle << GET_STR(STR_PRO_START_ANGLE) << _T(":");
            pCircleTmp->SetStart(dAngle);
        }
        else
        {
            strmAngle << GET_STR(STR_PRO_END_ANGLE) << _T(":");
            pCircleTmp->SetEnd(dAngle);
        }

        int iDecimalPlace = 3;
        strmAngle << CDrawingObject::GetValText(dAngle, true, iDecimalPlace);
	    snap.strSnap += strmAngle.str();

        pLine->SetPoint1(ptSnap);
        pLine->SetPoint2(GetCenter());

        pDef->SetMouseOver(pCircleTmp);
        pDef->SetMouseEmphasis(pLine);

        pCircleMissing->SetStart(pCircleTmp->GetStart());
        pCircleMissing->SetEnd(pCircleTmp->GetEnd());

        pNode->SetPoint(ptSnap);

    }

    if (!pCircleTmp->GetCircleInstance()->IsClose())
    {
        pDef->SetMouseEmphasis(pCircleMissing);
    }

    pSnap->strSnap = snap.strSnap;
}

//!< マーカ開放
bool CDrawingCircle::ReleaseNodeMarker(CNodeMarker* pMarker, 
                                       StdString strMarkerId,
                                       MOUSE_MOVE_POS posMouse)
{
    if (m_eType == DT_ELLIPSE)
    {
        return CDrawingEllipse::ReleaseNodeMarker(pMarker, strMarkerId, posMouse);
    }

    //右ボタンドラッグ(移動・コピー選択)できるのはセンターのみ
    CDrawingView* pView = pMarker->GetView();

    StdString strId = pMarker->GetSelectId();
    E_NODE eNode = GetNode(strId);
    SEL_MENU eMenu = SEL_MNU_MOVE;
    if ((pMarker->GetMouseButton() == SEL_RBUTTON)&&
        ( eNode == ND_C))
    {
        eMenu = _ShowMoveCopyMenu(pMarker);
    }

    CPartsDef*   pDef  = pView->GetPartsDef();
    CUndoAction*       pUndo;
    pUndo  = pDef->GetUndoAction();

    SetSelect(false);

    auto lstTmpObject = pView->GetDraggingTmpObjects();
    auto pCircleTmp = std::dynamic_pointer_cast<CDrawingCircle>(lstTmpObject->at(0));

    bool bUndo = false;
    if (eMenu == SEL_MNU_MOVE)
    {
        pUndo->AddStart(UD_CHG, pDef, this);
        m_Circle = *(pCircleTmp->GetCircleInstance());
        pUndo->AddEnd(shared_from_this(), true);
        bUndo = true;
    }
    else if (eMenu == SEL_MNU_COPY)
    {

        auto pNewCircle = std::make_shared<CDrawingCircle>( *pCircleTmp );

        pNewCircle->SetColor(GetColor());
        pNewCircle->SetLineType(GetLineType());
        pNewCircle->SetLineWidth(GetLineWidth());
        pNewCircle->m_Fill = m_Fill;

        pDef->RegisterObject(pNewCircle, true, false);
        pUndo->Add(UD_ADD, pDef, NULL, pNewCircle);
        bUndo = true;
    }

    if (bUndo)
    {
        pUndo->Push();
    }

    pView->ClearDraggingNodes();
    pView->ClearDraggingTmpObjects();

    ::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
    pMarker->Clear();

    return true;
}

//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void TEST_CDrawingCircle()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}
#endif //_DEBUG