/**
 * @brief			CDrawingCircleヘッダーファイル
 * @file			CDrawingCircle.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#if !defined(__CDRAWING_CIRCLE_H__)
#define __CDRAWING_CIRCLE_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DrawingObject/CDrawingObject.h"

#include "DrawingObject/CDrawingEllipse.h"
#include "System/MOCK_ERROR.h"
#include "DrawingObject/Primitive/CIRCLE2D.H"

/**
 * @class   CDrawingCircle
 * @brief   円                     
   
 */
class CDrawingCircle :public CDrawingEllipse//:public CRTTIClass<CDrawingCircle, CDrawingObject, DT_LINE>
{


public:
	//!< コンストラクタ
	CDrawingCircle();

	//!< コンストラクタ
    CDrawingCircle(int nID);

    //!< コピーコンストラクタ
    CDrawingCircle(const CDrawingCircle& Obj);

	//!< デストラクタ
	virtual ~CDrawingCircle();

	//!< 相対移動 (AS)
	virtual void Move(const POINT2D& pt2D);

    //!< 回転       (AS)
    virtual void Rotate(const POINT2D& pt2D, double dAngle);

	//!< 交点計算   (AS)
    virtual void CalcIntersection ( std::vector<POINT2D>* pLiat,   
                                    const CDrawingObject* pObj,
                                    bool bOnline = true,
                                    double dMin = NEAR_ZERO) const;

    //!< 鏡像       (AS)
	virtual void Mirror(const LINE2D& line);

    //!< 倍率       (AS)
	virtual void Scl2(const POINT2D& pt2D, double dXScl, double dYScl, ELLIPSE2D** ppEllipse);
    virtual void Scl(const POINT2D& pt2D, double dXScl, double dYScl);

    //!< 行列適用  (AS)
    virtual void Matrix2(const MAT2D& mat2, ELLIPSE2D** ppEllipse);

    //!< 行列適用 
    virtual void Matrix(const MAT2D& mat2);

    //!< 調整      (AS)
    virtual bool Trim (const POINT2D& ptIntersect, const POINT2D& ptClick, bool bReverse);

    //!< 調整          
    virtual bool TrimCorner (const POINT2D& ptIntersect, const POINT2D& ptClick);

      //!< 分割      (AS)
    virtual std::shared_ptr<CDrawingObject>  Break (const POINT2D& ptIntersect);

	//!< 範囲      (AS)
    virtual bool IsInner( const RECT2D& rcArea, bool bPart) const;

    virtual RELATION RelationRect(const RECT2D& rcArea) const override;

     //!< 描画
    virtual void Draw(CDrawingView* pView, 
                      const std::shared_ptr<CDrawingObject> pParentObject,
                      E_MOUSE_OVER_TYPE eMouseOver = E_MO_NO,
                      int iLayerId = -1) override;
    
    //!< 描画削除
    virtual void DeleteView(CDrawingView* pView,
                            const std::shared_ptr<CDrawingObject> pParentObject,
                            int iLayerId = -1) override;

    //!< 領域取得  (AS)
    virtual RECT2D GetBounds() const;

    //!< 距離
    virtual double Distance(const POINT2D& pt) const;

    bool IsEllipse() const;


    //-------------------------
    // リフレクション設定
    //-------------------------

    //!< 中心設定（AS)
    void  SetCenter(POINT2D ptPos)        {m_Circle.ptCenter = ptPos;} 

    //!< 位置取得（AS)
    POINT2D&  GetCenter() const;

    //!< 半径設定（AS)
    void    SetRadius(double dRad)        {m_Circle.dRad = dRad;} 

    //!< 半径取得（AS)
    double  GetRadius()     const         {return m_Circle.dRad;}

    //!< 開始角設定（AS)
    void   SetStart(double dAngle)        {m_Circle.SetStart(dAngle);}

    //!< 開始角取得（AS)
    double GetStart()    const            {return m_Circle.GetStart();}

    //!< 終了角設定（AS)
    void   SetEnd(double dAngle)          {m_Circle.SetEnd(dAngle);} 

    //!< 終了角取得（AS)
    double GetEnd()     const             {return m_Circle.GetEnd();}

    //!< 端点1取得
    POINT2D  GetStartPoint() const        {POINT2D ptRet; m_Circle.GetStartPoint(&ptRet); return ptRet;}

    //!< 端点2取得
    POINT2D  GetEndPoint() const          {POINT2D ptRet; m_Circle.GetEndPoint(&ptRet); return ptRet;}

    //!< 円取得
    CIRCLE2D*  GetCircleInstance()             {return &m_Circle;}
    const CIRCLE2D*  GetCircleInstance() const {return &m_Circle;}

    //!< リフレクション設定
    static void RegisterReflection();

    //!< スナップ点取得
    virtual  bool GetSnapList(std::list<SNAP_DATA>* pLstSnap)const;

    //!< 始点終点交換(Line,Circle,Splie,CompositLine用)
    virtual bool  SwapStartEnd();

    //!< 始点取得(Line,Circle,Splie,CompositLine用)
    virtual POINT2D  GetStartSide() const override;

    //!< 始点取得(Line,Circle,Splie,CompositLine用)
    virtual POINT2D  GetEndSide() const override;

    //!<  中点取得(Line,Circle,Splie,CompositLine用)
    virtual POINT2D  GetMidPoint() const override;

    //!<  任意点取得(Line,Circle,Splie,CompositLine用)
    virtual POINT2D  GetPointByRate(double d) const override;

    //!<  近接点取得(Line,Circle,Splie,CompositLine用)
    virtual bool  GetNearPoints(POINT2D*  ptNear, const POINT2D& pt, bool bOnline) const override;

    //!<  法線角取得(Line,Circle,Splie,CompositLine用)
    virtual bool GetNormAngle(double* pAngle, const POINT2D& pt, bool bOnline) const override;

    //!< 長さ(線,円弧,スプライン上)
    virtual double Length(const POINT2D& ptIntersect, bool bStart) const override;

    //!< オフセット(線,円弧,スプライン上)
    virtual bool Offset(double dLen, POINT2D ptDir) override;

    //!< オフセット(線,円弧,スプライン上)
    virtual bool Offset(double dLen) override;

    //!< 点が進行方向の右側にあるか
    virtual bool IsRightSide(POINT2D ptDir) const override;

    //-------------------------
    // 参照
    //-------------------------

    //!< 参照データに基づいて更新
    virtual CDrawingObject* UpdateRef();

protected:
friend CDrawingCircle;

    //!< プロパティ初期化
    virtual TREE_GRID_ITEM*  _CreateStdPropertyTree();

    //-------------------
    //!< プロパティ変更
    //-------------------
    static bool PropCenter   (CStdPropertyItem* pData, void* pObj);
    static bool PropRad      (CStdPropertyItem* pData, void* pObj);
    static bool PropStart    (CStdPropertyItem* pData, void* pObj);
    static bool PropEnd      (CStdPropertyItem* pData, void* pObj);
    static bool PropLineWidth (CStdPropertyItem* pData, void* pObj);
    static bool PropLineType  (CStdPropertyItem* pData, void* pObj);
    //-------------------


    //-------------------
    // ドラッグ用マーカ
    //-------------------
    //!< マーカ初期化 true:マーカあり
    virtual bool InitNodeMarker(CNodeMarker* pMarker) override;

    //!< マーカ選択
    virtual void SelectNodeMarker(CNodeMarker* pMarker, 
                                  StdString strMarkerId) override;

    //!< マーカ移動
    virtual void MoveNodeMarker(CNodeMarker* pMarker, 
                                     SNAP_DATA* pSnap,
                                     StdString strMarkerId,
                                     MOUSE_MOVE_POS posMouse) override;

    //!< マーカ開放
    virtual bool ReleaseNodeMarker(CNodeMarker* pMarker,
                                   StdString strMarkerId,
                                    MOUSE_MOVE_POS posMouse) override;

    bool OnSetCursor(CNodeMarker* pNodeMarker, CDrawingView* pView);

protected:



    //!< 円描画(マウスオーバー)   
    virtual void DrawOver( CDrawingView* pView, bool bOver);

protected:
    //!< 位置
    CIRCLE2D    m_Circle;

    //!< 線幅
    //int         m_iWidth;

    //!< 線種
    //int         m_iLineType;

private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
            ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(CDrawingObject);
            if(m_eType == DT_ELLIPSE)
            {
                SERIALIZATION_BOTH("Ellipse", m_Ellipse);
            }
            else
            {
                SERIALIZATION_BOTH("Circle", m_Circle);
            }
            SERIALIZATION_BOTH("Width", m_iWidth);
            SERIALIZATION_BOTH("LineType", m_iLineType);
            SERIALIZATION_BOTH("Fill", m_Fill);
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }

};
#endif // __CDRAWING_CIRCLE_H__
