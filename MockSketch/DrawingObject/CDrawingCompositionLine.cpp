/**
 * @brief			CDrawingCompositionLine実装ファイル
 * @file			CDrawingCompositionLine.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CDrawingPoint.h"
#include "./CDrawingCompositionLine.h"
#include "./CDrawingCircle.h"
#include "./CDrawingDim.h"
#include "./CDrawingCircle.h"
#include "./CDrawingSpline.h"
#include "./CDrawingEllipse.h"
#include "./CDrawingLine.h"
#include "./CDrawingParts.h"
#include "./CDrawingNode.h"      
#include "./CNodeBound.h"
#include "DefinitionObject/View/CDrawingView.h"
#include "DefinitionObject/CPartsDef.h"
#include "View/CUndoAction.h"
#include "System/CSystem.h"
#include "System/MOCK_ERROR.h"
#include "Utility/CUtility.h"
#include <math.h>
#include <boost/serialization/export.hpp> 
//BOOST_CLASS_EXPORT(SEGMENT);
//BOOST_CLASS_EXPORT(SEGMENT_LIST);
BOOST_CLASS_EXPORT(CDrawingCompositionLine);

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//#define _DEBUG_CROSS
//#define _CANVAS_ADD_

template<class Archive>
void SEGMENT::load(Archive& ar, const unsigned int version)
{
    std::string name;
    name = "Object";
    SERIALIZATION_INIT
        pObj= CDrawingObject::Load<Archive>(ar, 0, name);
    MOCK_EXCEPTION_FILE(e_file_read);
}

template<class Archive>
void SEGMENT::save(Archive& ar, const unsigned int version) const
{
    std::string name;
    name = "Object";
    SERIALIZATION_INIT
        pObj->Save<Archive>(ar, 0, name);
    MOCK_EXCEPTION_FILE(e_file_write);

}

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
CDrawingCompositionLine::CDrawingCompositionLine():
CDrawingObject(),
m_bCreatedPolygon(false)
//m_dSkew(0.0)
{
	m_eType = DT_COMPOSITION_LINE;
    m_iWidth =    1;
    m_iLineType = PS_SOLID;
    m_iPropFeatures = P_BASE | P_LINE; 
    SetUsePropertySet(true);
}

/**
 *  @brief  コンストラクター.
 *  @param  [in] nID 
 *  @retval なし 
 *  @note
 */
CDrawingCompositionLine::CDrawingCompositionLine(int nID):
CDrawingObject( nID, DT_COMPOSITION_LINE),
m_bCreatedPolygon(false)
{
    m_iWidth =    1;
    m_iLineType = PS_SOLID;
    m_iPropFeatures = P_BASE | P_LINE; 
    SetUsePropertySet(true);
}


void CDrawingCompositionLine::_CopyParm(const CDrawingCompositionLine& Obj)
{
    m_Fill   = Obj.m_Fill;
    //m_Pt     = Obj.m_Pt;
    m_iWidth = Obj.m_iWidth;
    m_iLineType = Obj.m_iLineType;
    //m_mat2D     = Obj.m_mat2D;
}

/**
 * コピーコンストラクタ
 */
CDrawingCompositionLine::CDrawingCompositionLine(const CDrawingCompositionLine& Obj):
CDrawingObject( Obj)
{
    m_eType  = DT_COMPOSITION_LINE;

    SEGMENT segNew;
    m_lstSegments.clear();
    SetUsePropertySet(true);

    for (auto l: Obj.m_lstSegments)
    {
        SEGMENT_LIST list;
        list.eRegionType = l.eRegionType;
        for (auto seg: l.lstSegment)
        {
            segNew.pObj  = CDrawingObject::CloneShared(seg.pObj.get());
            //auto pThis =  shared_from_this();  
            //segNew.pObj->SetParentParts(pThis); コンストラクタでは設定できない
            list.lstSegment.push_back(segNew);

        }
        m_lstSegments.push_back(list);
    }

    _CopyParm(Obj);

    m_bCreatedPolygon= false;
}


/**
 *  @brief  デストラクタ.
 *  @param  なし 
 *  @retval なし 
 *  @note
 */
CDrawingCompositionLine::~CDrawingCompositionLine()
{
    m_lstSegments.clear();
}


void CDrawingCompositionLine::SetParentShared()
{

    for (auto l : m_lstSegments)
    {
        for (auto seg : l.lstSegment)
        {
            seg.pObj->SetParentParts(shared_from_this());
        }
    }
}

/**
 *  @brief  回転.
 *  @param  [in]    pPt2D    回転中心(絶対座標)
 *  @param  [in]    dAngle   (絶対)回転角(rad)
 *  @retval     なし
 *  @note
 */
void CDrawingCompositionLine::Rotate(const POINT2D& pt2D, double dAngle)
{
   MAT2D matRot;

    //matRot.Rotate(pt2D, dAngle);
    //m_mat2D = matRot;
    //Matrix(m_mat2D);
    std::vector<std::weak_ptr<CDrawingObject>> lst;
    GetConnectionObjectList(&lst);
    for(auto wObj: lst)
    {
        auto pObj = wObj.lock();
        if (pObj)
        {
            pObj->Rotate(pt2D, dAngle);
        }
    }

    for (auto l : m_lstSegments)
    {
        for (auto seg : l.lstSegment)
        {
            seg.pObj->Rotate(pt2D, dAngle);
        }
    }
}


/**
 *  @brief  回転.
 *  @param  [in]    pPt2D    回転中心(絶対座標)
 *  @param  [in]    dAngle   (絶対)回転角(rad)
 *  @retval     なし
 *  @note
 */
/*
void CDrawingCompositionLine::RotateAbs(const POINT2D& pt2D, double dAngle)
{

    m_mat2D.SetAffineAngle(dAngle);
   MatrixDirect(m_mat2D);
}
*/

/**
 *  @brief  相対移動.
 *  @param  [in]    pPt2D   移動量
 *  @retval         なし
 *  @note
 */
void CDrawingCompositionLine::Move(const POINT2D& pt2D) 
{
    MAT2D matMove;
    matMove.Move(pt2D);
    //Matrix(matMove);

    for (auto l : m_lstSegments)
    {
        for (auto seg : l.lstSegment)
        {
            seg.pObj->Matrix(matMove);
        }
    }
}


/**
 *  @brief  絶対移動.
 *  @param  [in]    pPt2D   移動量
 *  @retval         なし
 *  @note    
 */
/*
void CDrawingCompositionLine::AbsMove(const POINT2D& pt2D) 
{
    m_mat2D.SetAffinePos(pt2D);
}
*/

/**
 *  @brief  内部データ移動.
 *  @param  [in]    pPt2D   移動量
 *  @retval         なし
 *  @note
 */
void CDrawingCompositionLine::MoveInnerPosition(const POINT2D& pt2D) 
{
    for (auto l: m_lstSegments)
    {
        for (auto seg : l.lstSegment)
        {
            seg.pObj->Move(pt2D);
        }
    }
}

//!< 角度設定
/*
void CDrawingCompositionLine::SetAngle(const  double& dAngle)
{
    RotateAbs(m_Pt, dAngle);
}
*/

/**
 *  @brief  鏡像.
 *  @param  [in]    line   対称軸
 *  @retval         なし
 *  @note
 */
void CDrawingCompositionLine::Mirror(const LINE2D& line)
{
    MAT2D matMirror;
    matMirror.Mirror(line);

    for (auto l : m_lstSegments)
    {
        for (auto seg : l.lstSegment)
        {
            seg.pObj->Matrix(matMirror);
        }
    }

}

/**
 *  @brief  倍率.
 *  @param  [in]    pPt2D   拡大中心
 *  @param  [in]    dSclX   X倍率
 *  @param  [in]    dSclY   Y倍率
 *  @retval         なし
 *  @note
 */
void CDrawingCompositionLine::Scl(const POINT2D& pt2D, double dXScl, double dYScl)
{
    MAT2D matScl;

    matScl.Scl(pt2D, dXScl, dYScl);

    for (auto l : m_lstSegments)
    {
        for (auto seg : l.lstSegment)
        {
            seg.pObj->Matrix(matScl);
        }
    }
}
/**
 *  @brief  線幅設定.
 *  @param  [in]    iWidth   線幅
 *  @retval         なし
 *  @note
 */
void CDrawingCompositionLine::SetLineWidth(int iWidth)
{
    m_iWidth = iWidth;
}

/**
 *  @brief  線幅取得.
 *  @param          なし
 *  @retval         線幅
 *  @note
 */
int CDrawingCompositionLine::GetLineWidth() const
{
    return m_iWidth;
}

/**
 *  @brief  線種設定.
 *  @param  [in]    iType   線種
 *  @retval         なし
 *  @note
 */
void CDrawingCompositionLine::SetLineType(int iType)
{
    m_iLineType = iType;
}

//!< 
/**
 *  @brief  線種取得.
 *  @param          なし 
 *  @retval         線種
 *  @note
 */
int  CDrawingCompositionLine::GetLineType() const
{
    return m_iLineType;
}

/**
 *  @brief  行列.
 *  @param  [in]    mat2D   適用する行列
 *  @retval         なし
 *  @note
 */
void CDrawingCompositionLine::Matrix(const MAT2D& mat2D)
{
    for (auto l : m_lstSegments)
    {
        for (auto seg : l.lstSegment)
        {
            seg.pObj->Matrix(mat2D);
        }
    }
    AddChgCnt();
}

/**
 *  @brief  行列.
 *  @param  [in]    mat2D   適用する行列
 *  @retval         なし
 *  @note
 */
/*
void CDrawingCompositionLine::MatrixDirect(const MAT2D& mat2D)
{
    m_mat2D = mat2D;
    m_mat2D.GetAffineParam(&m_Pt, &m_dAngle, &m_dSclX, &m_dSclY, &m_dSkew);

    AddChgCnt();
}
*/

/**
*  @brief  点方向判定.
*  @param  [in]   pObj     線分
*  @param  [in]   ptDir    方向判定点(交点)
*  @param  [in]   bStart    
*  @retval        なし
*  @note         
*/
bool CDrawingCompositionLine::_IsDirPoint(const CDrawingObject* pObj, POINT2D& ptDir, bool bStart) const
{
    if (pObj->GetType() == DT_LINE)
    {              
        const CDrawingLine* pLine = dynamic_cast<const CDrawingLine*>(pObj);
        auto ptStart = pLine->GetStartSide();
        auto ptEnd = pLine->GetEndSide();
        double dAns;


        if(bStart)
        {
            dAns = CUtil::GetDirection(ptEnd,  ptStart, ptDir);
        }
        else
        {
            dAns = CUtil::GetDirection(ptStart,  ptEnd, ptDir);
        }
        return (dAns > 0);
    }
    else if (pObj->GetType() == DT_CIRCLE)
    {
        return true;
    }
    else if (pObj->GetType() == DT_ELLIPSE)
    {
        return true;
    }
    else if (pObj->GetType() == DT_SPLINE)
    {
        return true;
    }
    else if (pObj->GetType() == DT_COMPOSITION_LINE)
    {
        return true;
    }
    else
    {
        STD_ASSERT(_T("CDrawingCompositionLine::IsDirPoint Type error."));
        return false;
    }
    return true;
}

/**
 *  @brief  交点計算.
 *  @param  [out]   pList   交点のリスト
 *  @param  [in]    pObj    交差するオブジェクト
 *  @retval         なし
 *  @note           対象が点の場合は近接点を出力する
 */
void CDrawingCompositionLine::CalcIntersection(std::vector<POINT2D>* pList,
    const CDrawingObject* pObj, 
    bool bOnline,
    double dT) const
{
    std::vector<POINT2D> lstIntersection;
    bool bNear = false;
    double dMin = DBL_MAX;

    POINT2D ptNear;
    POINT2D ptAns;

    pList->clear();

    //レイヤーの確認
    if (!IsSameScale(pObj))
    {
        return;
    }

    const MAT2D* pMat = GetParentMatrix();

    if (pObj->GetType() == DT_POINT)
    {
        bNear = true;
        const CDrawingPoint* pPoint = dynamic_cast<const CDrawingPoint*>(pObj);
        if (!pPoint)
        {
            return;
        }
        ptNear = *pPoint->GetPointInstance();
    }
    else if (pObj->GetType() == DT_NODE)
    {
        bNear = true;
        const CDrawingNode* pPoint = dynamic_cast<const CDrawingNode*>(pObj);
        if (!pPoint)
        {
            return;
        }
        ptNear = *pPoint->GetPointInstance();
    }

    if(bNear)
    {
        const MAT2D* pMat2 =  pObj->GetParentMatrix();
        if(pMat2)
        {
            ptNear.Matrix(*pMat2);
        }
    }

    for (auto l : m_lstSegments)
    {
        int segmetIndex = 0;
        int lastSegmentIndex = SizeToInt(l.lstSegment.size());
        if (lastSegmentIndex != 0)
        {
            lastSegmentIndex--;
        }

        for (auto seg : l.lstSegment)
        {
            lstIntersection.clear();

            bool bSegOnline = true;
            bool bStart = false;
            bool bEnd   = false;
            //bOnlineの設定は最初と最後のセグメントのみ有効
            if (segmetIndex == 0) 
            {
                bSegOnline = bOnline;
                bStart = true;
            }
            else if(segmetIndex == lastSegmentIndex)
            {
                bSegOnline = bOnline;
                bEnd = true;
            }

            if (bStart || bEnd)
            {
                std::vector<POINT2D> lstTmpIntersection;
                seg.pObj->CalcIntersection(&lstTmpIntersection, pObj, bSegOnline, dT);
                for (auto ptSection : lstTmpIntersection)
                {
                    if (_IsDirPoint(seg.pObj.get(), ptSection, bStart))
                    {
                        lstIntersection.push_back(ptSection);
                    }
                }
            }
            else
            {
                seg.pObj->CalcIntersection(&lstIntersection, pObj, bSegOnline, dT);
            }


            if (!bNear)
            {
                for(POINT2D & pt: lstIntersection)
                {
                    pList->push_back(pt);
                }
            }
            else
            {
                double dLen;
                for(POINT2D & pt: lstIntersection)
                {
                    dLen = ptNear.Distance(pt);
                    if (dMin > dLen)
                    {
                        ptAns = pt;
                        dMin = dLen;
                    }
                }
            }
            segmetIndex++;
        }
    }

    if (bNear)
    {
        pList->push_back(ptAns);
    }
}

/**
 *  @brief  位置調整.
 *  @param  [in]    pPtIntersect   調整位置
 *  @param  [in]    pPtClick       調整方向指定点
 *  @retval         なし
 *  @note
 */
bool CDrawingCompositionLine::Trim (const POINT2D& ptIntersect, 
                                    const POINT2D& ptClick,
                                    bool bReverse)
{
    //pPtClick側を調整する

    int iI;
    int iLoop;
    POINT2D pt;
    E_NEAR eRet = _NearPointSegment(&iLoop, &iI, &pt, ptIntersect); 

    if(eRet !=  E_ON_SECMENT)
    {
        return false;
    }

    int iIc;
    POINT2D ptC;
    E_NEAR eRetClick = _NearPointSegment(&iLoop, &iIc, &ptC, ptClick); 

    double dTotal  = Length(GetEndSide(), true);
    double dClick = Length(ptClick, true);

    bool bStart = true;
    if (dClick > (dTotal / 2.0) )
    {
        bStart = false;
    }

    if(bReverse)
    {
        bStart = (!bStart);
    }

    std::deque<SEGMENT>* pSeg = &m_lstSegments[iLoop].lstSegment;

    /*
    if(eRet == E_ON_NODE)
    {
        ここから
        //セグメント上にある場合
        POINT2D ptEdge;
        if(bStart)     
        {
            ptEdge = pSeg->begin()->pObj->GetStartSide();
            pSeg->erase(pSeg->begin(),      pSeg->begin() + iI);
        }
        else
        {
            ptEdge = (pSeg->end()-1)->pObj->GetStartSide();
            pSeg->erase(pSeg->begin() + iI, pSeg->end()); 
        }

        if (pSeg->empty())
        {
            
        }


        STD_ASSERT(!pSeg->empty());

        return true;
    }
    else 
    */
    {
        POINT2D ptClick2;
        if(bStart)
        {
            ptClick2 = pSeg->at(iI).pObj->GetStartSide();
        }
        else
        {
            ptClick2 = pSeg->at(iI).pObj->GetEndSide();
        }

        pSeg->at(iI).pObj->Trim(pt, ptClick2, bReverse);

        if(bStart)     { pSeg->erase(pSeg->begin(),      pSeg->begin() + iI);}
        else           { pSeg->erase(pSeg->begin() + iI + 1, pSeg->end()); }

        STD_ASSERT(!pSeg->empty());

        return true;
    }
    return false;
}


/**
 *  @brief  位置調整.
 *  @param  [in]    pPtIntersect   調整位置
 *  @param  [in]    pPtClick       調整方向指定点
 *  @retval         なし
 *  @note
 */
bool CDrawingCompositionLine::TrimCorner (const POINT2D& ptIntersect, const POINT2D& ptClick)
{
    int iI;
    POINT2D pt;
    E_NEAR eRet = _NearPointSegment(NULL, &iI, &pt, ptIntersect); 

    if(eRet !=  E_ON_SECMENT)
    {
        return false;
    }


    int iIClick;
    POINT2D ptSel;
    eRet = _NearPointSegment(NULL, &iIClick, &ptSel, ptClick); 

    if(eRet !=  E_ON_SECMENT)
    {
        return false;
    }

    auto lstSeg = m_lstSegments[0].lstSegment;
    auto segObj = lstSeg[iI].pObj;
    POINT2D ptSegStart = segObj->GetStartSide();
    POINT2D ptSegEnd   = segObj->GetEndSide();

    segObj->TrimCorner(ptIntersect, ptSel);

    bool bStart = true;
    if(iIClick < iI)
    {
        ptSel = ptSegStart;
    }
    else if (iIClick > iI)
    {
        ptSel = ptSegEnd;
        bStart = false;
    }
    else
    {
        if (segObj->GetEndSide() == ptSegEnd)
        {
            bStart = false;
        }
    }


    if(bStart)     { lstSeg.erase(lstSeg.begin(),      lstSeg.begin() + iI);}
    else           { lstSeg.erase(lstSeg.begin() + iI, lstSeg.end()); }

    STD_ASSERT(!lstSeg.empty());

    return true;
}

/**
 *  @brief  近接点取得.
 *  @param  [out]   pIndex  セグメント位置
 *  @param  [out]   pNearPoint  近接点
 *  @param  [in]    ptSel       選択点
 *  @retval         E_ON_SECMENT   近接点がセグメント上にある
 *                  E_ON_NODE      近接点がセグメントの開始点
 *                  E_ON_END_NODE  近接点がセグメントの終了点
 *                  E_NO_ANS       近接点がない    
 * 
 *  @note  
 */
CDrawingCompositionLine::E_NEAR CDrawingCompositionLine::_NearPointSegment(
                                                 int* pLoopIndex,
                                                 int* pIndex, 
                                                 POINT2D* pNearPoint, 
                                                 const POINT2D& ptSel) const
{
    double dMin = DBL_MAX;
    POINT2D pt;
    POINT2D ptMin;

    int iCnt = 0;
    int iLoopCnt = 0;
    int iIndex = -1;
    int iLoop  = -1;

    for (auto l : m_lstSegments)
    {
        for (auto seg : l.lstSegment)
        {
            if (seg.pObj->NearPoint(&pt, ptSel, true))
            {
                double dLen = ptSel.Distance(pt);

                if (dLen < dMin)
                {
                    dMin = dLen;
                    ptMin = pt;
                    iIndex = iCnt;
                    iLoop = iLoopCnt;
                }
            }
            iCnt++;
        }
        iLoopCnt++;

        if (!pLoopIndex)
        {
            //pLoopIndexを設定していないばあは最初のLoopのみ
            break;
        }
    }
    STD_ASSERT(iIndex != -1);
    
    if((iIndex != -1) &&  (iLoop != -1))
    {
        //分割点がセグメント間の場合
        if (ptMin == m_lstSegments[iLoop].lstSegment[iIndex].pObj->GetStartSide())
        {

            if(iIndex >=  m_lstSegments[iLoop].lstSegment.size())
            {
                if(pLoopIndex)
                {
                    *pLoopIndex = iLoop;
                }
                *pIndex = iIndex;
                return E_ON_END_NODE;
            }

            if(pLoopIndex)
            {
                *pLoopIndex = iLoop;
            }
            *pIndex = iIndex;
            *pNearPoint = ptMin;
            return E_ON_NODE;

        }
        else if (ptMin == m_lstSegments[iLoop].lstSegment[iIndex].pObj->GetEndSide())
        {
            if(pLoopIndex)
            {
                *pLoopIndex = iLoop;
            }
            *pNearPoint = ptMin;
            if(iIndex ==  m_lstSegments[iLoop].lstSegment.size())
            {
                *pIndex = iIndex;
                return E_ON_END_NODE;

            }
            else
            {
                *pIndex = iIndex + 1;
                return E_ON_NODE;
            }
        }

        *pNearPoint = ptMin;
        *pIndex = iIndex;

        if(iIndex >=  m_lstSegments[iLoop].lstSegment.size())
        {
            return E_ON_END_NODE;
        }

        return E_ON_SECMENT;
    }
   
   if (pLoopIndex)
   {
        *pLoopIndex = -1;
   }
    *pIndex = -1;
    return E_NO_ANS;
}


CDrawingCompositionLine::E_NEAR  CDrawingCompositionLine::_NearPointSegment2(
    int* pIndex, 
    POINT2D* pNearPoint, 
    int  iLoopIndex,
    const POINT2D& ptSel) const
{
    double dMin = DBL_MAX;
    POINT2D pt;
    POINT2D ptMin;

    int iCnt = 0;
    int iLoopCnt = 0;
    int iIndex = -1;

    if (iLoopIndex >=  m_lstSegments.size())
    {
        return E_NO_ANS;
    }

    auto l  = m_lstSegments[iLoopIndex];
    {
        for (auto seg : l.lstSegment)
        {
            if (seg.pObj->NearPoint(&pt, ptSel, true))
            {
                double dLen = ptSel.Distance(pt);

                if (dLen < dMin)
                {
                    dMin = dLen;
                    ptMin = pt;
                    iIndex = iCnt;
                }
            }
            iCnt++;
        }
    }

    STD_ASSERT(iIndex != -1);
    auto seg = m_lstSegments[iLoopIndex].lstSegment;
    int iLastIndex = SizeToInt(seg.size()) - 1;

    if(iIndex != -1) 
    {
        //分割点がセグメント間の場合
        if (ptMin == seg.at(iIndex).pObj->GetStartSide())
        {

            if(iIndex >=  m_lstSegments[iLoopIndex].lstSegment.size())
            {
                *pIndex = iIndex;
                return E_ON_END_NODE;
            }

            *pIndex = iIndex;
            *pNearPoint = ptMin;
            return E_ON_NODE;

        }
        else if (ptMin == seg.at(iIndex).pObj->GetEndSide())
        {
            *pNearPoint = ptMin;
            if(iIndex ==  m_lstSegments[iLoopIndex].lstSegment.size())
            {
                *pIndex = iIndex;
                return E_ON_END_NODE;

            }
            else
            {
                *pIndex = iIndex + 1;
                return E_ON_NODE;
            }
        }

        *pNearPoint = ptMin;
        *pIndex = iIndex;

        if(iIndex >=  m_lstSegments[iLoopIndex].lstSegment.size())
        {
            return E_ON_END_NODE;
        }

        return E_ON_SECMENT;
    }

    *pIndex = -1;
    return E_NO_ANS;
}

/**
 *  @brief  分割.
 *  @param  [in]    pPtBreak  分割位置
 *  @retval         分割によって出来た複合線
 *  @note
 */
std::shared_ptr<CDrawingObject>  CDrawingCompositionLine::Break (const POINT2D& ptBreak)
{
    int iI;
    int iLoop;
    POINT2D pt;
    E_NEAR eRet = _NearPointSegment(&iLoop, &iI, &pt, ptBreak); 

    if(eRet ==  E_NO_ANS)
    {
        return NULL;
    }

    else if( eRet ==  E_ON_END_NODE)
    {
        return NULL;
    }


    std::deque<SEGMENT>* pSeg = &m_lstSegments[iLoop].lstSegment;

    auto pRet =  CDrawingObject::CloneShared(this);
    auto pCompo =  std::dynamic_pointer_cast<CDrawingCompositionLine>(pRet);
    auto pNewSeg = &(pCompo->m_lstSegments[iLoop].lstSegment);
    
    if(eRet == E_ON_NODE)
    {
        pSeg->erase(pSeg->begin(),      pSeg->begin() + iI);
        pNewSeg->erase(pNewSeg->begin() + iI, pNewSeg->end()); 
    }
    else 
    {
        pSeg->at(iI).pObj = pNewSeg->at(iI).pObj->Break(pt);
        pSeg->erase(pSeg->begin(),      pSeg->begin() + iI);
        pNewSeg->erase(pNewSeg->begin() + iI + 1, pNewSeg->end());
    }
    return pCompo;
}

POINT2D CDrawingCompositionLine::_Draw(CDrawingView* pView,
     SEGMENT_LIST& l, 
     int iLayerId)
{

    RECT2D viewRect = pView->GetViewArea(iLayerId);
    POINT   ptView;
    POINT   ptViewStart;
    POINT2D ptStart;
    POINT2D ptCurrent;

    try
    {
    ptStart = l.lstSegment[0].pObj->GetStartSide();
    ptStart.Matrix(m_matDraw);
    }
    catch(...)
    {

    }


    pView->ConvWorld2Scr(&ptViewStart, ptStart, iLayerId);
    pView->MoveTo(ptViewStart);

#ifdef _CANVAS_ADD_
    TRACE(_T("MoveTo,%f,%f %x\n"),ptStart.dX,ptStart.dY);
#endif
    ptCurrent = ptStart; 

    for (auto seg : l.lstSegment)
    {
        switch (seg.pObj->GetType())
        {
        case DT_CIRCLE:
        {
            CDrawingCircle* pCircle = reinterpret_cast<CDrawingCircle*>(seg.pObj.get());
            CIRCLE2D tmpCircle(*pCircle->GetCircleInstance());
            ELLIPSE2D* pEllipse = NULL;

            tmpCircle.Matrix(m_matDraw, &pEllipse);
            if (pEllipse != NULL)
            {
                //楕円を描画
                std::vector<POINT2D> lstPoint;
                double dDotLength = pView->DotToLength(1);
                pEllipse->DivideLine(&lstPoint, viewRect, dDotLength);
                LineMulti(pView, iLayerId, &lstPoint, true);
                delete pEllipse;
                if (!lstPoint.empty())
                {
                    ptCurrent =  lstPoint[lstPoint.size()-1];
                }
            }
            else
            {
                RECT   rcEllipse;
                POINT  PtStart;
                POINT  PtEnd;
                GetCircleRect(&rcEllipse, iLayerId, &PtStart, &PtEnd, pView, tmpCircle);
                pView->ArcTo(rcEllipse, PtStart, PtEnd, tmpCircle.IsClockwise());
            }
            break;
        }
        case DT_LINE:
        {
            CDrawingLine* pLine = reinterpret_cast<CDrawingLine*>(seg.pObj.get());

            //OFFSETした場合前の図形の終点と始点が綱からない場合がある。
            if(  pLine->GetStartSide().Distance(ptCurrent) > NEAR_ZERO )
            {
                POINT2D ptStart;
                ptStart = pLine->GetStartSide();
                ptStart.Matrix(m_matDraw);
                pView->ConvWorld2Scr(&ptView, ptStart, iLayerId);
                pView->LineTo(ptView);
            }

            POINT2D ptEnd;
            ptEnd = pLine->GetEndSide();
            ptEnd.Matrix(m_matDraw);
            pView->ConvWorld2Scr(&ptView, ptEnd, iLayerId);
            pView->LineTo(ptView);
#ifdef _CANVAS_ADD_
            TRACE(_T("LineTo,%f,%f \n"),ptEnd.dX, ptEnd.dY);
#endif

            ptCurrent = ptEnd; 
            break;
        }

        case DT_ELLIPSE:
        {
            std::vector<POINT2D> lstPoint;
            CDrawingEllipse* pEllipse = reinterpret_cast<CDrawingEllipse*>(seg.pObj.get());
            ELLIPSE2D tmpEllipse(*pEllipse->GetEllipseInstance());
            tmpEllipse.Matrix(m_matDraw);
            double dDotLength = pView->DotToLength(1);
            tmpEllipse.DivideLine(&lstPoint, viewRect, dDotLength);
            LineMulti(pView, iLayerId, &lstPoint, true);

#ifdef _CANVAS_ADD_
            auto ell = std::dynamic_pointer_cast<CDrawingEllipse>(seg.pObj);
            DB_PRINT(_T("tmpEllipse seg IsClickwise %s \n"), (tmpEllipse.IsClockwise()?_T("TRUE"):_T("FALSE")));

            for(auto pt: lstPoint)
            {
                TRACE(_T("LineMulti,%f,%f \n"),pt.dX,pt.dY);
            }
#endif

            if (!lstPoint.empty())
            {
                ptCurrent = lstPoint[lstPoint.size()-1]; 
            }

            break;
        }
        case DT_SPLINE:
        {
            std::vector<POINT2D> lstPoint;
            CDrawingSpline* pSpline = reinterpret_cast<CDrawingSpline*>(seg.pObj.get());
            SPLINE2D tmpSpline(*pSpline->GetSplineInstance());
            tmpSpline.Matrix(m_matDraw);

            double dDotLength = pView->DotToLength(1);
            tmpSpline.DivideLine(&lstPoint, viewRect, dDotLength);
            LineMulti(pView, iLayerId, &lstPoint, true);
            if (!lstPoint.empty())
            {
                ptCurrent = lstPoint[lstPoint.size()-1]; 
            }
            break;
        }
        case DT_COMPOSITION_LINE:
        {
            auto pCompo = reinterpret_cast<CDrawingCompositionLine*>(seg.pObj.get());
            
            for (auto lCompo : pCompo->m_lstSegments)
            {
                ptCurrent = _Draw(pView, lCompo, iLayerId);
            }
            break;
        }
        default:
            break;
        }
    }

#ifdef _CANVAS_ADD_
        TRACE(_T("\n"));
#endif
    return ptCurrent;
}


bool CDrawingCompositionLine::IsClockwise() const
{
    
    if(m_lstSegments.empty())
    {
         return true;
    }

    size_t nSegment = m_lstSegments[0].lstSegment.size();

    if(nSegment == 0)
    {
         return true;
    }

    if(!_IsClose(0))
    {
        return true;
    }
    
    bool bClockwise = true;
    if(nSegment == 1)
    {
         auto pObj = m_lstSegments[0].lstSegment[0].pObj.get();
         if(!IsCloseObject(pObj))
         {
            return true;
         }

         DRAWING_TYPE  eDwType = pObj->GetType();
         if (eDwType == DT_COMPOSITION_LINE)
         {
             const CDrawingCompositionLine* pLoop = dynamic_cast<const CDrawingCompositionLine*>(pObj);
             bClockwise = pLoop->IsClockwise();
         }
         else if (eDwType == DT_CIRCLE)
         {
             const CDrawingCircle* pLoop = dynamic_cast<const CDrawingCircle*>(pObj);
             bClockwise = pLoop->GetCircleInstance()->IsClockwise();
         }
         else if (eDwType != DT_ELLIPSE)
         {
             const CDrawingEllipse* pLoop = dynamic_cast<const CDrawingEllipse*>(pObj);
             bClockwise = pLoop->GetEllipseInstance()->IsClockwise();
         }
         else if (eDwType == DT_SPLINE)
         {
             const CDrawingSpline* pLoop = dynamic_cast<const CDrawingSpline*>(pObj);
             bClockwise = pLoop->GetSplineInstance()->IsClockwise();
         }
         return bClockwise;
    }

    POINT2D ptStart = m_lstSegments[0].lstSegment[0].pObj->GetStartSide();
    POINT2D ptOld = ptStart;
    double dS = 0;
    for(auto seg: m_lstSegments[0].lstSegment)
    {
        POINT2D pt = seg.pObj->GetEndSide();
        dS += ptOld.Cross(pt);
        ptOld = pt;
    }
    dS += ptOld.Cross(ptStart);

    return (dS >= 0);
}

/**
 *  @brief  描画.
 *  @param  [in]  pView       表示View
 *  @param  [in]  bMouseOver  true オブジェクト上にマウスあり
 *  @retval         なし
 *  @note
 */
void CDrawingCompositionLine::Draw(CDrawingView* pView, 
                                    const std::shared_ptr<CDrawingObject> pParentObject,
                                    E_MOUSE_OVER_TYPE eMouseOver,
                                    int iLayerId)
{
    //======================
    // 変換マトリックス生成
    //======================
    bool bParent = false;
    if (pParentObject)
    {
        const MAT2D* pMat = pParentObject->GetDrawingMatrix();
        if (pMat)
        {
            m_matDraw = /*m_mat2D **/ (*pMat);
            bParent = true;
        }
    }

    if(!bParent)
    {
        /*m_matDraw = m_mat2D*/;

        m_matDraw.SetIdentity();
    }

    if (m_pMatOffset)
    {
        m_matDraw = m_matDraw * (*m_pMatOffset);
    }

    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }

    int iWidth = m_iWidth;
    bool bFirst = true;

	bool bIgnoreSelect = false;
	COLORREF crPen;
	int      iId;
	crPen = GetDrawColor(pView, iLayerId, pParentObject.get(), &iId, bIgnoreSelect);

    if (eMouseOver == E_MO_SELECTABLE)
    {
        pView->SetLineAlternateMode(true);
        iWidth += 2;
        //Group化で選択したときこのオブジェクトのみ選択色ならない
        //bIgnoreSelect = true;
    }
	else if (eMouseOver == E_MO_EMPHASIS)
	{
		crPen = DRAW_CONFIG->crEmphasis;
	}
	else if (eMouseOver == E_MO_CONNECTABLE)
	{
		crPen = DRAW_CONFIG->crConnectable;
	}

    pView->SetPen(m_iLineType, iWidth, crPen, m_nId); //個々の図形のIDを使用

    if (m_lstSegments.size()==0)
    {
        return;
    }


    int iLoop = 1;

    if (m_Fill.eFillType !=  FIT_NONE)
    {
        pView->BeginPath();
        iLoop = 2;
    }

    COLORREF crFill;
    CPartsDef* pCtrl = pView->GetPartsDef();
    CLayer* pLayer   = pCtrl->GetLayer(iLayerId);

    if (pLayer->bUseLayerColor)
    {
        crFill = pLayer->crLayer;
    }
    else
    {
        crFill = m_Fill.cr1;
    }


    for (int iCnt = 0; iCnt < iLoop; iCnt++)
    {
        if (iLoop == 2)
        {
            //塗りつぶし用の線を背景色で書いてから
            //塗りつぶした後 点線を書く
            if (iCnt == 0)
            {
                COLORREF crBack;
                crBack = DRAW_CONFIG->GetBackColor();
                pView->SetPen(PS_SOLID, 1, crBack, m_nId);
            }
            else
            {
                pView->SetPen(m_iLineType, iWidth, crPen, m_nId);
            }
        }


        int iLoopCnt = 0;
        for (auto l : m_lstSegments)
        {
            _Draw(pView, l, iLayerId);

            iLoopCnt++;
            if (iCnt == 0)
            {
                if (/*m_bClose &&*/ (m_Fill.eFillType != FIT_NONE))
                {
                    if (m_lstSegments.size() == iLoopCnt)
                    {
                        pView->CloseFigure();
                        pView->EndPath();

                        if (m_Fill.eFillType == FIT_SOLID)
                        {
                            pView->FillPath(crFill, m_nId);
                        }
                        else if (m_Fill.eFillType == FIT_HATCHING)
                        {
                            pView->HatchingPath(m_Fill.cr1, crPen, m_nId);
                        }
                    }
                }
            }
        }
    }

	if (eMouseOver == E_MO_SELECTABLE)
	{
		pView->SetLineAlternateMode(false);
	}


//ForTest---
    /*
    int iPolygonNum = GetNumberOfPolygon();
    POINT2D pt2d[3];
    POINT   pt[3];
    for (int iNo = 0; iNo < iPolygonNum; iNo++)
    {
        GetPolygonVertex(iNo, &pt2d[0], &pt2d[1], &pt2d[2]);

        for(int iVertex = 0; iVertex < 3; iVertex++)
        {
            pt2d[iVertex].Matrix(m_matDraw);
            pView->ConvWorld2Scr(&pt[iVertex], pt2d[iVertex], m_iLayerId);
        }
         pView->MoveTo(pt[0]);
         pView->LineTo(pt[1]);
         pView->LineTo(pt[2]);
         pView->LineTo(pt[0]);
    }
    */
//ForTest---

}

//!< 近接点取得
bool CDrawingCompositionLine::NearPoint(POINT2D* ptRet, const POINT2D& ptNear, bool bOnLine) const
{
    double dMin = DBL_MAX;
    POINT2D pt;
    POINT2D ptMin;
    bool bAns = false;
    for (auto l: m_lstSegments)
    {
        for (auto seg: l.lstSegment)
        {
            if(!seg.pObj->NearPoint(&pt, ptNear, bOnLine))
            {
                continue;
            }

            double dLen =  ptNear.Distance(pt);

            if(dLen < dMin)
            {
                ptMin = pt;
                dMin =  dLen;
                bAns = true;
            }
        }
    }
    
    if(bAns)
    {
        *ptRet = ptMin;
        return true;
    }
    return false;
}

/**
 *  @brief  描画削除.
 *  @param  [in]    pView   表示View
 *  @retval         なし
 *  @note
 */
void CDrawingCompositionLine::DeleteView(CDrawingView* pView, 
                                        const std::shared_ptr<CDrawingObject> pParentObject,
                                        int iLayerId)
{
    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }

    for (auto l: m_lstSegments)
    {
        for (const SEGMENT& seg: l.lstSegment)
        {
            seg.pObj->DeleteView(pView, pParentObject, iLayerId);
        }
    }
}


/**
 *  @brief   描画(マウスオーバー時).
 *  @param   [in] pView 表示View
 *  @param   [in] bOver true:開始 false:終了
 *  @retval  なし
 *  @note
 */
void CDrawingCompositionLine::DrawOver( CDrawingView* pView, bool bOver)
{
}


/**
 *  @brief   プロパティ初期化.
 *  @param   [in] pCtrl オブジェクトコントロールへのポインタ
 *  @retval     なし
 *  @note
 */
TREE_GRID_ITEM*  CDrawingCompositionLine::_CreateStdPropertyTree()
{
    NAME_TREE_ITEM<CStdPropertyItem>* pBeforeGroup;
    NAME_TREE_ITEM<CStdPropertyItem>* pNextItem;
    NAME_TREE_ITEM<CStdPropertyItem>* pGroupItem;
    NAME_TREE<CStdPropertyItem>* pTree;
    //int iId;
    CStdPropertyItem* pItem;
    boost::tuple< CStdPropertyItem*, void*> pairVal;


    pBeforeGroup = CDrawingObject::_CreateStdPropertyTree();
    pTree = m_psProperty->GetTreeInstance();

    pGroupItem  = pTree->AddNext(pBeforeGroup, NULL, GET_STR(STR_PRO_COMPOSITION_LINE), _T("Composition"));


    //------------
    //基準点
    //------------
    /*
    CStdPropertyItemDef DefCenter(PROP_POINT2D, 
        GET_STR(STR_PRO_DATUM)   , 
        _T("Datum"),
        GET_STR(STR_PRO_INFO_DATUM), 
        true,
        DISP_UNIT,
        POINT2D());

    pItem = new CStdPropertyItem( DefCenter, PropCenter, m_psProperty, NULL, &m_Pt);
    pNextItem = pTree->AddChild(pGroupItem, pItem, DefCenter.strDspName, pItem->GetName(), 2);

    //------------
    //角度(deg)
    //------------
    CStdPropertyItemDef DefAngle(PROP_DOUBLE, 
        GET_STR(STR_PRO_ANGLE)   , 
        _T("Angle"),
        GET_STR(STR_PRO_INFO_ANGLE), 
        true,
        DISP_UNIT,
        0.0);

    pItem = new CStdPropertyItem( DefAngle, PropAngle, m_psProperty, NULL, &m_dAngle);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefAngle.strDspName, pItem->GetName());
    */

#ifdef _DEBUG //Clockwise
    DB_PRINT(_T(" Clockwise %s \n"), (IsClockwise()? _T("true"): _T("false"))); 
#endif

    //------------
    //線幅
    //------------
    CStdPropertyItemDef DefLineWidth(PROP_LINE_WIDTH, 
        GET_STR(STR_PRO_LINE_WIDTH)   , 
        _T("Width"), 
        GET_STR(STR_PRO_INFO_LINE_WIDTH), 
        true,
        DISP_NONE,
        0);

    pItem = new CStdPropertyItem(DefLineWidth, PropLineWidth, m_psProperty, NULL, &m_iWidth);
    pNextItem = pTree->AddChild(pGroupItem, pItem, DefLineWidth.strDspName, pItem->GetName());

    //------------
    //線種
    //------------
    CStdPropertyItemDef DefLineType(PROP_LINE_TYPE, 
        GET_STR(STR_PRO_LINE_TYPE)   , 
        _T("Type"),
        GET_STR(STR_PRO_INFO_LINE_TYPE), 
        false,
        DISP_NONE,
        0);

    pItem = new CStdPropertyItem( DefLineType, PropLineType, m_psProperty, NULL, &m_iLineType);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefLineType.strDspName, pItem->GetName());
    //if (m_bClose)
    {

        //--------------------
        // 塗りつぶし
        //--------------------
        pNextItem = m_Fill.CreateStdPropertyTree(m_psProperty, pTree, pNextItem);

    }

    return pGroupItem;
}


/**
 *  @brief   プロパティ変更(中心点)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
/*
bool CDrawingCompositionLine::PropCenter   (CStdPropertyItem* pData, void* pObj)
{
    CDrawingCompositionLine* pDrawing = reinterpret_cast<CDrawingCompositionLine*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->AbsMove(pData->anyData.GetPoint());
        pDrawing->m_pCtrl->Draw(pDrawing->m_nId);

    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}
*/
/**
 *  @brief   プロパティ変更(角度)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
/*
bool CDrawingCompositionLine::PropAngle   (CStdPropertyItem* pData, void* pObj)
{
    CDrawingCompositionLine* pDrawing = reinterpret_cast<CDrawingCompositionLine*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->SetAngle(pData->anyData.GetDouble());
        pDrawing->m_pCtrl->Draw(pDrawing->m_nId);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}
*/

/**
 *  @brief   プロパティ変更(線幅)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingCompositionLine::PropLineWidth    (CStdPropertyItem* pData, void* pObj)
{
    CDrawingCircle* pDrawing = reinterpret_cast<CDrawingCircle*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->SetLineWidth(pData->anyData.GetInt());
        pDrawing->GetPartsDef()->Redraw();
   }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(線種)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingCompositionLine::PropLineType (CStdPropertyItem* pData, void* pObj)
{
    CDrawingCircle* pDrawing = reinterpret_cast<CDrawingCircle*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->SetLineType(pData->anyData.GetInt());
        pDrawing->GetPartsDef()->Redraw();
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(閉じる)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
/*
bool CDrawingCompositionLine::PropClose         (CStdPropertyItem* pData, void* pObj)
{
    CDrawingCompositionLine* pDrawing = reinterpret_cast<CDrawingCompositionLine*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}
*/

//!< 範囲
CDrawingObject::RELATION CDrawingCompositionLine::RelationRect(const RECT2D& rcArea) const
{
    bool bFirst = true;
    RELATION rel = REL_NONE;

    for (auto l: m_lstSegments)
    {
        for (auto seg: l.lstSegment)
        {
            auto ret = seg.pObj->RelationRect(rcArea);
            if ((ret & REL_PART) == REL_PART)
            {
                return REL_PART;
            }
            else if ((ret & REL_INNER) == REL_INNER)
            {
                if (bFirst)
                {
                    rel = REL_INNER;
                    bFirst = false;
                }
                else
                {
                    if (rel != REL_INNER)
                    {
                        return REL_PART;
                    }
                }
            }
            else if (ret == REL_NONE)
            {
                if (bFirst)
                {
                    bFirst = false;
                }
                else
                {
                    if (rel == REL_INNER)
                    {
                        return REL_PART;
                    }
                }
            }
        }
    }
    return rel;
}


/**
 *  @brief  範囲.
 *  @param  [in] p1 1点目
 *  @param  [in] p2 2点目
 *  @retval  true:2点で示される矩形内に点が存在する 
 *  @note
 */
bool CDrawingCompositionLine::IsInner( const RECT2D& rcArea, bool bPart) const
{

    if(bPart)
    {
        for (auto l: m_lstSegments)
        {
            for (auto seg: l.lstSegment)
            {
                if(seg.pObj->IsInner(rcArea, bPart))
                {
                    return true;
                }
            }
        }
        return false;
    }
    else
    {
        for (auto l: m_lstSegments)
        {
            for (auto seg: l.lstSegment)
            {
                if(!seg.pObj->IsInner(rcArea, bPart))
                {  
                    return false;
                }
            }
        }
        return true;
    }
    return false;
}


/**
 *  @brief   参照データに基づいて更新.
 *  @param   
 *  @retval  
 *  @note
 */
CDrawingObject* CDrawingCompositionLine::UpdateRef()
{
    CDrawingObject*      pRef;

    pRef = CDrawingObject::UpdateRef();

    if (pRef == NULL)
    {
        return NULL;
    }

    CDrawingCompositionLine* pLoop = dynamic_cast<CDrawingCompositionLine*>(pRef);

    STD_ASSERT(pLoop != NULL);

    if (pLoop == NULL)
    {
        return false;
    }

    //TODO: 実装

    m_iChgCnt = pRef->GetChgCnt();
    return pRef;
}

/**
 *  @brief   スナップ点取得.
 *  @param   [out] pLstSnap スナップ点リスト
 *  @retval  true: スナップ点あり
 *  @note
 */
bool CDrawingCompositionLine::GetSnapList(std::list<SNAP_DATA>* pLstSnap)const 
{
    using namespace VIEW_COMMON;

    DWORD dwSnap = DRAW_CONFIG->GetSnapType();
    bool bRet = false;

    STD_ASSERT(pLstSnap);

    if (!pLstSnap)
    {
        return false;
    }

    pLstSnap->clear();

    int iLoopIndex= 0;
    int iIndex = 0;

    for (auto l: m_lstSegments)
    {
        size_t iMax = l.lstSegment.size();
        if (iMax == 0)
        {
            continue;
        }

        POINT2D pt;
        SNAP_DATA snapData;
        snapData.bKeepSelect = false;
        if ((dwSnap & SNP_END_POINT) &&
            !(dwSnap &SNP_FEATURE_POINT))
        {
            if (!IsClose())
            {
                snapData.eSnapType = SNP_END_POINT;
                pt =  GetPoint1();
                //pt.Matrix(m_mat2D);
                snapData.pt = pt;
                pLstSnap->push_back(snapData);

                pt =  GetPoint2();
                //pt.Matrix(m_mat2D);
                snapData.pt = pt;
                snapData.iLoopIndex = iLoopIndex;

                pLstSnap->push_back(snapData);

                return true;
            }
            return false;
        }

        if (dwSnap &SNP_FEATURE_POINT)
        {
            size_t iCnt = 0;
            snapData.eSnapType = SNP_FEATURE_POINT;
            for(iCnt = 0; iCnt < iMax; iCnt++)
            {
                pt =  l.lstSegment[iCnt].pObj->GetStartSide();
                //pt.Matrix(m_mat2D);
                snapData.pt = pt;

                pLstSnap->push_back(snapData);
            }

            if (!IsClose())
            {
                pt =  l.lstSegment[iMax - 1].pObj->GetEndSide();
                //pt.Matrix(m_mat2D);
                snapData.pt = pt;

                pLstSnap->push_back(snapData);
                if(dwSnap & SNP_END_POINT)
                {
                    std::list<SNAP_DATA>::iterator ite;
                    pLstSnap->begin()->eSnapType = SNP_END_POINT;
                    ite = pLstSnap->end();
                    ite--;
                    ite->eSnapType = SNP_END_POINT;
                }
            }
            return true;
        }
        iLoopIndex++;
    }
    return false;
}

/**
 * @brief   領域取得
 * @param   なし
 * @retval  なし
 * @note	描画領域を取得する
 */
RECT2D CDrawingCompositionLine::GetBounds() const
{
    RECT2D rc;

    bool bFirst = true;
    for (auto l: m_lstSegments)
    {
        for (auto seg: l.lstSegment)
        {
            if (bFirst)
            {
                rc = seg.pObj->GetBounds();
                bFirst = false;
            }
            else
            {
                rc.ExpandArea(seg.pObj->GetBounds());
            }
        }
    }

    //rc.Matrix(m_mat2D);
    return rc;
}


/**
 * @brief   距離
 * @param   [in] pt 
 * @retval  近接点からの距離
 * @note
 */
double CDrawingCompositionLine::Distance(const POINT2D& pt) const
{
    double dNear = DBL_MAX;
    double dTmp;

    for (auto l: m_lstSegments)
    {
        for (auto seg: l.lstSegment)
        {
            dTmp = seg.pObj->Distance(pt);
            if (dTmp < dNear)
            {
                dNear = dTmp;
            }
        }
    }
    return dNear;
}

/**
 * @brief   色設定
 * @param   [in] color 
 * @retval  なし
 * @note
 */
void CDrawingCompositionLine::SetColor(COLORREF color)
{
    m_crObj = color;
    for (auto l: m_lstSegments)
    {
        for (auto seg: l.lstSegment)
        {
            seg.pObj->SetColor(color);
        }
    }
}

/**
 * @brief   Id設定
 * @param   [in] iId 
 * @retval  なし
 * @note
 */
void CDrawingCompositionLine::SetId(int iId)
{
    m_nId = iId;
    for (auto l: m_lstSegments)
    {
        for (auto seg: l.lstSegment)
        {
            seg.pObj->SetId(iId);
        }
    }
}

/**
 * @brief   レイヤー設定
 * @param   [in] iLayerId 
 * @retval  なし
 * @note
 */
void CDrawingCompositionLine::SetLayer(int iLayerId)
{
    m_iLayerId = iLayerId;
    for (auto l: m_lstSegments)
    {
        for (auto seg: l.lstSegment)
        {
            seg.pObj->SetLayer(iLayerId);
        }
    }
}

/**
 * @brief   表示・非表示設定
 * @param   [in] iLayerId 
 * @retval  なし
 * @note
 */
void CDrawingCompositionLine::SetVisible(bool bVisible)
{
    m_bVisible = bVisible;
    for (auto l: m_lstSegments)
    {
        for (auto seg: l.lstSegment)
        {
            seg.pObj->SetVisible(bVisible);
        }
    }
}

//!< 選択状態設定
void CDrawingCompositionLine::SetSelect(bool bSel, const RECT2D* pRect)
{
	if (bSel)
	{
		m_eSts |= ES_SELECT;
	}
	else
	{
		m_eSts &= ~ES_SELECT;
	}

    for (auto l: m_lstSegments)
    {
        for (auto seg: l.lstSegment)
        {
            seg.pObj->SetSelect(bSel);
        }
    }

    if ((m_eSts & ES_SELECT) == ES_SELECT)
    {
       ResetProperty();
    }
}

/**
 * @brief   開閉問合
 * @param   なし
 * @retval  true 閉じている
 * @note	
 */                                                                                                                          
bool CDrawingCompositionLine::IsClose () const
{
    return _IsClose (0);
}

bool CDrawingCompositionLine::_IsClose (int iLoopIndex) const
{
    if (m_lstSegments.size() <= iLoopIndex)
    {
        return false;
    }

    const std::deque<SEGMENT>* pSeg = &m_lstSegments[iLoopIndex].lstSegment;

    if(pSeg->empty())
    {
        return false;
    }

    POINT2D ptS = pSeg->at(0).pObj->GetStartSide();
    POINT2D ptE = pSeg->at(pSeg->size() -1).pObj->GetEndSide();

#ifdef DEBUG  //ADD
    POINT2D pt0E = pSeg->at(0).pObj->GetEndSide();
#endif // DEBUG


    if (ptS.Distance(ptE) <  DRAW_CONFIG->dCloseDistance)
    {
       return true;
    }
    return false;
}


//!< オブジェクト数取得
int CDrawingCompositionLine::GetNumberOfObjects() const
{
    return static_cast<int>(m_lstSegments[0].lstSegment.size());
}

//!< オブジェクト取得
const CDrawingObject*  CDrawingCompositionLine::GetObject(int iNo) const
{
    if ( iNo < 0) {return NULL;}
    if ( iNo >= GetNumberOfObjects()) {return NULL;}
    return m_lstSegments[0].lstSegment.at(iNo).pObj.get();
}

/**
 *  @brief   始点終点交換.
 *  @param   なし
 *  @retval  なし
 *  @note    Line,Circle,Splie,CompositLine
 */
bool  CDrawingCompositionLine::SwapStartEnd()
{
    if(m_lstSegments.size() != 1)
    {
        return false;
    }
   
    for (auto seg: m_lstSegments[0].lstSegment)
    {
        seg.pObj->SwapStartEnd();
    }

    std::reverse(m_lstSegments[0].lstSegment.begin(), m_lstSegments[0].lstSegment.end());
    return true;
}

/**
 *  @brief   始点取得.
 *  @param   なし
 *  @retval  始点
 *  @note    Line,Circle,Splie,CompositLine
 */
POINT2D  CDrawingCompositionLine::GetStartSide() const
{
    return GetPoint1();
}

/**
 *  @brief   終点取得.
 *  @param   なし
 *  @retval  始点
 *  @note    Line,Circle,Splie,CompositLine
 */
POINT2D  CDrawingCompositionLine::GetEndSide() const
{
    return GetPoint2();
}

POINT2D  CDrawingCompositionLine::GetMidPoint() const
{
    return GetPointByRate(0.5);
}


POINT2D  CDrawingCompositionLine::GetPointByRate(double d) const
{

    auto l =  m_lstSegments[0];

    int iNode = 0;
    bool bStart = true;
    double dTotalLen = 0.0;
    std::vector<double> lstLen;
    for (auto seg: l.lstSegment)
    {
        double dLen = seg.pObj->Length(seg.pObj->GetStartSide(), bStart);
        lstLen.push_back(dLen);
        dTotalLen += dLen;
    }

    double dLenByRate =  dTotalLen * d;

    dTotalLen = 0.0;
    int iSeg = 0;
    for (double dLen: lstLen)
    {
        dTotalLen +=  dLen;
        if(dTotalLen >= dLenByRate)
        {
            double dSub = dTotalLen -  dLenByRate;
            if (fabs(dSub) < NEAR_ZERO)
            {
                return GetEndSide();
            }
            double dT = dSub / dLen;  
            return l.lstSegment[iSeg].pObj->GetPointByRate(dT);
        }
        iSeg++;
    }
    return GetEndSide();
}


//!<  近接点取得(Line,Circle,Splie,CompositLine用)
bool  CDrawingCompositionLine::GetNearPoints(POINT2D*  ptNear, const POINT2D& pt, bool bOnline) const
{

    if(m_lstSegments.empty())
    {
        return false;
    }

    double dMin = DBL_MAX;
    int iEndSegNo = m_lstSegments[0].lstSegment.size() - 1;
    int iSegNo = 0;
    POINT2D ptRet;
    for (auto l: m_lstSegments[0].lstSegment)
    {
        bool bOnlineSeg = false;
        bool bEndSide = false;
        if((iSegNo == 0) ||
           (iSegNo == iEndSegNo))
        {
            bOnlineSeg =  bOnline;
        }
        POINT2D ptNearSeg;
        l.pObj->GetNearPoints(&ptNearSeg, pt, true /*bOnline*/);
        double dDistance = ptNearSeg.Distance(pt);
        if(dDistance < dMin)
        {
            ptRet = ptNearSeg;
            dMin  =dDistance;
        }
        iSegNo++;
    }

    *ptNear = ptRet;
    return true;
}


bool  CDrawingCompositionLine::GetNormAngle(double* pAngle, const POINT2D& pt, bool bOnline) const
{
    if(m_lstSegments.empty())
    {
        return false;
    }

    double dMin = DBL_MAX;
    int iEndSegNo = m_lstSegments[0].lstSegment.size() - 1;
    int iSegNo = 0;
    double dAngle;
    POINT2D ptRet;
    for (auto l: m_lstSegments[0].lstSegment)
    {
        bool bOnlineSeg = false;
        bool bEndSide = false;
        if((iSegNo == 0) ||
            (iSegNo == iEndSegNo))
        {
            bOnlineSeg =  bOnline;
        }
        POINT2D ptNearSeg;
        l.pObj->GetNearPoints(&ptNearSeg, pt, true /*bOnline*/);
        double dDistance = ptNearSeg.Distance(pt);
        if(dDistance < dMin)
        {
            ptRet = ptNearSeg;
            dMin  =dDistance;
            l.pObj->GetNormAngle(&dAngle, ptNearSeg, bOnlineSeg);
        }
        iSegNo++;
    }
    *pAngle = dAngle;
   return true;
}


//!< 長さ(線,円弧,スプライン上)
double CDrawingCompositionLine::Length(const POINT2D& ptIntersect,
                                       bool bStart) const
{

    double dMin = DBL_MAX;
    POINT2D pt;
    POINT2D ptMin;
    bool bAns = false;
    int iRetNode = 0;
    int iRetLoop = 0;
    int iLoop = 0;
    for (auto l: m_lstSegments)
    {
        int iNode = 0;
        for (auto seg: l.lstSegment)
        {
            if(!seg.pObj->NearPoint(&pt, ptIntersect, true))
            {
                continue;
            }

            double dLen =  ptIntersect.Distance(pt);

            if(dLen < dMin)
            {
                ptMin = pt;
                dMin =  dLen;
                bAns = true;
                iRetNode = iNode;
                iRetLoop = iLoop;
            }
            iNode++;
        }
        iLoop++;
    }

    int iStart;
    int iEnd;

    if(bStart)
    {
      iStart = 0;  
      iEnd = iRetNode;
    }
    else
    {
      iStart = iRetNode + 1;  
      iEnd = SizeToInt(m_lstSegments[iRetLoop].lstSegment.size());
    }

    double dLen = 0.0;
    for (int iCnt = iStart; iCnt < iEnd; iCnt++)
    {
        auto pObj = m_lstSegments[iRetLoop].lstSegment[iCnt].pObj;
        dLen +=  pObj->Length(pObj->GetEndSide(), true);
    }

    dLen +=  m_lstSegments[iRetLoop].lstSegment[iRetNode].pObj->Length(ptMin , bStart);

    return dLen;
}

/*
POINT2D CDrawingCompositionLine::NearPoint(int* pSegmetIndex, const POINT2D& pt, int iLoopIndex = 0 ) const
{
    auto NearPoint = [=](CDrawingObject* pObj)
    {
        int nCnt = 0;
        int nMin = 0;
        double dMin = DBL_MAX;
        POINT2D ptRet;
        const bool bOnline = true;
        switch(pObj->GetType())
        {
            case DT_LINE:
            {
                auto drawLine = dynamic_cast<CDrawingLine*>(pObj);
                ptRet = drawLine->GetLineInstance()->NearPoint(pt, bOnline); 
                break;
            }
            case DT_CIRCLE:
            {
                auto drawCircle = dynamic_cast<CDrawingCircle*>(pObj);
                ptRet = drawCircle->GetCircleInstance()->NearPoint(pt, bOnline); 
                break;
            }
            case DT_ELLIPSE:
            {
                auto drawEllipse = dynamic_cast<CDrawingEllipse*>(pObj);
                ptRet = drawEllipse->GetEllipseInstance()->NearPoint(pt, bOnline); 
                break;
            }
            case DT_SPLINE:
            {
                auto drawSpline = dynamic_cast<CDrawingSpline*>(pObj);
                ptRet = drawSpline->GetSplineInstance()->NearPoint(pt, bOnline); 
                break;
            }

            case DT_COMPOSITION_LINE:
            {
                int iSeg; 
                auto drawCompo = dynamic_cast<CDrawingCompositionLine*>(pObj);
                ptRet = drawCompo->NearPoint(&iSeg, pt); 
                break;
            }
            default:
                ptRet.dX = DBL_MAX; 
                ptRet.dY = DBL_MAX; 
                break;
        }
        return ptRet; 
    };

    if (m_lstSegments.size() <=  iLoopIndex)
    {
        return POINT2D(DBL_MAX, DBL_MAX);
    }

    POINT2D ptAns;
    int nCnt = 0;
    int nMin = 0;
    double dMin = DBL_MAX;
    for (auto seg: m_lstSegments[iLoopIndex].lstSegment)
    {
        POINT2D ptNear =  NearPoint(seg.pObj.get());
        double dLen = pt.Distance(ptNear);
        if (dLen < dMin)
        {
            dMin = dLen;
            ptAns = ptNear;
            nMin = nCnt; 
        }
        nCnt++;
    }

    if(pSegmetIndex)
    {
        *pSegmetIndex = nMin; 
    }
    return ptAns;
}
*/

//!< オフセット(線,円弧,スプライン上)
bool CDrawingCompositionLine::Offset(double dLen, POINT2D ptDir)
{

    double dT = NEAR_ZERO * 2;

    auto GetMin = [](const std::vector<POINT2D>& lst, CDrawingObject* pObj)
    {
        int nCnt = 0;
        int nMin = 0;
        double dMin = DBL_MAX;
        for(auto pt: lst)
        {
            double dLen = pt.Distance(pObj->GetEndSide());
            if (dLen < dMin)
            {
                dMin = dLen;
                nMin = nCnt; 
            }
            nCnt++;
        }
        return nMin; 
    };

    int nCnt = 0;
    std::vector<POINT2D> lst;
    bool bOnLine = false;



    int iLoopIndex = 0;
    for (auto l: m_lstSegments)
    {
        CDrawingObject* pBefore = NULL;
        bool bRight = _IsRightSide(ptDir, iLoopIndex);
        double dDir =( bRight? 1.0: -1.0);
        dLen = dDir * fabs(dLen);   
        bool bClose = _IsClose(iLoopIndex);

        for (auto seg: l.lstSegment)
        {
            seg.pObj->Offset(dLen);
            if(pBefore)
            {
                pBefore->CalcIntersection(&lst, seg.pObj.get(), bOnLine, dT); 
                if(!lst.empty())
                {
                    int nMin = GetMin(lst, pBefore);
                    pBefore->Trim(lst[nMin], pBefore->GetEndSide(), false);
                    seg.pObj->Trim(lst[nMin], seg.pObj->GetStartSide(), false);
                }
            }
            pBefore = seg.pObj.get();
        }

        if(bClose && pBefore)
        {
            pBefore->CalcIntersection(&lst, l.lstSegment[0].pObj.get(), bOnLine, dT); 
            if(lst.empty())
            {
                return false;
            }

            int nMin = GetMin(lst, pBefore);
            pBefore->Trim(lst[nMin], pBefore->GetEndSide(), false);
            l.lstSegment[0].pObj->Trim(lst[nMin], l.lstSegment[0].pObj->GetStartSide(), false);
        }
        iLoopIndex++;
    }
    return true;
}

//!< オフセット(線,円弧,スプライン上)
bool CDrawingCompositionLine::Offset(double dLen)
{
    for (auto l: m_lstSegments)
    {
        for (auto seg: l.lstSegment)
        {
            seg.pObj->Offset(dLen);
        }
    }
    return true;
}

//!< 点が進行方向の右側にあるか
// ptDirには逆変換Matrixを適用すること
bool CDrawingCompositionLine::IsRightSide(POINT2D ptDir) const
{
    
    return _IsRightSide(ptDir, 0) ;
}


bool CDrawingCompositionLine::_IsRightSide(POINT2D ptDir, int iLoopIndex) const
{

    STD_ASSERT(m_lstSegments.size() > 0);
    if (m_lstSegments.size() <= 0)
    {
        return true;
    }

    int iIndex;
    POINT2D ptNear;
    E_NEAR eNear = _NearPointSegment2(&iIndex, &ptNear, iLoopIndex, ptDir);

    STD_ASSERT(eNear != E_NO_ANS);
    if(eNear == E_NO_ANS)
    {
        return true;
    }

    if (m_lstSegments.size() <= iLoopIndex )
    {
        return true;
    }

    auto pLst = &m_lstSegments[iLoopIndex].lstSegment;
    if (pLst->size() > iIndex )
    {
        bool bRight = true;
        bRight = pLst->at(iIndex).pObj->IsRightSide(ptDir);
        return bRight;
    }
    return true;
}


/**
 * @brief   端点1取得
 * @brief   端点1取得
 * @param   なし
 * @retval  端点1
 * @note	
 */
POINT2D CDrawingCompositionLine::GetPoint1() const
{
    if (!m_lstSegments[0].lstSegment.empty())
    {
        return m_lstSegments[0].lstSegment[0].pObj->GetStartSide();
    }
    else
    {
        throw MockException(e_range_over); 
    }
    POINT2D ptRet;
    return ptRet;
}

/**
 * @brief   端点2取得
 * @param   なし
 * @retval  端点2
 * @note	
 */
POINT2D CDrawingCompositionLine::GetPoint2() const
{
    if (m_lstSegments[0].lstSegment.size() > 0)
    {
        size_t endPos = m_lstSegments[0].lstSegment.size() - 1;
        return m_lstSegments[0].lstSegment[endPos].pObj->GetEndSide();
    }
    else
    {
        STD_DBG(_T("m_lstSegment.size() == 0"));
        throw MockException(e_range_over); 
    }
    POINT2D ptRet;
    return ptRet;
}


/**
 *  @brief  位置設定.
 *  @param  [in]    ptPos   移動位置(絶対座標)
 *  @retval         なし
 *  @note
 */
/*
void  CDrawingCompositionLine::SetPoint(const POINT2D& ptAbs)
{
    //POINT2D ptDelta = ptAbs - m_Pt;
    AbsMove(ptAbs);
    //m_Pt = ptAbs;
} 

const POINT2D&  CDrawingCompositionLine::GetPoint()  const
{
    return m_Pt;
}

*/

#ifdef _DEBUG
void PrintSegment(int iSeg, std::shared_ptr<CDrawingObject> pObj)
{
    StdString strType = VIEW_COMMON::GetObjctTypeVarBaseName(pObj->GetType());
    POINT2D* pStart= &pObj->GetStartSide();
    POINT2D* pEnd  = &pObj->GetEndSide();
    DB_PRINT(_T("Seg[%d],[%s] (%f,%f)-(%f,%f)\n"), 
                iSeg,
                strType.c_str(),
                pStart->dX, pStart->dY, 
                pEnd->dX, pEnd->dY );
}

void PrintSegment2(StdString str, std::shared_ptr<CDrawingObject> pObj)
{
    StdString strType = VIEW_COMMON::GetObjctTypeVarBaseName(pObj->GetType());
    POINT2D* pStart= &pObj->GetStartSide();
    POINT2D* pEnd  = &pObj->GetEndSide();
    DB_PRINT(_T("%s,[%s] (%f,%f)-(%f,%f)\n"), 
                str.c_str(),
                strType.c_str(),
                pStart->dX, pStart->dY, 
                pEnd->dX, pEnd->dY );
}

#endif


bool CDrawingCompositionLine::IsCloseObject(const CDrawingObject* pObject)
{
    bool bClose = false;
    DRAWING_TYPE  eDwType = pObject->GetType();
    if (eDwType == DT_COMPOSITION_LINE)
    {
        const CDrawingCompositionLine* pLoop = dynamic_cast<const CDrawingCompositionLine*>(pObject);
        bClose = pLoop->IsClose();
    }
    else if (eDwType == DT_CIRCLE)
    {
        const CDrawingCircle* pLoop = dynamic_cast<const CDrawingCircle*>(pObject);
        bClose = pLoop->GetCircleInstance()->IsClose();
    }
    else if (eDwType == DT_ELLIPSE)
    {
        const CDrawingEllipse* pLoop = dynamic_cast<const CDrawingEllipse*>(pObject);
        bClose = pLoop->GetEllipseInstance()->IsClose();
    }
    else if (eDwType == DT_SPLINE)
    {
        const CDrawingSpline* pLoop = dynamic_cast<const CDrawingSpline*>(pObject);
        bClose = pLoop->GetSplineInstance()->IsClose();
    }
    return bClose;
}


bool CDrawingCompositionLine::IsClockwiseObject(const CDrawingObject* pObject)
{
    bool bClockwisw = true;
    DRAWING_TYPE  eDwType = pObject->GetType();
    if (eDwType == DT_COMPOSITION_LINE)
    {
        const CDrawingCompositionLine* pLoop = dynamic_cast<const CDrawingCompositionLine*>(pObject);
        bClockwisw = pLoop->IsClockwise();
    }
    else if (eDwType == DT_CIRCLE)
    {
        const CDrawingCircle* pLoop = dynamic_cast<const CDrawingCircle*>(pObject);
        bClockwisw = pLoop->GetCircleInstance()->IsClockwise();
    }
    else if (eDwType == DT_ELLIPSE)
    {
        const CDrawingEllipse* pLoop = dynamic_cast<const CDrawingEllipse*>(pObject);
        bClockwisw = pLoop->GetEllipseInstance()->IsClockwise();
    }
    else if (eDwType == DT_SPLINE)
    {
        const CDrawingSpline* pLoop = dynamic_cast<const CDrawingSpline*>(pObject);
        bClockwisw = pLoop->GetSplineInstance()->IsClockwise();
    }
    return bClockwisw;
}



bool CDrawingCompositionLine::SetClockwiseObject(CDrawingObject* pObject, bool bClockeise)
{
    DRAWING_TYPE  eDwType = pObject->GetType();
    if (eDwType == DT_COMPOSITION_LINE)
    {
        CDrawingCompositionLine* pLoop = dynamic_cast<CDrawingCompositionLine*>(pObject);
        if (pLoop->IsClockwise() != bClockeise)
        {
            pLoop->SwapStartEnd();
        }
    }
    else if (eDwType == DT_CIRCLE)
    {
        //単体Ciccleは逆       
        CDrawingCircle* pLoop = dynamic_cast<CDrawingCircle*>(pObject);
        if (pLoop->GetCircleInstance()->IsClockwise() == bClockeise)
        {
            pLoop->GetCircleInstance()->SwapStartEnd();
        }
    }
    else if (eDwType == DT_ELLIPSE)
    {
        CDrawingEllipse* pLoop = dynamic_cast<CDrawingEllipse*>(pObject);
        if (pLoop->GetEllipseInstance()->IsClockwise() == bClockeise)
        {
            pLoop->GetEllipseInstance()->SwapStartEnd();
        }
    }
    else if (eDwType == DT_SPLINE)
    {
        CDrawingSpline* pLoop = dynamic_cast<CDrawingSpline*>(pObject);
        if (pLoop->GetSplineInstance()->IsClockwise() != bClockeise)
        {
            pLoop->GetSplineInstance()->SwapStartEnd();
        }
    }
    else
    {
        return false;
    }

    return true;
}


bool CDrawingCompositionLine::IsLoop(const CDrawingObject* pObject)
{
    return (pObject->GetStartSide().Distance(pObject->GetEndSide()) < NEAR_ZERO);
}

bool CDrawingCompositionLine::CheckIncludeObject(int iId) const
{
    for (auto lst : m_lstSegments)
    {
        for (auto seg : lst.lstSegment)
        {
            if ((seg.iOrgId != -1) &&
                (seg.iOrgId == iId))
            {
                return true;
            }
        }
    }
    return false;
}

/*
* @brief   方向調査
* @param   [in]   pObject  調査する図形
* @param   [in]   pt       調査する点
* @retval  false  逆方向
* @note   ptがE->S方向上の点か調べる
*/

bool CDrawingCompositionLine::_DirectionCheck(std::shared_ptr<CDrawingObject> pObject, 
    const POINT2D& pt)const
{
    DRAWING_TYPE  eDwType = pObject->GetType();
    if (eDwType == DT_CIRCLE)
    {
        auto cir = std::dynamic_pointer_cast<CDrawingCircle>(pObject);
        POINT2D pE = cir->GetEndPoint();

    }
    else if(eDwType == DT_ELLIPSE)
    {
        auto eli = std::dynamic_pointer_cast<CDrawingEllipse>(pObject);

    }
    else if(eDwType == DT_LINE)
    {
        auto line = std::dynamic_pointer_cast<CDrawingLine>(pObject);
        auto primLine = line->GetLineInstance();

  


    }
    else if(eDwType == DT_SPLINE)
    {
        auto spl = std::dynamic_pointer_cast<CDrawingSpline>(pObject);

    }
    else if(eDwType == DT_COMPOSITION_LINE)
    {
        auto com = std::dynamic_pointer_cast<CDrawingCompositionLine>(pObject);

    }
    else
    {
        return false;
    }

    return false;
}

/**
 * @brief   図形追加
 * @param   [in]   ptClick  クリック位置
 * @param   [in]   pObject  追加する図形
 * @param   [out]  pIsClose   図形が閉じられたか否か
 * @param   [in]   bSimulation  追加可能かのみ調べる
 * @param   [out]  ppObjSim     シミュレーションモード時

 * @retval  false 追加失敗
 * @note    トリムしながらつなぐ場合と、トリム済みの場合とでは
 *          処理が若干異なる
 *          一定方向に図形を追加する（閉じるまでは右回りか、左回りか分からない)
 *          先頭ループのみ 
 */
EXCEPTION_TYPE CDrawingCompositionLine::Add(const POINT2D& ptClick, 
                        const CDrawingObject* pObject,
                        bool   bSimulation,
                        std::shared_ptr<CDrawingObject>* ppObjSim )
{
    if (!pObject)
    {
        return e_no_add_object;
    }


#ifdef _DEBUG_ADD_COMP
    bool bSim = false;
    //bSim = bSimulation;
#endif

    DRAWING_TYPE  eDwType = pObject->GetType();
    if (eDwType != DT_CIRCLE &&
        eDwType != DT_LINE   &&
        eDwType != DT_SPLINE &&
        eDwType != DT_ELLIPSE&&
        eDwType != DT_COMPOSITION_LINE
        )
    {
        //この線は選択できません
        return e_no_add_type;
    }
    
    SEGMENT  seg;
    seg.pObj = NULL;

    std::vector<POINT2D> lstPt;
    std::vector<POINT2D> lstPtEnd;
    //交点を調べる
    POINT2D ptCross;
    POINT2D ptCrossEndSide;
    bool bRetS = false;
    bool bRetE = false;

    if(m_lstSegments.size() == 0)
    {
        SEGMENT_LIST lstSeg;
        lstSeg.eRegionType = E_MAIN;
        m_lstSegments.push_back(lstSeg);
    }

    std::deque<SEGMENT>* pTopCompoSegmets = &m_lstSegments[0].lstSegment;                                                                        
    size_t  iEndPos = pTopCompoSegmets->size() - 1;
    double dMin = NEAR_ZERO;

    m_bCreatedPolygon = false;

    if (pObject != this)
    {
        /*
        時針が閉じているときに、閉じているオブジェクトは追加できる。
        基本的に、穴あけ用を想定
        */
        bool bClose = false;
        if (IsClose())
        {
            bClose  = IsCloseObject(pObject);
 
            if (bClose)
            {
                //自身が閉じている時に閉じているオブジェクトを追加
                if (!bSimulation)
                {
                    if(AddLoop(pObject, E_HOLE))
                    {
                        return e_no_error;
                    }
                }
                else
                {
                    if (ppObjSim)
                    {

                        auto pClone = CDrawingObject::CloneShared(this);
                        auto pCloneCompo = std::dynamic_pointer_cast<CDrawingCompositionLine>(pClone);
                        *ppObjSim = pCloneCompo;
                        if(pCloneCompo->AddLoop(pObject, E_HOLE))
                        {
                            return e_no_error;
                        }
                    }
                }
            }
            return e_no_add_open_line;
        }

        // コピーを生成
        seg.pObj = CDrawingObject::CloneShared(pObject);
        seg.pObj->SetParentParts(shared_from_this());
        seg.pObj->SetColor(m_crObj);
        seg.pObj->SetLayer(m_iLayerId);
        seg.pObj->SetId(m_nId);
        seg.pObj->SetVisible(m_bVisible);
        seg.pObj->SetSelect(false);
        seg.iOrgId = pObject->GetId();
        if (pTopCompoSegmets->size() == 0)
        {
            //初回のクリック位置は方向判定に必要なため、記録しておく
            m_ptClickFirst = ptClick;
            if (!bSimulation)
            {
                //初回は無条件に追加
                pTopCompoSegmets->push_back(seg);
            }
            else
            {
                if (ppObjSim)
                {
                    *ppObjSim = CDrawingObject::CloneShared(seg.pObj.get());
                }
            }
            return e_no_error;
        }
    }
    else
    {
        if (iEndPos == 0)
        {
            if (ppObjSim)
            {
                *ppObjSim = NULL;
            }
            return e_no_add_cross;
        }

        //------------
        //自身と交差
        //------------

        //先頭と終端が交差するか調べる
        bool bOnline = false;
        pTopCompoSegmets->at(iEndPos).pObj->CalcIntersection ( &lstPt,
            pTopCompoSegmets->at(0).pObj.get(),  
            bOnline, 
            dMin);
        bRetS = NearPointByList(&ptCross, &lstPt, ptClick);

        if (!bRetS)
        {
            if (ppObjSim)
            {
                (*ppObjSim).reset();
            }
            return e_no_add_cross;
        }

        std::shared_ptr<CDrawingCompositionLine> pThis;

        if (!bSimulation)
        {
            pThis = std::dynamic_pointer_cast<CDrawingCompositionLine>(shared_from_this());
        }
        else
        {
            pThis = std::dynamic_pointer_cast<CDrawingCompositionLine>(CDrawingObject::CloneShared(this));
        }

        auto pMulList = &pThis->m_lstSegments[0].lstSegment;

        //端部を交点位置に調整する

        {
            bool bWithoutChangeClickSide = false;
            POINT2D ptSegStart = pMulList->at(0).pObj->GetStartSide();
            POINT2D ptSegEnd = pMulList->at(iEndPos).pObj->GetEndSide();

            pMulList->at(0).pObj->Trim(ptCross,  ptSegStart, bWithoutChangeClickSide);
            pMulList->at(iEndPos).pObj->Trim(ptCross, ptSegEnd, bWithoutChangeClickSide);


            if(pMulList->at(0).pObj->GetStartSide() ==  pMulList->at(1).pObj->GetStartSide())
            {
                pMulList->at(0).pObj->SwapStartEnd();
            }

            if(pMulList->at(iEndPos).pObj->GetEndSide() ==  pMulList->at(iEndPos -1).pObj->GetEndSide())
            {
                pMulList->at(iEndPos).pObj->SwapStartEnd();
            }

        }

        if (ppObjSim)
        {
            *ppObjSim = pThis;
        }

        return e_no_error;
        //------------
    }

    //---------------------------------------------
    // 新たにオブジェクトを追加する場合
    //---------------------------------------------
    bool bOnline = false;
    {
        //開始側との交点を調べる
        auto pStartObj = pTopCompoSegmets->at(0).pObj;
        pStartObj->CalcIntersection ( &lstPt, seg.pObj.get(),  bOnline, dMin);
        bRetS = NearPointByList(&ptCross, &lstPt, ptClick);
    }

    if (pTopCompoSegmets->size() > 1)
    {
        //終端側との交点を調べる
        auto pEndObj = pTopCompoSegmets->at(iEndPos).pObj;
        pEndObj->CalcIntersection ( &lstPtEnd, seg.pObj.get(),  bOnline, dMin);
        bRetE = NearPointByList(&ptCrossEndSide, &lstPtEnd, ptClick);
    }

    if (!bRetS && !bRetE)
    {
        //選択した図形と交点がない
        if (bSimulation)
        {
            if (ppObjSim)
            {
                *ppObjSim = NULL;
            }
        }
        return e_no_add_intersection;
    }
    else if (bRetS && bRetE)
    {
        //開始、終端どちらとも交点がある場合は近いほうを採用する
        if(ptCrossEndSide.Distance(ptClick) < ptCross.Distance(ptClick))
        {
            bRetS = false;
        }
        else
        {
            bRetE = false;
        }
    }


    std::shared_ptr<CDrawingCompositionLine> pMul;
    if (!bSimulation)
    {
#ifdef _DEBUG_ADD_COMP
        DB_PRINT(_T("Non Simulation\n"));
#endif
        pMul = std::dynamic_pointer_cast<CDrawingCompositionLine>(shared_from_this());
    }
    else
    {
#ifdef _DEBUG_ADD_COMP
       // DB_PRINT(_T("Simulation\n"));
#endif
        pMul = std::dynamic_pointer_cast<CDrawingCompositionLine>(CDrawingObject::CloneShared(this));
    }
    auto pMulList = &pMul->m_lstSegments[0].lstSegment;

    /*
    seg.pObj->SetColor(m_crObj);
    seg.pObj->SetLayer(m_iLayerId);
    seg.pObj->SetId(m_nId);
    seg.pObj->SetVisible(m_bVisible);
    seg.pObj->SetSelect(false);
    seg.pObj->SetParentParts(pMul);
    */

    std::shared_ptr<CDrawingObject> pStartObj = pMulList->at(0).pObj;
    std::shared_ptr<CDrawingObject> pEndObj;

    if (bRetS && !bRetE)
    {

#ifdef _DEBUG_ADD_COMP
        if(!bSim) {
            DB_PRINT(_T("Org Seg  t S:%s E:%s\n"), seg.pObj->GetStartSide().Str().c_str(),
                seg.pObj->GetEndSide().Str().c_str());
            DB_PRINT(_T("Org Start S:%s E:%s\n"), pStartObj->GetStartSide().Str().c_str(),
                pStartObj->GetEndSide().Str().c_str());
        }
#endif
        //--------------------------
        //先頭オブジェクトを変更する
        //--------------------------
        bool bFirst = (pTopCompoSegmets->size() == 1);
        if (bFirst)
        {
            //最初に追加する時はどちらが開始点か決まっていない
            pStartObj->TrimCorner(ptCross, m_ptClickFirst);

            //交点側が最初のセグメントの終点となる
            if ((ptCross.Distance(pStartObj->GetStartSide()) < NEAR_ZERO) &&
                (ptCross.Distance(pStartObj->GetEndSide()) > NEAR_ZERO))
            {

#ifdef _DEBUG_ADD_COMP
                if(!bSim) {
                    DB_PRINT(_T("Swap start0\n"));
                }
#endif
                pStartObj->SwapStartEnd();
            }
        }
        else
        {
            bool bWithoutChangeClickSide = true;

            POINT2D ptEnd = pStartObj->GetEndSide();

            //FOR DEBUG
            std::shared_ptr<CDrawingObject> pStartObjOrg(pStartObj);

            pStartObj->Trim(ptCross, ptEnd , bWithoutChangeClickSide);

            //円、楕円の場合はTrimによって開始。終了位置が入れ替わってしまうので
            //再度チェックする
            try
            {
                if (!IsLoop(pStartObj.get()))
                {
                    if(!bFirst)
                    {
                        //    Start      StartNext                         END
                        // <----------><-------------><-------------><-------------->
                        //  S        E  S           E   S          E   S           E
                        auto pStatNext = pMulList->at(1).pObj;
                        if (pStartObj->GetStartSide().Distance( pStatNext->GetStartSide()) < DRAW_CONFIG->dCloseDistance)
                        {
        #ifdef _DEBUG_ADD_COMP
        if(!bSim) {
           DB_PRINT(_T("Swap start1\n"));
           DB_PRINT(_T("Swap before pStartObj S:%s E:%s \n"), pStartObj->GetStartSide().Str().c_str(), pStartObj->GetEndSide().Str().c_str());
        }
        #endif

                            pStartObj->SwapStartEnd();
                        }
                    }
                }
            }
            catch(...)
            {
                return e_no_add_cross;
            }
        }

        seg.pObj->TrimCorner(ptCross, ptClick);
#ifdef _DEBUG_ADD_COMP
        if(!bSim) {
            if(seg.pObj->GetType() == DT_ELLIPSE)
            {
                auto ell = std::dynamic_pointer_cast<CDrawingEllipse>(seg.pObj);
                DB_PRINT(_T("Elp IsClickwise %s \n"), (ell->GetEllipseInstance()->IsClockwise()?_T("TRUE"):_T("FALSE")));
            }
            else if(seg.pObj->GetType() == DT_CIRCLE)
            {
                auto cir = std::dynamic_pointer_cast<CDrawingCircle>(seg.pObj);
                DB_PRINT(_T("Cir IsClickwise %s \n"), (cir->GetCircleInstance()->IsClockwise()?_T("TRUE"):_T("FALSE")));
            }
        }
#endif



        if(!bFirst)
        {
            if (!IsLoop(seg.pObj.get()))
            {
                //    ADD(seg)       Start                          END
                // <----------><-------------><-------------><-------------->
                //  E        S  S           E   S          E   S           E
                //開始側が接続位置に近い場合は反転する


                if ((seg.pObj->GetStartSide().Distance(ptCross) <  DRAW_CONFIG->dCloseDistance) &&
                    (seg.pObj->GetEndSide().Distance(ptCross) >  DRAW_CONFIG->dCloseDistance))
                {
    #ifdef _DEBUG_ADD_COMP
                    if(!bSim) {
                        DB_PRINT(_T("Swap seg1\n"));
                        DB_PRINT(_T("Swap before Seg S:%s E:%s\n"), seg.pObj->GetStartSide().Str().c_str(), seg.pObj->GetEndSide().Str().c_str());
                    }
    #endif
                    seg.pObj->SwapStartEnd();
                }
            }
            pMulList->push_front(seg);

        }
        else
        {
            if (!IsLoop(seg.pObj.get()))
            {
                //      Start         ADD(seg) 
                // <-------------><------------->
                // S            E  E           S 
                if ((seg.pObj->GetEndSide().Distance(ptCross)   <  DRAW_CONFIG->dCloseDistance) &&
                    (seg.pObj->GetStartSide().Distance(ptCross) >  DRAW_CONFIG->dCloseDistance))
                {
    #ifdef _DEBUG_ADD_COMP
                    if(!bSim) {
                        DB_PRINT(_T("Swap seg2\n"));
                        }
    #endif
                    seg.pObj->SwapStartEnd();
                }
            }
            pMulList->push_back(seg);
        }

#ifdef _DEBUG_ADD_COMP
        if(!bSim) {

#ifdef _DEBUG_ADD_COMP
            if(!bSim) {
                if(pStartObj->GetType() == DT_ELLIPSE)
                {
                    auto ellStart = std::dynamic_pointer_cast<CDrawingEllipse>(pStartObj);
                    DB_PRINT(_T("after Start Elp IsClickwise %s \n"), (ellStart->GetEllipseInstance()->IsClockwise()?_T("TRUE"):_T("FALSE")));
                }
                else if(pStartObj->GetType() == DT_CIRCLE)
                {
                    auto cirStart = std::dynamic_pointer_cast<CDrawingCircle>(pStartObj);
                    DB_PRINT(_T("after Start Cir IsClickwise %s \n"), (cirStart->GetCircleInstance()->IsClockwise()?_T("TRUE"):_T("FALSE")));
                }
                
                if(seg.pObj->GetType() == DT_ELLIPSE)
                {
                    auto ell = std::dynamic_pointer_cast<CDrawingEllipse>(seg.pObj);
                    DB_PRINT(_T("after Elp seg IsClickwise %s \n"), (ell->GetEllipseInstance()->IsClockwise()?_T("TRUE"):_T("FALSE")));
                }
                else if(seg.pObj->GetType() == DT_CIRCLE)
                {
                    auto cir = std::dynamic_pointer_cast<CDrawingCircle>(seg.pObj);
                    DB_PRINT(_T("after Cir seg IsClickwise %s \n"), (cir->GetCircleInstance()->IsClockwise()?_T("TRUE"):_T("FALSE")));
                }
           }
#endif

            DB_PRINT(_T("Seg S:%s E:%s\n"), seg.pObj->GetStartSide().Str().c_str(), seg.pObj->GetEndSide().Str().c_str());
            DB_PRINT(_T("pStartObj S:%s E:%s \n"), pStartObj->GetStartSide().Str().c_str(), pStartObj->GetEndSide().Str().c_str());
        pStartObj->PrintObj(_T("pStartObj "));
        seg.pObj->PrintObj(_T("Seg "));
        }
#endif 

    }
    else if (!bRetS && bRetE)
    {
        //--------------------------
        //終端オブジェクトを変更する
        //--------------------------
        pEndObj =  pMulList->at(iEndPos).pObj;
        std::shared_ptr<CDrawingObject> pEndBeforeObj = pMulList->at(iEndPos - 1).pObj;

#ifdef _DEBUG_ADD_COMP
        if(!bSim) {
            DB_PRINT(_T("Org End S:%s E:%s\n"), pEndObj->GetStartSide().Str().c_str(),
                pEndObj->GetEndSide().Str().c_str());

            DB_PRINT(_T("Org Seg  t S:%s E:%s\n"), seg.pObj->GetStartSide().Str().c_str(),
                seg.pObj->GetEndSide().Str().c_str());

        }
#endif


        bool bWithoutChangeClickSide = true;
        pEndObj->Trim(ptCrossEndSide, pEndObj->GetStartSide() , bWithoutChangeClickSide);
        seg.pObj->TrimCorner(ptCrossEndSide, ptClick);

        //円、楕円の場合はTrimによって開始。終了位置が入れ替わってしまうので
        //再度チェックする

#ifdef _DEBUG_ADD_COMP
        if(!bSim) {
            DB_PRINT(_T("Trim End S:%s E:%s\n"), pEndObj->GetStartSide().Str().c_str(),
                pEndObj->GetEndSide().Str().c_str());

            DB_PRINT(_T("TrimCorner Seg  t S:%s E:%s\n"), seg.pObj->GetStartSide().Str().c_str(),
                seg.pObj->GetEndSide().Str().c_str());

        }
#endif

        double dDistance = pEndObj->GetEndSide().Distance( pEndBeforeObj->GetEndSide());
#ifdef _DEBUG_ADD_COMP
        if(!bSim) {

            DB_PRINT(_T("End Distance  %f Loop:%s\n"), dDistance, (IsLoop(pEndObj.get())?_T("true"):_T("false")));
        }
#endif  

        if (!IsLoop(pEndObj.get()))
        {
            //    Start                       EndBefore       END
            // <----------><-------------><-------------><-------------->
            //  S        E  S           E   S          E   S           E

            if (dDistance < DRAW_CONFIG->dCloseDistance)
            {
#ifdef _DEBUG_ADD_COMP
                if(!bSimulation) {

                    DB_PRINT(_T("pri Swap End S:%s E:%s\n"), pEndObj->GetStartSide().Str().c_str(), pEndObj->GetEndSide().Str().c_str());
                }
#endif                
                pEndObj->SwapStartEnd();
            }
        }

        if (!IsLoop(seg.pObj.get()))
        {
            //        Start                          END        ADD(seg)
            // <-------------><-------------><--------------><---------->
            //  S           E   S          E   S           E   E        S

            if ((seg.pObj->GetEndSide().Distance(ptCrossEndSide)   <  DRAW_CONFIG->dCloseDistance) &&
                (seg.pObj->GetStartSide().Distance(ptCrossEndSide) >  DRAW_CONFIG->dCloseDistance))
            {
#ifdef _DEBUG_ADD_COMP
                if(!bSim) {

                DB_PRINT(_T("Swap seg3\n"));
                }
#endif
                seg.pObj->SwapStartEnd();
            }
        }
        pMulList->push_back(seg);
#ifdef _DEBUG_ADD_COMP
        if(!bSim) {

            DB_PRINT(_T("EndBefore S:%s E:%s\n"), pEndBeforeObj->GetStartSide().Str().c_str(), pEndBeforeObj->GetEndSide().Str().c_str());
            DB_PRINT(_T("End S:%s E:%s\n"), pEndObj->GetStartSide().Str().c_str(), pEndObj->GetEndSide().Str().c_str());
            DB_PRINT(_T("Seg S:%s E:%s\n"), seg.pObj->GetStartSide().Str().c_str(), seg.pObj->GetEndSide().Str().c_str());
        pEndObj->PrintObj(_T("pEndtObj "));
        seg.pObj->PrintObj(_T("Seg "));
        }
#endif 
    }

    if (bSimulation)
    {
        if (ppObjSim)
        {
            *ppObjSim = pMul;
        }
    }

#ifdef _DEBUG_ADD_COMP
    if(!bSim) {
        DB_PRINT(_T("\n"));
        }
#endif 
    return e_no_error;
}

//図形追加 (端点が一致する場合のみ)
bool CDrawingCompositionLine::AddObject(const CDrawingObject* pObject)
{
	if (!pObject)
	{
		return false;
	}

	DRAWING_TYPE  eDwType = pObject->GetType();
	if (eDwType != DT_CIRCLE &&
		eDwType != DT_LINE &&
		eDwType != DT_SPLINE &&
		eDwType != DT_ELLIPSE &&
		eDwType != DT_COMPOSITION_LINE
		)
	{
		return false;
	}


	SEGMENT  seg;
	seg.pObj = NULL;

	// コピーを生成
	seg.pObj = CDrawingObject::CloneShared(pObject);
	seg.pObj->SetParentParts(shared_from_this());
	seg.pObj->SetColor(m_crObj);
	seg.pObj->SetLayer(m_iLayerId);
	seg.pObj->SetId(m_nId);
	seg.pObj->SetVisible(m_bVisible);
	seg.pObj->SetSelect(false);

    if(m_lstSegments.size() == 0)
    {
        SEGMENT_LIST lstSeg;
        lstSeg.eRegionType = E_MAIN;
        m_lstSegments.push_back(lstSeg);
    }
    auto pTopCompoSegmets = &m_lstSegments[0].lstSegment;

    if (pTopCompoSegmets->size() == 0)
	{
        pTopCompoSegmets->push_back(seg);
	}
	else
	{
		POINT2D ptOS = pObject->GetStartSide();
		POINT2D ptOE = pObject->GetEndSide();
		POINT2D ptS = GetStartSide();
		POINT2D ptE = GetEndSide();

		if (ptOS.Distance(ptE) < NEAR_ZERO)
		{
            pTopCompoSegmets->push_back(seg);
		}
		else if (ptOS.Distance(ptS) < NEAR_ZERO)
		{
			seg.pObj->SwapStartEnd();
            pTopCompoSegmets->push_front(seg);
		}
		else if (ptOE.Distance(ptS) < NEAR_ZERO)
		{
            pTopCompoSegmets->push_front(seg);
		}
		else if (ptOE.Distance(ptE) < NEAR_ZERO)
		{
			seg.pObj->SwapStartEnd();
            pTopCompoSegmets->push_back(seg);
		}
		else
		{
			return false;
		}
	}
	return true;
}

///閉じている図形を追加する 
bool CDrawingCompositionLine::AddLoop(const CDrawingObject* pObject,
                                     E_REGION_TYPE eRetion)
{
    DRAWING_TYPE  eDwType = pObject->GetType();
    if (eDwType != DT_CIRCLE &&
        eDwType != DT_SPLINE &&
        eDwType != DT_ELLIPSE &&
        eDwType != DT_COMPOSITION_LINE
        )
    {
        return false;
    }

    //最初に閉じた複合線が登録されている必要がある
    STD_ASSERT(m_lstSegments.size() > 0);

    if(eRetion == E_HOLE)
    {
        if (m_lstSegments.size() == 0)
        {
            return false;
        }
    
        if (!IsClose())
        {
            return false;
        }
    }

    SEGMENT segNew;
    segNew.pObj  = CDrawingObject::CloneShared(pObject);

    SEGMENT_LIST lstSegment;
    lstSegment.eRegionType = eRetion;

    if (eRetion == E_HOLE)
    {
        bool  bClockwise = IsClockwise();
        bool bChg = SetClockwiseObject(segNew.pObj.get(), !bClockwise);
#ifdef _DEBUG //Clockwise
        DB_PRINT(_T("bClockwise %d Chg:%d\n"), bClockwise, bChg); 
#endif
    }

    lstSegment.lstSegment.push_back(segNew);
    m_lstSegments.push_back(lstSegment);
    return true;
}

//!< 点列追加
bool CDrawingCompositionLine::CreateMultiLine(const std::vector<POINT2D>& lstPt)
{
    m_lstSegments.clear();

	if (lstPt.size() < 2)
	{
		return false;
	}

    E_REGION_TYPE eType = E_MAIN;
    if (!m_lstSegments.empty())
    {
        eType = E_HOLE;
    }

    SEGMENT_LIST lstSeg;                                
    lstSeg.eRegionType = eType;
    m_lstSegments.push_back(lstSeg);
    auto pList = &m_lstSegments[0].lstSegment;

    SEGMENT  seg;
    seg.pObj = NULL;

	// コピーを生成

	bool bInit = false;
	POINT2D pt0;
	for (auto pt1 : lstPt)
	{
		if (!bInit)
		{
			pt0 = pt1;
			bInit = true;
			continue;
		}

		seg.pObj = std::make_shared<CDrawingLine>(&LINE2D(pt0, pt1));
		seg.pObj->SetParentParts(shared_from_this());
		seg.pObj->SetColor(m_crObj);
		seg.pObj->SetLayer(m_iLayerId);
		seg.pObj->SetId(m_nId);
		seg.pObj->SetVisible(m_bVisible);
		seg.pObj->SetSelect(false);
        pList->push_back(seg);
		pt0 = pt1;
	}
	return true;
}


//!< 最終削除
void CDrawingCompositionLine::DelLast()
{
    auto pList = &m_lstSegments[0].lstSegment;

    pList->pop_front();

}

bool CDrawingCompositionLine::Disassemble(CPartsDef* pDef, bool bSetUndo)
{
    //オブジェクト自身は呼び出し側で削除する
    CUndoAction* pUndo = NULL;
    if(pDef)
    {
        if(bSetUndo)
        {
            pUndo  = pDef->GetUndoAction();
        }
    }

    if ( m_lstSegments.size() > 1)
    {
        //複数の複合線ががある場合は先にそれを分解する
        for(auto segList: m_lstSegments)
        {
            SEGMENT_LIST lstSeg;
            lstSeg.eRegionType = E_MAIN;
            lstSeg.lstSegment = segList.lstSegment; 
            auto pNew = std::make_shared< CDrawingCompositionLine>();
            pNew->m_lstSegments.push_back(lstSeg);
            pNew->_CopyParm(*this);

            pDef->RegisterObject(pNew, true, false);
            if(pUndo)
            {
                const bool bEnd = false;
                pUndo->Add(UD_ADD, pDef, NULL, pNew, bEnd);
            }
        }
        return true;
    }

    for(SEGMENT& seg: m_lstSegments[0].lstSegment)
    {
        //seg.pObj->Matrix(m_mat2D);
        pDef->RegisterObject(seg.pObj, true, false);
        if(pUndo)
        {
            pUndo->Add(UD_ADD, pDef, NULL, seg.pObj);
        }
    }

    if(pUndo)
    {
        const bool bEnd = true;
        pUndo->Add(UD_DEL, pDef, shared_from_this(), NULL, bEnd);
    }
    return true;
}

/**
 * @brief   ポリゴン用点列の生成
 * @param   なし
 * @retval  false 生成失敗
 * @note
 */
bool CDrawingCompositionLine::_CreatePointList(std::vector<POINT2D>* pListPoints,
                                               std::vector<int>* pVertexIDs)
{

    if (!pListPoints)
    {
        return false;
    }

    if (!IsClose())
    {
        return false;
    }

    pListPoints->clear();
    pVertexIDs->clear();

    auto pList = &m_lstSegments[0].lstSegment;

    int iSize = static_cast<int>(pList->size());

    if (iSize == 0)
    {
        return false;
    }

    for(SEGMENT& seg: *pList)
    {
        if(!seg.pObj)
        {
            continue;
        }

        switch(seg.pObj->GetType())
        {
        case DT_LINE:
            {
                pListPoints->push_back(seg.pObj->GetStartSide());
                pVertexIDs->push_back((int)pListPoints->size() - 1);

            }
            break;

        case DT_CIRCLE:
            {
                CDrawingCircle* pCircle = dynamic_cast<CDrawingCircle*>(seg.pObj.get());
                STD_ASSERT(pCircle);
                CIRCLE2D * pCir;
                pCir = pCircle->GetCircleInstance();
               
                double dStartAngle;
                double dEndAngle;
                double dStep;

                if (pCir->IsClockwise())
                {
                    dStartAngle = CUtil::NormAngle(pCir->GetEnd());
                    dEndAngle = CUtil::NormAngle(pCir->GetStart());
                    if (dEndAngle > dStartAngle)
                    {
                        dStartAngle += 360;
                    }
                }
                else
                {
                    dStartAngle = CUtil::NormAngle(pCir->GetStart());
                    dEndAngle = CUtil::NormAngle(pCir->GetEnd());

                    if (dEndAngle < dStartAngle)
                    {
                        dEndAngle += 360;
                    }
                }

                double dDiv = ceil(fabs(dEndAngle - dStartAngle) / DRAW_CONFIG->dDivPolygonAngle);
                dStep = (dEndAngle - dStartAngle) / dDiv;



                int iCntMax = static_cast<int>(dDiv) - 1;
                double dVal = dStartAngle;
                POINT2D ptPos;
                for(int iCnt = 0; iCnt < iCntMax; iCnt++)
                {
                    ptPos = pCir->GetCenter();
                    ptPos.dX +=  pCir->GetRadius() * cos(dVal * DEG2RAD);
                    ptPos.dY +=  pCir->GetRadius() * sin(dVal * DEG2RAD);
                    pListPoints->push_back(ptPos);
                    pVertexIDs->push_back((int)pListPoints->size() - 1);
                    dVal += dStep;
                }
            }
            break;
        case DT_ELLIPSE:
        {
            CDrawingEllipse* pEllipse = dynamic_cast<CDrawingEllipse*>(seg.pObj.get());
            STD_ASSERT(pEllipse);
            ELLIPSE2D * pElp;
            pElp = pEllipse->GetEllipseInstance();

            double dStartAngle;
            double dEndAngle;
            double dStep;

            if (pElp->IsClockwise())
            {
                dStartAngle = CUtil::NormAngle(pElp->GetEnd());
                dEndAngle = CUtil::NormAngle(pElp->GetStart());
                if (dEndAngle > dStartAngle)
                {
                    dStartAngle += 360;
                }
            }
            else
            {
                dStartAngle = CUtil::NormAngle(pElp->GetStart());
                dEndAngle = CUtil::NormAngle(pElp->GetEnd());

                if (dEndAngle < dStartAngle)
                {
                    dEndAngle += 360;
                }
            }

            double dDiv = ceil(fabs(dEndAngle - dStartAngle) / DRAW_CONFIG->dDivPolygonAngle);
            dStep = (dEndAngle - dStartAngle) / dDiv;



            int iCntMax = static_cast<int>(dDiv) - 1;
            double dVal = dStartAngle;
            POINT2D ptPos;
            for(int iCnt = 0; iCnt < iCntMax; iCnt++)
            {
                ptPos = pElp->GetPoint(dVal);
                pListPoints->push_back(ptPos);
                pVertexIDs->push_back((int)pListPoints->size() - 1);
                dVal += dStep;
            }
        }
        break;


            break;
        case DT_SPLINE:
            {
                 std::vector<LINE2D> lstLine;
                CDrawingSpline* pDrawingSpline = dynamic_cast<CDrawingSpline*>(seg.pObj.get());
                STD_ASSERT(pDrawingSpline);
                SPLINE2D* pSpline = pDrawingSpline->GetSplineInstance();
                pSpline->GetLines(&lstLine);
                foreach(LINE2D& line, lstLine)
                {
                    pListPoints->push_back(line.GetPt1());
                    pVertexIDs->push_back((int)pListPoints->size() - 1);
                }
            }
            break;

        case DT_COMPOSITION_LINE:
            {
                CDrawingCompositionLine* pDCompo = dynamic_cast<CDrawingCompositionLine*>(seg.pObj.get());
                pDCompo->_CreatePointList(pListPoints, pVertexIDs);
            }
            
        default:
            return false;
        }
    }

    return true;
}

/**
 * @brief   ポリゴン生成
 * @param   なし
 * @retval  なし
 * @note
 */
void CDrawingCompositionLine::CreatePolygon(POINT2D* pCenter)
{
    if (m_bCreatedPolygon)
    {
        return;
    }
    m_triangles.clear();
    m_vertexIDs.clear();
    m_points.clear();

    std::vector<int>  triangles;
    std::vector<POINT2D> points;

    _CreatePointList(&m_points, &m_vertexIDs);
    points = m_points;
    makeMonotone(points, m_vertexIDs, triangles);
    
    pCenter->dX = 0.0;
    pCenter->dY = 0.0;


    //三角形を取得する
    int iTriSize = (int)triangles.size() / 3;
    int iPosSize = (int)m_points.size();

    Triangle Tri;
    double dTotal = 0.0f;
    for (int iCnt = 0; iCnt < iTriSize; iCnt++)
    {
        Tri.Pos1 = triangles[iCnt * 3];
        Tri.Pos2 = triangles[iCnt * 3 + 1];
        Tri.Pos3 = triangles[iCnt * 3 + 2];

        //左回りになるように
        if (Tri.Pos1 < Tri.Pos2){std::swap(Tri.Pos1, Tri.Pos2);}
        if (Tri.Pos2 < Tri.Pos3){std::swap(Tri.Pos2, Tri.Pos3);}
        if (Tri.Pos1 < Tri.Pos2){std::swap(Tri.Pos1, Tri.Pos2);}

        if ((Tri.Pos1 < 0) || (Tri.Pos1 >= iPosSize)){continue;}
        if ((Tri.Pos2 < 0) || (Tri.Pos2 >= iPosSize)){continue;}
        if ((Tri.Pos3 < 0) || (Tri.Pos3 >= iPosSize)){continue;}

        double dL1 = m_points[Tri.Pos1].Distance(m_points[Tri.Pos2]);
        double dL2 = m_points[Tri.Pos2].Distance(m_points[Tri.Pos3]);
        double dL3 = m_points[Tri.Pos3].Distance(m_points[Tri.Pos1]);
        double ds  = (dL1 + dL2 + dL3) / 2.0;     
        double dS  = sqrt(ds * (ds - dL1) * (ds - dL2) * (ds - dL3));     

        //面積が小さいものは排除
        if (dS < NEAR_ZERO)
        {
            continue;
        }

        Tri.dArea     = dS;
        dTotal += dS;
        Tri.ptCenter.dX  = (m_points[Tri.Pos1].dX  + m_points[Tri.Pos2].dX + m_points[Tri.Pos3].dX) / 3.0;
        Tri.ptCenter.dY  = (m_points[Tri.Pos1].dY  + m_points[Tri.Pos2].dY + m_points[Tri.Pos3].dY) / 3.0;

        pCenter->dX += (dS * Tri.ptCenter.dX);
        pCenter->dY += (dS * Tri.ptCenter.dY);

        m_triangles.push_back(Tri);

    }

    if (m_triangles.size() != 0)
    {
        pCenter->dX /= dTotal;
        pCenter->dY /= dTotal;
    }
    m_bCreatedPolygon = true;
}

//-----------------------------------------
int CDrawingCompositionLine::GetNumberOfPolygon() const
{
    if (!m_bCreatedPolygon)
    {
        return 0;
    }

    return static_cast<int>(m_triangles.size());
}

//-----------------------------------------
bool CDrawingCompositionLine::GetPolygonVertex(int iCnt, POINT2D* pPt1, POINT2D* pPt2, POINT2D* pPt3) const
{
    if (!m_bCreatedPolygon)
    {
        return false;
    }

    int iSize = (int)m_triangles.size();
    int iPosSize = (int)m_points.size();
       
    if ((iCnt < 0) || (iCnt >= iSize)){return false;}

    *pPt1 = m_points[m_triangles[iCnt].Pos1];
    *pPt2 = m_points[m_triangles[iCnt].Pos2];
    *pPt3 = m_points[m_triangles[iCnt].Pos3];

    return true;
}

void CDrawingCompositionLine::splitEar(std::vector<POINT2D> &points, 
                      std::vector<int> &vertexIDs, 
                      std::vector<int> &triangles)
{
    // 3点しかない場合:
    if(points.size()==3) 
    {
        for(int i=0;i<3;i++) 
        {
            triangles.push_back(vertexIDs.at(i));
        }
        return;
    }

    // X座標の最小値を探す
    double minX = points[0].dX;
    int    minXIndex = 0;
    int    pointSize = SizeToInt(points.size());

    for(int i = 0; i < pointSize; i++) 
    {
        if(points[i].dX < minX) 
        {
            minXIndex = i;
            minX=points[i].dX;
        }
    }

    int previousIndex = minXIndex - 1;
    if(previousIndex < 0)
    {
        previousIndex = SizeToInt(points.size()) - 1;
    }

    int nextIndex = (minXIndex + 1) % points.size();

    triangles.push_back(vertexIDs[previousIndex]);
    triangles.push_back(vertexIDs[minXIndex]);
    triangles.push_back(vertexIDs[nextIndex]);

    points.erase(points.begin() + minXIndex);
    vertexIDs.erase(vertexIDs.begin() + minXIndex);

    splitEar(points,vertexIDs,triangles);
}

void CDrawingCompositionLine::makeMonotone(std::vector<POINT2D>& points, 
                  std::vector<int> &vertexIDs, 
                  std::vector<int>& triangles) 
{

    if(points.size() == 3) 
    {
        splitEar(points,vertexIDs,triangles);
        return;
    }

    // X座標の最大、最小値を求める
    double minX = points[0].dX;
    double maxX = points[0].dX;
    int minXIndex = 0;
    int maxXIndex = 0;
    
    for(size_t i=0;i < points.size(); i++) 
    {
        if(points[i].dX < minX) 
        {
            minXIndex = SizeToInt(i);
            minX = points[i].dX;
        }

        if(points[i].dX > maxX) 
        {
            maxXIndex = SizeToInt(i);
            maxX = points[i].dX;
        }
    }

    // go through the points from minXIndex to maxXIndex along one side of the polygon
    int i = minXIndex + 1;
    while(i % points.size() != maxXIndex) 
    {
        // check if the current vertex has a lower x coodinate than the last one
        int previous = SizeToInt(i) - 1;
        
        if(previous < 0) 
        {
            previous += SizeToInt(points.size());
        }

        if(points[ i % points.size()].dX - points[previous].dX < 0) 
        {
            // find the next vertex that's higher than the previous one
            size_t j=i;
            while(points[ j % points.size()].dX - points [previous].dX < 0) 
            {
                j++;
            }
            
            j++;
            // split off all vertices between i and j inclusively into new polygon
            std::vector<POINT2D> newPoints;
            std::vector<int> newVertexIDs;

            for(size_t k = i; k != j; k++) 
            {
                newPoints.push_back   (points   [k % (int)points.size()]);
                newVertexIDs.push_back(vertexIDs[k % (int)points.size()]);
            }

            j = j % points.size();
            // remove affected vertices from current polygon (tricky because of index shifts)
            if(j<i) 
            {
                // wraparound: deleting is a bit tricky
                if( i + 1 < points.size()) 
                {
                    for(size_t k = i+1; k < points.size(); k++) 
                    {
                        points.erase(points.begin() + i+1);
                        vertexIDs.erase(vertexIDs.begin() + i+1);
                    }
                }

                for(size_t k = 0; k < j; k++) 
                {
                    points.erase(points.begin());
                    vertexIDs.erase(vertexIDs.begin());
                }

            } 
            else 
            {
                // no wraparound: removal is straightforward
                for(size_t k = i+1; k < j; k++) 
                {
                    points.erase(points.begin() + i+1);
                    vertexIDs.erase(vertexIDs.begin() + i+1);
                }
            }

            // recurse into new polygon
            makeMonotone(newPoints,newVertexIDs,triangles);
            // recurse for this polygon (instead of index shifting)
            makeMonotone(points,vertexIDs,triangles);
            return;
        }

        i++;
        if(i >= points.size()) 
        {
            i=0;
        }
    }

    // repeat for the other side of the polygon (descending vertex indices)
    i=minXIndex-1;
    while( i % points.size()!= maxXIndex) 
    {
        if(i < 0) 
        {
            i = SizeToInt(points.size()) - 1;
        }

        // check if the current vertex has a lower x coodinate than the last one
        if(points[i].dX - points[(i+1) % (int)points.size()].dX < 0) 
        {

            // find the next vertex that's higher than the previous one
            size_t j=i;
            while(points[j].dX - points[(i+1) % (int)points.size()].dX < 0) 
            {
                j--;
                if(j < 0) 
                {
                    j = points.size() - 1;
                }
            }

            j--;

            if(j<0) 
            {
                j = points.size() - 1;
            }
            // split off all vertices between i and j inclusively into new polygon
            std::vector<POINT2D> newPoints;
            std::vector<int> newVertexIDs;

            if( i < j) 
            {
                for(int k = i; k >= 0; k--) 
                {
                    newPoints.push_back(points[k]);
                    newVertexIDs.push_back(vertexIDs[k]);
                }

                for(int k = SizeToInt(points.size()) - 1; k >= j; k--) 
                {
                    newPoints.push_back(points[k]);
                    newVertexIDs.push_back(vertexIDs[k]);
                }
            } 
            else 
            {
                for(size_t k = i; k >= j; k--) 
                {
                    newPoints.push_back(points[k]);
                    newVertexIDs.push_back(vertexIDs[k]);
                }
            }

            j = j % (int)points.size();
            // remove affected vertices from current polygon (tricky because of index shifts)
            if(i<j) 
            {
                // wraparound: deleting is a bit tricky
                if( j + 1 < points.size()) 
                {
                    for(size_t k = j+1; k < points.size(); k++) 
                    {
                        points.erase(points.begin() +j+1);
                        vertexIDs.erase(vertexIDs.begin() + j+1);
                    }
                }

                for(int k=0;k<i;k++) 
                {
                    points.erase(points.begin());
                    vertexIDs.erase(vertexIDs.begin());
                }
            } 
            else 
            {
                // no wraparound: removal is straightforward
                for(size_t k=j+1;k<i;k++) 
                {
                    points.erase(points.begin() + j + 1);
                    vertexIDs.erase(vertexIDs.begin() + j + 1);
                }
            }

            // recurse into new polygon
            makeMonotone(newPoints,newVertexIDs,triangles);
            // recurse for this polygon (instead of fancy index shifting)
            makeMonotone(points,vertexIDs,triangles);
            return;
        }

        i--;
        if(i<0) 
        {
            i = (int)points.size() - 1;
        }
    }

    // now that we have a monotone polygon we can actually triangulate it fully
    splitEar(points,vertexIDs,triangles);
}

void CDrawingCompositionLine::LoadAfter(CPartsDef* pDef)
{
    SetParentShared();
}

/**
 *  @brief   マーカ初期化.
 *  @param   [in] pMarker
 *  @retval  なし
 *  @note
 */
bool CDrawingCompositionLine::InitNodeMarker(CNodeMarker* pMarker)
{
    STD_ASSERT(pMarker);

    if(!m_psNodeBound)
    {
        m_psNodeBound = std::make_shared<CNodeBound>();
    }

    RECT2D rcBounds = GetBounds();

    CDrawingView* pView = pMarker->GetView();
    pView->ClearDraggingNodes();
    pView->ClearDraggingTmpObjects();

    //マーカーを用意
    m_psNodeBound->SetAngle(0.0);
    m_psNodeBound->Init(pMarker->GetView(), rcBounds, shared_from_this());
  
    return true;
}


/**
 *  @brief   マーカ移動
 *  @param   [in] pMarker
 *  @param   [in] strMarkerId
 *  @param   [in] ptRet
 *  @retval  なし
 *  @note
 */
void CDrawingCompositionLine::MoveNodeMarker(CNodeMarker* pMarker,
                                 SNAP_DATA* pSnap,
                                 StdString strMarkerId,
                                 MOUSE_MOVE_POS posMouse)
{
    if(!m_psNodeBound)
    {
        return;
    }
    CDrawingView* pView = pMarker->GetView();
    CPartsDef*   pDef  = pView->GetPartsDef();

    pView->SetDrawingMode(DRAW_SEL);

    pView->GetMousePoint2(pSnap, posMouse );
	m_psNodeBound->OnMouseMove(pSnap, posMouse);

}


/**
 *  @brief   マーカ選択
 *  @param   [in] pMarker
 *  @param   [in] iMarkerId
 *  @retval  なし
 *  @note
 */
 void CDrawingCompositionLine::SelectNodeMarker(CNodeMarker* pMarker,
                        StdString strMarkerId)
{
    if (m_psNodeBound)
    {
        m_psNodeBound->Select(pMarker, strMarkerId);
    }
}


 /**
 *  @brief   マーカ開放
 *  @param   [in] pMarker
 *  @retval  なし
 *  @note
 */
bool CDrawingCompositionLine::ReleaseNodeMarker(CNodeMarker* pMarker, StdString strMarkerId, MOUSE_MOVE_POS posMouse)
{

    //MK_LBUTTON   //マウスの左ボタンが押されている場合に設定します。
    //MK_MBUTTON   //マウスの中央ボタンが押されている場合に設定します。
    //MK_RBUTTON   //マウスの右ボタンが押されている場合に設定します。

    if(m_psNodeBound)
    {
        if (pMarker->GetMouseButton() == SEL_LBUTTON)
        {
            if(m_psNodeBound->OnMouseLButtonUp(posMouse))
            {
                m_psNodeBound->ApplyMatrix(false);
            }
        }
        else if(pMarker->GetMouseButton() == SEL_RBUTTON)
        {
            m_psNodeBound->OnMouseRButtonUp(posMouse);
        }
        m_psNodeBound->Release();
	    m_psNodeBound.reset();
    }
    return true;
}


/**
 *  @brief   マーカ開放
 *  @param   [in] pMarker
 *  @retval  なし
 *  @note
 */
void CDrawingCompositionLine::ChangeNode(StdString strMarkerId, E_NODE_CHANGE_TYPE eType)
{
}

//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void TEST_CDrawingCompositionLine()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}
#endif //_DEBUG