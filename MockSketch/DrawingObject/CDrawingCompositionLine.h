/**
* @brief        CDrawingCompositionLineヘッダーファイル
* @file	        CDrawingCompositionLine.h
* @author           Yasuhiro Sato
* @date	        07-2-2009 20:03:09
* 
* @note	
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
* 
* @remarks	        
*			
*
* $
* $
* 
*/
#ifndef __DRAWING_COMPOSITION_LINE_H_
#define __DRAWING_COMPOSITION_LINE_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DrawingObject/CDrawingObject.h"

#include "DrawingObject/Primitive/MAT2D.h"
#include "./CFill.h"

class SEGMENT
{
public:
    std::shared_ptr<CDrawingObject> pObj;
    int iOrgId = -1; //元のID 非保存
private:
    friend class boost::serialization::access;  
    BOOST_SERIALIZATION_SPLIT_MEMBER();

    template<class Archive>
    void load(Archive& ar, const unsigned int version);

    template<class Archive>
    void save(Archive& ar, const unsigned int version) const;


    /* Splineが保存できない
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {

        SERIALIZATION_INIT
        SERIALIZATION_BOTH("Object"      , pObj);
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }
    */
};

/*
std::string name;
name = "Object";
SERIALIZATION_INIT
if(Archive::is_loading::value)
{
    pObj= CDrawingObject::Load<Archive>(ar, 0, name);
}
else
{
    pObj->Save<Archive>(ar, 0, name);
}
MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
*/
enum E_REGION_TYPE
{
    E_MAIN,
    E_HOLE,     //LOOP方向はMAINと逆にする
    E_DIRECT,   //LOOP方向を変えない
};

class SEGMENT_LIST
{
public:
    E_REGION_TYPE eRegionType;        
    std::deque<SEGMENT>   lstSegment;

private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
            SERIALIZATION_BOTH("RegionType"       , eRegionType);
            SERIALIZATION_BOTH("SegmentList"      , lstSegment);
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }
};

/**
* @class   CDrawingCompositionLine
* @brief  
*/
class CDrawingCompositionLine :public CDrawingObject//:public CRTTIClass<CDrawingCompositionLine, CDrawingObject, DT_LINE>
{
protected:
    enum E_NEAR
    {
        E_ON_SECMENT,
        E_ON_NODE,
        E_ON_END_NODE,
        E_NO_ANS,
    };

public:
                                                       


    struct Triangle
    {
        int Pos1;
        int Pos2;
        int Pos3;
        
        POINT2D ptCenter;
        double  dArea;
    };


public:
    //!< コンストラクタ
    CDrawingCompositionLine();

    //!< コンストラクタ
    CDrawingCompositionLine(int nID);

    //!< コピーコンストラクタ
    CDrawingCompositionLine(const CDrawingCompositionLine& Obj);

    //!< デストラクタ
    virtual ~CDrawingCompositionLine();

	//!< 内部データ移動 (AS)(CDrawingObjectにはない
	virtual void MoveInnerPosition(const POINT2D& pt2D);

	//!< 絶対移動 (AS) (CDrawingObjectにはない
	//virtual void AbsMove(const POINT2D& pt2D);

    //!< 相対移動 (AS)
    virtual void Move(const POINT2D& pt2D);

    //!< 回転 (AS)
    virtual void Rotate(const POINT2D& pt2D, double dAngle);

    //!< 絶対回転 (AS) (CDrawingObjectにはない)
    //virtual void RotateAbs(const POINT2D& pt2D, double dAngle);

    //!< 鏡像 (AS)
	virtual void Mirror(const LINE2D& line);

    //!< 倍率 (AS)
	virtual void Scl(const POINT2D& pt2D, double dXScl, double dYScl);

    //!< 行列適用 (AS)
    virtual void Matrix(const MAT2D& mat2D);

    //virtual void MatrixDirect(const MAT2D& mat2D);

    //!< 交点計算
    virtual void CalcIntersection ( std::vector<POINT2D>* pLiat,   
        const CDrawingObject* pObj, 
        bool bOnline = true,
        double dMin = NEAR_ZERO)  const;

    //!< 調整 (AS)
    virtual bool Trim (const POINT2D& ptIntersect, const POINT2D& ptClick, bool bReverse);

    //!< 調整          
    virtual bool TrimCorner (const POINT2D& ptIntersect, const POINT2D& ptClick);

    //!< 分割
    virtual std::shared_ptr<CDrawingObject>  Break (const POINT2D& ptBreak);

    //!< 範囲
    virtual bool IsInner( const RECT2D& rcArea, bool bPart) const;

    //!< 範囲
    virtual RELATION RelationRect(const RECT2D& rcArea) const override;

    //!< 描画
    virtual void Draw(CDrawingView* pView, 
                      const std::shared_ptr<CDrawingObject> pParentObject,
                      E_MOUSE_OVER_TYPE eMouseOver = E_MO_NO,
                      int iLayerId = -1);

    //!< 描画削除
    virtual void DeleteView(CDrawingView* pView, 
                    const std::shared_ptr<CDrawingObject> pParentObject,
                    int iLayerId = -1) override;

    //!< 描画(マウスオーバー時)
    virtual void DrawOver( CDrawingView* pView, bool bOver);
    
    //!< 領域取得 (AS)
    virtual RECT2D GetBounds() const;

    bool CheckIncludeObject(int iId) const;

    //!< 図形追加
    EXCEPTION_TYPE Add(const POINT2D& ptClick, 
              const CDrawingObject* pObject,
              bool  bSimulation = false,
              std::shared_ptr<CDrawingObject>* ppObjSim = NULL);

	//!< 図形追加
	bool AddObject(const CDrawingObject* pObject);

    //!< 図形追加
    bool AddLoop(const CDrawingObject* pObject, E_REGION_TYPE eRetion = E_HOLE);
    
    //!< 折線作成
	bool CreateMultiLine(const std::vector<POINT2D>& lstPt);

    //!< 近接点
    //POINT2D NearPoint(int* pSegmetIndex, const POINT2D& pt, int iLoopIndex = 0 ) const;
	
	//!< 図形追加
    void DelLast (); 

    //!< 閉じているか (AS)
    bool IsClose () const;

    //!< オブジェクト数取得 (AS)
    int GetNumberOfObjects() const;

    //!< オブジェクト取得 (AS)
    const CDrawingObject*  GetObject(int iNo) const;

    //!< スナップ点取得
    virtual  bool GetSnapList(std::list<SNAP_DATA>* pLstSnap)const;

    //-------------------------
    // リフレクション設定
    //-------------------------
    //!< 端点1取得
    POINT2D GetPoint1() const;

    //!< 端点2取得
    POINT2D GetPoint2() const;

    //!< 位置取得 (AS)
    //void SetPoint(const POINT2D& pt2D);

    //!< 位置取得 (AS)
    //const POINT2D&  GetPoint()  const;
    
    //!< 角度設定 (AS)
    //void SetAngle(const double& dAngle);

    //!< 角度取得 (AS)
    //double  GetAngle()  const       { return m_dAngle;}

    //!< 距離 (AS) Override
    virtual double Distance(const POINT2D& pt) const;

    //!< 色設定             (AS) Override
    virtual void SetColor(COLORREF color);

    //!< レイヤーID設定  (AS) Override
    virtual void SetLayer(int iLayerId);

    //!<  表示・非表示設定 （AS) Override
    virtual void SetVisible(bool bVisible);

    //!< ID設定   （AS) Override
    virtual void SetId(int iId);

    //!< 線幅設定  (AS)
    void SetLineWidth(int iWidth);

    //!< 線幅取得 (AS)
    int GetLineWidth() const;

    //!< 線種設定 (AS)
    void SetLineType(int iType); 

    //!< 線種取得 (AS)
    int  GetLineType() const;

    //!< 選択状態設定
    void SetSelect(bool bSel, const RECT2D* pRect = NULL);

    //!< 描画用（画面座標変換用）マトリクス取得(AS)
    virtual const MAT2D* GetDrawingMatrix() const override {return  &m_matDraw;}

    //!< 始点終点交換(Line,Circle,Splie,CompositLine用)
    virtual bool  SwapStartEnd() override;

    //!< 始点取得(Line,Circle,Splie,CompositLine用)
    virtual POINT2D  GetStartSide() const override;

    //!< 始点取得(Line,Circle,Splie,CompositLine用)
    virtual POINT2D  GetEndSide() const override;

    //!<  中点取得(Line,Circle,Splie,CompositLine用)
    virtual POINT2D  GetMidPoint() const override;

    //!<  任意点取得(Line,Circle,Splie,CompositLine用)
    virtual POINT2D  GetPointByRate(double d) const override;

    //!<  近接点取得(Line,Circle,Splie,CompositLine用)
    virtual bool  GetNearPoints(POINT2D*  ptNear, const POINT2D& pt, bool bOnline) const override;

    //!<  法線角取得(Line,Circle,Splie,CompositLine用)
    virtual bool GetNormAngle(double* pAngle, const POINT2D& pt, bool bOnline) const override;

     //!< 長さ(線,円弧,スプライン上)
    virtual double Length(const POINT2D& ptIntersect, bool bStart) const override;

    //!< オフセット(線,円弧,スプライン上)
    virtual bool Offset(double dLen, POINT2D ptDir) override;

    //!< オフセット(線,円弧,スプライン上)
    virtual bool Offset(double dLen) override;

    //!< 点が進行方向の右側にあるか
    virtual bool IsRightSide(POINT2D ptDir) const override;

    //!< マトリクス取得
    //virtual MAT2D GetMatrix() const override{return m_mat2D;}

    //!< マトリクス取得(AS)
    //virtual const MAT2D* GetMatrix2() const {return &m_mat2D;}

    //!< 閉じた図形かの判定
    static bool IsCloseObject(const CDrawingObject* pObject);

    //!< 方向判定
    static bool IsClockwiseObject(const CDrawingObject* pObject);

    //!< 方向設定
    static bool SetClockwiseObject(CDrawingObject* pObject, bool bClockwise);

    void SetParentShared();

    //-------------------------
    // 参照
    //-------------------------

    //!< 参照データに基づいて更新
    virtual CDrawingObject* UpdateRef();

    //-------------------------
    //  ユーティリティ
    //-------------------------
    //開始点取得
    /*
    bool GetStart(POINT2D* ptStart, 
                  const CDrawingObject* pObject, 
                  bool bEnd = false) const;

    //端点取得
    bool GetOther (POINT2D* ptOther, 
                   const CDrawingObject* pObject, 
                   const POINT2D& ptOneSide) const;

    */
    //!< 近接点取得
    virtual bool NearPoint(POINT2D* ptRet, const POINT2D& ptNear, bool bOnLine) const override;

    //-------------------------
    //  ポリゴン分割
    //-------------------------
    bool _CreatePointList(std::vector<POINT2D>* pListPoints, std::vector<int>* pVertexIDs);
    void CreatePolygon(POINT2D* pCenter);
    int GetNumberOfPolygon() const;
    bool GetPolygonVertex(int iCnt, POINT2D* pPt1, POINT2D* pPt2, POINT2D* pPt3) const;

    void splitEar(std::vector<POINT2D> &points, 
              std::vector<int> &vertexIDs, 
              std::vector<int> &triangles);

    void makeMonotone(std::vector<POINT2D>& points, 
                  std::vector<int> &vertexIDs, 
                  std::vector<int>& triangles);


   //-------------------
    // ドラッグ用マーカ
    //-------------------
    //!< マーカ初期化 true:マーカあり
    virtual bool InitNodeMarker(CNodeMarker* pMarker) override;

    //!< マーカ選択
    virtual void SelectNodeMarker(CNodeMarker* pMarker,
                                     StdString strMarkerId) override;

    //!< マーカ移動
    virtual void MoveNodeMarker(CNodeMarker* pMarker,
                                    SNAP_DATA* pSnap,
                                    StdString strMarkerId,
                                    MOUSE_MOVE_POS posMouse) override;

    //!< マーカ開放
    virtual bool ReleaseNodeMarker(CNodeMarker* pMarker, StdString strMarkerId, MOUSE_MOVE_POS posMouse) override;

    //!< マーカ開放
    virtual void ChangeNode(StdString strMarkerId, E_NODE_CHANGE_TYPE eType) override;

     //----------------
    // その他
    //----------------
    //!< ロード終了後処理
    virtual void LoadAfter(CPartsDef* pDef) override;

    CFill<CDrawingCompositionLine>* GetFillInstance(){return &m_Fill;}

    bool Disassemble(CPartsDef* pDef, bool bSetUndo);

    static bool IsLoop(const CDrawingObject* pObject);

    //!< マーカ開放
    std::vector<SEGMENT_LIST>* GetSegmetList(){ return &m_lstSegments;};


protected:

    //-------------------------
    // プロパティ関連メソッド
    //-------------------------

    //!<プロパティ初期化
    virtual TREE_GRID_ITEM*  _CreateStdPropertyTree();

    //!< プロパティ値更新
    //virtual void _PrepareUpdatePropertyGrid();


    //--------------------
    // プロパティ変更処理
    //--------------------
    //static bool PropCenter      (CStdPropertyItem* pData, void* pObj);
    //static bool PropAngle       (CStdPropertyItem* pData, void* pObj);
    //static bool PropClose       (CStdPropertyItem* pData, void* pObj);
    static bool PropLineWidth (CStdPropertyItem* pData, void* pObj);
    static bool PropLineType  (CStdPropertyItem* pData, void* pObj);


    //bool _AddConnet (SEGMENT* pSeg, bool  bSimulation);

     //!<近接点
    E_NEAR _NearPointSegment(int* pLoopIndex, int* pIndex, POINT2D* pNearPoint, const POINT2D& pt) const;

    //!<近接点
    E_NEAR _NearPointSegment2(int* pIndex,  POINT2D* pNearPoint, int iLoopIndex, const POINT2D& pt) const;

    bool _IsRightSide(POINT2D ptDir, int iLoopIndex) const;

    bool _IsClose(int iLoopIndex) const;

    void _CopyParm(const CDrawingCompositionLine& Obj);

    POINT2D _Draw(CDrawingView* pView, SEGMENT_LIST& l, int iLayerId);

    bool IsClockwise() const;

    bool _DirectionCheck(std::shared_ptr<CDrawingObject> pObject, const POINT2D& pt)const;

    bool _IsDirPoint(const CDrawingObject* pObj, POINT2D& ptDir, bool bStart) const;

protected:
    friend class CDrawingCompositionLine;  
    //-----------------------------
    // 保存データ
    //-----------------------------
    //!< 区間データ(データ保持)
    std::vector<SEGMENT_LIST>                     m_lstSegments;


    //!< 線幅
    int                                     m_iWidth;

    //!< 線種
    int                                     m_iLineType;

    //!< 基準点
    //POINT2D                                 m_Pt;

    //!< 角度
    //double                                  m_dAngle;

    //!< 倍率
    //double                                  m_dSclX;
    //double                                  m_dSclY;

    //せん断
    //double                                  m_dSkew;

    //!< アファイン変換マトリックス
    //MAT2D                                   m_mat2D;

    CFill<CDrawingCompositionLine>           m_Fill;

    //-----------------------------
    // 一時データ
    //-----------------------------
    //!< 区間データ(一時表示用)
    //std::deque<SEGMENT>                     m_lstSegmentTmp;

    //!< 描画用（画面座標変換用）マトリクス  //保存不要
    mutable MAT2D                            m_matDraw;

    //!< ポリゴン分割
    std::vector<Triangle>                  m_triangles;

    //!< 頂点ID
    std::vector<int>                       m_vertexIDs; 

    //!< 頂点座標
    std::vector<POINT2D>                   m_points;

     POINT2D                                m_ptClickFirst;

    bool                                   m_bCreatedPolygon;

    std::shared_ptr<CNodeBound>            m_psNodeBound;


private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
            ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(CDrawingObject);
            SERIALIZATION_BOTH("Segments"     , m_lstSegments);
            //SERIALIZATION_BOTH("Datum"        , m_Pt);
            //SERIALIZATION_BOTH("Angle"        , m_dAngle);
            //SERIALIZATION_BOTH("Matrix"       , m_mat2D);
            SERIALIZATION_BOTH("Width"        , m_iWidth);
            SERIALIZATION_BOTH("LineType"     , m_iLineType);
            SERIALIZATION_BOTH("Fill"         , m_Fill);
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }
};

//BOOST_CLASS_VERSION(CDrawingNode, 3);

#endif // __DRAWING_LOOP_H_