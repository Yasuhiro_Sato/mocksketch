/**
 * @brief			CDrawingConnectionLine実装ファイル
 * @file			CDrawingConnectionLine.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CDrawingPoint.h"
#include "./CDrawingLine.h"
#include "./CDrawingConnectionLine.h"
#include "./CDrawingCircle.h"
#include "./CDrawingSpline.h"
#include "./CDrawingEllipse.h"
#include "./CDrawingCompositionLine.h"
#include "./CDrawingParts.h"
#include "./CDrawingNode.h"
#include "DefinitionObject/View/CDrawingView.h"
#include "DefinitionObject/CPartsDef.h"
#include "Utility/CUtility.h"
#include "View/CUndoAction.h"
#include "System/CSystem.h"
#include <math.h>
#include <boost/range/adaptor/reversed.hpp>
#include <boost/serialization/export.hpp> 
BOOST_CLASS_EXPORT(CDrawingConnectionLine);


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define _DEBUG_CONNECTION 

CConnection & CConnection::operator=(const CConnection& rhs)
{
    pt = rhs.pt;
    eArrowType = rhs.eArrowType;

    if (rhs.cd)
    {
        if (!cd)
        {
            cd = std::make_unique<CNodeData>(*rhs.cd);
        }
        else
        {
            *cd =  *rhs.cd;
        }
    }
    else
    {
          cd.reset();
    }

    return *this;
}

POINT_DATA::POINT_DATA(const POINT_DATA &m)
{
    *this = m;
}

POINT_DATA& POINT_DATA::operator =(const POINT_DATA &m)
{
    pt = m.pt;
    bEdit = m.bEdit;

    if (m.cd)
    {
        if (!cd)
        {
            cd = std::make_unique<CNodeData>(*m.cd);
        }
        else
        {
            *cd =  *m.cd;
        }
    }
    else
    {
        cd.reset();
    }
    return *this;
}

/**
 * @brief   シリアル化(ロード)
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  なし
 * @note	
 */
template<class Archive>
void POINT_DATA::load(Archive& ar, const unsigned int version)
{
    SERIALIZATION_INIT
        SERIALIZATION_LOAD("Position", pt);
        SERIALIZATION_LOAD("Edited"  , bEdit);
    MOCK_EXCEPTION_FILE(e_file_read);
}


/**
 * @brief   シリアル化(セーブ)
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  なし
 * @note	
 */
template<class Archive>
void POINT_DATA::save(Archive& ar, const unsigned int version) const
{
    SERIALIZATION_INIT
        SERIALIZATION_SAVE("Position", pt);
        SERIALIZATION_SAVE("Edited"  , bEdit);
    MOCK_EXCEPTION_FILE(e_file_write);
}

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
CDrawingConnectionLine::CDrawingConnectionLine():
CDrawingConnectionLine(-1)
{
    //TODO:実装
}

/**
 *  @brief  コンストラクター.
 *  @param  [in] nID 
 *  @retval なし 
 *  @note
 */
CDrawingConnectionLine::CDrawingConnectionLine(int nID):
CDrawingObject( nID, DT_CONNECTION_LINE),
m_eType(E_MULTI),
m_bCopyNodeDataMode(false),
m_iTmpConnectionObjId(-1)
{
    m_iWidth =    1;
    m_iLineType = PS_SOLID;                         
    CreateNodeData(0);
    CreateNodeData(1);
}


/**
 *  @brief  デストラクタ.
 *  @param  なし 
 *  @retval なし 
 *  @note
 */
CDrawingConnectionLine::~CDrawingConnectionLine()
{
 
}

/**
 * コピーコンストラクタ
 */
CDrawingConnectionLine::CDrawingConnectionLine(const CDrawingConnectionLine& Obj):
CDrawingObject( Obj)
{
    m_iWidth    = Obj.m_iWidth;
    m_iLineType = Obj.m_iLineType;
    m_eType      = Obj.m_eType;
    m_c[0]      =  Obj.m_c[0];
    m_c[1]      =  Obj.m_c[1];
    m_cName[0]  =  Obj.m_cName[0];
    m_cName[1]  =  Obj.m_cName[1];

    m_lstMidPoint        = Obj.m_lstMidPoint;

    if (!m_bCopyNodeDataMode)
    {
        for (auto &c: m_c)
        {
           for(auto& bind: c.cd->lstPlug)
           {
               bind.iId = -1;
               bind.iIndex = -1;
           }

           if( c.cd)
           {
               c.cd->lstSocket.clear();
           }
        }

       for(auto& pt :m_lstMidPoint)
       {
           if(pt.cd)
           {
               pt.cd->lstSocket.clear();
           }
       }
    }
    m_iTmpConnectionObjId = Obj.m_nId;
}


/**
 *  @brief  回転.
 *  @param  [in]    pPt2D    回転中心(絶対座標)
 *  @param  [in]    dAngle   (絶対)回転角(rad)
 *  @retval     なし
 *  @note
 */
void CDrawingConnectionLine::Rotate(const POINT2D& pt2D, double dAngle)
{
    AddChgCnt();

    m_c[0].pt.Rotate(pt2D, dAngle);
    m_c[1].pt.Rotate(pt2D, dAngle);
    for(auto &pd: m_lstMidPoint)
    {
        pd.pt.Rotate(pt2D, dAngle);
    }
}

/**
 *  @brief  相対移動.
 *  @param  [in]    pPt2D   移動量
 *  @retval         なし
 *  @note
 */
void CDrawingConnectionLine::Move(const POINT2D& pt2D) 
{
    AddChgCnt();
    m_c[0].pt.Move(pt2D);
    m_c[1].pt.Move(pt2D);
    for(auto &pd: m_lstMidPoint)
    {
        pd.pt.Move(pt2D);
    }
    UpdateNodeSocketData();
}

/**
 *  @brief  鏡像.
 *  @param  [in]    line   対称軸
 *  @retval         なし
 *  @note
 */
void CDrawingConnectionLine::Mirror(const LINE2D& line)
{
    AddChgCnt();

    m_c[0].pt.Mirror(line);
    m_c[1].pt.Mirror(line);

    for(auto &pd: m_lstMidPoint)
    {
        pd.pt.Mirror(line);
    }
}


/**
 *  @brief  行列.
 *  @param  [in]    mat2D   適用する行列
 *  @retval         なし
 *  @note
 */
void CDrawingConnectionLine::Matrix(const MAT2D& mat2D)
{
    AddChgCnt();

    m_c[0].pt.Matrix(mat2D);
    m_c[1].pt.Matrix(mat2D);

    for(auto &pd: m_lstMidPoint)
    {
        pd.pt.Matrix(mat2D);
    }
}

/**
 *  @brief  倍率.
 *  @param  [in]    pPt2D   拡大中心
 *  @param  [in]    dSclX   X倍率
 *  @param  [in]    dSclY   Y倍率
 *  @retval         なし
 *  @note
 */
void CDrawingConnectionLine::Scl(const POINT2D& pt2D, double dXScl, double dYScl)
{
    AddChgCnt();

    m_c[0].pt.Scl(pt2D, dXScl, dYScl);
    m_c[1].pt.Scl(pt2D, dXScl, dYScl);


    for(auto &pd: m_lstMidPoint)
    {
        pd.pt.Scl(pt2D, dXScl, dYScl);
    }
}


/**
 *  @brief  交点計算.
 *  @param  [out]   pList   交点のリスト
 *  @param  [in]    pObj    交差するオブジェクト
 *  @retval         なし
 *  @note
 */
void CDrawingConnectionLine::CalcIntersection ( std::vector<POINT2D>* pList,   
                                const CDrawingObject* pObj,
                                bool bOnline,
                                double dMin)  const
{
}

/**
 *  @brief  位置調整.
 *  @param  [in]    pPtIntersect   調整位置
 *  @param  [in]    pPtClick       調整方向指定点
 *  @retval         なし
 *  @note
 */
bool CDrawingConnectionLine::Trim (const POINT2D& ptIntersect, const POINT2D& ptClick, bool bReverse)
{
    //Trimは不可
    return false;
}


/**
 *  @brief  分割.
 *  @param  [in]    pPtBreak  分割位置
 *  @retval         分割によって出来たオブジェクト
 *  @note
 */
std::shared_ptr<CDrawingObject>  CDrawingConnectionLine::Break (const POINT2D& ptBreak)
{
    //分割不可
    return NULL;
}


/**
 *  @brief  描画.
 *  @param  [in]  pView       表示View
 *  @param  [in]  bMouseOver  true オブジェクト上にマウスあり
 *  @retval         なし
 *  @note
 */
void CDrawingConnectionLine::Draw(CDrawingView* pView, 
                                    const std::shared_ptr<CDrawingObject> pParentObject,
                                    E_MOUSE_OVER_TYPE eMouseOver,
                                    int iLayerId)
{

    int iWidth = m_iWidth;
    bool bFirst = true;

    bool bIgnoreSelect = false;

	if (iLayerId == -1)
	{
		iLayerId = m_iLayerId;
	}

	COLORREF crPen;
	int      iId;
	crPen = GetDrawColor(pView, iLayerId, pParentObject.get(), &iId, bIgnoreSelect);

    if (eMouseOver == E_MO_SELECTABLE)
    {
        pView->SetLineAlternateMode(true);
        iWidth += 2;
        //Group化で選択したときこのオブジェクトのみ選択色ならない
        //bIgnoreSelect = true;
    }
	else if (eMouseOver == E_MO_EMPHASIS)
	{
		crPen = DRAW_CONFIG->crEmphasis;
	}
	else if (eMouseOver == E_MO_CONNECTABLE)
	{
		crPen = DRAW_CONFIG->crConnectable;
	}



    _Draw( pView, crPen, m_nId, m_iLineType, iWidth, iLayerId, pParentObject.get());

	if (eMouseOver == E_MO_SELECTABLE)
	{
		pView->SetLineAlternateMode(false);
	}
}

void CDrawingConnectionLine::_Draw(CDrawingView* pView,
                    COLORREF crPen,
                    int      iId,
                    int      iType,
                    int      iWidth,
                    int      iLayerId,
                    const CDrawingObject* pParentObject)
{
    //======================
    // 変換マトリックス生成
    //======================

    std::vector<POINT_DATA> lstTmpMid(m_lstMidPoint);
    POINT2D ptTmp[2];

    ptTmp[0] = m_c[0].pt;
    ptTmp[1] = m_c[1].pt;


    if (pParentObject)
    {
        const MAT2D* pMat = pParentObject->GetDrawingMatrix();
        if (pMat)
        {
            ptTmp[0].Matrix(*pMat);
            ptTmp[1].Matrix(*pMat);
            for(auto& pd: lstTmpMid)
            {
                pd.pt.Matrix(*pMat);
            }
        }
    }

    if (m_pMatOffset)
    {
        ptTmp[0].Matrix(*m_pMatOffset);
        ptTmp[1].Matrix(*m_pMatOffset);
        for(auto& pd: lstTmpMid)
        {
            pd.pt.Matrix(*m_pMatOffset);
        }
    }


    bool bFirst = true;


    pView->SetPen(m_iLineType, iWidth, crPen, m_nId); //個々の図形のIDを使用

    std::vector <POINT> lstMidPt;
    POINT ptStart;
    POINT ptEnd;

    POINT ptView;

 
    for(auto pd: lstTmpMid)
    {
        pView->ConvWorld2Scr(&ptView, pd.pt, iLayerId);
        lstMidPt.push_back(ptView);
    }
    pView->ConvWorld2Scr(&ptStart, ptTmp[0], iLayerId);
    pView->ConvWorld2Scr(&ptEnd, ptTmp[1], iLayerId);

    pView->MoveTo(ptStart);

    POINT2D vArrowS;
    POINT2D vArrowE;
    switch(m_eType)
    {
        case CDrawingConnectionLine::E_STRAIGHT:
        {
            vArrowS =   ptTmp[1] - ptTmp[0];
            vArrowE =   ptTmp[0] - ptTmp[1];

            pView->LineTo(ptEnd);
            break;
        }

        case CDrawingConnectionLine::E_MULTI:
        {
#ifdef _DEBUG
            //if (pView->IsDebugMode())
            {
                DB_PRINT(_T("CDrawingConnectionLine::_Draw %d %I64x %s \n"), GetId(), this, GetTag().c_str());
                DB_PRINT(_T(" _Draw [S] %s \n"), m_c[0].pt.Str().c_str());

                int iNo = 0;
                for (auto pt : lstTmpMid)
                {
                    DB_PRINT(_T(" _Draw [%d] %s \n"), iNo, pt.pt.Str().c_str());
                    iNo++;
                }
                DB_PRINT(_T(" _Draw [E] %s \n"), m_c[1].pt.Str().c_str());
                DB_PRINT(_T("CDrawingConnectionLine::_Draw End \n\n"));

            }
#endif
            if (lstTmpMid.empty())
            {
                vArrowS =   ptTmp[0] - ptTmp[1];
                vArrowE =   ptTmp[1] - ptTmp[0];
            }
            else
            {
                vArrowS =   lstTmpMid.begin()->pt - ptTmp[0];
                auto   iteE = lstTmpMid.end();
                iteE--;
                vArrowE =   iteE->pt - ptTmp[1];

            }

            for(POINT& pt:lstMidPt)
            {
                pView->LineTo(pt);
            }
            pView->LineTo(ptEnd);

            break;
        }

        case CDrawingConnectionLine::E_SPLINE:

			if (lstTmpMid.empty())
			{
				vArrowS = ptTmp[0] - ptTmp[1];
				vArrowE = ptTmp[1] - ptTmp[0];
			}
			else
			{
				vArrowS = lstTmpMid.begin()->pt - ptTmp[0];
				auto   iteE = lstTmpMid.end();
				iteE--;
				vArrowE = iteE->pt - ptTmp[1];
			}



			pView->ConvWorld2Scr(&ptStart, ptTmp[0], iLayerId);
			pView->ConvWorld2Scr(&ptEnd, ptTmp[1], iLayerId);

			

			SPLINE2D spline;
			spline.SetType(SPLINE2D::SPT_BSPLINE2);
			spline.SetEditType(SPLINE2D::EDT_CTRL);
			spline.AddControlPoint (ptTmp[0]);
			if (!lstTmpMid.empty())
			{
				POINT2D pt = lstTmpMid.begin()->pt;
				//spline.AddPoint(pt);
				for (auto ite = lstTmpMid.begin();
					ite != lstTmpMid.end();
					++ite)
				{
					pt = ite->pt;
					spline.AddControlPoint(pt);
				}
			}
			spline.AddControlPoint(ptTmp[1]);

			std::vector<POINT2D> lstPt;
			RECT2D viewRect = pView->GetViewArea(iLayerId);
			double dDotLength = pView->DotToLength(1);
			spline.DivideLine(&lstPt, viewRect, dDotLength);

			LineMulti(pView, iLayerId, &lstPt);


            break;
    }
    DrawigArrow(pView, iLayerId, m_c[0].eArrowType, ptTmp[0], vArrowS);
    DrawigArrow(pView, iLayerId, m_c[1].eArrowType, ptTmp[1], vArrowE);

    pView->SetLineAlternateMode(false);

}


bool CDrawingConnectionLine::_DrawigArrow(CDrawingView* pView, 
                                int iLayerId,
                                int iWidth,
                                COLORREF crPen,
                                int iId,
                                int iNo) const
{
    pView->SetPen(PS_SOLID,  iWidth, crPen, iId);

    using namespace VIEW_COMMON;
    bool bFill = false;

    POINT2D pt = m_c[iNo].pt;
    POINT2D ptE = m_c[(iNo + 1) % 2].pt;
    E_ARROW_TYPE eType = m_c[iNo].eArrowType;
    POINT2D vec;

    vec = ptE - pt;
    vec.Normalize();

    
    switch(eType)
    {
    case AT_NONE:
        return true;

    case AT_DOT_FILL:
        bFill = true;
    case AT_DOT:
        DrawigArrowDot(pView, iLayerId, pt, bFill);
        break;

    case AT_OBLIQUE:
        DrawigOblique(pView, iLayerId, pt, vec);
        break;

    case AT_FILLED:
        bFill = true;
    case AT_OPEN:
        {
            DrawigArrowOpen(pView, iLayerId, pt, vec, bFill);
        }
        break;

    case AT_DATUM_FILL:
        bFill = true;
    case AT_DATUM_BLANK:
        DrawigDataum(pView, iLayerId, pt, vec, bFill);
        break;
    }
    return true;
}


/**
 *  @brief  描画削除.
 *  @param  [in]    pView   表示View
 *  @retval         なし
 *  @note
 */
void CDrawingConnectionLine::DeleteView(CDrawingView* pView, 
                        const std::shared_ptr<CDrawingObject> pParentObject,
                        int iLayerId)
{
   //背景色
    COLORREF crObj;
    crObj = DRAW_CONFIG->GetBackColor();
    
    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }

    _Draw( pView, crObj, m_nId, m_iLineType, m_iWidth, iLayerId, pParentObject.get());
}

/**
 *  @brief  範囲.
 *  @param  [in] p1 1点目
 *  @param  [in] p2 2点目
 *  @retval  true:2点で示される矩形内に点が存在する 
 *  @note
 */
bool CDrawingConnectionLine::IsInner( const RECT2D& rcArea, bool bPart) const
{
    if(bPart)
    {
        if(rcArea.IsInside(m_c[0].pt)){return true;}
        if(rcArea.IsInside(m_c[1].pt)){return true;}
    }
    else
    {
        bool bInside = true;
        if(!rcArea.IsInside(m_c[0].pt)){bInside = false;}
        if(!rcArea.IsInside(m_c[1].pt)){bInside = false;}
        return bInside;
    }
    return false;
}


/**
 *  @brief   参照データに基づいて更新.
 *  @param   
 *  @retval  
 *  @note
 */
CDrawingObject* CDrawingConnectionLine::UpdateRef()
{
    CDrawingObject*      pRef;

    pRef = CDrawingObject::UpdateRef();

    if (pRef == NULL)
    {
        return NULL;
    }

    CDrawingConnectionLine* pC = dynamic_cast<CDrawingConnectionLine*>(pRef);

    STD_ASSERT(pC != NULL);

    if (pC == NULL)
    {
        return NULL;
    }

    //TODO: 実装

    m_iChgCnt = pRef->GetChgCnt();
    return pRef;
}

//!< 始点終点交換(Line,Circle,Splie,CompositLine用)
bool  CDrawingConnectionLine::SwapStartEnd()
{
    std::swap(m_c[0], m_c[1]);
    std::reverse(m_lstMidPoint.begin(), m_lstMidPoint.end());
    return true;
}

//!< 始点取得(Line,Circle,Splie,CompositLine用)
POINT2D  CDrawingConnectionLine::GetStartSide() const 
{
    return m_c[0].pt;
}

//!< 始点取得(Line,Circle,Splie,CompositLine用)
POINT2D  CDrawingConnectionLine::GetEndSide() const 
{
    return m_c[1].pt;
}

//!< 始点設定
void CDrawingConnectionLine::SetStart(POINT2D pt)
{
    if(m_c[0].pt != pt)
    {
        m_c[0].pt = pt;
        _UpdatePosition(0);
        RecalcMidPoints(NULL, 0, false);
        UpdateNodeSocketData();
    }
}

//!< 終点設定
void CDrawingConnectionLine::SetEnd(POINT2D pt)
{
    if(m_c[1].pt != pt)
    {
        m_c[1].pt = pt;
        _UpdatePosition(1);
        RecalcMidPoints(NULL, 1, false);
        UpdateNodeSocketData();
    }
}

bool CDrawingConnectionLine::SetStartNode(CDrawingNode* pNode)
{
    return false;
}

bool CDrawingConnectionLine::SetEndNode(CDrawingNode* pNode)
{
    return false;
}

bool CDrawingConnectionLine::_UpdatePosition(int iNo)
{
    POINT2D pt;

    if (_GetConnectionData(&pt, iNo, NULL))
    {
        if (m_c[iNo].pt.Distance(pt) > NEAR_ZERO)
        {
#ifdef _DEBUG_CONNECTION
DB_PRINT(_T("接続解除(%s:%d) iNo\n"), GetName().c_str(), GetId(), iNo);
#endif
            m_c[iNo].cd->lstPlug[0].iId = -1;
            m_c[iNo].cd->lstPlug[0].iIndex = -1;
        }
    }

    return true;
}

bool CDrawingConnectionLine::_UpdateNode()
{
    POINT2D pt;
    for(int iNo = 0; iNo <2; iNo++)
    {
        if (_GetConnectionData(&pt, iNo, NULL))
        {
            m_c[iNo].pt = pt;
        }
    }
    return true;
}

bool CDrawingConnectionLine::_RecalcMidPoint()
{
    POINT2D pt;
    for(int iNo = 0; iNo <2; iNo++)
    {
        if (_GetConnectionData(&pt, iNo, NULL))
        {
            if (m_c[iNo].pt.Distance(pt) > NEAR_ZERO)
            {
#ifdef _DEBUG_CONNECTION
DB_PRINT(_T("_RecalcMidPoint \n"));
#endif
                m_c[iNo].cd->lstPlug[0].iId = -1;
                m_c[iNo].cd->lstPlug[0].iIndex = -1;
            }
        }
    }
    return false;
}

//開始点 接続先設定
bool CDrawingConnectionLine::SetConnection(int iNo, 
    CDrawingObject* pObj,
    int iConnectionIndex)
{
    if(!pObj)
    {
        return false;
    }

    if(iNo < 0){return false;}
    if(iNo > 1){return false;}

    if (iConnectionIndex != -1)
    {
        CNodeData* pCd;
        //相手側に自分を登録
        pObj->CreateNodeData(iConnectionIndex);
        pCd = pObj->GetNodeData(iConnectionIndex);
        pCd->AddDistObject(this, iNo);

        //相手側を登録
        m_c[iNo].cd->lstPlug[0].iId = pObj->GetId();
        m_c[iNo].cd->lstPlug[0].iIndex = iConnectionIndex;
   }
   else
   {
        m_c[iNo].cd->lstPlug[0].iId = -1;
        m_c[iNo].cd->lstPlug[0].iIndex = -1;
   }

   _UpdateConnectedNodeName(iNo);

#ifdef _DEBUG_CONNECTION
DB_PRINT(_T("接続線(%d) 方向%d 相手(%s Id %d 接続ID:%d \n"), GetId(), iNo, 
                        pObj->GetName().c_str(),
                        pObj->GetId(),
                        iConnectionIndex);
#endif
    return true;
}


//開始点 接続先設定
bool CDrawingConnectionLine::SetConnectionStart(CDrawingObject* pObj,
    int iConnectionIndex)
{
    return SetConnection(0, pObj, iConnectionIndex);
}

//終了点 接続先設定
bool CDrawingConnectionLine::SetConnectionEnd( CDrawingObject* pObj,
    int iConnectionIndex)
{
    return SetConnection(1, pObj, iConnectionIndex);
}

bool CDrawingConnectionLine::_UpdateConnectedNodeName(int iNo)
{
    bool bUpdate = false;
    int iId = m_c[iNo].cd->lstPlug[0].iId;
    int iIndex = m_c[iNo].cd->lstPlug[0].iIndex;

    CPartsDef* pDef = GetPartsDef();
    if (!pDef)
    {
        return false;
    }

    auto pDstObj = pDef->GetObjectById(iId).get();

    StdString strTmp;
    if(pDstObj)
    {
        strTmp= pDstObj->GetName();
        strTmp+= _T(":");
        strTmp+= boost::lexical_cast<StdString>(iIndex);

    }

    if(m_cName[iNo] != strTmp)
    {
       m_cName[iNo] = strTmp;
       bUpdate = true;
    }

    if (bUpdate)
    {
        ::PostMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_UPDATE_PROPERTY, 
                    (WPARAM)0, (LPARAM)0);
    }
    return bUpdate;
}

//
bool CDrawingConnectionLine::UpdateNodeSocketData(CDrawingObject* pTmpObj,
                                  CPartsDef* pDef)
{
    bool bRet;
    if (!pDef)
    {
        pDef = GetPartsDef();
    }

    bRet = CDrawingObject::UpdateNodeSocketData(pTmpObj, pDef);

    if (pDef)
    {
        if (!pDef->IsLockUpdateNodeData())
        {
            //


        }
    }

    _UpdateConnectedNodeName(0);
    _UpdateConnectedNodeName(1);

    return bRet;
}

/**
*  @brief  Node更新通知
*  @param  [out]   pPt   新たな位置
*  @param  [in]    pObj  Socket側のオブジェクト
*  @param  [in]    pObj  Socket側の接続Index
*  @retval         なし
*  @note   Socket側が移動したときに呼び出す
*/
bool CDrawingConnectionLine::OnUpdateNodeSocketData(const POINT2D* pPt, 
        const CDrawingObject* pSocketObj,
        int iSocketIndex,
        int iIndex,
        std::set<int>* pList,
        CUndoAction* pUndo)

{

    if (!pSocketObj)
    {
        return false;
    }

    int iId = pSocketObj->GetId();

    for(int iNo = 0; iNo < 2; iNo++)
    {
        if(m_c[iNo].cd->lstPlug[0].iId == iId)
        {
            if(m_c[iNo].cd->lstPlug[0].iIndex == iSocketIndex)
            {
#ifdef _DEBUG_CONNECTION
DB_PRINT(_T("接続更新受信(%d:%s) 方向%d 位置(%f,%f) 相手側接続ID %d\n"), 
                     GetId(), 
                     GetName().c_str(),
                     iNo, 
                     pPt->dX, pPt->dY,
                     iSocketIndex);
#endif
                m_c[iNo].pt = *pPt;
                RecalcMidPoints(NULL, iNo, false);
                UpdateNodeSocketData();
                return true;
            }
        }
    }

    return false;
}



//ベクトルの向き（９０°毎）
RECT2D::E_EDGE CDrawingConnectionLine::_GetPointDir(const POINT2D& vec) 
{
    if (vec.Length() < NEAR_ZERO)
    {
        return RECT2D::E_NONE;
    }

    if( fabs(vec.dY) < NEAR_ZERO )
    {
        if(vec.dX > 0)
        {
            return RECT2D::E_RIGHT;
        }
        else
        {
            return RECT2D::E_LEFT;
        }
    }
    else
    {
        if(vec.dY < 0)
        {
            return RECT2D::E_BOTTOM;
        }
        else
        {
            return RECT2D::E_TOP;
        }
    }
    return RECT2D::E_NONE;
};

RECT2D::E_EDGE CDrawingConnectionLine::_GetLineDir(const LINE2D& vec) 
{
    return  _GetPointDir(vec.GetPt2() - vec.GetPt1()); 

}

/**
 * @brief   最終固定点取得
 * @param   [in] iNoMoveSide 移動側端点番号 0 ro 1 
 * @retval  固定側のiConnectionId
 * @note   
 */
int CDrawingConnectionLine::_GetLastFixPoint(int iNoMoveSide) const
{
    int iCnt = 0;
    if (iNoMoveSide == 0)
    {
        for(auto pd:  m_lstMidPoint)
        {
            if (pd.bEdit)
            {
                return iCnt + 2; 
            }
            iCnt++;
        }
        return -1;
    }
    else
    {
        iCnt = SizeToInt(m_lstMidPoint.size()) - 1;
        for (auto pd : boost::adaptors::reverse(m_lstMidPoint))
        {
            if(pd.bEdit)
            {
                return iCnt + 2;
            }
            iCnt--;
        }
        return -1;
    }
}


//
/**
 * @brief   先端位置の方向
 * @param   [out] pDir 先端位置がとりうる方向のリスト
 * @param   [out] pRc  接続線が接続されるオブジェクトの領域
 * @param   [out] pFixConnectionNo  手動変更点(端点を移動する側から見た最後の番号。
 *                 変更がない場合は-1)
 * @param   [in] iNo 端点番号 0 ro 1 
 * @param   [in] iMoveSide 移動側の端点番号 0 ro 1 
 * @param   [in] bRecalcFixPoint 手動変更点を無視するか 
 * @param   [in] pTmpObj 一時オブジェクト 


 * @retval  なし
 * @note    pTmpObjを使用するのはドラッグ中等で一時的にオブジェクトを
 *          作成した場合に使用する(ObjectIdから検索できないため)
 */
RECT2D::E_EDGE CDrawingConnectionLine::_GetDirection(std::map<RECT2D::E_EDGE, LINE2D>* pDir, 
                                                     RECT2D* pRc, 
                                                     int* pFixConnectionNo,     
                                                     int iNo, 
                                                     int iMoveSide,
                                                     bool bRecalcFixPoint,
                                                     const CDrawingObject* pTmpObj)
{
    //pTmpObj ドラッグ中の一時オブジェクトは使用しない
    double dSnapLen = DRAW_CONFIG->DimToDispUnit(DRAW_CONFIG->dConnectionStraghtLength);

    auto pNd = m_c[iNo].cd.get();
    auto bind = pNd->lstPlug[0];

	//------------------------
	// 接続する対象がない場合
	//------------------------
	if(bind.iId == -1)
    {
        pRc->SetPoint(m_c[iNo].pt);
        pRc->Expansion(dSnapLen);
        _GetEdgeLines( pDir, m_c[iNo].pt, dSnapLen, RECT2D::E_ALL); 
        *pFixConnectionNo =   -1;
        return RECT2D::E_ALL;
    }
	//------------------------


    if ((iMoveSide != iNo)  && !bRecalcFixPoint)
    {
        //固定側の場合は手動で変更した部分が終端となる
        int iFixEdConnectionNo;
        iFixEdConnectionNo = _GetLastFixPoint(iMoveSide);
#ifdef _DEBUG_CONNECTION
DB_PRINT(_T("_GetDirection LastFix Side:%d-%d\n"), iMoveSide, iFixEdConnectionNo);
#endif
        if(iFixEdConnectionNo > 2)  //
        {
            POINT2D pt;
            STD_ASSERT(GetNodeDataPos(&pt, iFixEdConnectionNo));
            POINT2D ptNext;
            if (iMoveSide == 1)
            {
                _GetPriviousPoint(&ptNext, iFixEdConnectionNo ); 
            }
            else
            {
                _GetNextPoint(&ptNext, iFixEdConnectionNo ); 
            }
             
            int iAll = RECT2D::E_ALL;
            POINT2D vec = ptNext - pt;
            iAll = iAll ^ _GetPointDir(vec); 
            pRc->SetPoint(pt);
            pRc->Expansion(dSnapLen);
            RECT2D::E_EDGE edge = static_cast<RECT2D::E_EDGE>(iAll);

            _GetEdgeLines( pDir, pt, dSnapLen, edge);
            *pFixConnectionNo =   iFixEdConnectionNo;
            return edge;
        }
    }

    *pFixConnectionNo =   -1;
    const CDrawingObject* pObj = NULL;

    if(!pObj)
    {
        pObj = GetPartsDef()->GetDraggingObject(bind.iId).get();
        ;
#ifdef _DEBUG_CONNECTION
DB_PRINT(_T("_GetDirection  pObj1(%d) :%I64x\n"), bind.iId, pObj);
#endif
        if (!pObj)
        {
            pObj = GetPartsDef()->GetObjectById(bind.iId).get();

#ifdef _DEBUG_CONNECTION
DB_PRINT(_T("_GetDirection  pObj2(%d) :%I64x\n"), bind.iId, pObj);
#endif

        }
    }

    CNodeData* pCd = NULL;
    if (pObj)
    {
        pCd = pObj->GetNodeDataConst(bind.iIndex);
    }

    CNodeData::E_LINE_DIR_TYPE eDir = CNodeData::E_ALL;        
    if (pCd)
    {
         eDir = pCd->eLineDirection;
    }

    RECT2D::E_EDGE eEdge;
    if( eDir !=   CNodeData::E_NONE)
    {
        if (pObj)
        {
            //ノード番号で与えられる点からオブジェクト領域を少し(dConnectionStraghtLength)
            //膨張させた矩形の辺上の最も近い点を求め、その直線の向きと直線の組(pDIr)と
            //拡張した矩形(pRc)を求める
            eEdge = pObj->InquireConnectDirection(pDir, pRc, bind.iIndex );
        }
        else
        {
            eEdge = RECT2D::E_ALL;
            _GetEdgeLines( pDir, m_c[iNo].pt, dSnapLen, eEdge);
            pRc->SetPoint(m_c[iNo].pt);
            pRc->Expansion(dSnapLen);
        }
        return  eEdge;
   }
    return RECT2D::E_NONE;
}

//オブジェクトに接続していない先端位置の方向
/*
bool CDrawingConnectionLine::_GetDirectionNc(LINE2D* pLine, int iNo)
{
    int iOp = 0;
    if(iNo == 0)
    {
        iOp = 1;
    }

    POINT2D p2;
    double dX = m_c[iOp].pt.dX - m_c[iNo].pt.dX;
    double dY = m_c[iOp].pt.dY - m_c[iNo].pt.dY;
    if ( fabs(dY) > fabs(dX))
    {
        p2.dX = m_c[iNo].pt.dX;
        p2.dY = m_c[iOp].pt.dY;
    }
    else
    {
        p2.dX = m_c[iOp].pt.dX;
        p2.dY = m_c[iNo].pt.dY;
    }

    POINT2D vec = p2 - m_c[iNo].pt;
    vec.Normalize();

    p2 = m_c[iNo].pt + vec * 3.0;

    pLine->SetPt(m_c[iNo].pt, p2);
    return true;
}
*/

//半直線と直線の交点
bool CDrawingConnectionLine::_Intersection(
            POINT2D* pt,
            const LINE2D& half,
            const LINE2D& l,
            bool bInf)
{
    POINT2D ptN;
    if(!half.IntersectInfnityLine(&ptN, l))
    {
        return false;
    }


    //halfは P1を通りP2方向への半直線とする
    POINT2D vecA =   half.GetPt2() - half.GetPt1();
    POINT2D vecB =   ptN - half.GetPt1();

    double dX = fabs(vecB.dX);
    double dY = fabs(vecB.dY);

    double dA = 0;
    if (dX > dY)
    {
        dA = vecA.dX / vecB.dX;
    }

    if (dY != NEAR_ZERO)
    {
        dA = vecA.dY / vecB.dY;
    }

    if(bInf)
    {
        if (dA < 0)
        {
            return false;
        }
    }


    *pt = ptN;
    if(l.IsOnLine(ptN))
    {
        return true;
    }

    return false;
}


//半直線と半直線の交点
bool CDrawingConnectionLine::_IntersectionHalf(
            POINT2D* pt1,
            const LINE2D& half1,
            const LINE2D& half2)
{
    POINT2D ptN;
    if(!half1.IntersectInfnityLine(&ptN, half2))
    {
        return false;
    }

    //halfは P1を通りP2方向への半直線とする

    std::vector<const LINE2D*> lst = { &half1, &half2};

    for( auto l: lst)
    {
        POINT2D vecA =   l->GetPt2() - l->GetPt1();
        POINT2D vecB =   ptN - l->GetPt1();

        double dX = fabs(vecB.dX);
        double dY = fabs(vecB.dY);

        double dA = -1;
        if (dX > dY)
        {
            dA = vecA.dX / vecB.dX;
        }
        else if (dY != NEAR_ZERO)
        {
            dA = vecA.dY / vecB.dY;
        }

        if (dA < 0)
        {
            return false;
        }
    }

    *pt1 = ptN;
    return true;
}


//半直線と四角形の交点(複数ある場合は近いのを返す)
bool CDrawingConnectionLine::_Intersection(
            POINT2D* pt,
            const LINE2D& half,
            const RECT2D& rc,        
            bool bInf)
{
    std::vector<LINE2D> lst;
    rc.GetrLines(&lst);

    bool bRet = false;
    POINT2D ptN;
    POINT2D ptRet;
    double dMin = DBL_MAX;
    double dL;

    for(LINE2D& l: lst)
    {
        if(CDrawingConnectionLine::_Intersection(&ptN, half, l, bInf))
        {
            dL = half.GetPt2().Distance(ptN);
            if(dL < dMin)
            {
                dMin = dL;
                ptRet = ptN;
                bRet = true;
            }
        }
    }

    if (bRet)
    {
        *pt = ptRet;
    }

    return bRet;
}


bool CDrawingConnectionLine::_Intersection(
            std::vector<POINT2D> * plist,
            const LINE2D& half,
            const RECT2D& rc,        
            bool bInf)
{
    std::vector<LINE2D> lst;
    rc.GetrLines(&lst);

    bool bRet = false;
    POINT2D ptN;
    POINT2D ptRet;

    for(LINE2D& l: lst)
    {
        if(CDrawingConnectionLine::_Intersection(&ptN, half, l, bInf))
        {
            plist->push_back(ptN);
            bRet = true;
        }
    }
    return bRet;
}


int CDrawingConnectionLine::_CalcOptimalDirection(  LINE2D* pL1,
                                                    LINE2D* pL2,
                                                    const std::map<RECT2D::E_EDGE, LINE2D>& m1,
                                                    const std::map<RECT2D::E_EDGE, LINE2D>& m2,
                                                    const RECT2D& rc1,
                                                    const RECT2D& rc2)
{

    auto CalcScore = [&](int i)
    {
        int iScore = 0;

        bool bAngle = false;
        if (( i &  ED_SAME) == ED_SAME)
        {
            iScore = 3;
        }
        else if (( i &  ED_OPPSIT) == ED_OPPSIT)
        {
            if((( i &  ED_FROM_0) == ED_FROM_0) &&
               (( i &  ED_FROM_1) == ED_FROM_1))
            {
                iScore = 3;
            }
            else
            {
                iScore = 5;
            }
        }
        else if (( i &  ED_ANGLE) == ED_ANGLE)
        {
            if((( i &  ED_FROM_0) == ED_FROM_0) &&
               (( i &  ED_FROM_1) == ED_FROM_1))
            {
                iScore = 2;
            }
            else
            {
                iScore = 4;
            }
            bAngle = true;
        }

        if((i & ED_COLLISION) == ED_COLLISION)
        {
            if(!bAngle)
            {
                iScore += 3;
            }
        }
        return  iScore;
    };



    int iRelation;
    LINE2D l1;
    LINE2D l2;
    int iRelationAns = 0;
	 
   
    int iScore = 100;


    for(auto p1 = m1.begin();
             p1 != m1.end();
             p1++)
    {

        for (auto p2 = m2.begin();
            p2 != m2.end();
            p2++)
        {
            if(p1->first ==  p2->first)
            {
                iRelation =  ED_SAME;
            }
            else if((p1->first |  p2->first)== RECT2D::E_DIRX)
            {
                iRelation =  ED_OPPSIT;
            }
            else if((p1->first |  p2->first)== RECT2D::E_DIRY)
            {
                iRelation =  ED_OPPSIT;
            }
            else
            {
                iRelation = ED_ANGLE;
            }

            //１から２へ向かう
            POINT2D p11 = p1->second.GetPt2() - p1->second.GetPt1();
            POINT2D p12 = p2->second.GetPt2() - p1->second.GetPt1();
            double dDot1 = p11.Dot(p12);

 
            //2から1へ向かう
            POINT2D p22 = p2->second.GetPt2() - p2->second.GetPt1();
            POINT2D p21 = p1->second.GetPt2() - p2->second.GetPt1();
            double dDot2 = p22.Dot(p21);

            if (rc2.IsInside(p1->second.GetPt2()))
            {
                iRelation |= ED_INSIDE_0;
            }

            if (rc1.IsInside(p2->second.GetPt2()))
            {
                iRelation |= ED_INSIDE_1;
            }

            bool bRev = false;
            if (( dDot2 >= 0)  && (dDot1 < 0))
            {
                iRelation |= ED_REV;
                std::swap(dDot1, dDot2);
                bRev = true;
            }

            if(dDot1 >= 0)
            {
                iRelation |= ED_FROM_0;
            }

            if(dDot2 >= 0)
            {
                iRelation |= ED_FROM_1;
            }

            POINT2D ptCross;
            bool bConf;
            if (bRev)
            {
               bConf = _Intersection(&ptCross,  p2->second, rc1, false);
               if((p2->first == RECT2D::E_TOP) ||
                  (p2->first == RECT2D::E_BOTTOM))
               {
                  iRelation |= ED_ROTATE;
               }
            }
            else
            {
               bConf = _Intersection(&ptCross,  p1->second, rc2, false);
               if((p1->first == RECT2D::E_TOP) ||
                  (p1->first == RECT2D::E_BOTTOM))
               {
                  iRelation |= ED_ROTATE;
               }
            }

            if(bConf)
            {
                if((iRelation & ED_OPPSIT) != ED_OPPSIT)
                {
                    iRelation |= ED_COLLISION;
                }
            }

            int iAns = CalcScore(iRelation);

            if (iScore > iAns)
            {
                iRelationAns = iRelation;
                iScore = iAns;
                l1 = p1->second;
                l2 = p2->second;
            }
        }
    }

    *pL1 =  l1;
    *pL2 =  l2;
    return iRelationAns;
}

/**
 * @brief   中間点再計算
 * @param   [in] pTmpObj 一時オブジェクト 
 * @param   [in] iNoMoveSide 移動側端点番号 0 ro 1 
 * @param   [in] bRecalcEditedPoint 編集済みの点を再計算するかどうか 
 * @retval  なし
 * @note    pTmpObjを使用するのはドラッグ中等で一時的にオブジェクトを
 *          作成した場合に使用する(ObjectIdから検索できないため)
 */
void CDrawingConnectionLine::RecalcMidPoints(const CDrawingObject* pTmpObj,
                                             int iNoMoveSide,
                                             bool bRecalcEditedPoint)
{
    /*
    enum E_DIR 
    {
        NONE      = 0,
        LEFT      = 0x11,
        RIGHT     = 0x12,
        TOP       = 0x21,
        BOTTOM    = 0x22,
        DIRX      = 0x10,
        DIRY      = 0x20,
    };

    enum E_EDGE 
    {
        E_TOP      = 0,
        E_RIGHT    = 1,
        E_BOTTOM   = 2,
        E_LEFT     = 3,
    };
     */

    auto GetObjectByNo = [&](int& iNo)
    {
        const CDrawingObject* pObj = NULL;
        int iId = m_c[iNo].cd->lstPlug[0].iId;
        pObj = GetPartsDef()->GetDraggingObject(iId).get();
        if(!pObj)
        {
            pObj = GetPartsDef()->GetObjectById(iId).get();
        }
        return pObj;
    };


    enum E_STS
    {
        STS_NONE,
        STS_STRIGHT,
        STS_ONE_CORNER,
        STS_REV,
        STS_SAME1,
    };

static E_STS iSts = STS_NONE;
       E_STS iStsOld = iSts;

    //m_lstMidPoint.clear();
    if (m_eType == E_STRAIGHT)
    {
        return;
    }
    else if ((m_eType == E_MULTI) ||
    		(m_eType == E_SPLINE))
    {

        LINE2D l[2];
        int iOp = 0;
        if(iNoMoveSide == 0)
        {
            iOp = 1;
        }

        std::map<RECT2D::E_EDGE, LINE2D> mapEdge[2];
        RECT2D rc[2];
        RECT2D::E_EDGE eg[2];
        int iFixNo[2]; // 固定点（手動で変更した部分）の番号
		//	固定点を示す番号は 始点を０ 終点を１ 中間点は始点側から２，３，４，となる
		
        eg[0] = _GetDirection(&mapEdge[0], &rc[0], &iFixNo[0], 0, iNoMoveSide, bRecalcEditedPoint, pTmpObj);
        eg[1] = _GetDirection(&mapEdge[1], &rc[1], &iFixNo[1], 1, iNoMoveSide, bRecalcEditedPoint, pTmpObj);

#ifdef _DEBUG
for(int iSide = 0; iSide < 2; iSide++)
{
    int iNo = 0;
    for (auto ite = mapEdge[iSide].begin();
        ite != mapEdge[iSide].end();
        ite++)
    {
        DB_PRINT(_T("RecalcMidPoints side(%d)-%d (%s) (%s)-(%s)\n"),
            iSide, iNo,
            RECT2D::Edge2Str(ite->first).c_str(),
            ite->second.GetPt1().Str().c_str(),
            ite->second.GetPt2().Str().c_str());
        iNo++;
    }
}
#endif

        int iOptomalAns;
        iOptomalAns = _CalcOptimalDirection(&l[0], &l[1], mapEdge[0], mapEdge[1], rc[0], rc[1]);

        std::vector<POINT_DATA> lstMidSub;
        if (iFixNo[0] != -1)
        {
            //先頭から
            int iFix =  iFixNo[0] - 1;
            std::vector<POINT_DATA>::iterator end2; 
                
            end2 = m_lstMidPoint.begin() + iFix;
            std::copy(m_lstMidPoint.begin(), end2, std::back_inserter(lstMidSub));
        }
        else if (iFixNo[1] != -1)
        {
            //終端から
            int iFix =  iFixNo[1] - 2;
            std::vector<POINT_DATA>::iterator start;
            start = m_lstMidPoint.begin() + iFix;
            std::copy(start, m_lstMidPoint.end(), std::back_inserter(lstMidSub));
        }

#ifdef _DEBUG_CONNECTION

if (!lstMidSub.empty())
{
    StdStringStream strm;
    for(auto v:    lstMidSub)
    {
        strm << v.pt.Str() << _T(",");
    }
    strm << _T("\n");
    DB_PRINT(_T("lstMidSub:%s"), strm.str().c_str());
}
#endif

#ifdef _DEBUG_CONNECTION
auto Ans2Str =[&](int iAns)
{
    StdStringStream strm;
    if ((iAns & ED_SAME) == ED_SAME)
    {
        strm << _T("Same ");
    }
    if ((iAns & ED_OPPSIT) == ED_OPPSIT)
    {
        strm << _T("Oppsit ");
    }
    if ((iAns & ED_ANGLE) == ED_ANGLE)
    {
        strm << _T("Angle ");
    }
    if ((iAns & ED_FROM_0) == ED_FROM_0)
    {
        strm << _T("F0 ");
    }
    if ((iAns & ED_FROM_1) == ED_FROM_1)
    {
        strm << _T("F1 ");
    }
    if ((iAns & ED_COLLISION) == ED_COLLISION)
    {
        strm << _T("Col ");
    }
    if ((iAns & ED_REV) == ED_REV)
    {
        strm << _T("Rev ");
    }
    if ((iAns & ED_ROTATE) == ED_ROTATE)
    {
        strm << _T("Rot ");
    }
    if ((iAns & ED_INSIDE_0) == ED_INSIDE_0)
    {
        strm << _T("In0 ");
    }
    if ((iAns & ED_INSIDE_1) == ED_INSIDE_1)
    {
        strm << _T("In1 ");
    }

    return strm.str();
};



#endif

#ifdef _DEBUG_CONNECTION

StdString sO = Ans2Str(iOptomalAns);
StdString sE1 = RECT2D::Edge2Str(eg[0]);
StdString sE2 = RECT2D::Edge2Str(eg[1]);

DB_PRINT(_T("iOptomalAns (%s) (%s) (%s) (%s) (%s)\n"),Ans2Str(iOptomalAns).c_str(),
                                        RECT2D::Edge2Str(eg[0]).c_str(),
                                        RECT2D::Edge2Str(eg[1]).c_str(),
                                         rc[0].Str().c_str(),
                                         rc[1].Str().c_str()
                                         ); 
#endif



        int iOptomalAns2 = iOptomalAns &  0x0FFF;
        bool bRev    =   (iOptomalAns & ED_REV)  ==   ED_REV ;
        bool bRotate =   (iOptomalAns & ED_ROTATE)  ==   ED_ROTATE ;
        bool bInside =   ((iOptomalAns & ED_INSIDE_0) == ED_INSIDE_0) ||
                         ((iOptomalAns & ED_INSIDE_1) == ED_INSIDE_1);

        bool bNear = false;
        if(!bRev)
        {
            if(l[0].GetPt1().Distance(l[1].GetPt1())  <  l[0].Length())
            {
                bNear = true;
            }
        }
        else
        {
            if(l[1].GetPt1().Distance(l[0].GetPt1())  <  l[1].Length())
            {
                bNear = true;
            }
        }

        if (bRev)
        {
            std::swap(l[0], l[1]);
            std::swap(rc[0], rc[1]);
        }

        std::vector<POINT_DATA> lstPt;
        if(bNear)
        {
           if(fabs(l[0].GetPt1().dX - l[1].GetPt1().dX) < NEAR_ZERO)
            {
#ifdef _DEBUG_CONNECTION
 DB_PRINT(_T("bNear1\n"));
#endif
               //直線上
                return;
            }

            if(fabs(l[0].GetPt1().dY - l[1].GetPt1().dY) < NEAR_ZERO)
            {
                //直線上
#ifdef _DEBUG_CONNECTION
 DB_PRINT(_T("bNear2\n"));
#endif
                return;
            }

            POINT_DATA pd;
            if (l[0].GetPt1().dX > l[1].GetPt1().dX)
            {
                pd.pt.Set(l[0].GetPt1().dX, l[1].GetPt1().dY);
                lstPt.push_back(pd);
            }
            else
            {
                pd.pt.Set(l[1].GetPt1().dX, l[0].GetPt1().dY);
                lstPt.push_back(pd);
            }
#ifdef _DEBUG_CONNECTION
DB_PRINT(_T("bNear3\n"));
#endif
        }
        else if(iOptomalAns2 ==  (ED_OPPSIT | ED_FROM_0 | ED_FROM_1))
        {
            /*
                        +--------+
                        |        |
                        |  2     |
                        |   ↓   |
                        +---+----+

                +----+---+
                |   ↑   |
                |   1    |
                |        |
                +--------+
            */
#ifdef _DEBUG_CONNECTION
DB_PRINT(_T("_CalcOLLN\n"));
#endif
             //反対向き 互いが見える
             _CalcOLLN(&lstPt, l[0], l[1], rc[0], rc[1], bRotate);

        }
        else if ((iOptomalAns2 == (ED_OPPSIT))||
                 (iOptomalAns2 == (ED_OPPSIT | ED_FROM_0 ))||
                 (iOptomalAns2 == (ED_OPPSIT | ED_FROM_1 )))
        {
#ifdef _DEBUG_CONNECTION
DB_PRINT(_T("_CalcONNN\n"));
#endif
             //反対向き 互いが見えない
             _CalcONNN(&lstPt, l[0], l[1], rc[0], rc[1], bRotate);
        }
        else if(iOptomalAns2 ==  (ED_ANGLE| ED_FROM_0 | ED_FROM_1))
        {
            //90°互いが見える
            /*
                        +--------+
                        |        |
                        +← 2    |
                        |        |
                        +--------+

                +----+---+
                |   ↑   |
                |   1    |
                |        |
                +--------+
            */
             POINT2D ptN;
            if(_IntersectionHalf(&ptN, l[0], l[1]))
            {
#ifdef _DEBUG_CONNECTION
DB_PRINT(_T("ALLN\n"));
#endif
                POINT_DATA pd(ptN);
                lstPt.push_back(pd);
            }
            else
            {
#ifdef _DEBUG_CONNECTION
DB_PRINT(_T("ALLN Error\n"));
#endif
            }
        }
        else if(iOptomalAns2 ==  (ED_ANGLE| ED_FROM_0))
        {
#ifdef _DEBUG_CONNECTION
DB_PRINT(_T("_CalcALNN\n"));
#endif
             //90°片側のみ見える   衝突なし
             _CalcALNN(&lstPt, l[0], l[1], rc[0], rc[1], bRotate);
        }
        else if((iOptomalAns2 ==  (ED_ANGLE| ED_FROM_0|ED_COLLISION)) ||
                (iOptomalAns2 ==  (ED_ANGLE| ED_FROM_0 | ED_FROM_1| ED_COLLISION)))

        {
#ifdef _DEBUG_CONNECTION
DB_PRINT(_T("_CalcALNC\n"));
#endif
           //90°片側のみ見える  衝突有り
            /*
                 +--------+
                 |        |
               +-+← 2    |
               | |        |
               | +--------+
               +---------+
                         |
                    +----+---+
                    |   ↑   |
                    |   1    |
                    |        |
                    +--------+
            */
             _CalcALNC(&lstPt, l[0], l[1], rc[0], rc[1], bRotate);
        }
        else if((iOptomalAns2 ==  ED_ANGLE)  ||
                (iOptomalAns2 == (ED_ANGLE| ED_COLLISION)))
        {
            //90° 互いが見えない 衝突なし
            /*

               +---------+
               |         |
               |    +----+---+
               |    |   ↑   |
               |    |   1    |
               |    |        |
               |    +--------+
               |
               |
               | +--------+
               | |        |
               +-+← 2    |
                 |        |
                 +--------+
            */
#ifdef _DEBUG_CONNECTION
 DB_PRINT(_T("_CalcANNN\n"));
#endif
            _CalcANNN(&lstPt, l[0], l[1], rc[0], rc[1], bRotate);

        }
        else if((iOptomalAns2 &  (ED_SAME| ED_COLLISION)) == (ED_SAME| ED_COLLISION))
        {
#ifdef _DEBUG_CONNECTION
DB_PRINT(_T("_CalcSNNC\n"));
#endif
            //同方向 衝突あり
            if (!bInside)
            {
                _CalcSNNC(&lstPt, l[0], l[1], rc[0], rc[1], bRotate);

            }
            else
            {
                _CalcSLLN(&lstPt, l[0], l[1], rc[0], rc[1], bRotate, bInside);
            }   
        }
        else if((iOptomalAns2 ==  (ED_SAME | ED_FROM_0 | ED_FROM_1)) ||
                (iOptomalAns2 ==  (ED_SAME | ED_FROM_0 )) ||
                (iOptomalAns2 ==   ED_SAME)) 
        {
#ifdef _DEBUG_CONNECTION
DB_PRINT(_T("_CalcSLLN\n"));
#endif
              //同方向  両方/見え
             _CalcSLLN(&lstPt, l[0], l[1], rc[0], rc[1], bRotate, bInside);
        }
        else
        {
#ifdef _DEBUG_CONNECTION
 DB_PRINT(_T("Unknown\n"));
 #endif
             if (iOptomalAns2 == ED_ANGLE)
             {

             }
        }

        if (bRev)
        {
            std::reverse(lstPt.begin(), lstPt.end());
        }


#ifdef _DEBUG_CONNECTION
        DB_PRINT(_T("RecalcMidPoints %I64x %s NoMove(%d) bRev(%s)\n"), this ,GetTag().c_str(), iNoMoveSide, bRev?_T("TRUE"):_T("FALSE"));
        
 DB_PRINT(_T("Move:%d ,FixNo: %d,%d \n"), iNoMoveSide, iFixNo[0], iFixNo[1]);
 DB_PRINT(_T(" [S] %s %s\n"), l[0].GetPt1().Str().c_str(), m_c[0].pt.Str().c_str());

 int iNo = 0;
 for (auto pt : lstPt)
 {
     DB_PRINT(_T(" [%d] %s \n"), iNo, pt.pt.Str().c_str());
     iNo++;
 }
 DB_PRINT(_T(" [E] %s %s \n"), l[1].GetPt1().Str().c_str(), m_c[1].pt.Str().c_str());
 DB_PRINT(_T("-- \n"));

#endif

        //eg[0] = _GetDirection(&mapEdge[0], &rc[0], &iFixNo[0], 0, iNoMoveSide, bRecalcEditedPoint, pTmpObj);
        //eg[1] = _GetDirection(&mapEdge[1], &rc[1], &iFixNo[1], 1, iNoMoveSide, bRecalcEditedPoint, pTmpObj);

		//---------------------------------------
        //Textはノード位置がずれる場合があるので
        //ここで新たに取得したノード位置と入れ替える(l[n].GetPt1は取得し直した接点位置が入っている)
        //ただしノード位置がずれるのがそもそもの問題なので対応が必要
		//---------------------------------------
		if ((iNoMoveSide == 0) && (iFixNo[0] == -1))
        {
            if (bRev) { m_c[1].pt = l[0].GetPt1(); }
            else { m_c[1].pt = l[1].GetPt1(); }
        }
        else if((iNoMoveSide == 1) && (iFixNo[1] == -1))
        {
            if (bRev) { m_c[0].pt = l[1].GetPt1(); }
            else { m_c[0].pt = l[0].GetPt1(); }
        }
		//---------------------------------------

        if(!lstMidSub.empty())
        {
            if (iNoMoveSide == 0)
            {
                lstPt.insert(lstPt.end(), lstMidSub.begin(), lstMidSub.end());
                m_lstMidPoint =  lstPt;
            }
            else
            {
                lstMidSub.insert(lstMidSub.end(), lstPt.begin(), lstPt.end());
                m_lstMidPoint =  lstMidSub;
            }
        }
        else
        {
            m_lstMidPoint =  lstPt;
        }
    }
}

//反対向き 互いが見える
void CDrawingConnectionLine::_CalcOLLN(std::vector<POINT_DATA>* pList, 
                        const LINE2D& l1,
                        const LINE2D& l2,
                        const RECT2D& rc1,
                        const RECT2D& rc2,
                        bool bRotate)
{
    /*
                +--------+
                |        |
                |  2     |
                |   ↓   |
                +---+----+

        +----+---+
        |   ↑   |
        |   1    |
        |        |
        +--------+
    */

    //相手側と直接接続できる場合
    if(fabs(l1.GetPt1().dX - l2.GetPt1().dX) < NEAR_ZERO)
    {
        //同一直線上
        return;
    }

    if(fabs(l1.GetPt1().dY - l2.GetPt1().dY) < NEAR_ZERO)
    {
        //同一直線上
        return;
    }

    if(!bRotate)
    {
        pList->push_back(POINT_DATA(l1.GetPt2()));
        pList->push_back(POINT_DATA(l1.GetPt2().dX, l2.GetPt2().dY));
    }
    else
    {
        pList->push_back(POINT_DATA(l1.GetPt2()));
        pList->push_back(POINT_DATA(l2.GetPt2().dX, l1.GetPt2().dY));
    }
}

bool CDrawingConnectionLine::_CalcONNN_Wrap(std::vector<POINT_DATA>* pList, 
                    const LINE2D& l1,
                    const LINE2D& l2,
                    const std::vector<LINE2D>& lstLine1,
                    const std::vector<LINE2D> lstLine2,
                    bool bRotate)
{
    //接続方向が重ならない場合はその間を通す
    /*
                                    
                       +--------+
            +------+   |        |
            |      |   |  2     |
        +----+---+ |   |   ↓   |  
        |   ↑   | |   +---+----+   
        |   1    | |       |
        |        | +-------+
        +--------+
    */

    bool bWrap = false;
    POINT2D ptMid;
    if(bRotate)
    {
        double dR1 = lstLine1[RECT2D::EG_RIGHT].GetPt1().dX;
        double dL1 = lstLine1[RECT2D::EG_LEFT].GetPt1().dX;
        double dR2 = lstLine2[RECT2D::EG_RIGHT].GetPt1().dX;
        double dL2 = lstLine2[RECT2D::EG_LEFT].GetPt1().dX;


        if(dR1 < dL2)
        {
            ptMid.dX = (dR1 + dL2) / 2.0;
        }
        else if (dR2 < dL1)
        {
            ptMid.dX = (dR2 + dL1) / 2.0;
        }
        else
        {
            bWrap = true;
        }

        if (!bWrap)
        {
            pList->push_back(POINT_DATA(l1.GetPt2()));

            ptMid.dY = l1.GetPt2().dY;
            pList->push_back(POINT_DATA(ptMid));

            ptMid.dY = l2.GetPt2().dY;
            pList->push_back(POINT_DATA(ptMid));

            pList->push_back(POINT_DATA(l2.GetPt2()));
        }
    }
    else
    {
        //相手側が直接見えない
        double dT1 = lstLine1[RECT2D::EG_TOP].GetPt1().dY;
        double dB1 = lstLine1[RECT2D::EG_BOTTOM].GetPt1().dY;
        double dT2 = lstLine2[RECT2D::EG_TOP].GetPt1().dY;
        double dB2 = lstLine2[RECT2D::EG_BOTTOM].GetPt1().dY;

 
        if(dB1 > dT2)
        {
            ptMid.dY = (dB1 + dT2) / 2.0;
        }
        else if (dB2 > dT1)
        {
            ptMid.dY = (dB2 + dT1) / 2.0;
        }
        else
        {
            bWrap = true;
        }

        if (!bWrap)
        {
            pList->push_back(POINT_DATA(l1.GetPt2()));

            ptMid.dX = l1.GetPt2().dX;
            pList->push_back(POINT_DATA(ptMid));

            ptMid.dX = l2.GetPt2().dX;
            pList->push_back(POINT_DATA(ptMid));

            pList->push_back(POINT_DATA(l2.GetPt2()));
        }
    }
    return bWrap;
}

bool CDrawingConnectionLine::_CalcONNN_OutSide(std::vector<POINT_DATA>* pList, 
                    const LINE2D& l1,
                    const LINE2D& l2,
                    double dOut,
                    bool bRotate)
{
    /*
    +-------+
    |       |
    |  +----+---+
    |  |        |
    |  |        |
    |  +--------+
    |           +--------+
    |           |        |
    |           +---+----+
    |               |
    +---------------+
    */
#ifdef _DEBUG_CONNECTION
 DB_PRINT(_T("_CalcONNN_OutSide\n"));
#endif
    POINT2D pMid[2];
    pList->push_back(l1.GetPt2());
    pMid[0] = l1.GetPt2();
    pMid[1] = l2.GetPt2();
    if(bRotate)
    {
        pMid[0].dX = dOut;
        pMid[1].dX = dOut;
    }
    else
    {
        pMid[0].dY = dOut;
        pMid[1].dY = dOut;
    }
    pList->push_back(POINT_DATA(pMid[0]));
    pList->push_back(POINT_DATA(pMid[1]));
    pList->push_back(POINT_DATA(l2.GetPt2()));
    return true;
}


bool CDrawingConnectionLine::_CalcONNN_Mid(std::vector<POINT_DATA>* pList, 
                const LINE2D& l1,
                const LINE2D& l2,
                double dSide1,
                double dSide2,
                double dMid,
                bool bRotate)
{
    /*
         +-----+
         |     |
    +----+---+ |        
    |        | |         
    |        | |
    +--------+ |
           +---+
           |
           | +--------+
           | |        |
           | |        |
           | +---+----+
           |     |
           +-----+
        */
#ifdef _DEBUG_CONNECTION
 DB_PRINT(_T("_CalcONNN_Mid\n"));
#endif
    POINT2D pMid[4];
    pList->push_back(l1.GetPt2());
    pMid[0] = l1.GetPt2();
    pMid[3] = l2.GetPt2();
    if(bRotate)
    {
        pMid[0].dX = dSide1;
        pMid[1].dX = dSide1;
        pMid[1].dY = dMid;
        pMid[2].dX = dSide2;
        pMid[2].dY = dMid;
        pMid[3].dX = dSide2;
    }
    else
    {
        pMid[0].dY = dSide1;
        pMid[1].dY = dSide1;
        pMid[1].dX = dMid;
        pMid[2].dY = dSide2;
        pMid[2].dX = dMid;
        pMid[3].dY = dSide2;
    }
    pList->push_back(POINT_DATA(pMid[0]));
    pList->push_back(POINT_DATA(pMid[1]));
    pList->push_back(POINT_DATA(pMid[2]));
    pList->push_back(POINT_DATA(pMid[3]));
    pList->push_back(POINT_DATA(l2.GetPt2()));
    return true;

}

//反対向き 互いが見えない
void CDrawingConnectionLine::_CalcONNN(std::vector<POINT_DATA>* pList, 
                        const LINE2D& l1,
                        const LINE2D& l2,
                        const RECT2D& rc1,
                        const RECT2D& rc2,
                        bool bRotate)
{
    bool bWrap = false;
    std::vector<LINE2D> lstLine1;
    std::vector<LINE2D> lstLine2;

    rc1.GetrLines(&lstLine1);
    rc2.GetrLines(&lstLine2);
    //接続方向が重ならない場合はその間を通す
    /*
                                    
                       +--------+
            +------+   |        |
            |      |   |  2     |
        +----+---+ |   |   ↓   |  
        |   ↑   | |   +---+----+   
        |   1    | |       |
        |        | +-------+
        +--------+
    */
    if (!_CalcONNN_Wrap(pList, l1, l2, lstLine1, lstLine2, bRotate))
    {
        //重なりなし
        return;
    }


    //相手側が直接見えない
    //接続方向が重なる場合
    /*
    +----+---+
    |   ↑   |
    |   1    |
    +--------+
    L1       R1
            +--------+
            |  2     |
            |   ↓   |
            +---+----+
            L2       R2
   */

    double dR1; 
    double dL1; 
    double dR2; 
    double dL2; 

    double dT1; 
    double dB1; 
    double dT2; 
    double dB2; 

    double dB; 
    double p1;
    double p2;

    if(bRotate)
    {
        dR1 = lstLine1[RECT2D::EG_RIGHT].GetPt1().dX;
        dL1 = lstLine1[RECT2D::EG_LEFT].GetPt1().dX;
        dR2 = lstLine2[RECT2D::EG_RIGHT].GetPt1().dX;
        dL2 = lstLine2[RECT2D::EG_LEFT].GetPt1().dX;
        p1 = l1.GetPt2().dX;
        p2 = l2.GetPt2().dX;

        dT1 = lstLine1[RECT2D::EG_TOP].GetPt1().dY;
        dB1 = lstLine1[RECT2D::EG_BOTTOM].GetPt1().dY;
        dT2 = lstLine2[RECT2D::EG_TOP].GetPt1().dY;
        dB2 = lstLine2[RECT2D::EG_BOTTOM].GetPt1().dY;

    }
    else
    {
        dR1 = lstLine1[RECT2D::EG_TOP].GetPt1().dY;
        dL1 = lstLine1[RECT2D::EG_BOTTOM].GetPt1().dY;
        dR2 = lstLine2[RECT2D::EG_TOP].GetPt1().dY;
        dL2 = lstLine2[RECT2D::EG_BOTTOM].GetPt1().dY;
        p1 = l1.GetPt2().dY;
        p2 = l2.GetPt2().dY;

        dT1 = lstLine1[RECT2D::EG_RIGHT].GetPt1().dY;
        dB1 = lstLine1[RECT2D::EG_LEFT].GetPt1().dY;
        dT2 = lstLine2[RECT2D::EG_RIGHT].GetPt1().dY;
        dB2 = lstLine2[RECT2D::EG_LEFT].GetPt1().dY;
    }


    bool bUseMid = true;
    if (dB1 > dT2)
    {
        dB = dB1;
    }
    else if(dB2 > dT1)
    {
       dB = dT1;
    }
    else
    {
#ifdef _DEBUG_CONNECTION
DB_PRINT(_T("Mid False\n"));
#endif
        bUseMid = false;

    }

    if((dL2 > dL1) &&
       (dR2 > dR1))
    {
    /*
    +----+---+
    |   ↑   |
    |   1    |
    +--------+
    L1       R1
            +--------+
            |  2     |
            |   ↓   |
            +---+----+
            L2       R2
   */
                 
        double d[3];
        d[0] = p1 + p2 - 2 * dL1;           // 左廻りの線の長さ
        if (bUseMid)
        {
            d[1] = p2 - p1 - 2 * dL2 + 2 * dR1; // 中間を通る場合の線の長さ
        }
        else
        {
            d[1] = DBL_MAX; 
        }
        d[2] = 2 * dR2 - p1 - p2;           // 右廻りの線の長さ


        double dMin = DBL_MAX;
        int iMinNo = -1;

        for(int iNo = 0; iNo < 3; iNo++)
        {
            if (d[iNo] < dMin)
            {
                dMin = d[iNo];
                iMinNo = iNo;
            }
        }

        if (iMinNo == 0)
        {
        /*
            +-------+
            |       |
            |  +----+---+
            |  |        |
            |  |        |
            |  +--------+
            |           +--------+
            |           |        |
            |           +---+----+
            |               |
            +---------------+
            */
            //左廻り
            _CalcONNN_OutSide(pList, l1, l2, dL1, bRotate);
        }
        else if (iMinNo == 1)
        {
            /*
                 |-----+
            +----+---+ |        
            |        | |         
            |        | |
            +--------+ |
                   +---+
                   |
                   | +--------+
                   | |        |
                   | |        |
                   | +---+----+
                   +-----|
                */
            _CalcONNN_Mid(pList, l1, l2, dR1, dL2, dB, bRotate);
        }
        else if (iMinNo == 2)
        {
            /*
                 |--------------+       
            +----+---+          |
            |        |          |
            |        |          |
            +--------+          |
                                |
                     +--------+ |
                     |        | |
                     |        | |
                     +---+----+ |
                          |------+
            */
            _CalcONNN_OutSide(pList, l1, l2, dR2, bRotate);
        }
    }
    else if((dL2 <= dL1) &&
            (dR2 <= dR1))
    {

        /*
                    +----+---+
                    |        |
                    |   1    |
                    +--------+


            +--------+
            |        |
            |  2     |
            +---+----+
            */

        double d[3];
        d[0] = p1 + p2 - 2 * dL2;

        if(bUseMid)
        {
            d[1] = p1 - p2 - 2 * dL1 + 2 * dR2;
        }
        else
        {
            d[1] = DBL_MAX;
        }

        d[2] = 2 * dR1 - p1 - p2;


        double dMin = DBL_MAX;
        int iMinNo = -1;

        for(int iNo = 0; iNo < 3; iNo++)
        {
            if (d[iNo] < dMin)
            {
                dMin = d[iNo];
                iMinNo = iNo;
            }
        }

        if (iMinNo == 0)
        {
        /*
           -+--------------+
            |              |
            |         +----+---+
            |         |        |
            |         |   1    |
            |         +--------+
            |   
            |    
            |  +--------+
            |  |        |
            |  |  2     |
            |  +---+----+
            |      |
            +------+
            */
           _CalcONNN_OutSide(pList, l1, l2, dL2, bRotate);

        }
        else if (iMinNo == 1)
        {
            /*
                    +------+
                    |      |
                    | +----+---+
                    | |        |
                    | |   1    |
                    | +--------+
                    +-----+
                        |
            +--------+ |
            |        | |
            |  2     | |
            +---+----+ |
                |      |
                +------+
                */
          _CalcONNN_Mid(pList, l1, l2, dL1, dR2, dB, bRotate);
        }
        else if (iMinNo == 2)
        {
            /*
                        +-----+
                        |     |
                   +----+---+ |
                   |        | |
                   |   1    | |
                   +--------+ |
                              |
                              |
            +--------+        |
            |        |        |
            |  2     |        |
            +---+----+        |
                |             |
                +-------------+
                */

            _CalcONNN_OutSide(pList, l1, l2, dR1, bRotate);
        }
                        
    }
    else if ((dL2 > dL1) &&
             (dR2 < dR1))
    {

/*
        +------+--------+
        |               |
        |       1       |
        +---------------+


            +--------+
            |        |
            |  2     |
            +---+----+
*/
        double d[2];
        d[0] = p1 + p2 - 2 * dL1;
        d[1] = 2 * dR1 - p1 - p2;

        if ( d[0]  <  d[1] )
        {
/*
        +--------+ 
        |        |
        | +------+--------+
        | |               |
        | |       1       |
        | +---------------+
        |
        |
        |     +--------+
        |     |        |
        |     |  2     |
        |     +---+----+
        |         |
        +---------+
    */     
            _CalcONNN_OutSide(pList, l1, l2, dL1, bRotate);

        }
        else
        {
/*
                +---------+ 
                |         |
        +------+--------+ |
        |               | |
        |       1       | |
        +---------------+ |
                          |
                          |
            +--------+    |
            |        |    |
            |  2     |    |
            +---+----+    |
                |         |
                +---------+
    */     
            _CalcONNN_OutSide(pList, l1, l2, dR1, bRotate);
        }
    }
    else if ((dL2 <= dL1) &&
             (dR2 >= dR1))
    {
/*

            +---+----+
            |        |
            |   1    |
            +--------+

                            
        +---------------+
        |               |
        |       2       |
        +------+--------+
*/
        double dd[2];
        dd[0] = p1 + p2 - 2 * dL2;
        dd[1] = 2 * dR2 - p1 - p2;

        if ( dd[0]  <  dd[1] )
        {
/*
        +----------+
        |          |
        |      +---+----+
        |      |        |
        |      |   1    |
        |      +--------+
        |
        |      
        |  +---------------+
        |  |               |
        |  |       2       |
        |  +------+--------+
        |         |
        +---------+
*/
            _CalcONNN_OutSide(pList, l1, l2, dL2, bRotate);
        }
        else
        {
/*
                +---------+
                |         |
            +---+----+    |
            |        |    |
            |   1    |    |
            +--------+    |
                          |
                          |
        +---------------+ |
        |               | |
        |       2       | |
        +------+--------+ |
                |         |
                +-------- +
*/
            _CalcONNN_OutSide(pList, l1, l2, dR2, bRotate);
        }
    }
}


//90°片側のみ見える   衝突なし
void CDrawingConnectionLine::_CalcALNN(std::vector<POINT_DATA>* pList, 
                                        const LINE2D& l1,
                                        const LINE2D& l2,
                                        const RECT2D& rc1,
                                        const RECT2D& rc2,
                                        bool bRotate)
{
/*
          +--------+
          |        |
        +-+← 2    | +--------+
        | |        | |        |
        | |        | |   +----+---+
        | +--------+ |   |   ↑   |
        +------------+   |   1    |
                         |        |
                         +--------+
*/

    POINT2D ptN;
    LINE2D lN1  = l1.NormalLine(l1.GetPt2());
    std::vector<POINT2D> ptList;
    if(_Intersection(&ptList, lN1, rc2, true))
    {
        double dMin = DBL_MAX;
        double dL;



        for(POINT2D& ptC: ptList)
        {
            dL = l1.GetPt2().Distance(ptC);
            if(dL < dMin)
            {
                dMin = dL;
                ptN = ptC;
            }
        }

        if(rc2.IsOverlap(rc1))
        {

            LINE2D lN2  = l2.NormalLine(l2.GetPt2());

            LINE2D lB;
            POINT2D ptB;
            POINT2D ptC;
            auto eEdge = _GetLineDir(l1);

            if(rc2.GetrLine(&lB, eEdge))
            {
                bool bRet1, bRet2;
                bRet1 = lB.IntersectInfnityLine(&ptB,  l1);
                bRet2 = lB.IntersectInfnityLine(&ptC,  lN2);


                pList->push_back(POINT_DATA(ptB));
                pList->push_back(POINT_DATA(ptC));
                pList->push_back(POINT_DATA(l2.GetPt2()));
#ifdef _DEBUG_CONNECTION
DB_PRINT(_T("_CalcALNN_1\n "));
#endif
                return;
            }

        }


        pList->push_back(POINT_DATA(l1.GetPt2()));
        pList->push_back(POINT_DATA(ptN));

        if (bRotate)
        {
            double d1 = rc2.dTop - ptN.dY;
            double d2 = ptN.dY - rc2.dBottom;

            if (d1 < d2)
            {
                pList->push_back(POINT_DATA(ptN.dX, rc2.dTop));
                pList->push_back(POINT_DATA(l2.GetPt2().dX, rc2.dTop));
            }
            else
            {
                pList->push_back(POINT_DATA(ptN.dX, rc2.dBottom));
                pList->push_back(POINT_DATA(l2.GetPt2().dX, rc2.dBottom));
            }
        }
        else
        {
            double d1 = rc2.dRight - ptN.dX;
            double d2 = ptN.dX - rc2.dLeft;

            if (d1 < d2)
            {
                pList->push_back(POINT_DATA(rc2.dRight, ptN.dY ));
                pList->push_back(POINT_DATA(rc2.dRight, l2.GetPt2().dY));
            }
            else
            {
                pList->push_back(POINT_DATA(rc2.dLeft, ptN.dY ));
                pList->push_back(POINT_DATA(rc2.dLeft, l2.GetPt2().dY));
            }
        }
        pList->push_back(POINT_DATA(l2.GetPt2()));
#ifdef _DEBUG_CONNECTION
DB_PRINT(_T("_CalcALNN_2\n "));
#endif
        return;
    }
  /*
                 +--------+
                 |        |
               +-+← 2    |
               | |        |
               | +--------+
               |
               |
               +-------------------- +
                                     |
                                +----+---+
                                |   ↑   |
                                |   1    |
                                |        |
                                +--------+
   */  

    LINE2D lN2  = l2.NormalLine(l2.GetPt2());

    if(lN1.IntersectInfnityLine(&ptN, lN2))
    {
        pList->push_back(POINT_DATA(l1.GetPt2()));
        pList->push_back(POINT_DATA(ptN));
        pList->push_back(POINT_DATA(l2.GetPt2()));
#ifdef _DEBUG_CONNECTION
 DB_PRINT(_T("_CalcALNN_3\n "));
#endif
        return;
    }
}



//90°片側のみ見える   衝突あり
void CDrawingConnectionLine::_CalcALNC(std::vector<POINT_DATA>* pList, 
                        const LINE2D& l1,
                        const LINE2D& l2,
                        const RECT2D& rc1,
                        const RECT2D& rc2,
                        bool bRotate)
{
    /*
          +--------+
          |        |
        +-+← 2    |
        | |        |
        | +--------+
        +---------+
                  |
             +----+---+
             |   ↑   |
             |   1    |
             |        |
             +--------+
    */

    POINT2D ptC;
    STD_ASSERT(_Intersection( &ptC, l1, rc2, false));

    POINT_DATA pd(ptC);
    pList->push_back(pd);
    if (bRotate)
    {
        pList->push_back(POINT_DATA(l2.GetPt2().dX, ptC.dY));
    }
    else
    {
        pList->push_back(POINT_DATA(ptC.dX, l2.GetPt2().dY));
    }
    pList->push_back(POINT_DATA(l2.GetPt2()));
    return;
}


//90°互いが見えない 衝突なし  / 有り
void CDrawingConnectionLine::_CalcANNN(std::vector<POINT_DATA>* pList, 
                        const LINE2D& l1,
                        const LINE2D& l2,
                        const RECT2D& rc1,
                        const RECT2D& rc2,
                        bool bRotate)
{
    /*
       +-----------------+
       |                 |
       |   +-------------+---+
       |   |            ↑   |
       |   |            2    |
       |   |                 |
       |   +-----------------+
       +-------+
               |
               | +--------+
               | |        |
               +-+← 1    |
                 |        |
                 +--------+
    */

    LINE2D lN1  = l1.NormalLine(l1.GetPt2());
    LINE2D lN2  = l2.NormalLine(l2.GetPt2());

     POINT2D pt1;
     POINT2D pt2;

    bool b1 = _Intersection( &pt1, lN1, rc2, false);

    if(b1)
    {
        LINE2D lB;
        POINT2D ptB;
        POINT2D ptC;
        auto eEdge = _GetLineDir(l1);

        if(rc2.GetrLine(&lB, eEdge))
        {
            bool bRet1, bRet2;
            bRet1 = lB.IntersectInfnityLine(&ptB,  l1);
            bRet2 = lB.IntersectInfnityLine(&ptC,  lN2);

            pList->push_back(POINT_DATA(ptB));
            pList->push_back(POINT_DATA(ptC));
            pList->push_back(POINT_DATA(l2.GetPt2()));
        }
#ifdef _DEBUG_CONNECTION
DB_PRINT(_T("_CalcANNN_1\n"));
#endif
        return;
    }


    bool b2 = _Intersection( &pt2, lN2, rc1, false);
    if(b2)
    {
        LINE2D lB;
        POINT2D ptB;
        POINT2D ptC;
        auto eEdge = _GetLineDir(l2);

        if(rc1.GetrLine(&lB, eEdge))
        {
            bool bRet1, bRet2;
            bRet1 = lB.IntersectInfnityLine(&ptB,  l2);
            bRet2 = lB.IntersectInfnityLine(&ptC,  lN1);

            pList->push_back(POINT_DATA(ptB));
            pList->push_back(POINT_DATA(ptC));
            pList->push_back(POINT_DATA(l1.GetPt2()));
            std::reverse(pList->begin(), pList->end());
#ifdef _DEBUG_CONNECTION
DB_PRINT(_T("_CalcANNN_2\n"));
#endif
        }
        return;
    }

    if(!b1 && !b2)
    {
        POINT2D ptC;
        STD_ASSERT(lN1.Intersect2(&ptC,lN2));
        pList->push_back(POINT_DATA(l1.GetPt2()));
        pList->push_back(POINT_DATA(ptC));
        pList->push_back(POINT_DATA(l2.GetPt2()));
#ifdef _DEBUG_CONNECTION
DB_PRINT(_T("_CalcANNN_3\n"));
#endif
        return;
    }
   
#ifdef _DEBUG_CONNECTION
DB_PRINT(_T("_CalcANNN_4\n"));
#endif
    return;
}

//同方向   衝突あり
void CDrawingConnectionLine::_CalcSNNC(std::vector<POINT_DATA>* pList, 
                        const LINE2D& l1,
                        const LINE2D& l2,
                        const RECT2D& rc1,
                        const RECT2D& rc2,
                        bool bRotate)
{

    POINT2D ptC;

    STD_ASSERT (_Intersection( &ptC, l1, rc2, false));

    /*      +------+
            |      |
            | +----+---+
            | |   ↑   |
            | |   2    |
            | |        |
            | +--------+
            +------+
                   |
              +----+---+
              |   ↑   |
              |   1    |
              |        |
              +--------+
*/

    pList->push_back(POINT_DATA(ptC));

    if (bRotate)
    {
        double d1 = rc2.dRight - ptC.dX;
        double d2 = ptC.dX - rc2.dLeft;

        if (d1 < d2)
        {
            pList->push_back(POINT_DATA(rc2.dRight, ptC.dY ));
            pList->push_back(POINT_DATA(rc2.dRight, l2.GetPt2().dY));
        }
        else
        {
            pList->push_back(POINT_DATA(rc2.dLeft, ptC.dY ));
            pList->push_back(POINT_DATA(rc2.dLeft, l2.GetPt2().dY));
        }
    }
    else
    {
        double d1 = rc2.dTop - ptC.dY;
        double d2 = ptC.dY - rc2.dBottom;

        if (d1 < d2)
        {
            pList->push_back(POINT_DATA(ptC.dX, rc2.dTop));
            pList->push_back(POINT_DATA(l2.GetPt2().dX, rc2.dTop));
        }
        else
        {
            pList->push_back(POINT_DATA(ptC.dX, rc2.dBottom));
            pList->push_back(POINT_DATA(l2.GetPt2().dX, rc2.dBottom));
        }
    }
    pList->push_back(POINT_DATA(l2.GetPt2()));
    return;

}


//同方向   衝突なし
void CDrawingConnectionLine::_CalcSLLN(std::vector<POINT_DATA>* pList, 
                        const LINE2D& l1,
                        const LINE2D& l2,
                        const RECT2D& rc1,
                        const RECT2D& rc2,
                        bool bRotate,
                        bool bInside)
{
/*

         +-------+
         |       |
         |  +----+---+
         |  |   ↑   |
         |  |   2    |
         |  |        |
         |  +--------+
         |
    +----+---+
    |   ↑   |
    |   1    |
    |        |
    +--------+
*/

    if (bRotate)
    {
        pList->push_back(POINT_DATA(l1.GetPt2().dX, l2.GetPt2().dY));
    }
    else
    {
        pList->push_back(POINT_DATA(l2.GetPt2().dX, l1.GetPt2().dY));
    }
         
    pList->push_back(POINT_DATA(l2.GetPt2()));
    return;
}


bool CDrawingConnectionLine::OnReleaseNodeData(const CDrawingObject* pObj, int iConnectionId)
{
    if(iConnectionId < 0){return false;}
    if(iConnectionId > 1){return false;}
    
    if (!pObj)
    {
        return false;
    }

    int iId = pObj->GetId();
    STD_ASSERT(m_c[iConnectionId].cd->lstPlug[0].iId == iId);
    
    m_c[iConnectionId].cd->lstPlug[0].iId = -1;
    m_c[iConnectionId].cd->lstPlug[0].iIndex = -1;

    return true;
}

/**
 * @brief   領域取得
 * @param   なし
 * @retval  なし
 * @note	描画領域を取得する
 */
RECT2D CDrawingConnectionLine::GetBounds() const
{
    //TODO: 実装
    RECT2D rc;
    rc.ExpandArea(m_c[0].pt);
    rc.ExpandArea(m_c[1].pt);

    if ((m_eType == E_MULTI) ||
        (m_eType == E_SPLINE))
    {
        for(auto pd: m_lstMidPoint)
        {
            rc.ExpandArea(pd.pt);
        }
    }
    return rc;
}

/**
 * @brief   距離
 * @param   [in] pt 
 * @retval  近接点からの距離
 * @note
 */
double CDrawingConnectionLine::Distance(const POINT2D& pt) const
{
    if (m_eType == E_STRAIGHT) 
    {


    }
    else if(m_eType == E_MULTI)
    {


    }
    else if(m_eType == E_SPLINE)
    {

    }



    return DBL_MAX;
}



/**
 *  @brief  線幅設定.
 *  @param  [in]    iWidth   線幅
 *  @retval         なし
 *  @note
 */
void CDrawingConnectionLine::SetLineWidth(int iWidth)
{
    m_iWidth = iWidth;
}

/**
 *  線幅取得.
 *  @param          なし
 *  @retval         線幅
 *  @note
 */
int CDrawingConnectionLine::GetLineWidth() const
{
    return m_iWidth;
}

/**
 *  線種設定.
 *  @param  [in]    iType   線種
 *  @retval         なし
 *  @note
 */
void CDrawingConnectionLine::SetLineType(int iType)
{
    m_iLineType = iType;
}

/**
 *  線種取得.
 *  @param          なし 
 *  @retval         線種
 *  @note
 */
int  CDrawingConnectionLine::GetLineType() const
{
    return m_iLineType;
}

StdString CDrawingConnectionLine::GetLineFormToStr(CDrawingConnectionLine::E_TYPE eType)
{
    switch(eType)
    {
    case CDrawingConnectionLine::E_STRAIGHT:{ return GET_STR(STR_PRO_LINE_FORM_STRAIGHT);}
    case CDrawingConnectionLine::E_MULTI:{ return GET_STR(STR_PRO_LINE_FORM_MULTI);}
    case CDrawingConnectionLine::E_SPLINE:{ return GET_STR(STR_PRO_LINE_FORM_SPLINE);}
    default:
        break;
    }
    return _T("");
}


/**
 *  @brief   プロパティ初期化.
 *  @param   [in] pCtrl オブジェクトコントロールへのポインタ
 *  @retval     なし
 *  @note
 */
TREE_GRID_ITEM*  CDrawingConnectionLine::_CreateStdPropertyTree()
{
    NAME_TREE_ITEM<CStdPropertyItem>* pBeforeGroup;
    NAME_TREE_ITEM<CStdPropertyItem>* pNextItem;
    NAME_TREE_ITEM<CStdPropertyItem>* pGroupItem;
    NAME_TREE<CStdPropertyItem>* pTree;
    //int iId;
    CStdPropertyItem* pItem;
    boost::tuple< CStdPropertyItem*, void*> pairVal;


    pBeforeGroup = CDrawingObject::_CreateStdPropertyTree();
    pTree = m_psProperty->GetTreeInstance();

    pGroupItem  = pTree->AddNext(pBeforeGroup, NULL, GET_STR(STR_PRO_CONNECTION_LINE), _T("ConnectionLine"));



    //------------
    //始点
    //------------
    CStdPropertyItemDef DeStart(PROP_POINT2D, 
        GET_STR(STR_PRO_START)   , 
        _T("Start"),
        GET_STR(STR_PRO_INFO_START), 
        true,
        DISP_UNIT,
        POINT2D());

    pItem = new CStdPropertyItem( DeStart, PropStart, m_psProperty, NULL, &m_c[0].pt);
    pNextItem = pTree->AddChild(pGroupItem, pItem, DeStart.strDspName, pItem->GetName(), 2);

    //------------
    //終点
    //------------
    CStdPropertyItemDef DefEnd(PROP_POINT2D, 
        GET_STR(STR_PRO_END)   , 
        _T("End"),
        GET_STR(STR_PRO_INFO_END), 
        true,
        DISP_UNIT,
        POINT2D());

    pItem = new CStdPropertyItem( DefEnd, PropEnd, m_psProperty, NULL, &m_c[1].pt);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefEnd.strDspName, pItem->GetName(), 2);

    //------------
    // 
    //------------
    StdString strLineForm;
    std::vector<E_TYPE> lstLineForm = {
        E_STRAIGHT,        
        E_MULTI,
        E_SPLINE
    };

    for (E_TYPE eType: lstLineForm)
    {
        strLineForm += GetLineFormToStr(eType);
        strLineForm += _T(":");  
        strLineForm += boost::lexical_cast<StdString>(eType);  
        if (eType != E_SPLINE)
        {
            strLineForm += _T(",");
        }
    }

    CStdPropertyItemDef DefFormType(PROP_DROP_DOWN_ID, 
        GET_STR(STR_PRO_LINE_FORM)   , 
        _T("LineForm"), 
        GET_STR(STR_PRO_INFO_LINE_FORM), 
        false,                                       
        DISP_NONE,
        static_cast<int>(m_eType),
        strLineForm,
        0);

    pItem = new CStdPropertyItem(
        DefFormType, 
        PropLineForm, 
        m_psProperty, 
        NULL,
        (void*)&m_eType);
    
    pNextItem = pTree->AddNext(pNextItem, 
        pItem, 
        DefFormType.strDspName, 
        pItem->GetName());


    //------------
    //矢印形状 始点
    //------------
    using namespace VIEW_COMMON;
    StdString strArrowTypeList;
    std::vector<E_ARROW_TYPE> lstArrowType = {
        AT_NONE,        
        AT_DOT,
        AT_DOT_FILL,
        AT_OBLIQUE,
        AT_OPEN,
        AT_FILLED,
#ifdef _DEBUG   //デバッグ用
        AT_DATUM_BLANK,   
        AT_DATUM_FILL
#endif
    };

    for (E_ARROW_TYPE eType: lstArrowType)
    {
        strArrowTypeList += GetArrowTypeToStr(eType);
        strArrowTypeList += _T(":");  
        strArrowTypeList += boost::lexical_cast<StdString>(eType);  
        if (eType != AT_DATUM_FILL)
        {
            strArrowTypeList += _T(",");
        }
    }

    CStdPropertyItemDef DefArrowStart(PROP_DROP_DOWN_ID, 
        GET_STR(STR_PRO_ARROW_TYPE_START)   , 
        _T("ArrowStart"), 
        GET_STR(STR_PRO_INFO_ARROW_TYPE_START), 
        false,                                       
        DISP_NONE,
        static_cast<int>(m_c[0].eArrowType),
        strArrowTypeList,
        0);

    pItem = new CStdPropertyItem(
        DefArrowStart, 
        PropArrowStart, 
        m_psProperty, 
        NULL,
        (void*)&m_c[0].eArrowType);
    
    pNextItem = pTree->AddNext(pNextItem, 
        pItem, 
        DefArrowStart.strDspName, 
        pItem->GetName());

    //矢印形状 終点
        CStdPropertyItemDef DefArrowEnd(PROP_DROP_DOWN_ID, 
        GET_STR(STR_PRO_ARROW_TYPE_END)   , 
        _T("ArrowEnd"), 
        GET_STR(STR_PRO_INFO_ARROW_TYPE_END), 
        false,                                       
        DISP_NONE,
        static_cast<int>(m_c[1].eArrowType),
        strArrowTypeList,
        0);

    pItem = new CStdPropertyItem(
        DefArrowEnd, 
        PropArrowEnd, 
        m_psProperty, 
        NULL,
        (void*)&m_c[1].eArrowType);
    
    pNextItem = pTree->AddNext(pNextItem, 
        pItem, 
        DefArrowEnd.strDspName, 
        pItem->GetName());


    //------------
    //線幅
    //------------
    CStdPropertyItemDef DefLineWidth(PROP_LINE_WIDTH, 
        GET_STR(STR_PRO_LINE_WIDTH)   ,
        _T("Width"),
        GET_STR(STR_PRO_INFO_LINE_WIDTH), 
        true,
        DISP_NONE,
        0);

    pItem = new CStdPropertyItem( DefLineWidth, PropWidth, m_psProperty, NULL, &m_iWidth);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefLineWidth.strDspName, pItem->GetName());

    //------------
    //線種
    //------------
    CStdPropertyItemDef DefLineType(PROP_LINE_TYPE, 
        GET_STR(STR_PRO_LINE_TYPE)   , 
        _T("Type"),
        GET_STR(STR_PRO_INFO_LINE_TYPE), 
        false,
        DISP_NONE,
        0);

    pItem = new CStdPropertyItem( DefLineType, PropLineType, m_psProperty, NULL, &m_iLineType);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefLineType.strDspName, pItem->GetName());

    //------------
    //接続先1
    //------------
    CStdPropertyItemDef DefConnect1(PROP_STR, 
        GET_STR(STR_PRO_CONNECT1),
        _T("Connect1"),
        GET_STR(STR_PRO_INFO_CONNECT1), 
        false,
        DISP_NONE,
        0);

    pItem = new CStdPropertyItem( DefConnect1, 
                                  NULL,//PropConnct1, 
                                  m_psProperty, 
                                  NULL, 
                                  &m_cName[0]);
    pNextItem = pTree->AddNext(pNextItem, 
                                pItem, 
                                DefConnect1.strDspName, 
                                pItem->GetName());

    //------------
    //接続先2
    //------------
    CStdPropertyItemDef DefConnect2(PROP_STR, 
        GET_STR(STR_PRO_CONNECT2),
        _T("Connect2"),
        GET_STR(STR_PRO_INFO_CONNECT2), 
        false,
        DISP_NONE,
        0);

    pItem = new CStdPropertyItem( DefConnect2, 
                                  NULL,//PropConnct1, 
                                  m_psProperty, 
                                  NULL, 
                                  &m_cName[1]);
    pNextItem = pTree->AddNext(pNextItem, 
                                pItem, 
                                DefConnect2.strDspName, 
                                pItem->GetName());

    return pGroupItem;
}


/**
 *  @brief   プロパティ値更新
 *  @param   なし
 *  @retval  なし
 *  @note    
 */
void CDrawingConnectionLine::_PrepareUpdatePropertyGrid()
{
    CDrawingObject::_PrepareUpdatePropertyGrid();

}

//接続先の位置を取得する                              
bool CDrawingConnectionLine::_GetConnectionData(POINT2D* pPt, int iNo, CNodeData** ppConnectionData)
{
    if(iNo < 0 || iNo > 1)
    {
        return false;
    }

    auto pNd = GetNodeData(iNo); 
    if(!pNd)
    {
        return false;
    }

    if(pNd->lstPlug.empty())
    {
        return false;
    }

    int iId = pNd->lstPlug[0].iId;
    if (iId == -1)
    {
        return false;
    }


    CPartsDef* pDef  = GetPartsDef();
    if (!pDef)
    {
        return false;
    }

    auto pObj = pDef->GetObjectById(iId, true /*bIngnoreGroup*/);
    if(!pObj)
    {
        return false;
    }

    int iConnectId = pNd->lstPlug[0].iIndex;
    if (iConnectId == -1)
    {
        return false;
    }

    if (ppConnectionData)
    {
        *ppConnectionData = pObj->GetNodeData(iConnectId);
    }

    bool bRet;
    bRet = pObj->GetNodeDataPos(pPt, iConnectId);
    return bRet;
}

/**
 *  @brief   プロパティ変更(端点1)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingConnectionLine::PropStart (CStdPropertyItem* pData, void* pObj)
{
    CDrawingConnectionLine* pDrawing = reinterpret_cast<CDrawingConnectionLine*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->m_c[0].pt = pData->anyData.GetPoint();
        pDrawing->_UpdatePosition(0);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(端点2)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingConnectionLine::PropEnd (CStdPropertyItem* pData, void* pObj)
{
    CDrawingConnectionLine* pDrawing = reinterpret_cast<CDrawingConnectionLine*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->m_c[1].pt = pData->anyData.GetPoint();
        pDrawing->_UpdatePosition(1);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   線幅
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingConnectionLine::PropWidth      (CStdPropertyItem* pData, void* pObj)
{
    CDrawingConnectionLine* pDrawing = reinterpret_cast<CDrawingConnectionLine*>(pObj);
    try
    {
        int iWidth;
        iWidth = pData->anyData.GetInt();
        pDrawing->AddChgCnt();
        pDrawing->SetLineWidth(iWidth);
        pDrawing->GetPartsDef()->Redraw();
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   線種
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingConnectionLine::PropLineType   (CStdPropertyItem* pData, void*pObj)
{
    CDrawingConnectionLine* pDrawing = reinterpret_cast<CDrawingConnectionLine*>(pObj);
    try
    {
        int iType;
        iType = pData->anyData.GetInt();
        pDrawing->AddChgCnt();
        pDrawing->SetLineType(iType);
        pDrawing->GetPartsDef()->Redraw();
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
    }
    return true;

}

/*
 *  @brief   プロパティ変更(直線形状)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingConnectionLine::PropLineForm (CStdPropertyItem* pData, void* pObj)
{
   using namespace VIEW_COMMON;
    CDrawingConnectionLine* pDrawing = reinterpret_cast<CDrawingConnectionLine*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        CDrawingConnectionLine::E_TYPE eType = static_cast<CDrawingConnectionLine::E_TYPE>(pData->anyData.GetInt());
        pDrawing->m_eType = eType;
        pDrawing->GetPartsDef()->Redraw();
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}


/*
 *  @brief   プロパティ変更(始点矢印種別)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingConnectionLine::PropArrowStart (CStdPropertyItem* pData, void* pObj)
{
   using namespace VIEW_COMMON;
    CDrawingConnectionLine* pDrawing = reinterpret_cast<CDrawingConnectionLine*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        VIEW_COMMON::E_ARROW_TYPE eType = static_cast<VIEW_COMMON::E_ARROW_TYPE>(pData->anyData.GetInt());
        pDrawing->m_c[0].eArrowType = eType;
        pDrawing->GetPartsDef()->Redraw();
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/*
 *  @brief   プロパティ変更(始点矢印種別)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingConnectionLine::PropArrowEnd (CStdPropertyItem* pData, void* pObj)
{
   using namespace VIEW_COMMON;
    CDrawingConnectionLine* pDrawing = reinterpret_cast<CDrawingConnectionLine*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        VIEW_COMMON::E_ARROW_TYPE eType = static_cast<VIEW_COMMON::E_ARROW_TYPE>(pData->anyData.GetInt());
        pDrawing->m_c[1].eArrowType = eType;
        pDrawing->GetPartsDef()->Redraw();
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   スナップ点取得.
 *  @param   [out] pLstSnap スナップ点リスト
 *  @retval  true: スナップ点あり
 *  @note
 */
bool CDrawingConnectionLine::GetSnapList(std::list<SNAP_DATA>* pLstSnap)const 
{
    using namespace VIEW_COMMON;

    DWORD dwSnap = DRAW_CONFIG->GetSnapType();
    bool bRet = false;

    STD_ASSERT(pLstSnap);

    if (!pLstSnap)
    {
        return false;
    }

    pLstSnap->clear();
    SNAP_DATA snapData;
    snapData.bKeepSelect = false;

    bool bFeature = (dwSnap & SNP_FEATURE_POINT) == SNP_FEATURE_POINT;
    bool bNode    = (dwSnap & SNP_NODE) == SNP_NODE;
    bool bEnd     = (dwSnap & SNP_END_POINT) == SNP_END_POINT;

    //同じ座標は登録しない
    std::map<POINT2D, SNAP_DATA> mapSnap;

    if((m_eType == E_MULTI) ||
       (m_eType == E_SPLINE))
    {
        if (bFeature)
        {
            for(auto pd: m_lstMidPoint)
            {
                snapData.eSnapType = SNP_FEATURE_POINT;
                snapData.pt = pd.pt;
                mapSnap[snapData.pt] = snapData;
                bRet = true;
            }
        }
    }

    if (bEnd)
    {
        snapData.eSnapType = SNP_END_POINT;
        snapData.pt = m_c[0].pt;
        mapSnap[snapData.pt] = snapData;

        snapData.pt = m_c[1].pt;
        mapSnap[snapData.pt] = snapData;
        bRet = true;
    }

    if(bNode)
    {
        snapData.eSnapType = SNP_NODE;
        for(int iNo = 0; iNo < GetNodeDataNum(); iNo++)
        {
            GetNodeDataPos(&snapData.pt, iNo);
            snapData.iConnectionIndex = iNo;
            snapData.pConnection = std::make_shared<CNodeData>(_DefaultConnection());

            mapSnap[snapData.pt] = snapData;
            bRet = true;
        }
    }

    for(auto& ite: mapSnap)
    {
        pLstSnap->push_back(ite.second);
    }

    return bRet;
}

bool CDrawingConnectionLine::OnSetCursor(CNodeMarker* pNodeMarker, CDrawingView* pView)
{
    //AfxGetApp()->LoadCursor, AfxGetApp()->LoadStandardCursorを使用すること

	if (pView->IsDrag())
	{
		return false;
	}

    if(m_bDispNoIcon)
    {
        ::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_NO));
        return true;
    }


    /*
    StdString strMarkerId = pNodeMarker->GetMarkerId();
    int iNode = _GetNodeId(&strMarkerId);

    //端点が接続されている場合は分割する
    if((iNode == ND_START) ||
       (iNode == ND_END))
    {
        ::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_SIZEALL));
        return true;
    }
    else if (iNode >= ND_MID)
    {
        int iVal = iNode - ND_MID;
        int iSize = SizeToInt(m_lstMidPoint.size());
        CDrawingNode* pNode = pMarker->GetDrawingNode(strMarkerId);
 
        if (iVal == 0)
        {
 

        }
        else if( iVal == iSize)
        {
        }
    }
    */
	return true;
}

CNodeData CDrawingConnectionLine::_DefaultConnection() const
{
      CNodeData cd;
      cd.iMaxSocket = -1;
      cd.eConnectionType = E_ACTIVE;
      cd.eConnectionObject = CNodeData::NODE;
      cd.eConnectionDirection = E_INOUT;
      cd.eLineDirection = CNodeData::E_NORM_OBJECT_BOUNDS; 


      //テスト用
      cd.nodeProperty.m_eShape = DMS_RECT_FILL;
      return cd;
}

bool CDrawingConnectionLine::CreateNodeData(int iConnectionId)
{
    if (iConnectionId < 0)
    {
        return false;
    }

    if (iConnectionId < 2)
    {
        if (!m_c[iConnectionId].cd)                                    
        {
            m_c[iConnectionId].cd = std::make_unique<CNodeData>(_DefaultConnection());
            m_c[iConnectionId].cd->ptPos =   m_c[iConnectionId].pt;
            m_c[iConnectionId].cd->lstPlug.push_back(CBindingPartner());
        }
        return true;
    }

    int iMid =  iConnectionId - 2;

    if (iMid >= m_lstMidPoint.size()) 
    {
        return false;
    }

    if((m_eType == E_MULTI) ||
       (m_eType == E_SPLINE))
    {
        if(!m_lstMidPoint[iMid].cd)
        {
            m_lstMidPoint[iMid].cd = std::make_unique<CNodeData>(_DefaultConnection());
            m_lstMidPoint[iMid].cd->ptPos =   m_lstMidPoint[iMid].pt;
        }
        return true;
    }

    return true;
}

CNodeData* CDrawingConnectionLine::GetNodeData(int iConnectionId) 
{
   if (iConnectionId < 0)
    {
        return NULL;
    }

    if (iConnectionId < 2)
    {
        return m_c[iConnectionId].cd.get();
    }

    int iMid =  iConnectionId - 2;

    if (iMid >= m_lstMidPoint.size()) 
    {
        return NULL;
    }

    if((m_eType == E_MULTI) ||
       (m_eType == E_SPLINE))
    {
        return m_lstMidPoint[iMid].cd.get();
    }
    return NULL;
}


int CDrawingConnectionLine::GetNodeDataNum() const 
{
    int iTotal = 2;

   if((m_eType == E_MULTI) ||
       (m_eType == E_SPLINE))
    {
        iTotal += SizeToInt(m_lstMidPoint.size());
    }

    return iTotal;
}

bool CDrawingConnectionLine::GetNodeDataPos(POINT2D* pt, int iConnectionId) const 
{
    if (iConnectionId < 0)
    {
        return false;
    }

    if (iConnectionId < 2)
    {
        if (m_pMatOffset)
        {

            *pt = m_c[iConnectionId].pt * (*m_pMatOffset);
        }
        else
        {
            *pt = m_c[iConnectionId].pt;
        }
        return true;
    }

    int iMid =  iConnectionId - 2;

    if (iMid >= m_lstMidPoint.size()) 
    {
        return false;
    }

    if((m_eType == E_MULTI) ||
       (m_eType == E_SPLINE))
    {
        if (m_pMatOffset)
        {

            *pt = m_lstMidPoint[iMid].pt * (*m_pMatOffset);
        }
        else
        {
            *pt = m_lstMidPoint[iMid].pt;
        }
        return true;
    }
    return false;
}

bool CDrawingConnectionLine::_GetNextPoint(POINT2D* pPt, int iConnectionId ) const 
{
    //E_MULTIのみ
    if (iConnectionId == 0)
    {
        if (!m_lstMidPoint.empty())
        {
            *pPt =    m_lstMidPoint[0].pt;
            return true;
        }
        *pPt = m_c[1].pt;
        return true;
    }
    else if (iConnectionId == 1)
    {
        return false;
    }
    int iSize = SizeToInt(m_lstMidPoint.size());
    int iNo = iConnectionId - 2;

    if ( (iNo + 1) ==  iSize)
    {
         *pPt = m_c[1].pt;
        return true;
    }
    else if ( (iNo + 1) >  iSize)
    {
        return false;
    }
           

    *pPt =  m_lstMidPoint[iNo + 1].pt;
    return true;
}

bool CDrawingConnectionLine::_GetPriviousPoint(POINT2D* pPt, int iConnectionId ) const 
{
    //E_MULTIのみ
    int iSize = SizeToInt(m_lstMidPoint.size());
    if (iConnectionId == 1)
    {
        if (!m_lstMidPoint.empty())
        {
            *pPt =    m_lstMidPoint[iSize - 1].pt;
            return true;
        }
        *pPt = m_c[0].pt;
        return true;
    }
    else if (iConnectionId == 0)
    {
        return false;
    }

    int iNo = iConnectionId - 2;

    if(iNo == 0)
    {
        *pPt = m_c[0].pt;
      return true;

    }
    else if ( (iNo - 1) <  0)
    {
        return false;
    }
         
    *pPt =  m_lstMidPoint[iNo - 1].pt;
    return true;

}


void CDrawingConnectionLine::_GetEdgeLines(std::map<RECT2D::E_EDGE, LINE2D>* pDir,
                                                            const POINT2D& pt,
                                                            double  dSnapLen,
                                                            RECT2D::E_EDGE edge) const 
{

    RECT2D::E_EDGE eg[] = {RECT2D::E_TOP, RECT2D::E_RIGHT, RECT2D::E_BOTTOM, RECT2D::E_LEFT};

    auto GetEdgeLine = [&](RECT2D::E_EDGE eEdge, POINT2D ptFix)
    {
        LINE2D l;
        if(eEdge == RECT2D::E_TOP)
        {
            l.SetPt(ptFix, POINT2D(ptFix.dX, ptFix.dY + dSnapLen));
        }
        else if(eEdge == RECT2D::E_RIGHT)
        {
            l.SetPt(ptFix, POINT2D(ptFix.dX + dSnapLen, ptFix.dY));

        }
        else if(eEdge == RECT2D::E_BOTTOM)
        {
            l.SetPt(ptFix, POINT2D(ptFix.dX, ptFix.dY - dSnapLen));

        }
        else if(eEdge == RECT2D::E_LEFT)
        {
            l.SetPt(ptFix, POINT2D(ptFix.dX -  dSnapLen, ptFix.dY));
        }
        return l;
    };

    std::pair<RECT2D::E_EDGE, LINE2D> pair;

    for( auto e: eg)
    {
        if (( e & edge) == e)
        {
            pair.first = e;
            pair.second = GetEdgeLine(e, pt); 
            pDir->insert(pair);
        }
    }
}


RECT2D::E_EDGE CDrawingConnectionLine::InquireConnectDirection(std::map<RECT2D::E_EDGE, LINE2D>* pDir,
                                                               RECT2D* pRc,
                                                               int iConnectionId ) const 
{
    if (iConnectionId < 0)
    {
        return RECT2D::E_NONE;
    }

    double dSnapLen = DRAW_CONFIG->DimToDispUnit(DRAW_CONFIG->dConnectionStraghtLength);

     POINT2D pt;
    if(!GetNodeDataPos(&pt, iConnectionId))
    {
        return RECT2D::E_NONE;
    }

    if(m_eType == E_STRAIGHT) 
    {
        _GetEdgeLines(pDir, pt, dSnapLen, RECT2D::E_ALL);
        pRc->SetPoint(pt);
        pRc->Expansion(dSnapLen);
 
        return RECT2D::E_ALL;
    }
    else  if(m_eType == E_MULTI) 
    {
       int iAll= RECT2D::E_ALL;

       RECT2D r(pt,pt);

       POINT2D ptPre;
       POINT2D ptVec;

       if(_GetPriviousPoint(&ptPre, iConnectionId ))
       {
            RECT2D::E_EDGE eg;
            ptVec = ptPre - pt;
            eg = _GetPointDir(ptVec);
            iAll = iAll ^ eg;
            r.ExpandArea(ptPre);
       }
        
       POINT2D ptNext;
       if(_GetNextPoint(&ptNext, iConnectionId ))
       {
            RECT2D::E_EDGE eg;
            ptVec = ptNext - pt;
            eg = _GetPointDir(ptVec);
            iAll = iAll ^ eg;
            r.ExpandArea(ptPre);
       }

       RECT2D::E_EDGE eAll = static_cast<RECT2D::E_EDGE>(iAll);
       _GetEdgeLines(pDir, pt, dSnapLen, eAll);

       *pRc = r;
       return eAll;
   }

    return RECT2D::E_NONE;
}



int CDrawingConnectionLine::_GetNodeId(const StdString* pStr)
{
    if (*pStr == _T("START"))
    {
        return ND_START;
    }
    else if (*pStr == _T("END"))
    {
         return ND_END;
    }
    else  if(*pStr != _T(""))
    {
        int iVal = boost::lexical_cast<int>(*pStr);
        return iVal + ND_MID;
    }
    return -1;
}

bool CDrawingConnectionLine::_GetNorm( POINT2D* ptMid, 
                    POINT2D* ptNorm, 
                    const POINT2D& ptS,  
                    const POINT2D& ptE)
{
    *ptMid = (ptS + ptE) / 2.0; 
    LINE2D l(ptS, ptE) ;
    *ptNorm = l.Normal(); 
  
    return true;
};

//!< マーカ初期化 true:マーカあり
bool CDrawingConnectionLine::InitNodeMarker(CNodeMarker* pMarker)
{
    CDrawingView* pView = pMarker->GetView();
    pView->ClearDraggingNodes();
    pView->ClearDraggingTmpObjects();

    auto lstNode = pView->GetDraggingNodes();


    m_bDragDiv = false;

    if((m_eType == E_MULTI) ||
       (m_eType == E_SPLINE))
    {
        if (!m_lstMidPoint.empty())
        {
            size_t iSize = 2 + m_lstMidPoint.size() + 1;       
            lstNode->resize( iSize );
        }
    }
    else
    {
        lstNode->resize(2);
    }

    int iNo = 0;
    for(auto& pNode: *lstNode)
    {
        pNode = std::make_unique<CDrawingNode>();
        pNode->SetVisible(true);
        if (iNo < ND_MID)
        {
            pNode->SetMarkerShape(DMS_CIRCLE_FILL);
        }
        else
        {
            pNode->SetMarkerShape(DMS_RECT_FILL);
            pNode->SetMarkerMove(DMT_LIMITED_DIRECTION);
        }
        iNo++;
    }

    STD_ASSERT(pMarker);
    double dLen = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->GetMarkerSize());  
    int iR =  CUtil::Round(dLen * pView->GetDpu());
    pMarker->SetRadius(iR);
    pMarker->SetObject(shared_from_this());
  
	//マウスカーソル設定コールバック
    m_bDispNoIcon = false;
    namespace  PLC = std::placeholders;
	pMarker->SetOnSetcursorCallback(std::bind(&CDrawingConnectionLine::OnSetCursor, this, PLC::_1, PLC::_2));
  

   //ドラッグ表示用の図形を用意する
    auto lstTmpObject = pView->GetDraggingTmpObjects();
    lstTmpObject->push_back(std::make_unique<CDrawingConnectionLine>(*this)); //表示用
    auto pConnectorTmp = std::dynamic_pointer_cast<CDrawingConnectionLine>(lstTmpObject->at(0));
    pConnectorTmp->SetColor(DRAW_CONFIG->crImaginary);

    lstNode->at(ND_START)->SetName(_T("START"));
    lstNode->at(ND_END)->SetName(_T("END"));

  	lstNode->at(ND_START)->SetPoint(GetStartSide());
	lstNode->at(ND_END)->SetPoint(GetEndSide());


   if((m_eType == E_MULTI) ||
      (m_eType == E_SPLINE))
   {
       if (!m_lstMidPoint.empty())
       {
            StdString str;
            int iVal = 2;
            POINT2D ptOld = GetStartSide();
            POINT2D ptMid;
            POINT2D ptNorm;
            for(auto pd: m_lstMidPoint)
            {
                str = boost::lexical_cast<StdString>(iVal - 2);
                lstNode->at(iVal)->SetName(str.c_str());

                lstNode->at(iVal)->SetMarkerCordinateSystem(E_WORLD);

                _GetNorm(&ptMid, &ptNorm, ptOld, pd.pt);
	            lstNode->at(iVal)->SetPoint(ptMid);
                lstNode->at(iVal)->SetMarkerDir(ptNorm);
                ptOld = pd.pt;
                iVal++;
            }
            str = boost::lexical_cast<StdString>(iVal - 2);
            lstNode->at(iVal)->SetName(str.c_str());
            lstNode->at(iVal)->SetMarkerCordinateSystem(E_WORLD);
            _GetNorm(&ptMid, &ptNorm, ptOld, GetEndSide());
	        lstNode->at(iVal)->SetPoint(ptMid);
            lstNode->at(iVal)->SetMarkerDir(ptNorm);
       }
   }

    for(auto& pNode: *lstNode)
    {
        pMarker->Create(pNode.get(), pNode->GetPoint());
    }

    return true;
}


//!< マーカ選択
 void CDrawingConnectionLine::SelectNodeMarker(CNodeMarker* pMarker, 
                       StdString strMarkerId)
{
    CDrawingView* pView = pMarker->GetView();
    CPartsDef*   pDef = pView->GetPartsDef();
 
    auto lstTmpObject = pView->GetDraggingTmpObjects();
    auto pConnectorTmp = std::dynamic_pointer_cast<CDrawingConnectionLine>(lstTmpObject->at(0));

    int iNode = _GetNodeId(&strMarkerId);

    //端点が接続されている場合は分割する
    if (iNode >= ND_MID)
    {
        int iVal = iNode - ND_MID;
        int iSize = SizeToInt(pConnectorTmp->m_lstMidPoint.size());
        
        CDrawingNode* pNode = pMarker->GetDrawingNode(strMarkerId);
 
        if (iVal == 0)
        {
            //Start
            CNodeData* pCdStart = _GetDistConnectionData(pDef, 0);
            if (pCdStart)
            {
                POINT2D ptSel = pNode->GetPoint();
                POINT2D ptMid,  ptNorm;
                            
                CDrawingNode drNode;
                drNode.SetMarkerCordinateSystem(E_WORLD);

				//--------------------
				// 新しいノードの生成
				//--------------------
				_GetNorm(&ptMid, &ptNorm, GetStartSide(), ptSel);
	            drNode.SetPoint(ptMid);
                drNode.SetMarkerDir(ptNorm);

                for (int i = 0; i < 2; ++i)
                {
					//中間点の挿入
                    auto ite =  pConnectorTmp->m_lstMidPoint.begin();
                    pConnectorTmp->m_lstMidPoint.insert(ite, ptMid);
                }

                for(int iNo = iSize; iNo >= 0; --iNo)
                {
					//ノード名称の変更
					//今までのノード名＋２
					StdString strOld = boost::lexical_cast<StdString>(iNo);
                    StdString strNew = boost::lexical_cast<StdString>(iNo + 2);
                    STD_ASSERT(pMarker->ChangeName(strOld, strNew));
                }

                for(int iNo = 0; iNo < 2; ++iNo)
                {
					//マーカ名称の挿入
					StdString str = boost::lexical_cast<StdString>(iNo);
                    drNode.SetName(str.c_str());

                    auto lstNode = pView->GetDraggingNodes();
                    lstNode->push_back(std::make_unique<CDrawingNode>(drNode)); 

                    auto iteDraggingNode =  lstNode->end();
                    iteDraggingNode--;
                    pMarker->Create((*iteDraggingNode).get(), (*iteDraggingNode)->GetPoint());
                }
                strMarkerId = boost::lexical_cast<StdString>(iNode + 2);
            }
        }
        else if( iVal == iSize)
        {
            CNodeData* pCdEnd = _GetDistConnectionData(pDef, 1);
            if (pCdEnd)
            {
                POINT2D ptSel = pNode->GetPoint();
                POINT2D ptMid,  ptNorm;
                            
                CDrawingNode drNode;
                drNode.SetMarkerCordinateSystem(E_WORLD);
                _GetNorm(&ptMid, &ptNorm, ptSel, GetEndSide());
	            drNode.SetPoint(ptMid);  //ptMidは
                drNode.SetMarkerDir(ptNorm);

                POINT_DATA pd;
                pd.pt = ptMid;
                for (int i = 0; i < 2; ++i)
                {
                    pConnectorTmp->m_lstMidPoint.push_back(pd);
                }

                for(int iNo = 0; iNo < 2; ++iNo)
                {
                    int iAddNo =  iNo + iSize;
                    StdString str = boost::lexical_cast<StdString>(iAddNo);
                    drNode.SetName(str.c_str());

                    auto lstNode = pView->GetDraggingNodes();
                    lstNode->push_back(std::make_unique<CDrawingNode>(drNode)); 

                    auto iteDraggingNode =  lstNode->end();
                    iteDraggingNode--;
                    pMarker->Create((*iteDraggingNode).get(), (*iteDraggingNode)->GetPoint());
                }
            }
        }
    }
    pMarker->SetVisibleOnly( strMarkerId, true);
}

CNodeData* CDrawingConnectionLine::_GetDistConnectionData(CPartsDef* pDef, int iSide)
{
    CNodeData* pCd = NULL;
    auto pObj = pDef->GetObjectById(m_c[iSide].cd->lstPlug[0].iId);
    if (pObj)
    {
        pCd = pObj->GetNodeData(m_c[iSide].cd->lstPlug[0].iIndex); 
    }                                                           
   return   pCd;  
}

//!< マーカ移動
void CDrawingConnectionLine::MoveNodeMarker(CNodeMarker* pMarker, 
                                 SNAP_DATA* pSnap,
                                 StdString strMarkerId,
                                 MOUSE_MOVE_POS posMouse)
{
    CDrawingView* pView = pMarker->GetView();
    CPartsDef*   pDef = pView->GetPartsDef();

    auto lstTmpObject = pView->GetDraggingTmpObjects();
    auto pConnectorTmp = std::dynamic_pointer_cast<CDrawingConnectionLine>(lstTmpObject->at(0));
 
    auto pObj1 = pDef->GetObjectById(pSnap->iObject1);
    
    CNodeData* pCdDest = NULL;
    if (pObj1)
    {
        pCdDest = pObj1->GetNodeData(pSnap->iConnectionIndex);
#if 1
        int  iDest = -1;
        if (pCdDest)
        {
            iDest = pCdDest->iIndex;
        }


        DB_PRINT(_T("MoveNodeMarker pObj1 = %s Index:%d pCdDest:%d   \n"), pObj1->Text().c_str(), 
                                                             pSnap->iConnectionIndex,
                                                             iDest
                                                            );
#endif
    }

    POINT2D ptCenter;
    int iNode = pConnectorTmp->_GetNodeId(&strMarkerId);
    CDrawingNode* pNode = pMarker->GetDrawingNode(strMarkerId);

    bool bNoIcon  = false;

    if (iNode == ND_START)
    {
        CNodeData* pCdEnd = pConnectorTmp->_GetDistConnectionData(pDef, 1);


#if 1
{
    int iId = m_c[1].cd->lstPlug[0].iId;
    int iIndex = m_c[1].cd->lstPlug[0].iIndex;
    DB_PRINT(_T("  反対側 ID:%d Index:%d  "), iId, iIndex);
    auto pObj = pDef->GetObjectById(iId);
    if (pObj)
    {
        auto pCd = pObj->GetNodeData(iIndex);
        if (!pCd)
        {
            DB_PRINT(_T("  ノードデータが見つかりません\n"));
        }
        else
        {
            DB_PRINT(_T("\n"));
        }
    }
    else
    {
        DB_PRINT(_T("  オブジェクトが見つかりません\n"));
    }
}
#endif
            
        bool bConnect  = false;
        if ( pCdDest)
        {
            if(pCdEnd)
            {
                if(pCdEnd->IsAbleToConnectEndToEnd(pCdDest))
                {
                    bConnect = true;
                }
                else
                {
                    bNoIcon = true;
                }
            }
            else
            {
                bConnect = true;
            }
        }

        if(!bConnect)
        {
            pConnectorTmp->SetConnectionStart(NULL, -1);
        }
        else
        {
            pConnectorTmp->SetConnectionStart(pObj1.get(), pSnap->iConnectionIndex);
#if 1
            DB_PRINT(_T("SetConnectionStart %d\n"), pSnap->iConnectionIndex);
#endif

        }

        pConnectorTmp->SetStart(pSnap->pt);
        pConnectorTmp->RecalcMidPoints(NULL, 0, false);
    }
    else if (iNode == ND_END)
    {
       CNodeData* pCdStart = pConnectorTmp->_GetDistConnectionData(pDef, 0);

#if 1
{
    int iId = m_c[0].cd->lstPlug[0].iId;
    int iIndex = m_c[0].cd->lstPlug[0].iIndex;
    DB_PRINT(_T("  反対側 ID:%d Index:%d  "), iId, iIndex);
    auto pObj = pDef->GetObjectById(iId);
    if (pObj)
    {
        auto pCd = pObj->GetNodeData(iIndex);
        if (!pCd)
        {
            DB_PRINT(_T("  ノードデータが見つかりません\n"));
        }
        else
        {
            DB_PRINT(_T("\n"));
        }
    }
    else
    {
        DB_PRINT(_T("  オブジェクトが見つかりません\n"));
    }
}
#endif

        bool bConnect  = false;
        if (pCdDest)
        {
            if(pCdStart)
            {
                if(pCdStart->IsAbleToConnectEndToEnd(pCdDest))
                {
                    bConnect = true;
                }
                else
                {
                    bNoIcon = true;
                }
            }
            else
            {
                bConnect = true;
            }
        }

        if(!bConnect)
        {
            pConnectorTmp->SetConnectionEnd(NULL, -1);
        }
        else
        {
            pConnectorTmp->SetConnectionEnd(pObj1.get(), pSnap->iConnectionIndex);
#if 1
            DB_PRINT(_T("SetConnectionEnd %d\n"), pSnap->iConnectionIndex);
#endif

        }

        pConnectorTmp->SetEnd(pSnap->pt);
        pConnectorTmp->RecalcMidPoints(NULL, 1, false);
    }
    else if (iNode != ND_NONE)
    {
        //中間ノード番号 
        int iMidNodeNo = iNode - ND_MID;

        size_t iSize = pConnectorTmp->m_lstMidPoint.size();
        POINT2D ptDir = pNode->GetMarkerDir();

        //------------------------------------
        //中間点を削除できる場合にスナップを行う
        //------------------------------------
        POINT2D ptSnap = pSnap->pt;
        if(!(posMouse.nFlags & MK_SHIFT))
        {
            double dSnap = pView->GetSnapRadius();
            double dStartSide = DBL_MAX;
            double dEndSide = DBL_MAX;
            POINT2D ptNext;
            POINT2D ptPre;

            bool bExitSnapableNode = false;
            if((iMidNodeNo + 1) < iSize)
            {
                ptNext = pConnectorTmp->m_lstMidPoint[iMidNodeNo + 1].pt;
                if (  fabs(ptDir.dY) > NEAR_ZERO)
                {
                    dEndSide = fabs(ptNext.dY - ptSnap.dY);
                }
                else
                {
                    dEndSide = fabs(ptNext.dX - ptSnap.dX);
                }
                bExitSnapableNode = true;
            }

            if (iMidNodeNo > 1)
            {
                ptPre = pConnectorTmp->m_lstMidPoint[iMidNodeNo - 2].pt;
                if (  fabs(ptDir.dY) > NEAR_ZERO)
                {
                    dStartSide = fabs(ptPre.dY - ptSnap.dY);
                }
                else
                {
                    dStartSide = fabs(ptPre.dX - ptSnap.dX);
                }
                bExitSnapableNode = true;
            }


            if (bExitSnapableNode)
            {
                double dMin;
                bool bStart = true;
                if(dStartSide < dEndSide)
                {
                    dMin =   dStartSide;  
                }
                else
                {
                    dMin =   dEndSide;  
                    bStart = false;
                }

                if(dSnap > dMin)
                {
                    POINT2D pt;
                    if (bStart)
                    {
                        pt = ptPre;
                    }
                    else
                    {
                        pt = ptNext;
                    }


                    if (  fabs(ptDir.dY) > NEAR_ZERO)
                    {
                        ptSnap.dY = pt.dY;
                    }
                    else
                    {
                        ptSnap.dX = pt.dX;
                    }
                }
            }
        }

        //----------------------
        if (iMidNodeNo == 0)
        {
            POINT2D ptStart =  pConnectorTmp->GetStartSide();
            if (  fabs(ptDir.dY) > NEAR_ZERO)
            {
                ptStart.dY = ptSnap.dY;
                pConnectorTmp->m_lstMidPoint[iMidNodeNo].pt.dY  = ptSnap.dY;
            }
            else
            {
                ptStart.dX = ptSnap.dX;
                pConnectorTmp->m_lstMidPoint[iMidNodeNo].pt.dX  = ptSnap.dX;
            }
            pConnectorTmp->m_lstMidPoint[iMidNodeNo].bEdit  = true;
            pConnectorTmp->SetStart(ptStart);
        }
        else if( iMidNodeNo == iSize)
        {
            POINT2D ptEnd =  pConnectorTmp->GetEndSide();
            if (  fabs(ptDir.dY) > NEAR_ZERO)
            {
                ptEnd.dY = ptSnap.dY;
                pConnectorTmp->m_lstMidPoint[iMidNodeNo - 1].pt.dY  = ptSnap.dY;
            }
            else
            {
                ptEnd.dX = ptSnap.dX;
                pConnectorTmp->m_lstMidPoint[iMidNodeNo - 1].pt.dX  = ptSnap.dX;
            }
            pConnectorTmp->m_lstMidPoint[iMidNodeNo - 1].bEdit  = true;
            pConnectorTmp->SetEnd(ptEnd);
        }
        else
        {
            if (  fabs(ptDir.dY) > NEAR_ZERO)
            {
                pConnectorTmp->m_lstMidPoint[iMidNodeNo -1].pt.dY  = ptSnap.dY;
                pConnectorTmp->m_lstMidPoint[iMidNodeNo   ].pt.dY  = ptSnap.dY;
            }
            else
            {
                pConnectorTmp->m_lstMidPoint[iMidNodeNo -1].pt.dX  = ptSnap.dX;
                pConnectorTmp->m_lstMidPoint[iMidNodeNo   ].pt.dX  = ptSnap.dX;
            }
            pConnectorTmp->m_lstMidPoint[iMidNodeNo].bEdit  = true;
            pConnectorTmp->m_lstMidPoint[iMidNodeNo - 1].bEdit  = true;
        }
    }

    m_bDispNoIcon = bNoIcon;
    UpdateNodeSocketData(pConnectorTmp.get());
    pDef->SetMouseOver(pConnectorTmp);
}

void CDrawingConnectionLine::SelectedMouseMove(CDrawingView* pView, MOUSE_MOVE_POS posMouse)
{
    CNodeMarker* pNodeMarker = pView->GetNodeMarker();

    if (!pNodeMarker)
    {
        return;
    }

    CPartsDef*   pDef  = pView->GetPartsDef();
    if (!pDef)
    {
        return;
    }


    StdString strMarkerId = pNodeMarker->GetMarkerId(posMouse.ptSel);
    CConnection* pC = NULL; 
    int iNode = _GetNodeId(&strMarkerId);

    if (iNode == ND_START)
    {
        pC = &m_c[0];
    }
    else if (iNode == ND_END)
    {
        pC = &m_c[1];
    }
    else
    {
        return;
    }

    auto pObj = pDef->GetObjectById(pC->cd->lstPlug[0].iId);
    StdString strName;
    if (pObj)
    {
        strName = pObj->GetName();
    }

#ifdef _DEBUG_CONNECTION 
    StdStringStream sOut;
    sOut << StdFormat(_T("%d(%s)")) % pC->cd->lstPlug[0].iId % strName;


    DB_PRINT(_T("CL %s \n"), sOut.str().c_str());
#endif
}


//!< マーカ開放
bool CDrawingConnectionLine::ReleaseNodeMarker(CNodeMarker* pMarker, StdString strMarkerId, MOUSE_MOVE_POS posMouse)
{
    CDrawingView* pView = pMarker->GetView();

    auto lstTmpObject = pView->GetDraggingTmpObjects();
    CDrawingConnectionLine* pConnectorTmp = dynamic_cast<CDrawingConnectionLine*>(lstTmpObject->at(0).get());

    //----------------------
    //  不要な点を削除する
    //----------------------
    int iNo = 0;
    POINT2D ptOld = pConnectorTmp->GetStartSide();
    std::set<int> lstDel;
    for(auto pd: pConnectorTmp->m_lstMidPoint)
    {
        double dL = pd.pt.Distance(ptOld);
        if (dL < NEAR_ZERO)
        {
            lstDel.insert(iNo);
            if (iNo != 0)
            {
                lstDel.insert(iNo - 1);
            }
        }
       ptOld = pd.pt;
       iNo++;
    }
    POINT2D ptEnd = pConnectorTmp->GetEndSide();
    double dL = ptEnd.Distance(ptOld);
    if (dL < NEAR_ZERO)
    {
        lstDel.insert(iNo - 1);
    }

    std::vector<POINT_DATA> lstNew;

    size_t iSize = pConnectorTmp->m_lstMidPoint.size();
    for(size_t iNo = 0; iNo < iSize; iNo++)
    {
        auto ite = lstDel.find(SizeToInt(iNo));
        if (ite == lstDel.end())
        {
            lstNew.push_back(POINT_DATA(pConnectorTmp->m_lstMidPoint[iNo].pt)); 
            lstNew[lstNew.size() - 1].bEdit = pConnectorTmp->m_lstMidPoint[iNo].bEdit;
        }
    }
     pConnectorTmp->m_lstMidPoint =  lstNew;
    //----------------------
 
    SetSelect(false);

    CPartsDef*   pDef  = pView->GetPartsDef();
    CUndoAction*       pUndo;
    pUndo  = pDef->GetUndoAction();

    pUndo->AddStart(UD_CHG, pDef, this);
    this->m_lstMidPoint = pConnectorTmp->m_lstMidPoint;
    this->m_c[0] = pConnectorTmp->m_c[0];
    this->m_c[1] = pConnectorTmp->m_c[1];
    pUndo->AddEnd(shared_from_this(), true);
    pUndo->Push();
#ifdef _DEBUG_CONNECTION 
int iCnt = 0;
DB_PRINT(_T("----MidPointList----\n"));
for(auto data: pConnectorTmp->m_lstMidPoint)
{
    DB_PRINT(_T("%d:(%s) Edit%d \n"), iCnt, data.pt.Str().c_str(), data.bEdit);
    iCnt++;
}
DB_PRINT(_T("\n"));
#endif
    pView->ClearDraggingNodes();
    pView->ClearDraggingTmpObjects();

    m_bDispNoIcon = false;
    UpdateNodeSocketData();
    
    ::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
    pMarker->Clear();

     //再び選択状態にする
     return false;
}

//ロード終了後処理
void CDrawingConnectionLine::LoadAfter(CPartsDef* pDef)
{
    int iNo = -1;
    for(auto& c: m_c)
    {
        iNo++;

        if (c.cd->lstPlug.empty())
        {
            c.cd->lstPlug.push_back(CBindingPartner());
            continue;
        }

        if(c.cd->lstPlug[0].iId == -1)
        {
            continue;
        }
        
        auto pDest = pDef->GetObjectById(c.cd->lstPlug[0].iId);
        if(!pDest)
        {
            continue;
        }

        CNodeData* pCd;

        pCd = pDest->GetNodeData(c.cd->lstPlug[0].iIndex);

        if(!pCd)
        {
            continue;
        }
        pCd->AddDistObject(this, iNo);
        SetPartsDef(pDef);
        _UpdateConnectedNodeName(iNo);
    }
}



//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void TEST_CDrawingConnectionLine()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}
#endif //_DEBUG