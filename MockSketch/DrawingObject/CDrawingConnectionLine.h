/**
* @brief        CDrawingConnectionLineヘッダーファイル
* @file	        CDrawingConnectionLine.h
* @author           Yasuhiro Sato
* @date	        07-2-2009 20:03:09
* 
* @note	
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
* 
* @remarks	        
*			
*
* $
* $
* 
*/
#ifndef __DRAWING_CONNECTION_LINE_H_
#define __DRAWING_CONNECTION_LINE_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DrawingObject/CDrawingObject.h"

#include "DrawingObject/Primitive/LINE2D.h"
#include "./CNodeData.h"

class CConnection
{
public:
    CConnection(){;}
    CConnection(const CConnection &m){*this = m;}
    virtual ~CConnection(){;}
    CConnection& operator =(const CConnection& m);


    bool operator ==(const CConnection& m)
    {
        if(pt != m.pt){return false;}
        if(eArrowType != m.eArrowType){return false;}
        return true;
    }

    bool operator !=(const CConnection& m)
    {
        return !(*this == m );
    }

public:
    POINT2D pt;                 //接続点座標
    VIEW_COMMON::E_ARROW_TYPE eArrowType = VIEW_COMMON::AT_NONE;
    std::unique_ptr<CNodeData> cd;  



    private:
        friend class boost::serialization::access;  
        template<class Archive>
        void serialize(Archive& ar, const unsigned int version)
        {
            CNodeData tmpNd;
            if (!Archive::is_loading::value)
            {
                tmpNd = *cd;
            }

            SERIALIZATION_INIT
                SERIALIZATION_BOTH("ArrowType"       , eArrowType);
                SERIALIZATION_BOTH("Position"        , pt);
                SERIALIZATION_BOTH("ConnectionData", tmpNd);
            MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
            if (Archive::is_loading::value)
            {
                *cd = tmpNd;
            }
        }
};


class POINT_DATA
{
public:
    POINT_DATA(){;}
    POINT_DATA(const POINT2D& p){pt = p;}
    POINT_DATA(const double& x, const double& y){pt.Set(x,y);}
    POINT_DATA(const POINT_DATA& m);
    virtual POINT_DATA& operator = (const POINT_DATA & m);

    virtual ~POINT_DATA(){;}
public:
    POINT2D pt;
    bool    bEdit = false;
    //保存不要
    std::unique_ptr<CNodeData> cd;  //

private:
    friend class boost::serialization::access;  
    BOOST_SERIALIZATION_SPLIT_MEMBER();

    template<class Archive>
    void load(Archive& ar, const unsigned int version);

    template<class Archive>
    void save(Archive& ar, const unsigned int version) const;

};

/**
 * @class   CDrawingConnectionLine
 * @brief  
 */
class CDrawingConnectionLine :public CDrawingObject//:public CRTTIClass<CDrawingConnectionLine, CDrawingObject, DT_LINE>
{
public:
    enum E_TYPE
    {
        E_STRAIGHT,
        E_MULTI,
        E_SPLINE
    };

    enum E_NODE
    {
        ND_NONE = -1,
        ND_START,
        ND_END,
        ND_MID,
    };

    enum E_DIRECTION_RELATION
    {
        ED_SAME         = 0x0001,    //同一方向
        ED_OPPSIT       = 0x0002,    //反対方向
        ED_ANGLE        = 0x0004,    //９０°
        ED_FROM_0       = 0x0010,    //１から２が見える
        ED_FROM_1       = 0x0020,    //２から１が見える
        ED_COLLISION    = 0x0100,    //障害物あり
        //-------------------------   以下はマスクする
        ED_REV          = 0x1000,
        ED_ROTATE       = 0x2000,
        ED_INSIDE_0     = 0x4000,    // 1が２の内部にある
        ED_INSIDE_1     = 0x8000,    // ２が１の内部にある
    };


public:
    //!< コンストラクタ
    CDrawingConnectionLine();

    //!< コンストラクタ
    CDrawingConnectionLine(int nID);

    //!< コピーコンストラクタ
    CDrawingConnectionLine(const CDrawingConnectionLine& Obj);

    //!< デストラクタ
    virtual ~CDrawingConnectionLine();

    //!< 相対移動
    virtual void Move(const POINT2D& pt2D);

    //!< 回転
    virtual void Rotate(const POINT2D& pt2D, double dAngle);

    //!< 交点計算
    virtual void CalcIntersection ( std::vector<POINT2D>* pLiat,   
        const CDrawingObject* pObj,
        bool bOnline = true,
        double dMin = NEAR_ZERO) const;

    //!< 鏡像
	virtual void Mirror(const LINE2D& line);

    //!< 倍率
	virtual void Scl(const POINT2D& pt2D, double dXScl, double dYScl);

    //!< 行列適用
    virtual void Matrix(const MAT2D& mat2D);

    //!< 調整
    virtual bool Trim (const POINT2D& ptIntersect, const POINT2D& ptClick, bool bReverse);

    //!< 分割
    virtual std::shared_ptr<CDrawingObject>  Break (const POINT2D& ptBreak);

    //!< 範囲
    virtual bool IsInner( const RECT2D& rcArea, bool bPart) const;

    //!< 描画
    virtual void Draw(CDrawingView* pView, 
        const std::shared_ptr<CDrawingObject> pParentObject,
        E_MOUSE_OVER_TYPE eMouseOver = E_MO_NO,
        int iLayerId = -1) override;

    //!< 描画削除
    virtual void DeleteView(CDrawingView* pView,
                            const std::shared_ptr<CDrawingObject> pParentObject,
                            int iLayerId = -1) override;

    //!< 領域取得
    virtual RECT2D GetBounds() const;

    //!< 距離
    virtual double Distance(const POINT2D& pt) const;

    //-------------------------
    // リフレクション設定
    //-------------------------

    //-------------------------
    // 参照
    //-------------------------
    //!< 始点終点交換(Line,Circle,Splie,CompositLine用)
    virtual bool  SwapStartEnd();

    //!< 始点取得(Line,Circle,Splie,CompositLine用)
    virtual POINT2D  GetStartSide() const;

    //!< 始点取得(Line,Circle,Splie,CompositLine用)
    virtual POINT2D  GetEndSide() const;

    //!< 始点設定
    void SetStart(POINT2D pt);

    //!< 終点設定
    void SetEnd(POINT2D pt);

    //!< 始節点設定
    bool SetStartNode(CDrawingNode* pNode);

    //!< 終節点設定
    bool SetEndNode(CDrawingNode* pNode);

    //!< 線幅設定
    void SetLineWidth(int iWidth);

    //!< 線幅取得
    int GetLineWidth() const;

    //!< 線種設定
    void SetLineType(int iType); 

    //!< 線種取得
    int  GetLineType() const;

    //!< スナップ点取得
    virtual  bool GetSnapList(std::list<SNAP_DATA>* pLstSnap)const;


    //-------------------------
    // プロパティ関連メソッド
    //-------------------------

    //!<プロパティ初期化
    virtual TREE_GRID_ITEM*  _CreateStdPropertyTree();

    //!< プロパティ値更新
    virtual void _PrepareUpdatePropertyGrid();

    //-------------------------
    // 参照
    //-------------------------

    //!< 参照データに基づいて更新
    virtual CDrawingObject* UpdateRef();

    //ロード終了後処理
    virtual void LoadAfter(CPartsDef* pDef) override;

    //-------------------
    // ドラッグ用マーカ
    //-------------------
    //!< マーカ初期化 true:マーカあり
    virtual bool InitNodeMarker(CNodeMarker* pMarker) override;

    //!< マーカ選択
    virtual void SelectNodeMarker(CNodeMarker* pMarker, 
                                     StdString strMarkerId) override;

    //!< マーカ移動
    virtual void MoveNodeMarker(CNodeMarker* pMarker, 
                                     SNAP_DATA* pSnap,
                                     StdString strMarkerId,
                                     MOUSE_MOVE_POS posMouse) override;

    //!< マーカ開放
    virtual bool ReleaseNodeMarker(CNodeMarker* pMarker, StdString strMarkerId, MOUSE_MOVE_POS posMouse) override;

    virtual bool OnUpdateNodeSocketData(const POINT2D* pPt,
        const CDrawingObject* pSocketObj,
        int iSocketIndex,
        int iIndex,
        std::set<int>* pList,
        CUndoAction* pUndo
    ) override;

    virtual bool OnReleaseNodeData(const CDrawingObject* pObj, int iConnectionId) override;

    bool SetConnection(int iNo, CDrawingObject* pObj, int iConnectionIndex);

    bool SetConnectionStart(CDrawingObject* pObj, int iConnectionIndex);
    bool SetConnectionEnd(CDrawingObject* pObj, int iConnectionIndex);
    void RecalcMidPoints(const CDrawingObject* pTmpObj, int iNoMoveSide, bool bRecalcEditedPoint);
    bool OnSetCursor(CNodeMarker* pNodeMarker, CDrawingView* pView);

   //-------------------------
    //!< 選択状態でのマウス位置
    //---------------------------
    virtual void SelectedMouseMove(CDrawingView* pView, MOUSE_MOVE_POS posMouse);
 
    static StdString GetLineFormToStr(E_TYPE eType); 


    //-------------------
    // 接続
    //-------------------
    virtual bool CreateNodeData(int iConnectionId) override;
    virtual CNodeData* GetNodeData(int iConnectionId) override;
    virtual int GetCopySrcObjectId() const override{return m_iTmpConnectionObjId;}
    virtual void ResetCopySrcObjectId() override{m_iTmpConnectionObjId = -1;}
    virtual void SetCopyNodeDataMode(bool bSet) override{m_bCopyNodeDataMode = bSet;}
    virtual bool GetCopyNodeDataMode() const override{return m_bCopyNodeDataMode;}

    virtual int GetNodeDataNum() const override;
    virtual bool GetNodeDataPos(POINT2D* pt, int iNo) const override;
    virtual RECT2D::E_EDGE InquireConnectDirection(std::map<RECT2D::E_EDGE, LINE2D>* pDir, 
             RECT2D* pRc,
             int iConnectionId ) const override;
    virtual bool UpdateNodeSocketData(CDrawingObject* pTmpObj = NULL,
                                  CPartsDef* pDef = NULL);


    //-------------------
    // 選択状態
    //-------------------
    virtual bool IsIgnoreSelPart() const override { return true;}

friend CDrawingConnectionLine;
    //--------------------
    // プロパティ変更処理
    //--------------------
    static bool PropStart      (CStdPropertyItem* pData, void* pObj);
    static bool PropEnd        (CStdPropertyItem* pData, void* pObj);
    static bool PropWidth      (CStdPropertyItem* pData, void* pObj);
    static bool PropLineType   (CStdPropertyItem* pData, void* pObj);
    static bool PropArrowStart (CStdPropertyItem* pData, void* pObj);
    static bool PropArrowEnd   (CStdPropertyItem* pData, void* pObj);
    static bool PropLineForm   (CStdPropertyItem* pData, void* pObj);
    //static bool PropConnect1   (CStdPropertyItem* pData, void* pObj);
    //static bool PropConnect2   (CStdPropertyItem* pData, void* pObj);
friend CDrawingNode;
protected:

protected:
    virtual void _Draw(CDrawingView* pView,
                       COLORREF crPen,
                       int      iId,
                       int      iType,
                       int      iWidth,
                       int      iLayerId,
                       const CDrawingObject* pParentObject
                       );

    RECT2D::E_EDGE _GetDirection(std::map<RECT2D::E_EDGE, LINE2D>* pDir,
        RECT2D* pRc, 
        int* pFixConnectionNo,     
        int iNo, 
        int iMoveSide,
        bool bRecalcFixPoint,
        const CDrawingObject* pTmpObj);

    //bool _GetDirectionNc(LINE2D* pLine, int iNo);

    bool _UpdateConnectedNodeName(int iNo);
    bool _UpdatePosition(int iNo);
    bool _UpdateNode();
    bool _RecalcMidPoint();

    bool _GetConnectionData(POINT2D* pPt, int iNo, CNodeData** ppData);

    bool _DrawigArrow(CDrawingView* pView,
                                int iLayerId,
                                int iWidth,
                                COLORREF crPen,
                                int iId,
                                int iNo) const;

    static bool _Intersection(
                POINT2D* pt,
                const LINE2D& half,
                const LINE2D& l,
                bool bInf);

    //半直線と半直線の交点
    static bool _IntersectionHalf(
            POINT2D* pt1,
            const LINE2D& half1,
            const LINE2D& half2);

//半直線と四角形の交点
    static bool _Intersection(
            POINT2D* pt,
            const LINE2D& half,
            const RECT2D& rc,
            bool bInf);


    static bool _Intersection(
            std::vector<POINT2D> * plist,
            const LINE2D& half,
            const RECT2D& rc,        
            bool bInf);

   int _GetNodeId(const StdString* pStr);

    bool _GetNorm( POINT2D* ptMid, 
                        POINT2D* ptNorm, 
                        const POINT2D& ptS,  
                        const POINT2D& ptE);


    CNodeData* _GetDistConnectionData(CPartsDef* pDef, int iSide);
    CNodeData _DefaultConnection() const;

   
    bool _GetNextPoint(POINT2D* pPt, int iConnectionId ) const; 
    bool _GetPriviousPoint(POINT2D* pPt, int iConnectionId ) const; 

    int _GetLastFixPoint(int iNoMoveSide)const;

   static RECT2D::E_EDGE _GetPointDir(const POINT2D& vec); 
   static RECT2D::E_EDGE _GetLineDir(const LINE2D& vec); 

    void _GetEdgeLines(std::map<RECT2D::E_EDGE, LINE2D>* pDir,
                                                            const POINT2D& ptFix,
                                                            double  dSnapLen,
                                                            RECT2D::E_EDGE edge) const; 

    int _CalcOptimalDirection( LINE2D* pL1,
                                LINE2D* pL2,
                                const std::map<RECT2D::E_EDGE, LINE2D>& m1,
                                const std::map<RECT2D::E_EDGE, LINE2D>& m2,
                                const RECT2D& rc1,
                                const RECT2D& rc2
                                );

    //反対向き 互いが見える
    void _CalcOLLN(std::vector<POINT_DATA>* pList, 
                            const LINE2D& l1,
                            const LINE2D& l2,
                            const RECT2D& rc1,
                            const RECT2D& rc2,
                            bool bRotate);

    //反対向き 互いが見えない
    void _CalcONNN(std::vector<POINT_DATA>* pList, 
                            const LINE2D& l1,
                            const LINE2D& l2,
                            const RECT2D& rc1,
                            const RECT2D& rc2,
                            bool bRotate);

    bool _CalcONNN_Wrap(std::vector<POINT_DATA>* pList, 
                        const LINE2D& l1,
                        const LINE2D& l2,
                        const std::vector<LINE2D>& lstLine1,
                        const std::vector<LINE2D> lstLine2,
                        bool bRotate);


    bool _CalcONNN_OutSide(std::vector<POINT_DATA>* pList, 
                        const LINE2D& l1,
                        const LINE2D& l2,
                        double dLeft,
                        bool bRotate);

    bool _CalcONNN_Mid(std::vector<POINT_DATA>* pList, 
                        const LINE2D& l1,
                        const LINE2D& l2,
                        double dSide1,
                        double dSide2,
                        double dMid,
                       bool bRotate);

    //90°片側のみ見える   衝突なし
    void _CalcALNN(std::vector<POINT_DATA>* pList, 
                            const LINE2D& l1,
                            const LINE2D& l2,
                            const RECT2D& rc1,
                            const RECT2D& rc2,
                            bool bRotate);

    //90°片側のみ見える   衝突あり
    void _CalcALNC(std::vector<POINT_DATA>* pList, 
                            const LINE2D& l1,
                            const LINE2D& l2,
                            const RECT2D& rc1,
                            const RECT2D& rc2,
                            bool bRotate);

    //90°互いが見えない 衝突なし / あり
    void _CalcANNN(std::vector<POINT_DATA>* pList, 
                            const LINE2D& l1,
                            const LINE2D& l2,
                            const RECT2D& rc1,
                            const RECT2D& rc2,
                            bool bRotate);
    //同方向   衝突あり
    void _CalcSNNC(std::vector<POINT_DATA>* pList, 
                            const LINE2D& l1,
                            const LINE2D& l2,
                            const RECT2D& rc1,
                            const RECT2D& rc2,
                            bool bRotate);

    //同方向   衝突なし
    void _CalcSLLN(std::vector<POINT_DATA>* pList, 
                            const LINE2D& l1,
                            const LINE2D& l2,
                            const RECT2D& rc1,
                            const RECT2D& rc2,
                            bool bRotate,
                            bool bInside);
protected:
    std::vector<POINT_DATA> m_lstMidPoint;     //中間点

    E_TYPE m_eType;                         //種別

    //!< 線幅
    int                                     m_iWidth;

    //!< 線種
    int                                     m_iLineType;

    CConnection                             m_c[2];

    //-----------
    //保存不要
    //-----------
    bool m_bDispNoIcon;
    bool m_bDragDiv;
    bool m_bCopyNodeDataMode;
    int  m_iTmpConnectionObjId;
    StdString m_cName[2];
private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
            ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(CDrawingObject);
            SERIALIZATION_BOTH("ConnectionData"   , m_c);
            SERIALIZATION_BOTH("Type" , m_eType);
            SERIALIZATION_BOTH("MidPoint" , m_lstMidPoint);
            SERIALIZATION_BOTH("Width"    , m_iWidth);
            SERIALIZATION_BOTH("LineType" , m_iLineType);
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }
};

BOOST_CLASS_VERSION(CConnection, 0);

#endif // 