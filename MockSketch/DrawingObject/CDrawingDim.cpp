/**
 * @brief			CDrawingDim実装ファイル
 * @file			CDrawingDim.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CDrawingPoint.h"
#include "./CDrawingLine.h"
#include "./CDrawingDim.h"
#include "./CDrawingCircle.h"
#include "./CDrawingSpline.h"
#include "./CDrawingEllipse.h"
#include "./CDrawingCompositionLine.h"
#include "./CDrawingParts.h"
#include "./CDrawingText.h"
#include "./CDrawingNode.h"
#include "./CDimText.h"
#include "DefinitionObject/View/CDrawingView.h"
#include "DefinitionObject/CPartsDef.h"
#include "Utility/CUtility.h"
#include "Utility/ExtText/CExtText.h"
#include "System/CSystem.h"
#include "SubWindow/DimTextDlg.h"

#include <math.h>
#include <boost/serialization/export.hpp> 
BOOST_CLASS_EXPORT(CDrawingDim);


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


//-------------------------------
/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
CDrawingDim::CDrawingDim():
CDrawingObject(),
//m_dHText(0.0),
//m_bTextFlip(false),
m_iLineWidth(1),
m_eArrowType(VIEW_COMMON::AT_OPEN),
m_eArrowSide(VIEW_COMMON::ARS_BOTH),
m_bArrowFlip(false),
m_eShow(SM_SHOW),
m_main(NULL)
{
    m_iId[0] = -1;
    m_iId[1] = -1;
    m_bShowAux[0] = true;
    m_bShowAux[1] = true;
    m_bSelPoint[0] = false;
    m_bSelPoint[1] = false;
    m_psDimText = std::make_unique<CDimText>(this);
}

/**
 *  @brief  コンストラクター.
 *  @param  [in] nID 
 *  @retval なし 
 *  @note
 */
CDrawingDim::CDrawingDim(int nID, DRAWING_TYPE eType):
CDrawingObject( nID, eType),
//m_dHText(0.0),
m_iLineWidth(1),
m_eArrowType(VIEW_COMMON::AT_OPEN),
m_eArrowSide(VIEW_COMMON::ARS_BOTH),
m_bArrowFlip(false),
m_eShow(SM_SHOW),
m_bNodeMarker(false),
m_main(NULL)
{
    m_iId[0] = -1;
    m_iId[1] = -1;
    m_bShowAux[0] = true;
    m_bShowAux[1] = true;
    m_bSelPoint[0] = false;
    m_bSelPoint[1] = false;
    m_psDimText = std::make_unique<CDimText>(this);
}



/**
 *  @brief  デストラクタ.
 *  @param  なし 
 *  @retval なし 
 *  @note
 */
CDrawingDim::~CDrawingDim()
{
    if (m_main)
    {
        delete m_main;
        m_main = NULL;
    }
}

/**
 * コピーコンストラクタ
 */
CDrawingDim::CDrawingDim(const CDrawingDim& Obj):
CDrawingObject( Obj)
{
    m_eType           = Obj.m_eType;
   
    m_iId[0]           = Obj.m_iId[0];
    m_iId[1]           = Obj.m_iId[1];

    m_pt[0]           = Obj.m_pt[0];
    m_pt[1]           = Obj.m_pt[1];
//   m_ptLineCenter    = Obj.m_ptLineCenter;
//  m_ptCenter        = Obj.m_ptCenter;

//    m_dHText          = Obj.m_dHText;
    //m_bTextFlip       = Obj.m_bTextFlip;
    m_iLineWidth      = Obj.m_iLineWidth;
    m_bShowAux[0]     = Obj.m_bShowAux[0];
    m_bShowAux[1]     = Obj.m_bShowAux[1];
    m_eArrowType      = Obj.m_eArrowType;
    m_eArrowSide      = Obj.m_eArrowSide;
    m_bArrowFlip      = Obj.m_bArrowFlip;

    m_lineSub          = Obj.m_lineSub;

    m_main = NULL;
    if (Obj.m_main)
    {
        m_main  = CDrawingObject::Clone(Obj.m_main);
    }

    m_psDimText = std::make_unique<CDimText>(this);
    *m_psDimText.get() = *Obj.m_psDimText.get();


    m_bSelPoint[0]    = Obj.m_bSelPoint[0];
    m_bSelPoint[1]    = Obj.m_bSelPoint[1];

    m_eShow = SM_SHOW;
}

void CDrawingDim::_SetDefault()
{
    m_iId[0] = -1;
    m_iId[1] = -1;

    m_pt[0].Set(0.0, 0.0);
    m_pt[1].Set(0.0, 0.0);

//    m_ptLineCenter.Set(0.0, 0.0);
    //m_ptCenter.Set(0.0, 0.0);

    //m_dHText = 0.0;
    //m_bTextFlip = false;
    m_iLineWidth = DRAW_CONFIG->iDimWidth;

    //m_bShowAux  下で設定

    m_eArrowType = DRAW_CONFIG->eArrowType;
    m_eArrowSide = VIEW_COMMON::ARS_BOTH;

    m_bArrowFlip = false;
    //m_iPrecision  下で設定

    m_bSelPoint[0]    = false;
    m_bSelPoint[1]    = false;

    m_lineSub.clear();

    if (m_main)
    {
        delete m_main;
        m_main = NULL;
    }
    m_eShow = SM_SHOW;

    m_psDimText->SetDefault();
}

/**
 *  @brief  回転.
 *  @param  [in]    pPt2D    回転中心(絶対座標)
 *  @param  [in]    dAngle   (絶対)回転角(rad)
 *  @retval     なし
 *  @note
 */
void CDrawingDim::Rotate(const POINT2D& pt2D, double dAngle)
{
    m_pt[0].Rotate(pt2D,dAngle);
    m_pt[1].Rotate(pt2D,dAngle);
    _UpdateText();
}

/**
 *  @brief  相対移動.
 *  @param  [in]    pPt2D   移動量
 *  @retval         なし
 *  @note
 */
void CDrawingDim::Move(const POINT2D& pt2D) 
{
    AddChgCnt();

    if (!m_bSelPoint[0] && !m_bSelPoint[1])
    {
        m_pt[0].Move(pt2D);
        m_pt[1].Move(pt2D);
    }

    if (m_bSelPoint[0])
    {
        m_pt[0].Move(pt2D);
    }

    if (m_bSelPoint[1])
    {
        m_pt[1].Move(pt2D);
    }
    _UpdateText();
}

/**
 *  @brief  交点計算.
 *  @param  [out]   pList   交点のリスト
 *  @param  [in]    pObj    交差するオブジェクト
 *  @retval         なし
 *  @note
 */
void CDrawingDim::CalcIntersection ( std::vector<POINT2D>* pList,   
                                const CDrawingObject* pObj,
                                bool bOnline,
                                double dMin)  const
{
}

/**
 *  @brief  位置調整.
 *  @param  [in]    pPtIntersect   調整位置
 *  @param  [in]    pPtClick       調整方向指定点
 *  @retval         なし
 *  @note
 */
bool CDrawingDim::Trim (const POINT2D& ptIntersect, const POINT2D& ptClick, bool bReverse)
{
    //Trimは不可
    return false;
}


/**
 *  @brief  分割.
 *  @param  [in]    pPtBreak  分割位置
 *  @retval         分割によって出来たオブジェクト
 *  @note
 */
std::shared_ptr<CDrawingObject>  CDrawingDim::Break (const POINT2D& ptBreak)
{
    //分割不可
    return NULL;
}

/**
 *  @brief  鏡像.
 *  @param  [in]    line   対称軸
 *  @retval         なし
 *  @note
 */
void CDrawingDim::Mirror(const LINE2D& line)
{
    if (m_main)
    {
        m_main->Mirror(line);
    }
    m_pt[0].Mirror(line);
    m_pt[1].Mirror(line);
    _UpdateText();
    AddChgCnt();
}

/**
 *  @brief  行列.
 *  @param  [in]    mat2D   適用する行列
 *  @retval         なし
 *  @note
 */
void CDrawingDim::Matrix(const MAT2D& mat2D)
{
    m_pt[0].Matrix(mat2D);
    m_pt[1].Matrix(mat2D);

    if (m_main)
    {
        m_main->Matrix(mat2D);
    }


    _UpdateText();
    AddChgCnt();
}

/**
 *  @brief  倍率.
 *  @param  [in]    pPt2D   拡大中心
 *  @param  [in]    dSclX   X倍率
 *  @param  [in]    dSclY   Y倍率
 *  @retval         なし
 *  @note
 */
void CDrawingDim::Scl(const POINT2D& pt2D, double dXScl, double dYScl)
{
    if (m_main)
    {
        m_main->Scl(pt2D, dXScl, dYScl);
    }
    m_pt[0].Scl(pt2D, dXScl, dYScl);
    m_pt[1].Scl(pt2D, dXScl, dYScl);
    _UpdateText();
    AddChgCnt();
}

void CDrawingDim::Show(SHOW_MODE eShow )
{
    m_eShow = eShow;
}

/**
 *  @brief  範囲.
 *  @param  [in] p1 1点目
 *  @param  [in] p2 2点目
 *  @retval  true:2点で示される矩形内に点が存在する 
 *  @note
 */
bool CDrawingDim::IsInner( const RECT2D& rcArea, bool bPart) const
{
    if (m_main)
    {
        if (m_main->IsInner(rcArea, bPart))
        {
            return true;
        }
    }

    return false;
}

/**
 *  @brief   参照データに基づいて更新.
 *  @param   
 *  @retval  
 *  @note
 */
CDrawingObject* CDrawingDim::UpdateRef()
{
    CDrawingObject*      pRef;

    pRef = CDrawingObject::UpdateRef();

    if (pRef == NULL)
    {
        return NULL;
    }

    CDrawingDim* pDim = dynamic_cast<CDrawingDim*>(pRef);

    STD_ASSERT(pDim != NULL);

    if (pDim == NULL)
    {
        return NULL;
    }

    //TODO: 実装

    m_iChgCnt = pRef->GetChgCnt();
    return pRef;
}


/**
 * @brief   領域取得
 * @param   なし
 * @retval  なし
 * @note	描画領域を取得する
 */
RECT2D CDrawingDim::GetBounds() const
{
    RECT2D rc;

    rc = m_pt[0];
    rc.ExpandArea(m_pt[1]);
    return rc;
}

/**
 * @brief   距離
 * @param   [in] pt 
 * @retval  近接点からの距離
 * @note
 */
double CDrawingDim::Distance(const POINT2D& pt) const
{
    return DBL_MAX;
}

bool CDrawingDim::_IsNarrowAngle(const POINT2D& pt) const
{

    return false;
}



bool CDrawingDim::_DrawigArrow(CDrawingView* pView, 
                               CDrawingObject* pMain,
                                int iWidth,
                                COLORREF crPen,
                                int iId,
                                int iLayerId,
                                VIEW_COMMON::E_ARROW_TYPE eType,
                                VIEW_COMMON::E_ARROW_SIDE eSide,
                                bool bRev) const
{
    pView->SetPen(PS_SOLID,  iWidth, crPen, iId);
    //pView->SetBrush(crPen);
    //pView->SetBrush(RGB(0,255,0));


    POINT2D ptStart;
    POINT2D ptEnd;

    bool bFill = false;


    POINT2D vecStart;
    POINT2D vecEnd;

    if (pMain->GetType() == DT_LINE)
    {
        LINE2D lineMain;
        POINT2D ptNorm;

        _GetLengthMain(&lineMain, &ptNorm);
        ptStart = lineMain.Pt(0);
        ptEnd   = lineMain.Pt(1);

        if (m_pMatOffset)
        {
            ptStart = ptStart *   (*m_pMatOffset);
            ptEnd = ptEnd *   (*m_pMatOffset);
        }

        vecStart = ptEnd - ptStart;
        vecStart.Normalize();
        vecEnd = -vecStart;


    }
    else if (pMain->GetType() == DT_CIRCLE)
    {
        auto pC = dynamic_cast<CDrawingCircle*>(pMain);
        POINT2D ptC = pC->GetCenter();
        POINT2D p1 = m_pt[0];
        POINT2D p2 = m_pt[1];


        if (m_pMatOffset)
        {
            ptC = ptC *   (*m_pMatOffset);
            p1 = p1 *   (*m_pMatOffset);
            p2 = p2 *   (*m_pMatOffset);
        }


        POINT2D ptSNorm = p1 - ptC;
        POINT2D ptENorm = p2 - ptC;

        ptSNorm.Normalize();
        ptENorm.Normalize();

        //double dR = m_ptTextCenter.Distance(ptC);
        double dR = pC->GetRadius();

        ptStart = ptSNorm * dR + ptC;
        ptEnd   = ptENorm * dR + ptC;


        //90度回転
        vecStart.dX = -ptSNorm.dY;
        vecStart.dY =  ptSNorm.dX;

        vecEnd.dX =  ptENorm.dY;
        vecEnd.dY = -ptENorm.dX;
    }
    else
    {
        return false;
    }

    using namespace VIEW_COMMON;
    switch(eType)
    {
    case AT_NONE:
        return true;

    case AT_DOT_FILL:
        bFill = true;
    case AT_DOT:

        if (eSide & ARS_START)
        {
            DrawigArrowDot(pView, iLayerId, ptStart, bFill);
        }
        if (eSide & ARS_END)
        {
            DrawigArrowDot(pView, iLayerId, ptEnd, bFill);
        }
        break;
    case AT_OBLIQUE:
        if (eSide & ARS_START)
        {
            DrawigOblique(pView, iLayerId, ptStart, vecStart);
        }
        if (eSide & ARS_END)
        {
            DrawigOblique(pView, iLayerId, ptEnd, vecEnd);
        }
        break;

    case AT_FILLED:
        bFill = true;
    case AT_OPEN:
        {
            CLayer* pLayer = pView->GetLayer(iLayerId);
            double dLen = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->dArrowSize) * 2.0;  
            POINT pt1;
            POINT pt2;
            POINT2D ptArrowEnd;
            if (eSide & ARS_START)
            {
                if (bRev)
                {
                    vecStart = -vecStart;
                    ptArrowEnd = dLen * vecStart / pLayer->dScl + ptStart;
                    pView->ConvWorld2Scr(&pt1, ptStart, iLayerId);
                    pView->ConvWorld2Scr(&pt2, ptArrowEnd, iLayerId);
                    pView->Line(pt1, pt2);
                }
                DrawigArrowOpen(pView, iLayerId, ptStart, vecStart, bFill);
            }

            if (eSide & ARS_END)
            {
                if (bRev)
                {
                    vecEnd = -vecEnd;
                    ptArrowEnd = dLen * vecEnd / pLayer->dScl + ptEnd;
                    pView->ConvWorld2Scr(&pt1, ptEnd, iLayerId);
                    pView->ConvWorld2Scr(&pt2, ptArrowEnd, iLayerId);
                    pView->Line(pt1, pt2);
                }
                DrawigArrowOpen(pView, iLayerId, ptEnd, vecEnd, bFill);
            }
        }
        break;

    case AT_DATUM_FILL:
        bFill = true;
    case AT_DATUM_BLANK:
        if (eSide & ARS_START)
        {
            DrawigDataum(pView, iLayerId, ptStart, vecStart, bFill);
        }
        if (eSide & ARS_END)
        {
            DrawigDataum(pView, iLayerId, ptEnd, vecEnd, bFill);
        }
        break;
    }
    return true;
}


bool CDrawingDim::_GetPoint(POINT2D* pPt, const CDrawingObject* pObj)
{
    if (!pObj)
    {
        return false;
    }
    switch(pObj->GetType())
    {

    case DT_POINT:
    {
        auto pPoint = dynamic_cast<const CDrawingPoint*>(pObj);
        if (!pPoint)
        {
            return false;
        }
        *pPt = pPoint->GetPoint();
    }
        break;
    case DT_TEXT:
    {
        auto pText = dynamic_cast<const CDrawingText*>(pObj);
        if (!pText)
        {
            return false;
        }
        *pPt = pText->GetPoint();
    }
        break;


    case DT_NODE:
    {
        auto pNode = dynamic_cast<const CDrawingNode*>(pObj);
        if (!pNode)
        {
            return false;
        }
        *pPt = pNode->GetPoint();
    }
        break;

    case DT_GROUP:
    case DT_IMAGE:
         //TODO:IMAGE実装
        //break;
    default:
        return false;
    }

    return true;
}

bool CDrawingDim::Recalc()
{

    return false;
}

bool CDrawingDim::_SetTextPosLHV(double* pTextOffset, const POINT2D& pt,const POINT2D ptTextOrg)
{
    LINE2D lmain;
    POINT2D ptNear;
    double dTextOffset;
    POINT2D ptNorm;
    _GetLengthMain(&lmain,  &ptNorm); 


    auto pL = dynamic_cast<CDrawingLine*>(m_main);

    bool bRet;


    ptNear = lmain.NearPoint(pt, false);

    double dPos = ptNear.Distance(ptTextOrg);

    bRet = lmain.PosPram(&dTextOffset, ptTextOrg, ptNear);

    if(!bRet)
    {
        return false;
    }

    double dTextWidth2 = m_psDimText->GetTextSize().dX / 2.0;

    //Pt0 -> Pt1  が正方向
    double dT;
    int iSide = 0;
    if (dTextOffset > 0)
    {
        dT = dTextWidth2 + dTextOffset;
        if (dT >  dTextWidth2)
        {
            iSide = 2;
        }
    }
    else
    {
        dT = dTextWidth2 - dTextOffset;
        if (dT <  -dTextWidth2)
        {
            iSide = 1;
        }
    }

    POINT2D ptLineEnd;
    lmain.PramPos(&ptLineEnd, ptTextOrg, dT);

    if (iSide == 1)
    {
        pL->SetPoint1(ptLineEnd);
        pL->SetPoint2(lmain.GetPt2());

    }
    else if (iSide == 2)
    {
        pL->SetPoint2(ptLineEnd);
        pL->SetPoint1(lmain.GetPt1());
    }
    else
    {
        pL->SetPoint1(lmain.GetPt1());
        pL->SetPoint2(lmain.GetPt2());
    }
    *pTextOffset = dTextOffset;
    return true;
}

bool IsSameDirection(const POINT2D& pt1,
                     const POINT2D& pt2)

{
    double dCnt = 0;
    if ((pt1.dX >= 0) && (pt2.dX >= 0))
    {
        dCnt++;
    }
    else if ((pt1.dX < 0) && (pt2.dX < 0))
    {
        dCnt++;
    }

    if ((pt1.dY >= 0) && (pt2.dY >= 0))
    {
        dCnt++;
    }
    else if ((pt1.dY < 0) && (pt2.dY < 0))
    {
        dCnt++;
    }

    if (dCnt == 2)
    {
        return true;
    }
    return false;
}

//DIM_L, DIM_H, DIM_V で使用
bool CDrawingDim::_CreateLineDim(const POINT2D& pt1, const POINT2D &pt2)
{
    CDrawingLine* pLine;

    if (m_main)
    {
        if (m_main->GetType() != DT_LINE)
        {
            delete m_main;
            m_main = NULL;
        }
    }

    if (!m_main)
    {
        m_main = new CDrawingLine;
    }

    pLine = dynamic_cast<CDrawingLine*>(m_main);

    if (m_lineSub.size() != 2)
    {
        m_lineSub.clear();
        m_lineSub.push_back(LINE2D());
        m_lineSub.push_back(LINE2D());
    }

    m_pt[0] = pt1;
    m_pt[1] = pt2;


    return true;
}


bool CDrawingDim::_CreateAuxCircle(std::unique_ptr<CIRCLE2D>& pSub,
                                   POINT2D ptOnArc, const CIRCLE2D& c, double dScl) const
{
    // ptOnArc <- マウス位置から一番近い円弧上の点

    if (c.IsClose())
    {
        return false;
    }


    if (fabs(c.dRad) < NEAR_ZERO)
    {
        return false;
    }

    if (c.IsInsideAngle(ptOnArc))
    {
        return false;
    }


    double dMid = (c.GetStart() + c.GetEnd()) / 2.0;

    double dTop = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->dDimTop) / dScl;
    double dGap = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->dDimGap) / dScl;

    double dGapAngle = (dGap / c.dRad) * RAD2DEG;
    double dTopAngle = (dTop / c.dRad) * RAD2DEG;

    double dAngle = CUtil::NormAngle(CUtil::GetAngle(c.ptCenter, ptOnArc));

    double dStart = c.GetStart() - dGapAngle;
    double dEnd   = c.GetEnd() + dGapAngle;

    if (CUtil::IsInsideAngle( dStart,  dEnd,  dAngle))
    {
        return false;
    }
    

    double dStartSide;
    double dEndSide;


    dStartSide = CUtil::DiffAngle(dStart, dAngle);
    dEndSide   = CUtil::DiffAngle(dEnd, dAngle);

    double dSubS;
    double dSubE;

    if (dStartSide < dEndSide)
    {
        dSubS = dAngle - dTopAngle;
        dSubE = dStart;
    }
    else
    {
        dSubS = dEnd;
        dSubE = dAngle + dTopAngle;
    }
    
    if (!pSub)
    {
        pSub = std::make_unique<CIRCLE2D>(c);
    }

    pSub->SetCenter(c.GetCenter());
    pSub->SetRadius(c.GetRadius());
    pSub->SetStart(dSubS);
    pSub->SetEnd(dSubE);

    return true;
}

void  CDrawingDim::SetId(int iId)
{
    m_nId = iId;
    STD_ASSERT(m_main);
    if (!m_main)
    {
        m_main->SetId(iId);
    }
}

double CDrawingDim::NormalizeTextAngle(double dAngle)
{
    double dRet;
    //テキストの向きは(-45 <= θ < 135)
    dRet = CUtil::NormAngle(dAngle);

    if ( dRet > 314.99 )
    {
        return dRet;
    }
    else if ( dRet < 134.99 )
    {
        return dRet;
    }

    dRet = CUtil::NormAngle(dAngle - 180.0);
    return dRet;
}


/**
 *  @brief   プロパティ初期化.
 *  @param   [in] pCtrl オブジェクトコントロールへのポインタ
 *  @retval     なし
 *  @note
 */
TREE_GRID_ITEM*  CDrawingDim::_CreateStdPropertyTree()
{
    NAME_TREE_ITEM<CStdPropertyItem>* pBeforeGroup;
    NAME_TREE_ITEM<CStdPropertyItem>* pNextItem;
    NAME_TREE_ITEM<CStdPropertyItem>* pGroupItem;
    NAME_TREE<CStdPropertyItem>* pTree;
    //int iId;
    CStdPropertyItem* pItem;
    boost::tuple< CStdPropertyItem*, void*> pairVal;


    pBeforeGroup = CDrawingObject::_CreateStdPropertyTree();
    pTree = m_psProperty->GetTreeInstance();

    pGroupItem  = pTree->AddNext(pBeforeGroup, NULL, GET_STR(STR_PRO_DIM), _T("Dim"));
    using namespace VIEW_COMMON;


    //------------
    //線幅
    //------------
    CStdPropertyItemDef DefLineWidth(PROP_LINE_WIDTH, 
        GET_STR(STR_PRO_LINE_WIDTH)   , 
        _T("Width"),
        GET_STR(STR_PRO_INFO_LINE_WIDTH), 
        true,
        DISP_NONE,
        0);

    pItem = new CStdPropertyItem( DefLineWidth, PropLineWidth, m_psProperty, NULL, &m_iLineWidth);

    pNextItem = pTree->AddChild(pGroupItem, 
        pItem, 
        DefLineWidth.strDspName, 
        pItem->GetName());

    /*
    //------------
    // 種別
    //------------
    StdString strTypeList;

        strTypeList += GET_STR(STR_MNU_L_DIM);
        strTypeList += _T(":");  
        strTypeList += boost::lexical_cast<StdString>(DIM_L);  
        strTypeList += _T(",");

        strTypeList += GET_STR(STR_MNU_H_DIM);
        strTypeList += _T(":");  
        strTypeList += boost::lexical_cast<StdString>(DIM_H);  
        strTypeList += _T(",");

        strTypeList += GET_STR(STR_MNU_V_DIM);
        strTypeList += _T(":");  
        strTypeList += boost::lexical_cast<StdString>(DIM_V);  
        strTypeList += _T(",");

        strTypeList += GET_STR(STR_MNU_A_DIM);
        strTypeList += _T(":");  
        strTypeList += boost::lexical_cast<StdString>(DIM_A);  
        strTypeList += _T(",");

        strTypeList += GET_STR(STR_MNU_D_DIM);
        strTypeList += _T(":");  
        strTypeList += boost::lexical_cast<StdString>(DIM_D);  
        strTypeList += _T(",");

        strTypeList += GET_STR(STR_MNU_R_DIM);
        strTypeList += _T(":");  
        strTypeList += boost::lexical_cast<StdString>(DIM_R);  
        strTypeList += _T(",");

        strTypeList += GET_STR(STR_MNU_C_DIM);
        strTypeList += _T(":");  
        strTypeList += boost::lexical_cast<StdString>(DIM_C);  


    CStdPropertyItemDef DefDimType(PROP_DROP_DOWN_ID, 
        GET_STR(STR_PRO_DIM_TYPE)   , 
        _T("DimType"), 
        GET_STR(STR_PRO_INFO_DIM_TYPE), 
        false,                                       
        DISP_NONE,
        static_cast<int>(m_eDim),
        strTypeList,
        0);

    pItem = new CStdPropertyItem(
        DefDimType, 
        PropDimType, 
        m_psProperty, 
        NULL,
        (void*)&m_eDim);
    
    pNextItem = pTree->AddNext(pNextItem, pItem, DefDimType.strDspName, pItem->GetName());
    */

    //------------
    // 矢印種別
    //------------
    StdString strArrowTypeList;
    std::vector<E_ARROW_TYPE> lstArrowType = {
        AT_NONE,        
        AT_DOT,
        AT_DOT_FILL,
        AT_OBLIQUE,
        AT_OPEN,
        AT_FILLED,
#ifdef _DEBUG   //デバッグ用
        AT_DATUM_BLANK,   
        AT_DATUM_FILL
#endif
    };

    for (E_ARROW_TYPE eType: lstArrowType)
    {
        strArrowTypeList += GetArrowTypeToStr(eType);
        strArrowTypeList += _T(":");  
        strArrowTypeList += boost::lexical_cast<StdString>(eType);  
        if (eType != AT_DATUM_FILL)
        {
            strArrowTypeList += _T(",");
        }
    }

    CStdPropertyItemDef DefArrowType(PROP_DROP_DOWN_ID, 
        GET_STR(STR_PRO_DIM_ARROW_TYPE)   , 
        _T("DimType"), 
        GET_STR(STR_PRO_INFO_DIM_ARROW_TYPE), 
        false,                                       
        DISP_NONE,
        static_cast<int>(m_eArrowType),
        strArrowTypeList,
        0);

    pItem = new CStdPropertyItem(
        DefArrowType, 
        PropArrowType, 
        m_psProperty, 
        NULL,
        (void*)&m_eArrowType);
    
    pNextItem = pTree->AddNext(pNextItem, 
        pItem, 
        DefArrowType.strDspName, 
        pItem->GetName());


    //------------
    // 矢印方向
    //------------
    if (m_eArrowType == AT_OPEN)
    {
        CStdPropertyItemDef DefArrowDir(PROP_BOOL,           //表示タイプ
            GET_STR(STR_PRO_DIM_ARROW_DIR)   ,               //表示名
            _T("ArrowDir"),                                  //変数名
            GET_STR(STR_PRO_INFO_DIM_ARROW_DIR),             //表示説明
            true,                                            //編集可不可
            DISP_NONE,                                       //表示単位
            m_bArrowFlip                                     //初期値
            );

        pItem = new CStdPropertyItem(
            DefArrowDir, 
            PropArrowDir, 
            m_psProperty, 
            NULL,
            (void*)&m_bArrowFlip);
    
        pNextItem = pTree->AddNext(pNextItem, 
            pItem, 
            DefArrowType.strDspName, 
            pItem->GetName());
    }

    //--------------------
    // テキスト
    //--------------------
    pNextItem = m_psDimText->CreateStdPropertyTree(m_psProperty, pTree, pNextItem);

    //--------------------
    // 補助線１表示有無
    //--------------------
    CStdPropertyItemDef DefShowAux1(PROP_BOOL,        //表示タイプ
        GET_STR(STR_PRO_DIM_EXT_LINE1)   ,            //表示名
        _T("ShowAux1"),                               //変数名
        GET_STR(STR_PRO_INFO_DIM_EXT_LINE1),          //表示説明
        true,                                         //編集可不可
        DISP_NONE,                                    //表示単位
        m_bShowAux[0]                                 //初期値
        );

    pItem = new CStdPropertyItem(
        DefShowAux1, 
        PropExt1, 
        m_psProperty, 
        NULL,
        (void*)&m_bShowAux[0]);
    
    pNextItem = pTree->AddNext(pNextItem, 
        pItem, 
        DefShowAux1.strDspName, 
        pItem->GetName());

    
    if ( (m_eType != DT_DIM_C) &&
         (m_eType != DT_DIM_R) &&
         (m_eType != DT_DIM_D))
    {
        //--------------------
        // 補助線２表示有無
        //--------------------
        CStdPropertyItemDef DefShowAux2(PROP_BOOL,        //表示タイプ
            GET_STR(STR_PRO_DIM_EXT_LINE2)   ,            //表示名
            _T("Ext2"),                                   //変数名
            GET_STR(STR_PRO_INFO_DIM_EXT_LINE2),          //表示説明
            true,                                         //編集可不可
            DISP_NONE,                                    //表示単位
            m_bShowAux[1]                                 //初期値
            );

        pItem = new CStdPropertyItem(
            DefShowAux2, 
            PropExt2, 
            m_psProperty, 
            NULL,
            (void*)&m_bShowAux[1]);
    
        pNextItem = pTree->AddNext(pNextItem, 
            pItem, 
            DefShowAux2.strDspName, 
            pItem->GetName());
    }
    /*

    static bool        (CStdPropertyItem* pData, void* pObj);
    static bool PropExt2       (CStdPropertyItem* pData, void* pObj);
    static bool PropPos1       (CStdPropertyItem* pData, void* pObj);
    static bool PropPos2       (CStdPropertyItem* pData, void* pObj);


    //矢印反転
    int m_iPrecision;
    double     ;
    double     m_dTextPos;
    bool       m_bRevTexts;
    double     m_dGap;
    double     m_dTextSize;
    int        m_iTextHeightPoint;
    CExtText   m_extText;

    m_mapString[ ]         = _T("小数点以下の桁数");
    m_mapString[ ]    = _T("小数点以下の桁数を設定します");

   m_mapString[ STR_PRO_DIM_EXT_LINE2]         = _T("寸法補助線2");
    m_mapString[ STR_PRO_INFO_DIM_EXT_LINE2]    = _T("寸法補助線2の表示の有無を設定します");

    m_mapString[ STR_PRO_DIM_POS1]              = _T("寸法位置1");
    m_mapString[ STR_PRO_INFO_DIM_POS1]         = _T("寸法位置1を設定します");

    m_mapString[ STR_PRO_DIM_POS2]              = _T("寸法位置2");
    m_mapString[ STR_PRO_INFO_DIM_POS2]         = _T("寸法位置2を設定します");
    */

    return pGroupItem;
}

/*
 *  @brief   プロパティ変更(矢印種別)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingDim::PropArrowType (CStdPropertyItem* pData, void* pObj)
{
   using namespace VIEW_COMMON;
    CDrawingDim* pDim = reinterpret_cast<CDrawingDim*>(pObj);
    try
    {
        pDim->AddChgCnt();
        VIEW_COMMON::E_ARROW_TYPE eType = static_cast<VIEW_COMMON::E_ARROW_TYPE>(pData->anyData.GetInt());
        pDim->m_eArrowType = eType;
        pDim->GetPartsDef()->Redraw();
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/*
 *  @brief   プロパティ変更(矢印方向)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingDim::PropArrowDir(CStdPropertyItem* pData, void* pObj)
{
   using namespace VIEW_COMMON;
    CDrawingDim* pDim = reinterpret_cast<CDrawingDim*>(pObj);
    try
    {
        bool bFlip;
        bFlip = pData->anyData.GetBool();
        if (pDim->m_bArrowFlip == bFlip)
        {
            return true;   
        }
        pDim->AddChgCnt();
        pDim->m_bArrowFlip = bFlip;
        pDim->GetPartsDef()->Redraw();
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/*
 *  @brief   プロパティ変更(補助線1表示)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingDim::PropExt1(CStdPropertyItem* pData, void* pObj)
{
   using namespace VIEW_COMMON;
    CDrawingDim* pDim = reinterpret_cast<CDrawingDim*>(pObj);
    try
    {
        bool bShow;
        bShow  = pData->anyData.GetBool();

        if (pDim->m_bShowAux[0] == bShow)
        {
            return true;
        }

        pDim->m_bShowAux[0] = bShow;
        pDim->AddChgCnt();
        pDim->GetPartsDef()->Redraw();
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/*
 *  @brief   プロパティ変更(補助線2表示)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingDim::PropExt2       (CStdPropertyItem* pData, void* pObj)
{
   using namespace VIEW_COMMON;
    CDrawingDim* pDim = reinterpret_cast<CDrawingDim*>(pObj);
    try
    {
        bool bShow;
        bShow  = pData->anyData.GetBool();

        if (pDim->m_bShowAux[1] == bShow)
        {
            return true;
        }

        pDim->m_bShowAux[1] = bShow;
        pDim->AddChgCnt();
        pDim->GetPartsDef()->Redraw();
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/*
 *  @brief   プロパティ変更(位置１変更)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingDim::PropPos1       (CStdPropertyItem* pData, void* pObj)
{
   using namespace VIEW_COMMON;
    CDrawingDim* pDim = reinterpret_cast<CDrawingDim*>(pObj);
    try
    {
        POINT2D ptPos;
        ptPos  = pData->anyData.GetPoint();

        if (pDim->m_pt[0] == ptPos)
        {
            return true;
        }

        pDim->m_pt[0] = ptPos;
        pDim->AddChgCnt();
        pDim->_UpdateText();
        pDim->GetPartsDef()->Redraw();
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/*
 *  @brief   プロパティ変更(位置２変更)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingDim::PropPos2       (CStdPropertyItem* pData, void* pObj)
{
   using namespace VIEW_COMMON;
    CDrawingDim* pDim = reinterpret_cast<CDrawingDim*>(pObj);
    try
    {
        POINT2D ptPos;
        ptPos  = pData->anyData.GetPoint();

        if (pDim->m_pt[1] == ptPos)
        {
            return true;
        }

        pDim->m_pt[1] = ptPos;
        pDim->AddChgCnt();
        pDim->_UpdateText();
        pDim->GetPartsDef()->Redraw();
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(線幅)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingDim::PropLineWidth    (CStdPropertyItem* pData, void* pObj)
{
    CDrawingDim* pDim = reinterpret_cast<CDrawingDim*>(pObj);
    try
    {
        pDim->AddChgCnt();
        pDim->m_pCtrl->HideObject(pDim->m_nId);
        pDim->SetLineWidth(pData->anyData.GetInt());
        pDim->GetPartsDef()->Redraw();
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

//!< プロパティ値更新
void CDrawingDim::_PrepareUpdatePropertyGrid()
{
    CDrawingObject::_PrepareUpdatePropertyGrid();
}

/*
virtual POINT2D  CDrawingDim::GetTextCenter() const
{
    return 0.0;
}
*/

//寸法更新
void CDrawingDim::_UpdateText()
{
    StdStringStream strmDim;
    if (!m_psDimText->IsAuto())
    {
        return;
    }

    double dVal = GetVal();
    
    bool bAngle = false;
    if(m_eType == DT_DIM_A)
    {
        bAngle = true;
    }

DB_PRINT(_T("CDrawingDim::_UpdateText %f \n"), dVal );
    m_psDimText->SetVal(dVal, bAngle);
}


//!< 選択状態設定
void CDrawingDim::SetSelect(bool bSel, const RECT2D* pRect)
{
    m_eHit = H_NONE;
    CDrawingObject::SetSelect(bSel, pRect);

    if (bSel)
    {
        m_bSelPoint[0] = true;
        m_bSelPoint[1] = true;

        auto pSelect = m_pCtrl->GetSelectInstance();
        /*
        if ( bSel &&
             m_pCtrl &&
            (m_pCtrl->GetSelectInstance()->size() == 1))
            */
        {



        }
    }
    else
    {
        m_bSelPoint[0] = false;
        m_bSelPoint[1] = false;
    }

    if (pRect && bSel)
    {
        if (!pRect->IsInside (m_pt[0]))
        {
            m_bSelPoint[0] = false;
        }

        if (!pRect->IsInside (m_pt[1]))
        {
            m_bSelPoint[1] = false;
        }
    }
}


void CDrawingDim::SetSelectNoRelSelPoint(bool bSel)
{

    CDrawingObject::SetSelect(bSel);
}


/**
 *  @brief   スナップ点取得.
 *  @param   [out] pLstSnap スナップ点リスト
 *  @retval  true: スナップ点あり
 *  @note
 */
bool CDrawingDim::GetSnapList(std::list<SNAP_DATA>* pLstSnap)const 
{
    using namespace VIEW_COMMON;

    DWORD dwSnap = DRAW_CONFIG->GetSnapType();
    bool bRet = false;

    STD_ASSERT(pLstSnap);

    if (!pLstSnap)
    {
        return false;
    }

    pLstSnap->clear();
    SNAP_DATA snapData;

    if (dwSnap & SNP_END_POINT)
    {
        snapData.eSnapType = SNP_FEATURE_POINT;
        snapData.bKeepSelect = false;

        snapData.pt = GetPoint1();
        pLstSnap->push_back(snapData);

        snapData.pt = GetPoint2();
        pLstSnap->push_back(snapData);

        snapData.pt = GetTextCenter();
        pLstSnap->push_back(snapData);

        LINE2D l2;
        POINT2D ptNorm;
        _GetLengthMain(&l2, &ptNorm);

        snapData.pt = l2.GetPt1();
        pLstSnap->push_back(snapData);

        snapData.pt = l2.GetPt2();
        pLstSnap->push_back(snapData);

        bRet = true;
    }

    return bRet;
}


//!< マーカ初期化
bool CDrawingDim::_InitNodeMarker(CNodeMarker* pMarker,
                                  const POINT2D& ptText,
                                  bool bUseAuxLine)
{

    //ノードを表示してみる
    m_psNodeHeight[0] = std::make_unique<CDrawingNode>();
    m_psNodeHeight[1] = std::make_unique<CDrawingNode>();
    m_psNodeBottom[0] = std::make_unique<CDrawingNode>();
    m_psNodeBottom[1] = std::make_unique<CDrawingNode>();

    m_psNodeText      = std::make_unique<CDrawingNode>();
    m_bNodeMarker = true;

    /*
    CDrawingNode
    m_iMaxConnect            (1),
    m_eConnectionType        (E_ACTIVE),
    m_eConnectionObject      (NONE),
    m_eConnectionDirection   (E_IN),
    m_SnapType               (VIEW_COMMON::SNP_ALL_POINT)
    m_eType     = DT_NODE;
    m_crObj     = DRAW_CONFIG->crPoint;
    m_iPropFeatures   = P_BASE;
    SetUsePropertySet(true);
    */

    m_psNodeHeight[0]->SetVisible(true);
    m_psNodeHeight[1]->SetVisible(true);

    m_psNodeBottom[0]->SetVisible(bUseAuxLine);
    m_psNodeBottom[1]->SetVisible(bUseAuxLine);

    m_psNodeText->SetVisible(true);

    STD_ASSERT(pMarker);

    CDrawingView* pView = pMarker->GetView();
    double dLen = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->GetMarkerSize());  
    int iR =  CUtil::Round(dLen * pView->GetDpu());
 
    pMarker->SetRadius(iR);
    pMarker->SetObject(shared_from_this());

    m_psNodeHeight[0]->SetMarkerShape(DMS_CIRCLE_FILL);
    m_psNodeHeight[0]->SetName(MK_HIGHT1.c_str());

    m_psNodeHeight[1]->SetMarkerShape(DMS_CIRCLE_FILL);
    m_psNodeHeight[1]->SetName(MK_HIGHT2.c_str());

    m_psNodeBottom[0]->SetMarkerShape(DMS_CIRCLE_FILL);
    m_psNodeBottom[0]->SetName(MK_BOTTOM1.c_str());

    m_psNodeBottom[1]->SetMarkerShape(DMS_CIRCLE_FILL);
    m_psNodeBottom[1]->SetName(MK_BOTTOM2.c_str());

    m_psNodeText->SetMarkerShape(DMS_CIRCLE_FILL);
    m_psNodeText->SetName(MK_TEXT.c_str());


    m_psTmpLine = std::make_shared<CDrawingLine>();
    m_psTmpLine->SetColor(DRAW_CONFIG->crAdditional);
    m_psTmpLine->SetLineType(PS_DOT);




    //pMarker->Create(m_psNodeBottom[0].get(), l.GetPt1());
    //pMarker->Create(m_psNodeBottom[1].get(), l.GetPt2());

    pMarker->Create(m_psNodeText.get(), ptText);

    return true;

}


//!< マーカ初期化
bool CDrawingDim::InitNodeMarker(CNodeMarker* pMarker)
{
    /*
    LINE2D l;
    POINT2D ptNorm;
    _GetLengthMain(&l, &ptNorm);
    POINT2D ptText;

    l.PramPos(&ptText, m_ptLineCenter, m_dHText);

    bool bRet;
    bRet = _InitNodeMarker(pMarker,
                            ptText,
                            true);

    pMarker->Create(m_psNodeHeight[0].get(), l.GetPt1());
    pMarker->Create(m_psNodeHeight[1].get(), l.GetPt2());
    return bRet;
    */
    return false;
}

//!< マーカ選択
void CDrawingDim::SelectNodeMarker(CNodeMarker* pMarker, 
                                    StdString strMarkerId)
{
    if (strMarkerId == MK_HIGHT1)
    {
    }
    else if (strMarkerId == MK_HIGHT2)
    {
    }
    else if (strMarkerId == MK_TEXT)
    {
    }

}

//!< マーカ移動
void CDrawingDim::MoveNodeMarker(CNodeMarker* pMarker,
                                    SNAP_DATA* pSnap,
                                    StdString strMarkerId,
                                    MOUSE_MOVE_POS posMouse)
{
    pMarker->SetVisibleOnly(strMarkerId, true);

    if (strMarkerId != _T(""))
    {
        m_eHit = H_NONE;
    }

    if (strMarkerId == MK_HIGHT1)
    {
        SetTextCenter(pSnap->pt);
    }
    else if (strMarkerId == MK_HIGHT2)
    {
        SetTextCenter(pSnap->pt);
    }

}


//!< マーカ開放
bool CDrawingDim::ReleaseNodeMarker(CNodeMarker* pMarker,
                                    StdString strMarkerId,
                                     MOUSE_MOVE_POS posMouse)
{

    SetSelect(false);
    m_psNodeHeight[0].reset();
    m_psNodeHeight[1].reset();
    m_psNodeBottom[0].reset();
    m_psNodeBottom[1].reset();
    m_psNodeText.reset();
    m_psTmpLine.reset();
    m_bNodeMarker = false;

    return true;
}

//!< ノード変更
void CDrawingDim::ChangeNode(StdString strMarkerId, E_NODE_CHANGE_TYPE eType)
{
    if (strMarkerId == _T("HEIGHT"))
    {
    }
}

bool CDrawingDim::_IsHitArrow(const LINE2D &lineMain,
                              const POINT2D& pt2dMousePos)
{
    using namespace VIEW_COMMON;

    if ((m_eArrowType == AT_OPEN) ||
        (m_eArrowType == AT_FILLED))
    {

        double dLen;
        dLen = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->dArrowSize);  


        if (m_eArrowSide & ARS_START)
        {
            double dMouseToArrow;
            dMouseToArrow = lineMain.Pt(0).Distance(pt2dMousePos);

            if(dMouseToArrow < dLen)
            {
                return true;
            }
        }

        if (m_eArrowSide & ARS_END)
        {
            double dMouseToArrow;
            dMouseToArrow = lineMain.Pt(1).Distance(pt2dMousePos);

            if(dMouseToArrow < dLen)
            {
                return true;
            }
        }
    }
    return false;
}


//選択状態でのマウス移動
void CDrawingDim::SelectedMouseMove(CDrawingView* pView, MOUSE_MOVE_POS posMouse)
{
    //マーカが表示されていない時はまだ選択状態としない
    int iId  = pView->SearchPos(posMouse.ptSel);

    bool bOnNode;
    bOnNode = ((posMouse.nFlags & MK_ON_MARKER) != 0);

    if ((iId != m_nId) ||
        bOnNode)
    {
        m_eHit = H_NONE;
        return;
    }

    CPartsDef*pCtrl = pView->GetPartsDef();
    int iLayerId = pView->GetCurrentLayerId();
    POINT2D pt2dMousePos;
    POINT2D ptNear;
    pView->ConvScr2World(&pt2dMousePos, posMouse.ptSel, iLayerId);
    double dMinDistance = DBL_MAX;
    double dDistance = DBL_MAX;
    HIT_POSITON eHit = H_NONE;

    LINE2D lineMain;
    POINT2D ptNorm;
    _GetLengthMain(&lineMain, &ptNorm);

    bool bTop = m_psDimText->GetTextTop();


    if (m_psDimText->IsOnText(pt2dMousePos, bTop))
    {
        eHit = H_TEXT;
    }
    else
    {
        //文字列下側
#ifdef DIM_MAIN
        if (m_psDimText->IsOnText(pt2dMousePos, !bTop))
        {
            eHit = H_MAIN;
        }
#endif

        dMinDistance = pt2dMousePos.Distance(ptNear);




        if ((m_eType != DT_DIM_R) &&
            (m_eType == DT_DIM_D) &&
            (m_eType == DT_DIM_C))
        {
            //補助線1
            LINE2D* pLSub1 = &m_lineSub[0];
            ptNear = pLSub1->NearPointSegment(pt2dMousePos);
            dDistance = pt2dMousePos.Distance(ptNear);
            if (dMinDistance > dDistance)
            {
                dMinDistance = dDistance;
                eHit = H_SUB1;
            }

            //補助線2
            LINE2D* pLSub2 = &m_lineSub[1];
            ptNear = pLSub2->NearPointSegment(pt2dMousePos);
            dDistance = pt2dMousePos.Distance(ptNear);
            if (dMinDistance > dDistance)
            {
                dMinDistance = dDistance;
                eHit = H_SUB2;
            }
        }

        if(_IsHitArrow(lineMain, pt2dMousePos))
        {
            eHit = H_ARROW;
        }

    }
    m_eHit = eHit;
}

//選択状態でのマウス解放
void CDrawingDim::SelectedMouseUp  (CDrawingView* pView, MOUSE_MOVE_POS posMouse)
{
    switch(m_eHit)
    {

#ifdef DIM_MAIN
    case H_MAIN:
        m_psDimText->FlipTop();
        _PrepareUpdatePropertyGrid();
        break;
#endif
    case H_SUB1:
        break;
    case H_SUB2:
        break;
    case H_TEXT:
        {
            bool bAngle = false;
            if (m_eType == DT_DIM_A)
            {
                bAngle = true;
            }
            double dVal = GetVal();
            StdString strVal = m_psDimText->GetValDimText(dVal, bAngle);

            CDimTextDlg dlg;
            dlg.SetText(m_psDimText->GetDimText().c_str());
            dlg.SetValText(strVal.c_str());
            dlg.SetAuto(m_psDimText->IsAuto());

            if (dlg.DoModal() == IDOK)
            {
                m_psDimText->SetText(dlg.GetText().c_str());
                m_psDimText->SetAuto(dlg.IsAuto());
                _PrepareUpdatePropertyGrid();
            }
        }
        break;
    case H_ARROW:
        m_bArrowFlip = !m_bArrowFlip;
        _PrepareUpdatePropertyGrid();
    default:
        break;
    }

    DB_PRINT(_T("Selected Mouse Down\n"));
    m_eHit = H_NONE;
}

//選択状態でのマウス押下
void CDrawingDim::SelectedMouseDown(CDrawingView* pView, MOUSE_MOVE_POS posMouse)
{

}

//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void TEST_CDrawingDim()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());


    double dAngle1;
    double dAngle2;
    double dAngle;

    bool bRet;
    dAngle1 =  0.0;
    dAngle2 = 50.0;
    dAngle  = 30.0;
    bRet = CUtil::UpdateeAngle(&dAngle1, &dAngle2, dAngle);
    STD_ASSERT(!bRet);


    dAngle  = 60.0;
    bRet = CUtil::UpdateeAngle(&dAngle1, &dAngle2, dAngle);
    STD_ASSERT(bRet);
    STD_ASSERT_DBL_EQ(dAngle2, 60.0);

    dAngle1 =  0.0;
    dAngle2 = 50.0;
    dAngle  = 350.0;
    bRet = CUtil::UpdateeAngle(&dAngle1, &dAngle2, dAngle);
    STD_ASSERT(bRet);
    STD_ASSERT_DBL_EQ(dAngle1, dAngle);
    STD_ASSERT_DBL_EQ(dAngle2, 50.0);

    dAngle1 = 300.0;
    dAngle2 = 50.0;
    dAngle  = 20.0;
    bRet = CUtil::UpdateeAngle(&dAngle1, &dAngle2, dAngle);
    STD_ASSERT(!bRet);


    dAngle1 = 300.0;
    dAngle2 = 50.0;
    dAngle  = 100.0;
    bRet = CUtil::UpdateeAngle(&dAngle1, &dAngle2, dAngle);
    STD_ASSERT(bRet);
    STD_ASSERT_DBL_EQ(dAngle1, 300.0);
    STD_ASSERT_DBL_EQ(dAngle2, dAngle);
    

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}
#endif //_DEBUG