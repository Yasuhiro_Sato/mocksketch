/**
* @brief        CDrawingDimヘッダーファイル
* @file	        CDrawingDim.h
* @author           Yasuhiro Sato
* @date	        07-2-2009 20:03:09
* 
* @note	
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
* 
* @remarks	        
*			
*
* $
* $
* 
*/
#ifndef __DRAWING_DIM_H_
#define __DRAWING_DIM_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DrawingObject/CDrawingObject.h"

#include "DrawingObject/Primitive/LINE2D.h"
#include "DrawingObject/Primitive/MAT2D.h"
#include "Utility/ExtText/CExtText.h"


class CDimText;
class CDrawingDimL;
class CDrawingDimH;
class CDrawingDimV;
class CDrawingDimA;
class CDrawingDimD;
class CDrawingDimR;
class CDrawingDimC;


/**
 * @class   CDrawingDim
 * @brief  
 */
class CDrawingDim :public CDrawingObject//:public CRTTIClass<CDrawingDim, CDrawingObject, DT_LINE>
{
public:
    enum
    {
        T_POINT,
        T_LINE,
        T_CIRCLE,
        T_ELLIPSE,
        T_SPLINE,
    };
    
    enum SHOW_MODE
    {
        SM_SHOW,
        SM_HIDE,
        SM_DOT,
    };

    enum HIT_POSITON
    {
        H_NONE,
        H_MAIN,
        H_SUB1,
        H_SUB2,
        H_TEXT,
        H_ARROW,
    };

public:


public:
    //!< コンストラクタ
    CDrawingDim();

    //!< コンストラクタ
    CDrawingDim(int nID, DRAWING_TYPE eType);

    //!< コピーコンストラクタ
    CDrawingDim(const CDrawingDim& Obj);

    //!< デストラクタ
    virtual ~CDrawingDim();

    //!< 相対移動
    virtual void Move(const POINT2D& pt2D);

    //!< 回転
    virtual void Rotate(const POINT2D& pt2D, double dAngle);

    //!< 交点計算
    virtual void CalcIntersection ( std::vector<POINT2D>* pLiat,   
        const CDrawingObject* pObj, 
        bool bOnline = true,
        double dMin = NEAR_ZERO) const;

    //!< 鏡像
	virtual void Mirror(const LINE2D& line);

    //!< 倍率
	virtual void Scl(const POINT2D& pt2D, double dXScl, double dYScl);

    //!< 行列適用
    virtual void Matrix(const MAT2D& mat2D);

    //!< 調整
    virtual bool Trim (const POINT2D& ptIntersect, const POINT2D& ptClick, bool bReverse);

    //!< 分割
    virtual std::shared_ptr<CDrawingObject>  Break (const POINT2D& ptBreak);

    //!< 範囲
    virtual bool IsInner( const RECT2D& rcArea, bool bPart) const;

    //!< 領域取得
    virtual RECT2D GetBounds() const;

    //!< 距離
    virtual double Distance(const POINT2D& pt) const;

     //!< プロパティ初期化
    virtual TREE_GRID_ITEM*  _CreateStdPropertyTree();

    virtual void _PrepareUpdatePropertyGrid();

    //-------------------
    //!< プロパティ変更
    //-------------------
    static bool PropArrowType  (CStdPropertyItem* pData, void* pObj);
    static bool PropArrowDir   (CStdPropertyItem* pData, void* pObj);

    static bool PropExt1       (CStdPropertyItem* pData, void* pObj);
    static bool PropExt2       (CStdPropertyItem* pData, void* pObj);
    static bool PropPos1       (CStdPropertyItem* pData, void* pObj);
    static bool PropPos2       (CStdPropertyItem* pData, void* pObj);
    static bool PropLineWidth  (CStdPropertyItem* pData, void* pObj);

    //-------------------------
    // リフレクション設定
    //-------------------------
    //!< 位置取得
    POINT2D  GetPoint1() const            {return m_pt[0];}

    //!< 位置取得
    POINT2D  GetPoint2() const            {return  m_pt[1];}

    //!< 位置取得
    virtual POINT2D  GetTextCenter() const = 0;
    

    //!< スナップ点取得
    virtual  bool GetSnapList(std::list<SNAP_DATA>* pLstSnap)const;

    //-------------------------
    // 参照
    //-------------------------

    //!< 参照元の取得
    virtual CDrawingObject* UpdateRef();


    bool Recalc();

    virtual bool SetTextCenter(const POINT2D& pt) = 0;

    virtual bool SetTextOffset(const POINT2D& pt){ return false;}

    void SetTextGapToMainLine(double dGap);

    void Show(SHOW_MODE eShow);

    virtual double GetVal() const = 0;

    static double NormalizeTextAngle(double dAngle);


protected:
friend CDimText;

    virtual void _SetDefault();

    bool _GetPoint(POINT2D* pPt, const CDrawingObject* pObj);

    bool _CreateLineDim (const POINT2D& pt1, const POINT2D &pt2);

    bool _CreateArcDim(const CIRCLE2D& c1);

    bool _DrawigArrow(CDrawingView* pView, 
                    CDrawingObject* pMain,
                    int iWitdh,
                    COLORREF crPen,
                    int iId,
                    int iLayerId,
                     VIEW_COMMON::E_ARROW_TYPE eType,
                     VIEW_COMMON::E_ARROW_SIDE eSide,
                     bool bFlip) const;


    virtual void _GetLengthMain(LINE2D* pLineMane, POINT2D* ptNorm) const = 0;

    bool _SetTextPosLHV(double* pTextOffset, const POINT2D& pt,const POINT2D ptTextOrg);


    bool _IsNarrowAngle(const POINT2D& pt) const;

    virtual void  SetId(int iId);

    void _UpdateText();

    //!< 選択状態設定
    virtual void SetSelect(bool bSel, const RECT2D* pRect = NULL);

    virtual void SetSelectNoRelSelPoint(bool bSel);

    bool _IsHitArrow(const LINE2D &lineMain,
                                  const POINT2D& pt2dMousePos);


    bool _InitNodeMarker(CNodeMarker* pMarker,
                                  const POINT2D& ptText,
                                  bool bUseAuxLine);

    //!< 選択状態でのマウス
    virtual void SelectedMouseMove(CDrawingView* pView, MOUSE_MOVE_POS posMouse);
    virtual void SelectedMouseUp  (CDrawingView* pView, MOUSE_MOVE_POS posMouse);
    virtual void SelectedMouseDown(CDrawingView* pView, MOUSE_MOVE_POS posMouse);


    bool _CreateAuxCircle(std::unique_ptr<CIRCLE2D>& pSub,
                                       POINT2D ptOnArc,
                                       const CIRCLE2D& c,
                                       double dScl) const;



    //-------------------
    // ドラッグ用マーカ
    //-------------------
    //!< マーカ初期化 true:マーカあり
    virtual bool InitNodeMarker(CNodeMarker* pMarker) override;

    //!< マーカ選択
    virtual void SelectNodeMarker(CNodeMarker* pMarker, 
                                  StdString strMarkerId) override;

    //!< マーカ移動
    virtual void MoveNodeMarker(CNodeMarker* pMarker,
                                    SNAP_DATA* pSnap,
                                    StdString strMarkerId,
                                    MOUSE_MOVE_POS posMouse) override;

    //!< マーカ開放
    virtual bool ReleaseNodeMarker(CNodeMarker* pMarker, StdString strMarkerId, MOUSE_MOVE_POS posMouse) override;

    //!< ノード変更
    virtual void ChangeNode(StdString strMarkerId, E_NODE_CHANGE_TYPE eType) override;


    CDimText* GetDimText(){return m_psDimText.get();}

protected:
    friend CDrawingDim;
    friend CDrawingDimL;
    friend CDrawingDimH;
    friend CDrawingDimV;
    friend CDrawingDimA;
    friend CDrawingDimD;
    friend CDrawingDimR;
    friend CDrawingDimC;


    //計測オブジェクト
    int     m_iId[2]; //C I;

    POINT2D m_pt[2]; //C I


    //!< 寸法線中心
    // R <= 円中心
    // C <= 文字位置
    //POINT2D m_ptTextCenter;   //C I


    //!< 角度中心
    //POINT2D m_ptCenter;  //D,R,Cの場合は値をX値に保存 C I

    ///double m_dHText; //センター位置からの距離 C I
                     //角度の時はそのまま角度
                     // Chamferでは使用しない()


    int   m_iLineWidth; //線太さ //C  I

    //補助線表示有無
    bool m_bShowAux[2]; //C I

    //矢印種別
    VIEW_COMMON::E_ARROW_TYPE m_eArrowType; //C  I

    //矢印有無
    VIEW_COMMON::E_ARROW_SIDE m_eArrowSide; //C I

    //矢印反転
    bool m_bArrowFlip;                      //C I

    std::unique_ptr<CDimText> m_psDimText;
    std::unique_ptr<CDimText> m_psDimTextFlip;

    //-------------
    //保存不要

    bool    m_bSelPoint[2];                                     //C I

    //補助線
    mutable std::vector<LINE2D> m_lineSub;                      //C I

    //主寸法線
    mutable CDrawingObject* m_main;                             //C I

    std::unique_ptr<CDrawingNode> m_psNodeHeight[2];
    std::unique_ptr<CDrawingNode> m_psNodeBottom[2];
    std::unique_ptr<CDrawingNode> m_psNodeText;
    std::shared_ptr<CDrawingLine> m_psTmpLine;

    //!< 描画用（画面座標変換用）マトリクス  //保存不要
    mutable MAT2D                            m_matDraw;         

    SHOW_MODE m_eShow;                                          //C I

protected:
    //ヒットテスト用
    mutable HIT_POSITON           m_eHit;

    bool m_bNodeMarker;


    //マーカー名
    const StdString MK_HIGHT1   = _T("HEIGHT_1");
    const StdString MK_HIGHT2   = _T("HEIGHT_2");
    const StdString MK_BOTTOM1  = _T("BOTTOM_1");
    const StdString MK_BOTTOM2  = _T("BOTTOM_2");
    const StdString MK_TEXT     = _T("TEXT");

private:
    friend class boost::serialization::access;  
    template<class Archive>

    void serialize(Archive& ar, const unsigned int version)
    {
        std::string  s; try 
        { 
            DB_PRINT(_T("Start "));
            DB_FUNC_PRINT_TIME;
        //SERIALIZATION_INIT
            ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(CDrawingObject);

            SERIALIZATION_BOTH("ObjId1"     , m_iId[0]);
            SERIALIZATION_BOTH("ObjId2"     , m_iId[1]);
            SERIALIZATION_BOTH("LineWidth" , m_iLineWidth);

            if ((m_eType != DT_DIM_D) &&
                (m_eType != DT_DIM_R) &&
                (m_eType != DT_DIM_C))
            {
                SERIALIZATION_BOTH("Pt1"        , m_pt[0]);
                SERIALIZATION_BOTH("Pt2"        , m_pt[1]);
            }

            
            SERIALIZATION_BOTH("Aux1"        , m_bShowAux[0]);

            if ((m_eType != DT_DIM_D) &&
                (m_eType != DT_DIM_R) &&
                (m_eType != DT_DIM_C))
            {
                SERIALIZATION_BOTH("Aux2"        , m_bShowAux[1]);
            }

            SERIALIZATION_BOTH("ArrowType"   , m_eArrowType);

            if (m_eArrowType == VIEW_COMMON::AT_OPEN)
            {
                SERIALIZATION_BOTH("ArrowFlip"        , m_bArrowFlip);
            }

            SERIALIZATION_BOTH("Text" , *m_psDimText.get());
//FOR_DEBUG デバッグ用に展開
        //MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
        DB_PRINT(_T("End "));
        DB_FUNC_PRINT_TIME;
        }
        catch(std::exception e)
        {
            StdStringStream strm;
            StdString str     = CUtil::CharToString(s.c_str());
            StdString strWhat = CUtil::CharToString(e.what());

            strm << StdFormat(_T("%s %s:%s")) % 
                strWhat.c_str() % 
                DB_FUNC_NAME % str.c_str();

            long no = Archive::is_loading::value? e_file_read:e_file_write;
            STD_DBG(_T("%s"), strm.str().c_str());
            throw MockException(no, strm.str().c_str());
        }
        catch(...)
        {
            StdStringStream strm;
            StdString str     = CUtil::CharToString(s.c_str());

            strm << StdFormat(_T("%s:%s")) % 
                DB_FUNC_NAME % str.c_str();

            long no = Archive::is_loading::value? e_file_read:e_file_write;
            STD_DBG(_T("%s"), strm.str().c_str());
            throw MockException(no, strm.str().c_str());
        }
    }
};
BOOST_CLASS_VERSION(CDrawingDim, 0);


#endif // 