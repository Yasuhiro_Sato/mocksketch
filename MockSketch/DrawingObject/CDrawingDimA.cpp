/**
 * @brief			CDrawingDimA実装ファイル
 * @file			CDrawingDimA.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CDrawingDim.h"
#include "./CDrawingDimA.h"
#include "./CDrawingPoint.h"
#include "./CDrawingLine.h"
#include "./CDrawingCircle.h"
#include "./CDrawingSpline.h"
#include "./CDrawingEllipse.h"
#include "./CDrawingCompositionLine.h"
#include "./CDrawingParts.h"
#include "./CDrawingText.h"
#include "./CDrawingNode.h"
#include "./CDimText.h"
#include "DefinitionObject/View/CDrawingView.h"
#include "DefinitionObject/CPartsDef.h"
#include "Utility/CUtility.h"
#include "Utility/ExtText/CExtText.h"
#include "System/CSystem.h"
#include "SubWindow/DimTextDlg.h"

#include <math.h>
#include <boost/serialization/export.hpp> 
BOOST_CLASS_EXPORT(CDrawingDimA);


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
CDrawingDimA::CDrawingDimA():
CDrawingDim( 0, DT_DIM_A),
m_dHTextAngle(0.0)
{

    //TODO:実装
}

/**
 *  @brief  コンストラクター.
 *  @param  [in] nID 
 *  @retval なし 
 *  @note
 */
CDrawingDimA::CDrawingDimA(int nID):
CDrawingDim( nID, DT_DIM_A),
m_dHTextAngle(0.0)
{
    //TODO:実装
}


/**
 * コピーコンストラクタ
 */
CDrawingDimA::CDrawingDimA(const CDrawingDimA& Obj):
CDrawingDim( Obj)
{
     m_ptCenter        = Obj.m_ptCenter;
     m_ptTextOrg       = Obj.m_ptTextOrg;
     m_dHTextAngle     = Obj.m_dHTextAngle;
    _UpdateText();
}

/**
 *  @brief  デストラクタ.
 *  @param  なし 
 *  @retval なし 
 *  @note
 */
CDrawingDimA::~CDrawingDimA()
{
}

double CDrawingDimA::GetVal() const
{
    double dAngle = 0.0;
    bool bRet;
    bRet = CUtil::CalcAngle(&dAngle,
                    m_pt[0],
                    m_pt[1],
                    m_ptCenter);

    return dAngle;
}



/**
 *  @brief  回転.
 *  @param  [in]    pPt2D    回転中心(絶対座標)
 *  @param  [in]    dAngle   (絶対)回転角(rad)
 *  @retval     なし
 *  @note
 */
void CDrawingDimA::Rotate(const POINT2D& pt2D, double dAngle)
{
    if (m_main)
    {
        m_main->Rotate(pt2D, dAngle);
    }

    m_pt[0].Rotate(pt2D,dAngle);
    m_pt[1].Rotate(pt2D,dAngle);

    m_ptCenter.Rotate(pt2D,dAngle);
    m_ptTextOrg.Rotate(pt2D,dAngle);

    _UpdateText();
}

/**
 *  @brief  相対移動.
 *  @param  [in]    pPt2D   移動量
 *  @retval         なし
 *  @note
 */
void CDrawingDimA::Move(const POINT2D& pt2D) 
{
    AddChgCnt();

    if (m_main)
    {
        m_main->Move(pt2D);
    }
    m_pt[0].Move(pt2D);
    m_pt[1].Move(pt2D);

    m_ptCenter.Move(pt2D);
    m_ptTextOrg.Move(pt2D);

    _UpdateText();
}

/**
 *  @brief  鏡像.
 *  @param  [in]    line   対称軸
 *  @retval         なし
 *  @note
 */
void CDrawingDimA::Mirror(const LINE2D& line)
{
    if (m_main)
    {
        m_main->Mirror(line);
    }
    m_pt[0].Mirror(line);
    m_pt[1].Mirror(line);

    m_ptCenter.Mirror(line);
    m_ptTextOrg.Mirror(line);

    _UpdateText();
    AddChgCnt();
}

/**
 *  @brief  行列.
 *  @param  [in]    mat2D   適用する行列
 *  @retval         なし
 *  @note
 */
void CDrawingDimA::Matrix(const MAT2D& mat2D)
{
    if (m_main)
    {
        m_main->Matrix(mat2D);
    }
    m_pt[0].Matrix(mat2D);
    m_pt[1].Matrix(mat2D);

    m_ptCenter.Matrix(mat2D);
    m_ptTextOrg.Matrix(mat2D);

    _UpdateText();
    AddChgCnt();
}

/**
 *  @brief  倍率.
 *  @param  [in]    pPt2D   拡大中心
 *  @param  [in]    dSclX   X倍率
 *  @param  [in]    dSclY   Y倍率
 *  @retval         なし
 *  @note
 */
void CDrawingDimA::Scl(const POINT2D& pt2D, double dXScl, double dYScl)
{
    if (m_main)
    {
        m_main->Scl(pt2D, dXScl, dYScl);
    }

    m_pt[0].Scl(pt2D, dXScl, dYScl);
    m_pt[1].Scl(pt2D, dXScl, dYScl);
    m_ptCenter.Scl(pt2D, dXScl, dYScl);
    m_ptTextOrg.Scl(pt2D, dXScl, dYScl);

    _UpdateText();
    AddChgCnt();
}


void CDrawingDimA::_GetLengthMain(LINE2D* pLineMane, POINT2D* pNorm) const
{
    //矢印設定、マーカ表示に使用

    POINT2D ptNorm;
    POINT2D ptLen = m_ptTextOrg - m_ptCenter;
    POINT2D pt1 = m_pt[0] - m_ptCenter;
    POINT2D pt2 = m_pt[1] - m_ptCenter;
    double dRad = ptLen.Length();

    ptLen.Normalize();
    ptNorm = ptLen;

    POINT2D p1;
    POINT2D p2;
    if (dRad < NEAR_ZERO)
    {
        p1 = m_ptTextOrg;
        p2 = m_ptTextOrg;
    }
    else
    {
        pt1.Normalize();
        pt2.Normalize();

        p1.dX = pt1.dX * dRad + m_ptCenter.dX;
        p1.dY = pt1.dY * dRad + m_ptCenter.dY;

        p2.dX = pt2.dX * dRad + m_ptCenter.dX;
        p2.dY = pt2.dY * dRad + m_ptCenter.dY;
    }
    pLineMane->SetPt(p1, p2);
    *pNorm = ptNorm;
}


bool CDrawingDimA::SetAngleHeight(const POINT2D& pt, bool bFirst)
{

    // 2点目に選択したものを半径とする
    // 方向は挟角側とする
    // 180度の場合は

    POINT2D pt1 = m_pt[0] - m_ptCenter;
    POINT2D pt2 = m_pt[1] - m_ptCenter;
    POINT2D ptMouse = pt - m_ptCenter;

    pt1.Normalize();
    pt2.Normalize();

    double dCos = pt1.Dot(pt2);

    double dAngle = acos(dCos) * RAD2DEG;
    double dAngle1 = atan2(pt1.dY, pt1.dX) * RAD2DEG;
    double dAngle2 = atan2(pt2.dY, pt2.dX) * RAD2DEG;
    double dAngleM = atan2(ptMouse.dY, ptMouse.dX) * RAD2DEG;


    dAngle1 = CUtil::NormAngle(dAngle1);
    dAngle2 = CUtil::NormAngle(dAngle2);
    dAngleM = CUtil::NormAngle(dAngleM);


    if (dAngle1 > dAngle2)
    {
        std::swap(dAngle1, dAngle2);
        std::swap(m_pt[0], m_pt[1]);
    }


    if ((dAngle2 - dAngle1) > 180.0)
    {
        if (dAngleM < 180.0)
        {
            dAngleM += 360.0;
        }
        dAngle1 += 360.0;
        std::swap(dAngle1, dAngle2);
        std::swap(m_pt[0], m_pt[1]);
    }

    double dTmpAngle;
    double dCneterAngle;
    POINT2D ptCenter;

    dTmpAngle = (dAngle1 + dAngle / 2.0);
    dCneterAngle = dTmpAngle * DEG2RAD;

    ptCenter.dX = ptMouse.Length() * cos(dCneterAngle) + m_ptCenter.dX;
    ptCenter.dY = ptMouse.Length() * sin(dCneterAngle) + m_ptCenter.dY;

    POINT2D ptLc = m_ptTextOrg  - m_ptCenter;

    double dCorss1 = pt1.Cross(pt);
    double dCorss2 = pt2.Cross(pt);
    double dDot = ptLc.Dot(ptMouse);
#if 0
TRACE(_T("C1=%.2f, C2=%.2f, Dot=%.2f,\n"), dCorss1, dCorss2, dDot);
TRACE(_T("A1=%.2f, A2=%.2f, AM=%.2f, AM=%.2f,\n"), dAngle1, dAngle2, dAngleM, dTmpAngle);
TRACE(_T("Center(%.2f,%.2f)\n"), ptCenter.dX, ptCenter.dY);
TRACE(_T("P1 (%.2f,%.2f)\n"), m_pt[0].dX, m_pt[0].dY);
TRACE(_T("P2 (%.2f,%.2f)\n"), m_pt[1].dX, m_pt[1].dY);
TRACE(_T("Pt (%.2f,%.2f)\n"), pt.dX, pt.dY);
#endif
    /*
        角度寸法はは２直線を選択することで行う
        最初の選択は、直線のどちら側を選ぶ
        ２回めの選択は、寸法線の位置と内側の角(180度以下)か
        外側の角かを決める
    */

    bool bRev = false;
    if (!bFirst)
    {
        if ((dAngle1 >  dAngleM) ||
            (dAngle2 <  dAngleM))
        {
            bRev = true;
        }
    }

    if (bRev)
    {
        m_ptTextOrg.dX = - ptMouse.Length() * cos(dCneterAngle) + m_ptCenter.dX;
        m_ptTextOrg.dY = - ptMouse.Length() * sin(dCneterAngle) + m_ptCenter.dY;
        std::swap(m_pt[0], m_pt[1]);
    }
    else
    {
        m_ptTextOrg.dX = ptCenter.dX;
        m_ptTextOrg.dY = ptCenter.dY;
    }
    return true;
}

bool CDrawingDimA::SetTextCenter(const POINT2D& pt)
{
        POINT2D pt1 = m_pt[0] - m_ptCenter;
        POINT2D pt2 = m_pt[1] - m_ptCenter;
        double dR = m_ptCenter.Distance(pt);
        pt1.Normalize();
        pt2.Normalize();

        POINT2D pt3 = m_ptTextOrg - m_ptCenter;
        pt3.Normalize();
        m_ptTextOrg = pt3 * dR + m_ptCenter;
#if 0
TRACE(_T("pt3 (%0.2f, %0.2f)\n"), pt3.dX, pt3.dY);
TRACE(_T("m_ptCenter2 (%0.2f, %0.2f)\n"), m_ptCenter.dX, m_ptCenter.dY);
TRACE(_T("Rad1=%0.2f, (%0.2f, %0.2f)\n"), dR, m_ptLineCenter.dX, m_ptLineCenter.dY);
#endif
    return true;
}

bool CDrawingDimA::SetTextOffset(const POINT2D& pt)
{
    double  dAngle;
    double  dAngleCenter;

    dAngle = pt.Angle(m_ptCenter);
    dAngleCenter = m_ptTextOrg.Angle(m_ptCenter);

    m_dHTextAngle = CUtil::NormAngle(dAngle - dAngleCenter);

    return true;
}

POINT2D  CDrawingDimA::GetTextCenter() const
{
    POINT2D ptText;
    //TODP:実装

    double dPosAngle;

    dPosAngle= m_ptCenter.Angle(m_ptTextOrg) ;
    dPosAngle += m_dHTextAngle;

    dPosAngle = CUtil::NormAngle(dPosAngle);

    double dRad = m_ptCenter.Distance(m_ptTextOrg);

    ptText.dX = dRad * cos(dPosAngle * DEG2RAD) + m_ptCenter.dX;
    ptText.dY = dRad * sin(dPosAngle * DEG2RAD) + m_ptCenter.dY;


    return ptText;
}



//!< マーカ初期化
bool CDrawingDimA::InitNodeMarker(CNodeMarker* pMarker)
{
    POINT2D ptText;

    ptText = m_ptTextOrg;
    ptText.Rotate(m_ptCenter, m_dHTextAngle);

    static const bool bUseAuxLine = true;
    return CDrawingDim::_InitNodeMarker(pMarker,
                            ptText,
                            bUseAuxLine);
}

//!< マーカ選択
void CDrawingDimA::SelectNodeMarker(CNodeMarker* pMarker,
                                    StdString strMarkerId)
{
    CDrawingDim::SelectNodeMarker(pMarker, 
                                  strMarkerId);
}

//!< マーカ移動
void CDrawingDimA::MoveNodeMarker(CNodeMarker* pMarker, 
                                    SNAP_DATA* pSnap,
                                    StdString strMarkerId,
                                    MOUSE_MOVE_POS posMouse)
{
    CDrawingDim::MoveNodeMarker(pMarker, 
                                pSnap,
                                strMarkerId,
                                posMouse);
}

//!< マーカ開放
bool CDrawingDimA::ReleaseNodeMarker(CNodeMarker* pMarker,
                                        StdString strMarkerId,
                                        MOUSE_MOVE_POS posMouse)
{
    return CDrawingDim::ReleaseNodeMarker(pMarker, strMarkerId, posMouse);
}

//!< ノード変更
void CDrawingDimA::ChangeNode(StdString strMarkerId, E_NODE_CHANGE_TYPE eType)
{
}


bool CDrawingDimA::Create(const CDrawingObject* pObj1, 
                               const CDrawingObject* pObj2,
                               const POINT2D& pt1,
                               const POINT2D& pt2,
                               bool bFirst)
{

    if (!pObj1 ||
        !pObj2)
    {
        return false;
    }


    if (pObj1 == pObj2)
    {
        return false;
    }

    _SetDefault();


    if ((pObj1->GetType() != DT_LINE) ||
        (pObj2->GetType() != DT_LINE))
    {
        return false;
    }

    auto pL2D1 = dynamic_cast<const CDrawingLine*>(pObj1);
    auto pL2D2 = dynamic_cast<const CDrawingLine*>(pObj2);


    const LINE2D* pL1 = pL2D1->GetLineInstance();
    const LINE2D* pL2 = pL2D2->GetLineInstance();


    POINT2D ptCenter;
    bool bRet;

    bRet = pL1->IntersectInfnityLine(&ptCenter, *pL2);

    if (!bRet)
    {
        return false;
    }

    m_ptCenter = ptCenter;


    //交点からマウスが選択された方向で端点を決める
    m_pt[0] = pL1->GetSide( m_ptCenter,  pt1);
    m_pt[1] = pL2->GetSide( m_ptCenter,  pt2);

    double dL1 = m_ptCenter.Distance(m_pt[0]);
    double dL2 = m_ptCenter.Distance(m_pt[1]);
    if ((dL1 * dL2) < NEAR_ZERO )
    {
        return false;
    }

    _CreateDim(m_pt[0], m_pt[1]);

    if (bFirst)
    {
        SetAngleHeight(pt2, true);
    }

    _UpdateText();
    return true;
}

void CDrawingDimA::_SetDefault()
{
    CDrawingDim::_SetDefault();
    m_ptCenter.Set(0.0, 0.0);
    m_ptTextOrg.Set(0.0, 0.0);
    m_dHTextAngle     = 0.0;


    m_bShowAux[0] = true;
    m_bShowAux[1] = true;
    m_psDimText->SetPrecision(DRAW_CONFIG->iDimLengthPrecision);

}

bool CDrawingDimA::_CreateDim(const POINT2D& pt1, const POINT2D &pt2)
{

    if (!m_main)
    {
        m_main = new CDrawingCircle;
    }

    m_pt[0] = pt1;
    m_pt[1] = pt2;

    if (m_lineSub.size() != 2)
    {
        m_lineSub.clear();
        m_lineSub.push_back(LINE2D());
        m_lineSub.push_back(LINE2D());
    }
    return true;
}



void CDrawingDimA::Draw(CDrawingView* pView, 
                        const std::shared_ptr<CDrawingObject> pParentObject,
                        E_MOUSE_OVER_TYPE eMouseOver,
                        int iLayerId)
{
    COLORREF crPen;
    int      iId;

    bool bIgnoreSelect = false;
    if (eMouseOver == E_MO_IGNORE_SELECT)
    {
        bIgnoreSelect = true;
    }

    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }

    crPen = GetDrawColor(pView, iLayerId, pParentObject.get(), &iId, bIgnoreSelect);
    _Draw(pView, crPen, iId, iLayerId, pParentObject.get(), eMouseOver);
}

void CDrawingDimA::DeleteView(CDrawingView* pView, 
                              const std::shared_ptr<CDrawingObject> pParentObject,
                              int iLayerId)
{
    COLORREF crObj;
    crObj = DRAW_CONFIG->GetBackColor();

    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }

    _Draw(pView, crObj, -1, iLayerId, pParentObject.get(), E_MO_NO);
}



void CDrawingDimA::_Draw(CDrawingView* pView, 
                        COLORREF crPen,
                        int      iId,
                        int iLayerId,
                        const CDrawingObject* pParentObject,
                        E_MOUSE_OVER_TYPE eMouseOver){

    if (m_eShow == SM_HIDE)
    {
        return;
    }

    if (!m_main)
    {
        return ;
    }


    bool bChgMat = false;
    MAT2D matDraw;
    if (pParentObject )
    {
        const MAT2D* pMat = pParentObject->GetDrawingMatrix();
        if (pMat)
        {
            matDraw = *pMat;
        }
    }

    if (m_pMatOffset)
    {
        matDraw = matDraw * (*m_pMatOffset);
        bChgMat = true;
    }



    int iWidth = m_iLineWidth;
    if (eMouseOver == E_MO_SELECTABLE)
    {
        pView->SetLineAlternateMode(true);
        iWidth += 2;
    }
    else if (eMouseOver == E_MO_EMPHASIS)
    {
        crPen = DRAW_CONFIG->crEmphasis;
    }
	else if (eMouseOver == E_MO_CONNECTABLE)
	{
		crPen = DRAW_CONFIG->crConnectable;
	}

    double dTop = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->dDimTop);
    double dGap = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->dDimGap);

    auto pC = dynamic_cast<CDrawingCircle*>(m_main);

    if (!pC)
    {
        return;
    }
    
    POINT2D ptCenter;
    POINT2D ptTextOrg;
    POINT2D pt1;
    POINT2D pt2;
    CIRCLE2D* psCircle = NULL;
    CIRCLE2D* cMain = pC->GetCircleInstance();

    if (bChgMat)
    {
        ptCenter = m_ptCenter * matDraw;
        ptTextOrg = m_ptTextOrg * matDraw;
        pt1 = m_pt[0] * matDraw;
        pt2 = m_pt[1] * matDraw;

        psCircle =  new CIRCLE2D(*cMain);
        psCircle->Matrix2(*m_pMatOffset);
        cMain = psCircle;

    }
    else
    {
        ptCenter = m_ptCenter * matDraw;
        ptTextOrg = m_ptTextOrg * matDraw;
        pt1 = m_pt[0] * matDraw;
        pt2 = m_pt[1] * matDraw;

    }



    pC->SetCenter(ptCenter);

    double dRad = ptCenter.Distance(ptTextOrg);
    pC->SetRadius(dRad);


    POINT2D ptTmp = ptTextOrg - ptCenter;
    ptTmp.Normalize();
#if 0
TRACE(_T("ptTmp (%0.2f, %0.2f)\n"), ptTmp.dX, ptTmp.dY);
TRACE(_T("m_ptCenter1 (%0.2f, %0.2f)\n"), m_ptCenter.dX, m_ptCenter.dY);
TRACE(_T("Rad=%0.2f, (%0.2f, %0.2f)\n"), dRad, m_ptLineCenter.dX, m_ptLineCenter.dY);
#endif
    LINE2D* lSub1 = &m_lineSub[0];
    LINE2D* lSub2 = &m_lineSub[1];


    POINT2D ptNorm1 = (pt1 - ptCenter);
    POINT2D ptNorm2 = (pt2 - ptCenter);
    POINT2D ptC = ptTextOrg - ptCenter;


    ptNorm1.Normalize();
    ptNorm2.Normalize();

    POINT2D ptDir = (ptNorm1 + ptNorm2) / 2.0;

    bool bInnerAngle = ptDir.IsSameDirection(ptC);

    POINT2D ptS = dRad * ptNorm1 + ptCenter;
    POINT2D ptE = dRad * ptNorm2 + ptCenter;

    POINT2D ptGap1;
    POINT2D ptGap2;

    bool bShowSub1 = true;
    bool bShowSub2 = true;

    if (ptS.Distance(ptCenter) < pt1.Distance(ptCenter))
    {
        bShowSub1 = false;
    }

    if (ptE.Distance(ptCenter) < pt2.Distance(ptCenter))
    {
        bShowSub2 = false;
    }

    if (ptS.Distance(pt1) > dGap)
    {
        lSub1->SetPt1(pt1 + ptNorm1 * dGap);
    }
    else
    {
        lSub1->SetPt1(ptS);
    }


    if (ptE.Distance(pt2) > dGap)
    {
        lSub2->SetPt1(pt2 + ptNorm2 * dGap);
    }
    else
    {
        lSub2->SetPt1(ptE);
    }

    lSub1->SetPt2(ptS + ptNorm1 * dTop);
    lSub2->SetPt2(ptE + ptNorm2 * dTop);

    
    //テキスト位置

    double dAngle;    //文字の表示方向
    double dPosAngle; //テキストの位置
    double dPosTextCenter; //テキストの位置


    dPosAngle= ptCenter.Angle(ptTextOrg) ;
    dPosAngle += m_dHTextAngle;

    dPosAngle = CUtil::NormAngle(dPosAngle);
    dPosTextCenter = dPosAngle;

    dAngle = dPosAngle;

    POINT2D ptTextCnter;

    ptTextCnter.dX = dRad * cos(dPosAngle * DEG2RAD) + ptCenter.dX;
    ptTextCnter.dY = dRad * sin(dPosAngle * DEG2RAD) + ptCenter.dY;

    dAngle = dAngle - 90.0;
    if (dAngle >= 180.0)
    {
        dAngle -= 180.0;
    }

    m_psDimText->SetPos(ptTextCnter);
    m_psDimText->SetAngle(dAngle);
    COLORREF crText = crPen;
    if (m_eHit == CDrawingDim::H_TEXT)
    {
        crText =  DRAW_CONFIG->crImaginary;
    }
    m_psDimText->Draw(pView, crText,iId, iLayerId, eMouseOver);


    double dDspScl = pView->GetViewScale();



    pC->SetLineWidth(iWidth);
    if (m_eShow == SM_DOT)
    {
        pC->SetLineType(PS_DOT);
    }
    else
    {
        pC->SetLineType(PS_SOLID);
    }

    
    double dAngle1 = ptCenter.Angle(pt1);
    double dAngle2 = ptCenter.Angle(pt2);

    if (m_eShow != SM_DOT)
    {
        double d2 = m_psDimText->GetTextSize().dX / 2.0;
        double dTextWidthAngle;

        double dAngleTextLen = asin (d2 / (m_psDimText->GetTextGap() + dRad)) * RAD2DEG;

        dTextWidthAngle = CUtil::NormAngle(dAngleTextLen);
        double dTestS = dPosTextCenter + dTextWidthAngle;
        double dTestE = dPosTextCenter - dTextWidthAngle;

//TRACE(_T("d2=%.2f dAngle=%.2f  dAngleTextLen= %.2f\n"),d2, dPosTextCenter, dAngleTextLen);

        CUtil::UpdateeAngle(&dAngle1, &dAngle2, dTestS);
        CUtil::UpdateeAngle(&dAngle1, &dAngle2, dTestE);
    }

    pC->SetStart(dAngle1);
    pC->SetEnd(dAngle2);

    pC->SetId(iId);
    pC->SetColor(crPen);
    pC->Draw( pView, NULL, eMouseOver);
   
   if (m_eShow != SM_DOT)
   {
       if ((m_bShowAux[0]) && bShowSub1)
       {
           Line(pView, iLayerId, lSub1, PS_SOLID, iWidth, crPen, iId);
       }

       if ((m_bShowAux[1]) && bShowSub2)
       {
           Line(pView, iLayerId, lSub2, PS_SOLID, iWidth, crPen, iId);
       }

 
        //---------------------
        // 矢印描画
        //---------------------
        using namespace VIEW_COMMON;

        bool bHitArrow = false;
        if ((m_eArrowType == AT_OPEN) ||
            (m_eArrowType == AT_FILLED))
        {
            if (m_eHit == H_ARROW)
            {
                _DrawigArrow(pView, 
                    m_main,
                    iWidth, 
                    DRAW_CONFIG->crImaginary,
                    iId, 
                    iLayerId,
                    m_eArrowType, 
                    m_eArrowSide, 
                    !m_bArrowFlip);
                bHitArrow = true;
            }
        }

        if (!bHitArrow)
        {
            _DrawigArrow(pView, 
                                m_main,
                                iWidth, 
                                crPen,
                                iId,
                                iLayerId,
                                m_eArrowType, 
                                m_eArrowSide, 
                                m_bArrowFlip);
        }
        //---------------------
       //点線の隙間を塗りつぶすモード
   }

   pView->SetLineAlternateMode(false);

    if (psCircle)
    {
        delete psCircle;
    }

   return ;
}


//選択状態でのマウス移動
void CDrawingDimA::SelectedMouseMove(CDrawingView* pView, MOUSE_MOVE_POS posMouse)
{
     //マーカが表示されていない時はまだ選択状態としない
    int iId  = pView->SearchPos(posMouse.ptSel);

    bool bOnNode;
    bOnNode = ((posMouse.nFlags & MK_ON_MARKER) != 0);

    if ((iId != m_nId) ||
        bOnNode)
    {
        m_eHit = H_NONE;
        return;
    }

    CPartsDef*pCtrl = pView->GetPartsDef();
    int iLayerId = pView->GetCurrentLayerId();
    POINT2D pt2dMousePos;
    POINT2D ptNear;
    pView->ConvScr2World(&pt2dMousePos, posMouse.ptSel, iLayerId);
    double dMinDistance = DBL_MAX;
    double dDistance = DBL_MAX;
    HIT_POSITON eHit = H_NONE;

    LINE2D lineMain;
    POINT2D ptNorm;
    _GetLengthMain(&lineMain, &ptNorm);

    if (m_psDimText->IsOnText(pt2dMousePos))
    {
        eHit = H_TEXT;
    }
    else
    {
        //文字列下側
        double dR = m_psDimText->GetTextSize().dX / 2.0;

        CDrawingCircle* pDrawingCircle;
        pDrawingCircle = dynamic_cast<CDrawingCircle*>(m_main);

        bool bOnLine;
        bOnLine = pDrawingCircle->NearPoint(&ptNear, pt2dMousePos, true);
        if (bOnLine)
        {
            POINT2D ptS = m_ptCenter - m_psDimText->GetTextStart();
            POINT2D ptE = m_ptCenter - m_psDimText->GetTextEnd();

            //!< 角度計算
            double dS = CUtil::GetAngle(m_ptCenter, m_psDimText->GetTextStart()); 
            double dE = CUtil::GetAngle(m_ptCenter, m_psDimText->GetTextEnd()); 
            double dA = CUtil::GetAngle(m_ptCenter, ptNear); 

            if (dS > dE)
            {
                std::swap(dS, dE);
                if ((dE - dS) > 180.0)
                {
                    std::swap(dS, dE);
                }
            }

#ifdef DIM_MAIN
            if (CUtil::IsInsideAngle(dS, dE, dA))
            {
                eHit = H_MAIN;
            }
#endif
        }

        dMinDistance = pt2dMousePos.Distance(ptNear);


        if(_IsHitArrow(lineMain, pt2dMousePos))
        {
            eHit = H_ARROW;
        }

    }
    m_eHit = eHit;
}


/**
 * @brief   領域取得
 * @param   なし
 * @retval  なし
 * @note	描画領域を取得する
 */
RECT2D CDrawingDimA::GetBounds() const
{
    RECT2D rc;
    
    rc = m_ptCenter;
    return rc;
}

//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void TEST_CDrawingDimA()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}
#endif //_DEBUG