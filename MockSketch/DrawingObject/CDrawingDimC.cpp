/**
 * @brief			CDrawingDimC実装ファイル
 * @file			CDrawingDimC.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CDrawingDim.h"
#include "./CDrawingDimC.h"
#include "./CDrawingPoint.h"
#include "./CDrawingLine.h"
#include "./CDrawingCircle.h"
#include "./CDrawingSpline.h"
#include "./CDrawingEllipse.h"
#include "./CDrawingCompositionLine.h"
#include "./CDrawingParts.h"
#include "./CDrawingText.h"
#include "./CDrawingNode.h"
#include "./CDimText.h"
#include "DefinitionObject/View/CDrawingView.h"
#include "DefinitionObject/CPartsDef.h"
#include "Utility/CUtility.h"
#include "Utility/ExtText/CExtText.h"
#include "System/CSystem.h"
#include "SubWindow/DimTextDlg.h"

#include <math.h>
#include <boost/serialization/export.hpp> 
BOOST_CLASS_EXPORT(CDrawingDimC);


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
CDrawingDimC::CDrawingDimC():
CDrawingDim( 0, DT_DIM_C),
m_dCamfer(0.0),
m_dLayerScl(1.0)
{
    //TODO:実装
}

/**
 *  @brief  コンストラクター.
 *  @param  [in] nID 
 *  @retval なし 
 *  @note
 */
CDrawingDimC::CDrawingDimC(int nID):
CDrawingDim( nID, DT_DIM_C),
m_dCamfer(0.0),
m_dLayerScl(1.0)
{
    //TODO:実装
}


/**
 * コピーコンストラクタ
 */
CDrawingDimC::CDrawingDimC(const CDrawingDimC& Obj):
CDrawingDim( Obj)
{
    m_dCamfer       = Obj.m_dCamfer;
    m_ptTextCenter  = Obj.m_ptTextCenter;
    m_lChamfer      = Obj.m_lChamfer;
    m_dLayerScl     = 1.0;
    _UpdateText();
}

/**
 *  @brief  デストラクタ.
 *  @param  なし 
 *  @retval なし 
 *  @note
 */
CDrawingDimC::~CDrawingDimC()
{
}


/**
 *  @brief  回転.
 *  @param  [in]    pPt2D    回転中心(絶対座標)
 *  @param  [in]    dAngle   (絶対)回転角(rad)
 *  @retval     なし
 *  @note
 */
void CDrawingDimC::Rotate(const POINT2D& pt2D, double dAngle)
{
    if (m_main)
    {
        m_main->Rotate(pt2D, dAngle);
    }
    m_ptTextCenter.Rotate(pt2D,dAngle);
    m_lChamfer.Rotate(pt2D,dAngle);
    _UpdateText();
}

/**
 *  @brief  相対移動.
 *  @param  [in]    pPt2D   移動量
 *  @retval         なし
 *  @note
 */
void CDrawingDimC::Move(const POINT2D& pt2D) 
{
    AddChgCnt();

    if (!m_bSelPoint[0] && !m_bSelPoint[1])
    {
        if (m_main)
        {
            m_main->Move(pt2D);
        }
        m_ptTextCenter.Move(pt2D);
        m_lChamfer.Move(pt2D);
    }
    _UpdateText();
}

/**
 *  @brief  鏡像.
 *  @param  [in]    line   対称軸
 *  @retval         なし
 *  @note
 */
void CDrawingDimC::Mirror(const LINE2D& line)
{
    if (m_main)
    {
        m_main->Mirror(line);
    }
    m_ptTextCenter.Mirror(line);
    m_lChamfer.Mirror(line);
    _UpdateText();
    AddChgCnt();
}

/**
 *  @brief  行列.
 *  @param  [in]    mat2D   適用する行列
 *  @retval         なし
 *  @note
 */
void CDrawingDimC::Matrix(const MAT2D& mat2D)
{
    if (m_main)
    {
        m_main->Matrix(mat2D);
    }
    m_ptTextCenter.Matrix(mat2D);
    m_lChamfer.Matrix(mat2D);
    _UpdateText();
    AddChgCnt();
}

/**
 *  @brief  倍率.
 *  @param  [in]    pPt2D   拡大中心
 *  @param  [in]    dSclX   X倍率
 *  @param  [in]    dSclY   Y倍率
 *  @retval         なし
 *  @note
 */
void CDrawingDimC::Scl(const POINT2D& pt2D, double dXScl, double dYScl)
{
    if (m_main)
    {
        m_main->Scl(pt2D, dXScl, dYScl);
    }
    m_ptTextCenter.Scl(pt2D, dXScl, dYScl);
    m_lChamfer.Scl(pt2D, dXScl, dYScl);
    _UpdateText();
    AddChgCnt();
}

double CDrawingDimC::GetVal() const
{
    double dVal = 0.0;
    dVal = m_dCamfer;
    return dVal;
}

void CDrawingDimC::_GetLengthMain(LINE2D* pLineMane, POINT2D* pNorm) const
{
    //矢印設定、マーカ表示に使用
    //ptNorm 未使用
    //POINT2D ptNorm;
    auto pL = dynamic_cast<CDrawingLine*>(m_main);
    if (pL)
    {
        //lMain->SetPt1(ptOnline);
        //lMain->SetPt2(ptTextEnd);
        pLineMane->SetPt(pL->GetPoint1(), pL->GetPoint2());
    }
}

bool CDrawingDimC::_CreateAuxLine( const LINE2D& l) const
{
    //補助線
    POINT2D ptArrow;
    POINT2D ptMid = (l.GetPt1() + l.GetPt2()) / 2.0;
    ptArrow = l.NearPoint(m_ptTextCenter);


    double dArrowPos;
    l.PosPram(&dArrowPos, ptMid, ptArrow);


    double dTop = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->dDimTop) / m_dLayerScl;
    double dGap = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->dDimGap) / m_dLayerScl;

    double dWidth = (l.Length() / 2.0) + dGap;

    POINT2D ptStartSub;
    POINT2D ptEndSub;
    bool bSub = false;
    if (dArrowPos > dWidth)
    {
        l.PramPos(&ptStartSub, ptMid, dWidth);
        l.PramPos(&ptEndSub,   ptMid, dArrowPos + dTop);
        bSub = true;
    }
    else if (dArrowPos < -dWidth)
    {
        l.PramPos(&ptStartSub, ptMid, -dWidth);
        l.PramPos(&ptEndSub,   ptMid, dArrowPos - dTop);
        bSub = true;
    }

    if (bSub)
    {
        if (m_lineSub.size() != 1)
        {
            m_lineSub.clear();
            m_lineSub.push_back(LINE2D());
        }

        LINE2D* lSub1 = &m_lineSub[0];
        lSub1->SetPt1(ptStartSub);
        lSub1->SetPt2(ptEndSub);
    }
    else
    {
        m_lineSub.clear();
    }

    return bSub;
}


bool CDrawingDimC::SetTextCenter(const POINT2D& ptMouse)
{
    double dL = m_dCamfer;

    //CActionDim::_MovePointCDimから呼び出し

    if (m_lChamfer.Length() < NEAR_ZERO)
    {
        return false;
    }

    POINT2D p1;
    p1 = m_lChamfer.NearPoint(ptMouse);

    POINT2D vec;
    vec = ptMouse - p1;
    if (vec.Length() < NEAR_ZERO)
    {
        LINE2D ln;
        ln = m_lChamfer.NormalLine(ptMouse);

        vec = ln.Pt(1) - ln.Pt(0);  
    }

    vec.Normalize();

    //     TEXT   TEXT
    //  ------->|<--------
    //
    //      |   |   | 
    //   C    A    B   D
    //  マウス位置がA,B以内にある場合は、位置固定

    //テキスト端のマージンを矢印の長さとする
    double dMargin;
    dMargin = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->dArrowSize);  
    dMargin  += (m_psDimText->GetTextSize().dX /2.0);

    double dClick;
    dClick = p1.Distance(ptMouse);


    double dDir = 1.0;
    
    POINT2D ptPos;
    if (fabs (dClick) < dMargin)
    {
        ptPos = dDir * dMargin * vec + p1;
    }
    else
    {
        ptPos =  ptMouse;
    }

    m_ptTextCenter = ptPos;

    _CreateAuxLine(m_lChamfer);

    _UpdateText();
    return true;
}

POINT2D  CDrawingDimC::GetTextCenter() const
{
    //m_ptがチャンファー元の直線を示す
    return m_ptTextCenter;
}

bool CDrawingDimC::SetArrowPosition(const POINT2D& pt)
{
    //矢印先端位置を
    POINT2D ptA = m_lChamfer.NearPoint( pt, false);

    POINT2D ptNear = m_lChamfer.NearPoint(m_ptTextCenter);
    POINT2D ptVec = m_ptTextCenter - ptNear;

   POINT2D ptOnLine  = m_lChamfer.NearPoint(pt);

    m_ptTextCenter = ptOnLine + ptVec;

    _CreateAuxLine(m_lChamfer);

    return true;
}


//!< マーカ初期化
bool CDrawingDimC::InitNodeMarker(CNodeMarker* pMarker)
{
    POINT2D ptText;

    ptText = GetTextCenter();

    static const bool bUseAuxLine = false;

    bool bRet;
    bRet = _InitNodeMarker(pMarker,
                            ptText,
                            false);


    POINT2D ptOnline = m_lChamfer.NearPoint(m_ptTextCenter);

    m_psNodeHeight[1]->SetVisible(false);
    pMarker->Create(m_psNodeHeight[0].get(), ptOnline);

    return true;

}

//!< マーカ選択
void CDrawingDimC::SelectNodeMarker(CNodeMarker* pMarker,
                                    StdString strMarkerId)
{
    CDrawingDim::SelectNodeMarker(pMarker, 
                                  strMarkerId);
}

//!< マーカ移動
void CDrawingDimC::MoveNodeMarker(CNodeMarker* pMarker, 
                                    SNAP_DATA* pSnap,
                                    StdString strMarkerId,
                                    MOUSE_MOVE_POS posMouse)
{
    pMarker->SetVisibleOnly(strMarkerId, true);
    bool bAuxLine = false;
    POINT2D ptAux;
    POINT2D pt = pSnap->pt;

    if (strMarkerId == _T(""))
    {
        m_eHit = H_NONE;
    }


    else if ((strMarkerId == MK_HIGHT1) ||
             (strMarkerId == MK_HIGHT2))
    {

        POINT2D ptSnap;
        using namespace VIEW_COMMON;
        if (pSnap->eSnapType != SNP_NONE)
        {
            /*
            //中点を通る直線をスナップする
            bool bSnap;

            POINT2D ptMid = (m_lChamfer.GetPt1() + m_lChamfer.GetPt2()) / 2.0;
            POINT2D ptNear = m_lChamfer.NearPoint(m_ptTextCenter);
            POINT2D ptVec = m_ptTextCenter - ptNear;
            POINT2D pMidVec = ptMid + ptVec;


            bSnap = pMarker->GetView()->GetLineSnapPoint(&ptSnap,  
                                      ptMid,  
                                      pMidVec,  
                                      pt);



            if (bSnap)
            {
                pSnap->strSnap = GetSnapName(SNP_MID_POINT);
                pt = ptSnap;
            }
            */

        }

        POINT2D ptOnChamfer = m_lChamfer.NearPoint(pt);
        SetArrowPosition(ptOnChamfer);

        bAuxLine = true;
        ptAux = ptOnChamfer;
    }
    else  if (strMarkerId == MK_TEXT)
    {
        bAuxLine = true;

        SetTextCenter(pt);
        ptAux = GetTextCenter();
        
    }

    if (bAuxLine)
    {
        CPartsDef*   pDef  = pMarker->GetView()->GetPartsDef();
        m_psTmpLine->SetPoint1(pt);
        m_psTmpLine->SetPoint2(ptAux);
        pDef->SetMouseEmphasis(m_psTmpLine);
    }}

//!< マーカ開放
bool CDrawingDimC::ReleaseNodeMarker(CNodeMarker* pMarker, StdString strMarkerId, MOUSE_MOVE_POS posMouse)
{
    return CDrawingDim::ReleaseNodeMarker(pMarker, strMarkerId, posMouse);
}

void CDrawingDimC::ChangeNode(StdString strMarkerId, E_NODE_CHANGE_TYPE eType)
{
}


/**
 *  @brief   スナップ点取得.
 *  @param   [out] pLstSnap スナップ点リスト
 *  @retval  true: スナップ点あり
 *  @note
 */
bool CDrawingDimC::GetSnapList(std::list<SNAP_DATA>* pLstSnap)const 
{
    using namespace VIEW_COMMON;

    DWORD dwSnap = DRAW_CONFIG->GetSnapType();
    bool bRet = false;

    STD_ASSERT(pLstSnap);

    if (!pLstSnap)
    {
        return false;
    }

    pLstSnap->clear();
    SNAP_DATA snapData;

    if (dwSnap & SNP_END_POINT)
    {
        snapData.eSnapType = SNP_FEATURE_POINT;
        snapData.bKeepSelect = false;

        //snapData.pt = m_ptTextCenter;
        //pLstSnap->push_back(snapData);
 

        bRet = true;
    }

    return bRet;
}

bool CDrawingDimC::Create(const CDrawingLine* pL, const POINT2D& ptMouse)
{

    _SetDefault();
    m_iId[0] = pL->GetId();

    bool bRet;
    bRet = _CreateDim(pL->GetPoint1(), pL->GetPoint2(), ptMouse, m_ptTextCenter);


    _UpdateText();
    return bRet;

}

bool CDrawingDimC::_CreateDim(const POINT2D& pt1,
                              const POINT2D& pt2,
                              const POINT2D& ptMouse,
                              const POINT2D& ptTextCneter)
{
    if (m_main)
    {
        if (m_main->GetType() != DT_LINE)
        {
            delete m_main;
            m_main = NULL;
        }
    }


    if (!m_main)
    {
        m_main = new CDrawingLine;
    }

    auto pLine= dynamic_cast<CDrawingLine*>(m_main);

    m_lChamfer.SetPt(pt1, pt2);
    double dL = m_lChamfer.Length() / sqrt(2.0);

    m_dCamfer = dL;

    POINT2D ptOnLine = m_lChamfer.NearPoint(ptMouse);
    
    pLine->SetPoint1(ptOnLine);
    pLine->SetPoint2(ptMouse);

    SetTextCenter(ptTextCneter);


    return true;
}

void CDrawingDimC::_SetDefault()
{
    CDrawingDim::_SetDefault();
    m_dCamfer = 0.0;
    m_ptTextCenter.Set(0.0, 0.0);
    m_psDimText->SetPrifix(DRAW_CONFIG->strPriFixChamfer.c_str());
    m_eArrowSide = VIEW_COMMON::ARS_START;

    m_bShowAux[0] = true;
    m_bShowAux[1] = false;
}


void CDrawingDimC::Draw(CDrawingView* pView, 
                        const std::shared_ptr<CDrawingObject> pParentObject,
                        E_MOUSE_OVER_TYPE eMouseOver,
                        int iLayerId)
{
    COLORREF crPen;
    int      iId;

    bool bIgnoreSelect = false;
    if (eMouseOver == E_MO_IGNORE_SELECT)
    {
        bIgnoreSelect = true;
    }

    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }

    crPen = GetDrawColor(pView, iLayerId, pParentObject.get(), &iId, bIgnoreSelect);
    _Draw(pView, crPen, iId, iLayerId, pParentObject.get(), eMouseOver);
}

void CDrawingDimC::DeleteView(CDrawingView* pView, 
                              const std::shared_ptr<CDrawingObject> pParentObject,
                              int iLayerId )
{
    COLORREF crObj;
    crObj = DRAW_CONFIG->GetBackColor();

    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }

    _Draw(pView, crObj, -1, iLayerId, pParentObject.get(), E_MO_NO);
}



void CDrawingDimC::_Draw(CDrawingView* pView, 
                        COLORREF crPen,
                        int      iId,
                        int      iLayerId,
                        const CDrawingObject* pParentObject,
                        E_MOUSE_OVER_TYPE eMouseOver)
{

    if (m_eShow == SM_HIDE)
    {
        return;
    }

    if (!m_main)
    {
        return;
    }

    // m_pt[0] <- チャンファー１点目
    // m_pt[1] <- チャンファー２点目


    auto pL = dynamic_cast<CDrawingLine*>(m_main);

    LINE2D* lMain = pL->GetLineInstance();

    POINT2D ptOnline = m_lChamfer.NearPoint(m_ptTextCenter);

    LINE2D l(ptOnline, m_ptTextCenter);

    MAT2D matDraw;
    if (pParentObject )
    {
        const MAT2D* pMat = pParentObject->GetDrawingMatrix();
        if (pMat)
        {
            matDraw = *pMat;
        }
    }

    if (m_pMatOffset)
    {
        matDraw = matDraw * (*m_pMatOffset);
    }


    l.Matrix(matDraw);


    int iWidth = m_iLineWidth;
    if (eMouseOver == E_MO_SELECTABLE)
    {
        pView->SetLineAlternateMode(true);
        iWidth += 2;
    }
    else if (eMouseOver == E_MO_EMPHASIS)
    {
        crPen = DRAW_CONFIG->crEmphasis;
    }
	else if (eMouseOver == E_MO_CONNECTABLE)
	{
		crPen = DRAW_CONFIG->crConnectable;
	}

    double dDspScl = pView->GetViewScale();


    POINT2D ptTextCnter;

    l.PramPos(&ptTextCnter, m_ptTextCenter, 0.0);
    double dPosAngle= CUtil::NormAngle(pL->GetLineInstance()->Angle());
    double dAngle = dPosAngle;

    //角度は0〜180に正規化する
    dAngle = NormalizeTextAngle(dAngle);

    m_psDimText->SetPos(ptTextCnter);
    m_psDimText->SetAngle(dAngle);
    COLORREF crText = crPen;
    if (m_eHit == CDrawingDim::H_TEXT)
    {
        crText =  DRAW_CONFIG->crImaginary;
    }
    m_psDimText->Draw(pView, crText,iId, iLayerId, eMouseOver);

    POINT2D ptTextEnd;
    double dL = (m_psDimText->GetTextSize().dX / 2.0);
    double dTextEnd;
    double dDir = 1.0;

    l.PosPram(&dDir, ptOnline, ptTextCnter);
    if (dDir < 0.0)
    {
        dTextEnd =  (-dL);
    }
    else
    {
        dTextEnd =   dL;
    }

    l.PramPos(&ptTextEnd, m_ptTextCenter, dTextEnd);

    lMain->SetPt1(ptOnline);
    lMain->SetPt2(ptTextEnd);

    pL->SetId(iId);
    pL->SetColor(crPen);
    pL->Draw( pView, NULL, eMouseOver);
   
    //補助線
    if (m_bShowAux[0])
    {
        CPartsDef* pCtrl = pView->GetPartsDef();
        CLayer* pLayer   = pCtrl->GetLayer(iLayerId);
        double dScl = pLayer->dScl;

        if (fabs(m_dLayerScl - dScl) > NEAR_ZERO)
        {
            m_dLayerScl = dScl;
            _CreateAuxLine(m_lChamfer);
        }

        if (m_lineSub.size() == 1)
        {
            Line(pView, iLayerId, &m_lineSub[0], PS_SOLID, iWidth, crPen, iId);
        }
    }


 
    //---------------------
    // 矢印描画
    //---------------------
    using namespace VIEW_COMMON;

    bool bHitArrow = false;
    if ((m_eArrowType == AT_OPEN) ||
        (m_eArrowType == AT_FILLED))
    {
        if (m_eHit == H_ARROW)
        {
            _DrawigArrow(pView, 
                m_main,
                iWidth, 
                DRAW_CONFIG->crImaginary,
                iId, 
                iLayerId,
                m_eArrowType, 
                m_eArrowSide, 
                !m_bArrowFlip);
            bHitArrow = true;
        }
    }

    if (!bHitArrow)
    {
        _DrawigArrow(pView, 
                            m_main,
                            iWidth, 
                            crPen,
                            iId,
                            iLayerId, 
                            m_eArrowType, 
                            m_eArrowSide, 
                            m_bArrowFlip);
    }
    //点線の隙間を塗りつぶすモード

   pView->SetLineAlternateMode(false);
   return;
}

//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void TEST_CDrawingDimC()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}
#endif //_DEBUG