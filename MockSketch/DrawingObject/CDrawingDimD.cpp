/**
 * @brief			CDrawingDimD実装ファイル
 * @file			CDrawingDimD.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CDrawingDim.h"
#include "./CDrawingDimD.h"
#include "./CDrawingPoint.h"
#include "./CDrawingLine.h"
#include "./CDrawingCircle.h"
#include "./CDrawingSpline.h"
#include "./CDrawingEllipse.h"
#include "./CDrawingCompositionLine.h"
#include "./CDrawingParts.h"
#include "./CDrawingText.h"
#include "./CDrawingNode.h"
#include "./CDimText.h"
#include "DefinitionObject/View/CDrawingView.h"
#include "DefinitionObject/CPartsDef.h"
#include "Utility/CUtility.h"
#include "Utility/ExtText/CExtText.h"
#include "System/CSystem.h"
#include "SubWindow/DimTextDlg.h"

#include <math.h>
#include <boost/serialization/export.hpp> 
BOOST_CLASS_EXPORT(CDrawingDimD);


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
CDrawingDimD::CDrawingDimD():
CDrawingDim( 0, DT_DIM_D),
m_bSub(false),
m_dLayerScl(1.0),
m_dTextOffset(0.0)
{
    //TODO:実装
}

/**
 *  @brief  コンストラクター.
 *  @param  [in] nID 
 *  @retval なし 
 *  @note
 */
CDrawingDimD::CDrawingDimD(int nID):
CDrawingDim( nID, DT_DIM_D),
m_bSub(false),
m_dLayerScl(1.0),
m_dTextOffset(0.0)
{
    //TODO:実装
}


/**
 * コピーコンストラクタ
 */
CDrawingDimD::CDrawingDimD(const CDrawingDimD& Obj):
CDrawingDim( Obj)
{
    m_c     = Obj.m_c;
    m_dTextOffset   = Obj.m_dTextOffset;
    m_bSub = Obj.m_bSub;

    if (Obj.m_pCircleSub)
    {
        m_pCircleSub = std::make_unique<CIRCLE2D>(*(Obj.m_pCircleSub.get()));
    }

    _UpdateText();
}

/**
 *  @brief  デストラクタ.
 *  @param  なし 
 *  @retval なし 
 *  @note
 */
CDrawingDimD::~CDrawingDimD()
{
}


/**
 *  @brief  回転.
 *  @param  [in]    pPt2D    回転中心(絶対座標)
 *  @param  [in]    dAngle   (絶対)回転角(rad)
 *  @retval     なし
 *  @note
 */
void CDrawingDimD::Rotate(const POINT2D& pt2D, double dAngle)
{
    if (m_main)
    {
        m_main->Rotate(pt2D, dAngle);
    }

    m_pt[0].Rotate(pt2D,dAngle);
    m_pt[1].Rotate(pt2D,dAngle);
    m_c.Rotate(pt2D,dAngle);
    if (m_pCircleSub)
    {
        m_pCircleSub->Rotate(pt2D,dAngle);
    }
    _UpdateText();
}

/**
 *  @brief  相対移動.
 *  @param  [in]    pPt2D   移動量
 *  @retval         なし
 *  @note
 */
void CDrawingDimD::Move(const POINT2D& pt2D) 
{
    AddChgCnt();

    if (m_main)
    {
        m_main->Move(pt2D);
    }
    m_pt[0].Move(pt2D);
    m_pt[1].Move(pt2D);

    m_c.Move(pt2D);
    if (m_pCircleSub)
    {
        m_pCircleSub->Move(pt2D);
    }

    _UpdateText();
}

/**
 *  @brief  鏡像.
 *  @param  [in]    line   対称軸
 *  @retval         なし
 *  @note
 */
void CDrawingDimD::Mirror(const LINE2D& line)
{
    if (m_main)
    {
        m_main->Mirror(line);
    }
    m_pt[0].Mirror(line);
    m_pt[1].Mirror(line);
    m_c.Mirror(line);
    if (m_pCircleSub)
    {
        m_pCircleSub->Mirror(line);;
    }

    _UpdateText();
    AddChgCnt();
}

/**
 *  @brief  行列.
 *  @param  [in]    mat2D   適用する行列
 *  @retval         なし
 *  @note
 */
void CDrawingDimD::Matrix(const MAT2D& mat2D)
{
    if (m_main)
    {
        m_main->Matrix(mat2D);
    }
    m_pt[0].Matrix(mat2D);
    m_pt[1].Matrix(mat2D);

    ELLIPSE2D* pEllipse = NULL;
    
    //楕円化する場合の対処は今のところ行わない
    m_c.Matrix(mat2D, &pEllipse);
    if (m_pCircleSub)
    {
        ELLIPSE2D* pEllipse2 = NULL;
        m_pCircleSub->Matrix(mat2D, &pEllipse2);
        if(pEllipse2){delete  pEllipse2;}
    }

    if (pEllipse){delete pEllipse;}

    _UpdateText();
    AddChgCnt();
}

/**
 *  @brief  倍率.
 *  @param  [in]    pPt2D   拡大中心
 *  @param  [in]    dSclX   X倍率
 *  @param  [in]    dSclY   Y倍率
 *  @retval         なし
 *  @note
 */
void CDrawingDimD::Scl(const POINT2D& pt2D, double dXScl, double dYScl)
{
    if (m_main)
    {
        m_main->Scl(pt2D, dXScl, dYScl);
    }

    m_pt[0].Scl(pt2D, dXScl, dYScl);
    m_pt[1].Scl(pt2D, dXScl, dYScl);

    ELLIPSE2D* pEllipse = NULL;
    m_c.Scl(pt2D, dXScl, dYScl, &pEllipse);
    if (m_pCircleSub)
    {
        ELLIPSE2D* pEllipse2 = NULL;
        m_pCircleSub->Scl(pt2D, dXScl, dYScl, &pEllipse2);
        if(pEllipse2){delete  pEllipse2;}
    }
    if (pEllipse){delete pEllipse;}

    _UpdateText();
    AddChgCnt();
}

double CDrawingDimD::GetVal() const
{
    double dVal = 0.0;
    dVal = m_c.dRad * 2.0;
    return dVal;
}


bool CDrawingDimD::Create(const CDrawingCircle* pC, const POINT2D& ptClick)
{
     _SetDefault();
     m_iId[0] = pC->GetId();

     POINT2D ptCenter;
     double dR;

     ptCenter = pC->GetCenter();
     dR       = pC->GetRadius();

    LINE2D l;
    l.SetPt1(ptCenter);
    if (ptClick.Distance(ptCenter) < NEAR_ZERO)
    {
        POINT2D pt(ptCenter.dX + dR, ptCenter.dY);
        l.SetPt2(pt);
    }
    else
    {
        l.SetPt2(ptClick);
    }
    std::vector<POINT2D> lstPoint;

    if(!pC->GetCircleInstance()->Intersect(l, &lstPoint))
    {
        return false;
    }

    if(lstPoint.size() != 2)
    {
        return false;
    }

    POINT2D pt;

    if (lstPoint[0].Distance(ptClick) <
        lstPoint[1].Distance(ptClick))
    {
        pt = lstPoint[0];
    }
    else
    {
        pt = lstPoint[1];
    }

    _CreateDim(*(pC->GetCircleInstance()), pt);

    _UpdateText();
    return true;
}

void CDrawingDimD::_GetLengthMain(LINE2D* pLineMain, POINT2D* pNorm) const
{
    //矢印設定、マーカ表示に使用
    POINT2D ptTmp;
    POINT2D ptNorm = m_pt[1] - m_pt[0];
    ptNorm.Rotate(ptTmp, 90.0);
    ptNorm.Normalize();
    pLineMain->SetPt(m_pt[0], m_pt[1]);
    *pNorm = ptNorm;
}



bool CDrawingDimD::SetAngleHeight(const POINT2D& pt, bool bAngle)
{
    //CActionDim::_MovePointDDim から使用
    // m_pt[0] <- 主寸法線端点1
    // m_pt[1] <- 主寸法線端点2
    // m_dTextOffset 寸法位置
    // を決定する

    double dRet;

    if (!bAngle)
    {
        //角度決定後にテキスト位置を設定
        LINE2D l2(m_pt[0], m_pt[1]);
        POINT2D ptOnLine = l2.NearPoint(pt);
        if(!l2.PosPram(&dRet, m_c.ptCenter, ptOnLine))
        {
            return false;
        
        }
        m_dTextOffset = dRet;
    }
    else
    {
        //角度決定時はテキスト位置は中心
        SetTextCenter(pt);
        m_dTextOffset = 0.0;
    }
    _UpdateText();
    return true;
}

bool CDrawingDimD::SetTextOffset(const POINT2D& pt)
{
     bool bRet;
     bRet = SetAngleHeight(pt, false);
     return  bRet;
}


bool CDrawingDimD::SetTextCenter(const POINT2D& pt)
{

    double dL = m_c.ptCenter.Distance(pt);
    if (dL < NEAR_ZERO)
    {
        return false;
    }



    double dR = m_c.dRad;

    CIRCLE2D c(m_c.ptCenter, dR);
    LINE2D l;
    l.SetPt1(m_c.ptCenter);
    if (m_c.ptCenter.Distance(pt) < NEAR_ZERO)
    {
        POINT2D ptTmp = m_c.ptCenter;
        ptTmp.dX += dR;
        l.SetPt2(ptTmp);
    }
    else
    {
        l.SetPt2(pt);
    }

    std::vector<POINT2D> lstPoint;
    c.Intersect(l, &lstPoint);

    if (lstPoint.size() != 2)
    {
        return false;
    }

    if (lstPoint[0].Distance(m_pt[0]) <
        lstPoint[1].Distance(m_pt[0]))
    {
        m_pt[0] = lstPoint[0];
        m_pt[1] = lstPoint[1];
    }
    else
    {
        m_pt[0] = lstPoint[1];
        m_pt[1] = lstPoint[0];
    }


    bool bSub = false;
    POINT2D ptOutside;
    if (!m_c.IsClose())
    {
        bSub = _GetOffCirclePoint(&ptOutside);
        if (bSub)
        {
            m_bSub = _CreateAuxCircle( m_pCircleSub, ptOutside, m_c, m_dLayerScl);
        }
    }
    return true;
}

bool CDrawingDimD::_GetOffCirclePoint(POINT2D* pPt) const
{
    bool bRet = false;
    if (!m_c.IsInsideAngle(m_pt[0]))
    {
        *pPt = m_pt[0];
        bRet = true;
    }
    else if (!m_c.IsInsideAngle(m_pt[1]))
    {
        *pPt = m_pt[1];
        bRet = true;
    }
    return bRet;
}

bool CDrawingDimD::_CreateDim    (const CIRCLE2D& c,
                                  const POINT2D& ptOnCircle)
{
    if (m_main)
    {
        if (m_main->GetType() != DT_LINE)
        {
            delete m_main;
            m_main = NULL;
        }
    }

    if (!m_main)
    {
        m_main = new CDrawingLine;
    }

    auto pLine= dynamic_cast<CDrawingLine*>(m_main);
    m_c = c;
    POINT2D ptCenter = m_c.GetCenter();
    m_pt[0] = ptOnCircle;
    m_pt[1] = ptCenter * 2.0 - ptOnCircle;

    pLine->SetPoint1(m_pt[0]);
    pLine->SetPoint2(m_pt[1]);
    return true;
}

POINT2D  CDrawingDimD::GetTextCenter() const
{
    POINT2D ptText;
    //TODP:実装

    return ptText;
}


//!< マーカ初期化
bool CDrawingDimD::InitNodeMarker(CNodeMarker* pMarker)
{
    LINE2D l;
    POINT2D ptNorm;
    _GetLengthMain(&l, &ptNorm);
    POINT2D ptText;

    l.PramPos(&ptText, m_c.ptCenter, m_dTextOffset);

    static const bool bUseAuxLine = false;

    bool bRet;
    bRet = _InitNodeMarker(pMarker,
                            ptText,
                            bUseAuxLine);


    pMarker->Create(m_psNodeHeight[0].get(), l.GetPt1());
    pMarker->Create(m_psNodeHeight[1].get(), l.GetPt2());

    return bRet;
}

//!< マーカ選択
void CDrawingDimD::SelectNodeMarker(CNodeMarker* pMarker, 
                                    StdString strMarkerId)
{
    CDrawingDim::SelectNodeMarker(pMarker, 
                                  strMarkerId);

}


//!< マーカ移動
void CDrawingDimD::MoveNodeMarker(CNodeMarker* pMarker, 
                                    SNAP_DATA* pSnap,
                                    StdString strMarkerId,
                                    MOUSE_MOVE_POS posMouse)
{

    bool bAuxLine = false;
    POINT2D ptAux;

    if ((strMarkerId == MK_HIGHT1) ||
        (strMarkerId == MK_HIGHT2))
    {

        POINT2D pt = pSnap->pt;
        using namespace VIEW_COMMON;
        if (pSnap->eSnapType == SNP_NONE)
        {
            bool bSnap;
            double dAngle;
            double    dLength;

            POINT2D ptSnap;
            bSnap = pMarker->GetView()->GetLengthSnapPoint(&dAngle,
                                        &dLength,
                                        &ptSnap,  
                                        m_c.ptCenter,  
                                        pt,
                                        true,         //bAngle
                                        false     );  //bDistance


            if (bSnap)
            {
                pSnap->pt = ptSnap;
                pSnap->strSnap = GetSnapName(SNP_ANGLE);
                pSnap->strSnap += CUtil::StrFormat(_T(":%03f"), dAngle);
            }
            SetTextCenter(pSnap->pt);
        }


        bAuxLine = true;
        ptAux = m_c.GetCenter();
    }

    else if (strMarkerId == MK_TEXT)
    {
        bAuxLine = true;
        SetAngleHeight(pSnap->pt, false);

        LINE2D l(m_pt[0], m_pt[1]);
        POINT2D ptTextCnter;
        l.PramPos(&ptTextCnter, m_c.ptCenter, m_dTextOffset);
        ptAux = ptTextCnter;
    }

    if (bAuxLine)
    {
        CPartsDef*   pDef  = pMarker->GetView()->GetPartsDef();
        m_psTmpLine->SetPoint1(pSnap->pt);
        m_psTmpLine->SetPoint2(ptAux);
        pDef->SetMouseEmphasis(m_psTmpLine);
    }

}

//!< マーカ開放
bool CDrawingDimD::ReleaseNodeMarker(CNodeMarker* pMarker, StdString strMarkerId, MOUSE_MOVE_POS posMouse)
{
    return CDrawingDim::ReleaseNodeMarker(pMarker, strMarkerId, posMouse);
}

void CDrawingDimD::ChangeNode(StdString strMarkerId, E_NODE_CHANGE_TYPE eType)
{
}


/**
 *  @brief   スナップ点取得.
 *  @param   [out] pLstSnap スナップ点リスト
 *  @retval  true: スナップ点あり
 *  @note
 */
bool CDrawingDimD::GetSnapList(std::list<SNAP_DATA>* pLstSnap)const 
{
    using namespace VIEW_COMMON;

    DWORD dwSnap = DRAW_CONFIG->GetSnapType();
    bool bRet = false;

    STD_ASSERT(pLstSnap);

    if (!pLstSnap)
    {
        return false;
    }

    pLstSnap->clear();
    SNAP_DATA snapData;

    if (dwSnap & SNP_END_POINT)
    {
        snapData.eSnapType = SNP_FEATURE_POINT;
        snapData.bKeepSelect = false;

        //snapData.pt = m_ptTextCenter;
        //pLstSnap->push_back(snapData);
 

        bRet = true;
    }

    return bRet;

}


void CDrawingDimD::_SetDefault()
{
    CDrawingDim::_SetDefault();
    m_c.dRad = 0.0;
    m_dTextOffset = 0.0;
    m_c.ptCenter.Set(0.0, 0.0);

    m_psDimText->SetPrifix(DRAW_CONFIG->strPriFixDiameter.c_str());
    m_bShowAux[0] = true;
    m_bShowAux[1] = false;
}


void CDrawingDimD::Draw(CDrawingView* pView, 
                        const std::shared_ptr<CDrawingObject> pParentObject,
                        E_MOUSE_OVER_TYPE eMouseOver,
                        int iLayerId)
{
    COLORREF crPen;
    int      iId;

    bool bIgnoreSelect = false;
    if (eMouseOver == E_MO_IGNORE_SELECT)
    {
        bIgnoreSelect = true;
    }

    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }


    crPen = GetDrawColor(pView, iLayerId, pParentObject.get(), &iId, bIgnoreSelect);
    _Draw(pView, crPen, iId, iLayerId, pParentObject.get(), eMouseOver);
}

void CDrawingDimD::DeleteView(CDrawingView* pView, 
                              const std::shared_ptr<CDrawingObject> pParentObject,
                              int iLayerId)
{
    COLORREF crObj;
    crObj = DRAW_CONFIG->GetBackColor();

    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }

    _Draw(pView, crObj, -1, iLayerId, pParentObject.get(), E_MO_NO);
}

void CDrawingDimD::_Draw(CDrawingView* pView, 
                        COLORREF crPen,
                        int      iId,
                        int iLayerId, 
                        const CDrawingObject* pParentObject,
                        E_MOUSE_OVER_TYPE eMouseOver){

    if (m_eShow == SM_HIDE)
    {
        return;
    }

    if (!m_main)
    {
        return;
    }

    auto pL = dynamic_cast<CDrawingLine*>(m_main);

    LINE2D* lMain = pL->GetLineInstance();

    LINE2D l(m_pt[0], m_pt[1]);

    MAT2D matDraw;
    if (pParentObject)
    {
        const MAT2D* pMat = pParentObject->GetDrawingMatrix();
        if (pMat)
        {
            m_matDraw = (*pMat);
        }
    }

    if (m_pMatOffset)
    {
        matDraw = matDraw * (*m_pMatOffset);
    }

    l.Matrix(matDraw);


    int iWidth = m_iLineWidth;
    if (eMouseOver == E_MO_SELECTABLE)
    {
        pView->SetLineAlternateMode(true);
        iWidth += 2;
    }
    else if (eMouseOver == E_MO_EMPHASIS)
    {
        crPen = DRAW_CONFIG->crEmphasis;
    }
	else if (eMouseOver == E_MO_CONNECTABLE)
	{
		crPen = DRAW_CONFIG->crConnectable;
	}

    bool bShowSub1 = false;


    double dDspScl = pView->GetViewScale();

   if (m_pMatOffset)
   {
        //マウスで移動、回転時表示用に使用する行列
        ///lMain->SetPt1(lMain->GetPt1() * (*m_pMatOffset));
        //lMain->SetPt2(lMain->GetPt2() * (*m_pMatOffset));

       /*
        lSub1->SetPt1(lSub1->Pt(0) * (*m_pMatOffset));
        lSub1->SetPt2(lSub1->Pt(1) * (*m_pMatOffset));

        lSub2->SetPt1(lSub2->Pt(0) * (*m_pMatOffset));
        lSub2->SetPt2(lSub2->Pt(1) * (*m_pMatOffset));
        */
    }

    POINT2D ptTextCnter;
    l.PramPos(&ptTextCnter, m_c.ptCenter, m_dTextOffset);

    double dPosAngle= CUtil::NormAngle(pL->GetLineInstance()->Angle());
    double dAngle = dPosAngle;

    //角度は0〜180に正規化する
    dAngle = NormalizeTextAngle(dAngle);

    m_psDimText->SetPos(ptTextCnter);
    m_psDimText->SetAngle(dAngle );
    COLORREF crText = crPen;
    if (m_eHit == CDrawingDim::H_TEXT)
    {
        crText =  DRAW_CONFIG->crImaginary;
    }
    m_psDimText->Draw(pView, crText,iId, iLayerId, eMouseOver);


    double dStart;
    double dEnd;
    POINT2D ptTextEnd;

    dStart = m_dTextOffset -  (m_psDimText->GetTextSize().dX / 2.0);
    dEnd   = m_dTextOffset +  (m_psDimText->GetTextSize().dX / 2.0);

    POINT2D ptStart = l.GetPt1();
    POINT2D ptEnd   = l.GetPt2();

    double lLen = l.Length() / 2.0;
    if (dStart < -lLen)
    {
        l.PramPos(&ptStart, m_c.ptCenter, dStart);
    }

    if (dEnd > lLen)
    {
        l.PramPos(&ptEnd, m_c.ptCenter, dEnd);
    }

    lMain->SetPt1(ptStart);
    lMain->SetPt2(ptEnd);

    pL->SetId(iId);
    pL->SetColor(crPen);
    pL->Draw( pView, NULL, eMouseOver);

   if (m_bShowAux[0] &&
        m_bSub)
    {

        CPartsDef* pCtrl = pView->GetPartsDef();
        CLayer* pLayer   = pCtrl->GetLayer(iLayerId);
        double dScl = pLayer->dScl;

        if (fabs(m_dLayerScl - dScl) > NEAR_ZERO)
        {
            m_dLayerScl = dScl;
            POINT2D ptOnline;
            if (_GetOffCirclePoint(&ptOnline))
            {
                _CreateAuxCircle( m_pCircleSub, ptOnline, m_c, m_dLayerScl);
            }
        }

        if (m_pCircleSub)
        {
            Circle(pView, iLayerId, m_pCircleSub.get(), NULL, PS_SOLID, iWidth, crPen, iId);
        }
    }

    /*
    if ((m_bShowAux[0]) && bShowSub1)
    {
        Line(pView, lSub1, PS_SOLID, iWidth, crPen, iId);
    }
    */

    //---------------------
    // 矢印描画
    //---------------------
    using namespace VIEW_COMMON;

    bool bHitArrow = false;
    if ((m_eArrowType == AT_OPEN) ||
        (m_eArrowType == AT_FILLED))
    {
        if (m_eHit == H_ARROW)
        {
            _DrawigArrow(pView,
                m_main,
                iWidth, 
                DRAW_CONFIG->crImaginary,
                iId, 
                iLayerId,
                m_eArrowType, 
                m_eArrowSide, 
                !m_bArrowFlip);
            bHitArrow = true;
        }
    }

    if (!bHitArrow)
    {
        _DrawigArrow(pView, 
                            m_main,
                            iWidth, 
                            crPen,
                            iId,
                            iLayerId,
                            m_eArrowType, 
                            m_eArrowSide, 
                            m_bArrowFlip);
    }
    //点線の隙間を塗りつぶすモード

   pView->SetLineAlternateMode(false);
   return;
}

//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void TEST_CDrawingDimD()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}
#endif //_DEBUG