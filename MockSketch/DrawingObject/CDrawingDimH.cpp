/**
 * @brief			CDrawingDimH実装ファイル
 * @file			CDrawingDimH.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CDrawingDim.h"
#include "./CDrawingDimH.h"
#include "./CDrawingPoint.h"
#include "./CDrawingLine.h"
#include "./CDrawingCircle.h"
#include "./CDrawingSpline.h"
#include "./CDrawingEllipse.h"
#include "./CDrawingCompositionLine.h"
#include "./CDrawingParts.h"
#include "./CDrawingText.h"
#include "./CDrawingNode.h"
#include "./CDimText.h"
#include "DefinitionObject/View/CDrawingView.h"
#include "DefinitionObject/CPartsDef.h"
#include "Utility/CUtility.h"
#include "Utility/ExtText/CExtText.h"
#include "System/CSystem.h"
#include "SubWindow/DimTextDlg.h"

#include <math.h>
#include <boost/serialization/export.hpp> 
BOOST_CLASS_EXPORT(CDrawingDimH);


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
CDrawingDimH::CDrawingDimH():
CDrawingDim( 0, DT_DIM_H),
m_dTextOffset(0.0)
{
    //TODO:実装
}

/**
 *  @brief  コンストラクター.
 *  @param  [in] nID 
 *  @retval なし 
 *  @note
 */
CDrawingDimH::CDrawingDimH(int nID):
CDrawingDim( nID, DT_DIM_H),
m_dTextOffset(0.0)
{
    //TODO:実装
}


/**
 *  @brief  コンストラクター.
 *  @param  [in] nID 
 *  @retval なし 
 *  @note
 */
CDrawingDimH::CDrawingDimH(int nID, DRAWING_TYPE eType):
CDrawingDim( nID, eType),
m_dTextOffset(0.0)
{
    //TODO:実装
}


/**
 * コピーコンストラクタ
 */
CDrawingDimH::CDrawingDimH(const CDrawingDimH& Obj):
CDrawingDim( Obj)
{
    m_ptLineCenter = Obj.m_ptLineCenter;
    m_dTextOffset  = Obj.m_dTextOffset;

    if (Obj.GetType() == DT_DIM_H)
    {
        _UpdateText();
    }
}

/**
 *  @brief  デストラクタ.
 *  @param  なし 
 *  @retval なし 
 *  @note
 */
CDrawingDimH::~CDrawingDimH()
{
}


//!< 相対移動
void CDrawingDimH::Move(const POINT2D& pt2D)
{
    m_ptLineCenter.Move(pt2D);
    CDrawingDim::Move(pt2D);

}

//!< 回転
void CDrawingDimH::Rotate(const POINT2D& pt2D, double dAngle)
{
    m_ptLineCenter.Rotate(pt2D, dAngle);
    CDrawingDim::Rotate(pt2D, dAngle);
}

//!< 鏡像
void CDrawingDimH::Mirror(const LINE2D& line)
{
    m_ptLineCenter.Mirror(line);
    CDrawingDim::Mirror(line);
}

//!< 倍率
void CDrawingDimH::Scl(const POINT2D& pt2D, double dXScl, double dYScl)
{
    m_ptLineCenter.Scl(pt2D, dXScl, dYScl);
    CDrawingDim::Scl(pt2D, dXScl, dYScl);
}

//!< 行列適用
void CDrawingDimH::Matrix(const MAT2D& mat2D)
{
    m_ptLineCenter.Matrix(mat2D);
    CDrawingDim::Matrix(mat2D);

}


double CDrawingDimH::GetVal() const
{
    double dVal = 0.0;
    dVal = fabs(m_pt[0].dX - m_pt[1].dX);
DB_PRINT(_T("CDrawingDimH::GetVal %f \n"), dVal );
    return dVal;
}

void CDrawingDimH::_GetLengthMain(LINE2D* pLineMain, POINT2D* pNorm) const
{
    POINT2D ptNorm;
    //矢印設定、マーカ表示に使用
    ptNorm.Set(1.0, 0.0);
    pLineMain->SetPt(m_pt[0].dX, m_ptLineCenter.dY,
                        m_pt[1].dX, m_ptLineCenter.dY);
    *pNorm = ptNorm;
}


//!< マーカ初期化
bool CDrawingDimH::InitNodeMarker(CNodeMarker* pMarker)
{
    LINE2D l;
    POINT2D ptNorm;
    _GetLengthMain(&l, &ptNorm);
    POINT2D ptText;

    l.PramPos(&ptText, m_ptLineCenter, m_dTextOffset);

    bool bRet;
    bRet = _InitNodeMarker(pMarker,
                            ptText,
                            true);

    pMarker->Create(m_psNodeHeight[0].get(), l.GetPt1());
    pMarker->Create(m_psNodeHeight[1].get(), l.GetPt2());
    return bRet;
}

//!< マーカ選択
void CDrawingDimH::SelectNodeMarker(CNodeMarker* pMarker,
                                    StdString strMarkerId)
{
    CDrawingDim::SelectNodeMarker(pMarker, 
                                  strMarkerId);
}

//!< マーカ移動
void CDrawingDimH::MoveNodeMarker(CNodeMarker* pMarker, 
                                    SNAP_DATA* pSnap,
                                    StdString strMarkerId,
                                    MOUSE_MOVE_POS posMouse)
{
    CDrawingDim::MoveNodeMarker(pMarker, 
                                pSnap,
                                strMarkerId,
                                posMouse);
    if (strMarkerId == MK_TEXT)
    {
        CPartsDef*   pDef  = pMarker->GetView()->GetPartsDef();

        SetTextOffset(pSnap->pt);

        LINE2D l;
        POINT2D ptNorm;
        POINT2D ptText;
        _GetLengthMain(&l, &ptNorm);
        l.PramPos(&ptText, m_ptLineCenter, m_dTextOffset);
        
        m_psTmpLine->SetPoint1(pSnap->pt);
        m_psTmpLine->SetPoint2(ptText);
        pDef->SetMouseEmphasis(m_psTmpLine);
    }
}

//!< マーカ開放
bool CDrawingDimH::ReleaseNodeMarker(CNodeMarker* pMarker, 
                                     StdString strMarkerId,
                                        MOUSE_MOVE_POS posMouse)
{
    return CDrawingDim::ReleaseNodeMarker(pMarker, strMarkerId, posMouse);
}

void CDrawingDimH::ChangeNode(StdString strMarkerId, E_NODE_CHANGE_TYPE eType)
{
}



bool CDrawingDimH::Create(const CDrawingObject* pObj1, const CDrawingObject* pObj2)
{
   //                L  H  V  A  D  R  C 
    //POINT-POINT     ○ ○ ○ × ○ ○ ×   PP
    //LINE            ○ ○ ○ × × × ○   L
 
    bool bH = true;
    if (GetType() == DT_DIM_V)
    {
        bH = false;;
    }

    DbgGuiLeak dgl;

    if (!pObj1)
    {
        return false;
    }

    m_iId[0] = pObj1->GetId();


    if (pObj2)
    {
        m_iId[1] = pObj2->GetId();
    }


    POINT2D pt1;
    POINT2D pt2;
    _SetDefault();


    if (pObj1->GetType() == DT_LINE)
    {
        auto pL = dynamic_cast<const CDrawingLine*>(pObj1);
        pt1 = pL->GetPoint1();
        pt2 = pL->GetPoint2();

    }
    else if (pObj1->GetType() == DT_CIRCLE)
    {
        auto pC = dynamic_cast<const CDrawingCircle*>(pObj1);

        if (bH)
        {
            pt1.dY = pC->GetCenter().dY; 
            pt1.dX = pC->GetCenter().dX - pC->GetRadius(); 
            pt2.dY = pC->GetCenter().dY; 
            pt2.dX = pC->GetCenter().dX + pC->GetRadius(); 
        }
        else
        {
            pt1.dY = pC->GetCenter().dY - pC->GetRadius(); 
            pt1.dX = pC->GetCenter().dX; 
            pt2.dY = pC->GetCenter().dY + pC->GetRadius(); 
            pt2.dX = pC->GetCenter().dX; 
        }
    }
    else
    {
        if (!_GetPoint(&pt1, pObj1))
        {
            return false;
        }

        if (!_GetPoint(&pt2, pObj2))
        {
            return false;
        }
    }

    m_pt[0] = pt1;
    m_pt[1] = pt2;

    m_ptLineCenter = (pt1 + pt2) / 2.0;

    if (m_main)
    {
        if (m_main->GetType() != DT_LINE)
        {
            delete m_main;
            m_main = NULL;
        }
    }

    if (!m_main)
    {
        m_main = new CDrawingLine;
    }


    if (m_lineSub.size() != 2)
    {
        m_lineSub.clear();
        m_lineSub.push_back(LINE2D());
        m_lineSub.push_back(LINE2D());
    }

    _UpdateText();
    return true;
}


void CDrawingDimH::_SetDefault()
{
    CDrawingDim::_SetDefault();

    m_ptLineCenter.Set(0.0, 0.0);
    m_dTextOffset  = 0.0;

    m_bShowAux[0] = true;
    m_bShowAux[1] = true;
    m_psDimText->SetPrecision(DRAW_CONFIG->iDimLengthPrecision);
}

bool CDrawingDimH::SetTextCenter(const POINT2D& pt)
{
    m_ptLineCenter.dX = (m_pt[0].dX + m_pt[1].dX) / 2.0;
    m_ptLineCenter.dY = pt.dY;
    return true;
}

bool CDrawingDimH::SetTextOffset(const POINT2D& pt)
{

    return CDrawingDim::_SetTextPosLHV(&m_dTextOffset, pt, m_ptLineCenter);
}

POINT2D  CDrawingDimH::GetTextCenter() const
{
    POINT2D ptText;
    //TODP:実装
    LINE2D l;
    POINT2D ptNorm;
    _GetLengthMain(&l, &ptNorm);

    const MAT2D* pMat =  GetParentMatrix();
    if (pMat != NULL)
    {
        l.Matrix(*pMat);    
    }

    l.PramPos(&ptText, m_ptLineCenter, m_dTextOffset);
    return ptText;
}

void CDrawingDimH::Draw(CDrawingView* pView, 
                        const std::shared_ptr<CDrawingObject> pParentObject,
                        E_MOUSE_OVER_TYPE eMouseOver,
                        int iLayerId)
{
    COLORREF crPen;
    int      iId;

    bool bIgnoreSelect = false;
    if (eMouseOver == E_MO_IGNORE_SELECT)
    {
        bIgnoreSelect = true;
    }
        
    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }


    crPen = GetDrawColor(pView, iLayerId, pParentObject.get(), &iId, bIgnoreSelect);
    _Draw(pView, crPen, iId, iLayerId, pParentObject.get(), eMouseOver);
}

void CDrawingDimH::DeleteView(CDrawingView* pView, 
                              const std::shared_ptr<CDrawingObject> pParentObject,
                              int iLayerId)
{
    COLORREF crObj;
    crObj = DRAW_CONFIG->GetBackColor();

    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }

    _Draw(pView, crObj, -1, iLayerId, pParentObject.get(), E_MO_NO);
}

void CDrawingDimH::_Draw(CDrawingView* pView, 
                        COLORREF crPen,
                        int      iId,
                        int iLayerId,
                        const CDrawingObject* pParentObject,
                        E_MOUSE_OVER_TYPE eMouseOver)
{
    if (m_eShow == SM_HIDE)
    {
        return;
    }

    if (!m_main)
    {
        return;
    }

    LINE2D l(m_pt[0], m_pt[1]);


    POINT2D ptTmpNorm;
    LINE2D l2;

    _GetLengthMain(&l2, &ptTmpNorm);

   
    auto pL = dynamic_cast<CDrawingLine*>(m_main);
    LINE2D* lMain = pL->GetLineInstance();
    LINE2D* lSub1 = &m_lineSub[0];
    LINE2D* lSub2 = &m_lineSub[1];

    MAT2D matDraw;
    if (pParentObject )
    {
        const MAT2D* pMat = pParentObject->GetDrawingMatrix();
        if (pMat)
        {
            matDraw = *pMat;
            l.Matrix(matDraw);
        }
    }


    POINT2D pt1 = l.Pt(0);
    POINT2D pt2 = l.Pt(1);

    if (m_pMatOffset)
    {
        LINE2D tmpline(l2);
        if (m_bSelPoint[0])
        {
            pt1 = pt1 *   (*m_pMatOffset);
            tmpline.SetPt1(l2.Pt(0) * (*m_pMatOffset));
            l.SetPt1(pt1);
        }

        if (m_bSelPoint[1])
        {
            pt2 = pt2 *   (*m_pMatOffset);
            tmpline.SetPt2(l2.Pt(1) * (*m_pMatOffset));
            l.SetPt2(pt2);
        }
        l2 = tmpline;
    }


    POINT2D prC = (pt1 + pt2) / 2.0;
    double dDimLineCenterDistance;
    dDimLineCenterDistance = m_ptLineCenter.Distance2(prC);



    //l2.PramPos(&ptTextCenter, ptLineCenter, m_dTextOffset);

    int iWidth = m_iLineWidth;
    if (eMouseOver == E_MO_SELECTABLE)
    {
        pView->SetLineAlternateMode(true);
        iWidth += 2;
    }
    else if (eMouseOver == E_MO_EMPHASIS)
    {
        crPen = DRAW_CONFIG->crEmphasis;
    }
	else if (eMouseOver == E_MO_CONNECTABLE)
	{
		crPen = DRAW_CONFIG->crConnectable;
	}

    double dTop = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->dDimTop);
    double dGap = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->dDimGap);


    POINT2D ptNorm1 = l2.Pt(0) - l.Pt(0);
    POINT2D ptNorm2 = l2.Pt(1) - l.Pt(1);

    double dLen1 = ptNorm1.Length();
    double dLen2 = ptNorm2.Length();

    ptNorm1.Normalize();
    ptNorm2.Normalize();

    POINT2D ptTop1 = ptNorm1 * dTop;
    POINT2D ptTop2 = ptNorm2 * dTop;

    POINT2D ptGap1;
    POINT2D ptGap2;

    if (dLen1 > dGap)
    {
        ptGap1 = ptNorm1 * dGap;
        lSub1->SetPt1(l.GetPt1() + ptGap1);
    }
    else
    {
        lSub1->SetPt1(l2.GetPt1());
    }

    if (dLen2 > dGap)
    {
        ptGap2 = ptNorm2 * dGap;
        lSub2->SetPt1(l.GetPt2() + ptGap2);
    }
    else
    {
        lSub2->SetPt1(l2.GetPt2());
    }

    lSub1->SetPt2(l2.GetPt1() + ptTop1);
    lSub2->SetPt2(l2.GetPt2() + ptTop2);


    //テキスト位置
    POINT2D ptVec = l2.GetPt2() - l2.GetPt1();

    double dDspScl = pView->GetViewScale();

    POINT2D ptLineCenter;
    POINT2D ptTextCenter;
    ptLineCenter = (l2.GetPt1() + l2.GetPt2() ) / 2.0;

    l2.PramPos(&ptTextCenter, ptLineCenter, m_dTextOffset);

    double lLen = l2.Length() / 2.0;
    double dStart;
    double dEnd;
    POINT2D ptTextEnd;
    dStart = m_dTextOffset -  (m_psDimText->GetTextSize().dX / 2.0);
    dEnd   = m_dTextOffset +  (m_psDimText->GetTextSize().dX / 2.0);

    POINT2D ptStart = l2.GetPt1();
    POINT2D ptEnd   = l2.GetPt2();
    if (dStart < -lLen)
    {
        l2.PramPos(&ptStart, ptLineCenter, dStart);
    }

    if (dEnd > lLen)
    {
        l2.PramPos(&ptEnd, ptLineCenter, dEnd);
    }


    lMain->SetPt1(ptStart);
    lMain->SetPt2(ptEnd);

    double dAngle;
    bool bH = true;
    if (GetType() == DT_DIM_V)
    {
        bH = false;;
    }
    if (bH)
    {
        dAngle = 0.0;
    }
    else
    {
        dAngle = 90.0;
    }




    m_psDimText->SetVal(l2.Length(), false);
    m_psDimText->SetPos(ptTextCenter);
    m_psDimText->SetAngle(dAngle);

    COLORREF crText = crPen;
    if (m_eHit == CDrawingDim::H_TEXT)
    {
        crText =  DRAW_CONFIG->crImaginary;
    }
    m_psDimText->Draw(pView, crText,iId, iLayerId, eMouseOver);


   Line(pView, iLayerId, lMain, PS_SOLID, iWidth, crPen, iId);
   
   if (m_bShowAux[0])
   {
       Line(pView, iLayerId, lSub1, PS_SOLID, iWidth, crPen, iId);
   }

   if (m_bShowAux[1])
   {
       Line(pView, iLayerId, lSub2, PS_SOLID, iWidth, crPen, iId);
   }

    //---------------------
    // 矢印描画
    //---------------------
    using namespace VIEW_COMMON;

    bool bHitArrow = false;
    if ((m_eArrowType == AT_OPEN) ||
        (m_eArrowType == AT_FILLED))
    {
        if (m_eHit == H_ARROW)
        {
            _DrawigArrow(pView, 
                m_main,
                iWidth, 
                DRAW_CONFIG->crImaginary,
                iId, 
                iLayerId,
                m_eArrowType, 
                m_eArrowSide, 
                !m_bArrowFlip);
            bHitArrow = true;
        }
    }

    if (!bHitArrow)
    {
        _DrawigArrow(pView,
                            m_main,
                            iWidth, 
                            crPen,
                            iId,
                            iLayerId,
                            m_eArrowType, 
                            m_eArrowSide, 
                            m_bArrowFlip);
    }
    //---------------------
    pView->SetLineAlternateMode(false);

    return;
}

/////////////////////////////////////
/////////////////////////////////////
/////////////////////////////////////
/////////////////////////////////////

//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void TEST_CDrawingDimH()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}
#endif //_DEBUG