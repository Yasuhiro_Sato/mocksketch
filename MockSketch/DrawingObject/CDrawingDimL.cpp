/**
 * @brief			CDrawingDimL実装ファイル
 * @file			CDrawingDimL.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CDrawingDim.h"
#include "./CDrawingDimL.h"
#include "./CDrawingPoint.h"
#include "./CDrawingLine.h"
#include "./CDrawingCircle.h"
#include "./CDrawingSpline.h"
#include "./CDrawingEllipse.h"
#include "./CDrawingCompositionLine.h"
#include "./CDrawingParts.h"
#include "./CDrawingText.h"
#include "./CDrawingNode.h"
#include "./CDimText.h"
#include "DefinitionObject/View/CDrawingView.h"
#include "DefinitionObject/CPartsDef.h"
#include "Utility/CUtility.h"
#include "Utility/ExtText/CExtText.h"
#include "System/CSystem.h"
#include "SubWindow/DimTextDlg.h"

#include <math.h>
#include <boost/serialization/export.hpp> 
BOOST_CLASS_EXPORT(CDrawingDimL);


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
CDrawingDimL::CDrawingDimL():
CDrawingDim( 0, DT_DIM_L),
m_dTextOffset(0.0)
{
    //TODO:実装
}

/**
 *  @brief  コンストラクター.
 *  @param  [in] nID 
 *  @retval なし 
 *  @note
 */
CDrawingDimL::CDrawingDimL(int nID):
CDrawingDim( nID, DT_DIM_L),
m_dTextOffset(0.0)
{
    //TODO:実装
}


/**
 * コピーコンストラクタ
 */
CDrawingDimL::CDrawingDimL(const CDrawingDimL& Obj):
CDrawingDim( Obj)
{
    m_ptLineCenter = Obj.m_ptLineCenter;
    m_dTextOffset  = Obj.m_dTextOffset;
    _UpdateText();
}


/**
 *  @brief  デストラクタ.
 *  @param  なし 
 *  @retval なし 
 *  @note
 */
CDrawingDimL::~CDrawingDimL()
{
}

//!< 相対移動
void CDrawingDimL::Move(const POINT2D& pt2D)
{
    m_ptLineCenter.Move(pt2D);
    CDrawingDim::Move(pt2D);

}

//!< 回転
void CDrawingDimL::Rotate(const POINT2D& pt2D, double dAngle)
{
    m_ptLineCenter.Rotate(pt2D, dAngle);
    CDrawingDim::Rotate(pt2D, dAngle);
}

//!< 鏡像
void CDrawingDimL::Mirror(const LINE2D& line)
{
    m_ptLineCenter.Mirror(line);
    CDrawingDim::Mirror(line);
}

//!< 倍率
void CDrawingDimL::Scl(const POINT2D& pt2D, double dXScl, double dYScl)
{
    m_ptLineCenter.Scl(pt2D, dXScl, dYScl);
    CDrawingDim::Scl(pt2D, dXScl, dYScl);
}

//!< 行列適用
void CDrawingDimL::Matrix(const MAT2D& mat2D)
{
    m_ptLineCenter.Matrix(mat2D);
    CDrawingDim::Matrix(mat2D);

}

double CDrawingDimL::GetVal() const
{
    double dVal = 0.0;
    dVal = m_pt[0].Distance(m_pt[1]);
    return dVal;
}

void CDrawingDimL::_GetLengthMain(LINE2D* pLineMane, POINT2D* pNorm) const
{
    POINT2D ptCenter(m_ptLineCenter);


    //矢印設定、マーカ表示に使用
    POINT2D ptNorm;
    LINE2D l(m_pt[0], m_pt[1]);
    ptNorm = ptCenter - l.NearPoint( ptCenter, false);
    pLineMane->SetPt(m_pt[0] + ptNorm, m_pt[1] + ptNorm);
    *pNorm = ptNorm;
}

bool CDrawingDimL::SetTextCenter(const POINT2D& pt)
{
    //方向を決める
    if (!m_main)
    {
        return false;
    }
    LINE2D l(m_pt[0], m_pt[1]);

    POINT2D ptNear;
    ptNear = l.NearPoint( pt, false);

    POINT2D ptNorm = (pt - ptNear);

    POINT2D pt1 = m_pt[0] + ptNorm;
    POINT2D pt2 = m_pt[1] + ptNorm;

    m_ptLineCenter = (pt1 + pt2 ) / 2.0;
    return true;
}


bool CDrawingDimL::SetTextOffset(const POINT2D& pt)
{
    return CDrawingDim::_SetTextPosLHV(&m_dTextOffset, pt, m_ptLineCenter);
}


POINT2D  CDrawingDimL::GetTextCenter() const
{
    POINT2D ptText;
    //TODP:実装
    LINE2D l;
    POINT2D ptNorm;
    _GetLengthMain(&l, &ptNorm);

    const MAT2D* pMat =  GetParentMatrix();
    if (pMat != NULL)
    {
        l.Matrix(*pMat);
    }
    
    l.PramPos(&ptText, m_ptLineCenter, m_dTextOffset);
    return ptText;
}

//!< マーカ初期化
bool CDrawingDimL::InitNodeMarker(CNodeMarker* pMarker)
{
    LINE2D l;
    POINT2D ptNorm;
    _GetLengthMain(&l, &ptNorm);
    POINT2D ptText;

    l.PramPos(&ptText, m_ptLineCenter, m_dTextOffset);

    bool bRet;
    bRet = _InitNodeMarker(pMarker,
                            ptText,
                            true);

    pMarker->Create(m_psNodeHeight[0].get(), l.GetPt1());
    pMarker->Create(m_psNodeHeight[1].get(), l.GetPt2());
    return bRet;
}

//!< マーカ選択
void CDrawingDimL::SelectNodeMarker(CNodeMarker* pMarker, 
                                    StdString strMarkerId)
{
    CDrawingDim::SelectNodeMarker(pMarker, 
                                  strMarkerId);
}

//!< マーカ移動
void CDrawingDimL::MoveNodeMarker(CNodeMarker* pMarker, 
                                    SNAP_DATA* pSnap,
                                    StdString strMarkerId,
                                    MOUSE_MOVE_POS posMouse)
{

    CDrawingDim::MoveNodeMarker(pMarker, 
                                pSnap,   
                                strMarkerId,
                                posMouse);


    if (strMarkerId == MK_TEXT)
    {
        CPartsDef*   pDef  = pMarker->GetView()->GetPartsDef();

        SetTextOffset(pSnap->pt);

        LINE2D l;
        POINT2D ptNorm;
        POINT2D ptText;
        _GetLengthMain(&l, &ptNorm);
        l.PramPos(&ptText, m_ptLineCenter, m_dTextOffset);
        
        m_psTmpLine->SetPoint1(pSnap->pt);
        m_psTmpLine->SetPoint2(ptText);
        pDef->SetMouseEmphasis(m_psTmpLine);
    }
}

//!< マーカ開放
bool CDrawingDimL::ReleaseNodeMarker(CNodeMarker* pMarker, StdString strMarkerId, MOUSE_MOVE_POS posMouse)
{
    return CDrawingDim::ReleaseNodeMarker(pMarker, strMarkerId, posMouse);
}

void CDrawingDimL::ChangeNode(StdString strMarkerId, E_NODE_CHANGE_TYPE eType)
{
}

bool CDrawingDimL::Create(const CDrawingObject* pObj1, const CDrawingObject* pObj2)
{
    //POINT-POINT     ○ ○ ○ × × × ×   PP
    //POINT-LINE      ○ × × × × × ×   PL
    //POINT-CICLE     ○ × × × × × ×   PC
    //POINT-SPLINE    ○ × × × × × ×   PS
    //LINE            ○ ○ ○ × × × ○   L
    //CIRCLE          ○ × × × ○ ○ ×   C
    //LINE-LINE       × × × ○ × × ×   LL
    DbgGuiLeak dgl;
    if (!pObj1)
    { 
        return false;
    }

    if (pObj1)
    {
        m_iId[0] = pObj1->GetId();
    }

    if (pObj2)
    {
        m_iId[1] = pObj2->GetId();
    }


    POINT2D pt1;
    POINT2D pt2;

    bool bPt1 = false;
    bool bPt2 = false;

    _SetDefault();

    if (!_GetPoint(&pt1, pObj1))
    {
        if (_GetPoint(&pt2, pObj2))
        {
            std::swap(pObj1, pObj2);
            pt1 = pt2;
            bPt1 = true;
        }
    }
    else
    {
        bPt1 = true;
    }

    bool bRet = false;
    if (bPt1)
    {
        if (bPt2)
        {
            //POINT-POINT
            //bRet = _CreateLineDim(pt1, pt2);
            ;
        }
        else
        {
            if (!pObj2)
            {
                return false;
            }

            //POINT-LINE     
            //POINT-CICLE    
            //POINT-SPLINE   
            if (!pObj2->NearPoint(&pt2, pt1, false))
            {
                return false;
            }

        }
    }
    else
    {
        DRAWING_TYPE t1 = pObj1->GetType();

        if (t1 == DT_LINE)
        {
            //LINE            ○ ○ ○ × × × ○   L
            auto l = dynamic_cast<const CDrawingLine*>(pObj1);
            pt1 = l->GetPoint1();
            pt2 = l->GetPoint2();
        }
        else if (t1 == DT_CIRCLE)
        {
            //TODO:現状は円弧長は対応しない
            //CIRCLE          ○ × × × ○ ○ ×   C
            //auto c = dynamic_cast<const CDrawingCircle*>(pObj1);
            //return _CreateArcDim(*(c->: ()));
            return false;
        }
        else
        {
            return false;
        }
    }

    m_ptLineCenter = (pt1 + pt2) / 2.0;
    
    bRet = _CreateLineDim(pt1, pt2 );
    
    _UpdateText();

    return bRet;
}

void CDrawingDimL::_SetDefault()
{
    CDrawingDim::_SetDefault();
    m_bShowAux[0] = true;
    m_bShowAux[1] = true;
    m_psDimText->SetPrecision(DRAW_CONFIG->iDimLengthPrecision);
}

void CDrawingDimL::Draw(CDrawingView* pView, 
                        const std::shared_ptr<CDrawingObject> pParentObject,
                        E_MOUSE_OVER_TYPE eMouseOver,
                        int iLayerId)
{
    COLORREF crPen;
    int      iId;

    bool bIgnoreSelect = false;
    if (eMouseOver == E_MO_IGNORE_SELECT)
    {
        bIgnoreSelect = true;
    }

    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }

    crPen = GetDrawColor(pView, iLayerId, pParentObject.get(), &iId, bIgnoreSelect);
    _Draw(pView, crPen, iId, iLayerId, pParentObject.get(), eMouseOver);
}

void CDrawingDimL::DeleteView(CDrawingView* pView, 
                              const std::shared_ptr<CDrawingObject> pParentObject,
                              int iLayerId)
{
    COLORREF crObj;
    crObj = DRAW_CONFIG->GetBackColor();

    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }

    _Draw(pView, crObj, -1, iLayerId, pParentObject.get(), E_MO_NO);
}



void CDrawingDimL::_Draw(CDrawingView* pView, 
                        COLORREF crPen,
                        int      iId,
                        int iLayerId,
                        const CDrawingObject* pParentObject,
                        E_MOUSE_OVER_TYPE eMouseOver)
{

    if (m_eShow == SM_HIDE)
    {
        return;
    }

    if (!m_main)
    {
        return;
    }


    POINT2D ptTmpNorm;
    LINE2D l2;

    _GetLengthMain(&l2, &ptTmpNorm);



    CDrawingLine* pMainLine = dynamic_cast<CDrawingLine*>(m_main);

    LINE2D* lMain = pMainLine->GetLineInstance();
    LINE2D* lSub1 = &m_lineSub[0];
    LINE2D* lSub2 = &m_lineSub[1];

    LINE2D l(m_pt[0], m_pt[1]);


    MAT2D matDraw;
    if (pParentObject)
    {
        const MAT2D* pMat = pParentObject->GetDrawingMatrix();
        if (pMat)
        {
            l.Matrix(*pMat);
            l2.Matrix(*pMat);
            lMain->Matrix(*pMat);
        }
    }


    POINT2D ptLine = m_ptLineCenter;
    POINT2D pt1 = l.Pt(0);
    POINT2D pt2 = l.Pt(1);
    if (m_pMatOffset)
    {
        matDraw = matDraw * (*m_pMatOffset);

        LINE2D tmpline(l2);
        if (m_bSelPoint[0])
        {
            pt1 = pt1 *   (*m_pMatOffset);
            tmpline.SetPt1(l2.Pt(0) * (*m_pMatOffset));
            l.SetPt1(pt1);
        }

        if (m_bSelPoint[1])
        {
            pt2 = pt2 *   (*m_pMatOffset);
            tmpline.SetPt2(l2.Pt(1) * (*m_pMatOffset));
            l.SetPt2(pt2);
        }
        
        l2 = tmpline;
        ptLine = ptLine * (*m_pMatOffset);

    }

    int iWidth = m_iLineWidth;
    if (eMouseOver == E_MO_SELECTABLE)
    {
        pView->SetLineAlternateMode(true);
        iWidth += 2;
    }
    else if (eMouseOver == E_MO_EMPHASIS)
    {
        crPen = DRAW_CONFIG->crEmphasis;
    }
	else if (eMouseOver == E_MO_CONNECTABLE)
	{
		crPen = DRAW_CONFIG->crConnectable;
	}

    double dTop = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->dDimTop);
    double dGap = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->dDimGap);


    //-----------------------------------
    POINT2D ptNorm;
    ptNorm = m_ptLineCenter - l.NearPoint( m_ptLineCenter, false);
    //-----------------------------------

    ptNorm.Normalize();
    POINT2D ptTop = ptNorm * dTop;

    POINT2D ptGap;
    if (lSub1->Length() > dGap)
    {
        ptGap = ptNorm * dGap;
    }

    lSub1->SetPt1(l.GetPt1() + ptGap);
    lSub2->SetPt1(l.GetPt2() + ptGap);

    lSub1->SetPt2(l2.GetPt1() + ptTop);
    lSub2->SetPt2(l2.GetPt2() + ptTop);

    //--------------
    //テキスト位置
    //--------------
    POINT2D ptVec = l2.GetPt2() - l2.GetPt1();

    double dAngle;
    if (ptNorm.Length() < NEAR_ZERO)
    {
        dAngle= atan2(ptVec.dY, ptVec.dX) * RAD2DEG;
        if (dAngle < 0)
        {
            dAngle += 360.0;
        }

        if (dAngle >= 180.0)
        {
            dAngle -= 180.0;
        }
        dAngle += 90.0;
        ptNorm.dX = cos(dAngle * DEG2RAD);
        ptNorm.dY = sin(dAngle * DEG2RAD);
    }
    else
    {
        dAngle= atan2(ptNorm.dY, ptNorm.dX) * RAD2DEG;
    }

    dAngle = dAngle - 90.0;
    if (dAngle < 0)
    {
        dAngle += 360.0;
    }

    dAngle = NormalizeTextAngle(dAngle);

    POINT2D ptTextCenter;

    ptTextCenter = (l2.GetPt1() + l2.GetPt2() ) / 2.0;
    l2.PramPos(&ptTextCenter, ptLine, m_dTextOffset);


//ForDebug
    {
    DB_PRINT(_T("dAngle(%f)\n"), dAngle);
    DB_PRINT(_T("l2(%f,%f)- (%f,%f)\n"),  l2.GetPt1().dX, l2.GetPt1().dY,
                                             l2.GetPt2().dX, l2.GetPt2().dY);
    DB_PRINT(_T("\n"));
    }
//ForDebug

    m_psDimText->SetVal(l2.Length(), false);
    m_psDimText->SetPos(ptTextCenter);
    m_psDimText->SetAngle(dAngle);

    COLORREF crText = crPen;
    if (m_eHit == CDrawingDim::H_TEXT)
    {
        crText =  DRAW_CONFIG->crImaginary;
    }
    m_psDimText->Draw(pView, crText,iId, iLayerId, eMouseOver);
    double dDspScl = pView->GetViewScale();

    //-----------------------------------
    //文字位置に合わせて寸法線を拡張する
    //-----------------------------------
    double lLen = l2.Length() / 2.0;
    double dStart;
    double dEnd;

    dStart = m_dTextOffset -  (m_psDimText->GetTextSize().dX / 2.0);
    dEnd   = m_dTextOffset +  (m_psDimText->GetTextSize().dX / 2.0);

    POINT2D ptStart = l2.GetPt1();
    POINT2D ptEnd   = l2.GetPt2();
    if (dStart < -lLen)
    {
        l2.PramPos(&ptStart, ptLine, dStart);
    }

    if (dEnd > lLen)
    {
        l2.PramPos(&ptEnd, ptLine, dEnd);
    }

    lMain->SetPt1(ptStart);
    lMain->SetPt2(ptEnd);


    //-----------------------------------


    Line(pView, iLayerId, lMain, PS_SOLID, iWidth, crPen, iId);
   
    //---------------------
    // 補助線
    //---------------------
    if (m_eHit == H_SUB1)
    {
        Line(pView, iLayerId, lSub1, PS_DASH, iWidth, DRAW_CONFIG->crImaginary, iId);
    }
    else
    {
        if (m_bShowAux[0])
        {
            Line(pView, iLayerId, lSub1, PS_SOLID, iWidth, crPen, iId);
        }
    }

    if (m_eHit == H_SUB2)
    {
        Line(pView, iLayerId, lSub2, PS_DASH, iWidth, DRAW_CONFIG->crImaginary, iId);
    }
    else
    {
        if (m_bShowAux[1])
        {
            Line(pView, iLayerId, lSub2, PS_SOLID, iWidth, crPen, iId);
        }
    }



    //---------------------
    // 矢印描画
    //---------------------
    using namespace VIEW_COMMON;

    bool bHitArrow = false;
    if ((m_eArrowType == AT_OPEN) ||
        (m_eArrowType == AT_FILLED))
    {
        if (m_eHit == H_ARROW)
        {
            _DrawigArrow(pView, 
                pMainLine,
                iWidth, 
                DRAW_CONFIG->crImaginary,
                iId, 
                iLayerId,
                m_eArrowType, 
                m_eArrowSide, 
                !m_bArrowFlip);
            bHitArrow = true;
        }
    }

    if (!bHitArrow)
    {
        _DrawigArrow(pView, 
                            pMainLine,
                            iWidth, 
                            crPen,
                            iId,
                            iLayerId,
                            m_eArrowType, 
                            m_eArrowSide, 
                            m_bArrowFlip);
    }
    //---------------------

    pView->SetLineAlternateMode(false);
    return ;
}

//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void TEST_CDrawingDimL()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}
#endif //_DEBUG