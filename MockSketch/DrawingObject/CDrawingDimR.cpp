/**
 * @brief			CDrawingDimR実装ファイル
 * @file			CDrawingDimR.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CDrawingDim.h"
#include "./CDrawingDimR.h"
#include "./CDrawingPoint.h"
#include "./CDrawingLine.h"
#include "./CDrawingCircle.h"
#include "./CDrawingSpline.h"
#include "./CDrawingEllipse.h"
#include "./CDrawingCompositionLine.h"
#include "./CDrawingParts.h"
#include "./CDrawingText.h"
#include "./CDrawingNode.h"
#include "./CDimText.h"
#include "DefinitionObject/View/CDrawingView.h"
#include "DefinitionObject/CPartsDef.h"
#include "Utility/CUtility.h"
#include "Utility/ExtText/CExtText.h"
#include "System/CSystem.h"
#include "SubWindow/DimTextDlg.h"

#include <math.h>
#include <boost/serialization/export.hpp> 
BOOST_CLASS_EXPORT(CDrawingDimR);


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
CDrawingDimR::CDrawingDimR():
CDrawingDim( 0, DT_DIM_L),
m_dTextOffset(0.0),
m_bSub(false),
m_dLayerScl(1.0)
{
    //TODO:実装
}

/**
 *  @brief  コンストラクター.
 *  @param  [in] nID 
 *  @retval なし 
 *  @note
 */
CDrawingDimR::CDrawingDimR(int nID):
CDrawingDim( nID, DT_DIM_L),
m_dTextOffset(0.0),
m_bSub(false),
m_dLayerScl(1.0)
{
    //TODO:実装
}

/**
 * コピーコンストラクタ
 */
CDrawingDimR::CDrawingDimR(const CDrawingDimR& Obj):
CDrawingDim( Obj)
{
    m_c = Obj.m_c;
    m_dTextOffset = Obj.m_dTextOffset;
    m_bSub = Obj.m_bSub;

    if (Obj.m_pCircleSub)
    {
        m_pCircleSub = std::make_unique<CIRCLE2D>(*(Obj.m_pCircleSub.get()));
    }
    _UpdateText();
}

/**
 *  @brief  デストラクタ.
 *  @param  なし 
 *  @retval なし 
 *  @note
 */
CDrawingDimR::~CDrawingDimR()
{
}



/**
 *  @brief  回転.
 *  @param  [in]    pPt2D    回転中心(絶対座標)
 *  @param  [in]    dAngle   (絶対)回転角(rad)
 *  @retval     なし
 *  @note
 */
void CDrawingDimR::Rotate(const POINT2D& pt2D, double dAngle)
{
    if (m_main)
    {
        m_main->Rotate(pt2D, dAngle);
    }

    m_pt[0].Rotate(pt2D,dAngle);
    m_pt[1].Rotate(pt2D,dAngle);
    m_c.Rotate(pt2D,dAngle);
    if (m_pCircleSub)
    {
        m_pCircleSub->Rotate(pt2D,dAngle);
    }
    _UpdateText();
}

/**
 *  @brief  相対移動.
 *  @param  [in]    pPt2D   移動量
 *  @retval         なし
 *  @note
 */
void CDrawingDimR::Move(const POINT2D& pt2D) 
{
    AddChgCnt();

    if (m_main)
    {
        m_main->Move(pt2D);
    }
    m_pt[0].Move(pt2D);
    m_pt[1].Move(pt2D);

    m_c.Move(pt2D);
    if (m_pCircleSub)
    {
        m_pCircleSub->Move(pt2D);
    }

    _UpdateText();
}

/**
 *  @brief  鏡像.
 *  @param  [in]    line   対称軸
 *  @retval         なし
 *  @note
 */
void CDrawingDimR::Mirror(const LINE2D& line)
{
    if (m_main)
    {
        m_main->Mirror(line);
    }
    m_pt[0].Mirror(line);
    m_pt[1].Mirror(line);
    m_c.Mirror(line);
    if (m_pCircleSub)
    {
        m_pCircleSub->Mirror(line);;
    }

    _UpdateText();
    AddChgCnt();
}

/**
 *  @brief  行列.
 *  @param  [in]    mat2D   適用する行列
 *  @retval         なし
 *  @note
 */
void CDrawingDimR::Matrix(const MAT2D& mat2D)
{
    if (m_main)
    {
        m_main->Matrix(mat2D);
    }
    m_pt[0].Matrix(mat2D);
    m_pt[1].Matrix(mat2D);

    ELLIPSE2D* pEllipse = NULL;
    
    //楕円化する場合の対処は今のところ行わない
    m_c.Matrix(mat2D, &pEllipse);
    if (m_pCircleSub)
    {
        ELLIPSE2D* pEllipse2 = NULL;
        m_pCircleSub->Matrix(mat2D, &pEllipse2);
        if(pEllipse2){delete  pEllipse2;}
    }

    if (pEllipse){delete pEllipse;}

    _UpdateText();
    AddChgCnt();
}

/**
 *  @brief  倍率.
 *  @param  [in]    pPt2D   拡大中心
 *  @param  [in]    dSclX   X倍率
 *  @param  [in]    dSclY   Y倍率
 *  @retval         なし
 *  @note
 */
void CDrawingDimR::Scl(const POINT2D& pt2D, double dXScl, double dYScl)
{
    if (m_main)
    {
        m_main->Scl(pt2D, dXScl, dYScl);
    }

    m_pt[0].Scl(pt2D, dXScl, dYScl);
    m_pt[1].Scl(pt2D, dXScl, dYScl);

    ELLIPSE2D* pEllipse = NULL;
    m_c.Scl(pt2D, dXScl, dYScl, &pEllipse);
    if (m_pCircleSub)
    {
        ELLIPSE2D* pEllipse2 = NULL;
        m_pCircleSub->Scl(pt2D, dXScl, dYScl, &pEllipse2);
        if(pEllipse2){delete  pEllipse2;}
    }
    if (pEllipse){delete pEllipse;}

    _UpdateText();
    AddChgCnt();
}

double CDrawingDimR::GetVal() const
{
    double dVal = 0.0;
    dVal = m_c.dRad;
    return dVal;
}

void CDrawingDimR::_GetLengthMain(LINE2D* pLineMane, POINT2D* pNorm) const
{
    //矢印設定、マーカ表示に使用
    //ptNorm 未使用
    //POINT2D ptNorm;
    auto pL = dynamic_cast<CDrawingLine*>(m_main);
    if (pL)
    {
        //lMain->SetPt1(ptOnline);
        //lMain->SetPt2(ptTextEnd);
        pLineMane->SetPt(pL->GetPoint1(), pL->GetPoint2());
    }
}

bool CDrawingDimR::SetAngleHeight(const POINT2D& pt)
{
    // m_pt[0] <- マウス位置から一番近い円弧上の点

    bool bRet;
    bRet = SetArrowPosition(pt);

    if (bRet)
    {
        bRet = SetTextCenter(pt);
    }
    return bRet;
}

bool CDrawingDimR::SetArrowPosition(const POINT2D& pt)
{

    double dR = m_c.dRad;
    LINE2D lMouse;

    if (m_c.ptCenter.Distance(pt) < NEAR_ZERO)
    {
        return false;
    }

    POINT2D vec = pt - m_c.ptCenter;


    vec.Normalize();

    m_pt[0].dX = vec.dX * dR + m_c.ptCenter.dX;
    m_pt[0].dY = vec.dY * dR + m_c.ptCenter.dY;

    return true;
}

double CDrawingDimR::_GetTextStartMargin() const
{
    //テキスト端のマージンを矢印の長さとする
    double dMargin;
    dMargin = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->dArrowSize);  
    dMargin  += (m_psDimText->GetTextSize().dX /2.0);
    return dMargin;
}

bool CDrawingDimR::SetTextCenter(const POINT2D& pt)
{
    double dR = m_c.dRad;

    LINE2D l;
    l.SetPt1(m_pt[0]);
    l.SetPt2(m_c.ptCenter);

    POINT2D vec;
    vec = m_pt[0] - m_c.ptCenter;
    vec.Normalize();

    //     TEXT   TEXT
    //  ------->|<--------
    //
    //      |   |   | 
    //   C    A    B   D
    //  マウス位置がA,B以内にある場合は、位置固定

    //テキスト端のマージンを矢印の長さとする
    double dMargin;
    dMargin = _GetTextStartMargin();
    double dClick;
    dClick = m_c.ptCenter.Distance(pt);

    double dClick2;
    dClick2 = m_pt[0].Distance(pt);

    double dDir = 1.0;

    if((dClick < dR) )
    {
        dDir = -1.0;
    }


    POINT2D ptPos;
    if ((fabs(dClick - dR) < dMargin) &&
        (fabs (dClick2) < dMargin))
    {
        ptPos = dDir * dMargin * vec + m_pt[0];
    }
    else
    {
        ptPos =  pt;
    }


    double dRet;

    POINT2D ptNear = l.NearPoint( ptPos);

    if(!l.PosPram(&dRet, m_c.ptCenter, ptNear))
    {
        return false;
    }
//TRACE(_T("PosPram ptPos (%0.3f,%0.3f) %0.3f\n"), ptPos.dX, ptPos.dY, dRet);
    m_dTextOffset = dRet;

#if  0
    POINT2D ptTextCnter;
l.PramPos(&ptTextCnter, m_c.ptCenter, m_dTextOffset);
 
TRACE(_T("PosPram ptTextCnter2 (%0.3f,%0.3f) %0.3f\n"), ptTextCnter.dX, ptTextCnter.dY, m_dTextOffset);
#endif
    m_bSub = _CreateAuxCircle( m_pCircleSub, m_pt[0], m_c, m_dLayerScl);

    _UpdateText();

    return true;
}



POINT2D  CDrawingDimR::GetTextCenter() const
{
    POINT2D ptText;

    POINT2D ptOnline = m_pt[0];
    LINE2D l(ptOnline, m_c.ptCenter);

    l.PramPos(&ptText, m_c.ptCenter, m_dTextOffset);

    return ptText;
}

/*
bool CDrawingDimR::_CreateAuxCircle(std::unique_ptr<CIRCLE2D>& pSub,
                                   POINT2D ptOnArc, const CIRCLE2D& c) const
{
    // ptOnArc <- マウス位置から一番近い円弧上の点

    if (m_c.IsClose())
    {
        return false;
    }


    if (fabs(m_c.dRad) < NEAR_ZERO)
    {
        return false;
    }

    if (m_c.IsInsideAngle(ptOnArc))
    {
        return false;
    }


    double dMid = (m_c.GetStart() + m_c.GetEnd()) / 2.0;

    double dTop = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->dDimTop) / m_dLayerScl;
    double dGap = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->dDimGap) / m_dLayerScl;

    double dGapAngle = (dGap / m_c.dRad) * RAD2DEG;
    double dTopAngle = (dTop / m_c.dRad) * RAD2DEG;

    double dAngle = CUtil::NormAngle(CUtil::GetAngle(m_c.ptCenter, ptOnArc));

    double dStart = m_c.GetStart() - dGapAngle;
    double dEnd   = m_c.GetEnd() + dGapAngle;

    if (CUtil::IsInsideAngle( dStart,  dEnd,  dAngle))
    {
        return false;
    }
    

    double dStartSide;
    double dEndSide;


    dStartSide = CUtil::DiffAngle(dStart, dAngle);
    dEndSide   = CUtil::DiffAngle(dEnd, dAngle);

    double dSubS;
    double dSubE;

    if (dStartSide < dEndSide)
    {
        dSubS = dAngle - dTopAngle;
        dSubE = dStart;
    }
    else
    {
        dSubS = dEnd;
        dSubE = dAngle + dTopAngle;
    }
    
    if (!m_pCircleSub)
    {
        m_pCircleSub = std::make_unique<CIRCLE2D>(m_c);
    }

    m_pCircleSub->SetCenter(m_c.GetCenter());
    m_pCircleSub->SetRadius(m_c.GetRadius());
    m_pCircleSub->SetStart(dSubS);
    m_pCircleSub->SetEnd(dSubE);

    return true;
}
*/

//!< マーカ初期化
bool CDrawingDimR::InitNodeMarker(CNodeMarker* pMarker)
{

    POINT2D ptNorm;
    POINT2D ptOnline = m_pt[0];

    POINT2D ptText;

    ptText = GetTextCenter();

    static const bool bUseAuxLine = false;

    bool bRet;
    bRet = _InitNodeMarker(pMarker,
                            ptText,
                            false);

    pMarker->Create(m_psNodeHeight[0].get(), ptOnline);
    pMarker->Create(m_psNodeHeight[0].get(), ptOnline);

    return true;
}

//!< マーカ選択
void CDrawingDimR::SelectNodeMarker(CNodeMarker* pMarker, 
                                    StdString strMarkerId)
{
    CDrawingDim::SelectNodeMarker(pMarker, 
                                  strMarkerId);
}

//!< マーカ移動
void CDrawingDimR::MoveNodeMarker(CNodeMarker* pMarker, 
                                    SNAP_DATA* pSnap,
                                    StdString strMarkerId,
                                    MOUSE_MOVE_POS posMouse)
{
    pMarker->SetVisibleOnly(strMarkerId, true);
    bool bAuxLine = false;
    POINT2D ptAux;
    POINT2D pt = pSnap->pt;

    if (strMarkerId == _T(""))
    {
        m_eHit = H_NONE;
    }


    // m_pt[0] <- マウス位置から一番近い円弧上の点
    else if ((strMarkerId == MK_HIGHT1) ||
             (strMarkerId == MK_HIGHT2))
    {

        POINT2D ptSnap;
        using namespace VIEW_COMMON;

        if (pSnap->eSnapType == SNP_NONE)
        {
            bool bSnap;
            double dAngle;
            double    dLength;

            bSnap = pMarker->GetView()->GetLengthSnapPoint(&dAngle,
                                        &dLength,
                                        &ptSnap,  
                                        m_c.ptCenter,  
                                        pt,
                                        true,         //bAngle
                                        false     );  //bDistance


            if (bSnap)
            {
                pSnap->pt = ptSnap;
                pSnap->strSnap = GetSnapName(SNP_ANGLE);
                pSnap->strSnap += CUtil::StrFormat(_T(":%03f"), dAngle);
                pt = ptSnap;
            }
        }
        


        SetArrowPosition(pt);
        m_bSub = _CreateAuxCircle( m_pCircleSub, m_pt[0], m_c, m_dLayerScl);

        bAuxLine = true;
        ptAux = m_c.ptCenter;
    }
    else  if (strMarkerId == MK_TEXT)
    {
        bAuxLine = true;

        SetAngleHeight(pt);
        ptAux = GetTextCenter();
        
    }

    if (bAuxLine)
    {
        CPartsDef*   pDef  = pMarker->GetView()->GetPartsDef();
        m_psTmpLine->SetPoint1(pt);
        m_psTmpLine->SetPoint2(ptAux);
        pDef->SetMouseEmphasis(m_psTmpLine);
    }

}

//!< マーカ開放
bool CDrawingDimR::ReleaseNodeMarker(CNodeMarker* pMarker, StdString strMarkerId, MOUSE_MOVE_POS posMouse)
{
    return CDrawingDim::ReleaseNodeMarker(pMarker, strMarkerId, posMouse);
}

void CDrawingDimR::ChangeNode(StdString strMarkerId, E_NODE_CHANGE_TYPE eType)
{
}

/**
 *  @brief   スナップ点取得.
 *  @param   [out] pLstSnap スナップ点リスト
 *  @retval  true: スナップ点あり
 *  @note
 */
bool CDrawingDimR::GetSnapList(std::list<SNAP_DATA>* pLstSnap)const 
{
     using namespace VIEW_COMMON;

    DWORD dwSnap = DRAW_CONFIG->GetSnapType();
    bool bRet = false;

    STD_ASSERT(pLstSnap);

    if (!pLstSnap)
    {
        return false;
    }

    pLstSnap->clear();
    SNAP_DATA snapData;

    if (dwSnap & SNP_END_POINT)
    {
        snapData.eSnapType = SNP_FEATURE_POINT;
        snapData.bKeepSelect = false;

        snapData.pt = m_pt[0];
        pLstSnap->push_back(snapData);
 
        snapData.pt = GetTextCenter();
        pLstSnap->push_back(snapData);



        bRet = true;
    }

    return bRet;
}

bool CDrawingDimR::Create(const CDrawingCircle* pC, const POINT2D& ptClick)
{   
    _SetDefault();
     m_iId[0] = pC->GetId();

    double dR = pC->GetRadius();
    m_c = *(pC->GetCircleInstance());

    // CreateDR
    // m_pt[0] <- マウス位置から一番近い円弧上の点

    auto pCI = pC->GetCircleInstance();

    POINT2D pt2;
    double dAngle = (pCI->GetStart() + pCI->GetEnd()) / 2.0;

    bool bMidPoint = false;

    if (ptClick.Distance(m_c.ptCenter) < NEAR_ZERO)
    {
        bMidPoint = true;
    }

    if (!bMidPoint)
    {
        LINE2D l;
        l.SetPt1(pC->GetCenter());
        l.SetPt2(ptClick);

        std::vector<POINT2D> lstPoint;
        if(!pCI->Intersect(l, &lstPoint))
        {
            bMidPoint = false;
        }
        else
        {
            if(lstPoint.size() != 2)
            {
                bMidPoint = false;
            }
            else
            {
                if (lstPoint[1].Distance(ptClick) >
                    lstPoint[0].Distance(ptClick))
                {
                    pt2 = lstPoint[0];
                }
                else
                {
                    pt2 = lstPoint[1];
                }
            }
        }
    }

    if (bMidPoint)
    {
        pt2.dX = dR * cos(dAngle * DEG2RAD) + pC->GetCenter().dX;
        pt2.dY = dR * sin(dAngle * DEG2RAD) + pC->GetCenter().dY;
    }

    _CreateDim(pt2);
    _UpdateText();
    return true;
}

bool CDrawingDimR::_CreateDim(const POINT2D& pt1)
{
    if (m_main)
    {
        if (m_main->GetType() != DT_LINE)
        {
            delete m_main;
            m_main = NULL;
        }
    }

    if (!m_main)
    {
        m_main = new CDrawingLine;
    }

    m_pt[0] = pt1;

    return false;
}

void CDrawingDimR::_SetDefault()
{
    CDrawingDim::_SetDefault();
    m_c.dRad = 0.0;
    m_dTextOffset = 0.0;
    m_c.ptCenter.Set(0.0, 0.0);
    m_bShowAux[0] = true;
    m_bShowAux[1] = false;
    m_psDimText->SetPrifix(DRAW_CONFIG->strPriFixRadius.c_str());
    m_eArrowSide = VIEW_COMMON::ARS_START;
}

void CDrawingDimR::Draw(CDrawingView* pView, 
                         const std::shared_ptr<CDrawingObject> pParentObject,
                         E_MOUSE_OVER_TYPE eMouseOver,
                        int iLayerId)
{
    COLORREF crPen;
    int      iId;

    bool bIgnoreSelect = false;
    if (eMouseOver == E_MO_IGNORE_SELECT)
    {
        bIgnoreSelect = true;
    }

    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }

    crPen = GetDrawColor(pView, iLayerId, pParentObject.get(), &iId, bIgnoreSelect);
    _Draw(pView, crPen, iId, iLayerId, pParentObject.get(), eMouseOver);
}


void CDrawingDimR::DeleteView(CDrawingView* pView, 
                              const std::shared_ptr<CDrawingObject> pParentObject,
                              int iLayerId)
{
    COLORREF crObj;
    crObj = DRAW_CONFIG->GetBackColor();

    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }

    _Draw(pView, crObj, -1, iLayerId, pParentObject.get(), E_MO_NO);
}


void CDrawingDimR::_Draw(CDrawingView* pView, 
                         COLORREF crPen,
                         int      iId,
                         int iLayerId,
                         const CDrawingObject* pParentObject,
                         E_MOUSE_OVER_TYPE eMouseOver)
{

    if (m_eShow == SM_HIDE)
    {
        return;
    }

    if (!m_main)
    {
        return;
    }

    // m_pt[0] <- マウス位置から一番近い円弧上の点


    auto pL = dynamic_cast<CDrawingLine*>(m_main);

    POINT2D ptOnline = m_pt[0];
    double dRad = m_c.dRad;

    LINE2D* lMain = pL->GetLineInstance();

    LINE2D l(ptOnline, m_c.ptCenter);

    MAT2D matDraw;
    if (pParentObject )
    {
        const MAT2D* pMat = pParentObject->GetDrawingMatrix();
        if (pMat)
        {
            matDraw = *pMat;
        }
    }

    if (m_pMatOffset)
    {
        matDraw = matDraw * (*m_pMatOffset);
    }

    l.Matrix(matDraw);

    int iWidth = m_iLineWidth;
    if (eMouseOver == E_MO_SELECTABLE)
    {
        pView->SetLineAlternateMode(true);
        iWidth += 2;
    }
    else if (eMouseOver == E_MO_EMPHASIS)
    {
        crPen = DRAW_CONFIG->crEmphasis;
    }
	else if (eMouseOver == E_MO_CONNECTABLE)
	{
		crPen = DRAW_CONFIG->crConnectable;
	}

    bool bShowSub1 = false;


    double dDspScl = pView->GetViewScale();
#if 0
    StdString strDim = GetDimText();
    
    CExtText   extText;
    extText.SetText(strDim.c_str());
    _CalcTextHeight(pView, extText);

    extText.SetHeight(-1,-1, m_dTextHeightPoint);


    POINT2D ptTextCnter;


    POINT2D ptTextEnd;

    double dL = (m_psDimText->GetTextSize().dX / 2.0);
    double dTextEnd;

    l.PramPos(&ptTextCnter, m_c.ptCenter, m_dTextOffset);

    double dDir = 1.0;
    l.PosPram(&dDir, ptOnline, ptTextCnter);

    if (dDir < 0.0)
    {
        dTextEnd = m_dTextOffset - dL;
    }
    else
    {
        dTextEnd = m_dTextOffset + dL;
    }

    POINT2D ptTextCenter;
    //POINT2D ptTextEnd;

    l.PramPos(&ptTextEnd, m_c.ptCenter, dTextEnd);
 
TRACE(_T("PosPram ptTextCnter (%0.3f,%0.3f)\n"), ptTextCnter.dX, ptTextCnter.dY);
TRACE(_T("dTextEnd  %0.3f,%0.3f ,%0.3f\n"), dTextEnd, m_dTextOffset, dL);

    lMain->SetPt1(ptOnline);
    //lMain->SetPt2(ptTextCnter);
    lMain->SetPt2(ptTextEnd);


    double dPosAngle= CUtil::NormAngle(pL->GetLineInstance()->Angle());

    double dAngle = dPosAngle;
    POINT2D ptNorm(1.0, 0.0);
    ptNorm.Rotate(dPosAngle + 90.0);

    if (((dPosAngle >= 315.0) &&  (dPosAngle <= 360.0)) ||
         (dPosAngle >= 0.0) &&    (dPosAngle <  125.0))
    {
        ptTextCnter += ptNorm * m_dVText;
        extText.SetAlign(CExtText::BOTTTOM_MID);
    }
    else
    {
        dAngle = CUtil::NormAngle(dAngle - 180.0);
        ptTextCnter -= ptNorm * m_dVText;
        extText.SetAlign(CExtText::BOTTTOM_MID);
    }

TRACE(_T("Angle %.2f, %.2f\n"), dPosAngle, dAngle);

    extText.SetAngle(dAngle);
    extText.SetColor(-1, -1, crPen);

    POINT pt;
    pView->ConvWorld2Scr(&pt, ptTextCnter, iLayerId);

   if (m_pMatOffset)
   {
        //マウスで移動、回転時表示用に使用する行列
        ///lMain->SetPt1(lMain->GetPt1() * (*m_pMatOffset));
        //lMain->SetPt2(lMain->GetPt2() * (*m_pMatOffset));

       /*
        lSub1->SetPt1(lSub1->Pt(0) * (*m_pMatOffset));
        lSub1->SetPt2(lSub1->Pt(1) * (*m_pMatOffset));

        lSub2->SetPt1(lSub2->Pt(0) * (*m_pMatOffset));
        lSub2->SetPt2(lSub2->Pt(1) * (*m_pMatOffset));
        */
    }

    

    pView->Text(&extText, pt, 
                        0,
                        dDspScl,
                        false,  iId);

#endif
   if (m_pMatOffset)
   {
        //マウスで移動、回転時表示用に使用する行列
        ///lMain->SetPt1(lMain->GetPt1() * (*m_pMatOffset));
        //lMain->SetPt2(lMain->GetPt2() * (*m_pMatOffset));

       /*
        lSub1->SetPt1(lSub1->Pt(0) * (*m_pMatOffset));
        lSub1->SetPt2(lSub1->Pt(1) * (*m_pMatOffset));

        lSub2->SetPt1(lSub2->Pt(0) * (*m_pMatOffset));
        lSub2->SetPt2(lSub2->Pt(1) * (*m_pMatOffset));
        */
    }


    POINT2D ptTextCnter;

    ptTextCnter = GetTextCenter();
    //l.PramPos(&ptTextCnter, m_c.ptCenter, m_dTextOffset);
    double dPosAngle= CUtil::NormAngle(pL->GetLineInstance()->Angle());
    double dAngle = dPosAngle;

    //角度は0〜180に正規化する
    dAngle = NormalizeTextAngle(dAngle);

    m_psDimText->SetPos(ptTextCnter);
    m_psDimText->SetAngle(dAngle);
    COLORREF crText = crPen;
    if (m_eHit == CDrawingDim::H_TEXT)
    {
        crText =  DRAW_CONFIG->crImaginary;
    }
    m_psDimText->Draw(pView, crText,iId, iLayerId, eMouseOver);

    POINT2D ptTextEnd;
    double dL = (m_psDimText->GetTextSize().dX / 2.0);
    double dTextEnd;
    double dDir = 1.0;

    l.PosPram(&dDir, ptOnline, ptTextCnter);
    if (dDir < 0.0)
    {
        dTextEnd = m_dTextOffset - dL;
    }
    else
    {
        dTextEnd = m_dTextOffset + dL;
    }

    l.PramPos(&ptTextEnd, m_c.ptCenter, dTextEnd);

    lMain->SetPt1(ptOnline);
    lMain->SetPt2(ptTextEnd);

    pL->SetId(iId);
    pL->SetColor(crPen);
    pL->Draw( pView, NULL, eMouseOver);

    if (m_bShowAux[0] &&
        m_bSub)
    {

        CPartsDef* pCtrl = pView->GetPartsDef();
        CLayer* pLayer   = pCtrl->GetLayer(iLayerId);
        double dScl = pLayer->dScl;

        if (fabs(m_dLayerScl - dScl) > NEAR_ZERO)
        {
            m_dLayerScl = dScl;
            _CreateAuxCircle( m_pCircleSub, ptOnline, m_c, m_dLayerScl);
        }

        if (m_pCircleSub)
        {
            Circle(pView, iLayerId, m_pCircleSub.get(), NULL, PS_SOLID, iWidth, crPen, iId);
        }
    }

    //---------------------
    // 矢印描画
    //---------------------
    using namespace VIEW_COMMON;

    bool bHitArrow = false;
    if ((m_eArrowType == AT_OPEN) ||
        (m_eArrowType == AT_FILLED))
    {
        if (m_eHit == H_ARROW)
        {
            _DrawigArrow(pView, 
                m_main,
                iWidth, 
                DRAW_CONFIG->crImaginary,
                iId, 
                iLayerId,
                m_eArrowType, 
                m_eArrowSide, 
                !m_bArrowFlip);
            bHitArrow = true;
        }
    }

    if (!bHitArrow)
    {
        _DrawigArrow(pView,
                            m_main,
                            iWidth, 
                            crPen,
                            iId,
                            iLayerId,
                            m_eArrowType, 
                            m_eArrowSide, 
                            m_bArrowFlip);
    }
    //点線の隙間を塗りつぶすモード


   return;
}

/**
 * @brief   領域取得
 * @param   なし
 * @retval  なし
 * @note	描画領域を取得する
 */
RECT2D CDrawingDimR::GetBounds() const
{
    RECT2D rc;
    rc = m_pt[0];
    rc.ExpandArea(m_c.ptCenter);
    return rc;
}


//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void TEST_CDrawingDimR()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}
#endif //_DEBUG