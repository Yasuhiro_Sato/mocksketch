/**
* @brief        CDrawingScriptBaseヘッダーファイル
* @file	        CDrawingScriptBase.h
* @author           Yasuhiro Sato
* @date	        07-2-2009 20:03:09
* 
* @note	
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
* 
* @remarks	        
*			
*
* $
* $
* 
*/
#ifndef __DRAWING_DIM_R_H_
#define __DRAWING_DIM_R_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DrawingObject/CDrawingObject.h"
#include "DrawingObject/CDrawingDim.h"
#include "DrawingObject/Primitive/LINE2D.h"
#include "DrawingObject/Primitive/CIRCLE2D.h"
#include "DrawingObject/Primitive/MAT2D.h"
#include "Utility/ExtText/CExtText.h"

/*---------------------------------------------------*/
/*  classes                                          */
/*---------------------------------------------------*/

/**
 * @class   CDrawingDimR
 * @brief   寸法（距離）
 */
class CDrawingDimR :public CDrawingDim
{
protected:
    CIRCLE2D m_c;
    double m_dTextOffset;

    bool m_bSub;
    mutable std::unique_ptr<CIRCLE2D> m_pCircleSub;  
    mutable double m_dLayerScl;

public:
    //!< コンストラクタ
    CDrawingDimR();

    //!< コンストラクタ
	CDrawingDimR(int nID);

    //!< コピーコンストラクタ
	CDrawingDimR(const CDrawingDimR& Obj);

    virtual ~CDrawingDimR();

    //!< 相対移動
    virtual void Move(const POINT2D& pt2D);

    //!< 回転
    virtual void Rotate(const POINT2D& pt2D, double dAngle);

    //!< 鏡像
	virtual void Mirror(const LINE2D& line);

    //!< 倍率
	virtual void Scl(const POINT2D& pt2D, double dXScl, double dYScl);

    //!< 行列適用
    virtual void Matrix(const MAT2D& mat2D);

    //!< 領域取得
    virtual RECT2D GetBounds() const;

    virtual bool Create(const CDrawingCircle* pC, const POINT2D& ptClick);

    virtual void Draw(CDrawingView* pView,
                      const std::shared_ptr<CDrawingObject> pParentObject,
                      E_MOUSE_OVER_TYPE eMouseOver,
                      int iLayerId = -1) override;

    //!< 描画削除
    virtual void DeleteView(CDrawingView* pView, 
                            const std::shared_ptr<CDrawingObject> pParentObject,
                            int iLayerId = -1) override;


    virtual double GetVal() const;

    bool SetAngleHeight(const POINT2D& pt);

    virtual bool SetTextCenter(const POINT2D& pt) override;

    virtual bool SetArrowPosition(const POINT2D& pt);

    virtual POINT2D  GetTextCenter() const override;
    //-------------------
    // ドラッグ用マーカ
    //-------------------
    //!< マーカ初期化 true:マーカあり
    virtual bool InitNodeMarker(CNodeMarker* pMarker) override;

    //!< マーカ選択
    virtual void SelectNodeMarker(CNodeMarker* pMarker, 
                                     StdString strMarkerId) override;

    //!< マーカ移動
    virtual void MoveNodeMarker(CNodeMarker* pMarker, 
                                     SNAP_DATA* pSnap,
                                     StdString strMarkerId,
                                     MOUSE_MOVE_POS posMouse) override;

    //!< マーカ開放
    virtual bool ReleaseNodeMarker(CNodeMarker* pMarker, StdString strMarkerId, MOUSE_MOVE_POS posMouse) override;

    //!< ノード変更
    virtual void ChangeNode(StdString strMarkerId, E_NODE_CHANGE_TYPE eType) override;


    //!< スナップ点取得
    virtual  bool GetSnapList(std::list<SNAP_DATA>* pLstSnap)const override;

protected:
    virtual void _Draw(CDrawingView* pView,
                       COLORREF crPen,
                       int      iId,
                       int iLayerId,
                       const CDrawingObject* pParentObject,
                       E_MOUSE_OVER_TYPE eMouseOver);


    virtual void _SetDefault() override;
 
    bool _CreateDim (const POINT2D& pt1);

    bool _DrawigLength(CDrawingView* pView, 
                       COLORREF crPen,
                       int iId,
                       E_MOUSE_OVER_TYPE eMouseOver) const;


    void _GetLengthMain(LINE2D* pLineMane, POINT2D* ptNorm) const;

    double _GetTextStartMargin() const;


private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
            ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(CDrawingDim);

        SERIALIZATION_BOTH("Circle"        , m_c);
        SERIALIZATION_BOTH("TextOffset"    , m_dTextOffset);

        if(Archive::is_loading::value)
        {
            _CreateDim(m_pt[0]);
        }
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }
};

#endif // 