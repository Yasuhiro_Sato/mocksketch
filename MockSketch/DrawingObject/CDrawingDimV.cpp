/**
 * @brief			CDrawingDimH実装ファイル
 * @file			CDrawingDimH.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CDrawingDim.h"
#include "./CDrawingDimV.h"
#include "./CDrawingPoint.h"
#include "./CDrawingLine.h"
#include "./CDrawingCircle.h"
#include "./CDrawingSpline.h"
#include "./CDrawingEllipse.h"
#include "./CDrawingCompositionLine.h"
#include "./CDrawingParts.h"
#include "./CDrawingText.h"
#include "./CDrawingNode.h"
#include "./CDimText.h"
#include "DefinitionObject/View/CDrawingView.h"
#include "DefinitionObject/CPartsDef.h"
#include "Utility/CUtility.h"
#include "Utility/ExtText/CExtText.h"
#include "System/CSystem.h"
#include "SubWindow/DimTextDlg.h"

#include <math.h>
#include <boost/serialization/export.hpp> 
BOOST_CLASS_EXPORT(CDrawingDimV);


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
CDrawingDimV::CDrawingDimV():
CDrawingDimH( 0, DT_DIM_V)
{
    //TODO:実装
}

/**
 *  @brief  コンストラクター.
 *  @param  [in] nID 
 *  @retval なし 
 *  @note
 */
CDrawingDimV::CDrawingDimV(int nID):
CDrawingDimH( nID, DT_DIM_V)
{
    //TODO:実装
}

/**
 * コピーコンストラクタ
 */
CDrawingDimV::CDrawingDimV(const CDrawingDimV& Obj):
CDrawingDimH( Obj)
{
DB_PRINT(_T("CDrawingDimV %f \n"), GetVal() );
    _UpdateText();
}


CDrawingDimV::~CDrawingDimV()
{

}

double CDrawingDimV::GetVal() const
{
    double dVal = 0.0;
    dVal = fabs(m_pt[0].dY - m_pt[1].dY);
DB_PRINT(_T("CDrawingDimV::GetVal %f \n"), dVal );
    return dVal;
}

void CDrawingDimV::_GetLengthMain(LINE2D* pLineMane, POINT2D* pNorm) const
{
    //矢印設定、マーカ表示に使用
    POINT2D ptNorm;
    ptNorm.Set(0.0, 1.0);
    pLineMane->SetPt(m_ptLineCenter.dX, m_pt[0].dY,
                        m_ptLineCenter.dX, m_pt[1].dY);
    *pNorm = ptNorm;
}

bool CDrawingDimV::SetTextCenter(const POINT2D& pt)
{
    m_ptLineCenter.dX = pt.dX;
    m_ptLineCenter.dY = (m_pt[0].dY + m_pt[1].dY) / 2.0;
    return true;
}

//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void TEST_CDrawingDimV()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}
#endif //_DEBUG