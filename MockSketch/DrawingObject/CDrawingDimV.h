/**
* @brief        CDrawingScriptBaseヘッダーファイル
* @file	        CDrawingScriptBase.h
* @author           Yasuhiro Sato
* @date	        07-2-2009 20:03:09
* 
* @note	
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
* 
* @remarks	        
*			
*
* $
* $
* 
*/
#ifndef __DRAWING_DIM_V_H_
#define __DRAWING_DIM_V_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DrawingObject/CDrawingDimH.h"
#include "DrawingObject/Primitive/LINE2D.h"
#include "DrawingObject/Primitive/MAT2D.h"
#include "Utility/ExtText/CExtText.h"

/*---------------------------------------------------*/
/*  classes                                          */
/*---------------------------------------------------*/

/**
 * @class   CDrawingDimH
 * @brief   寸法（距離）
 */



class CDrawingDimV :public CDrawingDimH
{
public:
    //!< コンストラクタ
    CDrawingDimV();

    //!< コンストラクタ
	CDrawingDimV(int nID);

    //!< コピーコンストラクタ
	CDrawingDimV(const CDrawingDimV& Obj);

    virtual ~CDrawingDimV();

    virtual double GetVal() const;

    virtual bool SetTextCenter(const POINT2D& pt) override;

protected:
    void _GetLengthMain(LINE2D* pLineMane, POINT2D* ptNorm) const;

};

#endif // 