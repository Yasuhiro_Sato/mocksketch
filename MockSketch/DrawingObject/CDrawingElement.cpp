/**
 * @brief			CDrawingElement実装ファイル
 * @file			CDrawingElement.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MockSketch.h"

#include "DefinitionObject/CElementDef.h"
#include "./CDrawingElement.h"
#include "./CDrawingPoint.h"

#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/View/CDrawingView.h"

#include "Utility/CUtility.h"
#include "System/CSystem.h"
#include "Script/CScriptObject.h"
#include <math.h>
#include <boost/serialization/export.hpp> 
BOOST_CLASS_EXPORT(CDrawingElement);


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
CDrawingElement::CDrawingElement():
CDrawingScriptBase  (),
m_dAngle            (0.0),
m_dCenterAngle      (0.0),
m_iIconIndex        (0),
m_dValue            (0.0)
{
    m_eType  = DT_ELEMENT;
}

/**
 *  @brief  コンストラクター.
 *  @param  [in] nID 
 *  @retval なし 
 *  @note
 */
CDrawingElement::CDrawingElement(int nID):
CDrawingScriptBase( nID, DT_ELEMENT),
m_dAngle            (0.0),
m_dCenterAngle      (0.0),
m_iIconIndex        (0),
m_dValue            (0.0)
{
    m_eType  = DT_ELEMENT;
}


/**
 *  @brief  デストラクタ.
 *  @param  なし 
 *  @retval なし 
 *  @note
 */
CDrawingElement::~CDrawingElement()
{
}

/**
 * コピーコンストラクタ
 */
CDrawingElement::CDrawingElement(const CDrawingElement& Obj):
CDrawingScriptBase( Obj)
{
    m_Pt                = Obj.m_Pt;
    m_dAngle            = Obj.m_dAngle;
    m_dCenterAngle      = Obj.m_dCenterAngle;
    m_iIconIndex        = Obj.m_iIconIndex;
    m_dValue            = Obj.m_dValue;
}


/**
 *  @brief  回転.
 *  @param  [in]    pPt2D    回転中心(絶対座標)
 *  @param  [in]    dAngle   (絶対)回転角(rad)
 *  @retval     なし
 *  @note
 */
void CDrawingElement::Rotate(const POINT2D& pt2D, double dAngle)
{
    AddChgCnt();
    m_Pt.Rotate(pt2D, dAngle);
    m_dAngle = dAngle;
}

/**
 *  @brief  相対移動.
 *  @param  [in]    pPt2D   移動量
 *  @retval         なし
 *  @note
 */
void CDrawingElement::Move(const POINT2D& pt2D) 
{
    AddChgCnt();
    m_Pt.Move(pt2D);
}

/**
 *  @brief  交点計算.
 *  @param  [out]   pList   交点のリスト
 *  @param  [in]    pObj    交差するオブジェクト
 *  @retval         なし                
 *  @note
 */
void CDrawingElement::CalcIntersection ( std::vector<POINT2D>* pList,   
                                const CDrawingObject* pObj, 
                                bool bOnline,
                                double dMin)  const
{
    CDrawingObject::CalcIntersectionPoint ( pList, pObj, m_Pt, bOnline, dMin);
}

/**
 *  @brief  位置調整.
 *  @param  [in]    pPtIntersect   調整位置
 *  @param  [in]    pPtClick       調整方向指定点
 *  @retval         なし
 *  @note
 */
bool CDrawingElement::Trim (const POINT2D& ptIntersect, const POINT2D& ptClick, bool bReverse)
{
    //Trimは不可
    return false;
}


/**
 *  @brief  分割.
 *  @param  [in]    pPtBreak  分割位置
 *  @retval         分割によって出来たオブジェクト
 *  @note
 */
std::shared_ptr<CDrawingObject>  CDrawingElement::Break (const POINT2D& ptBreak)
{
    //分割不可
    return NULL;
}


/**
 *  @brief  描画.
 *  @param  [in]  pView       表示View
 *  @param  [in]  bMouseOver  true オブジェクト上にマウスあり
 *  @retval         なし
 *  @note
 */
void CDrawingElement::Draw(CDrawingView* pView, 
                        const std::shared_ptr<CDrawingObject> pParentObject,
                        E_MOUSE_OVER_TYPE eMouseOver,
                        int iLayerId)
{
    STD_ASSERT(pView != 0);

    POINT  pt;

    CScriptObject* pObj;
    pObj = GetScriptObject();
    if (pObj)
    {
        pObj->DrawScript(pView);
    }

    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }

    //-------------------
    // MATRIX
    //-------------------
    POINT2D ptDraw = m_Pt;
    MAT2D matDraw;

    if (pParentObject )
    {
        const MAT2D* pMat = pParentObject->GetDrawingMatrix();
        if (pMat)
        {
            matDraw = *pMat;
        }
    }

    if (m_pMatOffset)
    {
        matDraw = matDraw * (*m_pMatOffset);
    }

    ptDraw = m_Pt * matDraw;


    pView->ConvWorld2Scr(&pt, ptDraw, iLayerId);

    std::shared_ptr<CElementDef> pElemetDef = GetElementDef();

    if (!pElemetDef)
    {
        return;
    }

    CImageData* pImage;
    pImage = pElemetDef->GetIconData(m_iIconIndex);
    pView->SetIndexBufferId(m_nId);
    pView->Image(pt, pImage);


    //実装はこれから、現状では未だ不十分
    COLORREF crObj;
    if (eMouseOver == E_MO_SELECTABLE)
    {
        crObj = pView->GetCurrentLayer()->crBrush;
        pView->SetPen( PS_SOLID, 1, crObj, m_nId);
        pView->ImageBounds(pt, pImage);
    }
    else if (eMouseOver == E_MO_EMPHASIS)
    {
        crObj = DRAW_CONFIG->crEmphasis;
        pView->SetPen( PS_SOLID, 1, crObj, m_nId);
        pView->ImageBounds(pt, pImage);
    }
	else if (eMouseOver == E_MO_CONNECTABLE)
	{
		crObj = DRAW_CONFIG->crConnectable;
		pView->SetPen(PS_SOLID, 1, crObj, m_nId);
		pView->ImageBounds(pt, pImage);
	}


}

/**
 *  @brief  描画削除.
 *  @param  [in]    pView   表示View
 *  @retval         なし
 *  @note
 */
void CDrawingElement::DeleteView(CDrawingView* pView, 
                            const std::shared_ptr<CDrawingObject> pParentObject,
                              int iLayerId)
{
}

/**
 *  @brief  鏡像.
 *  @param  [in]    line   対称軸
 *  @retval         なし
 *  @note
 */
void CDrawingElement::Mirror(const LINE2D& line)
{
}


/**
 *  @brief  行列.
 *  @param  [in]    mat2D   適用する行列
 *  @retval         なし
 *  @note
 */
void CDrawingElement::Matrix(const MAT2D& mat2D)
{
    m_mat2D = m_mat2D * mat2D;

    double dSclX, dSclY, dT;
    m_mat2D.GetAffineParam(&m_Pt,  &m_dAngle, &dSclX, &dSclY, &dT);

    AddChgCnt();
}

void CDrawingElement::MatrixDirect(const MAT2D& mat2D)
{
    m_mat2D = mat2D;

    double dSclX, dSclY, dT;
    m_mat2D.GetAffineParam(&m_Pt,  &m_dAngle, &dSclX, &dSclY, &dT);

    AddChgCnt();
}

/**
 *  @brief  倍率.
 *  @param  [in]    pPt2D   拡大中心
 *  @param  [in]    dSclX   X倍率
 *  @param  [in]    dSclY   Y倍率
 *  @retval         なし
 *  @note
 */
void CDrawingElement::Scl(const POINT2D& pt2D, double dXScl, double dYScl)
{
}

/**
 *  @brief  範囲.
 *  @param  [in] p1 1点目
 *  @param  [in] p2 2点目
 *  @retval  true:2点で示される矩形内に点が存在する 
 *  @note
 */
bool CDrawingElement::IsInner( const RECT2D& rcArea, bool bPart) const
{
    return false;
}


/**
 *  @brief   参照データに基づいて更新.
 *  @param   
 *  @retval  
 *  @note
 */
CDrawingObject* CDrawingElement::UpdateRef()
{
    CDrawingObject*      pRef;

    pRef = CDrawingObject::UpdateRef();

    if (pRef == NULL)
    {
        return NULL;
    }

    CDrawingElement* pDim = dynamic_cast<CDrawingElement*>(pRef);

    STD_ASSERT(pDim != NULL);

    if (pDim == NULL)
    {
        return false;
    }

    //TODO: 実装

    m_iChgCnt = pRef->GetChgCnt();
    return pRef;
}

/**
 * @brief   位置設定
 * @param   [in] ptAbs 表示位置
 * @retval  なし
 * @note    
 */
void  CDrawingElement::SetPoint(POINT2D ptAbs)
{
    m_Pt = ptAbs;
}


/**
 *  @brief   スナップ点取得.
 *  @param   [out] pLstSnap スナップ点リスト
 *  @retval  true: スナップ点あり
 *  @note
 */
bool CDrawingElement::GetSnapList(std::list<SNAP_DATA>* pLstSnap)const 
{
    using namespace VIEW_COMMON;

    DWORD dwSnap = DRAW_CONFIG->GetSnapType();
    bool bRet = false;

    STD_ASSERT(pLstSnap);

    if (!pLstSnap)
    {
        return false;
    }

    pLstSnap->clear();
    SNAP_DATA snapData;

    if (dwSnap & SNP_FEATURE_POINT)
    {
        snapData.eSnapType = SNP_FEATURE_POINT;
        snapData.bKeepSelect = false;
        snapData.pt = m_Pt;
        pLstSnap->push_back(snapData);
        bRet = true;
    }

    return bRet;
}

/**
 * @brief   領域取得
 * @param   なし
 * @retval  なし
 * @note	描画領域を取得する
 */
RECT2D CDrawingElement::GetBounds() const
{
    //TODO: 実装
    RECT2D rc;
    return rc;
}




/**
 *  @brief   プロパティ初期化.
 *  @param   [in] pCtrl オブジェクトコントロールへのポインタ
 *  @retval     なし
 *  @note
 */
TREE_GRID_ITEM*  CDrawingElement::_CreateStdPropertyTree()
{
    NAME_TREE_ITEM<CStdPropertyItem>* pBeforeGroup;
    NAME_TREE_ITEM<CStdPropertyItem>* pNextItem;
    NAME_TREE_ITEM<CStdPropertyItem>* pGroupItem;
    NAME_TREE<CStdPropertyItem>* pTree;
    //int iId;
    CStdPropertyItem* pItem;
    boost::tuple< CStdPropertyItem*, void*> pairVal;


    pBeforeGroup = CDrawingScriptBase::_CreateStdPropertyTree();
    pTree = m_psProperty->GetTreeInstance();

    pGroupItem  = pTree->AddNext(pBeforeGroup, NULL, GET_STR(STR_PRO_ELEMENT), _T("Element"));

    //------------
    //位置
    //------------
    CStdPropertyItemDef DefPos(PROP_POINT2D, 
        GET_STR(STR_PRO_POS)   ,
        _T("Position"),
        GET_STR(STR_PRO_INFO_POS), 
        true, 
        DISP_UNIT,
        POINT2D());

    pItem = new CStdPropertyItem(DefPos, PropPosition, m_psProperty, NULL, &m_Pt);
    pNextItem = pTree->AddChild(pGroupItem, pItem, DefPos.strDspName, pItem->GetName(), 2);

    //------------
    //角度(deg)
    //------------
    CStdPropertyItemDef DefAngle(PROP_DOUBLE, 
        GET_STR(STR_PRO_ANGLE)   , 
        _T("Angle"),
        GET_STR(STR_PRO_INFO_ANGLE), 
        true,
        DISP_ANGLE,
        double(0.0));

    pItem = new CStdPropertyItem(DefAngle, PropAngle, m_psProperty, NULL, &m_dAngle);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefAngle.strDspName, pItem->GetName());

    //------------
    //中心角(deg)
    //------------
    CStdPropertyItemDef DefCenterAngle(PROP_DOUBLE, 
        GET_STR(STR_PRO_CENTER_ANGLE)   , 
        _T("CenterAngle"),
        GET_STR(STR_PRO_INFO_CENTER_ANGLE), 
        true,
        DISP_ANGLE,
        double(0.0));

    pItem = new CStdPropertyItem( DefCenterAngle, PropCenterAngle, m_psProperty, NULL, &m_dCenterAngle);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefCenterAngle.strDspName, pItem->GetName());

    //--------------------------
    // アイコンインデックス
    //--------------------------
    //TODO:ドロップダウンにするべきか？
    std::shared_ptr<CElementDef> pElemetDef = GetElementDef();
    STD_ASSERT(pElemetDef);

    int iIconMax = pElemetDef->GetMax() - 1;
    CStdPropertyItemDef DefIconIndex(PROP_INT_RANGE, 
        GET_STR(STR_PRO_ICON_INDEX)   , 
        _T("IconIndex"),
        GET_STR(STR_PRO_INFO_ICON_INDEX), 
        true,
        DISP_ANGLE,
        0,
        0,
        iIconMax
        );

    pItem = new CStdPropertyItem( DefIconIndex, PropIconIndex, m_psProperty, NULL, &m_iIconIndex);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefIconIndex.strDspName, pItem->GetName());

    //------------
    //値
    //------------
    CStdPropertyItemDef DefValue(PROP_DOUBLE, 
        GET_STR(STR_PRO_VALUE)   , 
        _T("CenterAngle"),
        GET_STR(STR_PRO_INFO_VALUE), 
        true,
        DISP_ANGLE,     //TODO:単位はElementDefで設定
        double(0.0));

    pItem = new CStdPropertyItem( DefValue, PropValue, m_psProperty, NULL, &m_dValue);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefValue.strDspName, pItem->GetName());


    return pGroupItem;
}

/**
 *  @brief   プロパティ変更(位置)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingElement::PropPosition     (CStdPropertyItem* pData, void* pObj)
{
    CDrawingElement* pDrawing = reinterpret_cast<CDrawingElement*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->m_pCtrl->HideObject(pDrawing->m_nId);
        pDrawing->SetPoint(pData->anyData.GetPoint());
        pDrawing->m_pCtrl->Draw(pDrawing->m_nId);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(位置)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingElement::PropAngle        (CStdPropertyItem* pData, void* pObj)
{
    CDrawingElement* pDrawing = reinterpret_cast<CDrawingElement*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->m_pCtrl->HideObject(pDrawing->m_nId);
        pDrawing->SetAngle(pData->anyData.GetDouble());
        pDrawing->m_pCtrl->Draw(pDrawing->m_nId);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(中心角)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingElement::PropCenterAngle  (CStdPropertyItem* pData, void* pObj)
{
    CDrawingElement* pDrawing = reinterpret_cast<CDrawingElement*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->m_pCtrl->HideObject(pDrawing->m_nId);
        pDrawing->SetCenterAngle(pData->anyData.GetDouble());
        pDrawing->m_pCtrl->Draw(pDrawing->m_nId);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(アイコンインデックス)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingElement::PropIconIndex    (CStdPropertyItem* pData, void* pObj)
{
    CDrawingElement* pDrawing = reinterpret_cast<CDrawingElement*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->m_pCtrl->HideObject(pDrawing->m_nId);
        pDrawing->SetIconIndex(pData->anyData.GetInt());
        pDrawing->m_pCtrl->Draw(pDrawing->m_nId);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(アイコンインデックス)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingElement::PropValue        (CStdPropertyItem* pData, void* pObj)
{
    CDrawingElement* pDrawing = reinterpret_cast<CDrawingElement*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->m_pCtrl->HideObject(pDrawing->m_nId);
        pDrawing->SetValue(pData->anyData.GetDouble());
        pDrawing->m_pCtrl->Draw(pDrawing->m_nId);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}



/**
 *  @brief   描画(マウスオーバー時).
 *  @param   [in] pView 表示View
 *  @param   [in] bOver true:開始 false:終了
 *  @retval  なし
 *  @note
 */
void CDrawingElement::DrawOver( CDrawingView* pView, bool bOver)
{
}


/**
 * @brief   参照元取得
 * @param   なし
 * @retval  参照元へのポインタ
 * @note    
 */
std::shared_ptr<CElementDef>  CDrawingElement::GetElementDef() const
{
    std::shared_ptr<CElementDef> pRet;
    std::shared_ptr<CObjectDef> pDef;

    pDef = GetObjectDef().lock();

    if (!pDef)
    {
        return pRet;
    }

    pRet = std::dynamic_pointer_cast<CElementDef> (pDef);
    return pRet;
}

/**
 * @brief   ロード終了後処理
 * @param   なし
 * @retval  なし
 * @note	
 */
void CDrawingElement::LoadAfter(CPartsDef* pDef)
{
    /*
    CObjectDef* pDef = NULL;

    if (!m_uuidElemetDef.is_nil())
    {
        pDef = THIS_APP->GetObjectDefByUuid(m_uuidElemetDef);
        m_pElementDef = dynamic_cast<CElementDef*>(pDef);
    }
    else
    {
        m_pElementDef = NULL;
    }
    */
}

/**
 * @brief   シリアル化(ロード)
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  なし
 * @note	
 */
template<class Archive>
void CDrawingElement::load(Archive& ar, const unsigned int version)
{
    SERIALIZATION_INIT;
        ar >> BOOST_SERIALIZATION_BASE_OBJECT_NVP(CDrawingScriptBase);
        SERIALIZATION_LOAD("Pos"   , m_Pt);
        //SERIALIZATION_LOAD("ElementID" , m_uuidElemetDef);
    MOCK_EXCEPTION_FILE(e_file_read);
}


/**
 * @brief   シリアル化(セーブ)
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  なし
 * @note	
 */
template<class Archive>
void CDrawingElement::save(Archive& ar, const unsigned int version) const
{
    SERIALIZATION_INIT;
        ar << BOOST_SERIALIZATION_BASE_OBJECT_NVP(CDrawingScriptBase);
        SERIALIZATION_SAVE("Pos"       , m_Pt);
        //SERIALIZATION_SAVE("ElementID" , m_uuidElemetDef);

   MOCK_EXCEPTION_FILE(e_file_write);
}

//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void TEST_CDrawingElement()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}
#endif //_DEBUG