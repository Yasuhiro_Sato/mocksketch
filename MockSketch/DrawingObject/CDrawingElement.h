/**
 * @brief        CDrawingElementヘッダーファイル
 * @file	        CDrawingElement.h
 * @author           Yasuhiro Sato
 * @date	        07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
  *			
 *
 * $
 * $
 * 
 */
#ifndef __DRAWING_ELEMENT_H_
#define __DRAWING_ELEMENT_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DrawingObject/CDrawingScriptBase.h"
#include "DrawingObject/Primitive/LINE2D.h"
#include "DrawingObject/Primitive/MAT2D.h"

/*---------------------------------------------------*/
/*  Classe                                           */
/*---------------------------------------------------*/
class CElementDef;

/**
 * @class   CDrawingElement
 * @brief  
 */
class CDrawingElement :public CDrawingScriptBase//:public CRTTIClass<CDrawingElement, CDrawingObject, DT_LINE>
{
public:

public:
    //!< コンストラクタ
    CDrawingElement();

    //!< コンストラクタ
    CDrawingElement(int nID);

    //!< コピーコンストラクタ
    CDrawingElement(const CDrawingElement& Obj);

    //!< デストラクタ
    virtual ~CDrawingElement();

    //!< 相対移動
    virtual void Move(const POINT2D& pt2D);

    //!< 回転
    virtual void Rotate(const POINT2D& pt2D, double dAngle);

    //!< 交点計算
    virtual void CalcIntersection ( std::vector<POINT2D>* pLiat,   
        const CDrawingObject* pObj, 
        bool bOnline = true,
        double dMin = NEAR_ZERO) const;

    //!< 鏡像
	virtual void Mirror(const LINE2D& line);

    //!< 倍率
	virtual void Scl(const POINT2D& pt2D, double dXScl, double dYScl);

    //!< 行列適用
    virtual void Matrix(const MAT2D& mat2D);

    virtual void MatrixDirect(const MAT2D& mat2D);

    //!< 描画用（画面座標変換用）マトリクス取得
    //const MAT2D* GetDrawingMatrix()const{return  &m_matDraw;}

    //!< 調整
    virtual bool Trim (const POINT2D& ptIntersect, const POINT2D& ptClick, bool bReverse);

    //!< 分割
    virtual std::shared_ptr<CDrawingObject>  Break (const POINT2D& ptBreak);

    //!< 範囲
    virtual bool IsInner( const RECT2D& rcArea, bool bPart) const;

    //!< 描画
    virtual void Draw(CDrawingView* pView, 
                      const std::shared_ptr<CDrawingObject> pParentObject,
                      E_MOUSE_OVER_TYPE eMouseOver = E_MO_NO,
                      int iLayerId = -1) override;

    //!< 描画削除
    virtual void DeleteView(CDrawingView* pView, 
                            const std::shared_ptr<CDrawingObject> pParentObject,
                            int iLayerId = -1) override;

    //!< 領域取得
    virtual RECT2D GetBounds() const;

    //!< エレメント設定
    //virtual void SetElementDef(CElementDef* pElementDef);

    //-------------------------
    // リフレクション設定
    //-------------------------
    //!< 位置設定
    void  SetPoint(POINT2D ptAbs);

    //!< 位置取得
    POINT2D  GetPoint()  const  {return m_Pt;}

    //!< 角度設定
    void   SetAngle(double dAngle)        {m_dAngle = dAngle;}

    //!< 角度取得
    double GetAngle() const               {return m_dAngle;}

    //!< 中心角設定
    void   SetCenterAngle(double dAngle)  {m_dCenterAngle = dAngle;}

    //!< 中心角取得
    double GetCenterAngle() const         {return m_dCenterAngle;}

    //!< アイコンインデックス設定
    void SetIconIndex(int iIndex)    {m_iIconIndex = iIndex;}

    //!< アイコンインデックス取得
    int GetIconIndex() const         {return m_iIconIndex;}

    //!< 値設定
    void  SetValue(double dValue)  {m_dValue = dValue;}

    //!< 値取得
    double GetValue() const     {return m_dValue;}

    //!< 単位ID設定
    //void SetUnitId(int iId)     {m_iUnitId = iId;}

    //!< 単位ID取得
    //int GetUnitId() const       {return m_iUnitId;}


    //!< スナップ点取得
    virtual  bool GetSnapList(std::list<SNAP_DATA>* pLstSnap)const;

    //!< プロパティ初期化
    TREE_GRID_ITEM*  _CreateStdPropertyTree();


    //-------------------
    //!< プロパティ変更
    //-------------------
    static bool PropPosition     (CStdPropertyItem* pData, void* pObj);
    static bool PropAngle        (CStdPropertyItem* pData, void* pObj);
    static bool PropCenterAngle  (CStdPropertyItem* pData, void* pObj);
    static bool PropIconIndex    (CStdPropertyItem* pData, void* pObj);
    static bool PropValue        (CStdPropertyItem* pData, void* pObj);
    //static bool PropUnitId       (CStdPropertyItem* pData, void* pObj);
    //-------------------

    //-------------------

    //-------------------------
    // 参照
    //-------------------------

    //!< 参照元の取得
    virtual CDrawingObject* UpdateRef();

protected:
    //!< エレメント描画(マウスオーバー)   
    virtual void DrawOver( CDrawingView* pView, bool bOver);

    //!< ロード終了後処理
    virtual void LoadAfter(CPartsDef* pDef) override;

    //!< 参照元
    std::shared_ptr<CElementDef>  GetElementDef() const;

protected:

    //!< 位置
    POINT2D             m_Pt;

    //!< 角度
    double              m_dAngle;

    //!< 中心角
    double              m_dCenterAngle;

    //!< アイコンインデックス
    int                 m_iIconIndex;

    //!< 値
    double              m_dValue;

    //!< 単位ID
    //int                 m_iUnitId;

    //!< アファイン変換マトリックス
    MAT2D               m_mat2D;

    //-----------------------------
    // 保存不要データ
    //-----------------------------
    //参照元UUID
    //mutable boost::uuids::uuid      m_uuidElemetDef;


private:
    friend class boost::serialization::access;  
    BOOST_SERIALIZATION_SPLIT_MEMBER();

    template<class Archive>
    void load(Archive& ar, const unsigned int version);

    template<class Archive>
    void save(Archive& ar, const unsigned int version) const;
};

#endif // 