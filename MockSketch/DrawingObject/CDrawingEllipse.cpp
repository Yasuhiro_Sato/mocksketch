/**
 * @brief			CDrawingEllipse実装ファイル
 * @file			CDrawingEllipse.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CDrawingPoint.h"
#include "./CDrawingEllipse.h"
#include "./CDrawingCircle.h"
#include "./CDrawingDim.h"
#include "./CDrawingCircle.h"
#include "./CDrawingSpline.h"
#include "./CDrawingLine.h"
#include "./CDrawingCompositionLine.h"
#include "./CDrawingNode.h"
#include "DefinitionObject/View/CDrawingView.h"
#include "DefinitionObject/CPartsDef.h"
#include "Utility/CUtility.h"
#include "View/CUndoAction.h"
#include "SYSTEM/CSystem.h"
#include <math.h>
#include <boost/serialization/export.hpp> 
BOOST_CLASS_EXPORT(CDrawingEllipse);


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
CDrawingEllipse::CDrawingEllipse():
CDrawingObject()
{
    m_eType  = DT_ELLIPSE;
    m_iWidth =    1;
    m_iLineType = PS_SOLID;
    m_iPropFeatures = P_BASE | P_LINE; 
}

/**
 *  @brief  コンストラクター.
 *  @param  [in] nID 
 *  @retval なし 
 *  @note
 */
CDrawingEllipse::CDrawingEllipse(int nID):
CDrawingObject( nID, DT_ELLIPSE)
{
    m_iWidth =    1;
    m_iLineType = PS_SOLID;
    m_iPropFeatures = P_BASE | P_LINE; 
}

/**
 * コピーコンストラクタ
 */
CDrawingEllipse::CDrawingEllipse(const CDrawingEllipse& Obj):
CDrawingObject( Obj)
{
    m_eType  = DT_ELLIPSE;
    m_iWidth    = Obj.m_iWidth;
    m_iLineType = Obj.m_iLineType;
    m_Ellipse   = Obj.m_Ellipse;
    m_Fill      = Obj.m_Fill; 
}

/**
 *  @brief  デストラクタ.
 *  @param  なし 
 *  @retval なし 
 *  @note
 */
CDrawingEllipse::~CDrawingEllipse()
{
}


/**
 *  @brief  回転.
 *  @param  [in]    pPt2D    回転中心(絶対座標)
 *  @param  [in]    dAngle   (絶対)回転角(rad)
 *  @retval     なし
 *  @note
 */
void CDrawingEllipse::Rotate(const POINT2D& pt2D, double dAngle)
{
    AddChgCnt();
    m_Ellipse.Rotate(pt2D, dAngle);
}

/**
 *  @brief  相対移動.
 *  @param  [in]    pPt2D   移動量
 *  @retval         なし
 *  @note
 */
void CDrawingEllipse::Move(const POINT2D& pt2D) 
{
    AddChgCnt();
    m_Ellipse.Move(pt2D);
}

/**
 *  @brief  鏡像.
 *  @param  [in]    line   対称軸
 *  @retval         なし
 *  @note
 */
void CDrawingEllipse::Mirror(const LINE2D& line)
{
    AddChgCnt();
    m_Ellipse.Mirror(line);
}

/**
 *  @brief  行列.
 *  @param  [in]    mat2D   適用する行列
 *  @retval         なし
 *  @note
 */
void CDrawingEllipse::Matrix(const MAT2D& mat2D)
{
    AddChgCnt();
    m_Ellipse.Matrix( mat2D);
}

/**
 *  @brief  倍率.
 *  @param  [in]    pPt2D   拡大中心
 *  @param  [in]    dSclX   X倍率
 *  @param  [in]    dSclY   Y倍率
 *  @retval         なし
 *  @note
 */
void CDrawingEllipse::Scl(const POINT2D& pt2D, double dXScl, double dYScl)
{
    AddChgCnt();
    m_Ellipse.Scl(pt2D, dXScl, dYScl);
}


/**
 *  @brief  交点計算.
 *  @param  [out]   pList   交点のリスト
 *  @param  [in]    pObj    交差するオブジェクト
 *  @retval         なし
 *  @note
 */
void CDrawingEllipse::CalcIntersection ( std::vector<POINT2D>* pList,   
                                const CDrawingObject* pObj, 
                                bool bOnline,
                                double dMin)  const
{
    STD_ASSERT(pList != NULL);

    POINT2D ptTmp;

    pList->clear();
    //レイヤーの確認
    if (!IsSameScale(pObj))
    {
        return;
    }
    const ELLIPSE2D* pEllipse = NULL;
    ELLIPSE2D* pMatEllipse = NULL;

    const MAT2D* pMat =  GetParentMatrix();
    const MAT2D* pMat2 =  pObj->GetParentMatrix();
    if (pMat == NULL)
    {
        pEllipse = &m_Ellipse;
    }
    else
    {
        pMatEllipse = new ELLIPSE2D(m_Ellipse);
        pMatEllipse->Matrix(*pMat);
        pEllipse = pMatEllipse;
    }
    
    
    CalcIntersectionStatic ( pEllipse, pList, pObj, bOnline);


    if (pMatEllipse)
    {
        delete pMatEllipse;
    }

}

void CDrawingEllipse::CalcIntersectionStatic ( const ELLIPSE2D* pEllipse,
                                         std::vector<POINT2D>* pList,   
                                         const CDrawingObject* pObj, 
                                         bool bOnline,
                                         double dMin)
{
    STD_ASSERT(pList != NULL);

    POINT2D ptTmp;

    pList->clear();

    const MAT2D* pMat2 =  pObj->GetParentMatrix();

    try
    {
        switch (pObj->GetType())
        {
        case DT_POINT:
            {
            //近接点を算出
            const CDrawingPoint* ptObj = dynamic_cast<const CDrawingPoint*>(pObj);

            STD_ASSERT(ptObj != NULL);

            const POINT2D* pt2D = ptObj->GetPointInstance();
            if (pMat2 != NULL)
            {
                POINT2D ptMat(*pt2D);
                ptMat.Matrix(*pMat2);
                ptTmp = pEllipse->NearPoint(ptMat, bOnline);
            }
            else
            {
                ptTmp = pEllipse->NearPoint(*pt2D, bOnline);
            }

            if (bOnline)
            {
                if(pEllipse->IsInsideAngle(ptTmp))
                {
                      pList->push_back(ptTmp);
                }
            }
            else
            {
                  pList->push_back(ptTmp);
            }
            }
            break;

        case DT_NODE:
            {
            //近接点を算出
            const CDrawingNode* ptObj = dynamic_cast<const CDrawingNode*>(pObj);

            STD_ASSERT(ptObj != NULL);

            const POINT2D* pt2D = ptObj->GetPointInstance();
            if (pMat2 != NULL)
            {
                POINT2D ptMat(*pt2D);
                ptMat.Matrix(*pMat2);
                ptTmp = pEllipse->NearPoint(ptMat);
            }
            else
            {
                ptTmp = pEllipse->NearPoint(*pt2D, bOnline);
            }

            if (bOnline)
            {
                if(pEllipse->IsInsideAngle(ptTmp))
                {
                      pList->push_back(ptTmp);
                }
            }
            else
            {
                  pList->push_back(ptTmp);
            }
            }
            break;

        case DT_LINE:
            {

            //交点を算出
            const CDrawingLine* pLine = dynamic_cast<const CDrawingLine*>(pObj);
            STD_ASSERT(pLine != NULL);

            if (pMat2 != NULL)
            {
                LINE2D line(*pLine->GetLineInstance());
                line.Matrix(*pMat2);
                pEllipse->Intersect(line, pList, bOnline);

            }
            else
            {
                pEllipse->Intersect(*pLine->GetLineInstance(), pList, bOnline, dMin);
            }

            }
            break;

        case DT_CIRCLE:
            {
            //交点を算出
            const CDrawingCircle* pCircle = dynamic_cast<const CDrawingCircle*>(pObj);
            STD_ASSERT(pCircle != NULL);

            if (pMat2 != NULL)
            {
                ELLIPSE2D* pEllipseIntersection = NULL;
                CIRCLE2D circle(*pCircle->GetCircleInstance());
                circle.Matrix(*pMat2, &pEllipseIntersection);
                pEllipse->Intersect( circle, pList, bOnline, dMin);

                if (pEllipseIntersection)
                {
                    pEllipse->Intersect( *pEllipseIntersection, pList, bOnline, dMin);
                    delete pEllipseIntersection;
                }
                else
                {
                    pEllipse->Intersect( circle, pList, bOnline, dMin);
                }
            }
            else
            {
                pEllipse->Intersect( *pCircle->GetCircleInstance(), pList, bOnline, dMin);
            }
            
            }
            break;

        case DT_SPLINE:
            {
            //交点を算出
            const CDrawingSpline* pSpline = dynamic_cast<const CDrawingSpline*>(pObj);
            STD_ASSERT(pSpline != NULL);
            if (pMat2 != NULL)
            {
                SPLINE2D spline(*pSpline->GetSplineInstance());
                spline.Matrix(*pMat2);
                pEllipse->Intersect( spline, pList, bOnline, dMin);
            }
            else
            {
                pEllipse->Intersect( *pSpline->GetSplineInstance(), pList, bOnline, dMin);
            }
            }
            break;

        case DT_ELLIPSE:
            {
            //交点を算出
            const CDrawingEllipse* pEllipse2 = dynamic_cast<const CDrawingEllipse*>(pObj);
            STD_ASSERT(pEllipse != NULL);
            if (pMat2 != NULL)
            {
                ELLIPSE2D ellipse(*pEllipse2->GetEllipseInstance());
                ellipse.Matrix(*pMat2);
                pEllipse->Intersect( ellipse, pList, bOnline, dMin);
            }
            else
            {
                pEllipse->Intersect( *pEllipse2->GetEllipseInstance(), pList, bOnline, dMin);
            }
            }
            break;

        case DT_COMPOSITION_LINE:
        {
            //交点を算出
            const CDrawingCompositionLine* pCom = dynamic_cast<const CDrawingCompositionLine*>(pObj);
            CDrawingEllipse elp;
            *elp.GetEllipseInstance() =  *pEllipse;

            STD_ASSERT(pCom != NULL);
            if (pMat2 == NULL)
            {
                pCom->CalcIntersection(pList, &elp, bOnline, dMin);
            }
            else
            {
                CDrawingCompositionLine com(*pCom);
                com.Matrix(*pMat2);
                com.CalcIntersection(pList, &elp, bOnline, dMin);
            }
        }
        break;


        default:
            break;
        }
    }
    catch(...)
    {
        STD_DBG(_T("Conv Error %d"), pObj->GetType());
    }
}

/**
 *  @brief  位置調整.
 *  @param  [in]    pPtIntersect   調整位置
 *  @param  [in]    pPtClick       調整方向指定点
 *  @retval         なし
 *  @note
 */
bool CDrawingEllipse::Trim (const POINT2D& ptIntersect, const POINT2D& ptClick, bool bReverse)
{
    AddChgCnt();
    m_Ellipse.Trim(ptIntersect, ptClick, bReverse);
    return false;
}

/**
 *  @brief  位置調整.
 *  @param  [in]    pPtIntersect   調整位置
 *  @param  [in]    pPtClick       調整方向指定点
 *  @retval         なし
 *  @note
 */
bool CDrawingEllipse::TrimCorner (const POINT2D& ptIntersect, const POINT2D& ptClick)
{
    AddChgCnt();
    m_Ellipse.TrimCorner(ptIntersect, ptClick);
    return false;
}

/**
 *  @brief  分割.
 *  @param  [in]    pPtBreak  分割位置
 *  @retval         分割によって出来た
 *  @note
 */
std::shared_ptr<CDrawingObject>  CDrawingEllipse::Break (const POINT2D& ptBreak)
{
    auto ellipseBreak = std::make_shared<CDrawingEllipse>(*this);

    ELLIPSE2D ellipse2D;
    m_Ellipse.Break(ptBreak, &ellipse2D);

    ellipseBreak->m_Ellipse = ellipse2D;

    //選択を解除しておく
    ellipseBreak->SetSelect(false);
    AddChgCnt();

    return ellipseBreak;
}


/**
 *  @brief  描画.
 *  @param  [in]  pView       表示View
 *  @param  [in]  bMouseOver  true オブジェクト上にマウスあり
 *  @retval         なし
 *  @note
 */
void CDrawingEllipse::Draw(CDrawingView* pView, 
                            const std::shared_ptr<CDrawingObject> pParentObject,
                            E_MOUSE_OVER_TYPE eMouseOver,
                            int iLayerId)
{
    COLORREF crObj;
    int      iID;
    bool bIgnoreSelect = false;
    if (eMouseOver == E_MO_IGNORE_SELECT)
    {
        bIgnoreSelect = true;
    }

    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }


    crObj = GetDrawColor(pView, iLayerId, pParentObject.get(), &iID, bIgnoreSelect);

    Ellipse( pView, iLayerId, pParentObject.get(), crObj, iID, eMouseOver);
}


/**
 *  @brief  描画.
 *  @param  [in]    pView      表示View
 *  @retval         なし
 *  @note
 */
void CDrawingEllipse::Ellipse(CDrawingView* pView, 
                             int iLayerId,
                             const CDrawingObject* pParentObject,
                             COLORREF crPen, int iId,
                             E_MOUSE_OVER_TYPE eMouseOver) const
{

    CPartsDef* pCtrl = pView->GetPartsDef();
    CLayer* pLayer   = pCtrl->GetLayer(iLayerId);

    //--------------------
    //  MATRIX
    //--------------------
    /*
    const MAT2D* pMat =  GetParentMatrix();
    ELLIPSE2D tmpEllipse(m_Ellipse);
    if (pMat != NULL)
    {
        tmpEllipse.Matrix(*pMat);
    }
    //--------------------

    if (m_pMatOffset)
    {
        tmpEllipse.Matrix(*m_pMatOffset);
    }
    */

    MAT2D matDraw;
    //m_Ellipse.GetMatrix(&matDraw);

#if 0
{
     double dSclX;
    double dSclY; 
    double dAngle; 
    double dT;
    POINT2D pt2D;
   
DB_PRINT(_T("Ep1 (%f,%f) a:%f sx:%f sy:%f \n"), m_Ellipse.GetCenter().dX, 
                                                m_Ellipse.GetCenter().dY,
                                                m_Ellipse.GetAngle(),
                                                m_Ellipse.GetRadius1(),
                                                m_Ellipse.GetRadius2());   
    
    
}
#endif    


    if (pParentObject )
    {
        const MAT2D* pMat = pParentObject->GetDrawingMatrix();
        if (pMat)
        {
            matDraw = *pMat;
        }
    }


    if (m_pMatOffset)
    {
        matDraw = matDraw * (*m_pMatOffset);
    }

    ELLIPSE2D tmpEllipse(m_Ellipse);
    tmpEllipse.Matrix(matDraw);

#if 0
{
     double dSclX;
    double dSclY; 
    double dAngle; 
    double dT;
    POINT2D pt2D;
    DB_PRINT(_T("Ep2 (%f,%f) a:%f sx:%f sy:%f \n"), tmpEllipse.GetCenter().dX, 
                                                    tmpEllipse.GetCenter().dY,
                                                    tmpEllipse.GetAngle(),
                                                    tmpEllipse.GetRadius1(),
                                                    tmpEllipse.GetRadius2());   
#endif

    std::vector<POINT2D> lstPoint;

	RECT2D viewRect;
	viewRect = pView->GetViewArea(iLayerId);
	double dDotLength = pView->DotToLength(1);

	tmpEllipse.DivideLine(&lstPoint, viewRect, dDotLength);

    bool bAddLine = false;
    if (m_Fill.eFillType !=  FIT_NONE )
    {
        if (!m_Ellipse.IsClose())
        {
            lstPoint.push_back(GetCenter());
            lstPoint.push_back(lstPoint[0]);
            bAddLine = true;
        }

        COLORREF crFill;
        if (pLayer->bUseLayerColor)
        {
            crFill = pLayer->crLayer;
        }
        else
        {
            crFill = m_Fill.cr1;
        }

        pView->BeginPath();

        pView->SetPen(PS_SOLID,  1, crPen, iId);

        LineMulti( pView, iLayerId, &lstPoint);

        pView->CloseFigure();
        pView->EndPath();

        if (m_Fill.eFillType == FIT_SOLID)
        {
            pView->FillPath(crFill, m_nId);
        }
        else if (m_Fill.eFillType == FIT_HATCHING)
        {
            pView->HatchingPath(m_Fill.cr1, crPen, m_nId);
        }

        POINT   ptViewStart;
        pView->ConvWorld2Scr(&ptViewStart, lstPoint[0], iLayerId);
        pView->MoveTo(ptViewStart);
    }

    if (bAddLine)
    {
        //lstPoint.pop_back();
        //lstPoint.pop_back();
    }


    int iWidth = m_iWidth;
    if (eMouseOver == E_MO_SELECTABLE)
    {
        pView->SetLineAlternateMode(true);
        iWidth += 2;
    }
    else if (eMouseOver == E_MO_EMPHASIS)
    {
        crPen = DRAW_CONFIG->crEmphasis;
    }
	else if (eMouseOver == E_MO_CONNECTABLE)
	{
		crPen = DRAW_CONFIG->crConnectable;
	}

    pView->SetPen(m_iLineType,  iWidth, crPen, iId);
    LineMulti( pView, iLayerId, &lstPoint);

    if (eMouseOver == E_MO_SELECTABLE)
    {
        pView->SetLineAlternateMode(false);
    }

}



/**
 *  @brief  描画削除.
 *  @param  [in]    pView   表示View
 *  @retval         なし
 *  @note
 */
void CDrawingEllipse::DeleteView(CDrawingView* pView,
                                 const std::shared_ptr<CDrawingObject> pParentObject,
                                 int iLayerId)
{
    //背景色
    COLORREF crObj;
    crObj = DRAW_CONFIG->GetBackColor();
    std::vector<POINT2D> lstPoint;

    //--------------------
    //  MATRIX
    //--------------------
    const MAT2D* pMat =  NULL;
    if (pParentObject )
    {
        pMat = pParentObject->GetDrawingMatrix();
    }

    ELLIPSE2D tmpEllipse(m_Ellipse);
    if (pMat != NULL)
    {
        tmpEllipse.Matrix(*pMat);
    }
    //--------------------

    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }

    RECT2D viewRect = pView->GetViewArea(iLayerId);
    double dDotLength = pView->DotToLength(1);
    tmpEllipse.DivideLine(&lstPoint, viewRect, dDotLength);

    pView->SetPen(m_iLineType,  m_iWidth, crObj, 0);

    LineMulti( pView, iLayerId, &lstPoint);

}

/**
 *  @brief   描画(マウスオーバー時).
 *  @param   [in] pView 表示View
 *  @param   [in] bOver true:開始 false:終了
 *  @retval  なし
 *  @note
 */
void CDrawingEllipse::DrawOver( CDrawingView* pView, bool bOver)
{
}

//!< 範囲
CDrawingObject::RELATION CDrawingEllipse::RelationRect(const RECT2D& rcArea) const
{
    std::vector<POINT2D> lstPoint;

    LINE2D line1(rcArea.dLeft, rcArea.dTop, rcArea.dLeft, rcArea.dBottom);
    LINE2D line2(rcArea.dRight, rcArea.dTop, rcArea.dRight, rcArea.dBottom);
    LINE2D line3(rcArea.dLeft, rcArea.dTop, rcArea.dRight, rcArea.dTop);
    LINE2D line4(rcArea.dLeft, rcArea.dBottom, rcArea.dRight, rcArea.dBottom);

    //--------------------
    //  MATRIX
    //--------------------
    const MAT2D* pMat = GetParentMatrix();
    ELLIPSE2D tmpEllipse(m_Ellipse);
    if (pMat != NULL)
    {
        tmpEllipse.Matrix(*pMat);
    }
    //--------------------

    if (tmpEllipse.Intersect(line1, &lstPoint, true)) { return REL_PART; }
    if (tmpEllipse.Intersect(line2, &lstPoint, true)) { return REL_PART; }
    if (tmpEllipse.Intersect(line3, &lstPoint, true)) { return REL_PART; }
    if (tmpEllipse.Intersect(line4, &lstPoint, true)) { return REL_PART; }

    RECT2D rcEllipse = tmpEllipse.GetBounds();

    if (rcArea.IsInside(rcEllipse))
    {
        return REL_INNER;
    }
    return REL_NONE;
}

/**
 *  @brief  範囲.
 *  @param  [in] p1 1点目
 *  @param  [in] p2 2点目
 *  @retval  true:2点で示される矩形内に点が存在する 
 *  @note
 */
bool CDrawingEllipse::IsInner( const RECT2D& rcArea, bool bPart) const
{

    auto ret = RelationRect(rcArea);

    if (bPart)
    {
        if ((ret & REL_PART) == REL_PART) { return true; }
    }
    if ((ret & REL_INNER) == REL_INNER) { return true; }

    return false;
}


/**
 *  @brief  線幅設定.
 *  @param  [in]    iWidth   線幅
 *  @retval         なし
 *  @note
 */
void CDrawingEllipse::SetLineWidth(int iWidth)
{
    m_iWidth = iWidth;
}

/**
 *  @brief  線幅取得.
 *  @param          なし
 *  @retval         線幅
 *  @note
 */
int CDrawingEllipse::GetLineWidth() const
{
    return m_iWidth;
}

/**
 *  @brief  線種設定.
 *  @param  [in]    iType   線種
 *  @retval         なし
 *  @note
 */
void CDrawingEllipse::SetLineType(int iType)
{
    m_iLineType = iType;
}

/**
 *  @brief  線種取得.
 *  @param          なし 
 *  @retval         線種
 *  @note
 */
int  CDrawingEllipse::GetLineType()  const
{
    return m_iLineType;
}


/**
 *  @brief   プロパティ初期化.
 *  @param   [in] pCtrl オブジェクトコントロールへのポインタ
 *  @retval     なし
 *  @note
 */
TREE_GRID_ITEM*  CDrawingEllipse::_CreateStdPropertyTree()
{
    NAME_TREE_ITEM<CStdPropertyItem>* pBeforeGroup;
    NAME_TREE_ITEM<CStdPropertyItem>* pNextItem;
    NAME_TREE_ITEM<CStdPropertyItem>* pGroupItem;
    NAME_TREE<CStdPropertyItem>* pTree;
    //int iId;
    CStdPropertyItem* pItem;
    boost::tuple< CStdPropertyItem*, void*> pairVal;


    pBeforeGroup = CDrawingObject::_CreateStdPropertyTree();
    pTree = m_psProperty->GetTreeInstance();

    pGroupItem  = pTree->AddNext(pBeforeGroup, NULL, GET_STR(STR_PRO_LINE), _T("Line"));

#ifdef _DEBUG_Clockwise
    DB_PRINT(_T(" Clockwise %s \n"), (m_Ellipse.IsClockwise()? _T("true"): _T("false"))); 
#endif

    //------------
    //中心点
    //------------
    CStdPropertyItemDef DefCenter(PROP_POINT2D, 
        GET_STR(STR_PRO_CENTER)   , 
        _T("Center"),
        GET_STR(STR_PRO_INFO_CENTER), 
        true, 
        DISP_UNIT,
        POINT2D());

    pItem = new CStdPropertyItem( DefCenter, PropCenter, m_psProperty, NULL, &m_Ellipse.m_ptCenter);
    pNextItem = pTree->AddChild(pGroupItem, pItem, DefCenter.strDspName, pItem->GetName(), 2);

    //------------
    //長径
    //------------
    CStdPropertyItemDef DefRad1(PROP_DOUBLE, 
        GET_STR(STR_PRO_RAD1)   , 
        _T("Rad1"),
        GET_STR(STR_PRO_INFO_RAD1), 
        true,
        DISP_UNIT,
        double(0.0));

    pItem = new CStdPropertyItem( DefRad1, PropMajorRad, m_psProperty, NULL, &m_Ellipse.m_dRad1);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefRad1.strDspName, pItem->GetName());

    //------------
    //短径
    //------------
    CStdPropertyItemDef DefRad2(PROP_DOUBLE, 
        GET_STR(STR_PRO_RAD2)   ,
        _T("Rad2"),
        GET_STR(STR_PRO_INFO_RAD2), 
        true,
        DISP_UNIT,
        double(0.0));

    pItem = new CStdPropertyItem( DefRad2, PropMinorRad, m_psProperty, NULL, &m_Ellipse.m_dRad2);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefRad2.strDspName, pItem->GetName());

    //------------
    //角度(deg)
    //------------
    CStdPropertyItemDef DefAngle(PROP_DOUBLE, 
        GET_STR(STR_PRO_ANGLE)   ,
        _T("Angle"),
        GET_STR(STR_PRO_INFO_ANGLE), 
        true,
        DISP_ANGLE,
        double(0.0));

    pItem = new CStdPropertyItem( DefAngle, PropAngle, m_psProperty, NULL, &m_Ellipse.m_dAngle);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefAngle.strDspName, pItem->GetName());

    //------------
    //傾斜
    //------------
    CStdPropertyItemDef DefSkew(PROP_DOUBLE, 
        GET_STR(STR_PRO_SKEW)   ,
        _T("Skew"),
        GET_STR(STR_PRO_INFO_SKEW), 
        true,
        DISP_NONE,
        double(0.0));

    pItem = new CStdPropertyItem( DefSkew, PropSkew, m_psProperty, NULL, &m_Ellipse.m_dSkew);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefSkew.strDspName, pItem->GetName());

    //------------
    //開始角(deg)
    //------------
    CStdPropertyItemDef DefStart(PROP_DOUBLE, 
        GET_STR(STR_PRO_START_ANGLE)   ,
        _T("Start"),
        GET_STR(STR_PRO_INFO_START_ANGLE), 
        true,
        DISP_ANGLE,
        double(0.0));

    pItem = new CStdPropertyItem( DefStart, PropStart, m_psProperty, NULL, &m_Ellipse.m_dStart);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefStart.strDspName, pItem->GetName());

    //------------
    //終了角(deg)
    //------------
    CStdPropertyItemDef DefEnd(PROP_DOUBLE, 
        GET_STR(STR_PRO_END_ANGLE)   ,
        _T("End"),
        GET_STR(STR_PRO_INFO_END_ANGLE), 
        true,
        DISP_ANGLE,
        double(0.0));

    pItem = new CStdPropertyItem( DefEnd, PropEnd, m_psProperty, NULL, &m_Ellipse.m_dEnd);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefEnd.strDspName, pItem->GetName());

    //------------
    //線幅
    //------------
    CStdPropertyItemDef DefLineWidth(PROP_LINE_WIDTH, 
        GET_STR(STR_PRO_LINE_WIDTH)   ,
        _T("Width"),
        GET_STR(STR_PRO_INFO_LINE_WIDTH), 
        true,
        DISP_NONE,
        0);

    pItem = new CStdPropertyItem( DefLineWidth, PropLineWidth, m_psProperty, NULL, &m_iWidth);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefLineWidth.strDspName, pItem->GetName());

    //------------
    //線種
    //------------
    CStdPropertyItemDef DefLineType(PROP_LINE_TYPE, 
        GET_STR(STR_PRO_LINE_TYPE)   ,
        _T("Type"),
        GET_STR(STR_PRO_INFO_LINE_TYPE), 
        false,
        DISP_NONE,
        0);

    pItem = new CStdPropertyItem( DefLineType, PropLineType, m_psProperty, NULL, &m_iLineType);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefLineType.strDspName, pItem->GetName());


    //--------------------
    // 塗りつぶし
    //--------------------
    pNextItem = m_Fill.CreateStdPropertyTree(m_psProperty, pTree, pNextItem);


    return pGroupItem;
}

//!< プロパティ値更新
void CDrawingEllipse::_PrepareUpdatePropertyGrid()
{
    CDrawingObject::_PrepareUpdatePropertyGrid();
}

/**
 *  @brief   プロパティ変更(中心点)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingEllipse::PropCenter   (CStdPropertyItem* pData, void* pObj)
{
    CDrawingEllipse* pDrawing = reinterpret_cast<CDrawingEllipse*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->m_pCtrl->HideObject(pDrawing->m_nId);
        pDrawing->SetCenter(pData->anyData.GetPoint());
        pDrawing->m_pCtrl->Draw(pDrawing->m_nId);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(長径)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingEllipse::PropMajorRad (CStdPropertyItem* pData, void* pObj)
{
    CDrawingEllipse* pDrawing = reinterpret_cast<CDrawingEllipse*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->m_pCtrl->HideObject(pDrawing->m_nId);
        pDrawing->SetRadius1(pData->anyData.GetDouble());
        pDrawing->m_pCtrl->Draw(pDrawing->m_nId);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(短径)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingEllipse::PropMinorRad (CStdPropertyItem* pData, void* pObj)
{
    CDrawingEllipse* pDrawing = reinterpret_cast<CDrawingEllipse*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->m_pCtrl->HideObject(pDrawing->m_nId);
        pDrawing->SetRadius2(pData->anyData.GetDouble());
        pDrawing->m_pCtrl->Draw(pDrawing->m_nId);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(角度)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingEllipse::PropAngle    (CStdPropertyItem* pData, void* pObj)
{
    CDrawingEllipse* pDrawing = reinterpret_cast<CDrawingEllipse*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->m_pCtrl->HideObject(pDrawing->m_nId);
        pDrawing->SetAngle(pData->anyData.GetDouble());
        pDrawing->m_pCtrl->Draw(pDrawing->m_nId);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(傾斜)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingEllipse::PropSkew    (CStdPropertyItem* pData, void* pObj)
{
    CDrawingEllipse* pDrawing = reinterpret_cast<CDrawingEllipse*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->m_pCtrl->HideObject(pDrawing->m_nId);
        pDrawing->GetEllipseInstance()->SetSkew(pData->anyData.GetDouble());
        pDrawing->m_pCtrl->Draw(pDrawing->m_nId);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(開始角)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingEllipse::PropStart    (CStdPropertyItem* pData, void* pObj)
{
    CDrawingEllipse* pDrawing = reinterpret_cast<CDrawingEllipse*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->m_pCtrl->HideObject(pDrawing->m_nId);
        pDrawing->SetStart(pData->anyData.GetDouble());
        pDrawing->m_pCtrl->Draw(pDrawing->m_nId);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(開始角)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingEllipse::PropEnd      (CStdPropertyItem* pData, void* pObj)
{
    CDrawingEllipse* pDrawing = reinterpret_cast<CDrawingEllipse*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->m_pCtrl->HideObject(pDrawing->m_nId);
        pDrawing->SetEnd(pData->anyData.GetDouble());
        pDrawing->m_pCtrl->Draw(pDrawing->m_nId);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(線幅)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingEllipse::PropLineWidth    (CStdPropertyItem* pData, void* pObj)
{
    CDrawingEllipse* pDrawing = reinterpret_cast<CDrawingEllipse*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->m_pCtrl->HideObject(pDrawing->m_nId);
        pDrawing->SetLineWidth(pData->anyData.GetInt());
        pDrawing->m_pCtrl->Draw(pDrawing->m_nId);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(線種)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingEllipse::PropLineType (CStdPropertyItem* pData, void* pObj)
{
    CDrawingEllipse* pDrawing = reinterpret_cast<CDrawingEllipse*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->m_pCtrl->HideObject(pDrawing->m_nId);
        pDrawing->SetLineType(pData->anyData.GetInt());
        pDrawing->m_pCtrl->Draw(pDrawing->m_nId);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 * @brief   端点1取得
 * @param   なし
 * @retval  端点1
 * @note	
 */
POINT2D CDrawingEllipse::GetPoint1() const
{
    POINT2D ptRet;
    ptRet = m_Ellipse.m_ptStart;
    return ptRet;
}

/**
 * @brief   端点2取得
 * @param   なし
 * @retval  端点2
 * @note	
 */
POINT2D CDrawingEllipse::GetPoint2() const
{
    POINT2D ptRet;
    ptRet = m_Ellipse.m_ptEnd;
    return ptRet;
}

/**
 *  @brief   参照データに基づいて更新.
 *  @param   
 *  @retval  
 *  @note
 */
CDrawingObject* CDrawingEllipse::UpdateRef()
{
    CDrawingObject*      pRef;

    pRef = CDrawingObject::UpdateRef();

    if (pRef == NULL)
    {
        return NULL;
    }

    CDrawingEllipse* pEllipse = dynamic_cast<CDrawingEllipse*>(pRef);

    STD_ASSERT(pEllipse != NULL);

    if (pEllipse == NULL)
    {
        return false;
    }


    m_Ellipse   = pEllipse->m_Ellipse;
    m_iWidth    = pEllipse->m_iWidth;
    m_iLineType = pEllipse->m_iLineType;

    m_iChgCnt = pRef->GetChgCnt();
    return pRef;
}

/**
 *  @brief   スナップ点取得.
 *  @param   [out] pLstSnap スナップ点リスト
 *  @retval  true: スナップ点あり
 *  @note
 */
bool CDrawingEllipse::GetSnapList(std::list<SNAP_DATA>* pLstSnap)const 
{
    using namespace VIEW_COMMON;

    DWORD dwSnap = DRAW_CONFIG->GetSnapType();
    bool bRet = false;

    STD_ASSERT(pLstSnap);

    if (!pLstSnap)
    {
        return false;
    }

    pLstSnap->clear();
    SNAP_DATA snapData;

    if (dwSnap & SNP_CENTER)
    {
        snapData.eSnapType = SNP_CENTER;
        snapData.pt = GetCenter();
        if (dwSnap & SNP_ELLIPSE)
        {
            snapData.bKeepSelect = true;
        }
        else
        {
            snapData.bKeepSelect = false;
        }
        pLstSnap->push_back(snapData);
        bRet = true;
    }

    if ( (dwSnap & SNP_END_POINT) &&
          (!m_Ellipse.IsClose()))
    {
        snapData.eSnapType = SNP_END_POINT;
        snapData.pt = GetPoint1();
        snapData.bKeepSelect = false;
        pLstSnap->push_back(snapData);

        snapData.eSnapType = SNP_END_POINT;
        snapData.pt = GetPoint2();
        pLstSnap->push_back(snapData);
        bRet = true;
    }

    return bRet;
}

/**
 *  @brief   始点終点交換.
 *  @param   なし
 *  @retval  なし
 *  @note    Line,Circle,Splie,CompositLine
 */
bool  CDrawingEllipse::SwapStartEnd()
{
    m_Ellipse.SwapStartEnd();
    return true;
}

/**
 *  @brief   始点取得.
 *  @param   なし
 *  @retval  始点
 *  @note    Line,Circle,Splie,CompositLine
 */
POINT2D  CDrawingEllipse::GetStartSide() const
{
    if(m_Ellipse.IsClockwise())
    {
        return GetPoint2();
    }
    else
    {
        return GetPoint1();
    }
}

/**
 *  @brief   終点取得.
 *  @param   なし
 *  @retval  始点
 *  @note    Line,Circle,Splie,CompositLine
 */
POINT2D  CDrawingEllipse::GetEndSide() const
{
    if(m_Ellipse.IsClockwise())
    {
        return GetPoint1();
    }
    else
    {
        return GetPoint2();
    }
}


/**
*  @brief   中点取得.
*  @param   なし
*  @retval  中点
*  @note    Line,Circle,Splie,CompositLine
*/
POINT2D  CDrawingEllipse::GetMidPoint() const
{
    return GetPointByRate(0.5);
}

POINT2D  CDrawingEllipse::GetPointByRate(double d) const
{
    double dAngle;
    double dStartOrg = m_Ellipse.GetStart();
    double dEndOrg   = m_Ellipse.GetEnd();
    double dS = dStartOrg;
    double dE = dEndOrg;

    if(fabs( dStartOrg - dEndOrg) < NEAR_ZERO)
    {
        dAngle =  dStartOrg + 180.0;
    }
    else
    {
        if(m_Ellipse.IsClockwise())
        {
            if(dEndOrg > dStartOrg)
            {
                dS = dStartOrg + 180.0;    
            }
        }
        else
        {
            if(dEndOrg < dStartOrg)
            {
                dE = dEndOrg + 180.0;    
            }
        }
        dAngle = dS * (1.0 - d) + dE * d;
    }
    return m_Ellipse.GetPoint(dAngle);
}

//!<  近接点取得(Line,Circle,Splie,CompositLine用)
bool  CDrawingEllipse::GetNearPoints(POINT2D*  ptNear, const POINT2D& pt, bool bOnline) const
{
    POINT2D ptRet = m_Ellipse.NearPoint(pt, bOnline);
    *ptNear = ptRet; 
    return true;
}


//!<  法線角取得(Line,Circle,Splie,CompositLine用)
bool  CDrawingEllipse::GetNormAngle(double* pAngle, const POINT2D& pt, bool bOnline) const
{
    POINT2D ptNear = m_Ellipse.NearPoint(pt, bOnline);
    double dCenterAngle =  m_Ellipse.GetCenterAngle(ptNear);
    LINE2D lTangent = m_Ellipse.TangentLine(dCenterAngle);

    *pAngle = lTangent.Angle();
    return true;
}

double CDrawingEllipse::Length(const POINT2D& ptIntersect, bool bStart) const
{
    POINT2D pt = m_Ellipse.NearPoint(ptIntersect, true);

    double dLen =  m_Ellipse.ArcLength(pt, bStart);
    return dLen;
}

//!< オフセット(線,円弧,スプライン上)
bool CDrawingEllipse::Offset(double dLen, POINT2D ptDir)
{
    m_Ellipse = m_Ellipse.Offset(dLen, ptDir);
    return true;
}

//!< オフセット(線,円弧,スプライン上)
bool CDrawingEllipse::Offset(double dLen)
{
    //進行方向右側を正とする
    POINT2D ptDir;

    ptDir = m_Ellipse.GetCenter();
    ptDir.dX +=  m_Ellipse.GetRadius1();
    if(m_Ellipse.IsClockwise())
    {
        ptDir.dX += dLen; 
    }
    else
    {
        ptDir.dX -= dLen; 
    }
    ptDir.Rotate(m_Ellipse.GetCenter(), m_Ellipse.GetAngle());

    m_Ellipse = m_Ellipse.Offset(fabs(dLen), ptDir);
    return true;
}

//!< 点が進行方向の右側にあるか
bool CDrawingEllipse::IsRightSide(POINT2D ptDir) const
{
    double dRet = m_Ellipse.CalcFunction(ptDir);

    bool bRet = false;
    if(dRet >= 0)
    {
        dRet = true;   
    }
 
    if(m_Ellipse.IsClockwise())
    {
        bRet = (!bRet);
    }
    return bRet;
}

/**
 * @brief   領域取得
 * @param   なし
 * @retval  なし
 * @note	描画領域を取得する
 */
RECT2D CDrawingEllipse::GetBounds()  const
{
    return m_Ellipse.GetBounds();
}

/**
 * @brief   距離
 * @param   [in] pt 
 * @retval  近接点からの距離
 * @note
 */
double CDrawingEllipse::Distance(const POINT2D& pt) const
{
    POINT2D ptNear = m_Ellipse.NearPoint( pt, true /*bOnline*/);
    return pt.Distance(ptNear);
}

CDrawingEllipse::E_NODE CDrawingEllipse::GetNode(StdString strMarkerId) const
{
    E_NODE eRet;

    if      (strMarkerId == _T("C")){eRet = ND_C;}
    else if (strMarkerId == _T("A")){eRet = ND_A;}
    else if (strMarkerId == _T("R1")){eRet = ND_R1;}
    else if (strMarkerId == _T("R2")){eRet = ND_R2;}
    else if (strMarkerId == _T("S")){eRet = ND_S;}
    else if (strMarkerId == _T("E")){eRet = ND_E;}
    else {eRet = ND_NONE;}
    return eRet;
}

bool CDrawingEllipse::OnSetCursor(CNodeMarker* pNodeMarker, CDrawingView* pView)
{
    //AfxGetApp()->LoadCursor, AfxGetApp()->LoadStandardCursorを使用すること

	if (pView->IsDrag())
	{
		return false;
	}

    StdString strId = pNodeMarker->GetMouseOver();
    E_NODE eNode = GetNode(strId);


    //右ボタンドラッグ(移動・コピー選択)できるのはセンターのみ
    if (pNodeMarker->GetMouseButton() == SEL_RBUTTON)
    {
        if( eNode != ND_C)
        {
            return false;
        }
    }

    HCURSOR hCursor;
	switch(eNode)
	{
	case ND_R1:
		::SetCursor(AfxGetApp()->LoadCursor(IDC_RADIUS1));
		break;

	case ND_R2:
		::SetCursor(AfxGetApp()->LoadCursor(IDC_RADIUS2));
		break;

	case ND_S:
		::SetCursor(AfxGetApp()->LoadCursor(IDC_START));
		break;

	case ND_E:
		::SetCursor(AfxGetApp()->LoadCursor(IDC_END));
		break;

	case ND_C:
		hCursor =(HCURSOR)LoadImage(NULL, IDC_SIZEALL,IMAGE_CURSOR,
                                    NULL,NULL,LR_DEFAULTCOLOR | LR_SHARED);
		::SetCursor(hCursor);
		break;

	case ND_A:
		::SetCursor(AfxGetApp()->LoadCursor(IDC_CURSOR_ROTATE));
		break;

	default:
		//::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
        //break;
		return false;
	}
	
	return true;

}


//!< マーカ初期化
bool CDrawingEllipse::InitNodeMarker(CNodeMarker* pMarker)
{
    CDrawingView* pView = pMarker->GetView();
    pView->ClearDraggingNodes();
    pView->ClearDraggingTmpObjects();

    auto lstNode = pView->GetDraggingNodes();
    lstNode->resize(ND_MAX);
    for(auto& pNode: *lstNode)
    {
        pNode = std::make_shared<CDrawingNode>();
        pNode->SetVisible(true);
        pNode->SetMarkerShape(DMS_CIRCLE_FILL);
    }

    lstNode->at(ND_C)->SetName(_T("C"));
    lstNode->at(ND_A)->SetName(_T("A"));
    lstNode->at(ND_R1)->SetName(_T("R1"));
    lstNode->at(ND_R2)->SetName(_T("R2"));
    lstNode->at(ND_S)->SetName(_T("S"));
    lstNode->at(ND_E)->SetName(_T("E"));
    lstNode->at(ND_A)->SetMarkerShape(DMS_CIRCLE_FILL_OVER);

    STD_ASSERT(pMarker);

    double dLen = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->GetMarkerSize());  
    int iR =  CUtil::Round(dLen * pView->GetDpu());

    pMarker->SetRadius(iR);
    pMarker->SetObject(shared_from_this());


	//マウスカーソル設定コールバック
    namespace  PLC = std::placeholders;
	pMarker->SetOnSetcursorCallback(std::bind(&CDrawingEllipse::OnSetCursor, this, PLC::_1, PLC::_2));


    //ドラッグ表示用の図形を用意する
    auto lstTmpObject = pView->GetDraggingTmpObjects();
    lstTmpObject->push_back(std::make_shared<CDrawingEllipse>(*this)); //表示用
    auto pEllipseTmp = std::dynamic_pointer_cast<CDrawingEllipse>(lstTmpObject->at(0));
    pEllipseTmp->SetColor(DRAW_CONFIG->crImaginary);
    
    //角度設定用マーカー位置
    POINT2D ptAngle;
    {
        double dRotOffset = DRAW_CONFIG->dRotateNodeOffset;

        double dDspScl;
        dDspScl = pView->GetViewScale();
        dRotOffset = (dRotOffset / dDspScl) + GetRadius2();

        ptAngle = GetCenter();

        double dS, dC;
        CUtil::GetTriFunc(GetAngle() + 90.0, &dS, &dC);
        ptAngle.dX += dRotOffset * dC;
        ptAngle.dY += dRotOffset * dS;
    }


    //　開始角　終了角
    lstTmpObject->push_back(std::make_shared<CDrawingEllipse>(*this)); //表示用
    auto pEllipseOffest = std::dynamic_pointer_cast<CDrawingEllipse>(lstTmpObject->at(1));


    pEllipseOffest->SetRadius1(GetRadius1() * 1.1);
    pEllipseOffest->SetRadius2(GetRadius2() * 1.1);

    //補助線

	lstNode->at(ND_C)->SetPoint(GetCenter());
	lstNode->at(ND_A)->SetPoint(ptAngle);
	lstNode->at(ND_R1)->SetPoint(m_Ellipse.GetPoint( 0.0));
	lstNode->at(ND_R2)->SetPoint(m_Ellipse.GetPoint(90.0));
	lstNode->at(ND_S)->SetPoint(pEllipseOffest->GetStartSide());
	lstNode->at(ND_E)->SetPoint(pEllipseOffest->GetEndSide());


    for(auto& pNode: *lstNode)
    {
        pMarker->Create(pNode.get(), pNode->GetPoint());
    }

    //補助線
    lstTmpObject->push_back(std::make_shared<CDrawingLine>());
    auto pLine = std::dynamic_pointer_cast<CDrawingLine>(lstTmpObject->at(2));

    pLine->SetColor(DRAW_CONFIG->crAdditional);
    pLine->SetLineType(PS_DOT);

    lstTmpObject->push_back(std::make_shared<CDrawingEllipse>(*this)); //楕円弧の欠けた部分を描画
    auto pEllipseMissing = std::dynamic_pointer_cast<CDrawingEllipse>(lstTmpObject->at(3));
    pEllipseMissing->SetColor(DRAW_CONFIG->crAdditional);
    pEllipseMissing->SetLineType(PS_DOT);
    pEllipseMissing->SwapStartEnd();

    CPartsDef*   pDef  = pView->GetPartsDef();
    if (!pEllipseTmp->GetEllipseInstance()->IsClose())
    {
        pDef->SetMouseEmphasis(pEllipseMissing);
    }

    return true;
}

//!< マーカ選択
void CDrawingEllipse::SelectNodeMarker(CNodeMarker* pMarker, 
                                    StdString strMarkerId)
{
    pMarker->SetVisibleOnly( strMarkerId, true);

    E_NODE eNode = GetNode(strMarkerId);
    if ((eNode == ND_R1) ||
        (eNode == ND_R2))
    {
        //補助線2
        CDrawingView* pView = pMarker->GetView();
        auto lstTmpObject = pView->GetDraggingTmpObjects();
        lstTmpObject->push_back(std::make_unique<CDrawingLine>());

        STD_ASSERT(lstTmpObject->size() == 5);

        CDrawingLine* pLine = dynamic_cast<CDrawingLine*>(lstTmpObject->at(4).get());

        pLine->SetColor(DRAW_CONFIG->crAdditional);
        pLine->SetLineType(PS_DOT);
    }
}


//!< マーカ移動
void CDrawingEllipse::MoveNodeMarker(CNodeMarker* pMarker,
                                    SNAP_DATA* pSnap,
                                    StdString strMarkerId,
                                    MOUSE_MOVE_POS posMouse)
{
	using namespace VIEW_COMMON;

    //pMarker->SetVisibleOnly(strMarkerId, true);
    CDrawingView* pView = pMarker->GetView();

    bool bIgnoreSnap = false;
	bool bAngle = true;
	bool bLength = true;


    bool bSnap = true;

    SNAP_DATA snap;
    DWORD dwSnapType = DRAW_CONFIG->GetSnapType();

    DWORD dwSnapMask = ~SNP_ON_LINE;
    dwSnapType   &=  dwSnapMask;

    bSnap = pView->GetSnapPoint(&snap, 
                      dwSnapType,
                      posMouse.ptSel);

    CPartsDef*   pDef  = pView->GetPartsDef();
    auto lstTmpObject = pView->GetDraggingTmpObjects();
    auto pObj1 = pDef->GetObjectById(snap.iObject1);
    auto pObj2 = pDef->GetObjectById(snap.iObject2);

    auto pEllipseTmp = std::dynamic_pointer_cast<CDrawingEllipse>(lstTmpObject->at(0));
    auto pLine = std::dynamic_pointer_cast<CDrawingLine>(lstTmpObject->at(2));

    pEllipseTmp->SetColor(DRAW_CONFIG->crImaginary);
    auto pEllipseMissing = std::dynamic_pointer_cast<CDrawingEllipse>(lstTmpObject->at(3));
    auto pNode = pMarker->GetDrawingNode(strMarkerId);

    if (posMouse.nFlags & MK_SHIFT)
    {
        //スナップ無視
        bIgnoreSnap = true;
		bAngle = false;
        pObj1 = NULL;
		bLength = false;
        pSnap->eSnapType = SNP_NONE;
        pSnap->pt = pSnap->ptOrg;
    }

    E_NODE eNode = GetNode(strMarkerId);

    //右ボタンドラッグ(移動・コピー選択)できるのはセンターのみ
    if (pMarker->GetMouseButton() == SEL_RBUTTON)
    {
        if( eNode != ND_C)
        {
            return;
        }
    }

    if (eNode == ND_C)
    {
		bool bRet = false;
        POINT2D ptNewCenter;
        StdString strToolTip;

		double dAngle;
		double dLength;

        if ((!bSnap) &&  
            (!bIgnoreSnap))
        {
		    bRet =pView->GetLengthSnapPoint(&dAngle,
										    &dLength,
										    &ptNewCenter,  
										    GetCenter(),  
										    pSnap->ptOrg,
										    bAngle,
										    bLength);
        }
        else
        {
            ptNewCenter = pSnap->pt;
            snap.strSnap = GetSnapName(snap.eSnapType, false);
            snap.strSnap += _T("\n");

            POINT2D ptCenter = GetCenter();
            dLength  = ptCenter.Distance(snap.pt);
            dAngle   = atan2(snap.pt.dY-ptCenter.dY,
                             snap.pt.dX-ptCenter.dX) * RAD2DEG;
        }

        
		snap.strSnap += CViewAction::GetSnapText(SNP_ANGLE, 
													dLength,
													dAngle,
													false);

        pLine->SetPoint1(GetCenter());
        pLine->SetPoint2(ptNewCenter);
        pDef->SetMouseEmphasis(pLine);
        pEllipseTmp->SetCenter(ptNewCenter);
        pDef->SetMouseOver(pEllipseTmp);
        pNode->SetPoint(ptNewCenter);

    }
    else if( eNode == ND_A)
    {
		bool bRet = false;
		double dAngle;
		double dLength;
        POINT2D ptSnap;

        if ((!bSnap) &&  
            (!bIgnoreSnap))
        {
		    bRet = pView->GetLengthSnapPoint(&dAngle,
								 		     &dLength,
										     &ptSnap,  
										     GetCenter(),  
										     pSnap->ptOrg,
										     true,
										     false);

        }
        else
        {
            ptSnap = snap.pt;
            snap.strSnap = GetSnapName(snap.eSnapType, false);
            snap.strSnap += _T("\n");

            POINT2D ptCenter = GetCenter();
            dLength  = ptCenter.Distance(snap.pt);
            dAngle   = atan2(snap.pt.dY-ptCenter.dY,
                             snap.pt.dX-ptCenter.dX) * RAD2DEG;
        }


		dAngle = dAngle - 90.0;

	    //角度を表示する
	    StdStringStream   strmAngle;
	    bool bTextAngle = true;
	    StdString strAngle;
	    strAngle = CDrawingObject::GetValText(dAngle, bTextAngle, DRAW_CONFIG->iDimAnglePrecision);
	    strmAngle << GET_STR(STR_ANGLE) << _T(":") << strAngle;
	    snap.strSnap += strmAngle.str();

        pMarker->SetMarkerPos(strMarkerId, ptSnap);

        pLine->SetPoint1(GetCenter());
        pLine->SetPoint2(ptSnap);
        pDef->SetMouseEmphasis(pLine);

        pNode->SetPoint(ptSnap);

        pEllipseTmp->SetAngle(dAngle);
        pDef->SetMouseOver(pEllipseTmp);
    }

    else if(( eNode == ND_R1) ||
            ( eNode == ND_R2))
    {
		bool bRet = false;
		double dLength;
        POINT2D ptSnap;
		double dAngle;
        double dAngleOffset = 0.0;

        StdStringStream   strmLen;
        if (eNode == ND_R2)
        {
            dAngleOffset = 90.0;
            strmLen << GET_STR(STR_PRO_RAD2) << _T(":");
        }
        else
        {
            strmLen << GET_STR(STR_PRO_RAD1) << _T(":");
        }

        dAngle = GetAngle() + dAngleOffset;

        bRet = pView->GetFixAngleSnapPoint(&dLength,
                                    &ptSnap,  
                                    dAngle,
                                    GetCenter(),  
                                    pSnap->pt,
                                    bLength); //bSnap


        //径方向の補助線　
        {
            POINT2D pt1;
            pt1.dX = GetCenter().dX + dLength * 1.1 * cos( dAngle * DEG2RAD);
            pt1.dY = GetCenter().dY + dLength * 1.1 * sin( dAngle * DEG2RAD);
            pLine->SetPoint1( pt1 );

            double dAngle2 = (dAngle + 180.0) * DEG2RAD;

            POINT2D pt2;
            pt2.dX = GetCenter().dX + dLength * 1.1 * cos( dAngle2);
            pt2.dY = GetCenter().dY + dLength * 1.1 * sin( dAngle2);
            pLine->SetPoint2( pt2 );
        }


        //ツールチップ
        if (bSnap)
        {
            snap.strSnap = GetSnapName(snap.eSnapType, false);
            snap.strSnap += _T("\n");
        }

        dLength = fabs(dLength);

        int iDecimalPlace = 3;
        strmLen << CDrawingObject::GetValText(dLength, false, iDecimalPlace);
	    snap.strSnap += strmLen.str();

        auto pLineSub = std::dynamic_pointer_cast<CDrawingLine>(lstTmpObject->at(4));
        pLineSub->SetPoint1(pSnap->pt);
        pLineSub->SetPoint2(ptSnap);


        if (eNode == ND_R1)
        {
            pEllipseTmp->SetRadius1(dLength);
        }
        else
        {
            pEllipseTmp->SetRadius2(dLength);
        }
        pMarker->SetMarkerPos(strMarkerId, ptSnap);

        pDef->SetMouseOver(pEllipseTmp);
        pDef->SetMouseEmphasis(pLine);
        pDef->SetMouseEmphasis(pLineSub);

        pNode->SetPoint(ptSnap);

    }
    


    else if((eNode ==ND_S) ||
            (eNode ==ND_E))
    {       

        auto pEllipseOffest = std::dynamic_pointer_cast<CDrawingEllipse>(lstTmpObject->at(1));

		bool bRet = false;
		double dAngle;
		double dLength;
        POINT2D ptSnap;

        if ((!bSnap) &&  
            (!bIgnoreSnap))
        {
		    bRet = pView->GetLengthSnapPoint(&dAngle,
								 		     &dLength,
										     &ptSnap,  
										     GetCenter(),  
										     pSnap->ptOrg,
										     true,
										     false,
                                             GetAngle());

        }
        else
        {
            ptSnap = snap.pt;
            snap.strSnap = GetSnapName(snap.eSnapType, false);
            snap.strSnap += _T("\n");

            POINT2D ptCenter = GetCenter();
            dLength  = ptCenter.Distance(snap.pt);
            dAngle   = atan2(snap.pt.dY-ptCenter.dY,
                             snap.pt.dX-ptCenter.dX) * RAD2DEG;

            dAngle = CUtil::NormAngle( dAngle - GetAngle());

        }


        StdStringStream   strmAngle;


        //ツールチップ
        if (bSnap)
        {
            snap.strSnap = GetSnapName(snap.eSnapType, false);
            snap.strSnap += _T("\n");
        }

        if (eNode ==ND_S)
        {
            strmAngle << GET_STR(STR_PRO_START_ANGLE) << _T(":");
            pEllipseTmp->SetStart(dAngle);
        }
        else
        {
            strmAngle << GET_STR(STR_PRO_END_ANGLE) << _T(":");
            pEllipseTmp->SetEnd(dAngle);
        }

        int iDecimalPlace = 3;
        strmAngle << CDrawingObject::GetValText(dAngle, true, iDecimalPlace);
	    snap.strSnap += strmAngle.str();

        pLine->SetPoint1(ptSnap);
        pLine->SetPoint2(GetCenter());

        pDef->SetMouseOver(pEllipseTmp);
        pDef->SetMouseEmphasis(pLine);

        pEllipseMissing->SetStart(pEllipseTmp->GetStart());
        pEllipseMissing->SetEnd(pEllipseTmp->GetEnd());

        pNode->SetPoint(ptSnap);

    }

    if (!pEllipseTmp->GetEllipseInstance()->IsClose())
    {
        pDef->SetMouseEmphasis(pEllipseMissing);
    }

    pSnap->strSnap = snap.strSnap;
}


//!< マーカ開放
bool CDrawingEllipse::ReleaseNodeMarker(CNodeMarker* pMarker, StdString strMarkerId, MOUSE_MOVE_POS posMouse)
{
    //右ボタンドラッグ(移動・コピー選択)できるのはセンターのみ
    CDrawingView* pView = pMarker->GetView();

    StdString strId = pMarker->GetSelectId();
    E_NODE eNode = GetNode(strId);
    SEL_MENU eMenu = SEL_MNU_MOVE;
    if ((pMarker->GetMouseButton() == SEL_RBUTTON)&&
        ( eNode == ND_C))
    {
        eMenu = _ShowMoveCopyMenu(pMarker);
    }

    CPartsDef*   pDef  = pView->GetPartsDef();
    CUndoAction*       pUndo;
    pUndo  = pDef->GetUndoAction();

    SetSelect(false);

    auto lstTmpObject = pView->GetDraggingTmpObjects();
    auto pEllipseTmp = std::dynamic_pointer_cast<CDrawingEllipse>(lstTmpObject->at(0));

    bool bUndo = false;
    if (eMenu == SEL_MNU_MOVE)
    {
        pUndo->AddStart(UD_CHG, pDef, this);
        m_Ellipse = *(pEllipseTmp->GetEllipseInstance());
        pUndo->AddEnd(shared_from_this(), true);
        bUndo = true;
    }
    else if (eMenu == SEL_MNU_COPY)
    {

        auto pNewEllipse = std::make_shared<CDrawingEllipse>( *pEllipseTmp );

        pNewEllipse->SetColor(GetColor());
        pNewEllipse->SetLineType(GetLineType());
        pNewEllipse->SetLineWidth(GetLineWidth());
        pNewEllipse->m_Fill = m_Fill;

        pDef->RegisterObject(pNewEllipse, true, false);
        pUndo->Add(UD_ADD, pDef, NULL, pNewEllipse);
        bUndo = true;
    }

    if (bUndo)
    {
        pUndo->Push();
    }

    pView->ClearDraggingNodes();
    pView->ClearDraggingTmpObjects();

    ::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
    pMarker->Clear();

    return true;
}



//!< ノード変更
void CDrawingEllipse::ChangeNode(StdString strMarkerId, E_NODE_CHANGE_TYPE eType)
{
    if (strMarkerId == _T("HEIGHT"))
    {
    }
}


//選択状態でのマウス移動
void CDrawingEllipse::SelectedMouseMove(CDrawingView* pView, MOUSE_MOVE_POS posMouse)
{
    //マーカが表示されていない時はまだ選択状態としない
    int iId  = pView->SearchPos(posMouse.ptSel);

    bool bOnNode;
    bOnNode = ((posMouse.nFlags & MK_ON_MARKER) != 0);

}

//選択状態でのマウス解放
void CDrawingEllipse::SelectedMouseUp  (CDrawingView* pView, MOUSE_MOVE_POS posMouse)
{
}

void CDrawingEllipse::SelectedMouseDown(CDrawingView* pView, MOUSE_MOVE_POS posMouse)
{

}

//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void TEST_CDrawingEllipse()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}
#endif //_DEBUG