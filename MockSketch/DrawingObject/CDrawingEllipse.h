/**
* @brief        CDrawingEllipseヘッダーファイル
* @file	        CDrawingEllipse.h
* @author           Yasuhiro Sato
* @date	        07-2-2009 20:03:09
* 
* @note	
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
* 
* @remarks	        
*			
*
* $
* $
* 
*/
#ifndef __DRAWING_ELLIOSE_H_
#define __DRAWING_ELLIOSE_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DrawingObject/CDrawingObject.h"

#include "DrawingObject/Primitive/ELLIPSE2D.h"
#include "./CFill.h"

/**
 * @class   CDrawingEllipse
 * @brief  
 */
class CDrawingEllipse :public CDrawingObject//:public CRTTIClass<CDrawingEllipse, CDrawingObject, DT_LINE>
{
public:
    enum E_NODE
    {
        ND_NONE = -1,
        ND_C,
        ND_A,
        ND_R1,
        ND_S,
        ND_E,
        ND_R2,
        ND_MAX
    };

public:
    //!< コンストラクタ
    CDrawingEllipse();

    //!< コンストラクタ
    CDrawingEllipse(int nID);

    //!< コピーコンストラクタ
    CDrawingEllipse(const CDrawingEllipse& Obj);

    //!< デストラクタ
    virtual ~CDrawingEllipse();

    //!< 相対移動
    virtual void Move(const POINT2D& pt2D);

    //!< 回転
    virtual void Rotate(const POINT2D& pt2D, double dAngle);

    //!< 鏡像
	virtual void Mirror(const LINE2D& line);

    //!< 倍率
	virtual void Scl(const POINT2D& pt2D, double dXScl, double dYScl);

    //!< 行列適用
    virtual void Matrix(const MAT2D& mat2D);

    //!< 交点計算
    virtual void CalcIntersection ( std::vector<POINT2D>* pLiat,   
        const CDrawingObject* pObj, 
        bool bOnline = true,
        double dMin = NEAR_ZERO)const;

    static void CalcIntersectionStatic ( const ELLIPSE2D* pEllipse,
                                   std::vector<POINT2D>* pLiat,   
        const CDrawingObject* pObj, 
        bool bOnline = true,
        double dMin = NEAR_ZERO);

    //!< 調整
    virtual bool Trim (const POINT2D& ptIntersect, const POINT2D& ptClick, bool bReverse);

    //!< 調整          
    virtual bool TrimCorner (const POINT2D& ptIntersect, const POINT2D& ptClick);

    //!< 分割
    virtual std::shared_ptr<CDrawingObject>  Break (const POINT2D& ptBreak);

    //!< 範囲
    virtual bool IsInner( const RECT2D& rcArea, bool bPart) const;

    //!< 範囲
    virtual RELATION RelationRect(const RECT2D& rcArea) const override;

    //!< 描画
    virtual void Draw(CDrawingView* pView, 
                      const std::shared_ptr<CDrawingObject> pParentObject,
                      E_MOUSE_OVER_TYPE eMouseOver = E_MO_NO,
                      int iLayerId = -1) override;

    //!< 描画削除
    virtual void DeleteView(CDrawingView* pView, 
                            const std::shared_ptr<CDrawingObject> pParentObject,
                            int iLayerId = -1) override;

    //-------------------------
    // 参照
    //-------------------------

    //!< 参照元の取得
    virtual CDrawingObject* UpdateRef();

    //-------------------------
    // リフレクション設定
    //-------------------------
friend CDrawingEllipse;

    void SetData(POINT2D ptCenter, double dRad1, double dRad2, double dAngle)
                {m_Ellipse.SetData(ptCenter, dRad1, dRad2, dAngle);}

    //!< 中心設定 (AS)
    void  SetCenter(POINT2D ptPos)        {m_Ellipse.SetCenter(ptPos);} 

    //!< 位置取得  (AS)
    POINT2D&  GetCenter() const            {return m_Ellipse.GetCenter();}

    //!< 長径設定   (AS)
    void    SetRadius1(double dRad)      {m_Ellipse.SetRadius1(dRad);} 

    //!< 長径取得   (AS)
    double  GetRadius1() const           {return m_Ellipse.GetRadius1();}

    //!< 短径設定   (AS)
    void    SetRadius2(double dRad)      {m_Ellipse.SetRadius2(dRad);} 

    //!< 短径取得    (AS)
    double  GetRadius2() const           {return m_Ellipse.GetRadius2();}

    //!< 角度設定   (AS)
    void   SetAngle(double dAngle)        {m_Ellipse.SetAngle(dAngle);}

    //!< 角度取得   (AS)
    double GetAngle() const               {return m_Ellipse.GetAngle();}

    //!< 開始角設定  (AS)
    void   SetStart(double dAngle)        {m_Ellipse.SetStart(dAngle);}

    //!< 開始角取得  (AS)
    double GetStart() const               {return m_Ellipse.GetStart();}

    //!< 終了角設定  (AS)
    void   SetEnd(double dAngle)          {m_Ellipse.SetEnd(dAngle);} 

    //!< 開始角取得 (AS)
    double GetEnd() const                 {return m_Ellipse.GetEnd();}

    //!< 線幅設定  (AS)
    void SetLineWidth(int iWidth);

    //!< 線幅取得 (AS)
    int GetLineWidth() const;

    //!< 線種設定 (AS)
    void SetLineType(int iType); 

    //!< 線種取得 (AS)
    int  GetLineType() const;

    //!< 円取得    (AS)
    ELLIPSE2D*  GetEllipseInstance()             {return &m_Ellipse;}
    const ELLIPSE2D*  GetEllipseInstance() const       {return &m_Ellipse;}

    //!< プロパティ初期化
    virtual TREE_GRID_ITEM*  _CreateStdPropertyTree();

    virtual void _PrepareUpdatePropertyGrid();

    //!< スナップ点取得
    virtual  bool GetSnapList(std::list<SNAP_DATA>* pLstSnap)const;

    //!< 始点終点交換(Line,Circle,Splie,CompositLine用)
    virtual bool  SwapStartEnd() override;

    //!< 始点取得(Line,Circle,Splie,CompositLine用)
    virtual POINT2D  GetStartSide() const override;

    //!< 終点取得(Line,Circle,Splie,CompositLine用)
    virtual POINT2D  GetEndSide() const override;

    //!<  中点取得(Line,Circle,Splie,CompositLine用)
    virtual POINT2D  GetMidPoint() const override;

    //!<  任意点取得(Line,Circle,Splie,CompositLine用)
    virtual POINT2D  GetPointByRate(double d) const override;

    //!<  近接点取得(Line,Circle,Splie,CompositLine用)
    virtual bool  GetNearPoints(POINT2D*  ptNear, const POINT2D& pt, bool bOnline) const override;

    //!<  法線角取得(Line,Circle,Splie,CompositLine用)
    virtual bool GetNormAngle(double* pAngle, const POINT2D& pt, bool bOnline) const override;

     //!< 長さ(線,円弧,スプライン上)
    virtual double Length(const POINT2D& ptIntersect, bool bStart) const override;

    //!< オフセット(線,円弧,スプライン上)
    virtual bool Offset(double dLen, POINT2D ptDir) override;

    //!< オフセット(線,円弧,スプライン上)
    virtual bool Offset(double dLen) override;

    //!< 点が進行方向の右側にあるか
    virtual bool IsRightSide(POINT2D ptDir) const override;

    //-------------------
    //!< プロパティ変更
    //-------------------
    static bool PropCenter   (CStdPropertyItem* pData, void* pObj);
    static bool PropMajorRad (CStdPropertyItem* pData, void* pObj);
    static bool PropMinorRad (CStdPropertyItem* pData, void* pObj);
    static bool PropAngle    (CStdPropertyItem* pData, void* pObj);
    static bool PropSkew     (CStdPropertyItem* pData, void* pObj);
    static bool PropStart    (CStdPropertyItem* pData, void* pObj);
    static bool PropEnd      (CStdPropertyItem* pData, void* pObj);
    static bool PropLineWidth (CStdPropertyItem* pData, void* pObj);
    static bool PropLineType  (CStdPropertyItem* pData, void* pObj);
    //-------------------

    //!< 領域取得
    virtual RECT2D GetBounds() const;

    //!< 距離
    virtual double Distance(const POINT2D& pt) const;

    //!< 端点1取得
    POINT2D GetPoint1() const;

    //!< 端点2取得
    POINT2D GetPoint2() const;

    CFill<CDrawingEllipse>* GetFillInstance(){return &m_Fill;}

    bool OnSetCursor(CNodeMarker* pNodeMarker, CDrawingView* pView);

protected:

    //!< 楕円描画
    void Ellipse( CDrawingView* pView, 
                  int iLayerId,
                  const CDrawingObject* pParentObject,
                  COLORREF crPen, int iId, E_MOUSE_OVER_TYPE eMouseOver) const;


    //!< 楕円描画(マウスオーバー)   
    virtual void DrawOver( CDrawingView* pView, bool bOver);



    //-------------------
    // ドラッグ用マーカ
    //-------------------
    //!< マーカ初期化 true:マーカあり
    virtual bool InitNodeMarker(CNodeMarker* pMarker) override;

    //!< マーカ選択
    virtual void SelectNodeMarker(CNodeMarker* pMarker, 
                                  StdString strMarkerId) override;

    //!< マーカ移動
    virtual void MoveNodeMarker(CNodeMarker* pMarker,
                                    SNAP_DATA* pSnap,
                                    StdString strMarkerId,
                                    MOUSE_MOVE_POS posMouse) override;

    //!< マーカ開放
    virtual bool ReleaseNodeMarker(CNodeMarker* pMarker, StdString strMarkerId, MOUSE_MOVE_POS posMouse) override;

    //!< ノード変更
    virtual void ChangeNode(StdString strMarkerId, E_NODE_CHANGE_TYPE eType) override;

    CDrawingEllipse::E_NODE CDrawingEllipse::GetNode(StdString strMarkerId) const;

    //-------------------------
    //!< 選択状態でのマウス位置
    //---------------------------
    virtual void SelectedMouseMove(CDrawingView* pView, MOUSE_MOVE_POS posMouse) override;
    virtual void SelectedMouseUp  (CDrawingView* pView, MOUSE_MOVE_POS posMouse) override;
    virtual void SelectedMouseDown(CDrawingView* pView, MOUSE_MOVE_POS posMouse) override;


protected:
    //!< 位置
    ELLIPSE2D   m_Ellipse;

    //!< 線幅
    int         m_iWidth;

    //!< 線種
    int         m_iLineType;

    CFill<CDrawingEllipse> m_Fill;

protected:
    

private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
            ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(CDrawingObject);
            SERIALIZATION_BOTH("Ellipse"  , m_Ellipse);
            SERIALIZATION_BOTH("Width"    , m_iWidth);
            SERIALIZATION_BOTH("LineType" , m_iLineType);
            SERIALIZATION_BOTH("Fill"     , m_Fill);
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }
};

#endif // __DRAWING_ELLIOSE_H_