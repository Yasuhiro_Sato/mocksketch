/**
 * @brief			CDrawingField実装ファイル
 * @file			CDrawingField.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MockSketch.h"

#include "DefinitionObject/CElementDef.h"
#include "./CDrawingField.h"
#include "./CDrawingParts.h"
#include "./CDrawingPoint.h"
#include "./CDrawingLine.h"
#include "./CDrawingCircle.h"
#include "./CDrawingSpline.h"
#include "./CDrawingText.h"
#include "./CDrawingNode.h"
#include "./CDrawingDimL.h"
#include "./CDrawingDimH.h"
#include "./CDrawingDimV.h"
#include "./CDrawingDimA.h"
#include "./CDrawingDimD.h"
#include "./CDrawingDimR.h"
#include "./CDrawingDimC.h"
#include "./CDrawingImage.h"
#include "./CDrawingParts.h"
#include "./CDrawingElement.h"
#include "./CDrawingConnectionLine.h"
#include "./CDrawingReference.h"
#include "./CDrawingGroup.h"

#include "./CDrawingCompositionLine.h"

#include "DefinitionObject/CPartsDef.h"
#include "Utility/CUtility.h"
#include "System/CSystem.h"
#include "Script/CScriptEngine.h"
#include "Script/CScriptObject.h"

#include "Utility/Script/ScriptHandle.h"

#include <math.h>
#include <boost/serialization/export.hpp> 

#include <angelscript.h>

BOOST_CLASS_EXPORT(CDrawingField);


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
CDrawingField::CDrawingField():
CDrawingParts  ()
{
    m_eType  = DT_FIELD;
    //TODO:実装
    m_iTypeId = 0;
    m_pData = NULL;
}   

/**
 *  @brief  コンストラクター.
 *  @param  [in] nID 
 *  @retval なし 
 *  @note
 */
CDrawingField::CDrawingField(int nID):
CDrawingParts( nID)

{
    m_eType  = DT_FIELD;
    //TODO:実装
    m_iTypeId = 0;
    m_pData = NULL;
}
/**
 *  @brief  コピーコンストラクター.
 *  @param  [in] group 
 *  @param  [in] bRef true:参照としてコピー 
 *  @retval なし 
 *  @note    ID,  コントロール
 *           はコピー元を引き継ぎます
 */
CDrawingField::CDrawingField(const  CDrawingField&  group, bool bRef):
CDrawingParts(group, bRef)
{
    m_iTypeId = 0;
    m_pData = NULL;

}

/**
 *  @brief  デストラクタ.
 *  @param  なし 
 *  @retval なし 
 *  @note
 */
CDrawingField::~CDrawingField()
{
    FreeObject();
}

//!< オブジェクト取得(名称) AS
CDrawingObject* CDrawingField::GetFieldObject(StdString& strName)
{
    CPartsDef* pParts = GetPartsDef();
    auto pObj = pParts->GetObjectByName(strName);
    return pObj.get();
}

//!< オブジェクト取得(名称) AS
CDrawingObject* CDrawingField::GetFieldObjectS(std::string& strName)
{
    CPartsDef* pParts = GetPartsDef();
    auto pObj = pParts->GetObjectByNameS(strName);
    return pObj.get();
}



//Forrest
void CDrawingField::SetPointList(const std::vector<POINT2D>& lstPoint)
{
    int iCnt = 0;
    foreach(const POINT2D& pt, lstPoint)
    {
        DB_PRINT(_T("No.%d (%f,%f)\n"), iCnt, pt.dX, pt.dY);
        iCnt++;
    }
}


void CDrawingField::GetPointList(std::vector<POINT2D>* pLstPoint)
{
    pLstPoint->clear();
    POINT2D pt1(1.2,2.3);
    POINT2D pt2(3.2,4.3);
    POINT2D pt3(5.2,6.3);

    pLstPoint->push_back(pt1);
    pLstPoint->push_back(pt2);
    pLstPoint->push_back(pt3);
}

void CDrawingField::SetObjectListTest(const std::vector<CDrawingObject*>* pObjectList)
{
    CDrawingObject* pObj;
    size_t iSize = pObjectList->size();
    for (size_t nCnt = 0; nCnt < iSize; nCnt++)
    {
        pObj = pObjectList->at(nCnt);
        m_lstTestObj.push_back(pObj);
    }
}

void CDrawingField::GetObjectListTest(std::vector<CDrawingObject*>* pObjectList)
{
    CDrawingObject* pObj;
    size_t iSize = m_lstTestObj.size();
    for (size_t nCnt = 0; nCnt < iSize; nCnt++)
    {
        pObj = m_lstTestObj.at(nCnt);
        pObjectList->push_back(pObj);
    }
}


void CDrawingField::CreateObjectListTest(std::vector<CDrawingObject*>* pObjectList)
{
    CDrawingLine* pLine = new CDrawingLine(1);
    pLine->SetName(_T("Line1"));
    pObjectList->push_back(pLine);
DB_PRINT(_T("Line1 %I64x \n"), pLine);

    CDrawingPoint* pPoint = new CDrawingPoint(2);
    pPoint->SetName(_T("Point2"));
    pObjectList->push_back(pPoint);
DB_PRINT(_T("Point2 %I64x \n"), pPoint);

    pLine = new CDrawingLine(2);
    pLine->SetName(_T("Line2"));
    pObjectList->push_back(pLine);
DB_PRINT(_T("Line2 %I64x \n"), pLine);

}

void CDrawingField::DispObjectListTest(std::vector<CDrawingObject*>* pObjectList)
{
    CDrawingObject* pObj;
    StdString strName;
    size_t iSize = pObjectList->size();
    for (size_t nCnt = 0; nCnt < iSize; nCnt++)
    {
        pObj = pObjectList->at(nCnt);
        strName = pObj->GetName();
        DB_PRINT(_T("Obj %d %s \n"), nCnt, strName.c_str());
    }
}


//Forrest
void CDrawingField::SetDrawingObjectList(const std::vector<CDrawingObject*>& lstPoint)
{
    int iCnt = 0;
    foreach(const CDrawingObject* pObj, lstPoint)
    {
        DB_PRINT(_T("No.%d %s \n"), iCnt, pObj->GetName().c_str());
        iCnt++;
    }
}


void CDrawingField::GetDrawingObjectList(std::vector<CDrawingObject*>* pLstPoint,
                                         StdString& strPropertySet)
{
    CDrawingPoint* pPoint = new CDrawingPoint();
    pPoint->SetName(_T("Test1"));

    pLstPoint->push_back(pPoint);
DB_PRINT(_T("Test1 %I64x \n"), pPoint);
    CDrawingLine* pLine = new CDrawingLine();
    pLine->SetName(_T("Test2"));

DB_PRINT(_T("Test2 %I64x \n"), pLine);
    pLstPoint->push_back(pLine);

    DB_PRINT(_T("Propertyset %s \n"), strPropertySet.c_str());

}

void CDrawingField::PrintObject(const CDrawingObject* pObject)
{
    const CDrawingObject* pObj = pObject;
    DB_PRINT(_T("Id.%d %s %I64x\n"), pObj->GetId(), pObj->GetName().c_str(), pObject);

}

void CDrawingField::PrintObject2(const CDrawingCompositionLine* pObject)
{
    const CDrawingCompositionLine* pObj = pObject;
    DB_PRINT(_T("Id.%d %s \n"), pObj->GetId(), pObj->GetName().c_str());

}

void CDrawingField::GetDrawingType(DRAWING_TYPE eDrawingType)
{
    DB_PRINT(_T("GetDrawingType %d \n"), eDrawingType);
}

void CDrawingField::PrintPoint2D(POINT2D* pPt2D)
{
    DB_PRINT(_T("Point2D(%f,%f) %I64x \n"), pPt2D->dX, pPt2D->dY, pPt2D);
    pPt2D->dX += 10.1;
    pPt2D->dY += 10.1;

}

void CDrawingField::SetData(void* pData)
{
    m_pData = pData;
    DB_PRINT(_T("SetData.%I64x \n"), m_pData);

}

void CDrawingField::GetData(void* pData) const
{
    pData = m_pData;
    DB_PRINT(_T("GetData %I64x \n"), m_pData);
}

void* CDrawingField::GetDataAddress() const
{
    DB_PRINT(_T("GetData %I64x \n"), m_pData);
    return m_pData;
}

CDrawingObject* CDrawingField::CreateObject(DRAWING_TYPE eType, StdString strName)
{

    std::shared_ptr<CDrawingObject> pObj;

    switch(eType)
    {
    case DT_POINT:  {pObj = std::make_shared< CDrawingPoint>(); break;}
    case DT_LINE:   {pObj = std::make_shared< CDrawingLine>(); break;}
    case DT_CIRCLE: {pObj = std::make_shared< CDrawingCircle>();break;}
    case DT_TEXT:   {pObj = std::make_shared< CDrawingText>();break;}
    case DT_SPLINE: {pObj = std::make_shared< CDrawingSpline>();break;}
    case DT_ELLIPSE:{pObj = std::make_shared< CDrawingEllipse>();break;}
    case DT_NODE:   {pObj = std::make_shared< CDrawingNode>();break;}

    case DT_DIM_L:   {pObj = std::make_shared< CDrawingDimL>();break;}
    case DT_DIM_H:   {pObj = std::make_shared< CDrawingDimH>();break;}
    case DT_DIM_V:   {pObj = std::make_shared< CDrawingDimV>();break;}
    case DT_DIM_A:   {pObj = std::make_shared< CDrawingDimA>();break;}
    case DT_DIM_D:   {pObj = std::make_shared< CDrawingDimD>();break;}
    case DT_DIM_R:   {pObj = std::make_shared< CDrawingDimR>();break;}
    case DT_IMAGE:   {pObj = std::make_shared< CDrawingImage>();break;}

    case DT_DIM_C:   {pObj = std::make_shared< CDrawingDimC>();break;}

    case DT_PARTS:  {pObj = std::make_shared< CDrawingParts>();break;}
    case DT_FIELD:  {pObj = std::make_shared< CDrawingField>();break;}
    case DT_GROUP:  {pObj = std::make_shared< CDrawingGroup>();break;}


    case DT_REFERENCE:  {pObj = std::make_shared< CDrawingReference>();break;}
    case DT_COMPOSITION_LINE:  {pObj = std::make_shared< CDrawingCompositionLine>();break;}
    case DT_CONNECTION_LINE:  {pObj = std::make_shared< CDrawingConnectionLine>();break;}
    case DT_ELEMENT:    {pObj = std::make_shared< CDrawingElement>();break;}
    default:
        break;
    }

    if (pObj)
    {
        auto pParet = GetParentParts();
        auto pPartParts = std::dynamic_pointer_cast<CDrawingParts>(pParet);

        pObj->SetName(strName.c_str());

        if (pPartParts)
        {
            //通常は画面を生成してから
            pPartParts->AddData(pObj, true, false);
        }

        const TCHAR* pChar = strName.c_str();
        DB_PRINT(_T("CreateObject [%I64x] %s \n"), pObj, pChar);
    }

    return pObj.get();
}
CDrawingObject* CDrawingField::CreateObjectS(DRAWING_TYPE eType,std::string strName)
{
    return CreateObject(eType, CUtil::CharToString(strName.c_str()));
}


void CDrawingField::AnalysisData(void *pRef, int refTypeId)
{
    StdStringStream strm;

    asIScriptEngine *engine = THIS_APP->GetScriptEngine()->GetEngine();
    asIObjectType *ot = engine->GetObjectTypeById(refTypeId);
    void*  pData = *(void**)pRef;
    CDrawingObject* pDrawingObj;
    CDrawingObject* pDrawingObj2;
    pDrawingObj = reinterpret_cast<CDrawingObject*>(pData);
    pDrawingObj2 = reinterpret_cast<CDrawingObject*>(pRef);



    switch(refTypeId)
    {
    case asTYPEID_VOID:    {strm << _T("void,"); break;} 
    case asTYPEID_BOOL:    {strm << _T("bool,"); break;}
    case asTYPEID_INT8:    {strm << _T("int8,"); break;}
    case asTYPEID_INT16:   {strm << _T("int16,"); break;}
    case asTYPEID_INT32:   {strm << _T("int32,"); break;}
    case asTYPEID_INT64:   {strm << _T("int64,"); break;}
    case asTYPEID_UINT8:   {strm << _T("uint8,"); break;}
    case asTYPEID_UINT16:  {strm << _T("uint16,"); break;}
    case asTYPEID_UINT32:  {strm << _T("uint32,"); break;}
    case asTYPEID_UINT64:  {strm << _T("uint64,"); break;}
    case asTYPEID_FLOAT:   {strm << _T("float,"); break;}
    case asTYPEID_DOUBLE:  {strm << _T("double,"); break;}
    default:
        break;
    }


    if (refTypeId & asTYPEID_OBJHANDLE)
    {
        strm << _T("ObjHandle,");

		engine->AddRefScriptObject(m_pData, engine->GetObjectTypeById(m_iTypeId));

    }

    if (refTypeId & asTYPEID_HANDLETOCONST)
    {
        strm << _T("handleToConst,");
    }

    if (refTypeId & asTYPEID_MASK_OBJECT)
    {
        //The type is an enum
        strm << _T("MaskObject,");
        std::string strName = ot->GetName();
        if (strName == "HOBJ")
        {
            int iType;
            CScriptHandle* pHObj;
            pHObj = reinterpret_cast<CScriptHandle*>(pRef);
            iType = pHObj->GetTypeId();
            asIObjectType *ot2;

            ot2 = pHObj->GetType();
            std::string strObjName = ot2->GetName();

            CDrawingObject* pDrawingObj = NULL;
            if (strObjName == "DrawingObject")
            {
                pDrawingObj = reinterpret_cast<CDrawingObject*>(pHObj->GetRef());
            }
            int a=10;
        }
    }

    if (refTypeId & asTYPEID_APPOBJECT)
    {
        //int asCScriptEngine::GetTypeIdFromDataType(const asCDataType &dtIn) const より
		//if( dt.GetObjectType()->flags & asOBJ_SCRIPT_OBJECT ) typeId |= asTYPEID_SCRIPTOBJECT;
		//else if( dt.GetObjectType()->flags & asOBJ_TEMPLATE ) typeId |= asTYPEID_TEMPLATE;
		//else if( dt.GetObjectType()->flags & asOBJ_ENUM ) {} // TODO: Should we have a specific bit for this?
		//else typeId |= asTYPEID_APPOBJECT;

        strm << _T("AppObject,");
    }

    if (refTypeId & asTYPEID_SCRIPTOBJECT)
    {
        strm << _T("ScriptObject,");

    }

    if (refTypeId & asTYPEID_TEMPLATE)
    {
        strm << _T("Template,");
    }


    DB_PRINT(_T("AnalysisData [%I64x] [%I64x] %s \n"), pRef, pData, strm.str().c_str());

}


void CDrawingField::FreeObject()
{
    asIScriptEngine *engine = THIS_APP->GetScriptEngine()->GetEngine();

    // If it is a handle or a ref counted object, call release
	if( m_iTypeId & asTYPEID_MASK_OBJECT )
	{
		// Let the engine release the object
		asIObjectType *ot = engine->GetObjectTypeById(m_iTypeId);
		engine->ReleaseScriptObject(m_pData, ot);

		// Release the object type info
		if( ot )
        {
			ot->Release();
        }

		m_pData = NULL;
		m_iTypeId = 0;
	}

	// For primitives, there's nothing to do
}


void CDrawingField::Store(void *ref, int refTypeId)
{
    asIScriptEngine *engine = THIS_APP->GetScriptEngine()->GetEngine();

	// Hold on to the object type reference so it isn't destroyed too early
	if( *(void**)ref && (refTypeId & asTYPEID_MASK_OBJECT) )
	{
		asIObjectType *ot = engine->GetObjectTypeById(refTypeId);
		if( ot )
			ot->AddRef();
	}

	FreeObject();

	m_iTypeId = refTypeId;
	if( m_iTypeId & asTYPEID_OBJHANDLE )
	{
		// We're receiving a reference to the handle, so we need to dereference it
		m_pData = *(void**)ref;
		engine->AddRefScriptObject(m_pData, engine->GetObjectTypeById(m_iTypeId));
	}
	else if( m_iTypeId & asTYPEID_MASK_OBJECT )
	{
		// Create a copy of the object
		m_pData = engine->CreateScriptObjectCopy(ref, engine->GetObjectTypeById(m_iTypeId));
	}
	else
	{
		// Primitives can be copied directly
		m_pData = 0;

		// Copy the primitive value
		// We receive a pointer to the value.
		int size = engine->GetSizeOfPrimitiveType(m_iTypeId);
		memcpy(m_pData, ref, size);
	}
}

bool CDrawingField::Retrieve(void *ref, int refTypeId) const
{
    asIScriptEngine *engine = THIS_APP->GetScriptEngine()->GetEngine();
	if( refTypeId & asTYPEID_OBJHANDLE )
	{
		// Is the handle type compatible with the stored value?

		// A handle can be retrieved if the stored type is a handle of same or compatible type
		// or if the stored type is an object that implements the interface that the handle refer to.
		if( (m_iTypeId & asTYPEID_MASK_OBJECT) && 
			engine->IsHandleCompatibleWithObject(m_pData, m_iTypeId, refTypeId) )
		{
			engine->AddRefScriptObject(m_pData, engine->GetObjectTypeById(m_iTypeId));
			*(void**)ref = m_pData;

			return true;
		}
	}
	else if( refTypeId & asTYPEID_MASK_OBJECT )
	{
		// Is the object type compatible with the stored value?

		// Copy the object into the given reference
		if( m_iTypeId == refTypeId )
		{
			engine->AssignScriptObject(ref, m_pData, engine->GetObjectTypeById(m_iTypeId));

			return true;
		}
	}
	else
	{
		// Is the primitive type compatible with the stored value?

		if( m_iTypeId == refTypeId )
		{
			int size = engine->GetSizeOfPrimitiveType(refTypeId);
			memcpy(ref, &m_pData, size);
			return true;
		}

		// We know all numbers are stored as either int64 or double, since we register overloaded functions for those
		if( m_iTypeId == asTYPEID_INT64 && refTypeId == asTYPEID_DOUBLE )
		{
			//*(double*)ref = double(static_cast<double>(m_pData));
			return false;
		}
		else if( m_iTypeId == asTYPEID_DOUBLE && refTypeId == asTYPEID_INT64 )
		{
			//*(asINT64*)ref = asINT64(value.valueFlt);
			return false;
		}
	}

	return false;
}



//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void TEST_CDrawingField()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}
#endif //_DEBUG