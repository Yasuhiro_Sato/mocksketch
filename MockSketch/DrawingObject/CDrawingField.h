/**
 * @brief        CDrawingFieldヘッダーファイル
 * @file	        CDrawingField.h
 * @author           Yasuhiro Sato
 * @date	        07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
  *			
 *
 * $
 * $
 * 
 */
#ifndef __DRAWING_FIELD_H_
#define __DRAWING_FIELD_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DrawingObject/CDrawingScriptBase.h"
#include "DrawingObject/Primitive/LINE2D.h"
#include "./CDrawingParts.h"

/*---------------------------------------------------*/
/*  Classe                                           */
/*---------------------------------------------------*/
class CFieldDef;
class CDrawingCompositionLine; //ForTest
/**
 * @class   CDrawingField
 * @brief  
 */
class CDrawingField :public CDrawingParts//:public CRTTIClass<CDrawingField, CDrawingObject, DT_LINE>
{
public:

public:
    //!< コンストラクタ
    CDrawingField();

    //!< コンストラクタ
    CDrawingField(int nID);

    //!< コピーコンストラクタ
    CDrawingField(const  CDrawingField&  group, bool bRef = true);

    //!< デストラクタ
    virtual ~CDrawingField();

    //!< オブジェクト取得(名称) AS
    CDrawingObject* GetFieldObject(StdString& strName);

    //!< オブジェクト取得(名称) AS
    CDrawingObject* GetFieldObjectS(std::string& strName);

//Forrest
   void SetPointList(const std::vector<POINT2D>& lstPoint);
   void GetPointList(std::vector<POINT2D>* pLstPoint);

   void SetDrawingObjectList(const std::vector<CDrawingObject*>& lstPoint);
   void GetDrawingObjectList(std::vector<CDrawingObject*>* pLstPoint,
                             StdString& strPropertySet);




   //====================================================================
   std::vector<CDrawingObject*> m_lstTestObject;

   void GetDrawingType(DRAWING_TYPE eDrawingType);

    void SetObjectListTest(const std::vector<CDrawingObject*>* pObjectList);

    void GetObjectListTest(std::vector<CDrawingObject*>* pObjectList);

    void CreateObjectListTest(std::vector<CDrawingObject*>* pObjectList);

    void DispObjectListTest(std::vector<CDrawingObject*>* pObjectList);

    void PrintObject(const CDrawingObject* pObject);
    void PrintObject2(const CDrawingCompositionLine* pObject);

    void PrintPoint2D(POINT2D* pPt2D);


    void SetData(void* pData);
    void GetData(void* pData) const;
    void* GetDataAddress() const;

    CDrawingObject* CreateObject(DRAWING_TYPE eType,StdString strName);
    CDrawingObject* CreateObjectS(DRAWING_TYPE eType,std::string strName);

    void AnalysisData(void *ref, int refTypeId);

    void Store(void *ref, int refTypeId);
    bool Retrieve(void *ref, int refTypeId) const;
    void FreeObject();




    int   m_iTypeId;
    void* m_pData;

    std::vector<CDrawingObject*> m_lstTestObj;

   //====================================================================
 

private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
            ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(CDrawingParts);
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }
};

#endif // 