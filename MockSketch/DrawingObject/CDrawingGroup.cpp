/**
 * @brief			CDrawingGroup実装ファイル
 * @file			CDrawingGroup.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MockSketch.h"

#include "DefinitionObject/CElementDef.h"
#include "./CDrawingGroup.h"
#include "./CDrawingParts.h"
#include "./CDrawingPoint.h"
#include "./CDrawingLine.h"
#include "./CDrawingCircle.h"
#include "./CDrawingSpline.h"
#include "./CDrawingText.h"
#include "./CDrawingNode.h"
#include "./CDrawingDimL.h"
#include "./CDrawingDimH.h"
#include "./CDrawingDimV.h"
#include "./CDrawingDimA.h"
#include "./CDrawingDimD.h"
#include "./CDrawingDimR.h"
#include "./CDrawingDimC.h"
#include "./CDrawingImage.h"
#include "./CDrawingParts.h"
#include "./CDrawingElement.h"
#include "./CDrawingConnectionLine.h"

#include "./CDrawingCompositionLine.h"

#include "DefinitionObject/CPartsDef.h"
#include "Utility/CUtility.h"
#include "System/CSystem.h"
#include "Script/CScriptEngine.h"
#include "Script/CScriptObject.h"

#include "Utility/Script/ScriptHandle.h"

#include <math.h>
#include <boost/serialization/export.hpp> 

#include <angelscript.h>

BOOST_CLASS_EXPORT(CDrawingGroup);


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
CDrawingGroup::CDrawingGroup():
CDrawingParts  ()
{
    m_eType  = DT_GROUP;
}   

/**
 *  @brief  コンストラクター.
 *  @param  [in] nID 
 *  @retval なし 
 *  @note
 */
CDrawingGroup::CDrawingGroup(int nID):
CDrawingParts( nID)

{
    m_eType  = DT_GROUP;
}
/**
 *  @brief  コピーコンストラクター.
 *  @param  [in] group 
 *  @param  [in] bRef true:参照としてコピー 
 *  @retval なし 
 *  @note    ID,  コントロール
 *           はコピー元を引き継ぎます
 */
CDrawingGroup::CDrawingGroup(const  CDrawingGroup&  group, bool bRef):
CDrawingParts(group, bRef)
{
    m_eType  = DT_GROUP;
	}

/**
 *  @brief  デストラクタ.
 *  @param  なし 
 *  @retval なし 
 *  @note
 */
CDrawingGroup::~CDrawingGroup()
{
}

void CDrawingGroup::OffsetMatrix(const MAT2D* pMat)
{
    if (GetId() == ID_TEMP_GROUP)
    {
#ifdef _DEBUG
        DB_PRINT(_T("CDrawingGroup::OffsetMatrix \n"));
#endif
        auto list = GetList();
        auto pDef = GetPartsDef();
        for (auto pObj : *list)
        {
            pObj->OffsetMatrix(pMat);
        }
#ifdef _DEBUG
        DB_PRINT(_T("CDrawingGroup::OffsetMatrix End\n"));
#endif
    }
    else
    {
        CDrawingObject::OffsetMatrix(pMat);
    }
}


//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void TEST_CDrawingGroup()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}
#endif //_DEBUG