/**
 * @brief        CDrawingGroupヘッダーファイル
 * @file	        CDrawingGroup.h
 * @author           Yasuhiro Sato
 * @date	        07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
  *			
 *
 * $
 * $
 * 
 */
#ifndef __DRAWING_GROUP_H_
#define __DRAWING_GROUP_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DrawingObject/CDrawingScriptBase.h"
#include "DrawingObject/Primitive/LINE2D.h"
#include "./CDrawingParts.h"

/*---------------------------------------------------*/
/*  Classe                                           */
/*---------------------------------------------------*/
class CFieldDef;
class CDrawingCompositionLine; //ForTest
/**
 * @class   CDrawingGroup
 * @brief  
 */
class CDrawingGroup :public CDrawingParts//:public CRTTIClass<CDrawingGroup, CDrawingObject, DT_LINE>
{
protected:

public:
    //!< コンストラクタ
    CDrawingGroup();

    //!< コンストラクタ
    CDrawingGroup(int nID);

    //!< コピーコンストラクタ
    CDrawingGroup(const  CDrawingGroup&  group, bool bRef = true);

    //!< デストラクタ
    virtual ~CDrawingGroup();

    //!< オフセット
    virtual void OffsetMatrix(const MAT2D* pMat);

private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
            ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(CDrawingParts);
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }
};

#endif // 