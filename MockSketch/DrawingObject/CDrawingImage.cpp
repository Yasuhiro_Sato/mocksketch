/**
 * @brief			CDrawingImage実装ファイル
 * @file			CDrawingImage.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MockSketch.h"

#include "./CDrawingImage.h"
#include "./CDrawingPoint.h"
#include "./CDrawingNode.h"
#include "./CDrawingText.h"
#include "./CDrawingLine.h"
#include "./CNodeBound.h"

#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/View/CDrawingView.h"
#include "View/CDrawMarker.h"

#include "Utility/CUtility.h"
#include "Utility/ExtText/TEXT_TYPE.H"
#include "Utility/ExtText/FONT_DATA.H"
#include "Utility/ExtText/TEXT_FRAME.H"
#include "Utility/ExtText/TEXT_MARGIN.H"
#include "Utility/ExtText/LINE_PROPERTY.H"
#include "System/CSystem.h"
#include <math.h>
#include <boost/serialization/export.hpp> 
BOOST_CLASS_EXPORT(CDrawingImage);


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
CDrawingImage::CDrawingImage():
CDrawingObject  (),
m_dAngle            (0.0),
m_iDrawIndex        (0),
m_bDotByDot         (false),
m_dWidth             (1.0),
m_dHeight             (1.0),
m_dOrgWidth            (1.0),
m_dOrgHeight           (1.0),
m_bMoveMode         (false),
m_iAlpha            (255),
m_dMinScl           (1e-03),
m_iDrawLayerId      (-1)
{
    m_eType  = DT_IMAGE;
}

/**
 *  @brief  コンストラクター.
 *  @param  [in] nID 
 *  @retval なし 
 *  @note
 */
CDrawingImage::CDrawingImage(int nID):
CDrawingObject( nID, DT_IMAGE),
m_dAngle            (0.0),
m_iDrawIndex        (0),
m_bDotByDot         (false),
m_dWidth             (1.0),
m_dHeight             (1.0),
m_dOrgWidth            (1.0),
m_dOrgHeight           (1.0),
m_bMoveMode         (false),
m_iAlpha            (255),
m_dMinScl           (1e-03),
m_iDrawLayerId      (-1)
{
    m_eType  = DT_IMAGE;
}


/**
 *  @brief  デストラクタ.
 *  @param  なし 
 *  @retval なし 
 *  @note
 */
CDrawingImage::~CDrawingImage()
{
}

/**
 * コピーコンストラクタ
 */
CDrawingImage::CDrawingImage(const CDrawingImage& Obj):
CDrawingObject( Obj)
{
    m_Pt                = Obj.m_Pt;
    m_dAngle            = Obj.m_dAngle;
    m_iDrawIndex        = Obj.m_iDrawIndex;
    m_bDotByDot         = Obj.m_bDotByDot;
    m_dWidth             = Obj.m_dWidth;
    m_dHeight             = Obj.m_dHeight;
    m_dOrgWidth            = Obj.m_dOrgWidth;
    m_dOrgHeight           = Obj.m_dOrgHeight;
    m_pImage            = Obj.m_pImage;
    m_iAlpha            = Obj.m_iAlpha;
    m_strImageName      = Obj.m_strImageName;
    m_bMoveMode         = false;
    m_mat2D             = Obj.m_mat2D;
    m_dMinScl           = Obj.m_dMinScl;
    m_iDrawLayerId      = Obj.m_iDrawLayerId;
}


/**
 *  @brief  回転.
 *  @param  [in]    pPt2D    回転中心(絶対座標)
 *  @param  [in]    dAngle   (絶対)回転角(rad)
 *  @retval     なし
 *  @note
 */
void CDrawingImage::Rotate(const POINT2D& pt2D, double dAngle)
{
    AddChgCnt();
    MAT2D matRot;
    matRot.Rotate(pt2D, dAngle);
    Matrix(matRot);
}

/**
 *  @brief  相対移動.
 *  @param  [in]    pPt2D   移動量
 *  @retval         なし
 *  @note
 */
void CDrawingImage::Move(const POINT2D& pt2D) 
{
    AddChgCnt();

    MAT2D matMove;
    matMove.Move(pt2D);
    Matrix(matMove);
}

/**
 *  @brief  絶対移動.
 *  @param  [in]    pPt2D   移動量
 *  @retval         なし
 *  @note
 */
void CDrawingImage::AbsMove(const POINT2D& pt2D) 
{
    AddChgCnt();
    m_mat2D.SetAffinePos(pt2D);
}

/**
 *  @brief  交点計算.
 *  @param  [out]   pList   交点のリスト
 *  @param  [in]    pObj    交差するオブジェクト
 *  @retval         なし                
 *  @note
 */
void CDrawingImage::CalcIntersection ( std::vector<POINT2D>* pList,   
                                const CDrawingObject* pObj,
                                bool bOnline,
                                double dMin)  const
{
    CDrawingObject::CalcIntersectionPoint ( pList, pObj, m_Pt, bOnline, dMin);
}

/**
 *  @brief  位置調整.
 *  @param  [in]    pPtIntersect   調整位置
 *  @param  [in]    pPtClick       調整方向指定点
 *  @retval         なし
 *  @note
 */
bool CDrawingImage::Trim (const POINT2D& ptIntersect, const POINT2D& ptClick, bool bReverse)
{
    //Trimは不可
    return false;
}


/**
 *  @brief  分割.
 *  @param  [in]    pPtBreak  分割位置
 *  @retval         分割によって出来たオブジェクト
 *  @note
 */
std::shared_ptr<CDrawingObject>  CDrawingImage::Break (const POINT2D& ptBreak)
{
    //分割不可
    return NULL;
}


/**
 *  @brief  描画.
 *  @param  [in]  pView       表示View
 *  @param  [in]  bMouseOver  true オブジェクト上にマウスあり
 *  @retval         なし
 *  @note
 */
void CDrawingImage::Draw(CDrawingView* pView, 
                         const std::shared_ptr<CDrawingObject> pParentObject,
                         E_MOUSE_OVER_TYPE eMouseOver,
                        int iLayerId)
{
    STD_ASSERT(pView != 0);

    if (!m_pCtrl){return;}

    POINT  pt;

    //-------------------
    // MATRIX
    //-------------------

    if (pParentObject)
    {
        const MAT2D* pMat = pParentObject->GetDrawingMatrix();
        if (pMat)
        {
            m_matDraw = m_mat2D * (*pMat);
        }
        else
        {
            m_matDraw = m_mat2D;
        }
    }
    else
    {
        m_matDraw = m_mat2D;
    }


    if (m_pMatOffset)
    {
        m_matDraw = m_matDraw * (*m_pMatOffset);
#ifdef _DEBUG  
        
        {
            POINT2D pt;
            double dAngle;
            double dSclX;
            double dSclY;
            double dT;
            m_pMatOffset->GetAffineParam(&pt, &dAngle, &dSclX, &dSclY, &dT);
            DB_PRINT(_T("CDrawingImage::Draw pt:(%f,%f) A:%f, SclX:%f SclY:%f T:%f\n"), pt.dX, pt.dY,
                dAngle, dSclX, dSclY, dT);

        }
#endif

    }
    
    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }

    m_iDrawLayerId = iLayerId;


    POINT2D ptDraw;
    double dObjSclX;
    double dObjSclY;
    double dAngle;
    double dT;

    m_matDraw.GetAffineParam(&ptDraw, &dAngle, &dObjSclX, &dObjSclY, &dT);

    pView->ConvWorld2Scr(&pt, ptDraw, iLayerId);

    //CImageData* pImage;
    //pImage = pElemetDef->GetIconData(m_iIconIndex);

    std::shared_ptr<CImageData> pImage;
    pImage = m_pImage.lock();

    CLayer* pLayer;
    pLayer = m_pCtrl->GetLayer(iLayerId);

    double dScl;
    dScl = pLayer->dScl;
    dScl = dScl * pView->GetViewScale();


    if (!pImage)
    {
        //イメージデータがない時の処理
        CExtText exText;
        exText.SetText(_T("No Image"));
        exText.SetDatum(CENTER_MID);

        TEXT_PARAM param;
        param.dScl      = 1.0l;
        param.bIndex    = false;         //インデックス使用有無
        param.iId       = m_nId;         //インデックス
        //-------------------------
        //param.iBase     = 0;
        //-------------------------
        param.dOffsetAngle  = m_dAngle;  //追加角度
        param.bForceColor        = false;     //強制色設定の有無
        param.cr            = 0;         //設定色
        param.bEmphasis    = false ;    //太字
        param.bFrameColor   = false;     //フレーム強制色設定の有無
        param.crFrame       = 0;         //フレーム強制設定色

        pView->Text(&exText, pt, &param, iLayerId);

        return;
    }

    //double dObjSclX = m_dWidth / m_dOrgWidth;
    //double dObjSclY = m_dHeight / m_dOrgHeight;



    double  dSclX = 1.0;
    double  dSclY = 1.0;

        //この場合は、印刷プレビューと断定する

    if(!m_bDotByDot)
    {
        dSclX = dObjSclX * dScl;
        dSclY = dObjSclY * dScl;
    }


    pView->SetIndexBufferId(m_nId);

    if (!m_bMoveMode)
    {
        pView->Image(pt, pImage.get(), 
                         m_iDrawIndex, 
                         dSclX, 
                         dSclY, 
                         dAngle, 
                         m_iAlpha);
    }
    else
    {

        pView->ImageBounds(pt, pImage.get(), 
                                     dSclX, 
                                     dSclY, 
                                     dAngle);                          }

    COLORREF crObj;
    bool bSel = false;
    if (IsBelongToRoot(pParentObject.get()))
    {
        if (IsSelect())
        {
            //選択色
            crObj = DRAW_CONFIG->GetSelColor();
            bSel = true;
        }
        else
        {
            if (eMouseOver == E_MO_SELECTABLE)
            {
                crObj = pView->GetCurrentLayer()->crBrush;
                bSel = true;
            }
            else if (eMouseOver == E_MO_EMPHASIS)
            {
                crObj = DRAW_CONFIG->crEmphasis;
                bSel = true;
            }
			else if (eMouseOver == E_MO_CONNECTABLE)
			{
				crObj = DRAW_CONFIG->crConnectable;
				bSel = true;
			}
        }
    }

    if (bSel)
    {
        pView->SetPen( PS_SOLID, 3, crObj, m_nId);
        pView->ImageBounds(pt, pImage.get());
    }
}

/**
 *  @brief  描画削除.
 *  @param  [in]    pView   表示View
 *  @retval         なし
 *  @note
 */
void CDrawingImage::DeleteView(CDrawingView* pView, 
                               const std::shared_ptr<CDrawingObject> pParentObject,
                               int iLayerId )
{
    STD_ASSERT(pView != 0);

    if (!m_pCtrl){return;}

    POINT  pt;

    //-------------------
    // MATRIX
    //-------------------

    MAT2D matDraw;
    if (pParentObject)
    {
        const MAT2D* pMat = pParentObject->GetDrawingMatrix();
        if (pMat)
        {
            matDraw = m_mat2D * (*pMat);
        }
        else
        {
            matDraw = m_mat2D;
        }
    }
    else
    {
        matDraw = m_mat2D;
    }

    if (m_pMatOffset)
    {
        matDraw = matDraw * (*m_pMatOffset);
    }
    
    POINT2D ptDraw;
    double dObjSclX;
    double dObjSclY;
    double dAngle;
    double dT;

    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }

    matDraw.GetAffineParam(&ptDraw, &dAngle, &dObjSclX, &dObjSclY, &dT);
    pView->ConvWorld2Scr(&pt, ptDraw, iLayerId);

    std::shared_ptr<CImageData> pImage;
    pImage = m_pImage.lock();

    CLayer* pLayer;
    pLayer = m_pCtrl->GetLayer(iLayerId);

    double dScl;
    dScl = pLayer->dScl;
    dScl = dScl * pView->GetViewScale();

    double  dSclX = 1.0;
    double  dSclY = 1.0;

    //この場合は、印刷プレビューと断定する
    if(!m_bDotByDot)
    {
        dSclX = dObjSclX * dScl;
        dSclY = dObjSclY * dScl;
    }

    pView->SetIndexBufferId(m_nId);

    COLORREF crObj = DRAW_CONFIG->GetBackColor();

    pView->ImageFill(pt, pImage.get(),
                                    dSclX, 
                                    dSclY, 
                                    m_dAngle,
                                    crObj);
}

/**
 *  @brief  鏡像.
 *  @param  [in]    line   対称軸
 *  @retval         なし
 *  @note
 */
void CDrawingImage::Mirror(const LINE2D& line)
{
}


/**
 *  @brief  行列.
 *  @param  [in]    mat2D   適用する行列
 *  @retval         なし
 *  @note
 */
void CDrawingImage::Matrix(const MAT2D& mat2D)
{
    m_mat2D = m_mat2D * mat2D;
    MatrixDirect(m_mat2D);
}

void CDrawingImage::MatrixDirect(const MAT2D& mat2D)
{
    double dSclX;
    double dSclY;
    double dSkew;

    m_mat2D = mat2D;

    m_mat2D.GetAffineParam(&m_Pt,  &m_dAngle, &dSclX, &dSclY, &dSkew);


    if (fabs(dSclX) < m_dMinScl)
    {
        dSclX = m_dMinScl * CUtil::sgn2(dSclX);
    }

    if (fabs(dSclY) < m_dMinScl)
    {
        dSclY = m_dMinScl * CUtil::sgn2(dSclY);
    }


    m_dWidth =  m_dOrgWidth * dSclX;
    m_dHeight = m_dOrgHeight * dSclY;

#if 0
DB_PRINT(_T("W:%f, H:%f\n"), m_dWidth, m_dHeight);
#endif
    AddChgCnt();


}
/**
 *  @brief  倍率.
 *  @param  [in]    pPt2D   拡大中心
 *  @param  [in]    dSclX   X倍率
 *  @param  [in]    dSclY   Y倍率
 *  @retval         なし
 *  @note
 */
void CDrawingImage::Scl(const POINT2D& pt2D, double dSclX, double dSclY)
{
    MAT2D matScl;

    if (fabs(dSclX) < NEAR_ZERO)
    {
        return;
    }

    if (fabs(dSclY) < NEAR_ZERO)
    {
        return;
    }

    if (fabs(dSclX) < m_dMinScl)
    {
        dSclX = m_dMinScl * CUtil::sgn2(dSclX);
    }

    if (fabs(dSclY) < m_dMinScl)
    {
        dSclY = m_dMinScl * CUtil::sgn2(dSclY);
    }

    matScl.Scl(m_Pt,  dSclX, dSclY);
    Matrix(matScl);
    AddChgCnt();
}

/**
 *  @brief  範囲.
 *  @param  [in] p1 1点目
 *  @param  [in] p2 2点目
 *  @retval  true:2点で示される矩形内に点が存在する 
 *  @note
 */
bool CDrawingImage::IsInner( const RECT2D& rcArea, bool bPart) const
{
    RECT2D rc;
    POINT2D pt[4];
    CDrawingLine  lList[4];

    _GetFeturePoint(&pt[0], &pt[1], &pt[2], &pt[3]);

    for(int i = 0; i < 4 ; i++)
    {
        int j = (i + 1) % 4;
        lList[i].SetPoint1(pt[i]);
        lList[i].SetPoint2(pt[j]);
    }

    if(bPart)
    {
        for(auto l: lList)
        {
            if(l.IsInner(rcArea, bPart))
            {
                return true;
            }
        }
        return false;
    }
    else
    {
        for(auto l: lList)
        {
            if(!l.IsInner(rcArea, bPart))
            {
                return false;
            }
        }
        return true;
    }
    return false;
}


/**
 *  @brief   参照データに基づいて更新.
 *  @param   
 *  @retval  
 *  @note
 */
CDrawingObject* CDrawingImage::UpdateRef()
{
    CDrawingObject*      pRef;

    pRef = CDrawingObject::UpdateRef();

    if (pRef == NULL)
    {
        return NULL;
    }

    CDrawingImage* pDim = dynamic_cast<CDrawingImage*>(pRef);

    STD_ASSERT(pDim != NULL);

    if (pDim == NULL)
    {
        return false;
    }

    //TODO: 実装

    m_iChgCnt = pRef->GetChgCnt();
    return pRef;
}

/**
 * @brief   位置設定
 * @param   [in] ptAbs 表示位置
 * @retval  なし
 * @note    
 */
void  CDrawingImage::SetPoint(POINT2D ptAbs)
{
    m_Pt = ptAbs;
    m_mat2D.SetAffinePos(m_Pt);
}

/**
 * @brief   幅設定
 * @param   [in] dWidth 画像幅
 * @retval  なし
 * @note    
 */
void CDrawingImage::SetWidth(double dWidth)
{
    m_dWidth = dWidth;
}

/**
 * @brief   高さ設定
 * @param   [in] dHeight 画像高さ
 * @retval  なし
 * @note    
 */
void CDrawingImage::SetHeight(double dHeight)
{
    m_dHeight = dHeight;
}

/**
 *  @brief   スナップ点取得.
 *  @param   [out] pLstSnap スナップ点リスト
 *  @retval  true: スナップ点あり
 *  @note
 */
bool CDrawingImage::GetSnapList(std::list<SNAP_DATA>* pLstSnap)const 
{
    using namespace VIEW_COMMON;

    DWORD dwSnap = DRAW_CONFIG->GetSnapType();
    bool bRet = false;

    STD_ASSERT(pLstSnap);

    if (!pLstSnap)
    {
        return false;
    }

    pLstSnap->clear();
    SNAP_DATA snapData;
    snapData.bKeepSelect = false;

    if (dwSnap & SNP_DATUME_POINT)
    {
        snapData.eSnapType = SNP_DATUME_POINT;
        snapData.pt = m_Pt;
        pLstSnap->push_back(snapData);
        bRet = true;
    }

    if (dwSnap & SNP_FEATURE_POINT)
    {
        POINT2D ptTL;
        POINT2D ptBR;
        POINT2D ptTR;
        POINT2D ptBL;

        _GetFeturePoint(&ptTL, &ptBR, &ptTR, &ptBL);

        snapData.eSnapType = SNP_FEATURE_POINT;
        snapData.pt = ptTL;
        pLstSnap->push_back(snapData);

        snapData.pt = ptBR;
        pLstSnap->push_back(snapData);

        snapData.pt = ptTR;
        pLstSnap->push_back(snapData);

        snapData.pt = ptBL;
        pLstSnap->push_back(snapData);
    }
    return bRet;
}

/**
 * @brief   領域取得
 * @param   なし
 * @retval  なし
 * @note	描画領域を取得する
 */
RECT2D CDrawingImage::GetBounds() const
{
    RECT2D rc;
    POINT2D pt[4];

    _GetFeturePoint(&pt[0], &pt[1], &pt[2], &pt[3]);

    for(auto p: pt)
    {
        rc.ExpandArea(p);
    }
    return rc;
}

/**
 *  @brief   プロパティ初期化.
 *  @param   [in] pCtrl オブジェクトコントロールへのポインタ
 *  @retval     なし
 *  @note
 */
TREE_GRID_ITEM*  CDrawingImage::_CreateStdPropertyTree()
{
    NAME_TREE_ITEM<CStdPropertyItem>* pBeforeGroup;
    NAME_TREE_ITEM<CStdPropertyItem>* pNextItem;
    NAME_TREE_ITEM<CStdPropertyItem>* pGroupItem;
    NAME_TREE<CStdPropertyItem>* pTree;
    //int iId;
    CStdPropertyItem* pItem;
    boost::tuple< CStdPropertyItem*, void*> pairVal;


    pBeforeGroup = CDrawingObject::_CreateStdPropertyTree();
    pTree = m_psProperty->GetTreeInstance();

    pGroupItem  = pTree->AddNext(pBeforeGroup, NULL, GET_STR(STR_PRO_ELEMENT), _T("Element"));

    //------------
    //位置
    //------------
    CStdPropertyItemDef DefPos(PROP_POINT2D, 
        GET_STR(STR_PRO_POS)   ,
        _T("Position"),
        GET_STR(STR_PRO_INFO_POS), 
        true, 
        DISP_UNIT,
        POINT2D());

    pItem = new CStdPropertyItem(DefPos, PropPosition, m_psProperty, NULL, &m_Pt);
    pNextItem = pTree->AddChild(pGroupItem, pItem, DefPos.strDspName, pItem->GetName(), 2);

    //------------
    //角度(deg)
    //------------
    CStdPropertyItemDef DefAngle(PROP_DOUBLE, 
        GET_STR(STR_PRO_ANGLE)   , 
        _T("Angle"),
        GET_STR(STR_PRO_INFO_ANGLE), 
        true,
        DISP_ANGLE,
        double(0.0));

    pItem = new CStdPropertyItem(DefAngle, PropAngle, m_psProperty, NULL, &m_dAngle);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefAngle.strDspName, pItem->GetName());


    //------------
    // 幅
    //------------
    CStdPropertyItemDef DefWidth(PROP_DOUBLE, 
        GET_STR(STR_PRO_WIDTH)   , 
        _T("Width"),
        GET_STR(STR_PRO_INFO_WIDTH), 
        true,
        DISP_UNIT,
        double(0.0));

    pItem = new CStdPropertyItem(DefWidth, PropWidth, m_psProperty, NULL, &m_dWidth);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefWidth.strDspName, pItem->GetName());

    //------------
    // 高さ
    //------------
    CStdPropertyItemDef DefHeight(PROP_DOUBLE, 
        GET_STR(STR_PRO_HEIGHT)   , 
        _T("Height"),
        GET_STR(STR_PRO_INFO_HEIGHT), 
        true,
        DISP_UNIT,
        double(0.0));

    pItem = new CStdPropertyItem(DefHeight, PropHeight, m_psProperty, NULL, &m_dHeight);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefHeight.strDspName, pItem->GetName());

    //--------------------------
    // 図形インデックス
    //--------------------------
    std::shared_ptr<CImageData> pImage;
    pImage = m_pImage.lock();
    int iDrawMax = 0;

    if (pImage)
    {
        iDrawMax = pImage->Getdiv() - 1;
    }

    CStdPropertyItemDef DefDrawIndex(PROP_INT_RANGE, 
        GET_STR(STR_PRO_DRAW_INDEX)   , 
        _T("DrawIndex"),
        GET_STR(STR_PRO_INFO_DRAW_INDEX), 
        true,
        DISP_NONE,
        0,
        0,
        iDrawMax
        );

    pItem = new CStdPropertyItem( DefDrawIndex, PropDrawIndex, m_psProperty, NULL, &m_iDrawIndex);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefDrawIndex.strDspName, pItem->GetName());

    //------------
    // 透過率
    //------------
    int iAlphaMax = 255;
    CStdPropertyItemDef DefAlpha(PROP_INT_RANGE, 
        GET_STR(STR_PRO_ALPHA)   , 
        _T("Alpha"),
        GET_STR(STR_PRO_INFO_ALPHA), 
        true,
        DISP_NONE,
        iAlphaMax,
        0,
        iAlphaMax
        );

    pItem = new CStdPropertyItem( DefAlpha, PropAlpha, m_psProperty, NULL, &m_iAlpha);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefAlpha.strDspName, pItem->GetName());


    //------------
    // ファイル名
    //------------
    /*
    CStdPropertyItemDef DefFile(PROP_STR, 
        GET_STR(STR_PRO_FILE_NAME)   , 
        _T("FileName"),
        GET_STR(STR_PRO_INFO_FILE_NAME), 
        false,
        DISP_NONE,
        0,
        0,
        iAlphaMax
        );

    pItem = new CStdPropertyItem( DefFile, PropFileName, m_psProperty, NULL, &m_strImageName);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefFile.strDspName, pItem->GetName());
    */
    return pGroupItem;
}

/**
 *  @brief   プロパティ変更(位置)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingImage::PropPosition     (CStdPropertyItem* pData, void* pObj)
{
    CDrawingImage* pDrawing = reinterpret_cast<CDrawingImage*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->m_pCtrl->HideObject(pDrawing->m_nId);
        pDrawing->SetPoint(pData->anyData.GetPoint());
        pDrawing->m_pCtrl->Draw(pDrawing->m_nId);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(位置)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingImage::PropAngle        (CStdPropertyItem* pData, void* pObj)
{
    CDrawingImage* pDrawing = reinterpret_cast<CDrawingImage*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->m_pCtrl->HideObject(pDrawing->m_nId);
        pDrawing->SetAngle(pData->anyData.GetDouble());
        pDrawing->m_pCtrl->Draw(pDrawing->m_nId);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(図形インデックス)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingImage::PropDrawIndex    (CStdPropertyItem* pData, void* pObj)
{
    CDrawingImage* pDrawing = reinterpret_cast<CDrawingImage*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->m_pCtrl->HideObject(pDrawing->m_nId);
        pDrawing->SetDrawIndex(pData->anyData.GetInt());
        pDrawing->m_pCtrl->Draw(pDrawing->m_nId);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(透過率)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingImage::PropAlpha    (CStdPropertyItem* pData, void* pObj)
{
    CDrawingImage* pDrawing = reinterpret_cast<CDrawingImage*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->m_pCtrl->HideObject(pDrawing->m_nId);
        pDrawing->SetAlpha(pData->anyData.GetInt());
        pDrawing->m_pCtrl->Draw(pDrawing->m_nId);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(幅)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingImage::PropWidth    (CStdPropertyItem* pData, void* pObj)
{
    CDrawingImage* pDrawing = reinterpret_cast<CDrawingImage*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->m_pCtrl->HideObject(pDrawing->m_nId);
        pDrawing->SetWidth(pData->anyData.GetDouble());
        pDrawing->m_pCtrl->Draw(pDrawing->m_nId);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(高さ)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingImage::PropHeight    (CStdPropertyItem* pData, void* pObj)
{
    CDrawingImage* pDrawing = reinterpret_cast<CDrawingImage*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->m_pCtrl->HideObject(pDrawing->m_nId);
        pDrawing->SetHeight(pData->anyData.GetDouble());
        pDrawing->m_pCtrl->Draw(pDrawing->m_nId);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

bool CDrawingImage::PropFileName     (CStdPropertyItem* pData, void* pObj)
{
    //値変更なし
    return false;
}


/**
 *  @brief   描画(マウスオーバー時).
 *  @param   [in] pView 表示View
 *  @param   [in] bOver true:開始 false:終了
 *  @retval  なし
 *  @note
 */
void CDrawingImage::DrawOver( CDrawingView* pView, bool bOver)
{
}


/**
 * @brief   ロード終了後処理
 * @param   なし
 * @retval  なし
 * @note	
 */
void CDrawingImage::LoadAfter(CPartsDef* pDef)
{
    SetImage(m_strImageName.c_str(), false);
}


//!< マーカ初期化 true:マーカあり
void CDrawingImage::_GetFeturePoint(POINT2D* pTL, POINT2D* pBR,
                                    POINT2D* pTR, POINT2D* pBL) const
{
    int iR = CUtil::Round(1.0 * (DRAW_CONFIG->dDpi /25.4));

    POINT2D pt[4]; //0:TL 1:BR 3:TR 4:BL

    std::shared_ptr<CImageData> pImage;
    pImage = m_pImage.lock();

    if (!pImage)
    {
        return;
    }

    //int iImageDpi = pImage->GetDpi();
    //double dConvFactor = DRAW_CONFIG->pDispUnit->ToBase(1.0);
    //double dImageDpu = iImageDpi * (1000.0  * dConvFactor) / 25.4;

    POINT2D ptOffset;

    //double dWidth  = pImage->GetWidth() / dImageDpu;
    //double dHeight = pImage->GetHeight() / dImageDpu;

    ptOffset.dX = m_dWidth  / 2.0;
    ptOffset.dY = m_dHeight / 2.0;
    
    pt[0].dX = -ptOffset.dX;
    pt[0].dY =  ptOffset.dY;

    pt[1].dX = pt[0].dX + m_dWidth;
    pt[1].dY = pt[0].dY - m_dHeight;

    pt[2].dX = pt[1].dX;
    pt[2].dY = pt[0].dY;

    pt[3].dX = pt[0].dX;
    pt[3].dY = pt[1].dY;


    double dSclX = m_dWidth / m_dOrgWidth;
    double dSclY = m_dHeight / m_dOrgHeight;


    for(POINT2D& ptA: pt)
    {
        //ptA =  ptA * mat2D;
        //ptA.dX = dSclX * ptA.dX;
        //ptA.dY = dSclY * ptA.dY;
        ptA.Move(m_Pt);
        ptA.Rotate(m_Pt, m_dAngle);
    }

    *pTL = pt[0];
    *pBR = pt[1];
    *pTR = pt[2];
    *pBL = pt[3];


#ifdef _DEBUG_DISP
    double dH = pt[0].Distance(pt[3]);
    double dW = pt[0].Distance(pt[2]);
    POINT2D ptC = (pt[0] + pt[1]) /2.0;
    double dAngle1;
    double dAngle2;

        dAngle1 = 
            atan2(pt[0].dY - pt[3].dY,
                  pt[0].dX - pt[3].dX) 
            * RAD2DEG + 90.0;

        dAngle2 = 
            atan2(pt[2].dY - pt[0].dY,
                  pt[2].dX - pt[0].dX) 
            * RAD2DEG;
DB_PRINT( _T("Org W:%f,H:%f,C(%f,%f),A1:%f\n"),m_dWidth, m_dHeight,
             m_Pt.dX, m_Pt.dY, m_dAngle);

DB_PRINT( _T("   W:%f,H:%f,C(%f,%f),A1:%f,A2:%f\n"),
            dW,dH,
            ptC.dX, ptC.dY,
            dAngle1,
            dAngle2);

DB_PRINT( _T("TL:(%f,%f), TR(%f,%f)\n"),
             pt[0].dX, pt[0].dY,
             pt[2].dX, pt[2].dY);

DB_PRINT( _T("BL:(%f,%f), BR(%f,%f)\n"),
             pt[3].dX, pt[3].dY,
             pt[1].dX, pt[1].dY);

#endif

}


bool CDrawingImage::_CalcFramePoint(std::vector<POINT2D>* pLst, CDrawingView* pView)const
{
    STD_ASSERT(pView != 0);

    POINT2D ptDraw;
    double dObjSclX;
    double dObjSclY;
    double dAngle;
    double dT;

    m_matDraw.GetAffineParam(&ptDraw, &dAngle, &dObjSclX, &dObjSclY, &dT);

    CLayer* pLayer;
    pLayer = m_pCtrl->GetLayer(m_iDrawLayerId);

    double dScl;
    dScl = pLayer->dScl;

    double  dSclX = 1.0;
    double  dSclY = 1.0;

    if(!m_bDotByDot)
    {
        dSclX = dObjSclX * dScl;
        dSclY = dObjSclY * dScl;
    }

    std::shared_ptr<CImageData> pImage;

    pImage = m_pImage.lock();

    if (!pImage)
    {
        return false;
    }

    pImage->GetFramePoints(dSclX, dSclY, m_dAngle, pLst);

    double dDpu = pView->GetDpu();
    POINT2D ptWorld;
    for(POINT2D& pt: *pLst)
    {
        ptWorld.dX =   pt.dX / (dDpu ) + pLayer->ptOffset.dX + m_Pt.dX;
        ptWorld.dY =  -pt.dY / (dDpu ) + pLayer->ptOffset.dY + m_Pt.dY;
        pt = ptWorld;
    }
    /*
    m_ptInit[ND_TL] = m_lstBoundOrg[0];
    m_ptInit[ND_TR] = m_lstBoundOrg[1];
    m_ptInit[ND_BR] = m_lstBoundOrg[2];
    m_ptInit[ND_BL] = m_lstBoundOrg[3];
    */
    std::swap(pLst->at(2), pLst->at(3));
    return true;
}

//!< マーカ初期化 true:マーカあり
bool CDrawingImage::InitNodeMarker(CNodeMarker* pMarker)
{
    STD_ASSERT(pMarker);

    if(!m_psNodeBound)
    {
        m_psNodeBound = std::make_shared<CNodeBound>();
    }

    CDrawingView* pView = pMarker->GetView();

    std::vector<POINT2D> lstFrame;
    _CalcFramePoint(&lstFrame, pView);

    pView->ClearDraggingNodes();
    pView->ClearDraggingTmpObjects();

    //マーカーを用意
    m_psNodeBound->EnableScale(true);
    m_psNodeBound->EnableRotate(true);
    m_psNodeBound->SetDragMode(CNodeBound::DM_ONE);

    m_psNodeBound->Init(pMarker->GetView(),
        lstFrame,
        m_dAngle,
        1.0,
        1.0,
        1.0,
        shared_from_this());

    return true;
}

//直線パラメータとの近接点
POINT2D CDrawingImage::_NearPalmLine(double dA, double dB, double dC, POINT2D pt)
{
    POINT2D ptRet;

    double dL = sqrt(dA*dA + dB*dB);
    double dAlpha = (dA * pt.dX + dB * pt.dY + dC) /dL;

    ptRet.dX = pt.dX - dA * dAlpha;
    ptRet.dY = pt.dY - dB * dAlpha;

    return ptRet;
}


//!< マーカ選択
void CDrawingImage::SelectNodeMarker(CNodeMarker* pMarker, 
                                    StdString strMarkerId)
{
    if (m_psNodeBound)
    {
        m_psNodeBound->Select(pMarker, strMarkerId);
    }
}


//!< マーカ移動
void CDrawingImage::MoveNodeMarker(CNodeMarker* pMarker, 
                                    SNAP_DATA* pSnap,
                                    StdString strMarkerId,
                                    MOUSE_MOVE_POS posMouse)
{
    if(m_psNodeBound)
    {
        CDrawingView* pView = pMarker->GetView();
        CPartsDef*   pDef  = pView->GetPartsDef();

        pView->SetDrawingMode(DRAW_SEL);

        pView->GetMousePoint2(pSnap, posMouse );
        m_psNodeBound->OnMouseMove(pSnap, posMouse);
    }


}

//!< マーカ開放
bool CDrawingImage::ReleaseNodeMarker(CNodeMarker* pMarker, StdString strMarkerId, MOUSE_MOVE_POS posMouse)
{
    if(m_psNodeBound)
    {
        if (pMarker->GetMouseButton() == SEL_LBUTTON)
        {
            if(m_psNodeBound->OnMouseLButtonUp(posMouse))
            {
                m_psNodeBound->ApplyMatrix(false);
            }
        }
        else if(pMarker->GetMouseButton() == SEL_RBUTTON)
        {
            m_psNodeBound->OnMouseRButtonUp(posMouse);
        }
        m_psNodeBound->Release();
        m_psNodeBound.reset();
    }
    //m_pMatOffset = NULL; 
    return true;
}

//!< ノード変更
void CDrawingImage::ChangeNode(StdString strMarkerId, E_NODE_CHANGE_TYPE eType)
{


}


bool CDrawingImage::_SetSize() 
{
    std::shared_ptr<CImageData> pImage;
    pImage = m_pImage.lock();

    if (!pImage)
    {
        return false;
    }

    int iImageDpi = pImage->GetDpi();
    double dConvFactor = DRAW_CONFIG->pDispUnit->ToBase(1.0);

    //1Dot当たりの長さ
    double dImageDpu = iImageDpi * (1000.0  * dConvFactor) / 25.4;

    m_dOrgWidth = pImage->GetWidth() / dImageDpu;
    m_dOrgHeight = pImage->GetHeight() / dImageDpu;

    /*
    if (m_dOrgWidth > m_dOrgHeight)
    {
        m_dMinScl = dImageDpu / m_dOrgWidth;
    }
    else
    {
        m_dMinScl = dImageDpu / m_dOrgHeight;
    }
    */
    
    return true;
}

/**
 * @brief   イメージ読み込み
 * @param   [in] flleName ファイル名(パスは含まない)
 * @retval  true 成功
 * @note	
 */
bool CDrawingImage::SetImage(LPCTSTR flleName, bool bSetSize)
{
    CPartsDef* pDef = GetPartsDef();
    if (!pDef)
    {
        return false;
    }


    if(!pDef->IsImage(flleName))
    {
        if(!pDef->LoadImage(flleName))
        {
#if 0
            StdStringStream  strmError;
            StdString        strFormat;
            strFormat = GET_ERR_STR(e_file_read_s);
            strmError << StdFormat(strFormat) % flleName;
#endif
            return false;
        }
    }
    m_pImage = pDef->GetImage(flleName);

    bool bRet;
    bRet = _SetSize();

    if (bSetSize)
    {
        m_dWidth   = m_dOrgWidth;
        m_dHeight  = m_dOrgHeight;
    }

    return bRet;
}

/**
 * @brief   シリアル化(ロード)
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  なし
 * @note	
 */
template<class Archive>
void CDrawingImage::load(Archive& ar, const unsigned int version)
{
    SERIALIZATION_INIT;
        ar >> BOOST_SERIALIZATION_BASE_OBJECT_NVP(CDrawingObject);
        SERIALIZATION_LOAD("Pos"       , m_Pt);
        SERIALIZATION_LOAD("Angle"     , m_dAngle);
        SERIALIZATION_LOAD("Skew"      , m_dSkew);
        SERIALIZATION_LOAD("Index"     , m_iDrawIndex);
        SERIALIZATION_LOAD("Width"     , m_dWidth);
        SERIALIZATION_LOAD("Height"    , m_dHeight);
        SERIALIZATION_LOAD("Alpha"     , m_iAlpha);
        SERIALIZATION_LOAD("FileName"  , m_strImageName);
    MOCK_EXCEPTION_FILE(e_file_read);
}


/**
 * @brief   シリアル化(セーブ)
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  なし
 * @note	
 */
template<class Archive>
void CDrawingImage::save(Archive& ar, const unsigned int version) const
{
    SERIALIZATION_INIT;
        ar << BOOST_SERIALIZATION_BASE_OBJECT_NVP(CDrawingObject);

        SERIALIZATION_SAVE("Pos"       , m_Pt);
        SERIALIZATION_SAVE("Angle"     , m_dAngle);
        SERIALIZATION_SAVE("Skew"      , m_dSkew);
        SERIALIZATION_SAVE("Index"     , m_iDrawIndex);
        SERIALIZATION_SAVE("Width"     , m_dWidth);
        SERIALIZATION_SAVE("Height"    , m_dHeight);
        SERIALIZATION_SAVE("Alpha"     , m_iAlpha);
        {
            StdString strImageName;
            std::shared_ptr<CImageData> pImage;
            pImage = m_pImage.lock();
            if (pImage)
            {
                strImageName = pImage->GetFileName();
            }
            SERIALIZATION_SAVE("FileName"  , strImageName);
        }


        //SERIALIZATION_SAVE("ElementID" , m_uuidElemetDef);

   MOCK_EXCEPTION_FILE(e_file_write);
}

//*******************************
//             TEST
//*******************************
#ifdef _DEBUG



void TEST_CDrawingImage()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    /*
    MAT2D matA;

    POINT2D  pt(2.0, 3.0);
    POINT2D  ptMouse;
    
    //------------------------------------------------
    pt.Set(4.0,5.0);
    ptMouse.Set(6.1, 6.0);
    POINT2D ptFix(6.0, 4.0);
    matA.SetAffineParam(pt, 0.0, 1.0, 1.0);
    double dOrgWidth = 4.0;
    double dOrgHeight = 2.0;

    POINT2D ptRet;
    ptRet =  _CalcMoveNodeMarker(ptFix,
                                0.0,
                                ptMouse,
                                dOrgWidth,
                                dOrgHeight
                                );

    _CalcAffine(ptFix, 
                0.0,        //Angle
                dOrgWidth,  //OrgWidth
                dOrgHeight, //OrgHeight
                ptRet,
                &matA);


    POINT2D ptMat;
    double dAngleMat;
    double dSclXmat;
    double dSclYmat;


    matA.GetAffineParam(&ptMat, &dAngleMat, &dSclXmat, &dSclYmat);

    POINT2D ptAns;
    ptAns.Set(10.0, 6.0);
    STD_ASSERT_DBL_EQ(ptRet.Distance(ptAns), 0.0);
    STD_ASSERT_DBL_EQ(dSclXmat, -1.0);
    STD_ASSERT_DBL_EQ(dSclYmat,  1.0);



    ptMouse.Set(8.0, 8.0);
    ptRet =  _CalcMoveNodeMarker(ptFix,
                                0.0,
                                ptMouse,
                                dOrgWidth,
                                dOrgHeight
                                );

    STD_ASSERT_DBL_EQ(ptRet.Distance(ptAns), 0.0);


    _CalcAffine(ptFix, 
                0.0,        //Angle
                dOrgWidth,  //OrgWidth
                dOrgHeight, //OrgHeight
                ptRet,
                &matA);

    matA.GetAffineParam(&ptMat, &dAngleMat, &dSclXmat, &dSclYmat);


    ptAns.Set(14.0, 8.0);
    STD_ASSERT_DBL_EQ(ptRet.Distance(ptAns), 0.0);
    STD_ASSERT_DBL_EQ(dSclXmat, -1.0);
    STD_ASSERT_DBL_EQ(dSclYmat,  1.0);

    */

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}
#endif //_DEBUG

