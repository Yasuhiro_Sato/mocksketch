/**
 * @brief        CDrawingImageヘッダーファイル
 * @file	        CDrawingImage.h
 * @author           Yasuhiro Sato
 * @date	        07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
  *			
 *
 * $
 * $
 * 
 */
#ifndef __DRAWING_PICTURE_H_
#define __DRAWING_PICTURE_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DrawingObject/CDrawingObject.h"
#include "DrawingObject/Primitive/LINE2D.h"
#include "DrawingObject/Primitive/MAT2D.h"

/*---------------------------------------------------*/
/*  Classe                                           */
/*---------------------------------------------------*/
class CImageData;

/**
 * @class   CDrawingImage
 * @brief  
 */
class CDrawingImage :public CDrawingObject//:public CRTTIClass<CDrawingImage, CDrawingObject, DT_LINE>
{
public:


public:

    //!< コンストラクタ
    CDrawingImage();

    //!< コンストラクタ
    CDrawingImage(int nID);

    //!< コピーコンストラクタ
    CDrawingImage(const CDrawingImage& Obj);

    //!< デストラクタ
    virtual ~CDrawingImage();

    //!< 相対移動
    virtual void Move(const POINT2D& pt2D);

	//!< 絶対移動
	virtual void AbsMove(const POINT2D& pt2D);

    //!< 回転
    virtual void Rotate(const POINT2D& pt2D, double dAngle);

    //!< 交点計算
    virtual void CalcIntersection ( std::vector<POINT2D>* pLiat,   
        const CDrawingObject* pObj, 
        bool bOnline = true,
        double dMin = NEAR_ZERO) const;

    //!< 鏡像
	virtual void Mirror(const LINE2D& line);

    //!< 倍率
	virtual void Scl(const POINT2D& pt2D, double dXScl, double dYScl);

    //!< 行列適用
    virtual void Matrix(const MAT2D& mat2D);
    virtual void MatrixDirect(const MAT2D& mat2D);

    //!< 描画用（画面座標変換用）マトリクス取得
    //const MAT2D* GetDrawingMatrix()const{return  &m_matDraw;}

    //!< 調整
    virtual bool Trim (const POINT2D& ptIntersect, const POINT2D& ptClick, bool bReverse);

    //!< 分割
    virtual std::shared_ptr<CDrawingObject>  Break (const POINT2D& ptBreak);

    //!< 範囲
    virtual bool IsInner( const RECT2D& rcArea, bool bPart) const;

    //!< 描画
    virtual void Draw(CDrawingView* pView, 
                      const std::shared_ptr<CDrawingObject> pParentObject,
                      E_MOUSE_OVER_TYPE eMouseOver = E_MO_NO,
                      int iLayerId = -1) override;

    //!< 描画削除
    virtual void DeleteView(CDrawingView* pView,
                            const std::shared_ptr<CDrawingObject> pParentObject,
                            int iLayerId = -1) override;

    //!< 領域取得
    virtual RECT2D GetBounds() const;

    //!< イメージ読み込み
    bool SetImage(LPCTSTR flleName, bool bSetSize);


    //-------------------------
    // リフレクション設定
    //-------------------------
    //!< 位置設定
    void  SetPoint(POINT2D ptAbs);

    //!< 位置取得
    POINT2D  GetPoint()  const  {return m_Pt;}

    //!< 角度設定
    void   SetAngle(double dAngle)        {m_dAngle = dAngle;}

    //!< 角度取得
    double GetAngle() const               {return m_dAngle;}

    //!< アイコンインデックス設定
    void SetDrawIndex(int iIndex)    {m_iDrawIndex = iIndex;}

    //!< アイコンインデックス取得
    int GetDrawIndex() const         {return m_iDrawIndex;}

    //!< 透過率設定
    void SetAlpha(int iAlpha)    {m_iAlpha = iAlpha;}

    //!< 透過率取得
    int GetAlpha() const         {return m_iAlpha;}

    //!< 幅設定
    void SetWidth(double dWidth);

    //!< 幅取得
    double GetWidth() const         {return m_dWidth;}

    //!< 高さ設定
    void SetHeight(double dHeight);

    //!< 高さ取得
    double GetHeight() const         {return m_dHeight;}


    //!< スナップ点取得
    virtual  bool GetSnapList(std::list<SNAP_DATA>* pLstSnap)const;

    //!< プロパティ初期化
    TREE_GRID_ITEM*  _CreateStdPropertyTree();

    void SetMoveMode(bool bMove = true){m_bMoveMode = bMove;}

    bool CalcFramePoint(std::vector<POINT>* pLst, CDrawingView* pView);


    //-------------------
    //!< プロパティ変更
    //-------------------
    static bool PropPosition        (CStdPropertyItem* pData, void* pObj);
    static bool PropAngle           (CStdPropertyItem* pData, void* pObj);
    static bool PropDrawIndex       (CStdPropertyItem* pData, void* pObj);
    static bool PropAlpha           (CStdPropertyItem* pData, void* pObj);
    static bool PropWidth           (CStdPropertyItem* pData, void* pObj);
    static bool PropHeight          (CStdPropertyItem* pData, void* pObj);
    static bool PropFileName        (CStdPropertyItem* pData, void* pObj);

    //static bool PropUnitId       (CStdPropertyItem* pData, void* pObj);
    //-------------------

    //-------------------
    //-------------------
    // ドラッグ用マーカ
    //-------------------
    //!< マーカ初期化 true:マーカあり
    virtual bool InitNodeMarker(CNodeMarker* pMarker) override;

    //!< マーカ選択
    virtual void SelectNodeMarker(CNodeMarker* pMarker, 
                                   StdString strMarkerId) override;

    //!< マーカ移動
    virtual void MoveNodeMarker(CNodeMarker* pMarker, 
                                     SNAP_DATA* pSnap,
                                     StdString strMarkerId,
                                     MOUSE_MOVE_POS posMouse) override;

    //!< マーカ開放
    virtual bool ReleaseNodeMarker(CNodeMarker* pMarker, StdString strMarkerId, MOUSE_MOVE_POS posMouse) override;

    //!< ノード変更
    virtual void ChangeNode(StdString strMarkerId, E_NODE_CHANGE_TYPE eType) override;


    //-------------------------
    // 参照
    //-------------------------

    //!< 参照元の取得
    virtual CDrawingObject* UpdateRef();

protected:
    //!< エレメント描画(マウスオーバー)   
    virtual void DrawOver( CDrawingView* pView, bool bOver);

    //!< ロード終了後処理
    virtual void LoadAfter(CPartsDef* pDef) override;

    bool _SetSize();
    //!< 参照元


    static POINT2D _NearPalmLine(double dA, double dB, double dC, POINT2D pt);

    void _GetFeturePoint(POINT2D* pTL, POINT2D* pBR,
                         POINT2D* pTR, POINT2D* pBL ) const;

    bool _CalcFramePoint(std::vector<POINT2D>* pLst, CDrawingView* pView) const;

protected:

    //!< 位置
    POINT2D             m_Pt;

    //!< 角度
    double              m_dAngle;

    //!< 図形インデックス
    int                 m_iDrawIndex;

    //!< 
    bool                m_bDotByDot;

    double              m_dWidth;
    
    double              m_dHeight;


    int                 m_iAlpha;
    //!< 単位ID
    //int                 m_iUnitId;

    //!< ファイル名
    StdString          m_strImageName;

    //-----------------------------
    // 保存不要データ
    //-----------------------------
    //参照元UUID
    //mutable boost::uuids::uuid      m_uuidElemetDef;

    //!< アファイン変換マトリックス
    MAT2D               m_mat2D;

    MAT2D               m_matDraw;

    int                 m_iDrawLayerId;

    //!< 
    double              m_dOrgWidth;

    //!< 
    double              m_dOrgHeight;

    bool                m_bMoveMode;

    double              m_dMinScl;

    double              m_dSkew;

    mutable std::weak_ptr<CImageData> m_pImage;

    std::shared_ptr<CNodeBound>            m_psNodeBound;

    std::shared_ptr<CDrawingLine> m_psAdditionalLine1;
    std::shared_ptr<CDrawingLine> m_psAdditionalLine2;

private:
    friend class boost::serialization::access;  
    BOOST_SERIALIZATION_SPLIT_MEMBER();

    template<class Archive>
    void load(Archive& ar, const unsigned int version);

    template<class Archive>
    void save(Archive& ar, const unsigned int version) const;
};

BOOST_CLASS_VERSION(CDrawingImage, 0);
#endif // 