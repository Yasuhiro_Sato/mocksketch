/**
 * @brief			CDrawingLine実装ファイル
 * @file			CDrawingLine.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "DefinitionObject/View/CDrawingView.h"
#include "./CDrawingPoint.h"
#include "./CDrawingLine.h"
#include "./CDrawingCircle.h"
#include "./CDrawingDim.h"
#include "./CDrawingCircle.h"
#include "./CDrawingSpline.h"
#include "./CDrawingEllipse.h"
#include "./CDrawingCompositionLine.h"
#include "./CDrawingParts.h"
#include "./CDrawingNode.h"
#include "./CNodeMarker.h"
#include "View/CUndoAction.h"
#include "DefinitionObject/CPartsDef.h"
#include <math.h>
#include <boost/serialization/export.hpp> 

#include "Utility/CustomPropertys.h"
#include "Utility/CStdPropertyTree.h"
#include "Utility/CPropertyGrid.h"
#include "Utility/CUtility.h"
#include "System/CSystem.h"


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

BOOST_CLASS_EXPORT(CDrawingLine);

/**
 * コンストラクタ
 */

/**
 *  @brief   コンストラクタ
 *  @param   なし
 *  @retval  なし
 *  @note
 */
CDrawingLine::CDrawingLine():
CDrawingObject()
{
    m_eType  = DT_LINE;
    m_iWidth =    1;
    m_iLineType = PS_SOLID;
    m_iPropFeatures = P_BASE | P_LINE; 
    m_bSelPoint[0] = false;
    m_bSelPoint[1] = false;
}


/**
 *  @brief   コンストラクタ
 *  @param   [in] pLin2D
 *  @retval  なし
 *  @note
 */
CDrawingLine::CDrawingLine(const LINE2D* pLin2D):
CDrawingObject()
{
    m_eType  = DT_LINE;
    m_iWidth =    1;
    m_iLineType = PS_SOLID;
    m_Line = *pLin2D;
    m_iPropFeatures = P_BASE | P_LINE; 
    m_bSelPoint[0] = false;
    m_bSelPoint[1] = false;

}

/**
 * コピーコンストラクタ
 */
CDrawingLine::CDrawingLine(const CDrawingLine& Obj):
CDrawingObject( Obj)
{
    m_eType  = DT_LINE;
    m_iWidth    = Obj.m_iWidth;
    m_iLineType = Obj.m_iLineType;
    m_iPropFeatures = Obj.m_iPropFeatures; 
    m_Line      = Obj.m_Line;
    m_bSelPoint[0] = Obj.m_bSelPoint[0];
    m_bSelPoint[1] = Obj.m_bSelPoint[1];
}


/**
 *  @brief   コンストラクタ
 *  @param   [in] nID ID
 *  @retval  なし
 *  @note
 */
CDrawingLine::CDrawingLine(int nID):
CDrawingObject( nID, DT_LINE)
{
    m_iWidth = 1;
    m_iLineType = PS_SOLID;
    m_bSelPoint[0] = false;
    m_bSelPoint[1] = false;

};


/**
 *  @brief   デストラクタ
 *  @param   なし
 *  @retval  なし
 *  @note
 */

CDrawingLine::~CDrawingLine()
{

}


/**
 *  @brief  回転.
 *  @param  [in]    pPt2D    回転中心
 *  @param  [in]    dAngle   回転角
 *  @retval     なし
 *  @note
 */
void CDrawingLine::Rotate(const POINT2D& pt2D, double dAngle)
{
    AddChgCnt();
    m_Line.Rotate(pt2D, dAngle);
}

/**
 *  @brief  相対移動.
 *  @param  [in]    pPt2D   移動量
 *  @retval         なし
 *  @note
 */
void CDrawingLine::Move(const POINT2D& pt2D) 
{
    AddChgCnt();

    if (!m_bSelPoint[0] && !m_bSelPoint[1])
    {
        m_Line.Move(pt2D);
    }

    if (m_bSelPoint[0])
    {
        POINT2D pt = GetPoint1();
        pt = pt + pt2D;
        m_Line.SetPt1 ( pt );
    }

    if (m_bSelPoint[1])
    {
        POINT2D pt = GetPoint2();
        pt = pt + pt2D;
        m_Line.SetPt2 ( pt );
    }
}


/**
 *  @brief  鏡像.
 *  @param  [in]    line   対称軸
 *  @retval         なし
 *  @note
 */
void CDrawingLine::Mirror(const LINE2D& line)
{
    AddChgCnt();
    m_Line.Mirror(line);
}

/**
 *  @brief  倍率.
 *  @param  [in]    pPt2D   拡大中心
 *  @param  [in]    dSclX   X倍率
 *  @param  [in]    dSclY   Y倍率
 *  @retval         なし
 *  @note
 */
void CDrawingLine::Scl(const POINT2D& pt2D, double dXScl, double dYScl)
{
    AddChgCnt();
    m_Line.Scl(pt2D, dXScl, dYScl);
}

/**
 *  @brief  行列.
 *  @param  [in]    mat2D   適用する行列
 *  @retval         なし
 *  @note
 */
void CDrawingLine::Matrix(const MAT2D& mat2D)
{
    AddChgCnt();

    if (!m_bSelPoint[0] && !m_bSelPoint[1])
    {
        m_Line.Matrix(mat2D);
    }

    if (m_bSelPoint[0])
    {
        POINT2D pt = GetPoint1();
        pt.Matrix(mat2D);
        m_Line.SetPt1(pt);
    }

    if (m_bSelPoint[1])
    {
        POINT2D pt = GetPoint2();
        pt.Matrix(mat2D);
        m_Line.SetPt2(pt);
    }

}

/**
 *  @brief  交点計算.
 *  @param  [out]   pList   交点のリスト
 *  @param  [in]    pObj    交差するオブジェクト
 *  @retval         なし
 *  @note
 */
void CDrawingLine::CalcIntersection ( std::vector<POINT2D>* pList,   
                                const CDrawingObject* pObj, 
                                bool bOnline, 
                                double dMin)  const
{
    STD_ASSERT(pList != NULL);

    POINT2D ptTmp;

    pList->clear();
    if (!pObj)
    {
        return;
    }

    //レイヤーの確認
    if (!IsSameScale(pObj))
    {
        return;
    }

    const LINE2D* pLine = NULL;
    LINE2D* pMatLine = NULL;

    const MAT2D* pMat =  GetParentMatrix();
    const MAT2D* pMat2 =  pObj->GetParentMatrix();
    if (pMat == NULL)
    {
        pLine = &m_Line;
    }
    else
    {
        pMatLine = new LINE2D(m_Line);
        pMatLine->Matrix(*pMat);
        pLine = pMatLine;
    }


    try
    {
        switch (pObj->GetType())
        {
        case DT_POINT:
            {
            //近接点を算出
            const CDrawingPoint* ptObj = dynamic_cast<const CDrawingPoint*>(pObj);

            STD_ASSERT(ptObj != NULL);

            if (pMat2 != NULL)
            {
                POINT2D ptMat(*ptObj->GetPointInstance());
                ptMat.Matrix(*pMat2);
                ptTmp = pLine->NearPoint(ptMat);
            }
            else
            {
                ptTmp = pLine->NearPoint(*ptObj->GetPointInstance());
            }


            pList->push_back(ptTmp);
            }
            break;

        case DT_NODE:
            {
            //近接点を算出
            const CDrawingNode* ptObj = dynamic_cast<const CDrawingNode*>(pObj);

            STD_ASSERT(ptObj != NULL);

            if (pMat2 != NULL)
            {
                POINT2D ptMat(*ptObj->GetPointInstance());
                ptMat.Matrix(*pMat2);
                ptTmp = pLine->NearPoint(ptMat);
            }
            else
            {
                ptTmp = pLine->NearPoint(*ptObj->GetPointInstance());
            }

            pList->push_back(ptTmp);
            }
            break;

        //case DT_PARTS:
        //case DT_TEXT

        case DT_LINE:
            {

            //交点を算出
            const CDrawingLine* pDrawingLine = dynamic_cast<const CDrawingLine*>(pObj);

            STD_ASSERT(pLine != NULL);
            const LINE2D* pLine2;
            LINE2D* pLine2Instance = NULL;

            if (pMat2 != NULL)
            {
                pLine2Instance = new LINE2D(*pDrawingLine->GetLineInstance());
                pLine2Instance->Matrix(*pMat2);
                pLine2 = pLine2Instance;
            }
            else
            {
                pLine2 = pDrawingLine->GetLineInstance();
            }
        
            if (!bOnline)
            {
                try
                {
                    if(pLine->IntersectInfnityLine(&ptTmp, *pLine2))
                    {
                        pList->push_back(ptTmp);
                    }
                }
                catch(...)
                {
                    ;
                }
            }
            else
            {
                if (pLine->Intersect2(&ptTmp, *pLine2))
                {
                    pList->push_back(ptTmp);
                }
            }


            if (pLine2Instance)
            {
                delete pLine2Instance;
            }

            }
            break;

        case DT_CIRCLE:
            {
            //交点を算出
            const CDrawingCircle* pCircle = dynamic_cast<const CDrawingCircle*>(pObj);
            STD_ASSERT(pCircle != NULL);

            if (pMat2 != NULL)
            {
                ELLIPSE2D* pEllipse = NULL;
                CIRCLE2D circle(*pCircle->GetCircleInstance());
                circle.Matrix(*pMat2, &pEllipse);

                if (!pEllipse)
                {
                    circle.Intersect(*pLine, pList, bOnline, dMin);
                }
                else
                {
                    pEllipse->Intersect(*pLine, pList, bOnline, dMin);
                    delete pEllipse;
                }
            }
            else
            {
                pCircle->GetCircleInstance()->Intersect(*pLine, pList, bOnline);
             }

            break;
            }

        case DT_SPLINE:
            {
            //交点を算出
            const CDrawingSpline* pSpline = dynamic_cast<const CDrawingSpline*>(pObj);
            STD_ASSERT(pSpline != NULL);

            if (pMat2 != NULL)
            {
                SPLINE2D spline(*pSpline->GetSplineInstance());
                spline.Matrix(*pMat2);
                spline.Intersect( *pLine, pList, bOnline);

            }
            else
            {
                pSpline->GetSplineInstance()->Intersect( *pLine, pList, bOnline);
            }
            break;

            }

        case DT_ELLIPSE:
            {
            //交点を算出
            const CDrawingEllipse* pEllipse = dynamic_cast<const CDrawingEllipse*>(pObj);
            STD_ASSERT(pEllipse != NULL);
            if (pMat2 != NULL)
            {
                ELLIPSE2D ellipse(*pEllipse->GetEllipseInstance());
                ellipse.Matrix(*pMat2);
                ellipse.Intersect( *pLine, pList, bOnline);

            }
            else
            {
                pEllipse->GetEllipseInstance()->Intersect( *pLine, pList, bOnline);
            }
            }
            break;

        case DT_COMPOSITION_LINE:
        {
            //交点を算出
            const CDrawingCompositionLine* pCom = dynamic_cast<const CDrawingCompositionLine*>(pObj);
            STD_ASSERT(pCom != NULL);
            if (pMat2 == NULL)
            {
                pCom->CalcIntersection(pList, this, bOnline, dMin);
            }
            else
            {
                CDrawingCompositionLine com(*pCom);
                com.Matrix(*pMat2);
                com.CalcIntersection(pList, this, bOnline, dMin);
            }
        }

        default:
            break;
        }
    }
    catch(...)
    {
        STD_ASSERT(_T("DBG"));
        return;
    }

    if (bOnline)
    {
        //TODO:実装
    }

    if (pMatLine)
    {
        delete pMatLine;
    }
}


/**
 *  @brief  位置調整.
 *  @param  [in]    pPtIntersect   調整位置
 *  @param  [in]    pPtClick       調整方向指定点
 *  @param  [in]    bReverse       調整方向指定点
 *  @retval         なし
 *  @note
 */
bool CDrawingLine::Trim (const POINT2D& ptIntersect, const POINT2D& ptClick, bool bReverse)
{
    m_Line.Trim(ptIntersect, ptClick, bReverse);
    return true;
}

/**
 *  @brief  位置調整.
 *  @param  [in]    pPtIntersect   調整位置
 *  @param  [in]    pPtClick       調整方向指定点
 *  @param  [in]    bReverse       調整方向指定点
 *  @retval         なし
 *  @note
 */
bool CDrawingLine::TrimCorner (const POINT2D& ptIntersect, const POINT2D& ptClick)
{
    m_Line.TrimCorner(ptIntersect, ptClick);
    return true;
}

/**
 *  @brief  分割.
 *  @param  [in]    pPtBreak  分割位置
 *  @retval         分割によって出来た
 *  @note
 */
std::shared_ptr<CDrawingObject>  CDrawingLine::Break (const POINT2D& ptBreak)
{

    auto pLineBreak = std::make_shared<CDrawingLine>(*this);

    //選択を解除しておく
    pLineBreak->SetSelect(false);

    //!<  念のため近接点を取得
    POINT2D ptNear = m_Line.NearPoint(ptBreak);

    SetPoint2(ptNear);
    pLineBreak->SetPoint1(ptNear);
    AddChgCnt();

    return pLineBreak;
}


/**
 *  @brief  描画.
 *  @param  [in]  pView       表示View
 *  @param  [in]  bMouseOver  true オブジェクト上にマウスあり
 *  @retval         なし
 *  @note
 */
void CDrawingLine::Draw(CDrawingView* pView, 
                        const std::shared_ptr<CDrawingObject> pParentObject,
                        E_MOUSE_OVER_TYPE eMouseOver,
                        int iLayerId)
{
    COLORREF crObj;
    int      iID;

    bool bIgnoreSelect = false;
    if (eMouseOver == E_MO_IGNORE_SELECT)
    {
        bIgnoreSelect = true;
    }

    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }

    crObj = GetDrawColor(pView, iLayerId, pParentObject.get(), &iID, bIgnoreSelect);

    std::unique_ptr<LINE2D> upLine;
    const LINE2D* pLine = &m_Line;

    int iWidth = m_iWidth;

    if (eMouseOver == E_MO_SELECTABLE)
    {
        pView->SetLineAlternateMode(true);
        iWidth += 2;
    }
    else if (eMouseOver == E_MO_EMPHASIS)
    {
        crObj = DRAW_CONFIG->crEmphasis;
    }
	else if (eMouseOver == E_MO_CONNECTABLE)
	{
		crObj = DRAW_CONFIG->crConnectable;
	}


    //--------------------
    //  MATRIX
    //--------------------
    const MAT2D* pMat =  NULL;
    if (pParentObject )
    {
        pMat = pParentObject->GetDrawingMatrix();
    }

    if (pMat != NULL)
    {
        upLine = std::make_unique<LINE2D>(m_Line);
        upLine->Matrix(*pMat);
        pLine = upLine.get();
    }
    
    if (m_pMatOffset)
    {
        LINE2D tmpline(*pLine);
        if (m_bSelPoint[0])
        {
            tmpline.SetPt1(pLine->Pt(0) * (*m_pMatOffset));
        }

        if (m_bSelPoint[1])
        {
            tmpline.SetPt2(pLine->Pt(1) * (*m_pMatOffset));
        }

        Line(pView, iLayerId, &tmpline, m_iLineType, iWidth, crObj, iID);
    }
    else
    {
        Line(pView, iLayerId, pLine, m_iLineType, iWidth, crObj, iID);
    }


    if (eMouseOver == E_MO_SELECTABLE)
    {
        pView->SetLineAlternateMode(false);
    }

#if 0

DB_PRINT(_T("LINE Mode:%d Id:%d Cr%08x W:%d (%f,%f) - (%f,%f)\n"), pView->GetDrawMode(),
                                           iID, 
                                           crObj,
                                           iWidth,
                                           line.GetPt1().dX,
                                           line.GetPt1().dY,
                                           line.GetPt2().dX,
                                           line.GetPt2().dY);
#endif
}

//!< 選択状態設定
void CDrawingLine::SetSelect(bool bSel, const RECT2D* pRect)
{
    CDrawingObject::SetSelect(bSel, pRect);

    if (bSel)
    {
        m_bSelPoint[0] = true;
        m_bSelPoint[1] = true;
    }
    else
    {
        m_bSelPoint[0] = false;
        m_bSelPoint[1] = false;
    }

    if (pRect && bSel)
    {
        if (!pRect->IsInside (GetPoint1()))
        {
            m_bSelPoint[0] = false;
        }

        if (!pRect->IsInside (GetPoint2()))
        {
            m_bSelPoint[1] = false;
        }
    }
}

//!< 選択状態設定(ActionMove時にコピーしたものをストレッチするために使用)
void CDrawingLine::SetSelectNoRelSelPoint(bool bSel)
{
    CDrawingObject::SetSelect(bSel);
}

/**
 *  @brief  描画削除.
 *  @param  [in]    pView   表示View
 *  @retval         なし
 *  @note
 */
void CDrawingLine::DeleteView(CDrawingView* pView, 
                            const std::shared_ptr<CDrawingObject> pParentObject,
                            int iLayerId)
{
    //背景色
    COLORREF crObj;
    crObj = DRAW_CONFIG->GetBackColor();

    LINE2D line(m_Line);
    //--------------------
    //  MATRIX
    //--------------------
    const MAT2D* pMat = NULL;
    if (pParentObject )
    {
        pMat = pParentObject->GetDrawingMatrix();
    }

    if(iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }

    if (pMat != NULL)
    {
        line.Matrix(*pMat);
    }

    Line(pView, iLayerId, &line, m_iLineType, m_iWidth, crObj, -1);
}

POINT2D*  CDrawingLine::GetPoint1As() 
{
    POINT2D* pPt;
    pPt = m_Line.PtInstance(0);
    return pPt;
}


POINT2D*  CDrawingLine::GetPoint2As() 
{
    POINT2D* pPt;
    pPt =  m_Line.PtInstance(1);
    return pPt;
}


/**
 *  @brief  線幅設定.
 *  @param  [in]    iWidth   線幅
 *  @retval         なし
 *  @note
 */
void CDrawingLine::SetLineWidth(int iWidth)
{
    m_iWidth = iWidth;
}

/**
 *  線幅取得.
 *  @param          なし
 *  @retval         線幅
 *  @note
 */
int CDrawingLine::GetLineWidth() const
{
    return m_iWidth;
}

/**
 *  線種設定.
 *  @param  [in]    iType   線種
 *  @retval         なし
 *  @note
 */
void CDrawingLine::SetLineType(int iType)
{
    m_iLineType = iType;
}

/**
 *  線種取得.
 *  @param          なし 
 *  @retval         線種
 *  @note
 */
int  CDrawingLine::GetLineType() const
{
    return m_iLineType;
}

/**
 *  リフレクション設定.
 *  @param      なし
 *  @retval     なし
 *  @note
 */
void CDrawingLine::RegisterReflection()
{
    /*
	//RegisterProperty<POINT2D>       ( _T("Point1")   , &GetPoint1     , &SetPoint1 );
	//RegisterProperty<POINT2D>       ( _T("Point2")   , &GetPoint2     , &SetPoint2 );
	//RegisterProperty<int>           ( _T("Width")       , &GetWidth      , &SetWidth );
	//RegisterProperty<int>           ( _T("LineType")    , &GetLineType   , &SetLineType );

	RegisterProperty<POINT2D>       ( "Point1"   , &GetPoint1     , &SetPoint1 );
	RegisterProperty<POINT2D>       ( "Point2"   , &GetPoint2     , &SetPoint2 );
	RegisterProperty<int>           ( "Width"       , &GetWidth      , &SetWidth );
	RegisterProperty<int>           ( "LineType"    , &GetLineType   , &SetLineType );
    */
}

//!< 範囲
CDrawingObject::RELATION CDrawingLine::RelationRect(const RECT2D& rcArea) const 
{
    //--------------------
    //  MATRIX
    //--------------------
    const MAT2D* pMat = GetParentMatrix();
    LINE2D tmpLine(m_Line);

    if (pMat != NULL)
    {
        tmpLine.Matrix(*pMat);
    }

    //--------------------
    RECT2D rcLine(tmpLine.Pt(0), tmpLine.Pt(1));
    if (rcArea.IsInside(rcLine))
    {
        return REL_INNER;
    }

    RELATION re = REL_NONE;
    //一方のみ端点が含まれている場合はストレッチ可能
    if (rcArea.IsInside(tmpLine.Pt(0)))
    {
        return REL_PART_STRATCH;
    }
    
    if (rcArea.IsInside(tmpLine.Pt(1)))
    {
        return REL_PART_STRATCH;
    }

    // それ以外で一部直線を含む場合
    POINT2D  pt;
    LINE2D line1(rcArea.dLeft, rcArea.dTop, rcArea.dLeft, rcArea.dBottom);
    LINE2D line2(rcArea.dRight, rcArea.dTop, rcArea.dRight, rcArea.dBottom);
    LINE2D line3(rcArea.dLeft, rcArea.dTop, rcArea.dRight, rcArea.dTop);
    LINE2D line4(rcArea.dLeft, rcArea.dBottom, rcArea.dRight, rcArea.dBottom);

    if (tmpLine.Intersect2(&pt, line1)) { return REL_PART; }
    if (tmpLine.Intersect2(&pt, line2)) { return REL_PART; }
    if (tmpLine.Intersect2(&pt, line3)) { return REL_PART; }
    if (tmpLine.Intersect2(&pt, line4)) { return REL_PART; }

    return REL_NONE;
}

/**
 *  範囲.
 *  @param      [in] p1 1点目
 *  @param      [in] p2 2点目
 *  @retval     true:2点で示される矩形内に点が存在する 
 *  @note
 */
bool CDrawingLine::IsInner( const RECT2D& rcArea, bool bPart) const
{
	auto ret = RelationRect(rcArea);

	if (bPart)
	{
		if (ret != REL_NONE)
		{
			return true;
		}
	}
	else
	{
		if (ret == REL_INNER)
		{
			return true;
		}

	}
    return false;
}

/**
 * @brief  線描画(マウスオーバー時)
 * @param  [in] pView 表示View      
 * @param  [in] bOver true:開始 false:終了
 * @retval   なし
 * @note	
 */
void CDrawingLine::DrawOver(CDrawingView* pView, bool bOver)
{

}

/**
 *  @brief   プロパティ初期化.
 *  @param   [in] pCtrl オブジェクトコントロールへのポインタ
 *  @retval     なし
 *  @note
 */
TREE_GRID_ITEM*  CDrawingLine::_CreateStdPropertyTree()
{
    NAME_TREE_ITEM<CStdPropertyItem>* pBeforeGroup;
    NAME_TREE_ITEM<CStdPropertyItem>* pNextItem;
    NAME_TREE_ITEM<CStdPropertyItem>* pGroupItem;
    NAME_TREE<CStdPropertyItem>* pTree;
    //int iId;
    CStdPropertyItem* pItem;
    boost::tuple< CStdPropertyItem*, void*> pairVal;


    pBeforeGroup = CDrawingObject::_CreateStdPropertyTree();
    pTree = m_psProperty->GetTreeInstance();

    pGroupItem  = pTree->AddNext(pBeforeGroup, NULL, GET_STR(STR_PRO_LINE), _T("Line"));



    //------------
    //端点１
    //------------
    CStdPropertyItemDef DefP1(PROP_POINT2D, 
        GET_STR(STR_PRO_P1)   , 
        _T("P1"),
        GET_STR(STR_PRO_INFO_P1), 
        true,
        DISP_UNIT,
        POINT2D());

    m_ptTmp1 = m_Line.Pt(0);
    pItem = new CStdPropertyItem( DefP1, PropP1, m_psProperty, NULL, &m_ptTmp1);
    pNextItem = pTree->AddChild(pGroupItem, pItem, DefP1.strDspName, pItem->GetName(), 2);

    //------------
    //端点２
    //------------
    CStdPropertyItemDef DefP2(PROP_POINT2D, 
        GET_STR(STR_PRO_P2)   , 
        _T("P2"),
        GET_STR(STR_PRO_INFO_P2), 
        true,
        DISP_UNIT,
        POINT2D());

    m_ptTmp2 = m_Line.Pt(1);
    pItem = new CStdPropertyItem( DefP2, PropP2, m_psProperty, NULL, &m_ptTmp2);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefP2.strDspName, pItem->GetName(), 2);

    //------------
    //線幅
    //------------
    CStdPropertyItemDef DefLineWidth(PROP_LINE_WIDTH, 
        GET_STR(STR_PRO_LINE_WIDTH)   ,
        _T("Width"),
        GET_STR(STR_PRO_INFO_LINE_WIDTH), 
        true,
        DISP_UNIT,
        0);

    pItem = new CStdPropertyItem( DefLineWidth, PropWidth, m_psProperty, NULL, &m_iWidth);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefLineWidth.strDspName, pItem->GetName());

    //------------
    //線種
    //------------
    CStdPropertyItemDef DefLineType(PROP_LINE_TYPE, 
        GET_STR(STR_PRO_LINE_TYPE)   , 
        _T("Type"),
        GET_STR(STR_PRO_INFO_LINE_TYPE), 
        false,
        DISP_UNIT,
        0);

    pItem = new CStdPropertyItem( DefLineType, PropLineType, m_psProperty, NULL, &m_iLineType);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefLineType.strDspName, pItem->GetName());

    return pGroupItem;
}


/**
 *  @brief   プロパティ値更新
 *  @param   なし
 *  @retval  なし
 *  @note    
 */
void CDrawingLine::_PrepareUpdatePropertyGrid()
{
    m_ptTmp1 = m_Line.Pt(0);
    m_ptTmp2 = m_Line.Pt(1);
    CDrawingObject::_PrepareUpdatePropertyGrid();

}

/**
 *  @brief   プロパティ変更(端点1)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingLine::PropP1 (CStdPropertyItem* pData, void* pObj)
{
    CDrawingLine* pDrawing = reinterpret_cast<CDrawingLine*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->m_ptTmp1 = pData->anyData.GetPoint();
        pDrawing->SetPoint1(pDrawing->m_ptTmp1);
        //pDrawing->m_pCtrl->Draw(pDrawing->m_nId);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(端点2)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingLine::PropP2 (CStdPropertyItem* pData, void* pObj)
{
    CDrawingLine* pDrawing = reinterpret_cast<CDrawingLine*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->m_ptTmp2 = pData->anyData.GetPoint();
        pDrawing->SetPoint2(pDrawing->m_ptTmp2);
        //pDrawing->m_pCtrl->Draw(pDrawing->m_nId);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   線幅
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingLine::PropWidth      (CStdPropertyItem* pData, void* pObj)
{
    CDrawingLine* pDrawing = reinterpret_cast<CDrawingLine*>(pObj);
    try
    {
        int iWidth;
        iWidth = pData->anyData.GetInt();
        pDrawing->AddChgCnt();
        pDrawing->SetLineWidth(iWidth);
        pDrawing->GetPartsDef()->Redraw();
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   線種
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingLine::PropLineType   (CStdPropertyItem* pData, void*pObj)
{
    CDrawingLine* pDrawing = reinterpret_cast<CDrawingLine*>(pObj);
    try
    {
        int iType;
        iType = pData->anyData.GetInt();
        pDrawing->AddChgCnt();
        pDrawing->SetLineType(iType);
        pDrawing->GetPartsDef()->Redraw();
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
    }
    return true;

}


/**
 * @brief   領域取得
 * @param   なし
 * @retval  なし
 * @note	描画領域を取得する
 */
RECT2D CDrawingLine::GetBounds() const
{
    return m_Line.GetBounds();
}

/**
 * @brief   距離
 * @param   [in] pt 
 * @retval  近接点からの距離
 * @note
 */
double CDrawingLine::Distance(const POINT2D& pt) const
{
    POINT2D ptNear = m_Line.NearPoint( pt, true /*bOnline*/);
    return pt.Distance(ptNear);
}

/**
 *  @brief   参照データに基づいて更新.
 *  @param   
 *  @retval  
 *  @note
 */
CDrawingObject* CDrawingLine::UpdateRef()
{
    CDrawingObject*      pRef;

    pRef = CDrawingObject::UpdateRef();

    if (pRef == NULL)
    {
        return NULL;
    }

    CDrawingLine* pLine = dynamic_cast<CDrawingLine*>(pRef);

    STD_ASSERT(pLine != NULL);

    if (pLine == NULL)
    {
        return false;
    }


    m_Line   = pLine->m_Line;
    m_iWidth    = pLine->m_iWidth;
    m_iLineType = pLine->m_iLineType;

    m_iChgCnt = pRef->GetChgCnt();
    return pRef;
}

/**
 *  @brief   始点終点交換.
 *  @param   なし
 *  @retval  なし
 *  @note    Line,Circle,Splie,CompositLine
 */
bool  CDrawingLine::SwapStartEnd()
{
    POINT2D pt1 = m_Line.GetPt1();
    POINT2D pt2 = m_Line.GetPt2();
    m_Line.SetPt(pt2,pt1);
    return true;
}

/**
 *  @brief   始点取得.
 *  @param   なし
 *  @retval  始点
 *  @note    Line,Circle,Splie,CompositLine
 */
POINT2D  CDrawingLine::GetStartSide() const
{
    return m_Line.GetPt1();
}

/**
 *  @brief   終点取得.
 *  @param   なし
 *  @retval  始点
 *  @note    Line,Circle,Splie,CompositLine
 */
POINT2D  CDrawingLine::GetEndSide() const
{
    return m_Line.GetPt2();
}

/**
*  @brief   中点取得.
*  @param   なし
*  @retval  中点
*  @note    Line,Circle,Splie,CompositLine
*/
POINT2D  CDrawingLine::GetMidPoint() const
{
    return GetPointByRate(0.5);
}

POINT2D  CDrawingLine::GetPointByRate(double d) const
{
    return (m_Line.GetPt1() * (1.0 - d) + m_Line.GetPt2() * d);

}

//!<  近接点取得(Line,Circle,Splie,CompositLine用)
bool  CDrawingLine::GetNearPoints(POINT2D*  ptNear, const POINT2D& pt, bool bOnline) const
{
    POINT2D ptRet = m_Line.NearPoint(pt, bOnline);
    *ptNear = ptRet; 
    return true;
}

//!<  法線角取得(Line,Circle,Splie,CompositLine用)
bool  CDrawingLine::GetNormAngle(double* pAngle, const POINT2D& pt, bool bOnline) const
{
    *pAngle = m_Line.Angle();
    return true;
}

double CDrawingLine::Length(const POINT2D& ptIntersect, bool bStart) const
{
    double dRet;
    POINT2D pt = m_Line.NearPoint(ptIntersect, true);

    if(bStart)
    {
        dRet = m_Line.GetPt1().Distance(pt);
    }
    else
    {
        dRet = m_Line.GetPt2().Distance(pt);
    }
    return dRet;
}

//!< オフセット(線,円弧,スプライン上)
bool CDrawingLine::Offset(double dLen, POINT2D ptDir)
{
    m_Line = m_Line.Offset(dLen, ptDir);
    return true;
}

//!< オフセット(線,円弧,スプライン上)
bool CDrawingLine::Offset(double dLen)
{
   //進行方向右側を正とする
    POINT2D ptDir;
    
    ptDir = (m_Line.GetPt2() + m_Line.GetPt1()) / 2.0;

    POINT2D ptVec = m_Line.GetPt2() - m_Line.GetPt1();
    POINT2D ptNorm;

    if(dLen >  0)
    {
        ptNorm.dX =  ptVec.dY;
        ptNorm.dY =  -ptVec.dX;
    }
    else
    {
        ptNorm.dX =  -ptVec.dY;
        ptNorm.dY =  ptVec.dX;
    }

    ptDir += ptNorm;
    Offset( fabs(dLen), ptDir);
 //DB_PRINT(_T("CDrawingLine::Offset %f %s\n"), dLen, ptDir.Str().c_str());

    return true;
}

//!< 点が進行方向の右側にあるか
bool CDrawingLine::IsRightSide(POINT2D ptDir) const
{
    POINT2D ptNear = m_Line.NearPoint(ptDir);
    POINT2D pt =  ptDir - ptNear;
    POINT2D ptVec = m_Line.GetPt2() - m_Line.GetPt1();
    
    double dCross = ptVec.Cross(pt);

    bool bRet = true;    
    if(dCross >= 0.0)
    {
        bRet = false;
    }

    return bRet;
}


/**
 *  @brief   スナップ点取得.
 *  @param   [out] pLstSnap スナップ点リスト
 *  @retval  true: スナップ点あり
 *  @note
 */
bool CDrawingLine::GetSnapList(std::list<SNAP_DATA>* pLstSnap)const 
{
    using namespace VIEW_COMMON;

    DWORD dwSnap = DRAW_CONFIG->GetSnapType();
    bool bRet = false;

    STD_ASSERT(pLstSnap);

    if (!pLstSnap)
    {
        return false;
    }

    pLstSnap->clear();
    SNAP_DATA snapData;
    snapData.bKeepSelect = false;

    if (dwSnap & SNP_END_POINT)
    {
        snapData.eSnapType = SNP_END_POINT;
        snapData.pt = m_Line.Pt(0);
        pLstSnap->push_back(snapData);

        snapData.pt = m_Line.Pt(1);
        pLstSnap->push_back(snapData);
        bRet = true;
    }

    if (dwSnap & SNP_MID_POINT)
    {
        snapData.eSnapType = SNP_MID_POINT;
        snapData.pt = (m_Line.Pt(0) + m_Line.Pt(1)) / 2.0;
        pLstSnap->push_back(snapData);
        bRet = true;
    }
    return bRet;
}

//!< マーカ初期化 true:マーカあり
bool CDrawingLine::InitNodeMarker(CNodeMarker* pMarker)
{
    CDrawingView* pView = pMarker->GetView();
    pView->ClearDraggingNodes();
    pView->ClearDraggingTmpObjects();

    auto lstNode = pView->GetDraggingNodes();
    lstNode->resize(ND_MAX);
    for(auto& pNode: *lstNode)
    {
        pNode = std::make_shared<CDrawingNode>();
        pNode->SetVisible(true);
        pNode->SetMarkerShape(DMS_CIRCLE_FILL);
    }

    lstNode->at(ND_PT1)->SetName(_T("ND_PT1"));
    lstNode->at(ND_PT2)->SetName(_T("ND_PT2"));
    lstNode->at(ND_C)->SetName(_T("ND_C"));

    STD_ASSERT(pMarker);

    double dLen = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->GetMarkerSize());  
    int iR =  CUtil::Round(dLen * pView->GetDpu());
    pMarker->SetRadius(iR);
    pMarker->SetObject(shared_from_this());

    POINT2D ptCneter = (GetPoint1() + GetPoint2()) / 2.0;
	lstNode->at(ND_PT1)->SetPoint(GetPoint1());
	lstNode->at(ND_PT2)->SetPoint(GetPoint2());
	lstNode->at(ND_C)->SetPoint(ptCneter);


    //ドラッグ表示用の図形を用意する
    auto lstTmpObject = pView->GetDraggingTmpObjects();
    auto tmpLine = std::make_shared<CDrawingLine>(*this);
    tmpLine->SetColor(DRAW_CONFIG->crImaginary);
    lstTmpObject->push_back(tmpLine); //表示用

    //補助線
    auto lineAux = std::make_shared<CDrawingLine>();
    lineAux->SetColor(DRAW_CONFIG->crAdditional);
    lineAux->SetLineType(PS_DOT);
    lineAux->SetPoint1(ptCneter);
    lstTmpObject->push_back(lineAux); 

    


    for(auto& pNode: *lstNode)
    {
        pMarker->Create(pNode.get(), pNode->GetPoint());
    }

    return true;
}

//!< マーカ選択
 void CDrawingLine::SelectNodeMarker(CNodeMarker* pMarker, 
                       StdString strMarkerId)
{
    pMarker->SetVisibleOnly( strMarkerId, true);

}

 CDrawingLine::E_NODE CDrawingLine::GetNode(StdString strMarkerId) const
{
    E_NODE eRet;

    if      (strMarkerId == _T("ND_PT1")){eRet = ND_PT1;}
    else if (strMarkerId == _T("ND_PT2")){eRet = ND_PT2;}
    else if (strMarkerId == _T("ND_C")){eRet = ND_C;}
    else {eRet = ND_NONE;}
    return eRet;
}

//!< マーカ移動
void CDrawingLine::MoveNodeMarker(CNodeMarker* pMarker, 
                                 SNAP_DATA* pSnap,
                                 StdString strMarkerId,
                                 MOUSE_MOVE_POS posMouse)
{

	using namespace VIEW_COMMON;

    CDrawingView* pView = pMarker->GetView();



    SNAP_DATA snap;
    DWORD dwSnapType = DRAW_CONFIG->GetSnapType();

    bool bSnap = pView->GetSnapPoint(&snap, 
                      dwSnapType,
                      posMouse.ptSel);

    CPartsDef*   pDef  = pView->GetPartsDef();
    auto pObj1 = pDef->GetObjectById(snap.iObject1);
    auto pObj2 = pDef->GetObjectById(snap.iObject2);

    auto lstTmpObject = pView->GetDraggingTmpObjects();
    auto pLineTmp = std::dynamic_pointer_cast<CDrawingLine>(lstTmpObject->at(0));
    pLineTmp->SetColor(DRAW_CONFIG->crImaginary);

    auto pLineAux = std::dynamic_pointer_cast<CDrawingLine>(lstTmpObject->at(1));


    auto pNode = pMarker->GetDrawingNode(strMarkerId);

    E_NODE eNode = GetNode(strMarkerId);

    bool bIgnoreSnap = false;
	bool bAngle = true;
	bool bLength = true;

    if (posMouse.nFlags & MK_SHIFT)
    {
        //スナップ無視
        bIgnoreSnap = true;
		bAngle = false;
        pObj1 = NULL;
		bLength = false;
        pSnap->eSnapType = SNP_NONE;
        pSnap->pt = pSnap->ptOrg;
    }

    //右ボタンドラッグ(移動・コピー選択)できるのはセンターのみ
    if (pMarker->GetMouseButton() == SEL_RBUTTON)
    {
        if( eNode != ND_C)
        {
            return;
        }
    }

    auto GetSnap = [=](StdString* pStrSnap, POINT2D ptStart, POINT2D ptOrg, double dOffsetAngle)
    {
        bool bRet = false;
        POINT2D ptNew;
        StdString strToolTip;
        double dAngle;
        double dLength;

  
        if ((!bSnap) &&  
        (!bIgnoreSnap))
        {
            bRet =pView->GetLengthSnapPoint(&dAngle,
								            &dLength,
								            &ptNew,  
								            ptStart,  
								            ptOrg,
								            bAngle,
								            bLength,
                                            dOffsetAngle);
        }
        else
        {
            ptNew = pSnap->pt;
            *pStrSnap = GetSnapName(snap.eSnapType, false);
            *pStrSnap += _T("\n");

            dLength  = ptStart.Distance(snap.pt);
            dAngle   = atan2(snap.pt.dY-ptStart.dY,
                             snap.pt.dX-ptStart.dX) * RAD2DEG;
        }


		*pStrSnap += CViewAction::GetSnapText(SNP_ANGLE, 
													dLength,
													dAngle,
													false);

        return  ptNew;
    };

    POINT2D ptNew;
    if (eNode == ND_C)
    {
        POINT2D ptCenter = (GetPoint1() + GetPoint2()) / 2.0;


        double dOffsetAngle = 0;
        dOffsetAngle = CUtil::GetAngle(GetPoint1(), GetPoint2());
        dOffsetAngle = CUtil::NormAngle2(dOffsetAngle);
        dOffsetAngle += 90.0;

        ptNew = GetSnap(&snap.strSnap, ptCenter, snap.ptOrg, dOffsetAngle);

        pLineAux->SetPoint2(ptNew);
        POINT2D ptDiff = ptCenter - ptNew ;

        pLineTmp->SetPoint1(GetPoint1() - ptDiff);
        pLineTmp->SetPoint2(GetPoint2() - ptDiff);
        pDef->SetMouseEmphasis(pLineAux);

    }
    else if (eNode == ND_PT1)
    {
        ptNew = GetSnap(&snap.strSnap, GetPoint1(), snap.ptOrg, 0.0);
        pLineTmp->SetPoint1(ptNew);
    }
     else if (eNode == ND_PT2)
    {
        ptNew = GetSnap(&snap.strSnap, GetPoint2(), snap.ptOrg, 0.0);
        pLineTmp->SetPoint2(ptNew);
    }
    
    pDef->SetMouseOver(pLineTmp);
    pNode->SetPoint(ptNew);
DB_PRINT(_T("CDrawingLine::MoveNodeMarker ptNew(%s) --\n"), ptNew.Str().c_str());

    pSnap->strSnap = snap.strSnap;
}

//!< マーカ開放
bool CDrawingLine::ReleaseNodeMarker(CNodeMarker* pMarker, StdString strMarkerId, MOUSE_MOVE_POS posMouse)
{
    //右ボタンドラッグ(移動・コピー選択)できるのはセンターのみ
    CDrawingView* pView = pMarker->GetView();

    StdString strId = pMarker->GetSelectId();
    E_NODE eNode = GetNode(strId);
    SEL_MENU eMenu = SEL_MNU_MOVE;
    if ((pMarker->GetMouseButton() == SEL_RBUTTON)&&
        ( eNode == ND_C))
    {
        eMenu = _ShowMoveCopyMenu(pMarker);
    }

    CPartsDef*   pDef  = pView->GetPartsDef();
    CUndoAction*       pUndo;
    pUndo  = pDef->GetUndoAction();

    SetSelect(false);

    auto lstTmpObject = pView->GetDraggingTmpObjects();
    auto pLineTmp = std::dynamic_pointer_cast<CDrawingLine>(lstTmpObject->at(0));

    bool bUndo = false;
    if (eMenu == SEL_MNU_MOVE)
    {
        pUndo->AddStart(UD_CHG, pDef, this);
        SetPoint1(pLineTmp->GetPoint1());
        SetPoint2(pLineTmp->GetPoint2());
        pUndo->AddEnd(shared_from_this(), true);
        bUndo = true;
    }
    else if (eMenu == SEL_MNU_COPY)
    {
        auto pNewLine = std::make_shared<CDrawingLine>( *pLineTmp );

        pNewLine->SetColor(GetColor());
        pNewLine->SetLineType(GetLineType());
        pNewLine->SetLineWidth(GetLineWidth());
        pNewLine->SetLayer(GetLayer());

        pDef->RegisterObject(pNewLine, true, false);
        pUndo->Add(UD_ADD, pDef, NULL, pNewLine);
        bUndo = true;
    }

    if (bUndo)
    {
        pUndo->Push();
    }

    pView->ClearDraggingNodes();
    pView->ClearDraggingTmpObjects();

    ::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
    pMarker->Clear();
    return true;
}


//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void TEST_CDrawingLine()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}
#endif //_DEBUG