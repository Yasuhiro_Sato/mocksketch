/**
 * @brief			CDrawingLineヘッダーファイル
 * @file			CDrawingLine.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#if !defined(EA_355B8576_BE84_4562_B3C9_07216543D721__INCLUDED_)
#define EA_355B8576_BE84_4562_B3C9_07216543D721__INCLUDED_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DrawingObject/CDrawingObject.h"

#include "DrawingObject/Primitive/LINE2D.h"

/**
 * @class   CDrawingLine
 * @brief   直線                     
 */
class CDrawingLine :public CDrawingObject//:public CRTTIClass<CDrawingLine, CDrawingObject, DT_LINE>
{
    enum E_NODE
    {
        ND_NONE = -1,
        ND_PT1        = 0,
        ND_PT2        = 1,
        ND_C          = 2,
        ND_MAX
    };

public:
	//!< コンストラクタ
	CDrawingLine();

    CDrawingLine(const LINE2D* pLin2D);

	//!< コンストラクタ
    CDrawingLine(int nID);

    //!< コピーコンストラクタ
    CDrawingLine(const CDrawingLine& Obj);

	//!< デストラクタ
	virtual ~CDrawingLine();

    //!< 絶対移動
    //virtual void AbsMove(const POINT2D& pt2D);

	//!< (相対)移動
	virtual void Move(const POINT2D& pt2D);

    //!< 回転
	virtual void Rotate(const POINT2D& pt2D, double dAngle);

    //!< 鏡像
	virtual void Mirror(const LINE2D& line);

    //!< 倍率
	virtual void Scl(const POINT2D& pt2D, double dXScl, double dYScl);

    //!< 行列適用
    virtual void Matrix(const MAT2D& mat2D);

    //!< 交点計算
    virtual void CalcIntersection ( std::vector<POINT2D>* pList,   
                                    const CDrawingObject* pObj, 
                                    bool bOnline = true,
                                    double dMin = NEAR_ZERO)const;

	//!< 調整
    virtual bool Trim (const POINT2D& ptIntersect, const POINT2D& ptClick, bool bReverse);

    //!< 調整          
    virtual bool TrimCorner (const POINT2D& ptIntersect, const POINT2D& ptClick);

    //!< 分割
    virtual std::shared_ptr<CDrawingObject>  Break (const POINT2D& ptBreak);

	//!< 範囲
    virtual bool IsInner( const RECT2D& rcArea, bool bPart) const;

    //!< 範囲
    virtual RELATION RelationRect(const RECT2D& rcArea) const override;

     //!< 描画
    virtual void Draw(CDrawingView* pView, 
                      const std::shared_ptr<CDrawingObject> pParentObject,
                      E_MOUSE_OVER_TYPE eMouseOver = E_MO_NO,
                      int iLayerId = -1) override;

    //!< 描画削除
    virtual void DeleteView(CDrawingView* pView,
                            const std::shared_ptr<CDrawingObject> pParentObject,
                            int iLayerId = -1) override;

    //!< 領域取得
    virtual RECT2D GetBounds() const;

    //!< 距離
    virtual double Distance(const POINT2D& pt) const;

    //!< 選択状態設定
    virtual void SetSelect(bool bSel, const RECT2D* pRect = NULL);

    virtual void SetSelectNoRelSelPoint(bool bSel);

    //-------------------------
    // リフレクション設定
    //-------------------------

    void  SetPoint(double dX1, double dY1, double dX2, double dY2)
                                         {m_Line.SetPt(dX1, dY1, dX2, dY2);} 

    void  SetPoint(const POINT2D& p1, const POINT2D& p2)
    {
        m_Line.SetPt(p1, p2);
    }

    void  SetLine(const LINE2D& line)
    {
        m_Line = line;
    }

    //!< 位置設定
    void  SetPoint1(const POINT2D& ptPos)       {m_Line.SetPt1(ptPos);} 

    //!< 位置取得
    POINT2D  GetPoint1() const            {return m_Line.Pt(0);}
    POINT2D*  GetPoint1As();

    //!< 位置設定
    void  SetPoint2(const POINT2D& ptPos)       {m_Line.SetPt2(ptPos);} 

    //!< 位置取得
    POINT2D  GetPoint2() const           {return m_Line.Pt(1);}
    POINT2D*  GetPoint2As();

    //!< 位置設定
    void  SetPoint1Ref(const POINT2D* pPos)       {m_Line.SetPt1(*pPos);} 

    //!< 位置設定
    void  SetPoint2Ref(const POINT2D* pPos)       {m_Line.SetPt2(*pPos);} 

    //!< 線幅設定
    void SetLineWidth(int iWidth);

    //!< 線幅取得
    int GetLineWidth() const;

    //!< 線種設定
    void SetLineType(int iType); 

    //!< 線種取得
    int  GetLineType() const;

    //!< 線取得
    LINE2D*  GetLineInstance()                     {return &m_Line;}
    const LINE2D*  GetLineInstance()  const        {return &m_Line;}

    //!< リフレクション設定
    static void RegisterReflection();

    //!< スナップ点取得
    virtual  bool GetSnapList(std::list<SNAP_DATA>* pLstSnap)const override;

    //!< 始点終点交換(Line,Circle,Splie,CompositLine用)
    virtual bool  SwapStartEnd() override;

    //!< 始点取得(Line,Circle,Splie,CompositLine用)
    virtual POINT2D  GetStartSide() const override;

    //!< 始点取得(Line,Circle,Splie,CompositLine用)
    virtual POINT2D  GetEndSide() const override;

    //!<  中点取得(Line,Circle,Splie,CompositLine用)
    virtual POINT2D  GetMidPoint() const override;

    //!<  任意点取得(Line,Circle,Splie,CompositLine用)
    virtual POINT2D  GetPointByRate(double d) const override;

    //!<  近接点取得(Line,Circle,Splie,CompositLine用)
    virtual bool  GetNearPoints(POINT2D*  ptNear, const POINT2D& pt, bool bOnline) const override;

    //!<  法線角取得(Line,Circle,Splie,CompositLine用)
    virtual bool GetNormAngle(double* pAngle, const POINT2D& pt, bool bOnline) const override;

    //!< 長さ(線,円弧,スプライン上)
    virtual double Length(const POINT2D& ptIntersect, bool bStart) const override;

    //!< オフセット(線,円弧,スプライン上)
    virtual bool Offset(double dLen, POINT2D ptDir) override;

    //!< オフセット(線,円弧,スプライン上)
    virtual bool Offset(double dLen) override;

    //!< 点が進行方向の右側にあるか
    virtual bool IsRightSide(POINT2D ptDir) const override;

    //-------------------------
    // プロパティ関連メソッド
    //-------------------------

    //!<プロパティ初期化
    virtual TREE_GRID_ITEM*  _CreateStdPropertyTree();

    //!< プロパティ値更新
    virtual void _PrepareUpdatePropertyGrid();

    //-------------------------
    // 参照
    //-------------------------

    //!< 参照データに基づいて更新
    virtual CDrawingObject* UpdateRef();

    //-------------------
    // ドラッグ用マーカ
    //-------------------
    //!< マーカ初期化 true:マーカあり
    virtual bool InitNodeMarker(CNodeMarker* pMarker) override;

    //!< マーカ選択
    virtual void SelectNodeMarker(CNodeMarker* pMarker, 
                                     StdString strMarkerId) override;

    //!< マーカ移動
    virtual void MoveNodeMarker(CNodeMarker* pMarker, 
                                     SNAP_DATA* pSnap,
                                     StdString strMarkerId,
                                     MOUSE_MOVE_POS posMouse) override;

    //!< マーカ開放
    virtual bool ReleaseNodeMarker(CNodeMarker* pMarker, StdString strMarkerId, MOUSE_MOVE_POS posMouse) override;

   CDrawingLine::E_NODE GetNode(StdString strMarkerId) const;


protected:

    //!< 線描画(マウスオーバー時)
    virtual void DrawOver(CDrawingView* pView, bool bOver);

    
friend CDrawingLine;
    //--------------------
    // プロパティ変更処理
    //--------------------
    static bool PropP1         (CStdPropertyItem* pData, void* pObj);
    static bool PropP2         (CStdPropertyItem* pData, void* pObj);
    static bool PropWidth      (CStdPropertyItem* pData, void* pObj);
    static bool PropLineType   (CStdPropertyItem* pData, void* pObj);

protected:
    //!< 位置
    LINE2D      m_Line;

    //!< 線幅
    int         m_iWidth;

    //!< 線種
    int         m_iLineType;

    bool        m_bSelPoint[2];
    //--------------------
    // プロパティ変更処理
    //--------------------
    //TAG:[処理がやっつけぽい]
    POINT2D     m_ptTmp1;

    POINT2D     m_ptTmp2;

private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
            ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(CDrawingObject);
            SERIALIZATION_BOTH("Line"     , m_Line);
            SERIALIZATION_BOTH("Width"    , m_iWidth);
            SERIALIZATION_BOTH("LineType" , m_iLineType);
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )

    }
};
#endif // !defined(EA_355B8576_BE84_4562_B3C9_07216543D721__INCLUDED_)
