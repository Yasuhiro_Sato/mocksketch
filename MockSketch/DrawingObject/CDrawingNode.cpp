/**
 * @brief			CDrawingNode実装ファイル
 * @file			CDrawingNode.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:02:47
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CDrawingNode.h"
#include "./CDrawingLine.h"
#include "./CDrawingCircle.h"
#include "./CDrawingText.h"
#include "./CDrawingParts.h"
#include "./CDrawingDim.h"
#include "./CDrawingEllipse.h"
#include "./CDrawingSpline.h"
#include "./CDrawingCompositionLine.h"
#include "./CNodeMarker.h"

#include "./ACTION/CViewAction.h"
#include "DefinitionObject/View/CDrawingView.h"
#include "DefinitionObject/CPartsDef.h"
#include "Utility/CUtility.h"
#include "System/CSystem.h"
#include <math.h>
#include <boost/serialization/export.hpp> 
BOOST_CLASS_EXPORT(CDrawingNode);

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/*---------------------------------------------------*/
/*  static                                           */
/*---------------------------------------------------*/
CDrawingNode::E_MENU_TYPE CDrawingNode::ms_eMenuType = E_MNUT_NONE;
std::map<int, boost::tuple<int, int> > CDrawingNode::ms_mapPosToObjectId;
POINT2D CDrawingNode::ms_ptStart;

/**
 * コンストラクタ
 */

CDrawingNode::CDrawingNode():
CDrawingNode::CDrawingNode(-1)
{
}

/**
 * コンストラクタ
 */
CDrawingNode::CDrawingNode(int nID):
CDrawingObject( nID, DT_NODE),
m_bCopyNodeDataMode(false),
m_iTmpConnectionObjId(-1)
{
    m_crObj     = DRAW_CONFIG->crPoint;
    m_iPropFeatures = P_BASE; 
    SetUsePropertySet(true);

    m_psNd = std::make_unique<CNodeData>();
    m_psMarkerData = std::make_unique<CMarkerData>();
    m_psMarkerData->m_pDrawingNode = this;
}

/**
 * コピーコンストラクタ
 */
CDrawingNode::CDrawingNode(const CDrawingNode& Obj):
CDrawingObject( Obj)
{
    m_eType     = DT_NODE;
    m_iPropFeatures = P_BASE; 
    SetUsePropertySet(true);

    m_psNd = std::make_unique<CNodeData>(*Obj.m_psNd);

    m_strTmpSnapTypeList   = Obj.m_strTmpSnapTypeList;

    *m_pPropertySetName    = *Obj.m_pPropertySetName;
    *m_pPropertySetMap     = *Obj.m_pPropertySetMap;
    
    bool bCopyNodeData = false;


    if (GetCopyNodeDataMode())
    {
        *m_psNd = *Obj.m_psNd;
    }
    else
    {
        CBindingPartner p; 
        m_psNd->lstPlug.resize(m_psNd->iMaxPlug);
        std::fill(m_psNd->lstPlug.begin(), m_psNd->lstPlug.end(), p);                            
    }

    m_psMarkerData = std::make_unique<CMarkerData>(*(Obj.m_psMarkerData.get()));
    m_psMarkerData->m_pDrawingNode = this;
}

/**
 * デストラクタ
 */

CDrawingNode::~CDrawingNode()
{
    m_psMarkerData->m_pDrawingNode = NULL;
}

/**
 *  @brief  回転.
 *  @param  [in]    pPt2D    回転中心
 *  @param  [in]    dAngle   回転角
 *  @retval     なし
 *  @note
 */
void CDrawingNode::Rotate(const POINT2D& pt2D, double dAngle)
{
    AddChgCnt();
    m_psNd->ptPos.Rotate(pt2D, dAngle);
}

/**
 *  @brief  相対移動.
 *  @param  [in]    pPt2D   移動量
 *  @retval         なし
 *  @note
 */
void CDrawingNode::Move(const POINT2D& pt2D) 
{
    AddChgCnt();
    m_psNd->ptPos = m_psNd->ptPos + pt2D;
}


/**
 *  @brief  位置設定.
 *  @param  [in]    ptPos   設定位置
 *  @retval         なし
 *  @note
 */
void  CDrawingNode::SetPoint(const POINT2D& ptPos)        
{
    AddChgCnt();
    m_psNd->ptPos = ptPos;
} 

/**
 *  @brief  位置設定.
 *  @param  [in]    dX   X座標
 *  @param  [in]    dY   X座標
 *  @retval         なし
 *  @note
 */
void  CDrawingNode::SetPoint(double dX, double dY)        
{
    AddChgCnt();
    m_psNd->ptPos.dX = dX;
    m_psNd->ptPos.dY = dY;
} 

/**
 *  @brief  鏡像.
 *  @param  [in]    line   対称軸
 *  @retval         なし
 *  @note
 */
void CDrawingNode::Mirror(const LINE2D& line)
{
    AddChgCnt();
    m_psNd->ptPos.Mirror(line);
}

/**
 *  @brief  倍率.
 *  @param  [in]    pPt2D   拡大中心
 *  @param  [in]    dSclX   X倍率
 *  @param  [in]    dSclY   Y倍率
 *  @retval         なし
 *  @note
 */
void CDrawingNode::Scl(const POINT2D& pt2D, double dXScl, double dYScl)
{
    AddChgCnt();
    m_psNd->ptPos.Scl(pt2D, dXScl, dYScl);
}


/**
 *  @brief  行列.
 *  @param  [in]    mat2D   適用する行列
 *  @retval         なし
 *  @note
 */
void CDrawingNode::Matrix(const MAT2D& mat2D)
{
    AddChgCnt();
    m_psNd->ptPos.Matrix( mat2D);
}

//!< 最大接続数
void CDrawingNode::SetMaxPlug(int iMaxPlug)
{
    m_psNd->iMaxPlug= iMaxPlug;
}

int  CDrawingNode::GetMaxPlug()const
{
    return m_psNd->iMaxPlug;
}

//!< 接続タイプ
void CDrawingNode::SetConnectionType(E_ACTIVE_PASSIVE eConnectionType)
{
    m_psNd->eConnectionType = eConnectionType;
}

E_ACTIVE_PASSIVE  CDrawingNode::GetConnectionType()const
{
    return m_psNd->eConnectionType;
}

//!< 接続対象
void CDrawingNode::SetConnectionObjectType(CNodeData::E_CONNECTION_OBJECT eObject)
{
    m_psNd->eConnectionObject = eObject;
}

CNodeData::E_CONNECTION_OBJECT  CDrawingNode::GetConnectionObjectType()const
{
    return m_psNd->eConnectionObject;
}

//!< 接続可能プロパティセットリスト
bool CDrawingNode::GetConnectionPropertySetList(std::vector<StdString>* pLst)const
{
    CUtil::TokenizeCsv(pLst, m_psNd->strConnectionPropertySet);
    return true;
}


//!< 接続方向
void CDrawingNode::SetConnectionDirection(E_IN_OUT eInOut)
{
    m_psNd->eConnectionDirection = eInOut;
}

E_IN_OUT  CDrawingNode::GetConnectionDirection()const
{
    return m_psNd->eConnectionDirection;
}


//!< 接続方向
void CDrawingNode::SetLineDirection(CNodeData::E_LINE_DIR_TYPE eDir)
{
    m_psNd->eLineDirection = eDir;
}

CNodeData::E_LINE_DIR_TYPE  CDrawingNode::GetLineDirection()const
{
    return m_psNd->eLineDirection;
}

//!< マーカー移動制限
void CDrawingNode::SetMarkerMove(NODE_MARKER_MOVE eMove)
{
    m_psNd->nodeProperty.m_eMove = eMove;
}

NODE_MARKER_MOVE  CDrawingNode::GetMarkerMove()const
{
    return m_psNd->nodeProperty.m_eMove;
}

//!< マーカー形状
void CDrawingNode::SetMarkerShape(NODE_MARKER_SHAPE eShape)
{
    m_psNd->nodeProperty.m_eShape = eShape;
}

NODE_MARKER_SHAPE  CDrawingNode::GetMarkerShape()const
{
    return m_psNd->nodeProperty.m_eShape;
}

//!< マーカー色
void CDrawingNode::SetMarkerColor(COLORREF crMarker)
{
    m_psNd->nodeProperty.m_cr = crMarker;
}

COLORREF  CDrawingNode::GetMarkerColor()const
{
    return m_psNd->nodeProperty.m_cr;
}

//!<マーカー座標系
void CDrawingNode::SetMarkerCordinateSystem(E_LOCAL_WORLD eCord)
{
    m_psNd->nodeProperty.m_eCordinateSystem = eCord;
}

E_LOCAL_WORLD  CDrawingNode::GetMarkerCordinateSystem()const
{
    return m_psNd->nodeProperty.m_eCordinateSystem;
}

//!<マーカー移動方向
void CDrawingNode::SetMarkerDir(POINT2D ptDir)
{
    m_psNd->nodeProperty.m_ptDir = ptDir;
}

POINT2D  CDrawingNode::GetMarkerDir()const
{
    return m_psNd->nodeProperty.m_ptDir;
}

//!<マーカー表示種別
void CDrawingNode::SetDispType(DISP_TYPE eDispType)
{
    m_psNd->nodeProperty.m_eDispType = eDispType;
}

DISP_TYPE CDrawingNode::GetDispType()const
{
    return m_psNd->nodeProperty.m_eDispType;
}

/**
 *  @brief  交点計算.
 *  @param  [out]   pList   交点のリスト
 *  @param  [in]    pObj    交差するオブジェクト
 *  @retval         なし
 *  @note
 */
void CDrawingNode::CalcIntersection ( std::vector<POINT2D>* pList,   
                                const CDrawingObject* pObj, 
                                bool bOnline,
                                double dMin) const
{
    CalcIntersectionPoint(pList, pObj, m_psNd->ptPos, bOnline, dMin);
}

/**
 *  @brief  描画.
 *  @param  [in]  pView       表示View
 *  @param  [in]  bMouseOver  true オブジェクト上にマウスあり
 *  @retval         なし
 *  @note
 */
void CDrawingNode::Draw(CDrawingView* pView, 
                       const std::shared_ptr<CDrawingObject> pParentObject,
                       E_MOUSE_OVER_TYPE eMouseOver,
                       int iLayerId)
{
    COLORREF crObj;
    int      iID;
    if (IsBelongToRoot(pParentObject.get()))
    {
        iID   = m_nId;
        if (IsSelect())
        {
            //選択色
            crObj = DRAW_CONFIG->GetSelColor();
        }
        else
        {
           //crObj = m_crObj;
            crObj = DRAW_CONFIG->GetPointColor();
        }
    }
    else
    {
        const CDrawingObject* pObj = GetTopDraw(pParentObject.get());

        STD_ASSERT(pObj != NULL);
        if (pObj == NULL)
        {
            return;
        }

        auto pParent =  m_pParent.lock();
        iID = m_nId;
        if(pParent)
        {
            if (pParent->GetType() == DT_REFERENCE)
            {
                iID = pParent->GetId();
            }
        }

        if (pObj->IsSelect())
        {
            //選択色
            crObj = DRAW_CONFIG->GetSelColor();
        }
        else
        {
            //crObj = pObj->GetColor();
            crObj = DRAW_CONFIG->GetPointColor();
        }
    }
    _Node( pView, iLayerId, pParentObject.get(), crObj, iID, eMouseOver);
}

/**
 *  @brief  描画削除.
 *  @param  [in]    pView   表示View
 *  @retval         なし
 *  @note
 */
void CDrawingNode::DeleteView(CDrawingView* pView, 
                              const std::shared_ptr<CDrawingObject> pParentObject,
                              int iLayerId)
{
    //背景色
    COLORREF crObj;
    crObj = DRAW_CONFIG->GetBackColor();

    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }

    _Node( pView, iLayerId, pParentObject.get(), crObj, 0, E_MO_NO);
}

/**
 *  @brief   範囲.
 *  @param   [in] p1 1点目
 *  @param   [in] p2 2点目
 *  @retval  true:2点で示される矩形内に点が存在する 
 *  @note
 */
bool CDrawingNode::IsInner( const RECT2D& rcArea, bool bPart) const
{
    const MAT2D* pMat =  GetParentMatrix();
    POINT2D tmpPt(m_psNd->ptPos);
    if (pMat != NULL)
    {
        tmpPt.Matrix(*pMat);
    }

    if (rcArea.IsInside(tmpPt))
    {
        return true;
    }
    return false;
}

/**
 *  @brief   点描画
 *  @param   [in] pView 表示View
 *  @param   [in] crPen 描画色
 *  @param   [in] iId   描画ID
 *  @retval  なし
 *  @note
 */
void CDrawingNode::_Node( CDrawingView* pView, 
                          int iLayerId,
                          const CDrawingObject* pParentObject,
                          COLORREF crPen, 
                          int iId, 
                          E_MOUSE_OVER_TYPE eMouseOver) const
{
    //とりあえずクリッピングしない
    STD_ASSERT(pView != 0);
    POINT  Pt;

    //-------------------
    // MATRIX
    //-------------------
    POINT2D ptDraw = m_psNd->ptPos;
    MAT2D mat2D;

    if (pParentObject )
    {
        const MAT2D* pMat = pParentObject->GetDrawingMatrix();
        if (pMat)
        {
            mat2D = *pMat;
        }
    }

    if (m_pMatOffset)
    {
        mat2D = mat2D * (*m_pMatOffset);
    }

    ptDraw = ptDraw * mat2D;

    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }

    pView->ConvWorld2Scr(&Pt, ptDraw, iLayerId);
    //-------------------

    if (pParentObject)
    {
        if(GetDispType() ==  NDT_SHOW_NODE_OPERATION)
        {
            return;
        }
    }
    _ScrNode( pView, iLayerId, Pt, crPen, iId, eMouseOver);
}


/**
 *  @brief   描画(マウスオーバー時).
 *  @param   [in] pView 表示View
 *  @param   [in] bOver true:開始 false:終了
 *  @retval  なし
 *  @note
 */
void CDrawingNode::DrawOver(CDrawingView* pView, bool bOver)
{

}

/**
 * ノード描画
 * @param   [in]    Pt      座標        
 * @param   [in]    crPt    色        
 * @param   [in]    iId     図形ID           
 * @retval   なし
 * @note	
 */
void CDrawingNode::_ScrNode(CDrawingView* pView, 
                             int iLayerId,
                             POINT Pt, 
                             COLORREF crPen, 
                             int iId, 
                             E_MOUSE_OVER_TYPE eMouseOver)  const
{
    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }

    GetLayerCoror(&crPen, pView, iLayerId);

    int iWidth = 1;

    if (eMouseOver == E_MO_SELECTABLE)
    {
        pView->SetLineAlternateMode(true);
        iWidth += 2;
    }
    else if (eMouseOver == E_MO_EMPHASIS)
    {
        crPen = DRAW_CONFIG->crEmphasis;
    }
    else if (eMouseOver == E_MO_CONNECTABLE)
    {
        crPen = DRAW_CONFIG->crConnectable;
    }


    double dLen = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->GetMarkerSize());
    int iR =  CUtil::Round(dLen * pView->GetDpu());

    m_psMarkerData->Draw(pView, Pt, iId, crPen, GetMarkerColor(), iWidth, iR); 


    if (eMouseOver == E_MO_SELECTABLE)
    {
        pView->SetLineAlternateMode(false);
    }

}


/**
 *  @brief   プロパティ初期化.
 *  @param   [in] pCtrl オブジェクトコントロールへのポインタ
 *  @retval     なし
 *  @note
 */
TREE_GRID_ITEM*  CDrawingNode::_CreateStdPropertyTree()
{
 
    NAME_TREE_ITEM<CStdPropertyItem>* pBeforeGroup;
    NAME_TREE_ITEM<CStdPropertyItem>* pNextItem;
    NAME_TREE_ITEM<CStdPropertyItem>* pGroupItem;
    NAME_TREE<CStdPropertyItem>* pTree;
    //int iId;
    CStdPropertyItem* pItem;
    boost::tuple< CStdPropertyItem*, void*> pairVal;


    pBeforeGroup = CDrawingObject::_CreateStdPropertyTree();
    pTree = m_psProperty->GetTreeInstance();

    pGroupItem  = pTree->AddNext(pBeforeGroup, NULL, GET_STR(STR_PRO_NODE), _T("Node"));


    //------------
    //位置
    //------------
    CStdPropertyItemDef DefPos(PROP_POINT2D, 
        GET_STR(STR_PRO_POS)   , 
        _T("Pos"),
        GET_STR(STR_PRO_INFO_POS), 
        true,
        DISP_UNIT,
        POINT2D());

    pItem = new CStdPropertyItem( DefPos, PropPos, m_psProperty, NULL, &m_psNd->ptPos);
    pNextItem = pTree->AddChild(pGroupItem, pItem, DefPos.strDspName, pItem->GetName(), 2);

    //------------
    //最大接続数
    //------------
    CStdPropertyItemDef DefMaxConnect(PROP_INT, //型
        GET_STR(STR_PRO_MAX_CONNECT),           //データ表示名
        _T("MaxPlug"),                       //変数名
        GET_STR(STR_PRO_INFO_MAX_CONNECT),      //データ説明
        true,                                   //編集可、不可
        DISP_NONE,                              //単位
        0);                                     //初期値


    pItem = new CStdPropertyItem(DefMaxConnect,     //定義データ
                                 PropMaxPlug,    //入力時コールバック
                                 m_psProperty,      //親グリッド
                                 NULL,              //更新時コールバック
                                 &m_psNd->iMaxPlug);   //実データ

    pNextItem = pTree->AddNext(pNextItem, pItem, 
                       DefMaxConnect.strDspName, 
                       pItem->GetName());

    //------------
    //接続タイプ
    //------------
    StdString strConnectList;
    strConnectList  = _T("Passive");
    strConnectList += _T(":");  
    strConnectList += boost::lexical_cast<StdString>(E_PASSIVE);
    strConnectList += _T(",");  

    strConnectList += _T("Active");
    strConnectList += _T(":");  
    strConnectList += boost::lexical_cast<StdString>(E_ACTIVE);


    CStdPropertyItemDef DefConnectionType(PROP_DROP_DOWN_ID, //型
        GET_STR(STR_PRO_CONNECTION_TYPE),       //データ表示名
        _T("ConnectionType"),  //名称
        GET_STR(STR_PRO_INFO_CONNECTION_TYPE),  //データ説明
        false,                                  //編集可、不可
        DISP_NONE,                              //単位
        static_cast<int>(m_psNd->eConnectionType),    //初期値
        strConnectList,                         //最小値・リスト設定
        0);                                

    pItem = new CStdPropertyItem(
                                 DefConnectionType,     //定義データ
                                 PropConnectionType,    //入力時コールバック
                                 m_psProperty,          //親グリッド
                                 NULL,                  //更新時コールバック
                                 &m_psNd->eConnectionType);   //実データ

    pNextItem = pTree->AddNext(pNextItem, pItem, 
                                DefConnectionType.strDspName, 
                                pItem->GetName());

    //------------
    //接続対象
    //------------
    StdString strObjectList;
    strObjectList  = _T("None");
    strObjectList += _T(":");  
    strObjectList += boost::lexical_cast<StdString>(CNodeData::NONE);
    strObjectList += _T(",");  

    strObjectList += _T("Field");
    strObjectList += _T(":");  
    strObjectList += boost::lexical_cast<StdString>(CNodeData::FIELD);
    strObjectList += _T(",");  

    strObjectList += _T("Node");
    strObjectList += _T(":");  
    strObjectList += boost::lexical_cast<StdString>(CNodeData::NODE);
    strObjectList += _T(",");  

    strObjectList += _T("Object");
    strObjectList += _T(":");  
    strObjectList += boost::lexical_cast<StdString>(CNodeData::OBJECT);

    CStdPropertyItemDef DefConnectionObject(PROP_DROP_DOWN_ID, //型
        GET_STR(STR_PRO_CONNECTION_OBJECT),         //データ表示名
        _T("ConnectionObject"),           //名称
        GET_STR(STR_PRO_INFO_CONNECTION_OBJECT),    //データ説明
        false,                                      //編集可、不可
        DISP_NONE,                                  //単位
        static_cast<int>(m_psNd->eConnectionObject),      //初期値
        strObjectList,                              //最小値・リスト設定
        0);                                

    pItem = new CStdPropertyItem(
                                 DefConnectionObject,       //定義データ
                                 PropConnectionObject,   //入力時コールバック
                                 m_psProperty,              //親グリッド
                                 NULL,                      //更新時コールバック
                                 &m_psNd->eConnectionObject);     //実データ

    pNextItem = pTree->AddNext(pNextItem, pItem, 
                                DefConnectionObject.strDspName, 
                                pItem->GetName());

    //------------
    //接続方向
    //------------
    StdString strDirectionList;
    strDirectionList  = _T("In");
    strDirectionList += _T(":");  
    strDirectionList += boost::lexical_cast<StdString>(E_IN);
    strDirectionList += _T(",");  

    strDirectionList += _T("Out");
    strDirectionList += _T(":");  
    strDirectionList += boost::lexical_cast<StdString>(E_OUT);
    strDirectionList += _T(",");  

    strDirectionList += _T("InOut");
    strDirectionList += _T(":");  
    strDirectionList += boost::lexical_cast<StdString>(E_INOUT);


    CStdPropertyItemDef DefConnectionDirection(PROP_DROP_DOWN_ID, //型
        GET_STR(STR_PRO_CONNECTION_DIR),            //データ表示名
        _T("Direction"),            //名称
        GET_STR(STR_PRO_INFO_CONNECTION_DIR),       //データ説明
        false,                                      //編集可、不可
        DISP_NONE,                                  //単位
        static_cast<int>(m_psNd->eConnectionDirection),   //初期値
        strDirectionList,                           //最小値・リスト設定
        0);                                

    pItem = new CStdPropertyItem(
                                 DefConnectionDirection,     //定義データ
                                 PropConnectionDirection,    //入力時コールバック
                                 m_psProperty,          //親グリッド
                                 NULL,                  //更新時コールバック
                                 &m_psNd->eConnectionDirection);   //実データ

    pNextItem = pTree->AddNext(pNextItem, pItem, 
                                DefConnectionDirection.strDspName, 
                                pItem->GetName());


    //------------
    //接続線方向
    //------------
    StdString strLineDirectionList;
    strLineDirectionList  = GET_STR(STR_DML_ALL);
    strLineDirectionList += _T(":");  
    strLineDirectionList += boost::lexical_cast<StdString>(CNodeData::E_ALL);
    strLineDirectionList += _T(",");  

    strLineDirectionList += GET_STR(STR_DML_OBJ);
    strLineDirectionList += _T(":");  
    strLineDirectionList += boost::lexical_cast<StdString>(CNodeData::E_NORM_OBJECT_BOUNDS);
    strLineDirectionList += _T(",");  

    strLineDirectionList += GET_STR(STR_DML_TOP);
    strLineDirectionList += _T(":");  
    strLineDirectionList += boost::lexical_cast<StdString>(CNodeData::E_TOP);
    strLineDirectionList += _T(",");  

    strLineDirectionList += GET_STR(STR_DML_RIGHT);
    strLineDirectionList += _T(":");  
    strLineDirectionList += boost::lexical_cast<StdString>(CNodeData::E_RIGHT);
    strLineDirectionList += _T(",");  

    strLineDirectionList += GET_STR(STR_DML_BOTTOM);
    strLineDirectionList += _T(":");  
    strLineDirectionList += boost::lexical_cast<StdString>(CNodeData::E_BOTTOM);
    strLineDirectionList += _T(",");  

    strLineDirectionList += GET_STR(STR_DML_LEFT);
    strLineDirectionList += _T(":");  
    strLineDirectionList += boost::lexical_cast<StdString>(CNodeData::E_LEFT);
    strLineDirectionList += _T(",");  

    strLineDirectionList += GET_STR(STR_DML_DIRX);
    strLineDirectionList += _T(":");  
    strLineDirectionList += boost::lexical_cast<StdString>(CNodeData::E_DIRX);
    strLineDirectionList += _T(",");  

    strLineDirectionList += GET_STR(STR_DML_DIRY);
    strLineDirectionList += _T(":");  
    strLineDirectionList += boost::lexical_cast<StdString>(CNodeData::E_DIRY);


    CStdPropertyItemDef DefLineDirection(PROP_DROP_DOWN_ID, //型
        GET_STR(STR_PRO_CONNECTION_LINE_DIR),            //データ表示名
        _T("LineDirection"),            //名称
        GET_STR(STR_PRO_INFO_CONNECTION_LINE_DIR),       //データ説明
        false,                                      //編集可、不可
        DISP_NONE,                                  //単位
        static_cast<int>(m_psNd->eLineDirection),   //初期値
        strLineDirectionList,                           //最小値・リスト設定
        0);                                

    pItem = new CStdPropertyItem(
                                 DefLineDirection,     //定義データ
                                 PropLineDirection,    //入力時コールバック
                                 m_psProperty,          //親グリッド
                                 NULL,                  //更新時コールバック
                                 &m_psNd->eLineDirection);   //実データ

    pNextItem = pTree->AddNext(pNextItem, pItem, 
                                DefLineDirection.strDspName, 
                                pItem->GetName());




    //-----------
    // 接続プロパティセット
    //-----------
    
    std::set<StdString> lstName;
    StdString strList;

    SYS_PROPERTY->GetPropertySetNameList(&lstName);

    strList = _T("NONE");

    bool bFirst = true;
    foreach(StdString name, lstName)
    {
        strList += _T(",");
        strList += name;
    }


    CStdPropertyItemDef DefConnectionPropertySet (PROP_CHCEK_LIST, //型
        GET_STR(STR_PRO_CONNECT_PROPERTYSET),            //データ表示名
        _T("ConnectionPropertySet"),   //名称
        GET_STR(STR_PRO_INFO_CONNECT_PROPERTYSET),       //データ説明
        false,                                 //編集可、不可
        DISP_NONE,                             //単位
        m_psNd->strConnectionPropertySet,               //初期値
        strList,                               //最小値・リスト設定
        0);                                   

    pItem = new CStdPropertyItem(
                                    DefConnectionPropertySet,   //定義データ
                                    PropConnectionPropertySet,  //入力時コールバック
                                    m_psProperty,               //親グリッド
                                    NULL,                       //更新時コールバック
                                    &m_psNd->strConnectionPropertySet);     //実データ

    pNextItem = pTree->AddNext(pNextItem, pItem, 
                                DefConnectionPropertySet.strDspName, 
                                pItem->GetName());



    //===========
    // Marker
    //===========
    NAME_TREE_ITEM<CStdPropertyItem>* pGroupMarker;
    pNextItem = pTree->AddNext(pNextItem, NULL, GET_STR(STR_PRO_MARKER), _T("Marker"));


    //------------
    //マーカー形状
    //------------
    StdString strMarkerShape;
    strMarkerShape  = GET_STR(STR_DMS_CIRCLE_FILL);
    strMarkerShape += _T(":");  
    strMarkerShape += boost::lexical_cast<StdString>(DMS_CIRCLE_FILL);
    strMarkerShape += _T(",");  

    strMarkerShape += GET_STR(STR_DMS_CIRCLE);
    strMarkerShape += _T(":");  
    strMarkerShape += boost::lexical_cast<StdString>(DMS_CIRCLE);
    strMarkerShape += _T(",");  

    strMarkerShape += GET_STR(STR_DMS_RECT_FILL);
    strMarkerShape += _T(":");  
    strMarkerShape += boost::lexical_cast<StdString>(DMS_RECT_FILL);
    strMarkerShape += _T(",");  

    strMarkerShape += GET_STR(STR_DMS_RECT);
    strMarkerShape += _T(":");  
    strMarkerShape += boost::lexical_cast<StdString>(DMS_RECT);
    strMarkerShape += _T(",");  

    strMarkerShape += GET_STR(STR_DMS_DBL_CIRCLE);
    strMarkerShape += _T(":");  
    strMarkerShape += boost::lexical_cast<StdString>(DMS_DBL_CIRCLE);
    strMarkerShape += _T(",");  

    strMarkerShape += GET_STR(STR_DMS_DBL_RECT);
    strMarkerShape += _T(":");  
    strMarkerShape += boost::lexical_cast<StdString>(DMS_DBL_RECT);
     
    CStdPropertyItemDef DefMarkerShape(PROP_DROP_DOWN_ID, //型
        GET_STR(STR_PRO_MARKER_SHAPE),              //データ表示名
        _T("MarkerShape"),         //名称
        GET_STR(STR_PRO_INFO_MARKER_SHAPE),         //データ説明
        false,                                      //編集可、不可
        DISP_NONE,                                  //単位
        static_cast<int>(m_psNd->nodeProperty.m_eShape),    //初期値
        strMarkerShape,                             //最小値・リスト設定
        0);                                

    pItem = new CStdPropertyItem(
                                 DefMarkerShape,            //定義データ
                                 PropMarkerShape,           //入力時コールバック
                                 m_psProperty,              //親グリッド
                                 NULL,                      //更新時コールバック
                                 &m_psNd->nodeProperty.m_eShape);   //実データ

    pGroupMarker = pTree->AddChild(pNextItem, pItem, 
                                DefMarkerShape.strDspName, 
                                pItem->GetName());

    //------------
    //マーカー色
    //------------
    CStdPropertyItemDef DefMarkerColor(PROP_COLOR, 
        GET_STR(STR_PRO_MARKER_COLOR)   , 
        _T("MarkerColor"),
        GET_STR(STR_PRO_MARKER_INFO_COLOR), 
        false,
        DISP_NONE,
        m_psNd->nodeProperty.m_cr);

    pItem = new CStdPropertyItem(
                                 DefMarkerColor,
                                 PropMarkerColor,
                                 m_psProperty,
                                 NULL,
                                 &m_psNd->nodeProperty.m_cr);

    pGroupMarker = pTree->AddNext(pGroupMarker, pItem, 
                                   DefMarkerColor.strDspName, 
                                   pItem->GetName());


    //----------------
    //マーカー移動制限
    //----------------
    StdString strMoveList;

    strMoveList  = GET_STR(STR_DMT_NOMOVE);
    strMoveList += _T(":");  
    strMoveList += boost::lexical_cast<StdString>(DMT_NOMOVE);
    strMoveList += _T(",");  

    strMoveList += GET_STR(STR_DMT_ALL_MOVE);
    strMoveList += _T(":");  
    strMoveList += boost::lexical_cast<StdString>(DMT_ALL_MOVE);
    strMoveList += _T(",");  

    strMoveList += GET_STR(STR_DMT_LIMITED_DIRECTION);
    strMoveList += _T(":");  
    strMoveList += boost::lexical_cast<StdString>(DMT_LIMITED_DIRECTION);
    strMoveList += _T(",");  

    strMoveList += GET_STR(STR_DMT_LIMITED_DISTANCE);
    strMoveList += _T(":");  
    strMoveList += boost::lexical_cast<StdString>(DMT_LIMITED_DISTANCE);



    CStdPropertyItemDef DefMarkerMoveType(PROP_DROP_DOWN_ID, //型
        GET_STR(STR_PRO_MARKER_MOVE_TYPE),            //データ表示名
        _T("MarkerMove"),      //名称
        GET_STR(STR_PRO_INFO_MARKER_MOVE_TYPE),       //データ説明
        false,                                        //編集可、不可
        DISP_NONE,                                    //単位
        static_cast<int>(m_psNd->nodeProperty.m_eMove),       //初期値
        strMoveList,                                  //最小値・リスト設定
        0);                                

    pItem = new CStdPropertyItem(
                                 DefMarkerMoveType,     //定義データ
                                 PropMarkerMoveType,    //入力時コールバック
                                 m_psProperty,          //親グリッド
                                 NULL,                  //更新時コールバック
                                 &m_psNd->nodeProperty.m_eMove);   //実データ

    pGroupMarker = pTree->AddNext(pGroupMarker, pItem, 
                                DefMarkerMoveType.strDspName, 
                                pItem->GetName());

    //--------------
    //マーカー座標系
    //--------------
    StdString strCord;

    strCord  = _T("Local");
    strCord += _T(":");  
    strCord += boost::lexical_cast<StdString>(E_LOCAL);
    strCord += _T(",");  

    strCord += _T("World");
    strCord += _T(":");  
    strCord += boost::lexical_cast<StdString>(E_WORLD);


    CStdPropertyItemDef DefMarkerCord(PROP_DROP_DOWN_ID, //型
        GET_STR(STR_PRO_MARKER_CORD),                    //データ表示名
        _T("MarkerCord"),    //名称
        GET_STR(STR_PRO_INFO_MARKER_CORD),               //データ説明
        false,                                           //編集可、不可
        DISP_NONE,                                       //単位
        static_cast<int>(m_psNd->nodeProperty.m_eCordinateSystem),  //初期値
        strCord,                                            //最小値・リスト設定
        0);                                

    pItem = new CStdPropertyItem(
                                 DefMarkerCord,       //定義データ
                                 PropMarkerCord,      //入力時コールバック
                                 m_psProperty,        //親グリッド
                                 NULL,                //更新時コールバック
                                 &m_psNd->nodeProperty.m_eCordinateSystem);   //実データ

    pGroupMarker = pTree->AddNext(pGroupMarker, pItem, 
                                DefMarkerCord.strDspName, 
                                pItem->GetName());

    //--------------
    //マーカー移動方向
    //--------------
    CStdPropertyItemDef DefDir(PROP_POINT2D, 
        GET_STR(STR_PRO_MARKER_DIR)   , 
        _T("Dir"), 
        GET_STR(STR_PRO_INFO_MARKER_DIR), 
        true,
        DISP_UNIT,
        m_psNd->nodeProperty.m_ptDir);

    pItem = new CStdPropertyItem(
                                 DefDir, 
                                 PropMarkerDir, 
                                 m_psProperty, 
                                 NULL, 
                                 &m_psNd->nodeProperty.m_ptDir);

    pGroupMarker = pTree->AddNext(pGroupMarker, 
                                   pItem, 
                                   DefDir.strDspName, 
                                   pItem->GetName(), 2);

    //--------------
    //マーカー表示種別
    //--------------
    StdString strDispType;

    strDispType  = GET_STR(STR_NDT_SHOW_ALWAYS);
    strDispType += _T(":");  
    strDispType += boost::lexical_cast<StdString>(NDT_SHOW_ALWAYS);
    strDispType += _T(",");  

    strDispType += GET_STR(STR_NDT_SHOW_NODE_OPERATION);
    strDispType += _T(":");  
    strDispType += boost::lexical_cast<StdString>(NDT_SHOW_NODE_OPERATION);


    CStdPropertyItemDef DefMarkerDispType(PROP_DROP_DOWN_ID, //型
        GET_STR(STR_PRO_MARKER_DISP_TYPE),                   //データ表示名
        _T("DispType"),    //名称
        GET_STR(STR_PRO_INFO_MARKER_DISP_TYPE),             //データ説明
        false,                                              //編集可、不可
        DISP_NONE,                                          //単位
        static_cast<int>(m_psNd->nodeProperty.m_eDispType),    //初期値
        strDispType,                                        //最小値・リスト設定
        0);                                

    pItem = new CStdPropertyItem(
                                 DefMarkerDispType,       //定義データ
                                 PropDispType,      //入力時コールバック
                                 m_psProperty,        //親グリッド
                                 NULL,                //更新時コールバック
                                 &m_psNd->nodeProperty.m_eDispType);   //実データ

    pGroupMarker = pTree->AddNext(pGroupMarker, pItem, 
                                DefMarkerDispType.strDspName, 
                                pItem->GetName());

    //-----------
    // スナップタイプ
    //-----------
    using namespace VIEW_COMMON;
    DWORD dwSnapType = SNP_ALL_POINT | SNP_DISTANCE | SNP_ANGLE;

    StdString strSnamTypeAll;
    strSnamTypeAll = _SnapTypeListToString(dwSnapType);
    m_strTmpSnapTypeList = _SnapTypeListToString(m_psNd->dwSnapType);

    CStdPropertyItemDef DefNodeSnap (PROP_CHCEK_LIST, //型
        GET_STR(STR_PRO_NODE_SNAP),            //データ表示名
        _T("ConnectionObject"),
        GET_STR(STR_PRO_INFO_NODE_SNAP),       //データ説明
        false,                                 //編集可、不可
        DISP_NONE,                             //単位
        m_strTmpSnapTypeList,                  //初期値
        strSnamTypeAll,                        //最小値・リスト設定
        0);                                

    pItem = new CStdPropertyItem(     //名称
                                    DefNodeSnap,                //定義データ
                                    PropPropertySnapType,       //入力時コールバック
                                    m_psProperty,               //親グリッド
                                    NULL,                       //更新時コールバック
                                    &m_strTmpSnapTypeList);     //実データ

    pNextItem = pTree->AddNext(pNextItem, pItem, 
                                DefNodeSnap.strDspName, 
                                pItem->GetName());


    //-----------
    // スナップ基準マーカ名
    //-----------
    StdString strListNode;

    if(m_pCtrl)
    {
        std::vector<CDrawingObject*> lstNodeObj;
        m_pCtrl->SearchObjectByType(&lstNodeObj, DT_NODE);

        bool bFirst = true;
        foreach(CDrawingObject* pObj, lstNodeObj)
        {

            if (!pObj)
            {
                continue;
            }

            if (pObj == this)
            {
                continue;
            }

            if (!bFirst)
            {
                strListNode += _T(",");
            }
             
            strListNode += pObj->GetName();
            bFirst = false;
        }
    }

    CStdPropertyItemDef DefBasePoint (PROP_DROP_DOWN, //型
        GET_STR(STR_PRO_NODE_BASE_POINT),      //データ表示名
        _T("BasePoint"),                       //変数名
        GET_STR(STR_PRO_INFO_NODE_BASE_POINT), //データ説明
        false,                                 //編集可、不可
        DISP_NONE,                             //単位
        m_psNd->strSnapOrginNode,                   //初期値
        strListNode,                           //最小値・リスト設定
        0);                                

    pItem = new CStdPropertyItem(
                                    DefBasePoint,                //定義データ
                                    PropPropertyBasePoint,       //入力時コールバック
                                    m_psProperty,                //親グリッド
                                    NULL,                        //更新時コールバック
                                    &m_psNd->strSnapOrginNode);       //実データ

    pNextItem = pTree->AddNext(pNextItem, pItem, 
                                DefBasePoint.strDspName, 
                                pItem->GetName());



    return pGroupItem;

}

/**
 *  @brief   スナップタイプ->文字列変換
 *  @param   [in] dwSnapType  スナップタイプ
 *  @retval  スナップタイプをカンマ区切りで表した文字列
 *  @note
 */
StdString CDrawingNode::_SnapTypeListToString(DWORD dwSnapType) const
{
    StdString strRet;
    using namespace VIEW_COMMON;

    if (dwSnapType & SNP_ALL_POINT)
    {
        strRet += GetSnapName(SNP_ALL_POINT);

    }

    if (dwSnapType & SNP_DISTANCE)
    {
        if (!strRet.empty())
        {
            strRet += _T(",");
        }
        strRet += GetSnapName(SNP_DISTANCE);
    }

    if (dwSnapType & SNP_ANGLE)
    {
        if (!strRet.empty())
        {
            strRet += _T(",");
        }
        strRet += GetSnapName(SNP_ANGLE);
    }

    return strRet;
}

/**
 *  @brief   文字列->スナップタイプ変換
 *  @param   [in] lstString  スナップタイプをカンマ区切りで表した文字列
 *  @retval  スナップタイプ
 *  @note
 */
DWORD CDrawingNode::_StringToSnapTypeList(StdString lstString) const
{
    using namespace VIEW_COMMON;
    //!< CSV区切りで分割
    std::vector<StdString> lstSnap;
    std::vector<E_SNAP_TYPE> lstAllItem = {SNP_ALL_POINT, SNP_DISTANCE, SNP_ANGLE};
                                         
    DWORD dwRet = 0;

    CUtil::TokenizeCsv(&lstSnap, lstString);
    StdString strAllItemName;
    foreach(StdString item, lstSnap)
    {
        item = CUtil::Trim(item);
        foreach(E_SNAP_TYPE eType, lstAllItem)
        {
            strAllItemName = GetSnapName(eType);
            if (item == strAllItemName)
            {
                dwRet |= eType;
                break;
            }
        }
    }
    return dwRet;
}

 bool CDrawingNode::CreateNodeData(int iIndex)
 {
     if(!CDrawingObject::CreateNodeData(iIndex))
     {
         if(iIndex == 0)
         {
            return true;
         }
     }
     return false;
 }

CNodeData* CDrawingNode::GetNodeData(int iIndex) 
{
    if(iIndex == 0)
    {
        return m_psNd.get();
    }
    return CDrawingObject::GetNodeData(iIndex);
}

CNodeData* CDrawingNode::GetNodeDataConst(int iIndex) const
{
    if(iIndex == 0)
    {
        return m_psNd.get();
    }
    return CDrawingObject::GetNodeDataConst(iIndex);
}


//!< 
/**
 *  @brief   接続可能オブジェクトリスト
 *  @param   なし
 *  @retval  接続可能オブジェクトリスト（CSV形式)
 *  @note    AS側でプロパテイセットを変更するのに用いる
 */
StdString CDrawingNode::GetConnectabaleObjectList() const
{
    StdString strRet;
    if(!m_pCtrl)
    {
        return strRet;
    }

    double dDistance;

    std::vector<StdString> lstPropertySet;
    GetConnectionPropertySetList(&lstPropertySet);

    std::vector<CDrawingObject*> lstTmp;
    m_pCtrl->SearchObjectByPropertyset(&lstTmp, &lstPropertySet);
    std::multimap<double, CDrawingObject*> mapDistanceObj;
    std::pair<double, CDrawingObject*> pairDistanceObj;

    POINT2D ptPos = GetPoint();
    foreach(CDrawingObject* pObj, lstTmp)
    {
        if (!pObj)
        {
            continue;
        }

        if (pObj->GetType() == DT_NODE)
        {
            continue;
        }

        if (pObj->GetName().empty())
        {
            continue;
        }
        dDistance = pObj->Distance(ptPos);

        pairDistanceObj.first = dDistance;
        pairDistanceObj.second = pObj;

        mapDistanceObj.insert(pairDistanceObj);
    }

    int iCnt = 0;
    CDrawingObject* pObj;
    std::multimap<double, CDrawingObject*>::iterator ite;
    for(ite  = mapDistanceObj.begin();
        ite != mapDistanceObj.end();
        ite++)
    {
        if (!strRet.empty())
        {
            strRet += _T(",");
        }
        pObj = ite->second;

        if (!pObj)
        {
            continue;
        }

        strRet += pObj->GetName();
        iCnt++;
        if(iCnt > 10)
        {
            break;
        }
    }

    return strRet;
}

bool  CDrawingNode::HaveConnectionDest() const
{
    for(auto bind: m_psNd->lstSocket)
    {
        if (bind.iId != -1)
        {
            return true;
        }
    }
    return false;
}

//接続オブジェクトID
int  CDrawingNode::GetConnectionObjectId(int iNo) const
{
    if (m_psNd->lstSocket.size() <= static_cast<size_t>(iNo))
    {
        return -1;
    }
    if (iNo < 0)
    {
        return -1;
    }
    return m_psNd->lstSocket[iNo].iId;
}



//接続オブジェクト名
StdString  CDrawingNode::GetConnectionObjectName(int iNo) const
{
    StdString strRet;
    auto pObj = GetConnectionObject(iNo);

    if (pObj)
    {
        strRet = pObj->GetName();
    }
    return strRet;
}



//接続オブジェクト名
std::shared_ptr<CDrawingObject> CDrawingNode::GetConnectionObject(int iNo) const
{
    std::shared_ptr<CDrawingObject> pObj;
    int iId = GetConnectionObjectId(iNo);

    if (iId != -1)
    {
        if (m_pCtrl)
        {
            pObj = m_pCtrl->GetObjectById(iId);
        }
    }
    return pObj;
}

/*
bool CDrawingNode::SetConnectionObjectId(int iSocketNo, int iId, int iIndex)
{
    if (m_psNd->lstSocket.size() <= static_cast<size_t>(iSocketNo))
    {
        return false;
    }
    if (iSocketNo < 0)
    {
        return false;
    }

    if (m_psNd->lstSocket[iSocketNo].iId != -1)
    {
        ReleaseConnection(iSocketNo);
    }

    if(m_pCtrl)
    {
        bool bIgnoreGroup = false;
        if (m_psNd->eConnectionObject == CNodeData::NODE)
        {
            bIgnoreGroup = true;
        }
        
        auto pObj = m_pCtrl->GetObjectById(iId, bIgnoreGroup);
        if (!pObj)
        {
            return false;
        }

        auto pNode = pObj->GetNodeData(iIndex);
        if (!pNode)
        {
            return false;
        }

        if(!pNode->IsAbleToConnect(&m_cd))
        {
            return false;
        }
        pObj->ConnectNode(this, );
    }
    m_psNd->lstSocket[iSocketNo].iId = iId;
    m_psNd->lstSocket[iSocketNo].iIndex = iIndex;
    return true;
}

//接続解除
void  CDrawingNode::ReleaseConnectionByObjectId(int iId)
{
    int iNo = 0;
    for(auto bind: m_psNd->lstSocket)
    {
        if (m_psNd->lstSocket[iNo].iId == bind.iId)
        {
            m_psNd->lstSocket[iNo].iId = -1;
            m_psNd->lstSocket[iNo].iIndex = -1;
            break;
        }
        iNo++;
    }
}

//接続解除
void  CDrawingNode::ReleaseConnection(int iNo)
{
    if(m_pCtrl)
    {
        int iId;
        iId = GetConnectionObjectId(iNo);
        if (iId == -1)
        {
            return;
        }

        bool bIgnoreGroup = false;
        if (m_psNd->eConnectionObject == CNodeData::NODE)
        {
            bIgnoreGroup = true;
        }
        
        auto pObj = m_pCtrl->GetObjectById(iId, bIgnoreGroup);

        if (pObj)
        {
            pObj->ReleaseNode(this);
        }
    }
    m_psNd->lstSocket[iNo].iId = -1;
    m_psNd->lstSocket[iNo].iIndex = -1;
}
*/

//マーカスナップタイプリスト
DWORD  CDrawingNode::GetSnapType() const
{
    return m_psNd->dwSnapType;
}
void CDrawingNode::SetSnapType(DWORD dwSnapType)
{
    m_strTmpSnapTypeList = _SnapTypeListToString(dwSnapType);
    m_psNd->dwSnapType = dwSnapType;
}

//マーカスナップ基準マーカ名
StdString  CDrawingNode::GetSnapOrginNodeName() const
{
    return m_psNd->strSnapOrginNode;
}

void CDrawingNode::SetSnapOrginNodeName(StdString strSnapOrgin)
{
    m_psNd->strSnapOrginNode = strSnapOrgin;
}

/**
 *  @brief   プロパティ変更(位置)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingNode::PropPos (CStdPropertyItem* pData, void* pObj)
{
    CDrawingNode* pDrawing = reinterpret_cast<CDrawingNode*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->m_pCtrl->HideObject(pDrawing->m_nId);
        pDrawing->SetPoint(pData->anyData.GetPoint());
        pDrawing->m_pCtrl->Draw(pDrawing->m_nId);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}


/**
 *  @brief   プロパティ変更(最大接続数)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingNode::PropMaxPlug(CStdPropertyItem* pData, void* pObj)
{
    CDrawingNode* pDrawing = reinterpret_cast<CDrawingNode*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->SetMaxPlug(pData->anyData.GetInt());
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(接続タイプ)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingNode::PropConnectionType (CStdPropertyItem* pData, void* pObj)
{
    CDrawingNode* pDrawing = reinterpret_cast<CDrawingNode*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        int iType = pData->anyData.GetInt();
        E_ACTIVE_PASSIVE eType = static_cast<E_ACTIVE_PASSIVE>(iType);
        pDrawing->SetConnectionType(eType);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(接続対象)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingNode::PropConnectionObject (CStdPropertyItem* pData, void* pObj)
{
    CDrawingNode* pDrawing = reinterpret_cast<CDrawingNode*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        int iType = pData->anyData.GetInt();
        CNodeData::E_CONNECTION_OBJECT eType = static_cast<CNodeData::E_CONNECTION_OBJECT>(iType);
        pDrawing->SetConnectionObjectType(eType);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(接続方向)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingNode::PropConnectionDirection   (CStdPropertyItem* pData, void* pObj)
{
    CDrawingNode* pDrawing = reinterpret_cast<CDrawingNode*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        int iType = pData->anyData.GetInt();
        E_IN_OUT eType = static_cast<E_IN_OUT>(iType);
        pDrawing->SetConnectionDirection(eType);

    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(接続方向)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingNode::PropLineDirection   (CStdPropertyItem* pData, void* pObj)
{
    CDrawingNode* pDrawing = reinterpret_cast<CDrawingNode*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        int iType = pData->anyData.GetInt();
        CNodeData::E_LINE_DIR_TYPE eType = static_cast<CNodeData::E_LINE_DIR_TYPE>(iType);
        pDrawing->SetLineDirection(eType);

    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}
/**
 *  @brief   プロパティ変更(マーカー形状)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingNode::PropMarkerShape (CStdPropertyItem* pData, void* pObj)
{
    CDrawingNode* pDrawing = reinterpret_cast<CDrawingNode*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        int iShape = pData->anyData.GetInt();
        NODE_MARKER_SHAPE eShape = static_cast<NODE_MARKER_SHAPE>(iShape);
        pDrawing->SetMarkerShape(eShape);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}
/**
 *  @brief   プロパティ変更(マーカー色)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingNode::PropMarkerColor (CStdPropertyItem* pData, void* pObj)
{
    CDrawingNode* pDrawing = reinterpret_cast<CDrawingNode*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        COLORREF crMarker = pData->anyData.GetColor();
        pDrawing->SetMarkerColor(crMarker);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(マーカー移動制限)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingNode::PropMarkerMoveType (CStdPropertyItem* pData, void* pObj)
{
    CDrawingNode* pDrawing = reinterpret_cast<CDrawingNode*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        int iMove = pData->anyData.GetInt();
        NODE_MARKER_MOVE eMove = static_cast<NODE_MARKER_MOVE>(iMove);
        pDrawing->SetMarkerMove(eMove);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}
/**
 *  @brief   プロパティ変更(マーカー座標系)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingNode::PropMarkerCord (CStdPropertyItem* pData, void* pObj)
{
    CDrawingNode* pDrawing = reinterpret_cast<CDrawingNode*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        int iCord = pData->anyData.GetInt();
        E_LOCAL_WORLD eCord = static_cast<E_LOCAL_WORLD>(iCord);
        pDrawing->SetMarkerCordinateSystem(eCord);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}
/**
 *  @brief   プロパティ変更(マーカー移動方向)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingNode::PropMarkerDir (CStdPropertyItem* pData, void* pObj)
{
    CDrawingNode* pDrawing = reinterpret_cast<CDrawingNode*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        POINT2D ptDir = pData->anyData.GetPoint();
        pDrawing->SetMarkerDir(ptDir);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(マーカー移動方向)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingNode::PropDispType (CStdPropertyItem* pData, void* pObj)
{
    CDrawingNode* pDrawing = reinterpret_cast<CDrawingNode*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        int iType = pData->anyData.GetInt();
        auto eType = static_cast<DISP_TYPE>(iType);
        pDrawing->SetDispType(eType);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(スナップタイプ)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingNode::PropPropertySnapType   (CStdPropertyItem* pData, void* pObj)
{
    CDrawingNode* pDrawing = reinterpret_cast<CDrawingNode*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        StdString strSnapType = pData->anyData.GetString();
        DWORD dwSnapType;
        dwSnapType = pDrawing->_StringToSnapTypeList(strSnapType);
        pDrawing->SetSnapType(dwSnapType);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(マーカスナップ基準マーカ名)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingNode::PropPropertyBasePoint  (CStdPropertyItem* pData, void* pObj)
{
    CDrawingNode* pDrawing = reinterpret_cast<CDrawingNode*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        StdString strProp = pData->anyData.GetString();
        pDrawing->SetSnapOrginNodeName(strProp);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(接続プロパティセット)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingNode::PropConnectionPropertySet  (CStdPropertyItem* pData, void*pObj)
{
    CDrawingNode* pDrawing = reinterpret_cast<CDrawingNode*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        StdString strProp = pData->anyData.GetString();
        pDrawing->m_psNd->strConnectionPropertySet = strProp;
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
    }
    return true;

}

/**
 *  @brief   参照データに基づいて更新.
 *  @param   
 *  @retval  
 *  @note
 */
CDrawingObject* CDrawingNode::UpdateRef()
{
    CDrawingObject*      pRef;

    pRef = CDrawingObject::UpdateRef();

    if (pRef == NULL)
    {
        return NULL;
    }

    CDrawingNode* pPoint = dynamic_cast<CDrawingNode*>(pRef);

    STD_ASSERT(pPoint != NULL);

    if (pPoint == NULL)
    {
        return false;
    }

    m_psNd->ptPos = pPoint->m_psNd->ptPos;

    m_iChgCnt = pRef->GetChgCnt();
    return pRef;
}

/**
 * @brief   領域取得
 * @param   なし
 * @retval  なし
 * @note	描画領域を取得する
 */
RECT2D CDrawingNode::GetBounds() const
{
    RECT2D rc(m_psNd->ptPos, m_psNd->ptPos);
    return rc;
}

/**
 * @brief   距離
 * @param   [in] pt 
 * @retval  近接点からの距離
 * @note
 */
double CDrawingNode::Distance(const POINT2D& pt) const
{
    return m_psNd->ptPos.Distance(pt);
}

/**
 *  @brief   スナップ点取得.
 *  @param   [out] pLstSnap スナップ点リスト
 *  @retval  true: スナップ点あり
 *  @note
 */
bool CDrawingNode::GetSnapList(std::list<SNAP_DATA>* pLstSnap)const 
{
    using namespace VIEW_COMMON;

    DWORD dwSnap = DRAW_CONFIG->GetSnapType();
    bool bRet = false;

    STD_ASSERT(pLstSnap);

    if (!pLstSnap)
    {
        return false;
    }

    pLstSnap->clear();
    SNAP_DATA snapData;
    snapData.bKeepSelect = false;

    if (dwSnap & SNP_DATUME_POINT)
    {
        snapData.eSnapType = SNP_DATUME_POINT;
        snapData.pt = m_psNd->ptPos;
        pLstSnap->push_back(snapData);
        bRet = true;
    }
    return bRet;
}


//!< ロード終了後処理
void CDrawingNode::LoadAfter(CPartsDef* pDef)
{
    CDrawingObject* pObj = this;
    int iIndex = 0;
    auto pParent = GetParentParts();
    if(pParent)
    {
        pObj = pParent.get();
        iIndex = m_psNd->iIndex;
    }

    for(auto nd: m_psNd->lstPlug)
    {
        if(nd.iId == -1)
        {
            continue;
        }
        
        auto pDest = pDef->GetObjectById(nd.iId);
        if(!pDest)
        {
            continue;
        }

        CNodeData* pCd;

        pCd = pDest->GetNodeData(nd.iIndex);

        if(!pCd)
        {
            continue;
        }
        pCd->AddDistObject(pObj, iIndex);
    }
}

bool CDrawingNode::NodeSelect (CDrawingView* pView)
{                   
    CPartsDef*pDef = pView->GetPartsDef();

    NODE_MARKER_MOVE eMoveMode;
    eMoveMode = GetMarkerMove();

    pDef->Redraw(pView);

    const MAT2D* pMat =  GetParentMatrix();

    if (pMat)
    {
        ms_ptStart = GetPoint() * (*pMat);
    }
    else
    {
        ms_ptStart = GetPoint();
    }

    CNodeData::E_CONNECTION_OBJECT eConnectionObject = CNodeData::NONE;
    eConnectionObject =  GetConnectionObjectType();

    pView->ClearConnectableNode();
    std::vector<CDrawingObject*> lstTmp;

    //TODO: pCtrl->SearchObjectByArea( --- , m_pView->GetViewArea())
    //を適用できるようにするする 現在 GetViewAreaは機能していない
    switch(eConnectionObject)
    {
    case CNodeData::NONE:
        break;

    case CNodeData::NODE:
        pDef->SearchObjectByType(&lstTmp, DT_NODE, NULL, true /*bIgnoreGroup*/);
        foreach(CDrawingObject* pObj, lstTmp)
        {
            CDrawingNode* pNode = dynamic_cast<CDrawingNode*>(pObj);
            if (IsAbleConnectNode(pNode->GetNodeData(0), 0))
            {
                pView->AddConnectableNode(pNode);
            }
        }
        break;

    case CNodeData::FIELD:

        break;
    case CNodeData::OBJECT:
    {
        std::vector<StdString> lstPropertySet; 
        GetConnectionPropertySetList(&lstPropertySet);
        pDef->SearchObjectByPropertyset(&lstTmp, &lstPropertySet);
        foreach(CDrawingObject* pObj, lstTmp)
        {
            if (pObj->GetType() == DT_NODE)
            {
                continue;
            }
            CDrawingNode* pNode = dynamic_cast<CDrawingNode*>(pObj);
            pView->AddConnectableNode(pNode);
        }
    }
        break;

    default:
        break;
    }
    return true;
}

bool CDrawingNode::OnContextMenu(CDrawingView* pView, MOUSE_MOVE_POS posMouse, StdString strNodeMarkerId)
{
    CPartsDef* pDef = pView->GetPartsDef();
    ms_eMenuType = E_MNUT_NONE;

    if (GetConnectionObjectType() != CNodeData::OBJECT)
    {
        //Object接続のみ
        return false;
    }

    std::multimap<double, CDrawingObject*> mapDistanceObj;
    std::pair<double, CDrawingObject*> pairDistanceObj;
    double dDistance;

    std::vector<CDrawingObject*> lstTmp;
    StdString strPropertyGroup;
    std::vector<StdString> lstPropertySet; 
    GetConnectionPropertySetList(&lstPropertySet);

    pDef->SearchObjectByPropertyset(&lstTmp, &lstPropertySet);


    POINT2D ptPos = GetPoint();

    //--------------------------------------------------
    //接続可能なノードをクリック位置の距離と共に登録する
    //--------------------------------------------------
    foreach(CDrawingObject* pObj, lstTmp)
    {
        if (!pObj)
        {
            continue;
        }

        if (pObj->GetType() == DT_NODE)
        {
            continue;
        }

        if (pObj->GetName().empty())
        {
            continue;
        }
        dDistance = pObj->Distance(ptPos);
        CDrawingNode* pNode = dynamic_cast<CDrawingNode*>(pObj);
        pView->AddConnectableNode(pNode);

        pairDistanceObj.first = dDistance;
        pairDistanceObj.second = pObj;

        mapDistanceObj.insert(pairDistanceObj);
    }


    CMenu menu;
    menu.CreatePopupMenu();

    UINT nPopup = MF_STRING | MF_ENABLED | MF_POPUP ;

    int iMaxPlug = GetMaxPlug();
    if (iMaxPlug < 1)
    {
        return false;
    }

    int iCnt = 0;
    CDrawingObject* pObj;
    boost::tuple<int, int> tupNoId;

    if (iMaxPlug == 1)
    {
        int iConnectionId = GetConnectionObjectId(0);
        menu.AppendMenu(nPopup, 0, GET_STR(STR_ACT_NO_CONNECTION)); //未接続
        tupNoId.get<0>() = 0;
        tupNoId.get<1>() = -1;
        ms_mapPosToObjectId[0] = tupNoId;

        int iCheck = 0;
        for(auto ite = mapDistanceObj.begin();
            ite != mapDistanceObj.end();
            ite++)
        {
            iCnt++;
            pObj = ite->second;
            menu.AppendMenu(nPopup, iCnt, pObj->GetName().c_str());
            tupNoId.get<0>() = 0;
            tupNoId.get<1>() = pObj->GetId();
            ms_mapPosToObjectId[iCnt] = tupNoId;

            if (iConnectionId == pObj->GetId())
            {
                iCheck = iCnt;
            }

            if (iCnt > 10)
            {
                break;
            }
        }
        menu.CheckMenuRadioItem(0, iCnt, iCheck, MF_BYPOSITION);
    }
    else
    {
        UINT nPopup = MF_STRING | MF_ENABLED | MF_POPUP ;

        int iPos = 0;
        for(int iPlugNo = 0; iPlugNo < iMaxPlug; iPlugNo++) 
        {
            iCnt = 0;
            StdStringStream  strmName;
            strmName << StdFormat(_T("%s %d")) % GET_STR(STR_ACT_CONNECTION) 
                                                % (iPlugNo + 1);

            CMenu mnuSub;
            mnuSub.CreatePopupMenu();
            HMENU hSubMenu = mnuSub.m_hMenu;
            menu.AppendMenu(MF_POPUP,  (UINT)hSubMenu, strmName.str().c_str());

            tupNoId.get<0>() = iPlugNo;
            tupNoId.get<1>() = -1;
            ms_mapPosToObjectId[iPos++] = tupNoId;

            int iConnectionId = GetConnectionObjectId(iPlugNo);

            mnuSub.AppendMenu(MF_STRING, -1, GET_STR(STR_ACT_NO_CONNECTION)); //未接続
            tupNoId.get<0>() = iPlugNo;
            tupNoId.get<1>() = -1;
            ms_mapPosToObjectId[iPos++] = tupNoId;

            int iCheck = 0;
            for(auto ite = mapDistanceObj.begin();
                ite != mapDistanceObj.end();
                ite++)
            {
                iCnt++;
                pObj = ite->second;
                mnuSub.AppendMenu(MF_STRING, iPos, pObj->GetName().c_str());
                tupNoId.get<0>() = iPlugNo;
                tupNoId.get<1>() = pObj->GetId();
                ms_mapPosToObjectId[iPos++] = tupNoId;

                if (iConnectionId == pObj->GetId())
                {
                    iCheck = iCnt;
                }

                if (iCnt > 10)
                {
                    break;
                }
            }
            mnuSub.CheckMenuRadioItem(0, iCnt, iCheck, MF_BYPOSITION);
        }
    }


    POINT pt = posMouse.ptSel;
    pView->GetWindow()->ClientToScreen(&pt);


    ms_eMenuType = E_MNUT_CONNECTION;
    int iSelObjId;
    iSelObjId = menu.TrackPopupMenu(
        TPM_LEFTALIGN  |    //クリック時のX座標をメニューの左辺にする
        TPM_NONOTIFY   |    
        TPM_RETURNCMD  |    // 関数の戻り値としてメニューＩＤを返す
        TPM_RIGHTBUTTON,    //右クリックでメニュー選択可能とする
        pt.x,   //メニューの表示位置
        pt.y,   
        pView->GetWindow()      //このメニューを所有するウィンドウ
    );

    menu.DestroyMenu();

    auto ite = ms_mapPosToObjectId.find(iSelObjId);
    tupNoId = ite->second;
    int iPlugNo = tupNoId.get<0>();
    int iObjId  = tupNoId.get<1>();
    if (iObjId != -1)

    {
        
        auto pDstObj = pDef->GetObjectById( iObjId , true);
        if( AddToPlug( 0, //iNodeIndex,
                       pDstObj.get(), 
                       -1,
                       iPlugNo ))
        {
            //AS側に通知
            auto pSelObj =  GetParentParts();
            while (pSelObj)
            {
                //AS関数  OnEdit_ChangeNodeMarker呼び出し
                pSelObj->ChangeNode(strNodeMarkerId, NCT_CONNECTION);
                pSelObj = pSelObj->GetParentParts();
            }
        }
    }
    return true;
}

bool CDrawingNode::ReleaseMouseOnNode(CDrawingView* pView, MOUSE_MOVE_POS posMouse)
{
    CNodeData::E_CONNECTION_OBJECT eConnectionObject =  GetConnectionObjectType();
    CPartsDef* pDef = pView->GetPartsDef();

    //対象との接続
    if (eConnectionObject == CNodeData::NODE)
    {
        bool bConnect = false;
        int iId = pView->SearchPos(posMouse.ptSel);

        auto pObject = pDef->GetObjectById( iId , true);

        if (pObject)
        {
            if (pObject->GetType() == DT_NODE)
            {
                auto pDstNode = std::dynamic_pointer_cast<CDrawingNode>(pObject);
                if(pDstNode)
                {
                    int iDstIndex = pDstNode->GetNodeData(0)->iIndex;

                    if(ConnectNode(pDstNode.get(), iDstIndex, 0))
                    {
                        bConnect = true;
                    }
                }
            }
        }


        if (!bConnect)
        {
            //接続解除
            ClearNode();
        }

    }
    else if (eConnectionObject == CNodeData::OBJECT)
    {
        //ノードと同じプロパティセット（プロパティグループ)
        //のオブジェクトをリストアップ
        std::multimap<double, CDrawingObject*> mapDistanceObj;
        std::pair<double, CDrawingObject*> pairDistanceObj;
        std::vector<CDrawingObject*> lstSearch;
                            
        std::vector<CDrawingNode*>* pConnectList= pView->GetConnectableNodeList();

        double dDistance;
        for(auto pNode: *pConnectList)
        {
            dDistance = pNode->Distance(GetPoint());
            pairDistanceObj.first = dDistance;
            pairDistanceObj.second = pNode;
            mapDistanceObj.insert(pairDistanceObj);
        }

        //一番近いオブジェクトを接続対象とする
        //TODO:boundBox内に限定するか考える
        if (mapDistanceObj.size() != 0)
        {
            //自動接続設定は必要か？
            {
                CDrawingObject* pDstObj;
                pDstObj = mapDistanceObj.begin()->second;              

                ConnectNode(pDstObj,  //接続先オブジェクト  
                                 -1,  //接続先NodeId(接続対象がオブジェクトのときは-1)
                                  0); //自NodeId DrawingNodeの場合は0固定 
            }
        }
        else
        {
            int iDstObjId = GetConnectionObjectId(0);
            auto pObject = pDef->GetObjectById( iDstObjId , true);

            if(pObject)
            {
                ReleaseNode(0, pObject.get(), -1);
            }
        }
    }
    return true;
}

void CDrawingNode::OnMenuSelect(CDrawingView* pView, UINT nItemID, UINT nFlags, HMENU hSysMenu)
{
    if (ms_eMenuType == E_MNUT_CONNECTION)
    {
        CPartsDef*   pCtrl  = pView->GetPartsDef();
        pCtrl->ClearMouseEmphasis();
        pCtrl->ClearMouseConnectable();

       auto ite =  ms_mapPosToObjectId.find(nItemID);
       if (ite == ms_mapPosToObjectId.end())
       {
           return;
       }

        boost::tuple<int, int> tupNoId;

        tupNoId = ite->second;
        int iId = tupNoId.get<1>();
        auto pObj = pCtrl->GetObjectById(iId);
        pCtrl->SetMouseEmphasis(pObj);
        pCtrl->DrawDragging(pView);
    }
}

void CDrawingNode::ClearNodeMenu()
{
    ms_eMenuType = E_MNUT_NONE;
    ms_mapPosToObjectId.clear();
}

//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void TEST_CDrawingNode()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}
#endif //_DEBUG