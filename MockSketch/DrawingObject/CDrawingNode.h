/**
 * @brief			CDrawingNodeヘッダーファイル
 * @file			CDrawingNode.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:02:46
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef __DRAWING_NODE_H
#define __DRAWING_NODE_H

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DrawingObject/CDrawingObject.h"
#include "COMMON_HEAD.h"
#include "DrawingObject/CNodeMarker.h"
#include "DrawingObject/CNodeData.h"

/**
 * @class   CDrawingNode
 * @brief   節点データ                     
 */
class CDrawingNode :public CDrawingObject
{
public:

    enum E_MENU_TYPE
    {
        E_MNUT_NONE,
        E_MNUT_CONNECTION,
        E_MNUT_OTHER,
    };


public:
	//!< コンストラクタ
	CDrawingNode();

	//!< コンストラクタ
	CDrawingNode(int nID);

	//!< デストラクタ
	virtual ~CDrawingNode();

	//!< コピーコンストラクター
    CDrawingNode(const CDrawingNode& Obj);

    //!< 相対移動
	virtual void Move(const POINT2D& pt2D);

    //!< 回転
	virtual void Rotate(const POINT2D& pt2D, double dAngle);

    //!< 鏡像
	virtual void Mirror(const LINE2D& line);

    //!< 倍率
	virtual void Scl(const POINT2D& pt2D, double dXScl, double dYScl);

    //!< 行列適用
    virtual void Matrix(const MAT2D& mat2D);

    //!< 交点計算
    virtual void CalcIntersection ( std::vector<POINT2D>* pLiat,   
                                    const CDrawingObject* pObj,
                                    bool bOnline = true,
                                    double dMin = NEAR_ZERO) const;

	//!< 範囲
    virtual bool IsInner( const RECT2D& rcArea, bool bPart) const;

    //!< 描画
    virtual void Draw(CDrawingView* pView, 
                      const std::shared_ptr<CDrawingObject> pParentObject,
                      E_MOUSE_OVER_TYPE eMouseOver = E_MO_NO,
                      int iLayerId = -1) override;

    //!< 描画削除
    virtual void DeleteView(CDrawingView* pView,
                            const std::shared_ptr<CDrawingObject> pParentObject,
                            int iLayerId = -1) override;

    //!< 領域取得
    virtual RECT2D GetBounds() const;

    //!< 距離
    virtual double Distance(const POINT2D& pt) const;

    //!<プロパティ設定
    //virtual void SetProperty(CMFCPropertyGridCtrl *pGrid, UINT uiMask);


    //!< 接続可能オブジェクトリスト(AS)
    StdString  GetConnectabaleObjectList() const;
    

    //-------------------------
    // リフレクション設定
    //-------------------------
friend CDrawingNode;

    //!< 位置設定(AS)
    void  SetPoint(const POINT2D& ptPos); 

    //!< 位置設定
    void  SetPoint(double dX, double dY);

    //!< 位置取得(AS)
    POINT2D&  GetPoint(){return m_psNd->ptPos;}
    const POINT2D&  GetPoint()const{return m_psNd->ptPos;}

    //!< 位置取得
    POINT2D*  GetPointInstance()        {return &m_psNd->ptPos;}
    const POINT2D*  GetPointInstance() const  {return &m_psNd->ptPos;}

    //!< プロパティ初期化
    virtual TREE_GRID_ITEM*  _CreateStdPropertyTree();

    //!< 最大接続数
    void SetMaxPlug(int iMaxConnect);
    //(AS)
    int  GetMaxPlug()const;

    //!< 接続タイプ
    void SetConnectionType(E_ACTIVE_PASSIVE eConnectionType);
    E_ACTIVE_PASSIVE  GetConnectionType()const;

    //!< 接続対象
    void SetConnectionObjectType(CNodeData::E_CONNECTION_OBJECT eObject);
    CNodeData::E_CONNECTION_OBJECT  GetConnectionObjectType()const;

    //!< 接続可能プロパティセットリスト
    bool GetConnectionPropertySetList(std::vector<StdString>* pLst)const;

    //!< 接続方向
    void SetConnectionDirection(E_IN_OUT eInOut);
    E_IN_OUT  GetConnectionDirection()const;

    //!< 接続線方向
    void SetLineDirection(CNodeData::E_LINE_DIR_TYPE eDir);
    CNodeData::E_LINE_DIR_TYPE  GetLineDirection()const;

    //!< マーカー移動制限
    void SetMarkerMove(NODE_MARKER_MOVE eMove);
    NODE_MARKER_MOVE  GetMarkerMove()const;

    //!< マーカー形状
    void SetMarkerShape(NODE_MARKER_SHAPE eMove);
    NODE_MARKER_SHAPE  GetMarkerShape()const;

    //!< マーカー色
    void SetMarkerColor(COLORREF crMarker);
    COLORREF  GetMarkerColor()const;

    //!<マーカー座標系
    void SetMarkerCordinateSystem(E_LOCAL_WORLD eCord);
    E_LOCAL_WORLD  GetMarkerCordinateSystem()const;

    //!<マーカー移動方向
    void SetMarkerDir(POINT2D ptDir);
    POINT2D  GetMarkerDir()const;

    //!<マーカー表示種別
    void SetDispType(DISP_TYPE eDispType);
    DISP_TYPE  GetDispType()const;

    //!< プロパティセット名取得
    //bool SetPropertySetName(StdString strName);
    //StdString  GetPropertySetName()const;

    //接続オブジェクト名
    bool  HaveConnectionDest() const;
    int  GetConnectionObjectId(int iNo ) const;
    //bool SetConnectionObjectId(int iSocketNo, int iId, int iIndex = -1);
    //void  ReleaseConnection(int iNo);
    //void ReleaseAllConnection();

    //(AS)
    StdString GetConnectionObjectName(int iNo ) const;

    //(AS)
    std::shared_ptr<CDrawingObject> GetConnectionObject(int iNo ) const;

    //接続解除(オブジェクト側より)
    void ReleaseConnectionByObjectId(int iId);

    //マーカスナップタイプリスト
    DWORD  GetSnapType() const;
    void SetSnapType(DWORD dwSnapType);

    //マーカスナップ基準マーカ名
    StdString  GetSnapOrginNodeName() const;
    void SetSnapOrginNodeName(StdString strSnapOrgin);


     //!< プロパティ変更(位置)
    static bool PropPos              (CStdPropertyItem* pData, void* pObj);
    static bool PropMaxPlug          (CStdPropertyItem* pData, void* pObj);
    static bool PropConnectionType   (CStdPropertyItem* pData, void* pObj);
    static bool PropConnectionObject (CStdPropertyItem* pData, void* pObj);
    static bool PropConnectionDirection   (CStdPropertyItem* pData, void* pObj);
    static bool PropLineDirection    (CStdPropertyItem* pData, void* pObj);
    static bool PropConnectionPropertySet (CStdPropertyItem* pData, void*pObj);

    static bool PropMarkerShape        (CStdPropertyItem* pData, void* pObj);
    static bool PropMarkerColor        (CStdPropertyItem* pData, void* pObj);
    static bool PropMarkerMoveType     (CStdPropertyItem* pData, void* pObj);
    static bool PropMarkerCord         (CStdPropertyItem* pData, void* pObj);
    static bool PropMarkerDir          (CStdPropertyItem* pData, void* pObj);
    static bool PropDispType           (CStdPropertyItem* pData, void* pObj);

    static bool PropPropertySnapType   (CStdPropertyItem* pData, void* pObj);
    static bool PropPropertyBasePoint  (CStdPropertyItem* pData, void* pObj);

    //-------------------------
    // 参照
    //-------------------------

    //!< 参照データに基づいて更新
    virtual CDrawingObject* UpdateRef();

    //!< スナップ
    virtual bool GetSnapList(std::list<SNAP_DATA>* pLstSnap)const;

    //!< ロード終了後処理
    virtual void LoadAfter(CPartsDef* pDef) override;


    //--------------
    //    接続
    //--------------
    virtual bool CreateNodeData(int iIndex) ;
    virtual CNodeData* GetNodeData(int iIndex) ;
    virtual CNodeData* GetNodeDataConst(int iIndex) const;
    
    virtual int GetCopySrcObjectId() const override{return m_iTmpConnectionObjId;}
    virtual void ResetCopySrcObjectId() override{m_iTmpConnectionObjId = -1;}
    virtual void SetCopyNodeDataMode(bool bSet) override{m_bCopyNodeDataMode = bSet;}
    virtual bool GetCopyNodeDataMode() const override{return m_bCopyNodeDataMode;}

    virtual int GetNodeDataNum() const{ return 1;}


    //---------------
    //Action Command
    //---------------
    bool OnContextMenu(CDrawingView* pView, MOUSE_MOVE_POS posMouse, StdString strNodeMarkerId);
    void OnMenuSelect(CDrawingView* pView, UINT nItemID, UINT nFlags, HMENU hSysMenu);
    bool ReleaseMouseOnNode(CDrawingView* pView, MOUSE_MOVE_POS posMouse);
    bool NodeSelect (CDrawingView* pView);
    POINT2D GetStartNodePosition(){return ms_ptStart;}
    virtual void ClearNodeMenu();

protected:

    //!< 節点描画
    void _Node( CDrawingView* pView, 
                 int iLayerId,
                 const CDrawingObject* pParentObject,
                 COLORREF crPen, 
                 int iId,
                 E_MOUSE_OVER_TYPE eMouseOver) const;

    //!< 節点描画(マウスオーバー時)
    virtual void DrawOver(CDrawingView* pView, bool bOver);

    //!< 節点描画
    void _ScrNode(CDrawingView* pView, 
                   int iLayerId,
                   POINT Pt, 
                   COLORREF crPen, 
                   int iId,
                   E_MOUSE_OVER_TYPE eMouseOver) const;

protected:
    std::unique_ptr<CNodeData> m_psNd;


    //=======================
    //  保存不要データ
    //=======================
    //スナップタイプリスト
    StdString m_strTmpSnapTypeList;

    //描画用
    std::unique_ptr<CMarkerData> m_psMarkerData;
    bool m_bCopyNodeDataMode;
    int   m_iTmpConnectionObjId;
    //-----------------------
    //一度に一つしか使用しない
    static E_MENU_TYPE ms_eMenuType;
    static std::map<int, boost::tuple<int, int> > ms_mapPosToObjectId;
    static POINT2D ms_ptStart;

protected:

    StdString _SnapTypeListToString(DWORD dwSnapType) const;

    DWORD _StringToSnapTypeList(StdString lstString) const;

private:
    friend class boost::serialization::access;  
    template<class Archive>

    void serialize(Archive& ar, const unsigned int version)
    {
        CNodeData nd;
        if(!Archive::is_loading::value)
        {
            nd = *m_psNd;
        }

        SERIALIZATION_INIT
            ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(CDrawingObject);
            SERIALIZATION_BOTH("NodeData"       , nd);
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
        if(Archive::is_loading::value)
        {
            *m_psNd = nd;
        }
    }

};

BOOST_CLASS_VERSION(CDrawingNode, 0);

/*
//Fieldと接続するノード
class CDrawingFieldNode :public CDrawingNode
{
public:
    static const int ms_iMaxCh = 3;

	//!< コンストラクタ
	CDrawingFieldNode();

	//!< コンストラクタ
	CDrawingFieldNode(int nID);

	//!< デストラクタ
	virtual ~CDrawingFieldNode();

	//!< コピーコンストラクター
    CDrawingFieldNode(const CDrawingFieldNode& Obj);

    //!< 接続Ch単位種別
    void SetUnitKind(int iCh, StdString strUnitKind);
    StdString  GetUnitKind(int iCh)const;

    //!< 接続Ch単位
    void SetUnit(int iCh, StdString strUnit);
    StdString  GetUnit(int iCh)const;

    static bool PropUnitKind_1       (CStdPropertyItem* pData, void* pObj);
    static bool PropUnit_1           (CStdPropertyItem* pData, void* pObj);
    static bool PropUnitKind_2       (CStdPropertyItem* pData, void* pObj);
    static bool PropUnit_2           (CStdPropertyItem* pData, void* pObj);
    static bool PropUnitKind_3       (CStdPropertyItem* pData, void* pObj);
    static bool PropUnit_3           (CStdPropertyItem* pData, void* pObj);

protected:
    //!< プロパティ初期化
    virtual TREE_GRID_ITEM*  _CreateStdPropertyTree();

protected:
    //!< 接続Ch
    StdString           m_chConnectionKind[ms_iMaxCh];
    StdString           m_chConnection[ms_iMaxCh];


private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
            ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(CDrawingNode);
            SERIALIZATION_BOTH("ConnectionUnit"     , m_chConnection);
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }
    

};


//Objectと接続するノード
class CDrawingObjectNode :public CDrawingNode
{
	//!< コンストラクタ
	CDrawingObjectNode();

	//!< コンストラクタ
	CDrawingObjectNode(int nID);

	//!< デストラクタ
	virtual ~CDrawingObjectNode();

	//!< コピーコンストラクター
    CDrawingObjectNode(const CDrawingObjectNode& Obj);

    //!< プロパティセット名取得
    bool SetPropertySetName(StdString strName);
    StdString  GetPropertySetName()const;

    static bool PropPropertySetName    (CStdPropertyItem* pData, void* pObj);


protected:
    //!< プロパティ初期化
    virtual TREE_GRID_ITEM*  _CreateStdPropertyTree();

protected:
    //!< 接続プロパティセット名
    StdString           m_PropertySetName;

private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
            ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(CDrawingNode);
            SERIALIZATION_BOTH("PropertySetName"     , m_PropertySetName);
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }


};
*/

#endif 
