/**
 * @brief			CDrawingObject実装ファイル
 * @file			CDrawingObject.cpp
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:37
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MockSketch.h"
#include "CProjectCtrl.h"
#include "./CDrawingObject.h"

#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/CElementDef.h"
#include "./CDrawingPoint.h"
#include "./CDrawingLine.h"
#include "./CDrawingCircle.h"
#include "./CDrawingText.h"
#include "./CDrawingParts.h"
#include "./CDrawingDim.h"
#include "./CDrawingDimL.h"
#include "./CDrawingDimH.h"
#include "./CDrawingDimV.h"
#include "./CDrawingDimA.h"
#include "./CDrawingDimD.h"
#include "./CDrawingDimR.h"
#include "./CDrawingDimC.h"
#include "./CDrawingImage.h"
#include "./CDrawingEllipse.h"
#include "./CDrawingSpline.h"
#include "./CDrawingCompositionLine.h"
#include "./CDrawingElement.h"
#include "./CDrawingField.h"
#include "./CDrawingNode.h"
#include "./CDrawingConnectionLine.h"
#include "./CDrawingGroup.h"
#include "./CDrawingReference.h"
#include "./CDimText.h"
#include "DefinitionObject/View/CDrawingView.h"
#include "Utility/CustomPropertys.h"
#include "Utility/CStdPropertyTree.h"
#include "Utility/CPropertyGrid.h"
#include "Utility/CPropertyGroup.h"
#include "Utility/CUtility.h"
#include "Utility/ExtText/TEXT_TYPE.H"
#include "Utility/ExtText/FONT_DATA.H"
#include "Utility/ExtText/TEXT_FRAME.H"
#include "Utility/ExtText/TEXT_MARGIN.H"
#include "Utility/ExtText/LINE_PROPERTY.H"
#
#include "View/CUndoAction.h"
#include "Script/CScriptEngine.h"
#include "CProjectCtrl.h"

#include "MockSketch.h"
#include "System/CSystem.h"
#include "SubWindow/DefaultSetDlg.h"

#include "AngelScript.h"
/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//テンプレート特殊化  BaseはMOCK__ERROR.h
template<> StdString GetObjectNameT(CDrawingObject* t)
{
    StdString s;
    s = t->GetName();
    return s;
};

StdString SnapToText(const SNAP_DATA snap)
{
    StdStringStream strRet;
    strRet << StdFormat(_T("Snap:%s  iObject:%d Index:%d pt:%s org:%s"))
        % VIEW_COMMON::GetSnapName(snap.eSnapType, true)
        % snap.iObject1
        % snap.iConnectionIndex
        % snap.pt.Str().c_str()
        % snap.ptOrg.Str().c_str();

    return strRet.str();
}

/**
 * コンストラクタ
 */
CDrawingObject::CDrawingObject():
m_nId           (0),
m_iLayerId      (1),
m_crObj         (DRAW_CONFIG->crFront),
m_eType         (DT_NONE),
m_eSts          (ES_NOMAL),
m_pCtrl         (NULL),
m_bInitProperty (false),
m_psRefObject   (NULL),
m_iChgCnt       (0),
m_iChgPropUserDefCnt    (0),
m_bUsePropertySet       (false),
m_bPropertySetSelecter  (true),
m_bSetDefault           (true),
m_pPropertySetName  (NULL),
m_pPropertySetMap   (NULL),
m_psPropertySet     (NULL),
m_pUserData         (NULL),
m_iUserDataType     (0),
m_pMatOffset        (NULL),
m_psProperty        (new CStdPropertyTree)


{


}

/**
 * コンストラクタ
 */
CDrawingObject::CDrawingObject(int nID, DRAWING_TYPE eType):
m_nId           (nID),
m_iLayerId      (1),
m_crObj         (DRAW_CONFIG->crFront),
m_eType         (eType),
m_eSts          (ES_NOMAL),
m_pCtrl         (NULL),
m_bInitProperty (false),
m_psRefObject   (NULL),
m_iChgCnt       (0),
m_iChgPropUserDefCnt    (0),
m_bUsePropertySet       (false),
m_bPropertySetSelecter  (true),
m_pPropertySetName  (NULL),
m_pPropertySetMap   (NULL),
m_psPropertySet     (NULL),
m_pUserData         (NULL),
m_iUserDataType     (0),
m_pMatOffset        (NULL),
m_psProperty        (new CStdPropertyTree)
{

}

/**
 *  @brief  コンストラクター.
 *  @param  [in] Obj 
 *  @retval なし 
 *  @note   主に Cloneから呼び出される
 */
CDrawingObject::CDrawingObject(const CDrawingObject& Obj)
{

    m_strName   = Obj.m_strName;
	m_eType     = Obj.m_eType;
	m_crObj     = Obj.m_crObj;
    m_iLayerId  = Obj.m_iLayerId;
    m_nId       = Obj.m_nId;
    m_nOffsetId = Obj.m_nOffsetId;
    m_bUsePropertySet       = Obj.m_bUsePropertySet;
    m_bPropertySetSelecter  = Obj.m_bPropertySetSelecter;
    m_pUserData         = 0;
    m_iUserDataType     = 0;
 

    if(Obj.m_psNodeData)
    {
            if(!m_psNodeData)
        {
            m_psNodeData = std::make_shared<CNodeData>();
        }
        *m_psNodeData = *(Obj.m_psNodeData);
    }
    else
    {
        m_psNodeData.reset();
    }

    if (Obj.m_bUsePropertySet)
    {
        m_pPropertySetName = new StdString;
        m_pPropertySetMap  = new std::map<StdString, CAny> ;

        *m_pPropertySetName        = *Obj.m_pPropertySetName;
        *m_pPropertySetMap         = *Obj.m_pPropertySetMap;

        if (Obj.m_psPropertySet)
        {
            m_psPropertySet    = new CPropertySet;
            *m_psPropertySet           = *Obj.m_psPropertySet;
        }
        else
        {
            m_psPropertySet = NULL;
        }

    }
    else
    {
        m_pPropertySetName = NULL;
        m_pPropertySetMap  = NULL;
        m_psPropertySet    = NULL;
    }

	m_eSts = ES_NOMAL;
	if (Obj.GetTmpCopy())
	{
		SetTmpCopy(true);
	}

    //!< 親オブジェクト
    m_pParent    = Obj.m_pParent;      

    //深いコピーを作成しないと、コピー後元オブジェクト削除でエラーが発生する
    m_psProperty  = new CStdPropertyTree; 

    m_strObject   = Obj.m_strObject;

    //!< 参照元オブジェクト
    if (Obj.m_psRefObject != NULL)
    {
        m_psRefObject = new QUERY_REF_OBJECT;

        m_psRefObject->uuidParts = Obj.m_psRefObject->uuidParts;
        m_psRefObject->iObjectId = Obj.m_psRefObject->iObjectId;
    }
    else
    {
        m_psRefObject = NULL;
    }

    m_iChgCnt = 0;
    m_iChgPropUserDefCnt = 0;

    m_pCtrl = Obj.m_pCtrl;

    m_bInitProperty     = Obj.m_bInitProperty;
    m_iPropFeatures     = Obj.m_iPropFeatures;
    m_pMatOffset        = NULL;
    //m_pDrawingParent = NULL;
}


/**
 * デストラクタ
 */

CDrawingObject::~CDrawingObject()
{
    if (m_psProperty)
    {
        delete m_psProperty;
        m_psProperty = NULL;
    }

    /*
    if (m_psRcBounds)
    {
        delete m_psRcBounds;
        m_psRcBounds = NULL;
    }
    */

    if (m_psRefObject)
    {
        delete m_psRefObject;
        m_psRefObject = NULL;
    }

    if (m_pPropertySetName)
    {
        delete m_pPropertySetName;
        m_pPropertySetName = NULL;
    }

    if (m_pPropertySetMap)
    {
        delete m_pPropertySetMap;
        m_pPropertySetMap = NULL;
    }

    if (m_psPropertySet)
    {
        delete m_psPropertySet;
        m_psPropertySet = NULL;
    }


    ClearNode();
    _FreeUserData();
}


/**
 *  参照パラメータ設定
 *  @param  [in] pObj コピー元
 *  @param  [in] cpy  コピー方法
 *  @note 　
 */
void CDrawingObject::SetRefParam(const CDrawingObject* pObj, COPY_TYPE cpy)
{
    //参照データ設定
    // 現状は CP_DEFAULTのみ



    if (!(cpy & CP_NO_REF))
    {
        if( m_psRefObject == NULL)
        {
            m_psRefObject = new QUERY_REF_OBJECT;
        }

        if (pObj->m_psRefObject == NULL)
        {
            if (pObj->m_pCtrl)
            {
                m_psRefObject->uuidParts = pObj->m_pCtrl->GetPartsId();
                m_psRefObject->iObjectId = pObj->m_nId;
            }
            else
            {
                delete m_psRefObject;
                m_psRefObject = NULL;
            }
        }
        else
        {
            m_psRefObject->uuidParts = pObj->m_psRefObject->uuidParts;
            m_psRefObject->iObjectId = pObj->m_psRefObject->iObjectId;
        }
    }

    //ID
    if (cpy & CP_ID)
    {
        m_nId = pObj->m_nId;
    }
    else if (cpy & CP_ID_CREATE)
    {
        //IDの生成を行う(Ctrlへの登録は行わない)
        if (m_pCtrl)
        {
            int iId;
            iId = m_pCtrl->CreateId();
            SetId(iId);
        }
    }

    //カウンタクリア
    m_iChgCnt = pObj->GetChgCnt();
}

/**
 *  複製.
 *  @param      [in] pObj 複製元
 *  @param      [in] bRef 参照元設定
 *  @retval     複製データ
 *  @note  iDは別途設定してください
 *         名称はコピーされるので注意してください
 */
CDrawingObject* CDrawingObject::Clone(const CDrawingObject* pObj, bool bRef)
{
    CDrawingObject* pNew = NULL;
    const type_info& info = typeid( *pObj );  // 実行時型情報を取得

    CDrawingObject* pTmpObj = const_cast<CDrawingObject*>(pObj);
    if( info == typeid( CDrawingPoint ) )
    {
        pNew = new CDrawingPoint(*dynamic_cast<CDrawingPoint*>(pTmpObj));
    }
    else if( info == typeid( CDrawingLine ) )
    {
        pNew = new CDrawingLine(*dynamic_cast<CDrawingLine*>(pTmpObj));
    }
    else if( info == typeid( CDrawingCircle ) )
    {
        pNew = new CDrawingCircle(*dynamic_cast<CDrawingCircle*>(pTmpObj));
    }
    else if( info == typeid( CDrawingText ) )
    {
        pNew = new CDrawingText(*dynamic_cast<CDrawingText*>(pTmpObj));
    }
    else if( info == typeid( CDrawingParts ) )
    {
        pNew = new CDrawingParts(*dynamic_cast<CDrawingParts*>(pTmpObj), false);
    }
    else if( info == typeid( CDrawingEllipse ) )
    {
        pNew = new CDrawingEllipse(*dynamic_cast<CDrawingEllipse*>(pTmpObj));
    }
    else if( info == typeid( CDrawingSpline ) )
    {
        pNew = new CDrawingSpline(*dynamic_cast<CDrawingSpline*>(pTmpObj));
    }
    else if( info == typeid( CDrawingCompositionLine ) )
    {
        pNew = new CDrawingCompositionLine(*dynamic_cast<CDrawingCompositionLine*>(pTmpObj));
    }
    else if( info == typeid( CDrawingElement ) )
    {
        pNew = new CDrawingElement(*dynamic_cast<CDrawingElement*>(pTmpObj));
    }
    else if( info == typeid( CDrawingField ) )
    {
        pNew = new CDrawingField(*dynamic_cast<CDrawingField*>(pTmpObj));
    }
    else if( info == typeid( CDrawingNode ) )
    {
        pNew = new CDrawingNode(*dynamic_cast<CDrawingNode*>(pTmpObj));
    }
    else if( info == typeid( CDrawingConnectionLine ) )
    {
        pNew = new CDrawingConnectionLine(*dynamic_cast<CDrawingConnectionLine*>(pTmpObj));
    }
    else if( info == typeid( CDrawingDimL ) )
    {
        pNew = new CDrawingDimL(*dynamic_cast<CDrawingDimL*>(pTmpObj));
    }
    else if( info == typeid( CDrawingDimH ) )
    {
        pNew = new CDrawingDimH(*dynamic_cast<CDrawingDimH*>(pTmpObj));
    }
    else if( info == typeid( CDrawingDimV ) )
    {
        pNew = new CDrawingDimV(*dynamic_cast<CDrawingDimV*>(pTmpObj));
    }
    else if( info == typeid( CDrawingDimA ) )
    {
        pNew = new CDrawingDimA(*dynamic_cast<CDrawingDimA*>(pTmpObj));
    }
    else if( info == typeid( CDrawingDimD ) )
    {
        pNew = new CDrawingDimD(*dynamic_cast<CDrawingDimD*>(pTmpObj));
    }
    else if( info == typeid( CDrawingDimR ) )
    {
        pNew = new CDrawingDimR(*dynamic_cast<CDrawingDimR*>(pTmpObj));
    }
    else if( info == typeid( CDrawingDimC ) )
    {
        pNew = new CDrawingDimC(*dynamic_cast<CDrawingDimC*>(pTmpObj));
    }
    else if( info == typeid( CDrawingImage ) )
    {
        pNew = new CDrawingImage(*dynamic_cast<CDrawingImage*>(pTmpObj));
    }
    else if( info == typeid( CDrawingGroup ) )
    {
        pNew = new CDrawingGroup(*dynamic_cast<CDrawingGroup*>(pTmpObj));
    }
    else if( info == typeid( CDrawingReference ) )
    {
        pNew = new CDrawingReference(*dynamic_cast<CDrawingReference*>(pTmpObj));
    }
    else
    {
        DBG_ASSERT(!_T("Unknown Type"));
    }

    if (bRef)
    {
        if (pNew)
        {
            //要 Deep Copy 
            pNew->SetRefParam(pObj, CP_DEFAULT);
            pNew->CloneAfter();
        }
    }
    return pNew;
}

/**
 *  複製.
 *  @param      [in] pObj 複製元
 *  @param      [in] bRef 参照元設定
 *  @retval     複製データ
 *  @note  iDは別途設定してください
 *         名称はコピーされるので注意してください
 */
std::shared_ptr<CDrawingObject> CDrawingObject::CloneShared(const CDrawingObject* pObj, bool bRef)
{
    std::shared_ptr<CDrawingObject> pNew(nullptr);
    const type_info& info = typeid( *pObj );  // 実行時型情報を取得

    CDrawingObject* pTmpObj = const_cast<CDrawingObject*>(pObj);
    if( info == typeid( CDrawingPoint ) )
    {
        pNew = std::make_shared<CDrawingPoint>(*dynamic_cast<CDrawingPoint*>(pTmpObj));
    }
    else if( info == typeid( CDrawingLine ) )
    {
        pNew = std::make_shared<CDrawingLine>(*dynamic_cast<CDrawingLine*>(pTmpObj));
    }
    else if( info == typeid( CDrawingCircle ) )
    {
        CDrawingCircle* pCircle = dynamic_cast<CDrawingCircle*>(pTmpObj);
        pNew = std::make_shared<CDrawingCircle>(*pCircle);
    }
    else if( info == typeid( CDrawingText ) )
    {
        pNew = std::make_shared<CDrawingText>(*dynamic_cast<CDrawingText*>(pTmpObj));
    }
    else if( info == typeid( CDrawingParts ) )
    {
        auto  pParts = dynamic_cast<CDrawingParts*>(pTmpObj);
        pNew = std::make_shared<CDrawingParts>(*pParts, false);

        auto pNewParts = std::dynamic_pointer_cast<CDrawingParts>(pNew);
        pNewParts->SetParentShared(pParts);
    }
    else if( info == typeid( CDrawingDimL ) )
    {
        pNew = std::make_shared<CDrawingDimL>(*dynamic_cast<CDrawingDimL*>(pTmpObj));
    }
    else if( info == typeid( CDrawingDimH ) )
    {
        pNew = std::make_shared<CDrawingDimH>(*dynamic_cast<CDrawingDimH*>(pTmpObj));
    }
    else if( info == typeid( CDrawingDimV ) )
    {
        pNew = std::make_shared<CDrawingDimV>(*dynamic_cast<CDrawingDimV*>(pTmpObj));
    }
    else if( info == typeid( CDrawingDimA ) )
    {
        pNew = std::make_shared<CDrawingDimA>(*dynamic_cast<CDrawingDimA*>(pTmpObj));
    }
    else if( info == typeid( CDrawingDimD ) )
    {
        pNew = std::make_shared<CDrawingDimD>(*dynamic_cast<CDrawingDimD*>(pTmpObj));
    }
    else if( info == typeid( CDrawingDimR ) )
    {
        pNew = std::make_shared<CDrawingDimR>(*dynamic_cast<CDrawingDimR*>(pTmpObj));
    }
    else if( info == typeid( CDrawingDimC ) )
    {
        pNew = std::make_shared<CDrawingDimC>(*dynamic_cast<CDrawingDimC*>(pTmpObj));
    }
    else if( info == typeid( CDrawingImage ) )
    {
        pNew = std::make_shared<CDrawingImage>(*dynamic_cast<CDrawingImage*>(pTmpObj));
    }
    else if( info == typeid( CDrawingEllipse ) )
    {
        pNew = std::make_shared<CDrawingEllipse>(*dynamic_cast<CDrawingEllipse*>(pTmpObj));
    }
    else if( info == typeid( CDrawingSpline ) )
    {
        pNew = std::make_shared<CDrawingSpline>(*dynamic_cast<CDrawingSpline*>(pTmpObj));
    }
    else if( info == typeid( CDrawingCompositionLine ) )
    {
        pNew = std::make_shared<CDrawingCompositionLine>(*dynamic_cast<CDrawingCompositionLine*>(pTmpObj));

        auto pCompo = std::dynamic_pointer_cast<CDrawingCompositionLine>(pNew);
        pCompo->SetParentShared();
    }
    else if( info == typeid( CDrawingElement ) )
    {
        pNew = std::make_shared<CDrawingElement>(*dynamic_cast<CDrawingElement*>(pTmpObj));
    }
    else if( info == typeid( CDrawingField ) )
    {
        pNew = std::make_shared<CDrawingField>(*dynamic_cast<CDrawingField*>(pTmpObj));
    }
    else if( info == typeid( CDrawingNode ) )
    {
        pNew = std::make_shared<CDrawingNode>(*dynamic_cast<CDrawingNode*>(pTmpObj));
    }
    else if( info == typeid( CDrawingConnectionLine ) )
    {
        pNew = std::make_shared<CDrawingConnectionLine>(*dynamic_cast<CDrawingConnectionLine*>(pTmpObj));
    }
    else if( info == typeid( CDrawingGroup ) )
    {
        pNew = std::make_shared<CDrawingGroup>(*dynamic_cast<CDrawingGroup*>(pTmpObj));
        auto  pGroup = dynamic_cast<CDrawingGroup*>(pTmpObj);
        pNew = std::make_shared<CDrawingGroup>(*pGroup);

        auto pNewGroup = std::dynamic_pointer_cast<CDrawingGroup>(pNew);
        pNewGroup->SetParentShared(pGroup);

    }
    else if( info == typeid( CDrawingReference ) )
    {
        pNew = std::make_shared<CDrawingReference>(*dynamic_cast<CDrawingReference*>(pTmpObj));
    }
    else
    {
        DBG_ASSERT(!_T("Unknown Type"));
    }
    if (bRef)
    {
        if (pNew)
        {
            //要 Deep Copy 
            pNew->SetRefParam(pObj, CP_DEFAULT);
            pNew->CloneAfter();
        }
    }
    return pNew;
}


/**
 *  複製.
 *  @param      [in] pObj 複製元
 *  @param      [in] bRef 参照元設定
 *  @retval     複製データ
 *  @note  iDは別途設定してください
 *         名称はコピーされるので注意してください
 */
std::unique_ptr<CDrawingObject> CDrawingObject::CloneUnique(const CDrawingObject* pObj, bool bRef)
{
    std::unique_ptr<CDrawingObject> pNew(nullptr);
    const type_info& info = typeid( *pObj );  // 実行時型情報を取得

    CDrawingObject* pTmpObj = const_cast<CDrawingObject*>(pObj);
    if( info == typeid( CDrawingPoint ) )
    {
        pNew = std::make_unique<CDrawingPoint>(*dynamic_cast<CDrawingPoint*>(pTmpObj));
    }
    else if( info == typeid( CDrawingLine ) )
    {
        pNew = std::make_unique<CDrawingLine>(*dynamic_cast<CDrawingLine*>(pTmpObj));
    }
    else if( info == typeid( CDrawingCircle ) )
    {
        CDrawingCircle* pCircle = dynamic_cast<CDrawingCircle*>(pTmpObj);
        pNew = std::make_unique<CDrawingCircle>(*pCircle);
    }
    else if( info == typeid( CDrawingText ) )
    {
        pNew = std::make_unique<CDrawingText>(*dynamic_cast<CDrawingText*>(pTmpObj));
    }
    else if( info == typeid( CDrawingParts ) )
    {
        pNew = std::make_unique<CDrawingParts>(*dynamic_cast<CDrawingParts*>(pTmpObj), false);
    }
    else if( info == typeid( CDrawingDimL ) )
    {
        pNew = std::make_unique<CDrawingDimL>(*dynamic_cast<CDrawingDimL*>(pTmpObj));
    }
    else if( info == typeid( CDrawingDimH ) )
    {
        pNew = std::make_unique<CDrawingDimH>(*dynamic_cast<CDrawingDimH*>(pTmpObj));
    }
    else if( info == typeid( CDrawingDimV ) )
    {
        pNew = std::make_unique<CDrawingDimV>(*dynamic_cast<CDrawingDimV*>(pTmpObj));
    }
    else if( info == typeid( CDrawingDimA ) )
    {
        pNew = std::make_unique<CDrawingDimA>(*dynamic_cast<CDrawingDimA*>(pTmpObj));
    }
    else if( info == typeid( CDrawingDimD ) )
    {
        pNew = std::make_unique<CDrawingDimD>(*dynamic_cast<CDrawingDimD*>(pTmpObj));
    }
    else if( info == typeid( CDrawingDimR ) )
    {
        pNew = std::make_unique<CDrawingDimR>(*dynamic_cast<CDrawingDimR*>(pTmpObj));
    }
    else if( info == typeid( CDrawingDimC ) )
    {
        pNew = std::make_unique<CDrawingDimC>(*dynamic_cast<CDrawingDimC*>(pTmpObj));
    }
    else if( info == typeid( CDrawingImage ) )
    {
        pNew = std::make_unique<CDrawingImage>(*dynamic_cast<CDrawingImage*>(pTmpObj));
    }
    else if( info == typeid( CDrawingEllipse ) )
    {
        pNew = std::make_unique<CDrawingEllipse>(*dynamic_cast<CDrawingEllipse*>(pTmpObj));
    }
    else if( info == typeid( CDrawingSpline ) )
    {
        pNew = std::make_unique<CDrawingSpline>(*dynamic_cast<CDrawingSpline*>(pTmpObj));
    }
    else if( info == typeid( CDrawingCompositionLine ) )
    {
        pNew = std::make_unique<CDrawingCompositionLine>(*dynamic_cast<CDrawingCompositionLine*>(pTmpObj));
    }
    else if( info == typeid( CDrawingElement ) )
    {
        pNew = std::make_unique<CDrawingElement>(*dynamic_cast<CDrawingElement*>(pTmpObj));
    }
    else if( info == typeid( CDrawingField ) )
    {
        pNew = std::make_unique<CDrawingField>(*dynamic_cast<CDrawingField*>(pTmpObj));
    }
    else if( info == typeid( CDrawingNode ) )
    {
        pNew = std::make_unique<CDrawingNode>(*dynamic_cast<CDrawingNode*>(pTmpObj));
    }
    else if( info == typeid( CDrawingConnectionLine ) )
    {
        pNew = std::make_unique<CDrawingConnectionLine>(*dynamic_cast<CDrawingConnectionLine*>(pTmpObj));
    }
    else if( info == typeid( CDrawingGroup ) )
    {
        pNew = std::make_unique<CDrawingGroup>(*dynamic_cast<CDrawingGroup*>(pTmpObj));
    }
    else if( info == typeid( CDrawingReference ) )
    {
        pNew = std::make_unique<CDrawingReference>(*dynamic_cast<CDrawingReference*>(pTmpObj));
    }
    else
    {
        DBG_ASSERT(!_T("Unknown Type"));
    }
    if (bRef)
    {
        if (pNew)
        {
            //要 Deep Copy 
            pNew->SetRefParam(pObj, CP_DEFAULT);
            pNew->CloneAfter();
        }
    }
    return pNew;
}

std::shared_ptr<CDrawingObject> CDrawingObject::CreateShared(DRAWING_TYPE eType)
{
	std::shared_ptr<CDrawingObject> pNew(nullptr);
	switch (eType)
	{
	case DT_POINT:
		pNew = std::make_shared<CDrawingPoint>();
		break;

	case DT_LINE:
		pNew = std::make_shared<CDrawingLine>();
		break;

	case DT_CIRCLE:
		pNew = std::make_shared<CDrawingCircle>();
		break;

	case DT_TEXT:
		pNew = std::make_shared<CDrawingText>();
		break;

	case DT_PARTS:
	{
		pNew = std::make_shared<CDrawingParts>();
		auto pNewParts = std::dynamic_pointer_cast<CDrawingParts>(pNew);
		pNewParts->SetParentShared(pNewParts.get());
	}
		break;

	case DT_DIM_L:
		pNew = std::make_shared<CDrawingDimL>();
		break;

	case DT_DIM_H:
		pNew = std::make_shared<CDrawingDimH>();
		break;

	case DT_DIM_V:
		pNew = std::make_shared<CDrawingDimV>();
		break;

	case DT_DIM_A:
		pNew = std::make_shared<CDrawingDimA>();
		break;

	case DT_DIM_D:
		pNew = std::make_shared<CDrawingDimD>();
		break;

	case DT_DIM_R:
		pNew = std::make_shared<CDrawingDimR>();
		break;

	case DT_DIM_C:
		pNew = std::make_shared<CDrawingDimC>();
		break;

	case DT_IMAGE:
		pNew = std::make_shared<CDrawingImage>();
		break;

	case DT_ELLIPSE:
		pNew = std::make_shared<CDrawingEllipse>();
		break;

	case DT_SPLINE:
		pNew = std::make_shared<CDrawingSpline>();
		break;

	case DT_COMPOSITION_LINE:
	{
		pNew = std::make_shared<CDrawingCompositionLine>();
		auto pCompo = std::dynamic_pointer_cast<CDrawingCompositionLine>(pNew);
		pCompo->SetParentShared();
	}
		break;

	case DT_ELEMENT:
		pNew = std::make_shared<CDrawingElement>();
		break;

	case DT_FIELD:
		pNew = std::make_shared<CDrawingField>();
		break;

	case DT_NODE:
		pNew = std::make_shared<CDrawingNode>();
		break;

	case DT_CONNECTION_LINE:
		pNew = std::make_shared<CDrawingConnectionLine>();
		break;

	case DT_GROUP:
	{
		pNew = std::make_shared<CDrawingGroup>();
		auto pNewGroup = std::dynamic_pointer_cast<CDrawingGroup>(pNew);
		pNewGroup->SetParentShared(pNewGroup.get());
	}
		break;

	case DT_REFERENCE:
		pNew = std::make_shared<CDrawingReference>();
		break;

	default:
		DBG_ASSERT(!_T("Unknown Type"));
	}

	return pNew;

}

/**
 *  クローン後処理.
 *  @param      なし
 *  @retval     なし
 *  @note     
 */
void CDrawingObject::CloneAfter()
{
    if (!m_psProperty)
    {
        m_psProperty = new CStdPropertyTree;
    }

    ResetProperty();
}

/**
 *  描画パラメータコピー
 *  @param      なし
 *  @retval     なし
 *  @note     
 */
void CDrawingObject::CopyDrawParm(const CDrawingObject& Obj)
{
    SetColor(Obj.GetColor());        
    SetLineWidth(Obj.GetLineWidth());        
    SetLineType(Obj.GetLineType());        
}

/**
 *  アップキャスト用.
 *  @param      なし
 *  @retval     なし
 *  @note     
 */
CDrawingLine* CDrawingObject::AsLine(){ return dynamic_cast<CDrawingLine*>(this);}
CDrawingText* CDrawingObject::AsText(){ return dynamic_cast<CDrawingText*>(this);}
CDrawingCircle* CDrawingObject::AsCircle(){ return dynamic_cast<CDrawingCircle*>(this);}
CDrawingPoint* CDrawingObject::AsPoint(){ return dynamic_cast<CDrawingPoint*>(this);}
CDrawingNode* CDrawingObject::AsNode(){ return dynamic_cast<CDrawingNode*>(this);}
CDrawingEllipse* CDrawingObject::AsEllipse(){ return dynamic_cast<CDrawingEllipse*>(this);}
CDrawingParts* CDrawingObject::AsParts(){ return dynamic_cast<CDrawingParts*>(this);}
CDrawingField* CDrawingObject::AsField(){ return dynamic_cast<CDrawingField*>(this);}
CDrawingSpline* CDrawingObject::AsSpline(){ return dynamic_cast<CDrawingSpline*>(this);}
CDrawingDimL* CDrawingObject::AsDimL(){ return dynamic_cast<CDrawingDimL*>(this);}
CDrawingDimH* CDrawingObject::AsDimH(){ return dynamic_cast<CDrawingDimH*>(this);}
CDrawingDimV* CDrawingObject::AsDimV(){ return dynamic_cast<CDrawingDimV*>(this);}
CDrawingDimA* CDrawingObject::AsDimA(){ return dynamic_cast<CDrawingDimA*>(this);}
CDrawingDimD* CDrawingObject::AsDimD(){ return dynamic_cast<CDrawingDimD*>(this);}
CDrawingDimR* CDrawingObject::AsDimR(){ return dynamic_cast<CDrawingDimR*>(this);}
CDrawingDimC* CDrawingObject::AsDimC(){ return dynamic_cast<CDrawingDimC*>(this);}
CDrawingImage* CDrawingObject::AsImage(){ return dynamic_cast<CDrawingImage*>(this);}

CDrawingConnectionLine* CDrawingObject::AsConnectionLine(){ return dynamic_cast<CDrawingConnectionLine*>(this);}
CDrawingCompositionLine* CDrawingObject::AsCompositionLine(){ return dynamic_cast<CDrawingCompositionLine*>(this);}

CDrawingGroup* CDrawingObject::AsGroup(){ return dynamic_cast<CDrawingGroup*>(this);}
CDrawingReference* CDrawingObject::AsReference(){ return dynamic_cast<CDrawingReference*>(this);}


/**
 *  @brief   参照データに基づいて更新.
 *  @param   
 *  @retval  
 *  @note
 */
CDrawingObject* CDrawingObject::UpdateRef()
{
    //各オブジェクトで実装
    CDrawingObject*      pRef;
    pRef = GetRefObj();

    if (pRef == NULL)
    {
        return NULL;
    }

    if (GetChgCnt() == pRef->GetChgCnt())
    {
        return NULL;
    }

    m_crObj = pRef->m_crObj;
    return pRef;
}

/**
 *  @brief    データ変更カウント取得
 *  @param    なし
 *  @retval   なし
 *  @note
 */
int  CDrawingObject::GetChgCnt() const
{
    return m_iChgCnt;
}

/**
 *  @brief   データ変更カウント設定.
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CDrawingObject::AddChgCnt()
{
    m_iChgCnt++;
    if (m_iChgCnt == INT_MAX)
    {
        m_iChgCnt = 0;
    }

    if (m_pParent.lock())
    {
        auto pGroup = std::dynamic_pointer_cast<CDrawingParts>(m_pParent.lock());

        if (pGroup)
        {
            pGroup->AddGroupChg();
        }
    }
}

/**
 *  @brief   リフレクション設定.
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CDrawingObject::RegisterReflection()
{
    /*
	//RegisterProperty<int>       ( _T("Id")   , &GetId     , &SetId );
	//RegisterProperty<COLORREF>  ( _T("Color"), &GetColor  , &SetColor  );
	//RegisterProperty<int>       ( _T("Layer"), &GetLayer  , &SetLayer  );
	//RegisterProperty<StdString> ( _T("Name") , &GetName   , &SetName   );

	RegisterProperty<int>       ( "Id"   , &GetId     , &SetId );
	RegisterProperty<COLORREF>  ( "Color", &GetColor  , &SetColor  );
	RegisterProperty<int>       ( "Layer", &GetLayer  , &SetLayer  );
	RegisterProperty<StdString> ( "Name" , &GetName   , &SetName   );
    */
}

//!< オフセット
void CDrawingObject::OffsetMatrix(const MAT2D* pMat)
{
    m_pMatOffset = pMat;
    UpdateNodeSocketData();
}

/**
 *  @brief   データ名設定
 *  @param   [in]    strName オブジェクト名
 *  @retval   なし
 *  @note
 */
void CDrawingObject::SetName(LPCTSTR strName)
{
    if(m_pCtrl)
    {
        bool bRet;
        bRet = m_pCtrl->SetObjectName(m_nId, strName);
    }
    m_strName = strName;
}

/**
 *  @brief   データ名設定
 *  @param   [in]    strName オブジェクト名
 *  @retval   なし
 *  @note
 */
void CDrawingObject::SetNameDirect(LPCTSTR strName)
{
    // Cloneで作成したObjに対して名称を変更、若しくは自動生成する場合
    //元の名称のマップが変更されたり、既に登録済みになってしまうことを防ぐ
    m_strName = strName;
}

/**
 *  @brief   データ名取得
 *  @param   [in]    strName オブジェクト名
 *  @retval   なし
 *  @note
 */
StdString CDrawingObject::GetName() const
{
    return m_strName;
}

/**
 *  @brief   データ名設定
 *  @param   [in]    strName オブジェクト名
 *  @retval   なし
 *  @note
 */
StdString CDrawingObject::GetFullName()
{
    //
    StdString strRet;
    auto pParent  =  m_pParent.lock();
    if (!pParent)
    {
        return m_strName;
    }

    strRet = pParent->GetFullName();
    strRet += _T(".");
    strRet +=m_strName;
    return strRet;
}


/**
 *  @brief   トップグループ取得
 *  @param   なし
 *  @retval  トップグループ
 *  @note    ルートは含まれない
 *  @note    対象は、ループ，グループ
 */
/*
    IDについて


    PartsDef ->CDrawingViewで表示
        CDrawingParts m_psObjects  親オブジェクトなし
                          
                                           画面上のID    
                           Line               1 ->GetParentParts = NULL
                           Point              2 ->GetParentParts = NULL
                           ComoisionLine      3 ->GetParentParts = NULL
                                 Line         4 ->GetParentParts = ID 3
                                 Line         5 ->GetParentParts = ID 3
                                 Line         6 ->GetParentParts = ID 3 

                           Group              7 ->GetParentParts = NULL
                                 Line         4 ->GetParentParts = ID 7
                                 Line         5 ->GetParentParts = ID 7
                                 Line         6 ->GetParentParts = ID 7 

                                 Group        8 ->GetParentParts = ID 7 
                                    LINE        9   ->GetParentParts = ID 8
                                    LINE        10  ->GetParentParts = ID 8
                                    LINE        11  ->GetParentParts = ID 8

                                Reference1    12 ->GetParentParts = ID 7
                                    LINE        12  (->GetParentParts = NULL)  GetDrawingParent Reference1
                                    LINE        12  (->GetParentParts = NULL)
                                    LINE        12  (->GetParentParts = NULL)
                                    Reference2   12 ->GetParentParts = 12 
                                        LINE        12  (->GetParentParts = NULL) GetDrawingParent Reference2
                                        LINE        12  (->GetParentParts = NULL)
                                        LINE        12  (->GetParentParts = NULL)

    通常のオブジェクトはIDをすべて展開するが、Referenceの場合はそれができないので
    描画時に親オブジェクトを知る手段がなくなってしまうため、描画時に親オブジェクトを
    引数で渡すようにしています.
*/

const CDrawingObject* CDrawingObject::GetTopDraw(const CDrawingObject* pParentObject) const
{
    const CDrawingObject* pRet = this;
    if (!pParentObject)
    {
        return pRet;
    }

    pRet = pParentObject;
    const CDrawingObject* pGroup = pParentObject;

    int nCnt = 0;
    while(1)
    {
        auto pParentGroup = pGroup->GetDrawingParent();
        pRet = pGroup;
        pGroup = pParentGroup.get();
        if (pParentGroup == NULL)
        {
            break;
        }
        nCnt++;
    }
    return pRet;
}


std::shared_ptr<CDrawingObject> CDrawingObject::GetTopGroup()
{
    auto pGroup = GetParentParts();
    auto pRet  = shared_from_this();
    while(1)
    {
        if (!pGroup)
        {
            break;
        }

        if ((pGroup->GetId() == 0) ||
            (pGroup->GetId() == -1)||
            (pGroup->GetId() == ID_TEMP_GROUP))
        {
            break;
        }

        auto pParentGroup = pGroup->GetParentParts();
        pRet = pGroup;
        pGroup = pParentGroup;
    }
    return pRet;
}

/**
 *  @brief   コントロール設定.
 *  @param   [in] pCtrl オブジェクトコントロールへのポインタ
 *  @retval  なし
 *  @note
 */
void CDrawingObject::SetPartsDef(CPartsDef* pCtrl)
{
    m_pCtrl = pCtrl;
}

/**
 *  @brief   コントロール取得.
 *  @param   
 *  @retval   オブジェクトコントロールへのポインタ
 *  @note
 */
CPartsDef* CDrawingObject::GetPartsDef() const
{
    return m_pCtrl;
}

/**
 *  @brief   ID割り当て.
 *  @param   なし
 *  @retval  設定したID値
 *  @note
 */
int CDrawingObject::AssignId(bool bKeepUnfindId)
{
    if(m_pCtrl == NULL)
    {
        return -1;
    }

    int iId;
    iId = m_pCtrl->CreateId();
    SetId(iId);
    return iId;
}

/**
 *  @brief   描画開始(マウスオーバー時).
 *  @param   [in] pView 表示View
 *  @retval  なし
 *  @note
 */
void CDrawingObject::DrawOverStart(CDrawingView* pView)
{
    DrawOver(pView, true);
//DB_PRINT("DrawOverStart\n");
}

/**
 *  @brief   描画終了(マウスオーバー時).
 *  @param   [in] pView 表示View
 *  @retval  なし
 *  @note
 */
void CDrawingObject::DrawOverEnd(CDrawingView* pView) 
{
    //pView->FlipSelectToFront();
    DrawOver(pView, false);
//DB_PRINT("DrawOverEnd\n");
}

/**
 *  @brief   描画(マウスオーバー時).
 *  @param   [in] pView 表示View
 *  @param   [in] bOver true:開始 false:終了
 *  @retval  なし
 *  @note
 */
void CDrawingObject::DrawOver(CDrawingView* pView, bool bOver) 
{
}

/**
 *  @brief   プロパティ初期化.
 *  @param   [in] pCtrl オブジェクトコントロールへのポインタ
 *  @retval  最終グループ
 *  @note
 */
TREE_GRID_ITEM*  CDrawingObject::_CreateStdPropertyTree()
{
    if (!m_pCtrl)
    {
        return NULL;
    }
    //オブジェクト種別
    m_strObject = VIEW_COMMON::GetObjctTypeName(m_eType);
    boost::tuple< CStdPropertyItem*, void*> pairVal;

    //int iId;
    CStdPropertyItem* pItem;
    NAME_TREE<CStdPropertyItem>* pTree;

    NAME_TREE_ITEM<CStdPropertyItem>* pNextItem;
    NAME_TREE_ITEM<CStdPropertyItem>* pGroupItem;

    if (!m_psProperty)
    {
        m_psProperty = new CStdPropertyTree;
    }

    m_psProperty->SetParent(this);
    pTree = m_psProperty->GetTreeInstance();
    pTree->Clear();

    //グループ作成
    pGroupItem  = pTree->AddChild(NULL, NULL, GET_STR(STR_PRO_FIG), _T("Property"));


    //定義追加

    //-----------
    //オブジェクト種別
    //-----------
    CStdPropertyItemDef DefType(PROP_STR  , 
        GET_STR(STR_PRO_TYPE)    ,
        _T("Type"),
        GET_STR(STR_PRO_INFO_TYPE), 
        false,
        DISP_NONE,
        m_strObject);

    pItem = new CStdPropertyItem( DefType, PropTmp, m_psProperty, NULL, (void*)&m_strObject);
    pNextItem = pTree->AddChild(pGroupItem, pItem, DefType.strDspName, pItem->GetName());
    STD_ASSERT(pItem != NULL);


    //-----------
    //データ名
    //-----------
    m_strName = m_pCtrl->GetObjectName(GetId());
    CStdPropertyItemDef DefName(PROP_STR  , 
        GET_STR(STR_PRO_NAME)    ,
        _T("Name"),
        GET_STR(STR_PRO_INFO_NAME),  
        true,
        DISP_NONE,
        m_strName);

    pItem = new CStdPropertyItem( DefName, PropName, m_psProperty, NULL, (void*)&m_strName);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefName.strDspName, pItem->GetName());

    //-----------
    // タグ
    //-----------
    CStdPropertyItemDef DefTag(PROP_STR  , 
        GET_STR(STR_PRO_TAG)    , 
        _T("Tag"),
        GET_STR(STR_PRO_INFO_TAG),  
        true,
        DISP_NONE,
        m_strTag);

    pItem = new CStdPropertyItem( DefTag, PropTag, m_psProperty, NULL, (void*)&m_strTag);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefTag.strDspName, pItem->GetName());


    //-----------
    //色
    //-----------
    CStdPropertyItemDef DefColor(PROP_COLOR, 
        GET_STR(STR_PRO_COLOR)   ,
        _T("Color"),
        GET_STR(STR_PRO_INFO_COLOR), 
        true,
        DISP_NONE,
        DRAW_CONFIG->crFront);

    pItem = new CStdPropertyItem( DefColor, PropColor, m_psProperty, NULL, &m_crObj);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefColor.strDspName, pItem->GetName());

    //-----------
    //レイヤーID
    //-----------
    CStdPropertyItemDef DefLayer(PROP_INT_RANGE, 
        GET_STR(STR_PRO_LAYER_ID),
        _T("Layer"),
        GET_STR(STR_PRO_INFO_LAYER_ID),
         true,
         DISP_NONE,
         0, 0, m_pCtrl->GetLayerMax() - 1);

    pItem = new CStdPropertyItem( DefLayer, PropLayer, m_psProperty, NULL, &m_iLayerId);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefLayer.strDspName, pItem->GetName());

    //-----------
    //識別ＩＤ
    //-----------
    CStdPropertyItemDef DefId(PROP_INT, 
        GET_STR(STR_PRO_ID), 
        _T("Id"),
        GET_STR(STR_PRO_INFO_ID),
         false,
         DISP_NONE,
         0);


    m_nOffsetId = GetId();
    pItem = new CStdPropertyItem( DefId, PropTmp, m_psProperty, NULL, &m_nOffsetId);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefId.strDspName, pItem->GetName());

    //-----------
    // 実行時表示
    //-----------
    CStdPropertyItemDef DefVisible(PROP_BOOL, 
        GET_STR(STR_PRO_VISIBLE), 
        _T("Visible"),
        GET_STR(STR_PRO_INFO_VISIBLE),
         false,
         DISP_NONE,
         true);


    pItem = new CStdPropertyItem( DefVisible, PropVisible, m_psProperty, NULL, &m_bVisible);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefVisible.strDspName, pItem->GetName());


    if (m_bUsePropertySet)
    {

        if (m_bPropertySetSelecter)
        {
            std::set<StdString> lstName;
            StdString strList;

            SYS_PROPERTY->GetPropertySetNameList(&lstName);

            strList = _T("NONE");

            bool bFirst = true;
            foreach(StdString name, lstName)
            {
                strList += _T(",");
                strList += name;
            }

            //-----------------
            // プロパティセット
            //-----------------
            CStdPropertyItemDef DefPropSet  (PROP_DROP_DOWN, //型
                GET_STR(STR_PRO_SET_NAME),            //データ表示名
                _T("PropertySetName"),      //名称
                GET_STR(STR_PRO_INFO_SET_NAME),        //データ説明
                false,                                 //編集可、不可
                DISP_NONE,                             //単位
                *m_pPropertySetName,                      //初期値
                strList,                               //最小値・リスト設定
                0);                                

            pItem = new CStdPropertyItem(
                                         DefPropSet,                 //定義データ
                                         PropPropertySetName,        //入力時コールバック
                                         m_psProperty,               //親グリッド
                                         NULL,                       //更新時コールバック
                                         m_pPropertySetName);        //実データ

            pNextItem = pTree->AddNext(pNextItem, pItem, 
                                        DefPropSet.strDspName, 
                                        pItem->GetName());

        }
    }


	//----------------------------
	// ディフォルト値設定
	//----------------------------
	if (m_bSetDefault)
	{
		auto pProj = THIS_APP->GetProject().lock();
		DefaultSetDlg* pDefDlg = pProj->GetDefaultObjectDlg();
		pDefDlg->SetProjectAndObject(pProj.get(), this);


		CDialog* pDlg = pDefDlg;
		static StdString strDisp = GET_STR(STR_INIT_DEFAULT_SET);

		CStdPropertyItemDef DefDefaultSet(PROP_SETTING,    //表示タイプ
			GET_STR(STR_PRO_DEFAULT_SET),            //表示名
			_T("Default"),                             //変数名
			GET_STR(STR_PRO_INFO_DEFAULT_SET),          //表示説明
			false,                                    //編集可不可
			DISP_NONE,                                //表示単位
			strDisp,                                //初期値
			pDlg);

		pItem = new CStdPropertyItem(DefDefaultSet,
			PropDummy,
			m_psProperty,
			NULL,
			(void*)& strDisp);

		pNextItem = pTree->AddNext(pNextItem, pItem,
			DefDefaultSet.strDspName,
			pItem->GetName());
	}

    return pGroupItem;
}


/**
 *  @brief   プロパティ画面更新準備
 *  @param   なし
 *  @retval  なし
 *  @note  
 */
void CDrawingObject::_IntilizeStdProperty()
{
#ifdef DEBUG_PROPERTY
DB_PRINT(_T("_IntilizeStdProperty  プロパティ画面更新準備 \n"));
#endif
int iPorpCnt = 0;
    CDrawingObject* pRefObj = GetRefObj();
    if (pRefObj != NULL)
    {
        iPorpCnt = pRefObj->m_pCtrl->GetUserPropertySet()->GetChgCnt();
    }

    if (!m_bInitProperty)
    {
#ifdef DEBUG_PROPERTY
DB_PRINT(_T("_IntilizeStdProperty プロパティ画面更新準備\n"));
#endif
        TREE_GRID_ITEM* pGroupItem;
        pGroupItem = _CreateStdPropertyTree();
        
        
        
        _CreateStdUserPropertyTree(pGroupItem);

        m_bInitProperty = true;
        m_iChgPropUserDefCnt = iPorpCnt;
    }
}

/**
 *  @brief   プロパティ画面更新準備
 *  @param   なし
 *  @retval  なし
 *  @note  
 */
void CDrawingObject::_PrepareUpdatePropertyGrid()
{
}


/**
 *  @brief   プロパティ画面更新
 *  @param   [in] pGrid プロパティグリッド
 *  @retval  なし
 *  @note
 */
void CDrawingObject::_UpdatePropertyGrid(CMFCPropertyGridCtrl *pGrid)
{
    m_psProperty->UpdatePropertyGrid(pGrid, false /*bSetVisibleOnly*/);
}



/**
 *  @brief   プロパティセット名設定
 *  @param   [in] strName プロパティグリッド
 *  @retval  なし
 *  @note
 */
bool CDrawingObject::_SetPropertySetName(StdString strName)
{
    if (!m_bUsePropertySet)
    {
        return false;
    }

    if(!m_pPropertySetName)
    {
        return false;
    }

    if (*m_pPropertySetName != strName)
    {

        *m_pPropertySetName = strName;


        CPropertySet* pSet;
        pSet = SYS_PROPERTY->GetPropertySetInstance(*m_pPropertySetName);

        if (!m_psPropertySet)
        {
            m_psPropertySet = new CPropertySet;
        }

        *m_psPropertySet = *pSet;
    }

    return true;
}


/**
 *  @brief   プロパティ設定.
 *  @param   [in] pGrid  グリッドコントロールへのポインタ
 *  @param   [in] uiMask オブジェクトコントロールへのポインタ
 *  @retval  なし
 *  @note
 */
void CDrawingObject::SetProperty(CMFCPropertyGridCtrl *pGrid, UINT uiMask)
{
    CPropertyGrid* pPropGrid = dynamic_cast<CPropertyGrid*>(pGrid);

    pPropGrid->RemoveAll();
    int iPorpCnt = 0;

    if (!m_psProperty)
    {
        m_psProperty = new CStdPropertyTree;
    }


    CDrawingObject* pRefObj = GetRefObj();
    if (pRefObj != NULL)
    {
        iPorpCnt = pRefObj->m_pCtrl->GetUserPropertySet()->GetChgCnt();
    }

    bool bUpdateUserProp = false;

    if (iPorpCnt != m_iChgPropUserDefCnt)
    {
        bUpdateUserProp = true;
    }

    TREE_GRID_ITEM* pGroupItem = NULL;

    if (!m_bInitProperty)
    {
        //プロパティーツリーの生成
        pGroupItem = _CreateStdPropertyTree();
    }

    if (!m_bInitProperty && m_bUsePropertySet)
    {
        //プロパティセット
        pGroupItem = _CreatePropertySetTree(pGroupItem);

        InitPropertySetValue(true);
    }

    if (!m_bInitProperty || bUpdateUserProp)
    {
        //ユーザ定義プロパティーツリーの生成
        pGroupItem = _CreateStdUserPropertyTree(pGroupItem);

        bool bCreate = false;
        if (!m_bInitProperty)
        {
            bCreate = true;
        }
        
        //定義が変更された箇所のユーザ定義初期値をObjectに設定する
        InitUserPropertyValue(bCreate);


        m_iChgPropUserDefCnt = iPorpCnt;
    }

#if 0
    m_psProperty->GetTreeInstance()->DspTree();
    m_psProperty->Print();
#endif

    //プロパティーツリーからグリッドを生成する
    pPropGrid->SetGridData(m_psProperty);

    //グリッド更新前準備
    _PrepareUpdatePropertyGrid();

    //オブジェクトのデータをグリッドに反映させる
    _UpdatePropertyGrid(pGrid);

    m_bInitProperty = true;
}


/**
 * @brief   プロパティセット設定
 * @param   [in] pGroupItem
 * @retval  なし
 * @note	
 */
TREE_GRID_ITEM* CDrawingObject::_CreatePropertySetTree(TREE_GRID_ITEM* pGroupItem)
{
    if (!m_bUsePropertySet)
    {
        return pGroupItem;
    }

    //-------------
    //
    //-------------
    NAME_TREE<CStdPropertyItem>*      pTree;
    pTree = m_psProperty->GetTreeInstance();

    STD_ASSERT(pTree);

    NAME_TREE_ITEM<CStdPropertyItem>* pTreeUserProp;
    pTreeUserProp = pTree->GetItem(GET_STR(STR_PRO_PROPERTY_SET_VAL), 0);

    if (pTreeUserProp)
    {
        pTree->DeleteChild(pTreeUserProp);
    }
    //-------------

    PAIR_ITEM_VAL pairVal;

    CStdPropertyItemDef* pGridDef;
    CStdPropertyItem* pItem;

    NAME_TREE_ITEM<CStdPropertyItem>* pNextItem;


    if (*m_pPropertySetName == _T("NONE"))
    {
        return pGroupItem;
    }


    if (!m_psPropertySet)
    {
        CPropertySet* pSet;
        pSet = SYS_PROPERTY->GetPropertySetInstance(*m_pPropertySetName);
        m_psPropertySet = new CPropertySet;
        *m_psPropertySet = *pSet;
    }

    if (!m_psPropertySet)
    {
        return pGroupItem;
    }
    
    int iMax = m_psPropertySet->GetCnt();
    if (iMax <= 0)
    {
        return pGroupItem;
    }

    TREE_GRID_ITEM* pPropertySetItem;

    pPropertySetItem = pTree->GetItem(GET_STR(STR_PRO_PROPERTY_SET_VAL), 0);
    if (!pPropertySetItem)
    {
        //新規にプロパティセットを設定する
        pPropertySetItem  = pTree->AddNext(pGroupItem, NULL, GET_STR(STR_PRO_PROPERTY_SET_VAL), _T("PropertySetVal"));
    }

    for (int iCnt = 0; iCnt < iMax; iCnt++)
    {
        pGridDef = m_psPropertySet->GetInstance(iCnt);

        pItem = new CStdPropertyItem(
            *pGridDef, 
            ChgPropertySetVal, 
            m_psProperty, 
            NULL, 
            this,   //データを取得するときは this->GetPropertySetVal(pGridDef->strDspName)
            PROP_CATE_PROP_SET);

        int iOffset = 1;
        if (pGridDef->type == PROP_POINT2D)
        {
            iOffset = 2;
        }
        else if(pGridDef->type == PROP_RECT2D)
        {
            iOffset = 4;
        }


        if (iCnt == 0)
        {
            pNextItem = pTree->AddChild(pPropertySetItem, pItem, pGridDef->strDspName, pItem->GetName(), iOffset);
        }
        else
        {
            pNextItem = pTree->AddNext(pNextItem, pItem, pGridDef->strDspName, pItem->GetName(), iOffset);
        }
    }

    return pPropertySetItem;
}


CDrawingObject::RELATION CDrawingObject::RelationRect(const RECT2D& rcArea) const
{
    if (IsInner(rcArea, false /*bPart*/))
    {
        return REL_INNER;
    }

    if (IsInner(rcArea, true /*bPart*/))
    {
        return REL_PART;
    }

    return REL_NONE;
}


//!< 
/**
 * @brief   選択状態設定
 * @param   [in] bSel; true 
 * @param   [in] pRect
 * @retval  なし
 * @note
 */
void CDrawingObject::SetSelect(bool bSel, const RECT2D* pRect)
{
    if (bSel)
    {
        //選択状態
        m_eSts |= ES_SELECT;
    }
    else
    {
        //選択を解除
        m_eSts &= ~ES_SELECT;

        //関連も解除
        if ((m_eSts & ES_RELATION) == ES_RELATION)
        {
            m_eSts &= ~ES_RELATION;
        }
    }

    if ((m_eSts & ES_SELECT) == ES_SELECT)
    {
        ResetProperty();
    }
}

void CDrawingObject::SetSelectNoRelSelPoint(bool bSel)
{
    SetSelect( bSel );
}

//!< 選択状態取得
bool CDrawingObject::IsSelect() const
{
    return  ((m_eSts & ES_SELECT) == ES_SELECT);
}

void CDrawingObject::SetImaginary(bool bImaginary)
{
    if (bImaginary)   {m_eSts |= ES_IMAGINARY; }
    else              {m_eSts &= ~ES_IMAGINARY; }
}

//!< 選択状態取得
bool CDrawingObject::GetImaginary() const
{
    return  ((m_eSts & ES_IMAGINARY) == ES_IMAGINARY);
}

void CDrawingObject::SetRelation(bool bRelation)
{
    if (bRelation) { m_eSts |= ES_RELATION; }
    else { m_eSts &= ~ES_RELATION; }
}

//!< 選択状態取得
bool CDrawingObject::GetRelation() const
{
    return  ((m_eSts & ES_RELATION) == ES_RELATION);
}


//!< 一時コピー
bool CDrawingObject::GetTmpCopy() const
{
	return  ((m_eSts & ES_TMP_COPY) == ES_TMP_COPY);
}

void CDrawingObject::SetTmpCopy(bool bRelation)
{
	if (bRelation) { m_eSts |= ES_TMP_COPY; }
	else { m_eSts &= ~ES_TMP_COPY; }
}


void CDrawingObject::ClearStatus()
{
    m_eSts = ES_NOMAL;
}

/**
 *  @brief   プロパティ開放
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CDrawingObject::ResetProperty()
{
    NAME_TREE<CStdPropertyItem>* pTree;
    m_bInitProperty = false;
    pTree = m_psProperty->GetTreeInstance();
    pTree->Clear();
}

//
/**
 * @brief  プロパティセット値設定(AS)
 * @param  なし
 * @retval false 失敗
 * @note   スクリプト側からプロパテイセットの初期値を変更できるようにする	
 */
bool CDrawingObject::_SetPropertySetDefVal(StdString strName, 
    int eItem, StdString strItem)
{

   if (!m_psPropertySet)
   {
        CPropertySet* pSet;
        pSet = SYS_PROPERTY->GetPropertySetInstance(*m_pPropertySetName);
        if (!pSet)
        {
            return false;
        }
        m_psPropertySet = new CPropertySet;
        *m_psPropertySet = *pSet;
   }


    int iSize = m_psPropertySet->GetCnt();
    CStdPropertyItemDef* pDef = NULL;
    bool bFind = false;
    for(int iNo = 0; iNo < iSize; iNo++)
    {
        pDef = m_psPropertySet->GetInstance(iNo);
        if (pDef->strValName == strName)
        {
            bFind = true;
            break;
        }
    }

    if (!bFind)
    {
        return false;
    }


    bool bRet = true;
    E_PROPERTY_ITEM_SET eItemSet;
    eItemSet = static_cast<E_PROPERTY_ITEM_SET>(eItem);
    bRet = pDef->SetItemString( eItemSet, strItem, false);
    return bRet;
}

bool CDrawingObject::SetPropertySetDefMin(StdString strName, StdString strItem)
{
    return _SetPropertySetDefVal(strName, PRS_MIN, strItem);
}

bool CDrawingObject::SetPropertySetDefMax(StdString strName, StdString strItem)
{
    return _SetPropertySetDefVal(strName, PRS_MAX, strItem);
}

bool CDrawingObject::SetPropertySetDefNote(StdString strName, StdString strItem)
{
    return _SetPropertySetDefVal(strName, PRS_NOTE, strItem);
}

bool CDrawingObject::SetPropertySetValAny(StdString strName, void* pRef, int refTypeId)
{
    std::map<StdString, CAny>::iterator ite;
    ite = m_pPropertySetMap->find(strName);
    if (ite == m_pPropertySetMap->end())
    {
        return false;
    }

    UniVal64 uniVal;
    uniVal.pVal = pRef;
    CAny any;

    bool bBaseType = true;
    switch(refTypeId)
    {
    case asTYPEID_VOID: 
        return false;
    case asTYPEID_BOOL:  {   any = (*reinterpret_cast<bool*>(pRef)); break;}
    case asTYPEID_INT8:  {   any = (int)(*reinterpret_cast<signed char*>(pRef)); break;}
    case asTYPEID_INT16: {   any = (int)(*reinterpret_cast<signed short*>(pRef)); break;}
    case asTYPEID_INT32: {   any = (int)(*reinterpret_cast<signed int*>(pRef)); break;}
    case asTYPEID_INT64: {   any = (int)(*reinterpret_cast<signed __int64*>(pRef)); break;}
    case asTYPEID_UINT8: {   any = (int)(*reinterpret_cast<unsigned char*>(pRef)); break;}
    case asTYPEID_UINT16:{   any = (int)(*reinterpret_cast<unsigned char*>(pRef)); break;}
    case asTYPEID_UINT32:{   any = (int)(*reinterpret_cast<float*>(pRef)); break;}
    case asTYPEID_UINT64:{   any = (int)(*reinterpret_cast<float*>(pRef)); break;}
    case asTYPEID_FLOAT: {   any = (double)(*reinterpret_cast<float*>(pRef)); break;}
    case asTYPEID_DOUBLE:{   any = *reinterpret_cast<double*>(pRef); break;}
    default:
        bBaseType = false;
        break;
    }


    if (!bBaseType)
    {
        if (refTypeId & asTYPEID_MASK_OBJECT)
        {
            asIScriptEngine *engine = THIS_APP->GetScriptEngine()->GetEngine();
            asIObjectType *ot = engine->GetObjectTypeById(refTypeId);
            std::string strTypeName = ot->GetName();

            void* pData;
            if (refTypeId & asTYPEID_OBJHANDLE)
            {
                pData = *(void**)pRef;
            }
            else
            {
                 pData = pRef;
            }

            if (strTypeName == "POINT2D")
            {
                any = *(POINT2D*)(pData);
            }
            else if (strTypeName == "RECT2D")
            {
                any = *(RECT2D*)(pData);
            }
            else if (strTypeName == "StdString")
            {
                any = *(StdString*)(pData);
            }
            else if (strTypeName == "string")
            {
                any = *(std::string*)(pData);
            }
            else if (strTypeName == "POINTLIST")
            {
                any = *(std::vector<POINT2D>*)(pData);
            }
            else
            {
                return false;
            }

        }
        else
        {
            return false;
        }
    }


    try
    {
        CAny* pAnyVal = &(ite->second);

        if(any.anyData.type() !=  pAnyVal->anyData.type())
        {
            return false;
        }

        *pAnyVal = any;


    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
    }


    //実行中でなければ



    return true;
}

//!< マトリクス取得
MAT2D CDrawingObject::GetMatrix() const
{
    MAT2D mat;
    mat.SetIdentity();
    return mat;
}

//!< 描画用マトリクス取得
const MAT2D* CDrawingObject::GetDrawingMatrix() const
{
    return NULL;
}


/**
 * @brief  描画用マトリクス取得
 * @param  なし
 * @retval 描画用マトリクスへのポインタ
 * @note	
 */
const MAT2D* CDrawingObject::GetParentMatrix() const
{
    const MAT2D* pRet = NULL;
    auto pParent = GetParentParts();

    if (pParent == NULL)
    {
        return pRet;
    }

    pRet = pParent->GetDrawingMatrix();

    return pRet;
}



//描画前にMatrixを使用するときに使う
/*
A
    B
        C


    MatC *	(MatB * (MatA * offsetA) * offsetB) * offsetC
    (MatC * MatB * MatA) * (offsetA * offsetB * offsetC)

*/

MAT2D CDrawingObject::CreateDrawingMatrix()
{
    MAT2D matParent;
    MAT2D matParentOffset;
    MAT2D matDraw   = GetMatrix();
    MAT2D matOffset;
    const MAT2D* pOffset;

    pOffset = GetOffsetMatrix();
    {
        matOffset = (*pOffset);
    }

    auto pParent = GetParentParts();
    while (1)
    {
        if (!pParent)
        {
            break;
        }

        matParent = pParent->GetMatrix();
        matDraw  = matDraw * matParent;

        pOffset = pParent->GetOffsetMatrix();

        if(pOffset)
        {
            matOffset = (*pOffset) * matOffset;
        }

        pParent = pParent->GetParentParts();
    }

    MAT2D matRet;
    matRet = matDraw * matOffset;
    return matRet;
}


const MAT2D* CDrawingObject::GetOffsetMatrix() const
{
    return m_pMatOffset;
}

    //!< ワールド->ローカル座標変換     (AS)
POINT2D CDrawingObject::ConvWorldToLocal(const POINT2D& pt) const
{
    POINT2D tmpPt(pt);
    const MAT2D* pMat;
    MAT2D mat;
    MAT2D matInv;
    pMat =  GetParentMatrix();

    if (pMat)
    {
        mat = (*pMat) * GetMatrix();
    }
    else
    {
        mat = GetMatrix();
    }
    //
    matInv = mat;
    matInv.Inv2D();
//----FORDEBUG
    MAT2D matInv2;
    matInv2 = mat;
    matInv2.Invert();


    MAT2D matInv3;
    matInv3 = mat;
    matInv3.Inv2();

    //----FORDEBUG

    tmpPt.Matrix(matInv);
    return tmpPt;
}

//!< ローカル->ワールド座標変換     (AS)
POINT2D CDrawingObject::ConvLocalToWorld(const POINT2D& pt) const
{
    POINT2D tmpPt(pt);
    const MAT2D* pMat;

    MAT2D mat;
    pMat =  GetParentMatrix();

    if (pMat)
    {
        mat = (*pMat) * GetMatrix();
    }
    else
    {
        mat = GetMatrix();
    }

    tmpPt.Matrix(mat);

    return tmpPt;
}


//!< 同一スケール判定
bool CDrawingObject::IsSameScale(const CDrawingObject* pObj) const
{
    int iLayer = pObj->GetLayer();

    if (m_iLayerId == iLayer)
    {
        return true;
    }

    if (!m_pCtrl)
    {
        return false;
    }

    CLayer* pLayer1;
    CLayer* pLayer2;

    pLayer1 = m_pCtrl->GetLayer(m_iLayerId);
    pLayer2 = m_pCtrl->GetLayer(iLayer);

    double dDiffScl = fabs(pLayer1->dScl - pLayer2->dScl);

    if (dDiffScl < NEAR_ZERO)
    {
        return true;
    }

    return false;
}


/**
 * @brief  プロパティセット使用有無設定
 * @param  [in] なし
 * @retval
 * @note	
 */
bool CDrawingObject::SetUsePropertySet(bool bUse)
{
    if(bUse)
    {
        if (!m_pPropertySetName)
        {
            m_pPropertySetName = new StdString;
            *m_pPropertySetName = _T("NONE");
        }

        if(!m_pPropertySetMap)
        {
            m_pPropertySetMap = new std::map<StdString, CAny> ;
        }

        m_bUsePropertySet = true;
    }
    else
    {
        if (m_pPropertySetName)
        {
            delete m_pPropertySetName;
            m_pPropertySetName = NULL;
        }

        if(m_pPropertySetMap)
        {
            delete m_pPropertySetMap;
            m_pPropertySetMap = NULL;
        }

        if (m_psPropertySet)
        {
            m_psPropertySet    = NULL;
        }

        m_bUsePropertySet = false;
    }
    return true;
}

/**
 * @brief   描画色取得
 * @param   [in]    eMouseOver     直線         
 * @retval   なし
 * @note	
 */
/*
COLORREF CDrawingObject::GetDrawColor(E_MOUSE_OVER_TYPE eMouseOver) const
{

    if (eMouseOver == E_MO_EMPHASIS)
    {
        return  DRAW_CONFIG->crEmphasis;
    }


    COLORREF crObj = m_crObj;
    bool bSelect = false;

    if (IsBelongToRoot())
    {
        if (IsSelect())
        {
            //選択色
            crObj = DRAW_CONFIG->GetSelColor();
            bSelect = true;
        }
    }
    else
    {

        const CDrawingObject* pObj = GetTopGroup();
        STD_ASSERT(pObj != NULL);
        if (pObj == NULL)
        {
            return crObj;
        }

        if (pObj->IsSelect())
        {
            //選択色
            crObj = DRAW_CONFIG->GetSelColor();
            bSelect = true;
        }
        else
        {
            crObj = pObj->GetColor();
        }
    }

    if(!bSelect)
    {
        if (m_pCtrl)
        {
            CLayer* pLayer;
            pLayer = m_pCtrl->GetLayer(m_iLayerId);
            if ( !pLayer->bEnable ||
                 !pLayer->bEnableForScl)
            {
                return DRAW_CONFIG->crDisable;
            }
            else if (pLayer->bUseLayerColor)
            {
                return pLayer->crLayer;
            }
        }
    }

    return crObj;
}
*/

void CDrawingObject::GetCircleRect(RECT* pRc,
    int iLayerId,
    POINT* pStart, POINT* pEnd,
    const CDrawingView* pView, const CIRCLE2D& circle)const
{

    POINT  Pt1;
    POINT  PtStart;
    POINT  PtEnd;

    POINT2D ptCnter = circle.ptCenter;
    double dStart;
    double dEnd;
    if (circle.IsClockwise())
    {
        dStart = circle.GetEnd();
        dEnd = circle.GetStart();
    }
    else
    {
        dStart = circle.GetStart();
        dEnd = circle.GetEnd();
    }
    double dRad = circle.dRad;

    CPartsDef* pCtrl = pView->GetPartsDef();
    CLayer* pLayer = pCtrl->GetLayer(iLayerId);

    double dSclLayer = pLayer->dScl;
    double dDspScl = pView->GetViewScale();
    double dScl = dSclLayer * dDspScl;


    long lRad = long(CUtil::Round(dRad * pView->GetDpu() * dScl));


    pView->ConvWorld2Scr(&Pt1, ptCnter, iLayerId);



    RECT rcEllipse;
    rcEllipse.left = Pt1.x - lRad;
    rcEllipse.top = Pt1.y - lRad;
    rcEllipse.right = Pt1.x + lRad;
    rcEllipse.bottom = Pt1.y + lRad;


    bool bEqualStatEnd = false;
    if (fabs(dEnd - dStart) < NEAR_ZERO)
    {
        dEnd = dStart;
        bEqualStatEnd = true;
    }
    else if (fabs(fabs(dEnd - dStart) - 360.0) < NEAR_ZERO)
    {
        dEnd = dStart;
        bEqualStatEnd = true;
    }

    POINT2D ptStart;
    POINT2D ptEnd;
    double dCosS;
    double dSinS;
    double dCosE;
    double dSinE;

    CUtil::GetTriFunc(dStart, &dSinS, &dCosS);
    ptStart.dX = 2.0 * dRad * dCosS;
    ptStart.dY = 2.0 * dRad * dSinS;
    ptStart = ptStart + ptCnter;
    pView->ConvWorld2Scr(&PtStart, ptStart, iLayerId);

    if (bEqualStatEnd)
    {
        PtEnd = PtStart;
    }
    else
    {
        CUtil::GetTriFunc(dEnd, &dSinE, &dCosE);
        ptEnd.dX = 2.0 * dRad * dCosE;
        ptEnd.dY = 2.0 * dRad * dSinE;
        ptEnd = ptEnd + ptCnter;
        pView->ConvWorld2Scr(&PtEnd, ptEnd, iLayerId);
    }

    *pRc =  rcEllipse;
    *pStart =  PtStart;
    *pEnd   =  PtEnd;
#if 0
DB_PRINT(_T("Start:%f End:%f\
            Rc(%d,%d,%d,%d) H:%d, W:%d,\
            ((%d,%d)-(%d,%d))\n"),  dStart,  dEnd,
    rcEllipse.top, rcEllipse.bottom,
    rcEllipse.right, rcEllipse.left,
    rcEllipse.bottom - rcEllipse.top,
    rcEllipse.right - rcEllipse.left,
    PtStart.x, PtStart.y,
    PtEnd.x, PtEnd.y);
#endif
}

/**
 * @brief  線描画
 * @param   [in]    pLine2D     直線         
 * @param   [in]    iPenStyle   線種        
 * @param   [in]    iWidth      線幅        
 * @param   [in]    crPen       線色        
 * @param   [in]    iId         ID      
 * @retval   なし
 * @note	
 */
void CDrawingObject::Line( CDrawingView* pView,
                        int iLayerId,
                        const LINE2D* pLine2D, 
                        int iPenStyle, int iWidth, 
                        COLORREF crPen, int iId) const
{
    POINT  Pt1, Pt2;

    //現在の表示エリアでクリッピング

    LINE2D line(*pLine2D);
    
    if(iLayerId == -1)
    {
        iLayerId = m_iLayerId;    
    }
    CLayer* pLayer = pView->GetLayer(iLayerId);
    
    

    pView->ConvWorld2Scr(&Pt1, line.Pt(0), iLayerId);
    pView->ConvWorld2Scr(&Pt2, line.Pt(1), iLayerId);

     GetLayerCoror(&crPen, pView, iLayerId);

    pView->SetPen(iPenStyle,  iWidth, crPen, iId);
    pView->Line(Pt1, Pt2);
}


/**
 * @brief  線描画(ペン変更なし)
 * @param   [in]    pView      ビュー         
 * @param   [in]    pLine2D    直線         
 * @retval   なし
 * @note	
 */
void CDrawingObject::LinePri( CDrawingView* pView,
                        int iLayerId,
                        const LINE2D* pLine2D, DRAWING_MODE eMode) const
{
    POINT  Pt1, Pt2;

    //現在の表示エリアでクリッピング

    LINE2D line(*pLine2D);

    //#51 作業中
    /*
    CLayer* pLayer = pView->GetLayer(m_iLayerId);
    if (!line.Clipping(pLayer->rcViewArea))
    {
        return;
    }
    */


    pView->ConvWorld2Scr(&Pt1, line.Pt(0), iLayerId);
    pView->ConvWorld2Scr(&Pt2, line.Pt(1), iLayerId);

    //-------------------

    pView->Line(Pt1, Pt2);
}


/**
 * @brief  線描画(ペン変更なし)
 * @param   [in]    pView      ビュー         
 * @param   [in]    pLstPt     点列         
 * @retval   なし
 * @note	
 */
void CDrawingObject::LineMulti( CDrawingView* pView,
                               int iLayerId,     
                               const std::vector<POINT2D>* pLstPt, bool bSkioFirstPoint) const
{
    LineMulti2(pView, iLayerId, pLstPt, bSkioFirstPoint, false); 
}

void CDrawingObject::LineMulti2( CDrawingView* pView,
    int iLayerId,     
    const std::vector<POINT2D>* pLstPt, bool bSkioFirstPoint,
    bool bFixLayerScl) const
{
    CPoint  Pt1, Pt2;
    LINE2D  line;
    POINT2D pt2DOld;
    std::vector<POINT> lstDot;
    int iSize;

    foreach(POINT2D pt, *pLstPt)
    {
        pView->ConvWorld2Scr(&Pt1, pt, iLayerId, bFixLayerScl);
        iSize = (int)lstDot.size();
        if (iSize != 0)
        {
            Pt2 = lstDot[iSize - 1];
            if (Pt2 != Pt1)
            {
                lstDot.push_back(Pt1);
            }
        }
        else
        {
            lstDot.push_back(Pt1);
        }
    }
    pView->Polyline(lstDot, bSkioFirstPoint);

}
void CDrawingObject::FillMulti( CDrawingView* pView,
                                int iLayerId,
                               const std::vector<POINT2D>* pLstPt) const
{
    FillMulti2(pView, iLayerId, pLstPt, false);
}

void CDrawingObject::FillMulti2( CDrawingView* pView,
    int iLayerId,
    const std::vector<POINT2D>* pLstPt,
    bool bFixLayerScl) const
{
    CPoint  Pt1, Pt2;
    LINE2D  line;
    POINT2D pt2DOld;
    std::vector<POINT> lstDot;
    int iSize;

    foreach(POINT2D pt, *pLstPt)
    {

        pView->ConvWorld2Scr(&Pt1, pt, iLayerId, bFixLayerScl);
        iSize = (int)lstDot.size();
        if (iSize != 0)
        {
            Pt2 = lstDot[iSize - 1];
            if (Pt2 != Pt1)
            {
                lstDot.push_back(Pt1);
            }
        }
        else
        {
            lstDot.push_back(Pt1);
        }
    }
    pView->FillPolyline(lstDot);

}
/**
 * @brief  線描画(ペン変更なし)
 * @param   [in]    pView      ビュー         
 * @param   [in]    pLstLine   線列         
 * @param   [in]    eMode      線列         

 * @retval   なし
 * @note	直線が連続していることが前提
 */
void CDrawingObject::LineMulti( CDrawingView* pView,
                               int iLayerId,
                               const std::vector<LINE2D>* pLstLine, 
                               bool bSkioFirstPoint) const
{
    CPoint  Pt1, Pt2, PtOld;
    POINT2D pt2DOld;
    std::vector<POINT> lstDot;

    int iSize = 0;
    const MAT2D* pMat = GetParentMatrix();
    foreach(LINE2D  line, *pLstLine)
    {
        pView->ConvWorld2Scr(&Pt1, line.Pt(0), iLayerId);
        pView->ConvWorld2Scr(&Pt2, line.Pt(1), iLayerId);

        iSize = (int)lstDot.size();
        if (iSize != 0)
        {
            PtOld = lstDot[iSize - 1];
            if (PtOld != Pt1)
            {
                lstDot.push_back(Pt1);
            }

            if (PtOld != Pt2)
            {
                lstDot.push_back(Pt2);
            }
        }
        else
        {
            lstDot.push_back(Pt1);
            lstDot.push_back(Pt2);
        }
    }

    iSize = (int)lstDot.size();
    if (iSize == 0)
    {
        return;
    }
    pView->Polyline(lstDot, bSkioFirstPoint);
}




/**
 * 円描画   
 * @param   [in]    pView            
 * @param   [in]    crPen            
 * @param   [in]    iWidth            
 * @param   [in]    iId    オブジェクトID        
 * @retval   なし
 * @note	
 */
void  CDrawingObject::Circle( CDrawingView* pView, 
                            int iLayerId,    
                            const CIRCLE2D* pCircle2D,
                            const CDrawingObject* pParentObject,
                            int iPenStyle,
                            int iWidth, 
                            COLORREF crPen, 
                            int iId) const
{

    STD_ASSERT(pView != 0);

    GetLayerCoror(&crPen, pView, iLayerId);

    CIRCLE2D tmpCircle(*pCircle2D);

    MAT2D matDraw;
    if (pParentObject)
    {
        const MAT2D* pMatParent = pParentObject->GetDrawingMatrix();
        if (pMatParent)
        {
            matDraw = (*pMatParent);
        }
    }

    if (m_pMatOffset)
    {
        matDraw = matDraw * (*m_pMatOffset);
    }

    ELLIPSE2D* pEllipse = NULL;

    tmpCircle.Matrix(matDraw, &pEllipse);
    if (pEllipse != NULL)
    {
        //楕円を描画
        std::vector<POINT2D> lstPoint;
        RECT2D viewRect = pView->GetViewArea(iLayerId);
        double dDotLength = pView->DotToLength(1);
        pEllipse->DivideLine(&lstPoint, viewRect, dDotLength);

        pView->SetPen(iPenStyle,  iWidth, crPen, iId);
        LineMulti( pView, iLayerId, &lstPoint);

        delete pEllipse;
        return;
    }

    RECT   rcEllipse;
    POINT  PtStart;
    POINT  PtEnd;

    GetCircleRect(&rcEllipse, iLayerId, &PtStart, &PtEnd, pView, tmpCircle);

    pView->SetPen(iPenStyle,  iWidth, crPen, iId);
#if 0
    DB_PRINT(_T("Circle  SetPen %x \n"),crPen);
#endif
    pView->Arc(rcEllipse, PtStart, PtEnd, tmpCircle.IsClockwise());
}

bool CDrawingObject::DrawigArrow(CDrawingView* pView, 
                                  int iLayerId,
                                  VIEW_COMMON::E_ARROW_TYPE eType,
                                  const POINT2D& pt,     //表示位置
                                  POINT2D vec) const
{
      vec.Normalize();  
      using namespace VIEW_COMMON;
      bool    bFill = false;
      switch(eType)
        {
        case AT_NONE:
            return true;

        case AT_DOT_FILL:
            bFill = true;
        case AT_DOT:
            DrawigArrowDot(pView, iLayerId, pt, bFill);
            break;

        case AT_OBLIQUE:
            DrawigOblique(pView, iLayerId, pt, vec);
            break;

        case AT_FILLED:
            bFill = true;
        case AT_OPEN:
            {
                DrawigArrowOpen(pView, iLayerId, pt, vec, bFill);
            }
            break;

        case AT_DATUM_FILL:
            bFill = true;
        case AT_DATUM_BLANK:
            DrawigDataum(pView, iLayerId, pt, vec, bFill);
            break;
        }
        return true;
}


bool CDrawingObject::DrawigArrowDot(CDrawingView* pView, 
                                   int iLayerId,
                                   const POINT2D& pt,     //表示位置
                                   bool bFill) const
{
    CLayer* pLayer = pView->GetLayer(iLayerId);
    double dScl = pLayer->dScl;
    double dLen = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->dArrowDotSize);  
    double dRad =  dLen * pView->GetDpu() / (dScl * 2.0);

    POINT2D ptTL;
    POINT2D ptBR;
    ptTL.dX    = pt.dX - dRad;
    ptTL.dY    = pt.dY + dRad;
    ptBR.dX    = pt.dX + dRad;
    ptBR.dY    = pt.dY - dRad;


    POINT pt1;
    POINT pt2;
    pView->ConvWorld2Scr(&pt1, ptTL, iLayerId);
    pView->ConvWorld2Scr(&pt2, ptBR, iLayerId);

    RECT rc;
    rc.top    = pt1.y;
    rc.bottom = pt2.y;
    rc.left   = pt1.x;
    rc.right  = pt2.x;

    if (bFill)
    {
        pView->FillEllipse(rc);
    }
    else
    {
        pView->Ellipse(rc);
    }
    return true;
}


//ptVecは正規化を前提
//PenStyle IDは設定済みとする
bool CDrawingObject::DrawigArrowOpen(CDrawingView* pView, 
                                   int iLayerId,
                                   const POINT2D& pt,     //表示位置
                                   const POINT2D& ptVec,   //矢印方向
                                   bool bFill) const
{
    CLayer* pLayer = pView->GetLayer(iLayerId);

    double dLen;
    dLen = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->dArrowSize);  

    LINE2D lineArrow(pt, ((dLen * ptVec) / pLayer->dScl) + pt);

    std::vector<POINT> ptLine;
    ptLine.resize(3);

    lineArrow.Rotate(pt, 15.0);
    pView->ConvWorld2Scr(&ptLine[0], lineArrow.Pt(0), iLayerId);
    pView->ConvWorld2Scr(&ptLine[1], lineArrow.Pt(1), iLayerId);

    lineArrow.Rotate(pt, -30.0);
    pView->ConvWorld2Scr(&ptLine[2], lineArrow.Pt(1), iLayerId);


    if (bFill)
    {
        COLORREF crPen;
        crPen = pView->GetPenColor();
        pView->SetBrush(crPen);
        pView->FillPolyline(ptLine);
    }

    pView->Line(ptLine[0], ptLine[1]);
    pView->Line(ptLine[0], ptLine[2]);

    return true;
}



bool CDrawingObject::DrawigOblique(CDrawingView* pView, 
                            int iLayerId,
                            const POINT2D& pt,
                            const POINT2D& ptVec) const
{

    CLayer* pLayer = pView->GetLayer(iLayerId);

    double dLen;
    dLen = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->dArrowObliqueSize)/ pLayer->dScl;  
    POINT2D ptOblique1 = pt + ptVec * dLen;
    POINT2D ptOblique2 = pt - ptVec * dLen;

    ptOblique1.Rotate(pt, 45.0);
    ptOblique2.Rotate(pt, 45.0);

    POINT pt1;
    POINT pt2;
    pView->ConvWorld2Scr(&pt1, ptOblique1, iLayerId);
    pView->ConvWorld2Scr(&pt2, ptOblique2, iLayerId);

    pView->Line(pt1, pt2);

    return true;
}

bool CDrawingObject::DrawigDataum(CDrawingView* pView, 
                            int iLayerId,
                            const POINT2D& pt,
                            const POINT2D& ptVec,
                            bool bFill) const
{

    CLayer* pLayer = pView->GetLayer(iLayerId);

    double dLen;
    dLen = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->dArrowDatumHeight)/ (pLayer->dScl * 2.0);  

    POINT2D pt1 = pt + ptVec * dLen * sqrt(3.0);
    POINT2D pt2 = pt + ptVec * dLen ;
    POINT2D pt3 = pt - ptVec * dLen ;

    pt2.Rotate(pt, 90.0);
    pt3.Rotate(pt, 90.0);

    std::vector<POINT> ptTri;
    ptTri.resize(3);

    pView->ConvWorld2Scr(&ptTri[0], pt1, iLayerId);
    pView->ConvWorld2Scr(&ptTri[1], pt2, iLayerId);
    pView->ConvWorld2Scr(&ptTri[2], pt3, iLayerId);


    COLORREF crPen;
    if (!bFill)
    {
        crPen = DRAW_CONFIG->GetBackColor();
        pView->SetBrush(crPen);
    }
    else
    {
        crPen = pView->GetPenColor();
        pView->SetBrush(crPen);
    }

    pView->FillPolyline(ptTri);



    pView->Line(ptTri[0], ptTri[1]);
    pView->Line(ptTri[1], ptTri[2]);
    pView->Line(ptTri[2], ptTri[0]);

    return true;

}



/**
 * @brief   描画色取得
 * @param   なし
 * @retval  描画色
 * @note	選択色 > レイヤー色 > オブジェクト色 (グループ色)
 */
COLORREF CDrawingObject::GetDrawColor(CDrawingView* pView,
                                      int iLayerId,
                                      const CDrawingObject* pParentObject,
                                      int* pID, 
									  bool bIgnoreSelect,
                                   	  bool* pFoce) const
{
    //色
    CPartsDef* pCtrl = pView->GetPartsDef();
    CLayer* pLayer   = pCtrl->GetLayer(iLayerId);
    COLORREF crObj;
    STD_ASSERT(pID != NULL);
	bool bForce = false;
	crObj = m_crObj;
#ifdef _DEBUG
	if (THIS_APP->GetDebugCount() > 10)
	{
		int a = 10;
	}
#endif
	if (!pParentObject)
    {
        *pID   = GetId();
        if (IsSelect())
        {
            //選択色
            crObj = DRAW_CONFIG->GetSelColor();
			bForce = true;
        }
        else 
        {
			GetLayerCoror(&crObj, pView, iLayerId);
        }

		if (GetRelation())
		{
			crObj = DRAW_CONFIG->crEmphasis;
			bForce = true;
		}
		else if (GetImaginary())
		{
			crObj = DRAW_CONFIG->crImaginary;
			bForce = true;
		}

    }
    else
    {
        const CDrawingObject* pObjTop = GetTopDraw(pParentObject);
        STD_ASSERT(pObjTop != NULL);

        if (pObjTop->GetType() == DT_REFERENCE)
        {
            *pID = pObjTop->GetId();
        }
        else
        {
            *pID   = GetId();
        }

		crObj = pObjTop->GetColor();

        //*pID   = pObj->GetId();
        if (pObjTop->IsSelect() && !bIgnoreSelect)
        {
            //選択色
            crObj = DRAW_CONFIG->GetSelColor();
			bForce = true;
		}
        else
        {
            if(!GetLayerCoror(&crObj, pView, iLayerId))
            {
                const CDrawingParts* pGroup = dynamic_cast<const CDrawingParts*>(pObjTop);

                crObj = pObjTop->GetColor();
                if (pGroup)
                {
                    if(!pGroup->GetUseGroupColor())
                    {
                        crObj = m_crObj;
						bForce = true;
					}
                }
            }
        }

		if (pObjTop->GetRelation())
		{
			crObj = DRAW_CONFIG->crEmphasis;
			bForce = true;
		}
		else if (pObjTop->GetImaginary())
		{
			crObj = DRAW_CONFIG->crImaginary;
			bForce = true;
		}
    }

	if (pFoce)
	{
		*pFoce = bForce;
	}

    return   crObj;
}

/**
 *  @brief   プロパティ変更(名称)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingObject::PropName(CStdPropertyItem* pData , void* pObj)
{
    CDrawingObject* pDrawing = reinterpret_cast<CDrawingObject*>(pObj);

    StdString strName = pData->anyData.GetString();

    if (strName != _T(""))
    {
        if(!CUtil::IsIdentifer(strName))
        {
            AfxMessageBox(GET_STR(STR_ERROR_OBJECT_NAME));
            return false;
        }
    }

    try
    {
        pDrawing->m_strName = strName;
        pDrawing->m_pCtrl->SetObjectName(pDrawing->GetId(), pDrawing->m_strName.c_str());
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }

    //TAG:[名称チェック]
    return true;
}


/**
 *  @brief   プロパティ変更(色)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingObject::PropColor(CStdPropertyItem* pData , void* pObj)
{
    CDrawingObject* pDrawing = reinterpret_cast<CDrawingObject*>(pObj);
    try
    {
        COLORREF crColor = pData->anyData.GetColor();
        pDrawing->AddChgCnt();
        pDrawing->SetColor(crColor);
        pDrawing->m_pCtrl->Draw(pDrawing->GetId());
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
    }
    return true;
}

/**
 *  @brief   プロパティ変更(レイヤー)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingObject::PropLayer(CStdPropertyItem* pData, void* pObj)
{
    CDrawingObject* pDrawing = reinterpret_cast<CDrawingObject*>(pObj);
    try
    {
        int  iLayer = pData->anyData.GetInt();
        pDrawing->AddChgCnt();
        pDrawing->SetLayer(iLayer);
        pDrawing->m_pCtrl->Draw(pDrawing->GetId());
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
    }
    return true;
}

/**
 *  @brief   プロパティ変更(タグ)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingObject::PropTag     (CStdPropertyItem* pData, void*pObj)
{
    CDrawingObject* pDrawing = reinterpret_cast<CDrawingObject*>(pObj);
    try
    {
        std::vector<StdString> lstStr;
        pDrawing->AddChgCnt();
        StdString strTag = pData->anyData.GetString();
        CUtil::TokenizeCsv(&lstStr, strTag);

        std::set<StdString> setStr;

        foreach(StdString& str, lstStr)
        {
            setStr.insert(str);
        }

        StdString strRet;
        foreach(const StdString& str, setStr)
        {
            strRet += str;
            strRet += _T(",");
        }

        pDrawing->SetTag(strRet);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
    }
    return true;
}

/**
 *  @brief   プロパティ変更(プロパティセット)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingObject::PropPropertySetName  (CStdPropertyItem* pData, void*pObj)
{
    CDrawingObject* pDrawing = reinterpret_cast<CDrawingObject*>(pObj);
    try
    {
        std::vector<StdString> lstStr;
        pDrawing->AddChgCnt();

        StdString strProp = pData->anyData.GetString();

        if (*(pDrawing->m_pPropertySetName) != strProp)
        {
            pDrawing->_SetPropertySetName(strProp);

            //TODO:リスト再描画
        }
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
    }
    return true;
}

/**
 *  @brief   プロパティ変更(表示)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingObject::PropVisible (CStdPropertyItem* pData, void*pObj)
{
    CDrawingObject* pDrawing = reinterpret_cast<CDrawingObject*>(pObj);
    try
    {
        bool bVisible;
        pDrawing->AddChgCnt();
        bVisible = pData->anyData.GetBool();
        pDrawing->SetVisible(bVisible);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
    }
    return true;
}

/*
 *  @brief   プロパティ変更(ダミー)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingObject::PropDummy(CStdPropertyItem* pData, void* pObj)
{
	CDrawingObject* pDrawing = reinterpret_cast<CDrawingObject*>(pObj);
	return true;
}


bool CDrawingObject::GetLayerCoror(COLORREF* pPen,
                                   const CDrawingView* pView, 
                                   int iLayerId) const
{
    CPartsDef* pCtrl = pView->GetPartsDef();
    CLayer* pLayer   = pCtrl->GetLayer(iLayerId);

    bool bEnable = (pLayer->bEnableForScl || pView->IsPrintMode());

    if ( !pLayer->bEnable ||
         !bEnable)
    {
        *pPen = DRAW_CONFIG->crDisable;
        return true;
    }
    else if (pLayer->bUseLayerColor)
    {
        *pPen = pLayer->crLayer;
        return true;
    }
    return  false;
}

//!< 
/**
 *  @brief   デバッグ用文字
 *  @param   なし
 *  @param   デバッグ用文字
 *  @retval     なし
 *  @note
 */
StdString CDrawingObject::GetStr() const
{
    StdStringStream strRet;
    if (m_pCtrl)
    {
        strRet << StdFormat(_T("Type:%s ID:%d  Name[%s][%s]")) 
                                            % VIEW_COMMON::GetObjctTypeName(m_eType)
                                            % GetId()
                                            % m_strName
                                            % m_pCtrl->GetObjectName(m_nId);

        if (m_psRefObject)
        {
            StdStringStream strm;
            strm << m_psRefObject->uuidParts << std::endl;
            

            strRet << StdFormat(_T(" Parts:%s Obj:%d")) 
                                                % strm.str()
                                                % m_psRefObject->iObjectId;
        }

    }
    else
    {
        strRet << StdFormat(_T("Type:%s ID:%d  Name[---]")) 
                                            % VIEW_COMMON::GetObjctTypeName(m_eType)
                                            % GetId();

    }
    return strRet.str();
}

/**
 *  @brief   タグ追加
 *  @param   [in] strAdd
 *  @retval  なし
 *  @note
 */
 bool CDrawingObject::AddTag(StdString strAdd)
 {
     if (!m_strTag.empty())
     {
         std::vector<StdString> lstTag;
         CUtil::TokenizeCsv(&lstTag, m_strTag);

         auto ite = std::find(lstTag.begin(), lstTag.end(), strAdd);
         if (ite != lstTag.end())
         {
             return false;
         }
         m_strTag +=_T(","); 
     }
     strAdd = CUtil::Trim(strAdd);
     m_strTag += strAdd; 
     return true;
 }

/**
 *  @brief   タグリスト取得
 *  @param   [out] pTagList
 *  @retval  なし
 *  @note
 */
void  CDrawingObject::GetTagList(std::vector<StdString>* pTagList) const
{
    CUtil::TokenizeCsv(pTagList, m_strTag);
}

/**
 *  @brief   ID取得
 *  @param   なし
 *  @param   ID値
 *  @retval     なし
 *  @note
 */
int  CDrawingObject::GetId() const
{
    return m_nId;
}

/**
 *  @brief   親オブジェクト(グループ)設定
 *  @param   [in] 親オブジェクト
 *  @retval  なし
 *  @note    親オブジェクトになれるのはグループ,LOOP
 */
void CDrawingObject::SetParentParts(std::weak_ptr<CDrawingObject> wParent)
{

    auto pParent = wParent.lock();
    if (pParent)
    {
        STD_ASSERT((pParent->GetType() == DT_PARTS) ||
                   (pParent->GetType() == DT_FIELD) ||
                   (pParent->GetType() == DT_GROUP) ||
                   (pParent->GetType() == DT_REFERENCE) ||
                   (pParent->GetType() == DT_COMPOSITION_LINE ) );
    }

    m_pParent = wParent;
}

/**
 *  @brief   親オブジェクト(グループ)取得
 *  @param   なし
 *  @retval  親オブジェクトへの参照
 *  @note   
 */
std::shared_ptr<CDrawingObject> CDrawingObject::GetParentParts() const
{
    //CDrawingParts* pRet = dynamic_cast<CDrawingParts*>(m_pParent);
    return m_pParent.lock();
}

//CDrawingReferenceのときはオブジェクトに親を設定できない
//GetParentPartsをなくしてこれに一本化できるか検討する
const std::shared_ptr<CDrawingObject> CDrawingObject::GetDrawingParent() const
{
    return m_pDrawingParent.lock();
}

void CDrawingObject::_SetDrawingParent(std::weak_ptr<CDrawingObject> wObj)
{
    m_pDrawingParent = wObj;
}

/**
 *  @brief   ルートに属しているか
 *  @param   なし
 *  @retval  true Rootに属している
 *  @note   
 */
bool CDrawingObject::IsBelongToRoot(const CDrawingObject* pParentObject) const
{
    if(pParentObject == NULL)
    {
        return true;
    }
    else
    {
        if ((pParentObject->GetType() != DT_PARTS) &&
            (pParentObject->GetType() != DT_FIELD) &&
            (pParentObject->GetType() != DT_GROUP) &&
            (pParentObject->GetType() != DT_COMPOSITION_LINE) &&
            (pParentObject->GetType() != DT_REFERENCE)
            )
        {
            return false;
        }

        if(pParentObject->GetParentParts() == NULL)
        {
            return true;
        }
    }
    return false;
}


/**
 *  @brief   参照元変更の有無
 *  @param   なし
 *  @retval  true 変更あり
 *  @note   
 */
bool CDrawingObject::IsChangeRef() const
{
    if (m_psRefObject == NULL)
    {
        return false;
    }
    
    if ( GetRefObj()->GetChgCnt() == GetChgCnt())
    {
        return false;
    }
    return true;
}
    
/**
 *  @brief   参照元の取得
 *  @param   なし
 *  @retval  参照元データ
 *  @note   
 */
CDrawingObject* CDrawingObject::GetRefObj() const
{
    CDrawingObject* pRetObj = NULL; 
    if (m_psRefObject == NULL)
    {
        return NULL;
    }


    if (m_pCtrl)
    {
        CProjectCtrl* pProj = m_pCtrl->GetProject();
        if (pProj)
        {
            pRetObj = pProj->QueryObject(m_psRefObject);
        }
    }

    if (pRetObj == NULL)
    {
        pRetObj = THIS_APP->QueryObject(m_psRefObject);
    }

    return pRetObj;
}

CPartsDef* CDrawingObject::GetRefDef() const
{
	CPartsDef* pRetDef = NULL;
	if (m_psRefObject == NULL)
	{
		return NULL;
	}


	if (m_pCtrl)
	{
		CProjectCtrl* pProj = m_pCtrl->GetProject();
		if (pProj)
		{
			pRetDef = pProj->QueryDef(m_psRefObject);
		}
	}

	if (pRetDef == NULL)
	{
		pRetDef = THIS_APP->QueryDef(m_psRefObject);
	}

	return pRetDef;
}

/**
 * @brief   領域取得
 * @param   なし
 * @retval  なし
 * @note	描画領域を取得する
 */
RECT2D CDrawingObject::GetBounds() const
{
    RECT2D rc;
    return rc;
}


/**
 * @brief   プロパティ削除
 * @param   [in] iId プロパティID
 * @retval  true 削除成功
 * @note	
 */
bool CDrawingObject::DeleteProperty(int iId)
{
    NAME_TREE<CStdPropertyItem>*      pTree;
    NAME_TREE_ITEM<CStdPropertyItem>* pTreeItem;
    pTree = m_psProperty->GetTreeInstance();
    pTreeItem = pTree->GetItem(iId);

    pTree->DeleteItem(pTreeItem);
    return true;
}

/**
 * @brief   プロパティアイテム取得
 * @param   プロパティ名
 * @retval  プロパティアイテム
 * @note	
 */
CStdPropertyItem* CDrawingObject::GetPropertyItem(StdString strName) const
{
    return m_psProperty->GetItem(strName, 1);
}

/**
 * @brief   ユーザプロパティ開放
 * @param   なし
 * @retval  なし
 * @note	
 */
void CDrawingObject::ReleaseUserProperty()
{
    NAME_TREE<CStdPropertyItem>*      pTree;
    pTree = m_psProperty->GetTreeInstance();

    STD_ASSERT(pTree);

    NAME_TREE_ITEM<CStdPropertyItem>* pTreeUserProp;
    pTreeUserProp = pTree->GetItem(GET_STR(STR_PRO_USER), 0);

    if (pTreeUserProp)
    {
        pTree->DeleteChild(pTreeUserProp);
    }
}

/**
 * @brief   ユーザプロパティ設定
 * @param   なし
 * @retval  なし
 * @note	CDrawingPartsで使用
 *          
 */
TREE_GRID_ITEM* CDrawingObject::_CreateStdUserPropertyTree(TREE_GRID_ITEM* pItem)
{

    return NULL;
}

/**
 * @brief   ユーザプロパティ設定
 * @param   [in] pData
 * @param   [in] pObj
 * @retval  なし
 * @note	Static
 *          
 */
bool CDrawingObject::ChgUserProp (CStdPropertyItem* pData, void*pObj)
{
    CDrawingObject* pDrawing = reinterpret_cast<CDrawingObject*>(pObj);
    try
    {
        StdString strPropName = pData->GetName();
        pDrawing->AddChgCnt();
        pDrawing->OnChgUserProp(pData, strPropName);
        pDrawing->m_pCtrl->Draw(pDrawing->GetId());
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
    }
    return true;
}


/**
 * @brief   プロパティセット値設定
 * @param   [in] pData
 * @param   [in] pObj
 * @retval  なし
 * @note	Static
 *          
 */
bool CDrawingObject::ChgPropertySetVal (CStdPropertyItem* pData, void*pObj)
{
    CDrawingObject* pDrawing = reinterpret_cast<CDrawingObject*>(pObj);
    try
    {
        StdString strPropName = pData->GetName();
        pDrawing->AddChgCnt();


        std::map<StdString, CAny>::iterator ite;
        ite = pDrawing->m_pPropertySetMap->find(strPropName);
        if (ite == pDrawing->m_pPropertySetMap->end())
        {
            return false;
        }

        CAny* pAny = &(ite->second);
        pAny->anyData = pData->anyData.anyData;

        //TODO: OnChgPropertySetを用意すべきか検討

        //pDrawing->OnChgUserProp(pData, strPropName);
        //pDrawing->m_pCtrl->Draw(pDrawing->GetId());
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
    }
    return true;
}


/**
 * @brief   プロパティセット初期値設定
 * @param   なし
 * @retval  なし
 * @note	貼付け時、画面更新時、実行時に設定する
 */
void CDrawingObject::InitPropertySetValue(bool bCreate)
{

    if (!m_bUsePropertySet)
    {
        return;
    }

    if (!bCreate)
    {
        return;
    }


    //  スクリプトからのプロパテイセット初期値変更に対応する
    //  （主にドロップダウンリストの変更に用いる）
    if (!m_psPropertySet)
    {
        CPropertySet* pSet;
        pSet = SYS_PROPERTY->GetPropertySetInstance(*m_pPropertySetName);

        if (!pSet)
        {
            STD_DBG(_T("%s is not find."), m_pPropertySetName->c_str()); 
            return;
        }
        m_psPropertySet = new CPropertySet;
        *m_psPropertySet = *pSet;
    }



    //種別、名前が変更した場合初期値にリセットする
    std::set<StdString> lstUse;
    std::map<StdString, CAny>::iterator iteMap;
    for(iteMap  = m_pPropertySetMap->begin();
        iteMap != m_pPropertySetMap->end();
        iteMap++)
    {
        //現在のプロパティセットの名称を保存
        lstUse.insert(iteMap->first);
    }
    std::set<StdString>::iterator iteSet;


    int iMax = m_psPropertySet->GetCnt();
    for (int iCnt = 0; iCnt < iMax; iCnt++)
    {
        CStdPropertyItemDef* pGridDef;
        CAny* pAny;

        pGridDef = m_psPropertySet->GetInstance(iCnt);

        pAny = GetPropertySetVal(pGridDef->strValName);

        if(!pAny)
        {
            std::pair<StdString, CAny> pairData;
            pairData.first  = pGridDef->strValName;
            pairData.second = pGridDef->anyInit;
            m_pPropertySetMap->insert(pairData); 
        }
        else
        {
            if (pAny->anyData.type() != pGridDef->anyInit.type())
            {
                pAny->anyData = pGridDef->anyInit;
            }
            else
            {

                if (pGridDef->type == PROP_INT_RANGE)
                {
                    int iMax = boost::any_cast<int>(pGridDef->anyMax);
                    int iMin = boost::any_cast<int>(pGridDef->anyMin);

                    if (pAny->GetInt() < iMin)
                    {
                        pAny->anyData = iMin;
                    }
                    else if (pAny->GetInt() > iMax)
                    {
                        pAny->anyData = iMax;
                    }
                }
            }
            
            iteSet = lstUse.find(pGridDef->strValName);
            if (iteSet != lstUse.end())
            {
                lstUse.erase(iteSet);
            }
        }
    }

    foreach(const StdString& strItem, lstUse)
    {
        iteMap = m_pPropertySetMap->find(strItem);

        if (iteMap != m_pPropertySetMap->end())
        {
            m_pPropertySetMap->erase(iteMap);
        }
    }

}

//プロパティセット値取得
CAny* CDrawingObject::GetPropertySetVal(StdString strName) const
{
    if (!m_pPropertySetMap)
    {
        return NULL;
    }

    std::map<StdString, CAny>::iterator ite;

    ite = m_pPropertySetMap->find(strName);

    if (ite == m_pPropertySetMap->end())
    {
        return NULL;
    }

    return &ite->second;
}

//!< 近接点取得
bool CDrawingObject::NearPointByList(POINT2D* pRet, 
                                    const std::vector<POINT2D>* pList, 
                                    const POINT2D& ptNear)
{
    STD_ASSERT(pList != NULL);
    STD_ASSERT(pRet  != NULL);

    bool bRet = false;
    POINT2D ptRet;
    double dDistance;
    double dMin = DBL_MAX;

    for(const POINT2D& ptEach: *pList)
    {
        dDistance = ptEach.Distance(ptNear);
        if (dMin > dDistance)
        {
            ptRet = ptEach;
            dMin = dDistance;
            bRet = true;
        }
    }
    *pRet = ptRet;
    return bRet;
}


//!< 近接点取得
bool CDrawingObject::NearPoint(POINT2D* ptRet, const POINT2D& ptNear, bool bOnLine) const
{
    switch(GetType())
    {
        case DT_ELEMENT:
        {
            const CDrawingElement* ptObj = dynamic_cast<const CDrawingElement*>(this);
            STD_ASSERT(ptObj != NULL);
            *ptRet = ptObj->GetPoint();
            break;
        }

        case DT_POINT:
        {
            //中点を算出
            const CDrawingPoint* ptObj = dynamic_cast<const CDrawingPoint*>(this);
            STD_ASSERT(ptObj != NULL);
            *ptRet = ptObj->GetPoint();
            break;
        }

        case DT_NODE:
        {
            const CDrawingNode* ptObj = dynamic_cast<const CDrawingNode*>(this);
            STD_ASSERT(ptObj != NULL);
            *ptRet = ptObj->GetPoint();
            break;
        }

        case DT_LINE:
        {
            const CDrawingLine* pLine = dynamic_cast<const CDrawingLine*>(this);
            STD_ASSERT(pLine != NULL);
            *ptRet = pLine->GetLineInstance()->NearPoint(ptNear, bOnLine);
            break;
        }

        case DT_CIRCLE:
        {
            const CDrawingCircle* pCircle = dynamic_cast<const CDrawingCircle*>(this);
            STD_ASSERT(pCircle != NULL);
            *ptRet = pCircle->GetCircleInstance()->NearPoint(ptNear, bOnLine);
            break;

        }

        case DT_SPLINE:
        {
            //近接点を算出
            const CDrawingSpline* pSpline = dynamic_cast<const CDrawingSpline*>(this);
            STD_ASSERT(pSpline != NULL);
            *ptRet = pSpline->GetSplineInstance()->NearPoint( ptNear, bOnLine);
            break;
        }

        case DT_ELLIPSE:
            {
            //交点を算出
            const CDrawingEllipse* pEllipse = dynamic_cast<const CDrawingEllipse*>(this);
            STD_ASSERT(pEllipse != NULL);
            *ptRet = pEllipse->GetEllipseInstance()->NearPoint( ptNear, bOnLine);
            break;
        }

        default:
            return false;
    }
    return true;
}

/**
 *  @brief  交点計算(点)
 *  @param  [out]   pList   交点のリスト
 *  @param  [in]    pObj    交差するオブジェクト
 *  @retval         なし
 *  @note  
 */
void CDrawingObject::CalcIntersectionPoint ( std::vector<POINT2D>* pList,   
                                const CDrawingObject* pObj,
                                POINT2D ptPos, 
                                bool bOnline,
                                double dMin) const
{
    STD_ASSERT(pList != NULL);

    POINT2D ptTmp;
    POINT2D* pPos;
    POINT2D* pMatPt = NULL;
    pList->clear();

    //レイヤーの確認
    if (!IsSameScale(pObj))
    {
        return;
    }

    const MAT2D* pMat =  GetParentMatrix();
    const MAT2D* pMat2 =  pObj->GetParentMatrix();


    if (pMat == NULL)
    {
        pPos = &ptPos;
    }
    else
    {
        pMatPt = new POINT2D(ptPos);
        pMatPt->Matrix(*pMat);
        pPos = pMatPt;
    }

    try
    {
        switch (pObj->GetType())
        {
            case DT_ELEMENT:
                {
                    //中点を算出
                    const CDrawingElement* ptObj = dynamic_cast<const CDrawingElement*>(pObj);

                    STD_ASSERT(ptObj != NULL);
                
                    if (pMat2 == NULL)
                    {
                        ptTmp =  (ptObj->GetPoint() + *pPos) / 2.0;       
                    }
                    else
                    {
                        POINT2D ptMat(ptObj->GetPoint());
                        ptMat.Matrix(*pMat2);
                        ptTmp =  (ptMat + *pPos) / 2.0;       
                    }
                    pList->push_back(ptTmp);
                }
                break;

            case DT_POINT:
                {
                    //中点を算出
                    const CDrawingPoint* ptObj = dynamic_cast<const CDrawingPoint*>(pObj);

                    STD_ASSERT(ptObj != NULL);
                    if (pMat2 == NULL)
                    {
                        ptTmp =  (*ptObj->GetPointInstance() + *pPos) / 2.0;       
                    }
                    else
                    {
                        POINT2D ptMat(*ptObj->GetPointInstance());
                        ptMat.Matrix(*pMat2);
                        ptTmp =  (ptMat + *pPos) / 2.0;       
                    }
                    pList->push_back(ptTmp);
                }
                break;

            case DT_NODE:
                {
                    //中点を算出
                    const CDrawingNode* ptObj = dynamic_cast<const CDrawingNode*>(pObj);

                    STD_ASSERT(ptObj != NULL);

                    if (pMat2 == NULL)
                    {
                        ptTmp =  (*ptObj->GetPointInstance() + *pPos) / 2.0;       
                    }
                    else
                    {
                        POINT2D ptMat(*ptObj->GetPointInstance());
                        ptMat.Matrix(*pMat2);
                        ptTmp =  (ptMat + *pPos) / 2.0;       
                    }
                    pList->push_back(ptTmp);
                }
                break;

            case DT_LINE:
                {
                    //近接点を算出
                    const CDrawingLine* pLine = dynamic_cast<const CDrawingLine*>(pObj);
                    STD_ASSERT(pLine != NULL);

                    if (pMat2 == NULL)
                    {
                        ptTmp = pLine->GetLineInstance()->NearPoint(*pPos, bOnline);
                    }
                    else
                    {
                        LINE2D line(*pLine->GetLineInstance());
                        line.Matrix(*pMat2);
                        ptTmp = line.NearPoint(*pPos, bOnline);

                    }
                    pList->push_back(ptTmp);
                }
                break;

            case DT_CIRCLE:
                {
                    //近接点を算出
                    const CDrawingCircle* pCircle = dynamic_cast<const CDrawingCircle*>(pObj);
                    STD_ASSERT(pCircle != NULL);

                    if (pMat2 == NULL)
                    {
                        ptTmp = pCircle->GetCircleInstance()->NearPoint(*pPos, bOnline);
                    }
                    else
                    {
                        ELLIPSE2D* pEllipse = NULL;
                        CIRCLE2D circle(*pCircle->GetCircleInstance());
                        circle.Matrix(*pMat2, &pEllipse);
                        if (pEllipse)
                        {
                            ptTmp = pEllipse->NearPoint(*pPos, bOnline);
                            delete pEllipse;
                        }
                        else
                        {
                            ptTmp = circle.NearPoint(*pPos, bOnline);
                        }
                        
                    }
                    pList->push_back(ptTmp);

                }
                break;

            case DT_SPLINE:
                {
                    //近接点を算出
                    const CDrawingSpline* pSpline = dynamic_cast<const CDrawingSpline*>(pObj);
                    STD_ASSERT(pSpline != NULL);
                    if (pMat2 == NULL)
                    {
                        ptTmp = pSpline->GetSplineInstance()->NearPoint( *pPos, bOnline);
                    }
                    else
                    {
                        SPLINE2D spline(*pSpline->GetSplineInstance());
                        spline.Matrix(*pMat2);
                        ptTmp = spline.NearPoint(*pPos, bOnline);
                    }

                    pList->push_back(ptTmp);
                }
                break;

            case DT_ELLIPSE:
                {
                    //交点を算出
                    const CDrawingEllipse* pEllipse = dynamic_cast<const CDrawingEllipse*>(pObj);
                    STD_ASSERT(pEllipse != NULL);
                    if (pMat2 == NULL)
                    {
                        ptTmp = pEllipse->GetEllipseInstance()->NearPoint( *pPos, bOnline);
                    }
                    else
                    {

                        ELLIPSE2D ellipse(*pEllipse->GetEllipseInstance());
                        ellipse.Matrix(*pMat2);
                        ptTmp = ellipse.NearPoint(*pPos, bOnline);
                    }
                    pList->push_back(ptTmp);
            }
            break;

            case DT_COMPOSITION_LINE:
            {
                //交点を算出
                const CDrawingCompositionLine* pCom = dynamic_cast<const CDrawingCompositionLine*>(pObj);
                STD_ASSERT(pCom != NULL);
                if (pMat2 == NULL)
                {
                    pCom->NearPoint(&ptTmp, *pPos, bOnline);
                }
                else
                {
                    CDrawingCompositionLine com(*pCom);
                    com.Matrix(*pMat2);
                    com.NearPoint(&ptTmp, *pPos, bOnline);
                }
                pList->push_back(ptTmp);
            }

        default:
                break;
         }
    }
    catch(...)
    {
        ;//何もしない
    }

    if (pMatPt)
    {
        delete pMatPt;
    }
}


//!< データ->ビュー変換
POINT CDrawingObject::GetScreenCoordinate(POINT2D pt2D, const CDrawingView& view) const
{
    POINT pt;
    view.ConvWorld2Scr(&pt, pt2D, m_iLayerId);
    return pt;
}
         
//!< ビュー->データ変換
POINT2D CDrawingObject::GetWorldCoordinate(POINT pt, const CDrawingView& view) const
{
    POINT2D pt2D;
    view.ConvScr2World(&pt2D, pt, m_iLayerId);
    return pt2D;
}

bool CDrawingObject::CreateNodeData(int iIndex) 
{
    if(iIndex != -1)
    {
        return false;
    }

    if(m_psNodeData)
    {
        m_psNodeData = std::make_shared<CNodeData>();
        m_psNodeData->eConnectionType = E_PASSIVE;
        m_psNodeData->eConnectionObject = CNodeData::OBJECT;
        m_psNodeData->eConnectionDirection= E_INOUT;
        m_psNodeData->dwSnapType = VIEW_COMMON::SNP_NONE;
        m_psNodeData->iMaxSocket = -1;
    }
    return true;
}

CNodeData* CDrawingObject::GetNodeData(int iIndex) 
{
    if(iIndex != -1)
    {
        return NULL;
    }

    if(!m_psNodeData)
    {
        CreateNodeData(iIndex);
    }
    return m_psNodeData.get();
}

CNodeData* CDrawingObject::GetNodeDataConst(int iIndex) const 
{
    if(iIndex != -1)
    {
        return NULL;
    }

    if(!m_psNodeData)
    {
       return NULL;
    }
    return m_psNodeData.get();
}

//!< ノード接続可能判定
bool CDrawingObject::IsAbleConnectNode(CNodeData* pData,int iNodeIndex) 
{ 
    auto pNodeData = GetNodeData( iNodeIndex);
    if (pNodeData)
    {
        return pNodeData->IsAbleToConnect(pData);
    }
    return false;
}

//!< ノード接続

bool CDrawingObject::ConnectNode(CDrawingObject* pDestObj, 
                                            int iDestNodeIndex, 
                                            int iSrcNodeIndex)
{
    //Accept側
    auto pNode = GetNodeData(iSrcNodeIndex);
    if (!pNode)
    {
        return false;
    }

    if(!pDestObj)
    {
       return false;
    }

    auto pDestNode = pDestObj->GetNodeData(iDestNodeIndex);
                    
    if(!pDestNode)
    {
        return false;
    }

    if(!pNode->IsAbleToConnect(pDestNode))
    {
        return false;
    }
                                 
    if(pDestNode->eConnectionType != E_PASSIVE)
    {
        return false;
    }


    pDestObj->AddToSocket(iDestNodeIndex, this, iSrcNodeIndex);
    AddToPlug(iSrcNodeIndex, pDestObj, iDestNodeIndex);

    return true;
}


bool CDrawingObject::AddToPlug(int iNodeIndex,
                               CDrawingObject* pDestObj, 
                               int iDestNodeIndex,
                               int iPlugNo )
{
    if(!pDestObj)
    {
        return false;
    }

    CBindingPartner p;
    p.iId = pDestObj->GetId();
    p.iIndex = iDestNodeIndex;

    auto pNode = GetNodeData(iNodeIndex);
    if (!pNode)
    {
        return false;
    }

    if (iPlugNo == -1)
    {
        if(pNode->iMaxPlug!= -1)
        {
            if(pNode->lstPlug.size() >= pNode->iMaxPlug)
            {
                return false;
            }
        }

        auto ite = std::find (pNode->lstPlug.begin(),
                              pNode->lstPlug.end(), p);

        if (ite == pNode->lstPlug.end())
        {
            pNode->lstPlug.push_back(p);
        }
    }
    else
    {
        if(pNode->iMaxPlug != -1)
        {
            if(pNode->iMaxPlug <= iPlugNo)
            {
                return false;
            }

            if(pNode->lstPlug.size() >= iPlugNo)
            {
                pNode->lstPlug.resize(iPlugNo+1);
            }

            pNode->lstPlug[iPlugNo] = p;
        }
    }
    return true;
}

bool CDrawingObject::AddToSocket(int iNodeIndex,
                                 CDrawingObject* pDestObj, 
                                int iDestNodeIndex )
{
    if(!pDestObj)
    {
        return false;
    }

    CBindingPartner p;
    p.iId = pDestObj->GetId();
    p.iIndex = iDestNodeIndex;

    auto pNode = GetNodeData(iNodeIndex);
    if (!pNode)
    {
        return false;
    }

    if(pNode->iMaxSocket != -1)
    {
        if(pNode->lstSocket.size() >= pNode->iMaxSocket)
        {
            return false;
        }
    }

    auto ite = std::find (pNode->lstSocket.begin(),
                          pNode->lstSocket.end(), p);

    if (ite == pNode->lstSocket.end())
    {
        pNode->lstSocket.push_back(p);
    }
    return true;
}

//!< ノード接続解除
bool CDrawingObject::ReleaseNode(int iIndex, 
                                 CDrawingObject* pDeleteObj,
                                 int iDeleteNodeIdOnThisObject)
{
    auto pNode = GetNodeData(iIndex);
    if(!pNode)
    {
        return false;
    }
    
    if(pNode->eConnectionType == E_PASSIVE)
    {
        pNode->DeleteSocket(pDeleteObj, iDeleteNodeIdOnThisObject);
    }
    else
    {
        pNode->DeletePlug(pDeleteObj, iDeleteNodeIdOnThisObject);
    }
    
    if(pDeleteObj)
    {
        pDeleteObj->ReleaseNode(iDeleteNodeIdOnThisObject, this, iIndex);     
    }         

    return true;
}

bool CDrawingObject::ReleaseNode(CDrawingObject *pObj)
{
    int iTotal = GetNodeDataNum();

    CNodeData* pNode;
    std::set<int> setObj;

    for(int iNo = -1; iNo < iTotal; ++iNo)
    {
        pNode = GetNodeData(iNo);

        if (!pNode)
        {
            continue;
        }

        if(pNode->eConnectionType == E_PASSIVE)
        {
            pNode->DeleteSocket(pObj);
        }
        else
        {
            pNode->DeletePlug(pObj);
        }
    }
    return true;
}

bool CDrawingObject::ClearNode()
{
    int iTotal = GetNodeDataNum();

    CNodeData* pNode;
    std::set<int> setObj;

    for(int iNo = -1; iNo < iTotal; ++iNo)
    {
        pNode = GetNodeData(iNo);

        if (!pNode)
        {
            continue;
        }
            
        for(auto bind: pNode->lstSocket)
        {                                   
            if(bind.iId != -1)
            {
                auto pObj = m_pCtrl->GetObjectById(bind.iId);                
                if(pObj)
                {
                    pObj->ReleaseNode(bind.iIndex, this, pNode->iIndex);
                }
            }
        }
        pNode->Clear();
    }
    return true;
}

bool CDrawingObject::GetNodeDataPos(POINT2D* pt, int iConnectionId) const
{
    CNodeData* pCd  = GetNodeDataConst(iConnectionId);
    if (pCd)
    {
        const MAT2D* pOffset = GetOffsetMatrix();
        MAT2D mat(GetMatrix());

        if (pOffset)
        {
            mat = mat * (*pOffset);
        }
        *pt = pCd->ptPos;
        pt->Matrix(mat);
        return true;
    }
    return false;
}
/*
void CDrawingObject::ClearConnection()
{
    int iTotal = GetNodeDataNum();

    CNodeData* pConnection;
    std::vector<int> lstDel;
    for(int iNo = 0; iNo < iTotal; ++iNo)
    {
        pConnection = GetNodeData(iNo);

        if (!pConnection)
        {
            continue;
        }

        for(auto& bind: pConnection->lstSocket)
        {
            auto pObj = m_pCtrl->GetObjectById(bind.iId);
            if(pObj)
            {
                pObj->OnReleaseNodeData(this, iNo);
            }
        }
        pConnection->lstSocket.clear();
    }

    if(!m_pLstConnectNode)
    {
        return;
    }

    for(int& iId: *m_pLstConnectNode)
    {
        auto pObj = m_pCtrl->GetObjectById(iId);
        if(pObj)
        {
            pObj->OnReleaseNodeData(this, -1);
        }
    }
    m_pLstConnectNode->clear();
}
*/

int CDrawingObject::GetConnectionObjectList(std::vector<std::weak_ptr<CDrawingObject>>* pList)
{
    int iTotal = GetNodeDataNum();

    CNodeData* pConnection;
    std::set<int> setObj;

    for(int iNo = -1; iNo < iTotal; ++iNo)
    {
        pConnection = GetNodeData(iNo);

        if (!pConnection)
        {
            continue;
        }

        for(auto& bind: pConnection->lstSocket)
        {
            setObj.insert(bind.iId);
        }
    }

    for(auto iId: setObj)
    {
        auto pObj = m_pCtrl->GetObjectById(iId);
        if(pObj)
        {
            pList->push_back(pObj);
        }
    }
    return SizeToInt(setObj.size());
}


int CDrawingObject::GetConnectionList(std::vector<CBindingPartner>* pList)
{
    int iTotal = GetNodeDataNum();

    CNodeData* pConnection;
    std::set<CBindingPartner> setObj;

    for(int iNo = -1; iNo < iTotal; ++iNo)
    {
        pConnection = GetNodeData(iNo);
        if (!pConnection)
        {
            continue;
        }

        for(auto& bind: pConnection->lstSocket)
        {
            setObj.insert(bind);
        }
    }

    for(auto bind: setObj)
    {
        pList->push_back(bind);
    }
    return SizeToInt(setObj.size());
}


/**
 *  @brief  ノード位置更新.
 *  @param  [in]     pDef    
 *  @param  [inout]  pList  
 *  @retval true 変更あり
 *  @note   オブジェクトが移動することによって
 *			変更が発生する可能性があるオブジェクトを列挙する（UNDOで元に戻す必要があるオブジェクト）
 *          これは、接続が解除される場合を含む
 */
bool CDrawingObject::GetChangeObjectsList(CPartsDef* pDef, std::set<NODE_CONNECTION_CHANGE>* pList)
{
    int iTotal = GetNodeDataNum();
    if (!pDef)
    {
        pDef = m_pCtrl;
    }

    if (!pDef)
    {
        return false;
    }

    if (!pList)
    {
        return false;
    }

    NODE_CONNECTION_CHANGE mcc;
    mcc.iChangeObjectId = GetId();

    auto ite = pList->find(mcc);
    if (ite != pList->end())
    {
        return false;
    }

    // グループ処理の場合すべての処理(移動など)が終わるまで
    // 位置が確定しないため最初に移動する可能性があるオブジェクトを列挙する.
    // 後でこのリストを元に一時データを作成し、それをドラッグ時の表示に用いる。
    // （Obj1->Socket Plug->Obj->Soket Plug->Obj->Soket のようにチェインする場合
    //   も考慮しなくてはならない) 
    // 後でUNDOするためにもコピーの作成は必要となる
    CNodeData* pConnection;
    std::vector<CBindingPartner> lstDel;
    bool bChg = false;
    for (int iNo = -1; iNo < iTotal; ++iNo)
    {
        pConnection = GetNodeData(iNo);

        if (!pConnection)
        {
            continue;
        }

        lstDel.clear();
        POINT2D pt;

        bool bChange = false;
        if (pConnection->eConnectionObject == CNodeData::FIELD)
        {
            continue;
        }

        for (auto socket : pConnection->lstSocket)
        {
            auto pObjDest = pDef->GetObjectById(socket.iId);
            if (pObjDest)
            {
                if (pObjDest->IsSelect() && 
                    pObjDest->GetStatus() != CDrawingObject::ES_RELATION)
                {
                    continue;
                }

                mcc.iChangeObjectId = pObjDest->GetId();
                mcc.bMove = true;    //このオブジェクトの移動に伴い変形する可能性あり
                                     //こちらが優先するので重複はチェックしない
                pList->insert(mcc);
                pObjDest->GetChangeObjectsList(pDef, pList);
                bChg = true;
            }
        }

        for (auto plug : pConnection->lstPlug)
        {
            auto pObjDest = pDef->GetObjectById(plug.iId);
            if (pObjDest)
            {
                if (pObjDest->IsSelect() &&
                    pObjDest->GetStatus() != CDrawingObject::ES_RELATION)
                {
                    continue;
                }

                mcc.iChangeObjectId = pObjDest->GetId();
                mcc.bMove = false;    //このオブジェクトの移動に伴い変形する可能性なし

                auto iteDest = pList->find(mcc);
                if (iteDest != pList->end())
                {
                    continue;
                }

                pList->insert(mcc);
                bChg = true;
            }
        }
    }

    return bChg;
}


/**
 *  @brief  ノード位置更新.
 *  @param  [in]  pTmpObject  移動中の一時オブジェクト(通常時はNULL)
 *  @param  [in]  pDef        このオブジェクトと表示する定義
 *  @retval       true 更新成功
 *  @note
 */
bool CDrawingObject::UpdateNodeSocketData(CDrawingObject *pTmpObject, CPartsDef* pDef)
{
    int iTotal = GetNodeDataNum();
    
    if (!pDef)
    {
        pDef = m_pCtrl;
    }

    if (!pDef)
    {
        return false;
    }
    
    // グループ処理の場合すべての処理(移動など)が終わるまで
    // 位置が確定しないため途中での処理は行わず
    // 最後にまとめて処理を行う
    CNodeData* pConnection;
    std::vector<CBindingPartner> lstDel;
    for(int iNo = -1; iNo < iTotal; ++iNo)
    {
        pConnection = GetNodeData(iNo);

        if (!pConnection)
        {
            continue;
        }

        lstDel.clear();
        POINT2D pt;

        bool bChange = false;
        if (pConnection->eConnectionObject == CNodeData::FIELD) 
        {
            continue;
        }

        auto ite = pConnection->lstSocket.begin();
        while( ite != pConnection->lstSocket.end())
        {
            //ドラッグ用にコピーしているオブジェクトを最初に探す
            auto pObj = pDef->GetDraggingObject(ite->iId);
            if (!pObj)
            {
                pObj = pDef->GetObjectById(ite->iId);
            }

            if(pObj)
            {
				if (pObj->GetTmpCopy())
				{
DB_PRINT(_T("CDrawingObject::UpdateNodeSocketData TmpCopy pObj：%d %I64x\n"), pObj->GetId(), pObj.get());
					ite++;
					continue;
				}

                if (pObj->IsSelect())
                {
                    ite++;
                    continue;
                }

                if (pTmpObject)
                {
                    //---------------------
                    //一時オブジェクトを使用している場合
                    // (現状 CDrawingConnectionLine::MoveNodeMarker)
                    //---------------------
                    pTmpObject->GetNodeDataPos(&pt, iNo);
                }
                else
                {
                    STD_ASSERT(GetNodeDataPos(&pt, iNo));
                }

#if 1
    DB_PRINT(_T("UpdateNodeSocketData ObjId:%d Index:%d 位置(%s) %I64x\n"),
        ite->iId, iNo,
        pt.Str().c_str(), pObj.get());
#endif


                if (!pDef->IsLockUpdateNodeData())
                {
                    if (!pObj->OnUpdateNodeSocketData(&pt, 
                                                      this, 
                                                      iNo, 
                                                      ite->iIndex,
                                                      NULL,
                                                      NULL))
                    {
                        //再接続しない場合は削除
                        ite = pConnection->lstSocket.erase(ite);
                    }
                    else
                    {
                        //変更有
                        ite++;
                    }
                }
                else
                {
                    // 接続している部分の変更に対してもUNDOを行う必要があるため
                    // SetLockUpdateNodeData(true)をセットして 
                    // 後でまとめてUNDOのチェックとセットを行う
                    if (!pTmpObject)
                    {
                        NODE_CHANGE_DATA data;
                        data.bPlug = false;                   //ソケットの変更
                        data.iChangeObjectId = ite->iId;      //プラグ側のオブジェクトID
                        data.iChangeNodeIndex = ite->iIndex;  //プラグ側のノードINDEX
                        data.iSrcId = GetId();                //ソケット側のオブジェクトID
                        data.iSrcIndex = iNo;                 //ソケット側のノードINDEX
                        data.pt = pt;
#ifdef _DEBUG
    DB_PRINT(_T("  UpdateNodeSocketData bPlug(%s) Chg(%d:%d) Src(%d:%d) pt(%s)\n"),
        data.bPlug ? _T("T") : _T("F"),
        data.iChangeObjectId, data.iChangeNodeIndex,
        data.iSrcId, data.iSrcIndex,
        data.pt.Str().c_str());
#endif                         
                        
                        pDef->AddListChangeByConnection(data);
                    }
                    ite++;
                }
             }
            else
            {
                if (!pDef->IsLockUpdateNodeData())
                {
                    //接続対象が見つからない場合も削除
                    ite = pConnection->lstSocket.erase(ite);
                }
                else
                {
                    ite++;
                }
            }
        }
    }
    return true;
}


/**
*  @brief  ノード位置更新.
*  @param  [in]  pTmpObject  移動中の一時オブジェクト(通常時はNULL)
*  @param  [in]  pDef        このオブジェクトと表示する定義
*  @retval       true 更新成功
*  @note
*/
bool CDrawingObject::UpdateNodePlugData(CPartsDef* pDef)
{
    int iTotal = GetNodeDataNum();

    if (!pDef)
    {
        pDef = m_pCtrl;
    }

    if (!pDef)
    {
        return false;
    }

    // グループ処理の場合すべての処理(移動など)が終わるまで
    // 位置が確定しないため途中での処理は行わず
    // 最後にまとめて処理を行う

    //--------------

    CNodeData* pConnection;
    std::vector<CBindingPartner> lstDel;
    for (int iNo = -1; iNo < iTotal; ++iNo)
    {
        pConnection = GetNodeData(iNo);

        if (!pConnection)
        {
            continue;
        }

        lstDel.clear();
        POINT2D pt;
        POINT2D ptDest;

        if (pConnection->eConnectionObject != CNodeData::NODE) 
        {
            continue;
        }

        auto ite = pConnection->lstPlug.begin();
        while (ite != pConnection->lstPlug.end())
        {
            auto pObj = pDef->GetObjectById(ite->iId);
            if (!pObj)
            {
                //接続対象が見つからない場合も削除
                ite = pConnection->lstSocket.erase(ite);
                continue;

            }

            if (pObj->IsSelect())
            {
                continue;
            }

            STD_ASSERT(GetNodeDataPos(&pt, iNo));

            if (pDef->IsLockUpdateNodeData())
            {
                //UNDOで必要になるので相手側のオブジェクトIDを記録

                NODE_CHANGE_DATA data;
                data.bPlug = true;
                data.iChangeObjectId  = ite->iId;
                data.iChangeNodeIndex = ite->iIndex;
                data.iSrcId = GetId();
                data.iSrcIndex = iNo;
                data.pt = pt;


                pDef->AddListChangeByConnection(data);
                ite++;
            }
            else
            {
                pObj->GetNodeDataPos(&ptDest, ite->iIndex);

                if (ptDest.Distance(pt) > NEAR_ZERO)
                {
                    ite = pConnection->lstSocket.erase(ite);
                    auto pConnectionDest = pObj->GetNodeData(iNo);

                    //相手側のデータを削除
                    pConnectionDest->DeletePlug(this, ite->iIndex);
                }
                else
                {
                    ite++;
                }
            }
        }
    }
    return true;
}

/**
 *  @brief  Node更新通知
 *  @param  [out]   pPt   新たな位置
 *  @param  [in]    pSocketObj  Socket側のオブジェクト
 *  @param  [in]    iSocketIndex  Socket側の接続Index
 *  @param  [in]    iIndex        接続Index
 *  @param  [inout] pList         循環変更回避用リスト
 *  @param  [inout] pUndo         
 *  @retval false  更新なし
 *  @note   Socket側が移動したときに呼び出す
 */
bool CDrawingObject::OnUpdateNodeSocketData(const POINT2D* pPt, 
            const CDrawingObject* pSocketObj,
            int iSocketIndex,
            int iIndex,
            std::set<int>* pList,
            CUndoAction* pUndo
            ) 
{
    if (pList)
    {
        if (pList->find(GetId()) != pList->end())
        {
            return false;
        }
        pList->insert(GetId());
    }

    auto pNode = GetNodeData(iIndex);

    if (pNode->eConnectionObject != CNodeData::NODE)
    {
        return false;
    }

    if (pNode->eConnectionType != E_ACTIVE)
    {
        return false;
    }

    //実体は各オブジェクトにて実装する

    return true; 
}


RECT2D::E_EDGE InquireConnectDirection(std::map<RECT2D::E_EDGE, LINE2D>* pDir,
                                            RECT2D* pRc,
                                            int iConnectionId )
{
    return RECT2D::E_NONE;
}


void CDrawingObject::_FreeUserData()
{
    asIScriptEngine *engine = THIS_APP->GetScriptEngine()->GetEngine();
	// If it is a handle or a ref counted object, call release
	if( m_iUserDataType & asTYPEID_MASK_OBJECT )
	{
		// Let the engine release the object
		asIObjectType *ot = engine->GetObjectTypeById(m_iUserDataType);
		engine->ReleaseScriptObject(m_pUserData, ot);

		// Release the object type info
		if( ot )
        {
			ot->Release();
        }

		m_pUserData = 0;
		m_iUserDataType = 0;
	}
}
void CDrawingObject::SetUserData(__int64 &iData)
{
    SetUserData(&iData, asTYPEID_INT64);
}

void CDrawingObject::SetUserData(double &dData)
{
    SetUserData(&dData, asTYPEID_DOUBLE);
}

void CDrawingObject::SetUserData(void* pUserData, int refTypeId)
{
    asIScriptEngine *engine = THIS_APP->GetScriptEngine()->GetEngine();
    void* pData = *(void**)pUserData;

	// Hold on to the object type reference so it isn't destroyed too early
	if( pData && (refTypeId & asTYPEID_MASK_OBJECT) )
	{
		asIObjectType *ot = engine->GetObjectTypeById(refTypeId);
		if( ot )
        {
			ot->AddRef();
        }
	}

	_FreeUserData();

	m_iUserDataType = refTypeId;
	if( m_iUserDataType & asTYPEID_OBJHANDLE )
	{
		// We're receiving a reference to the handle, so we need to dereference it
		m_pUserData = pData;
		engine->AddRefScriptObject(m_pUserData, engine->GetObjectTypeById(m_iUserDataType));
	}
	else if( m_iUserDataType & asTYPEID_MASK_OBJECT )
	{
		// Create a copy of the object
		m_pUserData = engine->CreateScriptObjectCopy(pUserData, engine->GetObjectTypeById(m_iUserDataType));
	}
	else
	{
		// Primitives can be copied directly
		m_pUserData = 0;

		// Copy the primitive value
		// We receive a pointer to the value.
		int size = engine->GetSizeOfPrimitiveType(m_iUserDataType);
		memcpy(m_pUserData, pUserData, size);
	}
}

bool CDrawingObject::GetUserData(__int64 &iData)
{
    return GetUserData(&iData, asTYPEID_INT64);
}

bool CDrawingObject::GetUserData(double &dData)
{
    return GetUserData(&dData, asTYPEID_DOUBLE);
}



bool CDrawingObject::GetUserData(void* pUserData, int refTypeId)
{
    asIScriptEngine *engine = THIS_APP->GetScriptEngine()->GetEngine();
    void* pData = *(void**)pUserData;

    if( refTypeId & asTYPEID_OBJHANDLE )
	{
		// Is the handle type compatible with the stored value?

		// A handle can be retrieved if the stored type is a handle of same or compatible type
		// or if the stored type is an object that implements the interface that the handle refer to.
		if( (m_iUserDataType & asTYPEID_MASK_OBJECT) && 
			engine->IsHandleCompatibleWithObject(m_pUserData, m_iUserDataType, refTypeId) )
		{
			engine->AddRefScriptObject(m_pUserData, engine->GetObjectTypeById(m_iUserDataType));
			*(void**)pUserData = m_pUserData;

			return true;
		}
	}
	else if( refTypeId & asTYPEID_MASK_OBJECT )
	{
		// Is the object type compatible with the stored value?

		// Copy the object into the given reference
		if( m_iUserDataType == refTypeId )
		{
			engine->AssignScriptObject(pUserData, m_pUserData, engine->GetObjectTypeById(m_iUserDataType));

			return true;
		}
	}
	else
	{

        UniVal64 uni;

        uni.pVal = m_pUserData;

		// Is the primitive type compatible with the stored value?

		if( m_iUserDataType == refTypeId )
		{
			int size = engine->GetSizeOfPrimitiveType(refTypeId);
			memcpy(pUserData, &uni.iVal, size);
			return true;
		}

		// We know all numbers are stored as either int64 or double, since we register overloaded functions for those
		if( m_iUserDataType == asTYPEID_INT64 && refTypeId == asTYPEID_DOUBLE )
		{
			*(double*)pUserData = double(uni.iVal);
			return true;
		}
		else if( m_iUserDataType == asTYPEID_DOUBLE && refTypeId == asTYPEID_INT64 )
		{
			*(asINT64*)pUserData = asINT64(uni.dVal);
			return true;
		}
	}

	return false;
}



StdString CDrawingObject::GetValText(double dVal, bool bAngle, int iPrecision)
{
    using namespace VIEW_COMMON;
    StdStringStream strmDim;

    if (!bAngle)
    {
        switch (DRAW_CONFIG->eDimLengthType)
        {

        case DLT_FRACTION:           //分数表記
        case DLT_ENGINEERING:        //工業図表記   インチのみ
        case DLT_ARCHTECTUAL:        //建築図面表記 インチのみ


            break;   
        case DLT_DECIMAL:            //10進表記
        default:
            strmDim << std::fixed 
                    << std::setprecision(iPrecision) 
                    << dVal;
            break;
        }
    }
    else
    {
        switch (DRAW_CONFIG->eDimAngleType)
        {
            case DIA_DEGMINSEC:          //度。分。秒
            case DIA_RADIANS:            //ラジアン
            break;
            case DIA_DECIMAL:            //10進表記
            default:
                strmDim << std::fixed 
                        << std::setprecision(iPrecision) 
                        << dVal;
                break;
        }
    }
    return strmDim.str();
}

CDrawingObject::SEL_MENU CDrawingObject::_ShowMoveCopyMenu(CNodeMarker* pMarker)
{
    CDrawingView* pView = pMarker->GetView();
    StdString strId = pMarker->GetSelectId();
    POINT2D pt2d = pMarker->GetMarkerPos(strId);
    POINT pt;
    pView->ConvWorld2Scr(&pt, pt2d, m_iLayerId);



    CMenu menu;
    menu.CreatePopupMenu();

    UINT nPopup = MF_STRING | MF_ENABLED | MF_POPUP ;

    pView->GetWindow()->ClientToScreen(&pt);
    
    menu.AppendMenu(nPopup, SEL_MNU_MOVE, GET_STR(STR_MNU_ITEM_MOVE));
    menu.AppendMenu(nPopup, SEL_MNU_COPY, GET_STR(STR_MNU_ITEM_COPY));
    menu.AppendMenu(MF_SEPARATOR);
    menu.AppendMenu(nPopup, SEL_MNU_CANCEL, GET_STR(STR_MNU_CANCEL));

    int iSelObjId;
    iSelObjId = menu.TrackPopupMenu(
        TPM_LEFTALIGN  |    //クリック時のX座標をメニューの左辺にする
        TPM_NONOTIFY   |    
        TPM_RETURNCMD  |    // 関数の戻り値としてメニューＩＤを返す
        TPM_RIGHTBUTTON,    //右クリックでメニュー選択可能とする
        pt.x,   //メニューの表示位置
        pt.y,   
        pView->GetWindow()      //このメニューを所有するウィンドウ
    );


    SEL_MENU eMenu = static_cast<SEL_MENU>(iSelObjId);
    return eMenu;
}

void CDrawingObject::Hide()
{
    if(m_pCtrl)
    {
        m_pCtrl->HideObject(m_nId);
    }
}

void CDrawingObject::Redraw()
{
    if(m_pCtrl)
    {
        m_pCtrl->Draw(m_nId);
    }
}

template<class Archive>
void CDrawingObject::load(Archive& ar, const unsigned int version)
{
	SERIALIZATION_INIT
	int iVisible;
	int iUsePropertySet;
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(CBaseObj);
	SERIALIZATION_LOAD("Type", m_eType);
	SERIALIZATION_LOAD("Name"      , m_strName);
	SERIALIZATION_LOAD("Tag"       , m_strTag);
	SERIALIZATION_LOAD("Color", m_crObj);
	SERIALIZATION_LOAD("LayerID", m_iLayerId);
	SERIALIZATION_LOAD("Id", m_nId);
	SERIALIZATION_LOAD("Visible", iVisible);
	SERIALIZATION_LOAD("PropFeatures", m_iPropFeatures);
	SERIALIZATION_LOAD("UsePropertySet", iUsePropertySet);

	m_bVisible = (iVisible != 0);
	m_bUsePropertySet = (iUsePropertySet != 0);

	if (m_bUsePropertySet)
	{
		if (!m_pPropertySetName)
		{
			m_pPropertySetName = new StdString;
		}

		if (!m_pPropertySetMap)
		{
			m_pPropertySetMap = new std::map<StdString, CAny>;
		}
	}

	if (m_bUsePropertySet)
	{
		SERIALIZATION_LOAD("PropertySetName", *m_pPropertySetName);
		SERIALIZATION_LOAD("PropertySet", *m_pPropertySetMap);
	}

	MOCK_EXCEPTION_FILE(e_file_read);
}

template<class Archive>
void CDrawingObject::save(Archive& ar, const unsigned int version) const
{
	//セーブ開始を通知
	SERIALIZATION_INIT;
	int iVisible;
	int iUsePropertySet;
	//!< ユーザー定義値(データ保存時に使用)
	iVisible = (m_bVisible ? 1 : 0);
	iUsePropertySet = (m_bUsePropertySet ? 1 : 0);
    ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(CBaseObj);
	SERIALIZATION_SAVE("Type", m_eType);
	SERIALIZATION_SAVE("Name"      , m_strName);
	SERIALIZATION_SAVE("Tag"       , m_strTag);
	SERIALIZATION_SAVE("Color", m_crObj);
	SERIALIZATION_SAVE("LayerID", m_iLayerId);
	SERIALIZATION_SAVE("Id", m_nId);
	SERIALIZATION_SAVE("Visible", iVisible);
	SERIALIZATION_SAVE("PropFeatures", m_iPropFeatures);
	SERIALIZATION_SAVE("UsePropertySet", iUsePropertySet);

	if (m_bUsePropertySet)
	{
		SERIALIZATION_SAVE("PropertySetName", *m_pPropertySetName);
		SERIALIZATION_SAVE("PropertySet", *m_pPropertySetMap);
	}
	MOCK_EXCEPTION_FILE(e_file_write);
}


void CDrawingObject::DebugPrintOnSelect()
{
    DB_PRINT(_T("--------------\n"));
    DB_PRINT(_T("  名称:%s -------\n"), GetName().c_str());
    int iTotalNodeNum = GetNodeDataNum();
    if (iTotalNodeNum >  0)
    {
        for (int iIndex = 0 ; iIndex < iTotalNodeNum; iIndex++)
        {
            auto pNodeData = GetNodeData(iIndex);

            if (!pNodeData)
            {
                continue;
            }

            if (pNodeData->lstPlug.empty() &&
                pNodeData->lstSocket.empty())
            {
                continue;

            }

            DB_PRINT(_T("     Node Index%d\n"), iIndex);

            if (!pNodeData->lstPlug.empty())
            {
                int iPlugIndex = 0;
                StdString strPairObjectName;
                for(auto bind: pNodeData->lstPlug)
                {
                    if (m_pCtrl)
                    {
                        auto pObj = m_pCtrl->GetObjectById(bind.iId);
                        if(pObj)
                        {
                            strPairObjectName = pObj->GetName();
                        }


                    }

                    DB_PRINT(_T("       Plug Index%d Id:(%d %d)%s\n"),
                        iPlugIndex, bind.iId, bind.iIndex,
                        strPairObjectName.c_str()
                                        );
                    iPlugIndex++;
                }
            }

            if (!pNodeData->lstSocket.empty())
            {
                int iSocketIndex = 0;
                StdString strPairObjectName;
                for (auto bind : pNodeData->lstSocket)
                {
                    if (m_pCtrl)
                    {
                        auto pObj = m_pCtrl->GetObjectById(bind.iId);
                        if (pObj)
                        {
                            strPairObjectName = pObj->GetName();
                        }


                    }

                    DB_PRINT(_T("       Socket Index%d Id:(%d %d)%s\n"),
                        iSocketIndex, bind.iId, bind.iIndex,
                        strPairObjectName.c_str()
                    );
                    iSocketIndex++;
                }
            }
        }
    }

    DB_PRINT(_T("---\n"));
}


void CDrawingObject::PrintObj(StdString strSpc) const
{
    DRAWING_TYPE eType = GetType();
    StdString strType = CUtil::StrFormat(_T("Id:%d Type:%s"),
        GetId(),
        VIEW_COMMON::GetObjctTypeName(eType).c_str());

    DB_PRINT(_T("%s%s\n"), strSpc.c_str(), strType.c_str());

    auto pParentParts = GetParentParts();;
    if (pParentParts)
    {
        DRAWING_TYPE eParentType = pParentParts->GetType();
        strType = CUtil::StrFormat(_T("ParentParts Id:%d Type:%s"),
            pParentParts->GetId(),
            VIEW_COMMON::GetObjctTypeName(eParentType).c_str());
        DB_PRINT(_T("%s%s\n"), strSpc.c_str(), strType.c_str());
    }

    auto pDrawingParent = GetDrawingParent();
    if (pDrawingParent)
    {
        DRAWING_TYPE eParentType = pDrawingParent->GetType();
        strType = CUtil::StrFormat(_T("DrawingParent Id:%d Type:%s"),
            pDrawingParent->GetId(),
            VIEW_COMMON::GetObjctTypeName(eParentType).c_str());
        DB_PRINT(_T("%s%s\n"), strSpc.c_str(), strType.c_str());

        const CDrawingObject*  pObjTop = GetTopDraw(pDrawingParent.get());

        if (pObjTop)
        {
            StdString strTop;
            DRAWING_TYPE eParentType = pObjTop->GetType();
            strType = CUtil::StrFormat(_T("ObjectTop Id:%d Type:%s"),
                pObjTop->GetId(),
                VIEW_COMMON::GetObjctTypeName(eParentType).c_str());
            DB_PRINT(_T("%s%s\n"), strSpc.c_str(), strType.c_str());
        }
    }

    const MAT2D* pParentMat = GetParentMatrix();
    if (pParentMat)
    {
        StdString strParentMat = _T("Parent: ");
        strParentMat += pParentMat->StrAffine();
        DB_PRINT(_T("%s%s\n"), strSpc.c_str(), strParentMat.c_str());
    }

    auto pParent = GetDrawingParent();
    if (pParent)
    {
        const MAT2D* pParentMat2 = pParent->GetDrawingMatrix();
        StdString strParentMat2 = _T("Parent2: ");
        strParentMat2 += pParentMat2->StrAffine();
        DB_PRINT(_T("%s%s\n"), strSpc.c_str(), strParentMat2.c_str());
    }

    const MAT2D* pOffsetMat = GetOffsetMatrix();
    if (pOffsetMat)
    {
        StdString strOffsetMat = _T("Offset: ");
        strOffsetMat += pOffsetMat->StrAffine();
        DB_PRINT(_T("%s%s\n"), strSpc.c_str(), strOffsetMat.c_str());
    }


    MAT2D  mat = GetMatrix();
    StdString strMat = _T("Mat: ");
    strMat += mat.StrAffine();

    DB_PRINT(_T("%s%s\n"), strSpc.c_str(), strMat.c_str());
    DB_PRINT(_T("\n"));
}



//デバッグ用表示文字列
StdString CDrawingObject::Text(StdString strSpc) const
{
    StdStringStream strRet;
    strRet << StdFormat(_T("%sType:%s ID:%d  Name[%s][%s] "))
            % strSpc.c_str()
            % VIEW_COMMON::GetObjctTypeName(m_eType)
            % GetId()
            % m_strName
            % m_pCtrl->GetObjectName(m_nId);
    return  strRet.str();
}

template<class Archive>
bool CDrawingObject::Save(Archive& ar, int iCnt, std::string&  s) const
{
	std::stringstream   strmName;
	std::stringstream   strmNameType;
	strmName << boost::format("Item_%d") % iCnt;
	strmNameType << boost::format("Type_%d") % iCnt;

	DRAWING_TYPE eType = GetType();
	SERIALIZATION_SAVE(strmNameType.str(), eType);
	if (eType == DT_POINT)
	{
		auto pNew = dynamic_cast<const CDrawingPoint*>(this);
		SERIALIZATION_SAVE(strmName.str(), *pNew);
	}
	else if (eType == DT_LINE)
	{
		auto pNew = dynamic_cast<const CDrawingLine*>(this);
		SERIALIZATION_SAVE(strmName.str(), (*pNew));
	}
	else if (eType == DT_CIRCLE)
	{
		auto pNew = dynamic_cast<const CDrawingCircle*>(this);
		SERIALIZATION_SAVE(strmName.str(), *pNew);
	}
	else if (eType == DT_TEXT)
	{
		auto pNew = dynamic_cast<const CDrawingText*>(this);
		SERIALIZATION_SAVE(strmName.str(), *pNew);
	}
	else if (eType == DT_PARTS)
	{
		auto pNew = dynamic_cast<const CDrawingParts*>(this);
		SERIALIZATION_SAVE(strmName.str(), *pNew);
	}
	else if (eType == DT_DIM_L)
	{
		auto pNew = dynamic_cast<const CDrawingDimL*>(this);
		SERIALIZATION_SAVE(strmName.str(), *pNew);
	}
	else if (eType == DT_DIM_H)
	{
		auto pNew = dynamic_cast<const CDrawingDimH*>(this);
		SERIALIZATION_SAVE(strmName.str(), *pNew);
	}
	else if (eType == DT_DIM_V)
	{
		auto pNew = dynamic_cast<const CDrawingDimV*>(this);
		SERIALIZATION_SAVE(strmName.str(), *pNew);
	}
	else if (eType == DT_DIM_A)
	{
		auto pNew = dynamic_cast<const CDrawingDimA*>(this);
		SERIALIZATION_SAVE(strmName.str(), *pNew);
	}
	else if (eType == DT_DIM_D)
	{
		auto pNew = dynamic_cast<const CDrawingDimD*>(this);
		SERIALIZATION_SAVE(strmName.str(), *pNew);
	}
	else if (eType == DT_DIM_R)
	{
		auto pNew = dynamic_cast<const CDrawingDimR*>(this);
		SERIALIZATION_SAVE(strmName.str(), *pNew);
	}
	else if (eType == DT_DIM_C)
	{
		auto pNew = dynamic_cast<const CDrawingDimC*>(this);
		SERIALIZATION_SAVE(strmName.str(), *pNew);
	}
	else if (eType == DT_IMAGE)
	{
		auto pNew = dynamic_cast<const CDrawingImage*>(this);
		SERIALIZATION_SAVE(strmName.str(), *pNew);
	}
	else if (eType == DT_ELLIPSE)
	{
		auto pNew = dynamic_cast<const CDrawingEllipse*>(this);
		SERIALIZATION_SAVE(strmName.str(), *pNew);
	}
	else if (eType == DT_SPLINE)
	{
		auto pNew = dynamic_cast<const CDrawingSpline*>(this);
		SERIALIZATION_SAVE(strmName.str(), *pNew);
	}
	else if (eType == DT_COMPOSITION_LINE)
	{
		auto pNew = dynamic_cast<const CDrawingCompositionLine*>(this);
		SERIALIZATION_SAVE(strmName.str(), *pNew);
	}
	else if (eType == DT_ELEMENT)
	{
		auto pNew = dynamic_cast<const CDrawingElement*>(this);
		SERIALIZATION_SAVE(strmName.str(), *pNew);
	}
	else if (eType == DT_FIELD)
	{
		auto pNew = dynamic_cast<const CDrawingField*>(this);
		SERIALIZATION_SAVE(strmName.str(), *pNew);
	}
	else if (eType == DT_NODE)
	{
		auto pNew = dynamic_cast<const CDrawingNode*>(this);
		SERIALIZATION_SAVE(strmName.str(), *pNew);
	}
	else if (eType == DT_CONNECTION_LINE)
	{
		auto pNew = dynamic_cast<const CDrawingConnectionLine*>(this);
		SERIALIZATION_SAVE(strmName.str(), *pNew);
	}
	else if (eType == DT_GROUP)
	{
		auto pNew = dynamic_cast<const CDrawingGroup*>(this);
		SERIALIZATION_SAVE(strmName.str(), *pNew);
	}
	else if (eType == DT_REFERENCE)
	{
		auto pNew = dynamic_cast<const CDrawingReference*>(this);
		SERIALIZATION_SAVE(strmName.str(), *pNew);
	}
	else
	{
		DBG_ASSERT(!_T("Unknown Type"));
		return false;
	}
	return true;
}

template<class Archive>
std::shared_ptr<CDrawingObject> CDrawingObject::Load(Archive& ar, int iCnt, std::string& s)
{
	std::stringstream   strmName;
	std::stringstream   strmNameType;
	strmName << boost::format("Item_%d") % iCnt;
	strmNameType << boost::format("Type_%d") % iCnt;

	iCnt++;
	DRAWING_TYPE eType;
	SERIALIZATION_LOAD(strmNameType.str(), eType);
	std::shared_ptr<CDrawingObject> pObj;
	if (eType == DT_POINT)
	{
		CDrawingPoint  Obj;
		SERIALIZATION_LOAD(strmName.str(), Obj);
		pObj = std::make_shared<CDrawingPoint>(Obj);
	}
	else if (eType == DT_LINE)
	{
		CDrawingLine  Obj;
		SERIALIZATION_LOAD(strmName.str(), Obj);
		pObj = std::make_shared<CDrawingLine>(Obj);
	}
	else if (eType == DT_CIRCLE)
	{
		CDrawingCircle  Obj;
		SERIALIZATION_LOAD(strmName.str(), Obj);
		pObj = std::make_shared<CDrawingCircle>(Obj);
	}
	else if (eType == DT_TEXT)
	{
		CDrawingText  Obj;
		SERIALIZATION_LOAD(strmName.str(), Obj);
		pObj = std::make_shared<CDrawingText>(Obj);
	}
	else if (eType == DT_PARTS)
	{
		CDrawingParts  Obj;
		SERIALIZATION_LOAD(strmName.str(), Obj);
		pObj = std::make_shared<CDrawingParts>(Obj);
        auto pNewParts = std::dynamic_pointer_cast<CDrawingParts>(pObj);
        pNewParts->SetParentShared(&Obj);
	}
	else if (eType == DT_DIM_L)
	{
		CDrawingDimL  Obj;
		SERIALIZATION_LOAD(strmName.str(), Obj);
		pObj = std::make_shared<CDrawingDimL>(Obj);
	}
	else if (eType == DT_DIM_H)
	{
		CDrawingDimH  Obj;
		SERIALIZATION_LOAD(strmName.str(), Obj);
		pObj = std::make_shared<CDrawingDimH>(Obj);
	}
	else if (eType == DT_DIM_V)
	{
		CDrawingDimV  Obj;
		SERIALIZATION_LOAD(strmName.str(), Obj);
		pObj = std::make_shared<CDrawingDimV>(Obj);
	}
	else if (eType == DT_DIM_A)
	{
		CDrawingDimA  Obj;
		SERIALIZATION_LOAD(strmName.str(), Obj);
		pObj = std::make_shared<CDrawingDimA>(Obj);
	}
	else if (eType == DT_DIM_D)
	{
		CDrawingDimD  Obj;
		SERIALIZATION_LOAD(strmName.str(), Obj);
		pObj = std::make_shared<CDrawingDimD>(Obj);
	}
	else if (eType == DT_DIM_R)
	{
		CDrawingDimR  Obj;
		SERIALIZATION_LOAD(strmName.str(), Obj);
		pObj = std::make_shared<CDrawingDimR>(Obj);
	}
	else if (eType == DT_DIM_C)
	{
		CDrawingDimC  Obj;
		SERIALIZATION_LOAD(strmName.str(), Obj);
		pObj = std::make_shared<CDrawingDimC>(Obj);
	}
	else if (eType == DT_IMAGE)
	{
		CDrawingImage  Obj;
		SERIALIZATION_LOAD(strmName.str(), Obj);
		pObj = std::make_shared<CDrawingImage>(Obj);
	}
	else if (eType == DT_ELLIPSE)
	{
		CDrawingEllipse  Obj;
		SERIALIZATION_LOAD(strmName.str(), Obj);
		pObj = std::make_shared<CDrawingEllipse>(Obj);
	}
	else if (eType == DT_SPLINE)
	{
		CDrawingSpline  Obj;
		SERIALIZATION_LOAD(strmName.str(), Obj);
		pObj = std::make_shared<CDrawingSpline>(Obj);
	}
	else if (eType == DT_COMPOSITION_LINE)
	{
		CDrawingCompositionLine  Obj;
		SERIALIZATION_LOAD(strmName.str(), Obj);
		pObj = std::make_shared<CDrawingCompositionLine>(Obj);
	}
	else if (eType == DT_ELEMENT)
	{
		CDrawingElement  Obj;
		SERIALIZATION_LOAD(strmName.str(), Obj);
		pObj = std::make_shared<CDrawingElement>(Obj);
	}
	else if (eType == DT_FIELD)
	{
		CDrawingField  Obj;
		SERIALIZATION_LOAD(strmName.str(), Obj);
		pObj = std::make_shared<CDrawingField>(Obj);
	}
	else if (eType == DT_NODE)
	{
		CDrawingNode  Obj;
		SERIALIZATION_LOAD(strmName.str(), Obj);
		pObj = std::make_shared<CDrawingNode>(Obj);
	}
	else if (eType == DT_CONNECTION_LINE)
	{
		CDrawingConnectionLine  Obj;
		SERIALIZATION_LOAD(strmName.str(), Obj);
		pObj = std::make_shared<CDrawingConnectionLine>(Obj);
	}
	else if (eType == DT_GROUP)
	{
		CDrawingGroup  Obj;
		SERIALIZATION_LOAD(strmName.str(), Obj);
		pObj = std::make_shared<CDrawingGroup>(Obj);
	}
	else if (eType == DT_REFERENCE)
	{
		CDrawingReference  Obj;
		SERIALIZATION_LOAD(strmName.str(), Obj);
		pObj = std::make_shared<CDrawingReference>(Obj);
	}
	else
	{
		DBG_ASSERT(!_T("Unknown Type"));
	}
	return pObj;
}


//!< load save のインスタンスが生成用ダミー
void CDrawingObject::Dummy()
{
	unsigned int iCnt = 0;
	std::string s = "";

	StdStreamOut outFs(_T(""));
	StdStreamIn  inFs(_T(""));
	//boost::filesystem::ofstream outTxtFs(_T(""));
	//boost::filesystem::ifstream inTxtFs(_T(""));

	boost::archive::text_woarchive txtOut(outFs);
	boost::archive::text_wiarchive txtIn(inFs);

	Save<boost::archive::text_woarchive>(txtOut, iCnt, s);
	Load<boost::archive::text_wiarchive>(txtIn, iCnt, s);

	StdXmlArchiveOut outXml(outFs);
	StdXmlArchiveIn inXml(inFs);

	Save<StdXmlArchiveOut>(outXml, iCnt, s);
	Load<StdXmlArchiveIn>(inXml, iCnt, s);


	boost::filesystem::ofstream outFsStd(_T(""));
	boost::filesystem::ifstream inFsStd(_T(""));

	boost::archive::binary_oarchive outBin(outFsStd);
	boost::archive::binary_iarchive inBin(inFsStd);

	Save<boost::archive::binary_oarchive>(outBin, iCnt, s);
	Load<boost::archive::binary_iarchive>(inBin, iCnt, s);
}
//*******************************
//             TESTw
//*******************************
#ifdef _DEBUG

void TEST_CDrawingObject()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());


    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}
#endif //_DEBUG