/**
 * @brief			CDrawingObjectヘッダーファイル
 * @file			CDrawingObject.h
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:36
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#if !defined(EA_F1391360_1A94_4b10_942C_80F38F4157CC__INCLUDED_)
#define EA_F1391360_1A94_4b10_942C_80F38F4157CC__INCLUDED_


/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DrawingObject/Primitive/POINT2D.h"
#include "DrawingObject/Primitive/MAT2D.h"
#include "Utility/CStdPropertyTree.h"             
#include "DrawingObject/BaseObj.h"             
#include "View/DRAWING_TYPE.h"
#include "View/DRAWING_MODE.h"
#include "View/QUERY_REF_OBJECT.h"
#include "View/ViewCommon.h"
#include "DrawingObject/ACTION/CViewAction.h"

/*---------------------------------------------------*/
/*  Classess                                         */
/*---------------------------------------------------*/
class CDrawingView;
class CPartsDef;
class CDrawingParts;
class CDrawingLine;
class CDrawingPoint;
class CDrawingCircle;
class CDrawingText;
class CDrawingScriptBase;
class CDrawingCompositionLine;
class CDrawingEllipse;
class CDrawingSpline;
class CDrawingDimL;
class CDrawingDimH;
class CDrawingDimV;
class CDrawingDimA;
class CDrawingDimD;
class CDrawingDimR;
class CDrawingDimC;
class CDrawingImage;
class CDrawingField;
class CDrawingGroup;
class CDrawingNode;
class CDrawingConnectionLine;
class CBoundedNumberSubProp;
class CDrawingReference;
class CStdPropertyItem;
class CStdPropertyTree;
class CMarkerData;
class CNodeMarker;
class CNodeBound;
class LINE2D;
class RECT2D;
class CIRCLE2D;
class CPropertySet;
class CViewAction;
class CNodeData;
class CBindingPartner;
class CUndoAction;
class SEGMENT;


/*---------------------------------------------------*/
/*  MACROS                                           */
/*---------------------------------------------------*/
#define DISP_UNIT  CSystem::GetInstance()->GetUnitCtrl()->GetUnit( CSystem::GetInstance()->GetDrawConfig()->GetDispUnitNo())->GetId()
#define DISP_ANGLE CSystem::GetInstance()->GetUnitCtrl()->GetUnit( CSystem::GetInstance()->GetDrawConfig()->GetAngleUnitNo())->GetId()
#define DISP_NONE  _T("NONE")

    struct SNAP_DATA
    {   
        VIEW_COMMON::E_SNAP_TYPE  eSnapType = VIEW_COMMON::SNP_NONE;
        POINT2D                   pt;
        int                       iConnectionIndex= -1;
        std::shared_ptr<CNodeData> pConnection;
        bool                      bKeepSelect; //Textの基準点等でマーカー上にマウスがない場合選択状態とするもの
                                               //GetSnapListで設定しておく（）
        //------------------------------------------
        int                       iObject1 = -1;  //スナップ時のオブジェクト
        int                       iObject2 = -1;  //スナップ時のオブジェクト（交点）
        POINT2D                   ptOrg;    
        StdString                 strSnap; 
        //------------------------------------------
        int                       iLoopIndex = -1; // CDrawingCompositionLine用
        std::set<int>             lstIndex;
    };

    StdString SnapToText(const SNAP_DATA snp);

    //ノード位置変更データ
    struct NODE_CHANGE_DATA
    {
        bool    bPlug; //Src側がPlugかどうか
        int     iChangeObjectId;
        int     iChangeNodeIndex;
        int     iSrcId;
        int     iSrcIndex;
        POINT2D pt;
    };


    // 接続に伴い変更が発生する可能性があるオブジェクト
    class NODE_CONNECTION_CHANGE :private boost::less_than_comparable<NODE_CONNECTION_CHANGE>
    {
    public:
        int     iChangeObjectId = -1; 
        bool    bMove = false;           //移動の可能性の有無

        bool operator == (const NODE_CONNECTION_CHANGE& x) const
        {
            return iChangeObjectId == x.iChangeObjectId;
        }

        bool operator<(const NODE_CONNECTION_CHANGE& x) const
        { 
            return iChangeObjectId < x.iChangeObjectId; 
        }
    };


//テンプレート特殊化  BaseはMOCK_ERROR.h
template<> StdString GetObjectNameT(CDrawingObject* t);

class IDrawingObject
{
public:

    enum E_MOUSE_OVER_TYPE
    {
        E_MO_NO = 0,            //マウスオーバなし
        E_MO_SELECTABLE   = 1,    //選択可能
        E_MO_EMPHASIS     = 2,    //強調表示
        E_MO_CONNECTABLE  = 3,    //接続可能
        E_MO_DRAGGING     = 4,    //ドラッグ中
        //-------------------------
        E_MO_IGNORE_SELECT = 5,   //SEKECT無視
    };

	enum E_OBJECT_STATUS
	{
		ES_NOMAL     = 0x0000,  //通常状態
		ES_SELECT    = 0x0001,  //選択状態
		ES_IMAGINARY = 0x0002,  //想像
		ES_RELATION  = 0x0004,  //関連
		ES_TMP_COPY  = 0x0010,  //一時的なコピー  これのみコピーコンストラクターでコピーされる(cloneされる)
    };

    enum PROPERTY_TYPE_GROUP
    {
        P_BASE = 0x0001,    // レイヤー, 色
        P_LINE = 0x0002,    // 線種,線幅



        P_ALL  = 0x0003,    // 全て込み
    };

    // 通常のコピーは、参照の設定を行い、 ID値は無視
    // ID値の
    enum COPY_TYPE
    {
        CP_DEFAULT  = 0x0000,    
        CP_NO_REF   = 0x0001,    //参照を無視 
        CP_ID       = 0x0002,    // IDをコピー
        CP_ID_CREATE= 0x0004,    // IDを作成
    };


    enum SEL_MENU
    {
        SEL_MNU_COPY,
        SEL_MNU_MOVE,
        SEL_MNU_CANCEL
    };


    enum RELATION
    {
        REL_NONE      = 0x0000,
        REL_RELATION  = 0x0001,  //関連性あり
        REL_STRATCH   = 0x0002,  //ストレッチ
        REL_PART      = 0x0010,  //一部内側
        REL_PART_STRATCH  = (REL_PART | REL_STRATCH),
        REL_PART_RELATION = (REL_PART | REL_RELATION),
        REL_INNER      = 0x0100,  //完全に内側
    };

};




/**
 * @class   CDrawingObject
 * @brief   描画データ                     
 */
class CDrawingObject  : public CBaseObj, public IDrawingObject, public std::enable_shared_from_this<CDrawingObject>
{
friend CPartsDef;
public:
/*
    enum PROPERTY_TYPE_GROUP
    {
        P_BASE = 0x0001,    // レイヤー, 色
        P_LINE = 0x0002,    // 線種,線幅



        P_ALL  = 0x0003,    // 全て込み
    };

    // 通常のコピーは、参照の設定を行い、 ID値は無視
    // ID値の
    enum COPY_TYPE
    {
        CP_DEFAULT  = 0x0000,    
        CP_NO_REF   = 0x0001,    //参照を無視 
        CP_ID       = 0x0002,    // IDをコピー
        CP_ID_CREATE= 0x0004,    // IDを作成
    };
*/

	//!< コンストラクタ
    CDrawingObject();

    //!< コンストラクタ
	CDrawingObject(int nID, DRAWING_TYPE eType);

    //!< コピーコンストラクタ
	CDrawingObject(const CDrawingObject& Obj);

    //!< デストラクタ
	virtual ~CDrawingObject();

    //複製
    static CDrawingObject* Clone(const CDrawingObject* pObj, bool bRef = true);
    static std::shared_ptr<CDrawingObject> CloneShared(const CDrawingObject* pObj, bool bRef = true);
    static std::unique_ptr<CDrawingObject> CloneUnique(const CDrawingObject* pObj, bool bRef = true);
	static std::shared_ptr<CDrawingObject> CreateShared(DRAWING_TYPE eType);

    //!< コピーコンストラクタ
	virtual void CopyDrawParm(const CDrawingObject& Obj);

    //!< 参照パラメータ設定
    virtual void SetRefParam(const CDrawingObject* pObj, COPY_TYPE cpy);

	//ディフォルト設定時の初期化
	virtual void InitDefault() { ; }

    // 本来は純粋仮想関数にするべきだが、リフレクションの関係で仮想関数とする
	//!< 絶対移動
    //virtual void AbsMove(const POINT2D& pt2D){;}


	//!< オフセット
	virtual void OffsetMatrix(const MAT2D* pMat);

	//!< 相対移動   (AS)
	virtual void Move(const POINT2D& pt2D) {;}

    //!< 回転          (AS)
	virtual void Rotate(const POINT2D& pt2D, double dAngle){;}

    //!< 鏡像          (AS)
	virtual void Mirror(const LINE2D& line){;}

    //!< 倍率          (AS)
	virtual void Scl(const POINT2D& pt2D, double dXScl, double dYScl){;}

    //!< 行列適用      (AS)
    virtual void Matrix(const MAT2D& mat2D){;}

	//!< 調整          (AS)
    virtual bool Trim (const POINT2D& ptIntersect, const POINT2D& ptClick, bool bReverse){ return false;}

	//!< 調整          
    virtual bool TrimCorner (const POINT2D& ptIntersect, const POINT2D& ptClick){ return false;}

    //!< 交点計算      (AS)
    virtual void CalcIntersection ( std::vector<POINT2D>* pLiat,   
                                    const CDrawingObject* pObj, 
                                    bool bOnLine = true,
                                    double dMin = NEAR_ZERO)const{;}

    //!< 交点計算(点)   (AS)
    virtual void CalcIntersectionPoint ( std::vector<POINT2D>* pLiat,   
                                    const CDrawingObject* pObj, 
                                    POINT2D ptPos, 
                                    bool bOnLine = true,
                                    double dMin = NEAR_ZERO) const;


    //!< 分割           (AS)
    //virtual CDrawingObject*  Break (const POINT2D& ptIntersect){ return NULL;}
    virtual std::shared_ptr<CDrawingObject>  Break (const POINT2D& ptIntersect){ return NULL;}

	//!< 範囲           (AS)
    virtual bool IsInner( const RECT2D& rcArea, bool bPart) const { return false;}

    //!< 範囲
    virtual RELATION RelationRect(const RECT2D& rcArea) const;


    //!< 距離
    virtual double Distance(const POINT2D& ptIntersect) const{return DBL_MAX;}

    //!< 描画
    virtual void Draw(CDrawingView* pView, 
                        const std::shared_ptr<CDrawingObject> pParentObject, 
                        E_MOUSE_OVER_TYPE eMouseOver = E_MO_NO,
                        int iLayerId = -1) {;}

    //!< 長さ(線,円弧,スプライン上)
    virtual double Length(const POINT2D& ptIntersect, bool bStart) const{return DBL_MAX;}

    //!< オフセット(線,円弧,スプライン上)
    virtual bool Offset(double dLen, POINT2D ptDir){return false;}

    //!< オフセット(線,円弧,スプライン上)
    virtual bool Offset(double dLen){return false;}

    //!< 点が進行方向の右側にあるか
    virtual bool IsRightSide(POINT2D ptDir) const {return false;}


    //!< 描画削除
    virtual void DeleteView(CDrawingView* pView, 
                            const std::shared_ptr<CDrawingObject>,
                            int iLayerId = -1){;}

    //!< 描画(マウスオーバー)
    virtual void DrawOverStart(CDrawingView* pView);

    //!< 描画(マウスオーバー)
    virtual void DrawOverEnd(CDrawingView* pView);


    //!< オブジェクトコントロール設定
    virtual void SetPartsDef(CPartsDef* pCtrl);

    //!< 領域取得       (AS)
    virtual RECT2D GetBounds() const;

    //!< オブジェクトコントロール取得
    CPartsDef* GetPartsDef() const;

    //!< ID割り当て
    virtual int AssignId(bool bKeepUnfindId);

    //!< 線描画(ペン変更なし)
    virtual void LinePri( CDrawingView* pView,
                          int iLayerId,
                          const LINE2D* pLine2D, DRAWING_MODE eMod) const;

    //!< 複合線描画(ペン変更なし)
    virtual void LineMulti( CDrawingView* pView,
                    int iLayerId,
                    const std::vector<POINT2D>* pLstPt, 
                    bool bSkioFirstPoint = false) const;

    virtual void LineMulti2( CDrawingView* pView,
        int iLayerId,
        const std::vector<POINT2D>* pLstPt, 
        bool bSkioFirstPoint,
        bool bFixLayerScl) const;

    virtual void LineMulti( CDrawingView* pView,
        int iLayerId,
        const std::vector<LINE2D>* pLstLine, 
        bool bSkioFirstPoint = false) const;
    
    virtual void FillMulti( CDrawingView* pView,
                            int iLayerId,
                            const std::vector<POINT2D>* pLstPt) const;

    virtual void FillMulti2( CDrawingView* pView,
        int iLayerId,
        const std::vector<POINT2D>* pLstPt,
        bool bFixLayerScl) const;

    //!< 描画色取得
    virtual COLORREF GetDrawColor(CDrawingView* pView,
                                 int iLayerId,
                                 const CDrawingObject* pParentObject,
                                 int* pID, 
							     bool bIgnoreSelect,
								 bool *pFoce = NULL) const;

    //!< スナップ点取得
    virtual  bool GetSnapList(std::list<SNAP_DATA>* pLstSnap)const {return false;}

    //!< デバッグ用文字
    virtual StdString GetStr() const;

    //!< ユーザーデータ設定(AS)
    virtual void SetUserData(void* pUserData, int refTypeId);

    virtual void SetUserData(__int64 &iData);

    virtual void SetUserData(double &dData);

    //!< ユーザーデータ取得(AS)
    virtual bool GetUserData(void* pUserData, int refTypeId);

    virtual bool GetUserData(__int64 &iData);

    virtual bool GetUserData(double &dData);

    //--------------------------

    //----------
    // 端部描画
    //----------
     bool DrawigArrow(CDrawingView* pView, 
                          int iLayerId,
                          VIEW_COMMON::E_ARROW_TYPE eType,
                          const POINT2D& pt,     //表示位置
                          POINT2D vec) const;


    bool DrawigArrowOpen(CDrawingView* pView, 
                                   int iLayerId,
                                   const POINT2D& pt,
                                   const POINT2D& ptVe,
                                   bool bFill) const;


    bool DrawigArrowDot(CDrawingView* pView, 
                                   int iLayerId,
                                   const POINT2D& pt,     //表示位置
                                   bool bFill) const;


    bool DrawigOblique(CDrawingView* pView, 
                               int iLayerId,
                               const POINT2D& pt,
                               const POINT2D& ptVe) const;

    bool DrawigDataum(CDrawingView* pView, 
                               int iLayerId,
                               const POINT2D& pt,
                               const POINT2D& ptVe,
                               bool bFill) const;


    //-------------------------
    // リフレクション設定
    //-------------------------

    //!< オブジェクト種別  (AS)
    DRAWING_TYPE GetType() const{ return m_eType;}

    //!< ID設定
    //通常は使用しない
    //DIM等、複合的な図形で使用する
    virtual void  SetId(int iId)        {m_nId = iId;} 

private:
friend CDrawingCompositionLine;

public:

    //!< 表示許可設定(AS)
    virtual void SetVisible(bool bVisible){m_bVisible = bVisible;}

    //!< 表示許可取得(AS)
    bool IsVisible() const { return m_bVisible;}

    //!< タグ設定(AS)
    bool SetTag(StdString strTag){m_strTag = strTag; return true;}

    //!< タグ取得(AS)
    StdString  GetTag() const { return m_strTag;}

    //!< タグリスト取得
    void  GetTagList(std::vector<StdString>* pTagList)const;

    //!< タグ追加
    bool AddTag(StdString strAdd);

    //!< ID取得             (AS)
    int  GetId() const;

    //!< 色設定             (AS)
    virtual void SetColor(COLORREF color){m_crObj = color;}

    //!< 色取得             (AS)
    COLORREF GetColor() const         {return m_crObj;}

    //!< レイヤーID設定     (AS)
    virtual void SetLayer(int iLayerId) {m_iLayerId = iLayerId;}

    //!< レイヤーID取得     (AS)
    int  GetLayer() const            {return m_iLayerId;}

    virtual bool GetLayerCoror(COLORREF* crPen, 
                               const CDrawingView* pView, 
                                int iLayerId) const; 

    //!< データ名設定  
    void SetName(LPCTSTR Name);

    //!< データ名設定 (AS)
    void SetNameStr(StdString strName){SetName(strName.c_str());}

    void SetNameDirect(LPCTSTR strName);

    //!< データ名取得       (AS)
    StdString  GetName() const;

    //!< データ名取得
    StdString  GetFullName();

    //!< 選択状態設定
    virtual void SetSelect(bool bSel, const RECT2D* pRect = NULL);
    virtual void SetSelectNoRelSelPoint(bool bSel);
    
    //!< ストレッチモードが設定されているときは、無条件ですべてが (DRAW_CONFIG->IsSelPartの結果によらず)
    //!< 選択されていない状態では選択されない
    //!< ex) connection line で線と寸法はストレッチするのに接続線は接続が切れてしまう
    //!< 同じような動作を行うオブジェクトを作りたい場合この設定にTrueを返すことで
    //!< 同じ動作ができるようになる。実装はDrawingParsで必要になる
    virtual bool IsIgnoreSelPart() const { return false; }

    //!< 選択状態取得
    virtual bool IsSelect() const;

    void SetImaginary(bool bImaginary);
    bool GetImaginary() const;

    void SetRelation(bool bRelation);
    bool GetRelation() const;

    void SetStatus(UINT iStatus) { m_eSts = iStatus; }
    UINT GetStatus() const { return m_eSts; }

	void SetTmpCopy(bool bTmpCopy);
	bool GetTmpCopy() const;
	
	void ClearStatus();

    //!< 始点終点交換(Line,Circle,Splie,CompositLine用)
    virtual bool  SwapStartEnd() {return false;}

    //!< 始点取得(Line,Circle,Splie,CompositLine用)
    virtual POINT2D  GetStartSide() const {return POINT2D();}

    //!< 始点取得(Line,Circle,Splie,CompositLine用)
    virtual POINT2D  GetEndSide() const {return POINT2D();}

    //!<  中点取得(Line,Circle,Splie,CompositLine用)
    virtual POINT2D  GetMidPoint() const {return POINT2D();}

    //!<  任意点取得(Line,Circle,Splie,CompositLine用)
    virtual POINT2D  GetPointByRate(double d) const {return POINT2D();}

    //!<  近接点取得(Line,Circle,Splie,CompositLine用)
    virtual bool  GetNearPoints(POINT2D*  ptNear, const POINT2D& pt, bool bOnline) const {return false;}

    //!<  法線角取得(Line,Circle,Splie,CompositLine用)
    virtual bool GetNormAngle(double* pAngle, const POINT2D& pt, bool bOnline) const {return false;}

    //描画色取得
    //COLORREF GetDrawColor(E_MOUSE_OVER_TYPE eMouseOver) const;


    //!< AS ダウンキャスト用
    CDrawingObject* GetDrawingObject(){ return this;}


    //!< AS アップキャスト用
    CDrawingLine* AsLine();
    CDrawingCircle* AsCircle();
    CDrawingPoint* AsPoint();
    CDrawingText* AsText();
    CDrawingNode* AsNode();
    CDrawingEllipse* AsEllipse();
    CDrawingParts* AsParts();
    CDrawingField* AsField();
    CDrawingSpline* AsSpline();
    CDrawingDimL* AsDimL();
    CDrawingDimH* AsDimH();
    CDrawingDimV* AsDimV();
    CDrawingDimA* AsDimA();
    CDrawingDimD* AsDimD();
    CDrawingDimR* AsDimR();
    CDrawingDimC* AsDimC();
    CDrawingImage* AsImage();
    CDrawingGroup* AsGroup();
    CDrawingReference* AsReference();

    CDrawingCompositionLine* AsCompositionLine();
    CDrawingConnectionLine* AsConnectionLine();

    //-------------------------
    // 共通設定用 PROPERTY_TYPE_GROUP
    //!< 線幅設定
    virtual void SetLineWidth(int iWidth){;}

    //!< 線幅取得
    virtual int GetLineWidth() const {return 0;}

    //!< 線種設定
    virtual void SetLineType(int iType){;} 

    //!< 線種取得
    virtual int  GetLineType() const{return 0;}
    //-------------------------

    //!< リフレクション設定
    static void RegisterReflection();

    //!< オブジェクト種別名称取得
    //static StdString GetObjctTypeName(DRAWING_TYPE eType);

    
    //!< プロパティ変更時描画
    virtual void Hide();
    virtual void Redraw();

    //-------------------------
    // 参照
    //-------------------------

    //!< 参照元変更の有無
    virtual bool IsChangeRef() const;

    //!< 参照元の取得
    virtual CDrawingObject* GetRefObj() const;

	virtual CPartsDef* GetRefDef() const;
	
	//!< 参照データに基づいて更新
    virtual CDrawingObject* UpdateRef();


    //-------------------------
    // プロパティ関連メソッド
    //-------------------------

protected:

    //!<オブジェクト内プロパティ初期化
    virtual void _IntilizeStdProperty();

    //!<プロパティ初期化
    virtual TREE_GRID_ITEM*  _CreateStdPropertyTree();

    //!< プロパティグリッド更新準備
    virtual void _PrepareUpdatePropertyGrid();

    //!< プロパティ画面更新
    virtual void _UpdatePropertyGrid(CMFCPropertyGridCtrl *pGrid);

    bool _SetPropertySetName(StdString strName);
    

public:

    //!<プロパティ設定
    virtual void SetProperty(CMFCPropertyGridCtrl *pGrid, UINT uiMask);

    //!<プロパティリセット
    virtual void ResetProperty();

    //!<プロパティ更新後処理
    //virtual void AfterFunc(CStdPropertyItem* pItemData);

    //!< プロパティグループ種別
    int  GetPropertyGroupFeatures()const{return m_iPropFeatures;}

    //!< プロパティ削除
    bool DeleteProperty(int iId);

    //!< プロパティアイテム取得 (AS)
    CStdPropertyItem* GetPropertyItem(StdString strName) const;

    //-------------------------

    //----------------------------------------
    // ユーザ定義プロパティ関連メソッド
    //----------------------------------------
    //!<ユーザプロパティ開放
    void ReleaseUserProperty();

protected:

    //!<ユーザプロパティ設定
    virtual TREE_GRID_ITEM* _CreateStdUserPropertyTree(TREE_GRID_ITEM* pItem);

    //!<プロパティセット設定
    virtual TREE_GRID_ITEM* _CreatePropertySetTree(TREE_GRID_ITEM* pItem);

    //!< ユーザーデータ削除
    void _FreeUserData();

public:
    //!< ユーザプロパティ初期値設定
    virtual void InitUserPropertyValue(bool bCreate){;}

    //!<ユーザプロパティIDリスト取得
    std::vector<int>* GetUserPropertyIdList();

    //!<ユーザプロパティ変更
    static bool ChgUserProp (CStdPropertyItem* pData, void*pObj);

    //!<ユーザプロパティイベント
    virtual void OnChgUserProp (CStdPropertyItem* pData, StdString strPropName){;}

    //----------------------
    // プロパティセット
    //----------------------
    bool IsUsePropertySet(){return m_bUsePropertySet;}

    StdString GetPropertySetName() const
    {
        if (m_bUsePropertySet)
        {
            if (m_pPropertySetName)
            {
                return *m_pPropertySetName;
            }
        }

        return _T("NONE");
    }

    //!< プロパティセット初期値設定
    virtual void InitPropertySetValue(bool bCreate);

    //プロパティセット値設定
    static bool ChgPropertySetVal (CStdPropertyItem* pData, void*pObj);

    //プロパティセット値取得
    CAny* GetPropertySetVal(StdString strName) const;

    //プロパティセット値取得
    CAny* GetPropertySetValS(std::string& strName) const
    {
        StdString strOut =  CUtil::CharToString(strName.c_str());
        return GetPropertySetVal(strOut);
    }

protected:
    //プロパティセット値設定
    bool _SetPropertySetDefVal(StdString strName, 
    int eItem, StdString strItem);

public:
    //プロパティセット値設定(AS)
    bool SetPropertySetDefMin(StdString strName, StdString strItem);
    bool SetPropertySetDefMax(StdString strName, StdString strItem);
    bool SetPropertySetDefNote(StdString strName, StdString strItem);
    bool SetPropertySetValAny(StdString strName, void* pRef, int refTypeId);

public:

    //----------------
    // データ変更
    //----------------
    //!< 変更カウント取得
    int GetChgCnt() const;

    //!< 変更カウント追加
    void AddChgCnt();
    //----------------

    //!< トップグループ取得
    std::shared_ptr<CDrawingObject> GetTopGroup();

    //!< 描画トップグループ取得
    const CDrawingObject* GetTopDraw(const CDrawingObject* pParentObject) const;

    //!< ルートに属しているか
    bool IsBelongToRoot(const CDrawingObject* pParentObject) const;

    //!< 近接点取得
    static bool NearPointByList(POINT2D* ptRet, const std::vector<POINT2D>* pList, const POINT2D& ptNear);

    //!< 近接点取得
    virtual bool NearPoint(POINT2D* ptRet, const POINT2D& ptNear, bool bOnLine) const;

    //!< ロード終了後処理
    virtual void LoadAfter(CPartsDef* pDef){;}

    //-------------------
    // ドラッグ用マーカ
    //-------------------
    //!< マーカ初期化 true:マーカあり
    virtual bool InitNodeMarker(CNodeMarker* pMarker){return false;}

    //!< マーカ選択
    virtual void SelectNodeMarker(CNodeMarker* pMarker,
                                     StdString strMarkerId){;}

    //!< マーカ移動
    virtual void MoveNodeMarker(CNodeMarker* pMarker,
                                     SNAP_DATA* pSnap,
                                     StdString strMarkerId,
                                     MOUSE_MOVE_POS posMouse){;}

    //!< マーカ開放
    virtual bool ReleaseNodeMarker(CNodeMarker* pMarker, StdString strMarkerId, MOUSE_MOVE_POS posMouse){return true;}

    //!< ノード変更
    virtual void ChangeNode(StdString strMarkerId, E_NODE_CHANGE_TYPE eType){;}

    //-------------------------
    //!< 選択前でのマウス位置
    //---------------------------
    virtual void PreSelectedMouseMove(CDrawingView* pView, MOUSE_MOVE_POS posMouse){;}

    //-------------------------
    //!< 選択状態でのマウス位置
    //---------------------------
    virtual void SelectedMouseMove(CDrawingView* pView, MOUSE_MOVE_POS posMouse){;}
    virtual void SelectedMouseUp  (CDrawingView* pView, MOUSE_MOVE_POS posMouse){;}
    virtual void SelectedMouseDown(CDrawingView* pView, MOUSE_MOVE_POS posMouse){;}


    //!< データ->ビュー変換     (AS)
    POINT GetScreenCoordinate(POINT2D pt2D, const CDrawingView& view) const;
         
    //!< ビュー->データ変換     (AS)
    POINT2D GetWorldCoordinate(POINT pt, const CDrawingView& view) const;

     //!< ワールド->ローカル座標変換     (AS)
    POINT2D ConvWorldToLocal(const POINT2D& pt) const;

    //!< ローカル->ワールド座標変換     (AS)
    POINT2D ConvLocalToWorld(const POINT2D& pt) const;

    //!< マトリクス取得
    virtual MAT2D GetMatrix() const;

    //!< 描画用マトリクス取得
    virtual const MAT2D* GetDrawingMatrix()const;

    //!< 描画用マトリクス取得
    const MAT2D* GetParentMatrix() const;

    //!< オフセット用マトリクス取得
    const MAT2D* GetOffsetMatrix() const;

    //!< 描画用マトリクス生成
    MAT2D CDrawingObject::CreateDrawingMatrix();

    //!< 同一スケール判定
    bool IsSameScale(const CDrawingObject* pObj) const;

    //!< CIRCLE変換
    void GetCircleRect(RECT* pRc, 
        int iLayerId, 
        POINT* ptStart, POINT* ptEnd,
        const CDrawingView* pView,
        const CIRCLE2D& circle)const;


    //----------
    // 接続
    //----------
    //!< ノード接続可能判定
    virtual bool IsAbleConnectNode(CNodeData * pNode, int iIndex);

    //!< ノード接続
    virtual bool ConnectNode(CDrawingObject* pDestObj, int iDestIndex, int iSrcIndex);
    virtual bool AddToSocket(int iNodeData, CDrawingObject* pDestObject, int iDestNodeIndex);
    virtual bool AddToPlug(int iNodeData, 
                           CDrawingObject* pDestObject, 
                           int iDestNodeIndex,
                           int iPlugNo = -1);


    //!< ノード接続解除
    virtual bool ReleaseNode(int iIndex,
                                 CDrawingObject* pDeleteObj,
                                 int iDeleteNodeIdOnThisObject);

    virtual bool ReleaseNode(CDrawingObject* pObj);
    virtual bool ClearNode();

    virtual bool CreateNodeData(int iIndex) ;
    virtual CNodeData* GetNodeData(int iIndex) ;
    virtual CNodeData* GetNodeDataConst(int iIndex) const;

    virtual void SetCopyNodeDataMode(bool bSet) {;}
    virtual bool GetCopyNodeDataMode() const { return false;}
    virtual int GetCopySrcObjectId() const {return -1;}
    virtual void ResetCopySrcObjectId(){;}

    virtual bool GetNodeDataPos(POINT2D* pt, int iIndex) const;

    virtual int GetNodeDataNum() const{ return 0;}
    //virtual int GetConnectionId(int iNo) const{ return 0;}
    //virtual void ClearNode(){;}
    virtual int GetConnectionObjectList(std::vector<std::weak_ptr<CDrawingObject>>* pList);
    virtual int GetConnectionList(std::vector<CBindingPartner>* pList);

    virtual bool UpdateNodeSocketData(CDrawingObject* pTmpObj = NULL,
                                  CPartsDef* pDef = NULL);

    virtual bool UpdateNodePlugData(CPartsDef* pDef = NULL);

    virtual bool OnUpdateNodeSocketData(const POINT2D* pPt,
                                        const CDrawingObject* pSocketObj,
                                        int iSocketIndex,
                                        int iIndex,
                                        std::set<int>* pList,
                                        CUndoAction* pUndo
                                    );
    virtual bool OnReleaseNodeData(const CDrawingObject* pObj, int iIndex){return false;}
    
    virtual bool GetChangeObjectsList(CPartsDef* pDef, std::set<NODE_CONNECTION_CHANGE>* pList);

    //方向問い合わせ 
    virtual RECT2D::E_EDGE InquireConnectDirection(std::map<RECT2D::E_EDGE, LINE2D>* pDir, RECT2D* pRc, int iConnectionId )const{return RECT2D::E_NONE;}

    //----------
    //!< 親オブジェクト取得
    std::shared_ptr<CDrawingObject> GetParentParts() const;

    //!< 描画時の親オブジェクト
    const std::shared_ptr<CDrawingObject> GetDrawingParent() const;

    //----------
    // Utility
    //----------
    static StdString GetValText(double dVal, bool bAngle, int iPrecision);

    //デバッグ用表示
    void PrintObj(StdString strSpc) const;

    //デバッグ用表示文字列
    virtual StdString Text(StdString strSpc = _T("")) const;

protected:
friend CDrawingScriptBase;
friend CDrawingParts;
friend CDrawingReference;
friend CDrawingGroup;
friend CDrawingNode;
friend CDrawingCompositionLine;
friend CDrawingObject;
friend CMarkerData;
friend CNodeMarker;
friend CNodeBound;
friend SEGMENT;

    void _SetDrawingParent(std::weak_ptr<CDrawingObject> pObjcet);


    virtual void Line( CDrawingView* pView, 
                            int iLayerId,
                            const LINE2D* pLine2D, 
                            int iPenStyle, int iWidth, 
                            COLORREF crPen, int iId) const;

    virtual void  Circle( CDrawingView* pView, 
                             int iLayerId,
                             const CIRCLE2D* pCircle2D,
                             const CDrawingObject* pParentObject,
                             int iPenStyle,
                             int iWidth, 
                             COLORREF crPen, 
                             int iId) const;


    //複製後処理
    virtual void CloneAfter();

    //!<描画(マウスオーバー時)
    virtual void DrawOver(CDrawingView* pView, bool bOver) ;

    //!< 親オブジェクト設定
    virtual void SetParentParts(std::weak_ptr<CDrawingObject> pParent);


    //!<プロパティセット使用有無設定
    bool SetUsePropertySet(bool bUse);

    //!< 単体選択時に情報を表示する
    virtual void DebugPrintOnSelect();

	template<class Archive>
	bool Save(Archive& ar, int iCnt, std::string& s) const;

	template<class Archive>
	static std::shared_ptr<CDrawingObject> Load(Archive& ar, int iCnt, std::string& s);

	void Dummy();
protected:
    friend CStdPropertyTree;
    friend CDrawingObject;

    //--------------------
    // プロパティ変更処理
    //--------------------
    static bool PropTmp     (CStdPropertyItem* pData, void* pObj){ return true;}
    static bool PropName    (CStdPropertyItem* pData, void* pObj);
    static bool PropColor   (CStdPropertyItem* pData, void* pObj);
    static bool PropLayer   (CStdPropertyItem* pData, void*pObj);
    static bool PropVisible (CStdPropertyItem* pData, void*pObj);
    static bool PropTag     (CStdPropertyItem* pData, void*pObj);
    static bool PropPropertySetName (CStdPropertyItem* pData, void*pObj);
	static bool PropDummy	(CStdPropertyItem* pData, void* pObj);


    //複写　移動メニュー表示
    SEL_MENU _ShowMoveCopyMenu(CNodeMarker* pMarker);

protected:
    //-----------------------------
    // 保存データ
    //-----------------------------

    //!< 名称
    //!< 変更用に用いる
    StdString        m_strName;

    //!< タグ
    StdString       m_strTag;

	//!< オブジェクト種別
	DRAWING_TYPE    m_eType;

    //!< 色
	COLORREF        m_crObj;

	//!< レイヤーID
	int             m_iLayerId;

	//!< 識別ＩＤ
	int             m_nId;

	//!< 実行時表示
    bool            m_bVisible;

	//!< 識別ＩＤ(プロパティ表示用）
	int             m_nOffsetId;

	//!< プロパティセット
	StdString*                              m_pPropertySetName;
    std::map<StdString, CAny>*              m_pPropertySetMap;
    CPropertySet*                           m_psPropertySet;

    //!< 親オブジェクト(グループ)
    std::weak_ptr<CDrawingObject>          m_pParent;

    //!< 参照元オブジェクト
    //!<       DrawingPartsに含まれているObjectのコピー元
    //!<       
    QUERY_REF_OBJECT*       m_psRefObject;



    //-----------------------------
    // 保存不要データ
    //-----------------------------

    //!< プロパティ
    CStdPropertyTree*      m_psProperty;

    //!< オブジェクト種別名
    StdString               m_strObject;

    //!< データ変更カウント
    int                     m_iChgCnt;

    //!< ユーザープロパティ変更カウント
    int                     m_iChgPropUserDefCnt;

	//!< 選択状態
	//bool					m_bSel;
	UINT                    m_eSts;

    //!<プロパティセット使用有無
    bool            m_bUsePropertySet;

    //!<プロパティセット選択 使用有無
    bool            m_bPropertySetSelecter;

	//!<ディフォルト設定の有無
	bool            m_bSetDefault;
	
	//!< AS ユーザーデータ
    void*           m_pUserData;
    int             m_iUserDataType;

    //!<描画時の親オブジェクト
    std::weak_ptr<CDrawingObject>  m_pDrawingParent;
    //--------------------
    // プロパティ変更処理
    //--------------------
    //!< コールバック呼び出し元
    CPartsDef* m_pCtrl;

    //!< プロパティ初期化フラグ
    bool m_bInitProperty;

    //!< プロパティグループ種別
    int m_iPropFeatures;

    //--------------------

    //接続ノードデータ
    //std::vector<int>*     m_pLstConnectNode;
    mutable std::shared_ptr<CNodeData>   m_psNodeData;

    //オフセット用マトリクス
    const MAT2D* m_pMatOffset;

    //==============================
    // ファイル保存
    //==============================
private:
    friend class boost::serialization::access;  

	BOOST_SERIALIZATION_SPLIT_MEMBER();

	template<class Archive>
	void load(Archive& ar, const unsigned int version);

	template<class Archive>
	void save(Archive& ar, const unsigned int version) const;

	/*
	template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
        int iVisible;
        int iUsePropertySet;
        //!< ユーザー定義値(データ保存時に使用)
        if ( !Archive::is_loading::value)
        {
            iVisible = (m_bVisible? 1:0);
            iUsePropertySet = (m_bUsePropertySet? 1:0);
        }

        SERIALIZATION_BOTH("Type"      , m_eType);
 //       SERIALIZATION_BOTH("Name"      , m_strName);
 //       SERIALIZATION_BOTH("Tag"       , m_strTag);
        SERIALIZATION_BOTH("Color"     , m_crObj);
        SERIALIZATION_BOTH("LayerID"   , m_iLayerId);
        SERIALIZATION_BOTH("Id"        , m_nId);
        SERIALIZATION_BOTH("Visible"   , iVisible);
        SERIALIZATION_BOTH("PropFeatures"  , m_iPropFeatures);
        SERIALIZATION_BOTH("UsePropertySet"  , iUsePropertySet);

        if ( Archive::is_loading::value)
        {
            m_bVisible = (iVisible != 0);
            m_bUsePropertySet = (iUsePropertySet != 0);

            if (m_bUsePropertySet)
            {
                if(!m_pPropertySetName)
                {
                    m_pPropertySetName = new StdString;
                }

                if(!m_pPropertySetMap)
                {
                    m_pPropertySetMap = new std::map<StdString, CAny>;
                }
            }
        }

        if (m_bUsePropertySet)
        {
            SERIALIZATION_BOTH("PropertySetName"  , *m_pPropertySetName);
            SERIALIZATION_BOTH("PropertySet"      , *m_pPropertySetMap);
        }

        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }
*/
};



BOOST_CLASS_VERSION(CDrawingObject, 0);

#endif // !defined(EA_F1391360_1A94_4b10_942C_80F38F4157CC__INCLUDED_)
