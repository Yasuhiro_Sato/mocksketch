/**
 * @brief			CDrawingParts実装ファイル
 * @file			CDrawingParts.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CDrawingObject.h"

#include "View/QUERY_REF_OBJECT.h"
#include "View/DRAWING_DATA.h"

#include "MockSketch.h"
#include "CProjectCtrl.h"

#include "./CDrawingPoint.h"
#include "./CDrawingParts.h"
#include "./CDrawingCircle.h"
#include "./CDrawingDim.h"
#include "./CDrawingCircle.h"
#include "./CDrawingSpline.h"
#include "./CDrawingEllipse.h"
#include "./CDrawingCompositionLine.h"
#include "./CDrawingLine.h"
#include "./CDrawingNode.h"
#include "./CNodeMarker.h"
#include "./CNodeBound.h"
#include "./CNodeData.h"


#include "DefinitionObject/View/CDrawingView.h"
#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/ObjectDef.h"
#include "DrawingObject/Primitive/MAT2D.h"
#include "View/CLayer.h"
#include "View/CDrawMarker.h"
#include "View/CUndoAction.h"

#include "System/CSystem.h"
#include "Utility/CustomPropertys.h"
#include "Utility/CStdPropertyTree.h"
#include "Utility/CPropertyGrid.h"
#include "Utility/CUtility.h"
#include "Script/CScriptObject.h"
#include <math.h>
#include <boost/serialization/export.hpp> 
BOOST_CLASS_EXPORT(CDrawingParts);


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
CDrawingParts::CDrawingParts():
CDrawingParts(-1)
{
}

/**
 *  @brief  コンストラクター.
 *  @param  [in] nID 
 *  @retval なし 
 *  @note
 */
CDrawingParts::CDrawingParts(int nID):
CDrawingScriptBase( nID, DT_PARTS),
m_bCopyNodeDataMode(true),
m_iTmpConnectionObjId(-1)
{
    m_dAngle = 0.0;
    m_bUseGroupColor = true;
    m_iPropFeatures = P_BASE; 
    m_bStopGabage = true;
    m_bExecGabage = false;
    m_bOperationInheritance = false;
    m_iChgGroup   = 0;
    m_dSclX = 1.0;
    m_dSclY = 1.0;
}
/**
 *  @brief  コピーコンストラクター.
 *  @param  [in] group 
 *  @param  [in] bRef true:参照としてコピー 
 *  @retval なし 
 *  @note    ID,  コントロール
 *           はコピー元を引き継ぎます
 */
CDrawingParts::CDrawingParts(const  CDrawingParts&  group, bool bRef):
CDrawingScriptBase(group)
{
    m_Pt        = group.m_Pt;
    m_dAngle    = group.m_dAngle;
    m_dSclX     = group.m_dSclX;
    m_dSclY     = group.m_dSclY;
    m_mat2D     = group.m_mat2D;

    m_bUseGroupColor= group.m_bUseGroupColor;
    m_bOperationInheritance = group.m_bOperationInheritance;
    m_bStopGabage = true;
    m_bExecGabage = false;
    m_iChgGroup   = 0;

    if (bRef)
    {
        SetRefParam(&group, CP_DEFAULT);
    }
    else
    {
    }
    m_iTmpConnectionObjId =  group.m_nId;
    //生成後はSetParentShared()を実行する
}

/**
 *  @brief  コピーコンストラクター.
 *  @param  [in] nID 
 *  @retval なし 
 *  @note    ID, 参照設定(m_psRefObject) コントロール
 *           はコピー元を引き継ぎます
 */
/*
CDrawingParts::CDrawingParts(const  CDrawingParts&  group):
CDrawingObject(group)
{
    CDrawingParts::CDrawingParts(group, false);

}
*/

void CDrawingParts::SetParentShared(CDrawingParts* pSrc)
{
    auto sThis = std::dynamic_pointer_cast<CDrawingParts>(shared_from_this());
     _Copy(pSrc, sThis);
}
/**
 *  @brief  デストラクタ.
 *  @param  なし 
 *  @retval なし 
 *  @note
 */
CDrawingParts::~CDrawingParts()
{
    DeleteAll();
    StopGarbageCollection();
    WaitGarbageCollection();
}

bool CDrawingParts::_Copy(CDrawingParts*  pSrc, std::shared_ptr<CDrawingParts>  pDst)
{
    int iId = 0;
    StdString strName;

    //cpy = CP_ID_CREATE;
    for(auto pSrcObj :pSrc->m_lstObjects)
    {
       iId = pSrcObj->GetId();

        bool bCopyNodeData = GetCopyNodeDataMode();
        pSrcObj->SetCopyNodeDataMode(bCopyNodeData);
        auto pDstObj = CDrawingObject::CloneShared(pSrcObj.get());

        pDst->m_lstObjects.push_back(pDstObj);

        pDstObj->SetId(iId);
        pDstObj->SetParentParts(pDst);

        pDst->m_mapObjects[iId] = pDstObj;

        strName = pSrcObj->GetName();
        if (strName != _T(""))
        {
            pDst->m_mapObjectName[strName] = pDstObj;        
        }
       
        if ((pSrcObj->GetType() == DT_PARTS)||
            (pSrcObj->GetType() == DT_FIELD)||
            (pSrcObj->GetType() == DT_GROUP))
    {
            auto  pGroupDst = std::dynamic_pointer_cast<CDrawingParts>(pDstObj);
            auto  pGroupSrc = std::dynamic_pointer_cast<CDrawingParts>(pSrcObj);

            STD_ASSERT(pGroupDst);
            if (pGroupDst)
            {
                pDst->m_lstGroup.insert(pGroupDst);
                _Copy(pGroupSrc.get(), pGroupDst);
            }
       }
       else
       {
            pDstObj->SetRefParam(pSrcObj.get(), CP_DEFAULT);
       }
    }


#if 1

    if(GetCopyNodeDataMode())
    {
        m_lstConnection  =   pSrc->m_lstConnection;
    }

    int iTotal = GetNodeDataNum();
    for(int iNo = 0; iNo < iTotal; ++iNo)
    {
        auto pConnection = GetNodeData(iNo);

        if (!pConnection)
        {
            DB_PRINT(_T("iNo(%d )は見つかりませんでした\n"),iNo);  
            continue;
        }

        for(auto& bind: pConnection->lstSocket)
        {
            if (m_pCtrl)
            {
                auto pObj = m_pCtrl->GetObjectById(bind.iId);

                if (pObj)
                {
                    DB_PRINT(_T("接続更新(%s:%d ) 方向%d 相手(%s:%d)\n"), 
                        GetName().c_str(),
                        GetId(),
                        iNo, 
                        pObj->GetName().c_str(),
                        pObj->GetId()
                        );
                }
                else
                {
                    DB_PRINT(_T("iNo(%d) 接続相手先(%d) は見つかりませんでした\n"),
                                                iNo, iId);  
                }
            }
        }
    }
#endif
    return true;
}


/**
 *  @brief  コピー.
 *  @param  [in] group コピー元オブジェクト
 *  @param  [in] bFirst true 先頭オブジェクト
 *  @retval なし 
 *  @note  グループのコピーを行う
 *　　　　　・　コントロールはこのグループの設定にあわせる
 *　　　　　・　コピー元を参照元に設定する
 *　　　　　・　IDの再割り当て
 *　　　　　グループを追加する場合は
 *　　　　　このグループのコピーを作成した後AddObjectを使用する
 *          SetGroupから使用
 */
bool CDrawingParts::Copy(const  CDrawingParts&  group, bool bFirst)
{
    if (!m_pCtrl)
    {
        return false;
    }

    StopGarbageCollection();
    WaitGarbageCollection();
    
    DeleteAll();   


    int iId;
    StdString strName;

    //既にIDを変更しない場合
    if (!bFirst)
    {
        iId = m_pCtrl->CreateId();
        SetId(iId);
    }

    for(auto pSrcObj: group.m_lstObjects)
    {
       // CDrawingPartsがあった場合、Cloneで最下層までの
       // コピーが作成されるがIDの振り直しが必要となる
       auto pDstObj = CDrawingObject::CloneShared(pSrcObj.get());

       pDstObj->SetPartsDef(m_pCtrl);

       iId = m_pCtrl->CreateId();
       pDstObj->SetId(iId);

       pDstObj->SetParentParts(shared_from_this());

       m_lstObjects.push_back(pDstObj);
       m_mapObjects[iId] = pDstObj;

       strName = pSrcObj->GetName();
       if (strName != _T(""))
       {
            m_mapObjectName[strName] = pDstObj;        
       }
       
       if ((pSrcObj->GetType() == DT_PARTS)||
           (pSrcObj->GetType() == DT_FIELD)||
           (pSrcObj->GetType() == DT_GROUP)
           )
       {
            auto pGroup = std::dynamic_pointer_cast<CDrawingParts>(pDstObj);
            if (pGroup)
            {
                m_lstGroup.insert(pGroup);
                bool bKeepUnfindIdSub = true;
                pGroup->_AssignId(true, bKeepUnfindIdSub);
            }
       }
       else
       {
            pDstObj->SetRefParam(pSrcObj.get(), CP_DEFAULT);
       }

       pSrcObj->GetCopySrcObjectId();

    }
    return true;
}


/**
 *  @brief  回転.
 *  @param  [in]    pPt2D    回転中心(絶対座標)
 *  @param  [in]    dAngle   (絶対)回転角(rad)
 *  @retval     なし
 *  @note
 */
void CDrawingParts::Rotate(const POINT2D& pt2D, double dAngle)
{
    MAT2D matRot;

    matRot.Rotate(pt2D, dAngle);
    Matrix(matRot);
}

/**
 *  @brief  回転.
 *  @param  [in]    pPt2D    回転中心(絶対座標)
 *  @param  [in]    dAngle   (絶対)回転角(rad)
 *  @retval     なし
 *  @note
 */
void CDrawingParts::RotateAbs(double dAngle)
{
    m_mat2D.SetAffineAngle(dAngle);
    MatrixDirect(m_mat2D);
}

/**
 *  @brief  相対移動.
 *  @param  [in]    pPt2D   移動量
 *  @retval         なし
 *  @note
 */
void CDrawingParts::Move(const POINT2D& pt2D) 
{
    MAT2D matMove;

    matMove.Move(pt2D);

    Matrix(matMove);
}

/**
 *  @brief  絶対移動.
 *  @param  [in]    pPt2D   移動量
 *  @retval         なし
 *  @note    
 */
void CDrawingParts::AbsMove(const POINT2D& pt2D) 
{
    m_mat2D.SetAffinePos(pt2D);
}

/**
 *  @brief  内部データ移動.
 *  @param  [in]    pPt2D   移動量
 *  @retval         なし
 *  @note
 */
void CDrawingParts::MoveInnerPosition(const POINT2D& pt2D) 
{
    for(auto pObject: m_lstObjects)
    {
        pObject->Move(pt2D);
    }
}

//!< 角度設定
void CDrawingParts::SetAngle(double dAngle)
{
    RotateAbs(dAngle);
}

/**
 *  @brief  鏡像.
 *  @param  [in]    line   対称軸
 *  @retval         なし
 *  @note
 */
void CDrawingParts::Mirror(const LINE2D& line)
{
    MAT2D matMirror;

    matMirror.Mirror(line);
    Matrix(matMirror);
}

/**
 *  @brief  倍率.
 *  @param  [in]    pPt2D   拡大中心
 *  @param  [in]    dSclX   X倍率
 *  @param  [in]    dSclY   Y倍率
 *  @retval         なし
 *  @note
 */
void CDrawingParts::Scl(const POINT2D& pt2D, double dXScl, double dYScl)
{
    MAT2D matScl;

    matScl.Scl(pt2D, dXScl, dYScl);
    Matrix(matScl);

}

/**
 *  @brief  行列.
 *  @param  [in]    mat2D   適用する行列
 *  @retval         なし
 *  @note
 */
void CDrawingParts::Matrix(const MAT2D& mat2D)
{
    //TODO: MATRIX計算に変更する
    //      描画時にMATRIXを評価するように変更
    //      そうしないと、参照データ変更時の対応が出来ない

    m_mat2D = m_mat2D * mat2D;
    double dT;
    m_mat2D.GetAffineParam(&m_Pt, &m_dAngle, &m_dSclX, &m_dSclY, &dT);

    //描画のタイミングで m_matDrawが更新されない
    //   CActionMove::SetMove から呼び出された場合
    //    UpdateNodeData -> GetNodeDataPos
    //    で呼び出された場合 m_matDrawは更新されていない
    m_matDraw = m_mat2D;

    UpdateNodeSocketData();
    AddChgCnt();

}

void CDrawingParts::MatrixDirect(const MAT2D& mat2D)
{
    m_mat2D = mat2D;
    double dT;
    m_mat2D.GetAffineParam(&m_Pt, &m_dAngle, &m_dSclX, &m_dSclY, &dT);

    UpdateNodeSocketData();
    AddChgCnt();
}

/**
 *  @brief  位置設定.
 *  @param  [in]    ptPos   移動位置(絶対座標)
 *  @retval         なし
 *  @note
 */
void  CDrawingParts::SetPoint(const POINT2D& ptAbs)
{
    POINT2D ptDelta = ptAbs - m_Pt;
    AbsMove(ptAbs);
    m_Pt = ptAbs;
} 

const POINT2D&  CDrawingParts::GetPoint()  const
{
    return m_Pt;
}

//!< 位置取得
void  CDrawingParts::GetPoint2(POINT2D& pos) const
{
    pos = m_Pt;
}



/*
 *  @brief  交点計算.
 *  @param  [out]   pList   交点のリスト
 *  @param  [in]    pObj    交差するオブジェクト
 *  @retval         なし
 *  @note
 */
void CDrawingParts::CalcIntersection ( std::vector<POINT2D>* pList,   
                                const CDrawingObject* pObj, 
                                bool bOnline,
                                double dMin)  const
{
    STD_ASSERT(pList != NULL);
    pList->clear();
}


/**
 *  @brief  位置調整.
 *  @param  [in]    pPtIntersect   調整位置
 *  @param  [in]    pPtClick       調整方向指定点
 *  @retval         なし
 *  @note
 */
bool CDrawingParts::Trim (const POINT2D& ptIntersect, const POINT2D& ptClick, bool bReverse)
{
    //Trimは不可
    return false;
}


/**
 *  @brief  分割.
 *  @param  [in]    pPtBreak  分割位置
 *  @retval         分割によって出来た
 *  @note
 */
std::shared_ptr<CDrawingObject>  CDrawingParts::Break (const POINT2D& ptBreak)
{
    //分割不可
    return NULL;
}

/**
 *  @brief  描画.
 *  @param  [in]    pView      表示View
 *  @retval         なし
 *  @note   実行インスタンスの表示用
 */
void CDrawingParts::DrawView(CDrawingView* pView) const
{
    STD_ASSERT(m_pCtrl);
    std::vector< DRAW_OBJ_LIST >  lstDisply;

    bool bClearFront = true;
    pView->ClearDisp(bClearFront);

    //if (m_iCntDraw != GetChgGroupCnt())
    {
        int iLayerMax = m_pCtrl->GetLayerMax();
        lstDisply.resize(iLayerMax);

        int iLayer;
        const CLayer* pLayer;
        for(auto pObject: m_lstObjects)
        {
            iLayer = pObject->GetLayer();
            pLayer = m_pCtrl->GetLayer(iLayer);
            if (!pLayer->bVisible)
            {
                continue;
            }

            if (pObject->GetType() != DT_POINT)
            {
                lstDisply.at(iLayer).push_back(pObject);
            }
        }

        //SetDrawData(&m_layerDraw, &m_lstPoint, &m_lstDrawingScriptBase);
        //m_iCntDraw = GetChgGroupCnt();
    }

    for(auto& lstObj: lstDisply)
    {
        for(auto wObject: lstObj)
        {
            auto pObject = wObject.lock();
            if(pObject)
            {
                pObject->Draw(pView, NULL);
            }
        }
    }
}



/**
 *  @brief  描画.
 *  @param  [in]  pView       表示View
 *  @param  [in]  bMouseOver  true オブジェクト上にマウスあり
 *  @retval         なし
 *  @note
 */
void CDrawingParts::Draw(CDrawingView* pView, 
                         const std::shared_ptr<CDrawingObject> pParentObject,
                         E_MOUSE_OVER_TYPE eMouseOver,
                        int iLayerId)
{
    //======================
    // 変換マトリックス生成
    //======================
    if (pParentObject)
    {
        const MAT2D* pMat = pParentObject->GetDrawingMatrix();
        if (pMat)
        {
            m_matDraw = m_mat2D * (*pMat);
        }
        else
        {
            m_matDraw = m_mat2D;
        }
    }
    else
    {
        m_matDraw = m_mat2D;
    }

    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }

    if (m_pMatOffset)
    {
        m_matDraw = m_matDraw * (*m_pMatOffset);
    }

    _SetDrawingParent(pParentObject);

    CScriptObject* pScriptObject;
    pScriptObject = GetScriptObject();
    if (pScriptObject)
    {
        pScriptObject->DrawScript(pView);
    }

if (pView->GetDebugMode() != CDrawingView::DM_DISABLE)
{
    PrintObj( _T("A "));
}

    //======================
    for(auto pObj: m_lstObjects)
    {
        pObj->_SetDrawingParent(shared_from_this());
        pObj->Draw(pView, shared_from_this(), eMouseOver, iLayerId);

if(pView->GetDebugMode() == CDrawingView::DM_VIEW)
{
    DRAWING_TYPE eType = pObj->GetType();
    if ((eType != DT_PARTS) &&
        (eType != DT_GROUP) &&
        (eType != DT_REFERENCE))
    {
        pObj->PrintObj( _T("B    "));
    }
}
    }
}

/**
 *  @brief  描画削除.
 *  @param  [in]    pView   表示View
 *  @retval         なし
 *  @note
 */
void CDrawingParts::DeleteView(CDrawingView* pView, 
                               const std::shared_ptr<CDrawingObject> pParentObject,
                               int iLayerId)
{
    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }

    for(auto pObj: m_lstObjects)
    {
        pObj->DeleteView(pView, pParentObject, iLayerId);
    }
}

/**
 *  @brief  リフレクション設定.
 *  @param  なし
 *  @retval なし
 *  @note
 */
void CDrawingParts::RegisterReflection()
{
    /*
	//RegisterProperty<POINT2D>       ( _T("Point1")   , &GetPoint1     , &SetPoint1 );
	//RegisterProperty<POINT2D>       ( _T("Point2")   , &GetPoint2     , &SetPoint2 );
	//RegisterProperty<int>           ( _T("Width")       , &GetWidth      , &SetWidth );
	//RegisterProperty<int>           ( _T("LineType")    , &GetLineType   , &SetLineType );

	RegisterProperty<POINT2D>       ( "Point1"   , &GetPoint1     , &SetPoint1 );
	RegisterProperty<POINT2D>       ( "Point2"   , &GetPoint2     , &SetPoint2 );
	RegisterProperty<int>           ( "Width"       , &GetWidth      , &SetWidth );
	RegisterProperty<int>           ( "LineType"    , &GetLineType   , &SetLineType );
    */
}

/**
 *  @brief  範囲.
 *  @param  [in] p1 1点目
 *  @param  [in] p2 2点目
 *  @retval  true:2点で示される矩形内に点が存在する 
 *  @note
 */
bool CDrawingParts::IsInner( const RECT2D& rcArea, bool bPart) const
{
    for(auto pObj: m_lstObjects)
    {
        if (pObj->IsInner(rcArea, bPart))
        {
            return true;
        }
    }
    return false;
}

/**
 * @brief 線描画(マウスオーバー時)
 * @param  [in]    pView 表示ビュー  
 * @param  [in] bOver true:開始 false:終了
 * @retval  なし
 * @note	
 */
void CDrawingParts::DrawOver(CDrawingView* pView, bool bOver)
{

}

/**
 *  @brief   生成処理
 *  @param   [in] pIoAccess  
 *  @param   [in] pDef  
 *  @retval  なし
 *  @note    
 */
bool CDrawingParts::Create(std::shared_ptr<CIoAccess> pIoAccess,
                           CObjectDef* pDef)
{
    bool bRet;
    bRet = CDrawingScriptBase::Create(pIoAccess, pDef);

    if (!bRet)
    {
        return false;
    }

    if (pDef)
    {
        CPartsDef* pParts = dynamic_cast<CPartsDef*>(pDef);
        SetGroup( pParts );
    }
    else
    {
        SetGroup( NULL );
    }
    return true;
}

/**
 * @brief   グループ設定
 * @param   [in]  pCtrlRef  参照データ(部品)
 * @param   [in]  bRoot     点描画の有無
 * @retval  なし
 * @note    位置，角度は先に設定しておく
 *　　　　　主に新たに生成したグループに対して使用する　
 */
void CDrawingParts::SetGroup( const CPartsDef* pCtrlRef,  bool bRoot)
{
    STD_ASSERT(m_pCtrl != NULL);

    if (!m_pCtrl)
    {
        return;
    }

    if (m_pCtrl->IsIncludeParts2( pCtrlRef ))
    {
        //循環参照が発生します
        throw MockException(e_cycle_ref);
        return;
    }

    if (bRoot)
    {
        //ルートとなるグループを作成
        if (m_psRefObject != NULL)
        {
            delete m_psRefObject;
            m_psRefObject = NULL;
        }
        return;
    }
    else
    {
        if (m_pCtrl == pCtrlRef)
        {
            throw MockException(e_cycle_ref);
            return;
        }
    }

    if (pCtrlRef)
    {
        if (m_psRefObject == NULL)
        {
            m_psRefObject = new QUERY_REF_OBJECT;
        }

        m_psRefObject->uuidParts  =  pCtrlRef->GetPartsId();
        m_psRefObject->iObjectId  =  -1; //グループを取得
    }


    CDrawingObject* pTmpObj;
    CDrawingParts* pRefGroup = NULL; 
    try
    {
        pTmpObj = GetRefObj();

        STD_ASSERT(pTmpObj);
        if (pTmpObj == NULL)
        {
            throw MockException(e_create);
        }
    }
    catch(MockException &e)
    {
        e;
        delete m_psRefObject;
        m_psRefObject = NULL;
        return;
    }

    try
    {
        pRefGroup = dynamic_cast<CDrawingParts*>(pTmpObj);

        STD_ASSERT(pRefGroup);
        if (pRefGroup == NULL)
        {
            throw MockException(e_create);
        }
    }
    catch(MockException &e)
    {
        e;
        delete m_psRefObject;
        m_psRefObject = NULL;
        return;
    }

    Copy(*pRefGroup);
}

/**
 *  @brief   参照元変更の有無
 *  @param   なし
 *  @retval  true 変更あり
 *  @note    参照していない場合は常にfalse
 */
bool CDrawingParts::IsChangeRef()
{
    if (m_psRefObject != NULL)
    {
        //参照元の変更を確認
        if ( GetRefObj()->GetChgCnt() != GetChgCnt())
        {
            return true;
        }

        for(auto pObj: m_lstObjects)
        {
            if (pObj->IsChangeRef())
            {
                return true;
            }
        }
    }
    else
    {
        for(auto pGroup: m_lstGroup)
        {
            if (pGroup->IsChangeRef())
            {
                return true;
            }
        }
    }

    return false;
}


/**
 *  @brief  データ更新.
 *  @param  
 *  @retval false 
 *  @note
 */
/*
bool CDrawingParts::UpdateGroup()
{
    bool bRet = false;

    if (!IsChangeRef())
    {
        return false;
    }

    CDrawingObject* pRefObj;
    pRefObj = GetRefObj();

    if (pRefObj == NULL)
    {
        return false;
    }

    if (pRefObj->GetChgCnt() == GetChgCnt())
    {
        return false;
    }

    //グループプロパティの更新
    //TODO:実装
    return bRet;
}
*/

/**
 * @brief   グループ内データ更新カウント
 * @param   なし
 * @retval  なし
 * @note    
 */
void CDrawingParts::AddGroupChg()
{
    m_iChgGroup++;
    if (m_iChgGroup == INT_MAX)
    {
        m_iChgGroup = 0;
    }

    auto pParent = m_pParent.lock();
    if (pParent)
    {
        auto pGroup = std::dynamic_pointer_cast<CDrawingParts>(pParent);

        if (pGroup)
        {
            pGroup->AddGroupChg();
        }
    }
}


/**
 * @brief   部品使用箇所削除
 * @param   [in]  pCtrl  表示に使用しているオブジェクトコントロール
 * @retval  なし
 * @note    
 */
bool CDrawingParts::DeleteUsingParts(const CPartsDef* pCtrl)
{
    //要テスト:
    STD_ASSERT(pCtrl != NULL);

    CDrawingObject* pRefObject;

    std::set<int> lstId;
    for(auto pObject: m_lstObjects)
    {
        if (!pObject)
        {
            continue;
        }

        pRefObject = pObject->GetRefObj();
        if (pRefObject == NULL)
        {
            continue;
        }

        if( pRefObject->GetPartsDef() != pCtrl)
        {
            continue;
        }

        lstId.insert(pObject->GetId());

        if ((pObject->GetType() == DT_PARTS)||
            (pObject->GetType() == DT_FIELD))
        {
            auto pGroup = std::dynamic_pointer_cast<CDrawingParts>(pObject);

            STD_ASSERT(pGroup);
            if (pGroup)
            {
                pGroup->DeleteUsingParts( pCtrl);
            }
        }
    }

    for(int iId: lstId)
    {
        Delete(iId);
    }

    if (lstId.size() == 0)
    {
        return false;
    }   

    return true;
}

/**
 * @brief   点リスト取得
 * @param   [out]  pLstPoint  点リスト
 * @retval  なし
 * @note    中心点は無条件で含まれる
 */
void  CDrawingParts::GetPointList(std::vector< POINT2D >* pLstPoint) const
{
    //TODO:実装
    STD_ASSERT(pLstPoint != NULL);
    //中心点
   // pLstPoint->push_back(m_Pt);


    std::shared_ptr<CDrawingPoint>  pPoint;
    POINT2D         ptPos;
    for(auto pObj: m_lstObjects)
    {
        if (pObj->GetType() != DT_POINT)
        {
            continue;
        }

        pPoint = std::dynamic_pointer_cast<CDrawingPoint>(pObj);
        ptPos = *pPoint->GetPointInstance();

        ptPos.Move(m_Pt);
        ptPos.Rotate(m_Pt, m_dAngle);
        pLstPoint->push_back(ptPos);
    }
}

/**
 *  @brief   プロパティ初期化.
 *  @param   [in] pCtrl オブジェクトコントロールへのポインタ
 *  @retval     なし
 *  @note
 */
TREE_GRID_ITEM*  CDrawingParts::_CreateStdPropertyTree()
{
    NAME_TREE_ITEM<CStdPropertyItem>* pBeforeGroup;
    NAME_TREE_ITEM<CStdPropertyItem>* pNextItem;
    NAME_TREE_ITEM<CStdPropertyItem>* pGroupItem;
    NAME_TREE<CStdPropertyItem>* pTree;
    //int iId;
    CStdPropertyItem* pItem;
    boost::tuple< CStdPropertyItem*, void*> pairVal;


    pBeforeGroup = CDrawingScriptBase::_CreateStdPropertyTree();
    pTree = m_psProperty->GetTreeInstance();

    pGroupItem  = pTree->AddNext(pBeforeGroup, NULL, GET_STR(STR_PRO_PARTS), _T("Parts"));

    //------------
    //基準点
    //------------
    CStdPropertyItemDef DefCenter(PROP_POINT2D, 
        GET_STR(STR_PRO_DATUM)   , 
        _T("Center"),
        GET_STR(STR_PRO_INFO_DATUM), 
        true,
        DISP_UNIT,
        POINT2D());

    pItem = new CStdPropertyItem( DefCenter, PropPos, m_psProperty, NULL, &m_Pt);
    pNextItem = pTree->AddChild(pGroupItem, pItem, DefCenter.strDspName, pItem->GetName(), 2);

    //------------
    //角度(deg)
    //------------
    CStdPropertyItemDef DefAngle(PROP_DOUBLE, 
        GET_STR(STR_PRO_ANGLE)   ,
        _T("Angle"),
        GET_STR(STR_PRO_INFO_ANGLE), 
        true,
        DISP_UNIT,
        0.0);

    pItem = new CStdPropertyItem( DefAngle, PropAngle, m_psProperty, NULL, &m_dAngle);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefAngle.strDspName, pItem->GetName());

    //------------
    //固有色
    //------------
    CStdPropertyItemDef DefUseColor(PROP_BOOL, 
        GET_STR(STR_PRO_USE_COLOR)   , 
        _T("UseColor"),
        GET_STR(STR_PRO_INFO_USE_COLOR), 
        false,
        DISP_NONE,
        true);

    pItem = new CStdPropertyItem( DefUseColor, PropUseColor, m_psProperty, NULL, &m_bUseGroupColor);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefUseColor.strDspName, pItem->GetName());

    //------------
    // 操作継承
    //------------
    CStdPropertyItemDef DefOperationInheritance(PROP_BOOL, 
        GET_STR(STR_PRO_OPERATION_INHERITANCE)   , 
        _T("OperationInheritance"),
        GET_STR(STR_PRO_INFO_OPERATION_INHERITANCE), 
        false,
        DISP_NONE,
        true);

    pItem = new CStdPropertyItem( DefOperationInheritance, PropOperationInheritance, m_psProperty, NULL, &m_bOperationInheritance);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefOperationInheritance.strDspName, pItem->GetName());

    //------------
    //
    //------------
    /*
    CStdPropertyItemDef DefExecGrpupId(PROP_INT_RANGE, 
        GET_STR(STR_PRO_EXEC_GROUP_ID)   , 
        GET_STR(STR_PRO_INFO_EXEC_GROUP_ID), 
        true,
        DISP_NONE,
        0, 0, SYS_CONFIG->iMaxExecGroup - 1);

    pItem = new CStdPropertyItem(_T("ExecGroupId"), DefExecGrpupId, PropExecGroupId, m_psProperty, NULL);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefExecGrpupId.strDspName, pItem->GetName());
    */

    //!<ユーザプロパティ設定
    //SetUserProperty();

    return pGroupItem;
}


/**
 *  @brief   プロパティ変更(中心点)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingParts::PropPos   (CStdPropertyItem* pData, void* pObj)
{
    CDrawingParts* pDrawing = reinterpret_cast<CDrawingParts*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->SetPoint(pData->anyData.GetPoint());
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(角度)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingParts::PropAngle   (CStdPropertyItem* pData, void* pObj)
{
    CDrawingParts* pDrawing = reinterpret_cast<CDrawingParts*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->SetAngle(pData->anyData.GetDouble());
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(色仕様)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingParts::PropUseColor   (CStdPropertyItem* pData, void* pObj)
{
    CDrawingParts* pDrawing = reinterpret_cast<CDrawingParts*>(pObj);
    try
    {
        //pDrawing->m_pCtrlRef->HideObject(pDrawing->m_nId);
        //pDrawing->m_pCtrlRef->Draw(pDrawing->m_nId);
        pDrawing->AddChgCnt();
        pDrawing->SetUseGroupColor(pData->anyData.GetBool());

    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}


/**
 *  @brief   プロパティ変更(実行グループID)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
// PropThreadTypeに変更
/*
bool CDrawingParts::PropExecGroupId   (CStdPropertyItem* pData, void* pObj)
{
    CDrawingParts* pDrawing = reinterpret_cast<CDrawingParts*>(pObj);
    try
    {
        //pDrawing->m_pCtrlRef->HideObject(pDrawing->m_nId);
        //pDrawing->m_pCtrlRef->Draw(pDrawing->m_nId);
        pDrawing->AddChgCnt();
        //pDrawing->SetExecGroupId(pData->anyData.GetInt());

    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}
*/

/**
 *  @brief   プロパティ変更(操作継承)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingParts::PropOperationInheritance(CStdPropertyItem* pData, void* pObj)
{
    CDrawingParts* pDrawing = reinterpret_cast<CDrawingParts*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->SetOperationInheritance(pData->anyData.GetBool());
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   有効判定
 *  @param   なし
 *  @retval  false 無効
 *  @note    参照元のデータが削除されていた場合
 *           このグループは無効となる
 */
bool CDrawingParts::IsValid() const
{
    bool bRet = false;
    if ( GetRefObj() == NULL)
    {
        return false;
    }
    return true;
}

/**
 *  @brief   コントロール設定.
 *  @param   [in] pCtrl オブジェクトコントロールへのポインタ
 *  @retval  なし
 *  @note    通常コントロールを設定しなおし,IDも再設定する
 */
void CDrawingParts::SetPartsDef(CPartsDef* pCtrl)
{
    CDrawingObject::SetPartsDef(pCtrl);

    if (m_pCtrl == NULL)
    {
        return;
    }

    for(auto pObj: m_lstObjects)
    {
        pObj->SetPartsDef(pCtrl);
        pObj->SetParentParts(shared_from_this());
    }
}

/**
 *  @brief   ID割り当て.
 *  @param   なし
 *  @retval  設定したID値
 *  @note    
 */
int CDrawingParts::_AssignId(bool bFirstSet, bool bKeepUnfindId)
{
    if(m_pCtrl == NULL)
    {
        return -1;
    }

    int iRetId = 0;
    int iId;


    // AddObjectで追加する際、自分自身のIDを２度生成してしまう
    // ことになるため、IDを生成しない処理を追加
    if (bFirstSet)
    {
        iRetId = m_pCtrl->CreateId();
        SetId(iRetId);
    }

    std::map<int, std::weak_ptr<CDrawingObject>> tmpMap;
    std::map<StdString , std::weak_ptr<CDrawingObject>>    tmpNameMap;

    StdString strName;

    m_mapObjects.clear();
    m_mapObjectName.clear();

    bool bNotLock = false;
    /*
    if (!m_pCtrl->IsLockUpdateNodeData())
    {
        m_pCtrl->SetLockUpdateNodeData(true);
        bNotLock = true;
    }
    */

    std::map<int, int> mapConvId;
    std::vector<std::shared_ptr<CDrawingObject>> lstConnection;
    mapConvId[-1] = -1;
    int iOldId;

    for(auto pObj: m_lstObjects)
    {
        iOldId = pObj->GetId();
        iId = pObj->AssignId(bKeepUnfindId);
        m_mapObjects[iId] = pObj;

        strName = pObj->GetName();
        if (strName != _T(""))
        {
            m_mapObjectName[strName] = pObj;
        }

        mapConvId[iOldId] = iId;
        if (pObj->GetNodeDataNum() > 0)
        {
            lstConnection.push_back(pObj);
        }

        if ((pObj->GetType() == DT_PARTS)||
            (pObj->GetType() == DT_FIELD)||
            (pObj->GetType() == DT_GROUP)
            )
        {
            bool bKeepUnfindIdSub = true;
            auto pGroup = std::dynamic_pointer_cast<CDrawingParts>(pObj);
            if (pGroup == NULL)
            {
                pGroup->_AssignId(false, bKeepUnfindIdSub);
            }
        }
    }

    //  接続ID更新
    m_pCtrl->UpdateConnection(&mapConvId, &lstConnection, bKeepUnfindId);

    /*
    if (bNotLock)
    {
        m_pCtrl->SetLockUpdateNodeData(false);
    }
    */
    UpdateNodeSocketData(NULL, m_pCtrl);

    return iRetId;
}

/**
 *  @brief   ユーザープロパティ値更新.
 *  @param   [in] pGroup
 *  @retval  なし
 *  @note    
 */
void CDrawingParts::UpdateAllUserPropertyValue(CDrawingParts* pGroup)
{
    if (pGroup == NULL)
    {
        pGroup = this;
    }

    for(auto pObj: pGroup->m_lstObjects)
    {
        if ( pObj->GetType() & DT_SCRIPTBASE)
        {
            auto pScriptBase = std::dynamic_pointer_cast<CDrawingScriptBase>(pObj);
            STD_ASSERT(pScriptBase);
            pScriptBase->InitUserPropertyValue(false);

            if ((pObj->GetType() == DT_PARTS)||
                (pObj->GetType() == DT_REFERENCE)||
                (pObj->GetType() == DT_FIELD))
            {
                auto pGroupNext = std::dynamic_pointer_cast<CDrawingParts>(pObj);
                STD_ASSERT(pGroupNext);
                UpdateAllUserPropertyValue(pGroupNext.get());
            }
        }
    }
}

/**
 *  @brief   ID割り当て.
 *  @param   なし
 *  @retval  設定したID値
 *  @note    
 */
int CDrawingParts::AssignId(bool bKeepUnfindId)
{
    return _AssignId(true, bKeepUnfindId);
}


//
bool CDrawingParts::CheckConfrictName(StdString* pNewName,
                                      std::map<StdString, std::weak_ptr<CDrawingObject>>* pMap,
                                      const StdString& strName)
{

    StdString strBase;
    int iDigit;
    int iVal;

    if (!CUtil::DevideLastNum(&strBase, &iDigit, &iVal, strName))
    {
        strBase = strName;
    }

    bool bRet = false;
    StdString strRegex = strBase+_T("_*([0-9]*)");
    //StdString strRegex = _T("(\w*)");
    StdStrXregex reStr = StdStrXregex::compile(strRegex);
    StdStrXMatchResult matchResult;

    int iMax = 0;
    int iMinDigit = INT_MAX;

    bool bMatch = false;
    for (auto ite = pMap->begin();
        ite != pMap->end();
        ite++)
    {
        if (boost::xpressive::regex_match(ite->first, matchResult, reStr))
        {
            auto sVal = matchResult.str(1);
            iVal = _tstoi(sVal.c_str());
            iDigit = SizeToInt(sVal.length());
            bMatch = true;

            if (iMax < iVal)
            {
                iMax = iVal;
            }

            if (iMinDigit > iDigit)
            {
                iMinDigit = iDigit;
            }
            bMatch = true;
        }
        else
        {
            if (bMatch)
            {
                break;
            }
        }
    }

    if (bMatch)
    {
        StdStringStream strmResult;
        strmResult << strBase;
        if (iDigit > 0)
        {
            strmResult.width(iMinDigit);
            strmResult.fill(_T('0'));
        }
        strmResult << iMax + 1;
        *pNewName = strmResult.str();
        bRet = true;
    }
    return bRet;
}


/*
StdString CheckConfrictName(std::map<StdString, std::weak_ptr<CDrawingObject>>* pMap,
    const StdString& str)
{
    int iVal;
    bool bRet;
    int iDigit;
    StdString strBase;
    bRet = DevideLastNum(str, &strBase, &iDigit, &iVal);

    if (bRet)
    {
        StdString strRegex = strBase + _T("([0-9]*)");
        StdStrXregex reStr = StdStrXregex::compile(strRegex);
        StdStrXMatchResult matchResult;

        for (auto ite = pMap->begin();
            ite != pMap->end();
            ite++)
        {
            if (boost::xpressive::regex_match(ite->first, matchResult, reStr))
            {
                if (strLine == strBase)
                {
                    bMatch = true;
                    continue;
                }
                else if (matchResult.size() != 3) { continue; }
                strVal = matchResult.str(2);
                iVal = _tstoi(strVal.c_str());


            }
        }

    }

    strVal = matchResult.str(2);
    iVal = _tstoi(strVal.c_str());


}
*/




/**
 *  @brief   データ追加
 *  @param   pObject 描画オブジェクト
 *  @retval  true 成功 false 失敗
 *  @note   内部でShared_ptrを作成する
 */
bool CDrawingParts::AddData(std::shared_ptr<CDrawingObject> pObject, 
                            bool bCreateId, 
                            bool bKeepUnfindId,
                            bool bKeepSelect)
{
    STD_ASSERT(m_pCtrl);
    if (m_pCtrl == NULL)
    {
        return false;
    }

    pObject->SetPartsDef(m_pCtrl);

    int iId;
    StdString strName;
    strName = pObject->GetName();

    //同じ名称は存在しないはず
    auto iteMap = m_mapObjectName.find(strName);
    if (strName != _T(""))
    {
        if (iteMap != m_mapObjectName.end())
        {
            //名称をつけ直す
            bool bRet;
            bRet = CheckConfrictName(&strName, &m_mapObjectName, strName);
            STD_ASSERT(bRet);
            pObject->SetNameDirect(strName.c_str());
        }
    }

    if (bCreateId)
    {
        //CDrawingPartsの場合は内部のIDの振り直しも行われる
        iId = pObject->AssignId(bKeepUnfindId);
    }
    else
    {
        iId = pObject->GetId();
    }

    //同じID称は存在しないはず
    auto iteMapId = m_mapObjects.find(iId);
    STD_ASSERT(iteMapId == m_mapObjects.end());

    pObject->SetId(iId);

    if (!bKeepSelect)
    {
        pObject->SetSelect(false);
    }

    m_lstObjects.push_back(pObject);
    m_mapObjects[iId] = pObject;

    if (strName != _T(""))
    {
        m_mapObjectName[strName] = pObject;
    }

    pObject->SetParentParts(shared_from_this());

    if ((pObject->GetType() == DT_PARTS)||
        (pObject->GetType() == DT_FIELD)||
        (pObject->GetType() == DT_GROUP)
        )
    {
        auto pGroup = std::dynamic_pointer_cast<CDrawingParts>(pObject);
        STD_ASSERT(pGroup);
        if (pGroup)
        {
            m_lstGroup.insert(pGroup);
        }
    }


    AddGroupChg();
    return true;
}

/**
 *  @brief  オブジェクト(グループ)再設定
 *  @param  なし
 *  @retval なし
 *  @note    Load後に再設定する
 */
void CDrawingParts::ResetParent()
{
    auto pThis = std::dynamic_pointer_cast<CDrawingParts>(shared_from_this());
    for(auto pObj: m_lstObjects)
    {
        pObj->SetParentParts(pThis);

        if ((pObj->GetType() == DT_PARTS)||
            (pObj->GetType() == DT_FIELD)||
            (pObj->GetType() == DT_GROUP)||
            (pObj->GetType() == DT_REFERENCE)
            )
        {
            auto pParts = std::dynamic_pointer_cast<CDrawingParts>(pObj);
            STD_ASSERT(pParts);

            if (pParts)
            {
                pParts->ResetParent();
            }
        }
    }
}

/**
 *  @brief  IDによる名称取得
 *  @param  [in] iId ID
 *  @retval 名称
 *  @note
 */
StdString  CDrawingParts::GetNameById(int iId)
{
    StdString strRet;
    auto  pObject = Find(iId);
    
    if (pObject)
    {
        strRet = pObject->GetName();
    }

    return strRet;
}

/**
 *  @brief  IDによる名称設定
 *  @param  [in] iId     ID
 *  @param  [in] strName 名称
 *  @retval true 成功
 *  @note
 */
bool  CDrawingParts::SetIdName(int iId,  LPCTSTR strName)
{

    auto iteMap = m_mapObjects.find(iId);
    if (iteMap == m_mapObjects.end())
    {
        for(auto pGroupObj: m_lstGroup)
        {
            if(pGroupObj->SetIdName(iId, strName))
            {
                return true;
            }
        }
    }
    else
    {
        auto pObj = iteMap->second.lock();
        if(pObj)
        {
            //元の名称を削除
            StdString strOldName;
            strOldName = pObj->GetName();

            auto iteStrMap = m_mapObjectName.find(strOldName);
            if (iteStrMap != m_mapObjectName.end())
            {
                m_mapObjectName.erase(iteStrMap);
            }

            //新たに名称を設定
            StdString strTmpName;
            strTmpName = strName;
            m_mapObjectName[strTmpName] = pObj;
            return true;
        }
    }
    return false;
}

/**
 *  @brief  削除
 *  @param  [in] iID  オブジェクトID
 *  @param  [in] bHandover  現状使用しない
 *  @retval なし
 *  @note
 */
bool  CDrawingParts::Delete(int iId, bool bHandover)
{
    auto iteMap = m_mapObjects.find(iId);
    if (iteMap == m_mapObjects.end())
    {
        for(auto pGroup: m_lstGroup)
        {
            if(pGroup->Delete(iId, bHandover))
            {
                AddGroupChg();
                return true;
            }
        }
    }
    else
    {
        StdString strName;
        auto pObj = iteMap->second.lock();
        

        strName = pObj->GetName();
        auto iteNameMap = m_mapObjectName.find(strName);
        if (iteNameMap != m_mapObjectName.end())
        {
            m_mapObjectName.erase(iteNameMap);
        }

        //TO_DO:動作確認

        CDrawingParts* pGroup = dynamic_cast<CDrawingParts*>(pObj.get());
        if (pGroup)
        {
            auto iteGroup = std::find_if(m_lstGroup.begin(),
                                    m_lstGroup.end(),
                                     [=]  ( std::weak_ptr<CDrawingParts>  wParts)
                                            {
                                                auto pParts = wParts.lock();
                                                if( pParts)
                                                {
                                                    return (pParts.get() == pGroup);
                                                }
                                                else
                                                {
                                                    return false;
                                                }
                                            });

            if (iteGroup != m_lstGroup.end())
            {
                m_lstGroup.erase(iteGroup);
            }
        }

        m_mapObjects.erase(iteMap);


        auto ite = std::find_if(m_lstObjects.begin(), 
                             m_lstObjects.end(),
                             [=]  ( std::shared_ptr<CDrawingObject>  sObj)
                                    {
                                        return (sObj->GetId() == iId);
                                    });
        if (ite !=   m_lstObjects.end())
        {
            m_lstObjects.erase(ite);
        }

        AddGroupChg();
        return true;
    }
    return false;
}

/**
 *  @brief  全削除
 *  @param  なし
 *  @retval なし
 *  @note
 */
bool  CDrawingParts::DeleteAll()
{
    m_mapObjects.clear();
    m_mapObjectName.clear();
    m_lstGroup.clear();
    m_lstObjects.clear();

    AddGroupChg();
    return false;
}

/**
 *  @brief  ガベージコレクション終了
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CDrawingParts::StopGarbageCollection()
{
    m_bStopGabage = true;
    for(auto pGroup: m_lstGroup)
    {
        pGroup->m_bStopGabage = true;
        
    }
}

/**
 *  @brief  ガベージコレクション待ち
 *  @param  なし
 *  @retval なし
 *  @note
 */
void CDrawingParts::WaitGarbageCollection()
{
    int iEndCnt = 100;
    int iCnt;
    for(iCnt = 0; iCnt < iEndCnt; iCnt++)
    {
        if(!m_bExecGabage)
        {
            break;
        }
        StdSleep(10);
    }

    STD_ASSERT(iCnt != iEndCnt);

    for(auto pObj: m_lstGroup)
    {
        pObj->WaitGarbageCollection();
    }
}

/**
 *  @brief  IDガベージコレクション
 *  @param  [in] bOneShot 一つのみ検出
 *  @retval true 回収あり
 *  @note   使用していないIDを回収する
 */
bool CDrawingParts::GarbageCollection(bool bOneShot)
{
    bool bRet = false;
    int iId;
    int iExpId = 1;  // 期待するID値
    int iCnt;

    m_bExecGabage = true;
    m_bStopGabage = false;

    for(auto pObj: m_lstObjects)
    {
        if (m_bStopGabage)
        {
            break;
        }

        iId = pObj->GetId();
        if (iId >  iExpId)
        {
            for(iCnt = iExpId; iCnt != iId; iCnt++)
            {
                if (m_bStopGabage)
                {
                    break;
                }

                m_pCtrl->ReUseId(iCnt);
                bRet = true;
                if (bOneShot)
                {
                    return bRet;
                }
            }
        }
    }

    for(auto pGroup: m_lstGroup)
    {
        if (m_bStopGabage)
        {
            break;
        }

        if(pGroup->GarbageCollection(bOneShot))
        {
            bRet = true;
        }
    }
    m_bExecGabage = false;
    return bRet;
}

/**
 *  @brief  IDマップ生成
 *  @param  なし
 *  @retval なし
 *  @note
 */
void  CDrawingParts::CreateIdMap()
{
    m_mapObjectName.clear();
    m_lstGroup.clear();
    m_mapObjects.clear();

    StdString strName;

    int iId;
    for(auto pObject: m_lstObjects)
    {
        iId     = pObject->GetId();

        m_mapObjects[iId] = pObject;


        strName = pObject->GetName();
        if (strName != _T(""))
        {
            m_mapObjectName[strName] = pObject;
        }

        if ((pObject->GetType() == DT_PARTS)||
            (pObject->GetType() == DT_FIELD)||
            (pObject->GetType() == DT_GROUP)
            )
        {
            auto pGroup = std::dynamic_pointer_cast<CDrawingParts>(pObject);
            STD_ASSERT(pGroup);
            if (pGroup)
            {
                m_lstGroup.insert(pGroup);
            }
        }
    }

    for(auto pGroup: m_lstGroup)
    {
        pGroup->CreateIdMap();
    }
}

/**
 *  @brief  入れ替え
 *  @param  [in] iId   元データID
 *  @param  [in] pData       入替データ
 *  @retval true:成功
 *  @note   元のデータは削除される
 */
bool CDrawingParts::Replace(int iId, std::shared_ptr<CDrawingObject> pObj)
{
    //要テスト:
    auto wObject = FindWeak(iId); 
    auto pObject =  wObject.lock();
    if (!pObject)
    {
        return false;
    }

    STD_ASSERT(m_pCtrl);
    if (m_pCtrl == NULL)
    {
        return false;
    }

    iId = pObject->GetId();
    StdString strName = pObject->GetName();

    Delete(iId);

    m_lstObjects.push_back(pObj);
    pObj->SetPartsDef(m_pCtrl);
    m_mapObjects[iId] = pObj;


    if (strName != _T(""))
    {
        m_mapObjectName[strName] = pObj;
    }

    if ((pObj->GetType() == DT_PARTS)||
        (pObj->GetType() == DT_FIELD)||
        (pObj->GetType() == DT_GROUP)
        )
    {
        auto pGroup = std::dynamic_pointer_cast<CDrawingParts>(pObj);
        STD_ASSERT(pGroup);
        if (pGroup)
        {
            m_lstGroup.insert(pGroup);
        }
    }

    AddGroupChg();
    return true;
}

/**
 *  @brief  検索
 *  @param  [in] iId  データID
 *  @retval 描画オブジェクト
 *  @note
 */
std::weak_ptr<CDrawingObject>  CDrawingParts::FindWeak(int iId) const
{
    std::weak_ptr<CDrawingObject> pRet;
    auto iteMap = m_mapObjects.find(iId);
    if (iteMap == m_mapObjects.end())
    {
        for(auto pGroup: m_lstGroup)
        {
            auto wRet = pGroup->FindWeak(iId);

            auto sRet = wRet.lock();
            if (sRet)
            {
                return sRet;
            }
        }
    }
    else
    {
        return  iteMap->second;
    }
    return pRet;
}

std::shared_ptr<CDrawingObject>  CDrawingParts::Find(int iId) const
{
    auto pObj = FindWeak(iId).lock();
    if(!pObj)
    {
        return  NULL;
    }
    return pObj;
}

/**
 *  @brief  検索 (先頭ピリオドまで）
 *  @param  [in] strName  データ名
 *  @retval 描画オブジェクト
 *  @note   名称をピリオドまで読み込み、その名称のオブジェクトを
 *          返す
 */
std::shared_ptr<CDrawingObject>  CDrawingParts::FindFirst(StdString strName) const
{
    bool bRet;
    StdString strObjName;
    StdString strFirstObjName;
    strObjName = strName;


    bRet = CUtil::SeparateComma(strObjName, &strFirstObjName);

    std::map<StdString, std::weak_ptr<CDrawingObject>>::const_iterator iteMap;

    if (bRet)
    {
        iteMap = m_mapObjectName.find(strFirstObjName);
    }
    else
    {
        iteMap = m_mapObjectName.find(strName);
    }

    if (iteMap != m_mapObjectName.end())
    {
        auto wObj = iteMap->second.lock(); 

        if (!bRet)
        {
            return wObj;
        }


        if ((wObj->GetType() != DT_PARTS)&&
            (wObj->GetType() != DT_FIELD))
        {
            return wObj;
        }

        auto pGroup = std::dynamic_pointer_cast<CDrawingParts>(wObj);

        if (pGroup)
        {
            auto pRet = pGroup->FindFirst(strObjName);
            return pRet;
        }
    }
    return NULL;
}


/**
 *  @brief  検索
 *  @param  [in] strName  データ名
 *  @retval 描画オブジェクト
 *  @note
 */
std::shared_ptr<CDrawingObject>  CDrawingParts::Find(StdString strName)
{
    auto pRet = FindFirst(strName);
    return pRet;
}


//!<
/**
 *  @brief  オブジェクト取得(名称)
 *  @param  [in] strName  データ名
 *  @retval 描画オブジェクト
 *  @note
 */
std::shared_ptr<CDrawingObject> CDrawingParts::GetObjectByNameS(std::string& strName)
{
    StdString strOut =  CUtil::CharToString(strName.c_str());
    return Find(strOut);
}

/**
 *  @brief  マップ取得
 *  @param  なし
 *  @retval ID検索用マップ
 *  @note
 */
/*
std::map<int , std::weak_ptr<CDrawingObject>>* CDrawingParts::GetList()
{
    return &m_mapObjects;
}
*/

/**
 *  @brief  リスト取得
 *  @param  なし
 *  @retval ID検索用マップ
 *  @note
 */
std::deque<std::shared_ptr<CDrawingObject>>* CDrawingParts::GetList()
{
    return &m_lstObjects;
}

/**
 *  @brief  名称リスト取得
 *  @param  [out] pList名称リスト
 *  @retval なし
 *  @note
 */
void CDrawingParts::GetNmaeList(std::vector<StdString>* pList)
{
    if (!pList)
    {
        return;
    }

    pList->clear();
            
    for(auto ite  = m_mapObjectName.begin();
        ite != m_mapObjectName.end();
        ite++)
    {
        pList->push_back(ite->first);
    }
}

/**
 *  @brief  部品使用有無
 *  @param  [in] pRefCtrl 対象
 *  @retval true 使用あり
 *  @note
 */
bool CDrawingParts::IsIncludeParts(const CPartsDef* pRefCtrl) const
{
    CDrawingObject*  pRefObj =  GetRefObj();

    if (pRefObj == NULL)
    {
        return false;
    }

    CPartsDef* pCurRefCtrl;
    pCurRefCtrl = pRefObj->GetPartsDef();


    if (m_uidObjectDef == boost::uuids::nil_uuid())
    {
        //CdrawingObjecCtrlが保持しているGroup
    }

    if( pCurRefCtrl == pRefCtrl)
    {
        return true;
    }

    for(auto pGroup: m_lstGroup)
    {
        if(pGroup->IsIncludeParts(pRefCtrl))
        {
            return true;
        }
    }
    return false;
}

/**
 *  @brief   参照データに基づいて更新.
 *  @param   
 *  @retval  
 *  @note
 */
CDrawingObject* CDrawingParts::UpdateRef()
{
    CDrawingObject*     pRef;

    //CDrawingObject*     pObj;
    CDrawingParts*     pGrp;
    std::shared_ptr<CDrawingParts>      pOrgGroup;
    std::shared_ptr<CObjectDef>         pOrgDef;
    CPartsDef*    pOrgParts;

    pRef = GetRefObj();
    pGrp = dynamic_cast<CDrawingParts*>(pRef);

    if (m_psRefObject != NULL)
    {
        //複製の場合は元のグループ

        CProjectCtrl* pProj = m_pCtrl->GetProject();

        if (pProj)
        {
            pOrgDef = pProj->GetParts(m_psRefObject->uuidParts).lock();
        }

        if (!pOrgDef)
        {
            pOrgDef = THIS_APP->GetObjectDefByUuid(m_psRefObject->uuidParts).lock();
        }

        STD_ASSERT(pOrgDef);

        if (!pOrgDef)
        {
            return NULL;
        }

        pOrgParts = dynamic_cast<CPartsDef*>(pOrgDef.get());
        STD_ASSERT(pOrgParts);
        pOrgGroup = pOrgParts->GetDrawingParts();
        pOrgGroup->UpdateRef();
    }

    std::map<int , CDrawingObject*>::iterator iteMap;
    if (pGrp != NULL)
    {
        if (pGrp != pOrgGroup.get())
        {
            if (GetChgCnt() != pGrp->GetChgCnt())
            {
                //グループの位置などが変化したとき
                CDrawingObject::UpdateRef();
                m_Pt        = pGrp->m_Pt;
                m_dAngle    = pGrp->m_dAngle;
                m_mat2D     = pGrp->m_mat2D;
                m_iChgCnt = pRef->GetChgCnt();
            }
        }
    }



    if (m_psRefObject != NULL)
    {
        // グループ内部の生成と削除をあわせる
        std::set<std::shared_ptr<CDrawingObject>> listGroup;
        std::deque<std::shared_ptr<CDrawingObject>>* pMap;

        if (pOrgParts == NULL)
        {
            //自分自身を削除してもらう
            return NULL;
        }
        else
        {
            //使用元パーツを取得
            auto pGrpOrg = pOrgParts->GetDrawingParts();

            if(GetChgGroupCnt() == pGrpOrg->GetChgGroupCnt())
            {
                //変更がなければ終わり
                return pRef;
            }


            STD_ASSERT(pGrpOrg != NULL);
            pMap = pGrpOrg->GetList(); 

            //----------------------------
            //  使用元のリストを作成 
            //----------------------------
            auto pOrgList = pGrpOrg->GetList(); 
            //for( iteMap  = pMap->begin();
            //      iteMap != pMap->end();
            //      iteMap++)
            for(auto pOrgObj: *pOrgList)
            {
                listGroup.insert(pOrgObj);
            }
            //----------------------------



            //----------------------------------
            // 削除されているオブジェクトを検索
            //----------------------------------
            std::vector<int> lstDel;
            int iDelId;
            bool bExistOrg = true;

            for(auto pObj: m_lstObjects)
            {
                bExistOrg = true;
                if ((pObj->GetType() == DT_PARTS)||
                    (pObj->GetType() == DT_FIELD)||
                    (pObj->GetType() == DT_REFERENCE))
                {
                    //CDrawingParts* pGrpObj;
                    auto pGrpObj = std::dynamic_pointer_cast<CDrawingParts>(pObj);
                    //元のパーツが削除されていないか
                    if (pGrpObj)
                    {
                        bExistOrg = (pGrpObj->GetRefObj() != NULL);
                    }
                    else
                    {
                        bExistOrg = false;
                    }
                }

                auto pRefObj =  pObj->GetRefObj();
                //auto  iteSet = listGroup.find(pRefObj);

                //TODO:動作未確認
                auto iteSet = std::find_if(listGroup.begin(),  
                                          listGroup.end(), [=](std::shared_ptr<CDrawingObject> pObj)
                                            {
                                                  return (pRefObj == pObj.get());
                                            });

                if (iteSet == listGroup.end())
                {
                    bExistOrg = false;
                }

                if (!bExistOrg)
                {
                    iDelId = iteMap->first;
                    lstDel.push_back(iDelId);
                }
                else
                {
                    // 存在するオブジェクトは更新し
                    // リストから削除する
                    pObj->UpdateRef();
                    listGroup.erase(iteSet);
                }
            }

            //削除分
            for(int iId: lstDel)
            {
                Delete( iId); 
            }


            //追加分
            for(auto pAddObj: listGroup)
            {
               auto  pCpyObj = CDrawingObject::CloneShared(pAddObj.get());

                STD_ASSERT(pCpyObj != NULL);
                if (pCpyObj)
                {
                    AddData(pCpyObj, true, false);
                }
            }
            m_iChgGroup = pGrpOrg->GetChgGroupCnt();
            return  pRef;
        }
    }

    //for( iteMap  = m_mapObjects.begin();
    //     iteMap != m_mapObjects.end();
    //     iteMap++)
    for (auto pObj: m_lstObjects)
    {
        //pObj = iteMap->second;
        pObj->UpdateRef();
    }
    return pRef;
}

/**
 *  @brief   スナップ点取得.
 *  @param   [out] pLstSnap スナップ点リスト
 *  @retval  true: スナップ点あり
 *  @note
 */
bool CDrawingParts::GetSnapList(std::list<SNAP_DATA>* pLstSnap)const 
{
    using namespace VIEW_COMMON;

    DWORD dwSnap = DRAW_CONFIG->GetSnapType();
    bool bRet = false;

    STD_ASSERT(pLstSnap);

    if (!pLstSnap)
    {
        return false;
    }

    pLstSnap->clear();
    SNAP_DATA snapData;
    if (dwSnap & SNP_DATUME_POINT)
    {
        snapData.eSnapType = SNP_DATUME_POINT;
        snapData.pt = m_Pt;
        if (dwSnap & SNP_PARTS)
        {
            snapData.bKeepSelect = true;
        }
        else
        {
            snapData.bKeepSelect = false;
        }
        pLstSnap->push_back(snapData);
        bRet = true;
    }
    
    
    bool bPoint = (dwSnap & SNP_FEATURE_POINT) == SNP_FEATURE_POINT;
    bool bNode = (dwSnap & SNP_NODE) == SNP_NODE;

    if (!bPoint && !bNode)
    {
        return bRet;
    }

    snapData.bKeepSelect = false;

    POINT2D         ptPos;
    const MAT2D* pMat = GetDrawingMatrix();

    for (auto pObject: m_lstObjects)
    {
        if (bPoint)
        {
            if (pObject->GetType() == DT_POINT)
            {
                auto pPoint = std::dynamic_pointer_cast<CDrawingPoint>(pObject);
                ptPos = *pPoint->GetPointInstance();
                ptPos.Matrix(*pMat);

                snapData.eSnapType = SNP_FEATURE_POINT;
                snapData.pt = ptPos;
                pLstSnap->push_back(snapData);
                bRet = true;
            }
        }

        if(bNode)
        {
            if (pObject->GetType() == DT_NODE)
            {
                auto pNode = std::dynamic_pointer_cast<CDrawingNode>(pObject);

                ptPos = pNode->GetPoint();
                ptPos.Matrix(*pMat);

                const CNodeData* pConnection =  pNode->GetNodeData(0);
                snapData.eSnapType = SNP_NODE;
                snapData.pt = ptPos;

                int iId  = pNode->GetId();
                auto ite = std::find(m_lstConnection.begin(),
                           m_lstConnection.end(),
                           iId);

                __int64 iIndex = -1;
                if(ite !=  m_lstConnection.end())
                {
                    iIndex = ite - m_lstConnection.begin();
                }

                snapData.iConnectionIndex = iIndex;
                snapData.pConnection = std::make_shared<CNodeData>(*pConnection);
                pLstSnap->push_back(snapData);
                bRet = true;
            }
        }
    }
    return bRet;
}

bool CDrawingParts::CreateNodeData(int iIndex)
{
    return true;
}

CNodeData* CDrawingParts::GetNodeData(int iIndex) 
{
    int iConnectionId = _GetConnectionId(iIndex);
    auto pObj =  Find(iConnectionId); 
    if (!pObj)
    {
        return NULL;
    }

    if (pObj->GetType() == DT_NODE)
    {
        auto pNode = std::dynamic_pointer_cast<CDrawingNode>(pObj);
        if (pNode)
        {
            return pNode->GetNodeData(0);
        }
    }
    return NULL;
}

CNodeData* CDrawingParts::GetNodeDataConst(int iIndex) const
{
    int iConnectionId = _GetConnectionId(iIndex);
    auto pObj = Find(iConnectionId);
    if (!pObj)
    {
        return NULL;
    }

    if (pObj->GetType() == DT_NODE)
    {
        auto pNode = std::dynamic_pointer_cast<CDrawingNode>(pObj);
        if (pNode)
        {
            return pNode->GetNodeData(0);
        }
    }
    return NULL;
}

void CDrawingParts::SetCopyNodeDataMode(bool bSet)
{
   for (auto pObj:  m_lstObjects)
   {
       pObj->SetCopyNodeDataMode(bSet);
   }
}

void CDrawingParts::_CreateNodeDataDataList(std::vector<int>* list) const
{
    int iIndex = 0;
    for (auto pObj:  m_lstObjects)
    {
        if(pObj->GetType() == DT_NODE)
        {
            auto pNode = std::dynamic_pointer_cast<CDrawingNode>(pObj); 
            if(pNode)
            {
                list->push_back(pNode->GetId());
                pNode->GetNodeData(0)->iIndex = iIndex;
                iIndex++;
            }
        }
    }

    /*
    for(auto pGroup: m_lstGroup)
    {
        pGroup->_CreateNodeDataDataList(list);
    }
    */
}

int CDrawingParts::GetNodeDataNum() const 
{
    return SizeToInt(m_lstConnection.size());
}

bool CDrawingParts::UpdateNodeSocketData(CDrawingObject *pTmpObject, CPartsDef* pDef)
{
    bool bRet = false;

    /*
    bool bChangeConnection = true;
    if (!pDef)
    {
        pDef =  GetPartsDef();
    }

    if (pDef)
    {
        if(pDef->IsLockUpdateNodeData())
        {
            bChangeConnection = false;
        }
    }

    if (bChangeConnection)
    {
    }
    */
    bRet = CDrawingObject::UpdateNodeSocketData(pTmpObject, pDef);

    m_lstConnection.clear();
    _CreateNodeDataDataList(&m_lstConnection);


    return bRet;
}


int CDrawingParts::_GetConnectionId(int iNo) const 
{
    if (iNo < 0)
    {
        return -1;
    }

    if (iNo >= m_lstConnection.size())
    {
        return -1;
    }
    return m_lstConnection[iNo];
}
/*
bool CDrawingParts::GetNodeDataPos(POINT2D* pt, int iIndex) const 
{
    int iConnectionId = _GetConnectionId(iIndex);
    auto pObj =  Find(iConnectionId); 
    if (pObj->GetType() == DT_NODE)
    {
        auto pNode = std::dynamic_pointer_cast<CDrawingNode>(pObj);
        MAT2D matDraw;
        auto pPraent = GetParentParts();
        if (pPraent)
        {
            auto pMat = pPraent->GetDrawingMatrix();
            if (pMat)
            {
                matDraw = m_mat2D * (*pMat);
            }
            else
            {
                matDraw = m_mat2D;
            }
        }
        else
        {
            matDraw = m_mat2D;
        }

        if (m_pMatOffset)
        {
            matDraw = matDraw * (*m_pMatOffset);
        }

        *pt = pNode->GetPoint() * matDraw;


        return true;
    }
    return false;
}
*/

/**
 *  @brief  Node更新通知
 *  @param  [out]   pPt   新たな位置
 *  @param  [in]    pSocketObj  Socket側のオブジェクト
 *  @param  [in]    iSocketIndex  Socket側の接続Index
 *  @param  [in]    iIndex        接続Index
 *  @param  [inout] pList         循環変更回避用リスト
 *  @param  [inout] pUndo
 *  @retval false  更新なし
 *  @note   Socket側が移動したときに呼び出す
 */
bool CDrawingParts::OnUpdateNodeSocketData(const POINT2D* pPt,
    const CDrawingObject* pSocketObj,
    int iSocketIndex,
    int iIndex,
    std::set<int>* pList,
    CUndoAction* pUndo
)
{
    if (!CDrawingObject::OnUpdateNodeSocketData(pPt, 
                                                pSocketObj, 
                                                iSocketIndex,
                                                iIndex,
                                                pList,
                                                pUndo))
    {
        return false;
    }

    //ACTIVE,NODEの設定以外では動かない
    int iNodeObjctId = _GetConnectionId(iIndex);
    auto pObj = Find(iNodeObjctId);
    if (!pObj)
    {
        return false;
    }

    if (pObj->GetType() != DT_NODE)
    {
        return false;
    }

    auto pNode = std::dynamic_pointer_cast<CDrawingNode>(pObj);
    if (!pNode)
    {
        return false;
    }

    //pNode->GetNodeData(0);

    auto strMarkerId = pNode->GetName();
    CNodeMarker marker(NULL);

    //ユーザー定義の場合簡単に定義できないか要検討
    /*
    marker.Create(pNode.get(), ptDraw);


    if (m_psScriptObject)
    {
        m_psScriptObject->OnMoveNodeMarkerId(pMarker, strMarkerId, *pPt, posMouse);
    }
    */
    return true;
}


RECT2D::E_EDGE CDrawingParts::InquireConnectDirection(std::map<RECT2D::E_EDGE, LINE2D>* pDir,
                                                               RECT2D* pRc,
                                                               int iIndex ) const 
{
    RECT2D::E_EDGE  eEdge;
    int iObj = _GetConnectionId(iIndex);
    auto pObj =  Find(iObj); 
#ifdef _DEBUG
    DB_PRINT(_T("InquireConnectDirection iObj:%d %I64x\n"), iObj, pObj.get());
#endif
    eEdge = _InquireConnectDirection2( pDir, pRc, pObj.get());
    return  eEdge;      
}

RECT2D::E_EDGE CDrawingParts::_InquireConnectDirection2(std::map<RECT2D::E_EDGE, LINE2D>* pDir,
                                                               RECT2D* pRc,
                                                               CDrawingObject* pObj ) const 
{
    RECT2D::E_EDGE  eEdge = RECT2D::E_NONE;

    if (pObj->GetType() != DT_NODE)
    {
        return  eEdge;      
    }

    CDrawingNode* pNode;
    pNode = dynamic_cast<CDrawingNode*>(pObj);

    double dSnapLen = DRAW_CONFIG->DimToDispUnit(DRAW_CONFIG->dConnectionStraghtLength);


    //--------------------------------------------------
    auto SetLine =[&](POINT2D& ptS, CNodeData::E_LINE_DIR_TYPE  eT)
    {
        POINT2D ptE = ptS;
        switch(eT)
        {
        case CNodeData::E_TOP:
              ptE.dY += dSnapLen;
              break;

        case CNodeData::E_RIGHT:
              ptE.dX += dSnapLen;
              break;

        case CNodeData::E_BOTTOM:
              ptE.dY -= dSnapLen;
              break;

        case CNodeData::E_LEFT:
              ptE.dX -= dSnapLen;
              break;
        }
        std::pair<RECT2D::E_EDGE, LINE2D> pair;
        pair.first = static_cast<RECT2D::E_EDGE>(eT);
        pair.second.SetPt(ptS,  ptE);
        pDir->insert(pair);
    };
    //--------------------------------------------------

    RECT2D rc;
    rc = GetBounds();

    if (m_pMatOffset)
    {
        rc.Matrix(*m_pMatOffset);
#ifdef _DEBUG
DB_PRINT(_T("_InquireConnectDirection2\n %s \n"), m_pMatOffset->Str().c_str());
#endif
    }


    rc.Expansion(dSnapLen);

    if (pRc)
    {
        *pRc = rc;
    }

    MAT2D matDraw = m_mat2D;

    if (m_pMatOffset)
    {
        matDraw = matDraw * (*m_pMatOffset);
    }

    POINT2D ptS = pNode->GetPoint() * matDraw;

    CNodeData::E_LINE_DIR_TYPE eType;
    eType = pNode->GetLineDirection();
    switch(eType)
    {
    case CNodeData::E_TOP:    
    case CNodeData::E_RIGHT:  
    case CNodeData::E_BOTTOM: 
    case CNodeData::E_LEFT:   
           SetLine(ptS, eType);
           break;

    case CNodeData::E_DIRX:   
           SetLine(ptS, CNodeData::E_RIGHT);
           SetLine(ptS, CNodeData::E_LEFT);
           break;

    case CNodeData::E_DIRY:  
           SetLine(ptS, CNodeData::E_TOP);
           SetLine(ptS, CNodeData::E_BOTTOM);
           break;

    case CNodeData::E_ALL :   
           SetLine(ptS, CNodeData::E_RIGHT);
           SetLine(ptS, CNodeData::E_LEFT);
           SetLine(ptS, CNodeData::E_TOP);
           SetLine(ptS, CNodeData::E_BOTTOM);
           break;

    case CNodeData::E_NORM_OBJECT_BOUNDS:
    {
        POINT2D ptOnEdge;
        eEdge = rc.NearEdgePoint(&ptOnEdge, ptS);
       

        std::pair<RECT2D::E_EDGE, LINE2D> pair;
        pair.first = eEdge;
        pair.second.SetPt(ptS, ptOnEdge); 
        pDir->insert(pair);
#if 0
 DB_PRINT(_T("_ICD2 :rc:%s ptS(%s)\n"), rc.Str().c_str(),
                                      ptS.Str().c_str());

#endif
    }
        break;
    case CNodeData::E_CUSTOM :
        break;

    default:
        break;

    }

    return  eEdge;      
}

void CDrawingParts::CalcFramePoint(std::vector<POINT2D>* pLst) const
{
    auto wRef = GetObjectDef();
    std::shared_ptr<CObjectDef> pRef = wRef.lock();

    bool bParts = false;
    if (pRef)
    {
        auto type = pRef->GetObjectType();
        bParts = (type == VIEW_COMMON::E_PARTS);
    }

    CPartsDef* pDef = NULL;
    if (bParts)
    {
        pDef = dynamic_cast<CPartsDef*>(pRef.get());
    }

    bool bFirst = true;
    RECT2D rc;
    for (auto pObject : m_lstObjects)
    {
        if (pDef)
        {
            //定義元のレイヤーでバウンド未使用のレイヤーは計算から除外する
            int iLayerId = pObject->GetLayer();
            auto pLayer = pDef->GetLayer(iLayerId);
            if (!pLayer->bUseBoundingBox)
            {
                continue;
            }
        }

        if (bFirst)
        {
            rc = pObject->GetBounds();
            bFirst = false;
        }
        else
        {
            rc.ExpandArea(pObject->GetBounds());
        }
    }


    pLst->resize(4);

    /*
    m_ptInit[ND_TL] = m_lstBoundOrg[0];
    m_ptInit[ND_TR] = m_lstBoundOrg[1];
    m_ptInit[ND_BR] = m_lstBoundOrg[2];
    m_ptInit[ND_BL] = m_lstBoundOrg[3];
    */

    pLst->at(0) = POINT2D(rc.dLeft , rc.dTop);
    pLst->at(1) = POINT2D(rc.dRight, rc.dTop);
    pLst->at(2) = POINT2D(rc.dRight, rc.dBottom);
    pLst->at(3) = POINT2D(rc.dLeft , rc.dBottom);

    for (auto &pt : *pLst)
    {
        pt.Matrix(m_mat2D);
    }
}

/**
 * @brief   領域取得
 * @param   なし
 * @retval  なし
 * @note	描画領域を取得する
 */
RECT2D CDrawingParts::GetBounds() const
{
    RECT2D rc;
	auto wRef = GetObjectDef();
	std::shared_ptr<CObjectDef> pRef = wRef.lock();

	bool bParts = false;
	if (pRef)
	{
		auto type = pRef->GetObjectType();
		bParts = (type == VIEW_COMMON::E_PARTS);
	}

	CPartsDef* pDef = NULL;
	if (bParts)
	{
		pDef = dynamic_cast<CPartsDef*>(pRef.get());
	}

    bool bFirst = true;
    for( auto pObject: m_lstObjects)
    {
		if (pDef)
		{
			//定義元のレイヤーでバウンド未使用のレイヤーは計算から除外する
			int iLayerId = pObject->GetLayer();
			auto pLayer = pDef->GetLayer(iLayerId);
			if (!pLayer->bUseBoundingBox)
			{
				continue;
			}
		}

        if (bFirst)
        {
            rc = pObject->GetBounds();
            bFirst = false;
        }
        else
        {
            rc.ExpandArea(pObject->GetBounds());
        }
    }

    rc.Matrix(m_mat2D);
    return rc;
}

/**
 * @brief   領域取得
 * @param   なし
 * @retval  なし
 * @note	全体表示のため縮尺を考慮した領域を取得する
 */
RECT2D CDrawingParts::GetViewBounds() const
{
    RECT2D rc2D;
    RECT2D rc;
    RECT2D rcRet;

    rcRet.dRight  = 0;
    rcRet.dTop    = 0;
    rcRet.dLeft   = 0;
    rcRet.dBottom = 0;

    CLayer* pLayer;
    int iLayer;
    bool bFirst = true;
    for( auto pObject: m_lstObjects)
    {
        iLayer = pObject->GetLayer();
        pLayer = GetPartsDef()->GetLayer(iLayer);

        rc2D = pObject->GetBounds();

        rc.dRight    =   rc2D.dRight *  pLayer->dScl;
        rc.dTop      =   rc2D.dTop   *  pLayer->dScl;
        rc.dLeft     =   rc2D.dLeft  *  pLayer->dScl;
        rc.dBottom   =   rc2D.dBottom *  pLayer->dScl;

        if (bFirst)
        {
            rcRet = rc;
            bFirst = false;
        }
        rcRet.ExpandArea(rc);
    }
    return rcRet;
}

/**
 * @brief   距離
 * @param   [in] pt 
 * @retval  近接点からの距離
 * @note
 */
double CDrawingParts::Distance(const POINT2D& pt) const
{
    double dNear = DBL_MAX;
    double dMin = DBL_MAX;

    MAT2D matTmp = m_mat2D;
    MAT2D matInv;
    matInv = matTmp.Invert();
    POINT2D ptGroup;
    ptGroup = pt * matInv;
    for( auto pObj: m_lstObjects)
    {
         dNear = pObj->Distance(ptGroup);

         if (dNear < dMin)
         {
             dMin = dNear;
         }
    }

    return dMin;
}


/**
 *  @brief   マーカ初期化.
 *  @param   [in] pMarker
 *  @retval  なし
 *  @note
 */
bool CDrawingParts::InitNodeMarker(CNodeMarker* pMarker)
{
    STD_ASSERT(pMarker);

    std::shared_ptr<CObjectDef> pDef;
    pDef = GetObjectDef().lock();
    bool bNodeBounds = true;
    if (pDef)
    {
        CPartsDef* pPartsDef = dynamic_cast<CPartsDef*>(pDef.get());

        if (pPartsDef)
        {
            bNodeBounds = pPartsDef->IsUseNodeBounds();
        }
    }

    if(!bNodeBounds)
    {
        if(m_psNodeBound)
        {
            m_psNodeBound.reset();
        }
        if(m_psScriptObject)
        {
            bool bInit = false;
            for( auto pObject: m_lstObjects)
            {
                if(pObject->GetType() != DT_NODE) 
                {
                    continue;
                }

                if (!bInit)
                {
                    pMarker->SetObject(shared_from_this());
                    bInit = true;
                }

                auto pNode = std::dynamic_pointer_cast<CDrawingNode>(pObject);

                if (!pNode)
                {
                    continue;
                }
                    
                if(pNode->GetNodeData(0)->eConnectionType != E_ACTIVE)
                {
                    continue;
                }

                //位置の補正
                const MAT2D* pMat = pNode->GetParentMatrix();
                POINT2D ptDraw = *(pNode->GetPointInstance());
                if (pMat != NULL)
                {
                    ptDraw = ptDraw * (*pMat);
                }

                pMarker->Create(pNode.get(), ptDraw);
            }

            m_psScriptObject->OnInitNodeMarker(pMarker);
        }
    }
    else
    {
        if(!m_psNodeBound)
        {
            m_psNodeBound = std::make_unique<CNodeBound>();
        }

        std::vector<POINT2D> lstFrame;
        CalcFramePoint(&lstFrame);

        CDrawingView* pView = pMarker->GetView();
        pView->ClearDraggingNodes();
        pView->ClearDraggingTmpObjects();

        //マーカーを用意
        m_psNodeBound->EnableScale(true);
        m_psNodeBound->EnableRotate(true);
        m_psNodeBound->SetDragMode(CNodeBound::DM_ONE);
        m_psNodeBound->Init(pMarker->GetView(),
                       lstFrame,
                       m_dAngle,
                       m_dSclX,
                       m_dSclY,
                       1.0,
                       shared_from_this());

    }
    return true;
}


/**
 *  @brief   マーカ移動
 *  @param   [in] pMarker
 *  @param   [in] strMarkerId
 *  @param   [in] ptRet
 *  @retval  なし
 *  @note
 */
void CDrawingParts::MoveNodeMarker(CNodeMarker* pMarker,
                                 SNAP_DATA* pSnap,
                                 StdString strMarkerId,
                                 MOUSE_MOVE_POS posMouse)
{
#ifdef _DEBUG
    DB_PRINT(_T("CDrawingParts::MoveNodeMarker --\n"));
#endif
    if(m_psNodeBound)
    {
        CDrawingView* pView = pMarker->GetView();
        CPartsDef*   pDef  = pView->GetPartsDef();

        pView->SetDrawingMode(DRAW_SEL);

        pView->GetMousePoint2(pSnap, posMouse );
		m_psNodeBound->OnMouseMove(pSnap, posMouse);
    }


    if (m_psScriptObject)
    {
        const MAT2D* pMat;
        MAT2D   mat;
        MAT2D   matInv;
        POINT2D ptTmp(pSnap->pt);
        CDrawingNode* pNode = NULL;
        E_LOCAL_WORLD eLocalWorld = E_LOCAL;
        if (pMarker)
        {
            pNode = pMarker->GetDrawingNode(strMarkerId);
        }

        if (pNode)
        {
            eLocalWorld = pNode->GetMarkerCordinateSystem();
        }

        if (eLocalWorld == E_LOCAL)
        {
            pMat  =  GetDrawingMatrix();
            if (pMat != NULL)
            {
                mat  = *pMat;
                matInv= mat.Invert();
                ptTmp.Matrix(matInv);
            }
        }
        m_psScriptObject->OnMoveNodeMarkerId(pMarker, strMarkerId, ptTmp, posMouse);
    }
#ifdef _DEBUG
    DB_PRINT(_T("CDrawingParts::MoveNodeMarker END--\n"));
#endif
}

/**
 *  @brief   マーカ選択
 *  @param   [in] pMarker
 *  @param   [in] iMarkerId
 *  @retval  なし
 *  @note
 */
 void CDrawingParts::SelectNodeMarker(CNodeMarker* pMarker,
                        StdString strMarkerId)
{
    if (m_psNodeBound)
    {
        m_psNodeBound->Select(pMarker, strMarkerId);
    }

    if (m_psScriptObject)
    {
        m_psScriptObject->OnSelectNodeMarker(pMarker, strMarkerId);
    }
}

 
/**
 *  @brief   マーカ開放
 *  @param   [in] pMarker
 *  @retval  なし
 *  @note
 */
bool CDrawingParts::ReleaseNodeMarker(CNodeMarker* pMarker, StdString strMarkerId, MOUSE_MOVE_POS posMouse)
{
    //MK_LBUTTON   //マウスの左ボタンが押されている場合に設定します。
    //MK_MBUTTON   //マウスの中央ボタンが押されている場合に設定します。
    //MK_RBUTTON   //マウスの右ボタンが押されている場合に設定します。

    if(m_psNodeBound)
    {
        if (pMarker->GetMouseButton() == SEL_LBUTTON)
        {
            if(m_psNodeBound->OnMouseLButtonUp(posMouse))
            {
                m_psNodeBound->ApplyMatrix(false);
            }
        }
        else if(pMarker->GetMouseButton() == SEL_RBUTTON)
        {
            m_psNodeBound->OnMouseRButtonUp(posMouse);
        }
        m_psNodeBound->Release();
	    m_psNodeBound.reset();
    }

    m_pMatOffset = NULL;

    if (m_psScriptObject)
    {
        m_psScriptObject->OnReleaseNodeMarker(pMarker);
    }

    /*  CActionNodeSelect::SelPointで 変更中（Matrixが適用中）の
        場合があるのと余計な描画となる場合を考えここでは再描画しない
    CDrawingView* pView = pMarker->GetView();
    CPartsDef*   pDef  = pView->GetPartsDef();
    pDef->Redraw();
    */
    return true;
}


/**
 *  @brief   マーカ変更
 *  @param   [in] pMarker
 *  @retval  なし
 *  @note
 */
void CDrawingParts::ChangeNode(StdString strMarkerId,
                                                    E_NODE_CHANGE_TYPE eType)
{
    if (m_psScriptObject)
    {
        m_psScriptObject->OnChangeNode(strMarkerId, eType);
    }
}

/**
 * @brief   カレントレイヤーID設定
 * @param   [in]    iLayerId     レイヤーID
 * @param   なし          
 * @retval  なし
 * @note	
 */
/*
void CDrawingParts::SetCurrentLayerId(int iLayerId)
{
    STD_ASSERT(iLayerId >= 0);
    STD_ASSERT(iLayerId < GetLayerMax());

    m_curLayerId = iLayerId;
}
*/

/**
 * @brief   カレントレイヤーID取得
 * @param   なし
 * @param   なし
 * @retval  カレントレイヤーID
 * @note	
 */
/*
int CDrawingParts::GetCurrentLayerId() const
{
    return m_curLayerId;
}
*/

/**
 * @brief   レイヤー取得
 * @param   [in]    iLayerId  レイヤーID
 * @param   なし          
 * @retval  レイヤーデータ
 * @note	
 */
/*
CLayer* CDrawingParts::GetLayer(int iLayerId)
{
    STD_ASSERT(iLayerId >= 0);
    STD_ASSERT(iLayerId < GetLayerMax());
    
    return   &m_lstLayer[iLayerId];
}

const CLayer* CDrawingParts::GetLayer(int iLayerId) const
{
    STD_ASSERT(iLayerId >= 0);
    STD_ASSERT(iLayerId < GetLayerMax());
    
    return   &m_lstLayer[iLayerId];
}
*/

/**
 * @brief   レイヤー設定
 * @param   なし          
 * @retval  なし
 * @note    
 */
/*
void CDrawingParts::SetLayer()
{
    CObjectDef* pDef;
    pDef = GetObjectDef();
    CPartsDef* pPartsDef = dynamic_cast<CPartsDef*>(pDef);

    STD_ASSERT(!pPartsDef);

    if (pPartsDef)
    {
        int iMax = pPartsDef->GetLayerMax();
        m_lstLayer.resize(iMax);

        for (int iLayer = 0; iLayer < iMax; iLayer++)
        {
            m_lstLayer[iLayer] = *pPartsDef->GetLayer(iLayer);
        }
    }
}
*/

/**
 * @brief   レイヤー消去
 * @param   なし          
 * @retval
 * @note	
 */
/*
void CDrawingParts::ClearLayer()
{
    m_lstLayer.clear();
}
*/

/**
 * @brief   レイヤー最大数取得
 * @param   なし          
 * @retval  レイヤー数
 * @note	
 */
/*
int CDrawingParts::GetLayerMax() const
{
    return int(m_lstLayer.size());
}
*/

/**
 * @brief   ロード終了後処理
 * @param   なし
 * @retval  なし
 * @note	
 */
void CDrawingParts::LoadAfter(CPartsDef* pDef)
{
    CDrawingScriptBase::LoadAfter(pDef);
    ResetParent();
    m_lstConnection.clear();
    _CreateNodeDataDataList(&m_lstConnection);

    for(auto pObj: m_lstObjects)
    {
        pObj->LoadAfter(pDef);
    }
}

//エリア選択  For AS
bool CDrawingParts::GetAreaObjects(std::vector<CDrawingObject*>* pLstObjects, 
                    POINT2D pt1, POINT2D pt2, 
                    DRAWING_TYPE eType)
{
    for( auto pObj: m_lstObjects)
    {

       if (pObj->GetType() != eType)
       {
           continue;
       }

        RECT2D rcArea(pt1, pt2);
        if (pObj->IsInner(rcArea, DRAW_CONFIG->IsSelPart()))
        {
            pLstObjects->push_back(pObj.get());
        }
    }
    return true;
}


/**
 *  @brief   デバッグ用文字
 *  @param   [in] strSpc 
 *  @param   デバッグ用文字
 *  @retval  なし
 *  @note
 */
StdString CDrawingParts::GetStr(StdString strSpc) const
{
    StdStringStream strRet;
    strRet << strSpc << CDrawingObject::GetStr();
    strRet << _T("\n");

    std::pair<int, CDrawingObject* > pairObj;

    strSpc += _T("  ");

    for(auto pObj: m_lstObjects)
    {
        if ((pObj->GetType() == DT_PARTS)||
            (pObj->GetType() == DT_FIELD))
        {
            auto pGroup = std::dynamic_pointer_cast<CDrawingParts>(pObj);

            if (pGroup)
            {
                strRet << strSpc << pGroup->GetStr(strSpc);
            }
            else
            {
                strRet << strSpc << _T("ERRROR");
            }
        }
        else
        {
            strRet << strSpc << pObj->GetStr();
        }
        strRet << _T("\n");
    }
    return strRet.str();
}



/**
 *  @brief   デバッグ用文字
 *  @param   なし
 *  @param   デバッグ用文字
 *  @retval     なし
 *  @note
 */
StdString CDrawingParts::GetStr() const
{
    return GetStr(_T(""));
}

void CDrawingParts::DebugPrintOnSelect()
{
    
    CDrawingObject::DebugPrintOnSelect();
}

//!< 分解
// 自分自身を消す必要があるので 通常はCPartsDef->Disassembleから呼び出す
// bSetUndo == true は 
bool CDrawingParts::Disassemble(CPartsDef* pDef, bool bSetUndo)
{
    auto pParent = GetParentParts();

    CUndoAction* pUndo  = pDef->GetUndoAction();

    pDef->SetLockUpdateNodeData(true);

    std::vector<std::shared_ptr<CDrawingObject>>   lstConnectionLine;

    for(auto pObj: m_lstObjects)
    {
        pObj->SetPartsDef(pDef);

        //MAT2D mat2D = m_mat2D* pObj->GetMatrix();

        pObj->Matrix(m_mat2D);
        pObj->SetLayer(m_iLayerId);
        pObj->SetParentParts(pParent);
        pObj->SetCopyNodeDataMode(true);   //接続リストをコピーする

        if (pObj->GetType() == DT_CONNECTION_LINE)
        {
            lstConnectionLine.push_back(pObj);
        }

        if(bSetUndo)
        {
            pUndo->Add(UD_ADD, pDef, NULL, pObj, false);
        }
        bool bCreateId = false;
        bool bKeepUnfindId = true;
        pDef->RegisterObject(pObj, bCreateId, bKeepUnfindId);
    }
    pDef->SetLockUpdateNodeData(false);

    // ここではUNDOをクローズしないため
    // bSetUndo を使用した場合呼び出し側で  pUndo->Push()を呼び出す必要がある
    m_mapObjects.clear();
    m_mapObjectName.clear();
    m_lstGroup.clear();
    m_lstObjects.clear();

    //接続先名称更新
    for (auto pObj : lstConnectionLine)
    {
        pObj->UpdateNodeSocketData();
    }

    return true;
}

           //描画順序
void CDrawingParts::ChangeDispOrder(VIEW_COMMON::E_DISP_ORDER order, int iId)
{
    auto iteObject = std::find_if(m_lstObjects.begin(),
                            m_lstObjects.end(),
                                [=]  ( std::shared_ptr<CDrawingObject>  pObj)
                                    {
                                        return(pObj->GetId() == iId);
                                    });
 
    if(iteObject == m_lstObjects.end())
    {
        return;
    }

    auto obj = *iteObject;
    if (order == VIEW_COMMON::E_TOPBACK)
    {
        m_lstObjects.erase(iteObject);
        m_lstObjects.push_front(obj);
    }
    else if (order == VIEW_COMMON::E_TOPMOST)
    {
        m_lstObjects.erase(iteObject);
        m_lstObjects.push_back(obj);
    }
    else if (order == VIEW_COMMON::E_BACK)
    {
        std::size_t index = m_lstObjects.size() - 
                    std::distance(m_lstObjects.begin(), iteObject) ;

        int iLayer =  (*iteObject)->GetLayer();
       
        auto iteStart = m_lstObjects.rbegin() + index - 1;

        for( auto ite =  iteStart + 1;
            ite !=  m_lstObjects.rend();
            ite++)
        {
            if((*ite)->GetLayer() ==  iLayer)
            {
                for(auto iteRev =  iteStart;
                    iteRev !=  ite ;
                    iteRev++)
                {
                  std::swap(*(iteRev+1), *iteRev);
                }
               break;
            }
        }
    }
    else if (order == VIEW_COMMON::E_FRONT)
    {
        int iLayer =  (*iteObject)->GetLayer();
        for( auto ite = (iteObject + 1);
            ite !=  m_lstObjects.end();
            ite++)
        {
            if((*ite)->GetLayer() ==  iLayer)
            {
                for(auto iteSwap =  iteObject;
                    iteSwap !=  ite;
                    iteSwap++)
                {
                    std::swap(*iteSwap, *(iteSwap+1));
                }
                break;
            }
        }
    }
}

bool CDrawingParts::IsIgnoreSelPart() const
{
    std::shared_ptr<CObjectDef> pObjDef;
    pObjDef = GetObjectDef().lock();
    auto pDef = std::dynamic_pointer_cast<CPartsDef>(pObjDef);

    if (pDef)
    {
        return pDef->IsIgnoreSelPart();
    }
    return false;
}

/**
 * @brief   シリアル化(ロード)
 * @param   ar
 * @param   version     バージョン番号
 * @retval  なし
 * @note
 */
template<class Archive>
void CDrawingParts::load(Archive& ar, const unsigned int version)
{
	SERIALIZATION_INIT
	ar& BOOST_SERIALIZATION_BASE_OBJECT_NVP(CDrawingScriptBase);
	SERIALIZATION_LOAD("Pos", m_Pt);
	SERIALIZATION_LOAD("Angle", m_dAngle);
	SERIALIZATION_LOAD("Matrix", m_mat2D);
	SERIALIZATION_LOAD("OperationInheritance", m_bOperationInheritance);
	SERIALIZATION_LOAD("UseGroupColor", m_bUseGroupColor);
#if 0
	SERIALIZATION_LOAD("Objects", m_lstObjects);
#else
	//デバッグ用にオブジェクトを一つづつ読み込む
	int iObjCount;
	m_lstObjects.clear();
	SERIALIZATION_LOAD("ObjectNum", iObjCount);
	std::shared_ptr<CDrawingObject> pObj;
	for (int iCnt = 0; iCnt < iObjCount; iCnt++)
	{
		pObj = CDrawingObject::Load<Archive>(ar, iCnt, s);
		m_lstObjects.push_back(pObj);

    }
#endif

	CreateIdMap();
	QUERY_REF_OBJECT ref;
	SERIALIZATION_LOAD("ReferenceObject", ref);

	if (ref.uuidParts != boost::uuids::nil_uuid())
	{
		m_psRefObject = new QUERY_REF_OBJECT;
		m_psRefObject->uuidParts = ref.uuidParts;
		m_psRefObject->iObjectId = ref.iObjectId;
	}
	else
	{
		if (m_psRefObject)
		{
			delete m_psRefObject;
			m_psRefObject = NULL;
		}
	}
	m_iChgGroup = 0;
	MOCK_EXCEPTION_FILE(e_file_read);


#ifdef _DEBUGSAVE                          
DB_PRINT(_T("----CDrawingParts::load----\n"));    
DB_PRINT(_T(" %s\n"), GetStr(_T("")).c_str());
DB_PRINT(_T("---------------------------\n"));
#endif
}


template<class Archive>
void CDrawingParts::save(Archive& ar, const unsigned int version) const
{
	//セーブ開始を通知
	SERIALIZATION_INIT
	ar& BOOST_SERIALIZATION_BASE_OBJECT_NVP(CDrawingScriptBase);
	SERIALIZATION_SAVE("Pos", m_Pt);
	SERIALIZATION_SAVE("Angle", m_dAngle);
	SERIALIZATION_SAVE("Matrix", m_mat2D);
	SERIALIZATION_SAVE("OperationInheritance", m_bOperationInheritance);
	SERIALIZATION_SAVE("UseGroupColor", m_bUseGroupColor);
#if 0
	SERIALIZATION_SAVE("Objects", m_lstObjects);
#else
	int iObjCount = SizeToInt(m_lstObjects.size());
	SERIALIZATION_SAVE("ObjectNum", iObjCount);
	int iCnt = 0;
	for (auto pObj : m_lstObjects)
	{
		pObj->Save<Archive>(ar, iCnt, s);
		iCnt++;
	}
#endif

	QUERY_REF_OBJECT ref;
	if (m_psRefObject)
	{
		SERIALIZATION_SAVE("ReferenceObject", *m_psRefObject);
	}
	else
	{
		ref.uuidParts = boost::uuids::nil_uuid();
		ref.iObjectId = -1;
		SERIALIZATION_SAVE("ReferenceObject", ref);
	}
	m_iChgGroup = 0;
	MOCK_EXCEPTION_FILE(e_file_write);

}

//!< load save のインスタンスが生成用ダミー
void CDrawingParts::Dummy()
{
	unsigned int iVersion = 0;

	StdStreamOut outFs(_T(""));
	StdStreamIn  inFs(_T(""));
	//boost::filesystem::ofstream outTxtFs(_T(""));
	//boost::filesystem::ifstream inTxtFs(_T(""));

	boost::archive::text_woarchive txtOut(outFs);
	boost::archive::text_wiarchive txtIn(inFs);

	save<boost::archive::text_woarchive>(txtOut, iVersion);
	load<boost::archive::text_wiarchive>(txtIn, iVersion);

	StdXmlArchiveOut outXml(outFs);
	StdXmlArchiveIn inXml(inFs);

	save<StdXmlArchiveOut>(outXml, iVersion);
	load<StdXmlArchiveIn>(inXml, iVersion);


	boost::filesystem::ofstream outFsStd(_T(""));
	boost::filesystem::ifstream inFsStd(_T(""));

	boost::archive::binary_oarchive outBin(outFsStd);
	boost::archive::binary_iarchive inBin(inFsStd);

	save<boost::archive::binary_oarchive>(outBin, iVersion);
	load<boost::archive::binary_iarchive>(inBin, iVersion);
}

//*******************************
//             TEST
//*******************************
#ifdef _DEBUG

#include "DrawingObject/CDrawingPoint.h"
#include "DrawingObject/CDrawingLine.h"
#include "DefinitionObject/CPartsDef.h"
#include "CProjectCtrl.h"



void TEST_CDrawingParts_ADD(CDrawingParts* pGroup,StdString strName, int iNo, DRAWING_TYPE eType)
{
    StdStringStream strmName;
    strmName << strName << _T("_") << StdFormat(_T("%03d")) % iNo;

    if (eType == DT_POINT)
    {
        auto pPt = std::make_shared< CDrawingPoint>();
        pPt->SetPoint((double)iNo * 0.1, (double)iNo * 0.2);
        pPt->SetName(strmName.str().c_str());
        pGroup->AddData(pPt, true, false);
    }
    else if (eType == DT_LINE)
    {
        auto pLn = std::make_shared< CDrawingLine>();
        pLn->SetPoint((double)iNo * 0.1, (double)iNo * 0.2, (double)iNo * 0.3, (double)iNo * 0.4);
        pLn->SetName(strmName.str().c_str());
    }

}


//-----------------------------------------------
void TEST_CDrawingParts_00(CDrawingParts* pGroup)
{
    //以下のデータを作成する
    // (pGroup)
    //     ----- Point("Pt_00_01")  
    //     ----- Point("Pt_00_01")  

    //データの追加
    auto pPt_00_01 = std::make_shared< CDrawingPoint>();
    auto pPt_00_02 = std::make_shared< CDrawingPoint>();

    pPt_00_01->SetPoint(0.5, 0.6);
    pPt_00_02->SetPoint(0.7, 0.8);


    pPt_00_01->SetName(_T("Pt_00_01"));
    pPt_00_02->SetName(_T("Pt_00_02"));

    pGroup->AddData(pPt_00_01, true, false);
    pGroup->AddData(pPt_00_02, true, false);
    pGroup->SetName(_T("Group_00"));
}


//-----------------------------------------------
void TEST_CDrawingParts_01(CDrawingParts* pGroup)
{

    //以下のデータを作成する
    // (pGroup)
    //     ----- Point("Pt_01_01")  
    //     ----- Line ("Ln_01_02")  
    //     ----- Point("Pt_01_03")  

    //データの追加

    auto pPt_01 = std::make_shared< CDrawingPoint>();
    auto pLn_02 = std::make_shared< CDrawingLine>();
    auto pPt_03 = std::make_shared< CDrawingPoint>();

    POINT2D pt1(0.1111,0.1122);
    POINT2D pt2(0.2211,0.2222);

    pPt_01->SetPoint(0.1, 0.2);
    pLn_02->SetPoint1(pt1);
    pLn_02->SetPoint2(pt2);
    pPt_03->SetPoint(0.1, 0.2);


    pPt_01->SetName(_T("Pt_01_01"));
    pLn_02->SetName(_T("Ln_01_02"));
    pPt_03->SetName(_T("Pt_01_03"));

    pGroup->AddData(pPt_01 ,true, false);
    pGroup->AddData(pLn_02, true, false);
    pGroup->AddData(pPt_03, true, false);
    pGroup->SetName(_T("Group_01")); //<<<<<名前は付けられない
}

//-----------------------------------------------
void TEST_CDrawingParts_02(CDrawingParts* pGroup)
{
    //データの追加

    auto pLn_01 = std::make_shared< CDrawingLine>();
    auto pLn_02 = std::make_shared< CDrawingLine>();
    auto pLn_03 = std::make_shared< CDrawingLine>();

    pLn_01->SetPoint(0.1111,0.1122, 0.2211,0.2222);
    pLn_02->SetPoint(0.3311,0.3322, 0.4411,0.4422);
    pLn_03->SetPoint(0.5511,0.5522, 0.6611,0.6622);

    pLn_01->SetName(_T("Line_02_01"));
    pLn_02->SetName(_T("Line_02_02"));
    pLn_03->SetName(_T("Line_02_03"));

    pGroup->AddData(pLn_01, true, false);
    pGroup->AddData(pLn_02, true, false);
    pGroup->AddData(pLn_03, true, false);
    pGroup->SetName(_T("Group_02"));
}


// ガベージコレクションテスト
void TEST_CDrawingParts_Gabage()
{
    /*
    //!< IDガベージコレクション
    bool GarbageCollection(bool bOneShot);

    //!< ガベージコレクション待ち
    void WaitGarbageCollection();

    //!< ガベージコレクション終了
    void StopGarbageCollection();
    */

    CPartsDef ctrlGabage;
    ctrlGabage.SetMaxId(30);

    auto pGrpGabage = ctrlGabage.GetDrawingParts();    


    try
    {
        for (int i = 0; i < 28; i++)
        {
            TEST_CDrawingParts_ADD(pGrpGabage.get(),_T("Gab"), i, DT_POINT);
        }
    }
    catch(MockException &e)
    {
        //ここを通ってはいけない
        STD_DBG(_T("テスト失敗 Error[%d] %s"), e.GetErr(), e.GetErrMsg().c_str());
    }

 
    StdString strGab;

    DB_PRINT(_T("----ガベージコレクションStep1 ----\n"));
    strGab = pGrpGabage->GetStr();
    DB_PRINT(_T("%s\n"), strGab.c_str());
 
    // ここでID １〜２９間での ２９個のデータが作成されているはず


    STD_ASSERT(  pGrpGabage->Delete(1)  ); 
    STD_ASSERT(  pGrpGabage->Delete(3)  ); 
    STD_ASSERT(  pGrpGabage->Delete(8)  ); 
    STD_ASSERT(  pGrpGabage->Delete(20) ); 
    STD_ASSERT(  pGrpGabage->Delete(22) ); 

    DB_PRINT(_T("----ガベージコレクションStep2 ----\n"));
    strGab = pGrpGabage->GetStr();
    DB_PRINT(_T("%s\n"), strGab.c_str());


    

   try
   {
        TEST_CDrawingParts_ADD(pGrpGabage.get(),_T("Add"), 1, DT_POINT);
        TEST_CDrawingParts_ADD(pGrpGabage.get(),_T("Add"), 2, DT_POINT);
        TEST_CDrawingParts_ADD(pGrpGabage.get(),_T("Add"), 3, DT_POINT);
        TEST_CDrawingParts_ADD(pGrpGabage.get(),_T("Add"), 4, DT_POINT);
        TEST_CDrawingParts_ADD(pGrpGabage.get(),_T("Add"), 5, DT_POINT);
   }
   catch(MockException &e)
   {
        //ここを通ってはいけない
        STD_DBG(_T("テスト失敗2 Error[%d] %s"), e.GetErr(), e.GetErrMsg().c_str());
   }

   try
   {
        TEST_CDrawingParts_ADD(pGrpGabage.get(),_T("Add"), 6, DT_POINT);
        //ここを通ってはいけない
        STD_DBG(_T("テスト失敗3 Error"));
   }
   catch(MockException &e)
   {
        //ここを通らなければならない
        STD_DBG(_T("テスト成功 Error[%d] %s"), e.GetErr(), e.GetErrMsg().c_str());
   }

   DB_PRINT(_T("----ガベージコレクションStep3 ----\n"));
   strGab = pGrpGabage->GetStr();
   DB_PRINT(_T("%s\n"), strGab.c_str());

   DB_PRINT(_T("----ガベージコレクション End ----\n"));
}

//-----------------------------------------------

void TestRename()
{
    {
        std::weak_ptr<CDrawingObject> wObj;
        std::map<StdString, std::weak_ptr<CDrawingObject>> mapObj;
        mapObj[_T("TEST_000")] = wObj;
        mapObj[_T("TEST_001")] = wObj;
        mapObj[_T("DDD_002")] = wObj;
        mapObj[_T("DDD_03")] = wObj;
        mapObj[_T("DDD_0005")] = wObj;
        mapObj[_T("DDD")] = wObj;
        mapObj[_T("TEST_007")] = wObj;

        bool bRet;
        StdString strNew;
        bRet = CDrawingParts::CheckConfrictName(&strNew, &mapObj, _T("TEST_001"));

        STD_ASSERT(bRet);
        STD_ASSERT(strNew == _T("TEST_008"));

        bRet = CDrawingParts::CheckConfrictName(&strNew, &mapObj, _T("TEST"));
        STD_ASSERT(!bRet);
    }

    int BreakPoint = 0;
}

//-----------------------------------------------
void TEST_CDrawingParts()
{

    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());
    TestRename();

#if 0
    //コントロールを用意
    //CPartsDef* pCrlMain;

    CPartsDef* pCtrl0;
    CPartsDef* pCtrl1;
    CPartsDef* pCtrl2;

    // GetRefObjを使用するためには ProjectCtrlに登録する必要がある
    CProjectCtrl* pProj = THIS_APP->GetProject();
    pProj->CreateParts(_T("Parts_0"));
    pProj->CreateParts(_T("Parts_1"));
    pProj->CreateParts(_T("Parts_2"));

    pCtrl0 = pProj->GetPartsByName(_T("Parts_0"));
    pCtrl1 = pProj->GetPartsByName(_T("Parts_1"));
    pCtrl2 = pProj->GetPartsByName(_T("Parts_2"));

    CDrawingParts* pGrp1;
    CDrawingParts* pGrp2;

    DB_PRINT(_T("crl1 PartsId %d\n"), pCtrl1->GetPartsId());
    DB_PRINT(_T("crl2 PartsId %d\n"), pCtrl2->GetPartsId());

    //===========================
    //===========================
    // テスト１
    // pCtrl1
    //     (pGrp1)
    //     ----- Point("Pt_01_01")  
    //     ----- Point("Ln_01_02")  
    //     ----- Point("Pt_01_03")  
    //            
    //   参照モードのコピーコンストラクタをテスト
    //   grp2 <- pGrp1
    //   
    //

    //  pCtrl1
    //     (pGrp1)
    pGrp1 = pCtrl1->GetDrawingParts();    
    TEST_CDrawingParts_01(pGrp1);

    StdString strGrp01;
    strGrp01 = pGrp1->GetStr();
    DB_PRINT(_T("%s\n"), strGrp01.c_str());
    //------------------------------------

    //参照モードでコピー
    DB_PRINT(_T("テスト１ 開始\n"));
    DB_PRINT(_T("参照モードでコピー\n"));
    CDrawingParts  grp2(*pGrp1, true);
    
    strGrp01 = grp2.GetStr();
    DB_PRINT(_T("%s\n"), strGrp01.c_str());


    //コピーしたグループの内部オブジェクトが 
    //コピー元を参照していることを確認 
    CDrawingObject* pSrc;
    CDrawingObject* pCpy;

    //オブジェクトが生成されたことを確認
    pSrc = pGrp1->Find(_T("Pt_01_01"));
    pCpy   = grp2.Find(_T("Pt_01_01"));
    STD_ASSERT (pSrc != NULL);
    STD_ASSERT (pCpy != NULL);
    STD_ASSERT (pSrc != pCpy);

    pSrc = pGrp1->Find(_T("Ln_01_02"));
    pCpy   = grp2.Find(_T("Ln_01_02"));
    STD_ASSERT (pSrc != NULL);
    STD_ASSERT (pCpy != NULL);
    STD_ASSERT (pSrc != pCpy);

    //コピーオブジェクトの参照がコピー元と等しいことを確認
    pSrc = pGrp1->Find(_T("Pt_01_01"));
    pCpy   = grp2.Find(_T("Pt_01_01"))->GetRefObj();
    STD_ASSERT (pCpy != NULL);
    STD_ASSERT (pSrc == pCpy);

    pSrc = pGrp1->Find(_T("Ln_01_02"));
    pCpy   = grp2.Find(_T("Ln_01_02"))->GetRefObj();
    STD_ASSERT (pCpy != NULL);
    STD_ASSERT (pSrc == pCpy);

    DB_PRINT(_T("テスト１ 終了\n"));
    //------------------------------------
    //ここから: COPYテスト

    //crl2
    //  (pGrp2)
    //     pGrp2_1
    //
    //  既存のグループにグループを追加する

    //pGrp2
    //   Line_02_01
    //   Line_02_02
    //   Line_02_03;
    //   Pt_02_001
    //   Pt_02_002
    //
    //pGrp2_1
    //   (pGrp1)
    //      Point("Pt_01_01")  
    //      Point("Ln_01_02")  
    //      Point("Pt_01_03")  

    // こんな感じになるはず
    //pGrp2
    //  Line_02_01
    //  Line_02_02
    //  Line_02_03;
    //  Pt_02_001
    //  Pt_02_002
    //      (pGrp1)
    //          Point("Pt_01_01")  
    //          Point("Ln_01_02")  
    //          Point("Pt_01_03")  

    DB_PRINT(_T("テスト２ 開始\n"));
    CDrawingParts*  pGrp2_1;
    pGrp2_1 = new CDrawingParts;


    pGrp2 = pCtrl2->GetDrawingParts();
    TEST_CDrawingParts_02(pGrp2);

    CDrawingPoint* pPt_01 = new CDrawingPoint;
    CDrawingPoint* pPt_02 = new CDrawingPoint;

    pPt_01->SetPoint(11.1, 22.2);
    pPt_02->SetPoint(33.3, 44.4);

    pPt_01->SetName(_T("Pt_02_001"));
    pPt_02->SetName(_T("Pt_02_002"));

    pCtrl2->AddObject(pPt_01);
    pCtrl2->AddObject(pPt_02);


    pGrp2_1->SetPartsDef(pCtrl2);
    pGrp2_1->Copy(*pGrp1);
    pGrp2_1->SetName(_T("Grp1"));
    pCtrl2->AddObject(pGrp2_1);

    DB_PRINT(_T("::Copy_01\n"));
    strGrp01 = pGrp2_1->GetStr();
    DB_PRINT(_T("%s\n"), strGrp01.c_str());

    DB_PRINT(_T("::Copy_02\n"));
    strGrp01 = pGrp2->GetStr();
    DB_PRINT(_T("%s\n"), strGrp01.c_str());

    DB_PRINT(_T("テスト２ 終了\n"));
    //------------------------------------
    // ネストしたグループに対するコピーコンストラクタ

    //CDrawingParts  grp_const(*pGrp2);

    DB_PRINT(_T("テスト ３ 開始\n"));

    //------------------------------------
    //pGrp2
    //  Line_02_01
    //  Line_02_02
    //  Line_02_03
    //  Pt_02_001
    //  Pt_02_002
    //      (pGrp1)
    //          Point("Pt_01_01")  
    //          Point("Ln_01_02")  
    //          Point("Pt_01_03")  


    // pGrp_const <- pGrp2をコピーコンストラクタで生成
    CDrawingParts*  pGrp_const= new CDrawingParts(*pGrp2, true);

    DB_PRINT(_T("::Cypy Construcotr \n"));
    strGrp01 = pGrp_const->GetStr();
    DB_PRINT(_T("%s\n"), strGrp01.c_str());

        
    CDrawingParts*  pGrp0;
    pGrp0 = pCtrl0->GetDrawingParts();
    TEST_CDrawingParts_00(pGrp0);

    CDrawingParts*  pGrp0_1;
    pGrp0_1 = new CDrawingParts;

    //-------------------------------------
    // pGrp0_1 <- pGrp2へ copy 
    pGrp0_1->SetPartsDef(pCtrl0);
    pGrp0_1->Copy(*pGrp2);
    pGrp0_1->SetName(_T("Grp2"));

    //COPY
    pCtrl0->AddObject(pGrp0_1);  //コントロールに設定しないと文字がでない
    DB_PRINT(_T("::pGrp0_1\n"));
    strGrp01 = pGrp0_1->GetStr();
    DB_PRINT(_T("%s\n"), strGrp01.c_str());
    //-------------------------------------



    // pCtrl0
    //   (pGrp0)
    //     ----- Point("Pt_00_01")  
    //     ----- Point("Pt_00_01")  
    //  に 

    //pGrp2
    //  Line_02_01
    //  Line_02_02
    //  Line_02_03;
    //  Pt_02_001
    //  Pt_02_002
    //      (pGrp1)("Grp1")
    //          Point("Pt_01_01")  
    //          Point("Ln_01_02")  
    //          Point("Pt_01_03")  
    // をコピーした pGrp0_1を追加する


    // pCtrl0
    //   (pGrp0)
    //     Pt_00_01  
    //     Pt_00_01  
    //     pGrp2("Grp2")
    //      Line_02_01
    //      Line_02_02
    //      Line_02_03;
    //      Pt_02_001
    //      Pt_02_002
    //      pGrp1("Grp1")
    //          Pt_01_01  
    //          Ln_01_02  
    //          Pt_01_03  

    DB_PRINT(_T("::Copy_Main\n"));
    strGrp01 = pGrp0->GetStr();
    DB_PRINT(_T("%s\n"), strGrp01.c_str());

    DB_PRINT(_T("テスト３ 終了\n"));


    //------------------------------------
    TEST_CDrawingParts_Gabage();

    //------------------------------------

    // 参照変更テスト
    DB_PRINT(_T("変更テスト\n"));

    //変更が検出されていないことを確認
    STD_ASSERT(!pGrp0->IsChangeRef());


    //pCtrl1 pGrp1 ----- Parts_1 (PartsId:D2)
    //pCtrl2 pGrp2 ----- Parts_2


    //Parts1に変更が無いことを確認
    STD_ASSERT(!pGrp1->IsChangeRef());

    //Parts1のデータを変更します
    CDrawingObject* pChgRefObj = pGrp1->Find(_T("Pt_01_03"));

    
    // 値を変える
    STD_ASSERT(pChgRefObj->GetType() == DT_POINT);
    CDrawingPoint* pChgRefPoint = dynamic_cast<CDrawingPoint*>(pChgRefObj);
    pChgRefPoint->GetPointInstance()->AbsMove(&POINT2D(2.0, 4.1));

    //データの直接変更は検出しない 
    STD_ASSERT(!pGrp1->IsChangeRef());
    STD_ASSERT(!pGrp0->IsChangeRef());

    //メソッドによる変更
    pChgRefPoint->SetPoint(POINT2D(7.0, 8.1));

    //自分自身は検出しない
    STD_ASSERT(!pGrp1->IsChangeRef());

    //使用箇所を参照しているところは検出する
    STD_ASSERT(pGrp0->IsChangeRef());

    //使用箇所を取得
    CDrawingObject* pChgObj = pGrp0->Find(_T("Grp2.Grp1.Pt_01_03"));
    STD_ASSERT(pChgObj->GetType() == DT_POINT);

    CDrawingPoint* pChgPoint = dynamic_cast<CDrawingPoint*>(pChgObj);

    // まだ値が変わっていない
    STD_ASSERT_DBL_EQ( pChgPoint->GetPointInstance()->dX, 0.1);
    STD_ASSERT_DBL_EQ( pChgPoint->GetPointInstance()->dY, 0.2);

    pGrp0->UpdateRef();


    STD_ASSERT_DBL_EQ( pChgPoint->GetPointInstance()->dX, 7.0);
    STD_ASSERT_DBL_EQ( pChgPoint->GetPointInstance()->dY, 8.1);


    //----------------------
    //  元データに追加
    //----------------------
    // pCtrl1
    //     (pGrp1)
    //     ----- Point("Pt_01_01")  
    //     ----- Point("Ln_01_02")  
    //     ----- Point("Pt_01_03")  
    //     ----- Point("Pt_01_04")  <<<<<<<


    CDrawingPoint*  pPt_01_04 = new CDrawingPoint;
    pPt_01_04->SetPoint(22.1, 33.2);
    pPt_01_04->SetName(_T("Pt_01_04"));
    pGrp1->AddData(pPt_01_04, true, false);

    DB_PRINT(_T("ADD Group1 \n"));
    DB_PRINT(_T("%s\n"),  pGrp1->GetStr().c_str());

    pGrp2->UpdateRef();
    DB_PRINT(_T("ADD Group1 1 \n"));
    DB_PRINT(_T("%s\n"),  pGrp2->GetStr().c_str());


    pGrp0->UpdateRef();

    DB_PRINT(_T("ADD Group1 2\n"));
    DB_PRINT(_T("%s\n"),  pGrp0->GetStr().c_str());

    CDrawingObject* pAddObj = pGrp0->Find(_T("Grp2.Grp1.Pt_01_04"));
    STD_ASSERT(pAddObj != NULL);
    STD_ASSERT(pAddObj->GetType() == DT_POINT);

    CDrawingPoint* pAddPoint = dynamic_cast<CDrawingPoint*>(pAddObj);
    STD_ASSERT_DBL_EQ( pAddPoint->GetPointInstance()->dX, 22.1);
    STD_ASSERT_DBL_EQ( pAddPoint->GetPointInstance()->dY, 33.2);

    //----------------------
    //  元データを削除
    //----------------------
    CDrawingObject* pDelObj = pGrp1->Find(_T("Pt_01_04"));
    STD_ASSERT(pDelObj != NULL);

    pGrp1->Delete(pDelObj->GetId());

    pGrp0->UpdateRef();

    CDrawingObject* pDelObj2 = pGrp0->Find(_T("Grp2.Grp1.Pt_01_04"));
    STD_ASSERT(pDelObj2 == NULL);

    DB_PRINT(_T("Del Group1 2\n"));
    DB_PRINT(_T("%s\n"),  pGrp0->GetStr().c_str());


    //---------------------
    //  描画テスト
    //---------------------









    delete pCtrl2;
    delete pCtrl1;
    delete pCtrl0;
    //delete pCrlMain;

#endif //0

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}
#endif //_DEBUG