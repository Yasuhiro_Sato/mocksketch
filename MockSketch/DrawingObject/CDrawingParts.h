/**
 * @brief			CDrawingPartsヘッダーファイル
 * @file			CDrawingParts.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef __DRAWING_PARTS_H_
#define __DRAWING_PARTS_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DrawingObject/CDrawingScriptBase.h"
#include "DrawingObject/Primitive/LINE2D.h"
#include "DrawingObject/Primitive/MAT2D.h"

/*---------------------------------------------------*/
/*  classes                                          */
/*---------------------------------------------------*/
struct DISPLAY_LIST;
class  CScriptModule;
class  CLayer;
class CNodeBound;
class CDrawMarker;

/**
 * @class   CDrawingParts
 * @brief  
 */
class CDrawingParts :public CDrawingScriptBase//:public CRTTIClass<CDrawingParts, CDrawingObject, DT_LINE>
{
public:

public:
	//!< コンストラクタ
	CDrawingParts();

	//!< コンストラクタ
    CDrawingParts(int nID);

    //!< コピーコンストラクタ
    //CDrawingParts(const  CDrawingParts&  group);

    //!< コピーコンストラクタ
    CDrawingParts(const  CDrawingParts&  group, bool bRef = true);

    //!< デストラクタ
	virtual ~CDrawingParts();

    //!< コピー
    bool Copy(const  CDrawingParts&  group, bool bFirst = true);

    //!< 生成処理
    virtual bool Create(std::shared_ptr<CIoAccess> pIoAccess,
                        CObjectDef* pDef);

    //!< パラメータコピー
    void ParamCopy();

	//!< 相対移動
	virtual void Move(const POINT2D& pt2D);

	//!< 絶対移動
	virtual void AbsMove(const POINT2D& pt2D);

	//!< 内部データ移動
	virtual void MoveInnerPosition(const POINT2D& pt2D);

    //!< 相対回転
	virtual void Rotate(const POINT2D& pt2D, double dAngle);

    //!< 絶対回転
	virtual void RotateAbs(double dAngle);

    //!< 鏡像
	virtual void Mirror(const LINE2D& line);

    //!< 倍率
	virtual void Scl(const POINT2D& pt2D, double dXScl, double dYScl);

    //!< 行列適用
    virtual void Matrix(const MAT2D& mat2D);
    virtual void MatrixDirect(const MAT2D& mat2D);

    //!< 交点計算
    virtual void CalcIntersection ( std::vector<POINT2D>* pLiat,   
                                    const CDrawingObject* pObj, 
                                    bool bOnline = true,
                                    double dMin = NEAR_ZERO)const;

	//!< 調整
    virtual bool Trim (const POINT2D& ptIntersect, const POINT2D& ptClick, bool bReverse);

    //!< 分割
    virtual std::shared_ptr<CDrawingObject>  Break (const POINT2D& ptBreak);

	//!< 範囲
    virtual bool IsInner( const RECT2D& rcArea, bool bPart) const;

    virtual bool IsIgnoreSelPart() const override;

     //!< 描画
    virtual void Draw(CDrawingView* pView, 
                      const std::shared_ptr<CDrawingObject> pParentObject,
                      E_MOUSE_OVER_TYPE eMouseOver = E_MO_NO,
                      int iLayerId = -1) override;

    //!< 描画削除
    virtual void DeleteView(CDrawingView* pView, 
                            const std::shared_ptr<CDrawingObject> pParentObject,
                            int iLayerId = -1) override;

    //!< 実行インスタンス描画
    void DrawView(CDrawingView* pView) const;

    //!< 有効判定
    bool IsValid() const;

    //!< オブジェクト追加
    bool AddData(std::shared_ptr<CDrawingObject>, bool bCreateId, bool bKeepUnfindId, bool bKeepSelect = false);

    //!< コントロール設定
    virtual void SetPartsDef(CPartsDef* pCtrl);

    //!< マトリクス取得(AS)
    virtual MAT2D GetMatrix() const override {return m_mat2D;}

    virtual const MAT2D* GetMatrix2() const{return &m_mat2D;}

    //!< 描画用（画面座標変換用）マトリクス取得
    virtual const MAT2D* GetDrawingMatrix()const override {return  &m_matDraw;}

    //!< 描画用（画面座標変換用）マトリクス取得(AS)
    virtual MAT2D GetDrawingMatrix2()const{return  m_matDraw;}


    //!< ID割り当て
    virtual int AssignId(bool bKeepUnfindId);

    //!< ID割り当て
    virtual int _AssignId(bool bFirstSet, bool bKeepUnfindId);

    //!< ユーザープロパティ値更新
    void UpdateAllUserPropertyValue(CDrawingParts* pGroup = NULL);

    //!< 分解
    virtual bool Disassemble(CPartsDef* pDef,  bool bSetUndo);

    //-------------------------
    // リフレクション設定
    //-------------------------

    //!< 位置設定
    void  SetPoint(const POINT2D& ptAbs);

    //!< 位置取得
    const POINT2D&  GetPoint()  const;

    //!< 位置取得
    void  GetPoint2(POINT2D& pos)  const;

    //!< 点リスト取得
    void  GetPointList(std::vector<POINT2D>* pLstPoint) const;

    //!< 角度設定
    void SetAngle(double dAngle);

    //!< 角度取得
    double  GetAngle()  const       { return m_dAngle;}

    //!< グループ色使用の有無設定
    void SetUseGroupColor(bool bUse){ m_bUseGroupColor = bUse;}

    //!< グループ色使用の有無取得
    bool GetUseGroupColor() const { return m_bUseGroupColor;}

    //!< グループ設定
    void SetGroup( const CPartsDef* pCtrlRef, bool bRoot = false);

    //!< グループ展開
    void ExpandGroup(CDrawingParts* pGroupObject, std::vector<CDrawingObject*>* pListObject,  bool bDrawPoint);

    void SetOperationInheritance(bool bInheritance){m_bOperationInheritance = bInheritance;}

    bool GetOperationInheritance(){return m_bOperationInheritance;}
                                    
    //!< 実行グループID設定
    //void SetExecGroupId(int iId);

    //!< 実行グループID取得
    //int  GetExecGroupId() const;

     //!< リフレクション設定
    static void RegisterReflection();

    //!< スナップ点取得
    virtual  bool GetSnapList(std::list<SNAP_DATA>* pLstSnap)const;


    //--------------------------

    //-------------------
    // グループ
    //-------------------
    //<! IDによる名称取得
    StdString  GetNameById(int iId); 

    //<! IDによる名称設定
    bool  SetIdName(int iId,  LPCTSTR strName); 

    //<!  削除
    bool  Delete(int iId, bool bHandover = false); 

    //<!  入替
    bool  Replace(int iId, std::shared_ptr<CDrawingObject> pObj);

    //<! 検索
    std::shared_ptr<CDrawingObject>  Find(int iId) const; 
    std::weak_ptr<CDrawingObject> FindWeak(int iId) const; 

    //<! 検索
    std::shared_ptr<CDrawingObject>  Find(StdString strName); 

    //!< オブジェクト取得(名称) AS
    std::shared_ptr<CDrawingObject> GetObjectByName(StdString& strName){return Find(strName);}

    //!< オブジェクト取得(名称) AS
    std::shared_ptr<CDrawingObject> GetObjectByNameS(std::string& strName);

    //<! 名称リスト取得
    void GetNmaeList(std::vector<StdString>* pList); 

    //<! マップ取得
    //std::map<int , std::weak_ptr<CDrawingObject>>* GetList(); 

    //<! リスト取得
    std::deque<std::shared_ptr<CDrawingObject>>* GetList(); 

    //!< IDマップ生成
    void  CreateIdMap();

    //<!  全削除
    bool  DeleteAll(); 

    //!< データ更新
    //bool UpdateGroup();

    //!< グループ内データ更新カウント
    void AddGroupChg();

    //!< グループ変更カウント
    int   GetChgGroupCnt(){return  m_iChgGroup;}

    //!< オブジェクト(グループ)再設定
    void ResetParent();

    //!< 親オブジェクト設定
    void SetParentShared(CDrawingParts* pSrc);

    //-------------------
    // ガベージコレクション
    //-------------------
    //!< IDガベージコレクション
    bool GarbageCollection(bool bOneShot);

    //!< ガベージコレクション待ち
    void WaitGarbageCollection();

    //!< ガベージコレクション終了
    void StopGarbageCollection();
    //-------------------

    //!< 部品使用有無
    bool IsIncludeParts(const CPartsDef* pRefCtrl) const;

    //!< 部品使用箇所削除
    bool DeleteUsingParts(const CPartsDef* pRefCtrl);

    //!< 領域取得
    virtual RECT2D GetBounds()  const;

    //!< 描画領域取得
    RECT2D GetViewBounds()  const;

    void CalcFramePoint(std::vector<POINT2D>* pLst) const;
  
    //!< 距離
    virtual double Distance(const POINT2D& pt) const;

    //-------------------
    // ドラッグ用マーカ
    //-------------------
    //!< マーカ初期化 true:マーカあり
    virtual bool InitNodeMarker(CNodeMarker* pMarker) override;

    //!< マーカ選択
    virtual void SelectNodeMarker(CNodeMarker* pMarker,
                                     StdString strMarkerId) override;

    //!< マーカ移動
    virtual void MoveNodeMarker(CNodeMarker* pMarker,
                                    SNAP_DATA* pSnap,
                                    StdString strMarkerId,
                                    MOUSE_MOVE_POS posMouse) override;

    //!< マーカ開放
    virtual bool ReleaseNodeMarker(CNodeMarker* pMarker, StdString strMarkerId, MOUSE_MOVE_POS posMouse) override;

    //!< マーカ開放
    virtual void ChangeNode(StdString strMarkerId, E_NODE_CHANGE_TYPE eType) override;

    //--------------
    //    接続
    //--------------
    virtual bool CreateNodeData(int iConnectionId) override;
    virtual CNodeData* GetNodeData(int iIndex) override;
    virtual CNodeData* GetNodeDataConst(int iIndex) const override;

    virtual int GetCopySrcObjectId() const override{return m_iTmpConnectionObjId;}
    virtual void ResetCopySrcObjectId() override{m_iTmpConnectionObjId = -1;}
    virtual void SetCopyNodeDataMode(bool bSet) override;
    virtual bool GetCopyNodeDataMode() const override{return m_bCopyNodeDataMode;}
    virtual bool UpdateNodeSocketData(CDrawingObject* pTmpObj = NULL,
                                  CPartsDef* pDef = NULL);

    virtual int GetNodeDataNum() const override;
    virtual int _GetConnectionId(int iIndex) const;
    //virtual bool GetNodeDataPos(POINT2D* pt, int iConnectionId) const override;
    virtual bool OnUpdateNodeSocketData(const POINT2D* pPt,
        const CDrawingObject* pSocketObj,
        int iSocketIndex,
        int iIndex,
        std::set<int>* pList,
        CUndoAction* pUndo
    ) override;

    virtual RECT2D::E_EDGE InquireConnectDirection(std::map<RECT2D::E_EDGE, LINE2D>* pDir, 
            RECT2D* pRc,
            int iConnectionId ) const override;

    RECT2D::E_EDGE _InquireConnectDirection2(std::map<RECT2D::E_EDGE, LINE2D>* pDir,
                                                               RECT2D* pRc,
                                                               CDrawingObject* pObj ) const; 

    //-------------------------
    // 参照
    //-------------------------
    //!< 参照元変更の有無
    virtual bool IsChangeRef();

    //!< 参照元の取得
    virtual CDrawingObject* UpdateRef();

    //!< ロード終了後処理
    virtual void LoadAfter(CPartsDef* pDef) override;

    //-------------------
    // Field用
    //-------------------
    //エリア選択 For AS
    bool GetAreaObjects(std::vector<CDrawingObject*>* pLstObjects, 
                    POINT2D pt1, POINT2D pt2, 
                    DRAWING_TYPE eType);

    //-------------------
    // デバッグ用
    //-------------------
    StdString GetStr(StdString strSpc) const;

    virtual StdString GetStr() const;

    //描画順序
    void ChangeDispOrder(VIEW_COMMON::E_DISP_ORDER order, int iId);

    virtual void DebugPrintOnSelect();

    static bool CheckConfrictName(StdString* pNewName,
        std::map<StdString, std::weak_ptr<CDrawingObject>>* pMap,
        const StdString& strName);

protected:
friend CDrawingParts;

    //!< プロパティ初期化
    virtual TREE_GRID_ITEM*  _CreateStdPropertyTree();

    //!< 検索 (先頭ピリオドまで）
    std::shared_ptr<CDrawingObject>  FindFirst(StdString strName) const;

    //!< パーツ元の有無
    //bool  IsOrgExist() const;

    //-------------------
    //!< プロパティ変更
    //-------------------
    static bool PropPos         (CStdPropertyItem* pData, void* pObj);
    static bool PropAngle       (CStdPropertyItem* pData, void* pObj);
    static bool PropUseColor    (CStdPropertyItem* pData, void* pObj);
    static bool PropOperationInheritance(CStdPropertyItem* pData, void* pObj);   
    //-------------------

    //!< 線描画(マウスオーバー時)
    void DrawOver(CDrawingView* pView, bool bOver);

    //!< コピー
    bool _Copy(CDrawingParts*  pSrc, std::shared_ptr<CDrawingParts>  pDst);

    void _CreateNodeDataDataList(std::vector<int>* list) const  ;

	void Dummy();

protected:
    //-----------------------------
    // 保存データ
    //-----------------------------
    
    //!< 位置
    POINT2D                                 m_Pt;

    //!< 角度
    double                                  m_dAngle;

    //!< 倍率
    double                                  m_dSclX;
    double                                  m_dSclY;


    //!< アファイン変換マトリックス
    MAT2D                                   m_mat2D;

    //!< グループ色使用有無
    bool                                    m_bUseGroupColor;

    //!< 操作継承 
    //!< この部品を使用しが部品でも操作可能とする(ボタン、ノード、テキスト等)
    bool                                    m_bOperationInheritance;
    //-----------------------------

    std::deque<std::shared_ptr<CDrawingObject>>   m_lstObjects; //実体


    //-----------------------------
    // 保存不要データ
    //-----------------------------
    //!< ID検索用マップ
    std::map<int, std::weak_ptr<CDrawingObject>>  m_mapObjects;
 
    //!< 描画用（画面座標変換用）マトリクス  //保存不要
    mutable MAT2D                            m_matDraw;

    //!< 名称検索用マップ(参照)
    std::map<StdString , std::weak_ptr<CDrawingObject>>    m_mapObjectName;

    //!< 子グループ検索用リスト(参照) setはweak_ptrでは使用できない
    //std::set< std::weak_ptr<CDrawingParts>>                m_lstGroup;
    std::set< std::shared_ptr<CDrawingParts>>                m_lstGroup;

    //!< 接続データIDリスト
    mutable std::vector<int>                  m_lstConnection;

    //!< 子グループ検索用リスト(参照)
    //std::multimap<double, CDrawingParts*>    m_lstGroup;


    //!< ガベージコレクション終了
    bool                                     m_bStopGabage;

    //!< ガベージコレクション実行中
    bool                                     m_bExecGabage;

    //!< グループ変更カウント
	mutable int                                      m_iChgGroup;
    
    std::unique_ptr<CNodeBound>              m_psNodeBound;

    bool m_bCopyNodeDataMode;
    int   m_iTmpConnectionObjId;

private:
    friend class boost::serialization::access;  

	BOOST_SERIALIZATION_SPLIT_MEMBER();

	template<class Archive>
	void load(Archive& ar, const unsigned int version);

	template<class Archive>
	void save(Archive& ar, const unsigned int version) const;

};

BOOST_CLASS_VERSION(CDrawingParts, 0);
#endif // __DRAWING_GROUTP_H_

