/**
 * @brief			CDrawingPoint実装ファイル
 * @file			CDrawingPoint.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:02:47
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CDrawingPoint.h"
#include "./CDrawingLine.h"
#include "./CDrawingCircle.h"
#include "./CDrawingText.h"
#include "./CDrawingParts.h"
#include "./CDrawingDim.h"
#include "./CDrawingEllipse.h"
#include "./CDrawingSpline.h"
#include "./CDrawingCompositionLine.h"
#include "DefinitionObject/View/CDrawingView.h"
#include "DefinitionObject/CPartsDef.h"
#include "Utility/CUtility.h"
#include "System/CSystem.h"
#include <math.h>
#include <boost/serialization/export.hpp> 
BOOST_CLASS_EXPORT(CDrawingPoint);

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 * コンストラクタ
 */

CDrawingPoint::CDrawingPoint():
CDrawingObject()
{
    m_crObj     = DRAW_CONFIG->crPoint;
    m_eType     = DT_POINT;
	m_ePointType = DRAW_CONFIG->GetPointType();
    m_iPropFeatures = P_BASE; 
}

/**
 * コンストラクタ
 */
CDrawingPoint::CDrawingPoint(int nID):
CDrawingObject( nID, DT_POINT)
{
    m_crObj     = DRAW_CONFIG->crPoint;
	m_ePointType = POINT_CROSS;
	m_iPropFeatures = P_BASE;
	m_ePointType = DRAW_CONFIG->GetPointType();
}

/**
 * コピーコンストラクタ
 */
CDrawingPoint::CDrawingPoint(const CDrawingPoint& Obj):
CDrawingObject( Obj)
{
    m_eType     = DT_POINT;
    m_iPropFeatures = P_BASE; 
	m_ePointType = Obj.m_ePointType;
    m_PtPos     = Obj.m_PtPos;

}

/**
 * デストラクタ
 */

CDrawingPoint::~CDrawingPoint()
{

}

/**
 *  @brief  回転.
 *  @param  [in]    pPt2D    回転中心
 *  @param  [in]    dAngle   回転角
 *  @retval     なし
 *  @note
 */
void CDrawingPoint::Rotate(const POINT2D& pt2D, double dAngle)
{
    AddChgCnt();
    m_PtPos.Rotate(pt2D, dAngle);
}

/**
 *  @brief  相対移動.
 *  @param  [in]    pPt2D   移動量
 *  @retval         なし
 *  @note
 */
void CDrawingPoint::Move(const POINT2D& pt2D) 
{
    AddChgCnt();
    m_PtPos = m_PtPos + pt2D;
}


/**
 *  @brief  位置設定.
 *  @param  [in]    ptPos   設定位置
 *  @retval         なし
 *  @note
 */
void  CDrawingPoint::SetPoint(const POINT2D& ptPos)        
{
    AddChgCnt();
    m_PtPos = ptPos;
} 

/**
 *  @brief  位置設定.
 *  @param  [in]    dX   X座標
 *  @param  [in]    dY   X座標
 *  @retval         なし
 *  @note
 */
void  CDrawingPoint::SetPoint(double dX, double dY)        
{
    AddChgCnt();
    m_PtPos.dX = dX;
    m_PtPos.dY = dY;
} 

/**
 *  @brief  鏡像.
 *  @param  [in]    line   対称軸
 *  @retval         なし
 *  @note
 */
void CDrawingPoint::Mirror(const LINE2D& line)
{
    AddChgCnt();
    m_PtPos.Mirror(line);
}

/**
 *  @brief  倍率.
 *  @param  [in]    pPt2D   拡大中心
 *  @param  [in]    dSclX   X倍率
 *  @param  [in]    dSclY   Y倍率
 *  @retval         なし
 *  @note
 */
void CDrawingPoint::Scl(const POINT2D& pt2D, double dXScl, double dYScl)
{
    AddChgCnt();
    m_PtPos.Scl(pt2D, dXScl, dYScl);
}


/**
 *  @brief  行列.
 *  @param  [in]    mat2D   適用する行列
 *  @retval         なし
 *  @note
 */
void CDrawingPoint::Matrix(const MAT2D& mat2D)
{
    AddChgCnt();
    m_PtPos.Matrix( mat2D);
}

/**
 *  @brief  交点計算.
 *  @param  [out]   pList   交点のリスト
 *  @param  [in]    pObj    交差するオブジェクト
 *  @retval         なし
 *  @note
 */
void CDrawingPoint::CalcIntersection ( std::vector<POINT2D>* pList,   
                                const CDrawingObject* pObj,
                                bool bOnline,
                                double dMin) const
{
    CalcIntersectionPoint(pList, pObj, m_PtPos, bOnline, dMin);
}

/**
 *  @brief  描画.
 *  @param  [in]  pView       表示View
 *  @param  [in]  bMouseOver  true オブジェクト上にマウスあり
 *  @retval         なし
 *  @note
 */
void CDrawingPoint::Draw(CDrawingView* pView, 
                         const std::shared_ptr<CDrawingObject> pParentObject,   
                         E_MOUSE_OVER_TYPE eMouseOver,
                        int iLayerId)
{
    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }

    COLORREF crObj;
    int      iID;
    if (IsBelongToRoot(pParentObject.get()))
    {
        iID   = m_nId;
        if (IsSelect())
        {
            //選択色
            crObj = DRAW_CONFIG->GetSelColor();
        }
        else
        {
           //crObj = m_crObj;
            crObj = DRAW_CONFIG->GetPointColor();
        }
    }
    else
    {
        const CDrawingObject* pTopObj = CDrawingObject::GetTopDraw(pParentObject.get());
        if (pTopObj == NULL)
        {
            return;
        }

        if (pParentObject->GetType() == DT_REFERENCE)
        {
            iID = pTopObj->GetId();
        }
        else
        {
            iID = m_nId;
        }
        
        if (pTopObj->IsSelect())
        {
            //選択色
            crObj = DRAW_CONFIG->GetSelColor();
        }
        else
        {
            //crObj = pObj->GetColor();
            crObj = DRAW_CONFIG->GetPointColor();
        }
    }
    _Point( pView, pParentObject.get(), crObj, iID, eMouseOver, iLayerId);
}

/**
 *  @brief  描画削除.
 *  @param  [in]    pView   表示View
 *  @retval         なし
 *  @note
 */
void CDrawingPoint::DeleteView(CDrawingView* pView, 
                               const std::shared_ptr<CDrawingObject> pParentObject,
                               int iLayerId)
{
    //背景色
    COLORREF crObj;
    crObj = DRAW_CONFIG->GetBackColor();

    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }

    _Point( pView, pParentObject.get(), crObj, 0, E_MO_NO, iLayerId);
}

/**
 *  @brief      リフレクション設定.
 *  @param      なし
 *  @retval     なし
 *  @note
 */
void CDrawingPoint::RegisterReflection()
{
    /*
	//RegisterProperty<POINT2D>       ( _T("Point")   , &GetPoint     , &SetPoint );
	RegisterProperty<POINT2D>       ( "Point"  , &GetPoint     , &SetPoint );
    */
}

/**
 *  @brief   スナップ点取得.
 *  @param   [out] pLstSnap スナップ点リスト
 *  @retval  true: スナップ点あり
 *  @note
 */
bool CDrawingPoint::GetSnapList(std::list<SNAP_DATA>* pLstSnap)const 
{
    using namespace VIEW_COMMON;

    DWORD dwSnap = DRAW_CONFIG->GetSnapType();
    bool bRet = false;

    STD_ASSERT(pLstSnap);

    if (!pLstSnap)
    {
        return false;
    }

    pLstSnap->clear();
    SNAP_DATA snapData;
    snapData.bKeepSelect = false;

    if (dwSnap & SNP_POINT)
    {
        snapData.eSnapType = SNP_POINT;
        snapData.pt = m_PtPos;
        pLstSnap->push_back(snapData);
        bRet = true;
    }
    return bRet;
}

/**
 *  @brief   範囲.
 *  @param   [in] p1 1点目
 *  @param   [in] p2 2点目
 *  @retval  true:2点で示される矩形内に点が存在する 
 *  @note
 */
bool CDrawingPoint::IsInner( const RECT2D& rcArea, bool bPart) const
{
    const MAT2D* pMat =  GetParentMatrix();
    POINT2D tmpPt(m_PtPos);
    if (pMat != NULL)
    {
        tmpPt.Matrix(*pMat);
    }

    if (rcArea.IsInside(tmpPt))
    {
        return true;
    }
    return false;
}


/**
 *  @brief   点描画
 *  @param   [in] pView 表示View
 *  @param   [in] crPen 描画色
 *  @param   [in] iId   描画ID
 *  @retval  なし
 *  @note
 */
void CDrawingPoint::_Point( CDrawingView* pView, 
                          const CDrawingObject* pParentObject,  
                          COLORREF crPen, 
                          int iId, 
                          E_MOUSE_OVER_TYPE eMouseOver,
                          int iLayerId) const
{
    //とりあえずクリッピングしない
    STD_ASSERT(pView != 0);
    POINT  Pt;

    //-------------------
    // MATRIX
    //-------------------
    POINT2D ptDraw = m_PtPos;
    MAT2D matDraw;

    if (pParentObject )
    {
        const MAT2D* pMat = pParentObject->GetDrawingMatrix();
        if (pMat)
        {
            matDraw = *pMat;
        }
    }

    if (m_pMatOffset)
    {
        matDraw = matDraw * (*m_pMatOffset);
    }
    ptDraw = ptDraw * (matDraw);

    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }

    pView->ConvWorld2Scr(&Pt, ptDraw, iLayerId);
    _ScrPoint( pView, Pt, crPen, iId, eMouseOver, iLayerId);
}


/**
 *  @brief   描画(マウスオーバー時).
 *  @param   [in] pView 表示View
 *  @param   [in] bOver true:開始 false:終了
 *  @retval  なし
 *  @note
 */
void CDrawingPoint::DrawOver(CDrawingView* pView, bool bOver)
{

}

/**
 * 点描画
 * @param   [in]    Pt      座標        
 * @param   [in]    crPt    色        
 * @param   [in]    iId     図形ID           
 * @retval   なし
 * @note	
 */
void CDrawingPoint::_ScrPoint(CDrawingView* pView, 
                             POINT Pt, 
                             COLORREF crPen, 
                             int iId, 
                             E_MOUSE_OVER_TYPE eMouseOver,
                             int iLayerId)  const
{
    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }

    GetLayerCoror(&crPen, pView, iLayerId);

    int iWidth = 1;

    if (eMouseOver == E_MO_SELECTABLE)
    {
        iWidth++;;
    }
    else if (eMouseOver == E_MO_EMPHASIS)
    {
        crPen = DRAW_CONFIG->crEmphasis;
    }
	else if (eMouseOver == E_MO_CONNECTABLE)
	{
		crPen = DRAW_CONFIG->crConnectable;
	}

    pView->SetPen(PS_SOLID,  iWidth, crPen, iId);

    _ScrPoint(pView, Pt);
}


/**
 * 点描画
 * @param   [in]    hDc     デバイスコンテキスト         
 * @param   [in]    Pt      座標        
 * @param   [in]    crPt    色        
 * @retval   なし
 * @note	
 */
inline
void CDrawingPoint::_ScrPoint( CDrawingView* pView, POINT Pt )  const
{
	pView->PointMark(Pt, m_ePointType, DRAW_CONFIG->GetPointSize());
}

void CDrawingPoint::SetPointType(POINT_TYPE ePointType)
{
	m_ePointType = ePointType;
}

POINT_TYPE CDrawingPoint::GetPointType()
{
	return m_ePointType;
}

/**
 *  @brief   プロパティ初期化.
 *  @param   [in] pCtrl オブジェクトコントロールへのポインタ
 *  @retval     なし
 *  @note
 */
TREE_GRID_ITEM*  CDrawingPoint::_CreateStdPropertyTree()
{
 
    NAME_TREE_ITEM<CStdPropertyItem>* pBeforeGroup;
    NAME_TREE_ITEM<CStdPropertyItem>* pNextItem;
    NAME_TREE_ITEM<CStdPropertyItem>* pGroupItem;
    NAME_TREE<CStdPropertyItem>* pTree;
    //int iId;
    CStdPropertyItem* pItem;
    boost::tuple< CStdPropertyItem*, void*> pairVal;


    pBeforeGroup = CDrawingObject::_CreateStdPropertyTree();
    pTree = m_psProperty->GetTreeInstance();

    pGroupItem  = pTree->AddNext(pBeforeGroup, NULL, GET_STR(STR_PRO_POINT), _T("Point"));


    //------------
    //位置
    //------------
    CStdPropertyItemDef DefPos(PROP_POINT2D, 
        GET_STR(STR_PRO_POS)   , 
        _T("Pos"), 
        GET_STR(STR_PRO_INFO_POS), 
        true,
        DISP_UNIT,
        POINT2D());

    pItem = new CStdPropertyItem(DefPos, PropPos, m_psProperty, NULL, &m_PtPos);
    pNextItem = pTree->AddChild(pGroupItem, pItem, DefPos.strDspName, pItem->GetName(), 2);

    return pGroupItem;

}


/**
 *  @brief   プロパティ変更(位置)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingPoint::PropPos (CStdPropertyItem* pData, void* pObj)
{
    CDrawingPoint* pDrawing = reinterpret_cast<CDrawingPoint*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->m_pCtrl->HideObject(pDrawing->m_nId);
        pDrawing->SetPoint(pData->anyData.GetPoint());
        pDrawing->m_pCtrl->Draw(pDrawing->m_nId);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}


/**
 *  @brief   参照データに基づいて更新.
 *  @param   
 *  @retval  
 *  @note
 */
CDrawingObject* CDrawingPoint::UpdateRef()
{
    CDrawingObject*      pRef;

    pRef = CDrawingObject::UpdateRef();

    if (pRef == NULL)
    {
        return NULL;
    }

    CDrawingPoint* pPoint = dynamic_cast<CDrawingPoint*>(pRef);

    STD_ASSERT(pPoint != NULL);

    if (pPoint == NULL)
    {
        return false;
    }

    m_PtPos = pPoint->m_PtPos;

    m_iChgCnt = pRef->GetChgCnt();
    return pRef;
}

/**
 * @brief   領域取得
 * @param   なし
 * @retval  なし
 * @note	描画領域を取得する
 */
RECT2D CDrawingPoint::GetBounds() const
{
    RECT2D rc(m_PtPos, m_PtPos);
    return rc;
}

/**
 * @brief   距離
 * @param   [in] pt 
 * @retval  近接点からの距離
 * @note
 */
double CDrawingPoint::Distance(const POINT2D& pt) const
{
    return m_PtPos.Distance(pt);
}

//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void TEST_CDrawingPoint()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}
#endif //_DEBUG