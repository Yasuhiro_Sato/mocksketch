/**
 * @brief			CDrawingPointヘッダーファイル
 * @file			CDrawingPoint.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:02:46
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#if !defined(EA_00ED2CBE_677D_4f62_891C_BB0BA32F8CD7__INCLUDED_)
#define EA_00ED2CBE_677D_4f62_891C_BB0BA32F8CD7__INCLUDED_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DrawingObject/CDrawingObject.h"

enum POINT_TYPE;

/**
 * @class   CDrawingPoint
 * @brief   点データ                     
   
 */
class CDrawingPoint :public CDrawingObject//:public CRTTIClass<CDrawingPoint, CDrawingObject, DT_POINT>
{

public:
	//!< コンストラクタ
	CDrawingPoint();

	//!< コンストラクタ
	CDrawingPoint(int nID);

	//!< デストラクタ
	virtual ~CDrawingPoint();

	//!< コピーコンストラクター
    CDrawingPoint(const CDrawingPoint& Obj);

    //!< 相対移動
	virtual void Move(const POINT2D& pt2D);

    //!< 回転
	virtual void Rotate(const POINT2D& pt2D, double dAngle);

    //!< 鏡像
	virtual void Mirror(const LINE2D& line);

    //!< 倍率
	virtual void Scl(const POINT2D& pt2D, double dXScl, double dYScl);

    //!< 行列適用
    virtual void Matrix(const MAT2D& mat2D);

    //!< 交点計算
    virtual void CalcIntersection ( std::vector<POINT2D>* pLiat,   
                                    const CDrawingObject* pObj, 
                                    bool bOnline = true,
                                    double dMin = NEAR_ZERO) const;

	//!< 範囲
    virtual bool IsInner( const RECT2D& rcArea, bool bPart) const;

    //!< 描画
    virtual void Draw(CDrawingView* pView,
                      const std::shared_ptr<CDrawingObject> pParentObject,
                      E_MOUSE_OVER_TYPE eMouseOver = E_MO_NO,
                      int iLayerId = -1) override;

    //!< 描画削除
    virtual void DeleteView(CDrawingView* pView,
                            const std::shared_ptr<CDrawingObject> pParentObject,
                            int iLayerId = -1) override;

    //!< 領域取得
    virtual RECT2D GetBounds() const;

    //!< 距離
    virtual double Distance(const POINT2D& pt) const;

    //!<プロパティ設定
    //virtual void SetProperty(CMFCPropertyGridCtrl *pGrid, UINT uiMask);

    //-------------------------
    // リフレクション設定
    //-------------------------
friend CDrawingPoint;

    //!< 位置設定
    void  SetPoint(const POINT2D& ptPos); 

    //!< 位置設定
    void  SetPoint(double dX, double dY);

   //!< 位置取得
    POINT2D&  GetPoint(){return m_PtPos;}
    const POINT2D&  GetPoint()const{return m_PtPos;}

    //!< 位置取得
    POINT2D*  GetPointInstance()        {return &m_PtPos;}
    const POINT2D*  GetPointInstance() const  {return &m_PtPos;}

    //!< プロパティ初期化
    TREE_GRID_ITEM*  _CreateStdPropertyTree();

    //!< プロパティ変更(位置)
    static bool PropPos    (CStdPropertyItem* pData, void* pObj);

    //!< リフレクション設定
    static void RegisterReflection();

    //!< スナップ点取得
    virtual  bool GetSnapList(std::list<SNAP_DATA>* pLstSnap)const;

	void SetPointType(POINT_TYPE ePointType);
	POINT_TYPE GetPointType();



    //-------------------------
    // 参照
    //-------------------------

    //!< 参照データに基づいて更新
    virtual CDrawingObject* UpdateRef();

protected:

    //!< 点描画
    void _Point( CDrawingView* pView, 
                 const CDrawingObject* pParentObject,
                 COLORREF crPen, 
                 int iId,
                 E_MOUSE_OVER_TYPE eMouseOver,
                 int iLayerId) const;

    //!< 点描画(マウスオーバー時)
    virtual void DrawOver(CDrawingView* pView, bool bOver);

    //!< 点描画
    void _ScrPoint(CDrawingView* pView, 
                   POINT Pt, 
                   COLORREF crPen, 
                   int iId,
                   E_MOUSE_OVER_TYPE eMouseOver,
                   int iLayerId) const;
   
    //!< 点描画
    void _ScrPoint( CDrawingView* pView, 
                    POINT Pt) const;


protected:
    //!< 位置
    POINT2D     m_PtPos;
	POINT_TYPE  m_ePointType;

private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
            ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(CDrawingObject);
            SERIALIZATION_BOTH("Pos"      , m_PtPos);
			SERIALIZATION_BOTH("Type", m_ePointType);
		MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }

};
BOOST_CLASS_VERSION(CDrawingPoint, 0);
#endif // !defined(EA_00ED2CBE_677D_4f62_891C_BB0BA32F8CD7__INCLUDED_)
