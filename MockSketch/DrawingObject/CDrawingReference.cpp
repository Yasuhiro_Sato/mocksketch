/**
 * @brief			CDrawingReference実装ファイル
 * @file			CDrawingReference.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MockSketch.h"

#include "DefinitionObject/CReferenceDef.h"
#include "./CDrawingReference.h"
#include "./CDrawingParts.h"
#include "./CDrawingPoint.h"
#include "./CDrawingLine.h"
#include "./CDrawingCircle.h"
#include "./CDrawingSpline.h"
#include "./CDrawingText.h"
#include "./CDrawingNode.h"
#include "./CDrawingDimL.h"
#include "./CDrawingDimH.h"
#include "./CDrawingDimV.h"
#include "./CDrawingDimA.h"
#include "./CDrawingDimD.h"
#include "./CDrawingDimR.h"
#include "./CDrawingDimC.h"
#include "./CDrawingImage.h"
#include "./CDrawingParts.h"
#include "./CDrawingElement.h"
#include "./CDrawingConnectionLine.h"
#include "./CDrawingCompositionLine.h"
#include "./CNodeData.h"

#include "DefinitionObject/View/CDrawingView.h"
#include "DefinitionObject/CPartsDef.h"
#include "Utility/CUtility.h"
#include "View/CUndoAction.h"

#include "System/CSystem.h"
#include "Script/CScriptEngine.h"
#include "Script/CScriptObject.h"
#include "Io/CIoRef.h"
#include "Io/CIoDef.h"

#include "Utility/Script/ScriptHandle.h"

#include <math.h>
#include <boost/serialization/export.hpp> 

#include <angelscript.h>

BOOST_CLASS_EXPORT(CDrawingReference);


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
CDrawingReference::CDrawingReference():
CDrawingParts  (-1)
{
}   

/**
 *  @brief  コンストラクター.
 *  @param  [in] nID 
 *  @retval なし 
 *  @note
 */
CDrawingReference::CDrawingReference(int nID):
CDrawingParts( nID)
{
    m_eType  = DT_REFERENCE;
}
/**
 *  @brief  コピーコンストラクター.
 *  @param  [in] group 
 *  @param  [in] bRef true:参照としてコピー 
 *  @retval なし 
 *  @note    ID,  コントロール
 *           はコピー元を引き継ぎます
 */
CDrawingReference::CDrawingReference(const  CDrawingReference&  group, bool bRef):
CDrawingParts(group, bRef)
{
    m_eType  = DT_REFERENCE;
    if (GetCopyNodeDataMode())
    {
        m_mapConnection.clear();
        for (auto ite = group.m_mapConnection.begin();
             ite != group.m_mapConnection.end();
             ite++)
        {
            int iConnectionId = ite->first;
            m_mapConnection[iConnectionId] = std::make_unique<CNodeData>(*ite->second);
        }
    }
}

/**
 *  @brief  デストラクタ.
 *  @param  なし 
 *  @retval なし 
 *  @note
 */
CDrawingReference::~CDrawingReference()
{
}

/**
 *  @brief   コントロール設定.
 *  @param   [in] pCtrl オブジェクトコントロールへのポインタ
 *  @retval  なし
 *  @note    
 */
void CDrawingReference::SetPartsDef(CPartsDef* pCtrl)
{
    CDrawingObject::SetPartsDef(pCtrl);
}

//!< 生成処理
bool CDrawingReference::Create(std::shared_ptr<CIoAccess> pIoAccess,
                               CObjectDef* pDef)
{
   m_psIo.reset();
    m_psIo = std::make_shared<CIoRef>();

    m_uidObjectDef = pDef->GetPartsId();
    if (!m_psIo->Create(this, pIoAccess))
    {
        STD_DBG(_T("m_psIo->Create fail"));
    }

    m_eExecThreadType = EXEC_COMMON::TH_NONE;

    m_strThreadType = EXEC_COMMON::GetThreadType2Name(EXEC_COMMON::TH_NONE);


    StdString strPropsetName = pDef->GetPropertySetName();
    _SetPropertySetName(strPropsetName);

    InitPropertySetValue(true /*bCreate*/);
    InitUserPropertyValue(true /*bCreate*/);

    return true;
}

//!< 描画
 void CDrawingReference::Draw(CDrawingView* pView, 
                              const std::shared_ptr<CDrawingObject> pParentObject,
                              E_MOUSE_OVER_TYPE eMouseOver,
                              int iLayerId)
 {
    std::shared_ptr<CObjectDef> pDef;
    pDef = GetObjectDef().lock();

    if (!pDef)
    {
        return;
    }

    CReferenceDef* pRef = dynamic_cast<CReferenceDef*>(pDef.get());

    if (!pRef)
    {
        return;
    }
    

    //======================
    // 変換マトリックス生成
    //======================
    if (pParentObject)
    {
        const MAT2D* pMat = pParentObject->GetDrawingMatrix();
        if (pMat)
        {
            m_matDraw = m_mat2D * (*pMat);
        }
        else
        {
            m_matDraw = m_mat2D;
        }
    }
    else
    {
        m_matDraw = m_mat2D;
    }

    if (m_pMatOffset)
    {
        m_matDraw = m_matDraw * (*m_pMatOffset);
    }

    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }

    _SetDrawingParent(pParentObject);

if (pView->GetDebugMode() != CDrawingView::DM_DISABLE)
{
    PrintObj(_T("Ref a:"));
}

    auto pParts = pRef->GetDrawingParts();

    std::deque<std::shared_ptr<CDrawingObject>>* pList;

    pList = pParts->GetList();

    for(auto pObj: *pList)
    {
        pObj->Draw(pView, shared_from_this(), eMouseOver, iLayerId);
        pObj->_SetDrawingParent(shared_from_this());

if (pView->GetDebugMode() == CDrawingView::DM_VIEW)
{
    DRAWING_TYPE eType = pObj->GetType();
    if ((eType != DT_PARTS) &&
        (eType != DT_GROUP) &&
        (eType != DT_REFERENCE))
    {
        pObj->PrintObj(_T("   Ref b:"));
    }
}
    }
 }


 
/**
 *  @brief   プロパティ初期化.
 *  @param   [in] pCtrl オブジェクトコントロールへのポインタ
 *  @retval     なし
 *  @note
 */
TREE_GRID_ITEM*  CDrawingReference::_CreateStdPropertyTree()
{
    NAME_TREE_ITEM<CStdPropertyItem>* pBeforeGroup;
    NAME_TREE_ITEM<CStdPropertyItem>* pNextItem;
    NAME_TREE_ITEM<CStdPropertyItem>* pGroupItem;
    NAME_TREE<CStdPropertyItem>* pTree;
    //int iId;
    CStdPropertyItem* pItem;
    boost::tuple< CStdPropertyItem*, void*> pairVal;


    pBeforeGroup = CDrawingObject::_CreateStdPropertyTree();
    pTree = m_psProperty->GetTreeInstance();


    pGroupItem  = pTree->AddNext(pBeforeGroup, NULL, GET_STR(STR_PRO_REFERENCE), _T("Reference"));

    //------------
    //基準点
    //------------
    CStdPropertyItemDef DefCenter(PROP_POINT2D, 
        GET_STR(STR_PRO_DATUM)   , 
        _T("Center"),
        GET_STR(STR_PRO_INFO_DATUM), 
        true,
        DISP_UNIT,
        POINT2D());

    pItem = new CStdPropertyItem( DefCenter, PropPos, m_psProperty, NULL, &m_Pt);
    pNextItem = pTree->AddChild(pGroupItem, pItem, DefCenter.strDspName, pItem->GetName(), 2);

    //------------
    //角度(deg)
    //------------
    CStdPropertyItemDef DefAngle(PROP_DOUBLE, 
        GET_STR(STR_PRO_ANGLE)   ,
        _T("Angle"),
        GET_STR(STR_PRO_INFO_ANGLE), 
        true,
        DISP_UNIT,
        0.0);

    pItem = new CStdPropertyItem( DefAngle, PropAngle, m_psProperty, NULL, &m_dAngle);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefAngle.strDspName, pItem->GetName());

    //------------
    //固有色
    //------------
    CStdPropertyItemDef DefUseColor(PROP_BOOL, 
        GET_STR(STR_PRO_USE_COLOR)   , 
        _T("UseColor"),
        GET_STR(STR_PRO_INFO_USE_COLOR), 
        false,
        DISP_NONE,
        true);

    pItem = new CStdPropertyItem( DefUseColor, PropUseColor, m_psProperty, NULL, &m_bUseGroupColor);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefUseColor.strDspName, pItem->GetName());

    return pGroupItem;
}

void CDrawingReference::_SetReference()
{
    //m_pRefObject = GetObjectDef();
}


    //!< ロード終了後処理
void CDrawingReference::LoadAfter(CPartsDef* pDef)
{
   // _SetReference();
}



/**
 * @brief   領域取得
 * @param   なし
 * @retval  なし
 * @note	描画領域を取得する
 */
RECT2D CDrawingReference::GetBounds() const
{
    RECT2D rc;

    std::shared_ptr<CObjectDef> pDef;
    pDef = GetObjectDef().lock();

    if (!pDef)
    {
        return rc;
    }

    CReferenceDef* pRef = dynamic_cast<CReferenceDef*>(pDef.get());
    if (!pRef)
    {
        return rc;
    }

    auto pParts = pRef->GetDrawingParts();

    std::map<int , CDrawingObject*>::const_iterator iteMap;

    auto pList = pParts->GetList();

    bool bFirst = true;
    for( auto pObject: *pList)
    {
        if (bFirst)
        {
            rc = pObject->GetBounds();
            bFirst = false;
        }
        else
        {
            rc.ExpandArea(pObject->GetBounds());
        }
    }

    rc.Matrix(m_mat2D);
    return rc;
}


/**
 *  @brief  範囲.
 *  @param  [in] p1 1点目
 *  @param  [in] p2 2点目
 *  @retval  true:2点で示される矩形内に点が存在する 
 *  @note
 */
bool CDrawingReference::IsInner( const RECT2D& rcArea, bool bPart) const
{
    RECT2D rc;

    std::shared_ptr<CObjectDef> pDef;
    pDef = GetObjectDef().lock();

    if (!pDef)
    {
        return false;
    }

    CReferenceDef* pRef = dynamic_cast<CReferenceDef*>(pDef.get());
    if (!pRef)
    {
        return false;
    }
    auto pParts = pRef->GetDrawingParts();

    auto pList = pParts->GetList();
    bool bRet = true;
    if (bPart)
    {
        bRet = false;
    }

    //TODO: 要テスト
    for (auto pObject : *pList)
    {
        auto pTmp = CDrawingObject::CloneShared(pObject.get());
        pTmp->Matrix(m_mat2D);

        bool bRetTmp = pTmp->IsInner(rcArea, bPart);

        if (bPart)
        {
            if (bRetTmp)
            {
                return true;
            }
        }
        else
        {
            if (bRetTmp)
            {
                return false;
            }
        }
    }
    return bRet;
}


//!< 分解
// 自分自身を消す必要があるので CPartsDef->Disassembleから呼び出す
bool CDrawingReference::Disassemble(CPartsDef* pDef, bool bSetUndo)
{
    std::shared_ptr<CObjectDef> pRefDef;
    pRefDef = GetObjectDef().lock();

    if (!pDef)
    {
        return false;
    }

    CUndoAction* pUndo  = pDef->GetUndoAction();

    CReferenceDef* pRef = dynamic_cast<CReferenceDef*>(pRefDef.get());
    if (!pRef)
    {
        return false;
    }

    auto pParts = pRef->GetDrawingParts();

    auto pList = pParts->GetList();

    for(auto pObj: *pList)
    {
        auto pObjNew = CDrawingObject::CloneShared(pObj.get());
        pObjNew->SetLayer(m_iLayerId);
        pObjNew->Matrix(m_mat2D);

        if(bSetUndo)
        {
            pUndo->Add(UD_ADD, pDef, NULL, pObjNew, false);
        }

        pDef->RegisterObject(pObjNew, true, false);
    }

    // ここではUNDOをクローズしないため
    // bSetUndo を使用した場合呼び出し側で  pUndo->Push()を呼び出す必要がある

    return true;
}

CDrawingParts* CDrawingReference::_GetParts() const
{
    std::shared_ptr<CObjectDef> pDef;
    pDef = GetObjectDef().lock();

    if (!pDef)
    {
        return NULL;
    }

    CReferenceDef* pRef = dynamic_cast<CReferenceDef*>(pDef.get());
    if (!pRef)
    {
        return NULL;
    }

    auto pParts = pRef->GetDrawingParts();

    return pParts.get();
}


bool CDrawingReference::CreateNodeData(int iConnectionId)
{
    CDrawingParts* pOrgParts = _GetParts();
    if (!pOrgParts)
    {
        return false;
    }

    CNodeData* pCd = pOrgParts->GetNodeData(iConnectionId);

    if (!pCd)
    {
        return false;
    }

    auto ite = m_mapConnection.find(iConnectionId);
    if (ite != m_mapConnection.end())
    {
        *ite->second  = *pCd;
    }
    else
    {
        m_mapConnection[iConnectionId] = std::make_unique<CNodeData>(*pCd);
    }
    return true;
}

CNodeData* CDrawingReference::GetNodeData(int iConnectionId) 
{
    CDrawingParts* pOrgParts = _GetParts();

    auto ite = m_mapConnection.find(iConnectionId);

    if (ite != m_mapConnection.end())
    {
        CNodeData* pCd = ite->second.get();
        return pCd;   
    }
    return NULL;
}

CNodeData* CDrawingReference::GetNodeDataConst(int iConnectionId) const
{
    CDrawingParts* pOrgParts = _GetParts();

    auto ite = m_mapConnection.find(iConnectionId);

    if (ite != m_mapConnection.end())
    {
        CNodeData* pCd = ite->second.get();
        return pCd;
    }
    return NULL;
}

//ローカル座標を返す
bool CDrawingReference::GetNodeDataPos(POINT2D* pt, int iConnectionId) const
{
    CNodeData* pCd = GetNodeDataConst(iConnectionId);

    if(!pCd)
    {
        return false;
    }

    *pt = pCd->ptPos;
    return true;
}

int CDrawingReference::GetNodeDataNum() const
{ 
    CDrawingParts* pOrgParts = _GetParts();

    if (!pOrgParts)
    {
        return 0;
    }

    return pOrgParts->GetNodeDataNum();
}

int CDrawingReference::_GetConnectionId(int iIndex) const
{
    CDrawingParts* pOrgParts = _GetParts();

    if (!pOrgParts)
    {
        return -1;
    }

    return pOrgParts->_GetConnectionId(iIndex);
}


RECT2D::E_EDGE CDrawingReference::InquireConnectDirection(std::map<RECT2D::E_EDGE, LINE2D>* pDir,
                                                               RECT2D* pRc,
                                                               int iConnectionId ) const 
{
    RECT2D::E_EDGE  eEdge = RECT2D::E_NONE;
    CDrawingParts* pOrgParts = _GetParts();

    if (!pOrgParts)
    {
        return eEdge;
    }

    auto  pObj =  pOrgParts->Find(iConnectionId); 
    if (pObj->GetType() != DT_NODE)
    {
        return  eEdge;      
    }

    return _InquireConnectDirection2( pDir, pRc, pObj.get() ); 
}


//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void TEST_CDrawingReference()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}
#endif //_DEBUG