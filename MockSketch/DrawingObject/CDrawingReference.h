/**
 * @brief        CDrawingReferenceヘッダーファイル
 * @file	        CDrawingReference.h
 * @author           Yasuhiro Sato
 * @date	        07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
  *			
 *
 * $
 * $
 * 
 */
#ifndef __DRAWING_REFERENCE_H_
#define __DRAWING_REFERENCE_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DrawingObject/CDrawingScriptBase.h"
#include "DefinitionObject/CReferenceDef.h"
#include "./CDrawingParts.h"

/*---------------------------------------------------*/
/*  Classe                                           */
/*---------------------------------------------------*/


/**
 * @class   CDrawingReference
 * @brief  
 */
class CDrawingReference :public CDrawingParts//:public CRTTIClass<CDrawingReference, CDrawingObject, DT_LINE>
{
public:

public:
    //!< コンストラクタ
    CDrawingReference();

    //!< コンストラクタ
    CDrawingReference(int nID);

    //!< コピーコンストラクタ
    CDrawingReference(const  CDrawingReference&  group, bool bRef = true);

    //!< デストラクタ
    virtual ~CDrawingReference();

    //!< コントロール設定
    virtual void SetPartsDef(CPartsDef* pCtrl) override;

    //!< 生成処理
    virtual bool Create(std::shared_ptr<CIoAccess> pIoAccess,
                        CObjectDef* pDef) override;

    //!< 描画
    virtual void Draw(CDrawingView* pView,
                       const std::shared_ptr<CDrawingObject> pParentObject,
                       E_MOUSE_OVER_TYPE eMouseOver = E_MO_NO,
                       int iLayerId = -1) override;

    //!< ロード終了後処理
    virtual void LoadAfter(CPartsDef* pDef) override;

    //!< 領域取得
    virtual RECT2D GetBounds()  const override;

	//!< 範囲
    virtual bool IsInner( const RECT2D& rcArea, bool bPart) const override;

    virtual bool Disassemble(CPartsDef* pDef, bool bSetUndo) override;

    //--------------
    //    接続
    //--------------
    virtual bool CreateNodeData(int iConnectionId) override;
    virtual int GetNodeDataNum() const override;
    virtual int _GetConnectionId(int iIndex) const;
    virtual CNodeData* GetNodeData(int iIndex) override;
    virtual CNodeData* GetNodeDataConst(int iIndex) const override;
    virtual bool GetNodeDataPos(POINT2D* pt, int iConnectionId) const;
    virtual RECT2D::E_EDGE InquireConnectDirection(std::map<RECT2D::E_EDGE, LINE2D>* pDir, 
             RECT2D* pRc,
             int iConnectionId ) const override;


protected:
friend CDrawingReference;
    
    void _SetReference();

   CDrawingParts* _GetParts() const;

    //!< プロパティ初期化
    virtual TREE_GRID_ITEM*  _CreateStdPropertyTree();

protected:
    std::map<int, std::unique_ptr<CNodeData>> m_mapConnection;

private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
            ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(CDrawingParts);
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }
};

#endif // 