/**
 * @brief			CDrawingScriptBase実装ファイル
 * @file			CDrawingScriptBase.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CDrawingPoint.h"
#include "./CDrawingLine.h"
#include "./CDrawingScriptBase.h"
#include "./CDrawingParts.h"
#include "DefinitionObject/CPartsDef.h"
#include "Utility/CustomPropertys.h"
#include "Utility/CStdPropertyTree.h"
#include "Utility/CPropertyGrid.h"
#include "Utility/CPropertyGroup.h"
#include "Utility/CUtility.h"
#include "System/CSystem.h"
#include "Script/CScriptObject.h"
#include "Io/IIoDef.h"
#include "Io/CIoRef.h"
#include "Io/CIoDef.h"
#include "Io/CIoConnect.h"
#include "Io/CIoPropertyBlock.h"
#include "MockSketch.h"
#include "CProjectCtrl.h"

#include <math.h>
#include <boost/serialization/export.hpp> 
BOOST_CLASS_EXPORT(CDrawingScriptBase);


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
CDrawingScriptBase::CDrawingScriptBase():
CDrawingObject(),
m_bLoadAfter (false),
m_dwCycleTime(100),
m_eExecThreadType(EXEC_COMMON::TH_NONE),
m_eDebug(EXEC_COMMON::EDB_FOLLOW_SETTING),
m_strThreadType(EXEC_COMMON::GetThreadType2Name(EXEC_COMMON::TH_NONE))
{
    SetUsePropertySet(true);
    m_psScriptObject = std::make_shared<CScriptObject>(this);
}


/**
 *  @brief  コンストラクター.
 *  @param  [in] nID 
 *  @retval なし 
 *  @note
 */
CDrawingScriptBase::CDrawingScriptBase(int nID, DRAWING_TYPE eType):
CDrawingObject( nID, eType),
m_bLoadAfter (false),
m_dwCycleTime(100),
m_eExecThreadType(EXEC_COMMON::TH_NONE),
m_eDebug(EXEC_COMMON::EDB_FOLLOW_SETTING),
m_strThreadType(EXEC_COMMON::GetThreadType2Name(EXEC_COMMON::TH_NONE))

{
    SetUsePropertySet(true);
    m_psIo = std::make_shared<CIoRef>();
    m_psScriptObject = std::make_shared<CScriptObject>(this);
}

/**
 *  @brief  コピーコンストラクター.
 *  @param  [in] scriptBase 
 *  @retval なし 
 *  @note    ID,  コントロール
 *           はコピー元を引き継ぎます
 */
CDrawingScriptBase::CDrawingScriptBase(const  CDrawingScriptBase&  scriptBase):
CDrawingObject(scriptBase),
m_bLoadAfter (scriptBase.m_bLoadAfter),
m_dwCycleTime(scriptBase.m_dwCycleTime),
m_eExecThreadType(scriptBase.m_eExecThreadType),
m_strThreadType(scriptBase.m_strThreadType),
m_eDebug(scriptBase.m_eDebug),
m_uidObjectDef(scriptBase.m_uidObjectDef)
{

    if (scriptBase.m_psIo)
    {
        CIoRef* pRef = scriptBase.m_psIo.get();
        m_psIo = std::make_shared<CIoRef>(*pRef);
    }

    m_psScriptObject = std::make_shared<CScriptObject>(this);
    m_mapUserPropertyData = scriptBase.m_mapUserPropertyData;


    InitScriptObject(false);
}


/**
 *  @brief  デストラクタ.
 *  @param  なし 
 *  @retval なし 
 *  @note
 */
CDrawingScriptBase::~CDrawingScriptBase()
{
}

/**
 *  @brief   生成処理
 *  @param   [in] pIoAccess  
 *  @param   [in] pDef  
 *  @retval  なし
 *  @note    
 */
bool CDrawingScriptBase::Create(std::shared_ptr<CIoAccess>  pIoAccess,
                                CObjectDef* pDef)
{
    m_psIo.reset();
    m_psIo = std::make_shared<CIoRef>();

    m_uidObjectDef = pDef->GetPartsId();
    if (!m_psIo->Create(this, pIoAccess))
    {
        STD_DBG(_T("m_psIo->Create fail"));
    }

    if(pDef)
    {
        m_eExecThreadType = pDef->GetDefaultExecThreadType();
    }


    m_strThreadType = EXEC_COMMON::GetThreadType2Name(m_eExecThreadType);

    int iRet;
    iRet = m_psScriptObject->Initialize(pDef->GetModule(), true);

    if (iRet != 0)
    {
        // イニシャライズ失敗でも続行する
        // メソッド使用時に再度トライ
        STD_DBG(_T("Initilize fail %d"), iRet);
    }

	if (pDef)
	{
		StdString strPropsetName = pDef->GetPropertySetName();
		_SetPropertySetName(strPropsetName);
	}

    InitPropertySetValue(true /*bCreate*/);
    InitUserPropertyValue(true /*bCreate*/);

    return true;
}

/**
 *  @brief   スクリプトコンテキスト取得
 *  @param   なし
 *  @retval  スクリプトコンテキストへのポインタ
 *  @note
 */
CScriptObject* CDrawingScriptBase::GetScriptObject() const
{
    return m_psScriptObject.get();
}

/**
 *  @brief   実行スレッド種別取得
 *  @param   なし 
 *  @retval  実行スレッド種別
 *  @note
 */
EXEC_COMMON::E_THREAD_TYPE  CDrawingScriptBase::GetExecThreadType() const
{
    EXEC_COMMON::E_THREAD_TYPE eType;
    eType = EXEC_COMMON::GetThreadName2Type(m_strThreadType);
    return eType;
}

/**
 *  @brief   実行スレッド種別設定
 *  @param   [in] eType スレッド種別
 *  @retval  なし
 *  @note
 */
void CDrawingScriptBase::SetExecThreadType(EXEC_COMMON::E_THREAD_TYPE eType)
{
    m_strThreadType = EXEC_COMMON::GetThreadType2Name(eType);
}

/**
 *  @brief   デバッグモード取得
 *  @param   なし 
 *  @retval  デバッグモード
 *  @note
 */
EXEC_COMMON::DEBUG_MODE  CDrawingScriptBase::GetDebugMode() const
{
    return m_eDebug;
}

/**
 *  @brief   デバッグモード設定
 *  @param   [in] eType デバッグモード
 *  @retval  なし
 *  @note
 */
void CDrawingScriptBase::SetDebugMode(EXEC_COMMON::DEBUG_MODE eType)
{
    m_eDebug = eType;
}

/**
 *  @brief   サイクルタイム取得
 *  @param   なし 
 *  @retval  サイクルタイム
 *  @note
 */
DWORD  CDrawingScriptBase::GetCycleTime() const
{
    return m_dwCycleTime;
}

/**
 *  @brief   サイクルタイム設定
 *  @param   [in] dwCycletime サイクルタイム
 *  @retval  なし
 *  @note
 */
void CDrawingScriptBase::SetCycleTime(DWORD dwCycletime)
{
    m_dwCycleTime = dwCycletime;
}


/**
 *  @brief   プライオリティ取得
 *  @param   なし 
 *  @retval  サイクルタイム
 *  @note
 */
int  CDrawingScriptBase::GetPriority() const
{
    return m_iTaskPriority;
}

/**
 *  @brief   プライオリティ設定
 *  @param   [in] dwCycletime サイクルタイム
 *  @retval  なし
 *  @note
 */
void CDrawingScriptBase::SetPriority(int iPriority)
{
    m_iTaskPriority = iPriority;
}


/**
 *  @brief   生成元取得
 *  @param   なし
 *  @retval  なし
 *  @note
 */
std::weak_ptr<CObjectDef>  CDrawingScriptBase::GetObjectDef() const
{
    return THIS_APP->GetObjectDefByUuid(m_uidObjectDef);
}

/**
 *  @brief   プロパティ初期化.
 *  @param   [in] pCtrl オブジェクトコントロールへのポインタ
 *  @retval     なし
 *  @note
 */
TREE_GRID_ITEM*  CDrawingScriptBase::_CreateStdPropertyTree()
{
    NAME_TREE_ITEM<CStdPropertyItem>* pBeforeGroup;
    NAME_TREE_ITEM<CStdPropertyItem>* pNextItem;
    NAME_TREE_ITEM<CStdPropertyItem>* pGroupItem;
    NAME_TREE<CStdPropertyItem>* pTree;
    //int iId;
    CStdPropertyItem* pItem;
    boost::tuple< CStdPropertyItem*, void*> pairVal;


    pBeforeGroup = CDrawingObject::_CreateStdPropertyTree();
    pTree = m_psProperty->GetTreeInstance();

    pGroupItem  = pTree->AddNext(pBeforeGroup, NULL, GET_STR(STR_PRO_SCRIPT), _T("Script"));

 
    //--------------------
    // 実行スレッド種別
    //--------------------

    //暫定処置
    StdString strTypeList;

    using namespace EXEC_COMMON;
    int iStart;
    int iEnd;
    if (GetType() == DT_FIELD)
    {
        iStart = TH_FIELD_1;
        iEnd   = TH_FIELD_MAX;
    }
    else
    {
        iStart = TH_GROUP_1;
        iEnd   = TH_GROUP_MAX;
    }

    E_THREAD_TYPE eType;
    for (int iCnt = iStart; iCnt < iEnd; iCnt++)
    {
        if (iCnt != iStart)
        {
            strTypeList += _T(",");
        }
        eType = static_cast<E_THREAD_TYPE>(iCnt);
        strTypeList += EXEC_COMMON::GetThreadType2Name(eType);
    }


    CStdPropertyItemDef DefExecThreadType
        (PROP_DROP_DOWN,                        //入力種別
        GET_STR(STR_PRO_EXEC_THREAD_TYPE)   ,   //データ表示名
        _T("ExecThreadType"),
        GET_STR(STR_PRO_INFO_EXEC_THREAD_TYPE), //データ説明
        false,                                  //編集可、不可   
        DISP_NONE,                              //単位 
        m_strThreadType,                        //初期値
        strTypeList,                            //最大値
        StdString(_T("")));                     //最小値

    pItem = new CStdPropertyItem(
        DefExecThreadType, 
        _PropThreadType, 
        m_psProperty, 
        NULL,
        (void*)&m_strThreadType);
    
    pNextItem = pTree->AddChild(pGroupItem, pItem, DefExecThreadType.strDspName, pItem->GetName());

    //---------------
    //実行サイクルタイム
    //---------------
    CStdPropertyItemDef DefCycleTime(PROP_INT_RANGE, 
        GET_STR(STR_PRO_EXEC_CYCKE_TIME)   , 
        _T("CycleTime"), 
        GET_STR(STR_PRO_INFO_EXEC_CYCKE_TIME), 
        true,                  //Edit許可                 
        DISP_NONE,             //UNIT
        m_dwCycleTime);   //INIT

    pItem = new CStdPropertyItem(
        DefCycleTime,           //定義
        _PropCycleTime,         //入力時コール爆
        m_psProperty,           //親グリッド
        NULL,                   //更新時コールバック
        (void*)&m_dwCycleTime); //実データ
    
    pNextItem = pTree->AddNext(pNextItem, 
        pItem, 
        DefCycleTime.strDspName, 
        pItem->GetName());

    //---------------
    //実行プライオリティ
    //---------------
    CStdPropertyItemDef DefPriority(PROP_INT_RANGE, 
        GET_STR(STR_PRO_EXEC_PRIORITY)   , 
        _T("Priority"), 
        GET_STR(STR_PRO_INFO_EXEC_PRIORITY), 
        true,                  //Edit許可                 
        DISP_NONE,             //UNIT
        m_iTaskPriority,
        1,5);   //INIT

    pItem = new CStdPropertyItem(
        DefPriority,           //定義
        _PropPriority,         //入力時コール爆
        m_psProperty,           //親グリッド
        NULL,                   //更新時コールバック
        (void*)&m_iTaskPriority); //実データ
    
    pNextItem = pTree->AddNext(pNextItem, 
        pItem, 
        DefPriority.strDspName, 
        pItem->GetName());
    //---------------
    //デバッグモード
    //---------------
    using namespace EXEC_COMMON;

    StdString strDebugList;

    strDebugList  = GetDebugMode2Name(EDB_DEBUG);
    strDebugList += _T(":");  
    strDebugList += boost::lexical_cast<StdString>(EDB_DEBUG);  
    strDebugList += _T(",");

    strDebugList += GetDebugMode2Name(EDB_RELEASE);
    strDebugList += _T(":");  
    strDebugList += boost::lexical_cast<StdString>(EDB_RELEASE);  
    strDebugList += _T(",");

    strDebugList += GetDebugMode2Name(EDB_FOLLOW_SETTING);
    strDebugList += _T(":");  
    strDebugList += boost::lexical_cast<StdString>(EDB_FOLLOW_SETTING);  


    CStdPropertyItemDef DefDebug(PROP_DROP_DOWN_ID, 
        GET_STR(STR_PRO_DEBUG_MODE)   , 
        _T("DebugMode"),
        GET_STR(STR_PRO_INFO_DEBUG_MODE), 
        false,                                       
        DISP_NONE,
        static_cast<int>(m_eDebug), 
        strDebugList,
        0);

    pItem = new CStdPropertyItem( 
        DefDebug, 
        _PropDebugMode, 
        m_psProperty, 
        NULL,
        (void*)&m_eDebug);
    
    pNextItem = pTree->AddNext(pNextItem, 
        pItem, 
        DefDebug.strDspName, 
        pItem->GetName());


    //---------------
    // 定義名称
    //---------------

    StdString strDefName;
    std::shared_ptr<CObjectDef> pDef;
    pDef = GetObjectDef().lock();
    if (pDef)
    {
        m_strDefName = pDef->GetName();
    }

    CStdPropertyItemDef DefDefinitionName(PROP_STR,     //入力種別 
        GET_STR(STR_PRO_DEFINTION_NAME)   ,             //データ表示名
        _T("DefinitionNmae"),
        GET_STR(STR_PRO_INFO_DEFINTION_NAME),           //データ説明
        false,                                          //編集可、不可 
        DISP_NONE,                                      //単位 
        strDefName);                                    //初期値

    pItem = new CStdPropertyItem( 
        DefDefinitionName, 
        NULL, 
        m_psProperty, 
        NULL,
        (void*)&m_strDefName);
    
    pNextItem = pTree->AddNext(pNextItem, 
        pItem, 
        DefDefinitionName.strDspName, 
        pItem->GetName());

   

    return pGroupItem;
}

/**
 * @brief   ユーザプロパティ初期値設定
 * @param   なし
 * @retval  なし
 * @note	貼付け時、画面更新時、実行時に設定する
 */
void CDrawingScriptBase::InitUserPropertyValue(bool bCreate)
{
    std::shared_ptr<CObjectDef> pDef;
    pDef = GetObjectDef().lock();

    if (!pDef)
    {
        return;
    }

    if (!bCreate)
    {
        if (m_iChgPropUserDefCnt == pDef->GetUserPropertySet()->GetChgCnt())
        {
            return;
        }
    }


    //種別、名前が変更した場合初期値にリセットする

    std::set<StdString> lstUse;
    std::map<StdString, CAny>::iterator iteMap;
    for(iteMap  = m_mapUserPropertyData.begin();
        iteMap != m_mapUserPropertyData.end();
        iteMap++)
    {
        lstUse.insert(iteMap->first);
    }

    std::set<StdString>::iterator iteSet;


    int iMax = pDef->GetUserPropertySet()->GetCnt();
    for (int iCnt = 0; iCnt < iMax; iCnt++)
    {
        CStdPropertyItemDef* pGridDef;
        CAny* pAny;

        pGridDef = pDef->GetUserPropertySet()->GetInstance(iCnt);
        pAny = GetUserProperty(pGridDef->strDspName);

        if(!pAny)
        {
            m_mapUserPropertyData[pGridDef->strDspName] = pGridDef->anyInit;
        }
        else
        {
            if (pAny->anyData.type() != pGridDef->anyInit.type())
            {
                pAny->anyData = pGridDef->anyInit;
            }
            else
            {

                if (pGridDef->type == PROP_INT_RANGE)
                {
                    int iMax = boost::any_cast<int>(pGridDef->anyMax);
                    int iMin = boost::any_cast<int>(pGridDef->anyMin);

                    if (pAny->GetInt() < iMin)
                    {
                        pAny->anyData = iMin;
                    }
                    else if (pAny->GetInt() > iMax)
                    {
                        pAny->anyData = iMax;
                    }
                }
            }
            
            iteSet = lstUse.find(pGridDef->strDspName);
            if (iteSet != lstUse.end())
            {
                lstUse.erase(iteSet);
            }
        }
    }

    foreach(const StdString& strItem, lstUse)
    {
        iteMap = m_mapUserPropertyData.find(strItem);

        if (iteMap != m_mapUserPropertyData.end())
        {
            m_mapUserPropertyData.erase(iteMap);
        }
    }

}

/**
 * @brief   ユーザプロパティ設定
 * @param   [in] pGroupItem
 * @retval  なし
 * @note	
 */
TREE_GRID_ITEM* CDrawingScriptBase::_CreateStdUserPropertyTree(TREE_GRID_ITEM* pGroupItem)
{
    CDrawingObject* pRefObj;

    pRefObj = GetRefObj();

    ReleaseUserProperty();

    PAIR_ITEM_VAL pairVal;

    CStdPropertyItemDef* pGridDef;
    CStdPropertyItem* pItem;
    NAME_TREE<CStdPropertyItem>* pTree;

    NAME_TREE_ITEM<CStdPropertyItem>* pNextItem;
    //NAME_TREE_ITEM<CStdPropertyItem>* pGroupItem;

    pTree = m_psProperty->GetTreeInstance();

    std::shared_ptr<CObjectDef> pDef;
    pDef = GetObjectDef().lock();

    if (!pDef)
    {
        return pGroupItem;
    }

    int iMax = pDef->GetUserPropertySet()->GetCnt();

    if (iMax <= 0)
    {
        return pGroupItem;
    }

    TREE_GRID_ITEM* pUserItem;

    pUserItem = pTree->GetItem(GET_STR(STR_PRO_USER), 0);
    if (!pUserItem)
    {
        //新規にユーザープロパティを設定する
        pUserItem  = pTree->AddNext(pGroupItem, NULL, GET_STR(STR_PRO_USER), _T("UserProperty"));
    }

    for (int iCnt = 0; iCnt < iMax; iCnt++)
    {
        pGridDef = pDef->GetUserPropertySet()->GetInstance(iCnt);

        pItem = new CStdPropertyItem(
            *pGridDef, 
            ChgUserProp, 
            m_psProperty, 
            NULL, 
            this,   //データを取得するときは this->GetUserProperty(pGridDef->strDspName)
            PROP_CATE_USER);

        int iOffset = 1;
        if (pGridDef->type == PROP_POINT2D)
        {
            iOffset = 2;
        }
        else if(pGridDef->type == PROP_RECT2D)
        {
            iOffset = 4;
        }

        if (iCnt == 0)
        {
            pNextItem = pTree->AddChild(pUserItem, pItem, pGridDef->strDspName, pItem->GetName(), iOffset);
        }
        else
        {
            pNextItem = pTree->AddNext(pNextItem, pItem, pGridDef->strDspName, pItem->GetName(), iOffset);
        }
    }

    return pGroupItem;
}

/**
 *  @brief   プロパティ変更(実行スレッド種別)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingScriptBase::_PropThreadType   (CStdPropertyItem* pData, void* pObj)
{
    using namespace EXEC_COMMON;
    CDrawingScriptBase* pDrawing = reinterpret_cast<CDrawingScriptBase*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        StdString strThreadType = pData->anyData.GetString();

        E_THREAD_TYPE eType = GetThreadName2Type( strThreadType);
        pDrawing->SetExecThreadType(eType);

    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}



/*
 *  @brief   プロパティ変更(実行サイクルタイム)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingScriptBase::_PropCycleTime   (CStdPropertyItem* pData, void* pObj)
{
    using namespace EXEC_COMMON;
    CDrawingScriptBase* pDrawing = reinterpret_cast<CDrawingScriptBase*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        int iTime = pData->anyData.GetInt();
        if (iTime < 0)
        {
            iTime = 0;
        }
        pDrawing->SetCycleTime(iTime);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}


/*
 *  @brief   プロパティ変更(実行プライオリティ)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingScriptBase::_PropPriority   (CStdPropertyItem* pData, void* pObj)
{
    /*
    using namespace EXEC_COMMON;
    CDrawingScriptBase* pDrawing = reinterpret_cast<CDrawingScriptBase*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        int iPriority = pData->anyData.GetInt();
        if (iPriority < 1){return false;}
        if (iPriority > 5){return false;}

        pDrawing->SetPriority(iPriority);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    */
    return true;
}
/**
 *  @brief   プロパティ変更(デバッグモード)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingScriptBase::_PropDebugMode   (CStdPropertyItem* pData, void* pObj)
{
    using namespace EXEC_COMMON;
    CDrawingScriptBase* pDrawing = reinterpret_cast<CDrawingScriptBase*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        int iType = pData->anyData.GetInt();

        DEBUG_MODE eType = static_cast<DEBUG_MODE>( iType);
        pDrawing->SetDebugMode(eType);

    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 * @brief   IO取得
 * @param   なし
 * @retval  IOへのポインタ
 * @note	
 */
CIoRef* CDrawingScriptBase::GetIoRef()
{
    return m_psIo.get();
}

/**
 * @brief   ユーザープロパティ取得
 * @param   [in] strPropName プロパティ名
 * @retval  ユーザープロパティデータ値
 * @note	
 */
CAny* CDrawingScriptBase::GetUserProperty(StdString& strPropName)
{
    std::map<StdString, CAny>::iterator iteMap;


    iteMap = m_mapUserPropertyData.find(strPropName);

    if (iteMap == m_mapUserPropertyData.end())
    {
        return NULL;
    }

    CAny* pAny;

    pAny = &(iteMap->second);

    return pAny;
}

CAny* CDrawingScriptBase::GetUserPropertyS(std::string& strPropName)
{
    StdString strOut =  CUtil::CharToString(strPropName.c_str());
    return GetUserProperty(strOut);
}

/**
 * @brief   ユーザプロパティイベント
 * @param   [in] 
 * @param   [in] strPropName プロパティ名
 * @retval  ユーザープロパティデータ値
 * @note	
 */
void CDrawingScriptBase::OnChgUserProp (CStdPropertyItem* pData, StdString strPropName)
{
    try
    {
        CAny* pAny = GetUserProperty(strPropName);
        pAny->anyData = pData->anyData.anyData;
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
    }

    m_psScriptObject->OnChangeProperty(strPropName);
    //m_pCtrl->Draw();
}


/**
 *  @brief   マーカ初期化.
 *  @param   [in] pMarker
 *  @retval  なし
 *  @note
 */
bool CDrawingScriptBase::InitNodeMarker(CNodeMarker* pMarker)
{
    STD_ASSERT(pMarker);
    m_psScriptObject->OnInitNodeMarker(pMarker);
    return true;
}

/**
 *  @brief   マーカ選択
 *  @param   [in] pMarker
 *  @param   [in] iMarkerId
 *  @retval  なし
 *  @note
 */
 void CDrawingScriptBase::SelectNodeMarker(CNodeMarker* pMarker,
                        StdString strMarkerId)
{
    m_psScriptObject->OnSelectNodeMarker(pMarker, strMarkerId);
}

/**
 *  @brief   マーカ移動
 *  @param   [in] pMarker
 *  @param   [in] iMarkerId
 *  @param   [in] ptRet
 *  @retval  なし
 *  @note
 */
void CDrawingScriptBase::MoveNodeMarker(CNodeMarker* pMarker, 
                                 SNAP_DATA* pSnap,
                                 StdString strMarkerId,
                                 MOUSE_MOVE_POS posMouse)
{
    m_psScriptObject->OnMoveNodeMarkerId(pMarker, strMarkerId, pSnap->pt, posMouse);
}

/**
 *  @brief   マーカ開放
 *  @param   [in] pMarker
 *  @retval  なし
 *  @note
 */
bool CDrawingScriptBase::ReleaseNodeMarker(CNodeMarker* pMarker, StdString strMarkerId, MOUSE_MOVE_POS posMouse)
{
    m_psScriptObject->OnReleaseNodeMarker(pMarker);
    return true;
}


/**
 *  @brief   マーカ変更
 *  @param   [in] pMarker
 *  @retval  なし
 *  @note
 */
void CDrawingScriptBase::ChangeNode(StdString strMarkerId,
                                                    E_NODE_CHANGE_TYPE eType)
{
    m_psScriptObject->OnChangeNode(strMarkerId, eType);
}

/**
 *  @brief   オブジェクトリスト取得
 *  @param   [out] pObjectList
 *  @param   [out] pObjectList
 *  @param   [out] pObjectList
 *  @param   [out] pObjectList
 *  @retval  true 成功
 *  @note    Field以外からは使用しないようにする
 */
bool CDrawingScriptBase::GetObjectList(std::vector<CDrawingObject*>* pObjectList,
                        StdString& strPropertySet, 
                        StdString& strTag, 
                        DRAWING_TYPE eDrawingType)
{
    CPartsDef* pCtrl;

    pCtrl = GetPartsDef();

    if (!pCtrl)
    {
        return false;
    }

    auto   pGroup = pCtrl->GetDrawingParts();

    if (!pGroup)
    {
        return false;
    }

    bool bRet;
    bRet = _GetObjectList(pGroup.get(), pObjectList, strPropertySet, strTag, eDrawingType);
    return bRet;
}




bool CDrawingScriptBase::_GetObjectList(
                        CDrawingParts* pGroup,
                        std::vector<CDrawingObject*>* pObjectList,
                        StdString& strPropertySet, 
                        StdString& strTag, 
                        DRAWING_TYPE eDrawingType)
{
    if (!pGroup)
    {
        return false;
    }

    //<! マップ取得
    auto pList =  pGroup->GetList(); 

    for (auto pObj: *pList)
    {
        if (eDrawingType != DT_NONE)
        {
            if(pObj->GetType() != eDrawingType)
            {
                continue;
            }
        }

        bool bSelect = false;
        if (!strTag.empty())
        {
            if(pObj->GetTag().empty())
            {
                continue;
            }

            std::vector<StdString> lstObjTag;
            pObj->GetTagList(&lstObjTag);
            foreach(StdString &tag, lstObjTag)
            {
                if (tag == strTag)
                {
                    bSelect = true;
                    break;
                }
            }

            if (!bSelect)
            {
                continue;
            }
        }

        if (!strPropertySet.empty())
        {
            if (strPropertySet != pObj->GetPropertySetName())
            {
                continue;
            }
        }

        if ((pObj->GetType() == DT_PARTS)||
            (pObj->GetType() == DT_FIELD))
        {
            auto pGroup = std::dynamic_pointer_cast<CDrawingParts>(pObj);
            
            if (pGroup)
            {
                pGroup->_GetObjectList(
                             pGroup.get(),   
                             pObjectList,
                             strPropertySet, 
                             strTag, 
                             eDrawingType);
            }
        }

        pObjectList->push_back(pObj.get());
    }

    return true;
}


bool CDrawingScriptBase::InitScriptObject(bool bThread)
{
    std::shared_ptr<CObjectDef>         pDef;
    pDef = GetObjectDef().lock();

    if (pDef)
    {
        int iRet;
        iRet = m_psScriptObject->Initialize(pDef->GetModule(), bThread);
       if (iRet == 0 ) //asSUCCESS
       {
           return true;
       }
    }
    return false;
}


/**
 * @brief   ロード終了後処理
 * @param   なし
 * @retval  なし
 * @note	
 */
void CDrawingScriptBase::LoadAfter(CPartsDef* pDef)
{

    if (m_bLoadAfter)
    {
        return;
    }
    m_bLoadAfter = true;
    
    if (m_pCtrl)
    {
        CProjectCtrl* pProj = m_pCtrl->GetProject();
    }

    //=======================
    // IO接続を再設定する
    //=======================
    std::map< int, std::shared_ptr<CIoConnect> >* pMapConnect;
    std::map< int, std::shared_ptr<CIoConnect> >::iterator iteMap;

    if(!m_psIo)
    {
        return;
    }

    pMapConnect = m_psIo->GetConnectMap();

    int iParentId;
    int iIoId;
    CIoDefBase*  pIo;
    CIoConnect*  pConnect;
    std::weak_ptr<CIoProperty> pIoProp;

    for(iteMap  = pMapConnect->begin();
        iteMap != pMapConnect->end();
        iteMap++)
    {

        pConnect = iteMap->second.get();
        pConnect->GetId(&iParentId, &iIoId);
        pIo = NULL;

        if (iParentId == 0)
        {
            pIo = m_pCtrl->GetIoDef();
        }
        else
        {
            auto pObj = m_pCtrl->GetDrawingParts()->Find(iParentId);
            auto pBase = std::dynamic_pointer_cast<CDrawingScriptBase>(pObj);

            STD_ASSERT(pBase);
            if (pBase)
            {
                pIo = pBase->m_psIo.get();
            }
        }

        STD_ASSERT(pIo);
        if (pIo)
        {
            bool bRet;

            bRet = pConnect->SetConnect(pIo, iIoId);
            STD_ASSERT(bRet);
        }
    }

    InitScriptObject(false);
    m_psIo->SetParent(this);
    m_psIo->LoadAfter();


}

/**
 * @brief   シリアル化(ロード)
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  なし
 * @note	
 */
template<class Archive>
void CDrawingScriptBase::load(Archive& ar, const unsigned int version)
{
    SERIALIZATION_INIT
        ar >> BOOST_SERIALIZATION_BASE_OBJECT_NVP(CDrawingObject);

        SERIALIZATION_LOAD("Io"           ,  m_psIo);
        SERIALIZATION_LOAD("ThreadType"   ,  m_strThreadType);
        SERIALIZATION_LOAD("Debug"        ,  m_eDebug);
        SERIALIZATION_LOAD("DefinitionId" ,  m_uidObjectDef);
        SERIALIZATION_LOAD("UserProperty" ,  m_mapUserPropertyData);



        if(m_psIo.get() != NULL)
        {
            m_psIo->SetParent(this);
        }
        
        m_bLoadAfter = false;
    MOCK_EXCEPTION_FILE(e_file_read);
}


/**
 * @brief   シリアル化(セーブ)
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  なし
 * @note	
 */
template<class Archive>
void CDrawingScriptBase::save(Archive& ar, const unsigned int version) const
{
    StdString strRead;

    /*
    if (m_pObjectDef)
    {
        m_uidObjectDef = m_pObjectDef->GetPartsId();
    }
    else
    {
        // CdrawingObjecCtrlが所持しているCDrawingPartsは
        // m_pObjectDefを設定しない
        m_uidObjectDef =boost::uuids::nil_uuid();
    }
    */

    SERIALIZATION_INIT
        CIoRef* pIoRef = m_psIo.get();
        ar << BOOST_SERIALIZATION_BASE_OBJECT_NVP(CDrawingObject);
        SERIALIZATION_SAVE("Io"           ,  m_psIo);
        SERIALIZATION_SAVE("ThreadType"   ,  m_strThreadType);
        SERIALIZATION_SAVE("Debug"        ,  m_eDebug);
        SERIALIZATION_SAVE("DefinitionId" ,  m_uidObjectDef);
        SERIALIZATION_SAVE("UserProperty" ,  m_mapUserPropertyData);
    MOCK_EXCEPTION_FILE(e_file_write);
    
}



//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void TEST_CDrawingScriptBase()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}
#endif //_DEBUG