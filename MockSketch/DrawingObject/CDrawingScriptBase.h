/**
* @brief        CDrawingScriptBaseヘッダーファイル
* @file	        CDrawingScriptBase.h
* @author           Yasuhiro Sato
* @date	        07-2-2009 20:03:09
* 
* @note	
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
* 
* @remarks	        
*			
*
* $
* $
* 
*/
#ifndef __DRAWING_SCRIPT_BASE_H_
#define __DRAWING_SCRIPT_BASE_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DrawingObject/CDrawingObject.h"

#include "DrawingObject/Primitive/LINE2D.h"
#include "../ExecCommon.H"

/*---------------------------------------------------*/
/*  classes                                          */
/*---------------------------------------------------*/
class CScriptObject;
      
class CIoDefBase;
class CIoRef;
class CObjectDef;
class CIoAccess;

/**
 * @class   CDrawingScriptBase
 * @brief   スクリプトとIOを持ったオブジェクト
 */
class CDrawingScriptBase :public CDrawingObject//:public CRTTIClass<CDrawingScriptBase, CDrawingObject, DT_LINE>
{
public:

public:
    //!< コンストラクタ
    CDrawingScriptBase();

    //!< コンストラクタ
	CDrawingScriptBase(int nID, DRAWING_TYPE eType);

    //!< コピーコンストラクタ
	CDrawingScriptBase(const CDrawingScriptBase& Obj);

    //!< デストラクタ
    virtual ~CDrawingScriptBase();

    //!< スクリプト削除
    void DeleteScript();

    //!< 生成処理
    virtual bool Create(std::shared_ptr<CIoAccess> pIoAccess,
                        CObjectDef* pDef);

    //-------------------------
    // リフレクション設定
    //-------------------------
    //!< スレッド種別取得
    virtual EXEC_COMMON::E_THREAD_TYPE  GetExecThreadType() const;
    
    //!< スレッド種別設定
    virtual void SetExecThreadType(EXEC_COMMON::E_THREAD_TYPE eType);

    //!< デバッグモード取得
    virtual EXEC_COMMON::DEBUG_MODE  GetDebugMode() const;
    
    //!< デバッグモード設定
    virtual void SetDebugMode(EXEC_COMMON::DEBUG_MODE eType);

    //!< サイクルタイム設定
    void SetCycleTime(DWORD dwCycletime);

    //!< サイクルタイム取得
    DWORD GetCycleTime() const;

    //!< プライオリティ設定
    void SetPriority(int iPriority);

    //!< プライオリティ設定
    int GetPriority() const;



    //!< 生成元取得
    std::weak_ptr<CObjectDef>   GetObjectDef() const;

    //-------------------------
    // スクリプト
    //-------------------------

    // スクリプトオブジェクト取得
    CScriptObject* GetScriptObject() const;

    // 編集時スクリプト準備
    bool PrepareScriptForEdit();

    // スクリプトオブジェクト初期化
    bool InitScriptObject(bool bThread);


    //-------------------------
    // IO
    //-------------------------
    //!< IO取得
    CIoRef* GetIoRef();

    //-------------------------
    // ユーザー定義プロパティ
    //-------------------------
    //!< ユーザプロパティ初期値設定
    virtual void InitUserPropertyValue(bool bCreate);

    virtual CAny* GetUserProperty(StdString& strPropName);
    virtual CAny* GetUserPropertyS(std::string& strPropName);

    //!<ユーザプロパティイベント
    virtual void OnChgUserProp (CStdPropertyItem* pData, StdString strPropName);

    //ロード終了後処理
    virtual void LoadAfter(CPartsDef* pDef) override;

    //-------------------
    // ドラッグ用マーカ
    //-------------------
    //!< マーカ初期化 true:マーカあり
    virtual bool InitNodeMarker(CNodeMarker* pMarker) override;

    //!< マーカ選択
    virtual void SelectNodeMarker(CNodeMarker* pMarker, 
                                     StdString strMarkerId) override;

    //!< マーカ移動
    virtual void MoveNodeMarker(CNodeMarker* pMarker, 
                                     SNAP_DATA* pSnap,
                                     StdString strMarkerId,
                                     MOUSE_MOVE_POS posMouse) override;  

    //!< マーカ開放
    virtual bool ReleaseNodeMarker(CNodeMarker* pMarker, StdString strMarkerId, MOUSE_MOVE_POS posMouse) override;

    //!< ノード変更
    virtual void ChangeNode(StdString strMarkerId, E_NODE_CHANGE_TYPE eType) override;


    //-----------------------------
    // Field関連
    //-----------------------------
    virtual bool GetObjectList(std::vector<CDrawingObject*>* pObjectList,
         StdString& strPropertySet, 
         StdString& strTag, 
         DRAWING_TYPE eDrawingType);

    virtual bool GetObjectListS(std::vector<CDrawingObject*>* pObjectList,
         std::string& strPropertySet, 
         std::string& strTag, 
         DRAWING_TYPE eDrawingType)
    {
        StdString sPropertySet =  CUtil::CharToString(strPropertySet.c_str());
        StdString sTag =  CUtil::CharToString(strTag.c_str());
        return GetObjectList(pObjectList, sPropertySet, sTag, eDrawingType);
    }

protected:
    virtual bool _GetObjectList(
         CDrawingParts* pGroup,
         std::vector<CDrawingObject*>* pObjectList,
         StdString& strPropertySet, 
         StdString& strTag, 
         DRAWING_TYPE eDrawingType);

protected:
friend CDrawingScriptBase;

    //!< プロパティ初期化
    virtual TREE_GRID_ITEM*  _CreateStdPropertyTree();

    //!<ユーザプロパティ設定
    virtual TREE_GRID_ITEM* _CreateStdUserPropertyTree(TREE_GRID_ITEM* pGroupItem);


    //!< プロパティ変更
    //-------------------
    static bool _PropThreadType   (CStdPropertyItem* pData, void* pObj);
    static bool _PropDebugMode    (CStdPropertyItem* pData, void* pObj);
    static bool _PropCycleTime    (CStdPropertyItem* pData, void* pObj);
    static bool _PropPriority     (CStdPropertyItem* pData, void* pObj);

    //-----------------------------
    // 保存データ
    //-----------------------------
    //!< 実行スレッド種別
    EXEC_COMMON::E_THREAD_TYPE              m_eExecThreadType;

    //!< 実行サイクルタイム
    DWORD                                   m_dwCycleTime;

    //!< 実行プライオリティ
    int                                     m_iTaskPriority;

    //!< IO
    std::shared_ptr<CIoRef>               m_psIo;

    //!< 実行スレッド種別
    StdString                               m_strThreadType;

    //!< 生成元
    mutable boost::uuids::uuid              m_uidObjectDef;

    //!< デバッグモード
    EXEC_COMMON::DEBUG_MODE                 m_eDebug;

    std::map<StdString, CAny>               m_mapUserPropertyData;
    //-----------------------------
    // 保存不要データ
    //-----------------------------
    //!< スクリプトコンテキスト
    std::shared_ptr<CScriptObject>       m_psScriptObject;

    //!< ロード終了後設定フラグ
    bool                                    m_bLoadAfter;

    //!< 定義名
    StdString                               m_strDefName;
;


private:
    friend class boost::serialization::access;  
    BOOST_SERIALIZATION_SPLIT_MEMBER();

    template<class Archive>
    void load(Archive& ar, const unsigned int version);

    template<class Archive>
    void save(Archive& ar, const unsigned int version) const;
};

#endif // 