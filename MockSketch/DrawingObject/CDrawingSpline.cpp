/**
 * @brief			CDrawingSpline実装ファイル
 * @file			CDrawingSpline.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CDrawingPoint.h"
#include "./CDrawingLine.h"
#include "./CDrawingSpline.h"
#include "./CDrawingCircle.h"
#include "./CDrawingText.h"
#include "./CDrawingParts.h"
#include "./CDrawingDim.h"
#include "./CDrawingEllipse.h"
#include "./CDrawingCompositionLine.h"
#include "./CDrawingNode.h"
#include "View/CUndoAction.h"
#include "DefinitionObject/View/CDrawingView.h"
#include "DefinitionObject/CPartsDef.h"
#include <math.h>
#include <boost/serialization/export.hpp> 

#include "Utility/CUtility.h"
#include "System/CSystem.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

BOOST_CLASS_EXPORT(CDrawingSpline);

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
CDrawingSpline::CDrawingSpline():
CDrawingObject()
{
    m_eType  = DT_SPLINE;
    m_iWidth =    1;
    m_iLineType = PS_SOLID;
    m_iPropFeatures = P_BASE | P_LINE; 
}

/**
 *  @brief  コンストラクター.
 *  @param  [in] nID 
 *  @retval なし 
 *  @note
 */
CDrawingSpline::CDrawingSpline(int nID):
CDrawingObject( nID, DT_SPLINE)
{
    m_iWidth =    1;
    m_iLineType = PS_SOLID;
    m_iPropFeatures = P_BASE | P_LINE; 
};

/**
 *  @brief  コピーコンストラクタ
 *  @param  [in] Obj コピー元オブジェクト
 *  @retval なし 
 *  @note
 */
CDrawingSpline::CDrawingSpline(const CDrawingSpline& Obj):
CDrawingObject( Obj)
{
    m_eType  = DT_SPLINE;
    m_iWidth    = Obj.m_iWidth;
    m_iLineType = Obj.m_iLineType;
    m_iPropFeatures = Obj.m_iPropFeatures; 
    m_Spline    = Obj.m_Spline;
	m_Fill = Obj.m_Fill;
}

/**
 *  @brief  デストラクタ.
 *  @param  なし 
 *  @retval なし 
 *  @note
 */
CDrawingSpline::~CDrawingSpline()
{
}

void CDrawingSpline::InitDefault()
{
	m_Spline.Clear();
}


/**
 *  @brief  回転.
 *  @param  [in]    pPt2D    回転中心(絶対座標)
 *  @param  [in]    dAngle   (絶対)回転角(rad)
 *  @retval     なし
 *  @note
 */
void CDrawingSpline::Rotate(const POINT2D& pt2D, double dAngle)
{
    AddChgCnt();
    m_Spline.Rotate(pt2D, dAngle);
}

/**
 *  @brief  相対移動.
 *  @param  [in]    pPt2D   移動量
 *  @retval         なし
 *  @note
 */
void CDrawingSpline::Move(const POINT2D& pt2D) 
{
    AddChgCnt();
    m_Spline.Move(pt2D);
}


/**
 *  @brief  鏡像.
 *  @param  [in]    line   対称軸
 *  @retval         なし
 *  @note
 */
void CDrawingSpline::Mirror(const LINE2D& line)
{
    AddChgCnt();
    m_Spline.Mirror(line);
}

/**
 *  @brief  行列.
 *  @param  [in]    mat2D   適用する行列
 *  @retval         なし
 *  @note
 */
void CDrawingSpline::Matrix(const MAT2D& mat2D)
{
    AddChgCnt();
    m_Spline.Matrix( mat2D);
}

/**
 *  @brief  倍率.
 *  @param  [in]    pPt2D   拡大中心
 *  @param  [in]    dSclX   X倍率
 *  @param  [in]    dSclY   Y倍率
 *  @retval         なし
 *  @note
 */
void CDrawingSpline::Scl(const POINT2D& pt2D, double dXScl, double dYScl)
{
    m_Spline.Scl(pt2D, dXScl, dYScl);
}

/**
 *  @brief  交点計算.
 *  @param  [out]   pList   交点のリスト
 *  @param  [in]    pObj    交差するオブジェクト
 *  @retval         なし
 *  @note
 */
void CDrawingSpline::CalcIntersection ( std::vector<POINT2D>* pList,   
                                const CDrawingObject* pObj,
                                bool bOnline,
                                double dMin) const
{
    STD_ASSERT(pList != NULL);

    POINT2D ptTmp;
    const SPLINE2D* pSpline = NULL;
    SPLINE2D* pMatSpline = NULL;

    pList->clear();
    //レイヤーの確認
    if (!IsSameScale(pObj))
    {
        return;
    }

    const MAT2D* pMat =  GetParentMatrix();
    const MAT2D* pMat2 =  pObj->GetParentMatrix();
    if (pMat == NULL)
    {
        pSpline = &m_Spline;
    }
    else
    {
        pMatSpline = new SPLINE2D(m_Spline);
        pMatSpline->Matrix(*pMat);
        pSpline = pMatSpline;
    }


    try
    {
        switch (pObj->GetType())
        {
        case DT_POINT:
            {
                //近接点を算出
                const CDrawingPoint* ptObj = dynamic_cast<const CDrawingPoint*>(pObj);

                STD_ASSERT(ptObj != NULL);


                if (pMat2 == NULL)
                {
                    const POINT2D* pt2D = ptObj->GetPointInstance();
                    ptTmp = pSpline->NearPoint(*pt2D, false);
                }
                else
                {
                    POINT2D ptMat(*ptObj->GetPointInstance());
                    ptMat.Matrix(*pMat2);
                    ptTmp = pSpline->NearPoint(ptMat, false);

                }
                pList->push_back(ptTmp);
            }
            break;

        case DT_NODE:
            {
                //近接点を算出
                const CDrawingNode* ptObj = dynamic_cast<const CDrawingNode*>(pObj);

                STD_ASSERT(ptObj != NULL);


                if (pMat2 == NULL)
                {
                    const POINT2D* pt2D = ptObj->GetPointInstance();
                    ptTmp = pSpline->NearPoint(*pt2D, false);
                }
                else
                {
                    POINT2D ptMat(*ptObj->GetPointInstance());
                    ptMat.Matrix(*pMat2);
                    ptTmp = pSpline->NearPoint(ptMat, false);
                }
                pList->push_back(ptTmp);
            }
            break;

        case DT_LINE:
            {
                //交点を算出
                const CDrawingLine* pLine = dynamic_cast<const CDrawingLine*>(pObj);

                STD_ASSERT(pLine != NULL);
                if (pMat2 == NULL)
                {
                    pSpline->Intersect(*pLine->GetLineInstance(), pList, bOnline);
                }
                else
                {
                    LINE2D line(*pLine->GetLineInstance());
                    line.Matrix(*pMat2);
                    pSpline->Intersect(line, pList, bOnline);
                }
            }
            break;

        case DT_CIRCLE:
            {
                //交点を算出
                const CDrawingCircle* pCircle = dynamic_cast<const CDrawingCircle*>(pObj);
                STD_ASSERT(pCircle != NULL);

                if (pMat2 == NULL)
                {
                    pSpline->Intersect( *pCircle->GetCircleInstance(), pList, bOnline);
                }
                else
                {
                    ELLIPSE2D* pEllipse = NULL;
                    CIRCLE2D circle(*pCircle->GetCircleInstance());
                    circle.Matrix(*pMat2, &pEllipse);

                    if (!pEllipse)
                    {
                        pSpline->Intersect( circle, pList, bOnline);
                    }
                    else
                    {
                        pEllipse->Intersect(*pSpline, pList, bOnline);
                        delete pEllipse;
                    }

                }
            }
            break;

        case DT_SPLINE:
            {
                //交点を算出
                const CDrawingSpline* pSpline2 = dynamic_cast<const CDrawingSpline*>(pObj);
                STD_ASSERT(pSpline != NULL);
                if (pMat2 == NULL)
                {
                    pSpline->Intersect( *pSpline2->GetSplineInstance(), pList, bOnline);
                }
                else
                {
                    SPLINE2D spline(*pSpline2->GetSplineInstance());
                    spline.Matrix(*pMat2);
                    pSpline->Intersect( spline, pList, bOnline);
                }
            }
            break;

        case DT_ELLIPSE:
            {
                //交点を算出
                const CDrawingEllipse* pEllipse = dynamic_cast<const CDrawingEllipse*>(pObj);
                STD_ASSERT(pEllipse != NULL);
                if (pMat2 == NULL)
                {
                    pEllipse->GetEllipseInstance()->Intersect( *pSpline, pList);
                }
                else
                {
                    ELLIPSE2D ellipse(*pEllipse->GetEllipseInstance());
                    ellipse.Matrix(*pMat2);
                    ellipse.Intersect( *pSpline, pList, bOnline);
                }
            }
            break;

        case DT_COMPOSITION_LINE:
        {
            //交点を算出
            const CDrawingCompositionLine* pCom = dynamic_cast<const CDrawingCompositionLine*>(pObj);
            STD_ASSERT(pCom != NULL);
            if (pMat2 == NULL)
            {
                pCom->CalcIntersection(pList, this, bOnline, dMin);
            }
            else
            {
                CDrawingCompositionLine com(*pCom);
                com.Matrix(*pMat2);
                com.CalcIntersection(pList, this, bOnline, dMin);
            }
        }
        break;


        default:
            break;
        }
    }
    catch(...)
    {

    }

    if (pMatSpline)
    {
        delete pMatSpline;
    }
}


/**
*  @brief オフセットスプラインとの交点.
*  @param  [in]    dOffsetLenThis  このスプラインのオフセット量
*  @param  [in]    ptOffsetDirThis このスプラインのオフセット方向
*  @param  [in]    pObj            交点を求めるオブジェクト
*  @param  [in]    dOffsetLenObj   交点を求めるオブジェクトのオフセット量
*  @param  [in]    ptOffsetDirObj  交点を求めるオブジェクトのオフセット方向
*  @param  [in]    bOnline         線分(弧)上に限定するか(true)、延長線上を許可するか
*  @param  [in]    dMin            最大誤差
* 
*  @retval         なし
*  @note   制御点をオフセットしただけでは正確なオフセット曲線とはならない
*          したがって、オフセットを使用してスプラインのコーナーの計算はうまくいかない為
*          別の関数を用意する
*/
void CDrawingSpline::CalcOffsetIntersection ( std::vector<POINT2D>* pList,   
    double   dOffsetLenThis,
    POINT2D  &ptOffsetDirThis,
    const CDrawingObject* pObj, 
    double   dOffsetLenObj,
    POINT2D  &ptOffsetDirObj,
    bool bOnline,
    double dMin) const
{
    STD_ASSERT(pList != NULL);

    POINT2D ptTmp;
    const SPLINE2D* pSpline = NULL;
    SPLINE2D* pMatSpline = NULL;

    pList->clear();
    //レイヤーの確認
    if (!IsSameScale(pObj))
    {
        return;
    }

    const MAT2D* pMat =  GetParentMatrix();
    const MAT2D* pMat2 =  pObj->GetParentMatrix();
    if (pMat == NULL)
    {
        pSpline = &m_Spline;
    }
    else
    {
        pMatSpline = new SPLINE2D(m_Spline);
        pMatSpline->Matrix(*pMat);
        pSpline = pMatSpline;
    }


    try
    {
        switch (pObj->GetType())
        {
        case DT_POINT:
        {
            //近接点を算出(使用することはないはず)
            const CDrawingPoint* ptObj = dynamic_cast<const CDrawingPoint*>(pObj);

            STD_ASSERT(ptObj != NULL);


            if (pMat2 == NULL)
            {
                const POINT2D* pt2D = ptObj->GetPointInstance();
                ptTmp = pSpline->NearPoint(*pt2D, false);
            }
            else
            {
                POINT2D ptMat(*ptObj->GetPointInstance());
                ptMat.Matrix(*pMat2);
                ptTmp = pSpline->NearPoint(ptMat, false);

            }
            pList->push_back(ptTmp);
        }
        break;

        case DT_NODE:
        {
            //近接点を算出(使用することはないはず)
            const CDrawingNode* ptObj = dynamic_cast<const CDrawingNode*>(pObj);

            STD_ASSERT(ptObj != NULL);


            if (pMat2 == NULL)
            {
                const POINT2D* pt2D = ptObj->GetPointInstance();
                ptTmp = pSpline->NearPoint(*pt2D, false);
            }
            else
            {
                POINT2D ptMat(*ptObj->GetPointInstance());
                ptMat.Matrix(*pMat2);
                ptTmp = pSpline->NearPoint(ptMat, false);
            }
            pList->push_back(ptTmp);
        }
        break;

        case DT_LINE:
        {
            //交点を算出
            auto  offse1 = CDrawingObject::CloneShared(pObj);
            offse1->Offset(dOffsetLenObj, ptOffsetDirObj);

            const CDrawingLine* pDrawLine = dynamic_cast<const CDrawingLine*>(pObj);
            STD_ASSERT(pDrawLine != NULL);

            auto pLine = pDrawLine->GetLineInstance();
            auto lineOffset = pLine->Offset(dOffsetLenObj, ptOffsetDirObj); 

            if (pMat2 == NULL)
            {
                pSpline->IntersectOffset( lineOffset, pList, 
                            ptOffsetDirThis, dOffsetLenThis,
                            bOnline, dMin);
            }
            else
            {
                LINE2D line(lineOffset);
                line.Matrix(*pMat2);
                pSpline->IntersectOffset(line, pList, 
                                         ptOffsetDirThis, dOffsetLenThis, 
                                         bOnline, dMin);
            }
        }
        break;

        case DT_CIRCLE:
        {
            //交点を算出
            auto  offse1 = CDrawingObject::CloneShared(pObj);
            offse1->Offset(dOffsetLenObj, ptOffsetDirObj);

            const CDrawingCircle* pDrawCircle = dynamic_cast<const CDrawingCircle*>(pObj);
            STD_ASSERT(pDrawCircle != NULL);

            CIRCLE2D circleOffset = pDrawCircle->GetCircleInstance()->Offset(dOffsetLenObj, ptOffsetDirObj); 

            if (pMat2 == NULL)
            {
                pSpline->IntersectOffset( circleOffset, pList, 
                    ptOffsetDirThis, dOffsetLenThis,
                    bOnline, dMin);
            }
            else
            {
                ELLIPSE2D* pEllipse = NULL;
                circleOffset.Matrix(*pMat2, &pEllipse);

                if (!pEllipse)
                {
                    pSpline->IntersectOffset( circleOffset, pList, 
                        ptOffsetDirThis, dOffsetLenThis,
                        bOnline, dMin);
                }
                else
                {
                    pSpline->IntersectOffset( *pEllipse, pList, 
                        ptOffsetDirThis, dOffsetLenThis,
                        bOnline, dMin);
                    delete pEllipse;
                }
            }
        }
        break;


        case DT_ELLIPSE:
        {
            //交点を算出
            auto  offse1 = CDrawingObject::CloneShared(pObj);
            offse1->Offset(dOffsetLenObj, ptOffsetDirObj);

            const CDrawingEllipse* pDrawEllipse = dynamic_cast<const CDrawingEllipse*>(pObj);
            STD_ASSERT(pDrawEllipse != NULL);

            ELLIPSE2D ellipseOffset = pDrawEllipse->GetEllipseInstance()->Offset(dOffsetLenObj, ptOffsetDirObj); 

            if (pMat2 != NULL)
            {
                ellipseOffset.Matrix(*pMat2);
            }
            pSpline->IntersectOffset( ellipseOffset, pList, 
                ptOffsetDirThis, dOffsetLenThis,
                bOnline, dMin);
        }
        break;

        case DT_SPLINE:
        {
            //交点を算出
            const CDrawingSpline* pSpline2 = dynamic_cast<const CDrawingSpline*>(pObj);
            STD_ASSERT(pSpline != NULL);
            if (pMat2 == NULL)
            {
                pSpline->IntersectOffset( *pSpline2->GetSplineInstance(),
                                          pList, 
                                          ptOffsetDirThis,
                                          dOffsetLenThis,     
                                          ptOffsetDirObj, 
                                          dOffsetLenObj,                     
                                          bOnline);
            }
            else
            {
                SPLINE2D spline(*pSpline2->GetSplineInstance());
                spline.Matrix(*pMat2);

                pSpline->IntersectOffset( spline,
                    pList, 
                    ptOffsetDirThis,
                    dOffsetLenThis,     
                    ptOffsetDirObj, 
                    dOffsetLenObj,                     
                    bOnline);
            }
        }
        break;

        case DT_COMPOSITION_LINE:
        {
            //交点を算出
            const CDrawingCompositionLine* pCom = dynamic_cast<const CDrawingCompositionLine*>(pObj);
            STD_ASSERT(pCom != NULL);
            if (pMat2 == NULL)
            {
                pCom->CalcIntersection(pList, this, bOnline, dMin);
            }
            else
            {
                CDrawingCompositionLine com(*pCom);
                com.Matrix(*pMat2);
                com.CalcIntersection(pList, this, bOnline, dMin);
            }
        }
        break;


        default:
            break;
        }
    }
    catch(...)
    {

    }

    if (pMatSpline)
    {
        delete pMatSpline;
    }
}


/**
 *  @brief  位置調整.
 *  @param  [in]    pPtIntersect   調整位置
 *  @param  [in]    pPtClick       調整方向指定点
 *  @retval         なし
 *  @note
 */
bool CDrawingSpline::Trim (const POINT2D& ptIntersect, const POINT2D& ptClick, bool bReverse)
{
    AddChgCnt();
    m_Spline.Trim(ptIntersect, ptClick, bReverse);
    return true;
}

/**
 *  @brief  位置調整.
 *  @param  [in]    pPtIntersect   調整位置
 *  @param  [in]    pPtClick       調整方向指定点
 *  @retval         なし
 *  @note
 */
bool CDrawingSpline::TrimCorner (const POINT2D& ptIntersect, const POINT2D& ptClick)
{
    AddChgCnt();
    m_Spline.TrimCorner(ptIntersect, ptClick);
    return true;
}

/**
 *  @brief  分割.
 *  @param  [in]    pPtBreak  分割位置
 *  @retval         分割によって出来た
 *  @note
 */
std::shared_ptr<CDrawingObject>  CDrawingSpline::Break (const POINT2D& ptBreak)
{

    auto  splineBreak = std::make_shared<CDrawingSpline>(*this);

    SPLINE2D spline2D;
    m_Spline.Break(ptBreak, &spline2D);

    splineBreak->m_Spline = spline2D;

    //選択を解除しておく
    splineBreak->SetSelect(false);

    AddChgCnt();

    return splineBreak;
}

/**
 *  @brief  制御点描画.
 *  @param  [in]    pView      表示View
 *  @retval         なし
 *  @note
 */
void CDrawingSpline::DrawControl(CDrawingView* pView, int iLayerId, bool bLast)
{
    //コントロールポイント

    HDC hDc = pView->GetViewDc();

    int OldMode = SetROP2(hDc , R2_XORPEN);

    HPEN hPen = (HPEN)::CreatePen(PS_SOLID, 1, DRAW_CONFIG->GetBackColor());
    HPEN hOldPen = (HPEN)::SelectObject(hDc, (HGDIOBJ) hPen);
    
    int iLoop = 2;
    std::vector<LINE2D> lstLines;

    if (bLast)
    {
        iLoop = 1;
    }

    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }

    SPLINE2D* pSpline = &m_oldSpline;
    //制御点
    for (int nCnt = 0; nCnt < iLoop; nCnt++)
    {
        if (nCnt == 1)
        {
            pSpline = &m_Spline;
        }

        pSpline->GetLines(&lstLines);
        LineMulti( pView, iLayerId, &lstLines);
    }
    m_oldSpline = m_Spline;


     ::SelectObject(hDc, (HGDIOBJ) hOldPen);
     DeleteObject(hPen);

    SetROP2(hDc , OldMode );
}


/**
 *  @brief  描画.
 *  @param  [in]  pView       表示View
 *  @param  [in]  bMouseOver  true オブジェクト上にマウスあり
 *  @retval       なし
 *  @note
 */
void CDrawingSpline::Draw(CDrawingView* pView,
                          const std::shared_ptr<CDrawingObject> pParentObject,
                          E_MOUSE_OVER_TYPE eMouseOver,
                          int iLayerId)
{
    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }

    CPartsDef* pCtrl = pView->GetPartsDef();
    CLayer* pLayer   = pCtrl->GetLayer(iLayerId);
    COLORREF crObj;
    int      iID;

    bool bIgnoreSelect = false;
    if (eMouseOver == E_MO_IGNORE_SELECT)
    {
        bIgnoreSelect = true;
    }

    crObj = GetDrawColor(pView, iLayerId, pParentObject.get(), &iID, bIgnoreSelect);

    std::vector<LINE2D> lstLine;

    //--------------------
    //  MATRIX
    //--------------------
    SPLINE2D tmpSpline(m_Spline);
    MAT2D matDraw;
    if (pParentObject )
    {
        const MAT2D* pMat = pParentObject->GetDrawingMatrix();
        if (pMat)
        {
            matDraw = *pMat;
        }
    }

    if (m_pMatOffset)
    {
        matDraw = matDraw * (*m_pMatOffset);
    }
    tmpSpline.Matrix(matDraw);


    //--------------------
	bool bRough = false;
	std::vector<POINT2D> lstPoint;
	RECT2D viewRect;
	if (bRough)
	{
		//荒い線
		tmpSpline.GetLines(&lstLine);
	}
	else
	{
		viewRect = pView->GetViewArea(iLayerId);
		double dDotLength = pView->DotToLength(1);
		tmpSpline.DivideLine(&lstPoint, viewRect, dDotLength);
	}

    int iWidth = m_iWidth;
    if (eMouseOver == E_MO_SELECTABLE)
    {
        pView->SetLineAlternateMode(true);
        iWidth += 2;
    }
    else if (eMouseOver == E_MO_EMPHASIS)
    {
        crObj = DRAW_CONFIG->crEmphasis;
    }
	else if (eMouseOver == E_MO_CONNECTABLE)
	{
		crObj = DRAW_CONFIG->crConnectable;
	}

	if (m_Fill.eFillType != FIT_NONE)
	{
		COLORREF crFill;
		if (pLayer->bUseLayerColor)
		{
			crFill = pLayer->crLayer;
		}
		else
		{
			crFill = m_Fill.cr1;
		}

		pView->BeginPath();
		pView->SetPen(PS_SOLID, 1, crObj, iID);
		if (bRough)
		{
			LineMulti(pView, iLayerId, &lstLine);
		}
		else
		{
			LineMulti(pView, iLayerId, &lstPoint);
		}

		pView->CloseFigure();
		pView->EndPath();

		if (m_Fill.eFillType == FIT_SOLID)
		{
			pView->FillPath(crFill, m_nId);
		}
		else if (m_Fill.eFillType == FIT_HATCHING)
		{
			pView->HatchingPath(m_Fill.cr1, crObj, m_nId);
		}


		POINT   ptViewStart;
		if (bRough)
		{
			pView->ConvWorld2Scr(&ptViewStart, lstLine[0].GetPt1(), iLayerId);
		}
		else
		{
			if (lstPoint.size() > 0)
			{
				pView->ConvWorld2Scr(&ptViewStart, lstPoint[0], iLayerId);
				pView->MoveTo(ptViewStart);
			}
		}
	}

    pView->SetPen(m_iLineType,  iWidth, crObj, iID);
	if (bRough)
	{
		LineMulti(pView, iLayerId, &lstLine);
	}
	else
	{
		LineMulti(pView, iLayerId, &lstPoint);
	}


    if (eMouseOver == E_MO_SELECTABLE)
    {
        pView->SetLineAlternateMode(false);
    }
}


/**
 *  @brief  描画削除.
 *  @param  [in]    pView   表示View
 *  @retval         なし
 *  @note
 */
void CDrawingSpline::DeleteView(CDrawingView* pView, 
                                const std::shared_ptr<CDrawingObject> pParentObject,
                                int iLayerId)
{
    //背景色
    COLORREF crObj;
    crObj = DRAW_CONFIG->GetBackColor();

    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }

    //--------------------
    //  MATRIX
    //--------------------
    const MAT2D* pMat =  NULL;
    if (pParentObject )
    {
        pMat = pParentObject->GetDrawingMatrix();
    }

    SPLINE2D tmpSpline(m_Spline);
    if (pMat != NULL)
    {
        tmpSpline.Matrix(*pMat);
    }
    //--------------------

    std::vector<LINE2D> lstLine;
    tmpSpline.GetLines(&lstLine);

    pView->SetPen(m_iLineType,  m_iWidth, crObj, 0);
    LineMulti( pView,  iLayerId, &lstLine);
}

/**
 *  @brief   描画(マウスオーバー時).
 *  @param   [in] pView 表示View
 *  @param   [in] bOver true:開始 false:終了
 *  @retval  なし
 *  @note
 */
void CDrawingSpline::DrawOver( CDrawingView* pView, bool bOver)
{
}

/**
 *  @brief  点追加.
 *  @param  [in]    pt   追加点w
 *  @retval         なし
 *  @note
 */
void CDrawingSpline::AddControlPoint(const POINT2D& pt)
{
    AddChgCnt();
    m_Spline.AddControlPoint(pt);
}

/**
 *  @brief  点追加.
 *  @param  [in]    pt   追加点w
 *  @retval         なし
 *  @note
 */
void CDrawingSpline::AddPoint(const POINT2D& pt)
{
	AddChgCnt();
	m_Spline.AddPoint(pt);
}

/**
 *  @brief  閉曲線設定
 *  @param  [in]    bClose true:閉曲線
 *  @retval         なし
 *  @note
 */
void CDrawingSpline::SetClose(bool bClose)
{
    m_Spline.SetClose(bClose);
}

//!< 
/**
 *  @brief  閉曲線設定問合
 *  @param  なし
 *  @retval true:閉曲線
 *  @note
 */
bool CDrawingSpline::IsClose() const
{
    return m_Spline.IsClose();
}

/**
 *  @brief  範囲.
 *  @param  [in] p1 1点目
 *  @param  [in] p2 2点目
 *  @retval  true:2点で示される矩形内に点が存在する 
 *  @note
 */
bool CDrawingSpline::IsInner( const RECT2D& rcArea, bool bPart) const
{
    std::vector<POINT2D> lstPoint;
    std::vector<LINE2D*> lstLine;

    LINE2D line1(rcArea.dLeft , rcArea.dTop,    rcArea.dLeft , rcArea.dBottom);
    LINE2D line2(rcArea.dRight, rcArea.dTop,    rcArea.dRight, rcArea.dBottom);
    LINE2D line3(rcArea.dLeft , rcArea.dTop,    rcArea.dRight, rcArea.dTop);
    LINE2D line4(rcArea.dLeft , rcArea.dBottom, rcArea.dRight, rcArea.dBottom);

    lstLine.push_back(&line1);
    lstLine.push_back(&line2);
    lstLine.push_back(&line3);
    lstLine.push_back(&line4);

    //--------------------
    //  MATRIX
    //--------------------
    const MAT2D* pMat =  GetParentMatrix();
    SPLINE2D tmpSpline(m_Spline);
    if (pMat != NULL)
    {
        tmpSpline.Matrix(*pMat);
    }
    //--------------------

    if(bPart)
    {
        foreach(LINE2D* pLine, lstLine)
        {
            if(tmpSpline.Intersect(*pLine, &lstPoint, true))
            {
                foreach(POINT2D& pt, lstPoint)
                {
                    if( pLine->IsOnLine(pt))
                    {
                        return true;
                    }
                }
            }
        }
    }

    RECT2D rcSpline = tmpSpline.GetBounds();

    if (rcArea.IsInside(rcSpline))
    {
        return true;
    }
    return false;
}

/**
 *  @brief  線幅設定.
 *  @param  [in]    iWidth   線幅
 *  @retval         なし
 *  @note
 */
void CDrawingSpline::SetLineWidth(int iWidth)
{
    m_iWidth = iWidth;
}

/**
 *  @brief  線幅取得.
 *  @param          なし
 *  @retval         線幅
 *  @note
 */
int CDrawingSpline::GetLineWidth() const
{
    return m_iWidth;
}

/**
 *  @brief  線種設定.
 *  @param  [in]    iType   線種
 *  @retval         なし
 *  @note
 */
void CDrawingSpline::SetLineType(int iType)
{
    m_iLineType = iType;
}

/**
 *  @brief  線種取得.
 *  @param          なし 
 *  @retval         線種
 *  @note
 */
int  CDrawingSpline::GetLineType() const
{
    return m_iLineType;
}

/**
 *  @brief  制御点検索.
 *  @param  [in] pt 制御点位置
 *  @retval 制御点番号 -1:見つからなかった
 *  @note
 */
/*
int  CDrawingSpline::SearchCtrlPoint(const POINT2D& pt)
{
    int iSize = GetCtrlPointNum();
    for (int iCnt = 0; iCnt < iSize; iCnt++)
    {
        if (GetCtrlPoint(iCnt) == pt)
        {
            return iCnt;
        }
    }
    return -1;
}
*/

/**
 * @brief   端点1取得
 * @param   なし
 * @retval  端点1
 * @note	
 */
POINT2D  CDrawingSpline::GetPoint1() const
{
    POINT2D ptRet;
	ptRet = m_Spline.GetStartPoint();
    return ptRet;
}

/**
 * @brief   端点2取得
 * @param   なし
 * @retval  端点2
 * @note	
 */
POINT2D  CDrawingSpline::GetPoint2() const
{
    POINT2D ptRet;
	ptRet = m_Spline.GetEndPoint();
    return ptRet;
}

//!< プロパティ初期化
TREE_GRID_ITEM*  CDrawingSpline::_CreateStdPropertyTree()
{
    NAME_TREE_ITEM<CStdPropertyItem>* pBeforeGroup;
    NAME_TREE_ITEM<CStdPropertyItem>* pNextItem;
    NAME_TREE_ITEM<CStdPropertyItem>* pGroupItem;
    NAME_TREE<CStdPropertyItem>* pTree;
    //int iId;
    CStdPropertyItem* pItem;
    boost::tuple< CStdPropertyItem*, void*> pairVal;


    pBeforeGroup = CDrawingObject::_CreateStdPropertyTree();
    pTree = m_psProperty->GetTreeInstance();

    pGroupItem  = pTree->AddNext(pBeforeGroup, NULL, GET_STR(STR_PRO_SPLINE), _T("Spline"));

#ifdef _DEBUG //Clockwise
    DB_PRINT(_T(" Clockwise %s \n"), (m_Spline.IsClockwise()? _T("true"): _T("false"))); 
#endif
    //------------
    //制御点
    //------------
    /*
	std::vector< POINT2D > tmpList;
    CStdPropertyItemDef DefCtrl(PROP_POINT_LIST, 
        GET_STR(STR_PRO_CTRL_POINT)   , 
        _T("CtrlPoints"),
        GET_STR(STR_PRO_INFO_CTRL_POINT), 
        true,
        DISP_UNIT,
        tmpList);

    pItem = new CStdPropertyItem( DefCtrl, PropPoints, m_psProperty, PropPointsUpdate, &m_Spline.m_lstPoints);
    pNextItem = pTree->AddChild(pGroupItem, pItem, DefCtrl.strDspName, pItem->GetName());
	*/

	//------------
	//スプライン種別
	//------------
	StdString strSplineType;
	strSplineType += GET_STR(STR_BSPLINE2);
	strSplineType += _T(":");
	strSplineType += boost::lexical_cast<StdString>(SPLINE2D::SPT_BSPLINE2);
	strSplineType += _T(",");

	strSplineType += GET_STR(STR_BSPLINE3);
	strSplineType += _T(":");
	strSplineType += boost::lexical_cast<StdString>(SPLINE2D::SPT_BSPLINE3);

	CStdPropertyItemDef DefSplineType(PROP_DROP_DOWN_ID, //型
		GET_STR(STR_PRO_SPLINE_TYPE),              //データ表示名
		_T("SplienType"),                          //名称
		GET_STR(STR_PRO_INFO_SPLINE_TYPE),         //データ説明
		false,                                     //編集可、不可
		DISP_NONE,                                 //単位
		static_cast<int>(m_Spline.m_eSplineType),      //初期値
		strSplineType,                             //最小値・リスト設定
		0);

	pItem = new CStdPropertyItem(
		DefSplineType,            //定義データ
		PropSplineType,           //入力時コールバック
		m_psProperty,              //親グリッド
		NULL,                      //更新時コールバック
		&m_Spline.m_eSplineType);  //実データ

	pNextItem = pTree->AddChild(pGroupItem, pItem,
		DefSplineType.strDspName,
		pItem->GetName());

	//------------
	// 編集方法
	//------------
	StdString strEditType;
	strEditType += GET_STR(STR_CONTROL_POINT);
	strEditType += _T(":");
	strEditType += boost::lexical_cast<StdString>(SPLINE2D::EDT_CTRL);
	strEditType += _T(",");

	strEditType += GET_STR(STR_PASS_POINT);
	strEditType += _T(":");
	strEditType += boost::lexical_cast<StdString>(SPLINE2D::EDT_PASS_POINT);

	CStdPropertyItemDef DefEditType(PROP_DROP_DOWN_ID, //型
		GET_STR(STR_PRO_EDIT_TYPE),              //データ表示名
		_T("EditType"),                          //名称
		GET_STR(STR_PRO_INFO_EDIT_TYPE),         //データ説明
		false,                                     //編集可、不可
		DISP_NONE,                                 //単位
		static_cast<int>(m_Spline.m_eEditType),      //初期値
		strEditType,                             //最小値・リスト設定
		0);

	pItem = new CStdPropertyItem(
		DefEditType,            //定義データ
		PropEditType,           //入力時コールバック
		m_psProperty,              //親グリッド
		NULL,                      //更新時コールバック
		&m_Spline.m_eEditType);  //実データ

	pNextItem = pTree->AddNext(pNextItem, pItem,
		DefEditType.strDspName,
		pItem->GetName());


	//--------------------
	//  閉曲線
	//--------------------
	CStdPropertyItemDef DefClose(PROP_BOOL,         //表示タイプ
		GET_STR(STR_PRO_SPLINE_CLOSE),              //表示名
		_T("Close"),                                //変数名
		GET_STR(STR_PRO_INFO_SPLINE_CLOSE),         //表示説明
		true,                                       //編集可不可
		DISP_NONE,                                  //表示単位
		static_cast<int>(m_Spline.m_bClose)         //初期値
	);

	pItem = new CStdPropertyItem(
		DefClose,
		PropClose,
		m_psProperty,
		NULL,
		(void*)& m_Spline.m_bClose);

	pNextItem = pTree->AddNext(pNextItem,
		pItem,
		DefClose.strDspName,
		pItem->GetName());



    //------------
    //線幅
    //------------
    CStdPropertyItemDef DefLineWidth(PROP_LINE_WIDTH, 
        GET_STR(STR_PRO_LINE_WIDTH)   , 
        _T("Width"),
        GET_STR(STR_PRO_INFO_LINE_WIDTH), 
        true,
        DISP_NONE,
        0);

    pItem = new CStdPropertyItem( DefLineWidth, PropLineWidth, m_psProperty, NULL, &m_iWidth);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefLineWidth.strDspName, pItem->GetName());

    //------------
    //線種
    //------------
    CStdPropertyItemDef DefLineType(PROP_LINE_TYPE, 
        GET_STR(STR_PRO_LINE_TYPE)   , 
        _T("Type"),
        GET_STR(STR_PRO_INFO_LINE_TYPE), 
        false,
        DISP_NONE,
        0);

    pItem = new CStdPropertyItem( DefLineType, PropLineType, m_psProperty, NULL, &m_iLineType);
    pNextItem = pTree->AddNext(pNextItem, pItem, DefLineType.strDspName, pItem->GetName());

	//--------------------
	// 塗りつぶし
	//--------------------
	pNextItem = m_Fill.CreateStdPropertyTree(m_psProperty, pTree, pNextItem);
	
	return pGroupItem;

}

/**
 *  @brief   プロパティ変更(制御点)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval  なし
 *  @note
 */
bool CDrawingSpline::PropPoints    (CStdPropertyItem* pData, void* pObj)
{
    //TODO:プロパティは通過点に変更する
    CDrawingSpline* pDrawing = reinterpret_cast<CDrawingSpline*>(pObj);
    try
    {
        pDrawing->m_pCtrl->HideObject(pDrawing->m_nId);
        pDrawing->AddChgCnt();
       
        std::vector<POINT2D> lstPoints = pData->anyData.GetPointList();
        pDrawing->m_Spline.SetPoints(lstPoints);
		pDrawing->Redraw();
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;

}

/**
 *  @brief   プロパティ変更(スプライン種別)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval  なし
 *  @note
 */
bool CDrawingSpline::PropSplineType(CStdPropertyItem* pData, void* pObj)
{
	//TODO:プロパティは通過点に変更する
	CDrawingSpline* pDrawing = reinterpret_cast<CDrawingSpline*>(pObj);
	try
	{
		pDrawing->AddChgCnt();
		int iShape = pData->anyData.GetInt();
		auto eShape = static_cast<SPLINE2D::SPLINE_TYPE>(iShape);
		pDrawing->m_pCtrl->HideObject(pDrawing->m_nId);
		pDrawing->m_Spline.SetType(eShape);
		pDrawing->Redraw();
	}
	catch (...)
	{
		STD_DBG(_T("SetError"));
		return false;
	}
	return true;

}


/**
 *  @brief   プロパティ変更(編集方法)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval  なし
 *  @note
 */
bool CDrawingSpline::PropEditType(CStdPropertyItem* pData, void* pObj)
{
	//TODO:プロパティは通過点に変更する
	CDrawingSpline* pDrawing = reinterpret_cast<CDrawingSpline*>(pObj);
	try
	{
		pDrawing->AddChgCnt();
		int iType = pData->anyData.GetInt();
		auto eType = static_cast<SPLINE2D::EDIT_TYPE>(iType);
		pDrawing->m_pCtrl->HideObject(pDrawing->m_nId);
		pDrawing->m_Spline.SetEditType(eType);
		pDrawing->Redraw();
	}
	catch (...)
	{
		STD_DBG(_T("SetError"));
		return false;
	}
	return true;
}

/**
 *  @brief   プロパティ変更(閉曲線)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval  なし
 *  @note
 */
bool CDrawingSpline::PropClose(CStdPropertyItem* pData, void* pObj)
{
	//TODO:プロパティは通過点に変更する
	CDrawingSpline* pDrawing = reinterpret_cast<CDrawingSpline*>(pObj);
	try
	{
		pDrawing->AddChgCnt();
		bool bClose = pData->anyData.GetBool();
		pDrawing->m_pCtrl->HideObject(pDrawing->m_nId);
		pDrawing->m_Spline.SetClose(bClose);
		pDrawing->Redraw();

	}
	catch (...)
	{
		STD_DBG(_T("SetError"));
		return false;
	}
	return true;

}

/**
 *  @brief   プロパティ変更(線幅)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingSpline::PropLineWidth (CStdPropertyItem* pData, void* pObj)
{
    CDrawingSpline* pDrawing = reinterpret_cast<CDrawingSpline*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->m_pCtrl->HideObject(pDrawing->m_nId);
        pDrawing->SetLineWidth(pData->anyData.GetInt());
		pDrawing->Redraw();
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(線種)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CDrawingSpline::PropLineType  (CStdPropertyItem* pData, void* pObj)
{
    CDrawingSpline* pDrawing = reinterpret_cast<CDrawingSpline*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->m_pCtrl->HideObject(pDrawing->m_nId);
        pDrawing->SetLineType(pData->anyData.GetInt());
		pDrawing->Redraw();
	}
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

bool CDrawingSpline::PropPointsUpdate  (CStdPropertyItem* pData, void* pObj)
{
    CDrawingSpline* pDrawing = reinterpret_cast<CDrawingSpline*>(pObj);
    pDrawing->m_Spline.CalcControlPoint();
    return true;
}


/**
 *  @brief   参照データに基づいて更新.
 *  @param   
 *  @retval  
 *  @note
 */
CDrawingObject* CDrawingSpline::UpdateRef()
{
    CDrawingObject*      pRef;

    pRef = CDrawingObject::UpdateRef();

    if (pRef == NULL)
    {
        return NULL;
    }

    CDrawingSpline* pSpline = dynamic_cast<CDrawingSpline*>(pRef);

    STD_ASSERT(pSpline != NULL);

    if (pSpline == NULL)
    {
        return false;
    }


    m_Spline     = pSpline->m_Spline;
    m_iWidth    = pSpline->m_iWidth;
    m_iLineType = pSpline->m_iLineType;

	m_iChgCnt = pRef->GetChgCnt();
    return pRef;
}

/**
 *  @brief   スナップ点取得.
 *  @param   [out] pLstSnap スナップ点リスト
 *  @retval  true: スナップ点あり
 *  @note
 */
bool CDrawingSpline::GetSnapList(std::list<SNAP_DATA>* pLstSnap)const 
{
    using namespace VIEW_COMMON;

    DWORD dwSnap = DRAW_CONFIG->GetSnapType();
    bool bRet = false;

    STD_ASSERT(pLstSnap);

    if (!pLstSnap)
    {
        return false;
    }

    pLstSnap->clear();
    SNAP_DATA snapData;
    snapData.bKeepSelect = false;
    
    if ((dwSnap & SNP_END_POINT) &&
        (!IsClose()))
    {
        snapData.eSnapType = SNP_END_POINT;
        snapData.pt = GetPoint1();
        pLstSnap->push_back(snapData);
        snapData.pt = GetPoint2();
        pLstSnap->push_back(snapData);
        bRet = true;
    }

    if (dwSnap & SNP_FEATURE_POINT)
    {
        snapData.eSnapType = SNP_FEATURE_POINT;
		if (GetEditType() == SPLINE2D::EDT_PASS_POINT)
		{
			int iNum = m_Spline.GetPointNum();
			for (int iCnt = 0; iCnt < iNum; iCnt++)
			{
				snapData.pt = m_Spline.GetPoint(iCnt);
				pLstSnap->push_back(snapData);
			}
		}
		else
		{
			int iNum = m_Spline.GetControlPointNum();
			for (int iCnt = 0; iCnt < iNum; iCnt++)
			{
				snapData.pt = m_Spline.GetControlPoint(iCnt);
				pLstSnap->push_back(snapData);
			}
		}
        bRet = true;
    }
    return bRet;
}

/**
 *  @brief   始点終点交換.
 *  @param   なし
 *  @retval  なし
 *  @note    Line,Circle,Splie,CompositLine
 */
bool  CDrawingSpline::SwapStartEnd()
{
    m_Spline.SwapStartEnd();
    return true;
}

/**
 *  @brief   始点取得.
 *  @param   なし
 *  @retval  始点
 *  @note    Line,Circle,Splie,CompositLine
 */
POINT2D  CDrawingSpline::GetStartSide() const
{
    return m_Spline.GetStartPoint();
}

/**
 *  @brief   終点取得.
 *  @param   なし
 *  @retval  始点
 *  @note    Line,Circle,Splie,CompositLine
 */
POINT2D  CDrawingSpline::GetEndSide() const
{
    return m_Spline.GetEndPoint();
}

/**
*  @brief   中点取得.
*  @param   なし
*  @retval  中点
*  @note    Line,Circle,Splie,CompositLine
*/
POINT2D  CDrawingSpline::GetMidPoint() const
{
    return GetPointByRate(0.5);
}

POINT2D  CDrawingSpline::GetPointByRate(double d) const
{
    POINT2D ptRet;
    double dMid  =  m_Spline.m_dStart * (1.0 - d) + m_Spline.m_dEnd * d;
    m_Spline.CalcPos(dMid, &ptRet);

    return ptRet;
}

//!<  近接点取得(Line,Circle,Splie,CompositLine用)
bool  CDrawingSpline::GetNearPoints(POINT2D*  ptNear, const POINT2D& pt, bool bOnline) const
{
    POINT2D ptRet = m_Spline.NearPoint(pt, bOnline);

    *ptNear = ptRet; 
    return true;
}


//!<  法線角取得(Line,Circle,Splie,CompositLine用)
bool  CDrawingSpline::GetNormAngle(double* pAngle, const POINT2D& pt, bool bOnline) const
{
    double dT = m_Spline.NearPointCoff( pt, bOnline);

    LINE2D lTangent;
    bool bRet = m_Spline.CalcTangent(dT, &lTangent);

    *pAngle = lTangent.Angle();
    return bRet;
}

double CDrawingSpline::Length(const POINT2D& ptIntersect, bool bStart) const
{
    double dRet;
    dRet = m_Spline.ArcLength(ptIntersect, bStart);
    return dRet;
}

//!< オフセット(線,円弧,スプライン上)
bool CDrawingSpline::Offset(double dLen, POINT2D ptDir)
{
    m_Spline = m_Spline.Offset(dLen, ptDir);
    return true;
}

//!< オフセット(線,円弧,スプライン上)
bool CDrawingSpline::Offset(double dLen)
{
    double dT = 0.5;
    LINE2D l;


    m_Spline.CalcNorm(dT, &l);

    POINT2D ptNorm = l.GetPt2() - l.GetPt1(); 
    if(dLen < 0.0)
    {
        ptNorm.dX = -ptNorm.dX;
        ptNorm.dY = -ptNorm.dY;
        POINT2D pt2 = l.GetPt1() +  ptNorm;
        m_Spline = m_Spline.Offset(fabs(dLen),  pt2);
    }
    else
    {
        m_Spline = m_Spline.Offset(dLen,  l.GetPt2());
    }
    return true;
}

//!< 点が進行方向の右側にあるか
bool CDrawingSpline::IsRightSide(POINT2D ptDir) const
{
    double dT =   m_Spline.NearPointCoff(ptDir, true);
    LINE2D l;
    m_Spline.CalcNorm(dT, &l);

    bool bRet = false;
    double dDir = CUtil::GetDirection(l.GetPt1(),  l.GetPt2(), ptDir);
    if(dDir >= 0.0)
    {
        bRet = true;
    }

    return bRet;
}


/**
 * @brief   領域取得
 * @param   なし
 * @retval  なし
 * @note	描画領域を取得する
 */
RECT2D CDrawingSpline::GetBounds() const
{
    return m_Spline.GetBounds();
}

/**
 * @brief   距離
 * @param   [in] pt 
 * @retval  近接点からの距離
 * @note
 */
double CDrawingSpline::Distance(const POINT2D& pt) const
{
    POINT2D ptNear = m_Spline.NearPoint( pt, true /*bOnline*/);
    return pt.Distance(ptNear);
}


//!< マーカ初期化 true:マーカあり
bool CDrawingSpline::InitNodeMarker(CNodeMarker* pMarker)
{
    CDrawingView* pView = pMarker->GetView();
    pView->ClearDraggingNodes();
    pView->ClearDraggingTmpObjects();


    double dLen = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->GetMarkerSize());  
    int iR =  CUtil::Round(dLen * pView->GetDpu());

    pMarker->SetRadius(iR);
    pMarker->SetObject(shared_from_this());

    auto lstNode = pView->GetDraggingNodes();

    lstNode->clear();
	int iPointMax;
	if (GetEditType() == SPLINE2D::EDT_CTRL)
	{
		iPointMax = m_Spline.GetControlPointNum();
	}
	else
	{
		iPointMax = m_Spline.GetPointNum();
	}

	for (int iIndex = 0;
		iIndex < iPointMax;
		++iIndex)
	{
		std::shared_ptr<CDrawingNode> pNode;
		POINT2D pt;
		if (GetEditType() == SPLINE2D::EDT_CTRL)
		{
			pt = m_Spline.GetControlPoint(iIndex);
		}
		else
		{
			pt = m_Spline.GetPoint(iIndex);
		}
		pNode = std::make_shared<CDrawingNode>();
		pNode->SetVisible(true);
		pNode->SetMarkerShape(DMS_CIRCLE_FILL);

		StdStringStream strm;
		strm << StdFormat(_T("P,%d")) % iIndex;
		pNode->SetName(strm.str().c_str());
		pNode->SetPoint(pt);
		lstNode->push_back(pNode);
		pMarker->Create(pNode.get(), pNode->GetPoint());
	}


	//マウスカーソル設定コールバック
    namespace  PLC = std::placeholders;
	pMarker->SetOnSetcursorCallback(std::bind(&CDrawingSpline::OnSetCursor, this, PLC::_1, PLC::_2));

    //ドラッグ表示用の図形を用意する
    auto lstTmpObject = pView->GetDraggingTmpObjects();
    lstTmpObject->clear();
    lstTmpObject->push_back(std::make_shared<CDrawingSpline>(*this)); //表示用
    auto pSplineTmp = std::dynamic_pointer_cast<CDrawingSpline>(lstTmpObject->at(0));
    pSplineTmp->SetColor(DRAW_CONFIG->crImaginary);

	if (GetEditType() == SPLINE2D::EDT_CTRL)
	{
		//制御線を用意する
		auto  psControlLines = std::make_shared<CDrawingCompositionLine>();
		psControlLines->SetColor(DRAW_CONFIG->crImaginary);
		psControlLines->SetLineType(PS_DOT);
		psControlLines->SetLineWidth(1);
		psControlLines->SetLayer(m_iLayerId);

		std::vector<POINT2D> lstPoint;
		pSplineTmp->GetSplineInstance()->GetControlPoints(&lstPoint);
		if (pSplineTmp->IsClose())
		{
			if (!lstPoint.empty())
			{
				lstPoint.push_back(lstPoint[0]);
			}
		}

		CPartsDef* pDef = pView->GetPartsDef();
		psControlLines->CreateMultiLine(lstPoint);
		pDef->SetMouseEmphasis(psControlLines);
		lstTmpObject->push_back(psControlLines); //制御線
	}

    return true;
}

//!< マーカ選択
void CDrawingSpline::SelectNodeMarker(CNodeMarker* pMarker, 
                                    StdString strMarkerId)
{
     pMarker->SetVisibleOnly( strMarkerId, true);

}

int  CDrawingSpline::_GetMarkerIndex(StdString strMarkerId) const
{
     //!< CSV区切りで分割
    std::vector<StdString> lstMarker;
    CUtil::TokenizeCsv(&lstMarker, strMarkerId);

    if (lstMarker.size() != 2)
    {
        return -1;
    }

    if (lstMarker[0] != _T("P"))
    {
        return -1;
    }

    return  boost::lexical_cast<int>(lstMarker[1]);
}

//!< マーカ移動
void CDrawingSpline::MoveNodeMarker(CNodeMarker* pMarker, 
                                    SNAP_DATA* pSnap,
                                    StdString strMarkerId,
                                    MOUSE_MOVE_POS posMouse)
{

 	using namespace VIEW_COMMON;
     

     //!< CSV区切りで分割
    std::vector<StdString> lstMarker;
    CUtil::TokenizeCsv(&lstMarker, strMarkerId);

 

    int iIndex =  _GetMarkerIndex(strMarkerId) ;
     if(iIndex == -1)
    {
        return;
    }

    CDrawingView* pView = pMarker->GetView();
    SNAP_DATA snap;
    DWORD dwSnapType = DRAW_CONFIG->GetSnapType();

    bool bSnap = pView->GetSnapPoint(&snap, 
                      dwSnapType,
                      posMouse.ptSel);

    bool bIgnoreSnap = false;
    if (posMouse.nFlags & MK_SHIFT)
    {
        //スナップ無視
        bIgnoreSnap = true;
        pSnap->eSnapType = SNP_NONE;
        pSnap->pt = pSnap->ptOrg;
    }

    auto lstTmpObject = pView->GetDraggingTmpObjects();


    auto pSplineTmp = std::dynamic_pointer_cast<CDrawingSpline>(lstTmpObject->at(0));
	CPartsDef* pDef = pView->GetPartsDef();
	pDef->SetMouseOver(pSplineTmp);

	if (GetEditType() == SPLINE2D::EDT_CTRL)
	{
		//制御線を用意する
		auto psControlLines = std::dynamic_pointer_cast<CDrawingCompositionLine>(lstTmpObject->at(1));
		std::vector<POINT2D> lstPoint;
		pSplineTmp->SetControlPoint(iIndex, snap.pt);
		pSplineTmp->GetSplineInstance()->GetControlPoints(&lstPoint);
		if (pSplineTmp->IsClose())
		{
			if (!lstPoint.empty())
			{
				lstPoint.push_back(lstPoint[0]);
			}
		}
		psControlLines->CreateMultiLine(lstPoint);
		pDef->SetMouseEmphasis(psControlLines);
	}
	else
	{
		pSplineTmp->SetPoint(iIndex, snap.pt);
	}
    pSnap->strSnap = snap.strSnap;
}

//!< マーカ開放
bool CDrawingSpline::ReleaseNodeMarker(CNodeMarker* pMarker, 
                                        StdString strMarkerId, 
                                        MOUSE_MOVE_POS posMouse)
{
    CDrawingView* pView = pMarker->GetView();
    CPartsDef*   pDef  = pView->GetPartsDef();
    CUndoAction*       pUndo;
    pUndo  = pDef->GetUndoAction();

    SetSelect(false);

    auto lstTmpObject = pView->GetDraggingTmpObjects();
    auto pSplineTmp = std::dynamic_pointer_cast<CDrawingSpline>(lstTmpObject->at(0));


   int iIndex =  _GetMarkerIndex(strMarkerId) ;
     if(iIndex == -1)
    {
        return false;
    }

    pUndo->AddStart(UD_CHG, pDef, this);
    m_Spline = *pSplineTmp->GetSplineInstance();
    pUndo->AddEnd(shared_from_this(), true);


    pView->ClearDraggingNodes();
    pView->ClearDraggingTmpObjects();

    ::SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
    pMarker->Clear();
    return true;

}

bool CDrawingSpline::OnSetCursor(CNodeMarker* pNodeMarker, CDrawingView* pView)
{
   StdString strId = pNodeMarker->GetMouseOver();


   if(_GetMarkerIndex(strId) != -1)
   {
	    HCURSOR hCursor;
		hCursor =(HCURSOR)LoadImage(NULL, IDC_SIZEALL,IMAGE_CURSOR,
                                    NULL,NULL,LR_DEFAULTCOLOR | LR_SHARED);
 		::SetCursor(hCursor);
        return true;
   }

	return false;

}


//!< load save のインスタンスが生成用ダミー
void CDrawingSpline::Dummy()
{
    unsigned int iCnt = 0;
    std::string s = "";

    StdStreamOut outFs(_T(""));
    StdStreamIn  inFs(_T(""));
    //boost::filesystem::ofstream outTxtFs(_T(""));
    //boost::filesystem::ifstream inTxtFs(_T(""));

    boost::archive::text_woarchive txtOut(outFs);
    boost::archive::text_wiarchive txtIn(inFs);

    Save<boost::archive::text_woarchive>(txtOut, iCnt, s);
    Load<boost::archive::text_wiarchive>(txtIn, iCnt, s);

    StdXmlArchiveOut outXml(outFs);
    StdXmlArchiveIn inXml(inFs);

    Save<StdXmlArchiveOut>(outXml, iCnt, s);
    Load<StdXmlArchiveIn>(inXml, iCnt, s);


    boost::filesystem::ofstream outFsStd(_T(""));
    boost::filesystem::ifstream inFsStd(_T(""));

    boost::archive::binary_oarchive outBin(outFsStd);
    boost::archive::binary_iarchive inBin(inFsStd);

    Save<boost::archive::binary_oarchive>(outBin, iCnt, s);
    Load<boost::archive::binary_iarchive>(inBin, iCnt, s);
}

//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void TEST_CDrawingSpline()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}
#endif //_DEBUG