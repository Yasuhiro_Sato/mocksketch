/**
* @brief        CDrawingSplineヘッダーファイル
* @file	        CDrawingSpline.h
* @author           Yasuhiro Sato
* @date	        07-2-2009 20:03:09
* 
* @note	
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
* 
* @remarks	        
*			
*
* $
* $
* 
*/
#ifndef __DRAWING_SPLINE_H_
#define __DRAWING_SPLINE_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DrawingObject/CDrawingObject.h"

#include "DrawingObject/Primitive/LINE2D.h"
#include "DrawingObject/Primitive/SPLINE2D.H"
#include "./CFill.h"


/**
* @class   CDrawingSpline
* @brief  
*/
class CDrawingSpline :public CDrawingObject//:public CRTTIClass<CDrawingSpline, CDrawingObject, DT_LINE>
{
public:


public:
    //!< コンストラクタ
    CDrawingSpline();

    //!< コンストラクタ
    CDrawingSpline(int nID);

	//!< コピーコンストラクター
    CDrawingSpline(const CDrawingSpline& Obj);

    //!< デストラクタ
    virtual ~CDrawingSpline();

	//ディフォルト設定時の初期化
	virtual void InitDefault();
	
	//!< 相対移動
    virtual void Move(const POINT2D& pt2D);

    //!< 回転
    virtual void Rotate(const POINT2D& pt2D, double dAngle);

    //!< 交点計算
    virtual void CalcIntersection ( std::vector<POINT2D>* pLiat,   
        const CDrawingObject* pObj, 
        bool bOnline = true,
        double dMin = NEAR_ZERO) const;


    //!< 交点計算
    virtual void CalcOffsetIntersection ( std::vector<POINT2D>* pList,   
        double   dOffsetLenThis,
        POINT2D  &ptOffsetDirThis,
        const CDrawingObject* pObj, 
        double   dOffsetLenObj,
        POINT2D  &ptOffsetDirObj,
        bool bOnline = true,
        double dMin = NEAR_ZERO) const;


    //!< 鏡像
	virtual void Mirror(const LINE2D& line) override;

    //!< 倍率
	virtual void Scl(const POINT2D& pt2D, double dXScl, double dYScl) override;

    //!< 行列適用
    virtual void Matrix(const MAT2D& mat2D) override;

    //!< 調整
    virtual bool Trim (const POINT2D& ptIntersect, const POINT2D& ptClick, bool bReverse) override;

    //!< 調整          
    virtual bool TrimCorner (const POINT2D& ptIntersect, const POINT2D& ptClick) override;

    //!< 分割
    virtual std::shared_ptr<CDrawingObject>  Break (const POINT2D& ptBreak) override;

    //!< 範囲
    virtual bool IsInner( const RECT2D& rcArea, bool bPart) const override;

    //!< 描画
    virtual void Draw(CDrawingView* pView,
                      const std::shared_ptr<CDrawingObject> pParentObject,
                      E_MOUSE_OVER_TYPE eMouseOver = E_MO_NO,
                      int iLayerId = -1) override;

    //!< 制御点描画
    virtual void DrawControl(CDrawingView* pView, int iLayerId, bool bLast);

    //!< 描画削除
    virtual void DeleteView(CDrawingView* pView,
                            const std::shared_ptr<CDrawingObject> pParentObject,
                            int iLayerId = -1) override;

    //!< マウスオーバー
    virtual void DrawOver( CDrawingView* pView , bool bOver);

    //!< 閉曲線設定
    void SetClose(bool bClose = true);

    //!< 閉曲線設定問合
    bool IsClose() const;


	//----------
	// 通過点
	//----------
	//!< 点追加
	void AddPoint(const POINT2D& pt);

	//!< 通過点数
    int GetPointNum() const {return m_Spline.GetPointNum();}

    //!< 通過点
    POINT2D GetPoint(int nNum) const {return m_Spline.GetPoint(nNum);}

    //!< 通過点設定
    bool SetPoint(int nNum, const POINT2D& pt){return m_Spline.SetPoint(nNum, pt);}

    //!< 最終通過点削除
    void  DelLastPoint(){m_Spline.DelLastPoint();}

    //!< 通過点削除
    bool DelPoint(int nNum){return m_Spline.DelPoint(nNum);}

	//!< 全点削除
	void  Clear() { m_Spline.Clear(); }

	//----------
	// 制御点
	//----------
	//!< 制御点追加
	void AddControlPoint(const POINT2D& pt);

	//!< 制御点数
	int GetControlPointNum() const { return m_Spline.GetControlPointNum(); }

	//!< 制御点
	POINT2D GetControlPoint(int nNum) const { return m_Spline.GetControlPoint(nNum); }

	//!< 制御点設定
	bool SetControlPoint(int nNum, const POINT2D& pt) { return m_Spline.SetControlPoint(nNum, pt); };

	//!< 最終制御点削除
	void DelLastControlPoint() { m_Spline.DelLastControlPoint(); }

	//!< 制御点削除
	bool DelControlPoint(int nNum) { return m_Spline.DelControlPoint(nNum); }

	//----------

	void SetEditType(SPLINE2D::EDIT_TYPE type) { m_Spline.SetEditType(type); }
	SPLINE2D::EDIT_TYPE GetEditType() const{ return m_Spline.GetEditType(); }



    //!< インスタンス取得
    SPLINE2D* GetSplineInstance(){return &m_Spline;}
    const SPLINE2D* GetSplineInstance() const {return &m_Spline;}

    //!< 領域取得
    virtual RECT2D GetBounds() const;

    //!< 距離
    virtual double Distance(const POINT2D& pt) const;

    void Dummy();

    //-------------------------
    // リフレクション設定
    //-------------------------
friend CDrawingSpline;

    //!< 線幅設定
    void SetLineWidth(int iWidth);

    //!< 線幅取得
    int GetLineWidth() const;

    //!< 線種設定
    void SetLineType(int iType); 

    //!< 線種取得
    int  GetLineType() const;

    //!< 端点1取得
    POINT2D  GetPoint1() const;

    //!< 端点2取得
    POINT2D  GetPoint2() const;

    //!< プロパティ初期化
    TREE_GRID_ITEM*  _CreateStdPropertyTree();

    //!< スナップ点取得
    virtual  bool GetSnapList(std::list<SNAP_DATA>* pLstSnap)const override;

    //!< 始点終点交換(Line,Circle,Splie,CompositLine用)
    virtual bool  SwapStartEnd()override ;

    //!< 始点取得(Line,Circle,Splie,CompositLine用)
    virtual POINT2D  GetStartSide() const override;

    //!< 始点取得(Line,Circle,Splie,CompositLine用)
    virtual POINT2D  GetEndSide() const override;

    //!<  中点取得(Line,Circle,Splie,CompositLine用)
    virtual POINT2D  GetMidPoint() const override;

    //!<  任意点取得(Line,Circle,Splie,CompositLine用)
    virtual POINT2D  GetPointByRate(double d) const override;

    //!<  近接点取得(Line,Circle,Splie,CompositLine用)
    virtual bool  GetNearPoints(POINT2D*  ptNear, const POINT2D& pt, bool bOnline) const override;

    //!<  法線角取得(Line,Circle,Splie,CompositLine用)
    virtual bool GetNormAngle(double* pAngle, const POINT2D& pt, bool bOnline) const override;

    //!< 長さ(線,円弧,スプライン上)
    virtual double Length(const POINT2D& ptIntersect, bool bStart) const override;

    //!< オフセット(線,円弧,スプライン上)
    virtual bool Offset(double dLen, POINT2D ptDir) override;

    //!< オフセット(線,円弧,スプライン上)
    virtual bool Offset(double dLen) override;

    //!< 点が進行方向の右側にあるか
    virtual bool IsRightSide(POINT2D ptDir) const override;

    //-------------------
    //!< プロパティ変更
    //-------------------
    static bool PropPoints    (CStdPropertyItem* pData, void* pObj);
    static bool PropLineWidth (CStdPropertyItem* pData, void* pObj);
    static bool PropLineType  (CStdPropertyItem* pData, void* pObj);
	static bool PropSplineType(CStdPropertyItem* pData, void* pObj);
	static bool PropEditType  (CStdPropertyItem* pData, void* pObj);
	static bool PropClose     (CStdPropertyItem* pData, void* pObj);
    static bool PropPointsUpdate  (CStdPropertyItem* pData, void* pObj);
    //-------------------

    //-------------------------
    // 参照
    //-------------------------

    //!< 参照データに基づいて更新
    virtual CDrawingObject* UpdateRef();

    //-------------------
    // ドラッグ用マーカ
    //-------------------
    //!< マーカ初期化 true:マーカあり
    virtual bool InitNodeMarker(CNodeMarker* pMarker) override;

    //!< マーカ選択
    virtual void SelectNodeMarker(CNodeMarker* pMarker, 
                                     StdString strMarkerId) override;

    //!< マーカ移動
    virtual void MoveNodeMarker(CNodeMarker* pMarker, 
                                     SNAP_DATA* pSnap,
                                     StdString strMarkerId,
                                     MOUSE_MOVE_POS posMouse) override;

    //!< マーカ開放
    virtual bool ReleaseNodeMarker(CNodeMarker* pMarker, StdString strMarkerId, MOUSE_MOVE_POS posMouse) override;

    bool OnSetCursor(CNodeMarker* pNodeMarker, CDrawingView* pView);

	CFill<CDrawingSpline>* GetFillInstance() { return &m_Fill; }

protected:
    int  _GetMarkerIndex(StdString strMarkerId) const;

protected:
    SPLINE2D       m_Spline;

    //!< 線幅
    int            m_iWidth;

    //!< 線種
    int            m_iLineType;

    //--------------
    // 制御点描画用
    //--------------
    SPLINE2D       m_oldSpline;

	CFill<CDrawingSpline> m_Fill;


private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
            ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(CDrawingObject);
            SERIALIZATION_BOTH("Spline"   , m_Spline); 
            SERIALIZATION_BOTH("Width"    , m_iWidth);
            SERIALIZATION_BOTH("LineType" , m_iLineType);
			SERIALIZATION_BOTH("Fill", m_Fill);
		MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }
};

BOOST_CLASS_VERSION(CDrawingSpline, 0);

#endif // __DRAWING_SPLINE_H_