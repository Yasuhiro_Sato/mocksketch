﻿/**
* @brief			CDrawingText実装ファイル
* @file			CDrawingText.cpp
* @author			Yasuhiro Sato
* @date			07-2-2009 20:03:09
* 
* @note	
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
* 
* @remarks	        
*			
*
* $
* $
* 
*/
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "Utility/CUtility.h"
#include "./CDrawingPoint.h"
#include "./CDrawingLine.h"
#include "./CDrawingSpline.h"
#include "./CDrawingCircle.h"
#include "./CDrawingText.h"
#include "./CDrawingParts.h"
#include "./CDrawingDim.h"
#include "./CDrawingEllipse.h"
#include "./CDrawingSpline.h"
#include "./CDrawingCompositionLine.h"
#include "./CDrawingGroup.h"
#include "./CDrawingNode.h"
#include "./CNodeMarker.h"
#include "./CNodeBound.h"
#include "./CNodeData.h"
#include "Utility/ExtText/TEXT_FRAME.H"
#include "Utility/ExtText/TEXT_MARGIN.H"
#include "Utility/ExtText/LINE_PROPERTY.H"
#include "Utility/ExtText/FONT_DATA.H"


#include "DefinitionObject/View/CDrawingView.h"
#include "DefinitionObject/CPartsDef.h"
#include "View/CLayer.h"
#include "View/CDrawMarker.h"
#include "View/CUndoAction.h"

#include "System/CSystem.h"
#include <math.h>
#include <boost/serialization/export.hpp> 
BOOST_CLASS_EXPORT(CDrawingText);

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define DRAW_MARGIN 1
#define DRAW_DEBUG_AREA 0

/**
* コンストラクタ
*/

/**
* コンストラクタ
*/
CDrawingText::CDrawingText(int nID):
    CDrawingObject( nID, DT_TEXT),
    m_bCopyNodeDataMode(true),
    m_iTmpConnectionObjId(-1),
    m_dLayerScl(1.0)
{
    m_iPropFeatures = P_BASE;                  
    m_dTmpAngle = 0.0;
    m_eHit = H_NONE;
    m_bDragNode = false;
    m_pViewOpen = NULL;
    namespace  PLC = std::placeholders;
}


CDrawingText::CDrawingText():
    CDrawingText(-1)
{
}


/**
* コピーコンストラクタ
*/
CDrawingText::CDrawingText(const CDrawingText& Obj):
    CDrawingObject( Obj)
{
    m_eType   = DT_TEXT;
    m_Text    = Obj.m_Text;
    m_ptPos   = Obj.m_ptPos;
    m_dTmpAngle = Obj.m_dTmpAngle;
    m_eHit      = Obj.m_eHit;
    m_bDragNode = false;
    m_pViewOpen = NULL;

    if (m_bCopyNodeDataMode)
    {
        size_t iSize = Obj.m_lstConnection.size();
        if (iSize != 0)
        {
            for(size_t iNo = 0; iNo < iSize; ++iNo)
            {
                CNodeData* pCd = Obj.m_lstConnection[iNo].get();
                m_lstConnection.push_back(std::make_unique<CNodeData>(*pCd));
            }
        }
    }
    m_iTmpConnectionObjId = Obj.m_nId;
    m_dLayerScl           = Obj.m_dLayerScl; 
}

/**
* デストラクタ
*/

CDrawingText::~CDrawingText()
{
    //ClearConnection();
}


/**
*  @brief  回転.
*  @param  [in]    pPt2D    回転中心
*  @param  [in]    dAngle   回転角
*  @retval     なし
*  @note   相対回転
*/
void CDrawingText::Rotate(const POINT2D& pt2D, double dAngle)
{
    if(m_pCtrl)
    {
        AddChgCnt();
    }
    m_ptPos.Rotate(pt2D, dAngle);

    if (!m_Text.IsFixRotate())
    {
        double dTxtAngle = m_Text.GetAngle();
        dTxtAngle += dAngle;
        dTxtAngle = CUtil::NormAngle(dTxtAngle); 
        m_Text.SetAngle(dTxtAngle);
        m_dTmpAngle = dTxtAngle;
    }
    RecalcFramePos();
    UpdateNodeSocketData();
}

/**
*  @brief  相対移動.
*  @param  [in]    pPt2D   移動量
*  @retval         なし
*  @note
*/
void CDrawingText::Move(const POINT2D& pt2D) 
{
    if(m_pCtrl)
    {
        AddChgCnt();
    }
    m_ptPos.Move(pt2D);
    RecalcFramePos();
    UpdateNodeSocketData();
}


/**
*  @brief  倍率.
*  @param  [in]    pPt2D   拡大中心
*  @param  [in]    dSclX   X倍率
*  @param  [in]    dSclY   Y倍率
*  @retval         なし
*  @note
*/
void CDrawingText::Scl(const POINT2D& pt2D, double dXScl, double dYScl)
{
    if(m_pCtrl)
    {
        AddChgCnt();
    }
    m_ptPos.Scl(pt2D, dXScl, dYScl);
    RecalcFramePos();
    UpdateNodeSocketData();
}


/**
*  @brief  鏡像.
*  @param  [in]    line   対称軸
*  @retval         なし
*  @note
*/
void CDrawingText::Mirror(const LINE2D& line)
{
    if(m_pCtrl)
    {
        AddChgCnt();
    }
    m_ptPos.Mirror(line);
    RecalcFramePos();
    UpdateNodeSocketData();
}

/**
*  @brief  行列.
*  @param  [in]    mat2D   適用する行列
*  @retval         なし
*  @note
*/
void CDrawingText::Matrix(const MAT2D& mat2D)
{
    if(m_pCtrl)
    {
        AddChgCnt();
    }

    m_Text.Matrix(mat2D);
    double dAngle = m_Text.GetAngle();
    m_dTmpAngle = dAngle;
    m_ptPos.Matrix( mat2D);

    RecalcFramePos();
    UpdateNodeSocketData();
}

const MAT2D* CDrawingText::GetDrawingMatrix()const
{
    return &m_mat2D;
}

/**
* @brief 線描画(マウスオーバー時)
* @param  [in]    pView 表示ビュー  
* @param  [in] bOver true:開始 false:終了
* @retval  なし
* @note	
*/
void CDrawingText::Draw(CDrawingView* pView, 
    const std::shared_ptr<CDrawingObject> pParentObject,
    E_MOUSE_OVER_TYPE eMouseOver,
    int iLayerId)
{
    COLORREF crObj;
    int      iID;
    bool bFroce = false;

    bool bIgnoreSelect = false;
    if (eMouseOver == E_MO_IGNORE_SELECT)
    {
        bIgnoreSelect = true;
    }

    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }

    CPartsDef* pCtrl = pView->GetPartsDef();
    CLayer* pLayer   = pCtrl->GetLayer(iLayerId);

    m_dLayerScl = pLayer->dScl;

    if (!pLayer->bVisible)
    {
        return;
    }


    crObj = GetDrawColor(pView, iLayerId, pParentObject.get(), &iID, bIgnoreSelect, &bFroce);

    if (eMouseOver == E_MO_EMPHASIS)
    {
        crObj = DRAW_CONFIG->crEmphasis;
        bFroce = true;
    }

    _Draw( pView, pParentObject.get(), bFroce, crObj, iID, eMouseOver, iLayerId);
}


/**
*  @brief  描画.
*  @param  [in]    pView   表示View
*  @param  [in]    bColor  強制色設定の有無
*  @param  [in]    crObj   強制描画色
*  @param  [in]    iId     ID
*  @retval         なし
*  @note
*/
void CDrawingText::GetDispSize(SIZE* pSize, const CDrawingView* pView) const
{
    //表示スケールを設定(レイヤーのスケールは無視)
    double dScl = pView->GetViewScale();

    POINT2D ptTl;
    m_Text.GetDispSize(pSize, pView->GetBackDc(), pView, dScl);
}

void CDrawingText::RecalcFramePos()
{
    /*
    CExtText::TEXT_FRAME* pFrame;
    pFrame = m_Text.Frame();
    if (!pFrame)
    {
    return;
    }

    POINT2D ptO = _FramePos(&m_Text);

    POINT2D ptTmp(m_ptPos);


    ptTmp.Matrix(m_mat2D);

    POINT2D ptTL;
    ptTL =   ptTmp + ptO;

    double dC = pFrame->GetCorner();
    bool bR = false;
    if (fabs(dC) > NEAR_ZERO)
    {
    bR = true;
    }

    _SetFrame(pFrame, ptTL);

    double dAngle = m_Text.GetAngle();
    double dSclX;
    double dSclY; 
    double dA;
    double dT;
    POINT2D pt2D;

    m_mat2D.GetAffineParam(&pt2D, &dA, &dSclX, &dSclY, &dT);
    dAngle = CUtil::NormAngle(dA + dAngle);

    std::vector<POINT2D> lstCenter;

    for(POINT2D& ptLine: pFrame->lstFrame)
    {
    ptLine.Rotate(ptTmp, dAngle);
    }

    pFrame->lstFrame.push_back(pFrame->lstFrame[0]);
    */
}



/**
*  @brief  描画.
*  @param  [in] pView   表示View
*  @param  [in] bForceColor  強制色設定の有無
*  @param  [in] crObj   強制描画色
*  @param  [in] iId     ID
*  @param  [in] bOver true:開始 false:終了
*  @retval         なし
*  @note
*/
void CDrawingText::_Draw(CDrawingView* pView, 
    const CDrawingObject* pParentObject,
    bool bForceColor, 
    COLORREF crObj, 
    int iId,
    E_MOUSE_OVER_TYPE eMouseOver,
    int iLayerId)
{

    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }

    //-------------------
    // MATRIX
    //-------------------
    MAT2D matDraw;
    if (pParentObject )
    {
        const MAT2D* pMat = pParentObject->GetDrawingMatrix();
        if (pMat)
        {
            matDraw = *pMat;
        }
    }

    m_ptTmp = m_ptPos;
    CExtText tmpText(m_Text);

  

    if (m_pMatOffset)
    {
        matDraw = matDraw * (*m_pMatOffset);
    }

    m_mat2D = matDraw;

    if(m_pCtrl)
    {
        AddChgCnt();
    }

    tmpText.Matrix(matDraw);
    m_dTmpAngle = tmpText.GetAngle();
    m_ptTmp.Matrix(matDraw);

    CLayer* pLayer = pView->GetLayer(iLayerId);

    DRAWING_MODE eMode = pView->GetDrawMode();

    //表示スケールを設定
    double dScl;

    dScl = pView->GetViewScale();
    if (!tmpText.IsFixSize())
    {
        dScl *= pLayer->dScl;
    }

    bool  bEmphasis= false;

    if (eMouseOver == E_MO_EMPHASIS)
    {
        bEmphasis = true;
    }
    else if (eMouseOver ==E_MO_SELECTABLE)
    {
        bEmphasis = true;
    }
    else if (eMouseOver == E_MO_CONNECTABLE)
    {
        // crPen = DRAW_CONFIG->crConnectable;
    }

    POINT pt;
    COLORREF crText = crObj;
    COLORREF crFrame = crObj;

    if (m_eHit == H_TEXT)
    {
        crText = DRAW_CONFIG->crImaginary;
        crFrame = DRAW_CONFIG->crSelect;
        bForceColor = true;
    }
    else if (m_eHit == H_FRAME)
    {
        crText = DRAW_CONFIG->crSelect;
        crFrame = DRAW_CONFIG->crImaginary;
        bForceColor = true;
    }

    pView->ConvWorld2Scr(&pt, m_ptTmp, iLayerId);
    TEXT_PARAM param;
    param.dScl         = dScl;           //倍率 
    param.dOffsetAngle = 0;
    param.pt2dSel =  m_ptTmp;

    param.bSelect =  IsSelect();
    param.bDrag = m_bDragNode;

    if (bEmphasis)
    {
        COLORREF crBack = DRAW_CONFIG->GetBackColor();
        param.iId       = 0;
        param.bForceColor    = true;         //強制色設定の有無
        param.cr        = crBack;
        param.bEmphasis = false;       //太字
        param.bFrameColor  = true;         //フレーム強制色設定の有無
        param.crFrame      = crBack;      //フレーム強制設定色

        pView->Text(&tmpText, 
            pt,         //表示位置
            &param,
            iLayerId);
    }


    param.iId          = iId;            //インデックス
    param.bForceColor       = bForceColor;    //強制色設定の有無
    param.cr           = crText;         //設定色
    param.bEmphasis    = bEmphasis;      //太字
    param.bFrameColor  = bForceColor;    //フレーム強制色設定の有無
    param.crFrame      = crFrame;        //フレーム強制設定色

    pView->Text(&tmpText,
        pt, 
        &param,
        iLayerId);


    ///================================
#if 0
    RECT2D rc = GetBounds();
    rc.Expansion(3.0);
    POINT2D pt2D_Bounds[4];

    pt2D_Bounds[0].Set(rc.dLeft, rc.dTop);
    pt2D_Bounds[1].Set(rc.dRight, rc.dTop);
    pt2D_Bounds[2].Set(rc.dRight, rc.dBottom);
    pt2D_Bounds[3].Set(rc.dLeft, rc.dBottom);

    POINT pt2Bounds[4];

    int iCnt = 0;
    for( auto p: pt2D_Bounds)
    {
        pView->ConvWorld2Scr(&pt2Bounds[iCnt], p, iLayerId);
        iCnt++;
    }

    pView->SetPen(PS_SOLID,  1, RGB(0,255,255), m_nId);

    pView->MoveTo(pt2Bounds[0]);
    pView->LineTo(pt2Bounds[1]);
    pView->LineTo(pt2Bounds[2]);
    pView->LineTo(pt2Bounds[3]);
    pView->LineTo(pt2Bounds[0]);
#endif
    ///================================

    if(m_Text.Frame()->IsAutoFit())
    {
        m_Text.Frame()->SetWidth(tmpText.Frame()->GetWidth());
        m_Text.Frame()->SetHeight(tmpText.Frame()->GetHeight());
        m_Text.Frame()->SetOffset(tmpText.Frame()->GetOffset());
    }
    m_Text.Frame()->lstDispFrame =  tmpText.Frame()->lstDispFrame;
    m_Text.Frame()->lstFrame =  tmpText.Frame()->lstFrame;
}


/**
*  @brief  描画削除.
*  @param  [in]    pView   表示View
*  @retval         なし
*  @note
*/
void CDrawingText::DeleteView(CDrawingView* pView, 
    const std::shared_ptr<CDrawingObject> pParentObject,
    int iLayerId)
{
    //背景色
    COLORREF crObj;
    crObj = DRAW_CONFIG->GetBackColor();

    if (iLayerId == -1)
    {
        iLayerId = m_iLayerId;
    }

    _Draw( pView, pParentObject.get(), true, DRAW_CONFIG->GetBackColor(), 0, E_MO_NO, iLayerId);
}



/** 
*  @brief   描画(マウスオーバー時).
*  @param   [in] pView 表示View
*  @param   [in] bOver true:開始 false:終了
*  @retval  なし
*  @note
*/
void CDrawingText::DrawOver(const CDrawingView* pView, bool bOver)
{


}

/**
*  範囲.
*  @param      [in] p1 1点目
*  @param      [in] p2 2点目
*  @retval     true:2点で示される矩形内に点が存在する 
*  @note
*/
bool CDrawingText::IsInner( const RECT2D& rcArea, bool bPart) const
{
    auto pLst = m_Text.GetFramePoint();
    _AdjustFrame(pLst, m_dLayerScl, m_Text.IsFixSize());


    if (pLst->size() < 4)
    {
        return false;
    }

    CDrawingLine  lList[4];
    for(int i = 0; i < 4 ; i++)
    {
        int j = (i + 1) % 4;
        lList[i].SetPoint1(pLst->at(i));
        lList[i].SetPoint2(pLst->at(j));
    }

    if(bPart)
    {
        for(auto l: lList)
        {
            if(l.IsInner(rcArea, bPart))
            {
                return true;
            }
        }
        return false;
    }
    else
    {
        for(auto l: lList)
        {
            if(!l.IsInner(rcArea, bPart))
            {
                return false;
            }
        }
        return true;
    }
    return false;
}

/**
*  @brief   プロパティ初期化.
*  @param   [in] pCtrl オブジェクトコントロールへのポインタ
*  @retval     なし
*  @note
*/
TREE_GRID_ITEM*  CDrawingText::_CreateStdPropertyTree()
{

    NAME_TREE_ITEM<CStdPropertyItem>* pBeforeGroup;
    NAME_TREE_ITEM<CStdPropertyItem>* pNextItem;
    NAME_TREE_ITEM<CStdPropertyItem>* pGroupItem;
    NAME_TREE<CStdPropertyItem>* pTree;
    //int iId;
    CStdPropertyItem* pItem;
    boost::tuple< CStdPropertyItem*, void*> pairVal;


    pBeforeGroup = CDrawingObject::_CreateStdPropertyTree();
    pTree = m_psProperty->GetTreeInstance();

    pGroupItem  = pTree->AddNext(pBeforeGroup, NULL, GET_STR(STR_PRO_TEXT), _T("Text"));


    //------------
    //位置
    //------------
    CStdPropertyItemDef DefPos(PROP_POINT2D, 
        GET_STR(STR_PRO_POS)   , 
        _T("Pos"),
        GET_STR(STR_PRO_INFO_POS), 
        true,
        DISP_UNIT,
        POINT2D());

    pItem = new CStdPropertyItem( DefPos, PropPos, m_psProperty, NULL, &m_ptPos);
    pNextItem = pTree->AddChild(pGroupItem, pItem, DefPos.strDspName, pItem->GetName(), 2);

    //------------

    pNextItem = m_Text.CreateStdPropertyTree(m_psProperty,  pTree,  pNextItem);


    return pGroupItem;

}


/**
*  @brief   プロパティ変更(位置)
*  @param   [in] pData  変更アイテム
*  @param   [in] pObj   このオブジェクト
*  @retval     なし
*  @note
*/
bool CDrawingText::PropPos (CStdPropertyItem* pData, void* pObj)
{
    CDrawingText* pDrawing = reinterpret_cast<CDrawingText*>(pObj);
    try
    {
        pDrawing->AddChgCnt();
        pDrawing->m_pCtrl->HideObject(pDrawing->m_nId);
        pDrawing->SetPoint(pData->anyData.GetPoint());
        pDrawing->m_pCtrl->Draw(pDrawing->m_nId);
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}



/**
*  @brief   角度取得
*  @param   なし
*  @retval  角度
*  @note
*/
double  CDrawingText::GetAngle() const
{
    return m_Text.GetAngle();
}

/**
*  @brief   角度設定
*  @param   [in] dAngle 角度
*  @retval  なし
*  @note
*/
void  CDrawingText::SetAngle(double dAngle)
{
    m_Text.SetAngle(dAngle);
    m_dTmpAngle = dAngle;
}

/**
*  @brief   色設定
*  @param   [in] color 色
*  @retval  なし
*  @note
*/
void CDrawingText::SetColor(COLORREF color)
{
    m_crObj = color;
    m_Text.SetColor(-1, -1, m_crObj);
}

/**
*  @brief   テキスト設定
*  @param   [in] strTxt 
*  @retval  なし
*  @note
*/
void CDrawingText::SetText(StdString& strText)
{
    m_Text.SetText(strText.c_str());
}

/**
*  @brief   テキスト設定
*  @param   [in] strTxt 
*  @retval  なし
*  @note
*/
void CDrawingText::SetTextS(std::string& strTxt)
{
    StdString strOut =  CUtil::CharToString(strTxt.c_str());
    SetText(strOut);
}

/**
*  @brief   参照データに基づいて更新.
*  @param   
*  @retval  
*  @note
*/
CDrawingObject* CDrawingText::UpdateRef()
{
    CDrawingObject*      pRef;

    pRef = CDrawingObject::UpdateRef();

    if (pRef == NULL)
    {
        return NULL;
    }

    CDrawingText* pText = dynamic_cast<CDrawingText*>(pRef);

    STD_ASSERT(pText != NULL);

    if (pText == NULL)
    {
        return false;
    }


    m_ptPos     = pText->m_ptPos; 
    m_Text      = pText->m_Text;

    m_iChgCnt = pRef->GetChgCnt();
    return pRef;
}

/**
*  @brief   スナップ点取得.
*  @param   [out] pLstSnap スナップ点リスト
*  @retval  true: スナップ点あり
*  @note
*/
bool CDrawingText::GetSnapList(std::list<SNAP_DATA>* pLstSnap)const 
{
    using namespace VIEW_COMMON;

    DWORD dwSnap = DRAW_CONFIG->GetSnapType();
    bool bRet = false;

    STD_ASSERT(pLstSnap);

    if (!pLstSnap)
    {
        return false;
    }

    pLstSnap->clear();
    SNAP_DATA snapData;

    if (dwSnap & SNP_DATUME_POINT)
    {
        snapData.eSnapType = SNP_DATUME_POINT;
        snapData.pt = GetPoint();

        if (dwSnap & SNP_TEXT)
        {
            snapData.bKeepSelect = true;
        }
        else
        {
            snapData.bKeepSelect = false;
        }
        pLstSnap->push_back(snapData);

        bRet = true;
    }

    if (!m_Text.IsEnableFrame())
    {
        return bRet;
    }

    bool bFeature = (dwSnap & SNP_FEATURE_POINT) == SNP_FEATURE_POINT;
    bool bNode    = (dwSnap & SNP_NODE) == SNP_NODE;


    snapData.bKeepSelect = false;

    if (bFeature)
    {
        snapData.eSnapType = SNP_FEATURE_POINT;

        auto pRect = m_Text.GetFramePoint();
        _AdjustFrame(pRect, m_dLayerScl, m_Text.IsFixSize());

        for(auto pt: *pRect)
        {
            snapData.pt = pt;
            pLstSnap->push_back(snapData);
            bRet = true;
        }
    }

    if (bNode)
    {
        const CNodeData* pConnection;
        snapData.eSnapType = SNP_NODE;
        for(int iNo = 0; iNo < 4; iNo++)
        {
            pConnection = GetNodeDataConst(iNo); 
            if(pConnection)
            {
                snapData.pt = pConnection->ptPos;
                snapData.iConnectionIndex = iNo;
                snapData.pConnection = std::make_shared<CNodeData>(*pConnection);
                pLstSnap->push_back(snapData);
                bRet = true;
            }
        }
    }

    return bRet;
}

bool CDrawingText::CreateNodeData(int iConnectionId)
{
    if (m_lstConnection.empty())
    {
        for(int i=0; i< 4; i++)
        {
            m_lstConnection.push_back(std::make_unique<CNodeData>());
        }

        for (auto ite =    m_lstConnection.begin();
            ite != m_lstConnection.end();
            ++ite)
        {
            (*ite)->iMaxSocket= -1;
            (*ite)->iMaxPlug= 1;
            (*ite)->eConnectionDirection = E_INOUT;
            (*ite)->eConnectionType = E_PASSIVE;
            //テスト用
            (*ite)->nodeProperty.m_eShape = DMS_RECT_FILL;
        }
    }
    return true;
}

CNodeData* CDrawingText::GetNodeData(int iId) 
{
    if(!m_Text.IsEnableFrame())
    {
        return NULL;
    }

    CreateNodeData(0);   

    if (iId < 0){return NULL;}
    if (iId > 3){return NULL;}

    GetNodeDataPos(&m_lstConnection[iId]->ptPos, iId);
    return m_lstConnection[iId].get();
}

CNodeData* CDrawingText::GetNodeDataConst(int iId) const 
{
    if(!m_Text.IsEnableFrame())
    {
        return NULL;
    }

    if (iId < 0){return NULL;}
    if (iId > 3){return NULL;}

    if (m_lstConnection.empty())
    {
        return NULL;
    }

    GetNodeDataPos(&m_lstConnection[iId]->ptPos, iId);
    return m_lstConnection[iId].get();
}


int CDrawingText::GetNodeDataNum() const 
{
    return 4;
}

bool CDrawingText::GetNodeDataPos(POINT2D* pPt, int iIndex) const
{
    if(!m_Text.IsEnableFrame()){return false;}
    if (iIndex < 0){return false;}
    if (iIndex > 3){return false;}
    if(!pPt)              {return false;}

    auto list = m_Text.GetFramePoint();
    _AdjustFrame(list, m_dLayerScl, m_Text.IsFixSize());


    if (list->size() != 5)
    {
        return false;
    }
    POINT2D pt;

    //LIST
    //０:左下
    //１:右下
    //２:右上
    //３:左上
    pt = (list->at(iIndex) +
        list->at((iIndex + 1) % 4)) / 2.0;
    /*
    switch (iIndex)
    {
    case 0://下
    pt.dX = list->at(4).dX;
    pt.dY = list->at(0).dY;
    break;

    case 1://右
    pt.dX = list->at(1).dX;
    pt.dY = list->at(4).dY;
    break;

    case 2://上
    pt.dX = list->at(4).dX;
    pt.dY = list->at(2).dY;
    break;

    case 3://左
    pt.dX = list->at(0).dX;
    pt.dY = list->at(4).dY;
    break;

    default:
    return false;
    }
    */


    *pPt = pt;
    //DB_PRINT(_T("CDrawingText::GetNodeDataPos %d (%s)\n"), iIndex, pt.Str().c_str());
    return true;
}

RECT2D::E_EDGE CDrawingText::InquireConnectDirection(std::map<RECT2D::E_EDGE, LINE2D>* pDir,
    RECT2D* pRc,
    int iIndex )const
{
    RECT2D::E_EDGE retEdge =  RECT2D::E_NONE;

    POINT2D pt;
    if(!GetNodeDataPos(&pt, iIndex))
    {
        return retEdge;
    }

    pDir->clear();
    double dSnapLen = DRAW_CONFIG->DimToDispUnit(DRAW_CONFIG->dConnectionStraghtLength);
    RECT2D rc;
    rc = GetBounds();
    rc.Expansion(dSnapLen);

    POINT2D ptOnEdge;
    retEdge = rc.NearEdgePoint(&ptOnEdge, pt);


    std::pair<RECT2D::E_EDGE, LINE2D> pair;
    pair.first = retEdge;
    pair.second.SetPt(pt, ptOnEdge); 
    pDir->insert(pair);
    *pRc =  rc;

    return retEdge;
}

/**
* @brief   領域取得
* @param   なし
* @retval  なし
* @note	描画領域を取得する
*/
RECT2D CDrawingText::GetBounds() const
{
    RECT2D rc;
    auto pLst = m_Text.GetFramePoint();
    _AdjustFrame(pLst, m_dLayerScl, m_Text.IsFixSize());

    if(pLst)
    {
        int iCnt = 0;
        for(auto pt: *pLst)
        {
            if(iCnt == 0)
            {
                rc.dTop = pt.dY;
                rc.dRight = pt.dX;
                rc.dBottom = pt.dY;
                rc.dLeft = pt.dX;
            }
            else
            {
                rc.ExpandArea(pt);
            }
            iCnt++;
        }
    }
    return rc;
}

/**
* @brief   距離
* @param   [in] pt 
* @retval  近接点からの距離
* @note
*/
double CDrawingText::Distance(const POINT2D& pt) const
{
    return m_ptPos.Distance(pt);
}

void CDrawingText::SetSelect(bool bSel, const RECT2D* pRect )
{
    bool bOld = IsSelect();
    if (!bSel)
    {
        m_eHit = H_NONE;
    }
    CDrawingObject::SetSelect( bSel,  pRect );
    if (!bSel)
    {
        CloseText();
    }
}

//選択状態でのマウス移動
void CDrawingText::PreSelectedMouseMove(CDrawingView* pView, MOUSE_MOVE_POS posMouse)
{
    //マーカが表示されていない時はまだ選択状態としない
    int iId  = pView->SearchPos(posMouse.ptSel);

    bool bOnNode;
    bOnNode = ((posMouse.nFlags & MK_ON_MARKER) != 0);

    if (iId != m_nId)
    {
        m_eHit = H_NONE;
        return;
    }

    POINT2D pt2dMousePos;
    int iLayerId = pView->GetCurrentLayerId();
    pView->ConvScr2World(&pt2dMousePos, posMouse.ptSel, iLayerId);

    E_HIT eHit;
    eHit = H_TEXT;


    bool bCheckFrame = true;

    if (bCheckFrame)
    {

        std::vector<POINT2D>* pRect;
        pRect = m_Text.GetFramePoint();
        _AdjustFrame(pRect, pView, m_Text.IsFixSize());

        bool bIn1, bIn2; 
        bIn1 = CUtil::IsInnerTriangle(pt2dMousePos, pRect->at(0), pRect->at(1), pRect->at(2));
        bIn2 = CUtil::IsInnerTriangle(pt2dMousePos, pRect->at(2), pRect->at(3), pRect->at(0));

        if (!bIn1 && !bIn2)
        {
            double dMinDistance = DBL_MAX;
            double dDistance = DBL_MAX;


            std::vector<LINE2D> lstLine;

            lstLine.push_back(LINE2D(pRect->at(0), pRect->at(1)));
            lstLine.push_back(LINE2D(pRect->at(1), pRect->at(2)));
            lstLine.push_back(LINE2D(pRect->at(2), pRect->at(3)));
            lstLine.push_back(LINE2D(pRect->at(3), pRect->at(0)));

            for (LINE2D& line: lstLine)
            {

                dDistance = line.Distance(pt2dMousePos);

                if (dDistance < dMinDistance)
                {
                    dMinDistance = dDistance;
                }

                double dSnapSize;
                dSnapSize =  pView->GetSnapRadius();


                if (dMinDistance < dSnapSize)
                {
                    eHit = H_FRAME;
                }
            }
        }
    }
    m_eHit = eHit;
}

//TODO NodeSelectedMouseMoveに
void CDrawingText::SelectedMouseMove(CDrawingView* pView, MOUSE_MOVE_POS posMouse)
{
}


//選択状態でのマウス解放
void CDrawingText::SelectedMouseUp  (CDrawingView* pView, MOUSE_MOVE_POS posMouse)
{
    m_eHit = H_NONE;
}

//選択状態でのマウス押下
void CDrawingText::SelectedMouseDown(CDrawingView* pView, MOUSE_MOVE_POS posMouse)
{
    int a = 10;
}

//!< テキスト入力ボックス表示
bool CDrawingText::OpenText(CDrawingView* pView)
{
    CPartsDef*   pCtrl  = pView->GetPartsDef();
    int             iLayerId  = GetLayer();


    CExtText* pExTxt = GetTextInstance();


    //リッチエディットに設定
    SIZE  sizeDisp;

    //!< 文字表示位置設定
    //pView->SetTextAlign(pExTxt->GetAlign());

    pView->InitRichEdit();
    m_pRich = pView->GetRichEdit();
    pExTxt->SetToRich(m_pRich);

    //１文字目の右上の座標を取得
    TEXT_FRAME* pFrame;
    pFrame = pExTxt->Frame();

    POINT ptTL;
    ptTL.x = 0;
    ptTL.y = 0;

    if (pFrame)
    {
        if(!pFrame->lstDispFrame.empty())
        {
            ptTL = pFrame->lstDispFrame.at(0);
        }
    }

    m_pRich->SetModify(TRUE);
    pView->CalcRichEditSize(&sizeDisp);


    //GetDispSize(&sizeDisp, pView);

    POINT ptSel;
    ptSel = ptTL;


    //端部の処理
    if ((ptSel.x + sizeDisp.cx) > pView->GetWindowWidth())
    {
        ptSel.x = pView->GetWindowWidth() - sizeDisp.cx;
        if (ptSel.x < 0)
        {
            ptSel.x = 0;
        }
    }

    if ((ptSel.y + sizeDisp.cy) > pView->GetWindowHeight())
    {
        ptSel.y = pView->GetWindowHeight() - sizeDisp.cy;
        if (ptSel.y < 0)
        {
            ptSel.y = 0;
        }
    }

    m_pRich->MoveWindow(ptSel.x, ptSel.y, 
        sizeDisp.cx , 
        sizeDisp.cy );
    m_pRich->ShowWindow(SW_SHOW);
    m_pRich->SetFocus();
    m_pViewOpen = pView;


    return true;
}

/**
*  @brief   テキスト入力終了
*  @param   [in]    strVal 入力値
*  @retval  false エラー発生
*  @note    必要であればエラー表示を行う
*  @note    エラー発生で入力コンボボックスは選択状態になる      
*  @note    エラーがない場合はコンボボックスに値が登録される
*/
bool CDrawingText::CloseText()
{
    if (m_pRich == NULL)
    {
        return false;
    }

    if (!m_pViewOpen)
    {
        return false;
    }

    CPartsDef*   pCtrl  = m_pViewOpen->GetPartsDef();
    CLayer*           pLayer  = m_pViewOpen->GetCurrentLayer();
    CUndoAction*       pUndo  = pCtrl->GetUndoAction();


    int iLayerId = m_pViewOpen->GetCurrentLayerId();

    //!< リッチエディット取得
    CString strInput;
    m_pRich->GetWindowText(strInput);

    if (strInput == _T(""))
    {
        m_pRich->ShowWindow(SW_HIDE);
        m_pRich = NULL;
        return false;
    }

    //文字を作成

    {   
        //削除
        pUndo->AddStart(UD_CHG, pCtrl, this);
        DeleteView(m_pViewOpen, NULL, iLayerId);

        m_pRich->ShowWindow(SW_HIDE);
        CExtText* pExtText = GetTextInstance();

        pExtText->SetFromRich(m_pRich);
        m_pRich->SetWindowText(_T(""));


        //再描画(Redrawが良いか？）
        Draw(m_pViewOpen, NULL);
        pUndo->AddEnd(shared_from_this(), true);
    }

    //m_pView->SetComment(GET_SYS_STR(STR_SEL_TEXT_POINT));
    m_pRich = NULL;

    m_pViewOpen->SetFocus();
    m_pViewOpen = NULL;
    return true;
}


//!< マーカ初期化
bool CDrawingText::InitNodeMarker(CNodeMarker* pMarker)
{
    CDrawingView* pView = pMarker->GetView();
    CNodeBound* pBounds = pView->GetNodeBound();

    if(m_eHit == H_TEXT)
    {
        OpenText(pView);
        return true;
    }

    m_eHit = H_NONE;
    m_bDragNode = false;
    pView->ClearDraggingNodes();
    pView->ClearDraggingTmpObjects();

    auto pDef = pView->GetPartsDef();
    pDef->GetTmpGroupObjectList()->clear();


    if (m_psMatTmp)
    {
        m_psMatTmp.reset();
    }
    m_psMatTmp = std::make_unique<MAT2D>();

    //if (m_Text.IsEnableFrame() )
    {
        TEXT_FRAME* pFrame = m_Text.Frame();
        POINT2D ptDraw = m_ptPos;
        double dAngle = m_Text.GetAngle();

        //std::vector<POINT>* pLst; 
        //pLst = m_Text.GetDispFramePoint();

        std::vector<POINT2D> lstRect;

        if(!m_psFrame)
        {
            m_psFrame = std::make_unique<std::vector<POINT2D>>() ;
        }
        *m_psFrame.get() = *m_Text.GetFramePoint();
        lstRect = *m_Text.GetFramePoint();
        _AdjustFrame(&lstRect, pView, m_Text.IsFixSize());

        bool bScale = !pFrame->IsAutoFit() && m_Text.IsEnableFrame();
        pBounds->EnableScale(bScale);
        pBounds->SetDragMode(CNodeBound::DM_ONE);
        pBounds->Init(pView, lstRect, 
            dAngle,
            1.0,
            1.0,
            1.0,
        shared_from_this() );

        return true;
    }
    return false;
}

//!< マーカ選択
void CDrawingText::SelectNodeMarker(CNodeMarker* pMarker, 
    StdString strMarkerId)
{
    CDrawingView* pView = pMarker->GetView();
    CNodeBound* pBounds = pView->GetNodeBound();

    if (pBounds)
    {
        pBounds->Select(pMarker, strMarkerId);
    }

}

void CDrawingText::_AdjustFrame(std::vector<POINT2D>* pList, 
    CDrawingView* pView,
    bool bFixLayerScl) const
{
    CPartsDef* pCtrl = pView->GetPartsDef();
    CLayer* pLayer   = pCtrl->GetLayer(m_iLayerId);


    _AdjustFrame( pList,  pLayer->dScl,  bFixLayerScl);

}


void CDrawingText::_AdjustFrame(std::vector<POINT2D>* pList, 
    double dLayerScl,
    bool bFixLayerScl) const
{
    if(!bFixLayerScl)
    {
        return;
    }

    double dScl =   1.0 / dLayerScl;
    if(pList->size() != 5)
    {
        return;
    }

    //フレームの原点は右上
    POINT2D ptO =  pList->at(0);
    for(auto& pt: *pList)
    {
        pt.Scl(ptO, dScl, dScl);
    }
}

//*********************************************************
// 点 p が有向線分 e(a,b) の左右どちら側にあるか調べる。
// 点 p が有向線分 e の　左側にある場合  1 を、
// 　   　有向線分 e の直線上にある場合  0 を、
// 　   　有向線分 e の　右側にある場合 -1 を返す。
//*********************************************************
int _Side( const POINT2D& pt, const LINE2D &l )
{
    const POINT2D* p[3];

    p[0] = &pt;
    p[1] = &l.GetPt1();
    p[2] = &l.GetPt2();

    // 有向線分 ( の外積の z 成分を求める
    double d  = p[0]->dX * (p[1]->dY - p[2]->dY) +
        p[1]->dX * (p[2]->dY - p[0]->dY) + 
        p[2]->dX * (p[0]->dY - p[1]->dY);
    if (fabs(d) < NEAR_ZERO)   {return 0;}
    else if ( d > 0 )          {return  1;} // 左
    else if ( d < 0 )          {return -1;}// 右
    return 0;
}



//!< マーカ移動
void CDrawingText::MoveNodeMarker(CNodeMarker* pMarker,
    SNAP_DATA* pSnap,
    StdString strMarkerId,
    MOUSE_MOVE_POS posMouse)
{
    //pMarker->SetVisibleOnly(strMarkerId, true);
    m_bDragNode = true;

    CDrawingView* pView = pMarker->GetView();
    CNodeBound* pBounds = pView->GetNodeBound();

    if (!pBounds)
    {
        return;
    }

    if(!m_psFrame)
    {
        return;
    }


    pView->SetDrawingMode(DRAW_SEL);


    pView->GetMousePoint2(pSnap, posMouse, false, true, m_nId);
    pBounds->OnMouseMove(pSnap, posMouse);

    CNodeBound::E_NODE eNode = CNodeBound::GetNode(strMarkerId);

}

void CDrawingText::OffsetMatrix(const MAT2D* pMat)
{
    CDrawingObject::OffsetMatrix(pMat);
}

//!< マーカ開放
bool CDrawingText::ReleaseNodeMarker(CNodeMarker* pMarker,
    StdString strMarkerId,
    MOUSE_MOVE_POS posMouse)
{

    CDrawingView* pView = pMarker->GetView();
    CNodeBound* pBounds = pView->GetNodeBound();

    CPartsDef*   pDef = pView->GetPartsDef();

    if (pMarker->GetMouseButton() == SEL_LBUTTON)
    {

        if(pBounds->OnMouseLButtonUp(posMouse))
        {

            LINE2D l1;
            CNodeBound::E_NODE eNode = CNodeBound::GetNode(strMarkerId);

            if (eNode == CNodeBound::ND_NONE)
            {
                CloseText();
            }
            else
            {
                pBounds->ApplyMatrix(false);
            }
        }
    }
    else if(pMarker->GetMouseButton() == SEL_RBUTTON)
    {
        pBounds->OnMouseRButtonUp(posMouse);
    }

    m_pMatOffset = NULL;

    SetSelect(false);
    if(m_psFrame)
    {
        m_psFrame.reset();
    }

    m_bDragNode = false;
    m_psMatTmp.reset();

    pDef->GetTmpGroupObjectList()->clear();
    pDef->GetConnectingTmpObjectList()->clear();
    pDef->GetTmpGroupObjectList()->clear();

    pDef->Redraw();

    pBounds->Release();

    /*
    m_psNodeHeight[0].release();
    m_psNodeHeight[1].release();
    m_psNodeBottom[0].release();
    m_psNodeBottom[1].release();
    m_psNodeText.release();
    m_psTmpLine.release();
    m_bNodeMarker = false;
    */
    return true;
}

//!< ノード変更
void CDrawingText::ChangeNode(StdString strMarkerId, E_NODE_CHANGE_TYPE eType)
{
    if (strMarkerId == _T("HEIGHT"))
    {
    }
}


bool CDrawingText::Convert(CDrawingView* pView, std::vector<std::shared_ptr<CDrawingCompositionLine>>* pGroup)
{
    std::vector<std::vector<CExtText::LINE_LIST>> lst;
    m_Text.ConvSpline(&lst, pView, m_iLayerId);

#ifdef DBG_CONV_SPLINE
    DB_PRINT(_T("txt-----\n"));
#endif
    for (auto str : lst)
    {
#ifdef DBG_CONV_SPLINE
        DB_PRINT(_T("str-----\n"));
#endif
        auto pCompositionLine = std::make_shared<CDrawingCompositionLine>();
        //1文字
        for (auto loop : str)
        {
#ifdef DBG_CONV_SPLINE
            DB_PRINT(_T("loop-----\n"));
#endif
            auto pCompositionLoop = std::make_shared<CDrawingCompositionLine>();
            //閉曲線
            for (auto segment : loop.lstAny)
            {
                if(segment.anyData.type() == typeid(LINE2D))
                {
                    auto line = CDrawingLine(&segment.ToLINE2D());
                    pCompositionLoop->AddObject(&line);
#ifdef DBG_CONV_SPLINE
DB_PRINT(_T("LINE %s-%s \n"), line.GetPoint1().Str().c_str(),
                              line.GetPoint2().Str().c_str());
#endif
                }
                else if (segment.anyData.type() == typeid(SPLINE2D))
                {
                    auto spline = CDrawingSpline();
                    (*spline.GetSplineInstance()) = segment.ToSPLINE2D();
                    pCompositionLoop->AddObject(&spline);

#ifdef DBG_CONV_SPLINE
                     DB_PRINT(_T("Spline "));
                     
                     int iNum = spline.GetControlPointNum();
                     for(int iCnt = 0; iCnt < iNum; iCnt++)
                     {
                         if (iCnt == iNum-1)
                         {
                             DB_PRINT(_T("%s \n"), spline.GetControlPoint(iCnt).Str().c_str());
                         }
                         else
                         {
                            DB_PRINT(_T("%s-"), spline.GetControlPoint(iCnt).Str().c_str());
                         }
                     }
#endif
                }
            }

            pCompositionLoop->Rotate(POINT2D(0.0, 0.0), GetAngle());
            pCompositionLoop->Move(loop.ptOffset + m_ptPos);
            pCompositionLine->AddLoop(pCompositionLoop.get(), E_DIRECT);
        
        }
        pGroup->push_back(pCompositionLine);
    }

#ifdef DBG_CONV_SPLINE
    DB_PRINT(_T("txt end-----\n"));
#endif

    return true;
}



//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void TEST_CDrawingText()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}
#endif //_DEBUG