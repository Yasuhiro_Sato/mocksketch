/**
 * @brief			CDrawingTextヘッダーファイル
 * @file			CDrawingText.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#if !defined(__DRAWING_TEXT_)
#define __DRAWING_TEXT_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DrawingObject/CDrawingObject.h"
#include "DrawingObject/Primitive/MAT2D.h"
#include "Utility/ExtText/CExtText.H"

class CNodeMarker;

/**
 * @class   CDrawingText
 * @brief   テキスト                     
   
 */
class CDrawingText :public CDrawingObject//:public CRTTIClass<CDrawingLine, CDrawingObject, DT_LINE>
{
    enum E_HIT
    {
        H_NONE,
        H_TEXT,
        H_FRAME,
        H_DATUM
    };

    /*
    struct DRAW_FRAME
    {
        //!< 線幅
        int         iWidth;

        //!< 線種
        int         iLineType;

        COLORREF    crLine;

        double      dRadius;

        bool        bUseBackColor;

        COLORREF    crBack;

        double      dWidth;

        double      dHeight;

        CExtText::POS_TYPE eBaseType;
    };
    */

public:
	//!< コンストラクタ
	CDrawingText();

  	//!< コンストラクタ
    CDrawingText(int nID);

  	//!< コピーコンストラクタ
    CDrawingText(const CDrawingText& Obj);

	//!< デストラクタ
	virtual ~CDrawingText();

    //!< 絶対移動
    //virtual void AbsMove(const POINT2D& pt2D);

    //!< オフセット
    virtual void OffsetMatrix(const MAT2D* pMat) override;

	//!< 相対移動
	virtual void Move(const POINT2D& pt2D) override;

    //!< 回転
	virtual void Rotate(const POINT2D& pt2D, double dAngle) override;

    //!< 鏡像
	virtual void Mirror(const LINE2D& line) override;

    //!< 倍率
	virtual void Scl(const POINT2D& pt2D, double dXScl, double dYScl) override;

    //!< 行列適用
    virtual void Matrix(const MAT2D& mat2D) override;

    //!< 範囲
    virtual bool IsInner( const RECT2D& rcArea, bool bPart) const override;

     //!< 描画
    virtual void Draw(CDrawingView* pView, 
                      const std::shared_ptr<CDrawingObject> pParentObject,
                      E_MOUSE_OVER_TYPE eMouseOver = E_MO_NO,
                      int iLayerId = -1) override;

    void RecalcFramePos();

    //!< 描画削除
    virtual void DeleteView(CDrawingView* pView, 
                            const std::shared_ptr<CDrawingObject> pParentObject,
                            int iLayerId = -1) override;

    //!< 領域取得
    virtual RECT2D GetBounds() const;

    //!< 領域取得
    void GetDispSize(SIZE* pSize, const CDrawingView* pView) const;

    //!< 距離
    virtual double Distance(const POINT2D& pt) const;

    //-------------------------
    // リフレクション設定
    //-------------------------

    //!< 位置設定(AS)
    void  SetPoint(POINT2D ptPos)        {m_ptPos = ptPos;} 

    //!< 位置設定(AS)
    void  SetPointDbl(double dX, double dY)   {m_ptPos.dX = dX; m_ptPos.dY = dY;} 

    //!< 位置取得
    POINT2D  GetPoint() const            {return m_ptPos;}

    //!< 角度取得
    double  GetAngle() const;

    //!< 角度設定
    void  SetAngle(double dAngle);

    //!< 色設定
    virtual void  SetColor(COLORREF color);

    //!< テキスト取得
    CExtText*  GetTextInstance()                  {return &m_Text;}

    //!< 描画用（画面座標変換用）マトリクス取得
    const MAT2D* GetDrawingMatrix()const override;

    //-----------------------------------------------------
    //-------------------------
    // テキスト設定
    //-------------------------
    //!< テキスト取得(AS)
    StdString GetText()const{return m_Text.GetText();}

    //!< テキスト設定(AS)
    void SetText(StdString& strText);
    void SetTextS(std::string& strTxt);

    //!< 文字アライメント設定(AS)
    void SetAlign(ALIGNMENT_TYPE ePos){m_Text.SetAlign(ePos);}

    //!< 文字アライメント取得(AS)
    ALIGNMENT_TYPE GetAlign() const{return m_Text.GetAlign();}
    //-----------------------------------------------------


     virtual void SetSelect(bool bSel, const RECT2D* pRect = NULL) override;

    //!< スナップ点取得
    virtual  bool GetSnapList(std::list<SNAP_DATA>* pLstSnap)const;

    //--------------
    //    接続
    //--------------
    virtual bool CreateNodeData(int iIndex) override;
    virtual CNodeData* GetNodeData(int iIndex) override;
    virtual CNodeData* GetNodeDataConst(int iIndex) const override;
    virtual int GetCopySrcObjectId() const override{return m_iTmpConnectionObjId;}
    virtual void ResetCopySrcObjectId() override{m_iTmpConnectionObjId = -1;}
    virtual void SetCopyNodeDataMode(bool bSet) override{m_bCopyNodeDataMode = bSet;}
    virtual bool GetCopyNodeDataMode() const override{return m_bCopyNodeDataMode;}
    virtual int GetNodeDataNum() const override;
    //virtual int GetConnectionId(int iNo) const override;
    virtual bool GetNodeDataPos(POINT2D* pt, int iIndex) const override;
    virtual RECT2D::E_EDGE InquireConnectDirection(std::map<RECT2D::E_EDGE, LINE2D>* pDir, RECT2D* pRc, int iIndex )const;
    //--------------

    //!< 選択状態でのマウス
    virtual void PreSelectedMouseMove(CDrawingView* pView, MOUSE_MOVE_POS posMouse);
    virtual void SelectedMouseMove(CDrawingView* pView, MOUSE_MOVE_POS posMouse);
    virtual void SelectedMouseUp  (CDrawingView* pView, MOUSE_MOVE_POS posMouse);
    virtual void SelectedMouseDown(CDrawingView* pView, MOUSE_MOVE_POS posMouse);
    
    //-------------------
    // ドラッグ用マーカ
    //-------------------
    //!< マーカ初期化 true:マーカあり
    virtual bool InitNodeMarker(CNodeMarker* pMarker) override;

    //!< マーカ選択
    virtual void SelectNodeMarker(CNodeMarker* pMarker, 
                                  StdString strMarkerId) override;

    //!< マーカ移動
    virtual void MoveNodeMarker(CNodeMarker* pMarker,
                                    SNAP_DATA* pSnap,
                                    StdString strMarkerId,
                                    MOUSE_MOVE_POS posMouse) override;

    //!< マーカ開放
    virtual bool ReleaseNodeMarker(CNodeMarker* pMarker, StdString strMarkerId, MOUSE_MOVE_POS posMouse) override;

    //!< ノード変更
    virtual void ChangeNode(StdString strMarkerId, E_NODE_CHANGE_TYPE eType) override;



    //-------------------------
    // 参照
    //-------------------------
    //プロパティ初期化
    virtual TREE_GRID_ITEM*  _CreateStdPropertyTree();

    //!< 参照元の取得
    virtual CDrawingObject* UpdateRef();

    //--------------------
    // プロパティ変更処理
    //--------------------
friend CDrawingText;
    static bool PropPos (CStdPropertyItem* pData, void* pObj);
 
    bool OpenText(CDrawingView* pView);
    bool CloseText();

    bool Convert(CDrawingView* pView, std::vector<std::shared_ptr<CDrawingCompositionLine>>* pGroup);

protected:

    void _Draw(CDrawingView* pView, 
        const CDrawingObject* pParentObject,
        bool bColor, 
        COLORREF crObj, 
        int iId,
        E_MOUSE_OVER_TYPE eMouseOver,
        int iLineId);

    virtual void DrawOver(const CDrawingView* pView, bool bOver);
    POINT2D _GetDirection(CDrawingObject* pObj, POINT2D pt);

    void _AdjustFrame(std::vector<POINT2D>* pList, 
            CDrawingView* pView, 
             bool bFixLayerScl) const;

    void _AdjustFrame(std::vector<POINT2D>* pList, 
        double  dLayerScl, 
        bool bFixLayerScl) const;

protected:
    //!< 位置
    CExtText      m_Text;

    //!< 位置
    POINT2D       m_ptPos;

   // std::unique_ptr<DRAW_FRAME> m_psFrame;

    //------------------
    // プロパティ用
    //------------------

    //!< 角度
    double       m_dTmpAngle;

    //------------------
    // 一時使用
    //------------------
    //Todo:余計なメモリーを取るため、あとで構造体に纏める
    E_HIT m_eHit;
    POINT2D       m_ptTmp;
	std::unique_ptr<MAT2D>   m_psMatTmp;
    bool m_bDragNode;
    std::unique_ptr<std::vector<POINT2D> > m_psFrame;
    CRichEditCtrl*      m_pRich;
    CDrawingView*       m_pViewOpen;
    mutable MAT2D m_mat2D;
    double m_diFirstTextLineHeight;
    double m_dLastTextLineHeight;
    double m_dWidthBase;
    mutable std::vector<std::unique_ptr<CNodeData>> m_lstConnection;
    bool  m_bCopyNodeDataMode;
    int   m_iTmpConnectionObjId;
    double m_dLayerScl;                       //IsInner でpView を使用できないため

private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
            ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(CDrawingObject);
            SERIALIZATION_BOTH("Pos"      , m_ptPos);
            SERIALIZATION_BOTH("Text"     , m_Text);
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }


};
#endif // !defined(__DRAWING_TEXT_)
