﻿/**
 * @brief			CFillヘッダーファイル
 * @file			CFill.h
 * @author			Yasuhiro Sato
 * @date			12-16-2016 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

#ifndef __CFILL_H_
#define __CFILL_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "Utility/CStdPropertyTree.h"             
#include "System/CSystem.h"
#include "resource.h"

class CStdPropertyTree;
enum E_FIILL_TYPE
{
    FIT_NONE,
    FIT_SOLID,
    FIT_HATCHING,
    FIT_GRADATION,
};

enum E_GRADATION_TYPE
{
    GT_LR,
    GT_RL,
    GT_TB,
    GT_BT,

    GT_TLBR,
    GT_TRBL,

    GT_BRTL,
    GT_BLTR,

    GT_PATH,
};



/**
 * @class   CFillt
 * @brief   塗りつぶし                     
 */
/*********************************************
 これを使用するクラスは
 CFill* GetFillInstance()
 を実装すること
**********************************************/
template<class D>
class CFill
{
public:
    E_FIILL_TYPE eFillType = FIT_NONE;
    COLORREF cr1 = DRAW_CONFIG->crBack;
    COLORREF cr2 = DRAW_CONFIG->crFront;
    E_GRADATION_TYPE  eGradation = GT_LR;

public:
    CFill();
    virtual ~CFill();


    //!< 代入演算子 
    CFill<D>& operator = (const CFill<D> & m);

    //!< 等価演算子
    bool operator == (const CFill<D> & m) const;

    //!< 不等価演算子
    bool operator != (const CFill<D> & m) const;


    TREE_GRID_ITEM*  CreateStdPropertyTree( CStdPropertyTree* pProperty,
                                            NAME_TREE<CStdPropertyItem>* pTree,
                                            NAME_TREE_ITEM<CStdPropertyItem>* pNextItem);



    //-------------------
    //!< プロパティ変更
    //-------------------
    static bool PropFillType  (CStdPropertyItem* pData, void* pObj);
    static bool PropFillColor1(CStdPropertyItem* pData, void* pObj);
    static bool PropHatching  (CStdPropertyItem* pData, void* pObj);
    


private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
        SERIALIZATION_BOTH("FillType" , eFillType);

        if (eFillType == FIT_HATCHING)
        {
            SERIALIZATION_BOTH("HATCH"    , cr1);
        }
        else if ((eFillType == FIT_SOLID)||
                 (eFillType == FIT_GRADATION))
        {
            SERIALIZATION_BOTH("COLOR1"    , cr1);

            if (eFillType == FIT_GRADATION)
            {
                SERIALIZATION_BOTH("COLOR2"         , cr2);
                SERIALIZATION_BOTH("GradationType"  , eGradation);
            }
        }
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }
};




/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
template<class D>
CFill<D>::CFill()
{

}

template<class D>
CFill<D>::~CFill()
{
}


//!< 代入演算子 
template<class D>
CFill<D>& CFill<D>::operator = (const CFill<D> & m)
{
    eFillType = m.eFillType;
    cr1 = m.cr1;
    cr2 = m.cr2;
    eGradation = m.eGradation;
    return *this;
}

//!< 等価演算子
template<class D>
bool CFill<D>::operator == (const CFill<D> & m) const
{
    if(eFillType != m.eFillType) {return false;}
    if(cr1 != m.cr1) {return false;}
    if(cr2 != m.cr2) {return false;}
    if(eGradation != m.eGradation) {return false;}

    return true;
}

//!< 不等価演算子
template<class D>
bool CFill<D>::operator != (const CFill<D> & m) const
{
    return !(*this == m);
}


template<class D>
TREE_GRID_ITEM*  CFill<D>::CreateStdPropertyTree( CStdPropertyTree* pProperty,
                                        NAME_TREE<CStdPropertyItem>* pTree,
                                        NAME_TREE_ITEM<CStdPropertyItem>* pNextItem)
{
   using namespace VIEW_COMMON;

    CStdPropertyItem* pItem;
    //----------------
    // 塗りつぶし方式
    //----------------
    StdString strFillType;

    strFillType = GET_STR(STR_FILL_NONE);
    strFillType += _T(":");  
    strFillType += boost::lexical_cast<StdString>(FIT_NONE);
    strFillType += _T(",");  

    strFillType += GET_STR(STR_FILL_SOLID);
    strFillType += _T(":");  
    strFillType += boost::lexical_cast<StdString>(FIT_SOLID);
    strFillType += _T(",");  

    strFillType += GET_STR(STR_FILL_HATCHING);
    strFillType += _T(":");  
    strFillType += boost::lexical_cast<StdString>(FIT_HATCHING);

#ifdef USE_GRADATION
    strFillType += _T(",");  

    strFillType   = GET_STR(STR_FILL_GRADATION);
    strFillType += _T(":");  
    strFillType += boost::lexical_cast<StdString>(FT_GRADATION);
#endif


    CStdPropertyItemDef DefFillType(PROP_DROP_DOWN_ID,       //表示タイプ
        GET_STR(STR_PRO_FILL_TYPE)   ,                   //表示名
        _T("FillType"),                                  //変数名
        GET_STR(STR_PRO_INFO_FILL_TYPE),                 //表示説明
        true,                                            //編集可不可
        DISP_NONE,                                       //表示単位
        static_cast<int>(eFillType),              
        strFillType,
        0);

    pItem = new CStdPropertyItem(
        DefFillType, 
        PropFillType, 
        pProperty, 
        NULL,
        (void*)&eFillType);
    
    pNextItem = pTree->AddNext(pNextItem, 
        pItem, 
        DefFillType.strDspName, 
        pItem->GetName());

    if(eFillType == FIT_SOLID)
    {
        //------------
        //塗りつぶし色
        //------------
        CStdPropertyItemDef DefFillColor(PROP_COLOR, 
            GET_STR(STR_PRO_FILL_COLOR1)   , 
            _T("FillColor"),
            GET_STR(STR_PRO_INFO_FILL_COLOR1), 
            false,
            DISP_NONE,
            cr1);

        pItem = new CStdPropertyItem(
                                     DefFillColor,
                                     PropFillColor1,
                                     pProperty,
                                     NULL,
                                     &(cr1));

        pNextItem = pTree->AddNext(pNextItem, pItem, 
                                       DefFillColor.strDspName, 
                                       pItem->GetName());

    }
    else if (eFillType == FIT_HATCHING)
    {

    StdString strTypeList;
    //IDB_HATCHING_TYPEの図形の順番に合わせる
        strTypeList += boost::lexical_cast<StdString>(IDB_HATCHING_TYPE);  
        strTypeList += _T(",");

        strTypeList += GET_STR(STR_HATCH_HORIZONTAL);
        strTypeList += _T(":");  
        strTypeList += boost::lexical_cast<StdString>(HS_HORIZONTAL);  
        strTypeList += _T(",");

        strTypeList += GET_STR(STR_HATCH_VERTICAL);
        strTypeList += _T(":");  
        strTypeList += boost::lexical_cast<StdString>(HS_VERTICAL);  
        strTypeList += _T(",");

        strTypeList += GET_STR(STR_HATCH_FDIAGONAL);
        strTypeList += _T(":");  
        strTypeList += boost::lexical_cast<StdString>(HS_FDIAGONAL);  
        strTypeList += _T(",");

        strTypeList += GET_STR(STR_HATCH_BDIAGONAL);
        strTypeList += _T(":");  
        strTypeList += boost::lexical_cast<StdString>(HS_BDIAGONAL);  
        strTypeList += _T(",");

        strTypeList += GET_STR(STR_HATCH_CROSS);
        strTypeList += _T(":");  
        strTypeList += boost::lexical_cast<StdString>(HS_CROSS);  
        strTypeList += _T(",");

        strTypeList += GET_STR(STR_HATCH_DIAGCROSS);
        strTypeList += _T(":");  
        strTypeList += boost::lexical_cast<StdString>(HS_DIAGCROSS);  


    CStdPropertyItemDef DefHatchingType(PROP_DROP_DOWN_BMP, 
        GET_STR(STR_PRO_HATCHING_TYPE)   , 
        _T("HatchingType"), 
        GET_STR(STR_PRO_INFO_HATCHING_TYPE), 
        false,                                       
        DISP_NONE,
        static_cast<int>(cr1),
        strTypeList,
        0);

    pItem = new CStdPropertyItem(
        DefHatchingType, 
        PropHatching, 
        pProperty, 
        NULL,
        (void*)&(cr1));
    
    pNextItem = pTree->AddNext(pNextItem, pItem, DefHatchingType.strDspName, pItem->GetName());

    }

    return pNextItem;
}

template<class D>
bool CFill<D>::PropFillType  (CStdPropertyItem* pData, void* pObj)
{
   using namespace VIEW_COMMON;
   D* pDrawing = reinterpret_cast<D*>(pObj);
   CFill* pFill = pDrawing->GetFillInstance();
    try
    {
        int iType;
        iType = pData->anyData.GetInt();

        E_FIILL_TYPE eOldFill = pFill->eFillType;

        pFill->eFillType = static_cast<E_FIILL_TYPE>(iType);
        pDrawing->GetPartsDef()->Redraw();

        if((pFill->eFillType != eOldFill) &&
            pFill->eFillType == FIT_HATCHING)
        {
            pFill->cr1 = 0;
        }

        //リスト再描画
        pDrawing->ResetProperty();
        ::PostMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_UPDATE_PROPERTY, 
                    (WPARAM)0, (LPARAM)0);

    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

template<class D>
bool CFill<D>::PropFillColor1(CStdPropertyItem* pData, void* pObj)
{
   using namespace VIEW_COMMON;
   D* pDrawing = reinterpret_cast<D*>(pObj);
   CFill* pFill = pDrawing->GetFillInstance();
    try
    {
        COLORREF cr1;
        cr1 = pData->anyData.GetColor();
        pFill->cr1 = cr1;
        pDrawing->GetPartsDef()->Redraw();
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}


template<class D>
bool CFill<D>::PropHatching(CStdPropertyItem* pData, void* pObj)
{
   using namespace VIEW_COMMON;
   D* pDrawing = reinterpret_cast<D*>(pObj);
   CFill* pFill = pDrawing->GetFillInstance();
    try
    {
        int cr1;
        cr1 = pData->anyData.GetInt();
        pFill->cr1 = static_cast<COLORREF>(cr1);
        pDrawing->GetPartsDef()->Redraw();
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

#endif