/**
 * @brief			CNodeMarker実装ファイル
 * @file			CNodeMarker.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "resource.h"
#include "MockSketch.h"
#include "./CNodeBound.h"
#include "./CDrawingObject.h"

#include "DefinitionObject/View/CDrawingView.h"
#include "DrawingObject/Primitive/MAT2D.h"
#include "DrawingObject/CDrawingNode.h"
#include "DrawingObject/CDrawingParts.h"
#include "DrawingObject/CDrawingPoint.h"
#include "DrawingObject/CDrawingLine.h"
#include "DrawingObject/CDrawingCircle.h"
#include "DrawingObject/CDrawingEllipse.h"
#include "DrawingObject/CDrawingGroup.h"
#include "DrawingObject/CNodeMarker.h"
#include "DrawingObject/Action/CActionCommand.h"

#include "View/CDrawMarker.h"
#include "View/CUndoAction.h"
    

#include "System/CSystem.h"
#include "DefinitionObject/CPartsDef.h"

#define _DEBUG_SCL  0


CNodeBound::CNodeBound(bool bRotate, bool bMove, bool bScale):
m_dMinScl (1e-03),
m_pView   (NULL),
m_bShowRect (false),
m_eDragMode(DM_BOTH),
m_eSnapType(ST_DIM)
{
    m_bRotate = bRotate;
    m_bMove = bMove;
	m_bScale = bScale;

	//最小スナップ長(mm)
    double dSnapLen = DRAW_CONFIG->pDimUnit->ToBase(DRAW_CONFIG->dMinSnapLength) * 1000.0;

	//画面上の最小スナップ長に対するDot数
	m_dMinResDot = ceil((DRAW_CONFIG->dDpi / 25.4) * dSnapLen);
}

CNodeBound::CNodeBound():
CNodeBound(true, true, true)
{
}

void CNodeBound::EnableRotate(bool bRotate)
{
    m_bRotate = bRotate;
}

void CNodeBound::EnableMove(bool bMove)
{
    m_bMove = bMove;

}

void CNodeBound::EnableScale(bool bScale)
{
	m_bScale = bScale;
}

void CNodeBound::EnableMinus(bool bMinus)
{
    m_bEnableMinus = bMinus;
}

void CNodeBound::SetMinSize(double dWidth, double dHeight)
{
    m_dMinWidth = dWidth;
    m_dMinHeight = dHeight;
}

void CNodeBound::SetSnapType(CNodeBound::E_SNAP_TYPE eSnapType)
{
    m_eSnapType = eSnapType;
}


void CNodeBound::ShowRect(bool bShow)
{
	m_bShowRect = bShow;
	if (!m_bShowRect)
	{
		return;
	}

	for(std::unique_ptr<CDrawingLine>& pLine: m_psLectLine)
	{
		if(pLine)
		{
			continue;
		}
		pLine = std::make_unique<CDrawingLine>();
	}
}

CNodeBound::~CNodeBound()
{

}

double CNodeBound::GetAngle() const
{
    return m_dAngle;
}

double CNodeBound::GetWidth() const
{
    return m_dWidth;
}

double CNodeBound::GetHeight() const
{
    return m_dHeight;
}
POINT2D CNodeBound::GetCenter() const
{
    return m_ptCenter;
}

POINT2D CNodeBound::GetMove() const
{
    return m_ptMove;
}

double CNodeBound::GetXScl() const
{

    return (m_dWidth / m_dOrgWidth);
}

double CNodeBound::GetYScl() const
{
    return (m_dHeight / m_dOrgHeight);
}

const MAT2D* CNodeBound::GetMatrix() const
{
    return &m_mat2D;
}

/**
 * @brief   位置設定
 * @param   [in] ptAbs 表示位置
 * @retval  なし
 * @note    
 */
void  CNodeBound::SetCenter(POINT2D ptAbs)
{
    m_ptCenter = ptAbs;
    //m_mat2D.SetAffinePos(m_ptCenter + m_ptOffset);
}

void  CNodeBound::SetMove(POINT2D ptAbs)
{
    m_ptMove = ptAbs;
    m_mat2D.SetAffinePos(m_ptMove);
}


void CNodeBound::SetAngle(double dAngle)
{
    m_mat2D.SetAffineAngle(dAngle);
    m_dAngle = dAngle;
}

void CNodeBound::SetDragMode(CNodeBound::E_DRAG_MODE eMode)
{
    m_eDragMode = eMode;
}

CNodeBound::E_DRAG_MODE CNodeBound::GetDragMode() const
{

    return m_eDragMode;
}


//!< マーカ初期化 true:マーカあり
bool CNodeBound::Init(CDrawingView* pView,
    const RECT2D& rc,
    std::shared_ptr<CDrawingObject> pParent)
{
    std::vector<POINT2D> lstRect;
    lstRect.resize(4);
    lstRect[0].dX = rc.dLeft;
    lstRect[0].dY = rc.dTop;
    lstRect[1].dX = rc.dRight;
    lstRect[1].dY = rc.dTop;
    lstRect[2].dX = rc.dRight;
    lstRect[2].dY = rc.dBottom;
    lstRect[3].dX = rc.dLeft;
    lstRect[3].dY = rc.dBottom;

    return  Init(pView,  lstRect, 0.0, 1.0, 1.0, 1.0, pParent);
}



//!< マーカ初期化 true:マーカあり
bool CNodeBound::Init(CDrawingView* pView,
    const std::vector<POINT2D>& lstRect,
    double dAngle,
    double dXScl,
    double dYScl,
    double dTDummy,
    std::shared_ptr<CDrawingObject> pParent)
{
    for(std::unique_ptr<CDrawingNode>& pNode: m_psNode)
    {
        pNode = std::make_unique<CDrawingNode>();
        pNode->SetVisible(true);
        pNode->SetMarkerShape(DMS_CIRCLE_FILL);
    }

    m_psNode[ND_TL]->SetName(_T("TL"));
    m_psNode[ND_TM]->SetName(_T("TM"));
    m_psNode[ND_TR]->SetName(_T("TR"));
    m_psNode[ND_MR]->SetName(_T("MR"));
    m_psNode[ND_BR]->SetName(_T("BR"));
    m_psNode[ND_BM]->SetName(_T("BM"));
    m_psNode[ND_BL]->SetName(_T("BL"));
    m_psNode[ND_ML]->SetName(_T("ML"));
    m_psNode[ND_C]->SetName(_T("C"));
    m_psNode[ND_R]->SetName(_T("R"));
    m_psNode[ND_R]->SetMarkerShape(DMS_CIRCLE_FILL_OVER);

    m_pView = pView;
    m_ptMove.Set(0.0,0.0);

    int iR = CUtil::Round( DRAW_CONFIG->GetMarkerSize() * pView->GetDpu());
    CNodeMarker* pNodeMarker = m_pView->GetNodeMarker();
    STD_ASSERT(pNodeMarker);

    pNodeMarker->Clear();

    pNodeMarker->SetRadius(iR);
    pNodeMarker->SetObject(pParent);

	//マウスカーソル設定コールバック
    namespace  PLC = std::placeholders;
	pNodeMarker->SetOnSetcursorCallback(std::bind(&CNodeBound::OnSetCursor, this, PLC::_1, PLC::_2));

    STD_ASSERT(lstRect.size() >= 4);
    m_lstBoundOrg = lstRect;

    m_dOrgWidth  = m_lstBoundOrg[0].Distance(m_lstBoundOrg[1]) / dXScl;
    m_dOrgHeight = m_lstBoundOrg[1].Distance(m_lstBoundOrg[2]) / dYScl;
    m_dWidth  =  m_dOrgWidth;
    m_dHeight =  m_dOrgHeight;

    POINT2D vec = m_lstBoundOrg[1] - m_lstBoundOrg[0];
    //double dAngle = CUtil::NormAngle(atan2(vec.dY, vec.dX)* RAD2DEG);
    m_dOrgAngle  = dAngle;
    m_dAngle = 0.0;
    m_ptOrgCenter  = (m_lstBoundOrg[0] + m_lstBoundOrg[2]) / 2.0;
    m_ptCenter  = m_ptOrgCenter;

    m_ptInit.resize(ND_MAX);

    m_ptInit[ND_TL] = m_lstBoundOrg[0];
    m_ptInit[ND_TR] = m_lstBoundOrg[1];
    m_ptInit[ND_BR] = m_lstBoundOrg[2];
    m_ptInit[ND_BL] = m_lstBoundOrg[3];

    m_ptInit[ND_C]  = m_ptOrgCenter;

    m_ptInit[ND_TM] = (m_ptInit[ND_TR] + m_ptInit[ND_TL]) / 2.0;
    m_ptInit[ND_MR] = (m_ptInit[ND_TR] + m_ptInit[ND_BR]) / 2.0;
    m_ptInit[ND_BM] = (m_ptInit[ND_BR] + m_ptInit[ND_BL]) / 2.0;
    m_ptInit[ND_ML] = (m_ptInit[ND_BL] + m_ptInit[ND_TL]) / 2.0;

    //CDrawingView* pView = pMarker->GetView();

    double dRotOffset = DRAW_CONFIG->dRotateNodeOffset;
    if(pView)
    {
        double dDspScl;
        dDspScl = m_pView->GetViewScale();
        dRotOffset = dRotOffset / dDspScl;
    }
    POINT2D vecR = m_ptInit[ND_TR] -  m_ptInit[ND_BR];
    vecR.Normalize();
    m_ptInit[ND_R]  = m_ptInit[ND_TM] + (dRotOffset * vecR);

    double dSclX = dXScl;
    double dSclY = dYScl;
    double dT = 0.0;

    m_mat2D.SetAffineParam( m_ptCenter, m_dAngle, dSclX, dSclY, dT);


    for (int iNo = ND_TL; iNo < ND_MAX; iNo++)
    {
        bool bVisible = true;
        if (!m_bRotate && (iNo == ND_R))
        {
            bVisible = false;
            //continue;
        }

        if (!m_bMove && (iNo == ND_C))
        {
            bVisible = false;
            //continue;
        }

        if (!m_bScale && 
			((iNo != ND_R) &&
			 (iNo != ND_C)))
        {
            bVisible = false;
            //continue;
		}
			
		m_psNode[iNo]->SetPoint(m_ptInit[iNo]);
        pNodeMarker->Create(m_psNode[iNo].get(), m_ptInit[iNo]);
        pNodeMarker->SetVisible(m_psNode[iNo]->GetName(), bVisible);
    }

    m_psAdditionalLine1 = std::make_shared<CDrawingLine>();
    m_psAdditionalLine2 = std::make_shared<CDrawingLine>();

    m_psAdditionalLine1->SetColor(DRAW_CONFIG->crAdditional);
    m_psAdditionalLine1->SetLineType(PS_DOT);
    m_psAdditionalLine1->SetLayer(m_pView->GetCurrentLayerId());


    m_psAdditionalLine2->SetColor(DRAW_CONFIG->crAdditional);
    m_psAdditionalLine2->SetLineType(PS_DOT);
    m_psAdditionalLine2->SetLayer(m_pView->GetCurrentLayerId());

    //コピーを作成する
    m_pTmpMovingObject = CDrawingObject::CloneShared(pParent.get());

    //CPartsDef::GetDraggingObjectで見つけられるように m_lstTmpGroupに登録する
    auto pDef = m_pView->GetPartsDef();
    pDef->GetTmpGroupObjectList()->push_back(m_pTmpMovingObject);

    //m_pTmpMovingObject->SetColor(DRAW_CONFIG->crImaginary);
	m_pTmpMovingObject->SetImaginary(true);
    m_pTmpMovingObject->SetLayer(m_pView->GetCurrentLayerId());

    CDrawMarker* pMarker= pView->GetMarker();
    //pMarker->AddObject(m_pTmpMovingObject);
#ifdef _DEBUG
THIS_APP->SetDebugCount(0);
#endif
    return true;
}

bool CNodeBound::IsInit() const
{
    if (m_pView)
    {
        return true;
    }
    return false;
}

bool CNodeBound::OnSetCursor(CNodeMarker* pNodeMarker, CDrawingView* pView)
{

    StdString strId = pNodeMarker->GetMouseOver();
    E_NODE eNode = GetNode(strId);

    eNode = GetNode(strId);


    //右ボタンドラッグ(移動・コピー選択)できるのはセンターのみ
    /*
    if (pNodeMarker->GetMouseButton() == SEL_RBUTTON)
    {
        if( eNode != ND_C)
        {
            return false;
        }
    }
    */


	HCURSOR hCursor;
	switch(eNode)
	{
	case ND_TM:
	case ND_BM:
		hCursor =(HCURSOR)LoadImage(NULL, IDC_SIZENS,IMAGE_CURSOR,
                                    NULL,NULL,LR_DEFAULTCOLOR | LR_SHARED);
		::SetCursor(hCursor);
		break;

	case ND_MR:
	case ND_ML:
		hCursor =(HCURSOR)LoadImage(NULL, IDC_SIZEWE,IMAGE_CURSOR,
                                    NULL,NULL,LR_DEFAULTCOLOR | LR_SHARED);
		::SetCursor(hCursor);
		break;

	case ND_TL:
	case ND_BR:
		hCursor =(HCURSOR)LoadImage(NULL, IDC_SIZENWSE,IMAGE_CURSOR,
                                    NULL,NULL,LR_DEFAULTCOLOR | LR_SHARED);
		::SetCursor(hCursor);
		break;

	case ND_TR:
	case ND_BL:
		hCursor =(HCURSOR)LoadImage(NULL, IDC_SIZENESW,IMAGE_CURSOR,
                                    NULL,NULL,LR_DEFAULTCOLOR | LR_SHARED);
		::SetCursor(hCursor);
		break;

	case ND_C:
		hCursor =(HCURSOR)LoadImage(NULL, IDC_SIZEALL,IMAGE_CURSOR,
                                    NULL,NULL,LR_DEFAULTCOLOR | LR_SHARED);
		::SetCursor(hCursor);
		break;

	case ND_R:
		::SetCursor(AfxGetApp()->LoadCursor(IDC_CURSOR_ROTATE));
		break;

	default:
		//hCursor =(HCURSOR)LoadImage(NULL, IDC_ARROW,IMAGE_CURSOR,
        //                            NULL,NULL,LR_DEFAULTCOLOR | LR_SHARED);
		//::SetCursor(hCursor);
		return false;
	}
	
	return true;
}
bool CNodeBound::_OnMouseButtonDown(MOUSE_MOVE_POS posMouse, MOUSE_BUTTON eButton)
{
    if(!m_pView)
    {
        return false;
    }

    CNodeMarker* pNodeMarker;
    pNodeMarker = m_pView->GetNodeMarker();



    if(eButton == SEL_RBUTTON)
    {
        //コピーを作成する
        auto lstTmpObject = m_pView->GetDraggingTmpObjects();
        auto tmpClone = CDrawingObject::CloneShared(pNodeMarker->GetObject().get());
        tmpClone->SetPartsDef(NULL);   //名前とIDをコピーしている場合名前を変更した場合
                            //元のデータを変更することになる
        tmpClone->SetName(_T("Test"));     
        //tmpClone->SetColor(DRAW_CONFIG->crImaginary);
		tmpClone->SetImaginary(true);
		lstTmpObject->clear();
        pNodeMarker->SetObject(tmpClone);
        lstTmpObject->push_back(tmpClone);
//ドラッグ用のオブジェクトを生成
    }

    bool bMark = false;
    StdString strNodeMarkerId = pNodeMarker->GetMarkerId(posMouse.ptSel);
    if (strNodeMarkerId != _T(""))
    {
        bMark = true;
    }

    if (bMark)
    {
        m_strSelectNodeMarkerId = strNodeMarkerId;
        pNodeMarker->Select(strNodeMarkerId, eButton);
        Select(pNodeMarker, strNodeMarkerId);

        return true;
    }
    return false;
}


bool CNodeBound::OnMouseLButtonDown(MOUSE_MOVE_POS posMouse)
{
    return _OnMouseButtonDown(posMouse, SEL_LBUTTON);
}

bool CNodeBound::OnMouseLButtonUp(MOUSE_MOVE_POS posMouse)
{
    if(!m_pView)
    {
        return false;
    }
    return true;
}

bool CNodeBound::OnMouseRButtonDown(MOUSE_MOVE_POS posMouse)
{
    return _OnMouseButtonDown(posMouse, SEL_RBUTTON);
}

bool CNodeBound::OnMouseRButtonUp(MOUSE_MOVE_POS posMouse)
{
    ShowMenu(posMouse.ptSel);
    return true;
}

void CNodeBound::Select(CNodeMarker* pNodeMarker, 
                                    StdString strMarkerId)
{
    E_NODE eNode = GetNode(strMarkerId);

    CDrawingView* pView = pNodeMarker->GetView();

    if(pNodeMarker->GetMouseButton() == SEL_RBUTTON)
    {

    }


    CPartsDef*pDef = pView->GetPartsDef();

    m_strSelectNodeMarkerId = strMarkerId;

    switch(eNode)
    {
    case ND_TL:
    case ND_TR:
    case ND_BL:
    case ND_BR:
        break;

    case ND_MR:
    case ND_ML:
    case ND_BM:
    case ND_TM:
    case ND_R:
         break;

    case ND_C:
        pDef->Redraw();
        break;
    }
}

/**
 *  @brief  行列.
 *  @param  [in]    mat2D   適用する行列
 *  @retval         なし
 *  @note
 */
void CNodeBound::Matrix(const MAT2D& mat2D)
{
    m_mat2D = mat2D;
}

//直線パラメータとの近接点
POINT2D CNodeBound::_NearPalmLine(double dA, double dB, double dC, POINT2D pt)
{
    POINT2D ptRet;

    double dL = sqrt(dA*dA + dB*dB);
    double dAlpha = (dA * pt.dX + dB * pt.dY + dC) /dL;

    ptRet.dX = pt.dX - dA * dAlpha;
    ptRet.dY = pt.dY - dB * dAlpha;

    return ptRet;
}


POINT2D CNodeBound::_GetSnapPointOnLine(const LINE2D& l,       //スナップポイントがのる直線
                                        const POINT2D& ptFix,  //固定点
                                        const POINT2D& ptMove, //移動点
                                        bool bSnap)            //スナップの有無
{
    POINT2D ptDir;
    l.NearPoint(ptDir, ptMove);
    POINT2D ptRet = ptDir;

    if(bSnap)
    {
        double dLen    = ptFix.Distance(ptDir);
        m_pView->GetSnapLength(&dLen, dLen);

        CIRCLE2D cr(ptFix, dLen);
        std::vector<POINT2D> lstPoint;

        if(cr.Intersect(l, &lstPoint))
        {
            double dLMin = DBL_MAX;
            double dL;
            for(POINT2D pt: lstPoint)
            {
                dL = pt.Distance(ptDir);
                if(dLMin > dL)
                {
                    ptRet =  pt;
                    dLMin = dL;
                }
            }
        }
    }

    return  ptRet;
}

void CNodeBound::_CalcWidthAndHeight(
    POINT2D* pPtW,
    POINT2D* pPtH, 
    POINT2D* pPtWH, 
    LINE2D*  pLw,
    LINE2D*  pLh,
    POINT2D ptFix, 
    POINT2D ptMouse,
    double  dAngle,
    bool bSnap)
{

    POINT2D ptW;
    POINT2D ptH;

    double dC = cos(dAngle * DEG2RAD);
    double dS = sin(dAngle * DEG2RAD);

    //固定点を通る水平線
    double dAw = dS;
    double dBw = -dC;
    double dCw = dC * ptFix.dY - dS * ptFix.dX;

    //固定点を通る水直線
    double dAh = dC;
    double dBh = dS;
    double dCh = -dS * ptFix.dY - dC * ptFix.dX;

    LINE2D lw(dAw, dBw, dCw);
    LINE2D lh(dAh, dBh, dCh);


    *pPtW = _GetSnapPointOnLine(lw, ptFix,  ptMouse, bSnap);
    *pPtH = _GetSnapPointOnLine(lh, ptFix,  ptMouse, bSnap);

    if(pLw)
    {
        *pLw =  lw;
    }

    if(pLh)
    {
        *pLh =  lh;
    }

    if(pPtWH)
    {
        double dCwh = dC * pPtH->dY - dS *  pPtH->dX;
        LINE2D lwh(dAw, dBw, dCwh);

        double dChw = -dS * pPtW->dY - dC * pPtW->dX;
        LINE2D lhw(dAh, dBh, dChw);

        lwh.IntersectInfnityLine(pPtWH, lhw);
    }
}



/**
 *  @brief   マーカ移動
 *  @param   [in] ptFix     固定点(中心点)
 *  @param   [out] pbWidth  幅基準かどうか
 *  @param   [in] ptMouse    マウス位置
 
 *  @retval  なし
 *  @note
 */

POINT2D CNodeBound::_CalcMoveNodeMarker(POINT2D ptFix,
                            bool* pbWidth,
                            const POINT2D& ptMouse
                         )
{
    double dAngle = m_dAngle;
    double dOrgWidth = m_dOrgWidth;
    double dOrgHeight = m_dOrgHeight;

    //アスペクト比固定
    MAT2D matNoSclFix;
    POINT2D ptM;
    matNoSclFix.SetAffineParam(ptFix, dAngle, 1.0, 1.0, 0.0);

    ptM = matNoSclFix.GetRevAffinePoint(ptMouse);

    double dAbsPosX = fabs(ptM.dX);
    double dAbsPosY = fabs(ptM.dY);

    double dAspect;
    double dPosAspect;

    bool bWidth  = false;
    bool bHeight = false;

    POINT2D ptRet;

    //移動点が 第一,三象限にある時は 傾きdAspect
    //第二,四象限にある時は 傾き-dAspectの直線上の点を使用する
    //したがって ptMのX,Yの値が同符号かどうかで判定する

    double dAspectDir = 1.0;
    if ((ptM.dX * ptM.dY) < 0.0)
    {
        dAspectDir = -1.0;
    }

    if ((dAbsPosX < NEAR_ZERO) &&
        (dAbsPosY < NEAR_ZERO))
    {
        ptRet.dX = ptM.dX;
        ptRet.dY = ptM.dY;
    }
    else if (dAbsPosX > dAbsPosY)
    {
        dAspect = dOrgHeight / dOrgWidth;
        dPosAspect = ptM.dY / dAbsPosX;
        if ((dPosAspect > -dAspect) &&
            (dPosAspect < dAspect))
        {
            //幅基準
            ptRet.dX = ptM.dX;
            ptRet.dY = dAspectDir * fabs(dAspect) * ptM.dX;
            bWidth = true;
        }
        else
        {
            //高さ基準
            ptRet.dY = ptM.dY;
            ptRet.dX = dAspectDir * ptM.dY / fabs(dAspect);
        }
    }
    else
    {
        dAspect = dOrgWidth / dOrgHeight;
        dPosAspect = ptM.dX / dAbsPosY;
        if ((dPosAspect > -dAspect) &&
            (dPosAspect < dAspect))
        {
            //高さ基準
            ptRet.dY = ptM.dY;
            ptRet.dX = dAspectDir * fabs(dAspect) * ptM.dY;
        }
        else
        {
            //幅基準
            ptRet.dX= ptM.dX;
            ptRet.dY = dAspectDir * ptM.dX / fabs(dAspect);
            bWidth = true;
        }
    }

    if (pbWidth)
    {
        *pbWidth = bWidth;
    }

    ptRet = ptRet * matNoSclFix ;

    return    ptRet;
}

POINT2D CNodeBound::_GetFixPoint( CNodeBound::E_NODE eNode,
                               bool bBothSide)
{
    POINT2D ptFix;
    if (!bBothSide)
    {
        E_NODE eOppsit = _GetOppositSide(eNode);
        STD_ASSERT(eOppsit != ND_NONE);

        ptFix = m_psNode[eOppsit]->GetPoint();

    }
    else
    {
        ptFix = m_psNode[ND_C]->GetPoint();
    }
    return ptFix;
}


double CNodeBound::_GetSnapScl(CDrawingView* pView, double dOrgLen, double dLen)
{
    double dDispScl = pView->GetCurrentLayer()->dScl * pView->GetViewScale() * pView->GetDpu();

    //最小ドットの長さ（表示単位系) 
    double dMinRezLen = m_dMinResDot / dDispScl;


    //最小スナップ長に対する倍率
    double dMagnificationOfMinSnap = dMinRezLen/dOrgLen; 
	double dLog = log10(dMagnificationOfMinSnap);
	double dN =  ceil(dLog);
    double dMinMag = pow(10,dN);
	double dScl = dLen / dOrgLen;
	double dSnapScl = CUtil::Round(dScl / dMinMag) * dMinMag;

	return dSnapScl;
}


MAT2D CNodeBound::_MoveMiddle(CDrawingView* pView,
                             SNAP_DATA* pSnap,
                             CNodeBound::E_NODE eNode,
                             bool bIgnoreSnap,
                             bool bBoth)
{
    POINT2D ptFix;
    ptFix = _GetFixPoint(eNode, bBoth);

    double dFactor = 1.0;
    if(bBoth)
    {
        dFactor = 2.0;
    }


    bool bWidth = false;
    if ((eNode == ND_MR) ||
        (eNode == ND_ML))
    {
        bWidth = true;
    }

    POINT2D ptW;
    POINT2D ptH;
            
    POINT2D ptMouse;
    ptMouse = pSnap->ptOrg;



    DWORD dwSnapType = DRAW_CONFIG->GetSnapType();
    bool bLengthSnap = false;
	if (!bIgnoreSnap)
	{
        if(pSnap->eSnapType & dwSnapType)
        {
            ptMouse = pSnap->pt;
        }
        else
        {
            bLengthSnap = true;
        }
	}
    
            
    _CalcWidthAndHeight( &ptW,
                         &ptH,
                         NULL,
                         NULL,
                         NULL,
                         ptFix, 
                         ptMouse,
                         m_dAngle + m_dOrgAngle,
                         bLengthSnap);


    
    StdString strWideHight;


    double dSclX = 1.0;
    double dSclY = 1.0;
    POINT2D ptMove = m_ptInit[eNode];

	double dLen;
    double dDir;
    double dScl;
    if (bWidth)
    {
        dDir =  CUtil::GetDirection(ptFix, ptMove, ptW);
        strWideHight = GET_STR(STR_WIDTH);
        dLen    = dFactor * ptFix.Distance(ptW);
        dScl = dSclX = dDir * (dLen / m_dOrgWidth);
        m_dWidth = dLen;
        ptMouse = ptW;
    }
    else
    {
        dDir =  CUtil::GetDirection(ptFix, ptMove, ptH);
        strWideHight = GET_STR(STR_HEIGHT);
        dLen    = dFactor * ptFix.Distance(ptH);
        dScl =  dSclY = dDir * (dLen / m_dOrgHeight);
        m_dHeight = dLen;
        ptMouse = ptH;
    }


#if 0
    DB_PRINT(_T("_GetDirection ptF:%s ptM:%s ptW:%s Dir:%f\n"), ptFix.Str().c_str(), 
                                                                ptMove.Str().c_str(),
                                                                ptW.Str().c_str(),  dDir); 
#endif
    StdString strString;
    StdStringStream   strmLength;

    if (!pSnap->strSnap.empty())
    {
        strmLength << pSnap->strSnap << _T("\n");
    }

           
    bool bTextAngle = false;

    strmLength << strWideHight << _T(":") << dLen << _T("\n");

    StdString strScl = CDrawingObject::GetValText(dScl , bTextAngle, DRAW_CONFIG->iDimLengthPrecision);
    strmLength << GET_STR(STR_SCALE) << _T(":") << strScl << _T("\n");
    pSnap->strSnap = strmLength.str();


    POINT2D ptCenter = (ptMouse + ptFix) / 2.0;
    MAT2D mat;
    mat.Scl(ptFix, m_dAngle + m_dOrgAngle, dSclX, dSclY);

#if 0

    POINT2D ptFixCalc = ptFix * mat;
    POINT2D ptCCalc = m_ptInit[ND_C] * mat;
    POINT2D ptMoveCalc = m_ptInit[eNode] * mat;

    POINT2D ptTL = m_ptInit[ND_TL] * mat;
    POINT2D ptTR = m_ptInit[ND_TR] * mat;
    POINT2D ptBR = m_ptInit[ND_BR] * mat;
    POINT2D ptBL = m_ptInit[ND_BL] * mat;

    double dCW = ptTL.Distance(ptTR);
    double dCH = ptTL.Distance(ptBL);

    double dSx = dCW / m_dOrgWidth;
    double dSy = dCH / m_dOrgHeight;


    DB_PRINT(_T("CNodeBound3_R ptF:%s ptC:%s ptM:%s Sx%f: Sy:%f W%f: H:%f Len:%f Ang:%f\n"),
        ptFix.Str().c_str(),
        ptCenter.Str().c_str(), 
        ptMouse.Str().c_str(),
        dSclX,  dSclY,
        m_dWidth, m_dHeight,
        dLen, m_dAngle
        );

    DB_PRINT(_T("CNodeBound3_C ptF:%s ptC:%s ptM:%s Sx%f: Sy:%f W%f: H:%f \n"),
        ptFixCalc.Str().c_str(),
        ptCCalc.Str().c_str(), 
        ptMoveCalc.Str().c_str(),
        dSx, dSy,
        dCW, dCH
    );

    POINT2D pt;
    double dAngle;
    double dSclMX;
    double dSclMY;
    double dT;
    mat.GetAffineParam(&pt, &dAngle, &dSclMX, &dSclMY, &dT);
    DB_PRINT(_T("CNodeBound3 pt:%s, dAngle:%f dSclX:%f dSclY:%f  \n"),
        pt.Str().c_str(),
        dAngle,
        dSclMX,
        dSclMY
    );


    DB_PRINT(_T("CNodeBound3 ptF:%s ptC:%s m_dW:%f m_dH:%f dW:%f  dH:%f m_dAngle %f \n\n"),
        ptFix.Str().c_str(),
        ptCenter.Str().c_str(), m_dWidth,  m_dHeight,  dCW, dCH,
        m_dAngle);


    DB_PRINT(_T("CNodeBound Mat Start -----\n"));
    DB_PRINT(_T("%s\n"), mat.Str().c_str());
    DB_PRINT(_T("CNodeBound Mat End -----\n"));

    double dNewSclX;
    double dNewSclY;
    double dNewSkew;
    double dNewAxis;

    mat.GetAxixParam( &dNewSclX, &dNewSclY,&dNewAxis, &dNewSkew, m_dAngle, 0.0);


#endif

    m_ptMove   = ptMove;
    m_ptCenter = ptCenter;

    pSnap->pt =  ptMouse;
    return mat;

}



MAT2D CNodeBound::_MoveCorner(CDrawingView* pView,
                             SNAP_DATA* pSnap,
                             CNodeBound::E_NODE eNode,
                             bool bIgnoreSnap,
                             bool bBoth)
{



    POINT2D ptFix;
    double dFactor = 1.0;
    ptFix = _GetFixPoint(eNode, bBoth);
    if(bBoth)
    {
        dFactor = 2.0;
    }

           
    POINT2D ptMouse;
    ptMouse = pSnap->ptOrg;
            


    POINT2D ptRet;
    bool bWightHeightSnap = false;
    DWORD dwSnapType = DRAW_CONFIG->GetSnapType();
    if (!bIgnoreSnap)
	{
        if(pSnap->eSnapType & dwSnapType)
        {
            ptMouse = pSnap->pt;
        }
        else
        {
            bWightHeightSnap = true;
        }
	}

    double dSclX = 1.0;
    double dSclY = 1.0;
    POINT2D ptCenter;
    double dWidth;
    double dHeight;


    POINT2D ptMove = m_ptInit[eNode];


    bool bAspect = false;
    if(bWightHeightSnap)
    {

        POINT2D ptOnAspectLine;
        //アスペクト比一定の直線
        POINT2D ptAspectLine = m_psNode[eNode]->GetPoint();

        //!< 中心点と一点を通る直線へのスナップ点
        bAspect = pView->GetInfLineSnapPoint(  &ptOnAspectLine,  
            ptFix,  
            ptAspectLine,  
            ptMouse);

       if(bAspect)
       {
            //参照線設定
           LINE2D  lAspect(ptFix,  ptAspectLine);
           double dA, dB, dC;
           lAspect.GetParam(&dA, &dB, &dC);
           m_psAdditionalLine2->GetLineInstance()->SetParam(dA, dB, dC);

           CDrawMarker* pMarker= pView->GetMarker();
           pMarker->AddObject(m_psAdditionalLine2);

           POINT2D ptVec = ptAspectLine-ptFix;
           ptVec.Normalize();

            //きりのいい倍率にする
           double dMin = m_pView->GetSnapMinLength();
           double dMinScl;

           LINE2D  lAspect2(ptFix,  ptOnAspectLine);
           lAspect2.Rotate(ptFix, -(m_dOrgAngle + m_dAngle));

           double dSclLen;  
           double dScl;     //現在のマーカー位置でのスケール
           if(m_dOrgWidth >  m_dOrgHeight)
           {
               dMinScl = dMin/m_dOrgWidth;
               dSclLen = dFactor * fabs(lAspect2.GetPt1().dX - lAspect2.GetPt2().dX); 
               dScl = dSclLen/m_dOrgWidth;
           }
           else
           {
               dMinScl = dMin/m_dOrgHeight;
               dSclLen = dFactor * fabs(lAspect2.GetPt1().dY - lAspect2.GetPt2().dY);
               dScl = dSclLen/m_dOrgHeight;
           }

           double dNOrg = log10(dMinScl);
           double dN = 0.0;
           
            dN = ceil(dNOrg);

           double dMinDiv = 1.0;
           if(dN < 0)
           {
               dN += 1.0;
           }
           dMinDiv = pow(10, dN) / 10;;

           double dir = CUtil::GetDirection(ptFix, ptMove, ptOnAspectLine);

           double dSnapScl =  (CUtil::Round(dScl / dMinDiv)) * dMinDiv;  

           double dCoffScl = sqrt((m_dOrgWidth * m_dOrgWidth) +  (m_dOrgHeight * m_dOrgHeight));
           POINT2D ptSnap =  (dir * dSnapScl * dCoffScl) * ptVec + ptFix;

 
           double dDst = ptSnap.Distance(ptOnAspectLine);
           dScl *= dir;
           dSnapScl *= dir;

#if _DEBUG_SCL
          
           m_psListAdditional.clear();
           std::shared_ptr<CDrawingPoint> drawPt = std::make_shared<CDrawingPoint>(); 
           drawPt->SetPoint(ptSnap);
           m_psListAdditional.push_back(drawPt);

           double snap = ptSnap.Dot(ptOnAspectLine);

           DB_PRINT(_T("_MoveCornerOne snap %f dir:%f \n"), snap, dir );
           DB_PRINT(_T("dMinDiv %f dSnapScl:%f dCoffScl:%f dDst:%f Snap:%s \n"), dMinDiv, dSnapScl, 
           dCoffScl, dDst,
           ptSnap.Str().c_str());
#endif

           if(dDst < dMin)
           {
               pSnap->pt = ptSnap;

               dWidth =  fabs(dSnapScl* m_dOrgWidth);
               dHeight = fabs(dSnapScl* m_dOrgHeight);

               dSclX =  dSnapScl;
               dSclY =  dSnapScl;
           }
           else
           {
               pSnap->pt = ptOnAspectLine;
               dWidth  = fabs(dScl* m_dOrgWidth);
               dHeight = fabs(dScl* m_dOrgHeight);

               dSclX =  dScl;
               dSclY =  dScl;
           }


           {
               StdString strString;
               StdStringStream   strmLength;

               if (!pSnap->strSnap.empty())
               {
                   strmLength << pSnap->strSnap << _T("\n");
               }

               StdString strScl;
               StdString strWidth;
               StdString strHeight;

               bool bTextAngle = false;

               strScl = CDrawingObject::GetValText(dSnapScl, bTextAngle, DRAW_CONFIG->iDimLengthPrecision);
               strWidth = CDrawingObject::GetValText(dWidth, bTextAngle, DRAW_CONFIG->iDimLengthPrecision);
               strHeight = CDrawingObject::GetValText(dHeight, bTextAngle, DRAW_CONFIG->iDimLengthPrecision);

               strmLength << GET_STR(STR_SCALE) << _T(":") << strScl << _T("\n");
               strmLength << GET_STR(STR_WIDTH) << _T(":") << strWidth << _T("\n");
               strmLength << GET_STR(STR_HEIGHT) << _T(":") << strHeight;
               pSnap->strSnap = strmLength.str();
           }
       }
    }




    if(!bAspect)
    {

        POINT2D ptW;
        POINT2D ptH;
        POINT2D ptWH;
        LINE2D lW;
        LINE2D lH;

        _CalcWidthAndHeight( &ptW,
                             &ptH,
                             &ptWH,
                             &lW,
                             &lH,
                             ptFix, 
                             ptMouse,
                             m_dOrgAngle + m_dAngle,
                             bWightHeightSnap);



        dWidth =  dFactor * ptFix.Distance(ptW);
        dHeight = dFactor * ptFix.Distance(ptH);

        StdString strString;
        StdStringStream   strmLength;
        bool bTextAngle = false;

        if(bWightHeightSnap)
        {

            if (!pSnap->strSnap.empty())
            {
                strmLength << pSnap->strSnap << _T("\n");
            }

            StdString strWidth;
            StdString strHeight;


            strWidth = CDrawingObject::GetValText(dWidth, bTextAngle, DRAW_CONFIG->iDimLengthPrecision);
            strHeight = CDrawingObject::GetValText(dHeight, bTextAngle, DRAW_CONFIG->iDimLengthPrecision);

            strmLength << GET_STR(STR_WIDTH) << _T(":") << strWidth << _T("\n");
            strmLength << GET_STR(STR_HEIGHT) << _T(":") << strHeight << _T("\n");

            
        }

        if (m_dOrgWidth > NEAR_ZERO)
        {

            POINT2D pOrgDirW = lW.NearPoint(ptMove);
            double dDirX = CUtil::GetDirection(ptFix, pOrgDirW, ptW);
            dSclX = dDirX * (dWidth / m_dOrgWidth);

            StdString strSclWidth = CDrawingObject::GetValText(dSclX, bTextAngle, DRAW_CONFIG->iDimLengthPrecision);
            strmLength << GET_STR(STR_SCALE) << _T(" ") << GET_STR(STR_WIDTH) << _T(":") << strSclWidth << _T("\n");
        }

        if (m_dOrgHeight > NEAR_ZERO)
        {
            POINT2D pOrgDirH = lH.NearPoint(ptMove);
            double dDirY =  CUtil::GetDirection(ptFix, pOrgDirH, ptH);
            dSclY = dDirY * (dHeight / m_dOrgHeight);

            StdString strSclHeight = CDrawingObject::GetValText(dSclY, bTextAngle, DRAW_CONFIG->iDimLengthPrecision);
            strmLength << GET_STR(STR_SCALE) << _T(" ") << GET_STR(STR_HEIGHT) << _T(":") << strSclHeight << _T("\n");
        }

        pSnap->strSnap = strmLength.str();
        ptCenter = (ptWH + ptFix) / 2.0;

#if _DEBUG_SCL
        DB_PRINT(_T("SclX %f, SclY %f w:%f h:%f W:%s H:%s WH:%s \n"),dSclX, dSclY,
        dWidth, dHeight,
        ptW.Str().c_str(), ptH.Str().c_str(), ptWH.Str().c_str());
#endif
    }

    MAT2D mat;
    mat.Scl(ptFix, m_dAngle + m_dOrgAngle, dSclX, dSclY);

    m_ptMove   = ptCenter - m_ptCenter;
    m_ptCenter = ptCenter;


    return mat;
}
 



MAT2D CNodeBound::_MoveCenter(CDrawingView* pView,
                     SNAP_DATA* pSnap,
                     CNodeBound::E_NODE eMode,
                     bool bIgnoreSnap)
{

    POINT2D ptMouse;
    
	bool bAngle = true;
	bool bLength = true;
	if (bIgnoreSnap)
	{
		ptMouse = pSnap->ptOrg;
		bAngle = false;
		bLength = false;
	}
	else
	{
		ptMouse = pSnap->pt;

	}

	using namespace VIEW_COMMON;

	if (pSnap->eSnapType == SNP_NONE) 
	{
		double dAngle;
		double dLength;
		bool bRet;
		bRet =pView->GetLengthSnapPoint(&dAngle,
										&dLength,
										&ptMouse,  
                                        m_ptOrgCenter,  
										ptMouse,
										bAngle,
										bLength);

		if (bRet)
		{
			pSnap->strSnap = CViewAction::GetSnapText(SNP_ANGLE, 
														dLength,
														dAngle,
														false);
		}
	}
	else
	{
        ;
	}


	m_ptCenter = ptMouse;

    MAT2D mat;

	m_psAdditionalLine1->SetPoint1(m_ptOrgCenter);
	m_psAdditionalLine1->SetPoint2(ptMouse);
    m_psAdditionalLine1->SetLayer(m_pView->GetCurrentLayerId());

	CDrawMarker* pMarker= m_pView->GetMarker();
	pMarker->AddObject(m_psAdditionalLine1);


    POINT2D ptDiff = m_ptCenter - m_ptOrgCenter;
    mat.SetAffineParam(ptDiff, 0.0, GetXScl(), GetYScl(), 0.0);

#if 0 
    DB_PRINT(_T("CNodeBound MoveCenter Mat Start -----\n"));
    DB_PRINT(_T("PtDiff:%s Sx%f Sy%f\n"), ptDiff.Str().c_str(), GetXScl(),GetYScl());
    DB_PRINT(_T("%s\n"), mat.Str().c_str());
    DB_PRINT(_T("CNodeBound Mat End -----\n"));
#endif
    return mat;

}

MAT2D CNodeBound::_RotateCenter(CDrawingView* pView,
                     SNAP_DATA* pSnap,
                     CNodeBound::E_NODE eMode,
                     bool bIgnoreSnap)
{
	bool bRet;
	double dAngle;
	double dLength;
	POINT2D ptSnap;
	bool bAngle = true;

	if (bIgnoreSnap)
	{
		bAngle = false;
	}

	POINT2D ptPos = m_ptCenter;
	if (pSnap->eSnapType == VIEW_COMMON::SNP_NONE)
	{
		bRet = m_pView->GetLengthSnapPoint(&dAngle,
										&dLength,
										&ptSnap,  
										ptPos,         //開始点
										pSnap->pt,     //マウスカーソル位置
										bAngle,        //角度スナップの有無
										false,         //距離スナップの有無
                                        m_dOrgAngle    //角度オフセット
                                        );
			//SetMoveMode();
		dAngle = dAngle - 90.0;

	}
	else
	{
		POINT2D ptAngle;
		ptAngle = pSnap->pt - ptPos;
		dAngle = atan2(ptAngle.dY, ptAngle.dX)* RAD2DEG - 90.0;   
		ptSnap = pSnap->pt;
        dAngle = CUtil::NormAngle( dAngle - m_dOrgAngle);
	}


	//角度を表示する
	StdStringStream   strmAngle;
	if (!pSnap->strSnap.empty())
	{
		strmAngle << pSnap->strSnap << _T("\n");
	}

	bool bTextAngle = true;
	StdString strAngle;
	strAngle = CDrawingObject::GetValText(dAngle, bTextAngle, DRAW_CONFIG->iDimAnglePrecision);

	strmAngle << GET_STR(STR_ANGLE) << _T(":") << strAngle;
	pSnap->strSnap = strmAngle.str();


	m_psAdditionalLine1->SetPoint1(ptPos);
	m_psAdditionalLine1->SetPoint2(ptSnap);
    m_psAdditionalLine1->SetLayer(m_pView->GetCurrentLayerId());

     CDrawMarker* pMarker= m_pView->GetMarker();

	pMarker->AddObject(m_psAdditionalLine1); 

	m_dAngle = dAngle;

    MAT2D mat;

    //mat.Rotate(m_ptCenter, m_dAngle);
    //相対移動量を算出

    POINT2D ptDiff = m_ptCenter - m_ptOrgCenter;
    //mat.SetAffineParam(ptDiff, m_dAngle, GetXScl(), GetYScl(), 0.0);


    POINT2D ptS = -(m_ptCenter);
    MAT2D matA;
    MAT2D matB;
    MAT2D matC;
    MAT2D matD;
    matA.Move(ptS);
    matB.Rotate(m_dAngle);
    matC.Scl(GetXScl(),GetYScl());
    matD.Move( m_ptCenter);

if(pView->GetDebugMode() != CDrawingView::DM_DISABLE)
{
DB_PRINT(_T("Rotate ptCenter %s:"), m_ptCenter.Str().c_str());
DB_PRINT(_T(" ptCenterOrg %s:"), m_ptOrgCenter.Str().c_str());
DB_PRINT(_T(" Angle %f \n"), m_dAngle);
}

    //mat.SetCenter(m_ptCenter);
    mat = matA * matB * matC * matD;
    return mat;
    
}


//!< マウス移動(ドラッグ)
void CNodeBound::OnMouseMove(SNAP_DATA* pSnap,
                             MOUSE_MOVE_POS posMouse)
{
    if(!m_pView)
    {
        return;
    }

    CNodeMarker* pNodeMarker;
    pNodeMarker = m_pView->GetNodeMarker();

    //Snap処理は外部

    bool bMark = false;
    pNodeMarker->MouseOver(posMouse.ptSel);

    
    //StdString strId = pNodeMarker->GetMarkerId(posMouse.ptSel);

    if (m_strSelectNodeMarkerId != _T(""))
    {

        MAT2D mat;
        mat = Move(pNodeMarker,
             pSnap,
             m_strSelectNodeMarkerId,
             posMouse);

        Matrix(mat);

#ifdef _DEBUG_PRINT
        DB_PRINT(_T("CNodeBound::OnMouseMove--%s \n"), mat.StrAffine().c_str());
#endif


        CDrawMarker* pMarker= m_pView->GetMarker();
        if(m_pTmpMovingObject)
        {
            auto pDef = m_pView->GetPartsDef();
            pDef->SetLockUpdateNodeData(true);
            pDef->GetListChangeByConnection()->clear();

            const MAT2D* pMat = GetMatrix();
            m_pTmpMovingObject->OffsetMatrix(pMat);
            pMarker->AddObject(m_pTmpMovingObject); 

           pDef->SetLockUpdateNodeData(false);

            //全ノード位置の更新と関連オブジェクト、その関係の更新
            std::set<int> lstObj;
            CViewAction::UpdateAllNodePositionAndRelation(pDef,
                pDef->GetListChangeByConnection(),
                &lstObj,
                NULL);

            auto pList = pDef->GetConnectingTmpObjectList();
            for (auto pRefObj : *pList)
            {
#ifdef _DEBUG_PRINT
DB_PRINT(_T("CNodeBound::OnMouseMove pMarker->AddObject %d %I64x %s\n"), pRefObj->GetId(), pRefObj.get(), pRefObj->GetTag().c_str());
#endif
                pMarker->AddObject(pRefObj);
                pDef->SetMouseEmphasis(pRefObj);
            }

            for(auto obj: m_psListAdditional)
            {
                pMarker->AddObject(obj);
            }

        }
        else
        {
        }
        pNodeMarker->SetMarkerPosView(m_strSelectNodeMarkerId, pSnap->pt);
#ifdef _DEBUG_PRINT
   DB_PRINT(_T("CNodeBound::OnMouseMove END--\n"));
#endif
    }
}

//!< マーカ移動GetCenter
MAT2D CNodeBound::Move(CNodeMarker* pMarker, 
                      SNAP_DATA* pSnap,
                      StdString strMarkerId,
                      MOUSE_MOVE_POS posMouse)
{
    MAT2D retMat;
    if (!m_pView)
    {
        return retMat;
    }

#ifdef _DEBUG
	THIS_APP->IncDebugCount();
#endif


    //MoveNodeMarkerに対応
    pMarker->SetVisibleOnly(strMarkerId, true);

    bool bIgnoreSnap = false;
    if (posMouse.nFlags & MK_SHIFT)
    {
        //スナップ無視
        bIgnoreSnap = true;
    }

    if (posMouse.nFlags & MK_CONTROL)
    {
        //両側
        m_eDragMode = DM_BOTH;
    }
    else
    {
        m_eDragMode = DM_ONE;
    }

    POINT2D ptMouse;
    if (bIgnoreSnap)
    {
        ptMouse = pSnap->ptOrg;
    }
    else
    {
        ptMouse = pSnap->pt;
    }

    //tanα = h/w とした時
    //tan(α+θ) = tan(α+θ) = (tanα+tanθ)/1- tanα*tanθ)
    //=(h+w*tanθ)/(w-h*tanθ)

    /*
    double w = -m_dOrgWidth;
    double h = m_dOrgHeight;
    double t = tan(m_dAngle * DEG2RAD);
    */
    
    E_NODE eNode = GetNode(strMarkerId);

    bool bBoth = false;
    if (m_eDragMode == DM_BOTH)
    {
        bBoth = true;
    }
    switch(eNode)
    {
    case ND_TL:
    case ND_TR:
    case ND_BL:
    case ND_BR:
        retMat = _MoveCorner(m_pView, pSnap, eNode, bIgnoreSnap, bBoth);
        break;

    case ND_TM:
    case ND_BM:
    case ND_MR:
    case ND_ML:
        retMat = _MoveMiddle(m_pView, pSnap, eNode, bIgnoreSnap, bBoth);
        break;

    case ND_C:
        retMat = _MoveCenter(m_pView, pSnap, eNode, bIgnoreSnap);
        break;

    case ND_R:
        retMat = _RotateCenter(m_pView, pSnap, eNode, bIgnoreSnap);
        break;

    default:
        break;
    }
    return retMat;
}

//!< マーカ開放
void CNodeBound::Release()
{
    for(std::unique_ptr<CDrawingNode>& pNode: m_psNode)
    {
        pNode.reset();
    }
    m_psAdditionalLine1.reset();
    m_psAdditionalLine2.reset();

    POINT2D ptDraw;
    m_eMoveMode = ND_NONE;
    m_pView = NULL;
}


CNodeBound::E_NODE CNodeBound::_GetOppositSide(CNodeBound::E_NODE eMode) const
{
    E_NODE eOpposit;
    if      (eMode == ND_TL){eOpposit = ND_BR;}
    else if (eMode == ND_TR){eOpposit = ND_BL;}
    else if (eMode == ND_BR){eOpposit = ND_TL;}
    else if (eMode == ND_BL){eOpposit = ND_TR;}
    else if (eMode == ND_TM){eOpposit = ND_BM;}
    else if (eMode == ND_BM){eOpposit = ND_TM;}
    else if (eMode == ND_MR){eOpposit = ND_ML;}
    else if (eMode == ND_ML){eOpposit = ND_MR;}
    else {eOpposit =ND_NONE;}

    return eOpposit;
}

POINT2D CNodeBound::GetOrgCenter()
{
    return m_ptOrgCenter;
}

POINT2D CNodeBound::GetC()
{
    return m_psNode[ND_C]->GetPoint();
}


POINT2D CNodeBound::GetTL()
{
    return m_psNode[ND_TL]->GetPoint();
}

POINT2D CNodeBound::GetTR()
{
   return m_psNode[ND_TR]->GetPoint();
}

POINT2D CNodeBound::GetBL()
{
    return m_psNode[ND_BL]->GetPoint();
}

POINT2D CNodeBound::GetBR()
{
    return m_psNode[ND_BR]->GetPoint();
}

POINT2D CNodeBound::GetOppositPoint(CNodeBound::E_NODE eNode ) const
{
     E_NODE eOpposit;
     POINT2D ptRet;
     eOpposit = _GetOppositSide(eNode);
     ptRet = m_psNode[eOpposit]->GetPoint();
     return ptRet;
}


POINT2D CNodeBound::GetOppositPoint(StdString strMarkerId) const
{
     E_NODE eNode = GetNode(strMarkerId);
     return GetOppositPoint(eNode);
}

CNodeBound::E_NODE CNodeBound::GetNode(StdString strMarkerId)
{
    E_NODE eRet;

    if      (strMarkerId == _T("TL")){eRet = ND_TL;}
    else if (strMarkerId == _T("TR")){eRet = ND_TR;}
    else if (strMarkerId == _T("BR")){eRet = ND_BR;}
    else if (strMarkerId == _T("BL")){eRet = ND_BL;}
    else if (strMarkerId == _T("TM")){eRet = ND_TM;}
    else if (strMarkerId == _T("BM")){eRet = ND_BM;}
    else if (strMarkerId == _T("MR")){eRet = ND_MR;}
    else if (strMarkerId == _T("ML")){eRet = ND_ML;}
    else if (strMarkerId == _T("C")){eRet = ND_C;}
    else if (strMarkerId == _T("R")){eRet = ND_R;}
    else {eRet = ND_NONE;}
    return eRet;
}

bool IsUsePlug(CDrawingObject& obj)
{
    int iTotal = obj.GetNodeDataNum();

    CNodeData* pConnection;
    std::set<CBindingPartner> setObj;

    for (int iNo = 0; iNo < iTotal; ++iNo)
    {
        pConnection = obj.GetNodeData(iNo);
        if (!pConnection)
        {
            continue;
        }

        if (!pConnection->lstPlug.empty())
        {
            return true;
        }
    }
    return false;
}

/*
 *  倍率
 *  @param   [in] ptCenter 中心座標
 *  @param   [in] dXScl    X軸倍率
 *  @param   [in] dYScl    Y軸倍率
 *  @retval  true 成功
 *  @note    
 */
bool CNodeBound::ApplyMatrix(bool bCopy)
{
    if(!m_pView)
    {
        return false;
    }
    CPartsDef*   pDef  = m_pView->GetPartsDef();
    //std::vector<CDrawingObject*> * pList; 
    //pList = pCtrl->GetSelectInstance();
    CUndoAction* pUndo  = pDef->GetUndoAction();

#ifdef _DEBUG_PRINT
{
POINT2D pt;
double dAngle;
double dSclX;
double dSclY;
double dT;
m_mat2D.GetAffineParam(&pt, &dAngle, &dSclX, &dSclY, &dT);
DB_PRINT(_T("ApplyMatrix Pt:%s A:%f Sx:%f Sy:%f \n"), pt.Str().c_str(), dAngle, dSclX, dSclY);
} 
#endif

	auto pObj = m_pView->GetNodeMarker()->GetObject();
	bool bRet = CViewAction::ApplyMatrix(pDef, pObj, m_mat2D, 1, bCopy);


    //!< 変更を通知
    //::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_VIEW_UPDATE_OBJECTS, (WPARAM)pParts, 0);
    return true;
}



bool CNodeBound::ShowMenu(POINT pt)
{
    CMenu menu;
    menu.CreatePopupMenu();

    UINT nPopup = MF_STRING | MF_ENABLED | MF_POPUP ;

    m_pView->GetWindow()->ClientToScreen(&pt);
    
    menu.AppendMenu(nPopup, 0, GET_STR(STR_MNU_ITEM_MOVE));
    menu.AppendMenu(nPopup, 1, GET_STR(STR_MNU_ITEM_COPY));
    menu.AppendMenu(MF_SEPARATOR);
    menu.AppendMenu(nPopup, 2, GET_STR(STR_MNU_CANCEL));

    int iSelObjId;
    iSelObjId = menu.TrackPopupMenu(
        TPM_LEFTALIGN  |    //クリック時のX座標をメニューの左辺にする
        TPM_NONOTIFY   |    
        TPM_RETURNCMD  |    // 関数の戻り値としてメニューＩＤを返す
        TPM_RIGHTBUTTON,    //右クリックでメニュー選択可能とする
        pt.x,   //メニューの表示位置
        pt.y,   
        m_pView->GetWindow()      //このメニューを所有するウィンドウ
    );

    switch(iSelObjId)
    {
    case -1:
        break;

    case 0:
        ApplyMatrix(false);
        break;

    case 1:
        ApplyMatrix(true);
        break;

    case 2:
        break;
 
    default:
        break;
    }

    menu.DestroyMenu();

    return true;
}

//*******************************
//             TEST
//*******************************
#ifdef _DEBUG

void TEST_GetDirection()
{
	POINT2D pt1( 5.0, 2.0);
	POINT2D pt2(10.0, 4.0);
	POINT2D pt3( 2.5, 1.0);
	POINT2D pt4( 7.5, 3.0);

    STD_ASSERT_DBL_EQ( CUtil::GetDirection(pt1, pt2, pt3) , -1.0);
    STD_ASSERT_DBL_EQ( CUtil::GetDirection(pt1, pt2, pt4) ,  1.0);
}

void TEST_CNodeBound()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());


	TEST_GetDirection();


    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}
#endif //_DEBUG

