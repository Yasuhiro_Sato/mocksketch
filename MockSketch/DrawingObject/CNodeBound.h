/**
 * @brief			CNodeBoundヘッダーファイル
 * @file			CNodeBound.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#if !defined(_NODE_BOUND_H_)
#define _NODE_BOUND_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DrawingObject/Primitive/POINT2D.h"
#include "DrawingObject/Primitive/RECT2D.h"
#include "DrawingObject/Primitive/MAT2D.h"
#include "DrawingObject/BaseObj.h"
#include "DrawingObject/CDrawingObject.h"
#include "COMMON_HEAD.h"
#include "MockDef.h"
#include "./CNodeMarker.h"


/*---------------------------------------------------*/
/*  classes                                          */
/*---------------------------------------------------*/
class CDrawingView;
class CDrawingObject;
class CDrawingNode;
class CDrawingLine;


class CNodeBound  :public CBaseObj
{
public:
    enum E_DRAG_MODE
    {
        DM_BOTH,
        DM_ONE,
    };

    enum E_SNAP_TYPE
    {
        ST_SCL, //倍率
        ST_DIM, //寸法
    };


        enum E_NODE
    {
        ND_NONE = -1,
        ND_TL = 0,
        ND_TM,
        ND_TR,
        ND_MR,
        ND_BR,
        ND_BM,
        ND_BL,
        ND_ML,
        ND_C,
        ND_R,
        ND_MAX
    };

private:
    //RECT2D  m_rcBoundOrg;
    std::vector<POINT2D> m_lstBoundOrg;


    std::unique_ptr<CDrawingNode> m_psNode[ND_MAX];

    std::shared_ptr<CDrawingLine> m_psAdditionalLine1;
    std::shared_ptr<CDrawingLine> m_psAdditionalLine2;

    std::unique_ptr<CDrawingLine> m_psLectLine[4];

    //初期値の保存
    std::vector<POINT2D> m_ptInit;

    //デバッグ用図形
    std::vector<std::shared_ptr<CDrawingObject>> m_psListAdditional;

    MAT2D m_mat2D;

    double m_dMinScl;

    double m_dAngle;

    double m_dOrgAngle;

    double m_dWidth;

    double m_dHeight;

    double m_dOrgWidth;

    double m_dOrgHeight;

    POINT2D m_ptCenter;

    POINT2D m_ptOrgCenter;

    POINT2D m_ptOrgX;

    POINT2D m_ptOrgY;

    POINT2D m_ptMove;   // 移動量

    CDrawingView* m_pView;

    bool m_bRotate;

    bool m_bMove;

    bool m_bScale;

    bool m_bShowRect;

    E_DRAG_MODE m_eDragMode;

    E_SNAP_TYPE m_eSnapType;

	CNodeBound::E_NODE m_eMoveMode;

	//画面上の最小スナップ長に対するDot数
	double m_dMinResDot;

    StdString m_strSelectNodeMarkerId;

    std::shared_ptr<CDrawingObject> m_pTmpMovingObject;

    double m_dMinWidth;

    double m_dMinHeight;

    double m_bEnableMinus;



public:
    CNodeBound();

    CNodeBound(bool bRotate, bool bMove, bool bScale);

    ~CNodeBound();

    void EnableRotate(bool bRotate);
	void EnableMove(bool bMOve);
	void EnableScale(bool bScale);
    void EnableMinus(bool bMinus);
    void ShowRect(bool bShow = true);
    void SetMinSize(double dWidth, double dHeight);
    void SetSnapType(CNodeBound::E_SNAP_TYPE eSnapType);    //未設定


    bool Init(CDrawingView* pView,
              const RECT2D& rc,
              std::shared_ptr<CDrawingObject> pParent);

    bool Init(CDrawingView* pView,
        const std::vector<POINT2D>& lstRect,
        double dAngle,
        double dXScl,
        double dYScl,
        double dT,
        std::shared_ptr<CDrawingObject> pParent
    );

    bool OnMouseLButtonDown(MOUSE_MOVE_POS posMouse);
    bool OnMouseLButtonUp(MOUSE_MOVE_POS posMouse);
    bool OnMouseRButtonDown(MOUSE_MOVE_POS posMouse);
    bool OnMouseRButtonUp(MOUSE_MOVE_POS posMouse);
    void OnMouseMove(SNAP_DATA* pSnap,
                   MOUSE_MOVE_POS posMouse);


    void Select(CNodeMarker* pMarker, 
                 StdString strMarkerId);


    MAT2D Move(CNodeMarker* pMarker,
                SNAP_DATA* pSnap,
                StdString strMarkerId,
                MOUSE_MOVE_POS posMouse);

    void Release();

    void SetCenter(POINT2D ptCenter);
    void SetAngle(double dAngle); 
    void SetMove(POINT2D ptAbs);
    void RecalcPosition();
    
    POINT2D GetOrgCenter();
    POINT2D GetC();
    POINT2D GetTL();
    POINT2D GetTR();
    POINT2D GetBL();
    POINT2D GetBR();


    double GetAngle() const;
    double GetWidth() const;
    double GetHeight() const;
    POINT2D GetCenter() const;
    POINT2D GetMove() const;
    double GetXScl() const;
    double GetYScl() const;
    const MAT2D* GetMatrix() const;
    MAT2D GetCenterdMatrix() const;


	POINT2D GetOppositPoint(CNodeBound::E_NODE eOpposit ) const;
    POINT2D GetOppositPoint(StdString strMarkerId) const;

    static CNodeBound::E_NODE GetNode(StdString strMarkerId);
    
    void Matrix(const MAT2D& mat2D);

    bool OnSetCursor(CNodeMarker* pNodeMarker, CDrawingView* pView);

	bool ApplyMatrix(bool bCopy);
    bool ShowMenu(POINT pt);

    void SetDragMode(E_DRAG_MODE eMode);
    E_DRAG_MODE GetDragMode() const;

    bool IsInit() const;


protected:

    CNodeBound::E_NODE _GetOppositSide(CNodeBound::E_NODE eMode) const;
    bool _OnMouseButtonDown(MOUSE_MOVE_POS posMouse, 
                            MOUSE_BUTTON eButton);

    MAT2D _MoveCorner(CDrawingView* pView,
                     SNAP_DATA* pSnap,
                     CNodeBound::E_NODE eMode,
                     bool bIgnoreSnap,
                     bool bBoth);


    MAT2D _MoveMiddle(CDrawingView* pView,
                     SNAP_DATA* pSnap,
                     CNodeBound::E_NODE eMode,
                     bool bIgnoreSnap,
                     bool bBoth);

    MAT2D _MoveCenter(CDrawingView* pView,
                     SNAP_DATA* pSnap,
                     CNodeBound::E_NODE eMode,
                     bool bIgnoreSnap);

    MAT2D _RotateCenter(CDrawingView* pView,
                     SNAP_DATA* pSnap,
                     CNodeBound::E_NODE eMode,
                     bool bIgnoreSnap);


	POINT2D _CalcMoveNodeMarker(POINT2D ptFix,
                                bool* pbWidth,
                                const POINT2D& ptMouse);

    void _CalcWidthAndHeight(
                         POINT2D* pPtW,
                         POINT2D* pPtH, 
                         POINT2D* pPtWH, 
                         LINE2D*  pLw,
                         LINE2D*  pLh,
                         POINT2D ptFix, 
                         POINT2D ptMouse,
                         double  dAngle,
                         bool bSnap);
     
    POINT2D _NearPalmLine(double dA, double dB, double dC, POINT2D pt);


    POINT2D _GetFixPoint( CNodeBound::E_NODE eNode,
                               bool bBothSide);


	double _GetSnapScl(CDrawingView* pView, double dOrgLen, double dLen);

    POINT2D _GetSnapPointOnLine(const LINE2D& l,       //スナップポイントがのる直線
        const POINT2D& ptFix,  //固定点
        const POINT2D& ptMove, //移動点
        bool bSnap);            //スナップの有無


};



#endif // _NODE_BOUND_H_
