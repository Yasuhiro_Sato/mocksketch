/**
* @brief        CNodeDataΐt@C
* @file	        CNodeData.cpp
* @author           Yasuhiro Sato
* @date	        07-2-2009 20:03:09
* 
* @note	
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
* 
* @remarks	        
*			
*
* $
* $
* 
*/

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "View/ViewCommon.h"
#include "./CDrawingObject.h"
#include "./CNodeData.h"


CNodeData::CNodeData():
iIndex              (-1),
iMaxPlug            (1),
iMaxSocket          (-1),
eConnectionType        (E_ACTIVE),
eConnectionObject      (NONE),
eConnectionDirection   (E_IN),
eLineDirection         (E_NORM_OBJECT_BOUNDS),
dwSnapType             (VIEW_COMMON::SNP_ALL_POINT)
{

}

CNodeData::~CNodeData()
{

}

CNodeData::CNodeData(const CNodeData & m)
{
    *this = m;
}

//!< γόZq
CNodeData& CNodeData::operator = (const CNodeData & m)
{
    //!< Κu
    ptPos                       = m.ptPos;
    iIndex                      = m.iIndex;
    iMaxPlug                    = m.iMaxPlug;
    iMaxSocket                  = m.iMaxSocket;
    eConnectionType             = m.eConnectionType;
    eConnectionObject           = m.eConnectionObject;
    eConnectionDirection        = m.eConnectionDirection;
    eLineDirection              = m.eLineDirection;
    nodeProperty                = m.nodeProperty;
    strConnectionPropertySet    = m.strConnectionPropertySet;
    lstSocket                   = m.lstSocket;
    lstPlug                     = m.lstPlug;
    dwSnapType                  = m.dwSnapType;
    strSnapOrginNode            = m.strSnapOrginNode;    
    return *this;
}

//!< ΏZq
bool CNodeData::operator == (const CNodeData & m) const
{
    if(ptPos                       != m.ptPos){return false;}
    if(iIndex                      != m.iIndex){return false;}
    if(iMaxPlug                    != m.iMaxPlug){return false;}
    if(iMaxSocket                  != m.iMaxSocket){return false;}
    if(eConnectionType             != m.eConnectionType){return false;}
    if(eConnectionObject           != m.eConnectionObject){return false;}
    if(eConnectionDirection        != m.eConnectionDirection){return false;}
    if(eLineDirection              != m.eLineDirection){return false;}
    if(nodeProperty                != m.nodeProperty){return false;}
    if(strConnectionPropertySet    != m.strConnectionPropertySet){return false;}
    if(lstSocket                   != m.lstSocket){return false;}
    if(lstPlug                     != m.lstPlug){return false;}
    if(dwSnapType                  != m.dwSnapType){return false;}
    if(strSnapOrginNode            != m.strSnapOrginNode){return false;}

    return true;
}

//!< ΏZq
bool CNodeData::operator != (const CNodeData & m) const
{
    return !(*this == m );
}

bool CNodeData::IsAbleToConnect(const CNodeData* pC) const
{

    if(!pC)
    {
        return false;
    }

    if((lstSocket.size() > iMaxSocket) && 
        (iMaxSocket != -1))
    {
        return false;
    }

    if ((eConnectionObject != NONE) &&
        (pC->eConnectionObject != NONE))
    {
        if(eConnectionObject != pC->eConnectionObject)
        {
            return false;
        }
    }

    if(eConnectionType == pC->eConnectionType)
    {
        return false;
    }

    if((eConnectionDirection != E_INOUT)&&
       (pC->eConnectionDirection != E_INOUT))
    {
        if(eConnectionDirection == pC->eConnectionDirection)
        {
            return false;
        }
    }

    if((strConnectionPropertySet != _T("")) &&
       (pC->strConnectionPropertySet != _T("")))
    {
        if(strConnectionPropertySet != pC->strConnectionPropertySet)
        {
            return false;
        }
    }
    
    if((eConnectionType == E_PASSIVE) &&
       (iMaxSocket != -1))
    {
        if(lstSocket.size() >= iMaxSocket)
        {
            return false;
        }
    }
    else if((eConnectionType == E_PASSIVE) &&
            (iMaxPlug != -1))
    {
        if(lstPlug.size() >= iMaxPlug)
        {
            return false;
        }
    }
    return true;
}

bool CNodeData::IsAbleToConnectEndToEnd(const CNodeData* pC) const
{
    if(!pC)
    {
#if 1
        DB_PRINT(_T("IsAbleToConnectEndToEnd 1\n"));
#endif

        return false;
    }

    if((lstSocket.size() > iMaxSocket) && 
        (iMaxSocket != -1))
    {
#if 1
        DB_PRINT(_T("IsAbleToConnectEndToEnd 2\n"));
#endif
        return false;
    }

    if ((eConnectionObject != NONE) &&
        (pC->eConnectionObject != NONE))
    {
        if(eConnectionObject != pC->eConnectionObject)
        {
#if 1
            DB_PRINT(_T("IsAbleToConnectEndToEnd 3\n"));
#endif
            return false;
        }
    }

    if(eConnectionType != pC->eConnectionType)
    {
#if 1
        DB_PRINT(_T("IsAbleToConnectEndToEnd 4\n"));
#endif
        return false;
    }

    if((eConnectionDirection != E_INOUT)&&
       (pC->eConnectionDirection != E_INOUT))
    {
        if(eConnectionDirection != pC->eConnectionDirection)
        {
#if 1
            DB_PRINT(_T("IsAbleToConnectEndToEnd 5\n"));
#endif
            return false;
        }
    }

    if((strConnectionPropertySet != _T("")) &&
       (pC->strConnectionPropertySet != _T("")))
    {
        if(strConnectionPropertySet != pC->strConnectionPropertySet)
        {
#if 1
            DB_PRINT(_T("IsAbleToConnectEndToEnd 6\n"));
#endif
            return false;
        }
    }
    
    if((eConnectionType == E_PASSIVE) &&
       (iMaxSocket != -1))
    {
        if(lstSocket.size() >= iMaxSocket)
        {
#if 1
            DB_PRINT(_T("IsAbleToConnectEndToEnd 7\n"));
#endif
            return false;
        }
    }
    else if((eConnectionType == E_PASSIVE) &&
            (iMaxPlug != -1))
    {
        if(lstPlug.size() >= iMaxPlug)
        {
#if 1
            DB_PRINT(_T("IsAbleToConnectEndToEnd 8\n"));
#endif
            return false;
        }
    }
    return true;
}
bool CNodeData::AddDistObject(CDrawingObject* pObj, int iIndex)
{
    if(!pObj)
    {
        return false;
    }

    CBindingPartner p;
    p.iId = pObj->GetId();
    p.iIndex = iIndex;

    auto ite = std::find (lstSocket.begin(),
                          lstSocket.end(), p);

    if (ite == lstSocket.end())
    {
        lstSocket.push_back(p);
    }
    return true;
}

bool CNodeData::DeleteSocket(const CDrawingObject* pObj, int iIndex)
{
    if(!pObj)
    {
        return false;
    }

    CBindingPartner p;
    p.iId = pObj->GetId();
    p.iIndex = iIndex;

    auto ite = std::find (lstSocket.begin(),
                          lstSocket.end(), p);

    if (ite != lstSocket.end())
    {
        lstSocket.erase(ite);
        return true;
    }
    return false;
}

bool CNodeData::DeletePlug(const CDrawingObject* pObj, int iIndex)
{
    if(!pObj)
    {
        return false;
    }

    CBindingPartner p;
    p.iId = pObj->GetId();
    p.iIndex = iIndex;

    auto ite = std::find (lstPlug.begin(),
                          lstPlug.end(), p);

    if (ite != lstPlug.end())
    {
        lstPlug.erase(ite);
        return true;
    }
    return false;
}

bool CNodeData::DeleteSocket(const CDrawingObject* pObj)
{
    bool bRet = false;
    if(!pObj)
    {
        return false;
    }

    int iId = pObj->GetId();
    
    for(auto it  = lstSocket.begin();
             it != lstSocket.end();)
    {
        if ((it->iId) == iId)
        { 
            it = lstSocket.erase(it); 
            bRet = true;
        }
        else
        {
            ++it;
        }
    }
    return bRet;
}

bool CNodeData::DeletePlug(const CDrawingObject* pObj)
{
    bool bRet = false;
    if(!pObj)
    {
        return false;
    }

    int iId = pObj->GetId();
    
    for(auto it  = lstPlug.begin();
             it != lstPlug.end();)
    {
        if ((it->iId) == iId)
        { 
            it = lstPlug.erase(it); 
            bRet = true;
        }
        else
        {
            ++it;
        }
    }
    return bRet;
}

bool CNodeData::Clear()
{
    lstPlug.clear();
    lstSocket.clear();
    return true;
}
