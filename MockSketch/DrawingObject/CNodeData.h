/**
* @brief        CNodeDataヘッダーファイル
* @file	        CNodeData.h
* @author           Yasuhiro Sato
* @date	        07-2-2009 20:03:09
* 
* @note	
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
* 
* @remarks	        
*			
*
* $
* $
* 
*/
#ifndef __CONNECTION_DATA_H_
#define __CONNECTION_DATA_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DrawingObject/CNodeMarker.h"
#include "DrawingObject/Primitive/Rect2D.h"


class CBindingPartner
{
public:
    int iId    = -1;   //接続先のオブジェクトID
    int iIndex = -1;   //接続先オブジェクトの接続Index

    //!< 代入演算子
    virtual CBindingPartner& operator = (const CBindingPartner & m)
    {
        iId = m.iId; 
        iIndex = m.iIndex; 
        return *this;
    }

    //!< 等価演算子
    virtual bool operator == (const CBindingPartner & m) const
    {
        if(iId != m.iId){return false;} 
        if(iIndex != m.iIndex){return false;} 
        return true;
    }

    //!< 等価演算子
    virtual bool operator != (const CBindingPartner & m) const
    {
        return !(*this == m );
    }


    //!< 比較演算子     (setを使用している箇所があるので比較演算子が必要となる)
    bool CBindingPartner::operator > (const CBindingPartner & m) const 
    {
        return !( *this <= m);
    }

    //!< 比較演算子
    bool CBindingPartner::operator < (const CBindingPartner & m) const 
    {
        return !( *this >= m);
    }

    //!< 比較演算子
    bool CBindingPartner::operator >= (const CBindingPartner & m) const 
    {
         if (this->iId > m.iId)
         {
            return true;
         }
         else if (this->iId == m.iId)
         {
             if(this->iIndex > m.iIndex)
             {
                return true;
             }
         }
         else if(*this == m)
         {
            return true;
         }
         return false;
    }

    //!< 比較演算子
    bool CBindingPartner::operator <= (const CBindingPartner & m) const 
    {
         if (this->iId < m.iId)
         {
            return true;
         }
         else if (this->iId == m.iId)
         {
             if(this->iIndex < m.iIndex)
             {
                return true;
             }
         }
         else if(*this == m)
         {
            return true;
         }
         return false;
    }

private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
            SERIALIZATION_BOTH("ObjectId"     , iId);
            SERIALIZATION_BOTH("Index"    , iIndex);
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }
};

/**
 * @class   CNodeData
 * @brief  
 */
class CNodeData 
{
public:
    //!< 接続対象
    enum E_CONNECTION_OBJECT
    {
        NONE,
        FIELD,
        NODE,
        OBJECT,
    };

    enum E_LINE_DIR_TYPE
    {
        E_NONE   = 0,
        E_TOP    =   RECT2D::E_TOP,
        E_RIGHT  =   RECT2D::E_RIGHT,
        E_BOTTOM =   RECT2D::E_BOTTOM,
        E_LEFT   =   RECT2D::E_LEFT,
        E_DIRX   =   RECT2D::E_DIRX,
        E_DIRY   =   RECT2D::E_DIRY,
        E_ALL    =   RECT2D::E_ALL,
        E_NORM_OBJECT_BOUNDS = 0x10, 
        E_CUSTOM  = 0x20, 

    };
public:
    //!< コンストラクタ
    CNodeData();

    CNodeData(const CNodeData & m);

    //!< デストラクタ
    virtual ~CNodeData();

    //!< 代入演算子
    virtual CNodeData& operator = (const CNodeData & m);

    //!< 等価演算子
    virtual bool operator == (const CNodeData & m) const;

    //!< 等価演算子
    virtual bool operator != (const CNodeData & m) const;

    bool IsAbleToConnect(const CNodeData* pC) const;

    bool IsAbleToConnectEndToEnd(const CNodeData* pC) const;

    bool AddDistObject(CDrawingObject* pObj, int iIndex);

    bool DeleteSocket(const CDrawingObject* pObj, int iIndex);
    
    bool DeletePlug(const CDrawingObject* pObj, int iIndex);

    bool DeleteSocket(const CDrawingObject* pObj);
    
    bool DeletePlug(const CDrawingObject* pObj);

    bool Clear();

public:
    //!< 位置
    POINT2D     ptPos;

    //!< 
    int         iIndex;

    //!< 最大被接続数(-1:無制限)
    int         iMaxSocket;

    //!< 最大接続数(通常は１, オブジェクトに対するPINのような場合のみ複数)
    int         iMaxPlug;

    //!< 接続タイプ
    E_ACTIVE_PASSIVE   eConnectionType;

    //!< 接続対象
    E_CONNECTION_OBJECT eConnectionObject;

    //!< 接続方向
    E_IN_OUT            eConnectionDirection;
    
    //!< 接続線方向
    E_LINE_DIR_TYPE     eLineDirection;

    //NodeMarker
    CNodeProperty       nodeProperty;

    //!< 接続プロパティセット名(or プロパティグループ)
    StdString           strConnectionPropertySet;

    // 接続元オブジェクトIDリスト
    std::vector<CBindingPartner>       lstSocket;

    // 接続先オブジェクトIDリスト
    std::vector<CBindingPartner>        lstPlug;

    //マーカスナップタイプ
    DWORD                     dwSnapType;

    //スナップ基準ノード名
    //距離、角度の基準となる点
    StdString                 strSnapOrginNode;


private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
            SERIALIZATION_BOTH("MaxPlugCount"       , iMaxPlug);
            SERIALIZATION_BOTH("ConnectionType"     , eConnectionType);
            SERIALIZATION_BOTH("ConnectionObject"   , eConnectionObject);
            SERIALIZATION_BOTH("ConnectionDirction" , eConnectionDirection);
            SERIALIZATION_BOTH("LineDirction"       , eLineDirection);
            SERIALIZATION_BOTH("Pos"                , ptPos);
            SERIALIZATION_BOTH("Marker"             , nodeProperty);
            SERIALIZATION_BOTH("ConnectionPropertySet"    , strConnectionPropertySet);
            //SERIALIZATION_BOTH("SocketList"    , lstSocket);
            SERIALIZATION_BOTH("PlugList"            , lstPlug);
            SERIALIZATION_BOTH("SnapTypeList"        , dwSnapType);
            SERIALIZATION_BOTH("SnapOrginMarker"     , strSnapOrginNode);

        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }
};

#endif
