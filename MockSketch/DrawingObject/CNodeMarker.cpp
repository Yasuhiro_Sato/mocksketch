/**
 * @brief			CNodeMarker実装ファイル
 * @file			CNodeMarker.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CNodeMarker.h"
#include "./CDrawingObject.h"


#include "DefinitionObject/View/CDrawingView.h"
#include "DrawingObject/Primitive/MAT2D.h"
#include "DrawingObject/CDrawingNode.h"
#include "DrawingObject/CDrawingParts.h"
#include "View/CDrawMarker.h"
#include "View/CLayer.h"
#include "System/CSystem.h"


/**
 * @brief   スクリーン座標取得
 * @param   なし
 * @return	なし
 * @note	 
 */
POINT CMarkerData::GetScreenPos(CDrawingView* pView, 
                                  const CDrawingObject* pObj) const 
{
    //const    MAT2D*  pMat = NULL;
    POINT2D  ptTmp(m_ptPos);
    POINT    ptScr;
    int      iLayerId;

    if (pObj)
    {
        //pMat        = pObj->GetDrawingMatrix();
        iLayerId    = pObj->GetLayer();
    }
    else
    {
        iLayerId = pView->GetCurrentLayerId();
    }
    /*
    if (pMat != NULL)
    {
        ptTmp.Matrix(*pMat);
    }
    */
    
    pView->ConvWorld2Scr(&ptScr, ptTmp, iLayerId);

    return ptScr;
}

/**
 * @brief   ノード位置更新
 * @param   [in] pView
 * @return	なし
 * @note	DrawingNodeの位置をマーカに合わせる
 */
void CMarkerData::UpdateNodePosition()
{
    if (m_pDrawingNode)
    {
        const MAT2D* pMat = m_pDrawingNode->GetParentMatrix();
        if (pMat)
        {
            MAT2D matOrg = *pMat; 
            MAT2D matInv; 

            matInv = matOrg.Inv2();
            POINT2D ptMove = m_ptPos;
            ptMove.Matrix(matInv);
            m_pDrawingNode->SetPoint(ptMove);
        }
    }
}

/**
 * @brief   描画処理
 * @param   [in] pView
 * @return	なし
 * @note	 
 */
void CMarkerData::Draw(CDrawingView* pView, 
        const CDrawingObject* pObj,
        long lRad) const
{
    POINT ptScr = GetScreenPos(pView, pObj);
    int iId = 0;
    if (pObj)
    {
        iId = pObj->GetId();
    }


    COLORREF cr;
    COLORREF cFill;
    int iLineWidth = 1;

    cr    = pView->GetCurrentLayer()->crDefault;
    cFill = _GetColor();

    Draw(pView, ptScr, iId, cr, cFill, iLineWidth, lRad); 
 
}

void CMarkerData::Draw(CDrawingView* pView, 
        POINT pt,
        int   iId,
        COLORREF cr,
        COLORREF cFill,
        int iLineWidth,

        long lRad) const
{
    if (!m_pDrawingNode)
    {
        return;
    }

    if (!m_bVisible)
    {
        return;
    }
#ifdef _DEBUG
    if (pView->GetDebugMode() != CDrawingView::DM_DISABLE)
    {
        DB_PRINT(_T("CMarkerData::Draw pt(%d, %d) \n"), pt.x, pt.x);
    }
#endif

    NODE_MARKER_SHAPE eShape;
    eShape = m_pDrawingNode->GetMarkerShape();
    switch(eShape)
    {
    case DMS_CIRCLE_FILL:
        _DrawCircle(pView, pt, iId, cr, cFill, iLineWidth, lRad, true, false);
        break;

    case DMS_CIRCLE:
        _DrawCircle(pView, pt, iId, cr, cFill, iLineWidth, lRad, false, false);
        break;

    case DMS_RECT_FILL:
        _DrawRect(pView, pt, iId, cr, cFill, iLineWidth, lRad, true, false);
        break;

    case DMS_RECT:
        _DrawRect(pView, pt, iId, cr, cFill, iLineWidth, lRad, false, false);
        break;

    case DMS_DBL_CIRCLE:
        _DrawCircle(pView, pt, iId, cr, cFill, iLineWidth, lRad, true, true);
        break;

    case DMS_DBL_RECT:
        _DrawRect(pView, pt, iId, cr, cFill, iLineWidth, lRad, true, true);
        break;

    case DMS_CIRCLE_FILL_OVER:
        _DrawCircle(pView, pt, iId, cr, cFill, iLineWidth, lRad, true, false);
		_DrawArc(pView, pt, iId, cr, cFill, iLineWidth, lRad * 2);
        break;

    }
}


COLORREF CMarkerData::_GetColor() const
{
    COLORREF crRet;

    if(m_bMouseOver)
    {
        //暫定
        crRet = DRAW_CONFIG->crConnectable;
    }
    else if (m_bConnect)
    {
        crRet = DRAW_CONFIG->crConnectable;
    }
    else
    {
        if (m_bDrag)
        {
            crRet = DRAW_CONFIG->GetSelColor();
        }
        else
        {
            if(m_pDrawingNode)
            {
                crRet = m_pDrawingNode->GetMarkerColor();

            }
            else
            {
                crRet = DRAW_CONFIG->crAdditional;
            }
        }
    }

    return crRet;
}

/**
 * @brief   描画処理
 * @param   [in] pView
 * @param   [in] lRad    半径
 * @param   [in] bFill   塗りつぶし
 * @param   [in] bDouble 二重丸 
 * @return	なし
 * @note	 
 */
void CMarkerData::_DrawCircle(CDrawingView* pView, 
        POINT ptScr,
        int iId,
        COLORREF cr,
        COLORREF crFill,
        int iLineWidth,
        long lRad,
        bool bFill,
        bool bDouble
        ) const
{
   DRAWING_MODE eMode = pView->GetDrawMode();
    if(eMode & DRAW_INDEX)
    {
        int iIdHi; 
        iIdHi  = (iId >> 24);

        CDrawMarker::DrawCircle(pView->GetIndexDc(), 
            ptScr, iId, iId, iLineWidth, lRad, bFill, bDouble);
        CDrawMarker::DrawCircle(pView->GetIndexHiDc(), 
            ptScr, iIdHi, iIdHi, iLineWidth, lRad, bFill, bDouble);
    }

    if(eMode & DRAW_FIG)
    {
        CDrawMarker::DrawCircle(pView->GetBackDc(), 
            ptScr, cr, crFill, iLineWidth, lRad, bFill, bDouble);
    }

    if(eMode & DRAW_SEL)
    {
        CDrawMarker::DrawCircle(pView->GetSelDc(), 
            ptScr, cr, crFill, iLineWidth, lRad, bFill, bDouble);
    }

    if ((eMode & DRAW_FRONT) ||
        (eMode & DRAW_PRINT))
    {
        CDrawMarker::DrawCircle(pView->GetViewDc(), 
            ptScr, cr, crFill, iLineWidth, lRad, bFill, bDouble);
    }
}

/**
 * @brief   描画処理
 * @param   [in] pView
 * @param   [in] lRad    半径
 * @param   [in] bFill   塗りつぶし
 * @param   [in] bDouble 二重丸 
 * @return	なし
 * @note	 
 */
void CMarkerData::_DrawArc(CDrawingView* pView, 
        POINT ptScr,
        int iId,
        COLORREF cr,
        COLORREF crFill,
        int iLineWidth,
        long lRad
        ) const
{

    RECT rcEllipse;
    rcEllipse.left = ptScr.x - lRad;
    rcEllipse.top  = ptScr.y - lRad;
    rcEllipse.right  = ptScr.x + lRad;
    rcEllipse.bottom = ptScr.y + lRad;

    pView->SetPen( PS_SOLID, iLineWidth, cr, iId);

    HBRUSH hBrFill;
    hBrFill = ::CreateSolidBrush(crFill);

    HGDIOBJ hBrFillOld;
    HBRUSH  hBrIndex = ::CreateSolidBrush(iId);

    DRAWING_MODE eMode = pView->GetDrawMode();
    if(eMode & DRAW_INDEX)
    {
         hBrFillOld = ::SelectObject(pView->GetIndexDc(), hBrIndex);
         ::Arc(pView->GetIndexDc(), rcEllipse.left, rcEllipse.top, 
                                    rcEllipse.right, rcEllipse.bottom,
									rcEllipse.right, ptScr.y,
									rcEllipse.left, ptScr.y);
         ::SelectObject(pView->GetIndexDc(), hBrFillOld);
        ::DeleteObject(hBrFill);
    }

    if(eMode & DRAW_FIG)
    {
        hBrFillOld = ::SelectObject(pView->GetBackDc(), hBrFill);
        ::Arc(pView->GetBackDc(), rcEllipse.left, rcEllipse.top, 
                                rcEllipse.right, rcEllipse.bottom,
								rcEllipse.right, ptScr.y,
								rcEllipse.left, ptScr.y);

        ::SelectObject(pView->GetBackDc(), hBrFillOld);
    }

    if(eMode & DRAW_SEL)
    {

        hBrFillOld = ::SelectObject(pView->GetSelDc(), hBrFill);
        ::Arc(pView->GetSelDc(), rcEllipse.left, rcEllipse.top, 
                                rcEllipse.right, rcEllipse.bottom,
								rcEllipse.right, ptScr.y,
								rcEllipse.left, ptScr.y);
        ::SelectObject(pView->GetSelDc(), hBrFillOld);
    }


    if ((eMode & DRAW_FRONT) ||
        (eMode & DRAW_PRINT))
    {
        hBrFillOld = ::SelectObject(pView->GetViewDc(), hBrFill);
        ::Arc(pView->GetViewDc(), rcEllipse.left, rcEllipse.top, 
                                rcEllipse.right, rcEllipse.bottom,
								rcEllipse.right, ptScr.y,
								rcEllipse.left, ptScr.y);

        ::SelectObject(pView->GetViewDc(), hBrFillOld);
    }

    ::DeleteObject(hBrFill);
    ::DeleteObject(hBrIndex);
}
  
void CMarkerData::_DrawRect(CDrawingView* pView, 
        POINT ptScr,
        int iId,
        COLORREF cr,
        COLORREF crFill,
        int iLineWidth,
        long lRad,
        bool bFill,
        bool bDouble
        ) const
{
    DRAWING_MODE eMode = pView->GetDrawMode();
    if(eMode & DRAW_INDEX)
    {
        int iIdHi; 
        iIdHi  = (iId >> 24);

        CDrawMarker::DrawRect(pView->GetIndexDc(), 
            ptScr, iId, iId, iLineWidth, lRad, bFill, bDouble);
        CDrawMarker::DrawRect(pView->GetIndexHiDc(), 
            ptScr, iIdHi, iIdHi, iLineWidth, lRad, bFill, bDouble);
    }

    if(eMode & DRAW_FIG)
    {
        CDrawMarker::DrawRect(pView->GetBackDc(), 
            ptScr, cr, crFill, iLineWidth, lRad, bFill, bDouble);
    }

    if(eMode & DRAW_SEL)
    {
        CDrawMarker::DrawRect(pView->GetSelDc(), 
            ptScr, cr, crFill, iLineWidth, lRad, bFill, bDouble);
    }

    if ((eMode & DRAW_FRONT) ||
        (eMode & DRAW_PRINT))
    {
        CDrawMarker::DrawRect(pView->GetViewDc(), 
            ptScr, cr, crFill, iLineWidth, lRad, bFill, bDouble);
    }
}

//======================================================
//======================================================
//======================================================


/**
 * @brief   コンストラクタ
 * @param   なし         
 * @retval  なし
 * @note	
 */
CNodeMarker::CNodeMarker():
m_lRad      (10),
m_pObject   (NULL),
m_pView     (NULL),
m_eSelButton (SEL_NOUSE_NONE)

{

}

/**
 * @brief   コンストラクタ
 * @param   [in] pParent 
 * @retval  なし
 * @note	
 */
CNodeMarker::CNodeMarker(CDrawingView*  pView):
m_lRad      (10),
m_pObject   (NULL),
m_pView     (pView),
m_eSelButton (SEL_NOUSE_NONE)
{

}

CNodeMarker& CNodeMarker::operator = (const CNodeMarker& rhs)
{

    m_pView = rhs.m_pView;
    m_pObject = rhs.m_pObject;
    m_lRad = rhs.m_lRad;
    m_mapDragableData = rhs.m_mapDragableData;
    m_eSelButton = rhs.m_eSelButton;
    return *this;
}


/**
 * @brief   オブジェクト設定
 * @param   [in] pObject 
 * @retval  なし
 * @note	
 */
void CNodeMarker::SetObject(std::shared_ptr<CDrawingObject> pObject)
{
    Clear();
    m_pObject = pObject;
}


std::shared_ptr<CDrawingObject> CNodeMarker::GetObject()
{
    return m_pObject;
}

/**
 * @brief   オブジェクトID取得
 * @param   なし
 * @retval  オブジェクトID
 * @note	
 */
int CNodeMarker::GetObjectId()
{
    if(m_pObject)
    {
        return m_pObject->GetId();
    }
    return -1;
}


/**
 * @brief   ドラッグ可能マーカー生成
 * @param   [in] strMarker 
 * @param   [in] ptPos 
 * @retval  false 失敗
 * @note	
 */
bool CNodeMarker::Create(CDrawingNode* pNode, POINT2D ptPos)
{
    if (!pNode)
    {
        return false;
    }
    if(m_mapDragableData.find(pNode->GetName()) != m_mapDragableData.end())
    {
        return false;
    }

    CMarkerData data;
    StdString strMarker;
    data.m_ptPos = ptPos;
    data.m_pDrawingNode = pNode;
    if (pNode->HaveConnectionDest())
    {
        data.m_bConnect = true;
    }
    else
    {
        data.m_bConnect = false;
    }
    strMarker = pNode->GetName();
    m_mapDragableData[strMarker] = data;
    return true;
}

bool CNodeMarker::Create(StdString strNode, POINT2D ptPos)
{
    // 削除予定
    if(m_mapDragableData.find(strNode) != m_mapDragableData.end())
    {
        return false;
    }

    CMarkerData data;
    data.m_ptPos = ptPos;
    data.m_pDrawingNode = NULL;
    m_mapDragableData[strNode] = data;
    return true;
}

/**
 * @brief   ドラッグ可能マーカークリア
 * @param   なし
 * @retval  なし
 * @note	
 */
void CNodeMarker::Clear()
{
    m_pFuncCallbackCursor = nullptr;
    boost::recursive_mutex::scoped_lock(mtxGuard);
    m_mapDragableData.clear();
}

/**
 * @brief   マーカーの有無
 * @param   なし
 * @retval  なし
 * @note	
 */
bool CNodeMarker::IsMarkerExist()
{
    return (m_mapDragableData.size() != 0);
}


/**
 * @brief   描画
 * @param   [in] pView 
 * @retval  なし
 * @note	
 */
void CNodeMarker::Draw()
{
    if (m_mapDragableData.size() == 0)
    {
        return ;
    }

    boost::recursive_mutex::scoped_lock(mtxGuard);

    CMarkerData* pData;
    std::map<StdString, CMarkerData>::iterator iteMap;

    std::vector<CMarkerData*> lstPostDraw;

    for(iteMap  = m_mapDragableData.begin();
        iteMap != m_mapDragableData.end();
        iteMap++)
    {
        pData = &(iteMap->second);
        if (pData->m_bMouseOver)
        {
            lstPostDraw.push_back(pData);
        }
        else
        {
            pData->Draw(m_pView, m_pObject.get(), m_lRad);
        }
    }

    for(CMarkerData* pData:lstPostDraw)
    {
        pData->Draw(m_pView, m_pObject.get(), m_lRad);
    }
}

/**
 * @brief   マーカID選択
 * @param   [in] iMarkerId 
 * @retval  なし
 * @note	
 */
void CNodeMarker::Select(StdString strMarker, MOUSE_BUTTON eButton)
{
    std::map<StdString, CMarkerData>::iterator iteMap;

    for(iteMap  = m_mapDragableData.begin();
        iteMap != m_mapDragableData.end();
        iteMap++)
    {
        if(iteMap->first == strMarker)
        {
            iteMap->second.m_bDrag = true;
        }
        else
        {
            iteMap->second.m_bDrag = false;
        }
    }

    m_eSelButton = eButton;
}

/**
 * @brief   選択マーカID取得
 * @param   なし 
 * @retval  選択マーカID
 * @note	
 */
StdString CNodeMarker::GetSelectId()
{
    std::map<StdString, CMarkerData>::iterator iteMap;

    for(iteMap  = m_mapDragableData.begin();
        iteMap != m_mapDragableData.end();
        iteMap++)
    {
        if(iteMap->second.m_bDrag)
        {
            return iteMap->first;
        }
    }
    return _T("");
}

/**
 * @brief   マーカ位置設定
 * @param   [in] iId   マーカID 
 * @param   [in] ptPos  オブジェクト座標 
 * @retval  なし
 * @note	
 */
void CNodeMarker::SetMarkerPos(StdString strMarker, POINT2D& ptPos)
{
    POINT2D ptRet;
    std::map<StdString, CMarkerData>::iterator iteMap;
    iteMap = m_mapDragableData.find(strMarker);

    if (iteMap != m_mapDragableData.end())
    {
        POINT2D  ptTmp = _ConvLocalToView(ptPos);
        iteMap->second.m_ptPos = ptTmp;
    }
}

void CNodeMarker::SetMarkerPosS(std::string strMarker, POINT2D& ptPos)
{
    StdString strOut =  CUtil::CharToString(strMarker.c_str());
    SetMarkerPos(strOut, ptPos);
}


/**
 * @brief   マーカ位置設定(ワールド座標系)
 * @param   [in] iId   マーカID 
 * @param   [in] ptPos 設定位置D 
 * @retval  なし
 * @note	
 */
void CNodeMarker::SetMarkerPosView(StdString strMarker, POINT2D& ptPos)
{
    std::map<StdString, CMarkerData>::iterator iteMap;
    iteMap = m_mapDragableData.find(strMarker);

    if (iteMap != m_mapDragableData.end())
    {
#if 0
DB_PRINT(_T("CNodeMarker::SetMarkerPosView (%s) ptPos(%s) --\n"), strMarker.c_str(), ptPos.Str().c_str());
#endif
        iteMap->second.m_ptPos = ptPos;
    }
    else
    {
#if 0
   DB_PRINT(_T("CNodeMarker::SetMarkerPosView fail(%s) --\n"), strMarker.c_str());
#endif
    }
}
void CNodeMarker::SetMarkerPosViewS(std::string strMarker, POINT2D& ptPos)
{
    StdString strOut =  CUtil::CharToString(strMarker.c_str());
    SetMarkerPos(strOut, ptPos);
}

/**
 * @brief   マーカ座標変換
 * @param   [in] pt   ローカル座標 
 * @retval  ワールド座標
 * @note	
 */
POINT2D CNodeMarker::_ConvLocalToView(const POINT2D& pt)const
{
    POINT2D ptRet = pt;

    const MAT2D*   pMat = NULL;
    if (m_pObject)
    {
        if((m_pObject->GetType() == DT_PARTS)||
           (m_pObject->GetType() == DT_FIELD))
        {
            auto pGroup = std::dynamic_pointer_cast<CDrawingParts>(m_pObject);
            STD_ASSERT(pGroup);
            if (pGroup)
            {
                pMat = pGroup->GetDrawingMatrix();
            }
        }
    }

    if (pMat != NULL)
    {
        ptRet.Matrix(*pMat);
    }
    return ptRet;
}


POINT2D CNodeMarker::_ConvViewToLocal(const POINT2D& pt) const
{
    POINT2D ptRet = pt;

    const MAT2D*   pMat = NULL;
    MAT2D   matInv;
    if (m_pObject)
    {
        if((m_pObject->GetType() == DT_PARTS)||
           (m_pObject->GetType() == DT_FIELD))
        {
            auto pGroup = std::dynamic_pointer_cast<CDrawingParts>(m_pObject);
            STD_ASSERT(pGroup);
            if (pGroup)
            {
                pMat = pGroup->GetDrawingMatrix();
            }
        }
    }

    if (pMat != NULL)
    {
        matInv = pMat->Invert();
        ptRet.Matrix(matInv);
    }
    return ptRet;
}

/**
 * @brief   マーカ位置取得
 * @param   [in] strMarker   マーカID 
 * @retval  マーカ位置
 * @note	
 */
POINT2D& CNodeMarker::GetMarkerPos(StdString strMarker) const
{
    static POINT2D ptRet;
    std::map<StdString, CMarkerData>::const_iterator iteMap;
    iteMap = m_mapDragableData.find(strMarker);
    if (iteMap != m_mapDragableData.end())
    { 
       iteMap->second.m_ptPosLocal = _ConvViewToLocal(iteMap->second.m_ptPos);
       return iteMap->second.m_ptPosLocal;
    }
    return ptRet;
}
POINT2D& CNodeMarker::GetMarkerPosS(std::string strMarker) const
{
    StdString strOut =  CUtil::CharToString(strMarker.c_str());
    return GetMarkerPos(strOut);
}

/**
 * @brief   マーカ位置取得(ワールド座標系)
 * @param   [in] strMarker   マーカID 
 * @param   [in] ptPos 設定位置D 
 * @retval  なし
 * @note	
 */
POINT2D& CNodeMarker::GetMarkerPosView(StdString strMarker) const
{
    static POINT2D ptRet;
    std::map<StdString, CMarkerData>::const_iterator iteMap;
    iteMap = m_mapDragableData.find(strMarker);
    if (iteMap != m_mapDragableData.end())
    {
        ptRet = (iteMap->second.m_ptPos);
    }
    return ptRet;
}
POINT2D& CNodeMarker::GetMarkerPosViewS(std::string strMarker) const
{
    StdString strOut =  CUtil::CharToString(strMarker.c_str());
    return GetMarkerPos(strOut);
}

/**
 * @brief   マーカ表示切替
 * @param   [in] strMarker   マーカID 
 * @param   [in] ptPos 設定位置D 
 * @retval  なし
 * @note	空文字設定時はすべてを変更する
 */
void CNodeMarker::SetVisible(StdString strMarker, bool bVisible)
{
    std::map<StdString, CMarkerData>::iterator iteMap;

    if (strMarker != _T(""))
    {
        iteMap = m_mapDragableData.find(strMarker);
        if (iteMap != m_mapDragableData.end())
        {
            iteMap->second.m_bVisible = bVisible;
        }
    }
    else
    {
        for(iteMap = m_mapDragableData.begin();
            iteMap != m_mapDragableData.end();
            iteMap++)
        {
            iteMap->second.m_bVisible = bVisible;
        }

    }
}

//!<マーカ表示切替
void CNodeMarker::SetVisibleOnly(StdString strMarker, bool bVisible)
{
    std::map<StdString, CMarkerData>::iterator iteMap;
    for(iteMap = m_mapDragableData.begin();
        iteMap != m_mapDragableData.end();
        iteMap++)
    {
        if (iteMap->first == strMarker)
        {
            iteMap->second.m_bVisible = bVisible;
        }
        else
        {
            iteMap->second.m_bVisible = (!bVisible);
        }
    }

}


void CNodeMarker::SetVisibleS(std::string strMarker, bool bVisible)
{
    StdString strOut =  CUtil::CharToString(strMarker.c_str());
    SetVisible(strOut, bVisible);
}


/**
 * @brief   ノードデータの取得
 * @param   [in] strMarker マーカーID
 * @retval  CMarkerDataへのポインタ
 * @note	
 */
CMarkerData* CNodeMarker::GetMarker(StdString strMarker)
{
     std::map<StdString, CMarkerData>::iterator iteMap;
     iteMap = m_mapDragableData.find(strMarker);

     if (iteMap != m_mapDragableData.end())
     {
         return &iteMap->second;

     }
     return NULL;
}

/**
 * @brief   ノードの取得
 * @param   [in] strMarker マーカーID
 * @retval  DrawingNodeへのポインタ
 * @note	
 */
CDrawingNode* CNodeMarker::GetDrawingNode(StdString strMarker)
{

    CMarkerData* pNodeData = CNodeMarker::GetMarker(strMarker);

    if (pNodeData)
    {
        return pNodeData->GetDrawingNode();
    }

    return NULL;
}

bool CNodeMarker::ChangeName(StdString strOld, StdString strNew)
{
    static POINT2D ptRet;
    auto iteMap = m_mapDragableData.find(strOld);
    if (iteMap == m_mapDragableData.end())
    {
        return false;
    }

    auto iteMapNew = m_mapDragableData.find(strNew);
    if (iteMapNew != m_mapDragableData.end())
    {
        return false;
    }

    CDrawingNode* pNode = iteMap->second.m_pDrawingNode;
    pNode->SetNameStr(strNew);

    m_mapDragableData[strNew] = iteMap->second;   

    iteMap = m_mapDragableData.find(strOld);
    if (iteMap != m_mapDragableData.end())
    {
        m_mapDragableData.erase(iteMap);
    }

    return true;
}



/**
 * @brief   移動
 * @param   [in] ptPos    画面位置
 * @param   [in] strMarker 
 * @retval  移動後の座標(オブジェクト座標系)
 * @note	
 */
POINT2D CNodeMarker::Move(MOUSE_MOVE_POS posMouse, StdString strMarker)
{
    const MAT2D*  pMat;
    MAT2D   matInv;
    POINT2D  ptTmp;
    int      iLayerId;

    pMat        = m_pObject->GetParentMatrix();
    iLayerId    = m_pObject->GetLayer();



    m_pView->ConvScr2World(&ptTmp, posMouse.ptSel, iLayerId);

    



    
    m_mapDragableData[strMarker].m_ptPos = ptTmp;
    return ptTmp;
}


/**
 * @brief   マーカID取得
 * @param   [in] pView 
 * @param   [in] ptPos 
 * @retval  座標上のマーカID (-1:選択なし)
 * @note	
 */
StdString CNodeMarker::GetMarkerId(POINT ptPos)
{
    StdString strRet = _T("");
    if (m_mapDragableData.size() == 0)
    {
        return strRet;
    }


    CMarkerData* pData;
    //一番近い距離のものを探す
    std::map<StdString, CMarkerData>::iterator iteMap;

    POINT ptObj;
    double dLengthMin = DBL_MAX; 
    double dLength; 
    int    tmpX;
    int    tmpY;

    for(iteMap  = m_mapDragableData.begin();
        iteMap != m_mapDragableData.end();
        iteMap++)
    {
        pData = &(iteMap->second);
        ptObj = pData->GetScreenPos(m_pView, m_pObject.get());

        tmpX = (ptObj.x - ptPos.x);
        tmpY = (ptObj.y - ptPos.y);

        dLength = (tmpX * tmpX + tmpY * tmpY); 

        if (dLengthMin > dLength)
        {
            dLengthMin = dLength;
            strRet = iteMap->first;
        }
    }

    if(m_lRad < sqrt(dLengthMin))
    {
        return _T("");
    }

    return strRet;
}

/**
 * @brief   マーカID開放
 * @param   なし
 * @retval  なし
 * @note	
 */
void CNodeMarker::Release()
{
    m_eSelButton = SEL_NOUSE_NONE;
    std::map<StdString, CMarkerData>::iterator iteMap;
    for(iteMap  = m_mapDragableData.begin();
        iteMap != m_mapDragableData.end();
        iteMap++)
    {
        (iteMap->second).m_bDrag = false;
    }
}


//!< マウスオーバー
bool CNodeMarker::MouseOver(POINT ptPos)
{
    StdString strId;
    bool bRet = false;
    strId = GetMarkerId(ptPos);

    if (m_strMouseOver != _T(""))
    {
        CMarkerData* pNode = GetMarker(m_strMouseOver);
        if (pNode)
        {
            pNode->m_bMouseOver = false;
        }
    }

    CMarkerData* pNode = GetMarker(strId);
    if (pNode)
    {
        pNode->m_bMouseOver = true;
        bRet = true;

    }
    m_strMouseOver = strId;
    return bRet;
}

StdString CNodeMarker::GetMouseOver() const
{
    StdString strRet;
    CMarkerData* pData;
    for (auto ite: m_mapDragableData)
    {
        pData = &(ite.second);
        if (pData->m_bMouseOver)
        {
            strRet = ite.first;
        }
    }
    return strRet;
}

bool CNodeMarker::SetOnSetcursorCallback(std::function<bool (CNodeMarker*, CDrawingView*) > func)
{
    m_pFuncCallbackCursor = func;
    return true;
}

bool CNodeMarker::OnSetCursor()
{

	if (m_pFuncCallbackCursor)
	{
		return m_pFuncCallbackCursor(this, m_pView);
	}
	return false;
}

//!< コンテキストメニュー表示
void CNodeMarker::OnContextMenu(CWnd* pWnd, POINT ptSel, StdString strMarker)
{
    /*
    std::map<StdString, CMarkerData>::const_iterator iteMap;
    iteMap = m_mapDragableData.find(strMarker);
    if (iteMap == m_mapDragableData.end())
    {
        return;
    }

    CNodeData::E_CONNECTION_OBJECT eObj;
    CDrawingNode* pMarker;

    pMarker = (iteMap->second).m_pDrawingNode;

    if (pMarker == NULL)
    {
        return;
    }
    eObj = pMarker->GetConnectionObjectType();

    if (eObj != CDrawingNode::OBJECT)
    {
        return;
    }

    CMenu menu;
    menu.CreatePopupMenu();

    UINT nPopup = MF_STRING | MF_ENABLED | MF_POPUP ;
    UINT nFlagMove = MF_STRING | MF_ENABLED;

    //TODO:暫定処理
    menu.AppendMenu(nPopup, 1, GET_STR(STR_PRO_UNIT_1));
    menu.AppendMenu(nPopup, 2, GET_STR(STR_PRO_UNIT_2));
    menu.AppendMenu(nPopup, 3, GET_STR(STR_PRO_UNIT_3));
 

                
    UINT uiMenu;
    uiMenu = menu.TrackPopupMenu(
        TPM_RETURNCMD  |    // メニュー項目を選択しても通知メッセージを送らない
        TPM_NONOTIFY   |    // 関数の戻り値としてメニューＩＤを返す
        TPM_LEFTALIGN  |    // クリック時のX座標をメニューの左辺にする
        TPM_RIGHTBUTTON,    // 右クリックでメニュー選択可能とする

        ptSel.x, 
        ptSel.y, pWnd);


    menu.DestroyMenu();

    if (uiMenu == 1)
    {
    }
    else if (uiMenu == 2)
    {
    }
    //-----------------------------------------------
    */
}

//-----------------------------------------------------