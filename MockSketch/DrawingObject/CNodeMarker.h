/**
 * @brief			CNodeMarkerヘッダーファイル
 * @file			CNodeMarker.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#if !defined(_NODE_MARKER_H_)
#define _NODE_MARKER_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DrawingObject/Primitive/POINT2D.h"
#include "DrawingObject/BaseObj.h"
#include "COMMON_HEAD.h"


/*---------------------------------------------------*/
/*  classes                                          */
/*---------------------------------------------------*/
class CDrawingView;
class CDrawingObject;
class POINT2D;
class CDrawingNode;

enum NODE_MARKER_MOVE
{
    DMT_NOMOVE             = 0,  //!< 移動なし
    DMT_ALL_MOVE           = 1,  //!< 全方向移動可能
    DMT_LIMITED_DIRECTION  = 2,  //!< 1方向移動可能
    DMT_LIMITED_DISTANCE   = 3,  //!< 距離固定(回転角設定時に使用することを想定)
};

enum NODE_MARKER_SHAPE
{
    DMS_CIRCLE_FILL  = 0,   //!< 円塗りつぶし       
    DMS_CIRCLE       = 1,   //!< 円                 
    DMS_RECT_FILL    = 2,   //!< 四角塗りつぶし     
    DMS_RECT         = 3,   //!< 四角               
    DMS_DBL_CIRCLE   = 4,   //!< 二重丸             
    DMS_DBL_RECT     = 5,   //!< 二重角  
    DMS_CIRCLE_FILL_OVER  = 6,   //!< 円塗りつぶし+上部線       
};

enum MOUSE_BUTTON
{
    SEL_NOUSE_NONE,
    SEL_RBUTTON,
    SEL_LBUTTON,
    SEL_MBUTTON,
};

enum DISP_TYPE
{
    NDT_SHOW_ALWAYS,       // 常にマーカーを表示する
    NDT_SHOW_NODE_OPERATION,    // ノード操作状態のみ表示
};

/**
 * @class   CNodeProperty
 * @brief   CDrawingNOde用                        
 */
class CNodeProperty
{
public:

    //!<Nodeマーカー移動制限
    NODE_MARKER_MOVE        m_eMove;

    //!<Nodeマーカー形状
    NODE_MARKER_SHAPE       m_eShape;

    //!<Nodeマーカー色
    COLORREF                m_cr;

    //!<Nodeマーカー座標系
    E_LOCAL_WORLD           m_eCordinateSystem;

    //!<Nodeマーカー移動方向
    POINT2D                 m_ptDir;

    //!<Nodeマーカー表示形式
    DISP_TYPE               m_eDispType;

    CNodeProperty():m_eMove (DMT_ALL_MOVE),
                    m_eShape(DMS_CIRCLE),
                    m_cr(RGB( 0,255,255)),
                    m_eCordinateSystem(E_LOCAL) ,
                    m_eDispType(NDT_SHOW_NODE_OPERATION)
                    {;}

    CNodeProperty& operator = (const CNodeProperty & m)
    {
        m_eMove             = m.m_eMove;
        m_eShape            = m.m_eShape;
        m_cr                = m.m_cr;
        m_eCordinateSystem  = m.m_eCordinateSystem;
        m_ptDir             = m.m_ptDir;
        m_eDispType         = m.m_eDispType;

        return *this;
    }

    //!< 等価演算子
    bool operator == (const CNodeProperty & m) const
    {
        if(m_eMove             != m.m_eMove){return false;}
        if(m_eShape            != m.m_eShape){return false;}
        if(m_cr                != m.m_cr){return false;}
        if(m_eCordinateSystem  != m.m_eCordinateSystem){return false;}
        if(m_ptDir             != m.m_ptDir){return false;}
        if(m_eDispType         != m.m_eDispType){return false;}

        return true;
    }

    bool operator != (const CNodeProperty & m) const
    {
        return !(*this == m );
    }

private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
            SERIALIZATION_BOTH("MoveType"           , m_eMove);
            SERIALIZATION_BOTH("Shape"              , m_eShape);
            SERIALIZATION_BOTH("Color"              , m_cr);
            SERIALIZATION_BOTH("CordinateSystem"    , m_eCordinateSystem);
            SERIALIZATION_BOTH("Direction"          , m_ptDir);
            SERIALIZATION_BOTH("DispType"           , m_eDispType);
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }

};

BOOST_CLASS_VERSION(CNodeProperty, 0);


/**
 * @class   CMarkerData
 * @brief                        
 */
class CMarkerData
{
public:
    //!< マーカー位置
    POINT2D                 m_ptPos;

    //!< マーカー位置(ローカル位置保存用)
    mutable POINT2D         m_ptPosLocal;

    //!< ドラッグ中フラグ
    bool                    m_bDrag;

    //!< マウスオーバー
    bool                    m_bMouseOver
        ;
    //!< 接続フラグ
    bool                    m_bConnect;

    //表示非表示切り替え
    bool                    m_bVisible;

    CDrawingNode*           m_pDrawingNode;

public:
    CMarkerData():    m_bDrag (false),
                    m_pDrawingNode(NULL),
                    m_bVisible(true),
                    m_bConnect(false),
                    m_bMouseOver(false)
                    {;}


    virtual ~CMarkerData(){;}

    CMarkerData& operator = (const CMarkerData& rhs)
    {
        m_ptPos = rhs.m_ptPos;
        m_bDrag = rhs.m_bDrag;
        m_bConnect = rhs.m_bConnect;
        m_bVisible = rhs.m_bVisible;
        m_pDrawingNode = rhs.m_pDrawingNode;
        m_bMouseOver = false;
        return *this;
    }

    void Draw(CDrawingView* pView, 
              const CDrawingObject* pObj,
              long lRad) const;

 
    void Draw(CDrawingView* pView, 
            POINT pt,
            int   iId,
            COLORREF cr,
            COLORREF cFill,
            int iLineWidth,
            long lRad) const;


    POINT GetScreenPos(CDrawingView* pView, 
                       const CDrawingObject* pObj) const ;

    POINT2D GetPoint() const {return m_ptPos;}

    void UpdateNodePosition();

    CDrawingNode* GetDrawingNode()
    {
        return m_pDrawingNode;
    }

protected:
    COLORREF _GetColor() const;

    void _DrawCircle(CDrawingView* pView, 
        POINT ptScr,
        int iId,
        COLORREF cr,
        COLORREF crFill,
        int iLineWidth,
        long lRad,
        bool bFill,
        bool bDouble
        ) const;

    void _DrawArc(CDrawingView* pView, 
        POINT ptScr,
        int iId,
        COLORREF cr,
        COLORREF crFill,
        int iLineWidth,

        long lRad
        ) const;

    void _DrawRect(CDrawingView* pView, 
        POINT ptScr,
        int iId,
        COLORREF cr,
        COLORREF crBack,
        int iLineWidth,
        long lRad,
        bool bFill,
        bool bDouble
        ) const;

};

/**
 * @class   CDrawMarker
 * @brief                        
 */
class CNodeMarker  :public CBaseObj
{
    
public:
    CNodeMarker(CDrawingView*  pView);

    virtual ~CNodeMarker(){;}

    CNodeMarker& operator=(const CNodeMarker& rhs);

    void SetRadius(long lRad){m_lRad = lRad;}

    //!< オブジェクト設定
    void SetObject(std::shared_ptr<CDrawingObject>  pParent);

    //!< マーカー生成
    bool Create(CDrawingNode* pNode, POINT2D ptPos);

    //!< マーカー生成
    bool Create(StdString strNode, POINT2D ptPos);

    //!< マウスオーバー
    bool MouseOver(POINT ptPos);


    //!< 削除
    void Clear();

    bool IsMarkerExist();

    void Draw();

    StdString GetSelectId();

    //!< オブジェクトID取得
    int GetObjectId();

    std::shared_ptr<CDrawingObject> GetObject();

    StdString GetMarkerId(POINT ptPos);

    //!<マーカ位置設定(AS)
    void SetMarkerPos(StdString strMarker, POINT2D& ptPos);
    void SetMarkerPosS(std::string strMarker, POINT2D& ptPos);

    //!<マーカ位置設定(View座標系)
    void SetMarkerPosView(StdString strMarker, POINT2D& ptPos);
    void SetMarkerPosViewS(std::string strMarker, POINT2D& ptPos);

    //!<マーカ位置取得(AS)
    POINT2D& GetMarkerPos(StdString strMarker) const;
    POINT2D& GetMarkerPosS(std::string strMarker) const;

    //!<マーカ位置取得(View座標系)
    POINT2D& GetMarkerPosView(StdString strMarker) const;
    POINT2D& GetMarkerPosViewS(std::string strMarker) const;

    //!<マーカ表示切替
    void SetVisible(StdString strMarker, bool bVisible);
    void SetVisibleS(std::string strMarker, bool bVisible);

    //!<マーカ表示切替
    void SetVisibleOnly(StdString strMarker, bool bVisible);

    //!< マーカ取得
    CMarkerData* GetMarker(StdString strMarker);

    CDrawingNode* GetDrawingNode(StdString strMarker); 

    bool ChangeName(StdString strOld, StdString strNew); 

    void Select(StdString strMarker, MOUSE_BUTTON eButton);

    MOUSE_BUTTON GetMouseButton(){return m_eSelButton;}

    StdString GetMouseOver() const;

    POINT2D Move(MOUSE_MOVE_POS posMouse, StdString strMarker);

    //!< コンテキストメニュー表示
    void OnContextMenu(CWnd* pWnd, POINT ptSel, StdString strMarOnContextMenuer);

	//マウスカーソル設定コールバック
	bool SetOnSetcursorCallback(std::function<bool (CNodeMarker*, CDrawingView*) > func);

	bool OnSetCursor();

    void Release();

    CDrawingView* GetView(){return m_pView;}

protected:
    CNodeMarker();

    POINT2D _ConvLocalToView(const POINT2D& pt) const;
    POINT2D _ConvViewToLocal(const POINT2D& pt) const;


protected:
    mutable boost::mutex     mtxGuard;

    CDrawingView*       m_pView;

    std::shared_ptr<CDrawingObject>     m_pObject;

    long                m_lRad;

    StdString           m_strMouseOver;

    MOUSE_BUTTON        m_eSelButton;

	std::function<bool (CNodeMarker*, CDrawingView*) >  m_pFuncCallbackCursor;

    std::map<StdString, CMarkerData> m_mapDragableData;

};



#endif // _NODE_MARKER_H_
