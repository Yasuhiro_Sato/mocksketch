/**
 * @brief       MAT2D実装ファイル
 * @file        MAT2D.cpp
 * @author      Yasuhiro Sato
 * @date        01-2-2009 10:36:26
 * 
 * @note
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks    2次元描画用マトリックス
 *
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./MAT2D.h"
#include "./POINT2D.h"
#include "./LINE2D.h"
#include "Utility/CUtility.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

MAT2D::MAT2D():
NUM_MATRIX(3,3, 0.0)
{
    //初期値は単位行列
    SetIdentity();
//    m_pMat = this;
}
 
MAT2D::~MAT2D()
{

}


/*
    行列に関しての注意事項
    POINT2D = POINT2D * MAT2D
    POINT2Dは列ベクトルとみなす。
    ※ MAT2D * POINT2D の演算は定義しない

    従って、一般的によく見られる以下の形式は

    X’ = |m11 m12 m13 | * X 
    Y'    |m21 m22 m23 |   Y
    1     |m31 m32 m33 |   1    


   [X' Y' 1] = [X Y 1]   |a11 a12 a13 | 
                         |a21 a22 a23 | 
                         |a31 a32 a33 | 

   と表される  係数をｍとすると
   [X' Y' 1] = [X Y 1]   |m11 m21 m31 | 
                         |m12 m22 m32 | 
                         |m13 m23 m33 | 

   になる


*/


void MAT2D::SetAll(double d11, double d12, double d13,
                   double d21, double d22, double d23,
                   double d31, double d32, double d33)
{
    (*this)(0,0) =  d11;
    (*this)(0,1) =  d12;
    (*this)(0,2) =  d13;
    (*this)(1,0) =  d21;
    (*this)(1,1) =  d22;
    (*this)(1,2) =  d23;
    (*this)(2,0) =  d31;
    (*this)(2,1) =  d32;
    (*this)(2,2) =  d33;
}

//!< 絶対移動
void MAT2D::Move(const IPoint2D& pt)
{
    SetIdentity();
    (*this)(2,0) = pt.GetX();
    (*this)(2,1) = pt.GetY();
}

//!< 回転 (deg)
void MAT2D::Rotate(double dAngle)
{
    double dC;
    double dS;
    CUtil::GetTriFunc(dAngle, &dS, &dC);

    (*this)(0,0) =  dC;
    (*this)(0,1) =  dS;
    (*this)(0,2) =  0.0;
    (*this)(1,0) =  -dS;
    (*this)(1,1) =  dC;
    (*this)(1,2) =  0.0;
    (*this)(2,2) =  0.0;
    (*this)(2,2) =  0.0;
    (*this)(2,2) =  1.0;
}

//!< 回転 (deg)
void MAT2D::Rotate(const IPoint2D& pt, double dAngle)
{
    MAT2D matA;
    MAT2D matB;
    MAT2D matC;
    //MAT2D matD;

    POINT2D ptSub;
    pt.Minus(&ptSub);
    matA.Move(ptSub);
    matB.Rotate(dAngle);
    matC.Move( pt);

    //(NUM_MATRIX<double>)(*this)  = matA * matB * matC;
    *this  = matA * matB * matC;
}

//!< 鏡像
void MAT2D::Mirror(const ILine2D& line)
{
    const LINE2D* pLine = dynamic_cast<const LINE2D*>(&line);

    STD_ASSERT(pLine != NULL);

    double dL = pLine->Length();
    if (dL < NEAR_ZERO)
    {
        SetIdentity();
    }

    double dDeltaX = pLine->Pt(1).dX - pLine->Pt(0).dX;
    double dDeltaY = pLine->Pt(1).dY - pLine->Pt(0).dY;

    double dC = dDeltaX / dL;
    double dS = dDeltaY / dL;

    double dS2 = 2.0 * dC * dS;
    double dC2 = dC * dC - dS * dS;
    
    POINT2D ptOrg;
    POINT2D ptCross;
    ptCross = pLine->NearPoint(ptOrg);

    MAT2D  matRot;
    MAT2D  matA;
    MAT2D  matB;

    matA.Move(-ptCross);
    matB.Move( ptCross);

    matRot(0,0) =  dC2;
    matRot(0,1) =  dS2;
    matRot(0,2) =  0.0;
    matRot(1,0) =  dS2;
    matRot(1,1) = -dC2;
    matRot(1,2) =  0.0;
    matRot(2,2) =  0.0;
    matRot(2,2) =  0.0;
    matRot(2,2) =  1.0;

    *this =  matA * matRot * matB;
}


//!< スケール
void MAT2D::Scl( double dSclX, double dSclY)
{
    (*this)(0,0) =  dSclX;
    (*this)(0,1) =  0.0;
    (*this)(0,2) =  0.0;
    (*this)(1,0) =  0.0;
    (*this)(1,1) =  dSclY;
    (*this)(1,2) =  0.0;
    (*this)(2,2) =  0.0;
    (*this)(2,2) =  0.0;
    (*this)(2,2) =  1.0;
}

//!< スケール
void MAT2D::Scl(const IPoint2D& pt, double dSclX, double dSclY)
{
    MAT2D  matScl;
    MAT2D  matA;
    MAT2D  matB;

    POINT2D ptSub;

    matScl.Scl(dSclX, dSclY);
 
    pt.Minus(&ptSub);
    matA.Move(ptSub);
    matB.Move( pt);


    *this = matA * matScl * matB;
} 

/**
*  @brief  スケール.
*  @param  [in] pPt         拡大中心
*  @param  [in] dAxisXAngle X軸方向
*  @param  [in] pSclX X倍率
*  @param  [in] pSclY Y倍率
*  @param  [in] pAngle 角度
*  @retval なし
*  @note   
*/
void MAT2D::Scl(const IPoint2D& pt, double dAxisXAngle, double dSclX, double dSclY)
{
    MAT2D mM1;
    MAT2D mR1;
    MAT2D mS1;
    MAT2D mR2;
    MAT2D mM2;


    mM1.Move(POINT2D(-pt.GetX(), -pt.GetY()));
    mR1.Rotate(-dAxisXAngle);
    mS1.Scl(dSclX, dSclY);
    mR2.Rotate(dAxisXAngle);
    mM2.Move(pt);

    *this = mM1*mR1*mS1*mR2*mM2;
}


/**
 *  @brief  アファイン変換パラメータ取得.
 *  @param  [out] pSclX X倍率
 *  @param  [out] pSclY Y倍率
 *  @param  [out] pAngle 角度
 *  @param  [out] pPt    位置
 *  @retval なし
 *  @note   
 */
void MAT2D::GetAffineParam(POINT2D* pPt, double* pAngle, double* pSclX, 
                          double* pSclY, double* pT) const
{
    //QR分解参照
    double d00 = Val(0,0);
    double d01 = Val(0,1);
    double d10 = Val(1,0);
    double d11 = Val(1,1);

    pPt->SetX(Val(2,0));
    pPt->SetY(Val(2,1));

    double dSclX;
    double dSclY  = 0.0;
    double dS     = 0.0;
    double dC     = 1.0;
    double dT     = 0.0;
    
    dSclX = sqrt(d00 * d00 + d01 * d01);

    if (dSclX > NEAR_ZERO)
    {
        dC = d00 / dSclX;
        dS = d01 / dSclX;
        dSclY = dC * d11 - dS * d10;
        dT = (dC * d10 + dS * d11) / dSclX;
    }

    *pSclX = dSclX;
    *pSclY = dSclY;
    *pT = dT;
    *pAngle = CUtil::NormAngle(atan2(dS, dC) * RAD2DEG);
}

/**
 *  @brief  アファイン変換パラメータ設定.
 *  @param  [in] pt     位置
 *  @param  [in] dAngle 角度
 *  @param  [in] dSclX  X倍率
 *  @param  [in] dSclY  Y倍率
 *  @retval なし
 *  @note   
 */
void MAT2D::SetAffineParam(POINT2D  pt, double dAngle, double dSclX, double dSclY, double dT)
{
    (*this)(2,0) = pt.GetX();
    (*this)(2,1) = pt.GetY();
    (*this)(2,2) = 1.0;

    double dC;
    double dS;
    CUtil::GetTriFunc(dAngle, &dS, &dC);

    (*this)(0,0) =  dC * dSclX;
    (*this)(0,1) =  dS * dSclX;
    (*this)(1,0) =  -dS * dSclY + dC * dSclX * dT;
    (*this)(1,1) =   dC * dSclY + dS * dSclX * dT;
}

/**
 *  @brief  アファイン変換位置パラメータ設定.
 *  @param  [in] pt     位置
 *  @retval なし
 *  @note   
 */
void MAT2D::SetAffinePos(POINT2D  pt)
{
    (*this)(2,0) = pt.GetX();
    (*this)(2,1) = pt.GetY();

}

/**
 *  @brief  アファイン変換角度パラメータ設定.
 *  @param  [in] dAngle 角度
 *  @param  [in] dAngleOrg 元の角度
 *  @retval なし
 *  @note   
 */
void MAT2D::SetAffineAngle(double dAngle)
{
    double dSclX;
    double dSclY;
    double dAngleOld;
    POINT2D pt;
    double dT     = 0.0;

    GetAffineParam(&pt, &dAngleOld, &dSclX, &dSclY, &dT);
    SetAffineParam(pt, dAngle, dSclX, dSclY, dT);
  
}

void MAT2D::SetAffineSkew(double dT)
{
    /*
    dT  tan(X軸固定の歪角)

      ↓ ここの角度
  Y |    ______________
    |   /             /
    |  /             /
    | /             /
    |/             /
    +-----------------
    X
    */


    double dSclX;
    double dSclY;
    double dAngle;
    POINT2D pt;
    double dTOld;

    GetAffineParam(&pt, &dAngle, &dSclX, &dSclY, &dTOld);
    SetAffineParam(pt, dAngle, dSclX, dSclY, dT);

}

void MAT2D::SetAffineScl(double dSclX, double dSclY)
{
    double dSclXOld;
    double dSclYOld;
    double dAngle;
    POINT2D pt;
    double dT     = 0.0;

    GetAffineParam(&pt, &dAngle, &dSclXOld, &dSclYOld, &dT);
    SetAffineParam(pt, dAngle, dSclX, dSclY, dT);
}



void MAT2D::MultiAffine(double dM)
{

	double dSclX;
	double dSclY;
	double dAngle;
	POINT2D pt;
	double dT;

	GetAffineParam(&pt, &dAngle, &dSclX, &dSclY, &dT);


	if (pt.Length() > NEAR_ZERO)
	{
		pt = pt * dM;
	}

	if (fabs(dAngle) > NEAR_ZERO)
	{
		dAngle = dAngle * dM;
	}
	
	if (fabs(dSclX - 1.0) > NEAR_ZERO)
	{
		dSclX = dSclX * dM;
	}

	if (fabs(dSclY - 1.0) > NEAR_ZERO)
	{
		dSclY = dSclY * dM;
	}

	SetAffineParam(pt, dAngle, dSclX, dSclY, dT);
}

//アフィン逆変換点取得
POINT2D MAT2D::GetRevAffinePoint(POINT2D  pt) const
{
   double a =  Val(0,0);
   double b =  Val(0,1);
   double c =  Val(1,0);
   double d =  Val(1,1);
   double e =  Val(2,0);
   double f =  Val(2,1);


   double det = a * d - c * b;
   if (fabs(det) < NEAR_ZERO)
   {
       return pt;
   }

   double aa =  d ;
   double bb = -b ;
   double cc = -c ;
   double dd =  a ;
   double ee = (-d * e + c * f) ;
   double ff = ( b * e - a * f) ;

   POINT2D ptRet;

   ptRet.dX = (aa * pt.dX + cc * pt.dY + ee) / det;
   ptRet.dY = (bb * pt.dX + dd * pt.dY + ff) / det;

   return ptRet;

}


bool MAT2D::Inv2D()
{
    double a11 = Elem(0, 0);
    double a12 = Elem(0, 1);
    double a13 = Elem(0, 2);
    double a21 = Elem(1, 0);
    double a22 = Elem(1, 1);
    double a23 = Elem(1, 2);
    double a31 = Elem(2, 0);
    double a32 = Elem(2, 1);
    double a33 = Elem(2, 2);

    double det = a11*a22*a33
                +a21*a32*a13
                +a31*a12*a23
                -a11*a32*a23
                -a31*a22*a13
                -a21*a12*a33;
    if (fabs(det) < NEAR_ZERO)
    {
        return false;
    }

    *ElemDirect(0,0) =   (a22*a33-a23*a32) / det;
    *ElemDirect(0,1) =   (a13*a32-a12*a33) / det;
    *ElemDirect(0,2) =   (a12*a23-a13*a22) / det;

    *ElemDirect(1,0) =   (a23*a31-a21*a33) / det;
    *ElemDirect(1,1) =   (a11*a33-a13*a31) / det;
    *ElemDirect(1,2) =   (a13*a21-a11*a23) / det;

    *ElemDirect(2,0) =   (a21*a32-a22*a31) / det;
    *ElemDirect(2,1) =   (a12*a31-a11*a32) / det;
    *ElemDirect(2,2) =   (a11*a22-a12*a21) / det;

    return true;
}

//!<  乗法演算子
/*
POINT2D MAT2D::operator * (const POINT2D & pt)
{
    POINT2D retPt;
    retPt.dX = (*this)(0,0) *  pt.dX +
               (*this)(0,1) *  pt.dY +
               (*this)(0,2) *  1.0;  

    retPt.dY = (*this)(1,0) *  pt.dX +
               (*this)(1,1) *  pt.dY +
               (*this)(1,2) *  1.0;  

    return retPt;
}
*/

POINT2D operator *(const IPoint2D& pt, const MAT2D& mat)
{
    POINT2D retPt;
    retPt.dX = mat.Val(0,0) *  pt.GetX() +
               mat.Val(1,0) *  pt.GetY() +
               mat.Val(2,0) *  1.0;  

    retPt.dY = mat.Val(0,1) *  pt.GetX() +
               mat.Val(1,1) *  pt.GetY() +
               mat.Val(2,1) *  1.0;  

    return retPt;
}

MAT2D   operator *(const double & val, const MAT2D& mat)
{
    MAT2D matRet;
    matRet = mat.Mul(val);
    return matRet;
}

void MAT2D::SetCenter(const IPoint2D& pt)
{
    // ある点を中心に拡大、回転する


    double dN = -Elem(0,0) * pt.GetX()
                -Elem(1,0) * pt.GetY()
               + Elem(2,0) + pt.GetX();
       
    double dM = -Elem(0,1) * pt.GetX()
                -Elem(1,1) * pt.GetY()
               + Elem(2,1) + pt.GetY();

    Elem(2,0) = dN;
    Elem(2,1) = dM;

    return;
}


StdString MAT2D::Str() const
{
    StdString strRet;
    strRet = CUtil::StrFormat(_T("[%03f,%03f,%03f,\n \
%03f,%03f,%03f,\n \
%03f,%03f,%03f]"), 
              Elem(0,0), Elem(0,1), Elem(0,2),
              Elem(1,0), Elem(1,1), Elem(1,2),
              Elem(2,0), Elem(2,1), Elem(2,2));

    return strRet;
}


StdString MAT2D::StrAffine() const
{
    StdString str;
    POINT2D pt;
    double dAngle;
    double dSclX;
    double dSclY;
    double dT;
    GetAffineParam(&pt, &dAngle, &dSclX, &dSclY, &dT);
    str = CUtil::StrFormat(_T("Pt:%s A:%f Sx:%f Sy:%f"), pt.Str().c_str(), dAngle, dSclX, dSclY);
    return str;
}

void MAT2D::GetAxixParam( double* pSclX, double* pSclY, 
                          double* pNewAxisAngle,
                          double* pNewSkewAngle, 
                          double dAxisAngle, double dSkewAngle) const
{
    /*
    dSkewAngle X軸固定の歪角

       
         ↓ ここの角度
      Y |    ______________
        |   /             /
        |  /             /
        | /             /
        |/             /
        +-----------------
                         X
    */
    
    /*
    (1,0) (0,1) がマトリクスによってどのように移動するか調べる。
    この時、X軸は常に正となると仮定する。
    従ってX方向の倍率は常に正となる。

    Skewは -90度以上 90度以下
    */
    
    
    POINT2D o(0,0);

    double dS = sin(dAxisAngle * DEG2RAD);
    double dC = cos(dAxisAngle * DEG2RAD);
    POINT2D w(dC, dS);

    double dSs = sin((dAxisAngle +  90.0 - dSkewAngle)* DEG2RAD);
    double dCs = cos((dAxisAngle +  90.0 - dSkewAngle)* DEG2RAD);
    POINT2D h(dCs, dSs);

    POINT2D o2 = o * (*this);
    POINT2D w2 = w * (*this);
    POINT2D h2 = h * (*this);
    POINT2D w0 =  POINT2D( 1.0 , 0.0);
    POINT2D ww = w0 * (*this);

    double dSx  =  o2.Distance(w2);
    double dSy  =  o2.Distance(h2);
    double dSxx  =  o2.Distance(ww);


    POINT2D w3 = w2 - o2;
    POINT2D h3 = h2 - o2;
    POINT2D ww3 = ww - o2;


    //------------------------------
   //
   double lxy3 = dSx * dSy;

    double cs = 0.0;
    if(lxy3 > NEAR_ZERO)
    {
        double dot = w3.Dot(h3);
        cs = dot/ lxy3;
    }
    double dNewSkew =  90.0 - acos(cs)* RAD2DEG;
    //------------------------------


    //Y軸の向きを判定する
    double dYDir;
    if(w3.Cross(h3) < 0)
    {
        dYDir = -1.0;
    }
    else
    {
        dYDir =  1.0;
    }

    //------------------------------
    double dNewAngle = atan2(w3.dY,w3.dX) * RAD2DEG;
    //------------------------------

 #if 0

    DB_PRINT(_T("GetAxixParam Angle %f->%f Sx:%f Sy:%f Skew:%f->%f o2:%s ww3:%s o:%s ww:%s\n"),
        dAxisAngle, dNewAngle,
        dSx,
        dSy,
        dSkewAngle, dNewSkew,
        o2.Str().c_str(),
        ww3.Str().c_str(),
        o.Str().c_str(),
        ww.Str().c_str());

    DB_PRINT(_T("Mat Start -----\n"));
    DB_PRINT(_T("%s\n"), Str().c_str());
    DB_PRINT(_T("Mat End -----\n"));
#endif

    if (pSclX)
    {
        *pSclX = dSx;
    }

    if (pSclY)
    {
        *pSclY = dYDir * dSy;
    }

    if (pNewSkewAngle)
    {
        *pNewSkewAngle = dNewSkew;
    }

    if (pNewAxisAngle)
    {
        *pNewAxisAngle = dNewAngle;
    }
    
    return;
}

//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void TEST_MAT2D()
{
    STD_DBG(_T("%s Start\n"), DB_FUNC_NAME.c_str());
    
    MAT2D mat1;
    MAT2D mat2;
    MAT2D mat3;
    MAT2D mat4;

    mat3(0,0) =  7.0;
    mat3(0,1) =  8.0;
    mat3(0,2) =  9.0;
    mat3(1,0) = 10.0;
    mat3(1,1) = 11.0;
    mat3(1,2) = 12.0;
    mat3(2,0) = 13.0;
    mat3(2,1) = 14.0;
    mat3(2,2) = 15.0;

    mat1.Print();

    mat3 = 2.0 * mat1;

    mat2 = mat1 * 2.0;

    mat4 = mat3 - mat2;
    STD_DBG(_T("Mat2=\n"));
    mat2.Print();

    STD_DBG(_T("Mat3=\n"));
    mat3.Print();

    STD_DBG(_T("Mat4=\n"));
    mat4.Print();


    //--------------------------------------
    STD_DBG(_T("MAT2D move test\n"));
    MAT2D matMove1;
    MAT2D matMove2;
    POINT2D ptMove1(3.0,6.0);
    POINT2D pt1(2.0,3.0);
    POINT2D pt1_2;
    POINT2D pt1_3;
    POINT2D pt1_Ans(5.0,9.0);
    
    matMove1.Move(ptMove1);
    
    pt1_3 =  pt1 * matMove1;

    STD_ASSERT(pt1_3 == pt1_Ans);

    matMove2 = matMove1 * matMove1;
    pt1_3 =  pt1 * matMove2;

    pt1_Ans.Set(5.0 + 3.0, 9.0 + 6.0);
    STD_ASSERT(pt1_3 == pt1_Ans);

    //--------------------------------------
    STD_DBG(_T("MAT2D rotate test\n"));
    MAT2D matRot1;
    double  dDeg1 = 15.0;
    POINT2D ptRotRes;

    double dC;
    double dS;
    CUtil::GetTriFunc(dDeg1, &dS, &dC);

    {
        matRot1.SetIdentity();
        POINT2D ptRot1(3.0,0.0);
        matRot1.Rotate(dDeg1);

        POINT2D ptRotAns( 3.0 * dC,
                          3.0 * dS);

        ptRotRes = ptRot1 * matRot1;
        STD_ASSERT(ptRotRes == ptRotAns);


    }
    

    double dSclX;
    double dSclY;
    double dDeg2;
    double dT;
    POINT2D ptPos;

    double dSclX_A;
    double dSclY_A;
    double dDeg2_A;
    POINT2D ptPos_A;

    matRot1.GetAffineParam(&ptPos, &dDeg2, &dSclX, &dSclY, &dT);

    STD_ASSERT(dSclX == 1.0);
    STD_ASSERT(dSclY == 1.0);
    STD_ASSERT(dDeg2 == dDeg1);

    dSclX = 0.5;
    dSclY = 2.5;
    dDeg2 = 30.0;
    ptPos.dX = 1.0;
    ptPos.dY = 2.0;
    dT = 0.0;
    double dT_A;

    MAT2D matAffin;
    matAffin.SetAffineParam(ptPos, dDeg2, dSclX, dSclY, dT);
    matAffin.GetAffineParam(&ptPos_A, &dDeg2_A, &dSclX_A, &dSclY_A, &dT_A);

    STD_ASSERT_DBL_EQ(ptPos.Distance(ptPos_A), 0.0);
    STD_ASSERT_DBL_EQ(dDeg2, dDeg2_A);
    STD_ASSERT_DBL_EQ(dSclX, dSclX_A);
    STD_ASSERT_DBL_EQ(dSclY, dSclY_A);



    dSclX = -1.5;
    dSclY = 0.5;
    dDeg2 = 190.0;
    ptPos.dX = -1.0;
    ptPos.dY =  2.0;

    matAffin.SetAffineParam(ptPos, dDeg2, dSclX, dSclY, dT);
    matAffin.GetAffineParam(&ptPos_A, &dDeg2_A, &dSclX_A, &dSclY_A, &dT_A);

    STD_ASSERT_DBL_EQ(ptPos.Distance(ptPos_A), 0.0);
    STD_ASSERT_DBL_EQ(dDeg2, dDeg2_A);
    STD_ASSERT_DBL_EQ(dSclX, dSclX_A);
    STD_ASSERT_DBL_EQ(dSclY, dSclY_A);

    dSclX = 1.5;
    dSclY = -0.5;
    dDeg2 = 190.0;
    ptPos.dX = -1.0;
    ptPos.dY =  2.0;

    matAffin.SetAffineParam(ptPos, dDeg2, dSclX, dSclY, dT);
    matAffin.GetAffineParam(&ptPos_A, &dDeg2_A, &dSclX_A, &dSclY_A, &dT_A);

    STD_ASSERT_DBL_EQ(ptPos.Distance(ptPos_A), 0.0);
    STD_ASSERT_DBL_EQ(dDeg2, dDeg2_A);
    STD_ASSERT_DBL_EQ(dSclX, dSclX_A);
    STD_ASSERT_DBL_EQ(dSclY, dSclY_A);


    dSclX = -1.5;
    dSclY = -0.5;
    dDeg2 = 190.0;
    ptPos.dX = -1.0;
    ptPos.dY =  2.0;

    matAffin.SetAffineParam(ptPos, dDeg2, dSclX, dSclY, dT);
    matAffin.GetAffineParam(&ptPos_A, &dDeg2_A, &dSclX_A, &dSclY_A, &dT_A);

    STD_ASSERT_DBL_EQ(ptPos.Distance(ptPos_A), 0.0);
    STD_ASSERT_DBL_EQ(dDeg2, dDeg2_A);
    STD_ASSERT_DBL_EQ(dSclX, dSclX_A);
    STD_ASSERT_DBL_EQ(dSclY, dSclY_A);

    dSclX = 1.5;
    dSclY = 0.5;
    dDeg2 = 190.0;
    ptPos.dX = -1.0;
    ptPos.dY =  2.0;

    matAffin.SetAffineParam(ptPos, dDeg2, dSclX, dSclY, dT);
    matAffin.GetAffineParam(&ptPos_A, &dDeg2_A, &dSclX_A, &dSclY_A, &dT_A);

    STD_ASSERT_DBL_EQ(ptPos.Distance(ptPos_A), 0.0);
    STD_ASSERT_DBL_EQ(dDeg2, dDeg2_A);
    STD_ASSERT_DBL_EQ(dSclX, dSclX_A);
    STD_ASSERT_DBL_EQ(dSclY, dSclY_A);



    dSclX = 1.5;
    dSclY = 0.5;
    dDeg2 = 190.0;
    dT    = 0.5;
    ptPos.dX = -1.0;
    ptPos.dY =  2.0;

    matAffin.SetAffineParam(ptPos, dDeg2, dSclX, dSclY, dT);
    matAffin.GetAffineParam(&ptPos_A, &dDeg2_A, &dSclX_A, &dSclY_A, &dT_A);

    STD_ASSERT_DBL_EQ(ptPos.Distance(ptPos_A), 0.0);
    STD_ASSERT_DBL_EQ(dDeg2, dDeg2_A);
    STD_ASSERT_DBL_EQ(dSclX, dSclX_A);
    STD_ASSERT_DBL_EQ(dSclY, dSclY_A);
    STD_ASSERT_DBL_EQ(dT, dT_A);


    dSclX = 1.0;
    dSclY = 1.0;
    dDeg2 = 30.0;
    dT    = 0.0;
    ptPos.dX =  0.0;
    ptPos.dY =  0.0;

    MAT2D matA;
    MAT2D matB;
    POINT2D pt2;

    matA.SetAffineParam(pt2, 0.0, 1.0, 0.5, 0.0); 
    matAffin.SetAffineParam(ptPos, dDeg2, dSclX, dSclY, dT);

    matB =   matAffin * matA ;
    matB.GetAffineParam(&ptPos_A, &dDeg2_A, &dSclX_A, &dSclY_A, &dT_A);

    /*
    STD_ASSERT_DBL_EQ(ptPos.Distance(ptPos_A), 0.0);
    STD_ASSERT_DBL_EQ(dDeg2, dDeg2_A);
    STD_ASSERT_DBL_EQ(dSclX, dSclX_A);
    STD_ASSERT_DBL_EQ(dSclY, dSclY_A);
    STD_ASSERT_DBL_EQ(dT, dT_A);
    */



    POINT2D  ptSrc;
    POINT2D  ptDst;

    ptSrc.dX = 10.0;
    ptSrc.dY = 20.0;

    ptDst = ptSrc * matAffin;

    POINT2D  ptRev;
    ptRev = matAffin.GetRevAffinePoint(ptDst);

    STD_ASSERT_DBL_EQ(ptRev.Distance(ptSrc), 0.0);


    //--------------------------------------
    STD_DBG(_T("MAT2D mirror test\n"));
    MAT2D matMirror1;

    POINT2D ptMirror1(1.0,4.0);
    LINE2D  lineMirror(3.0, 1.0, 2.0, 5.0);
    POINT2D ptMirrorRes;
    POINT2D ptMirrorAns;

    matMirror1.Mirror(lineMirror);
    ptMirrorRes = ptMirror1 * matMirror1;
    ptMirrorAns = ptMirror1;
    ptMirrorAns.Mirror(lineMirror);

    STD_ASSERT(ptMirrorRes == ptMirrorAns);
    
    //--------------------------------------
    STD_DBG(_T("MAT2D scale test\n"));
    MAT2D matScl1;

    POINT2D ptScl1(1.0,4.0);
    POINT2D ptSclCenter(3.0,6.0);
    POINT2D ptSclRes;
    POINT2D ptSclAns;

    matScl1.Scl(ptSclCenter, 10, 20);
    ptSclRes = ptScl1 * matScl1;
    ptSclAns = ptScl1;
    ptSclAns.Scl(ptSclCenter, 10, 20);


    //--------------------------------------
    STD_DBG(_T("MAT2D Invert"));
    {


        MAT2D matMove1;
        MAT2D matInv;
        POINT2D ptMove1(3.0,6.0);
        POINT2D pt1(2.0,3.0);
        POINT2D pt2;
        POINT2D ptAns;
        MAT2D matAns;

        POINT2D ptAns2;
        MAT2D matInv2;
        MAT2D matAns2;

        matMove1.Move(ptMove1);
        MAT2D matMove1_cpy = matMove1;
        
        pt2 = pt1 * matMove1;

        matInv = matMove1.Invert();
        ptAns = pt2 * matInv;

        matInv2 = matMove1_cpy.Inv2();
        ptAns2 = pt2 * matInv2;

        STD_ASSERT(ptAns == pt1);
        matAns = matMove1_cpy * matInv;

        STD_ASSERT(ptAns2 == pt1);
        matAns2 = matMove1_cpy * matInv2;

        MAT2D matTest;

        int a = 10;

    }


    STD_DBG(_T("MAT2D Invert2"));
    {
        MAT2D matTest1;
        MAT2D matTest2;

        matTest1.SetAll(1.0, 0.0, 0.0,
                        0.0, 1.0, 0.0,  
                       20.0, 0.0, 1.0);

        matTest2.SetAll(1.0, 0.0, 0.0,
                        0.0, 1.0, 0.0,  
                       -20.0, 0.0, 1.0);
        STD_ASSERT(matTest1.Inv2D());
        STD_ASSERT(matTest1 == matTest2);




    }

    POINT2D p1( -6.0,-3.0);
    POINT2D p2( -6.0,-5.0);
    POINT2D p3(-10.0,-5.0);
    POINT2D p4(-10.0,-3.0);
    POINT2D pCenter(-8.0,-4.0);
    POINT2D pMove(0.0,0.0);
   
    MAT2D matRotScl;
    matRotScl.SetAffineParam(pMove, 0.0, 2.0, 2.0, 0.0);
    matRotScl.SetCenter(pCenter);

    POINT2D p1Ret = p1 * matRotScl;
    POINT2D p2Ret = p2 * matRotScl;
    POINT2D p3Ret = p3 * matRotScl;
    POINT2D p4Ret = p4 * matRotScl;

    POINT2D pAns1( -4.0,-2.0);
    POINT2D pAns2( -4.0,-6.0);
    POINT2D pAns3(-12.0,-6.0);
    POINT2D pAns4(-12.0,-2.0);

    STD_ASSERT(p1Ret == pAns1);
    STD_ASSERT(p2Ret == pAns2);
    STD_ASSERT(p3Ret == pAns3);
    STD_ASSERT(p4Ret == pAns4);


    STD_ASSERT(ptSclRes == ptSclAns);
    STD_DBG(_T("%s End\n"), DB_FUNC_NAME.c_str());
    STD_DBG(_T("\n"));



    //--------------------------------------
    STD_DBG(_T("%s End\n"), DB_FUNC_NAME.c_str());
    STD_DBG(_T("\n"));

}
#endif //_DEBUG