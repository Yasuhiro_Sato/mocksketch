/**
 * @brief			MAT2Dwb_[t@C
 * @file			MAT2D.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

#ifndef __MAT2D_H_
#define __MAT2D_H_


/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "Utility/TMatrix.h"
#include "DrawingObject/BaseObj.h"

/*---------------------------------------------------*/
/*  classes                                          */
/*---------------------------------------------------*/
class IPoint2D;
class POINT2D;
class ILine2D;
class LINE2D;

class IMat2D
{
public:
    virtual void Move(const IPoint2D& pt) = 0;
    virtual void Rotate(double dAngle) = 0;
    virtual void Rotate(const IPoint2D& pt, double dAngle) = 0;
    virtual void Mirror(const ILine2D& pt) = 0;
    virtual void Scl(double dSclX, double dSclY) = 0;
    virtual void Scl(const IPoint2D& pt, double dSclX, double dSclY) = 0;
    virtual size_t  Size() const = 0;
    virtual double  Data(size_t iCnt) const = 0;
    virtual bool operator == (const IMat2D& mat) const = 0;
    virtual bool Inv2D() = 0;
    // ret = this x mat
    //virtual void Mul(MAT2D & ret, const IMat2D& mat) = 0;


};

/**
 * @class   MAT2D
 * @brief   
 */
class MAT2D : public NUM_MATRIX<double> , public CBaseObj, public IMat2D
{

private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
            ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(CBaseObj);
            SERIALIZATION_BOTH("Data"     , m_Data);
            SERIALIZATION_BOTH("RowSize"  , m_nRowSize);
            SERIALIZATION_BOTH("ColSize"  , m_nColSize);
            SERIALIZATION_BOTH("Total"    , m_nTotal);
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }

public:
    MAT2D();
    ~MAT2D();

    void SetAll(double d11, double d12, double d13,
                double d21, double d22, double d23,
                double d31, double d32, double d33);

    virtual void Move(const IPoint2D& pt);      // (AS)
    virtual void Rotate(double dAngle);         // (AS)
    virtual void Rotate(const IPoint2D& pt, double dAngle);   //(AS)
    virtual void Mirror(const ILine2D& pt);                    //(AS)
    virtual void Scl(double dSclX, double dSclY);                //(AS)
    virtual void Scl(const IPoint2D& pt, double dSclX, double dSclY);  //(AS)
    virtual void Scl(const IPoint2D& pt, double dAxisXAngle, double dSclX, double dSclY);
    virtual void GetAffineParam(POINT2D* pPt, double* pAngle, double* pSclX, 
                                         double* pSclY, double* dT) const;
    virtual void SetAffineParam(POINT2D  pt, double dAngle, double dSclX, 
                                         double dSclY, double dT);
    virtual void SetAffinePos(POINT2D  pt);
    virtual void SetAffineAngle(double dAngle);
    virtual void SetAffineScl(double dSclX, double dSclY);
    virtual void GetAxixParam( double* pSclX, double* pSclY, 
                               double* pNewAxisAngle, double* pNewSkewAngle, 
                               double dAxisAngle, double dSkewAngle = 0.0) const;


    virtual void SetAffineSkew(double dT);
	virtual void MultiAffine(double dM);

    virtual POINT2D GetRevAffinePoint(POINT2D  pt) const;

    virtual bool Inv2D();

    virtual size_t Size() const {return m_Data.size();}
    virtual double  Data(size_t iCnt) const {return m_Data[iCnt];}
    void SetCenter(const IPoint2D& pt);

    StdString Str() const;
    const StdChar* CStr() const{ return Str().c_str();}

    StdString StrAffine() const;


    bool operator == (const IMat2D& mat) const       //(AS)
    {
        size_t size = mat.Size();
        for (size_t iCnt = 0; iCnt < size; iCnt++)
        {
            if( fabs(mat.Data(iCnt)- m_Data[iCnt]) > NEAR_ZERO)
            {
                return false;
            }
        }
        return true;
    }


    // At@Cฯทอ vec = vec * matฬ`ฎศฬล
    // ํ
    //POINT2D operator * (const POINT2D & pt);

    //* ๆ@Zq   //(AS)
    MAT2D operator * (const double & val) const
    {
        MAT2D matRet;
        matRet.Eql(this->Mul(val));
        return matRet;
    }

    //* ๆ@Zq   //(AS)
    MAT2D operator * (const MAT2D & mat) const
    {
        MAT2D matRet;
        matRet.Eql(this->Mul(mat));
        return matRet;
    }

    //* ม@Zq   //(AS)
    MAT2D operator + (const double & val) const
    {
        MAT2D matRet;
        matRet.Eql(this->Add(val));
        return matRet;
    }

    //* ม@Zq   //(AS)
    MAT2D operator + (const MAT2D & mat) const
    {
        MAT2D matRet;
        matRet.Eql(this->Add(mat));
        return matRet;
    }

    //* ม@Zq   //(AS)
    MAT2D operator - (const double & val) const
    {
        MAT2D matRet;
        matRet.Eql(this->Sub(val));
        return matRet;
    }

    MAT2D operator - (const MAT2D & mat) const
    {
        MAT2D matRet;
        matRet.Eql(this->Sub(mat));
        return matRet;
    }

    MAT2D operator - () const
    {
        MAT2D matRet;
        matRet.Eql(this->Mul(-1.0));
        return matRet;
    }

    double GetVal (size_t nRow, size_t nCol) const
    {
        double dRet;
        dRet = Val( nRow, nCol);
        return dRet;
    }

    MAT2D& operator = (const NUM_MATRIX<double> & mat)  {this->Eql(mat); return *this;}
    MAT2D& operator = (const MAT2D & mat)  {this->Eql(mat); return *this;}
    MAT2D& operator = (const double& val)  {this->Eql(val); return *this;}


    friend POINT2D operator *(const IPoint2D&  pt, const MAT2D& mat);
    friend MAT2D   operator *(const double & val, const MAT2D& mat);

    //NUM_MATRIX<double>* m_pMat;
};


#endif //__MAT2D_H_