/**
 * @brief			RECT2D実装ファイル
 * @file			RECT2D.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./POINT2D.H"
#include "./RECT2D.H"
#include "./LINE2D.H"

#include "Utility/CUtility.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//!< コンストラクタ
RECT2D::RECT2D()
{
    dTop    = 0.0;
    dBottom = 0.0;
    dLeft   = 0.0;
    dRight  = 0.0;
}

//!< コンストラクタ(幅,高さ指定)
RECT2D::RECT2D(double dWidth, double dHeight)
{
    dTop    = 0.0;
    dBottom = dHeight;
    dLeft   = 0.0;
    dRight  = dWidth;
}

//!< コンストラクタ(2点指定)
RECT2D::RECT2D(const POINT2D& ptP1, const POINT2D &ptP2)
{
    SetPoint(ptP1, ptP2);
}

RECT2D::RECT2D(double left, double top, double right, double bottom)
{
    dTop    = top;
    dBottom = bottom;
    dLeft   = left;
    dRight  = right;
}


/**
 *  @brief   代入演算子
 *  @param   [in] m 代入元 
 *  @retval  代入値
 *  @note
 */
RECT2D& RECT2D::operator = (const RECT2D & m)
{
    dTop    = m.dTop;
    dBottom = m.dBottom;
    dLeft   = m.dLeft;
    dRight  = m.dRight;
    return *this;
}

/**
 *  @brief  代入演算子.
 *  @param  [in]    pt    点
 *  @retval         なし
 *  @note
 */
RECT2D& RECT2D::operator = (const POINT2D & pt)
{
    dTop    = pt.dY;
    dBottom = pt.dY;
    dLeft   = pt.dX;
    dRight  = pt.dX;
    return *this;
}

// **
// @brief   等価演算子
// @param	m (i) 値
// @return  ture 同値
// @note    
// */
bool RECT2D::operator == (const RECT2D & m)  const
{
    if(fabs(dTop     - m.dTop)    > NEAR_ZERO)    {return false;}
    if(fabs(dBottom  - m.dBottom) > NEAR_ZERO)    {return false;}
    if(fabs(dLeft    - m.dLeft)   > NEAR_ZERO)    {return false;}
    if(fabs(dRight   - m.dRight)  > NEAR_ZERO)    {return false;}
    return true;
}

// **
// @brief   不等価演算子
// @param	m (i) 値
// @return  false 同値
// @note    
// */
bool RECT2D::operator != (const RECT2D & m) const
{
    return !(*this == m);
}


/**
 *  @brief  点含む領域に拡張する.
 *  @param  [in]    pt    点
 *  @retval         なし
 *  @note
 */
void RECT2D::ExpandArea(const POINT2D & pt)
{
    if ( dTop < pt.dY)
    {
        dTop = pt.dY;
    }
    else if (dBottom > pt.dY)
    {
        dBottom = pt.dY;
    }

    if (dRight < pt.dX)
    {
        dRight = pt.dX;
    }
    else if (dLeft > pt.dX)
    {
        dLeft = pt.dX;
    }
}

/**
 *  @brief  領域を拡張する.
 *  @param  [in]    rc    領域
 *  @retval         なし
 *  @note
 */
void RECT2D::ExpandArea(const RECT2D & rc)
{
    if ( dTop < rc.dTop)
    {
        dTop = rc.dTop;
    }

    if (dBottom > rc.dBottom)
    {
        dBottom = rc.dBottom;
    }

    if (dRight < rc.dRight)
    {
        dRight = rc.dRight;
    }

    if (dLeft > rc.dLeft)
    {
        dLeft = rc.dLeft;
    }
}


///**
// @brief   指定した点が内部かどうか
// @param	[in] pt 点
// @return  true 四角形内 false 四角形外
// @note   
// */
bool RECT2D::IsInside(const POINT2D & pt) const
{
    if (dLeft   > pt.dX) {return false;}
    if (dRight  < pt.dX) {return false;}
    if (dBottom > pt.dY) {return false;}
    if (dTop    < pt.dY) {return false;}
    return true;
}

/**
 *  @brief  指定した矩形が内部かどうか
 *  @param  [in]    rc    四角形
 *  @retval true 四角形内 false 四角形外
 *  @note
 */
bool RECT2D::IsInside(const RECT2D & rc) const
{
    if (dLeft  > rc.dLeft) {return false;}
    if (dRight < rc.dRight){return false;}

    if (dTop    < rc.dTop)   {return false;}
    if (dBottom > rc.dBottom){return false;}

    return true;
}

bool RECT2D::IsOverlap(const RECT2D & rc) const
{
    if(dLeft  > rc.dRight){ return false; }
    if(dRight < rc.dLeft) { return false; }

    if(dTop    < rc.dBottom){ return false; }
    if(dBottom > rc.dTop)   { return false; }

    return true;
}


//矩形に最も近い矩形上の点を取得する
RECT2D::E_EDGE RECT2D::NearPoint(POINT2D* pEdge, const POINT2D & pt)  const
{
    POINT2D ptRet = pt;

    POINT2D pTL(dLeft, dTop); 
    POINT2D pTR(dRight, dTop); 
    POINT2D pBL(dLeft, dBottom); 
    POINT2D pBR(dRight, dBottom); 

    POINT2D P[4];
    P[0] = pt - pTL ;
    P[1] = pt - pTR ;
    P[2] = pt - pBR ;
    P[3] = pt - pBL ;
 
    double dA[4];

    dA[0] = P[0].Angle();
    dA[1] = P[1].Angle();
    dA[2] = P[2].Angle();
    dA[3] = P[3].Angle();

    E_EDGE eEdge =  E_NONE;

    if ((dA[0] >=  0) && (dA[0] < 135) &&
        (dA[1] >= 45) && (dA[0] < 180))
    {
          eEdge = E_TOP;
          ptRet.dY = dTop;
    }
    else if   ((dA[2] >=  180) && (dA[2] < 315) &&
               (dA[3] >= 225)  && (dA[3] < 360))
    {
          eEdge = E_BOTTOM;
          ptRet.dY = dBottom;
    }
    else  if (dLeft > pt.dX)
    {
          eEdge = E_LEFT;
          ptRet.dX = dLeft;
    }
    else  if (dRight < pt.dX)
    {
          eEdge = E_RIGHT;
          ptRet.dX = dRight;
    }
    else
    {
         return NearEdgePoint(pEdge, pt);
    }

    if ( (eEdge == E_RIGHT) ||
         (eEdge == E_LEFT))
    {
        if (ptRet.dY > dTop)
        {
            ptRet.dY  = dTop;
        }
        else if ( ptRet.dY < dBottom)
        {
            ptRet.dY  = dBottom;
        }
    }
    else
    {
        if (ptRet.dX > dRight)
        {
            ptRet.dX  = dRight;
        }
        else if ( ptRet.dX < dLeft)
        {
            ptRet.dX  = dLeft;
        }
    }

    *pEdge =  ptRet;
    return eEdge;

}

 

//矩形内部の点に最も近い矩形上の点を取得する
RECT2D::E_EDGE RECT2D::NearEdgePoint(POINT2D* pEdge, const POINT2D & pt)  const
{
    double dMin = DBL_MAX;


    double dL;

    POINT2D ptRet;
    double dRet = false;

    E_EDGE eEdge =  E_NONE;
    dL = dTop - pt.dY;
    if ((dL > 0) && dL < dMin)
    {
        dMin = dL;
        ptRet.dY = dTop;
        ptRet.dX = pt.dX;
        eEdge = E_TOP;
    }

    dL = pt.dY - dBottom;
    if ((dL > 0) && dL < dMin)
    {
        dMin = dL;
        ptRet.dY = dBottom;
        ptRet.dX = pt.dX;
        eEdge = E_BOTTOM;
    }

    dL = pt.dX - dLeft;
    if ((dL > 0) && dL < dMin)
    {
        dMin = dL;
        ptRet.dX = dLeft;
        ptRet.dY = pt.dY;
        eEdge = E_LEFT;
    }

    dL = dRight - pt.dX;
    if ((dL > 0) && dL < dMin)
    {
        dMin = dL;
        ptRet.dX = dRight;
        ptRet.dY = pt.dY;
        eEdge = E_RIGHT;
    }

    if(eEdge != E_NONE)
    {
        *pEdge = ptRet;
        return eEdge;
    }

    return eEdge;

}

void RECT2D::Expansion(double dLen)
{
    dTop += dLen;
    dBottom -= dLen;
    dLeft -= dLen;
    dRight += dLen;
}

void RECT2D::GetrLines(std::vector<LINE2D> * pList)  const
{
    if(!pList){return;}
    pList->clear();
    pList->push_back(LINE2D(dLeft,dTop, dRight, dTop));
    pList->push_back(LINE2D(dRight,dTop, dRight, dBottom));
    pList->push_back(LINE2D(dRight, dBottom, dLeft, dBottom));
    pList->push_back(LINE2D(dLeft, dBottom, dLeft,dTop));
}
       
bool RECT2D::GetrLine(LINE2D* pLine, RECT2D::E_EDGE edge)  const
{
    if((edge & E_TOP) ==  E_TOP)
    {
        pLine->SetPt(dLeft,dTop, dRight, dTop);
        return true;
    }

    if((edge & E_RIGHT) ==  E_RIGHT)
    {
        pLine->SetPt(dRight,dTop, dRight, dBottom);
        return true;
    }

    if((edge & E_BOTTOM) ==  E_BOTTOM)
    {
         pLine->SetPt(dRight, dBottom, dLeft, dBottom);
        return true;
    }

    if((edge & E_LEFT) ==  E_LEFT)
    {
        pLine->SetPt(dLeft, dBottom, dLeft,dTop);
        return true;
    }
    return false;
}

//!<  点のリストを取得する
void RECT2D::GetPoints(std::vector<POINT2D>* lst)  const
{
    POINT2D pt;

    pt.dX = dLeft;
    pt.dY = dTop;
    lst->push_back(pt);

    pt.dX = dRight;
    lst->push_back(pt);

    pt.dY = dBottom;
    lst->push_back(pt);

    pt.dX = dLeft;
    lst->push_back(pt);
}

//!<  ある点からの視野角を求める
void RECT2D::GetAngeleFormPoint(double* pStartAngle, double* pEndAngle, POINT2D  pt)  const
{
    double dStart = 0.0;
    double dEnd   = 0.0;
    double dAngleMax = -DBL_MAX;

    if (IsInside(pt))
    {
        *pStartAngle = 0.0;
        *pEndAngle = 360.0;
        return;
    }


    std::vector<POINT2D> lst;
    GetPoints(&lst);

    std::vector<double> lstAngle;
    for(POINT2D& ptBox:lst)
    {
        double dAngle = CUtil::NormAngle(atan2(ptBox.dY - pt.dY,
                                               ptBox.dX - pt.dX)
                                               * RAD2DEG);
        lstAngle.push_back(dAngle);
    }

    std::sort(lstAngle.begin(),lstAngle.end(),std::less<double>());


    int iSize = static_cast<int>(lstAngle.size());

    for(int iStart = 0; iStart<iSize; iStart++)
    {
        double dS;
        dS = lstAngle[iStart];
        for(int iEnd = iStart+1; iEnd<iSize; iEnd++)
        {
            double dE;
            dE = lstAngle[iEnd];

            double dDiff = dE - dS;

            if (dDiff > 180.0)
            {
                dS += 360.0;
                std::swap(dS,dE);
                dDiff = dE - dS;
            }

            if (dAngleMax < dDiff)
            {
                dStart = dS;
                dEnd   = dE;
                dAngleMax = dDiff;
            }
        }
    }

    *pStartAngle  = dStart;
    *pEndAngle    = dEnd;
}

//!< 幅 (AS)
double RECT2D::Width() const
{
   return fabs(dRight - dLeft);
}

//!< 高さ (AS)
double RECT2D::Height() const
{
   return fabs(dBottom - dTop);
}

void RECT2D::Move(const POINT2D& pt)
{
    dLeft += pt.dX;
    dTop  += pt.dY;
    dRight  += pt.dX;
    dBottom  += pt.dY;
}

void RECT2D::Rotate(const POINT2D& pt, double dAngle)
{
    POINT2D p[4];
    p[0].dX = dLeft;
    p[0].dY = dTop;

    p[1].dX = dRight;
    p[1].dY = dTop;

    p[2].dX = dRight;
    p[2].dY = dBottom;

    p[3].dX = dLeft;
    p[2].dY = dBottom;

    for(POINT2D& pos:p)
    {
        pos.Rotate(pt, dAngle);
    }

    double dL =  DBL_MAX;
    double dR = -DBL_MAX;
    double dT = -DBL_MAX;
    double dB =  DBL_MAX;

    for(POINT2D& pos:p)
    {
        if(pos.dX > dR)
        {
            dR = pos.dX;
        }

        if(pos.dX < dL)
        {
            dL = pos.dX;
        }

        if(pos.dY > dT)
        {
            dT = pos.dY;
        }

        if(pos.dY < dB)
        {
            dB = pos.dY;
        }
    }
    dTop        = dT;
    dBottom     = dB;
    dLeft       = dL;
    dRight      = dR;
}


void RECT2D::SetPoint(const POINT2D& pt)
{
    dTop        = pt.dY;
    dBottom     = pt.dY;
    dLeft       = pt.dX;
    dRight      = pt.dX;
}

void RECT2D::SetPoint(const POINT2D& ptP1, const POINT2D &ptP2)
{
    if (ptP1.dX > ptP2.dX)
    {
        dLeft = ptP2.dX;
        dRight = ptP1.dX;
    }
    else
    {
        dLeft = ptP1.dX;
        dRight = ptP2.dX;
    }

    if (ptP1.dY > ptP2.dY)
    {
        dBottom = ptP2.dY;
        dTop = ptP1.dY;
    }
    else
    {
        dBottom = ptP1.dY;
        dTop = ptP2.dY;
    }
}


void RECT2D::Matrix(const MAT2D& mat2)
{
    POINT2D p[4];
    p[0].dX = dLeft;
    p[0].dY = dTop;

    p[1].dX = dRight;
    p[1].dY = dTop;

    p[2].dX = dRight;
    p[2].dY = dBottom;

    p[3].dX = dLeft;
    p[3].dY = dBottom;

    for(POINT2D& pos:p)
    {
        pos.Matrix(mat2);
    }

    double dL =  DBL_MAX;
    double dR = -DBL_MAX;
    double dT = -DBL_MAX;
    double dB =  DBL_MAX;

    for(POINT2D& pos:p)
    {
        if(pos.dX > dR)
        {
            dR = pos.dX;
        }

        if(pos.dX < dL)
        {
            dL = pos.dX;
        }

        if(pos.dY > dT)
        {
            dT = pos.dY;
        }

        if(pos.dY < dB)
        {
            dB = pos.dY;
        }
    }
    dTop        = dT;
    dBottom     = dB;
    dLeft       = dL;
    dRight      = dR;
}

StdString RECT2D::Str() const
{
    StdString strRet;

    strRet = CUtil::StrFormat(_T("(%f,%f,%f,%f)"), 
              dLeft, dTop, dRight, dBottom);
    return strRet;
}

StdString RECT2D::Edge2Str(E_EDGE e)
{
    StdStringStream strm;
    if ((e & RECT2D::E_TOP) == RECT2D::E_TOP)
    {
        strm << _T("T ");
    }
    if ((e & RECT2D::E_BOTTOM) == RECT2D::E_BOTTOM)
    {
        strm << _T("B ");
    }
    if ((e & RECT2D::E_LEFT) == RECT2D::E_LEFT)
    {
        strm << _T("L ");
    }
    if ((e & RECT2D::E_RIGHT) == RECT2D::E_RIGHT)
    {
        strm << _T("R ");
    }
    return strm.str();
}

//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void TEST_RECT2D()
{
    STD_DBG(_T("%s Start\n"), DB_FUNC_NAME.c_str());
    
    STD_DBG(_T("%s End\n"), DB_FUNC_NAME.c_str());
    STD_DBG(_T("\n"));
}
#endif //_DEBUG