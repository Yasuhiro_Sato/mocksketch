/**
 * @brief			RefBase
 * @file			RefBase.h
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:37
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "RefBase.h"


/*---------------------------------------------------*/
/* Macros                                            */
/*---------------------------------------------------*/
#ifdef _DEBUG
#undef new 
#define new DEBUG_NEW
#endif

/*---------------------------------------------------*/
/* Static variables                                  */
/*---------------------------------------------------*/
CAsRefCtrl* CAsRefCtrl::ms_pCtrl = NULL;

/**
 *  @brief  コンストラクタ
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CAsRefObj::CAsRefObj():
    m_iRefCount (1),
    m_bAppCtrl  (true),
    m_iRefObjId(-1)
{
}

/**
 *  @brief  デストラクタ.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CAsRefObj::~CAsRefObj()
{
    if (!m_bAppCtrl)
    {
        CAsRefCtrl::GetInstance()->DelAsObj(this);
    }
}

/**
 *  @brief  参照追加
 *  @param  なし
 *  @retval なし     
 *  @note
 */
void CAsRefObj::AddRef()
{
    ++m_iRefCount;
    if (m_iRefObjId != -1)
    {
        DB_PRINT(_T("AddRef %d: %d\n"), m_iRefObjId, m_iRefCount);
    }
}

/**
 *  @brief  参照開放
 *  @param  なし
 *  @retval なし     
 *  @note
 */
void CAsRefObj::Release() 
{
    if (m_iRefCount > 0)
    {
        --m_iRefCount;
    }

    if (m_iRefObjId != -1)
    {
        DB_PRINT(_T("Release %d: %d\n"), m_iRefObjId, m_iRefCount);
    }

    if (!m_bAppCtrl)
    {
        if (m_iRefCount == 0)
        {
            delete this;
        }

    }
}

/**
 *  @brief  所有権移行(APP)
 *  @param  なし
 *  @retval なし     
 *  @note
 */
void CAsRefObj::SetAppCtrl()
{
    m_bAppCtrl = true;
    CAsRefCtrl::GetInstance()->DelAsObj(this);
}

/**
 *  @brief  所有権移行(SCRIPT)
 *  @param  なし
 *  @retval なし     
 *  @note
 */
void CAsRefObj::SetScriptCtrl()
{
    m_bAppCtrl = false;
    m_iRefCount = 0;
    CAsRefCtrl::GetInstance()->AddAsObj(this);
}


/**
 *  @brief  削除可・不可問い合わせ
 *  @param  なし
 *  @retval true 削除可
 *  @note
 */
bool CAsRefObj::IsDelete()
{
    if (m_bAppCtrl)
    {
        return false;
    }
    return (m_iRefCount == 0);
}


//=============================================
//=============================================
//=============================================

/**
 *  @brief  コンストラクタ
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CAsRefCtrl::CAsRefCtrl()
{

}

/**
 *  @brief  デストラクタ
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CAsRefCtrl::~CAsRefCtrl()
{
}

/** 
 *  @brief  インスタンス取得
 *  @param  なし
 *  @retval インスタンス     
 *  @note   シングルトン 
 */
CAsRefCtrl* CAsRefCtrl::GetInstance()
{
    if (!ms_pCtrl)
    {
        ms_pCtrl = new CAsRefCtrl;
    }
    return ms_pCtrl;
}

/** 
 *  @brief  インスタンス開放
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CAsRefCtrl::DeleteInstance()
{
    delete ms_pCtrl;
    ms_pCtrl = NULL;
}


/** 
 *  @brief  オブジェクト追加
 *  @param  [in] pObj オブジェクト 
 *  @retval なし
 *  @note   
 */
void CAsRefCtrl::AddAsObj(CAsRefObj* pObj)
{
    pObj->m_bAppCtrl = false;
    m_lstObj.insert(pObj);
}

/** 
 *  @brief  オブジェクト削除
 *  @param  [in] pObj    オブジェクト 
 *  @param  [in] bDelete オブジェクト消去の有無
 *  @retval なし
 *  @note   
 */
void CAsRefCtrl::DelAsObj(CAsRefObj* pObj, bool bDelete)
{
    std::set<CAsRefObj*>::iterator iteFind;

    iteFind = m_lstObj.find(pObj);


    if (iteFind != m_lstObj.end())
    {
        m_lstObj.erase(iteFind);
        if( bDelete)
        {
            delete pObj;
        }
    }
}

/** 
 *  @brief  全オブジェクト
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CAsRefCtrl::DeleteAll()
{
    std::set<CAsRefObj*> lstDelete;
    std::set<CAsRefObj*>::iterator iteDelete;

    CAsRefObj* pObj;
    for (iteDelete  =  m_lstObj.begin();
         iteDelete !=  m_lstObj.end();
         iteDelete++)
    {
        pObj = *iteDelete;

        if (pObj->IsDelete())
        {
            lstDelete.insert(pObj);
        }
    }

    for (iteDelete  =  lstDelete.begin();
         iteDelete !=  lstDelete.end();
         iteDelete++)
    {
        pObj = *iteDelete;
        DelAsObj(pObj, true);
    }
    m_lstObj.clear();
}
