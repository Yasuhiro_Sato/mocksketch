/**
 * @brief			RefBase
 * @file			RefBase.h
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:37
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

#ifndef REF_BASE_H
#define REF_BASE_H
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/


/*---------------------------------------------------*/
/*  class                                            */
/*---------------------------------------------------*/
class CAsRefCtrl;


/**
 * @class   CAsRefObj
 * @brief   AngelScriptに参照型として登録するオブジェクトの
 *          基底となるクラス
 *          AngelScript内では参照カウンターの操作のみとし、
 *          オブジェクトの削除はアプリケーションで行う。
 */
class CAsRefObj
{
public: 
    //!< コンストラクタ
    CAsRefObj();

    //!< デストラクタ
    virtual ~CAsRefObj();

    //!< 削除可・不可問い合わせ
    bool IsDelete(); 

    //<! 参照追加
    void AddRef();

    //<! 参照開放
    void Release();

    //<! 所有権移行(APP)
    void SetAppCtrl();

    //<! 所有権移行(SCRIPT)
    void SetScriptCtrl();

    int  GetRefId(){return m_iRefObjId;}
    void SetRefId(int id){m_iRefObjId = id;}

protected:
    friend CAsRefCtrl;

    //! 参照カウント
    int m_iRefCount;

    //!　APP側所有権
    bool m_bAppCtrl;

    //! デバッグ用ID
    int   m_iRefObjId;
};


/**
 * @class   CAsRefCtrl
 * @brief   CAsRefObjを管理するシングルトン
 */
class CAsRefCtrl
{
public:
    //!< インスタンス取得
    static CAsRefCtrl* GetInstance();

    //!< インスタンス開放
    static void DeleteInstance();

    //!< オブジェクト追加
    void AddAsObj(CAsRefObj* pObj);

    //!< オブジェクト削除
    void DelAsObj(CAsRefObj* pObj, bool bDelete = false);

    //!< 全オブジェクト削除
    void DeleteAll();

    //!< デストラクタ
    virtual ~CAsRefCtrl();

protected:
    //!< コンストラクタ
    CAsRefCtrl();

protected:
    //!< オブジェクトリスト
    std::set<CAsRefObj*> m_lstObj;

    //!< インスタンス
    static CAsRefCtrl* ms_pCtrl;
};
#endif
