/**
 * @brief			ExecCommon実装ファイル
 * @file			ExecCommon.cpp
 * @author			Yasuhiro Sato
 * @date			29-1-2009 23:18:35
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./ExecCommon.h"
#include "Utility/CUtility.h"
#include "System/CSystem.h"


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace EXEC_COMMON;

/*---------------------------------------------------*/
/*  Grobal                                           */
/*---------------------------------------------------*/
boost::bimaps::bimap<E_THREAD_TYPE, StdString>  g_mapThreadTypeName;
std::map<std::string, CFunctionProperty>        g_mapFunctionProperty;

/**
 *  @brief   初期化処理
 *  @param   なし
 *  @retval  なし
 *  @note     
 */
void EXEC_COMMON::InitName()
{
    typedef boost::bimaps::bimap<E_THREAD_TYPE, StdString>::value_type value_t;

    if (!g_mapThreadTypeName.empty())
    {
        return;
    }

    g_mapThreadTypeName.insert(value_t(TH_NONE      , _T("NONE")));

    g_mapThreadTypeName.insert(value_t(TH_GROUP_1   , _T("GROUP_1")));
    g_mapThreadTypeName.insert(value_t(TH_GROUP_2   , _T("GROUP_2")));
    g_mapThreadTypeName.insert(value_t(TH_GROUP_3   , _T("GROUP_3")));
    g_mapThreadTypeName.insert(value_t(TH_GROUP_4   , _T("GROUP_4")));
    g_mapThreadTypeName.insert(value_t(TH_GROUP_5   , _T("GROUP_5")));
    g_mapThreadTypeName.insert(value_t(TH_GROUP_MAX , _T("GROUP_MAX")));

    g_mapThreadTypeName.insert(value_t(TH_FIELD_1   , _T("FIELD_1")));
    g_mapThreadTypeName.insert(value_t(TH_FIELD_2   , _T("FIELD_2")));
    g_mapThreadTypeName.insert(value_t(TH_FIELD_3   , _T("FIELD_3")));
    g_mapThreadTypeName.insert(value_t(TH_FIELD_4   , _T("FIELD_4")));
    g_mapThreadTypeName.insert(value_t(TH_FIELD_5   , _T("FIELD_5")));
    g_mapThreadTypeName.insert(value_t(TH_FIELD_MAX , _T("FIELD_MAX")));

    g_mapThreadTypeName.insert(value_t(TH_DISP      , _T("DISP")));
    g_mapThreadTypeName.insert(value_t(TH_PANE      , _T("PANE")));

    g_mapThreadTypeName.insert(value_t(TH_IO_1      , _T("IO_1")));
    g_mapThreadTypeName.insert(value_t(TH_IO_2      , _T("IO_2")));
    g_mapThreadTypeName.insert(value_t(TH_IO_3      , _T("IO_3")));

    g_mapThreadTypeName.insert(value_t(TH_MAX       ,_T("MAX")));
    
    CFunctionProperty funcProperty;


    g_mapFunctionProperty["Constructor"]= CFunctionProperty(THREAD_TYPE_NONE , METHOD_CONSTRUCTOR);
    g_mapFunctionProperty["Setup"]      = CFunctionProperty(THREAD_TYPE_GROUP, METHOD_SETUP);
    g_mapFunctionProperty["Abort"]      = CFunctionProperty(THREAD_TYPE_GROUP, METHOD_LOOP);
    g_mapFunctionProperty["Loop"]       = CFunctionProperty(THREAD_TYPE_GROUP, METHOD_ABORT);
    g_mapFunctionProperty["Draw"]       = CFunctionProperty(THREAD_TYPE_DISP , METHOD_DRAW);
    g_mapFunctionProperty["OnChangeProperty"]       = CFunctionProperty(THREAD_TYPE_NONE, METHOD_ON_CHANGE_PROPERTY);
    g_mapFunctionProperty["OnEdit_InitNodeMarker"]   = CFunctionProperty(THREAD_TYPE_NONE, METHOD_ON_INIT_NODE_MARKER);
    g_mapFunctionProperty["OnEdit_SelectNodeMarker"] = CFunctionProperty(THREAD_TYPE_NONE, METHOD_ON_SELECT_NODE_MARKER);
    g_mapFunctionProperty["OnEdit_MoveNodeMarker"]   = CFunctionProperty(THREAD_TYPE_NONE, METHOD_ON_MOVE_NODE_MARKER);
    g_mapFunctionProperty["OnEdit_ReleaseNodeMarker"]    = CFunctionProperty(THREAD_TYPE_NONE, METHOD_ON_RELEASE_NODE_MARKER);
    g_mapFunctionProperty["OnEdit_ChangeNode"] = CFunctionProperty(THREAD_TYPE_NONE, METHOD_ON_CHANGE_NODE);

    
}

/**
 *  @brief   スレッド型->名称変換
 *  @param   [in] eType
 *  @retval  名称
 *  @note     
 */
StdString EXEC_COMMON::GetThreadType2Name(E_THREAD_TYPE eType)
{

    using namespace boost::bimaps;

    InitName();

    bimap<E_THREAD_TYPE, StdString>::left_map::iterator iteLeft;


    iteLeft = g_mapThreadTypeName.left.find(eType);

    if (iteLeft == g_mapThreadTypeName.left.end())
    {
        return _T("NONE");
    }

    return iteLeft->second;
}


/**
 *  @brief   名称変換->スレッド型
 *  @param   [in] 名称
 *  @retval  スレッド型
 *  @note     
 */
E_THREAD_TYPE EXEC_COMMON::GetThreadName2Type(StdString strName)
{
    using namespace boost::bimaps;

    InitName();

    bimap<E_THREAD_TYPE, StdString>::right_map::iterator iteRight;
    iteRight = g_mapThreadTypeName.right.find(strName);

    if (iteRight == g_mapThreadTypeName.right.end())
    {
        return TH_NONE;
    }

    return iteRight->second;
}


StdString  EXEC_COMMON::GetDebugMode2Name(DEBUG_MODE eType)
{
    if (eType == EDB_DEBUG)
    {
        return _T("Debug");
    }
    else if (eType == EDB_RELEASE)
    {
        return _T("Release");
    }
    else if (eType == EDB_FOLLOW_SETTING)
    {
        return GET_STR(STR_EXEC_EDB_FLOOW_SETTING);
    }
    
    return _T("");
}

DEBUG_MODE    EXEC_COMMON::GetDebugMode2Type( StdString strName)
{
    if (strName == _T("Debug"))
    {
        return EDB_DEBUG;
    }
    else if (strName == _T("Release"))
    {
        return EDB_RELEASE;
    }
    /*
    else if (strName == GET_STR(STR_EXEC_EDB_FLOOW_SETTING))
    {
        return EDB_FOLLOW_SETTING;
    }
    */
    return EDB_ERROR;

}


/**
 *  @brief   関数名->関数プロパティ
 *  @param   [in] 名称
 *  @retval  関数プロパティ
 *  @note     
 */
CFunctionProperty* EXEC_COMMON::GetFunctionProperty(const char* strFunction)
{
    InitName();

    std::map<std::string, CFunctionProperty>::iterator itMap;
    
    itMap = g_mapFunctionProperty.find(strFunction);

    if (itMap == g_mapFunctionProperty.end())
    {
        return NULL;
    }

    return &(itMap->second);
}


//================================================
/*static */
/*static */
#if 0
CExecAppCtrl*   CExecAppCtrl::ms_pInstance = NULL;
boost::mutex    CExecAppCtrl::mtxGuard;

/*
 * @brief   インスタンス取得
 * @param   なし
 * @retval  インスタンス
 * @note    static
 */
CExecAppCtrl* CExecAppCtrl::GetInstance(void)
{
    boost::mutex::scoped_lock look(mtxGuard);
    if (ms_pInstance == NULL) 
    {
        ms_pInstance = new CExecAppCtrl;
    }
    return ms_pInstance;
}

/*
 * @brief   インスタンス削除
 * @param   なし
 * @retval  なし
 * @note    static
 */
void CExecAppCtrl::DeleteInstance(void)
{
    if (ms_pInstance != NULL) 
    {
        delete ms_pInstance;
        ms_pInstance = NULL;
    }
}

/*
 * @brief   デストラクタ
 * @param   なし
 * @retval  なし
 * @note    
 */
CExecAppCtrl::~CExecAppCtrl()
{

}

/*
 * @brief   アプリケーションモード設定確認
 * @param   [in] eMode 遷移予定アプリケーションモード
 * @retval  true 成功
 * @note    static
 */
bool CExecAppCtrl::CheckAppMode(EXEC_COMMON::E_APP_MODE eModeNext,
                                CObjectDef* pDef)
{
    if (m_eMode == eModeNext)
    {
        switch(m_eMode)
        {
            case MODE_EXEC:
            case MODE_ABORTING:
                if (pDef != m_pExecDef)
                {
                    return false;
                }
                break;
            default:
                break;
        }
        return true;
    }

    switch(m_eMode)
    {
    case MODE_NONE:
        switch(eModeNext)
        {
            case MODE_EDIT:          //編集中
            case MODE_COMPILE:       //編集中
                break;

            default:
                return false;
        }
        break;

    case MODE_EDIT:          //編集中
        switch(eModeNext)
        {
            case MODE_COMPILE:          //コンパイル中
            case MODE_EXEC:             //実行中
                break;

            default:
                return false;
        }
        break;

    case MODE_COMPILE:      //コンパイル中
        switch(eModeNext)
        {
            case MODE_EDIT:          //編集中
            case MODE_COMPILED:      //コンパイル完了
                break;

            default:
                return false;

        }
        break;

    case MODE_COMPILED:      //コンパイル完了
        switch(eModeNext)
        {
            case MODE_EDIT:          //編集中
                break;

            default:
                return false;
        }
        break;

    case MODE_EXEC:          //実行中
        switch(eModeNext)
        {
            case MODE_EXEC:          //実行中
            case MODE_EXEC_PART:     //一部実行中
            case MODE_ABORTING:      //アボート中
                break;

            default:
                return false;
        }
        break;

    case MODE_EXEC_PART:          //一部実行中
        switch(eModeNext)
        {
            case MODE_EXEC:          //実行中
            case MODE_PAUSE:         //一時停止
            case MODE_ABORTING:      //アボート中
                break;

            default:
                return false;
        }
        break;

    case MODE_PAUSE:                 //一時停止
        switch(eModeNext)
        {
            case MODE_EXEC:          //実行中
            case MODE_EXEC_PART:     //一時停止
            case MODE_ABORTING:      //アボート中
                break;

            default:
                return false;
        }
        break;

    case MODE_ABORTING:      //アボート中
        switch(eModeNext)
        {
            case MODE_END:
                break;

            default:
                return false;
        }
        break;

    default:
                return false;
    }
    return true;
}

/**
 * @brief   アプリケーションモード設定
 * @param   [in] eMode プリケーションモード
 * @retval  true 成功
 * @note    static
 */
bool CExecAppCtrl::SetAppMode(EXEC_COMMON::E_APP_MODE eMode,
                              CObjectDef* pDef,
                              bool bSync)
{
    boost::mutex::scoped_lock lk(mtxGuard);

    /*
    if (!CheckAppMode( eMode, pDef))
    {
        return false;
    }
    */

    m_eMode    = eMode;

    if (m_eMode == MODE_EXEC)
    {
        m_pExecDef = pDef;
    }
    else if ((m_eMode == MODE_NONE)||
             (m_eMode == MODE_EDIT)||
             (m_eMode == MODE_COMPILE)||
             (m_eMode == MODE_COMPILED))
    {
        m_pExecDef = NULL;
    }

    CWnd* pWnd;
    pWnd = AfxGetApp()->m_pMainWnd;

    if (!pWnd)
    {
        STD_DBG(_T("AfxGetApp()->m_pMainWnd == NULL"));
        return true;
    }

    if (bSync)
    {
        ::SendMessage(pWnd->m_hWnd, 
            WM_MODE_CHANGE, 
            (WPARAM)m_eMode, 
            (LPARAM)m_pExecDef);
    }
    else
    {
        ::PostMessage(pWnd->m_hWnd, 
            WM_MODE_CHANGE, 
            (WPARAM)m_eMode, 
            (LPARAM)m_pExecDef);
    }
    return true;
}
#endif

//*******************************
//             TEST
//*******************************
#ifdef _DEBUG

#include "Utility/TestDlg.h"

#if 0
void TEST_CExecAppCtrl()
{
    STD_DBG(_T("%s Start\n"), DB_FUNC_NAME.c_str());






    STD_DBG(_T("%s End\n"), DB_FUNC_NAME.c_str());
    STD_DBG(_T("\n"));
}
#endif




void TEST_ExecCommon()
{
    STD_DBG(_T("%s Start\n"), DB_FUNC_NAME.c_str());



    STD_ASSERT ( GetThreadName2Type( _T("IO_1")) == TH_IO_1 );
    STD_ASSERT ( GetThreadName2Type( _T("XX"))  == TH_NONE );

    STD_ASSERT ( GetThreadType2Name( TH_IO_1 ) ==  _T("IO_1"));
    //STD_ASSERT ( GetThreadType2Name( d(1) ) == _T("XX") );

    //TEST_CExecAppCtrl();

    STD_DBG(_T("%s End\n"), DB_FUNC_NAME.c_str());
    STD_DBG(_T("\n"));

}
#endif //_DEBUG