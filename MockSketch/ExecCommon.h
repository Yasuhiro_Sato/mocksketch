/**
 * @brief			ExecCommon ヘッダーファイル
 * @file			ExecCommon.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

#ifndef  _EXEC_COMMON_H__
#define  _EXEC_COMMON_H__
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/

/*---------------------------------------------------*/
/*  Class                                            */
/*---------------------------------------------------*/
class    CObjectDef;

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
//#define SET_APP_MODE(mode, def) EXEC_COMMON::CExecAppCtrl::GetInstance()->SetAppMode(mode,  def)
//#define SET_APP_MODE_ASYNC(mode, def) EXEC_COMMON::CExecAppCtrl::GetInstance()->SetAppMode(mode,  def, false)
//#define CHK_APP_MODE(mode, def) EXEC_COMMON::CExecAppCtrl::GetInstance()->CheckAppMode(mode,  def)
//#define GET_APP_MODE()     EXEC_COMMON::CExecAppCtrl::GetInstance()->GetAppMode()
//#define GET_APP_CTRL()     EXEC_COMMON::CExecAppCtrl::GetInstance()


/*---------------------------------------------------*/
/*  Enums                                            */
/*---------------------------------------------------*/

namespace EXEC_COMMON{

enum E_THREAD_TYPE
{
    TH_NONE   = -2,
    TH_ALL    = -1,

    TH_COMMON   = 0,

    TH_GROUP_1  = 1,
    TH_GROUP_2 ,
    TH_GROUP_3 ,
    TH_GROUP_4 ,
    TH_GROUP_5 ,
    TH_GROUP_MAX ,

    TH_FIELD_1 , //BOX2D
    TH_FIELD_2 ,
    TH_FIELD_3 ,
    TH_FIELD_4 ,
    TH_FIELD_5 ,
    TH_FIELD_MAX ,

    TH_DISP    ,
    TH_PANE    ,

    TH_IO_1    ,
    TH_IO_2    ,
    TH_IO_3    ,

    TH_MAX     ,
};

// ブレークポイント発生時に、その関数を保有しているスレッドを停止する
// 関数名からスレッドを特定するため、以下を定義する。
enum E_FUNCTION_THREAD_TYPE
{
    THREAD_TYPE_NONE,
    THREAD_TYPE_GROUP, // TH_GROUP1〜TH_FIELD_5
    THREAD_TYPE_DISP,  // TH_DISP
    THREAD_TYPE_IO
};

enum E_EXEC_STS
{
    EX_EDIT,
    EX_EXEC,
    EX_BREAK,
};


enum E_SCRIPT_FUNCTION
{
    FUNC_NONE,
    /*
    FUNC_INITIALIZE,
    FUNC_SETUP     ,
    FUNC_LOOP      ,
    FUNC_ABORT     ,
    FUNC_DRAW      ,
    FUNC_ON_CHANGE_PROPERTY,
    FUNC_ON_INIT_NODE_MARKER,
    FUNC_ON_SELECT_NODE_MARKER,
    FUNC_ON_MOVE_NODE_MARKER,
    FUNC_ON_RELEASE_NODE_MARKER,
    FUNC_ON_CHANGE_NODE,
    */
    E_END_OF_FUNC
};


enum E_SCRIPT_METHOD
{
    METHOD_NONE,
    METHOD_CONSTRUCTOR,
    METHOD_SETUP     ,
    METHOD_LOOP      ,
    METHOD_ABORT     ,
    METHOD_DRAW      ,
    METHOD_ON_CHANGE_PROPERTY,
    METHOD_ON_INIT_NODE_MARKER,
    METHOD_ON_SELECT_NODE_MARKER,
    METHOD_ON_MOVE_NODE_MARKER,
    METHOD_ON_RELEASE_NODE_MARKER,
    METHOD_ON_CHANGE_NODE,

    E_END_OF_METHOD
};

//-----------------------
// アプリケーションモード
//-----------------------
/*
enum E_APP_MODE
{
    MODE_NONE,
    MODE_EDIT,          //編集中
    MODE_COMPILE,       //コンパイル中
    MODE_COMPILED,      //コンパイル完了
    MODE_EXEC,          //実行中
    MODE_EXEC_PART,     //一部実行中
    MODE_PAUSE,         //一時停止
    MODE_ABORTING,      //アボート中
    MODE_END,           //終了
};
*/

//-----------------------
// ステップ実行モード
//-----------------------
enum E_STEP_MODE
{
    STEP_NONE,      //ステップ実行なし (含 CONTINUE)
    STEP_IN,        //ステップイン     (関数内部に入る)
    STEP_OVER,      //ステップオーバー (関数内部に入らない)
    STEP_OUT,       //ステップアウト   (今実行している関数の外（呼び出し元）に出るまでプログラムを進める)
};



//------------------------------
// デバッグモードについて
//     部品定義で基本のデバッグモードを設定
//   部品側がFOLLOWになっている場合は
//   部品定義の設定に合わせる
//    
enum DEBUG_MODE
{
    EDB_DEBUG    = 0,
    EDB_RELEASE  = 1,
    EDB_FOLLOW_SETTING = 2, // 部品のみ
    EDB_ERROR    = -1,
};



/**
 * @class   CFunctionProperty
 * @brief   
 * @Note
 */
class CFunctionProperty
{
public:
    CFunctionProperty():eThreadType(THREAD_TYPE_NONE),
        eMethod  (METHOD_NONE){;}

    CFunctionProperty(E_FUNCTION_THREAD_TYPE eType,
                      E_SCRIPT_METHOD eScriptMethod):
    eThreadType(eType),
    eMethod  (eScriptMethod){;}

public:
    E_FUNCTION_THREAD_TYPE eThreadType;
    E_SCRIPT_METHOD      eMethod;

};


/**
 * @class   CExecAppCtrl
 * @brief   アプリケーションの実行状態を保持
 * @Note    本来は、アプリケーションのインスタンス
 *          として定義するべきと考えられるが、
 *          後々ライブラリを分離しやすくするため
 *          別定義とする
 */
#if 0
class CExecAppCtrl
{
public:
    //!< インスタンス取得
    static CExecAppCtrl* GetInstance();

    //!< インスタンス取得
    static void DeleteInstance ();

    //!< アプリケーションモード取得
    E_APP_MODE  GetAppMode(){return m_eMode;}

    //!< アプリケーションモード設定確認
    bool CheckAppMode(E_APP_MODE eMode, CObjectDef* pDef);

    //!< アプリケーションモード設定
    bool SetAppMode(E_APP_MODE eMode, CObjectDef* pDef, bool bSync = true);

    //!< 実行中オブジェクト定義取得
    CObjectDef*  GetExecDef(){return m_pExecDef;}

private:
    //!< コンストラクタ
    CExecAppCtrl():m_eMode(MODE_NONE),
                   m_pExecDef(NULL) {;}

    //!< デストラクタ
    virtual ~CExecAppCtrl();


private:
    static CExecAppCtrl*    ms_pInstance;

    E_APP_MODE              m_eMode;

    CObjectDef*             m_pExecDef;

    static boost::mutex     mtxGuard;
};
#endif

void InitName();

StdString     GetThreadType2Name(E_THREAD_TYPE eType);

E_THREAD_TYPE GetThreadName2Type( StdString strName);

StdString     GetDebugMode2Name(DEBUG_MODE eType);

DEBUG_MODE    GetDebugMode2Type( StdString strName);


CFunctionProperty* GetFunctionProperty(const char* strFunction);



};//namespace EXEC_COMMON


#endif  //_EXEC_COMMON_H__
