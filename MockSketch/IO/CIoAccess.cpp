/**
 * @brief			CIoAccess実装ファイル
 * @file			CIoAccess.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CIoAccess.h"
#include "./CIoAccessBoard.h"
#include "./CIoAccessTcp.h"
#include "./CIoPropertyBlock.h"
#include "./CIoProperty.h"
#include "./IIoDef.h"
#include "System/CSystem.h"
#include "Utility/CUtility.h"

/*---------------------------------------------------*/
/* Static variables                                  */
/*---------------------------------------------------*/
namespace{
    using namespace IO_COMMON;
    static const DT_IO_TYPE  initIoType[] ={
                                                IT_NONE  ,
                                                IT_DI    ,
                                                IT_DO    ,
                                                IT_AI_8  ,
                                                IT_AO_8  ,
                                                IT_AI_10 ,
                                                IT_AO_10 ,
                                                IT_AI_12 ,
                                                IT_AO_12 ,
                                                IT_AI_14 ,
                                                IT_AO_14 ,
                                                IT_AI_16 ,
                                                IT_AO_16 ,
                                                IT_AI_18 ,
                                                IT_AO_18 ,
                                                IT_AI_20 ,
                                                IT_AO_20 ,
                                                IT_AI_24 ,
                                                IT_AO_24 ,
                                                IT_AI_32 ,
                                                IT_AO_32 ,
                                                IT_LONG_I   ,
                                                IT_LONG_O   ,
                                                IT_FLOAT_I  ,
                                                IT_FLOAT_O  ,
                                                IT_DOUBLE_I ,
                                                IT_DOUBLE_O ,
                                                IT_STR_I    ,
                                                IT_STR_O    ,
                                                IT_SIO      
                                                };
}

std::vector<IO_COMMON::DT_IO_TYPE> 
CIoAccess::ms_lstAllType( initIoType, 
                          initIoType + 
                          sizeof(initIoType)/ sizeof(IO_COMMON::DT_IO_TYPE));


/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CIoAccess::CIoAccess():
m_eAccessType   (ACCESS_NOMAL),
m_eState        (STS_NONE),
m_pDef          (NULL),
m_iMaxPage      (1),
//m_iMaxItem      (256),
m_iMaxPageSize  (2065/*2048*/),
m_bMassUpdate   (false),
m_bRef          (false)
{
}

/**
 *  @brief  コピーコンストラクタ.
 *  @param  なし
 *  @retval なし     
 *  @note   バッファーの内容はコピーしない
 */
CIoAccess::CIoAccess(const CIoAccess& acccess):
m_eState    (STS_NONE)
{
    size_t  iSize;

    m_iMaxPage          =   acccess.m_iMaxPage;
    //m_iMaxItem          =   acccess.m_iMaxItem;
    m_iMaxPageSize      =   acccess.m_iMaxPage;
    m_bMassUpdate       =   acccess.m_bMassUpdate;
    m_bRef              =   acccess.m_bRef;
    m_eAccessType       =   acccess.m_eAccessType;
    m_strUserDefItem    =   acccess.m_strUserDefItem;

    iSize = acccess.m_lstBuffer.size();
    m_lstBuffer.resize(iSize);

    int     iCnt = 0;
    foreach(std::vector<char>& vec, m_lstBuffer)
    {
        iSize = acccess.m_lstBuffer[iCnt].size();
        vec.resize(iSize);
        memset(&vec[0], 0, iSize);
    }

    iSize = acccess.m_lstSioBuf.size();
    m_lstSioBuf.resize(iSize);

    //!< 必要に応じて生成元に設定し直す
    m_pDef = acccess.m_pDef;
}

/**
 *  @brief  デストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CIoAccess::~CIoAccess()
{


}

/**
 *  @brief   代入演算子
 *  @param   [in] obj 代入元 
 *  @retval  代入値
 *  @note 
 */
CIoAccess& CIoAccess::operator = (const CIoAccess & obj)
{
    size_t  iSize;

    m_iMaxPage          =   obj.m_iMaxPage;
    m_iMaxPageSize      =   obj.m_iMaxPageSize;
    m_bMassUpdate       =   obj.m_bMassUpdate;
    m_bRef              =   obj.m_bRef;
    m_eAccessType       =   obj.m_eAccessType;
    m_strUserDefItem    =   obj.m_strUserDefItem;
    m_eState            =   obj.m_eState;

    iSize = obj.m_lstBuffer.size();
    m_lstBuffer.resize(iSize);

    int     iCnt = 0;
    foreach(std::vector<char>& vec, m_lstBuffer)
    {
        iSize = obj.m_lstBuffer[iCnt].size();
        vec.resize(iSize);
        memset(&vec[0], 0, iSize);
    }

    iSize = obj.m_lstSioBuf.size();
    m_lstSioBuf.resize(iSize);

    //!< 必要に応じて生成元に設定し直す
    m_pDef = obj.m_pDef;
    return *this;
}

/**
 * @brief   等価演算子
 * @param   [in] obj 値
 * @return  ture 同値
 * @note    
 */
bool CIoAccess::operator == (const CIoAccess & obj) const
{
    if (m_iMaxPage      != obj.m_iMaxPage)      {return false;}
    if (m_iMaxPageSize  != obj.m_iMaxPageSize)  {return false;}
    if (m_bMassUpdate   != obj.m_bMassUpdate)   {return false;}
    if (m_bRef          != obj.m_bRef)          {return false;}

    return true;
}

/**
 * @brief   不等価演算子
 * @param   [in] obj 値
 * @return  false 同値
 * @note    
 */
bool CIoAccess::operator != (const CIoAccess & obj) const
{
    return !(*this == obj);
}

/**
 *  @brief  IoDe設定
 *  @param  [in] pDef
 *  @retval true 成功 
 *  @note
 */
bool CIoAccess::SetIoDef(CIoDefBase* pDef)
{
    m_pDef = pDef;
    return true;
}

/**
 *  @brief  データ同期
 *  @param  [in] sync 同期元
 *  @retval true: 成功
 *  @note
 */
bool CIoAccess::SyncData(const CIoAccess& sync)
{
    m_iMaxPage      = sync.m_iMaxPage;
    //m_iMaxItem      = sync.m_iMaxItem;
    m_iMaxPageSize  = sync.m_iMaxPageSize;
    m_bMassUpdate   = sync.m_bMassUpdate;
    return true;
}


/**
 *  @brief  クローン生成
 *  @param  [in] pObj
 *  @retval CIoAccess
 *  @note
 */
std::shared_ptr<CIoAccess>  CIoAccess::Clone(CIoAccess* pObj)
{
    std::shared_ptr<CIoAccess> pNew;
    const type_info& info = typeid( *pObj );  // 実行時型情報を取得

    CIoAccess* pTmpObj = const_cast<CIoAccess*>(pObj);
    if( info == typeid( CIoAccessBoard ) )
    {
        CIoAccessBoard* pBoard = dynamic_cast<CIoAccessBoard*>(pTmpObj);
        pNew = std::make_shared<CIoAccessBoard>(*pBoard);
    }
    else if( info == typeid( CIoAccessTcp ) )
    {
        CIoAccessTcp* pTcp = dynamic_cast<CIoAccessTcp*>(pTmpObj);
        pNew = std::make_shared<CIoAccessTcp>(*pTcp);
    }
    else if( info == typeid( CIoAccess ) )
    {
        pNew = std::make_shared<CIoAccess>(*pTmpObj);
    }
    return pNew;
}


/**
 *  @brief  データ取得
 *  @param  [out] pVal
 *  @param  [in]  iIoId
 *  @param  [in]  iLen
 *  @retval true: 成功     
 *  @note
 */
bool CIoAccess::GetDataBuf(void* pVal, 
                           int iIoId, 
                           int iLen) const
{
    DBG_ASSERT(m_pDef);

    CIoProperty* pProp = m_pDef->GetBlock()->GetProperty(iIoId);

    if (!pProp)
    {
        return NULL;
    }

    int iBufferPos = pProp->GetBufferPos();
    int iPage      = pProp->GetPage();

    DBG_ASSERT ( m_lstBuffer.size() >= static_cast<size_t>(iPage) );
    DBG_ASSERT ( m_lstBuffer[iPage].size() >= static_cast<size_t>(iBufferPos) );

    memcpy( pVal, &m_lstBuffer[iPage].at(iBufferPos), iLen);
    return true;
}

/**
 *  @brief  データ設定
 *  @param  [in]  pVal
 *  @param  [in]  iIoId
 *  @param  [in]  iLen
 *  @retval true: 成功     
 *  @note
 */
bool CIoAccess::SetDataBuf(void* pVal, 
                        int iIoId, 
                        int iLen)
{

    memcpy( GetBufAddress(iIoId), pVal, iLen);
    return true;
}

/**
 *  @brief  データアドレス取得
 *  @param  [in]  iId
 *  @retval 該当バッファのアドレス
 *  @note
 */
char* CIoAccess::GetBufAddress(int iIoId)
{
    DBG_ASSERT(m_pDef);

    CIoProperty* pProp = m_pDef->GetBlock()->GetProperty(iIoId);

    if (!pProp)
    {
        return NULL;
    }

    int iBufferPos = pProp->GetBufferPos();
    int iPage      = pProp->GetPage();

    return GetBufAddress(iPage, iBufferPos);
}

/**
 *  @brief  データアドレス取得
 *  @param  [in]  iBufferPos
 *  @retval 該当バッファのアドレス
 *  @note
 */
char* CIoAccess::GetBufAddress(int iPage, int iBufferPos)
{
    DBG_ASSERT ( m_lstBuffer.size() >= static_cast<size_t>(iPage) );
    DBG_ASSERT ( m_lstBuffer[iPage].size() >= static_cast<size_t>(iBufferPos) );
    return &m_lstBuffer[iPage].at(iBufferPos);
}

/**
 *  @brief  ページアドレス取得
 *  @param  [in]  iPage
 *  @retval 該当バッファのアドレス
 *  @note
 */
char* CIoAccess::GetPageAddress(int iPage)
{
    DBG_ASSERT ( m_lstBuffer.size() >= static_cast<size_t>(iPage) );
    return &m_lstBuffer[iPage].at(0);
}

/**
 *  @brief  データ取得
 *  @param  [out] pVal
 *  @param  [in]  iPage
 *  @param  [in]  iId
 *  @param  [in]  iLen
 *  @retval true: 成功     
 *  @note  Boradで使用する
 */
bool CIoAccess::GetData(void* pVal, 
                        int iPage, 
                        int iPos, 
                        int iLen) const
{
    return false;
}


/**
 *  @brief  データ設定
 *  @param  [in]  pVal
 *  @param  [in]  iPage
 *  @param  [in]  iId
 *  @param  [in]  iLen
 *  @retval true: 成功     
 *  @note   Boradで使用する
 */
bool CIoAccess::SetData(void* pVal, 
                        int iPage, 
                        int iPos, 
                        int iLen)
{
    return false;
}

/**
 *  @brief  データ更新
 *  @param  なし
 *  @retval true: 成功     
 *  @note   Boradで使用する
 */
bool CIoAccess::Update(int iPage)
{



    return false;
}


/**
 *  @brief  種別取得
 *  @param  なし
 *  @retval AccessType種別
 *  @note
 */
CIoAccess::E_ACCESS_TYPE CIoAccess::GetAccessType() const
{
    return m_eAccessType;
}

/**
 *  @brief  ページ最大数取得
 *  @param  なし
 *  @retval ページ数
 *  @note
 */
int CIoAccess::GetMaxPage() const
{
    return m_iMaxPage;
}


/**
 *  @brief  ページ最大数設定
 *  @param  [in] iMaxPage ページ数
 *  @retval true 成功
 *  @note
 */
bool CIoAccess::SetMaxPage(int iMaxPage)
{
    if (m_eState > STS_FINISHED)
    {
        return false;
    }

    if (iMaxPage <= 0)
    {
        return false;
    }

    if (iMaxPage >= 256)
    {
        // 256以上は不可
        //（多すぎる)
        return false;
    }
    m_iMaxPage = iMaxPage;
    return true;
}

/**
 *  @brief  最大ページサイズ取得
 *  @param  なし
 *  @retval 最大ページサイズ(byte)
 *  @note
 */
int CIoAccess::GetMaxPageSize() const
{
    return m_iMaxPageSize;
}

/**
 *  @brief  最大ページサイズ設定
 *  @param  [in] iMaxPageSize 最大ページサイズ(byte)
 *  @retval true:成功
 *  @note
 */
bool CIoAccess::SetMaxPageSize(int iMaxPageSize)
{
    if (m_eState > STS_FINISHED)
    {
        return false;
    }

    if (iMaxPageSize <= 0)
    {
        return false;
    }

    m_iMaxPageSize = iMaxPageSize;

    return true;
}

/**
 *  @brief  一括更新の有無取得
 *  @param  なし
 *  @retval true:一括更新あり
 *  @note
 */
bool  CIoAccess::IsMassUpdate() const
{
    return m_bMassUpdate;
}

/**
 *  @brief  一括更新の有無設定
 *  @param  [in] bUse true:一括更新あり
 *  @retval true:成功
 *  @note
 */
bool  CIoAccess::SetMassUpdate(bool bUse)
{
    if (m_eState > STS_FINISHED)
    {
        return false;
    }

    m_bMassUpdate = bUse;
    return true;
}

/**
 *  @brief  最大アイテム数取得
 *  @param  なし
 *  @retval 最大アイテム数
 *  @note
 */
/*
int CIoAccess::GetMaxItem() const
{
    return m_iMaxItem;
}
*/

/**
 *  @brief  最大アイテム数設定
 *  @param  [in] 最大アイテム数
 *  @retval true:成功
 *  @note
 */
/*
bool  CIoAccess::SetMaxItem(int iMaxItem)
{
    if (m_eState > STS_FINISHED)
    {
        return false;
    }

    if (iMaxItem <= 0 )
    {
        return false;
    }


    m_iMaxItem = iMaxItem;

    return true;
}
*/

/**
 *  @brief  初期化
 *  @param  なし
 *  @retval true:成功
 *  @note
 */
bool CIoAccess::Init()
{
    if (m_eState >= STS_SETUP)
    {
        return false;
    }

    //先にIoDefを設定しておく
    if(m_pDef == NULL)
    {
        return false;
    }

    m_lstBuffer.resize(m_iMaxPage);
    foreach( std::vector<char>& Buf, m_lstBuffer)
    {
        Buf.resize(m_iMaxPageSize);
    }

    m_eState = STS_INITILIZED;

    return true;
}


/**
 *  @brief  開始前処理
 *  @param  なし
 *  @retval true:成功
 *  @note   CIoDefBase::SetupStartから使用する
 */
bool CIoAccess::SetupStart()
{
    if(m_pDef == NULL)
    {
        return false;
    }


    CIoPropertyBlock* pBlock;
    pBlock = m_pDef->GetBlock();

    
    int iRow;
    int iIoId;
    CIoProperty* pProp;

    //---------------------
    // IOバッファーの設定
    //---------------------
    m_lstBuffer.clear();
    for (int iPage = 0; iPage < m_iMaxPage; iPage++)
    {
        iRow = pBlock->GetRowMax(iPage) - 1;

        if (iRow < 0) {return false;}

        iIoId = pBlock->GetIoId(iPage, iRow);

        if (iIoId < 0) {return false;}

        pProp = pBlock->GetProperty(iIoId);
        if (pProp == NULL){return false;}

        int iMaxPageSize;
        iMaxPageSize  = pProp->GetBufferPos();
        iMaxPageSize += pProp->GetLen();

        std::vector<char> lstBuf;
        lstBuf.resize(iMaxPageSize);
        memset(&lstBuf[0], 0, iMaxPageSize);

        m_lstBuffer.push_back(lstBuf);
    }

    m_eState = STS_INITILIZED;

    return true;
}

/**
 *  @brief  アイテム設定制限
 *  @param  [out] pLstType
 *  @param  [in] PageNo
 *  @param  [in] ItemNo
 *  @retval true:成功
 *  @note
 */
bool  CIoAccess::GetItemRestriction(std::vector<IO_COMMON::DT_IO_TYPE>* pLstType, 
                                                      int  PageNo,
                                                      int  ItemNo) const
{
    //CIoAccessでは制限なし
    *pLstType = ms_lstAllType;
    return true;
}

/**
 *  @brief  アイテム設定
 *  @param  [in] eType
 *  @param  [in] PageNo
 *  @param  [in] ItemNo
 *  @retval true:成功
 *  @note   CIoBoardAccessで使用予定
 */
bool  CIoAccess::SetItemDef (IO_COMMON::DT_IO_TYPE eType,  
                                         int  PageNo, 
                                         int  ItemNo)
{
    //CIoAccessでは処理なし
    return true;
}

/**
 *  @brief  ユーザ定義アイテム設定問合わせ
 *  @param  [inOut] StdString& strSet
 *  @param  [in]    ItemNo
 *  @param  [in]    PageNo
 *  @param  [in]    iPos
 *  @retval true:設定可
 *  @note   CIoBoardAccessで使用予定
 *          
 */              
bool CIoAccess::IsSetUserItem(StdString& strSet,
                              int iItemNo, 
                              int iPage, int iRow) const
{
    return true;
}

/**
 *  @brief  ユーザ定義アイテム変更前問合わせ
 *  @param  [out] pListCombo  コンボボックス項目
 *  @param  [in]  ItemNo
 *  @param  [in]  PageNo
 *  @param  [in]  iPos
 *  @retval true 変更可
 *  @note   CIoBoardAccessで使用予定
 *          
 */
bool CIoAccess::PreChangeUserItem(std::vector<StdString>* pListCombo,
                                  int iItemNo, 
                                  int iPage, int iRow) const
{
    // ユーザ定義アイテムにコンボボックスを表示させたいとこに
    // 使う

    if (pListCombo)
    {
        pListCombo->clear();
    }
    return true;
}


/**
 *  @brief  ユーザ定義アイテム数
 *  @param  なし
 *  @retval アイテム数
 *  @note   CIoBoardAccessで使用予定
 *          ボード側からCIoDefに対して ユーザー定義項目を
 *          設定したい場合に使う(ex Contec AioSetAoRange等)
 *
 *
 */
int CIoAccess::GetUserItemNumber()  const
{
    return static_cast<int>(m_strUserDefItem.size());
}

/**
 *  @brief  ユーザ定義アイテム取得
 *  @param  [in] アイテム番号
 *  @retval ユーザ定義アイテムの定義
 *  @note
 */
StdString CIoAccess::GetUserItem(int iItemNo) const
{
    if (GetUserItemNumber() <= iItemNo) {return _T("");}
    if (iItemNo < 0)                    {return _T("");}

    return m_strUserDefItem[iItemNo];
}

/**
 * @brief   シリアル化(ロード)
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  なし
 * @note	
 */
template<class Archive>
void CIoAccess::load(Archive& ar, const unsigned int version)
{
    SERIALIZATION_INIT;

        SERIALIZATION_LOAD("MaxPage"       , m_iMaxPage);
        SERIALIZATION_LOAD("MaxPageSize"   , m_iMaxPageSize);
        SERIALIZATION_LOAD("MassUpdate"    , m_bMassUpdate);
        SERIALIZATION_LOAD("Ref"           , m_bRef);
        m_pDef = NULL;
        m_eState       = STS_NONE;

    MOCK_EXCEPTION_FILE(e_file_read);
}

/**
 * @brief   シリアル化(セーブ)
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  なし
 * @note	
 */
template<class Archive>
void CIoAccess::save(Archive& ar, const unsigned int version) const
{
    SERIALIZATION_INIT;

        SERIALIZATION_SAVE("MaxPage"       , m_iMaxPage);
        SERIALIZATION_SAVE("MaxPageSize"   , m_iMaxPageSize);
        SERIALIZATION_SAVE("MassUpdate"    , m_bMassUpdate);
        SERIALIZATION_SAVE("Ref"           , m_bRef);

    MOCK_EXCEPTION_FILE(e_file_write);
}


//load save のインスタンスが生成されないため
//ダミーとして実装
void CIoAccess::Dummy()
{
    unsigned int iVersion = 0;

    StdStreamOut outFs(_T(""));
    StdStreamIn  inFs(_T(""));

    boost::archive::text_woarchive txtOut(outFs); 
    boost::archive::text_wiarchive txtIn(inFs);

    save<boost::archive::text_woarchive>( txtOut, iVersion);
    load<boost::archive::text_wiarchive>( txtIn,  iVersion);


    StdXmlArchiveOut outXml(outFs);
    StdXmlArchiveIn inXml(inFs);

    save<StdXmlArchiveOut>(outXml, iVersion);
    load<StdXmlArchiveIn>(inXml, iVersion);

    boost::filesystem::ofstream outFsStd(_T(""));
    boost::filesystem::ifstream inFsStd(_T(""));

    boost::archive::binary_oarchive outBin(outFsStd);
    boost::archive::binary_iarchive inBin(inFsStd);

    save<boost::archive::binary_oarchive>(outBin, iVersion);
    load<boost::archive::binary_iarchive>(inBin, iVersion);


}

//*******************************
//             TEST
//*******************************
#ifdef _DEBUG

#include "CIoDef.h"

void Test_Normal()
{
    CIoDef    ioDef;        //Io定義

    CIoAccess ioAccess;

    // IO設定ダイアログで設定する
    // 通常のIO定義では Page数の定義だけでよいはず

    ioAccess.SetMaxPage(5);         //  5Page
    //ioAccess.SetMaxPageSize(100);   //  100Byte  <<<

    
    ioAccess.Init();                //  初期化

    STD_ASSERT( ioAccess.IsMassUpdate() == false);
    STD_ASSERT( ioAccess.GetMaxPage() == 5);


    STD_ASSERT( ioAccess.IsMassUpdate() == false);
}

void TEST_CIoAccess_File()
{

    CIoAccess access1;
    CIoAccess access2;

    namespace fs = boost::filesystem;
    StdPath  pathTest = CSystem::GetSystemPath();
    pathTest /= _T("TEST_CIoAccess.xml");

    //保存
    StdStreamOut outFs(pathTest);
    {
        StdXmlArchiveOut outXml(outFs);
        outXml << boost::serialization::make_nvp("Root", access1);
    }
    outFs.close();


    //読込
    StdStreamIn inFs(pathTest);
    {
        StdXmlArchiveIn inXml(inFs);
        inXml >> boost::serialization::make_nvp("Root", access2);
    }
    inFs.close();
}




void TEST_CIoAccess()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    Test_Normal();

    TEST_CIoAccess_File();

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));

}
#endif //_DEBUG