/**
 * @brief			CIoAccess ヘッダーファイル
 * @file			CIoAccess.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

#ifndef  _IO_ACCESS_H__
#define  _IO_ACCESS_H__
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CIoCommon.h"

/*---------------------------------------------------*/
/*  Class                                            */
/*---------------------------------------------------*/
class CIoDefBase;

/*---------------------------------------------------*/
/*  Enums                                            */
/*---------------------------------------------------*/


//       1         2         3         4         5         6         7         8
//345678901234567890123456789012345678901234567890123456789012345678901234567890

/**
 * @class   CIoAccess
 * @brief                       
 */
class CIoAccess
{
public:
    //--------------------
    // シリアルバッファー
    //--------------------
    struct SioBuffer
    {
        std::vector<char> lstIn;
        std::vector<char> lstOut;
    };

    //--------------------
    // IoAccess種別
    //--------------------
    enum E_ACCESS_TYPE
    {
        ACCESS_NOMAL,
        ACCESS_BOARD,
        ACCESS_TCP,
    };

    //--------------------
    // IoAccess状態
    //--------------------
    enum E_ACCESS_STATE
    {
        STS_NONE,
        STS_INIT,
        STS_INITILIZED,
        STS_FINISHED,
        //                   ↑ 設定変更可
        //----------------------------------
        //                   ↓ 設定変更不可
        STS_SETUP,
        STS_STARTED,
        STS_STOPPED,
        STS_FINISHING,
    };


public:

    //!< コンストラクタ
    CIoAccess();

    //!< コピーコンストラクタ
    CIoAccess(const CIoAccess& acccess);

    //!< デストラクタ
    virtual ~CIoAccess();

    //!< 代入演算子
    virtual CIoAccess& operator = (const CIoAccess & obj);

    //!< 等価演算子
    virtual bool operator == (const CIoAccess & obj) const;

    //!< 不等価演算子
    virtual bool operator != (const CIoAccess & obj) const;

    // IO定義設定
    virtual bool SetIoDef(CIoDefBase* pDef);

    //!< データ同期
    virtual bool SyncData(const CIoAccess& sync);

    //!< クローン生成
    static std::shared_ptr<CIoAccess>  Clone(CIoAccess* pObj);

    //!< データ取得(バッファ)
    virtual bool GetDataBuf(void* pVal, int iIoId, int iLen) const;

    //!< データ設定(バッファ)
    virtual bool SetDataBuf(void* pVal, int iIoId, int iLen);

    //!< データ取得(バッファ)
    virtual char* GetBufAddress(int iIoId);

    //!< データ取得(バッファ)
    virtual char* GetBufAddress(int iPage, int iBufferPos);

    //!< ページアドレス取得(バッファ)
    virtual char* GetPageAddress(int iPage);

    //--------------
    // ユーザアイテム
    //--------------
    //!< ユーザ定義アイテム数
    virtual int GetUserItemNumber()  const;

    //!< ユーザ定義アイテム取得
    virtual StdString GetUserItem(int iItemNo) const;

    //!< ユーザ定義アイテム設定問合わせ
    virtual bool IsSetUserItem(StdString& strSet, 
                               int iItemNo, int iPage, int iRow) const;

    //!< ユーザ定義アイテム変更前問合わせ
    virtual bool PreChangeUserItem(std::vector<StdString>* listCombo,
                                          int iItemNo, int iPage, int iRow) const;

    //!< ユーザ定義アイテム設定(テスト用)
#ifdef _DEBUG
    void AddUserItem(StdString strItem)
    {
        m_strUserDefItem.push_back(strItem);
    }
#endif

    //--------------
    // 動作
    //--------------
    // 初期化
    virtual bool Init();

    // リセット
    virtual bool Reset()   { return true;}

    // 開始前処理
    virtual bool SetupStart();

    // 開始
    virtual bool Start()    { return true;}

    //!< データ取得
    virtual bool GetData(void* pVal, int iPage, int iPos, int iLen) const;

    //!< データ設定
    virtual bool SetData(void* pVal, int iPage, int iPos, int iLen);

    //!< データ更新
    virtual bool Update(int iPage);

    // 停止
    virtual bool Stop()    { return true;}

    // 終了
    virtual bool Finish()   { return true;}

    //--------------
    // プロパティ
    //--------------
    //!< 種別
    virtual E_ACCESS_TYPE GetAccessType() const;

    //!< ページ最大数取得
    virtual int GetMaxPage() const;

    //!< ページ最大数設定
    virtual bool SetMaxPage(int iMaxPage);

    //最大ページサイズ取得
    virtual int  GetMaxPageSize() const;
    
    //最大ページサイズ設定
    virtual bool SetMaxPageSize(int iMaxPageSize);

    //一括更新の有無取得
    virtual bool  IsMassUpdate() const;  

    //一括更新の有無設定
    virtual bool  SetMassUpdate(bool bUse);  

    //最大アイテム数取得
    //virtual int   GetMaxItem() const;

    //最大アイテム数設定
    //virtual bool  SetMaxItem(int iMaxItem);

    //--------------

    //アイテム設定制限
    virtual bool  GetItemRestriction(std::vector<IO_COMMON::DT_IO_TYPE>* pLstType, 
                                                      int  PageNo,
                                                      int  ItemNo) const;

    //アイテム設定
    virtual bool  SetItemDef (IO_COMMON::DT_IO_TYPE eType,  
                                         int  PageNo, 
                                         int  ItemNo);


protected:
friend CIoAccess;
    //!< ダミー
    void Dummy();

protected:
    static std::vector<IO_COMMON::DT_IO_TYPE> ms_lstAllType;

protected:
    //----------------
    // 保存データ
    //----------------
    //最大ページ数
    int                   m_iMaxPage;

    //最大ページサイズ(入力制限に使用)
    int                   m_iMaxPageSize;

    //一括更新
    bool                  m_bMassUpdate;

    //参照データ
    bool                  m_bRef;

    //----------------
    // 非保存データ
    //----------------
    E_ACCESS_TYPE                   m_eAccessType;

    //!< IOステータス
    E_ACCESS_STATE                  m_eState;

    //!< ユーザ定義アイテム(Boardのみで使用予定)
    std::vector<StdString>          m_strUserDefItem;

    //!< IOバッファ
    std::vector< std::vector<char> > m_lstBuffer;

    //!< シリアル用バッファ
    std::vector<SioBuffer>          m_lstSioBuf;

    //!< IO定義
    CIoDefBase*                     m_pDef;




private:
    friend class boost::serialization::access;  
    BOOST_SERIALIZATION_SPLIT_MEMBER();

    template<class Archive>
    void load(Archive& ar, const unsigned int version);

    template<class Archive>
    void save(Archive& ar, const unsigned int version) const;

};

#endif  //_IO_ACCESS_H__
