/**
 * @brief			CIoAccessBoard実装ファイル
 * @file			CIoAccessBoard.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CIoAccessBoard.h"
#include "System/CSystem.h"
#include "Utility/CUtility.h"
#include <boost/serialization/export.hpp> 
BOOST_CLASS_EXPORT(CIoAccessBoard);

/*---------------------------------------------------*/
/*  MACROS                                           */
/*---------------------------------------------------*/
#define REGSTER_FUNC(strErrFunc, funcName)                  \
    m_##funcName = reinterpret_cast<F_##funcName>(::GetProcAddress(m_hAddin, #funcName)); \
    if (m_##funcName == 0)          \
    {                               \
        strErrFunc = _T(#funcName); \
        m_hAddin   = 0;             \
        return false;               \
    }                               \

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CIoAccessBoard::CIoAccessBoard():
CIoAccess(),
m_iBoardNo  (0),
m_hAddin    (0),
m_GetBoardName      (0),

//m_GetBoardTypeNum   (0),
//m_GetBoardTypeName  (0),
//m_SetBoardType      (0),
m_GetBoardNoMax     (0),
m_UseBoard          (0),    //ボード使用
m_ReleaseBoard      (0),    //ボード使用解除

m_GetMaxPage        (0),
m_IsMassUpdate      (0),
m_GetMaxUpdateSize  (0),
m_GetMaxItem        (0),
m_GetMaxPageSize    (0),
m_GetItemRestriction(0),
m_GetItemDef        (0),

m_GetUserItemMax    (0),
m_GetUserItemName   (0),
m_GetUserItemDef    (0),
m_SetUserItem       (0),

m_GetData           (0),
m_SetData           (0),

m_Reset             (0),
m_Init              (0),
m_Setup             (0),
m_Start             (0),
m_Stop              (0),
m_Finish            (0),

m_SetLang           (0)
{
    m_eAccessType  = ACCESS_BOARD;
}

/**
 *  @brief  デストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CIoAccessBoard::~CIoAccessBoard()
{
}

/**
 *  @brief  DLL設定
 *  @param  [ouy] pStrErrorFunc エラー発生関数名
 *  @param  [in]  hModule DLLハンドル
 *  @retval true: 成功   
 *  @note
 */
bool CIoAccessBoard::SetDll(StdString* pStrErrorFunc, HMODULE hModule)
{
    if (m_hAddin != 0)
    {
        return false;
    }

    m_hAddin = hModule; 

    //エラー発生時は m_hAddinは0に設定される。

    REGSTER_FUNC(*pStrErrorFunc,  GetBoardName       );
                             
    //REGSTER_FUNC(*pStrErrorFunc,  GetBoardTypeNum    );
    //REGSTER_FUNC(*pStrErrorFunc,  GetBoardTypeName   );
    //REGSTER_FUNC(*pStrErrorFunc,  SetBoardType       );
    REGSTER_FUNC(*pStrErrorFunc,  GetBoardNoMax      );
    REGSTER_FUNC(*pStrErrorFunc,  UseBoard           );
    REGSTER_FUNC(*pStrErrorFunc,  ReleaseBoard       );
                             
    REGSTER_FUNC(*pStrErrorFunc,  GetMaxPage         );
    REGSTER_FUNC(*pStrErrorFunc,  IsMassUpdate       );
    REGSTER_FUNC(*pStrErrorFunc,  GetMaxUpdateSize   );
    REGSTER_FUNC(*pStrErrorFunc,  GetMaxItem         );
    REGSTER_FUNC(*pStrErrorFunc,  GetMaxPageSize     );
    REGSTER_FUNC(*pStrErrorFunc,  GetItemRestriction );
    REGSTER_FUNC(*pStrErrorFunc,  GetItemDef         );
                             
    REGSTER_FUNC(*pStrErrorFunc,  GetUserItemMax     );
    REGSTER_FUNC(*pStrErrorFunc,  GetUserItemName    );
    REGSTER_FUNC(*pStrErrorFunc,  GetUserItemDef     );
    REGSTER_FUNC(*pStrErrorFunc,  SetUserItem        );
                             
    REGSTER_FUNC(*pStrErrorFunc,  GetData            );
    REGSTER_FUNC(*pStrErrorFunc,  SetData            );
                             
    REGSTER_FUNC(*pStrErrorFunc,  Reset              );
    REGSTER_FUNC(*pStrErrorFunc,  Init               );
    REGSTER_FUNC(*pStrErrorFunc,  Setup              );
    REGSTER_FUNC(*pStrErrorFunc,  Start              );
    REGSTER_FUNC(*pStrErrorFunc,  Stop               );
    REGSTER_FUNC(*pStrErrorFunc,  Finish             );
                             
    REGSTER_FUNC(*pStrErrorFunc,  SetLang            );

    return true;
}

//!< ボード番号取得
int CIoAccessBoard::GetBoardNo() const
{

    return 0;
}

//!< ボード番号設定
bool CIoAccessBoard::SetBoardNo(int iBoardNo)
{

    return false;
}


/**
 *  @brief  データ同期
 *  @param  なし
 *  @retval true: 成功   
 *  @note   BOARDでは各々のデータが独立しているので
 *          Syncは使用しない
 */
bool CIoAccessBoard::SyncData(const CIoAccess& sync)
{
    return false;
}

/**
 *  @brief  クローン生成
 *  @param  [out] pAccess
 *  @retval なし
 *  @note
 */
/*
void  CIoAccessBoard::Clone(std::shared_ptr<CIoAccess>* pAccess)
{
    //TODO:実装
    *pAccess = std::make_shared<CIoAccessBoard>();
}
*/

/**
 *  @brief  データ取得
 *  @param  [out] pVal
 *  @param  [in]  iPage
 *  @param  [in]  iId
 *  @param  [in]  iLen
 *  @retval true: 成功     
 *  @note
 */
bool CIoAccessBoard::GetData(void* pVal, 
                            int iPage, 
                            int iId, 
                            int iLen) const
{
    bool bRet = false;
    DBG_ASSERT(m_eState == STS_STARTED);

    if (m_GetData)
    {
        bRet = m_GetData(pVal, m_iBoardNo, iPage, iId, iLen);
    }
    return bRet;
}

/**
 *  @brief  データ設定
 *  @param  [in]  pVal
 *  @param  [in]  iPage
 *  @param  [in]  iId
 *  @param  [in]  iLen
 *  @retval true: 成功     
 *  @note
 */
bool CIoAccessBoard::SetData(void* pVal, 
                            int iPage, 
                            int iId, 
                            int iLen)
{
    bool bRet = false;
    DBG_ASSERT(m_eState == STS_STARTED);

    if (m_SetData)
    {
        bRet = m_SetData(pVal, m_iBoardNo, iPage, iId, iLen);
    }
    return bRet;
}



/**
 *  @brief  初期化
 *  @param  なし
 *  @retval true: 成功   
 *  @note
 */
bool CIoAccessBoard::Init()
{
    if (!CIoAccess::Init())
    {
        return false;
    }

    bool bRet = false;
    if (m_eState > STS_FINISHED)
    {
        return false;
    }

    if (m_hAddin == 0)
    {
        return false;
    }


    if (m_Init)
    {
        bRet = m_Init(m_iBoardNo);
        if (bRet)
        {
            m_eState = STS_INITILIZED;
        }
    }
    return bRet;
}

/**
 *  @brief  リセット
 *  @param  なし
 *  @retval true: 成功   
 *  @note   ボードリセット後は要イニシャライズ
 */
bool CIoAccessBoard::Reset()
{
    bool bRet = false;
    if (m_Reset)
    {
        bRet = m_Reset(m_iBoardNo);
        if (bRet)
        {
            m_eState = STS_INIT;
        }
    }
    return bRet;
}

/**
 *  @brief  開始前処理
 *  @param  なし
 *  @retval true: 成功   
 *  @note
 */
bool CIoAccessBoard::Setup()
{
    bool bRet = false;

    
    if (m_eState <= STS_INIT)
    {
        return false;
    }

    if (m_eState > STS_FINISHED)
    {
        return false;
    }

    if (m_Setup)
    {
        bRet = m_Setup(m_iBoardNo);
        if (bRet)
        {
            m_eState = STS_SETUP;
        }
    }
    return bRet;
}

/**
 *  @brief  開始
 *  @param  なし
 *  @retval true: 成功   
 *  @note
 */
bool CIoAccessBoard::Start()
{
    if  ((m_eState != STS_SETUP) &&
         (m_eState != STS_STOPPED))
    {
        return false;
    }


    bool bRet = false;
    if (m_Start)
    {
        bRet = m_Start(m_iBoardNo);
        m_eState = STS_STARTED;
    }
    return bRet;
}

//!< データ更新
bool CIoAccessBoard::Update(int iPage)
{


    return true;
}


/**
 *  @brief  停止
 *  @param  なし
 *  @retval true: 成功   
 *  @note
 */
bool CIoAccessBoard::Stop()
{
    if  ((m_eState != STS_STARTED))
    {
        return false;
    }

    bool bRet = false;
    if (m_Stop)
    {
        bRet = m_Stop(m_iBoardNo);
        m_eState = STS_STOPPED;
    }
    return bRet;
}

/**
 *  @brief  終了
 *  @param  なし
 *  @retval true: 成功   
 *  @note
 */
bool CIoAccessBoard::Finish()
{
    bool bRet = false;
    if (m_Finish)
    {
        bRet = m_Finish(m_iBoardNo);
    }
    return bRet;
}



//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void TEST_CIoAccessBoard()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());


    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));

}
#endif //_DEBUG