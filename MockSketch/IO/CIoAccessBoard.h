/**
 * @brief			CIoAccessBoard ヘッダーファイル
 * @file			CIoAccessBoard.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

#ifndef  _IO_ACCESS_BOARD_H__
#define  _IO_ACCESS_BOARD_H__
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CIoAccess.h"

/*---------------------------------------------------*/
/*  Class                                            */
/*---------------------------------------------------*/

/*---------------------------------------------------*/
/*  Enums                                            */
/*---------------------------------------------------*/

/**
 * @class   CIoAccessBoard
 * @brief                       
 */
class CIoAccessBoard :public CIoAccess
{
    //------------
    // バージョン
    //------------
    typedef int(__stdcall *F_HeaderVersion)();                          //ヘッダーバージョン取得
    

    //------------
    // ボード関連
    //------------
    typedef bool (__stdcall *F_GetBoardName)(LPTSTR, int /*BoardNo*/);             //ボード名称

    //typedef int     (__stdcall *F_GetBoardTypeNum)  ();                     //ボード種別数取得
    //typedef LPCTSTR (__stdcall *F_GetBoardTypeName) (int /*TypeNo*/);       //ボード種別名取得
    //typedef bool    (__stdcall *F_SetBoardType)     (int /*TypeNo*/);       //ボード種別設定
    typedef bool    (__stdcall *F_GetBoardNoMax)    (int*);                     //最大ボード番号取得
    typedef bool    (__stdcall *F_UseBoard)       (int /*BoardNo*/);        //ボード使用
    typedef bool    (__stdcall *F_ReleaseBoard)   (int /*BoardNo*/);        //ボード解放


    //---------------
    // アイテム設定
    //---------------
    typedef bool    (__stdcall *F_GetMaxPage)(int* /*pBoardMax*/, int /*BoardNo*/);                            //最大ページ数

    typedef bool    (__stdcall *F_IsMassUpdate)(bool* pUpdate ,int /*BoardNo*/);                          //一括更新の有無
    typedef bool    (__stdcall *F_GetMaxUpdateSize)(int* /*pMax*/,int /*BoardNo*/);      //一括更新データサイズ

    typedef bool    (__stdcall *F_GetMaxItem)       (int* /*pMax*/,int /*BoardNo*/);                 //最大アイテム数
    typedef bool    (__stdcall *F_GetMaxPageSize)   (int* /*pMax*/,int /*BoardNo*/);                 //最大ページサイズ
    
    typedef bool    (__stdcall *F_GetItemRestriction)(int* /*DT_IO_TYPE[]*/, //アイテム設定制限取得
                                                      int* /*[]size*/,
                                                      int  /*BoardNo*/,
                                                      int  /*PageNo*/,
                                                      int  /*ItemNo*/);      
    typedef bool    (__stdcall *F_GetItemDef)        (int  /*DT_IO_TYPE*/,  //アイテム設定
                                                      int  /*BoardNo*/,
                                                      int  /*PageNo*/, 
                                                      int  /*ItemNo*/);          

    //---------------------
    //ユーザ定義アイテム
    //---------------------
    typedef bool    (__stdcall *F_GetUserItemMax)(int* /*pMax*/,int  /*BoardNo*/);                       //ユーザ定義アイテム最大数
    typedef bool    (__stdcall *F_GetUserItemName)(LPSTR /*ItemHeadName*/, //ユーザ定義アイテム項目名取得
                                                   int   /*BoardNo*/,
                                                   int   /*ItemCol*/);     
    typedef bool    (__stdcall *F_GetUserItemDef) (LPSTR /*ItemDef*/,      //ユーザ定義アイテム定義取得
                                                   int   /*BoardNo*/,
                                                   int   /*ItemCol*/);       
    typedef bool    (__stdcall *F_SetUserItem)(LPCTSTR,  /*UserItem*/      //ユーザ定義アイテム設定
                                                   int   /*BoardNo*/,
                                                   int   /*PageNo*/, 
                                                   int   /*ItemCol*/, 
                                                   int   /*ItemNo*/);     

    //---------------
    // データ取得
    //---------------
    typedef bool    (__stdcall *F_GetData)(void*,       //データ取得
                                           int /*BoardNo*/ ,
                                           int /*PageNo*/,
                                           int /*ItemNo*/,
                                           int /*Length*/);

    typedef bool    (__stdcall *F_SetData)(void*,     //データ設定
                                           int /*BoardNo*/, 
                                           int /*PageNo*/,
                                           int /*ItemNo*/,
                                           int /*Length*/);



    //---------------------
    // 開始・停止
    //---------------------
    typedef bool     (__stdcall *F_Reset) (int /*BoardNo*/);               //ボードリセット
    typedef bool     (__stdcall *F_Init)  (int /*BoardNo*/);               //ボードイニシャライズ
    typedef bool     (__stdcall *F_Setup) (int /*BoardNo*/);               //開始前処理
    typedef bool     (__stdcall *F_Start) (int /*BoardNo*/);               //計測開始
    typedef bool     (__stdcall *F_Stop)  (int /*BoardNo*/);               //計測停止
    typedef bool     (__stdcall *F_Finish)(int /*BoardNo*/);               //終了処理


    //---------------
    // その他
    //---------------
    typedef bool     (__stdcall *F_SetLang) (LPCTSTR);     //言語設定


public:

    //!< コンストラクタ
    CIoAccessBoard();

    //!< デストラクタ
    virtual ~CIoAccessBoard();

    //!< DLL設定
    bool SetDll(StdString* pStrErrorFunc, HMODULE hModule);

    //!< データ同期
    virtual bool SyncData(const CIoAccess& sync);

    //!< クローン生成
    //virtual void  Clone(std::shared_ptr<CIoAccess>* pAccess);

    //!< データ取得(バッファ)
    //virtual bool GetDataBuf(void* pVal, int iPage, int iAddress, int iLen) const;

    //!< データ設定(バッファ)
    //virtual bool SetDataBuf(void* pVal, int iPage, int iAddress, int iLen);


    //--------------
    // ユーザアイテム
    //--------------
    //!< ユーザ定義アイテム数
    //virtual int GetUserItemNumber()  const;

    //!< ユーザ定義アイテム取得
    //virtual StdString GetUserItem(int iItemNo) const;

    //--------------
    // 動作
    //--------------
    // 初期化
    virtual bool Init();

    // リセット
    virtual bool Reset();

    // 開始前処理
    virtual bool Setup();

    // 開始
    virtual bool Start();

    //!< データ取得
    virtual bool GetData(void* pVal, int iPage, int iPos, int iLen) const;

    //!< データ設定
    virtual bool SetData(void* pVal, int iPage, int iPos, int iLen);

    //!< データ更新
    virtual bool Update(int iPage);

    // 停止
    virtual bool Stop();

    // 終了
    virtual bool Finish();
    //--------------
    // プロパティ
    //--------------
    //!< 種別
    //virtual E_ACCESS_TYPE GetAccessType() const;

    //!< ボード番号取得
    virtual int GetBoardNo() const;

    //!< ボード番号設定
    virtual bool SetBoardNo(int iBoardNo);

    // 以下の設定は外部からは変更不可
    // アドインのDLLから設定する。

    //!< ページ最大数取得
    //virtual int GetMaxPage() const;

    //!< ページ最大数設定
    virtual bool SetMaxPage(int iMaxPage){ return false;}

    //最大ページサイズ取得
    //virtual int   GetMaxPageSize() const;
    
    //最大ページサイズ設定
    virtual bool SetMaxPageSize(int iMaxPageSize){ return false;}

    //一括更新の有無取得
    //virtual bool  IsMassUpdate() const;  

    //一括更新の有無設定
    virtual bool  SetMassUpdate(bool bUse){ return false;}

    //最大アイテム数取得
    //virtual int   GetMaxItem() const;

    //最大アイテム数設定
    virtual bool  SetMaxItem(int iMaxItem){ return false;}


protected:
    //!< ボード番号
    int m_iBoardNo;

    // アドインDLLハンドル
    HMODULE m_hAddin;

    //-------------
    // DLL 関数
    //-------------
    F_GetBoardName          m_GetBoardName;         //ボード名称

    //F_GetBoardTypeNum       m_GetBoardTypeNum;      //ボード種別数取得
    //F_GetBoardTypeName      m_GetBoardTypeName;     //ボード種別名取得
    //F_SetBoardType          m_SetBoardType;         //ボード種別設定
    F_GetBoardNoMax         m_GetBoardNoMax;        //最大ボード番号取得
    F_UseBoard              m_UseBoard;             //ボード使用
    F_ReleaseBoard          m_ReleaseBoard;         //ボード使用解除

    F_GetMaxPage            m_GetMaxPage;           //最大ページ数
    F_IsMassUpdate          m_IsMassUpdate;         //一括更新の有無
    F_GetMaxUpdateSize      m_GetMaxUpdateSize;     //一括更新データサイズ



    F_GetMaxItem            m_GetMaxItem;           //最大アイテム数
    F_GetMaxPageSize        m_GetMaxPageSize;       //最大ページサイズ
    F_GetItemRestriction    m_GetItemRestriction;   //アイテム設定制限取得
    F_GetItemDef            m_GetItemDef;           //アイテム設定

    F_GetUserItemMax        m_GetUserItemMax;       //ユーザ定義アイテム最大数
    F_GetUserItemName       m_GetUserItemName;      //ユーザ定義アイテム項目名取得
    F_GetUserItemDef        m_GetUserItemDef;       //ユーザ定義アイテム定義取得
    F_SetUserItem           m_SetUserItem;          //ユーザ定義アイテム設定

    F_GetData               m_GetData;              //データ取得
    F_SetData               m_SetData;              //データ設定

    F_Reset                 m_Reset;                //ボードリセット
    F_Init                  m_Init;                 //ボードイニシャライズ
    F_Setup                 m_Setup;                //開始前処理
    F_Start                 m_Start;                //計測開始
    F_Stop                  m_Stop;                 //計測停止
    F_Finish                m_Finish;               //終了処理

    F_SetLang               m_SetLang;              //言語設定


private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        StdString strRead;
        try
        {
        }
        catch(...)
        {
            STD_DBG(_T("Serlization Error %s"), strRead.c_str());
        }
    }
};

#endif  //_IO_ACCESS_H__
