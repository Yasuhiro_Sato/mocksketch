/**
 * @brief			CIoAccessTcp実装ファイル
 * @file			CIoAccessTcp.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CIoAccessTcp.h"
#include "System/CSystem.h"
#include "Utility/CUtility.h"
#include <boost/serialization/export.hpp> 
BOOST_CLASS_EXPORT(CIoAccessTcp);

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CIoAccessTcp::CIoAccessTcp()
{
}

/**
 *  @brief  デストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CIoAccessTcp::~CIoAccessTcp()
{
}


//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void TEST_CIoAccessTcp()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());


    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));

}
#endif //_DEBUG