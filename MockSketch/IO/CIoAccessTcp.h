/**
 * @brief			CIoAccessTcp ヘッダーファイル
 * @file			CIoAccessTcp.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

#ifndef  _IO_ACCESS_TCP_H__
#define  _IO_ACCESS_TCP_H__
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CIoAccess.h"

/*---------------------------------------------------*/
/*  Class                                            */
/*---------------------------------------------------*/

/*---------------------------------------------------*/
/*  Enums                                            */
/*---------------------------------------------------*/

/**
 * @class   CIoAccessTcp
 * @brief                       
 */
class CIoAccessTcp: public CIoAccess
{
public:
    CIoAccessTcp();
    virtual ~CIoAccessTcp();


private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        StdString strRead;
        try
        {
            ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(CIoAccess);
        }
        catch(...)
        {
            STD_DBG(_T("Serlization Error %s"), strRead.c_str());
        }
    }
};

#endif  //_IO_TABLE_H__
