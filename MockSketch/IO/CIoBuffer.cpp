/**
 * @brief			CIoBuffer実装ファイル
 * @file			CIoBuffer.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CIoBuffer.h"
#include "System/CSystem.h"
#include "Utility/CUtility.h"
#include "Utility/CDelayExec.h"

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CIoBuffer::CIoBuffer()
//:   m_lastAddress(0),
//    m_lastOffset(0)
:m_bDispInvalidAddress(true),
 m_pListCtrl          (NULL) 
{
    
    boost::shared_ptr< CDelayExec> pDelay( new CDelayExec ); 
    m_pDaray = pDelay;
    m_pDaray->SetFunc(100, RedrawDelay, this);
    Init(1);
}

/**
 *  @brief  デストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CIoBuffer::~CIoBuffer()
{
}


/**
 *  @brief  初期化.
 *  @param  [in] iBufferSize バッファーサイズ
 *  @retval true 成功     
 *  @note
 */
bool CIoBuffer::Init(int iBufferSize)
{
    m_ucBuffer.resize(iBufferSize);
    m_listVirtual.resize(iBufferSize);
    m_listIo.clear();
    m_undoBuffer.clear();
    RecalcAddress();
    return true;
}

/**
 *  @brief  初期値設定.
 *  @param  なし
 *  @retval true 成功     
 *  @note
 */
bool CIoBuffer::InitData()
{

    foreach(IoData& Data, m_listIo)
    {
        SetStr(Data.ioPorperty.GetInit(), Data.iAddress, Data.iOffset);
    }
    return true;
}

/**
 *  @brief  アドレス再計算.
 *  @param  なし
 *  @retval true 成功     
 *  @note
 */
void CIoBuffer::RecalcAddress()
{
    m_mapIoAddress.clear();
    m_mapIoName.clear();

    //------------------
    // リストサイズ決定
    //------------------
    int iLvItemSize;
    // BitのOffset0 以外無視 Offset0で７追加
    // その他は 長さ -1を減ずる
    iLvItemSize = m_ucBuffer.size();
    foreach(IoData& Data, m_listIo)
    {
        DAT_TYPE eType =  Data.ioPorperty.GetType();
        int iLen = Data.ioPorperty.GetLen();
        if (eType == DAT_BIT)
        {
            if (Data.iOffset == 0)
            {
                iLvItemSize += 7;
            }
        }
        else
        {
            if (!m_bDispInvalidAddress)
            {
                iLvItemSize -= (iLen - 1);
            }
        }
    }

    //-----------
    //初期化
    //-----------
    m_listVirtual.resize(iLvItemSize);
    std::vector<VlistData>::iterator ite;
    for (ite  = m_listVirtual.begin();
         ite != m_listVirtual.end();
         ite++)
    {
        ite->iPos = -1;
        ite->iPerent = -1;
        ite->iAddress = 0;
        ite->iOffset  = 0;
    }
    //-----------
    

    int iPos = 0;
    int iListPos = 0;    //VirtualList用の位置
    int iOldPos = -1;     //VirtualList用の位置
    int iOldAddress = 0;     //VirtualList用の位置
    int iBitAddress = -1;
    int iBitAddressOffset = 0;
    int iBitAddressOffset2 = 0;
    int iVisibleOffset = 0;
    foreach(IoData& Data, m_listIo)
    {
        //データ再設定
        m_mapIoAddress[Data.iAddress * 10 + Data.iOffset] = iPos;
        m_mapIoName[Data.ioPorperty.GetName()] = iPos;




        //VirtualList用のデータを生成する
        if (Data.ioPorperty.GetType() == DAT_BIT)
        {
            if(iBitAddress != Data.iAddress)
            {
                iBitAddressOffset2 = iBitAddressOffset;
                iBitAddressOffset += 7;
                iBitAddress = Data.iAddress;
                iListPos = Data.iAddress + iBitAddressOffset2 + iVisibleOffset + Data.iOffset;
                //先に全ビット分をマークしておく
                int iBitLen = Data.ioPorperty.GetBit();
                for (int iBit = 0; iBit < 8; iBit++)
                {
                    m_listVirtual[iListPos + iBit].iPos = -2;
                    m_listVirtual[iListPos + iBit].iAddress = iBitAddress;
                    m_listVirtual[iListPos + iBit].iOffset  = iBit;
                    m_listVirtual[iListPos + iBit].iPerent  = iPos;
                }
            }
            iListPos = Data.iAddress + iBitAddressOffset2 + iVisibleOffset + Data.iOffset;
        }
        else
        {
            iListPos = Data.iAddress + iBitAddressOffset + iVisibleOffset;
        }

        //---------------------
        //未定義の項目
        //---------------------
        int iStartPos;
        int iCnt = 0;

        //初回は最初の開始位置までアドレスを設定する
        if (iPos == 0)
        {
            iStartPos = 0;
        }
        else
        { 
            iStartPos = iOldPos + 1;
        }

        for (int iUndefPos = iStartPos; iUndefPos < iListPos; iUndefPos++)
        {
            if (m_listVirtual[iUndefPos].iOffset != 0)
            {
                continue;
            }
            m_listVirtual[iUndefPos].iAddress = iOldAddress + iCnt;
            iCnt++;
        }
        //---------------------

        //---------------------
        //データを設定
        //---------------------
        m_listVirtual[iListPos].iPos = iPos;
        m_listVirtual[iListPos].iAddress = Data.iAddress;
        m_listVirtual[iListPos].iOffset  = Data.iOffset;
        m_listVirtual[iListPos].iPerent  = -1;
        if (Data.ioPorperty.GetType() != DAT_BIT)
        {
            iOldPos = iListPos;
        }
        else
        {
            iOldPos = iListPos + 7 - Data.iOffset;
        }
        iOldAddress = Data.iAddress + 1;
        //---------------------
 
        //データ使用箇所をマーキングしておく
        int iLen = Data.ioPorperty.GetLen();
        if (iLen != 1)
        {
            if (m_bDispInvalidAddress)
            {
                for (int nCnt = iListPos + 1; nCnt < iListPos + iLen; nCnt++)
                {
                    m_listVirtual[nCnt].iPos = -2;
                    m_listVirtual[nCnt].iPerent  = iPos;
                    m_listVirtual[nCnt].iAddress = m_listIo[iPos].iAddress + nCnt - iListPos;
                    iOldAddress = m_listVirtual[nCnt].iAddress + 1;
                 }
                 iOldPos = iListPos + iLen - 1;
            }
            else
            {
                iVisibleOffset -= (iLen - 1);
            }
        }
        iPos++;
    }

    int iCnt = 0;
    for (int iUndefPos = iOldPos + 1; iUndefPos < iLvItemSize; iUndefPos++)
    {
        if (m_listVirtual[iUndefPos].iOffset != 0)
        {
            continue;
        }
        m_listVirtual[iUndefPos].iAddress = iOldAddress + iCnt;
        iCnt++;
    }

    //遅延描画
    m_pDaray->Exec();
}


/**
 *  @brief  削除(名称指定).
 *  @param  [in] strName 
 *  @retval true 成功     
 *  @note
 */
bool CIoBuffer::Del(StdString strName)
{
    std::map<StdString, int>::iterator ite;

    ite = m_mapIoName.find( strName );

    if (ite == m_mapIoName.end())
    {
        return false;
    }

    int iAddress = m_listIo[ite->second].iAddress;
    return Del( iAddress );
}

/**
 *  @brief  削除(アドレス指定).
 *  @param  [in] iAddress アドレス
 *  @retval true 成功     
 *  @note 
 */
bool CIoBuffer::Del(int iAddress)
{

    std::map<int, int>::iterator ite;

    ite = m_mapIoAddress.find( iAddress * 10);
    if (ite == m_mapIoAddress.end())
    {
        return false;
    }

    int iPos =  ite->second;
    DAT_TYPE eType  = GetProperty(iPos)->GetType();

    {
        m_listIo.erase(m_listIo.begin() + iPos);
        m_mapIoAddress.erase(ite);

        std::vector<IoData>::iterator iteListIo;
        if ( eType == DAT_BIT)
        {
            //アドレスごと消去
            for(int iOffset = 1; iOffset < 8; iOffset++)
            {
                ite = m_mapIoAddress.find( iAddress * 10 + iOffset);
                if (ite == m_mapIoAddress.end())
                {
                    continue;
                }

                for( iteListIo  =  m_listIo.begin();
                     iteListIo !=  m_listIo.end();
                     iteListIo++)
                {
                    if ((iteListIo->iAddress == iAddress) &&
                        (iteListIo->iOffset == iOffset))
                    {
                        m_listIo.erase(iteListIo);
                        break;
                    }
                }
                m_mapIoAddress.erase(ite);
            }

        }
    }

    RecalcAddress();
    return true;
}

/**
 *  @brief  挿入可・不可問い合わせ.
 *  @param  [in] eType   データ種別
 *  @param  [in] iAddress アドレス
 *  @param  [in] iLen    データ長
 *  @retval true 挿入可     
 *  @note   
 */
bool CIoBuffer::IsEnableInsert(DAT_TYPE eType, int iAddress, int iLen)
{
    if ( (eType != DAT_STR ) &&
         (eType != DAT_NONE ))
    {
        iLen = CIoProperty::GetUniValLen(eType);
    }
    else
    {
        if (iLen == -1)
        {
            iLen = 1;
        }
    }
    

    int iEnd = iAddress + iLen;

    int iS, iE;
    foreach(IoData& data, m_listIo)
    {
        iS = data.iAddress;
        iE = iS + data.ioPorperty.GetLen();

        if (iAddress > iE)
        {
            continue;
        }

        if (iEnd    < iS)
        {
            break;
        }

        if (iAddress == iEnd)
        {   
            if ((eType == DAT_BIT) &&
                (data.ioPorperty.GetType() == DAT_BIT))
            {
                //BIT同士は挿入可能としておく
                return true;
            }

        }
        return false;
    }
    return true;
}

/**
 *  @brief  変更可・不可問い合わせ
 *  @param  [in] eIoType   データIo種別
 *  @param  [in] iAddress アドレス
 *  @retval true 挿入可     
 *  @note   
 */
bool CIoBuffer::IsEnableChange(DT_IO_INPUT eIoType, int iAddress)
{

    CIoProperty tmpProperty;
    tmpProperty.SetType(_T(""), eIoType);

    DAT_TYPE eType = tmpProperty.GetType();

    //現在のデータを取得する
    int iPos =  GetPos(iAddress);
    CIoProperty* pProperty = GetProperty  (iPos);

    if (pProperty == NULL)
    {
        //指定アドレスにデータが無い
        return IsEnableInsert(eType, iAddress, -1);
    }

    int iLen = 1;
    if ( (eType != DAT_STR ) &&
         (eType != DAT_NONE ))
    {
        iLen = CIoProperty::GetUniValLen(eType);
    }
   

    int iEnd = iAddress + iLen;

    int iS, iE;
    {
        foreach(IoData& data, m_listIo)
        {
            iS = data.iAddress;
            iE = iS + data.ioPorperty.GetLen();

            if (iAddress > iE)
            {
                continue;
            }

            if (iAddress == iS)
            {
                //現在設定されているデータは飛ばす
                continue;
            }

           if (iEnd    < iS)
            {
                break;
            }

            if (iAddress == iEnd)
            {   
                if ((eType == DAT_BIT) &&
                    (data.ioPorperty.GetType() == DAT_BIT))
                {
                    //BIT同士は挿入可能としておく
                    return true;
                }

            }
            return false;
        }
    }
    return true;

}


/**
 *  @brief  挿入.
 *  @param  [in] Property    挿入データ
 *  @param  [in] iAddress     アドレス
 *  @param  [in] iOffset     オフセット
 *  @retval true 成功     
 *  @note   
 */
bool CIoBuffer::Insert(CIoProperty& Property, int iAddress, int iOffset)
{

    // 種別設定を先に行う必要があるため
    // ここでは名称の重複をチェックしない
    /*
    std::map<StdString, int>::iterator ite;

    ite = m_mapIoName.find( Property.GetName());

    if (ite != m_mapIoName.end())
    {
        return false;
    }
    */

    DAT_TYPE eType =Property.GetType();

    int iLen;

    if ( (eType != DAT_STR ) &&
         (eType != DAT_NONE ))
    {
        iLen = CIoProperty::GetUniValLen(eType);
    }
    else
    {
        iLen = Property.GetLen();
    }

    int iEnd = iAddress + iLen - 1;

    //挿入位置を検索
    {
        int iS, iE;
        IoData* pData;
        std::vector<IoData>::iterator iteIo;
        for (iteIo  = m_listIo.begin();
             iteIo != m_listIo.end();
             iteIo++)
        {

            pData = &(*iteIo);

            iS = pData->iAddress;
            iE = iS + pData->ioPorperty.GetLen() - 1;

            if (iAddress > iE)
            {
                continue;
            }

            if (iEnd    < iS)
            {
                break;
            }

            if (iAddress == iEnd)
            {   
                if ((eType == DAT_BIT) &&
                    (pData->ioPorperty.GetType() == DAT_BIT))
                {
                    std::map<int, int>::iterator ite;
                    ite = m_mapIoAddress.find( iAddress * 10 + iOffset);
                    if (ite != m_mapIoAddress.end())
                    {
                        //重複あり
                        return false;
                    }

                    if ( pData->iOffset < iOffset)
                    {
                        continue;
                    }
                    break;
                }
            }

            //重複部分あり
            return false;
        }

        IoData Data;
        Data.ioPorperty = Property;
        Data.iAddress = iAddress;
        Data.iOffset  = iOffset;
        m_listIo.insert(iteIo, Data);
    }

    RecalcAddress();
    return true;
}

/**
 *  @brief  データ数取得.
 *  @param  なし
 *  @retval データ数
 *  @note   
 */
int   CIoBuffer::GetDataNum()
{
    return (int)m_listIo.size();
}


/**
 *  @brief  アドレス数取得.
 *  @param  なし
 *  @retval アドレス数
 *  @note   
 */
int   CIoBuffer::GetAddressNum()
{
    return (int)m_ucBuffer.size();
}


/**
 *  @brief  データ位置取得(名称).
 *  @param  [in] strName データ名
 *  @retval データ位置
 *  @note   
 */
int   CIoBuffer::GetPos(StdString strName)
{
    std::map<StdString, int>::iterator ite;

    ite = m_mapIoName.find( strName);

    if (ite != m_mapIoName.end())
    {
        return -1;
    }
    return ite->second;
}

/**
 *  @brief  データ位置取得(アドレス).
 *  @param  [in] iAddress データ名
 *  @param  [in] iOffset オフセット
 *  @retval データ位置
 *  @note   
 */
int   CIoBuffer::GetPos(int iAddress, int iOffset)
{

    std::map<int, int>::iterator ite;
    ite = m_mapIoAddress.find( iAddress * 10 + iOffset);
    if (ite == m_mapIoAddress.end())
    {
        return -1;
    }
    return ite->second;
}

/**
 *  @brief  VirtualList表示数取得
 *  @param  なし
 *  @retval VirtualList表示項目数
 *  @note   
 */
int   CIoBuffer::GetVListNum()
{
    return (int)m_listVirtual.size();
}

/**
 *  @brief  VirtualList表示数取得
 *  @param  [in] iVLstPos VirtualList位置
 *  @param  [out] pAdress アドレス
 *  @param  [out] pOffset オフセット
 *  @param  [out] pPerent 親データ位置
 *  @retval データ位置 -1 データなし -2 データ使用中
 *  @note   
 */
int   CIoBuffer::GetVListPos(int iVLstPos, int* pAdress, int* pOffset, int* pPerent)
{
    STD_ASSERT(iVLstPos >= 0);
    STD_ASSERT(iVLstPos < (int)m_listVirtual.size());

    *pAdress =  m_listVirtual[iVLstPos].iAddress;
    *pOffset =  m_listVirtual[iVLstPos].iOffset;
    *pPerent =  m_listVirtual[iVLstPos].iPerent;
    return m_listVirtual[iVLstPos].iPos;
}


/**
 *  @brief  データ位置取得(アドレス).
 *  @param  [in] iAddress データ名
 *  @param  [in] iOffset オフセット
 *  @retval データ位置
 *  @note   
 */
CIoProperty*   CIoBuffer::GetProperty(int iPos)
{
    if(iPos < 0){return NULL;}
    if(iPos > (int)m_listIo.size()){return NULL;}

    return   &m_listIo[iPos].ioPorperty;
}

/**
 *  @brief  アドレス取得(位置).
 *  @param  [in] iPos 位置
 *  @retval アドレス
 *  @note   
 */
int   CIoBuffer::GetAddress(int iPos)
{
    DBG_ASSERT(iPos >= 0);
    DBG_ASSERT(iPos <= (int)m_listIo.size());

    return   m_listIo[iPos].iAddress;
}


/**
 *  @brief  オフセット取得(位置).
 *  @param  [in] iPos 位置
 *  @retval オフセット
 *  @note   
 */
int   CIoBuffer::GetOffset(int iPos)
{
    DBG_ASSERT(iPos >= 0);
    DBG_ASSERT(iPos <= (int)m_listIo.size());

    return   m_listIo[iPos].iOffset;
}

/**
 *  @brief  アドレス取得(名称).
 *  @param  [ in] strName 名称
 *  @param  [out] pAddress アドレス
 *  @param  [out] pOffset オフセット
 *  @retval true 成功
 *  @note   
 */
bool CIoBuffer::GetAddress(StdString strName, int* pAddress, int* pOffset )
{
    std::map<StdString, int>::iterator ite;

    ite = m_mapIoName.find( strName);

    if (ite == m_mapIoName.end())
    {
        return false;
    }

    *pAddress = m_listIo[ite->second].iAddress;
    *pOffset = m_listIo[ite->second].iOffset;

    return true;
}


/**
 *  @brief  整数値取得(アドレス).
 *  @param  [in] pAddress アドレス
 *  @param  [in] pOffset オフセット
 *  @retval 整数値
 *  @note   
 */
int  CIoBuffer::GetInt(int iAddress, int iOffset)
{
    std::map<int, int>::iterator ite;
    ite = m_mapIoAddress.find( iAddress * 10 + iOffset);
    if (ite == m_mapIoAddress.end())
    {
        //例外
       STD_ASSERT(_T("NOTFIND"));
    }

    DAT_TYPE eType = GetProperty(ite->second)->GetType();


    //TAG:[エンディアン]

    UniVal* pUni =  (UniVal*)&m_ucBuffer[iAddress];
    switch (eType)
    {
    case DAT_BIT:
        if (m_ucBuffer[iAddress]  & (0x01 << iOffset)){return 1;}
        else                                         {return 0;}
        break;

    case DAT_USHORT:
        return (int)pUni->usVal;
        break;

    case DAT_SHORT:
        return (int)pUni->sVal;
        break;

    case DAT_UINT:
        return (int)pUni->uiVal;
        break;

    case DAT_INT:
        return pUni->iVal;
        break;

    case DAT_FLOAT:
        return (int) pUni->fVal;
        break;

    case DAT_DOUBLE:
        return (int) pUni->dVal;
        break;

    case DAT_STR:
        {
            std::vector< char> ucBuffer;
            int iLen = GetProperty(ite->second)->GetLen();
            ucBuffer.resize(iLen + 1);
            memcpy(&ucBuffer[0], &m_ucBuffer[iAddress], iLen);
            return atoi(&ucBuffer[0]);
        }
        break;
    default:
        STD_DBG(_T("Unknown type %d"), eType);
        return  -1;
        break;
    }
}

/**
 *  @brief  整数値取得(名称).
 *  @param  [in] 名称
 *  @retval 整数値
 *  @note   
 */
int  CIoBuffer::GetInt(StdString strName)
{
    int iAddress;
    int iOffset;
    if( !GetAddress( strName, &iAddress, &iOffset ))
    {
        //例外
        STD_DBG(_T("GetAddress type %s"), strName.c_str());
        return -1;
    }
    return GetInt(iAddress, iOffset);
}

/**
 *  @brief  整数値取得(アドレス).
 *  @param  [in] iVal    整数値
 *  @param  [in] pAddress アドレス
 *  @param  [in] pOffset オフセット
 *  @retval 
 *  @note   
 */
void CIoBuffer::SetInt(int iVal, int iAddress, int iOffset)
{
    std::map<int, int>::iterator ite;
    ite = m_mapIoAddress.find( iAddress * 10 + iOffset);
    if (ite == m_mapIoAddress.end())
    {
        //例外
       STD_DBG(_T("NOTFIND"));
    }

    DAT_TYPE eType = GetProperty(ite->second)->GetType();


    //TAG:[エンディアン]

    UniVal* pUni =  (UniVal*)&m_ucBuffer[iAddress];
    switch (eType)
    {
    case DAT_BIT:
        if (iVal != 0)
        {
            m_ucBuffer[iAddress] |= (0x01 << iOffset);
        }
        else
        {
            m_ucBuffer[iAddress] &= ~(0x01 << iOffset);
        }
        break;

    case DAT_USHORT:
        pUni->usVal = (unsigned short)iVal;
        break;

    case DAT_SHORT:
        pUni->sVal = ( short)iVal;
        break;

    case DAT_UINT:
        pUni->uiVal = (unsigned int)iVal;
        break;

    case DAT_INT:
        pUni->iVal = iVal;
        break;

    case DAT_FLOAT:
        pUni->fVal = (float)iVal;
        break;

    case DAT_DOUBLE:
        pUni->dVal = (double)iVal;
        break;

    case DAT_STR:
        {
            int iLen = GetProperty(ite->second)->GetLen();
            _snprintf_s(&m_ucBuffer[iAddress], iLen, _TRUNCATE, "%d", iVal);
            break;
        }
        break;
    default:
        STD_DBG(_T("Unknown type %d"), eType);
        break;
    }
}

/**
 *  @brief  整数値設定(名称).
 *  @param  [in] 名称
 *  @retval 整数値
 *  @note   
 */
void  CIoBuffer::SetInt(int iVal, StdString strName)
{
    int iAddress;
    int iOffset;
    if( !GetAddress( strName, &iAddress, &iOffset ))
    {
        //例外
        STD_DBG(_T("GetAddress %s"), strName.c_str());
        return;
    }
    SetInt(iVal, iAddress, iOffset);
}

/**
 *  @brief  実数値取得(アドレス).
 *  @param  [in] pAddress アドレス
 *  @param  [in] pOffset オフセット
 *  @retval 実数値
 *  @note   
 */
float CIoBuffer::GetFloat(int iAddress, int iOffset)
{
    std::map<int, int>::iterator ite;
    ite = m_mapIoAddress.find( iAddress * 10 + iOffset);
    if (ite == m_mapIoAddress.end())
    {
        //例外
       STD_DBG(_T("NOTFIND"));
    }

    DAT_TYPE eType = GetProperty(ite->second)->GetType();


    //TAG:[エンディアン]

    UniVal* pUni =  (UniVal*)&m_ucBuffer[iAddress];
    switch (eType)
    {
    case DAT_BIT:
        if (m_ucBuffer[iAddress]  & (0x01 << iOffset)){return 1.0f;}
        else                                         {return 0.0f;}
        break;

    case DAT_USHORT:
        return (float)pUni->usVal;
        break;

    case DAT_SHORT:
        return (float)pUni->sVal;
        break;

    case DAT_UINT:
        return (float)pUni->uiVal;
        break;

    case DAT_INT:
        return (float)pUni->iVal;
        break;

    case DAT_FLOAT:
        return pUni->fVal;
        break;

    case DAT_DOUBLE:
        return  pUni->dVal;
        break;

    case DAT_STR:
        {
            std::vector< char> ucBuffer;
            int iLen = GetProperty(ite->second)->GetLen();
            ucBuffer.resize(iLen + 1);
            memcpy(&ucBuffer[0], &m_ucBuffer[iAddress], iLen);
            return atof(&ucBuffer[0]);
        }
        break;
    default:
        STD_DBG(_T("Unknown type %d"), eType);
        return  -1;
        break;
    }
}


/**
 *  @brief  実数値取得(名称).
 *  @param  [in] 名称
 *  @retval 実数値
 *  @note   
 */
float  CIoBuffer::GetFloat(StdString strName)
{
    int iAddress;
    int iOffset;
    if( !GetAddress( strName, &iAddress, &iOffset ))
    {
        //例外
        STD_DBG(_T("GetAddress %s"), strName.c_str());
        return -1;
    }
    return GetFloat(iAddress, iOffset);
}

/**
 *  @brief  実数値設定(アドレス).
 *  @param  [in] fVal    実数値
 *  @param  [in] pAddress アドレス
 *  @param  [in] pOffset オフセット
 *  @retval なし
 *  @note   
 */
void  CIoBuffer::SetFloat(float fVal, int iAddress, int iOffset)
{
    std::map<int, int>::iterator ite;
    ite = m_mapIoAddress.find( iAddress * 10 + iOffset);
    if (ite == m_mapIoAddress.end())
    {
        //例外
       STD_DBG(_T("NOTFIND"));
    }

    DAT_TYPE eType =GetProperty(ite->second)->GetType();


    //TAG:[エンディアン]

    UniVal* pUni =  (UniVal*)&m_ucBuffer[iAddress];
    switch (eType)
    {
    case DAT_BIT:
        if (fVal * fVal <  NEAR_ZERO)
        {
            m_ucBuffer[iAddress] |= (0x01 << iOffset);
        }
        else
        {
            m_ucBuffer[iAddress] &= ~(0x01 << iOffset);
        }
        break;

    case DAT_USHORT:
        pUni->usVal = (unsigned short)fVal;
        break;

    case DAT_SHORT:
        pUni->sVal = ( short)fVal;
        break;

    case DAT_UINT:
        pUni->uiVal = (unsigned int)fVal;
        break;

    case DAT_INT:
        pUni->iVal = (int)fVal;
        break;

    case DAT_FLOAT:
        pUni->fVal = (float)fVal;
        break;

    case DAT_DOUBLE:
        pUni->dVal = (double)fVal;
        break;

    case DAT_STR:
        {
            int iLen = GetProperty(ite->second)->GetLen();
            _snprintf_s(&m_ucBuffer[iAddress], iLen, _TRUNCATE, "%f", fVal);
            break;
        }
        break;
    default:
        STD_DBG(_T("Unknown type %d"), eType);
        break;
    }
}


/**
 *  @brief  実数値設定(名称).
 *  @param  [in] fVal    実数値
 *  @param  [in] strName 名称
 *  @retval なし
 *  @note   
 */
void  CIoBuffer::SetFloat(float fVal, StdString strName)
{
    int iAddress;
    int iOffset;
    if( !GetAddress( strName, &iAddress, &iOffset ))
    {
        //例外
        STD_DBG(_T("GetAddress %s"), strName.c_str());
        return;
    }
    SetFloat(fVal, iAddress, iOffset);
}


/**
 *  @brief  データ取得(アドレス)
 *  @param  [out] pVal    取得データ
 *  @param  [in ] pAddress アドレス
 *  @retval 実数値
 *  @note   
 */
void   CIoBuffer::GetData(void* pVal, int iAddress)
{
    std::map<int, int>::iterator ite;
    ite = m_mapIoAddress.find( iAddress * 10);
    if (ite == m_mapIoAddress.end())
    {
        //例外
       STD_ASSERT(_T("NOTFIND"));
    }

    int iLen = GetProperty(ite->second)->GetLen();

    STD_ASSERT(iLen != -1);
    
    //TAG:[エンディアン]
    memcpy(pVal, &m_ucBuffer[iAddress], iLen);

}

/**
 *  @brief  データ設定(名称)
 *  @param  [out] pVal    取得データ
 *  @param  [in ] pAddress アドレス
 *  @retval 実数値
 *  @note   
 */
void   CIoBuffer::GetData(void* pVal, StdString strName)
{
    int iAddress;
    int iOffset;
    if( !GetAddress( strName, &iAddress, &iOffset ))
    {
        //例外
        STD_DBG(_T("GetAddress %s"), strName.c_str());
        return;
    }

    GetData(pVal, iAddress);
}

/**
 *  @brief  データ設定(アドレス).
 *  @param  [in] fVal    実数値
 *  @param  [in] pAddress アドレス
 *  @retval なし
 *  @note   
 */
void    CIoBuffer::SetData(const void* pVal, int iAddress)
{
    std::map<int, int>::iterator ite;
    ite = m_mapIoAddress.find( iAddress * 10);
    if (ite == m_mapIoAddress.end())
    {
        //例外
       STD_ASSERT(_T("NOTFIND"));
    }

    int iLen = GetProperty(ite->second)->GetLen();

    STD_ASSERT(iLen != -1);
    
    //TAG:[エンディアン]
    memcpy(&m_ucBuffer[iAddress], pVal, iLen);
}

/**
 *  @brief  データ設定(名称).
 *  @param  [in] pVal     データ
 *  @param  [in] strName  データ名
 *  @retval なし
 *  @note   
 */
void    CIoBuffer::SetData(const void* pVal, StdString strName)
{
    int iAddress;
    int iOffset;
    if( !GetAddress( strName, &iAddress, &iOffset ))
    {
        //例外
        STD_DBG(_T("GetAddress %s"), strName.c_str());
        return;
    }

    SetData(pVal, iAddress);
}

/**
 *  @brief  文字列取得(アドレス).
 *  @param  [in] iAddress アドレス
 *  @param  [in] iOffset オフセット
 *  @retval なし
 *  @note   
 */
StdString  CIoBuffer::GetStr(int iAddress, int iOffset)
{
    std::map<int, int>::iterator ite;
    ite = m_mapIoAddress.find( iAddress * 10 + iOffset);
    if (ite == m_mapIoAddress.end())
    {
        //例外
       STD_DBG(_T("NOTFIND"));
    }

    DAT_TYPE eType = GetProperty(ite->second)->GetType();


    //TAG:[エンディアン]

    UniVal* pUni =  (UniVal*)&m_ucBuffer[iAddress];
    StdString strRet;

    switch (eType)
    {
    case DAT_BIT:
        if (m_ucBuffer[iAddress]  & (0x01 << iOffset)){strRet = _T("ON");}
        else                                         {strRet = _T("OFF");}
        break;

    case DAT_USHORT:
        strRet = CUtil::StrFormat(_T("%d"), pUni->usVal);
        break;

    case DAT_SHORT:
        strRet = CUtil::StrFormat(_T("%d"), pUni->sVal);
        break;

    case DAT_UINT:
        strRet = CUtil::StrFormat(_T("%d"), pUni->uiVal);
        break;

    case DAT_INT:
        strRet = CUtil::StrFormat(_T("%d"), pUni->iVal);
        break;

    case DAT_FLOAT:
        strRet = CUtil::StrFormat(_T("%f"), pUni->fVal);
        break;

    case DAT_DOUBLE:
        strRet = CUtil::StrFormat(_T("%f"), pUni->dVal);
        break;

    case DAT_STR:
        {
            std::vector< char> ucBuffer;
            int iLen = GetProperty(ite->second)->GetLen();
            ucBuffer.resize(iLen + 1);
            std::fill( ucBuffer.begin(), ucBuffer.end(), 0);
            memcpy(&ucBuffer[0], &m_ucBuffer[iAddress], iLen);
            strRet = CUtil::CharToString(&ucBuffer[0]);
        }
        break;
    default:
        STD_DBG(_T("Unknown type %d"), eType);
        strRet = _T("---");
        break;
    }
    return strRet;
}

/**
 *  @brief  文字列取得(名称).
 *  @param  [in] iAddress アドレス
 *  @param  [in] iOffset オフセット
 *  @retval なし
 *  @note   
 */
StdString   CIoBuffer::GetStr(StdString strName)
{
    int iAddress;
    int iOffset;
    if( !GetAddress( strName, &iAddress, &iOffset ))
    {
        //例外
        STD_DBG(_T("GetAddress %s"), strName.c_str());
        StdString strRet = _T("---");
        return strRet;
    }

    return GetStr(iAddress, iOffset);
}

/**
 *  @brief  文字列設定(アドレス).
 *  @param  [in] fVal    実数値
 *  @param  [in] pAddress アドレス
 *  @param  [in] pOffset オフセット
 *  @retval なし
 *  @note   
 */
void  CIoBuffer::SetStr(StdString Str, int iAddress, int iOffset)
{

    std::map<int, int>::iterator ite;
    ite = m_mapIoAddress.find( iAddress * 10 + iOffset);
    if (ite == m_mapIoAddress.end())
    {
        //例外
       STD_DBG(_T("NOTFIND"));
    }

    DAT_TYPE eType = GetProperty(ite->second)->GetType();


    //TAG:[エンディアン]

    UniVal* pUni =  (UniVal*)&m_ucBuffer[iAddress];
    StdString strRet;

    switch (eType)
    {
    case DAT_BIT:

        if (Str == _T("ON") || Str == _T("1") )
        {
            m_ucBuffer[iAddress] |= (0x01 << iOffset);
        }
        else
        {
            m_ucBuffer[iAddress] &= ~(0x01 << iOffset);
        }
        break;

    case DAT_USHORT:
        pUni->usVal = _tstoi(Str.c_str());
        break;

    case DAT_SHORT:
        pUni->sVal = _tstoi(Str.c_str());
        break;

    case DAT_UINT:
        pUni->uiVal  = _tstoi(Str.c_str());
        break;

    case DAT_INT:
        pUni->iVal  = _tstoi(Str.c_str());
        break;

    case DAT_FLOAT:
        pUni->fVal  = _tstof(Str.c_str());
        break;

    case DAT_DOUBLE:
        pUni->dVal = _tstof(Str.c_str());
        break;

    case DAT_STR:
        {
            std::vector< char> ucBuffer;
            int iLen = GetProperty(ite->second)->GetLen();
            ucBuffer.resize(iLen + 1);
            std::fill( ucBuffer.begin(), ucBuffer.end(), 0);

            std::string strChar =  CUtil::StringToChar(Str);
            int iStrLen = strChar.length();

            if (iStrLen > iLen)
            {
                iStrLen = iLen;
            }

            memcpy(&ucBuffer[0], strChar.c_str(), iStrLen);
            memcpy(&m_ucBuffer[iAddress], &ucBuffer[0] , iLen);
        }
        break;
    default:
        STD_DBG(_T("Unknown type %d"), eType);
        break;
    }
}

/**
 *  @brief  文字列設定(名称).
 *  @param  [in] Str     設定文字
 *  @param  [in] strName 名称
 *  @retval なし
 *  @note   
 */
void    CIoBuffer::SetStr(StdString Str, StdString strName)
{
    int iAddress;
    int iOffset;
    if( !GetAddress( strName, &iAddress, &iOffset ))
    {
        //例外
        STD_DBG(_T("GetAddress %s"), strName.c_str());
        return;
    }
    SetStr(Str, iAddress, iOffset);
}

/**
 *  @brief  VirtualList表示データ取得（設定）.
 *  @param  [in] iVLstPos     リスト位置
 *  @param  [in] eCol         項目番号
 *  @param  [out] pChange     変更の有無
 *  @retval 表示データ
 *  @note   
 */
StdString   CIoBuffer::GetVListSet(int iVLstPos, E_IO_PROPERTY_ITEM_SET eCol, bool* pChange )
{
    StdStringStream strRet;

    int iAddress;
    int iOffset;
    int iParent;


    int iPos = GetVListPos(iVLstPos, &iAddress, &iOffset, &iParent);

    CIoProperty*  pPrpperty = NULL;
    if (iPos >= 0)
    {
        pPrpperty = GetProperty  (iPos);
    }

    *pChange = false;
    switch(eCol)
    {
    case IOS_ADDRESS:
        {
            strRet << StdFormat(_T("%04d:%d")) % iAddress % iOffset;
        }
        break;

    case IOS_TYPE:       // 種別
        {
           if (iPos >= 0)
           {
               *pChange = true;
               strRet << pPrpperty->GetTypeName();
           }
           else if (iPos == -1)
           {

               *pChange = true;
               strRet <<  CIoProperty::GetIoTypeName(IT_NONE);
           }
           else
           {
               if (iParent >= 0)
               {
                    CIoProperty*  pPaent = GetProperty  (iParent);
                    strRet <<  _T("(") << pPaent->GetTypeName() << _T(")");
               }
               else
               {
                    strRet << CIoProperty::GetIoTypeName(IT_NONE);
               }
           }
        }
        break;
    case IOS_NAME:       // 名称
        {
           if (iPos >= 0)
           {
                *pChange = true;
                strRet << pPrpperty->GetName();
           }
        }
        break;
    case IOS_INIT:       // 初期値
        {
           if (iPos >= 0)
           {
                *pChange = true;
                strRet << pPrpperty->GetInit();
           }
        }
        break;
    case IOS_NOTE:       // 説明
        {
           if (iPos >= 0)
           {
                *pChange = true;
                strRet << pPrpperty->GetNote();
           }
        }
        break;
    case IOS_LEN:        // データ長
        {
            if (iPos >= 0)
            {
                if (pPrpperty->GetType() == DAT_STR)
                {
                    *pChange = true;
                }
                strRet << pPrpperty->GetLen();
            }
        }
        break;
    case IOS_UNIT:       // 単位
        {
            if (iPos >= 0)
            {
                if (pPrpperty->GetBit() != 0)
                {
                    strRet << pPrpperty->GetUnit().GetUnit();
                    *pChange = true;
                }
                else
                {
                    strRet << _T("---");
                    *pChange = false;
                }
            }
        }
        break;
    case IOS_MAX:        // 最大値
        {
            if (iPos >= 0)
            {
                if (pPrpperty->GetBit() != 0)
                {
                    strRet << pPrpperty->GetMax();
                    *pChange = true;
                }
                else
                {
                    strRet << _T("---");
                    *pChange = false;
                }
            }
        }
        break;
    case IOS_MIN:        // 最小値
        {
            if (iPos >= 0)
            {
                if (pPrpperty->GetBit() != 0)
                {
                    strRet << pPrpperty->GetMin();
                    *pChange = true;
                }
                else
                {
                    strRet << _T("---");
                    *pChange = false;
                }
            }
        }
        break;
    default:
        strRet << _T("---");
        break;
    }
    return strRet.str();
}

//!<  VirtualList表示データ取得（表示）
StdString   CIoBuffer::GetVListDisp(int iVLstPos, E_IO_PROPERTY_ITEM_DISP eCol, bool* pChange)
{
    return _T("");
}

/**
 *  @brief  VirtualListデータ設定（設定）.
 *  @param  [in] iVLstPos     リスト位置
 *  @param  [in] eCol         項目番号
 *  @param  [in] strData      設定データ
 *  @retval true 成功
 *  @note   
 */
bool  CIoBuffer::SetVListSet(int iVLstPos, E_IO_PROPERTY_ITEM_SET eCol , StdString strData)
{
    StdStringStream strRet;
    int iAddress;
    int iOffset;
    int iParent;


    int iPos = GetVListPos(iVLstPos, &iAddress, &iOffset, &iParent);
    CIoProperty*  pPrpperty = NULL;
    pPrpperty = GetProperty  (iPos);
    DAT_TYPE eType = DAT_NONE;
    bool bRet = true;

    if (pPrpperty)
    {
        eType = pPrpperty->GetType();
    }
    else
    {
        if (eCol != IOS_TYPE)
        {
            return false;
        }
    }



    switch(eCol)
    {
    case IOS_ADDRESS:
        {
            //設定不可
            bRet = false;
        }
        break;

    case IOS_TYPE:       // 種別
        {
            //BITはOffset0のもの以外は編集不可
            if (   eType == DAT_BIT)
            {
                int iOffset =  GetOffset    (iPos);
                if (iOffset != 0)
                {
                    return false;
                }
            }

            DT_IO_INPUT eIoType = CIoProperty::GetIoType(strData);
            bRet = ChangeType(eIoType, iAddress);
        }
        break;
    case IOS_NAME:       // 名称
        {
            bRet =  pPrpperty->SetName(strData);
        }
        break;
    case IOS_INIT:       // 初期値
        {
            pPrpperty->SetInit(strData);
        }
        break;
    case IOS_NOTE:       // 説明
        {
            pPrpperty->SetNote(strData);
        }
        break;
    case IOS_LEN:        // データ長
        {
            int iLen = boost::lexical_cast<int>(strData);
        }
        break;
    case IOS_UNIT:       // 単位
        {
            CUnit* pUnit = SYS_UNIT->GetUnitByUnit(strData);
            pPrpperty->SetUnit(pUnit);
        }
        break;
    case IOS_MAX:        // 最大値
        {
            if ( (eType == DAT_BIT) ||
                 (eType == DAT_STR))
            {
                bRet = false;
            }
            else
            {
                double dMax = boost::lexical_cast<double>(strData);
                pPrpperty->SetMax(dMax);
            }
        }
        break;
    case IOS_MIN:        // 最小値
        {
            if ( (eType == DAT_BIT) ||
                 (eType == DAT_STR))
            {
                bRet = false;
            }
            else
            {
                double dMin = boost::lexical_cast<double>(strData);
                pPrpperty->SetMax(dMin);
            }
        }
        break;
    default:
        strRet << _T("---") << std::endl;
        break;
    }
    return bRet;
}

//!<  VirtualListデータ設定
bool  CIoBuffer::SetVListDisp(int iVLstPos, E_IO_PROPERTY_ITEM_DISP eCol, StdString strData)
{
    return true;
}

/**
 *  @brief  種別変更.
 *  @param  [in] eIoType      種別
 *  @param  [in] iAddress     アドレス
 *  @retval true 成功
 *  @note   
 */
bool CIoBuffer::ChangeType(DT_IO_INPUT eIoType, int iAddress)
{
    //  Offset 0 以外での変更は不可
    //  変更があった場合 -> 挿入可能を確認
    //  BITの場合はすべてのoffset値をInsertしておく
    //  (名称の重複チェックは行わない）


    int iPos =  GetPos(iAddress);
    CIoProperty* pProperty =  GetProperty(iPos);
    

    CIoProperty newProperty;
    CIoProperty tmpProperty;
    if (pProperty != NULL) 
    {
        //種別に変更が無い場合は何もしない
        if (eIoType == pProperty->GetIoType())
        {
            return true;
        }

        //該当データ削除
        Del(iAddress);
        tmpProperty = *pProperty;
        newProperty.SetType(tmpProperty.GetName(), eIoType);
    }
    else
    {
        newProperty.SetType(_T(""), eIoType);
        tmpProperty = newProperty;
    }


    //データ挿入
    if (eIoType == IT_NONE)
    {
        return true;
    }
    Insert(tmpProperty,iAddress);

    //Bitの処理
    if (newProperty.GetType() == DAT_BIT)
    {
        tmpProperty.SetName(_T(""));
        for (int iOffset = 1; iOffset < 8; iOffset++)
        {
            //名前は空欄とする
            //StdStringStream strmName;
            //strmName << StdFormat(_T("_%d")) % iOffset;
            //tmpProperty.SetName(strmName.str());
            Insert(tmpProperty, iAddress, iOffset);
        }
    }
    return true;
}


/**
 *  @brief  使用不能アドレスの表示有無.
 *  @param  [in] bEnable     true 表示する false 非表示
 *  @retval なし
 *  @note   
 */
void  CIoBuffer::DispInvalidAddress(bool bEnable)
{
    if (bEnable != m_bDispInvalidAddress)
    {
        m_bDispInvalidAddress = bEnable;
        RecalcAddress();
    }
    
}

/**
 *  @brief  VirtualList空きByte数取得
 *  @param  [in]  iVLstPos   VirtualList位置
 *  @param  [out] pList      ドロップダウンリスト設定データ
 *  @retval false 変更不可
 *  @note   
 */
int  CIoBuffer::GetVListSpareBit(int iVLstPos)
{
    StdStringStream strRet;
    int iAddress;
    int iAddressNext = 0;
    int iOffset;
    int iParent;


    int iPos = GetVListPos(iVLstPos, &iAddress, &iOffset, &iParent);
    CIoProperty*  pPrpperty = NULL;
    pPrpperty = GetProperty  (iPos);



    if (iParent > 0)
    {
        //子の設定の場合は変更不可
        return 0;
    }

    if (iOffset > 0)
    {
        //子の設定の場合は変更不可(bit)
        return 0;
    }
    

    //空きアドレスを調べる
    {
        foreach(IoData& Data, m_listIo)
        {
            if (Data.iAddress <= iAddress)
            {
                continue;
            }
            iAddressNext = Data.iAddress;
            break;
        }
    }

    if (iAddressNext == 0)
    {
        iAddressNext = GetAddressNum();
    }

    int iByte = iAddressNext - iAddress;

    return iByte;
}

/**
 *  @brief  VirtualList種別ドロップダウン.
 *  @param  [in]  iVLstPos   VirtualList位置
 *  @param  [out] pList      ドロップダウンリスト設定データ
 *  @retval false 変更不可
 *  @note   
 */
bool  CIoBuffer::GetVListTypeList(int iVLstPos, std::vector<StdString>* pList)
{
    int iByte = GetVListSpareBit(iVLstPos);

    if (iByte <= 0)
    {
        return false;
    }

    if (pList == NULL)
    {
        return false;
    }


    DT_IO_INPUT eIoType;
    DT_IO_DIR   eDir;
    DAT_TYPE    eDatType;
    int         iBit;
    int         iLen;

    pList->clear();

    for (int iCnt = 0; iCnt < IT_END; ++iCnt)
    {
        eIoType = static_cast<DT_IO_INPUT>(iCnt);

        CIoProperty::ConvIoType(eIoType, &eDir, &eDatType, &iBit);
        iLen = CIoProperty::GetUniValLen(eDatType);
        if (iByte >= iLen)
        {
            pList->push_back(CIoProperty::GetIoTypeName(eIoType));
        }
    }
    return true;
}

/**
 *  @brief  VirtualList設定.
 *  @param  [in]  pListCtrl   VirtualList
 *  @retval なし
 *  @note   
 */
void  CIoBuffer::SetVList(CListCtrl* pListCtrl)
{
    m_pListCtrl = pListCtrl;
}


/**
 *  @brief  遅延描画処理.
 *  @param  [in]  pCIoBuffer  自オブジェクトへのポインタ
 *  @retval なし
 *  @note   CDelayExecを使用しての遅延処理
 */
void CIoBuffer::RedrawDelay(void* pCIoBuffer)
{
    CIoBuffer* pBuffer = reinterpret_cast<CIoBuffer*>(pCIoBuffer);
    pBuffer->RedrawExec();
}

/**
 *  @brief  遅延描画処理本体.
 *  @param  なし
 *  @retval なし
 *  @note   CDelayExecを使用しての遅延処理
 *          直接は使用しない
 */
void CIoBuffer::RedrawExec()
{
    if (m_pListCtrl)
    {
        m_pListCtrl->SetItemCount(m_listVirtual.size());
        m_pListCtrl->RedrawWindow();
    }

}
