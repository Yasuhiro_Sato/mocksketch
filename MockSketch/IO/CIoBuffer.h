/**
 * @brief			CIoBuffer ヘッダーファイル
 * @file			CIoBuffer.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

#ifndef  _IO_BUFFER_H__
#define  _IO_BUFFER_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CIoProperty.h"

/*---------------------------------------------------*/
/*  Class                                            */
/*---------------------------------------------------*/
class CDelayExec;

/*---------------------------------------------------*/
/*  Enums                                            */
/*---------------------------------------------------*/
//!< VirtualList用 表示項目(設定用) 
enum E_IO_PROPERTY_ITEM_SET
{
    IOS_ADDRESS,    //!< アドレスオフセット
    IOS_TYPE,       //!< 種別
    IOS_NAME,       //!< 名称
    IOS_INIT,       //!< 初期値
    IOS_NOTE,       //!< 説明
    IOS_LEN,        //!< データ長
    IOS_UNIT,       //!< 単位
    IOS_MAX,        //!< 最大値
    IOS_MIN,        //!< 最小値
};


//!< VirtualList用 表示項目(表示用) 
enum E_IO_PROPERTY_ITEM_DISP
{
    IOD_ADDRESS,    //!< アドレスオフセット
    IOD_TYPE,       //!< 種別
    IOD_NAME,       //!< 名称
    IOD_VAL,        //!< 値
    IOD_CONECT,     //!< 接続先(入力)
    IOD_UNIT,       //!< 単位
    IOD_NOTE,       //!< 説明
};


/**
 * @class   CIoBuffer
 * @brief                       
 */
class CIoBuffer
{
protected:
    struct IoData
    {
        CIoProperty     ioPorperty;
        int             iAddress;
        int             iOffset;

    private:
        friend class boost::serialization::access;  
        template<class Archive>
        void serialize(Archive& ar, const unsigned int version)
        {
            StdString strRead;
            try
            {
                ar & boost::serialization::make_nvp("Property"  , ioPorperty);
                ar & boost::serialization::make_nvp("Address"  , iAddress);
                ar & boost::serialization::make_nvp("Offset"  , iOffset);
            }
            catch(...)
            {
                STD_DBG(_T("Serlization Error %s"), strRead.c_str());
            }
        }
    };

    struct VlistData
    {
        int iPos;
        int             iPerent;
        int             iAddress;
        int             iOffset;
    };

    //!< 遅延実行
    boost::shared_ptr<CDelayExec> m_pDaray;

public:
    //!< コンストラクタ
    CIoBuffer();

    //!< デストラクタ
    ~CIoBuffer();

    //!< 初期化
    bool Init(int iBufferSize);

    //!< 初期値設定
    bool InitData();

    //!< 削除(名称指定)
    bool Del(StdString strName);

    //!< 削除(アドレス指定)
    bool Del(int iAddress);

    //!< 挿入可・不可問い合わせ
    bool IsEnableInsert(DAT_TYPE eType, int iAddress, int iLen = -1);

    //!< 変更可・不可問い合わせ
    bool IsEnableChange(DT_IO_INPUT eIoType, int iAddress);

    //!< 挿入
    bool Insert(CIoProperty& Property, int iAddress, int iOffset = 0);

    //!< 種別変更
    bool ChangeType(DT_IO_INPUT eIoType, int iAddress);

    //!< データ数取得
    int   GetDataNum();

    //!< アドレス数取得
    int   GetAddressNum();

    //!< データ位置取得(名称)
    int   GetPos       (StdString strName);

    //!< データ位置取得(アドレス)
    int   GetPos       (int iAddress, int iOffset = 0);

    //!< プロパティ取得
    CIoProperty*  GetProperty  (int iPos);

    //!< アドレス取得(位置)
    int   GetAddress    (int iPos);

    //!< オフセット取得(位置)
    int   GetOffset    (int iPos);

    //!< アドレス取得(名称)
    bool GetAddress(StdString strName, int* pAddress, int* pOffset );

    //!< 整数値取得(アドレス)
    int  GetInt(int iAddress, int iOffset = 0);

    //!< 整数値取得(名称)
    int  GetInt(StdString strName);

    //!< 整数値設定(アドレス)
    void SetInt(int iVal, int iAddress, int iOffset = 0);

    //!< 整数値設定(名称)
    void SetInt(int iVal, StdString strName);

    //!< 実数値取得(アドレス)
    float   GetFloat(int iAddress, int iOffset = 0);

    //!< 実数値取得(名称)
    float   GetFloat(StdString strName);

    //!< 実数値設定(アドレス)
    void    SetFloat(float fVal, int iAddress, int iOffset = 0);

    //!< 実数値設定(名称)
    void    SetFloat(float fVal, StdString strName);

    //!< データ取得(アドレス)
    void   GetData(void* pVal, int iAddress);

    //!< データ取得(名称)
    void   GetData(void* pVal, StdString strName);

    //!< データ設定(アドレス)
    void    SetData(const void* pVal, int iAddress);

    //!< データ設定(名称)
    void    SetData(const void* pVal, StdString strName);

    //!< 文字列取得(アドレス)
    StdString   GetStr(int iAddress, int iOffset = 0 );

    //!< 文字列取得(名称)
    StdString   GetStr(StdString strName);

    //!< 文字列設定(アドレス)
    void    SetStr(StdString Str, int iAddress, int iOffset = 0 );

    //!< 文字列設定(名称)
    void    SetStr(StdString Str, StdString strName);


    //!< アンドウ
    bool Undo(){return true;}

    //----------------------
    // VirtualList用
    //----------------------
    //!<  VirtualList表示数取得
    int   GetVListNum();

    //!<  VirtualList表示数取得
    int   GetVListPos(int iVLstPos, int* pAdress, int* pOffset, int* pPerent);

    //!<  使用不能アドレスの表示有無
    void  DispInvalidAddress(bool bEnable);

    //!<  VirtualList表示データ取得（設定）
    StdString   GetVListSet(int iVLstPos, E_IO_PROPERTY_ITEM_SET eCol, bool* pChange);

    //!<  VirtualList表示データ取得（表示）
    StdString   GetVListDisp(int iVLstPos, E_IO_PROPERTY_ITEM_DISP eCol, bool* pChange);

    //!<  VirtualListデータ設定
    bool  SetVListSet(int iVLstPos, E_IO_PROPERTY_ITEM_SET eCol , StdString strData);

    //!<  VirtualListデータ設定
    bool  SetVListDisp(int iVLstPos, E_IO_PROPERTY_ITEM_DISP eCol, StdString strData);

    //!<  VirtualList空きByte数取得
    int  GetVListSpareBit(int iVLstPos);
    
    //!<  VirtualListデータ設定
    bool  GetVListTypeList(int iVLstPos, std::vector<StdString>* pList);

    //!<  VirtualList設定
    void  SetVList(CListCtrl* pListCtrl);

    //!<  VirtualListコピー
    void  CopyVList(int iStartPos, E_IO_PROPERTY_ITEM_DISP eStartCol,
                    int iEndPos,   E_IO_PROPERTY_ITEM_DISP eEnd, 
                    bool bCut,     bool bInsert);


protected:
    void RedrawExec();

    static void RedrawDelay(void* pCIoBuffer);


public:
    void RecalcAddress();


protected:
    
    std::vector<char> m_ucBuffer;
    std::vector<IoData>        m_listIo;
    std::map<int, int>     m_mapIoAddress;

    //!< virtual リスト用配列
    std::vector<VlistData>       m_listVirtual;

    std::map<StdString, int>     m_mapIoName;

    std::deque< std::vector<IoData> > m_undoBuffer;


    bool m_bDispInvalidAddress;

    CListCtrl* m_pListCtrl;


    //int m_lastAddress;
    //int m_lastOffset;
private:
    //boost::mutex   m_mtxGuard;

private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        StdString strRead;
        try
        {
            ar & boost::serialization::make_nvp("IoList"      , m_listIo);
            ar & boost::serialization::make_nvp("DispInvAdr"  , m_bDispInvalidAddress);
        }
        catch(...)
        {
            STD_DBG(_T("Serlization Error %s"), strRead.c_str());
        }
        RecalcAddress();
    }

};

#endif  //_IO_BUFFER_H__
