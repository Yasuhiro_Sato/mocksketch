/**
 * @brief			CIoCommon 実装ファイル
 * @file			CIoCommon.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CIoCommon.h"
#include "System/CSystem.h"
#include "Utility/CUtility.h"

/*---------------------------------------------------*/
/*  Class                                            */
/*---------------------------------------------------*/

/*---------------------------------------------------*/
/*  Enums                                            */
/*---------------------------------------------------*/

/*---------------------------------------------------*/
/*  Defines                                          */
/*---------------------------------------------------*/


namespace IO_COMMON
{

/*---------------------------------------------------*/
/* Grobal variables                                  */
/*---------------------------------------------------*/
std::map<DT_IO_TYPE, StdString> g_mapIoTypeName;

std::vector< std::set<int> > g_lstSetUseProperty;
std::vector< std::set<int> > g_lstSetUseConnect;



/**
 *  @brief  データサイズ取得.
 *  @param  [in] eType データ種別
 *  @retval データ長 
 *  @note   static
 */
int GetUniValLen(DAT_TYPE eType)
{
    switch (eType)
    {
    case DAT_BIT:    {     return 1;}
    case DAT_CHAR:   {     return 1;}
    case DAT_USHORT: {     return 2;}
    case DAT_SHORT:  {     return 2;}
    case DAT_ULONG:  {     return 4;}
    case DAT_LONG:   {     return 4;}
    case DAT_FLOAT:  {     return 4;}
    case DAT_DOUBLE: {     return 8;}
    case DAT_STR:    {     return -1;} 
    }
    return -1;
}


/**
 *  @brief  IO種別名称取得.
 *  @param  [in] eType データ種別
 *  @retval データ名 
 *  @note   static
 */
StdString GetIoTypeName(DT_IO_TYPE eType)
{
    if (g_mapIoTypeName.size() == 0)
    {
        g_mapIoTypeName[IT_NONE] =_T("---");
        g_mapIoTypeName[IT_DI] =_T("D/I");
        g_mapIoTypeName[IT_DO] =_T("D/O");

        g_mapIoTypeName[IT_AI_8] =_T("A/I 8");
        g_mapIoTypeName[IT_AO_8] =_T("A/O 8");

        g_mapIoTypeName[IT_AI_10] =_T("A/I 10");
        g_mapIoTypeName[IT_AO_10] =_T("A/O 10");

        g_mapIoTypeName[IT_AI_12] =_T("A/I 12");
        g_mapIoTypeName[IT_AO_12] =_T("A/O 12");

        g_mapIoTypeName[IT_AI_14] =_T("A/I 14");
        g_mapIoTypeName[IT_AO_14] =_T("A/O 14");

        g_mapIoTypeName[IT_AI_16] =_T("A/I 16");
        g_mapIoTypeName[IT_AO_16] =_T("A/O 16");

        g_mapIoTypeName[IT_AI_18] =_T("A/I 18");
        g_mapIoTypeName[IT_AO_18] =_T("A/O 18");

        g_mapIoTypeName[IT_AI_20] =_T("A/I 20");
        g_mapIoTypeName[IT_AO_20] =_T("A/O 20");

        g_mapIoTypeName[IT_AI_24] =_T("A/I 24");
        g_mapIoTypeName[IT_AO_24] =_T("A/O 24");

        g_mapIoTypeName[IT_AI_32] =_T("A/I 32");
        g_mapIoTypeName[IT_AO_32] =_T("A/O 32");

        g_mapIoTypeName[IT_LONG_I] =_T("LONG IN");
        g_mapIoTypeName[IT_LONG_O] =_T("LONG OUT");

        g_mapIoTypeName[IT_FLOAT_I] =_T("FLOAT IN");
        g_mapIoTypeName[IT_FLOAT_O] =_T("FLOAT OUT");

        g_mapIoTypeName[IT_DOUBLE_I] =_T("DOUBLE IN");
        g_mapIoTypeName[IT_DOUBLE_O] =_T("DOUBLE OUT");

        g_mapIoTypeName[IT_STR_I] =_T("STR IN");
        g_mapIoTypeName[IT_STR_O] =_T("STR OUT");
    }

    std::map<DT_IO_TYPE, StdString>::iterator ite;
    ite = g_mapIoTypeName.find(eType);

    if (ite == g_mapIoTypeName.end())
    {
        STD_DBG(_T("Type %d"), eType);
        return g_mapIoTypeName[IT_NONE];
    }
    return ite->second;
}


/**
 *  @brief  IO種別名称取得.
 *  @param  [in] eType データ種別
 *  @retval データ名 
 *  @note   
 */
DT_IO_TYPE GetIoTypeFromName(StdString strIoType)
{
    if (g_mapIoTypeName.size() == 0)
    {
        GetIoTypeName(IT_NONE);
    }

    std::map<DT_IO_TYPE, StdString>::iterator ite;
    for (ite  = g_mapIoTypeName.begin();
         ite != g_mapIoTypeName.end();
         ite++)
    {
        if (ite->second == strIoType)
        {
            return ite->first;
        }
    }
    return IT_NONE;
}


/**
 *  @brief  種別変換
 *  @param  [in] eIoType IO種別
 *  @param  [out] pDir   方向
 *  @param  [out] pType  種別
 *  @param  [out] pBit   ビット長
 *  @retval なし
 *  @note   
 */
void ConvIoType(DT_IO_TYPE eIoType, DT_IO_DIR* pDir, DAT_TYPE* pType, int* pBit)
{
    *pDir  = static_cast<DT_IO_DIR>(eIoType & IO_MASK);
    *pType = static_cast<DAT_TYPE> (eIoType & DAT_MASK);
    *pBit  = static_cast<int>      (eIoType & BL_MASK);
}


/**
 *  @brief  IOプロパティ項目名取得
 *  @param  [in] eIoType IOプロパティ項目
 *  @retval 項目名
 *  @note   
 */
StdString GetPropertyColName(E_IO_PROPERTY_COL eCol)
{
    StdString strRet;
    switch(eCol)
    {
    case COL_ID:        {strRet = GET_SYS_STR(STR_IOS_ID); break;}
    case COL_ADRESS:    {strRet = GET_SYS_STR(STR_IOS_ADDRESS);break;}
    case COL_TYPE:      {strRet = GET_SYS_STR(STR_IOS_TYPE);break;}
    case COL_NAME:      {strRet = GET_SYS_STR(STR_IOS_NAME);break;}
    case COL_INIT:      {strRet = GET_SYS_STR(STR_IOS_INIT);break;}
    case COL_NOTE:      {strRet = GET_SYS_STR(STR_IOS_NOTE);break;}
    case COL_LEN:       {strRet = GET_SYS_STR(STR_IOS_LEN);break;}
    case COL_UNIT:      {strRet = GET_SYS_STR(STR_IOS_UNIT);break;}
    case COL_CONV:      {strRet = GET_SYS_STR(STR_IOS_CONV);break;}
    default:
        break;
    }

    return strRet;
}

/**
 *  @brief  IOプロパティ項目使用可否取得
 *  @param  [in] eListType
 *  @param  [in] iCol
 *  @retval 表示可否
 *  @note   
 */
bool GetPropertyColDisp(E_LIST_TYPE eListType, int iCol)
{
    std::set<int> setList;

    if (g_lstSetUseProperty.size() == 0)
    {
        using namespace boost::assign;
    
        //LT_CONFIG 
        setList         += COL_ID, 
                           COL_ADRESS, 
                           COL_TYPE, 
                           COL_NAME,
                           COL_INIT,
                           COL_NOTE,
                           COL_LEN,
                           COL_UNIT,
                           COL_CONV,
                           COL_USER;
        g_lstSetUseProperty.push_back(setList);
        setList.clear();

        //LT_REF
        setList         += COL_ID, 
                           COL_TYPE, 
                           COL_NAME; 
        g_lstSetUseProperty.push_back(setList);
        setList.clear();

        //LT_EXEC
        setList         += COL_ID, 
                           COL_TYPE, 
                           COL_NAME,
                           COL_UNIT; 
        g_lstSetUseProperty.push_back(setList);
        setList.clear();
    }

    std::set<int>* pSet;
    switch (eListType)
    {
    case LT_CONFIG: {pSet = &g_lstSetUseProperty[0]; break;}
    case LT_REF:    {pSet = &g_lstSetUseProperty[1]; break;}
    case LT_EXEC:   {pSet = &g_lstSetUseProperty[2]; break;}
    default:
        return false;
    }

    std::set<int>::iterator ite;
    ite = pSet->find(iCol);

    if (ite == pSet->end())
    {
        return false;
    }
    return true;
}

/**
 *  @brief  IOプロパティ項目編集可・不可
 *  @param  [in] eListType
 *  @param  [in] iCol
 *  @retval true 編集可
 *  @note   
 */
bool GetPropertyColEdit(E_LIST_TYPE eListType, int iCol)
{
    if (eListType == LT_REF){return false;}
    else if (eListType == LT_EXEC){return false;}

    switch(iCol)
    {
    case COL_ID:         {return false;}    //!< データID        ×
    case COL_ADRESS:     {return false;}    //!< アドレス        ×
    case COL_TYPE:       {return true;}     //!< 種別            ◯
    case COL_NAME:       {return true;}     //!< 名称            ◯
    case COL_INIT:       {return true;}     //!< 初期値          ◯
    case COL_NOTE:       {return true;}     //!< 説明            ◯
    case COL_LEN:        {return true;}     //!< データ長        ◯
    case COL_UNIT:       {return true;}     //!< 単位            ◯
    case COL_CONV:       {return true;}     //!< 変換係数        ◯
    case COL_USER:       {return false;}    //!< ユーザ定義      ×
    }
    return false;
}


/**
 *  @brief  IOプロパティ項目名取得
 *  @param  [in] eIoType IOプロパティ項目
 *  @retval 項目名
 *  @note   
 */
StdString GetConnectColName(E_IO_CONNECT_COL  eCol)
{
    StdString strRet;
    switch(eCol)
    {
    case COL_VAL_DIRECT:     {strRet = GET_SYS_STR(STR_IOC_VAL_DIRECT); break;}
    case COL_VAL:            {strRet = GET_SYS_STR(STR_IOC_VAL)       ;break;}
    case COL_CONNECT:        {strRet = GET_SYS_STR(STR_IOC_CONNECT)   ;break;}
    default:
        break;
    }

    return strRet;
}


//!< IO接続項目表示可否取得
bool GetConnectColDisp(E_LIST_TYPE eListType, int  iCol)
{

                        //!<               設定  参照  実行
    // COL_VAL_DIRECT, //!< 数値(直値)     ×    ×    ○
    // COL_VAL,        //!< 数値           ×    ×    ○
    // COL_CONNECT,    //!< 接続先         ○    ○    ×


    std::set<int> setList;

    if (g_lstSetUseConnect.size() == 0)
    {
        using namespace boost::assign;
    
        //LT_CONFIG 
        setList         += COL_CONNECT; 
        g_lstSetUseConnect.push_back(setList);
        setList.clear();

        //LT_REF
        setList         += COL_CONNECT; 
        g_lstSetUseConnect.push_back(setList);
        setList.clear();

        //LT_EXEC
        setList         += COL_VAL_DIRECT, 
                           COL_VAL;
        g_lstSetUseConnect.push_back(setList);
        setList.clear();
    }

    std::set<int>* pSet;
    switch (eListType)
    {
    case LT_CONFIG: {pSet = &g_lstSetUseConnect[0]; break;}
    case LT_REF:    {pSet = &g_lstSetUseConnect[1]; break;}
    case LT_EXEC:   {pSet = &g_lstSetUseConnect[2]; break;}
    default:
        return false;
    }

    std::set<int>::iterator ite;
    ite = pSet->find(iCol);

    if (ite == pSet->end())
    {
        return false;
    }
    return true;
}


}//namespace IO_COMMON
