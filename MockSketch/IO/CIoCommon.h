/**
 * @brief			CIoCommon ヘッダーファイル
 * @file			CIoCommon.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

#ifndef  _IO_COMMON_H__
#define  _IO_COMMON_H__
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "../include/IoDef.h"
/*---------------------------------------------------*/
/*  Class                                            */
/*---------------------------------------------------*/

/*---------------------------------------------------*/
/*  Enums                                            */
/*---------------------------------------------------*/

namespace IO_COMMON
{

/*
IOリスト表示について

   





*/


//--------------
//<! IO特性項目
//--------------
enum E_IO_PROPERTY_COL              // リスト表示種別 (GetPropertyColで実装)
{                   //!<               設定  参照  実行  |編集可・不可(設定時のみ)
    COL_ID,         //!< データID       ○    ○    ○   | ×
    COL_ADRESS,     //!< アドレス       ○    ×    ×   | ×
    COL_TYPE,       //!< 種別           ○    ○    ○   | ◯
    COL_NAME,       //!< 名称           ○    ○    ○   | ◯
    COL_INIT,       //!< 初期値         ○    ×    ×   | ◯
    COL_NOTE,       //!< 説明           ○    ×    ×   | ◯
    COL_LEN,        //!< データ長       ○    ×    ×   | ◯
    COL_UNIT,       //!< 単位           ○    ×    ○   | ◯
    COL_CONV,       //!< 変換係数       ○    ×    ×   | ◯
    COL_USER,       //!< ユーザ定義     ○    ×    ×   | ×

    COL_PROPERTY_MAX,

};


//--------------------
//<! IO特性接続項目
//--------------------
enum E_IO_CONNECT_COL                // リスト表示種別
{                   //!<               設定  参照  実行
    COL_VAL_DIRECT, //!< 数値(直値)     ×    ×    ○
    COL_VAL,        //!< 数値           ×    ×    ○
    COL_CONNECT,    //!< 接続先         ○    ○    ×

    COL_CONNECT_MAX,

};


//--------------------
//<! リスト表示種別
//--------------------
enum E_LIST_TYPE
{
    LT_NONE,        //!< 未設定
    LT_CONFIG,      //!< 設定
    LT_REF,         //!< 参照
    LT_EXEC,        //!< 実行
};


//--------------------
//<! 保管形式
//--------------------
enum E_INTERPOLATION
{
    IP_NONE,    // 補間なし
    IP_POLY,    // 多項式補間
    IP_PICE,    // 区分線形補間
};


//データサイズ取得
int GetUniValLen(DAT_TYPE eType);

// IO種別名称取得
StdString GetIoTypeName(DT_IO_TYPE eType);

//!< IO種別取得
DT_IO_TYPE GetIoTypeFromName(StdString strIoType);

//!< IO種別取得
void ConvIoType(DT_IO_TYPE eIoType, DT_IO_DIR* pDir, DAT_TYPE* pType, int* pBit) ;

//!< IOプロパティ項目名取得
StdString GetPropertyColName(E_IO_PROPERTY_COL eCol);

//!< IOプロパティ項目表示可否取得
bool GetPropertyColDisp(E_LIST_TYPE eListType, int iCol);

//!< IOプロパティ編集可・不可
bool GetPropertyColEdit(E_LIST_TYPE eListType, int iCol) ;

//!< IO接続項目名取得
StdString GetConnectColName(E_IO_CONNECT_COL  eCol);

//!< IO接続項目表示可否取得
bool GetConnectColDisp(E_LIST_TYPE eListType, int  iCol);

//!< IO接続編集可・不可
//bool GetConnectColEdit(E_LIST_TYPE eListType, E_IO_CONNECT_COL  eCol);


};//namespace IO_COMMON

#endif  //_IO_COMMON_H__
