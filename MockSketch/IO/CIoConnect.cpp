/**
 * @brief			CIoConnect実装ファイル
 * @file			CIoConnect.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CIoConnect.h"
#include "./CIoProperty.h"
#include "./CIoPropertyBlock.h"
#include "./CIoDef.h"
#include "./CIoRef.h"
#include "DrawingObject/CDrawingScriptBase.h"
#include "System/CSystem.h"
#include "Utility/CUtility.h"

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CIoConnect::CIoConnect():
m_iConnctIoId       (-1),
m_iConnctParentId   (-1)
{
}

/**
 *  @brief  コピーコンストラクタ.
 *  @param  [in] Connect コピー元
 *  @retval なし     
 *  @note
 */
CIoConnect::CIoConnect(const CIoConnect& Connect)
{
    *this = Connect;
}

/**
 *  @brief  コンストラクター.
 *  @param  [in] pParent 親
 *  @retval なし     
 *  @note
 */
/*
CIoConnect::CIoConnect(CIoProperty* pParent)
{
    m_pParent = pParent;
}
*/

/**
 *  @brief  デストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CIoConnect::~CIoConnect()
{
}

/**
 *  @brief   代入演算子
 *  @param   [in] obj 代入元 
 *  @retval  代入値
 *  @note 
 */
CIoConnect& CIoConnect::operator = (const CIoConnect & obj)
{
    m_iConnctParentId = obj.m_iConnctParentId;
    m_iConnctIoId     = obj.m_iConnctIoId;

    m_pConnectParent  = obj.m_pConnectParent;
    m_pConnect        = obj.m_pConnect;
    return *this;
}


/**
 * @brief   等価演算子
 * @param   [in] obj 値
 * @return  ture 同値
 * @note    
 */
bool CIoConnect::operator == (const CIoConnect & obj) const
{
    if(m_iConnctParentId != obj.m_iConnctParentId){return false;}
    if(m_iConnctIoId     != obj.m_iConnctIoId    ){return false;}
    if(m_pConnectParent  != obj.m_pConnectParent ){return false;}
    
    CIoProperty* pPropSrc = NULL;
    CIoProperty* pPropDst = NULL;

    if (std::shared_ptr<CIoProperty> tmp = obj.m_pConnect.lock())
    {
        pPropDst = tmp.get();
    }

    if (std::shared_ptr<CIoProperty> tmp = m_pConnect.lock())
    {
        pPropSrc = tmp.get();
    }

    if ((pPropDst == NULL) ||
        (pPropSrc == NULL))
    {
        return false;
    }

    if(pPropSrc        != pPropDst       ){return false;}
    return true;
}

/**
 * @brief   不等価演算子
 * @param   [in] obj 値
 * @return  false 同値
 * @note    
 */
bool CIoConnect::operator != (const CIoConnect & obj) const
{
    return !(*this == obj);
}

/**
 *  @brief   接続先取得
 *  @param   なし
 *  @retval  接続先
 *  @note    
 */
CIoProperty* CIoConnect::GetConnect() const
{
    if( std::shared_ptr<CIoProperty> tmp = m_pConnect.lock() )
    {
        return tmp.get();
    }
    return NULL;
}

/**
 *  @brief   接続先親オブジェクト取得
 *  @param   なし
 *  @retval  接続先親オブジェクト取得
 *  @note    
 */
CIoDefBase* CIoConnect::GetConnectParent() const
{
    return m_pConnectParent;
}


/**
 *  @brief   接続先ID取得
 *  @param   [out] pParentId 親オブジェクトID
 *  @param   [out] pIoId     IOID
 *  @retval  なし
 *  @note    
 */
void CIoConnect::GetId(int* pParentId, int* pIoId) const
{

    *pParentId =  m_iConnctParentId;
    *pIoId     =  m_iConnctIoId;
}

/**
 *  @brief   接続先設定
 *  @param   [in] pIoDef 接続先
 *  @param   [in] ioProp 接続先
 *  @retval  true 成功
 *  @note 
 */
bool CIoConnect::SetConnect(CIoDefBase* pIoDef,
                            int iIoId)
{
    STD_ASSERT(pIoDef);


    m_pConnectParent = pIoDef;

    std::weak_ptr<CIoProperty> prop;

    bool bRet;
    
    bRet = pIoDef->GetBlock()->GetPropertyWeak(&prop, iIoId);

    if (!bRet)
    {
        return false;
    }

    m_pConnect       = prop;

    if( std::shared_ptr<CIoProperty> tmp = prop.lock() )
    {
        m_iConnctIoId    = tmp->GetId();
    }
    else
    {
        return false;
    }

    const type_info& info = typeid( *m_pConnectParent );  // 実行時型情報を取得
    

    // 接続するIOのオブジェクトIDを取得する
    //
    // ・接続元が CIoDefの場合 
    //        m_iConnctParentIdは0とする
    //
    // ･ 接続元が CIoRefの場合
    //        CPartsDef内のデータのはず
    //        親オブジェクトを辿り、ObjectIdを設定する

    if( info == typeid( CIoRef ) )
    {
        CIoRef* pRef = dynamic_cast<CIoRef*>(m_pConnectParent);
        CDrawingScriptBase* pBase = pRef->GetParent();
        m_iConnctParentId =  pBase->GetId();
    }
    else
    {
        m_iConnctParentId = 0;
    }
    return true;
}

/**
 *  @brief   接続先名称取得
 *  @param   なし
 *  @retval  接続先名称
 *  @note 
 */
StdString CIoConnect::GetConnectStr() const
{
    StdString strRet;
    std::shared_ptr<CIoProperty> pProp = m_pConnect.lock();

    if( !pProp )
    {
        return strRet;
    }

    if (m_pConnectParent)
    {
        strRet = m_pConnectParent->GetName();
        if (strRet != _T(""))
        {
            strRet += _T(".");
        }
        strRet += pProp->GetName();
    }
    return strRet;
}

/**
 *  @brief   データ設定
 *  @param   なし
 *  @retval  なし
 *  @note    全データload後実行する
 */
void CIoConnect::SetUp()
{


}


/**
 * @brief   シリアル化(ロード)
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  なし
 * @note	
 */
template<class Archive>
void CIoConnect::load(Archive& ar, const unsigned int version)
{
    SERIALIZATION_INIT

        SERIALIZATION_LOAD("ConnectParent", m_iConnctParentId);
        SERIALIZATION_LOAD("ConnectIoId"  , m_iConnctIoId);

    MOCK_EXCEPTION_FILE(e_file_read);
}


/**
 * @brief   シリアル化(セーブ)
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  なし
 * @note	
 */
template<class Archive>
void CIoConnect::save(Archive& ar, const unsigned int version) const
{
    SERIALIZATION_INIT

        SERIALIZATION_SAVE("ConnectParent", m_iConnctParentId);
        SERIALIZATION_SAVE("ConnectIoId"  , m_iConnctIoId);

    MOCK_EXCEPTION_FILE(e_file_write);
}

//load save のインスタンスが生成されないため
//ダミーとして実装
void CIoConnect::Dummy()
{
    unsigned int iVersion = 0;

    StdStreamOut outFs(_T(""));
    StdStreamIn  inFs(_T(""));
    //boost::filesystem::ofstream outTxtFs(_T(""));
    //boost::filesystem::ifstream inTxtFs(_T(""));

    boost::archive::text_woarchive txtOut(outFs); 
    boost::archive::text_wiarchive txtIn(inFs);

    save<boost::archive::text_woarchive>( txtOut, iVersion);
    load<boost::archive::text_wiarchive>( txtIn,  iVersion);

    StdXmlArchiveOut outXml(outFs);
    StdXmlArchiveIn inXml(inFs);

    save<StdXmlArchiveOut>(outXml, iVersion);
    load<StdXmlArchiveIn>(inXml, iVersion);


    boost::filesystem::ofstream outFsStd(_T(""));
    boost::filesystem::ifstream inFsStd(_T(""));

    boost::archive::binary_oarchive outBin(outFsStd);
    boost::archive::binary_iarchive inBin(inFsStd);

    save<boost::archive::binary_oarchive>(outBin, iVersion);
    load<boost::archive::binary_iarchive>(inBin, iVersion);

}



//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void TEST_CIoConnect()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    CIoConnect connect1;
    CIoConnect connect2;

    namespace fs = boost::filesystem;
    StdPath  pathTest = CSystem::GetSystemPath();
    pathTest /= _T("TEST_CIoConnect.xml");




    //保存
    StdStreamOut outFs(pathTest);
    {
        StdXmlArchiveOut outXml(outFs);
        outXml << boost::serialization::make_nvp("Root", connect1);
    }
    outFs.close();


    //読込
    StdStreamIn inFs(pathTest);
    {
        StdXmlArchiveIn inXml(inFs);
        inXml >> boost::serialization::make_nvp("Root", connect2);
    }
    inFs.close();


    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));

}
#endif //_DEBUG