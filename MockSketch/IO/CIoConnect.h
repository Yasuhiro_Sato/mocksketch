/**
 * @brief			CIoConnect ヘッダーファイル
 * @file			CIoConnect.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

#ifndef  _IO_CONNECT_H__
#define  _IO_CONNECT_H__
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/

/*---------------------------------------------------*/
/*  Class                                            */
/*---------------------------------------------------*/
class CIoProperty;
class CIoDefBase;

/*---------------------------------------------------*/
/*  Enums                                            */
/*---------------------------------------------------*/


/**
 * @class   CIoConnect
 * @brief                       
 */
class CIoConnect
{
public:
    //!< コンストラクタ
    CIoConnect();

    //!< コピーコンストラクタ
    CIoConnect(const CIoConnect& Connect);

    //!< コンストラクタ
    //CIoConnect(CIoProperty* pParent);

    //!< デストラクタ
    virtual ~CIoConnect();

    //!< 代入演算子
    virtual CIoConnect& operator = (const CIoConnect & obj);

    //!< 等価演算子
    virtual bool operator == (const CIoConnect & obj) const;

    //!< 不等価演算子
    virtual bool operator != (const CIoConnect & obj) const;


    //!< 接続先取得
    CIoProperty* GetConnect() const;

    //!< 接続先親オブジェクト取得
    CIoDefBase* GetConnectParent() const;

    //!< 接続先ID取得
    void GetId(int* pParentId, int* pIoId) const;

    //!< 接続先設定
    bool SetConnect(CIoDefBase* pIoDef, int iIoId);

    //!< 接続先名称取得
    StdString GetConnectStr() const;
    
    // データ設定
    void SetUp();

    //ダミー
    void Dummy();

protected:
friend  CIoDefBase;

    //!親設定
    void SetParent(CIoDefBase* pBase){m_pConnectParent = pBase;}

protected:

    //----------
    //保存データ
    //----------
    int m_iConnctParentId;    //接続先親オブジェクトID

    int m_iConnctIoId;        //接続先IoID

    //------------
    //非保存データ
    //------------
    //!<接続データ
    std::weak_ptr<CIoProperty>  m_pConnect;

    //!<接続元親データ
    CIoDefBase*     m_pConnectParent;

    
private:
    friend class boost::serialization::access;  
    BOOST_SERIALIZATION_SPLIT_MEMBER();

    template<class Archive>
    void load(Archive& ar, const unsigned int version);

    template<class Archive>
    void save(Archive& ar, const unsigned int version) const;
};

#endif  //_IO_CONNECT_H__
