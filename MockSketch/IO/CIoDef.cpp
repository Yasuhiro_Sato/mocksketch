/**
 * @brief			CIoDef実装ファイル
 * @file			CIoDef.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CIoDef.h"
#include "./CIoRef.h"
#include "./CIoProperty.h"
#include "./CIoPropertyBlock.h"
#include "./CIoConnect.h"
#include "./CIoAccess.h"
#include "System/CSystem.h"
#include "Utility/CUtility.h"
#include "DefinitionObject/ObjectDef.h"
#include "DrawingObject/CDrawingParts.h"
#include "DefinitionObject/CPartsDef.h"

#include <boost/serialization/export.hpp> 

BOOST_CLASS_EXPORT(CIoDef);

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CIoDef::CIoDef():
m_iMaxDataSize  (256),
m_pParent   (NULL)
{
    m_psPropetyBlock = std::make_shared<CIoPropertyBlock>();
    m_pBlock         = m_psPropetyBlock.get();
    m_eDispIo        = IO_COMMON::LT_CONFIG;
}

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CIoDef::CIoDef(const CIoDef& ioDef):
m_pParent   (NULL) //SetParentで設定する
{
    m_psPropetyBlock = std::make_shared<CIoPropertyBlock>(*ioDef.m_psPropetyBlock.get());
    m_iMaxDataSize   = ioDef.m_iMaxDataSize;
    m_pBlock         = m_psPropetyBlock.get();
    m_eDispIo        = IO_COMMON::LT_CONFIG;
}

/**
 *  @brief  デストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CIoDef::~CIoDef()
{
}

/**
 *  @brief  リスト初期化
 *  @param  [in] pVirtualList
 *  @retval true 成功
 *  @note
 */
bool CIoDef::InitList(CVirtualCheckList* pVirtualList)
{
    bool bRet;
    bRet = CIoDefBase::InitList(pVirtualList);
    pVirtualList->SetContextMenuFunc(CallbackContextMenu);
    pVirtualList->SetOnCommandFunc(CallbackOnCommand);
    return bRet;
}

/**
 *  @brief  生成処理
 *  @param  [in] pParent
 *  @param  [in] pIoAccess
 *  @retval なし
 *  @note   
 */
void CIoDef::Create(CObjectDef* pParent,
                    std::shared_ptr<CIoAccess> pIoAccess)
{


    STD_ASSERT(m_pBlock);
    m_pParent  = pParent;
    m_psAccess = pIoAccess;
    m_psAccess->SetIoDef(this);
    m_psAccess->Init();

    int iPageMax = m_psAccess->GetMaxPage();
    GetBlock()->SetPageMax(iPageMax);


    //!< 基本的にユーザ定義データは CIoAccessBoardで設定して使用する
    int iUserItemNum = m_psAccess->GetUserItemNumber();
    m_pBlock->SetUserDataNum(iUserItemNum);
    if(m_psAccess->GetAccessType() == CIoAccess::ACCESS_BOARD)
    {
        //TODO:ボードの場合は。ボードの設定に合わせる
        //m_psAccess->GetMaxPage();
        

    }
    else
    {

    }
}

/**
 *  @brief  停止設定.
 *  @param  なし
 *  @retval true 設定成功
 *  @note   
 */
bool CIoDef::Stop()
{
    m_eDispIo = IO_COMMON::LT_CONFIG;
    return true;
}

/**
 *  @brief  IO定義ブロック取得
 *  @param  なし
 *  @retval IO定義ブロックへのポインタ
 *  @note
 */
CIoPropertyBlock*  CIoDef::GetBlock() const
{
    return m_psPropetyBlock.get();
}


/**
 *  @brief  IO定義ブロック設定
 *  @param  なし
 *  @retval プロパティブロックへのポインタ
 *  @note
 */
 bool CIoDef::SetBlockWeak(std::weak_ptr<CIoPropertyBlock>* pWeak) const
 {
    *pWeak = m_psPropetyBlock;
    return true;
 }

 /**
 *  @brief  オブジェクト定義取得
 *  @param  なし
 *  @retval オブジェクト定義
 *  @note   
 */
 /*
CObjectDef* CIoDef::GetDef() const
{
    return m_pParent;
}
*/

/**
 *  @brief  IDによる接続追加
 *  @param  [in] iIdFrom   接続先オブジェクトID
 *  @param  [in] iIoIdFrom 接続先IoID
 *  @param  [in] iIoIdTO   接続元IoID
 *  @retval true 成功
 *  @note   
 */
bool CIoDef::AddConnectById(int iIdFrom, int iIoIdFrom, int iIoIdTo)
{
    if (!m_pParent)
    {
        return false;
    }

    CPartsDef* pCtrl;

    pCtrl = dynamic_cast<CPartsDef*>(m_pParent);

    if (!pCtrl)
    {
        return false;
    }

    std::shared_ptr<CIoConnect> spConnect = std::make_shared<CIoConnect>();
    if (iIdFrom == 0)
    {
        CIoDef* pIoDef;
        pIoDef = pCtrl->GetIoDef();
        spConnect->SetConnect(pIoDef, iIoIdFrom);
    }
    else
    {
        auto pObject = pCtrl->GetObjectById(iIdFrom);
        auto pBase = std::dynamic_pointer_cast<CDrawingScriptBase>(pObject);
        if (!pBase)
        {
            return false;
        }

        CIoRef* pIoRef;
        pIoRef = pBase->GetIoRef();
        spConnect->SetConnect(pIoRef, iIoIdFrom);

    }

    bool bConnect;
    bConnect = AddConnect( iIoIdTo, spConnect);
    return bConnect;
}

/**
 *  @brief  描画領域取得
 *  @param  なし
 *  @retval 描画領域
 *  @note   
 */
CPartsDef* CIoDef::GetPartsDef() const
{
    if (m_pParent == NULL)
    {
        return NULL;
    }
            
    if ((m_pParent->GetObjectType() != VIEW_COMMON::E_PARTS) &&
        (m_pParent->GetObjectType() != VIEW_COMMON::E_FIELD))
    {
        return NULL;
    }

    CPartsDef* pCtrl;
    
    pCtrl = dynamic_cast<CPartsDef*>(m_pParent);

    return pCtrl;
}

/**
 *  @brief  名称取得
 *  @param  なし
 *  @retval 名称
 *  @note   IO接続時に相手側を表示するために必要
 */
StdString CIoDef::GetName() const
{
    // IO接続の名称は基本的に
    // IoDef →  IoName
    // IoRef →  Object.IoName
    // とする
    
    // CPartsDefが親になる場合は、名称無し
    // CDrawingObjectが親になる場合は、そのオブジェクトの名称を
    // 返す

    /*
    if (m_pParent)
    {
        return m_pParent->GetName();
    }
    */
    return _T("");

}

/**
 *  @brief   ページ設定
 *  @param   なし
 *  @retval  true:成功
 *  @note
 */
bool CIoDef::SetPage()
{
    bool bRet;
    int iMaxPage;
    iMaxPage = m_psAccess->GetMaxPage();
    bRet = m_psPropetyBlock->SetPageMax(iMaxPage);


    if (m_iPage >= iMaxPage)
    {
        m_iPage = iMaxPage - 1;
    }

    return bRet;
}

/**
 *  @brief  ページ数設定
 *  @param  [in] iPage ページ数
 *  @retval true: 成功
 *  @note
 */
bool CIoDef::SetMaxPage(int iPage)
{
    bool bRet;

    bRet = m_psAccess->SetMaxPage(iPage);

    if (bRet)
    {
        bRet = SetPage();
    }
    return bRet;
}


/**
 *  @brief  アドレス再計算
 *  @param  [in] iPage ページ
 *  @retval true 成功
 *  @note
 */
bool CIoDef::RecalcAddress(int iPage)
{
    return m_psPropetyBlock->RecalcAddress(iPage);
}

/**
 *  @brief  更新
 *  @param  なし 
 *  @retval true 成功
 *  @note
 */
bool CIoDef::Update()
{
    return false;
}

/**
 *  @brief  データ更新
 *  @param  [in] iPage ページ
 *  @retval true: 成功
 *  @note
 */
bool CIoDef::UpdateData(int iPage)
{
    //TODO:実装
    return false;
}

/**
 *  @brief  実行完了
 *  @param  なし
 *  @retval true 成功
 *  @note   
 */
bool CIoDef::Finish()
{
    //TODO:実装
    return false;
}


/**
 * @brief   コンテキストメニュー表示
 * @param   [in]    point  クリック位置
 * @retval  true メニュー表示あり
 * @note
 */
bool  CIoDef::OnContextMenu(CVirtualCheckList* pCheckList,
                        POINT ptClick, int nRow, int nCol)
{
    CMenu menu;
    menu.CreatePopupMenu();

    menu.AppendMenu(MF_STRING, ID_EDIT_COPY, GET_STR(STR_MNU_WATCH_COPY));
    menu.AppendMenu(MF_STRING, ID_EDIT_PASTE,GET_STR(STR_MNU_WATCH_PASTE));

    menu.TrackPopupMenu(
        TPM_LEFTALIGN  |    //クリック時のX座標をメニューの左辺にする
        TPM_RIGHTBUTTON,    //右クリックでメニュー選択可能とする
        ptClick.x,ptClick.y,//メニューの表示位置
        pCheckList          //このメニューを所有するウィンドウ
    );


    menu.DestroyMenu();
    return true;
}


/**
 * @brief   コンテキストメニューコールバック
 * @param   [in]  pParent    呼び出し元     
 * @param   [in]  pCheckList リストコントロール     
 * @param   [in]  ptClick クリック位置       
 * @param   [in]  nRow    データを取得する行         
 * @param   [in]  nCol    データを取得する列        
 * @retval   なし
 * @note	
 */
void CIoDef::CallbackContextMenu
                        (LPVOID pParent, CVirtualCheckList* pCheckList,
                        POINT ptClick, int nRow, int nCol)
{
    CIoDef* pBase = reinterpret_cast<CIoDef*>(pParent);
    pBase->OnContextMenu( pCheckList, ptClick, nRow, nCol);
}




/**
 * @brief   コンテキストメニューコールバック
 * @param   [in]  pParent    呼び出し元     
 * @param   [in]  nCode      コマンドコード
 * @param   [in]  nID        コマンドID
 * @retval  true 処理を実行
 * @note	
 */
bool CIoDef::CallbackOnCommand (LPVOID pParent, 
                                CVirtualCheckList* pCheckList,
                                  UINT nCode, UINT nID)
{
    CIoDef* pBase = reinterpret_cast<CIoDef*>(pParent);
    return pBase->OnCommand( pCheckList, nCode, nID);
}


/**
 * @brief   コマンド
 * @param   [in]  nCode      コマンドコード
 * @param   [in]  nID        コマンドID
 * @retval  true 処理を実行
 * @note
 */
bool  CIoDef::OnCommand(CVirtualCheckList* pCheckList, UINT nCode, UINT nID)
{
    bool bRet = false;
    int iRow;
    int iCol;

    pCheckList->GetCurCell(&iRow, &iCol);
    switch(nID)
    {
    case ID_EDIT_COPY:
        bRet = Copy(iRow);
        break;

    case ID_EDIT_PASTE:
        bRet = Paste(iRow);
        break;

    default:
        break;
    }


    return bRet;
}

/**
 *  @brief  コピー
 *  @param  [in] iRow 行
 *  @retval true 成功
 *  @note   
 */
bool CIoDef::Copy(int iRow)
{
    //TODO:実装
    DB_PRINT(_T("COPY %d\n"), iRow);
    return true;
}

/**
 *  @brief  貼付け
 *  @param  [in] iRow 行
 *  @retval true 成功
 *  @note   
 */
bool CIoDef::Paste(int iRow)
{
    //TODO:実装
    DB_PRINT(_T("PASTE %d\n"), iRow);
    return false;
}

/**
 *  @brief  切り取り
 *  @param  [in] iRow 行
 *  @retval true 成功
 *  @note   
 */
bool CIoDef::Cut(int iRow)
{
    //TODO:実装

    return false;
}

/**
 *  @brief  挿入
 *  @param  [in] iRow 行
 *  @retval true 成功
 *  @note   
 */
bool CIoDef::Insert(int iRow)
{
    //TODO:実装

    return false;
}

/**
 *  @brief  削除
 *  @param  [in] iRow 行
 *  @retval true 成功
 *  @note   
 */
bool CIoDef::Delete(int iRow)
{
    //TODO:実装
    return false;
}


/**
 *  @brief  親オブジェクト設定
 *  @param  [in] pParent
 *  @retval なし
 *  @note   
 */
void CIoDef::SetParent(CObjectDef* pParent)
{
    m_pParent = pParent;
}

/**
 *  @brief  親オブジェクト取得
 *  @param  なし
 *  @retval 親オブジェクト
 *  @note   
 */
CObjectDef* CIoDef::GetParent()
{
    return m_pParent;
}


/**
 * @brief   スクリプト生成
 * @param   [out] pScript   スクリプト
 * @retval  true:成功
 * @note	
    Object名.Io.データ名 でアクセスすることを考える
    データ種別によって呼び出しを変える

    共通
    IN
    StdString str  = Object名.Io.データ名.Str()

    OUT
    StdString str  = Object名.Io.データ名.StrOut()
    Object名.Io.データ名.SetStr(StdString& str);

    //DI
    bool Object名.Io.データ名.IsOn()

    //DO
    Object名.Io.データ名.Set(bool bOn)
    bool Object名.Io.データ名.IsOut();

    //AI
    double Object名.Io.データ名.Val()
    UINT   Object名.Io.データ名.ValDirect() //変換前数値

    //AO
    double Object名.Io.データ名.ValOut()
    UINT   Object名.Io.データ名.ValDirectOut()
    bool   Object名.Io.データ名.Set(double dVal)      変換前数値
    bool   Object名.Io.データ名.SetDirect(double dVal)

    int(uint,double,float) in
    int Object名.Io.データ名.Val()

    int(uint,double,float) out
    int    Object名.Io.データ名.ValOut()
    void   Object名.Io.データ名.Set(int dVal)

    String 

    //配列的アクセスも用意したい



 */
bool CIoDef::CreateScript(std::vector<StdString>* pLstScript)
{
    //TODO:実装

    return true;
}

/**
 * @brief   読込後初期化処理
 * @param   なし
 * @retval  true 成功
 * @note	
 */
bool CIoDef::LoadAfter()
{
    bool bRet;
    bRet = CIoDefBase::LoadAfter();

    return bRet;
}




/**
 * @brief   シリアル化(ロード)
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  なし
 * @note	
 */
template<class Archive>
void CIoDef::load(Archive& ar, const unsigned int version)
{
    SERIALIZATION_INIT
        SERIALIZATION_LOAD("PropertyBlock",  m_psPropetyBlock);
        ar >> BOOST_SERIALIZATION_BASE_OBJECT_NVP(CIoDefBase);
    MOCK_EXCEPTION_FILE(e_file_read);
}


/**
 * @brief   シリアル化(セーブ)
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  なし
 * @note	
 */
template<class Archive>
void CIoDef::save(Archive& ar, const unsigned int version) const
{
    SERIALIZATION_INIT
        SERIALIZATION_SAVE("PropertyBlock",  m_psPropetyBlock);
        ar << BOOST_SERIALIZATION_BASE_OBJECT_NVP(CIoDefBase);
    MOCK_EXCEPTION_FILE(e_file_write);
}


//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
#include "Utility/VirtualTreeCtrl/VirtualCheckList.h"
#include "Utility/TestDlg.h"
#include "DefinitionObject/CPartsDef.h"

CTestDlg* g_IoDefDlg;

namespace TEST_IO_DEF_CHECK
{

//-------------------------
// データ初期化
//-------------------------
void InitData(CIoDef* pIoDef)
{



}

//-------------------------
// ラベル編集終了
//-------------------------
/*
void OnLvnEndlabeleditVlst(NMHDR *pNMHDR, LRESULT *pResult)
{
    NMLVDISPINFO *pDispInfo = reinterpret_cast<NMLVDISPINFO*>(pNMHDR);

    *pResult = 0;
	if(pDispInfo->item.pszText == NULL)
	{
        return;
    }

    int nRow = pDispInfo->item.iItem;
    int nCol = pDispInfo->item.iSubItem;

    StdString strData;
    strData = pDispInfo->item.pszText;


}
*/

//-------------------------
// 通知処理
//-------------------------
void OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResul)
{
    NMHDR* pNMHDR = reinterpret_cast<NMHDR*>(lParam);

    if (pNMHDR->code == LVN_ENDLABELEDIT)
    {
        CIoDef* pIoDef = reinterpret_cast<CIoDef*>(g_IoDefDlg->GetUserData());
        CVirtualCheckList* pList = reinterpret_cast<CVirtualCheckList*>(g_IoDefDlg->GetObj());
        pIoDef->OnLvnEndlabeleditVlst(pList, pNMHDR, pResul);
        return;
    }

    return;
}

//-------------------------
// ウインドウサイズ変更
//-------------------------
void TestOnSize(CWnd* pTestObj, UINT nType, int cx, int cy)
{
    CVirtualCheckList* pList;
    pList = dynamic_cast<CVirtualCheckList*>(pTestObj);
}

//!< コンボボックス変更
void TestOnSelChg(StdString strSel)
{
    CIoDef* pIoDef = reinterpret_cast<CIoDef*>(g_IoDefDlg->GetUserData());

    CVirtualCheckList* pVList;
    pVList = dynamic_cast<CVirtualCheckList*>(g_IoDefDlg->GetTestObj());
    STD_ASSERT(pVList != NULL);


    int iPage;
    iPage = _tstol( strSel.c_str() );

    pIoDef->SetCurrentPage(iPage);
    pIoDef->UpdateList(pVList);

}

//!< コンボボックス2変更
void TestOnSelChg2(StdString strSel)
{
    using namespace IO_COMMON;
    CIoDef* pIoDef = reinterpret_cast<CIoDef*>(g_IoDefDlg->GetUserData());

    IO_COMMON::E_LIST_TYPE eDispIo;
    if      (strSel == _T("DEF"))   { eDispIo = LT_CONFIG;}
    else if (strSel == _T("REF"))   { eDispIo = LT_REF;}
    else if (strSel == _T("EXEC"))  { eDispIo = LT_EXEC;}
    else                            { return;}


    CVirtualCheckList* pVList;
    pVList = dynamic_cast<CVirtualCheckList*>(g_IoDefDlg->GetTestObj());

    STD_ASSERT(pVList != NULL);


    pIoDef->TEST_SetDispMode(eDispIo);
    pIoDef->UpdateCol(pVList);
}

// ダイアログコールバック
//-------------------------
//  ウインドウプロシジャ
//-------------------------
LRESULT TestWindowProc(CWnd* pTestObj, UINT message, WPARAM wParam, LPARAM lParam)
{
    CVirtualCheckList* pList;
    pList = dynamic_cast<CVirtualCheckList*>(pTestObj);
    
    static UINT uiOldMsg = 0;

    if (uiOldMsg != message)
    {
        uiOldMsg = message;
#if 0
        DB_PRINT(_T("%s,%x,%x\n"), CUtil::ConvWinMessage(message),wParam, lParam);
#endif
    }

    LRESULT lRes = 0;
    if (message == WM_NOTIFY)
    {
        OnNotify( wParam, lParam, &lRes);
        return lRes;
    }
    return 0;
}

//-------------------------
//  ウインドウプロシジャ
//-------------------------
BOOL TestPreTransProc(CWnd* pTestObj, MSG* pMsg)
{
    CVirtualCheckList* pList;
    pList = dynamic_cast<CVirtualCheckList*>(pTestObj);
    
    static UINT uiOldMsg = 0;
    if ((WM_PAINT == pMsg->message)||
        (WM_TIMER == pMsg->message))
    {
        return 0;
    }

    if (uiOldMsg != pMsg->message)
    {
        uiOldMsg = pMsg->message;
        DB_PRINT(_T("DLG %s,%x,%x,%x\n"), 
            CUtil::ConvWinMessage(pMsg->message),
            pMsg->hwnd,
            pMsg->wParam, 
            pMsg->lParam);
    }
    return 0;
}

//-------------------------
//  ダイアログ初期化
//-------------------------
void TestOnInitDialog(CWnd* pTestObj)
{
    CVirtualCheckList* pList;

    pList = dynamic_cast<CVirtualCheckList*>(pTestObj);
    pList->Create( LVS_REPORT | LVS_ALIGNLEFT | LVS_OWNERDATA | WS_CHILD | 
                       WS_BORDER | WS_VISIBLE |WS_TABSTOP, CRect(), g_IoDefDlg, 10); 

    CIoDef* pIoDef = reinterpret_cast<CIoDef*>(g_IoDefDlg->GetUserData());

    pIoDef->InitList(pList);
}


} //namespace TEST_IO_DEF_CHECK


//===================================================
//===================================================
//===================================================
void TEST_CIoDefdialog(CIoDef* pIoDef)
{
    using namespace TEST_IO_DEF_CHECK;
    using namespace boost::assign;

    CTestDlg dlgTest;
    g_IoDefDlg = &dlgTest;
    CVirtualCheckList  list;

    std::vector<StdString> lstCombo;
    lstCombo += _T("000"), _T("001"), _T("002"), _T("003");

    std::vector<StdString> lstCombo2;
    lstCombo2 += _T("DEF"), _T("REF"), _T("EXEC");

    dlgTest.SetCombo(lstCombo);
    dlgTest.SetCombo2(lstCombo2);
    dlgTest.SetUserData(pIoDef);
    dlgTest.SetObj(&list);

    g_IoDefDlg = &dlgTest;

    // データ初期化
    InitData(pIoDef);


    list.UseVisibleCol();


    dlgTest.SetOnSize       (TestOnSize);
    dlgTest.SetWindowProc   (TestWindowProc);
    dlgTest.SetPreTransProc (TestPreTransProc);
    dlgTest.SetOnInit       (TestOnInitDialog);
    dlgTest.SetSelChgCb     (TestOnSelChg);
    dlgTest.SetSelChgCb2    (TestOnSelChg2);
    
    dlgTest.EnableAutoFit();


    dlgTest.DoModal();
}


//===================================================
//===================================================
//===================================================
void TEST_CIoDef_GetHeader()
{
    CIoDef  IoDef;

    StdString strTest;
    CVirtualCheckList::COL_SET colSet;

    std::shared_ptr<CIoAccess> pTmpAccess = 
        std::make_shared<CIoAccess>();

    StdString strUserItem1 = _T("R40,USER_ITEM_1"); 
    StdString strUserItem2 = _T("R40,USER_ITEM_2"); 

    pTmpAccess->AddUserItem(strUserItem1);
    pTmpAccess->AddUserItem(strUserItem2);

    StdString strName = _T("TEST_CIoDef_GetHeader");
    /*
    IoDef.Init(pTmpAccess, &strName);


    int iColMax = IoDef.GetColNum();
    STD_ASSERT(iColMax == (9 + 2 + 1));

    IoDef.GetHeader(&strTest, &colSet, 0);
    STD_ASSERT(strTest == GET_SYS_STR(STR_IOS_ID));

    IoDef.GetHeader(&strTest, &colSet, 10);
    STD_ASSERT(strTest == strUserItem2);

    IoDef.GetHeader(&strTest, &colSet, 11);
    STD_ASSERT(strTest == GET_SYS_STR(STR_IOC_CONNECT));
    */
}


//===================================================
//===================================================
//===================================================
void TEST_CIoDef_SetData(CIoDef& ioDef)
{
    using namespace IO_COMMON;
    CIoPropertyBlock* pblock;
    pblock = ioDef.GetBlock();

    ioDef.SetMaxPage(3);

    //データ追加
    //                                                  Offset
    //                                                  |  UnitNo
    //                                                  |  |    Len
    pblock->Add(0)->SetData(_T("Item_0_0"),  IT_AI_16, 0,   0,  0, _T("NOTE_1"), _T(""), _T(""));
    pblock->Add(0)->SetData(_T("Item_0_1"),  IT_AO_8 , 0,   0,  0, _T("NOTE_2"), _T(""), _T(""));
    pblock->Add(0)->SetData(_T("Item_0_2"),  IT_AI_32, 0,   0,  0, _T("NOTE_3"), _T(""), _T(""));
    pblock->Add(0)->SetData(_T("Item_0_3"),  IT_STR_I, 0,   0,256, _T("NOTE_4"), _T(""), _T(""));
    pblock->Add(0)->SetData(_T("Item_0_4"),  IT_AI_16, 0,   0,  0, _T("NOTE_5"), _T(""), _T(""));

    pblock->Add(1)->SetData(_T("Item_1_0"),  IT_DOUBLE_I, 0, 0,  0, _T("NOTE_1_1"), _T(""), _T(""));
    pblock->Add(1)->SetData(_T("Item_1_DI_0"),  IT_DI,    0, 0,  0, _T("NOTE_1_2"), _T(""), _T(""));
    pblock->Add(1)->SetData(_T("Item_1_DI_1"),  IT_DI,    1, 0,  0, _T("NOTE_1_2"), _T(""), _T(""));
    pblock->Add(1)->SetData(_T("Item_1_DI_2"),  IT_DI,    2, 0,  0, _T("NOTE_1_2"), _T(""), _T(""));
    pblock->Add(1)->SetData(_T("Item_1_DI_3"),  IT_DI,    3, 0,  0, _T("NOTE_1_2"), _T(""), _T(""));
    pblock->Add(1)->SetData(_T("Item_1_DI_4"),  IT_DI,    4, 0,  0, _T("NOTE_1_2"), _T(""), _T(""));
    pblock->Add(1)->SetData(_T("Item_1_DI_5"),  IT_DI,    5, 0,  0, _T("NOTE_1_2"), _T(""), _T(""));
    pblock->Add(1)->SetData(_T("Item_1_DI_6"),  IT_DI,    6, 0,  0, _T("NOTE_1_2"), _T(""), _T(""));
    pblock->Add(1)->SetData(_T("Item_1_DI_7"),  IT_DI,    7, 0,  0, _T("NOTE_1_2"), _T(""), _T(""));

    //                                                                  Note            Init    Conv
    pblock->Add(2)->SetData(_T("Item_2_0"),  IT_AI_16,     0, 0,  0, _T("NOTE_2_1"), _T(""), _T(""));
    pblock->Add(2)->SetData(_T("Item_2_1"),  IT_STR_I,     0, 0,  5, _T("NOTE_2_2"), _T(""), _T(""));
    pblock->Add(2)->SetData(_T("Item_2_2"),  IT_LONG_I,    0, 0,  0, _T("NOTE_2_3"), _T(""), _T(""));
    pblock->Add(2)->SetData(_T("Item_2_3"),  IT_FLOAT_I,   0, 0,  0, _T("NOTE_2_4"), _T(""), _T(""));
    pblock->Add(2)->SetData(_T("Item_2_4"),  IT_AI_16,     0, 0,  0, _T("NOTE_2_5"), _T(""), _T("20.0:500.0"));
    pblock->Add(2)->SetData(_T("Item_2_5"),  IT_AI_16,     0, 0,  0, _T("NOTE_2_6"), _T(""), _T("20.0:500.0"));
    pblock->Add(2)->SetData(_T("Item_2_6"),  IT_AO_16,     0, 0,  0, _T("NOTE_2_7"), _T(""), _T("20.0:500.0"));
    pblock->Add(2)->SetData(_T("Item_2_7"),  IT_AO_16,     0, 0,  0, _T("NOTE_2_8"), _T(""), _T("1,1"));
    pblock->Add(2)->SetData(_T("Item_2_8"),  IT_AO_16,     0, 0,  0, _T("NOTE_2_9"), _T(""), _T("[0:1],[100:101],[200:401]"));

    ioDef.RecalcAddress(-1);
}

//ファイルテスト
void TEST_CIoDef_File(CIoDef& ioDef)
{

    namespace fs = boost::filesystem;
    StdPath  pathTest = CSystem::GetSystemPath();
    pathTest /= _T("TEST_CIoDef.xml");

    CIoDef ioDef2;

    //保存
    StdStreamOut outFs(pathTest);
    {
        StdXmlArchiveOut outXml(outFs);
        outXml << boost::serialization::make_nvp("Root", ioDef);
    }
    outFs.close();


    //読込
    StdStreamIn inFs(pathTest);
    {
        StdXmlArchiveIn inXml(inFs);
        inXml >> boost::serialization::make_nvp("Root", ioDef2);
    }
    inFs.close();


    STD_ASSERT(ioDef == ioDef2);

}

//===================================================
//===================================================
//===================================================
void TEST_CIoDef()
{
    using namespace TEST_IO_DEF_CHECK;
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());


    TEST_CIoDef_GetHeader();


    CIoDef  IoDef;
    StdString strName = _T("HELLO");

    std::shared_ptr<CIoAccess> pTmpAccess = 
        std::make_shared<CIoAccess>();

    StdString strUserItem1 = _T("R40,USER_ITEM_1"); 
    StdString strUserItem2 = _T("R40,USER_ITEM_2"); 

    pTmpAccess->AddUserItem(strUserItem1);
    pTmpAccess->AddUserItem(strUserItem2);

    CPartsDef  objCtrl;


    IoDef.Create(&objCtrl, pTmpAccess);


    TEST_CIoDef_SetData(IoDef); 
    
    
    TEST_CIoDefdialog(&IoDef);

    TEST_CIoDef_File(IoDef);



    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));

}



#endif //_DEBUG