/**
 * @brief			CIoDef ヘッダーファイル
 * @file			CIoDef.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

#ifndef  _IO_DEF_H__
#define  _IO_DEF_H__
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "Utility/VirtualTreeCtrl/VirtualCheckList.h"
//#include "Utility/CIdObj.h"
#include "CIoCommon.h"
#include "IIoDef.h"

/*---------------------------------------------------*/
/*  Class                                            */
/*---------------------------------------------------*/
class CIoLine;
class CIoAccess;
class CIoProperty;
class CIoConnect;
class CIoPropertyBlock;
class CIoRef;
class CObjectDef;

/*---------------------------------------------------*/
/*  Enums                                            */
/*---------------------------------------------------*/

/*---------------------------------------------------*/
/*  Defines                                          */
/*---------------------------------------------------*/


/**
 * @class   CIoDef
 * @brief                       
 */
class CIoDef :public CIoDefBase
{
friend CIoDef;
public:

    //!< コンストラクタ
    CIoDef();

    //!< コピーコンストラクタ
    CIoDef(const CIoDef& ioDef);

    //!< デストラクタ
    virtual ~CIoDef();

    //!<----------------------
    //!<  初期化処理
    //!<----------------------
    //!< リスト初期化
    virtual bool InitList(CVirtualCheckList* pVirtualList);

    //!< 初期化
    virtual void Create(CObjectDef* pParent,
                        std::shared_ptr<CIoAccess> pIoAccess);

    //!< ページ設定
    bool SetPage();

    //!< ページ最大数設定
    bool SetMaxPage(int iPage);

    //!<----------------------

    //!<----------------------
    //!<  プロパティ
    //!<----------------------
    //!< IO定義ブロック取得
    virtual CIoPropertyBlock*  GetBlock() const;

    //!< IO定義ブロック設定
     bool SetBlockWeak(std::weak_ptr<CIoPropertyBlock>* pWeak) const;

    //!< 名称取得
    virtual StdString GetName() const;

    //!< オブジェクト定義取得
    //virtual CObjectDef* GetDef() const;

    //!< IDによる接続追加
    virtual bool AddConnectById(int iIdFrom, int iIoIdFrom, int iIoIdTo);

    //!< 描画領域取得
    virtual CPartsDef* GetPartsDef() const;

    //!<----------------------
    //!<  ListCtrl編集
    //!<----------------------
    //!< コンテキストメニューコールバック
    static void __cdecl CallbackContextMenu
                        (LPVOID pParent, CVirtualCheckList* pCheckList,
                        POINT ptClick, int nRow, int nCol);

    //!< コンテキストメニュー表示
    bool  OnContextMenu(CVirtualCheckList* pCheckList,
                        POINT ptClick, int nRow, int nCol);


    //!< コマンドコールバック
    static bool __cdecl CallbackOnCommand (LPVOID pParent,
                                  CVirtualCheckList* pCheckList,
                                  UINT nCode, UINT nID);

    //!< コマンド
    bool  OnCommand(CVirtualCheckList* pCheckList,
                    UINT nCode, UINT nID);

    //!< コピー
    virtual bool Copy(int iRow);

    //!< 貼付け
    virtual bool Paste(int iRow);

    //!< 切り取り
    virtual bool Cut(int iRow);

    //!< 挿入
    virtual bool Insert(int iRow);

    //!< 削除
    virtual bool Delete(int iRow);

    //!<----------------------
    //!<  実行前準備
    //!<----------------------
    //!< アドレス再計算
    virtual bool RecalcAddress(int iPage);

    //!< 更新
    bool Update();

    //!< スクリプト生成
    virtual bool CreateScript(std::vector<StdString>* pScript);

    //!<----------------------
    //!<  実行処理
    //!<----------------------

    //!< 停止設定
    virtual bool Stop();

    //!< データ更新
    bool UpdateData(int iPage);


    //!<----------------------
    //!<  実行完了
    //!<----------------------
    bool Finish();


    //!< 実行データ取得
    //bool IsExec() const;



    //!< 読込後初期化処理
    virtual bool LoadAfter();

    //----------------------

protected:
    friend class CIoConnect;
    friend class CObjectDef;

    //!< 親オブジェクト設定
    void SetParent(CObjectDef* pParent);

    //!< 親オブジェクト取得
    CObjectDef* GetParent();


protected:
    friend class CIoRef;
    //!<----------------
    //!< 保存データ
    //!<----------------

    //!< プロパティ
    std::shared_ptr<CIoPropertyBlock>  m_psPropetyBlock;


    //!<----------------
    //!< 非保存データ
    //!<----------------

    int m_iChgCnt; //変更カウント

    //!< 最大データサイズ(byte)
    int m_iMaxDataSize;

    //!< 親オブジェクト
    CObjectDef* m_pParent;

    //===============================



private:
    friend class boost::serialization::access;  
    BOOST_SERIALIZATION_SPLIT_MEMBER();

    template<class Archive>
    void load(Archive& ar, const unsigned int version);

    template<class Archive>
    void save(Archive& ar, const unsigned int version) const;

};

#endif  //_IO_DEF_H__