/**
 * @brief			CIoProperty実装ーファイル
 * @file			CIoProperty.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CIoProperty.h"
#include "System/CSystem.h"
#include "Utility/CUtility.h"
#include "System/MOCK_ERROR.h"

/*---------------------------------------------------*/
/*  Defines                                          */
/*---------------------------------------------------*/
using namespace IO_COMMON;

/*---------------------------------------------------*/
/* Static variables                                  */
/*---------------------------------------------------*/



/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
CIoProperty::CIoProperty():
    m_bChg              (true),
    m_iId               (0),
    m_iBufferPos        (0),
    m_iOffset           (0),
    m_iRow              (-1),
    m_iPage             (-1),
    m_iLen              (-1),
    m_iUnitId           (-1),
    m_eIoType           (IT_NONE),
    m_eInterpolation    (IP_NONE)
{
    ConvIoType(m_eIoType, &m_iDir, &m_eType, &m_iBit);
}

/**
 *  @brief  コピーコンストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
CIoProperty::CIoProperty(const CIoProperty& prop )
{
    *this = prop;
    ConvIoType(m_eIoType, &m_iDir, &m_eType, &m_iBit);
}

/**
 *  @brief  デストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
CIoProperty::~CIoProperty()
{
    
}

//TODO:削除
/**
 *  @brief  データ設定.
 *  @param  [in] strName IO名
 *  @param  [in] eType   データ種別
 *  @param  [in] iLen    データ長(str以外は機能しない)
 *  @retval なし
 *  @note   
 */
void CIoProperty::SetType(StdString strName, DT_IO_TYPE eType, int iLen)
{
    SetIoType( eType);

    if (m_eType != DAT_STR)
    {
        m_iLen = GetUniValLen(m_eType);
    }
    else
    {
        if (iLen == -1)
        {
            iLen = 1;
        }
        m_iLen = iLen;
    }
    SetName(strName);
}

/**
 *  @brief   代入演算子
 *  @param   [in] obj 代入元 
 *  @retval  代入値
 *  @note 
 */
CIoProperty& CIoProperty::operator = (const CIoProperty & obj)
{
    if((m_iId      != obj.m_iId)     ||
       (m_strName  != obj.m_strName) ||
       (m_eIoType  != obj.m_eIoType))
    {
        SetNeedCompileChange();
    }

    m_iId           = obj.m_iId;        // データID
    m_iBufferPos    = obj.m_iBufferPos;    // アドレス
    m_iOffset       = obj.m_iOffset;    // ビットオフセット
    m_strName       = obj.m_strName;    // データ名
    m_strNote       = obj.m_strNote;    // 説明
    m_iLen          = obj.m_iLen;       // データ長
    m_eIoType       = obj.m_eIoType;    // IO種別
    m_iUnitId       = obj.m_iUnitId;    // 単位
    m_strInit       = obj.m_strInit;    // 初期値
    m_strConv       = obj.m_strConv;    // データ変換
    m_strUser       = obj.m_strUser;    // ユーザーデータ

    //-----------------------------------------
    m_iPage         = obj.m_iPage;      // 頁
    m_iRow          = obj.m_iRow;       // 行

    SetIoType(obj.m_eIoType);
    try
    {
        SetConv(obj.m_strConv);
    }
    catch (MockException& e)
    {
        STD_DBG(_T("%s"), e.GetErrMsg().c_str());
    }


    // E_IO_PROPERTY_COLと内部データを一致させる
    // boost::STATIC_ASSERTION_FAILURE<x>が発生したらチェックすること
    STATIC_ASSERT(10 == IO_COMMON::COL_PROPERTY_MAX);

    return *this;
}

/**
 * @brief   等価演算子
 * @param	[in] obj 比較元 
 * @return  ture 同値
 * @note    テスト用
 */
bool CIoProperty::operator == (const CIoProperty & obj) const
{
    if( m_iId        != obj.m_iId)   { return false;}     // データID
    if( m_iBufferPos != obj.m_iBufferPos){ return false;}    // アドレス
    if( m_iOffset    != obj.m_iOffset){ return false;}    // ビットオフセット
    if( m_strName    != obj.m_strName){ return false;}    // データ名
    if( m_strNote    != obj.m_strNote){ return false;}    // 説明
    if( m_iLen       != obj.m_iLen)   { return false;}    // データ長
    if( m_eIoType    != obj.m_eIoType){ return false;}    // IO種別
    if( m_iUnitId    != obj.m_iUnitId){ return false;}    // 単位
    if( m_strInit    != obj.m_strInit){ return false;}    // 初期値
    if( m_strConv    != obj.m_strConv){ return false;}    // データ変換
    if( m_strUser    != obj.m_strUser){ return false;}    // ユーザーデータ

    if( m_iRow     != obj.m_iRow){ return false;}     // 行
    if( m_iPage    != obj.m_iPage){ return false;}    // 頁

    // E_IO_PROPERTY_COLと内部データを一致させる
    // boost::STATIC_ASSERTION_FAILURE<x>が発生したらチェックすること
    STATIC_ASSERT(10 == IO_COMMON::COL_PROPERTY_MAX);

    return true;

}

/**
 *  @brief  ID設定
 *  @param  [in] iId ID値
 *  @retval false 失敗
 */
bool CIoProperty::SetId(int iId) 
{
    if(m_iId != iId)
    {
        SetNeedCompileChange();
    }

    m_iId = iId;
    return true;
}


/**
 *  @brief  ビットオフセット設定.
 *  @param  [in] iOffset オフセット値
 *  @retval false 失敗
 */
bool CIoProperty::SetOffset(int iOffset)
{
    if (m_eType != DAT_BIT)
    {
        m_iOffset = 0;
        return true;
    }

    if(iOffset < 0)
    {
        iOffset = 0;
    }
    else if(iOffset > 7)
    {
        iOffset = 7;
    }

    m_iOffset = iOffset;
    return true;
}

/**
 *  @brief  IO種別名称取得.
 *  @param  [in] eType データ種別
 *  @retval データ名 
 *  @note   static
 */
StdString CIoProperty::GetTypeName() const
{
    return GetIoTypeName(m_eIoType);
}

/**
 *  @brief  名称設定
 *  @param  [in] strName IO名
 *  @retval なし
 *  @note   命名規則チェック
 *          例外あり
 */
bool CIoProperty::SetName(StdString strName)
{
    using namespace boost::xpressive;
    StdStrXregex satatic_id =  + boost::xpressive::set[ alpha |  _T('_')] >> *_w;
    if(! regex_match( strName, satatic_id))
    {
        MockException(e_no_use_char);
        return false;
    }
    
    if(m_strName != strName)
    {
        SetNeedCompileChange();
    }

    m_strName = strName;
    return true;
}

/**
 *  @brief  種別設定
 *  @param  [in] eType IO種別
 *  @retval なし
 *  @note   
 */
void CIoProperty::SetIoType(DT_IO_TYPE eType)
{
    m_iBit = 0;

    if(m_eType != eType)
    {
        SetNeedCompileChange();
    }
    m_eIoType = eType;

    ConvIoType(eType, &m_iDir, &m_eType, &m_iBit);

    if (m_iBit < 8)
    {
        m_eInterpolation = IP_NONE;
        m_mapRange.clear();
        m_lstCoff.clear();
    }

    if (m_eType != DAT_STR)
    {
        m_iLen = GetUniValLen(m_eType);
    }


}

/**
 *  @brief  データ長設定
 *  @param  [in] iLen データ長
 *  @retval ture 文字型
 *  @note   文字型以外は設定不要
 */
bool   CIoProperty::SetLen(int iLen)
{
    STD_ASSERT(iLen > 0);

    if (m_eType != DAT_STR)
    {
        m_iLen = GetUniValLen(m_eType);
        return false;
    }
    else
    {
        m_iLen = iLen;
        return true;
    }
}

/**
 *  @brief  単位設定
 *  @param  [in] iUnitId 単位ID
 *  @retval true: 成功
 *  @note 
 */
bool CIoProperty::SetUnit(int iUnitId)
{
    CUnit* pUnit;
    pUnit = SYS_UNIT->GetUnit(iUnitId);

    if (pUnit == NULL)
    {
        return false;
    }
    m_iUnitId = iUnitId;
    return true;
}

/**
 *  @brief  単位取得
 *  @param  なし
 *  @retval 単位
 *  @note 
 */
const CUnit&   CIoProperty::GetUnit()  const 
{
    return *SYS_UNIT->GetUnit(m_iUnitId);
}


/**
 *  @brief  初期値設定
 *  @param  [in]  strInit 初期値
 *  @retval true: 成功
 *  @note   
 */
bool CIoProperty::SetInit(StdString strInit)
{
    StdStringStream strmInit;
    switch(m_eType)
    {
    case DAT_BIT:     // ビットデータ
        {
            boost::trim(strInit);
            if (strInit != _T("0"))
            {
                strInit = _T("1");
            }
            m_strInit = strInit;
        }
        break;
    case DAT_CHAR:    // 8bit
    case DAT_SHORT:   // 16bit
    case DAT_LONG:     // 32bit
        {
            unsigned long ulVal = _tcstoul(strInit.c_str(), NULL, 10);
            strmInit << ulVal;
            m_strInit = strmInit.str();
        }
        break;
    case DAT_UCHAR:   // 8bit
    case DAT_USHORT:  // 16bit
    case DAT_ULONG:    // 32bit
        {
            int iVal = _tstoi(strInit.c_str());
            strmInit << iVal;
            m_strInit = strmInit.str();
        }
        break;
    case DAT_FLOAT:   // 浮動小数点値
    case DAT_DOUBLE:  // 浮動小数点値
        {
            double dVal = _tstof(strInit.c_str());
            strmInit << dVal;
            m_strInit = strmInit.str();
        }
        break;

    case DAT_NONE:    // 未設定
    case DAT_STR:     // 文字データ
        m_strInit = strInit;
        break;

    default:
        break;
    }
    return true;
}

/**
 *  @brief  注記設定
 *  @param  [in]   strNote  注記
 *  @retval true: 成功
 *  @note   
 */
bool CIoProperty::SetNote(StdString strNote)
{
    m_strNote = strNote;
    return true;
}


/**
 *  @brief  文字列によるデータ取得
 *  @param  [out]  pStrCol 文字列
 *  @param  [in]   iCol   列位置
 *  @retval true: 成功
 *  @note   
 */
bool CIoProperty::GetStr(StdString* pStrCol, int iCol) const
{
    StdStringStream strmRet;
    bool bRet = true;

    switch(iCol)
    {
    case COL_ID:          //!< ID
       strmRet << m_iId;
       break;

    case COL_ADRESS:     //!< アドレス
       strmRet << StdFormat(_T("%04d:%d")) % m_iBufferPos % m_iOffset;
       break;

    case COL_TYPE:       //!< 種別
       strmRet << GetTypeName();
       break;

    case COL_NAME:       //!< 名称
       strmRet << GetName();
       break;

    case COL_INIT:       //!< 初期値
       strmRet <<  GetInit();
       break;

    case COL_NOTE:       //!< 説明
       strmRet <<  GetNote();
       break;

    case COL_LEN:        //!< データ長
       strmRet << GetLen();
       break;

    case COL_UNIT:       //!< 単位
        if (GetBit() != 0)
        {
            const CUnit* pUint = &GetUnit();
            if (pUint)
            {
                strmRet << pUint->GetUnit();
            }
            else
            {
                strmRet << _T("---");
            }
        }
        else
        {
            strmRet << _T("---");
        }
        break;

    case COL_CONV:      //!< 変換係数
        strmRet << GetConv();
        break;

    default:
        // ユーザーデータ
        {
            int iPos = iCol - COL_USER;
            if ( iPos < (GetUserDataSize()))
            {
                strmRet << GetUserData(iPos);
            }
            else
            {
                bRet = false;
            }
        }
        break;
    }
    *pStrCol = strmRet.str();
    return bRet;
}

/**
 *  @brief  文字列によるデータ設定
 *  @param  [in]   strCol 文字列
 *  @param  [in]   iCol   列位置
 *  @retval true: 成功
 *  @note   
 */
bool CIoProperty::SetStr(const StdString strCol, int iCol)
{
    bool bRet = false;
    switch(iCol)
    {
    case COL_ID:     //!< データID
        bRet = SetId(_tstoi(strCol.c_str()));
        break;

    case COL_ADRESS: //!< アドレス
        break;

    case COL_TYPE:       //!< 種別
        SetIoType(GetIoTypeFromName(strCol));
        bRet = true;
       break;

    case COL_NAME:       //!< 名称
        bRet =  SetName(strCol);
        break;

    case COL_INIT:       //!< 初期値
        bRet =  SetInit(strCol);
        break;

    case COL_NOTE:       //!< 説明
        bRet =  SetNote(strCol);
       break;

    case COL_LEN:        //!< データ長
        {
            int iLen = _tstol(strCol.c_str());
            bRet = SetLen(iLen);
        }
       break;

    case COL_UNIT:       //!< 単位
        {
            CUnit* pUnit = SYS_UNIT->GetUnitByUnit(strCol);
            if (pUnit != NULL)
            {
                SetUnit(pUnit->GetNo());
                bRet = true;
            }
        }
       break;

    case COL_CONV:       //!< 変換係数
        try
        {
            bRet = SetConv(strCol);
        }
        catch (MockException& e)
        {
            e.DispMessageBox();
            bRet = false;
        }
       break;

    default:
        // ユーザーデータ
        {
            int iPos = iCol - COL_USER;
            if ( iPos < (GetUserDataSize() - 1))
            {
                bRet = SetUserData(iPos, strCol);
            }
        }
       break;
    }

    return bRet;
}

/**
 *  @brief  変換係数取得
 *  @param  なし
 *  @retval 変換係数
 *  @note   
 */
StdString   CIoProperty::GetConv()  const
{
    return m_strConv;
}

/**
 *  @brief  変換係数設定
 *  @param  [in]   strConv  変換係数
 *  @retval true: 成功
 *  @note   3種類の補間方式を設定できる
 *           多項式補間
 *           an *x^n + a(n-1) *x^(n-1) ..... a2*x^2 + a1*x + a0 ..の係数は
 *           an, a(n-1) .... a2, a1, a0.と入力する
 * 
 *           最小値、最大値を設定する場合は
 *           min : max と入力する.
 *           
 *           区分線形補間 []
 *           [0:0],[100:20.0],[200:30.0], ....., [入力値:出力値]
 *           (入力値は単調増加である必要があります)
 *           区間は最低２つ以上必要となります
 *           区間外の入力値は、前後の区間を延長して計算します
 *            
 * 失敗時は例外発生
 */
bool CIoProperty::SetConv(StdString strConv)
{
    using namespace boost::xpressive;
    bool bRet;

    if (m_iBit <= 1)
    {
        m_eInterpolation = IP_NONE;
        return false;
    }

    if (strConv == _T(""))
    {
        m_eInterpolation = IP_NONE;
        m_strConv = strConv;
        return true;
    }

   StdStrXMatchResult matchResult;


    std::vector<StdString> lstStr;
    CUtil::TokenizeCsv(&lstStr, strConv);

    if (lstStr.size() == 0)
    {
        m_eInterpolation = IP_NONE;
        throw MockException(e_not_value); 
        return false;
    }
    else if (lstStr.size() == 1)
    {
        //---------------
        // Min:Max
        //---------------
        StdStrXregex satatic_reg = (s1 =  *_) >> _T(":") >>
                                  (s2 =  *_);

        bRet = regex_match( strConv , matchResult, satatic_reg);

        size_t iMatchSize =  matchResult.size();
        if (bRet)
        {
            if (iMatchSize == 3)
            {
                StdString str1, str2;
                str1 = matchResult.str(1);
                str2 = matchResult.str(2);
                
                boost::trim(str1);
                boost::trim(str2);

                if (!CUtil::IsFloatVal(str1))
                {
                    throw MockException(e_not_value); 
                    return false;
                }

                if (!CUtil::IsFloatVal(str2))
                {
                    m_eInterpolation = IP_NONE;
                    throw MockException(e_not_value); 
                    return false;
                }

                if ((m_eType != DAT_UCHAR) &&
                    (m_eType != DAT_USHORT) &&
                    (m_eType != DAT_ULONG))
                {
                    m_eInterpolation = IP_NONE;
                    throw MockException(e_type_err_min_max); 
                    return false;
                }


                double dMin, dMax;

                try{
                dMin = boost::lexical_cast<double>(str1);
                dMax = boost::lexical_cast<double>(str2);
                }catch (boost::bad_lexical_cast& ex) {
                    ex;
                    m_eInterpolation = IP_NONE;
                    throw MockException(e_not_value); 
                    return false;
                }


                if (fabs(dMin - dMax) < NEAR_ZERO)
                {
                    m_eInterpolation = IP_NONE;
                    throw MockException(e_not_eql_min_max); 
                    return false;
                }


                m_lstCoff.clear();

                
                double dRange = pow(2.0, m_iBit) - 1;

                double dA = (dMax - dMin) / dRange;

                m_lstCoff.push_back(dMin);
                m_lstCoff.push_back(dA);

                m_eInterpolation = IP_POLY;
                m_strConv = strConv;
                return true;
            }
        }

        m_eInterpolation = IP_NONE;
        throw MockException(e_io_conv_inp); 
        return false;
    }


    StdStrXregex val_reg = _T('[') >>
                            (s1 =  +_ )  >> _T(':') >>
                            (s2 =  +_ ) >>
                           _T(']') ;

    bool bRange = true;
    int iCnt = 0;
    double dValIn;
    double dValOut;

    std::deque<double>     lstCoff;
    std::map<double, double> mapCoff;
    std::map<double, boost::tuple<double, double> > mapRange;

    foreach(StdString& strVal, lstStr)
    {
        if (bRange)
        {
            //---------------
            // [val:val]
            //---------------
           boost::trim(strVal); 
           bRet = regex_match( strVal , matchResult, val_reg);
            if (!bRet) 
            {
                if (iCnt == 0)
                {
                    bRange = false;
                }
                else
                {
                    throw MockException(e_io_conv_format1); 
                    return false;
                }
            }
            else
            {
                StdString str1, str2;
                str1 = matchResult.str(1);
                str2 = matchResult.str(2);
                
                boost::trim(str1);
                boost::trim(str2);

                try{
                dValIn  = boost::lexical_cast<double>(str1);
                dValOut = boost::lexical_cast<double>(str2);
                }catch (boost::bad_lexical_cast& ex) {
                    ex;
                    throw MockException(e_io_conv_format1); 
                    return false;
                }
                mapCoff[dValIn] = dValOut;
            }
        }

        if (!bRange)
        {
            boost::trim(strVal);
            if (!CUtil::IsFloatVal(strVal))
            {
                throw MockException(e_io_conv_format2); 
                return false;
            }
            lstCoff.push_front(boost::lexical_cast<double>(strVal));
        }
        iCnt++;
    }

    if (bRange)
    {
        double dValInOld;
        double dValOutOld;
        double dA;
        double dB;


        int iPos = -1;

        if (mapCoff.size() < 2)
        {
            m_eInterpolation = IP_NONE;
            throw MockException(e_io_conv_inp); 
            return false;
        }

        foreach(boost::tie(dValIn, dValOut), mapCoff)
        {
            iPos++;
            if (iPos == 0)
            {
                dValInOld  = dValIn;
                dValOutOld = dValOut;
                continue;
            }

            if (fabs(dValIn - dValInOld) < NEAR_ZERO)
            {
                throw MockException(e_io_conv_inc); 
                return false;
            }


            dA = (dValOut - dValOutOld) / (dValIn - dValInOld);
            dB = dValOut - dA * dValIn;

            dValInOld  = dValIn;
            dValOutOld = dValOut;

            mapRange[dValIn] = boost::make_tuple(dA, dB);
        }
        m_eInterpolation = IP_PICE;

        m_mapRange.clear();
        m_mapRange = mapRange;
    }
    else
    {
        m_eInterpolation = IP_POLY;
        m_lstCoff = lstCoff;
    }
    m_strConv = strConv;
    return true;
}

/**
 *  @brief  最大直値取得
 *  @param  なし
 *  @retval 最大直値
 *  @note   
 */
double CIoProperty::GetImmediateMax()  const
{
    double dRet = 0.0;
    switch(m_eType)
    {
    case DAT_NONE:      dRet = 0.0;break;
    case DAT_BIT:       dRet = 1.0;break;
    case DAT_CHAR:      dRet = SCHAR_MAX;break;
    case DAT_UCHAR:     dRet = UCHAR_MAX;break;
    case DAT_USHORT:
        { 
            if (m_iLen == BL_10)
            {
                dRet = 1023;
            }
            else if (m_iLen == BL_12)
            {
                dRet = 4095;
            }
            else if (m_iLen == BL_14)
            {
                dRet = 16383;

            }
            else if (m_iLen == BL_16)
            {
                dRet = 65535;
            }
            else
            {
                STD_DBG(_T("DAT_USHORT %d"), m_iLen);
            }
            break;
        }

    case DAT_SHORT:    dRet = SHRT_MAX;break;
    case DAT_ULONG:
        {
            if (m_iLen == BL_18)
            {
                dRet = 262143;
            }
            else if (m_iLen == BL_20)
            {
                dRet = 1048575;
            }
            else if (m_iLen == BL_24)
            {
                dRet = 16777215;
            }
            else if (m_iLen == BL_32)
            {
                dRet = 4294967295;
            }
            else
            {
                STD_DBG(_T("DAT_ULONG %d"), m_iLen);
            }
            break;
        }
    case DAT_LONG:      dRet = LONG_MAX;break;
    case DAT_FLOAT:     dRet = FLT_MAX; break;
    case DAT_DOUBLE:    dRet = DBL_MAX; break;
    case DAT_STR:       dRet = 0.0;break;
    }
    return dRet;
}

/**
 *  @brief  最小直値取得
 *  @param  なし
 *  @retval 最小直値
 *  @note   
 */
double CIoProperty::GetImmediateMin()  const
{
    double dRet = 0.0;
    switch(m_eType)
    {
    case DAT_NONE:      dRet = 0.0;break;
    case DAT_BIT:       dRet = 1.0;break;
    case DAT_CHAR:      dRet = SCHAR_MIN;break;
    case DAT_UCHAR:     dRet = 0.0;break;
    case DAT_USHORT:    dRet = 0.0;break;
    case DAT_SHORT:     dRet = SHRT_MIN;break;
    case DAT_ULONG:     dRet = 0.0;break;
    case DAT_LONG:      dRet = LONG_MIN;break;
    case DAT_FLOAT:     dRet = -FLT_MAX; break;
    case DAT_DOUBLE:    dRet = -DBL_MAX; break;
    case DAT_STR:       dRet = 0.0;break;
    }
    return dRet;
}

/**
 *  @brief  データ変換
 *  @param  [in]  dX  変換元
 *  @retval 変換結果
 *  @note   
 */
double CIoProperty::GetConvVal(double dX)  const
{
    if (m_eInterpolation == IP_NONE)
    {
        //補間なし
        return dX;
    }
    else if (m_eInterpolation ==IP_POLY)
    {
        //多項式補間
        double dRet = 0.0;
        double dPowX = 1.0;
        std::deque<double>::const_iterator itCoff; 
        for (itCoff =m_lstCoff.begin(); 
             itCoff!=m_lstCoff.end(); 
             itCoff++)
        {
            dRet += *itCoff * dPowX;
            dPowX = dPowX * dX;
        }
        return dRet;
    }
    else 
    {
        //区分線形補間
        double dRet, dA, dB;
        std::map<double, boost::tuple<double, double> >::const_iterator iteRange;
        iteRange = m_mapRange.lower_bound(dX);

        if (iteRange == m_mapRange.end())
        {
            iteRange--;
        }
        boost::tie(dA, dB) = iteRange->second;

        dRet = dX * dA + dB;
        return dRet;
    }
}

/**
 *  @brief  データ逆変換
 *  @param  [in]  dX  変換元
 *  @retval 変換結果
 *  @note   現状では、２次式以上の逆変換は行わない
 */
bool CIoProperty::GetInvVal(double* pY, double dX)  const
{
    if (m_eInterpolation == IP_NONE)
    {
        //補間なし
        *pY = dX;
        return true;
    }
    else if (m_eInterpolation ==IP_POLY)
    {
        //多項式補間
        if (m_lstCoff.size() == 2)
        {
            if (fabs(m_lstCoff[1]) > NEAR_ZERO)
            {
                *pY = (dX - m_lstCoff[0]) / m_lstCoff[1];
                return true;
            }
        }
        else
        {
            //TODO:多項式の対応
        }
        return false;
    }
    else 
    {
        using namespace boost;
        //区分線形補間
        double dRet = 0.0;

        bool   bFind;

        std::map<double, tuple<double, double> >::const_iterator iteRange;

        double dA;
        double dB;

        size_t iCnt = 0;
        for(iteRange  = m_mapRange.begin();
            iteRange != m_mapRange.end();
            iteRange++)
        {

            double dXMax;
            iCnt++;
            if( iCnt == m_mapRange.size())
            {
                dXMax = DBL_MAX;
            }
            else
            {
                dXMax = iteRange->first;
            }
            double dXMin = -DBL_MAX;

            tie(dA, dB) = iteRange->second;

            if ( fabs(dA) > NEAR_ZERO)
            {
                dRet = (dX - dB) / dA ;
            }

            bFind = true;

            if (dRet > dXMax)
            {
                bFind = false;
            }

            if (dRet < dXMin)
            {
                bFind = false;
            }


            dXMin = dXMax;

            if (bFind)
            {
                *pY = dRet;
                return true;
            }
        }
        return false;
    }
}


/**
 * @brief   ユーザーデータサイズ設定
 * @param   [in] ユーザーデータサイズ
 * @return  true:成功
 * @note    
 */
bool CIoProperty::SetUserDataSize(int iSize)
{
    if (iSize < 0)
    {
        return false;
    }
    m_strUser.resize(iSize);
    return true;
}

/**
 * @brief   ユーザーデータ設定
 * @param   [in] iPos        データ位置
 * @param   [in] strUserData データ
 * @return  ユーザーデータ取得
 * @note    
 */
bool CIoProperty::SetUserData(int iPos, StdString strUserData)
{
    if (iPos < 0){return false;}
    if (iPos >= GetUserDataSize()){return false;}

    m_strUser[iPos] = strUserData;
    return true;
}

/**
 * @brief   ユーザーデータ取得
 * @param   [in] iPos データ位置
 * @return  ユーザーデータ取得
 * @note    
 */
StdString CIoProperty::GetUserData(int iPos) const
{
    StdString strRet;
    if (iPos < 0){return strRet;}
    if (iPos >= GetUserDataSize()){return strRet;}

    return m_strUser[iPos];
}

/**
 *  @brief  データ設定
 *  @param  [in] strName    データ名
 *  @param  [in] eType 
 *  @param  [in] iOffet 
 *  @param  [in] iUnitId 
 *  @param  [in] iLen 
 *  @param  [in] strNote 
 *  @param  [in] strInit 
 *  @param  [in] strConv 
 *  @retval true 成功
 *  @note   主にテスト用
 */
bool CIoProperty::SetData(
            StdString             strName, 
            IO_COMMON::DT_IO_TYPE eType, 
            int iOffet,
            int iUnitId,
            int iLen,
            StdString strNote,
            StdString strInit,
            StdString strConv
            )
{
    bool bRet = true;

    bRet = SetName(strName);
    if (!bRet){return false;}

    SetIoType(eType);

    bRet = SetOffset(iOffet);
    if (!bRet){return false;}

    bRet = SetUnit(iUnitId);
    if (!bRet){return false;}

    if ((IO_COMMON::DAT_MASK & eType) == DAT_STR)
    {
        bRet = SetLen(iLen);
    }
    if (!bRet){return false;}

    bRet = SetNote(strNote);
    if (!bRet){return false;}

    if (strInit != _T(""))
    {
        bRet = SetInit(strInit);
        if (!bRet){return false;}
    }

    if (strConv != _T(""))
    {
        try
        {
            bRet = SetConv(strConv);
            if (!bRet){return false;}
        }
        catch (MockException& e)
        {
            STD_DBG(_T("%s"), e.GetErrMsg().c_str());
            bRet = false;
        }
    }
    return bRet;
}


/**
 * @brief   データ表示
 * @param   なし
 * @return  なし
 * @note    
 */
void CIoProperty::Print() const
{
    StdString strOut;
    StdStringStream strmOut;
    DB_PRINT(_T("------------------------\n"));
    for(int iCnt = COL_ID;
        iCnt <=    COL_CONV;
        iCnt++)
    {
        CUtil::FormatedText(GetPropertyColName(static_cast<E_IO_PROPERTY_COL>(iCnt)), NULL, NULL, &strOut);
        boost::trim(strOut);
        strmOut <<  strOut;
        GetStr(&strOut, iCnt);
        strmOut << _T("\t\t:") << strOut << std::endl;
        DB_PRINT(strmOut.str().c_str());
        strmOut.str(_T(""));
    }
    DB_PRINT(_T("------------------------\n"));
}

/**
 * @brief   接続可否問い合わせ
 * @param   [in] pProp 接続するIOのプロパティ
 * @return  true:接続可
 * @note    
 */
bool CIoProperty::IsConnectable(CIoProperty* pProp) const
{
    if (!pProp)                   {return false;}

    //TODO:入力->入力の接続も可能としたい
    if (m_iDir  == pProp->m_iDir) {return false;}

    if (m_eType != pProp->m_eType)
    {
        //AIO同士は接続可能
        if ((m_eType != DAT_UCHAR)  &&
            (m_eType != DAT_USHORT) &&
            (m_eType != DAT_ULONG))
        {
            return false;
        }

        if ((pProp->m_eType != DAT_UCHAR)  &&
            (pProp->m_eType != DAT_USHORT) &&
            (pProp->m_eType != DAT_ULONG))
        {
            return false;
        }
    }

    if (m_iDir  != IO_SIO)
    {
        // 0 -> I の接続のみ
        // 接続は Inputのみ持てる
        if (m_iDir         == IO_OUT) {return false;}
        if (pProp->m_iDir  == IO_IN)  {return false;}
    }

    return true;
}

/**
 * @brief   補間形式問い合わせ
 * @param   なし
 * @return  補間形式
 * @note    
 */
IO_COMMON::E_INTERPOLATION CIoProperty::GetInterpolation() const
{
    return m_eInterpolation;
}

/**
 * @brief   再コンパイルが必要な変更の有無
 * @param   なし
 * @return  true 要再コンパイル
 * @note    
 */
bool CIoProperty::IsNeedCompileChange()
{
    return m_bChg;
}

/**
 * @brief   再コンパイルが必要な変更の設定
 * @param   [in] bChg (true:要再コンパイル)
 * @return  なし
 * @note    
 */
void CIoProperty::SetNeedCompileChange(bool bChg)
{
    m_bChg = bChg;
}

/**
 * @brief   シリアル化(ロード)
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  なし
 * @note	
 */
template<class Archive>
void CIoProperty::load(Archive& ar, const unsigned int version)
{
    SERIALIZATION_INIT;

        SERIALIZATION_LOAD("ID"        , m_iId);
        SERIALIZATION_LOAD("Address"   , m_iBufferPos);
        SERIALIZATION_LOAD("Offset"    , m_iOffset);
        SERIALIZATION_LOAD("Name"      , m_strName);
        SERIALIZATION_LOAD("Note"      , m_strNote);
        SERIALIZATION_LOAD("Length"    , m_iLen);
        SERIALIZATION_LOAD("IoType"    , m_eIoType);
        SERIALIZATION_LOAD("Unit"      , m_iUnitId);
        SERIALIZATION_LOAD("Init"      , m_strInit);
        SERIALIZATION_LOAD("Conv"      , m_strConv);
        SERIALIZATION_LOAD("UserData"  , m_strUser);

        SetIoType(m_eIoType);
        try
        {
            SetConv(m_strConv);
        }
        catch (MockException& e)
        {
            STD_DBG(_T("%s"), e.GetErrMsg().c_str());
        }

    MOCK_EXCEPTION_FILE(e_file_read);
}

/**
 * @brief   シリアル化(セーブ)
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  なし
 * @note	
 */
template<class Archive>
void CIoProperty::save(Archive& ar, const unsigned int version) const
{
    SERIALIZATION_INIT;

        SERIALIZATION_SAVE("ID"        , m_iId);
        SERIALIZATION_SAVE("Address"   , m_iBufferPos);
        SERIALIZATION_SAVE("Offset"    , m_iOffset);
        SERIALIZATION_SAVE("Name"      , m_strName);
        SERIALIZATION_SAVE("Note"      , m_strNote);
        SERIALIZATION_SAVE("Length"    , m_iLen);
        SERIALIZATION_SAVE("IoType"    , m_eIoType);
        SERIALIZATION_SAVE("Unit"      , m_iUnitId);
        SERIALIZATION_SAVE("Init"      , m_strInit);
        SERIALIZATION_SAVE("Conv"      , m_strConv);
        SERIALIZATION_SAVE("UserData"  , m_strUser);

    MOCK_EXCEPTION_FILE(e_file_write);
}


//load save のインスタンスが生成されないため
//ダミーとして実装
void CIoProperty::Dummy()
{
    unsigned int iVersion = 0;

    StdStreamOut outFs(_T(""));
    StdStreamIn  inFs(_T(""));

    boost::archive::text_woarchive txtOut(outFs); 
    boost::archive::text_wiarchive txtIn(inFs);

    save<boost::archive::text_woarchive>( txtOut, iVersion);
    load<boost::archive::text_wiarchive>( txtIn,  iVersion);


    StdXmlArchiveOut outXml(outFs);
    StdXmlArchiveIn inXml(inFs);

    save<StdXmlArchiveOut>(outXml, iVersion);
    load<StdXmlArchiveIn>(inXml, iVersion);

    boost::filesystem::ofstream outFsStd(_T(""));
    boost::filesystem::ifstream inFsStd(_T(""));

    boost::archive::binary_oarchive outBin(outFsStd);
    boost::archive::binary_iarchive inBin(inFsStd);

    save<boost::archive::binary_oarchive>(outBin, iVersion);
    load<boost::archive::binary_iarchive>(inBin, iVersion);


}
//*******************************
//             TEST
//*******************************
#ifdef _DEBUG


void TEST_SetType(CIoProperty* pProp)
{
    pProp->Print();


}

void TEST_Regex()
{
    using namespace boost::xpressive;
    bool bRet;
    StdString strVal;

    StdStrXregex val_reg = _T('[') >>
                            (s1 =  +_ )  >> _T(':') >>
                            (s2 =  +_ ) >>
                           _T(']') ;
    StdStrXMatchResult matchResult;
    StdString ss1, ss2;

    strVal = _T("[0:0]");
    bRet = regex_match( strVal , matchResult, val_reg);
    if (bRet)
    {
        ss1 = matchResult.str(1);
        ss2 = matchResult.str(2);
    }

   strVal = _T("[10:0]");
   bRet = regex_match( strVal , matchResult, val_reg);
    if (bRet)
    {
        ss1 = matchResult.str(1);
        ss2 = matchResult.str(2);
    }

   strVal = _T("[ 10: 20.1 ]");
   bRet = regex_match( strVal , matchResult, val_reg);
    if (bRet)
    {
        ss1 = matchResult.str(1);
        ss2 = matchResult.str(2);
    }

}


void TEST_SetConv()
{
    CIoProperty  propIo;
    propIo.SetIoType(IT_AI_10);
    
    //最大-最小値
    STD_ASSERT(propIo.SetConv(_T("20.0  :  300.0")));
    STD_ASSERT(propIo.m_eInterpolation == IO_COMMON::IP_POLY);
    STD_ASSERT(propIo.m_lstCoff.size() == 2);
    STD_ASSERT_DBL_EQ(20.0,   propIo.m_lstCoff[0]);
    double dMaxVal = propIo.m_lstCoff[1] * 1024.0 + propIo.m_lstCoff[0];
    STD_ASSERT_DBL_EQ(300.0,  dMaxVal);


    //係数設定
    STD_ASSERT(propIo.SetConv(_T("2, 7.0, 4, 5")));
    STD_ASSERT(propIo.m_eInterpolation == IO_COMMON::IP_POLY);
    STD_ASSERT(propIo.m_lstCoff.size() == 4);
    STD_ASSERT_DBL_EQ(2  ,  propIo.m_lstCoff[3]);
    STD_ASSERT_DBL_EQ(7.0,  propIo.m_lstCoff[2]);
    STD_ASSERT_DBL_EQ(4  ,  propIo.m_lstCoff[1]);
    STD_ASSERT_DBL_EQ(5  ,  propIo.m_lstCoff[0]);

    STD_ASSERT_DBL_EQ( 2745  ,  propIo.GetConvVal(10.0));
    STD_ASSERT_DBL_EQ(18885  ,  propIo.GetConvVal(20.0));
    STD_ASSERT_DBL_EQ(60425  ,  propIo.GetConvVal(30.0));
    

    //区分線形補間
    STD_ASSERT(propIo.SetConv(_T("[0:2], [10:5], [30: 10.0], [200:55]")));
    STD_ASSERT(propIo.m_eInterpolation == IO_COMMON::IP_PICE);
    STD_ASSERT(propIo.m_mapRange.size() == 3);

    double dVal, dA, dB;
    std::map<double, boost::tuple<double, double> >::iterator iteRange;

    iteRange = propIo.m_mapRange.lower_bound(10);
    dVal = iteRange->first;
    boost::tie(dA, dB) = iteRange->second;
    STD_ASSERT_DBL_EQ(dVal  ,  10);
    STD_ASSERT_DBL_EQ(dA    ,  0.3);
    STD_ASSERT_DBL_EQ(dB    ,  2.0);

    STD_ASSERT_DBL_EQ(   -1  ,  propIo.GetConvVal( -10));
    STD_ASSERT_DBL_EQ(  4.4  ,  propIo.GetConvVal(   8));
    STD_ASSERT_DBL_EQ( 6.25  ,  propIo.GetConvVal(  15));
    STD_ASSERT_DBL_EQ(13.9705882352941000 ,  propIo.GetConvVal(  45));
    STD_ASSERT_DBL_EQ(81.4705882352941000 ,  propIo.GetConvVal( 300));

    iteRange = propIo.m_mapRange.lower_bound(30);
    dVal = iteRange->first;
    boost::tie(dA, dB) = iteRange->second;
    STD_ASSERT_DBL_EQ(dVal  ,  30);
    STD_ASSERT_DBL_EQ(dA    ,  0.25);
    STD_ASSERT_DBL_EQ(dB    ,  2.5);

    iteRange = propIo.m_mapRange.lower_bound(200);
    dVal = iteRange->first;
    boost::tie(dA, dB) = iteRange->second;
    STD_ASSERT_DBL_EQ(dVal  ,  200);
    STD_ASSERT_DBL_EQ(dA    ,  0.264705882352941);
    STD_ASSERT_DBL_EQ(dB    ,  2.05882352941176);


    iteRange = propIo.m_mapRange.lower_bound(0);
    dVal = iteRange->first;
    STD_ASSERT_DBL_EQ(dVal  ,  10);

    iteRange = propIo.m_mapRange.lower_bound(300);
    STD_ASSERT(iteRange == propIo.m_mapRange.end());


    //---------------------------------------------
    // 異常系
    //---------------------------------------------
#if 0
    try{
        //同値
        STD_ASSERT(propIo.SetConv(_T("20.0  :  20.0")));
        STD_DBG(_T("ここは通らない"));
    }
    catch (MockException& e)
    {
        STD_ASSERT(e.GetErr() == e_not_eql_min_max);
    }

    try{
        //非数値
        STD_ASSERT(propIo.SetConv(_T("2e.0  :  20.0")));
        STD_DBG(_T("ここは通らない"));
    }
    catch (MockException& e)
    {
        STD_ASSERT(e.GetErr() == e_not_value);
    }

    try{
        //非数値
        STD_ASSERT(propIo.SetConv(_T("10.0  :  20.cc")));
        STD_DBG(_T("ここは通らない"));
    }
    catch (MockException& e)
    {
        STD_ASSERT(e.GetErr() == e_not_value);
    }

    try{
        //非数値
        STD_ASSERT(propIo.SetConv(_T("  10.0  ")));
        STD_DBG(_T("ここは通らない"));
    }
    catch (MockException& e)
    {
        STD_ASSERT(e.GetErr() == e_io_conv_inp);
    }

    try{
        STD_ASSERT(propIo.SetConv(_T("  [10:20], 10.0  ")));
        STD_DBG(_T("ここは通らない"));
    }
    catch (MockException& e)
    {
        STD_ASSERT(e.GetErr() == e_io_conv_format1);
    }

    try{
        STD_ASSERT(propIo.SetConv(_T("  [10:20], [10: c20]  ")));
        STD_DBG(_T("ここは通らない"));
    }
    catch (MockException& e)
    {
        STD_ASSERT(e.GetErr() == e_io_conv_format1);
    }


    try{
        STD_ASSERT(propIo.SetConv(_T("  10,20,AAA  ")));
        STD_DBG(_T("ここは通らない"));
    }
    catch (MockException& e)
    {
        STD_ASSERT(e.GetErr() == e_io_conv_format2);
    }
#endif 

}

void TEST_CIoProperty_IsConnactaable()
{


}



void TEST_CIoProperty()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    CIoProperty  propIo;

    TEST_SetType(&propIo);

    TEST_Regex();
    TEST_SetConv();

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));

}
#endif //_DEBUG