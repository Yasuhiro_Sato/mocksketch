/**
 * @brief			CIoPropertyヘッダーファイル
 * @file			CIoProperty.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef _IO_PROPERTY_H_
#define _IO_PROPERTY_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "System/CUnit.h"
#include "CIoCommon.h"

/*---------------------------------------------------*/
/*  Enum                                             */
/*---------------------------------------------------*/

/*---------------------------------------------------*/
/*  Defines                                          */
/*---------------------------------------------------*/
//using namespace IO_COMMON;


/**
 * @class   CIoProperty
 * @brief                       
 */
class CIoProperty
{
    friend void TEST_SetConv();

public:

    //!< コンストラクター
    CIoProperty();

    //!< コピーコンストラクター
    CIoProperty(const CIoProperty& prop );

    //!< デストラクター
    virtual ~CIoProperty();

    //TODO:削除
    //!< データ設定
    void SetType(StdString strName, IO_COMMON::DT_IO_TYPE eType, int iLen = -1);

    //!< 代入演算子
    CIoProperty& operator = (const CIoProperty & obj);

    //!< 等価演算子
    bool  operator == (const CIoProperty & obj) const;


    //!< ID設定
    bool SetId(int iId);

    //!< ID取得
    int GetId() {return m_iId;}
    
    //!< バッファ位置
    bool SetBufferPos(int iBufferPos) {m_iBufferPos = iBufferPos; return true;}

    //!< バッファ位置
    int GetBufferPos() const {return m_iBufferPos;}

    //!< ビットオフセット設定
    bool SetOffset(int iOffset);

    //!< ビットオフセット取得
    int GetOffset() const {return m_iOffset;}

    //!< IO種別設定
    void SetIoType(IO_COMMON::DT_IO_TYPE eType);

    //!< IO種別取得
    IO_COMMON::DT_IO_TYPE   GetIoType() const {return m_eIoType;}

    //!< 種別名称取得
    StdString GetTypeName() const;

    //!< 名称取得
    StdString GetName() const {return m_strName;}

    //!< 名称設定
    bool SetName(StdString strName);

    //!< 注記取得
    StdString GetNote() const {return m_strNote;}

    //!< 注記設定
    bool SetNote(StdString strNote);

    //!< データ種別取得
    IO_COMMON::DAT_TYPE   GetType() {return m_eType;}

    //!< データ長取得
    int   GetLen() const  {return m_iLen;}

    //!< データ長設定
    bool   SetLen(int iLen);

    //!< データ方向取得
    IO_COMMON::DT_IO_DIR GetDir() const { return m_iDir;}

    //!< ビット長取得
    int   GetBit()  const {return m_iBit;}
    
    //!< ビット長設定
    void   SetBit(int iBit)  {m_iBit = iBit;}

    //!< 単位取得
    const CUnit&   GetUnit()  const;
    
    //!< 単位番号取得
    int   GetUnitId()  const    {return m_iUnitId;}

    //!< 単位設定
    bool   SetUnit(int iUnitId);

    //!< 初期値取得
    StdString   GetInit()  const {return m_strInit;}
    
    //!< 初期値設定
    bool   SetInit(StdString strInit);

    //!< 変換係数取得
    StdString   GetConv()  const;
    
    //!<  変換係数設定
    bool   SetConv(StdString strConv);

    //!< データ変換
    double GetConvVal(double dX)  const;

    //!< データ逆変換
    bool GetInvVal(double* pY, double dX)  const;

    //!< ユーザーデータサイズ設定
    bool SetUserDataSize(int iSize);

    //!< ユーザーデータサイズ取得
    int GetUserDataSize() const{ return static_cast<int>(m_strUser.size());};

    //!< ユーザーデータ設定
    bool SetUserData(int iPos, StdString strUserData);

    //!< ユーザーデータ取得
    StdString GetUserData(int iPos) const;

    //!< データ設定
    bool SetData(StdString strName, 
                IO_COMMON::DT_IO_TYPE eType, 
                int iOffet,
                int iUnitId,
                int iLen,
                StdString strNote,
                StdString strInit,
                StdString strConv );

    //!< 接続可否問い合わせ
    bool IsConnectable(CIoProperty* pProp) const;

    //!< 補間形式問い合わせ
    IO_COMMON::E_INTERPOLATION GetInterpolation() const;

    //!< 最大直値取得
    double GetImmediateMax()  const;

    //!< 最小直値取得
    double GetImmediateMin()  const;

    //!< 行番号設定
    void SetRow(int iRow){m_iRow = iRow;}

    //!< 行番号取得
    int  GetRow() const{return m_iRow;}

    //!< 頁行番号設定
    void SetPage(int iPage){m_iPage = iPage;}

    //!< 頁番号取得
    int  GetPage () const{return m_iPage;}

    //!< 文字列によるデータ取得
    bool GetStr(StdString* pStrCol, int iCol)  const;

    //!< 文字列によるデータ設定
    bool SetStr(const StdString strCol, int iCol);

    //!< データ表示
    void Print() const;

    //!< 再コンパイルが必要な変更の有無
    bool IsNeedCompileChange();

    //!< 再コンパイルが必要な変更の設定
    void SetNeedCompileChange(bool bChg = true);

protected:
    //!< ダミー
    void Dummy();

protected:
    //---------------------
    // 保存データ
    //---------------------
    // E_IO_PROPERTY_COLと一致させる
    int             m_iId;          // データID

    int             m_iBufferPos;   // バッファ位置
    int             m_iOffset;      // ビットオフセット
    StdString       m_strName;      // データ名
    StdString       m_strNote;      // 説明
    int             m_iLen;         // データ長
    IO_COMMON::DT_IO_TYPE      m_eIoType;      // IO種別
    int             m_iUnitId;      // 単位番号
    StdString       m_strInit;      // 初期値
    StdString       m_strConv;      // データ変換
    std::vector<StdString> m_strUser;   //ユーザーデータ


    //---------------------
    // 非保存データ
    //---------------------
    int                     m_iBit;         // ビット長
    IO_COMMON::DT_IO_DIR    m_iDir;         // データ方向
    IO_COMMON::DAT_TYPE     m_eType;        // データ型
    std::deque<double>     m_lstCoff;       // 変換係数
    std::map<double, boost::tuple<double, double> >     m_mapRange;      // 変換係数
    IO_COMMON::E_INTERPOLATION         m_eInterpolation;       //補間方式

    //----------------------------------
    int                     m_iPage;        // ページ
    int                     m_iRow;         // 行番号

    bool                    m_bChg;

private:
    friend class boost::serialization::access;  
    BOOST_SERIALIZATION_SPLIT_MEMBER();

    template<class Archive>
    void load(Archive& ar, const unsigned int version);

    template<class Archive>
    void save(Archive& ar, const unsigned int version) const;


};

#endif // _IO_PROPERTY_H_
