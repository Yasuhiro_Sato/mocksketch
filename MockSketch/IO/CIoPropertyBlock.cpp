/**
 * @brief			CIoPropertyBlock実装ファイル
 * @file			CIoPropertyBlock.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CIoPropertyBlock.h"
#include "./CIoProperty.h"
#include "Utility/CUtility.h"
#include "System/CSystem.h"
#include "System/MOCK_ERROR.h"



/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CIoPropertyBlock::CIoPropertyBlock():
m_iUserDataNum  (0),
m_iChgCnt       (0),
m_bNeedUpdate   (false)
{

}

/**
 *  @brief  コピーコンストラクター.
 *  @param  [in] ioPropBlock
 *  @retval なし     
 *  @note
 */
CIoPropertyBlock::CIoPropertyBlock(const CIoPropertyBlock & ioPropBlock):
m_iChgCnt       (0),
m_bNeedUpdate   (false)
{
    m_iUserDataNum = ioPropBlock.m_iUserDataNum;
    m_IdObj        = ioPropBlock.m_IdObj;
    m_lstId        = ioPropBlock.m_lstId;


    std::map<int, std::shared_ptr<CIoProperty> >:: iterator iteProp;

    int iIoId;

    for (iteProp  = m_mapProperty.begin();
         iteProp != m_mapProperty.end();
         iteProp++)
    {
        iIoId = iteProp->first;

        m_mapProperty[iIoId] = 
            std::make_shared<CIoProperty>(*iteProp->second.get());
    }
    
    m_mapNameId = ioPropBlock.m_mapNameId;
}


/**
 *  @brief  デストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CIoPropertyBlock::~CIoPropertyBlock()
{
}


/**
 *  @brief  ユーザデータ数設定
 *  @param  [in] iNum ユーザデータ数
 *  @retval false 失敗
 *  @note
 */
bool CIoPropertyBlock::SetUserDataNum(int iNum)
{
    if (iNum < 0) {return false;}
    m_iUserDataNum = iNum;

    return true;
}


/**
 *  @brief  ユーザデータ数設定
 *  @param  なし
 *  @retval ユーザデータ数
 *  @note
 */
int CIoPropertyBlock::GetUserDataNum() const
{
    return m_iUserDataNum;
}


/**
 *  @brief  ページ数設定
 *  @param  
 *  @retval なし     
 *  @note
 */
bool CIoPropertyBlock::SetPageMax(int iPageMax)
{
    if (iPageMax < 0)
    {
        return false;
    }
    m_lstId.resize(iPageMax);
    m_iChgCnt++;
    m_bNeedUpdate = true;
    return true;
}

/**
 *  @brief  ページ数取得
 *  @param  なし
 *  @retval ページ数
 *  @note
 */
int CIoPropertyBlock::GetPageMax() const
{
    return static_cast<int>(m_lstId.size());
}

/**
 *  @brief  ID取得
 *  @param  [in] iPage ページ
 *  @param  [in] iRow  行
 *  @retval IoID
 *  @note   -1の時は失敗
 */
int CIoPropertyBlock::GetIoId(int iPage, int iRow) const
{
    if(iPage < 0){return -1;}
    if(iRow  < 0){return -1;}

    int iRowMax =  GetRowMax(iPage);
    if (iRowMax < 0)    
    {
        return -1;
    }

    if (iRow >= iRowMax)
    {
        return -1;
    }

    return m_lstId[iPage].at(iRow);
}

/**
 *  @brief  ページ、行取得
 *  @param  [out] pPage ページ
 *  @param  [out] iRow  行
 *  @param  [in]  iIoId 
 *  @retval true:成功
 *  @note
 */
bool CIoPropertyBlock::GetPageRow(int* pPage, int* pRow, int iIoId) const
{
    //TODO:要最適化

    /*
    int iPage = 0;
    foreach (const std::vector< int>& lstIo, m_lstId)
    {
        int iRow = 0;
        foreach (int iSearch, lstIo)
        {
            if(iSearch == iIoId)
            {
                *pPage = iPage;
                *pRow  = iRow;
                return true;
            }
            iRow++;
        }

        iPage++;
    }
    */
    return false;
}


/**
 *  @brief  ID取得
 *  @param  [in] strName IO名
 *  @retval IoID
 *  @note   -1の時は失敗
 */
int CIoPropertyBlock::GetIoIdName(const StdString& strName) const
{
    std::map< StdString, int >::const_iterator iteMap;
    
    iteMap = m_mapNameId.find(strName);

    if (iteMap == m_mapNameId.end())
    {
        return -1;
    }

    return iteMap->second;
}


/**
 *  @brief  行数取得
 *  @param  [in] iPage ページ
 *  @retval 行数 (失敗:-1)
 *  @note
 */
int CIoPropertyBlock::GetRowMax(int iPage) const
{
    if (iPage >= GetPageMax())
    {
        return -1;
    }

    return static_cast<int>(m_lstId[iPage].size());
}

/**
 *  @brief  プロパティ取得
 *  @param  [in] iIoId  行
 *  @retval プロパティ
 *  @note
 */
CIoProperty*  CIoPropertyBlock::GetProperty(int iIoId) const
{
    std::map<int, std::shared_ptr<CIoProperty> >::const_iterator  ite;
    ite = m_mapProperty.find(iIoId);
    if (ite == m_mapProperty.end())
    {
        return NULL;
    }

    CIoProperty* pRet;

    pRet = (ite->second).get();
    return pRet;
}

/**
 *  @brief  プロパティ取得
 *  @param  [out]   pProperty Ioプロパティ
 *  @param  [in]    iIoId  
 *  @retval true 瀬尾行
 *  @note
 */
bool  CIoPropertyBlock::GetPropertyWeak(std::weak_ptr<CIoProperty>* pProperty, int iIoId)
{
    std::map<int, std::shared_ptr<CIoProperty> >::iterator  ite;
    ite = m_mapProperty.find(iIoId);
    if (ite == m_mapProperty.end())
    {
        return false;
    }

    *pProperty = ite->second;
    return true;
}

/**
 *  @brief  IO名称生成
 *  @param  [in] iIoID   
 *  @retval 名称
 *  @note
 */
StdString CIoPropertyBlock::CreateIoName(int iIoId)  const
{
    StdString strRet;

    CIoProperty*  pPorp =  GetProperty( iIoId);
    if (!pPorp)
    {
        return strRet;
    }

    //TODO:実装


    return strRet;
}

/**
 *  @brief  削除
 *  @param  [in] iID   
 *  @retval true:成功
 *  @note
 */
bool CIoPropertyBlock::Del(int iId)
{
    std::map<int, std::shared_ptr<CIoProperty> >::iterator  ite;
    ite = m_mapProperty.find(iId);
    if (ite == m_mapProperty.end())
    {
        return false;
    }

    size_t iRow;
    size_t iPage;
    CIoProperty* pProp;

    pProp = (ite->second).get();

    iRow  = pProp->GetRow();
    iPage = pProp->GetPage();

    STD_ASSERT(iPage < 0);
    STD_ASSERT(iRow  < 0);

    STD_ASSERT(iPage >= m_lstId.size());
    STD_ASSERT(iRow  >= m_lstId[iPage].size());

    std::vector<int>::iterator  iteRow;
    iteRow = m_lstId[iPage].begin();
    iteRow += iRow;

    STD_ASSERT ( *iteRow == iId);

    m_lstId[iPage].erase(iteRow);
    m_mapProperty.erase(ite);

    //Row番号振りなおし
    int iRowId;

    for(int iCnt = 0; 
        iCnt  < GetRowMax(SizeToInt(iPage));
        iCnt++)
    {
        iRowId = m_lstId[iPage].at(iCnt);
        ite = m_mapProperty.find(iRowId);
        if (ite != m_mapProperty.end())
        {
            (ite->second)->SetRow(iCnt);
        }
    }
    m_iChgCnt++;
    m_bNeedUpdate = true;
    return true;
}

/**
 *  @brief  入替え
 *  @param  [in] iID1
 *  @param  [in] iID2
 *  @retval true:成功
 *  @note
 */
bool CIoPropertyBlock::Swap(int iId1, int iId2)
{

    CIoProperty* pProp1 =  GetProperty(iId1);
    CIoProperty* pProp2 =  GetProperty(iId2);

    if (pProp1 == NULL) {return false;}
    if (pProp2 == NULL) {return false;}


    int iPage1 = pProp1->GetPage();
    int iRow1  = pProp1->GetRow();

    int iPage2 = pProp2->GetPage();
    int iRow2  = pProp2->GetRow();

    pProp1->SetPage( iPage2);
    pProp1->SetRow ( iRow2 );

    pProp2->SetPage( iPage1);
    pProp2->SetRow ( iRow1 );

    m_lstId[iPage1].at(iRow1) = iId2;
    m_lstId[iPage2].at(iRow2) = iId1;

    m_iChgCnt++;
    m_bNeedUpdate = true;
    return true;
}

/**
 *  @brief  追加
 *  @param  [in] iPage
 *  @retval true:成功
 *  @note
 */
CIoProperty*  CIoPropertyBlock::Add(int iPage)
{
    if (iPage < 0)
    {
        return NULL;
    }

    if(iPage >= GetPageMax())
    {
        return NULL;
    }


    int iRow = static_cast<int>(m_lstId[iPage].size());
    int iId = m_IdObj.CreateId();


    typedef std::pair<int, std::shared_ptr<CIoProperty> > pair_t;
    m_mapProperty.insert( pair_t(iId, std::make_shared<CIoProperty>()) );

    CIoProperty* pProp;
    pProp = m_mapProperty[iId].get();

    pProp->SetId(iId);
    pProp->SetPage(iPage);
    pProp->SetRow(iRow);

    if (m_iUserDataNum > 0)
    {
        pProp->SetUserDataSize(m_iUserDataNum);
    }

    m_lstId[iPage].push_back(iId);
    m_iChgCnt++;
    m_bNeedUpdate = true;
    return pProp;
}


/**
 *  @brief  追加
 *  @param  [in] iPage
 *  @param  [in] iRow
 *  @retval true:成功
 *  @note   iRowの前に追加する
 */
bool CIoPropertyBlock::Insert(int iPage, int iRow)
{
    if (iPage < 0)
    {
        return false;
    }

    if(iPage >= GetPageMax())
    {
        return false;
    }

    if (iRow >= GetRowMax(iPage))
    {
        CIoProperty* pProp;
        pProp = Add(iPage);
        return ((pProp == NULL)? false :true);
    }

    int iId = m_IdObj.CreateId();

    std::vector<int>::iterator  iteRow;
    iteRow = m_lstId[iPage].begin();
    iteRow += iRow;

    m_lstId[iPage].insert(iteRow, iId);

    typedef std::pair<int, std::shared_ptr<CIoProperty> > pair_t;
    m_mapProperty.insert( pair_t(iId, std::make_shared<CIoProperty>()) );

    CIoProperty* pProp;

    pProp = m_mapProperty[iId].get();
    pProp->SetId(iId);
    pProp->SetPage(iPage);
    pProp->SetRow(iRow);
    if (m_iUserDataNum > 0)
    {
        pProp->SetUserDataSize(m_iUserDataNum);
    }

    std::vector<int>::iterator  ite;


    iRow++;
    iteRow = m_lstId[iPage].begin();
    iteRow += iRow;

    for (ite  = iteRow;
         ite != m_lstId[iPage].end();
         ite++)
    {
        pProp = m_mapProperty[*ite].get();
        pProp->SetRow(iRow);
        iRow++;
    }
    m_iChgCnt++;
    m_bNeedUpdate = true;
    return true;
}

/**
 * @brief   読込後初期化処理
 * @param   なし
 * @retval  true 成功
 * @note	
 */
bool CIoPropertyBlock::LoadAfter()
{

    return true;
}


/**
 *  @brief  読込後初期化処理
 *  @param  なし
 *  @retval true:成功
 *  @note
 */
bool CIoPropertyBlock::LoadInit()
{
    int iPage = 0;
    int iRow  = 0;
    CIoProperty* pProp;

    foreach( std::vector< int>& lst ,m_lstId)
    {
        foreach( int iId ,lst)
        {
            pProp = GetProperty(iId);
            pProp->SetPage(iPage);
            pProp->SetRow (iRow);
            STD_ASSERT(pProp->GetUserDataSize() == m_iUserDataNum);
            iRow++;
        }
        iPage++;
        iRow = 0;
    }
    return true;
}

/**
 *  @brief  アドレス再計算
 *  @param  [in] iPage ページ
 *  @retval true 成功
 *  @note   iPage == -1 で全頁再設定
 */
bool CIoPropertyBlock::RecalcAddress(int iPage)
{
    using namespace IO_COMMON;

    int iPageMax = GetPageMax();
    if (iPage == -1)
    {
        bool bRet = true;
        for (int iCnt = 0; iCnt < iPage; iCnt++)
        {
            if(!RecalcAddress(iCnt))
            {
                bRet = false;
            }
        }
        return bRet;
    }

    if (iPage >= iPageMax)
    {
        return false;
    }

    if (iPage < 0)
    {
        return false;
    }

    std::vector< int>* pIdList;

    pIdList = &m_lstId[iPage];

    int iIAddress  = 0;
    int iIOffest  = 0;
    int iRow      = -1;
    CIoProperty* pProperty;
    DAT_TYPE     eType;
    bool         bBit = false;

    foreach(int iIoId, *pIdList)
    {
        iRow++;
        pProperty = GetProperty(iIoId);
        eType = pProperty->GetType();
        pProperty->SetRow(iRow);

        if (eType == DAT_BIT)
        {
            bBit = true;
            pProperty->SetBufferPos(iIAddress);
            pProperty->SetOffset(iIOffest);
            iIOffest++;

            if (iIOffest == 7)
            {
                iIOffest = 0;
                iIAddress++;
                bBit = false;
            }
            continue;
        }
        else
        {
            //STD_ASSERT(iIOffest == 0);
            if (bBit)
            {
                iIAddress++;
                bBit = false;
            }

            pProperty->SetBufferPos(iIAddress);
            pProperty->SetOffset(iIOffest);

            int iSizeOfChar = sizeof(StdChar) / sizeof(char);

            if (pProperty->GetType() == DAT_STR)
            {
                iIAddress += ((pProperty->GetLen() + 1) * iSizeOfChar);
            }
            else
            {
                iIAddress += pProperty->GetLen();
            }
        }
    }
    return true;
}


/**
 *  @brief  実行前準備
 *  @param  なし
 *  @retval true 成功
 *  @note
 */
bool CIoPropertyBlock::SetupStart()
{
    if (!m_bNeedUpdate)
    {
        return true;
    }

    bool bRet = true;
    std::map<int, std::shared_ptr<CIoProperty> >::iterator iteMap;

    m_mapNameId.clear();
    for(iteMap  = m_mapProperty.begin();
        iteMap != m_mapProperty.end();
        ++iteMap)
    {
        m_mapNameId[iteMap->second->GetName()] = iteMap->first;
    }

    int iPageMax;
    iPageMax = GetPageMax();

    for (int iPage = 0; iPage < iPageMax; iPage++)
    {
        if (!RecalcAddress(iPage))
        {
            bRet = false;
        }
    }

    if (m_bNeedUpdate)
    {
        m_bNeedUpdate = false;
    }

    return bRet;
}

/**
 * @brief   再コンパイルが必要な変更の有無
 * @param   なし
 * @return  true 要再コンパイル
 * @note    
 */
bool CIoPropertyBlock::IsNeedCompileChange()
{
    if (m_bNeedUpdate)
    {
        return true;
    }
  
    std::map<int, std::shared_ptr<CIoProperty> >:: iterator iteProp;

    for (iteProp  = m_mapProperty.begin();
         iteProp != m_mapProperty.end();
         iteProp++)
    {
        if(iteProp->second->IsNeedCompileChange())
        {
            return true;
        }
    }

    return false;
}

/**
 * @brief   再コンパイルが必要な変更の設定
 * @param   [in] bChg (true:要再コンパイル)
 * @return  なし
 * @note    
 */
void CIoPropertyBlock::SetNeedCompileChange(bool bChg)
{
    if (bChg)
    {
        SetupStart();
    }

    std::map<int, std::shared_ptr<CIoProperty> >:: iterator iteProp;

    for (iteProp  = m_mapProperty.begin();
         iteProp != m_mapProperty.end();
         iteProp++)
    {
        iteProp->second->SetNeedCompileChange(bChg);
    }
}

/**
 * @brief   シリアル化(ロード)
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  なし
 * @note	
 */
template<class Archive>
void CIoPropertyBlock::load(Archive& ar, const unsigned int version)
{
    SERIALIZATION_INIT;

        SERIALIZATION_LOAD("Map"             , m_mapProperty);
        SERIALIZATION_LOAD("Page"            , m_lstId);
        SERIALIZATION_LOAD("UserDataNumber"  , m_iUserDataNum);
        SERIALIZATION_LOAD("IdDef"           , m_IdObj);

        LoadInit();
        m_iChgCnt = 0;

    MOCK_EXCEPTION_FILE(e_file_read);
}

/**
 * @brief   シリアル化(セーブ)
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  なし
 * @note	
 */
template<class Archive>
void CIoPropertyBlock::save(Archive& ar, const unsigned int version) const
{
    SERIALIZATION_INIT;

        SERIALIZATION_SAVE("Map"             , m_mapProperty);
        SERIALIZATION_SAVE("Page"            , m_lstId);
        SERIALIZATION_SAVE("UserDataNumber"  , m_iUserDataNum);
        SERIALIZATION_SAVE("IdDef"           , m_IdObj);
        m_iChgCnt = 0;

    MOCK_EXCEPTION_FILE(e_file_write);
}


//load save のインスタンスが生成されないため
//ダミーとして実装
void CIoPropertyBlock::Dummy()
{
    unsigned int iVersion = 0;

    StdStreamOut outFs(_T(""));
    StdStreamIn  inFs(_T(""));

    boost::archive::text_woarchive txtOut(outFs); 
    boost::archive::text_wiarchive txtIn(inFs);

    save<boost::archive::text_woarchive>( txtOut, iVersion);
    load<boost::archive::text_wiarchive>( txtIn,  iVersion);


    StdXmlArchiveOut outXml(outFs);
    StdXmlArchiveIn inXml(inFs);

    save<StdXmlArchiveOut>(outXml, iVersion);
    load<StdXmlArchiveIn>(inXml, iVersion);

    boost::filesystem::ofstream outFsStd(_T(""));
    boost::filesystem::ifstream inFsStd(_T(""));

    boost::archive::binary_oarchive outBin(outFsStd);
    boost::archive::binary_iarchive inBin(inFsStd);

    save<boost::archive::binary_oarchive>(outBin, iVersion);
    load<boost::archive::binary_iarchive>(inBin, iVersion);


}

//*******************************
//             TEST
//*******************************
#ifdef _DEBUG





#include "System/CSystem.h"


void TEST_CIoPropertyBlock_SetupSstart()
{
    using namespace IO_COMMON;
    CIoPropertyBlock block;

    block.SetPageMax(3);

    //データ追加
    //                                                  Offset
    //                                                  |  UnitNo
    //                                                  |  |    Len
    block.Add(0)->SetData(_T("Item_0_0"),  IT_AI_16, 0,   0,  0, _T("NOTE_1"), _T(""), _T(""));
    block.Add(0)->SetData(_T("Item_0_1"),  IT_AO_8 , 0,   0,  0, _T("NOTE_2"), _T(""), _T(""));
    block.Add(0)->SetData(_T("Item_0_2"),  IT_AI_32, 0,   0,  0, _T("NOTE_3"), _T(""), _T(""));
    block.Add(0)->SetData(_T("Item_0_3"),  IT_STR_I, 0,   0,256, _T("NOTE_4"), _T(""), _T(""));
    block.Add(0)->SetData(_T("Item_0_4"),  IT_AI_16, 0,   0,  0, _T("NOTE_5"), _T(""), _T(""));

    block.Add(1)->SetData(_T("Item_1_0"),  IT_DOUBLE_I, 0, 0,  0, _T("NOTE_1_1"), _T(""), _T(""));
    block.Add(1)->SetData(_T("Item_1_DI_0"),  IT_DI,    0, 0,  0, _T("NOTE_1_2"), _T(""), _T(""));
    block.Add(1)->SetData(_T("Item_1_DI_1"),  IT_DI,    1, 0,  0, _T("NOTE_1_2"), _T(""), _T(""));
    block.Add(1)->SetData(_T("Item_1_DI_2"),  IT_DI,    2, 0,  0, _T("NOTE_1_2"), _T(""), _T(""));
    block.Add(1)->SetData(_T("Item_1_DI_3"),  IT_DI,    3, 0,  0, _T("NOTE_1_2"), _T(""), _T(""));
    block.Add(1)->SetData(_T("Item_1_DI_4"),  IT_DI,    4, 0,  0, _T("NOTE_1_2"), _T(""), _T(""));
    block.Add(1)->SetData(_T("Item_1_DI_5"),  IT_DI,    5, 0,  0, _T("NOTE_1_2"), _T(""), _T(""));
    block.Add(1)->SetData(_T("Item_1_DI_6"),  IT_DI,    6, 0,  0, _T("NOTE_1_2"), _T(""), _T(""));
    block.Add(1)->SetData(_T("Item_1_DI_7"),  IT_DI,    7, 0,  0, _T("NOTE_1_2"), _T(""), _T(""));


    //STD_ASSERT( block.RecalcAddress(0) );
    //STD_ASSERT( block.RecalcAddress(1) );

    STD_ASSERT( block.SetupStart());
    STD_ASSERT( block.GetProperty(block.GetIoIdName(_T("Item_0_0")))->GetBufferPos() == 0);
    STD_ASSERT( block.GetProperty(block.GetIoIdName(_T("Item_0_1")))->GetBufferPos() == 2);
    STD_ASSERT( block.GetProperty(block.GetIoIdName(_T("Item_0_2")))->GetBufferPos() == 3);
    STD_ASSERT( block.GetProperty(block.GetIoIdName(_T("Item_0_4")))->GetBufferPos() == 7);
    STD_ASSERT( block.GetProperty(block.GetIoIdName(_T("Item_0_4")))->GetBufferPos() ==263);

}


void TEST_CIoPropertyBlock()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());


    //=================================
    CIoPropertyBlock block;

    block.SetPageMax(3);

    //データ追加
    STD_ASSERT( block.Add(0) );
    STD_ASSERT( block.Add(0) );
    STD_ASSERT( block.Add(0) );
    STD_ASSERT( block.Add(0) );

    STD_ASSERT( block.Add(1) );
    STD_ASSERT( block.Add(1) );

    STD_ASSERT( block.Add(2) );
    STD_ASSERT( block.Add(2) );
    STD_ASSERT( block.Add(2) );

    STD_ASSERT( !block.Add(3) );
    STD_ASSERT( !block.Add(-1) );


    //データ数の表示
    STD_ASSERT( block.GetPageMax() == 3);
    STD_ASSERT( block.GetRowMax(0) == 4);
    STD_ASSERT( block.GetRowMax(1) == 2);
    STD_ASSERT( block.GetRowMax(2) == 3);
    //=================================
    
    int id_0_0 = block.GetIoId(0, 0);
    int id_0_1 = block.GetIoId(0, 1);
    int id_0_2 = block.GetIoId(0, 2);
    int id_0_3 = block.GetIoId(0, 3);

    int id_1_0 = block.GetIoId(1, 0);
    int id_1_1 = block.GetIoId(1, 1);
 
    int id_2_0 = block.GetIoId(2, 0);
    int id_2_1 = block.GetIoId(2, 1);
    int id_2_2 = block.GetIoId(2, 2);

    STD_ASSERT(block.GetProperty(id_2_1)->GetPage() == 2);
    STD_ASSERT(block.GetProperty(id_2_1)->GetRow() == 1);
    
    block.Del(id_2_0);

    STD_ASSERT(block.GetProperty(id_2_1)->GetRow() == 0);

    //=================================
    namespace fs = boost::filesystem;
    StdPath  pathTest = CSystem::GetSystemPath();
    pathTest /= _T("TEST_CIoPropertyBlock.xml");

    //保存
    StdStreamOut outFs(pathTest);
    {
        StdXmlArchiveOut outXml(outFs);
        outXml << boost::serialization::make_nvp("Root", block);
    }
    outFs.close();

    CIoPropertyBlock block2;

    //読込
    StdStreamIn inFs(pathTest);
    {
        StdXmlArchiveIn inXml(inFs);
        inXml >> boost::serialization::make_nvp("Root", block2);
    }
    inFs.close();

    STD_ASSERT( block2.GetPageMax() == 3);
    STD_ASSERT( block2.GetRowMax(0) == 4);
    STD_ASSERT( block2.GetRowMax(1) == 2);
    STD_ASSERT( block2.GetRowMax(2) == 2);

    STD_ASSERT( id_0_0 == block2.GetIoId(0, 0));
    STD_ASSERT( id_0_1 == block2.GetIoId(0, 1));
    STD_ASSERT( id_0_2 == block2.GetIoId(0, 2));
    STD_ASSERT( id_0_3 == block2.GetIoId(0, 3));

    STD_ASSERT( id_1_0 == block2.GetIoId(1, 0));
    STD_ASSERT( id_1_1 == block2.GetIoId(1, 1));
 
    //int id_2_0 = block.GetId(2, 0);
    STD_ASSERT( id_2_1 == block2.GetIoId(2, 0));
    STD_ASSERT( id_2_2 == block2.GetIoId(2, 1));
    //=================================
    
    //SWAP
    block2.Swap(id_1_1, id_0_2);

    STD_ASSERT( block2.GetProperty(id_0_2)->GetPage() == 1);
    STD_ASSERT( block2.GetProperty(id_0_2)->GetRow()  == 1);

    STD_ASSERT( block2.GetProperty(id_1_1)->GetPage() == 0);
    STD_ASSERT( block2.GetProperty(id_1_1)->GetRow()  == 2);

    //Insert

    block.Insert(0, 2);
    int iNew_0_2 = block.GetIoId(0, 2);

    CIoProperty*  pNew_0_2 = block.GetProperty(iNew_0_2);

    STD_ASSERT( pNew_0_2->GetPage() == 0);
    STD_ASSERT( pNew_0_2->GetRow()  == 2);

    STD_ASSERT( id_0_2 == block.GetIoId(0, 3));
    STD_ASSERT( id_0_3 == block.GetIoId(0, 4));

    //========================================
    TEST_CIoPropertyBlock_SetupSstart();

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));

}
#endif //_DEBUG