/**
 * @brief			CIoPropertyBlock ヘッダーファイル
 * @file			CIoPropertyBlock.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

#ifndef  _IO_PROPERTY_BLOCK_H__
#define  _IO_PROPERTY_BLOCK_H__
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CIoCommon.h"
#include "Utility/CIdObj.h"

/*---------------------------------------------------*/
/*  Class                                            */
/*---------------------------------------------------*/
class CIoLine;
class CIoAccess;
class CIoProperty;
class CIoConnect;

/*---------------------------------------------------*/
/*  Enums                                            */
/*---------------------------------------------------*/

/*---------------------------------------------------*/
/*  Defines                                          */
/*---------------------------------------------------*/

/*


*/
class CIoPropertyBlock
{
friend void TEST_CIoPropertyBlock();
friend void TEST_CIoPropertyBlock_RecalcAddress();
protected:

    //==================
    // 保存データ
    //==================
    std::vector< std::vector< int> > m_lstId;

    std::map<int, std::shared_ptr<CIoProperty> >     m_mapProperty;

    CIdObj                          m_IdObj;

    //==================
    // 非保存データ
    //==================
    //!< ユーザデータ数
    int                       m_iUserDataNum;

    mutable bool              m_iChgCnt;

    bool                      m_bNeedUpdate;

    std::map< StdString, int > m_mapNameId;

public:

    //!< コンストラクタ
    CIoPropertyBlock();

    //!< コンストラクタ
    CIoPropertyBlock(const CIoPropertyBlock & ioPropBlock);

    //!< デストラクタ
    ~CIoPropertyBlock();

    //!< ユーザデータ数設定
    bool SetUserDataNum(int iNum);

    //!< ユーザデータ数取得
    int  GetUserDataNum() const;

    //!< ID取得
    int GetIoId(int iPage, int iRow) const;

    //!< ID取得
    int GetIoIdName(const StdString& strName)  const;

    //!< ページ、行取得
    bool GetPageRow(int* pPage, int* pRow, int iIoId) const;
    
    //!< ページ数設定
    bool SetPageMax(int iPageMax);

    //!< ページ数取得
    int GetPageMax()  const;

    //!<行数取得
    int GetRowMax(int iPage)  const;

     //!< プロパティ取得
    CIoProperty*  GetProperty(int iIoId) const;

    //!< プロパティ取得(WeakPtr)
    bool GetPropertyWeak(std::weak_ptr<CIoProperty>* pProperty, int iIoId);

    //!< IO名称生成
    StdString CreateIoName(int iIoId)  const;

    //!< 削除
    bool Del(int iIoId);

    //!< 入替え
    bool Swap(int iIoId1, int iIoId2);

    //!< 追加
    CIoProperty*  Add(int iPage);

    //!< 追加
    bool  Insert(int iPage, int iRow);

    //!< アドレス再計算
    bool RecalcAddress(int iPage);

    //!< 更新
    //bool Update();

    //!< 再コンパイルが必要な変更の有無
    bool IsNeedCompileChange();

    //!< 再コンパイルが必要な変更の設定
    void SetNeedCompileChange(bool bChg = true);

    //!<----------------------
    //!<  実行前準備
    //!<----------------------
    //!< 実行前準備
    virtual bool SetupStart();

 
    //!< 読込後初期化処理
    bool LoadAfter();


protected:
    //!< ダミー
    void Dummy();

    //!< 読込時初期化処理
    bool LoadInit();



private:
    friend class boost::serialization::access;  
    BOOST_SERIALIZATION_SPLIT_MEMBER();

    template<class Archive>
    void load(Archive& ar, const unsigned int version);

    template<class Archive>
    void save(Archive& ar, const unsigned int version) const;

};
#endif  //_IO_PROPERTY_BLOCK_H__
