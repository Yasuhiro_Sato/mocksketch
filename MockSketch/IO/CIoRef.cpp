/**
 * @brief			CIoRef実装ファイル
 * @file			CIoRef.cpp
 * @author			Yasuhiro Sato
 * @date			19-8-2011 17:17:00
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CIoRef.h"
#include "./CIoDef.h"
#include "./CIoProperty.h"
#include "./CIoPropertyBlock.h"
#include "./CIoConnect.h"
#include "./CIoAccess.h"
#include "System/CSystem.h"
#include "Utility/CUtility.h"
#include "DefinitionObject/ObjectDef.h"
#include "DrawingObject/CDrawingScriptBase.h"
#include "DefinitionObject/CPartsDef.h"
#include <boost/serialization/export.hpp> 

BOOST_CLASS_EXPORT(CIoRef);

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CIoRef::CIoRef():
m_pRef          (NULL),
m_pParent       (NULL),
m_bLoadAfter    (false)
{
    m_eDispIo = IO_COMMON::LT_REF;
}

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CIoRef::CIoRef(const  CIoRef&  ref):
CIoDefBase  (ref)   ,
m_pRef      (NULL),
m_pParent   (NULL),
m_bLoadAfter    (false)
{

    m_pRef = ref.m_pRef;
    if( std::shared_ptr<CIoPropertyBlock> tmp = ref.m_wpPropety.lock() )
    {
        m_wpPropety = tmp;
    }
}

/**
 *  @brief  デストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CIoRef::~CIoRef()
{
}

/**
 *  @brief  生成処理
 *  @param  [in] pIoAccess
 *  @param  [in] pDef
 *  @retval true 成功   
 *  @note
 */

bool CIoRef::Create(CDrawingScriptBase* pBase,
                    std::shared_ptr<CIoAccess>  pIoAccess)
{
    m_pParent = NULL;
    if (!pBase)
    {
        return false;
    }

    m_psAccess = pIoAccess;

    std::shared_ptr<CObjectDef> pObjDef;
    pObjDef = pBase->GetObjectDef().lock();
    if (!pObjDef)
    {
        return false;
    }

    m_pRef = pObjDef->GetIoDef();

    m_pRef->SetBlockWeak(&m_wpPropety);
    m_psAccess->SetIoDef(this);
    m_psAccess->Init();

    m_pParent       = pBase;
    m_bLoadAfter    = true;

    //!< 基本的にユーザ定義データは CIoAccessBoardで設定して使用する
    int iUserItemNum = m_psAccess->GetUserItemNumber();

    CIoPropertyBlock* pBlock = GetBlock();
    if (!pBlock)
    {
        return false;
    }

    pBlock->SetUserDataNum(iUserItemNum);


    if(m_psAccess->GetAccessType() == CIoAccess::ACCESS_BOARD)
    {
        // ボードの場合は。ボードの設定に合わせる
        // CIoRefでボードは使用しないはず
    }

    return true;
}


/**
 *  @brief  開始前処理
 *  @param  なし
 *  @retval true:成功
 *  @note
 */
bool CIoRef::SetupStart()
{
    bool bRet;
    if(m_psAccess->GetAccessType() == CIoAccess::ACCESS_BOARD)
    {
        // ボードの場合は。ボードの設定に合わせる
    }
    else
    {
        int iMaxPage;
        iMaxPage = m_pRef->GetMaxPage();
        bRet = m_psAccess->SetMaxPage(iMaxPage);
        STD_ASSERT(bRet);
    }

    bRet = CIoDefBase::SetupStart();
    return bRet;
}


/**
 *  @brief  IO定義ブロック取得
 *  @param  なし
 *  @retval IO定義ブロックへのポインタ
 *  @note
 */
CIoPropertyBlock*  CIoRef::GetBlock() const
{
  if( std::shared_ptr<CIoPropertyBlock> tmp = m_wpPropety.lock() )
  {
    return tmp.get();
  }
  else
  {
    return NULL;
  }
}

/**
 *  @brief  オブジェクト定義取得
 *  @param  なし
 *  @retval オブジェクト定義
 *  @note   
 */
/*
CObjectDef* CIoRef::GetDef() const
{
    if (!m_pParent)
    {
        return NULL;
    }

    std::shared_ptr<CObjectDef> pObjDef;
    pObjDef = pBase->GetObjectDef().lock();
    if (!pObjDef)
    {
        return NULL;
    }
    return pObjDef.get();
}
*/

/**
 *  @brief  IDによる接続追加
 *  @param  [in] iIdFrom   接続先オブジェクトID
 *  @param  [in] iIoIdFrom 接続先IoID
 *  @param  [in] iIoIdTO   接続元IoID
 *  @retval true 成功
 *  @note   
 */
bool CIoRef::AddConnectById(int iIdFrom, int iIoIdFrom, int iIoIdTo)
{
    if (!m_pParent)
    {
        return false;
    }

    CPartsDef* pCtrl;

    pCtrl = m_pParent->GetPartsDef();



    std::shared_ptr<CIoConnect> spConnect = std::make_shared<CIoConnect>();

    if (iIdFrom == 0)
    {
        CIoDef* pIoDef;
        pIoDef = pCtrl->GetIoDef();
        spConnect->SetConnect(pIoDef, iIoIdFrom);

    }
    else
    {
        auto pObject = pCtrl->GetObjectById(iIdFrom);
        auto pBase = std::dynamic_pointer_cast<CDrawingScriptBase>(pObject);
        if (!pBase)
        {
            return false;
        }

        CIoRef* pIoRef;
        pIoRef = pBase->GetIoRef();
        spConnect->SetConnect(pIoRef, iIoIdFrom);

    }

    bool bConnect;
    bConnect = AddConnect( iIoIdTo, spConnect);
    return bConnect;
}

/**
 *  @brief  描画領域取得
 *  @param  なし
 *  @retval 描画領域
 *  @note   
 */
CPartsDef* CIoRef::GetPartsDef() const
{
    if (m_pParent)
    {
        return m_pParent->GetPartsDef();
    }

    return NULL;
}

/**
 *  @brief  名称取得
 *  @param  なし
 *  @retval 名称
 *  @note   IO接続時に相手側を表示するために必要
 */
StdString CIoRef::GetName() const
{
    // IO接続の名称は基本的に
    // IoDef →  IoName
    // IoRef →  Object.IoName
    // とする
    
    // CPartsDefが親になる場合は、名称無し
    // CDrawingObjectが親になる場合は、そのオブジェクトの名称を
    // 返す

    if (m_pParent)
    {
        return m_pParent->GetName();
    }
    return _T("");

}
/**
 *  @brief  停止設定.
 *  @param  なし
 *  @retval true 設定成功
 *  @note   
 */
bool CIoRef::Stop()
{
    m_eDispIo = IO_COMMON::LT_REF;
    return true;
}

/**
 *  @brief  親オブジェクト設定
 *  @param  [in] pParent
 *  @retval なし
 *  @note   
 */
void CIoRef::SetParent(CDrawingScriptBase* pParent)
{
    m_pParent = pParent;
}

/**
 *  @brief  親オブジェクト取得
 *  @param  なし
 *  @retval 親オブジェクト
 *  @note   
 */
CDrawingScriptBase* CIoRef::GetParent()
{
    return m_pParent;
}

/**
 * @brief   読込後初期化処理
 * @param   なし
 * @retval  true 成功
 * @note	
 */
bool CIoRef::LoadAfter()
{
    bool bRet = true;

    if  (m_bLoadAfter)
    {
        return true;
    }
    m_bLoadAfter    = true;

    bRet = CIoDefBase::LoadAfter();

    STD_ASSERT(m_pParent);

    std::shared_ptr<CObjectDef> pDef;
    pDef = m_pParent->GetObjectDef().lock();
    auto pPartsDef = dynamic_cast<CPartsDef*>(pDef.get());
    if (pPartsDef)
    {
        m_pParent->LoadAfter(pPartsDef);
        pDef = m_pParent->GetObjectDef().lock();
    }

    if (pDef)
    {
        m_pRef = pDef->GetIoDef();
        m_pRef->SetBlockWeak(&m_wpPropety);
    }
    else
    {
        m_pRef = NULL;  
    }


    m_pBlock = NULL;
    if( std::shared_ptr<CIoPropertyBlock> tmp = m_wpPropety.lock() )
    {
        m_pBlock = tmp.get();
    }

    m_iPage = 0;
    m_eDispIo = IO_COMMON::LT_REF;
    return bRet;
}

/**
 * @brief   シリアル化(ロード)
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  なし
 * @note	
 */
template<class Archive>
void CIoRef::load(Archive& ar, const unsigned int version)
{
    SERIALIZATION_INIT
        ar >> BOOST_SERIALIZATION_BASE_OBJECT_NVP(CIoDefBase);
        m_pRef = NULL;
        m_bLoadAfter    = false;
    MOCK_EXCEPTION_FILE(e_file_read);

}


/**
 * @brief   シリアル化(セーブ)
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  なし
 * @note	
 */
template<class Archive>
void CIoRef::save(Archive& ar, const unsigned int version) const
{
    SERIALIZATION_INIT

        ar << BOOST_SERIALIZATION_BASE_OBJECT_NVP(CIoDefBase);
        //TODO: 実装

    MOCK_EXCEPTION_FILE(e_file_read);
}



//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
#include "DefinitionObject/CPartsDef.h"
#include "CProjectCtrl.h"

#include "Utility/VirtualTreeCtrl/VirtualCheckList.h"
#include "Utility/TestDlg.h"

#include "MockSketch.h"

extern  void TEST_CIoDef_SetData(CIoDef& ioDef); 


CTestDlg* g_IoRefDlg;


namespace TEST_IO_REF_CHECK
{

//-------------------------
// データ初期化
//-------------------------
void InitData(CIoRef* pIoRef)
{



}

//-------------------------
// 通知処理
//-------------------------
void OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResul)
{
    NMHDR* pNMHDR = reinterpret_cast<NMHDR*>(lParam);

    if (pNMHDR->code == LVN_ENDLABELEDIT)
    {
        //CIoRef* pIoRef = reinterpret_cast<CIoDef*>(g_IoRefDlg->GetUserData());
        //CVirtualCheckList* pList = reinterpret_cast<CVirtualCheckList*>(g_IoRefDlg->GetObj());
        //pIoDef->OnLvnEndlabeleditVlst(pList, pNMHDR, pResul);
        return;
    }

    return;
}

//-------------------------
// ウインドウサイズ変更
//-------------------------
void TestOnSize(CWnd* pTestObj, UINT nType, int cx, int cy)
{
    CVirtualCheckList* pList;
    pList = dynamic_cast<CVirtualCheckList*>(pTestObj);
}

//!< コンボボックス変更
void TestOnSelChg(StdString strSel)
{
    CIoRef* pIoRef = reinterpret_cast<CIoRef*>(g_IoRefDlg->GetUserData());

    CVirtualCheckList* pVList;
    pVList = dynamic_cast<CVirtualCheckList*>(g_IoRefDlg->GetTestObj());
    STD_ASSERT(pVList != NULL);


    int iPage;
    iPage = _tstol( strSel.c_str() );

    pIoRef->SetCurrentPage(iPage);
    pIoRef->UpdateList(pVList);

}

//!< コンボボックス2変更
void TestOnSelChg2(StdString strSel)
{
    using namespace IO_COMMON;
    CIoRef* pIoRef = reinterpret_cast<CIoRef*>(g_IoRefDlg->GetUserData());

    IO_COMMON::E_LIST_TYPE eDispIo;
    if      (strSel == _T("DEF"))   { eDispIo = LT_CONFIG;}
    else if (strSel == _T("REF"))   { eDispIo = LT_REF;}
    else if (strSel == _T("EXEC"))  { eDispIo = LT_EXEC;}
    else                            { return;}


    CVirtualCheckList* pVList;
    pVList = dynamic_cast<CVirtualCheckList*>(g_IoRefDlg->GetTestObj());

    STD_ASSERT(pVList != NULL);


    pIoRef->TEST_SetDispMode(eDispIo);
    pIoRef->UpdateCol(pVList);
}

// ダイアログコールバック
//-------------------------
//  ウインドウプロシジャ
//-------------------------
LRESULT TestWindowProc(CWnd* pTestObj, UINT message, WPARAM wParam, LPARAM lParam)
{
    CVirtualCheckList* pList;
    pList = dynamic_cast<CVirtualCheckList*>(pTestObj);
    
    static UINT uiOldMsg = 0;

    if (uiOldMsg != message)
    {
        uiOldMsg = message;
#if 0
        DB_PRINT(_T("%s,%x,%x\n"), CUtil::ConvWinMessage(message),wParam, lParam);
#endif
    }

    LRESULT lRes = 0;
    if (message == WM_NOTIFY)
    {
        OnNotify( wParam, lParam, &lRes);
        return lRes;
    }
    return 0;
}

//-------------------------
//  ウインドウプロシジャ
//-------------------------
BOOL TestPreTransProc(CWnd* pTestObj, MSG* pMsg)
{
    CVirtualCheckList* pList;
    pList = dynamic_cast<CVirtualCheckList*>(pTestObj);
    
    static UINT uiOldMsg = 0;
    if ((WM_PAINT == pMsg->message)||
        (WM_TIMER == pMsg->message))
    {
        return 0;
    }

    /*
    if (uiOldMsg != pMsg->message)
    {
        uiOldMsg = pMsg->message;
        DB_PRINT(_T("DLG %s,%x,%x,%x\n"), 
            CUtil::ConvWinMessage(pMsg->message),
            pMsg->hwnd,
            pMsg->wParam, 
            pMsg->lParam);
    }
    */
    return 0;
}

//-------------------------
//  ダイアログ初期化
//-------------------------
void TestOnInitDialog(CWnd* pTestObj)
{
    CVirtualCheckList* pList;

    pList = dynamic_cast<CVirtualCheckList*>(pTestObj);
    pList->Create( LVS_REPORT | LVS_ALIGNLEFT | LVS_OWNERDATA | WS_CHILD | 
                       WS_BORDER | WS_VISIBLE |WS_TABSTOP, CRect(), g_IoRefDlg, 10); 

    CIoRef* pIoRef = reinterpret_cast<CIoRef*>(g_IoRefDlg->GetUserData());

    pIoRef->InitList(pList);
}


} //namespace TEST_IO_REF_CHECK

void TEST_CIoRefDialog(CIoRef* pIoRef)
{
    using namespace TEST_IO_REF_CHECK;
    using namespace boost::assign;

    CTestDlg dlgTest;
    g_IoRefDlg = &dlgTest;
    CVirtualCheckList  list;

    std::vector<StdString> lstCombo;
    lstCombo += _T("000"), _T("001"), _T("002"), _T("003");

    std::vector<StdString> lstCombo2;
    lstCombo2 += _T("DEF"), _T("REF"), _T("EXEC");

    dlgTest.SetCombo(lstCombo);
    dlgTest.SetCombo2(lstCombo2);
    dlgTest.SetUserData(pIoRef);
    dlgTest.SetObj(&list);


    // データ初期化
    InitData(pIoRef);


    list.UseVisibleCol();


    dlgTest.SetOnSize       (TestOnSize);
    dlgTest.SetWindowProc   (TestWindowProc);
    dlgTest.SetPreTransProc (TestPreTransProc);
    dlgTest.SetOnInit       (TestOnInitDialog);
    dlgTest.SetSelChgCb     (TestOnSelChg);
    dlgTest.SetSelChgCb2    (TestOnSelChg2);
    
    dlgTest.EnableAutoFit();


    dlgTest.DoModal();
}

void TEST_CIoRef_Convert()
{
    double dOrg = 7.8;
    float  fOrg = 7.8f;

    StdString strVal1 = _T("7.8");

    double dVal1 = _tstof(strVal1.c_str());
    double dVal2;

    float  fVal;
    dVal2 = fOrg;
    fVal   = static_cast<float>(dVal2);

    STD_ASSERT_DBL_EQ(dOrg, dVal1);
    //STD_ASSERT_DBL_EQ(dOrg, dVal2);
    STD_ASSERT_DBL_EQ(fVal, fOrg);

    int a = 1;
}

void TEST_CIoRef_SetData(CIoDef& ioDef)
{
    using namespace IO_COMMON;
    CIoPropertyBlock* pblock;
    pblock = ioDef.GetBlock();

    ioDef.SetMaxPage(3);

    bool bRet;
    //データ追加
    //                                                        Offset
    //                                                         |  UnitNo
    //                                                         |  |    Len
    bRet = pblock->Add(0)->SetData(_T("Item2_0_0"),  IT_AO_16, 0, 0,   0, _T("NOTE_1"), _T(""), _T("")); STD_ASSERT(bRet);
    bRet = pblock->Add(0)->SetData(_T("Item2_0_1"),  IT_AI_8 , 0, 0,   0, _T("NOTE_2"), _T(""), _T("")); STD_ASSERT(bRet);
    bRet = pblock->Add(0)->SetData(_T("Item2_0_2"),  IT_AO_32, 0, 0,   0, _T("NOTE_3"), _T(""), _T("")); STD_ASSERT(bRet);
    bRet = pblock->Add(0)->SetData(_T("Item2_0_3"),  IT_STR_O, 0, 0, 256, _T("NOTE_4"), _T(""), _T("")); STD_ASSERT(bRet);
    bRet = pblock->Add(0)->SetData(_T("Item2_0_4"),  IT_AO_16, 0, 0,   0, _T("NOTE_5"), _T(""), _T("")); STD_ASSERT(bRet);

    bRet = pblock->Add(1)->SetData(_T("Item2_1_0"),  IT_DOUBLE_O, 0, 0,  0, _T("NOTE_1_1"), _T(""), _T("")); STD_ASSERT(bRet);
    bRet = pblock->Add(1)->SetData(_T("Item2_1_DO_0"),  IT_DO,    0, 0,  0, _T("NOTE_1_2"), _T(""), _T("")); STD_ASSERT(bRet);
    bRet = pblock->Add(1)->SetData(_T("Item2_1_DO_1"),  IT_DO,    1, 0,  0, _T("NOTE_1_3"), _T(""), _T("")); STD_ASSERT(bRet);
    bRet = pblock->Add(1)->SetData(_T("Item2_1_DO_2"),  IT_DO,    2, 0,  0, _T("NOTE_1_4"), _T(""), _T("")); STD_ASSERT(bRet);
    bRet = pblock->Add(1)->SetData(_T("Item2_1_DO_3"),  IT_DO,    3, 0,  0, _T("NOTE_1_5"), _T(""), _T("")); STD_ASSERT(bRet);
    bRet = pblock->Add(1)->SetData(_T("Item2_1_DO_4"),  IT_DO,    4, 0,  0, _T("NOTE_1_6"), _T(""), _T("")); STD_ASSERT(bRet);
    bRet = pblock->Add(1)->SetData(_T("Item2_1_DO_5"),  IT_DO,    5, 0,  0, _T("NOTE_1_7"), _T(""), _T("")); STD_ASSERT(bRet);
    bRet = pblock->Add(1)->SetData(_T("Item2_1_DO_6"),  IT_DO,    6, 0,  0, _T("NOTE_1_8"), _T(""), _T("")); STD_ASSERT(bRet);
    bRet = pblock->Add(1)->SetData(_T("Item2_1_DO_7"),  IT_DO,    7, 0,  0, _T("NOTE_1_9"), _T(""), _T("")); STD_ASSERT(bRet);

    //                                                                          Note            Init    Conv
    bRet = pblock->Add(2)->SetData(_T("Item2_2_0"),  IT_AO_16,     0, 0,  0, _T("NOTE_2_1"), _T("30"), _T("")); STD_ASSERT(bRet);
    bRet = pblock->Add(2)->SetData(_T("Item2_2_1"),  IT_STR_O,     0, 0, 10, _T("NOTE_2_2"), _T("1234567890"), _T("")); STD_ASSERT(bRet);
    bRet = pblock->Add(2)->SetData(_T("Item2_2_2"),  IT_LONG_O,    0, 0,  0, _T("NOTE_2_3"), _T("5"), _T("")); STD_ASSERT(bRet);
    bRet = pblock->Add(2)->SetData(_T("Item2_2_3"),  IT_FLOAT_O,   0, 0,  0, _T("NOTE_2_4"), _T("7.8"), _T("")); STD_ASSERT(bRet);
    bRet = pblock->Add(2)->SetData(_T("Item2_2_4"),  IT_AO_16,     0, 0,  0, _T("NOTE_2_5"), _T("20") , _T("20.0:500.0")); STD_ASSERT(bRet);
    bRet = pblock->Add(2)->SetData(_T("Item2_2_5"),  IT_AO_16,     0, 0,  0, _T("NOTE_2_6"), _T("0")  , _T("20.0:500.0")); STD_ASSERT(bRet);
    bRet = pblock->Add(2)->SetData(_T("Item2_2_6"),  IT_AI_16,     0, 0,  0, _T("NOTE_2_7"), _T("800"), _T("20.0:500.0")); STD_ASSERT(bRet);
    bRet = pblock->Add(2)->SetData(_T("Item2_2_7"),  IT_AI_16,     0, 0,  0, _T("NOTE_2_8"), _T("2"), _T("1,1")); STD_ASSERT(bRet);
    bRet = pblock->Add(2)->SetData(_T("Item2_2_8"),  IT_AI_16,     0, 0,  0, _T("NOTE_2_9"), _T("3"), _T("[0:1],[100:101],[200:401]")); STD_ASSERT(bRet);
    ioDef.RecalcAddress(-1);
}

void TEST_CIoRef_CHK_INIT(CIoRef* pIoParts2Ref, CIoDef*     pIoParts1)
{
    //初期値確認

    //                                              Note            Init    Conv
    //_T("Item2_2_0"),  IT_AO_16,     0, 0,  0, _T("NOTE_2_1"), _T("30"), _T(""));
    //_T("Item2_2_1"),  IT_STR_O,     0, 0, 10, _T("NOTE_2_2"), _T("1234567890"), _T(""));
    //_T("Item2_2_2"),  IT_LONG_O,    0, 0,  0, _T("NOTE_2_3"), _T("5"), _T(""));
    //_T("Item2_2_3"),  IT_FLOAT_O,   0, 0,  0, _T("NOTE_2_4"), _T("7.8"), _T(""));
    //_T("Item2_2_4"),  IT_AO_16,     0, 0,  0, _T("NOTE_2_5"), _T("20") , _T("20.0:500.0"));
    //_T("Item2_2_5"),  IT_AO_16,     0, 0,  0, _T("NOTE_2_6"), _T("0")  , _T("20.0:500.0"));
    //_T("Item2_2_6"),  IT_AO_16,     0, 0,  0, _T("NOTE_2_7"), _T("800"), _T("20.0:500.0"));
    //_T("Item2_2_7"),  IT_AO_16,     0, 0,  0, _T("NOTE_2_8"), _T("2"), _T("1,1"));
    //_T("Item2_2_8"),  IT_AO_16,     0, 0,  0, _T("NOTE_2_9"), _T("3"), _T("[0:1],[100:101],[200:401]"));

    CIoPropertyBlock*  pBlock1 = pIoParts1->GetBlock();
    CIoPropertyBlock*  pBlock2Ref = pIoParts2Ref->GetBlock();

    int iIn1 = 2;
    int iIn2 = 5;

    double dIn1 = 123.45;
    double dIn2 = 321.23;
    double dOut = 2345.4;

    StdString strOut;
    StdString strIn1;
    StdString strIn2;
 

    //_T("Item2_2_0"),  IT_AO_16,     0, 0,  0, _T("NOTE_2_1"), _T("30"), _T(""));
    iIn1 = pIoParts2Ref->GetInt( pBlock2Ref->GetIoId(2, 0));
    STD_ASSERT(iIn1 == 30);
    iIn2 = pIoParts1->GetInt( pBlock1->GetIoId(2, 0));
    STD_ASSERT(iIn2 == 30);

    //出力側の文字列長は５に設定してある
    //_T("Item2_2_1"),  IT_STR_O,     0, 0, 10, _T("NOTE_2_2"), _T("1234567890"), _T(""));
    strOut = _T("1234567890");
    strIn1 = pIoParts2Ref->GetStr( pBlock2Ref->GetIoId(2, 1));
    STD_ASSERT(strIn1 == strOut);

    //TODO: 文字長が出力側の設定になってしまう
    strOut = _T("12345");
    strIn2 = pIoParts1->GetStr( pBlock1->GetIoId(2, 1));
    STD_ASSERT(strIn2 == strOut);  

    //_T("Item2_2_2"),  IT_LONG_O,    0, 0,  0, _T("NOTE_2_3"), _T("5"), _T(""));
    iIn1 = pIoParts2Ref->GetInt( pBlock2Ref->GetIoId(2, 2));
    STD_ASSERT(iIn1 == 5);
    iIn2 = pIoParts1->GetInt( pBlock1->GetIoId(2, 2));
    STD_ASSERT(iIn2 == 5);

    //_T("Item2_2_3"),  IT_FLOAT_O,   0, 0,  0, _T("NOTE_2_4"), _T("7.8"), _T(""));
    dIn1 = pIoParts2Ref->GetDouble( pBlock2Ref->GetIoId(2, 3));
    float fVal = 7.8f;
    float fIn1 = static_cast<float>(dIn1);
    STD_ASSERT_DBL_EQ(fIn1 , fVal);
    dIn2 = pIoParts1->GetDouble( pBlock1->GetIoId(2, 3));
    float fIn2 = static_cast<float>(dIn1);
    STD_ASSERT_DBL_EQ(fIn2 , fVal);

    //_T("Item2_2_4"),  IT_AO_16,     0, 0,  0, _T("NOTE_2_5"), _T("20") , _T("20.0:500.0"));
    dIn1 = pIoParts2Ref->GetDouble( pBlock2Ref->GetIoId(2, 4));
    STD_ASSERT_DBL_EQ(dIn1 , 20.0);
    dIn2 = pIoParts1->GetDouble( pBlock1->GetIoId(2, 4));
    STD_ASSERT_DBL_EQ(dIn2 , 20.0);

    //範囲外の初期値を設定した場合 (下限値)
    //_T("Item2_2_5"),  IT_AO_16,     0, 0,  0, _T("NOTE_2_6"), _T("0")  , _T("20.0:500.0"));
    dIn1 = pIoParts2Ref->GetDouble( pBlock2Ref->GetIoId(2, 5));
    STD_ASSERT_DBL_EQ(dIn1 , 20.0);
    dIn2 = pIoParts1->GetDouble( pBlock1->GetIoId(2, 5));
    STD_ASSERT_DBL_EQ(dIn2 , 20.0);

    //範囲外の初期値を設定した場合 (上限値)
    //_T("Item2_2_6"),  IT_AO_16,     0, 0,  0, _T("NOTE_2_7"), _T("800"), _T("20.0:500.0"));
    dIn1 = pIoParts2Ref->GetDouble( pBlock2Ref->GetIoId(2, 6));
    STD_ASSERT_DBL_EQ(dIn1 , 500.0);
    dIn2 = pIoParts1->GetDouble( pBlock1->GetIoId(2, 6));
    STD_ASSERT_DBL_EQ(dIn2 , 500.0);

    //_T("Item2_2_7"),  IT_AO_16,     0, 0,  0, _T("NOTE_2_8"), _T("2"), _T("1,1"));
    //_T("Item2_2_8"),  IT_AO_16,     0, 0,  0, _T("NOTE_2_9"), _T("3"), _T("[0:1],[100:101],[200:401]"));
}

void TEST_CIoRef_CHK(CIoRef* pIoParts2Ref, CIoDef*     pIoParts1)
{
    //=====================
    // 未変換値テスト
    //=====================
    // Item2_0_0,  IT_AO_16 に   15を入力して
    // Item_0_0,  IT_AI_16  から 15が出力されるか確認
    CIoPropertyBlock*  pBlock1 = pIoParts1->GetBlock();
    CIoPropertyBlock*  pBlock2Ref = pIoParts2Ref->GetBlock();

    int iOut = 15;
    int iIn1  = 3;
    int iIn2  = 4;

    // Item2_0_0,  IT_AO_16
    int iIoIdObj1 = pBlock2Ref->GetIoId(0, 0);

    // Item_0_0,  IT_AI_16
    int iIoId1 = pBlock1->GetIoId(0, 0);


    pIoParts2Ref->SetInt( iOut, iIoIdObj1);
    iIn1 = pIoParts2Ref->GetInt( iIoIdObj1);
    iIn2 = pIoParts1->GetInt( iIoId1);
    STD_ASSERT(iIn1 == iIn2);


    iIn1 = 2;
    iIn2 = 5;
    pIoParts1->SetInt( iOut, pBlock1->GetIoId(0, 1));
    iIn1 = pIoParts2Ref->GetInt( pBlock2Ref->GetIoId(0, 1));
    iIn2 = pIoParts1->GetInt( pBlock1->GetIoId(0, 1));
    STD_ASSERT(iIn1 == iIn2);

    iIn1 = 2;
    iIn2 = 5;
    pIoParts2Ref->SetInt( iOut, pBlock2Ref->GetIoId(0, 2));
    iIn1 = pIoParts2Ref->GetInt( pBlock2Ref->GetIoId(0, 2));
    iIn2 = pIoParts1->GetInt( pBlock1->GetIoId(0, 2));
    STD_ASSERT(iIn1 == iIn2);

    StdString strOut;
    strOut = _T("Hello");
    pIoParts2Ref->SetStr( strOut, pBlock2Ref->GetIoId(0, 3));
    StdString strIn1 = pIoParts2Ref->GetStr( pBlock2Ref->GetIoId(0, 3));
    StdString strIn2 = pIoParts1->GetStr( pBlock1->GetIoId(0, 3));
    STD_ASSERT(strIn1 == StdString(_T("Hello")));
    STD_ASSERT(strIn1 == strOut);
    STD_ASSERT(strIn1 == strIn2);

    iIn1 = 2;
    iIn2 = 5;
    pIoParts2Ref->SetInt( iOut, pBlock2Ref->GetIoId(0, 4));
    iIn1 = pIoParts2Ref->GetInt( pBlock2Ref->GetIoId(0, 4));
    iIn2 = pIoParts1->GetInt( pBlock1->GetIoId(0, 4));
    STD_ASSERT(iIn1 == iIn2);

    double dIn1 = 123.45;
    double dIn2 = 321.23;
    double dOut = 2345.4;

    pIoParts2Ref->SetDouble( dOut, pBlock2Ref->GetIoId(1, 0));
    dIn1 = pIoParts2Ref->GetDouble( pBlock2Ref->GetIoId(1, 0));
    dIn2 = pIoParts1->GetDouble( pBlock1->GetIoId(1, 0));
    STD_ASSERT_DBL_EQ(dIn1, dIn2);

    iIn1 = 2;
    iIn2 = 5;
    pIoParts2Ref->SetInt( 1, pBlock2Ref->GetIoId(1, 2));
    iIn1 = pIoParts2Ref->GetInt( pBlock2Ref->GetIoId(1, 2));
    iIn2 = pIoParts1->GetInt( pBlock1->GetIoId(1, 2));
    STD_ASSERT(iIn1 == iIn2);
    STD_ASSERT(iIn1 == 1);

    iIn1 = 2;
    iIn2 = 5;
    pIoParts2Ref->SetInt( 0, pBlock2Ref->GetIoId(1, 7));
    iIn1 = pIoParts2Ref->GetInt( pBlock2Ref->GetIoId(1, 7));
    iIn2 = pIoParts1->GetInt( pBlock1->GetIoId(1, 7));
    STD_ASSERT(iIn1 == iIn2);
    STD_ASSERT(iIn1 == 0);

    iIn1 = 2;
    iIn2 = 5;
    pIoParts2Ref->SetInt( 1, pBlock2Ref->GetIoId(1, 8));
    iIn1 = pIoParts2Ref->GetInt( pBlock2Ref->GetIoId(1, 8));
    iIn2 = pIoParts1->GetInt( pBlock1->GetIoId(1, 8));
    STD_ASSERT(iIn1 == iIn2);
    STD_ASSERT(iIn1 == 1);

    //他のビットに影響がないか調べる
    pIoParts2Ref->SetInt( 1, pBlock2Ref->GetIoId(1, 7));
    iIn1 = pIoParts2Ref->GetInt( pBlock2Ref->GetIoId(1, 8));
    STD_ASSERT(iIn1 == 1);

    pIoParts2Ref->SetInt( 0, pBlock2Ref->GetIoId(1, 7));
    iIn1 = pIoParts2Ref->GetInt( pBlock2Ref->GetIoId(1, 8));
    STD_ASSERT(iIn1 == 1);

    // 未変換テスト END
    //=====================

}

void TEST_CIoRef()
{
   //using namespace TEST_IO_DEF_CHECK;

    //


    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    TEST_CIoRef_Convert();


    CProjectCtrl Proj;

    //----------------
    //!< 部品の作成
    //----------------
    //部品１
    Proj.CreateParts(_T("Parts1"), VIEW_COMMON::E_PARTS, -1);
    std::shared_ptr<CObjectDef> pParts1    = Proj.GetPartsByName(_T("Parts1")).lock();
    CIoDef*     pIoParts1  = pParts1->GetIoDef();
    TEST_CIoDef_SetData(*pIoParts1); 


    //部品２
    Proj.CreateParts(_T("Parts2"), VIEW_COMMON::E_PARTS, -1);
    std::shared_ptr<CObjectDef> pParts2    = Proj.GetPartsByName(_T("Parts2")).lock();
    CIoDef*     pIoParts2  = pParts2->GetIoDef();
    TEST_CIoRef_SetData(*pIoParts2); 



    //部品１の中に 部品２のオブジェクトを作成する
    CPartsDef* pCtrl1
        = dynamic_cast<CPartsDef*>(pParts1.get());

    auto pObj1 = pParts2->CreateInstance(pParts1.get());
    pCtrl1->RegisterObject(pObj1, true, false);

    //内部で pGroup->Create(pIoAccess, this);が呼び出される
    CIoRef* pIoParts2Ref = pObj1->GetIoRef();

    //----------------
    //-----------------
    //名称について確認
    //-----------------
    pObj1->SetName(_T("Obj1"));

    STD_ASSERT(pIoParts2Ref->GetName() == _T("Obj1"));
    STD_ASSERT(pIoParts2->GetName() == _T(""));





    //生成済みを確認
    STD_ASSERT(pIoParts2->IsCreated());
    STD_ASSERT(pIoParts2Ref->IsCreated());


    //-----------------
    //プロパティブロック
    //-----------------

    CIoPropertyBlock*  pBlock1 = pIoParts1->GetBlock();
    CIoPropertyBlock*  pBlock2 = pIoParts2->GetBlock();
    CIoPropertyBlock*  pBlock2Ref = pIoParts2Ref->GetBlock();

    //プロパティブロックが同一であることを確認
    STD_ASSERT(pBlock2 == pBlock2Ref);
    //STD_ASSERT(pParts2    == pIoParts2Ref->GetDef());
    


    int iRowMax;
    int iPageMax;

    iPageMax = pBlock1->GetPageMax();
    iRowMax  = pBlock1->GetRowMax(0);


    //----------
    //接続作成
    //----------
    //     Parts1            Parts2
    //   +--------------+   +--------------+
    //   |   Obj1       |   |              |
    //   |  +-------+   |   |              |
    //   |  |       |   |   |              |
    //   |  |       |<------|              |
    //   |  +-------+   |   |              |
    //   |              |   |              |
    //   +--------------+   +--------------+


    // Parts1自身のIOと接続を作成する

    //
    // CPartsDef "Parts2"  pCtrl  pIoDstDef
    //                                     CDrawingScriptBase   "Obj1"  pBase 
    // CPartsDef "Parts1"  CObjectDef* pDef2

    // Parts2                              Parts1
    //Page 0
    // 0 Item2_0_0,  IT_AO_16, 0,                Item_0_0,  IT_AI_16, 0,
    // 1 Item2_0_1,  IT_AI_8 , 0,                Item_0_1,  IT_AO_8 , 0,
    // 2 Item2_0_2,  IT_AO_32, 0,                Item_0_2,  IT_AI_32, 0,
    // 3 Item2_0_3,  IT_STR_O, 0,                Item_0_3,  IT_STR_I, 0,
    // 4 Item2_0_4,  IT_AO_16, 0,                Item_0_4,  IT_AI_16, 0,
                                                 
    //Page 1                                     
    // 0 Item2_1_0,     IT_DOUBLE_O              Item_1_0,  IT_DOUBLE_I,
    // 1 Item2_1_DO_0,  IT_DO                    Item_1_DI_0,  IT_DI,   
    // 2 Item2_1_DO_1,  IT_DO                    Item_1_DI_1,  IT_DI,   
    // 3 Item2_1_DO_2,  IT_DO                    Item_1_DI_2,  IT_DI,   
    // 4 Item2_1_DO_3,  IT_DO                    Item_1_DI_3,  IT_DI,   
    // 5 Item2_1_DO_4,  IT_DO                    Item_1_DI_4,  IT_DI,   
    // 6 Item2_1_DO_5,  IT_DO                    Item_1_DI_5,  IT_DI,   
    // 7 Item2_1_DO_6,  IT_DO                    Item_1_DI_6,  IT_DI,   
    // 8 Item2_1_DO_7,  IT_DO                    Item_1_DI_7,  IT_DI,   



    //-----------------
    // 接続追加
    //-----------------
    //接続は O -> Iに対して行われる
    //       O:1-n  I:1
    //接続を保持するのはInput側のみ


    int iIoId1;
    int iIoIdObj1;

    // Item2_0_0,  IT_AO_16
    iIoIdObj1 = pBlock2Ref->GetIoId(0, 0);

    // Item_0_0,  IT_AI_16
    iIoId1 = pBlock1->GetIoId(0, 0);


    CIoProperty* pPorpObj1 = pBlock2Ref->GetProperty(iIoIdObj1);
    CIoProperty* pPorp1    = pBlock1->GetProperty(iIoId1);


    bool bConnect;

    //pPorp1にpPorpObj1を接続  IT_AI_16 <- IT_AO_16
    bConnect = pPorp1->IsConnectable(pPorpObj1);
    STD_ASSERT(bConnect);


    std::shared_ptr<CIoConnect> spConnect = std::make_shared<CIoConnect>();
    spConnect->SetConnect(pIoParts2Ref, iIoIdObj1);


    bConnect = pIoParts1->AddConnect( iIoId1, spConnect);
    STD_ASSERT(bConnect);
    // 0 Item2_0_0,  IT_AO_16, 0,                Item_0_0,  IT_AI_16, 0,
    // 1 Item2_0_1,  IT_AI_8 , 0,                Item_0_1,  IT_AO_8 , 0,
    // 2 Item2_0_2,  IT_AO_32, 0,                Item_0_2,  IT_AI_32, 0,
    // 3 Item2_0_3,  IT_STR_O, 0,                Item_0_3,  IT_STR_I, 0,
    // 4 Item2_0_4,  IT_AO_16, 0,                Item_0_4,  IT_AI_16, 0,
                                                 

    iIoIdObj1 = pBlock2Ref->GetIoId(0, 1);
    iIoId1    = pBlock1->GetIoId(0, 1);
    STD_ASSERT(pIoParts2Ref->AddConnectById(    0, iIoId1, iIoIdObj1));  //IT_AI_32<-IT_AO_32

    int objId = pObj1->GetId();

    iIoIdObj1 = pBlock2Ref->GetIoId(0, 2);
    iIoId1    = pBlock1->GetIoId(0, 2);
    STD_ASSERT(pIoParts1->AddConnectById(objId, iIoIdObj1, iIoId1));  //Item_0_2 IT_AI_32<-IT_AO_32

    iIoIdObj1 = pBlock2Ref->GetIoId(0, 3);
    iIoId1    = pBlock1->GetIoId(0, 3);
    STD_ASSERT(pIoParts1->AddConnectById(objId, iIoIdObj1, iIoId1));  //Item_0_3 IT_STR_I<-IT_STR_O

    iIoIdObj1 = pBlock2Ref->GetIoId(0, 4);
    iIoId1    = pBlock1->GetIoId(0, 4);
    STD_ASSERT(pIoParts1->AddConnectById(objId, iIoIdObj1, iIoId1));  //Item_0_4 IT_AI_16<-IT_AO_16


    //Page 1                                     
    // 0 Item2_1_0,     IT_DOUBLE_O              Item_1_0,  IT_DOUBLE_I,
    // 1 Item2_1_DO_0,  IT_DO                    Item_1_DI_0,  IT_DI,   
    // 2 Item2_1_DO_1,  IT_DO                    Item_1_DI_1,  IT_DI,   
    // 3 Item2_1_DO_2,  IT_DO                    Item_1_DI_2,  IT_DI,   
    // 4 Item2_1_DO_3,  IT_DO                    Item_1_DI_3,  IT_DI,   
    // 5 Item2_1_DO_4,  IT_DO                    Item_1_DI_4,  IT_DI,   
    // 6 Item2_1_DO_5,  IT_DO                    Item_1_DI_5,  IT_DI,   
    // 7 Item2_1_DO_6,  IT_DO                    Item_1_DI_6,  IT_DI,   
    // 8 Item2_1_DO_7,  IT_DO                    Item_1_DI_7,  IT_DI,   

    STD_ASSERT(pIoParts1->AddConnectById(objId, pBlock2Ref->GetIoId(1, 0), 
                                                   pBlock1->GetIoId(1, 0)));  //Item_0_2 IT_AI_32<-IT_AO_32

    for (int iIoCnt = 1; iIoCnt < 9; iIoCnt++)
    {
        int iIdIn  = pBlock2Ref->GetIoId(1, iIoCnt);
        int iIdOut = pBlock1->GetIoId(1, iIoCnt);
        STD_ASSERT(pIoParts1->AddConnectById(objId, iIdIn, iIdOut));
    }

    for (int iIoCnt = 0; iIoCnt < 9; iIoCnt++)
    {
        STD_ASSERT(pIoParts1->AddConnectById(objId, pBlock2Ref->GetIoId(2, iIoCnt), 
                                                       pBlock1->GetIoId(2, iIoCnt)));
    }

    //----------------
    //
    //----------------
    bool bSetUp;
    bSetUp = pIoParts1->SetupStart();
    STD_ASSERT(bSetUp);

    bSetUp = pIoParts2Ref->SetupStart();
    STD_ASSERT(bSetUp);

    //アドレスキャッシュの生成
    bSetUp = pIoParts1->SetupAdress();
    STD_ASSERT(bSetUp);

    bSetUp = pIoParts2Ref->SetupAdress();
    STD_ASSERT(bSetUp);
    //----------------------

    TEST_CIoRef_CHK(pIoParts2Ref, pIoParts1);

    TEST_CIoRef_CHK_INIT(pIoParts2Ref, pIoParts1);

    //IOが正しくコピーされているか確認



    namespace fs = boost::filesystem;
    StdPath  pathTest = CSystem::GetSystemPath();
    pathTest /= _T("TEST_CIoRef.xml");

    //保存
    StdStreamOut outFs(pathTest);
    {
        StdXmlArchiveOut outXml(outFs);
        outXml << boost::serialization::make_nvp("Root", Proj);
    }
    outFs.close();



    CProjectCtrl Proj2;
    //読込
    StdStreamIn inFs(pathTest);
    {
        StdXmlArchiveIn inXml(inFs);
        inXml >> boost::serialization::make_nvp("Root", Proj2);
    }
    inFs.close();


    Proj2.LoadAfter();

    //部品１
    std::shared_ptr<CObjectDef> pProj2_Parts1 = Proj2.GetPartsByName(_T("Parts1")).lock();
    CIoDef*     pProj2_IoParts1  = pProj2_Parts1->GetIoDef();
    boost::uuids::uuid uuidParts1 = pProj2_Parts1->GetPartsId();
    
    //部品２
    std::shared_ptr<CObjectDef> pProj2_Parts2    = Proj2.GetPartsByName(_T("Parts2")).lock();
    CIoDef*     pProj2_IoParts2  = pParts2->GetIoDef();
    boost::uuids::uuid uuidParts2 = pProj2_Parts2->GetPartsId();

    CPartsDef* pProj2Ctrl = dynamic_cast<CPartsDef*>(pProj2_Parts1.get());
    STD_ASSERT(pProj2Ctrl);

    auto pProj2_Obj = pProj2Ctrl->GetObject(_T("Obj1"));
    STD_ASSERT(pProj2_Obj);

    auto  pProj2_Base = std::dynamic_pointer_cast<CDrawingScriptBase>(pProj2_Obj);
    STD_ASSERT(pProj2_Base);


    CIoRef* pProj2_IoParts2Ref = pProj2_Base->GetIoRef();



    bool bSetUp2;
    bSetUp2 = pProj2_IoParts1->SetupStart();
    STD_ASSERT(bSetUp2);

    bSetUp2 = pProj2_IoParts2Ref->SetupStart();
    STD_ASSERT(bSetUp2);

    //アドレスキャッシュの生成
    bSetUp2 = pProj2_IoParts1->SetupAdress();
    STD_ASSERT(bSetUp2);

    bSetUp2 = pProj2_IoParts2Ref->SetupAdress();
    STD_ASSERT(bSetUp2);
    //----------------------

    TEST_CIoRef_CHK(pProj2_IoParts2Ref, pProj2_IoParts1);

    TEST_CIoRef_CHK_INIT(pProj2_IoParts2Ref, pProj2_IoParts1);



    //!リストコントロール
    TEST_CIoRefDialog(pIoParts2Ref);


    //部品１
    Proj.DeleteParts(uuidParts1);

    //部品２
    Proj.DeleteParts(uuidParts2);





    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}
#endif //_DEBUG