/**
 * @brief			CIoRef ヘッダーファイル
 * @file			CIoRef.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

#ifndef  _IO_REF_H__
#define  _IO_REF_H__
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "Utility/VirtualTreeCtrl/VirtualCheckList.h"
#include "CIoCommon.h"
#include "IIoDef.h"

/*---------------------------------------------------*/
/*  Class                                            */
/*---------------------------------------------------*/
class CIoLine;
class CIoAccess;
class CIoDef;
class CIoProperty;
class CIoConnect;
class CIoPropertyBlock;
class CDrawingScriptBase;

/*---------------------------------------------------*/
/*  Enums                                            */
/*---------------------------------------------------*/

/*---------------------------------------------------*/
/*  Defines                                          */
/*---------------------------------------------------*/

/**
 * @class   CIoDef
 * @brief                       
 */
class CIoRef :public CIoDefBase
{
friend CIoRef;

public:

    //!< コンストラクタ
    CIoRef();

    //!< コピーコンストラクタ
    CIoRef(const  CIoRef&  Ref);

    //!< デストラクタ
    virtual ~CIoRef();


    //!<----------------------
    //!<  初期化処理
    //!<----------------------
    //!< 生成処理
    bool Create(CDrawingScriptBase* pBase,
                std::shared_ptr<CIoAccess>  pIoAccess);

    //!<----------------------
    //!<  実行前準備
    //!<----------------------
    //!< 実行前準備
    virtual bool SetupStart();


    //!<----------------------
    //!<  プロパティ
    //!<----------------------
    //!< プロパティブロック取得 
    virtual CIoPropertyBlock*  GetBlock() const;

    //!< 名称取得
    virtual StdString GetName() const;

    //!< オブジェクト定義取得
    //virtual CObjectDef* GetDef() const;

    //!< IDによる接続追加
    virtual bool AddConnectById(int iIdFrom, int iIoIdFrom, int iIoIdTo);
    
    //!< 描画領域取得
    virtual CPartsDef* GetPartsDef() const;
    //!<----------------------
    //!<  実行処理
    //!<----------------------
 
    //!< 停止設定
    virtual bool Stop();

    //!< 読込後初期化処理
    virtual bool LoadAfter();


protected:
    friend CIoConnect;
    friend CDrawingScriptBase;

    //!< 親オブジェクト設定
    void SetParent(CDrawingScriptBase* pParent);

    //!< 親オブジェクト取得
    CDrawingScriptBase* GetParent();

    //

protected:
    //!<----------------
    //!< 保存データ
    //!<----------------

    //!< 親データID
    //int m_iIdParent;

    //!<----------------
    //!< 非保存データ
    //!<----------------

    //!< プロパティ
    std::weak_ptr<CIoPropertyBlock>  m_wpPropety;

    //!< 参照データ
    CIoDef*                            m_pRef;

    //!< 親オブジェクト
    CDrawingScriptBase*                m_pParent;  //CDrawingScriptBase::Loadで設定

    //!< ロード終了後設定フラグ
    bool                               m_bLoadAfter;

private:

    friend class boost::serialization::access;  
    BOOST_SERIALIZATION_SPLIT_MEMBER();

    template<class Archive>
    void load(Archive& ar, const unsigned int version);

    template<class Archive>
    void save(Archive& ar, const unsigned int version) const;
};

#endif  //_IO_REF_H__
