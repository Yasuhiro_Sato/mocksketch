/**
 * @brief			IIoDef実装ファイル
 * @file			IIoDef.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./IIoDef.h"
#include "./CIoPropertyBlock.h"
#include "./CIoProperty.h"
#include "./CIoConnect.h"
#include "./CIoAccess.h"
#include "./CIoDef.h"
#include "./CIoRef.h"
#include "./CIoProperty.h"
#include "Utility/CUtility.h"
#include "DefinitionObject/ObjectDef.h"
#include "System/CSystem.h"
#include "resource.h"
#include "resource2.h"
#include "SubWindow/IoConnectDlg.h"

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CIoDefBase::CIoDefBase():
m_pBlock        (NULL),
m_eDispIo       (IO_COMMON::LT_NONE),
m_iPage         (0),
m_iChgCnt       (1),
m_pVirtualList  (NULL)
{
}


/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CIoDefBase::CIoDefBase(const  CIoDefBase&  def):
m_iPage         (0),
m_iChgCnt       (1),
m_pVirtualList  (NULL)
{
    m_eDispIo = def.m_eDispIo;
}

/**
 *  @brief  デストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CIoDefBase::~CIoDefBase()
{
}


/**
 *  @brief   代入演算子
 *  @param   [in] obj 代入元 
 *  @retval  代入値
 *  @note 
 */
CIoDefBase& CIoDefBase::operator = (const CIoDefBase & obj)
{
    m_iPage = 0;
    m_pBlock = NULL;
    m_mapAddress.clear();

    m_eDispIo  = obj.m_eDispIo;

    m_psAccess = CIoAccess::Clone(obj.m_psAccess.get());

    std::map< int, std::shared_ptr<CIoConnect> >::const_iterator ite;
    
    m_mapIdConnect.clear();
    for (ite  = obj.m_mapIdConnect.begin();
         ite != obj.m_mapIdConnect.end();
         ite++)
    {
        m_mapIdConnect[ite->first] = 
            std::make_shared<CIoConnect>(*ite->second.get());
    }

    return *this;
}

/**
 * @brief   等価演算子
 * @param   [in] obj 値
 * @return  ture 同値
 * @note    
 */
bool CIoDefBase::operator == (const CIoDefBase & obj) const
{
    if (*m_psAccess.get() != *obj.m_psAccess.get()){return false;}

    if (m_mapIdConnect.size() != obj.m_mapIdConnect.size())
    {
        return false;
    }

    CIoConnect* pConnect;
    int iIoId;
    std::map< int, std::shared_ptr<CIoConnect> >::const_iterator ite;
    std::map< int, std::shared_ptr<CIoConnect> >::const_iterator iteSrc;


    for (ite  = obj.m_mapIdConnect.begin();
         ite != obj.m_mapIdConnect.end();
         ite++)
    {
        iIoId = ite->first;
        iteSrc = m_mapIdConnect.find(iIoId);
        if (iteSrc == m_mapIdConnect.end())
        {
            return false;
        }

        pConnect = iteSrc->second.get();
        if (*pConnect != *ite->second.get())
        {
            return false;
        }
    }
    return true;
}

/**
 * @brief   不等価演算子
 * @param   [in] obj 値
 * @return  false 同値
 * @note    
 */
bool CIoDefBase::operator != (const CIoDefBase & obj) const
{
    return !(*this == obj);
}


/**
 *  @brief   接続先取得
 *  @param   [in] iIoId 
 *  @retval  接続先
 *  @note
 */
std::weak_ptr<CIoConnect> CIoDefBase::GetConnect(int iIoId)
{
    std::weak_ptr<CIoConnect> pRet;

    std::map< int, std::shared_ptr<CIoConnect> >::iterator iteFind;

    iteFind = m_mapIdConnect.find(iIoId);

    if (iteFind != m_mapIdConnect.end())
    {
        pRet = iteFind->second;
    }

    return pRet;
}


/**
 *  @brief  接続確認
 *  @param  なし
 *  @retval true:成功
 *  @note   
 */
bool CIoDefBase::VaridateConnection()
{
    //TODO:Test
    //接続先の検証
    std::set<int> lstDel;
    int         iId;
    CIoConnect* pConnect;
    CIoProperty* pPorpB;
    CIoProperty* pPorpC;
    bool bDelete;

    std::map< int, std::shared_ptr<CIoConnect> >::const_iterator iteMap;
    for(iteMap  = m_mapIdConnect.begin();
        iteMap != m_mapIdConnect.end();
        iteMap++)
    {
        iId      = iteMap->first;
        pConnect = iteMap->second.get();

        bDelete = false; 

        if (pConnect == NULL)
        {
            bDelete = true;
        }
        else
        {
            pPorpB = m_pBlock->GetProperty(iId);
            pPorpC = pConnect->GetConnect();
            if(!pPorpB->IsConnectable(pPorpC))
            {
                bDelete = true;
            }
        }

        if (bDelete)
        {
            lstDel.insert(iId);
        }
    }

    std::map< int, std::shared_ptr<CIoConnect> >::const_iterator iteFind;

    foreach(int iDelId, lstDel)
    {
        iteFind = m_mapIdConnect.find(iDelId);
        m_mapIdConnect.erase(iteFind); 
    }
    return true;
}

/**
 *  @brief  アドレス設定
 *  @param  なし
 *  @retval true:成功 
 *  @note   実行前に全てのオブジェクトでSetupStart()を
 *          おこなっておく必要がある。
 */
bool CIoDefBase::SetupAdress()
{
    using namespace IO_COMMON;
    //-----------------------
    //アドレスキャッシュ生成
    //-----------------------
    int iPageMax = m_pBlock->GetPageMax();
    int iRowMax;
    int iRow;
    int iIoId;

    CIoProperty*    pProp;
    CIoProperty*    pPropOut;
    CIoConnect*     pConnect;
    m_mapAddress.clear();
    StdString strInit;

    std::map< int, std::shared_ptr<CIoConnect> >::iterator itefind;
    bool bInit;
    CONNECT_OUTPUT out;

    for (int iPage = 0; iPage < iPageMax; iPage++)
    {
        bInit = true;
        iRowMax = m_pBlock->GetRowMax(iPage);
        for (iRow = 0; iRow < iRowMax; iRow++)
        {
            iIoId = m_pBlock->GetIoId(iPage, iRow);

            //-------------
            // 初期値設定
            //-------------
            // 接続があって入力の場合は初期値を設定しない

            pProp = m_pBlock->GetProperty(iIoId);
            STD_ASSERT(pProp);
            double dBitIn;
            double dBitOut;

            out.pAdress = GetSetupAddress(iIoId);
            out.dFactor = 1.0;
            out.eType   = pProp->GetType();

            itefind = m_mapIdConnect.find(iIoId);
            if (itefind != m_mapIdConnect.end())
            {
                if(pProp->GetDir() == IO_COMMON::IO_IN)
                {
                    //Out->Inに変換するための係数を計算する
                    if ((pProp->GetType() == DAT_UCHAR) ||
                        (pProp->GetType() == DAT_USHORT)||
                        (pProp->GetType() == DAT_ULONG))
                    {
                        pConnect = itefind->second.get();
                        pPropOut = pConnect->GetConnect();
                        dBitIn  = pow(2.0, static_cast<double>(pProp->GetBit())) -1;
                        dBitOut = pow(2.0, static_cast<double>(pPropOut->GetBit())) -1;
                        out.dFactor = dBitIn / dBitOut;
                        out.eType = pPropOut->GetType();
                    }
                    bInit = false;
                }
            }

            m_mapAddress[iIoId] = out;

            strInit = pProp->GetInit();
            if (strInit != _T(""))
            {
                if (bInit)
                {
                    SetStr(strInit, iIoId);
                }
            }
        }
    }
    return true;
}


/**
 *  @brief  実行前準備
 *  @param  なし
 *  @retval  true:成功 
 *  @note
 */
bool CIoDefBase::SetupStart()
{
    m_pBlock = GetBlock();
    if (!m_pBlock)
    {
        return false;
    }

    m_pBlock->SetupStart();

    //IoAccessの設定
    STD_ASSERT(m_psAccess.get() != NULL);
    if (m_psAccess.get() == NULL)
    {
        return false;
    }

    if(!m_psAccess->SetupStart())
    {
        return false;
    }

    if (!VaridateConnection())
    {
        return false;
    }

    return true;
}

/**
 *  @brief  列数取得
 *  @param  なし
 *  @retval  true:成功 
 *  @note
 */
int CIoDefBase::GetColNum() const
{
    using namespace IO_COMMON;
    int iColMax = COL_PROPERTY_MAX - 1;

    if (m_psAccess)
    {
        iColMax += m_psAccess->GetUserItemNumber();
    }

    iColMax += COL_CONNECT_MAX;

    return iColMax;
}


/**
 *  @brief  設定用IOアドレス取得
 *  @param  [in] iIoId
 *  @retval アドレス 
 *  @note 
 */
char* CIoDefBase::GetSetupAddress(int iIoId)
{
    std::map< int, std::shared_ptr<CIoConnect> >::iterator itefind;
    
    itefind = m_mapIdConnect.find(iIoId);
    if (itefind == m_mapIdConnect.end())
    {
        //接続が存在しない場合は時バッファーのアドレスを返す
        return m_psAccess->GetBufAddress(iIoId);
    }

    CIoConnect* pConnect = itefind->second.get();

    if (pConnect == NULL)
    {
        return NULL;   
    }

    //接続先のアドレスを取得する
    CIoDefBase* pIo = pConnect->GetConnectParent();
    int iParentIoId = pConnect->m_iConnctIoId;

    return pIo->GetSetupAddress(iParentIoId);
}

/**
 *  @brief  変換前数値
 *  @param  [out] iIoId
 *  @param  [in] iIoId
 *  @param  [in] iIoId
 *  @retval 変換前の数値
 *  @note
 */
UniVal* CIoDefBase::GetPreConvertVal(double* pFactor, 
                                     IO_COMMON::DAT_TYPE* pType,
                                     int iIoId) const
{

    using namespace boost;
    std::map<int, CONNECT_OUTPUT >::const_iterator iteMap;

    CONNECT_OUTPUT out;
    out.dFactor = 1.0;
    out.eType   = IO_COMMON::DAT_NONE;
    out.pAdress = NULL;

    iteMap = m_mapAddress.find(iIoId);

    if (iteMap != m_mapAddress.end())
    {
        out = iteMap->second;
    }

    *pFactor  = out.dFactor;
    *pType    = out.eType;

    return reinterpret_cast<UniVal*>(out.pAdress);
}

/**
 *  @brief  変換前符号なし整数値取得
 *  @param  [in] iIoId
 *  @param  [in] bConvert  変換有無
 *  @retval 符号なし整数値
 *  @note
 */
UINT CIoDefBase::GetUint(int iIoId, bool bConvert) const
{
    using namespace IO_COMMON;
    std::stringstream   strmRet;

    UniVal* pUni;
    double  dFactor;
    IO_COMMON::DAT_TYPE eType;
    pUni = GetPreConvertVal(&dFactor, &eType, iIoId);

    CIoProperty* pProp;
    pProp = m_pBlock->GetProperty(iIoId);

    if (!pProp)
    {
        return 0;
    }


    int   iOffset;
    UINT  uiVal;
    bool bBit;
    bool bVal = true;

    switch(eType)
    {

    case DAT_BIT:
        iOffset = pProp->GetOffset();
        bBit = (((0x1 << iOffset) & pUni->ucVal) == 1); 
        (bBit ? uiVal = 1 :  uiVal = 0);
        break;

    case DAT_CHAR:
        uiVal =  pUni->cVal;
        break;

    case DAT_UCHAR:
        uiVal = CUtil::Round(pUni->ucVal * dFactor);
        break;

    case DAT_SHORT:
        uiVal =  pUni->sVal;
        break;

    case DAT_USHORT:
        uiVal =  CUtil::Round(pUni->usVal  * dFactor);
        break;

    case DAT_LONG:
        uiVal =  pUni->uiVal;
        break;

    case DAT_ULONG:
        uiVal =  CUtil::Round(pUni->uiVal * dFactor);
        break;

    case DAT_FLOAT:
        uiVal =  CUtil::Round(pUni->fVal);
        break;

    case DAT_DOUBLE:
        uiVal =  CUtil::Round(pUni->dVal);
        break;

    case DAT_STR:
        bVal = false;
        break;

    default:
        bVal = false;
        break;

    }

    if (!bVal)
    {
        return 0;
    }

    if (bConvert)
    {
        if (pProp->GetInterpolation() != IP_NONE)
        {
            double dRet = pProp->GetConvVal(uiVal);
            uiVal =  CUtil::Round(dRet);
        }
    }
    return uiVal;
}

/**
 *  @brief  整数値取得
 *  @param  [in] iIoId
 *  @param  [in] bConvert  変換有無
 *  @retval 整数値
 *  @note
 */
int CIoDefBase::GetInt(int iIoId, bool bConvert) const
{
    using namespace IO_COMMON;
    std::stringstream   strmRet;

    UniVal* pUni;
    double  dFactor;
    IO_COMMON::DAT_TYPE eType;
    pUni = GetPreConvertVal(&dFactor, &eType, iIoId);

    CIoProperty* pProp;
    pProp = m_pBlock->GetProperty(iIoId);

    if (!pProp)
    {
        return 0;
    }


    int   iOffset;
    int  iVal;
    bool bBit;
    bool bVal = true;

    switch(eType)
    {

    case DAT_BIT:
        iOffset = pProp->GetOffset();
        bBit = (((0x1 << iOffset) & pUni->ucVal) == 1);
        (bBit ? iVal = 1 :  iVal = 0);
        break;

    case DAT_CHAR:
        iVal =  pUni->cVal;
        break;

    case DAT_UCHAR:
        iVal =  CUtil::Round(pUni->ucVal * dFactor);
        break;

    case DAT_SHORT:
        iVal =  pUni->sVal;
        break;

    case DAT_USHORT:
        iVal =  CUtil::Round(pUni->usVal * dFactor);
        break;

    case DAT_LONG:
        iVal =  pUni->iVal;
        break;

    case DAT_ULONG:
        iVal =  CUtil::Round(pUni->uiVal * dFactor);
        break;

    case DAT_FLOAT:
        iVal =  CUtil::Round(pUni->fVal);
        break;

    case DAT_DOUBLE:
        iVal =  CUtil::Round(pUni->dVal);
        break;

    case DAT_STR:
        bVal = false;
        break;

    default:
        bVal = false;
        break;

    }

    if (!bVal)
    {
        return 0;
    }

    if (bConvert)
    {
        if (pProp->GetInterpolation() != IP_NONE)
        {
            double dRet = pProp->GetConvVal(iVal);
            iVal =  CUtil::Round(dRet);
        }
    }
    return iVal;
}


/**
 *  @brief  変換前文字列
 *  @param  [in] iIoId
 *  @retval 変換前のデータ
 *  @note
 */
StdString   CIoDefBase::GetPreConvertStr(int iIoId) const
{
    using namespace IO_COMMON;
    StdStringStream   strmRet;


    UniVal* pUni;
    double  dFactor;
    IO_COMMON::DAT_TYPE eType;
    pUni = GetPreConvertVal(&dFactor, &eType, iIoId);

    if (!pUni)
    {
        return _T("");
    }

    CIoProperty* pProp;
    pProp = m_pBlock->GetProperty(iIoId);

    if (!pProp)
    {
        return _T("");
    }

    int  iOffset;
    bool bBit;
    switch(eType)
    {

    case DAT_BIT:
        iOffset = pProp->GetOffset();
        bBit = (((0x1 << iOffset) & pUni->ucVal) == 1);
        if (bBit) { strmRet << _T("ON"); }
        else      {strmRet << _T("OFF");}
        break;

    case DAT_CHAR:
        strmRet << StdFormat(_T("%d")) % pUni->cVal;
        break;

    case DAT_UCHAR:
        strmRet << StdFormat(_T("%d")) % int(pUni->ucVal * dFactor);
        break;

    case DAT_USHORT:
        strmRet << StdFormat(_T("%d")) % int(pUni->usVal * dFactor);
        break;

    case DAT_SHORT:
        strmRet << StdFormat(_T("%d")) % pUni->sVal;
        break;

    case DAT_ULONG:
        strmRet << StdFormat(_T("%d")) % pUni->uiVal;
        break;

    case DAT_LONG:
        strmRet << StdFormat(_T("%d")) % pUni->iVal;
        break;

    case DAT_FLOAT:
        strmRet << StdFormat(_T("%f")) % pUni->fVal;
        break;

    case DAT_DOUBLE:
        strmRet << StdFormat(_T("%f")) % pUni->dVal;
        break;

    case DAT_STR:
        strmRet << StdFormat(_T("%s")) % &pUni->strVal;
        break;
    }

    strmRet << std::endl;
    return strmRet.str();
}

/**
 *  @brief  文字列取得
 *  @param  [in] iIoId
 *  @param  [in] bConvert  変換有無
 *  @retval 文字列
 *  @note
 */
StdString CIoDefBase::GetStr(int iIoId, bool bConvert) const
{

    if (!bConvert)
    {
        return GetPreConvertStr(iIoId);
    }

    using namespace IO_COMMON;
    StdStringStream   strmRet;

    UniVal* pUni;
    double  dFactor;
    IO_COMMON::DAT_TYPE eType;
    pUni = GetPreConvertVal(&dFactor, &eType, iIoId);

    if (!pUni)
    {
        return _T("");
    }

    CIoProperty* pProp;
    pProp = m_pBlock->GetProperty(iIoId);

    if (!pProp)
    {
        return _T("");;
    }

    int     iOffset;
    double  dVal;
    bool    bBit;
    bool    bVal = true;

    switch(eType)
    {

    case DAT_BIT:
        iOffset = pProp->GetOffset();
        bBit = (((0x1 << iOffset) & pUni->ucVal) == 1);
        if (bBit) {strmRet << _T("ON"); }
        else      {strmRet << _T("OFF");}
        bVal = false;
        break;

    case DAT_CHAR:
        dVal = pUni->cVal;
        break;

    case DAT_UCHAR:
        dVal =  pUni->ucVal;
        break;

    case DAT_USHORT:
        dVal =  pUni->usVal;
        break;

    case DAT_SHORT:
        dVal =  pUni->sVal;
        break;

    case DAT_ULONG:
        dVal =  pUni->uiVal;
        break;

    case DAT_LONG:
        dVal =  pUni->iVal;
        break;

    case DAT_FLOAT:
        dVal =  pUni->fVal;
        break;

    case DAT_DOUBLE:
        dVal =  pUni->dVal;
        break;

    case DAT_STR:
        strmRet << StdFormat(_T("%s")) % &pUni->strVal;
        bVal = false;
        break;

    default:
        bVal = false;
        break;

    }

    if (bVal)
    {
        double dRet = pProp->GetConvVal(dVal);
        strmRet << StdFormat(_T("%f")) % dRet;
    }

    return strmRet.str();
}


/**
 *  @brief  実数値取得
 *  @param  [in] iIoId
 *  @param  [in] bConvert  変換有無
 *  @retval 実数値
 *  @note
 */
double CIoDefBase::GetDouble(int iIoId, bool bConvert) const
{
    using namespace IO_COMMON;
    StdStringStream   strmRet;

    UniVal* pUni;
    double  dFactor;
    IO_COMMON::DAT_TYPE eType;
    pUni = GetPreConvertVal(&dFactor, &eType, iIoId);

    CIoProperty* pProp;
    pProp = m_pBlock->GetProperty(iIoId);

    if (!pProp)
    {
        return 0.0;
    }


    int     iOffset;
    double  dVal;
    bool    bBit;

    switch(eType)
    {

    case DAT_BIT:
        iOffset = pProp->GetOffset();
        bBit = (((0x1 << iOffset) & pUni->ucVal) == 1);
        if (bBit) { dVal = 1.0; }
        else      {dVal = 0.0; }
        break;

    case DAT_CHAR:
        dVal = pUni->cVal;
        break;

    case DAT_UCHAR:
        dVal =  pUni->ucVal;
        break;

    case DAT_USHORT:
        dVal =  pUni->usVal;
        break;

    case DAT_SHORT:
        dVal =  pUni->sVal;
        break;

    case DAT_ULONG:
        dVal =  pUni->uiVal;
        break;

    case DAT_LONG:
        dVal =  pUni->iVal;
        break;

    case DAT_FLOAT:
        dVal =  pUni->fVal;
        break;

    case DAT_DOUBLE:
        dVal =  pUni->dVal;
        break;

    case DAT_STR:
        dVal = _tstof(&pUni->strVal);
        break;

    default:
        dVal = 0.0;
        break;
    }


    if (bConvert)
    {
        double dRet = pProp->GetConvVal(dVal);
        return dRet;
    }

    return dVal;
}

/**
 *  @brief  接続名取得
 *  @param  [in] iIoId
 *  @retval 接続名
 *  @note
 */
StdString   CIoDefBase::GetConnectName    (int iIoId) const
{
    std::map< int, std::shared_ptr<CIoConnect> >::const_iterator iteFind;

    StdString  strRet;
    iteFind = m_mapIdConnect.find(iIoId);
    if (iteFind != m_mapIdConnect.end())
    {
        strRet = iteFind->second->GetConnectStr();
    }

    return strRet;
}


/**
 *  @brief  生成確認
 *  @param  なし
 *  @retval true 生成済み
 *  @note
 */
bool CIoDefBase::IsCreated() const
{
    if (!m_psAccess)
    {
        return false;
    }
    return true;
}


/**
 *  @brief  ページ数取得
 *  @param  なし
 *  @retval ページ数
 *  @note
 */
int CIoDefBase::GetMaxPage()
{
    if (!m_psAccess)
    {
        return 0;
    }
   return m_psAccess->GetMaxPage();
}



//!<----------------------
//!<  値設定
//!<----------------------
/**
 *  @brief  文字列設定.
 *  @param  [in] strVal  設定値
 *  @param  [in] iIoId ID値
 *  @retval true 設定成功
 *  @note   
 */
bool CIoDefBase::SetStr(StdString  strVal, int iIoId)
{
    using namespace IO_COMMON;
    StdStringStream   strmRet;

    UniVal* pUni;
    double  dFactor;
    IO_COMMON::DAT_TYPE eType;
    pUni = GetPreConvertVal(&dFactor, &eType, iIoId);

    CIoProperty* pProp;
    pProp = m_pBlock->GetProperty(iIoId);

    if (!pProp)
    {
        return false;
    }

    int     iOffset;
    bool    bOn = false;
    bool    bRet = true;
    double  dVal;
    long    lVal;
    unsigned long ulVal;

    E_INTERPOLATION eInterpolation;
    eInterpolation = pProp->GetInterpolation();
    long              lConvVal = 0;
    double            dConvVal = 0.0;

    switch(eType)
    {

    case DAT_BIT:

        if (strVal == _T("ON"))
        {
            bOn = true;
        }
        else
        {
            if (_tstol(strVal.c_str()) != 0)
            {
                bOn = true;
            }
        }

        iOffset = pProp->GetOffset();

        if (bOn)
        {
            pUni->ucVal = (pUni->ucVal | (0x1 << iOffset));
        }
        else
        {
            pUni->ucVal = (pUni->ucVal & (~(0x1 << iOffset)));
        }
        break;

    case DAT_CHAR:
    case DAT_UCHAR:
    case DAT_USHORT:
    case DAT_SHORT:
    case DAT_LONG:

        lVal = _tstol(strVal.c_str());
        if (eInterpolation == IP_NONE)
        {
            lConvVal = lVal;
        }
        else 
        {
            bRet = pProp->GetInvVal(&dVal, static_cast<double>(lVal));
            lConvVal = CUtil::Round(dVal);
        }
        break;

    case DAT_ULONG:
        ulVal = _tcstoul(strVal.c_str(), 0, 0);

        if (eInterpolation == IP_NONE)
        {
            pUni->uiVal = ulVal;
        }
        else 
        {
            bRet = pProp->GetInvVal(&dVal, static_cast<double>(ulVal));
            if (dVal > ULONG_MAX)
            {
                pUni->uiVal = ULONG_MAX;
            }
            else if (dVal < 0)
            {
                pUni->uiVal = 0;
            }
            else
            {
                pUni->uiVal = CUtil::Round(dVal);
            }
        }
        break;


    case DAT_FLOAT:
    case DAT_DOUBLE:
        dVal = _tstof(strVal.c_str());

        if (eInterpolation == IP_NONE)
        {
            dConvVal = dVal;
        }
        else 
        {
            bRet = pProp->GetInvVal(&dConvVal, dVal);
        }
        break;

    case DAT_STR:
        {
            errno_t errNo;
            int iLen = pProp->GetLen();
            if (iLen <= 0)
            {
                bRet = false;
            }
            else
            {
                // バッファーは文字長＋１の長さを確保している
                // CIoPropertyBlock::RecalcAddress参照
                errNo = _tcscpy_s(&pUni->strVal, iLen + 1, strVal.c_str());
                if (errNo != 0)
                {
                    bRet = false;
                }
            }
        }
        break;

    default:
        bRet = false;
        break;
    }

    switch(eType)
    {

    case DAT_CHAR:
        if (lConvVal > SCHAR_MAX)
        {
            pUni->cVal = SCHAR_MAX;
        }
        else if (lConvVal < SCHAR_MIN)
        {
            pUni->cVal = SCHAR_MIN;
        }
        else
        {
            pUni->cVal = static_cast<char>(lConvVal);
        }
        break;

    case DAT_UCHAR:
        if (lConvVal > UCHAR_MAX)
        {
            pUni->ucVal = UCHAR_MAX;
        }
        else if (lConvVal < 0)
        {
            pUni->ucVal = 0;
        }
        else
        {
            pUni->ucVal = static_cast<unsigned char>(lConvVal);
        }
        break;

    case DAT_USHORT:
        if (lConvVal > USHRT_MAX)
        {
            pUni->usVal = USHRT_MAX;
        }
        else if (lConvVal < 0)
        {
            pUni->usVal = 0;
        }
        else
        {
            pUni->usVal = static_cast<unsigned short>(lConvVal);
        }
        break;

    case DAT_SHORT:
        if (lConvVal > SHRT_MAX)
        {
            pUni->sVal = SHRT_MAX;
        }
        else if (lConvVal < SHRT_MIN)
        {
            pUni->sVal = SHRT_MIN;
        }
        else
        {
            pUni->sVal = static_cast<short>(lConvVal);
        }
        break;

    case DAT_LONG:
        pUni->iVal = lConvVal;
        break;

    case DAT_FLOAT:
        pUni->fVal = static_cast<float>(dConvVal);
        break;

    case DAT_DOUBLE:
        pUni->dVal = dConvVal;
        break;
      
    }
    return bRet;
}

/**
 *  @brief  実数値設定.
 *  @param  [in] dVal  設定値
 *  @param  [in] iIoId ID値
 *  @retval true 設定成功
 *  @note   
 */
bool CIoDefBase::SetDouble   (double dVal, int iIoId)
{
    using namespace IO_COMMON;
    StdStringStream   strmRet;

    CIoProperty* pProp;
    pProp = m_pBlock->GetProperty(iIoId);

    UniVal* pUni;
    double  dFactor;
    IO_COMMON::DAT_TYPE eType;
    pUni = GetPreConvertVal(&dFactor, &eType, iIoId);

    if (!pProp)
    {
        return false;
    }

    bool bRet = false;

    double dConvVal;
    long   lConvVal;
    long   iOffset;

    E_INTERPOLATION eInterpolation;
    eInterpolation = pProp->GetInterpolation();

    if (eInterpolation == IP_NONE)
    {
        lConvVal = CUtil::Round(dVal);
        dConvVal = dVal;
    }
    else 
    {
        pProp->GetInvVal(&dConvVal, dVal);
        lConvVal = CUtil::Round(dConvVal);
    }
    


    switch(eType)
    {

    case DAT_BIT:
        

        iOffset  = pProp->GetOffset();

        if (lConvVal)
        {
            pUni->ucVal = (pUni->ucVal | (0x1 << iOffset));
        }
        else
        {
            pUni->ucVal = (pUni->ucVal & (~(0x1 << iOffset)));
        }

        break;

    case DAT_CHAR:
        if (lConvVal > SCHAR_MAX)
        {
            pUni->cVal = SCHAR_MAX;
        }
        else if (lConvVal < SCHAR_MIN)
        {
            pUni->cVal = SCHAR_MIN;
        }
        else
        {
            pUni->cVal = static_cast<char>(lConvVal);
        }
        break;

    case DAT_UCHAR:
        if (lConvVal > UCHAR_MAX)
        {
            pUni->ucVal = UCHAR_MAX;
        }
        else
        {
            pUni->ucVal = static_cast<unsigned char>(lConvVal);
        }
        break;

    case DAT_USHORT:
        if (lConvVal > USHRT_MAX)
        {
            pUni->usVal = USHRT_MAX;
        }
        else
        {
            pUni->usVal = static_cast<unsigned short>(lConvVal);
        }
        break;

    case DAT_SHORT:
        if (lConvVal > SHRT_MAX)
        {
            pUni->sVal = SHRT_MAX;
        }
        else if (lConvVal < SHRT_MIN)
        {
            pUni->sVal = SHRT_MIN;
        }
        else
        {
            pUni->sVal = static_cast<short>(lConvVal);
        }
        break;

    case DAT_ULONG:
        if (lConvVal < 0)
        {
            pUni->uiVal = 0;
        }
        else
        {
            pUni->uiVal = lConvVal;
        }
        break;

    case DAT_LONG:
        pUni->iVal = lConvVal;
        break;

    case DAT_FLOAT:
        pUni->fVal = static_cast<float>(lConvVal);
        break;

    case DAT_DOUBLE:
        pUni->dVal = static_cast<double>(lConvVal);
        break;

    case DAT_STR:
        pUni->strVal;
        break;

    default:

        break;
    }
    return true;
}


/**
 *  @brief  符号なし整数値設定.
 *  @param  [in] uiVal  設定値
 *  @param  [in] iIoId ID値
 *  @retval true 設定成功
 *  @note   
 */
bool CIoDefBase::SetUint(UINT  uiVal, int iIoId)
{
    using namespace IO_COMMON;
    StdStringStream   strmRet;

    CIoProperty* pProp;
    pProp = m_pBlock->GetProperty(iIoId);

    UniVal* pUni;
    double  dFactor;
    IO_COMMON::DAT_TYPE eType;
    pUni = GetPreConvertVal(&dFactor, &eType, iIoId);

    if (!pProp)
    {
        return false;
    }

    bool bRet = false;

    double dConvVal;
    ULONG  ulConvVal;
    long   iOffset;

    E_INTERPOLATION eInterpolation;
    eInterpolation = pProp->GetInterpolation();

    if (eInterpolation == IP_NONE)
    {
        ulConvVal = uiVal;
    }
    else 
    {
        pProp->GetInvVal(&dConvVal, static_cast<double>(uiVal));
        ulConvVal = static_cast<unsigned long>(dConvVal);
    }
    
    switch(eType)
    {

    case DAT_BIT:
        

        iOffset  = pProp->GetOffset();

        if (ulConvVal)
        {
            pUni->ucVal = (pUni->ucVal | (0x1 << iOffset));
        }
        else
        {
            pUni->ucVal = (pUni->ucVal & (~(0x1 << iOffset)));
        }

        break;

    case DAT_CHAR:
        if (ulConvVal > SCHAR_MAX)
        {
            pUni->cVal = SCHAR_MAX;
        }
        else
        {
            pUni->cVal = static_cast<char>(ulConvVal);
        }
        break;

    case DAT_UCHAR:
        if (ulConvVal > UCHAR_MAX)
        {
            pUni->ucVal = UCHAR_MAX;
        }
        else
        {
            pUni->ucVal = static_cast<unsigned char>(ulConvVal);
        }
        break;

    case DAT_USHORT:
        if (ulConvVal > USHRT_MAX)
        {
            pUni->usVal = USHRT_MAX;
        }
        else
        {
            pUni->usVal = static_cast<unsigned short>(ulConvVal);
        }
        break;

    case DAT_SHORT:
        if (ulConvVal > SHRT_MAX)
        {
            pUni->sVal = SHRT_MAX;
        }
        else
        {
            pUni->sVal = static_cast<short>(ulConvVal);
        }
        break;

    case DAT_ULONG:
        if (ulConvVal < 0)
        {
            pUni->uiVal = 0;
        }
        else
        {
            pUni->uiVal = ulConvVal;
        }
        break;

    case DAT_LONG:
        if (ulConvVal > INT_MAX)
        {
            pUni->iVal = INT_MAX;
        }
        else
        {
            pUni->iVal = ulConvVal;
        }
        break;

    case DAT_FLOAT:
        pUni->fVal = static_cast<float>(ulConvVal);
        break;

    case DAT_DOUBLE:
        pUni->dVal = static_cast<double>(ulConvVal);
        break;

    case DAT_STR:
        {
        int iLen = pProp->GetLen();
        _sntprintf_s(&pUni->strVal, iLen, _TRUNCATE, _T("%d"), ulConvVal);
        }
        break;

    default:
        break;
    }
    return true;
}

/**
 *  @brief  整数値設定.
 *  @param  [in] iVal  設定値
 *  @param  [in] iIoId ID値
 *  @retval true 設定成功
 *  @note   
 */
bool CIoDefBase::SetInt(int iVal, int iIoId)
{
    using namespace IO_COMMON;
    StdStringStream   strmRet;

    CIoProperty* pProp;
    pProp = m_pBlock->GetProperty(iIoId);

    UniVal* pUni;
    double  dFactor;
    IO_COMMON::DAT_TYPE eType;
    pUni = GetPreConvertVal(&dFactor, &eType, iIoId);

    if (!pProp)
    {
        return false;
    }

    bool bRet = false;

    double dConvVal;
    long   lConvVal;
    long   iOffset;

    E_INTERPOLATION eInterpolation;
    eInterpolation = pProp->GetInterpolation();

    if (eInterpolation == IP_NONE)
    {
        lConvVal = iVal;
    }
    else 
    {
        pProp->GetInvVal(&dConvVal, static_cast<double>(iVal));
        lConvVal = static_cast<long>(dConvVal);
    }
    


    switch(eType)
    {

    case DAT_BIT:
        
        iOffset  = pProp->GetOffset();
        if (lConvVal)
        {
            pUni->ucVal = (pUni->ucVal | (0x1 << iOffset));
        }
        else
        {
            pUni->ucVal = (pUni->ucVal & (~(0x1 << iOffset)));
        }

        break;

    case DAT_CHAR:
        if (lConvVal > SCHAR_MAX)
        {
            pUni->cVal = SCHAR_MAX;
        }
        else if (lConvVal < SCHAR_MIN)
        {
            pUni->cVal = SCHAR_MIN;
        }
        else
        {
            pUni->cVal = static_cast<char>(lConvVal);
        }
        break;

    case DAT_UCHAR:
        if (lConvVal > UCHAR_MAX)
        {
            pUni->ucVal = UCHAR_MAX;
        }
        else
        {
            pUni->ucVal = static_cast<unsigned char>(lConvVal);
        }
        break;

    case DAT_USHORT:
        if (lConvVal > SHRT_MAX)
        {
            pUni->usVal = USHRT_MAX;
        }
        else if (lConvVal < SHRT_MIN)
        {
            pUni->usVal = SHRT_MIN;
        }
        else
        {
            pUni->usVal = static_cast<unsigned short>(lConvVal);
        }
        break;

    case DAT_SHORT:
        if (lConvVal > SHRT_MAX)
        {
            pUni->sVal = SHRT_MAX;
        }
        else
        {
            pUni->sVal = static_cast<short>(lConvVal);
        }
        break;

    case DAT_ULONG:
        if (lConvVal > INT_MAX)
        {
            pUni->iVal = INT_MAX;
        }
        else
        {
             pUni->uiVal = lConvVal;
        }
        break;

    case DAT_LONG:
        pUni->iVal = lConvVal;
        break;

    case DAT_FLOAT:
        pUni->fVal = static_cast<float>(lConvVal);
        break;

    case DAT_DOUBLE:
        pUni->dVal = static_cast<double>(lConvVal);
        break;

    case DAT_STR:
        {
        int iLen = pProp->GetLen();
        _sntprintf_s(&pUni->strVal, iLen, _TRUNCATE, _T("%d"), lConvVal);
        }
        break;

    default:

        break;
    }
    return true;
}

/**
 *  @brief  実行設定.
 *  @param  なし
 *  @retval true 設定成功
 *  @note   
 */
bool CIoDefBase::Exec()
{
    m_eDispIo = IO_COMMON::LT_EXEC;
    return true;
}

/**
 *  @brief  実行中判定
 *  @param  なし
 *  @retval true: 実行中
 *  @note
 */
bool CIoDefBase::IsExec() const
{
    //TODO:実装
    return false;
}

//----------------------
// VirtualListData
//----------------------

/**
 *  @brief  ページ設定
 *  @param  [in] iPage ページ
 *  @retval true: 成功
 *  @note
 */
bool CIoDefBase::SetCurrentPage(int iPage)
{
    CIoPropertyBlock* pBlock = GetBlock();
    if (!pBlock)
    {
        return false;
    }

    if (iPage >= pBlock->GetPageMax())
    {
        return false;
    }

    if (iPage < 0)
    {
        return false;
    }

    boost::recursive_mutex::scoped_lock lk(m_mtxPage);
    m_iPage = iPage;

    //TODO:表示切替
    return false;
}

/**
 *  @brief  ページ取得
 *  @param  なし
 *  @retval ページ
 *  @note
 */
int  CIoDefBase::GetCurrentPage() const
{
    return m_iPage;
}

/**
 *  @brief  リスト初期化
 *  @param  [in] pVirtualList
 *  @retval true 成功
 *  @note
 */
bool CIoDefBase::InitList(CVirtualCheckList* pVirtualList)
{
    int iColMax = GetColNum();

    CVirtualCheckList::COL_SET ColSet;

    ListView_SetExtendedListViewStyleEx(pVirtualList->m_hWnd, LVS_EX_FULLROWSELECT, LVS_EX_FULLROWSELECT);
    ListView_SetExtendedListViewStyleEx(pVirtualList->m_hWnd, LVS_EX_GRIDLINES, LVS_EX_GRIDLINES);
    ListView_SetExtendedListViewStyleEx(pVirtualList->m_hWnd, LVS_EX_HEADERDRAGDROP, LVS_EX_HEADERDRAGDROP);

    StdString strHeaderDef;
    StdString strHeader;
    int iWidth;
    int iPos;

    //全ての列を削除
    int iColumnCount = pVirtualList->GetHeaderCtrl()->GetItemCount();
    for (int iCol = 0; iCol < iColumnCount; iCol++)
    {
       pVirtualList->DeleteColumn(0);
    }

    for (int iCol = 0; iCol < iColMax; iCol++)
    {
        GetHeader(&strHeaderDef, &ColSet, iCol);
        CUtil::FormatedText(strHeaderDef, &iWidth,  &iPos, &strHeader);
        pVirtualList->InsertColumn ( iCol,   strHeader.c_str(),  iPos, iWidth);
        pVirtualList->SetCol(iCol, ColSet);
    }

    //コールバック設定
    pVirtualList->SetFunc(Callbackfunc, CallbackChek, this);
    pVirtualList->SetChgFunc(CallbackChg);

    m_pVirtualList = pVirtualList;

    //リスト更新
    UpdateList(pVirtualList);
    return true;
}

/**
 *  @brief  リスト切り離し
 *  @param  [in] pVirtualList
 *  @retval true 成功
 *  @note
 */
bool CIoDefBase::ReleaseList(CVirtualCheckList* pVirtualList)
{

    if (!pVirtualList)
    {
        return false;
    }

    //コールバック設定
    pVirtualList->SetFunc(NULL, NULL, NULL);
    pVirtualList->SetChgFunc(NULL);
    pVirtualList->SetItemCount( 0 );

    //リスト更新
    UpdateList(pVirtualList);
    return true;
}

/**
 *  @brief  リスト更新
 *  @param  [in] pVirtualList
 *  @retval true 成功
 *  @note
 */
bool CIoDefBase::UpdateList(CVirtualCheckList* pVirtualList)
{
    if (!pVirtualList)
    {
        return false;
    }

    VaridateConnection();

    int iPage  = GetCurrentPage();

    CIoPropertyBlock* pBlock = GetBlock();
    
    if (!pBlock)
    {
        return false;
    }

    int iRowMax = pBlock->GetRowMax(iPage);
    if (!IsExec())
    {
        //!< 入力行の分を追加する
        iRowMax++;
    }
    pVirtualList->SetItemCount( iRowMax );
    pVirtualList->UpdateWindow();
    return true;
}

/**
 *  @brief  列更新
 *  @param  [in] pVirtualList
 *  @retval true 成功
 *  @note   列の表示、非表示を切り替え
 */
bool CIoDefBase::UpdateCol(CVirtualCheckList* pVirtualList)
{
    StdString strHeaderDef;
    CVirtualCheckList::COL_SET ColSet;

    STD_ASSERT(pVirtualList);

    int iColMax = GetColNum();
    for (int iCol = 0; iCol < iColMax; iCol++)
    {
        pVirtualList->GetCol(iCol, &ColSet);
        ColSet.bVisible = IsVisibleCol(iCol, m_eDispIo);
        pVirtualList->SetCol(iCol, ColSet);
    }

    pVirtualList->UpdateHeaderVisible();
    return true;
}

/**
 *  @brief  列表示問い合わせ
 *  @param  [in] pVirtualList
 *  @param  [in] pVirtualList
 *  @retval true 表示
 *  @note   
 */
bool CIoDefBase::IsVisibleCol(int iCol, IO_COMMON::E_LIST_TYPE eType) const
{
    using namespace IO_COMMON;

    bool bRet = false;
    if (iCol < COL_USER)
    {
        E_IO_PROPERTY_COL ePropertyCol = static_cast<E_IO_PROPERTY_COL>(iCol);
        bRet = GetPropertyColDisp(eType,  iCol);
    }
    else
    {
        STD_ASSERT(m_psAccess != NULL);
        int iUserDefMax;
        iUserDefMax = m_psAccess->GetUserItemNumber();

        if (iCol < (COL_USER + iUserDefMax))
        {
           int iUSerDefCol = iCol - COL_USER;
           bRet = GetPropertyColDisp(eType,  COL_USER);
        }
        else if (iCol < (COL_USER + iUserDefMax + COL_CONNECT_MAX))
        {
            int iConnectCol = iCol - COL_USER - iUserDefMax;
            E_IO_CONNECT_COL eCol = static_cast<E_IO_CONNECT_COL>(iConnectCol);
            bRet = GetConnectColDisp(eType, eCol);
        }
    }
    return bRet;
}

/**
 *  @brief  ヘッダデータ取得
 *  @param  [out] pStrHeader セルデータ
 *  @param  [out] pColSet    セルデータ
 *  @param  [in]  iCol     列
 *  @retval true: 成功
 *  @note   VirtualList用
 */
bool CIoDefBase::GetHeader(StdString* pStrHeader, 
                      CVirtualCheckList::COL_SET* pColSet,
                      int iCol)  const
{
    using namespace IO_COMMON;

    STD_ASSERT(pColSet);

    StdString strRet;

    pColSet->iStyle      = DFCS_BUTTONCHECK;
    pColSet->bDraw       = false;
    pColSet->eEditType   = CVirtualCheckList::NONE;        //SetChangeで変更する
    pColSet->bColor      = false;
    pColSet->crDefault   = ::GetSysColor(COLOR_WINDOW);
    pColSet->bVisible    = true;

    if (iCol < COL_USER)
    {
        E_IO_PROPERTY_COL ePropertyCol = static_cast<E_IO_PROPERTY_COL>(iCol);
        strRet = GetPropertyColName(ePropertyCol);

        if (ePropertyCol == COL_UNIT)
        {
            pColSet->iStyle = DFCS_BUTTONPUSH;
            pColSet->bDraw  = true;           //ボタン描画
        }

    }
    else
    {
        STD_ASSERT(bool(m_psAccess));
        int iUserDefMax;
        iUserDefMax = m_psAccess->GetUserItemNumber();

        if (iCol < (COL_USER + iUserDefMax))
        {
           int iUSerDefCol = iCol - COL_USER;
           strRet = m_psAccess->GetUserItem(iUSerDefCol);
        }
        else if (iCol < (COL_USER + iUserDefMax + COL_CONNECT_MAX))
        {
            int iConnectCol = iCol - COL_USER - iUserDefMax;
            E_IO_CONNECT_COL eCol = static_cast<E_IO_CONNECT_COL>(iConnectCol);
            strRet = GetConnectColName(eCol);

            if (eCol == COL_CONNECT)
            {
                pColSet->iStyle = DFCS_BUTTONPUSH;
                pColSet->bDraw  = true;           //ボタン描画
            }
        }
        else
        {
            return false;
        }
    }

    STD_ASSERT(pStrHeader != NULL);
    STD_ASSERT(pColSet != NULL);

    if (pStrHeader)
    {
        *pStrHeader          = strRet;
    }
    return true;
}

/**
 *  @brief  編集可・不可問い合わせ
 *  @param  [in] iRow 行
 *  @param  [in] iCol 列
 *  @retval true: 編集可
 *  @note
 */
bool CIoDefBase::IsEnableEdit(int iRow, int iCol) const
{
    //TODO:実装
    //EnableEditIoProperty(E_IO_PROPERTY_COL eIoType)
    if (IsExec())
    {
        return false;
    }

    return true;
}

/**
 *  @brief  文字列によるデータ取得
 *  @param  [out] pStrCell セルデータ
 *  @param  [in]  iRow     行
 *  @param  [in]  iCol     列
 *  @retval true: 成功
 *  @note   VirtualList用
 */
bool CIoDefBase::GetCellStr(StdString* pStrCell, int iRow, int iCol)  const
{
    using namespace IO_COMMON;
    StdString strRet;
    bool bRet = false;

    boost::recursive_mutex::scoped_lock lk(m_mtxPage);

    if (m_iPage < 0)
    {
        return false;
    }

    CIoPropertyBlock* pBlock = GetBlock();
    if (!pBlock)
    {
        return false;
    }


    int iRowMax = pBlock->GetRowMax(m_iPage);
    if (iRow < 0)
    {
        return false;
    }

    if (iRow > iRowMax)
    {
        return false;
    }

    *pStrCell = _T("");
    if (iRow == iRowMax)
    {
        if (!IsExec())
        {
            //最終行は、入力行として機能させる
            return true;
        }
        else
        {
            return false;
        }
    }

    int iIoId;
    int iColMax = COL_USER +
                  pBlock->GetUserDataNum();

    iIoId = pBlock->GetIoId(m_iPage, iRow);

    CIoProperty* pProperty = pBlock->GetProperty(iIoId);

    STD_ASSERT(pProperty);

    if (pProperty)
    {
        if (iCol < iColMax)
        {
            bRet = pProperty->GetStr(pStrCell, iCol);
            STD_ASSERT(bRet);
        }
        else
        {
            //接続データ
            switch(iCol - iColMax)
            {
            case COL_VAL_DIRECT:
                *pStrCell = GetStr( iIoId, false);
                break;

            case COL_VAL:
                *pStrCell = GetStr(iIoId);
                break;

            case COL_CONNECT:
            {
                std::map< int, std::shared_ptr<CIoConnect> >::const_iterator iteFind;
                iteFind = m_mapIdConnect.find(iIoId);
                if (iteFind != m_mapIdConnect.end())
                {
                    *pStrCell = iteFind->second->GetConnectStr();
                }
                break;
            }
            default:
                return false;
            }
        }
    }
    return true;
}

/**
 *  @brief  文字列によるデータ設定
 *  @param  [out] pIsRedraw 再描画の有無
 *  @param  [in]  strCell   セルデータ
 *  @param  [in]  iRow      行
 *  @param  [in]  iCol      列
 *  @retval true: 成功
 *  @note   VirtualList用
 */
bool CIoDefBase::SetCellStr(bool* pIsRedraw, 
                            const StdString& strCell, 
                            int iRow, int iCol)
{

    const type_info& info = typeid( *this );
    if (info != typeid(CIoDef))
    {
        return false;
    }
    bool bRedraw = false;

    using namespace IO_COMMON;
    boost::recursive_mutex::scoped_lock(m_mtxPage);
    CIoPropertyBlock* pBlock = GetBlock();
    if (!pBlock)
    {
        return false;
    }

    int iId = pBlock->GetIoId(m_iPage, iRow);
    bool bRet = false;

    if (iId >= 0)
    {
        CIoProperty* pProperty = pBlock->GetProperty(iId);
        int iColMax = COL_USER +
                      pBlock->GetUserDataNum();

        if (iCol < COL_USER)
        {
            switch(iCol)
            {
            case COL_ID:
                break;

            case COL_TYPE:
            {
                DT_IO_TYPE eIoType =GetIoTypeFromName(strCell);
                pProperty->SetIoType(eIoType);


                bRet = true;
                break;
            }

            case COL_NAME:
                bRet = pProperty->SetName(strCell);
                break;

            case COL_INIT:
                bRet = pProperty->SetInit(strCell);
                break;

            case COL_NOTE:
                bRet = pProperty->SetNote(strCell);
                break;

            case COL_LEN:
            {
                int iLen = _tstol(strCell.c_str());
                bRet = pProperty->SetLen(iLen);
                break;
            }

            case COL_UNIT:
            {
                int iUnitId = SYS_UNIT->GetKindIdByName(strCell);
                bRet = pProperty->SetUnit(iUnitId);
                break;
            }


            default:
                break;

            }
        }
        else if (iCol < iColMax)
        {

            int iColUser = iCol - COL_USER;

            //!< ユーザーデータ設定
            StdString strUser = strCell;
            bRet = m_psAccess->IsSetUserItem(strUser,  iColUser, m_iPage, iRow);
            if (bRet)
            {
                bRet = pProperty->SetUserData(iColUser, strUser);
            }


        }
        else if (iCol < (iColMax + COL_CONNECT_MAX))
        {
            //接続データ
            //TODO: 現状は設定不可、設定については後で考える。


            int iColUser = iCol - iColMax;
            switch( iColUser )
            {
            case COL_VAL_DIRECT:
                break;

            case COL_VAL:
                break;

            case COL_CONNECT:
            {
                std::map< int, std::shared_ptr<CIoConnect> >::iterator iteMap;
                CIoConnect* pConnect = NULL;

                iteMap = m_mapIdConnect.find(iId);
                if (iteMap != m_mapIdConnect.end())
                {
                    pConnect = iteMap->second.get();
                }

                if (strCell == _T(""))
                {
                    if(pConnect)
                    {
                        //接続削除
                        m_mapIdConnect.erase(iteMap);
                    }
                    bRet = true;
                }
                else
                {
                    bool bReg = false;
                    std::map< int, std::shared_ptr<CIoConnect> >::iterator iteMapName;
                    //名称から接続先を取得
                    for (iteMapName  = m_mapIdConnect.begin();
                         iteMapName != m_mapIdConnect.end();
                         iteMapName++)
                    {

                        if (strCell == iteMapName->second->GetConnectStr())
                        {
                            //既に登録済み
                            bReg = true;
                            break;
                        }
                    }

                    
                    if (!bReg)
                    {
                        if(pConnect)
                        {
                            //接続削除
                            m_mapIdConnect.erase(iteMap);
                        }
                            //!< 接続先設定
                            //bool SetConnect(CIoDef* pIoDef, std::shared_ptr<CIoProperty>& pProp)
                                
                    };
                }
                break;
            }

            default:
                break;
            }

        }
        else
        {
            
        }
    }
    else
    {
        if (iCol == COL_TYPE)
        {
            //種別設定で新たに行を追加する
            CIoProperty* pProperty;
            DT_IO_TYPE eIoType =GetIoTypeFromName(strCell);
            CIoPropertyBlock* pBlock = GetBlock();
            if (pBlock)
            {
                if (eIoType == DAT_BIT)
                {
                    for (int iBit = 0; iBit < 8; iBit++)
                    {
                        pProperty = pBlock->Add(m_iPage);
                        pProperty->SetIoType(eIoType);
                    }
                }
                else
                {
                    pProperty = pBlock->Add(m_iPage);
                    pProperty->SetIoType(eIoType);
                }

                bRet = true;
                pBlock->RecalcAddress(m_iPage);
            }

        }   
        bRedraw = true;
    }

    if (pIsRedraw)
    {
        *pIsRedraw = bRedraw;
    }
    return bRet;
}


/**
 * @brief   リスト描画
 * @param   [in]  nRow          データを取得する行
 * @param   [in]  nCol          データを取得する列
 * @param   [out] pData         文字列
 * @param   [out] pbCheck       チェックボックス
 * @retval  なし
 * @note	Callbackfuncから呼び出し
 */
void  CIoDefBase::DrawList(int nRow, int nCol, _TCHAR* pData, bool* pbCheck)
{
    using namespace IO_COMMON;
    bool bRet;
    boost::recursive_mutex::scoped_lock(m_mtxPage);
    //TODO:実装

    STD_ASSERT(m_pVirtualList);
    CIoPropertyBlock* pBlock = GetBlock();
    if (!pBlock)
    {
        return;
    }

    int iId = pBlock->GetIoId(m_iPage, nRow);
    if (iId < 0)
    {
        return;
    }

    //-------------------------------
    // Ioが出力に設定されている時は
    // 接続ボタンを押せないようにする
    //-------------------------------
    if (nCol > COL_USER)
    {
        int iColMax = COL_USER +
                  pBlock->GetUserDataNum();
        int iColUser = nCol - iColMax;

        if (iColUser == COL_CONNECT)
        {
            CIoProperty*  pProp = pBlock->GetProperty(iId);
            int iStye = DFCS_BUTTONPUSH;
            if ( pProp )
            {
                if ( pProp->GetDir() == IO_OUT)
                {
                    iStye = (DFCS_INACTIVE | DFCS_BUTTON3STATE);
                }
            }
            CVirtualCheckList::COL_SET colSet;

            BOOL bRetCol;
            bRetCol  = m_pVirtualList->GetCol (nCol, &colSet);
            STD_ASSERT(bRetCol);

            colSet.iStyle = iStye;
            bRetCol = m_pVirtualList->SetCol(nCol, colSet);
            STD_ASSERT(bRetCol);
        }
    }

    StdString strCol;
    bRet = GetCellStr(&strCol, nRow, nCol);
    STD_ASSERT(bRet);

    lstrcpy(pData ,strCol.c_str());
}

/**
 * @brief   チェックボックス呼び出し
 * @param   [in]  nRow    選択時の行
 * @param   [in]  nCol    選択時の列
 * @retval  なし
 * @note	
 */
void  CIoDefBase::SetChek( int iRow, int iCol)
{
    using namespace IO_COMMON;
    CIoPropertyBlock* pBlock = GetBlock();
    if (!pBlock)
    {
        return;
    }

    int iColMax = COL_USER +
                  pBlock->GetUserDataNum();

    if (iCol < COL_USER)
    {
        switch(iCol)
        {
        case COL_ID:            break;
        case COL_TYPE:          break;
        case COL_NAME:          break;
        case COL_INIT:          break;
        case COL_NOTE:          break;
        case COL_LEN:           break;
        case COL_UNIT:          break;
        default:                break;
        }
    }
    else if (iCol < iColMax)
    {
        int iColUser = iCol - COL_USER;
    }
    else if (iCol < (iColMax + COL_CONNECT_MAX))
    {
        int iColUser = iCol - iColMax;
        switch( iColUser )
        {
            case COL_VAL_DIRECT:    break;
            case COL_VAL:           break;

            case COL_CONNECT:
            {
               
                int iIoId = pBlock->GetIoId(m_iPage, iRow);

                CIoProperty*  pProp = pBlock->GetProperty(iIoId);
                std::weak_ptr<CIoConnect>  wpConnect = GetConnect(iIoId);

                CIoConnect* pConnect = NULL;
                if( std::shared_ptr<CIoConnect> tmp = wpConnect.lock() )
                {
                    pConnect = tmp.get();
                }

                CIoConnectDlg dlgIoConnect;
                dlgIoConnect.SetIo(this, iIoId);

                int iConnectId = -1;
                int iConnectIoId = -1;

                if (pConnect)
                {
                   pConnect->GetId(&iConnectId, &iConnectIoId);
                }

                dlgIoConnect.SetConnect(iConnectId, iConnectIoId);

                if( dlgIoConnect.DoModal() == IDOK)
                {
                    bool bRet;
                    bRet = dlgIoConnect.GetConnect(&iConnectId, &iConnectIoId);
                    if (bRet)
                    {
                        if (iConnectIoId == -1)
                        {
                            DelConnect( iIoId);
                        }
                        else
                        {
                            AddConnectById(iConnectId, iConnectIoId, iIoId);
                        }
                    }
                }
            }
            break;

            default:
                break;
        }
    }
}

/**
 * @brief   変更前呼び出し
 * @param   [in]  nRow           データを取得する行
 * @param   [in]  nCol           データを取得する列
 * @param   [in]  pData          変更データ
 * @retval  
 * @note	
 */
void  CIoDefBase::SetChg( CVirtualCheckList* pVirtualList,
                         int nRow, int nCol, const _TCHAR* pData)
{
    using namespace IO_COMMON;
    CVirtualCheckList::COL_SET colSet;

    pVirtualList->GetCol ( nCol, &colSet);

    if (IsEnableEdit(nRow, nCol))
    {
        colSet.eEditType = CVirtualCheckList::ALLOW;
    }
    else
    {
        colSet.eEditType = CVirtualCheckList::NONE;
        pVirtualList->SetCol ( nCol, colSet);
        return;
    }

    if (nCol == COL_TYPE)
    {
        bool bRet;
        //IO種別
        std::vector<DT_IO_TYPE> lstType;

        boost::recursive_mutex::scoped_lock(m_mtxPage);
        bRet = m_psAccess->GetItemRestriction(&lstType, m_iPage, nRow);

        if (bRet)
        {
            std::vector<StdString> lstCombo;
            foreach(DT_IO_TYPE eType, lstType)
            {
                lstCombo.push_back(GetIoTypeName(eType)); 
            }

            colSet.eEditType = CVirtualCheckList::COMBO;
            colSet.lstCombo  = lstCombo;
        }
    }

    pVirtualList->SetCol ( nCol, colSet);
}


/**
 * @brief   描画コールバック
 * @param   [in]  pParent 呼び出し元         
 * @param   [in]  nRow    データを取得する行         
 * @param   [in]  nCol    データを取得する列        
 * @param   [out] pData   文字列        
 * @param   [out] pbCheck チェックボックス         
 * @retval  なし
 * @note	仮想リスト用表示関数
 */
void CIoDefBase::Callbackfunc (LPVOID pParent, 
                               int nRow, 
                               int nCol, 
                               _TCHAR* pData, 
                               bool* pbCheck)
{
    CIoDefBase* pBase = reinterpret_cast<CIoDefBase*>(pParent);

    //描画処理
    pBase->DrawList( nRow, nCol, pData,  pbCheck);
}


/**
 * @brief   チェックボックスコールバック
 * @param   [in]  nRow    選択時の行         
 * @param   [in]  nCol    選択時の列        
 * @retval  なし
 * @note	仮想リストボタンチェック時に呼び出し
 */
void CIoDefBase::CallbackChek   (LPVOID pParent, int nRow, int nCol)
{
    CIoDefBase* pBase = reinterpret_cast<CIoDefBase*>(pParent);
    pBase->SetChek( nRow, nCol);
}

/**
 * @brief    変更前コールバック
 * @param   [in]  pParent    呼び出し元     
 * @param   [in]  pCheckList リストコントロール     
 * @param   [in]  nRow    データを取得する行         
 * @param   [in]  nCol    データを取得する列        
 * @param   [in]  pData   変更データ        
 * @retval   なし
 * @note	
 */
void CIoDefBase::CallbackChg(LPVOID pParent, CVirtualCheckList* pCheckList,
                             int nRow, int nCol, const _TCHAR* pData)
{
    CIoDefBase* pBase = reinterpret_cast<CIoDefBase*>(pParent);
    pBase->SetChg( pCheckList, nRow, nCol, pData);
}


/**
 * @brief   ラベル変更
 * @param   [in]   pCheckList
 * @param   [in]   pNMHDR         
 * @param   [out]  pResult  
 * @retval   なし
 * @note	
 */
void CIoDefBase::OnLvnEndlabeleditVlst(CVirtualCheckList* pCheckList,
                                       NMHDR *pNMHDR, 
                                       LRESULT *pResult)
{

    NMLVDISPINFO *pDispInfo = reinterpret_cast<NMLVDISPINFO*>(pNMHDR);


	if(pDispInfo->item.pszText == NULL)
	{
        return;
    }

    StdString strItem = pDispInfo->item.pszText;
    int nRow = pDispInfo->item.iItem;
    int nCol = pDispInfo->item.iSubItem;


    bool bRet = false;
    bool bRedraw;

    bRet = SetCellStr(&bRedraw, strItem, nRow, nCol);
    
    if (bRedraw)
    {
        UpdateList(pCheckList);
    }

    
    if (bRet)
    {
        *pResult = 0;
    }
    else
    {
        *pResult = 1;
    }
}


//================================================

/**
 * @brief   接続追加
 * @param   [in]  iIoId         IO項目ID
 * @param   [in]  spConnect     
 * @retval  true 成功
 * @note	
 */
bool CIoDefBase::AddConnect( int iIoId, std::shared_ptr<CIoConnect> spConnect)
{
    if (m_mapIdConnect.find(iIoId) != m_mapIdConnect.end())
    {
        //データを上書き
    }

    m_mapIdConnect[iIoId] =  spConnect;
    m_iChgCnt++;
    return true;
}



/**
 * @brief   接続削除
 * @param   [in]  iIoId         IO項目ID
 * @retval  true 成功
 * @note	
 */
bool CIoDefBase::DelConnect( int iIoId)
{
    std::map< int, std::shared_ptr<CIoConnect> >::iterator iteMap;

    iteMap = m_mapIdConnect.find(iIoId);

    if (iteMap == m_mapIdConnect.end())
    {
        return false;
    }

    m_mapIdConnect.erase(iteMap);

    m_iChgCnt++;
    return true;
}



/**
 * @brief   接続取得
 * @param   [in]  iIoId         IO項目ID
 * @retval  接続情報
 * @note
 */
CIoConnect* CIoDefBase::GetConnect(int iIoId) const
{
    CIoConnect* pConnect = NULL;
    std::map< int, std::shared_ptr<CIoConnect> >::const_iterator ite;

    ite = m_mapIdConnect.find(iIoId);

    if (ite != m_mapIdConnect.end())
    {
        pConnect = ite->second.get();
    }           

    return pConnect;
}

/**
 * @brief   接続データ
 * @param   なし
 * @retval  接続データマップの取得
 * @note    CPartsDef::LoadAfter から使用する
 */
std::map< int, std::shared_ptr<CIoConnect> >* CIoDefBase::GetConnectMap()
{
    return &m_mapIdConnect;
}


/**
 * @brief   読込後初期化処理
 * @param   なし
 * @retval  true 成功
 * @note	
 */
bool CIoDefBase::LoadAfter()
{
    return true;
}


// 変更の有無
bool CIoDefBase::IsChange() const
{
    if (m_iChgCnt != 0)
    {
        return true;
    }
    return false;
}


/**
 * @brief   シリアル化(ロード)
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  なし
 * @note	
 */
template<class Archive>
void CIoDefBase::load(Archive& ar, const unsigned int version)
{
    SERIALIZATION_INIT;
        SERIALIZATION_LOAD("Access",  m_psAccess);
        SERIALIZATION_LOAD("Connect", m_mapIdConnect);

        if (m_psAccess.get() != NULL)
        {
            m_psAccess->SetIoDef(this);
            m_psAccess->Init();
        }

        m_iPage  = 0;
        m_pBlock = NULL;
        m_mapAddress.clear();
        m_pVirtualList = NULL;
        m_iChgCnt = 0;

    MOCK_EXCEPTION_FILE(e_file_read);
}

/**
 * @brief   シリアル化(セーブ)
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  なし
 * @note	
 */
template<class Archive>
void CIoDefBase::save(Archive& ar, const unsigned int version) const
{
    SERIALIZATION_INIT;

        SERIALIZATION_SAVE("Access", m_psAccess);
        SERIALIZATION_SAVE("Connect"  , m_mapIdConnect);
        m_iChgCnt = 0;

    MOCK_EXCEPTION_FILE(e_file_write);
}

/**
 * @brief   表示モード設定
 * @param   [in] eDispIo  
 * @retval  なし
 * @note    テスト用
 */
void CIoDefBase::TEST_SetDispMode(IO_COMMON::E_LIST_TYPE eDispIo)
{
#ifdef _DEBUG
    m_eDispIo = eDispIo;
#else
    STD_DBG(_T("Don't use TEST_SetDispMode"));
#endif
}

/**
 * @brief   表示モード設定
 * @param   [in] eDispIo  
 * @retval  なし
 * @note    テスト用
 */
void CIoDefBase::TEST_SetPropertyBlock(CIoPropertyBlock*  pBlock)
{
#ifdef _DEBUG
    m_pBlock = pBlock;
#else
    STD_DBG(_T("Don't use TEST_SetPropertyBlock"));
#endif
}

//load save のインスタンスが生成されないため
//ダミーとして実装
void CIoDefBase::Dummy()
{
    unsigned int iVersion = 0;

    StdStreamOut outFs(_T(""));
    StdStreamIn  inFs(_T(""));
    //boost::filesystem::ofstream outTxtFs(_T(""));
    //boost::filesystem::ifstream inTxtFs(_T(""));

    boost::archive::text_woarchive txtOut(outFs); 
    boost::archive::text_wiarchive txtIn(inFs);

    save<boost::archive::text_woarchive>( txtOut, iVersion);
    load<boost::archive::text_wiarchive>( txtIn,  iVersion);

    StdXmlArchiveOut outXml(outFs);
    StdXmlArchiveIn inXml(inFs);

    save<StdXmlArchiveOut>(outXml, iVersion);
    load<StdXmlArchiveIn>(inXml, iVersion);


    boost::filesystem::ofstream outFsStd(_T(""));
    boost::filesystem::ifstream inFsStd(_T(""));

    boost::archive::binary_oarchive outBin(outFsStd);
    boost::archive::binary_iarchive inBin(inFsStd);

    save<boost::archive::binary_oarchive>(outBin, iVersion);
    load<boost::archive::binary_iarchive>(inBin, iVersion);

}


//*******************************
//             TEST
//*******************************
#ifdef _DEBUG


void TEST_CIoDefBase()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());


    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}
#endif //_DEBUG