/**
 * @brief			IIoDef ヘッダーファイル
 * @file			IIoDef.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

#ifndef  _I_IO_DEF_H__
#define  _I_IO_DEF_H__
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "Utility/VirtualTreeCtrl/VirtualCheckList.h"
#include "CIoCommon.h"
#include "DrawingObject/BaseObj.h"

/*---------------------------------------------------*/
/*  Class                                            */
/*---------------------------------------------------*/
class CIoPropertyBlock;
class CIoAccess;
class CIoConnect;
class CObjectDef;
class CDrawingParts;
class CIoPropertyBlock;
class CIoProperty;
class CPartsDef;
union UniVal;

/**
 * @class   IIoDef
 * @brief                       
 */
class IIoDef
{
public:
 
};



/**
 * @class   CIoDefBase
 * @brief   CIoDef CIoRef 共通部
 */
class CIoDefBase  :public CBaseObj, public IIoDef 
{
    struct CONNECT_OUTPUT
    {
        char*   pAdress;
        double  dFactor;
        IO_COMMON::DAT_TYPE eType;
    };

public:
    //!< コンストラクタ
    CIoDefBase();

    //!< コピーコンストラクタ
    CIoDefBase(const  CIoDefBase&  def);

    //!< デストラクタ
    ~CIoDefBase();

    //!< 代入演算子
    virtual CIoDefBase& operator = (const CIoDefBase & obj);

    //!< 等価演算子
    virtual bool operator == (const CIoDefBase & obj) const;

    //!< 不等価演算子
    virtual bool operator != (const CIoDefBase & obj) const;

    //!<----------------------
    //!<  プロパティ
    //!<----------------------
    //!< プロパティブロック取得 
    virtual CIoPropertyBlock*  GetBlock() const = 0;

    //!< 名称取得
    virtual StdString GetName() const = 0;

    //!< オブジェクト定義取得
    //virtual CObjectDef* GetDef() const = 0;

    //!< 接続先取得
    std::weak_ptr<CIoConnect> GetConnect(int iIoId);

    //!< 生成確認
    bool IsCreated() const;

    //!< 描画領域取得
    virtual CPartsDef* GetPartsDef() const = 0;

    //!< ページ最大数取得
    int GetMaxPage();

    //!<----------------------
    //!<  初期化処理
    //!<----------------------
    //!< 親オブジェクト設定
    //virtual void SetParent(CObjectDef* pParent);
    
    
    //!<----------------------
    //!<  実行前準備
    //!<----------------------
    //!< 実行前準備
    virtual bool SetupStart();

    //!< アドレス設定
    virtual bool SetupAdress();

    //!<----------------------
    //!<  実行処理
    //!<----------------------
    //!< 実行設定
    virtual bool Exec();

    //!< 停止設定
    virtual bool Stop() = 0;

    //!< 実行中判定
    virtual bool IsExec() const;

    //!<----------------------
    //!<  値取得
    //!<----------------------

    //!< 変換前数値
    virtual UniVal*  GetPreConvertVal(double* pFactor, 
                                      IO_COMMON::DAT_TYPE* pType,
                                      int iIoId) const;

    //!< 変換前符号なし整数値取得
    virtual UINT        GetUint         (int iIoId, bool bConvert = true) const;

    //!< 変換前整数値取得
    virtual int         GetInt          (int iIoId, bool bConvert = true) const;

    //!< 変換前文字列
    virtual StdString   GetPreConvertStr(int iIoId) const;

    //!< 文字列取得
    virtual StdString   GetStr          (int iIoId, bool bConvert = true) const;

    //!< 実数値取得
    virtual double      GetDouble       (int iIoId, bool bConvert = true) const;

    //!< 接続名取得
    virtual StdString   GetConnectName  (int iIoId) const;

    //!<----------------------
    //!<  値設定
    //!<----------------------

    //!< 文字列設定
    virtual bool    SetStr      (StdString strVal,  int iIoId);

    //!< 符号なし整数値設定
    virtual bool    SetUint     (UINT uiVal,        int iIoId);

    //!< 実数値設定.
    virtual bool    SetDouble   (double dVal,       int iIoId);

    //!< 整数値設定
    virtual bool    SetInt      (int iVal,          int iIoId);

    //----------------------
    // VirtualListData
    //----------------------
    //!< ページ設定
    virtual bool SetCurrentPage(int iPage);

    //!< ページ取得
    virtual int  GetCurrentPage() const;

    //!< リスト初期化
    virtual bool InitList(CVirtualCheckList* pVirtualList);

    //!< リスト切り離し
    virtual bool ReleaseList(CVirtualCheckList* pVirtualList);

    //!< リスト更新
    virtual bool UpdateList(CVirtualCheckList* pVirtualList);

    //!< 列更新
    virtual bool UpdateCol(CVirtualCheckList* pVirtualList);


    //!< 列数取得
    virtual int GetColNum() const;

    //!< ヘッダデータ取得
    virtual bool GetHeader(StdString* pStrHeader, 
                          CVirtualCheckList::COL_SET* pColSet,
                          int iCol)  const;


    virtual bool IsVisibleCol(int iCol, IO_COMMON::E_LIST_TYPE eType) const;

        //!< 編集可・不可問い合わせ
    virtual bool IsEnableEdit(int iRow, int iCol) const;

    //!< 文字列によるデータ取得
    virtual bool GetCellStr(StdString* pStrCell, int iRow, int iCol)  const;

    //!< 文字列によるデータ設定
    virtual bool SetCellStr(bool* pIsRedraw, const StdString& strCell, int iRow, int iCol);

    //!< リスト描画
    virtual void  DrawList(int nRow, int nCol, _TCHAR* pData, bool* pbCheck);

    //!< チェックボックス呼び出し
    virtual void  SetChek( int nRow, int nCol);

    //!< 変更前呼び出し
    virtual void  SetChg( CVirtualCheckList* pVirtualList,
                          int nRow, int nCol, const _TCHAR* pData);

    //!< 描画コールバック
    static void __cdecl Callbackfunc(LPVOID pParent, int nRow, int nCol, _TCHAR* pData, bool* pbCheck);

    //!< チェックコールバック
    static void __cdecl CallbackChek(LPVOID pParent, int nRow, int nCol);

    //!< 変更コールバック
    static void __cdecl CallbackChg(LPVOID pParent, CVirtualCheckList* pCheckList,
                        int nRow, int nCol, const _TCHAR* pData);

    //!< エディット完了
    afx_msg void OnLvnEndlabeleditVlst(CVirtualCheckList* pCheckList,
                                      NMHDR *pNMHDR, 
                                      LRESULT *pResult);


    //!< 読込後初期化処理
    virtual bool LoadAfter();

    //!< 接続確認
    bool VaridateConnection();

    //!< 接続追加
    bool AddConnect( int iIoId, std::shared_ptr<CIoConnect> spConnect);

    //!< IDによる接続追加
    virtual bool AddConnectById(int iIdFrom, int iIoIdFrom, int iIoIdTo) = 0;

    //!< 接続削除
    bool DelConnect( int iIoId);

    //!< 接続取得
    CIoConnect* GetConnect(int iIoId) const;

    // 変更の有無
    virtual bool IsChange() const;

     //!< 再コンパイルが必要な変更の有無
    virtual bool GetNeedsCompile(){return false;}

    //-----------
    //テスト用
    //-----------
    void TEST_SetDispMode(IO_COMMON::E_LIST_TYPE eDispIo);

    void TEST_SetPropertyBlock(CIoPropertyBlock*  pBlock);
    
protected:
friend CIoDefBase;
friend CPartsDef;


    //!< load save のインスタンスが生成用ダミー
    void Dummy();

    //!< 接続データ
    std::map< int, std::shared_ptr<CIoConnect> >* GetConnectMap();

    //!< 設定用IOアドレス取得
    char* GetSetupAddress(int iIoId);
    

protected:
    //!<----------------
    //!< 保存データ
    //!<----------------
    //!< IOデータへのアクセス
    std::shared_ptr<CIoAccess>                  m_psAccess;

    //!< 接続データ
    std::map< int, std::shared_ptr<CIoConnect> > m_mapIdConnect;


    //!<----------------
    //!< 非保存データ
    //!<----------------
    //!< 表示モード
    IO_COMMON::E_LIST_TYPE           m_eDispIo;


    //!< 現在表示中のページ
    mutable int                      m_iPage;


    //!< ページ変更ミューテックス
    mutable boost::recursive_mutex   m_mtxPage;

       
    //!< プロパティ
    CIoPropertyBlock*                m_pBlock;


    //!< アドレスキャッシュ
    std::map<int, CONNECT_OUTPUT>   m_mapAddress;

    //!< IOリスト
    CVirtualCheckList*              m_pVirtualList;

    //!< 変更カウント
    mutable int                     m_iChgCnt;

private:
    friend class boost::serialization::access;  
    BOOST_SERIALIZATION_SPLIT_MEMBER();

    template<class Archive>
    void load(Archive& ar, const unsigned int version);

    template<class Archive>
    void save(Archive& ar, const unsigned int version) const;

};


#endif  //_I_IO_DEF_H__
