/**
* @brief			CMainFrame実装ファイル
* @file			MainFrame.cpp
* @author			Yasuhiro Sato
* @date			07-2-2009 20:03:09
* 
* @note	
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
* 
* @remarks	        
*			
*
* $
* $
* 
*/

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MockSketch.h"

#include "./MainFrm.h"
#include "DefinitionObject/View/MockSketchDoc.h"

#include "CProjectCtrl.h"
#include "System/CSystem.h"
#include "ToolBar/CLineStyleMenuItem.h"
#include "ToolBar/CLineWidthMenuItem.h"
#include "ToolBar/CToolBarFontSizeCombo.h"
#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/View/ObjectDefView.h"

#include "Script/EditorView.h"
#include "Script/CScript.h"
#include "Script/CExecCtrl.h"

#include "System/DlgSetting.h"
#include "MultiDocReloadableTemplate.h"
#include "SubWindow/NewProjectDlg.h"
#include "SubWindow/PropertyGroupDlg.h"

#include "ExecCommon.h"
#include "ExecCommon.h"

#include "DefinitionObject/View/MockSketchView.h"
#include "DefinitionObject/View/CElementView.h"


/*---------------------------------------------------*/
/*  Defines                                          */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace EXEC_COMMON;

const int  iMaxUserToolbars = 20;
const UINT uiFirstUserToolBarId = AFX_IDW_CONTROLBAR_FIRST + 40;
const UINT uiLastUserToolBarId = uiFirstUserToolBarId + iMaxUserToolbars - 1;

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
IMPLEMENT_DYNAMIC(CMainFrame, CMDIFrameWndEx)

BEGIN_MESSAGE_MAP(CMainFrame, CMDIFrameWndEx)
    ON_WM_CREATE()
    ON_COMMAND(ID_WINDOW_MANAGER, &CMainFrame::OnWindowManager)
    ON_COMMAND(ID_VIEW_CUSTOMIZE, &CMainFrame::OnViewCustomize)
    ON_REGISTERED_MESSAGE(AFX_WM_CREATETOOLBAR, &CMainFrame::OnToolbarCreateNew)
    ON_COMMAND_RANGE(ID_VIEW_APPLOOK_WIN_2000, ID_VIEW_APPLOOK_OFF_2007_AQUA, &CMainFrame::OnApplicationLook)
    ON_UPDATE_COMMAND_UI_RANGE(ID_VIEW_APPLOOK_WIN_2000, ID_VIEW_APPLOOK_OFF_2007_AQUA, &CMainFrame::OnUpdateApplicationLook)

    //==========ユーザー定義
    ON_MESSAGE(WM_OUTPUT_PRINT,      OnPrintMessage)
    ON_MESSAGE(WM_VIEW_CREATE,       OnViewCreate)
    ON_MESSAGE(WM_VIEW_SEL_CHANGEE,  OnViewSelchange)
    ON_MESSAGE(WM_OUTPUT_CLEAR,      OnOutputClear)
    ON_MESSAGE(WM_OUTPUT_SEL,        OnOutputSel)

    ON_COMMAND_RANGE(ID_MNU_SCRIPT_BUILD, ID_MNU_SCRIPT_REBUILD, &CMainFrame::OnMenuBuild)

    ON_COMMAND(ID_MNU_SCRIPT_STOP_BUILD  , &CMainFrame::OnMenuStopBuild)
    ON_COMMAND(ID_MNU_SCRIPT_EXEC        , &CMainFrame::OnMenuExecute)
    ON_COMMAND(ID_MNU_SCRIPT_ABORT       , &CMainFrame::OnMenuAbort)
    ON_COMMAND(ID_MNU_SCRIPT_PAUSE       , &CMainFrame::OnMenuPause)
    //ON_COMMAND(ID_MNU_SCRIPT_DEBUG       , &CMainFrame::OnMenuDebug)
    //ON_COMMAND(ID_MNU_SCRIPT_RELEASE     , &CMainFrame::OnMenuRelease)
    ON_UPDATE_COMMAND_UI_RANGE(ID_MNU_SCRIPT_BUILD, ID_MNU_SCRIPT_REBUILD, &CMainFrame::OnUpdateMenuBuild)
    ON_UPDATE_COMMAND_UI(ID_MNU_SCRIPT_STOP_BUILD  , &CMainFrame::OnUpdateMenuStopBuild)
    ON_UPDATE_COMMAND_UI(ID_MNU_SCRIPT_EXEC        , &CMainFrame::OnUpdateMenuExecute)
    ON_UPDATE_COMMAND_UI(ID_MNU_SCRIPT_ABORT       , &CMainFrame::OnUpdateMenuAbort)
    ON_UPDATE_COMMAND_UI(ID_MNU_SCRIPT_PAUSE       , &CMainFrame::OnUpdateMenuPause)
    //ON_UPDATE_COMMAND_UI(ID_MNU_SCRIPT_DEBUG       , &CMainFrame::OnUpdateMenuDebug)
    //ON_UPDATE_COMMAND_UI(ID_MNU_SCRIPT_RELEASE     , &CMainFrame::OnUpdateMenuRelease)

    //==========ユーザー定義
    ON_COMMAND(ID_MNU_NEW_PROJECT,    &CMainFrame::OnMenuNewProject)
    ON_COMMAND(ID_MNU_OPEN_PROJECT,   &CMainFrame::OnMenuOpenProject)

    ON_COMMAND(ID_MNU_SAVE_FILE,      &CMainFrame::OnMenuSeveFile)
    ON_COMMAND(ID_MNU_SAVE_AS_FILE,   &CMainFrame::OnMenuSeveAs)
    ON_COMMAND(ID_MNU_SAVE_ALL,       &CMainFrame::OnMenuSeveAll)

    //==========タブグループ
    ON_COMMAND(ID_MDI_MOVE_TO_NEXT_GROUP, OnMdiMoveToNextGroup)
    ON_COMMAND(ID_MDI_MOVE_TO_PREV_GROUP, OnMdiMoveToPrevGroup)
    ON_COMMAND(ID_MDI_NEW_HORZ_TAB_GROUP, OnMdiNewHorzTabGroup)
    ON_COMMAND(ID_MDI_NEW_VERT_GROUP, OnMdiNewVertGroup)
    ON_COMMAND(ID_MDI_CANCEL, OnMdiCancel)
    ON_COMMAND(ID_MDI_TABBED_DOCUMENT, OnMdiTabbedDocument)
    ON_UPDATE_COMMAND_UI(ID_MDI_TABBED_DOCUMENT, OnUpdateMdiTabbedDocument)

    ON_UPDATE_COMMAND_UI(ID_MDI_NEW_HORZ_TAB_GROUP, OnUpdateMdiNewHorzTabGroup)
    ON_UPDATE_COMMAND_UI(ID_MDI_NEW_VERT_GROUP, OnUpdateMdiNewVertTabGroup)
    ON_UPDATE_COMMAND_UI(ID_MDI_MOVE_TO_NEXT_GROUP, OnUpdateMdiMoveToNextGroup)


    //========== 最近使用した
    ON_COMMAND_RANGE(ID_MOCKSKETCH_MRU_FILE, ID_MOCKSKETCH_MRU_FILE20, OnMruFile)
    ON_UPDATE_COMMAND_UI_RANGE(ID_MOCKSKETCH_MRU_FILE, ID_MOCKSKETCH_MRU_FILE20, OnUpdateMruFile)
    ON_COMMAND_RANGE(ID_PROJECT_MRU_FILE, ID_PROJECT_MRU_FILE20, OnMruProject)
    ON_UPDATE_COMMAND_UI_RANGE(ID_PROJECT_MRU_FILE, ID_PROJECT_MRU_FILE20, OnUpdateMruProject)

    //==========
    ON_COMMAND(ID_MNU_OPTION,   &CMainFrame::OnMenuOption)
    ON_UPDATE_COMMAND_UI(ID_MNU_OPTION, OnUpdateMenuOption)
    ON_COMMAND(ID_MNU_PROPERTY_GROUP,   &CMainFrame::OnMenuPropertyGroup)
    ON_UPDATE_COMMAND_UI(ID_MNU_PROPERTY_GROUP, OnUpdateMenuPropertyGroup)

    ON_WM_INITMENUPOPUP()
    ON_WM_CLOSE()
    ON_REGISTERED_MESSAGE(AFX_WM_AFTER_TASKBAR_ACTIVATE, OnAfterTaskbarActivate)
    ON_REGISTERED_MESSAGE(AFX_WM_RESETTOOLBAR, &OnResetToolbar) 

END_MESSAGE_MAP()

//----------------------------------
// ステータス ライン インジケータ
//----------------------------------
static UINT indicators[] =
{
    ID_SEPARATOR,
    ID_INDICATOR_CAPS,
    ID_INDICATOR_NUM,
    ID_INDICATOR_SCRL,
};


/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CMainFrame::CMainFrame()
{
    theApp.m_nAppLook = theApp.GetInt(_T("ApplicationLook"), ID_VIEW_APPLOOK_VS_2005);

    m_psProject =  std::make_shared<CProjectCtrl>();
    m_psLibrary =  std::make_shared<CProjectCtrl>(VIEW_COMMON::E_PROJ_LIB);
    m_wndLibView.SetProjectType(VIEW_COMMON::E_PROJ_LIB);
    m_pCtrl =  std::make_unique<CExecCtrl>();

}

/**
 * @brief  デストラクター.
 * @param  なし
 * @retval なし     
 * @note
 */
CMainFrame::~CMainFrame()
{
    m_mnuMruFiles.DestroyMenu();
    m_mnuMruProjects.DestroyMenu();
}

/**
 * @brief 生成時処理要求
 * @param [in]    lpCreateStruct	初期化パラメータへのポインタ	 	  
 * @return		
 * @note	 
 * @exception   なし
 */
int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (CMDIFrameWndEx::OnCreate(lpCreateStruct) == -1)
    {
        return -1;
    }

    CMenu* pNemu = NULL;
    pNemu = GetMenu();
    CUtil::ConvertMenuString(pNemu->m_hMenu);

    CMFCToolBar::AddToolBarForImageCollection (IDR_LINE_TYPE);
    CMFCToolBar::AddToolBarForImageCollection (IDR_TEXT_ALIGN);
    CMFCToolBar::AddToolBarForImageCollection (IDR_DIM_TYPE);
    CMFCToolBar::AddToolBarForImageCollection (IDR_TEXT_DATUM);

    //BOOL bNameValid;
  
    CMDITabInfo mdiTabParams;
    mdiTabParams.m_style = CMFCTabCtrl::STYLE_3D_ONENOTE; // 使用可能なその他の視覚スタイル...
    mdiTabParams.m_bActiveTabCloseButton = TRUE;      // タブ領域の右部に [閉じる] ボタンを配置するには、FALSE に設定します
    mdiTabParams.m_bTabIcons = FALSE;    // MDI タブでドキュメント アイコンを有効にするには、TRUE に設定します
    mdiTabParams.m_bAutoColor = TRUE;    // MDI タブの自動色設定を無効にするには、FALSE に設定します
    mdiTabParams.m_bDocumentMenu = TRUE; // タブ領域の右端にあるドキュメント メニューを有効にします
    EnableMDITabbedGroups(TRUE, mdiTabParams);

    if (!m_wndMenuBar.Create(this))
    {
        STD_DBG(_T("Failed to create menubar\n"));
        return -1;      // 作成できませんでした。
    }

    m_wndMenuBar.SetPaneStyle(m_wndMenuBar.GetPaneStyle() | CBRS_SIZE_DYNAMIC | CBRS_TOOLTIPS | CBRS_FLYBY);

    // アクティブになったときメニュー バーにフォーカスを移動しない
    CMFCPopupMenu::SetForceMenuFocus(FALSE);

    // 全てのコマンドを表示する
    m_wndMenuBar.SetShowAllCommands(true);

    if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP |
                                                   CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY |
                                                   CBRS_SIZE_DYNAMIC) ||
        !m_wndToolBar.LoadToolBar(theApp.m_bHiColorIcons ? IDR_MAINFRAME_256 : IDR_MAINFRAME))
    {
        STD_DBG(_T("Failed to create toolbar\n"));
        return -1;      // 作成できませんでした。
    }

    m_wndToolBar.SetName(_T("m_wndToolBar"));

    BOOL bNameValid;
    CString strToolBarName;
    bNameValid = strToolBarName.LoadString(IDS_TOOLBAR_STANDARD);
    ASSERT(bNameValid);
    m_wndToolBar.SetWindowText(GET_STR(STR_TOOLBAR_STANDARD));

	CString strCustomize;
	bNameValid = strCustomize.LoadString(IDS_TOOLBAR_CUSTOMIZE);
	ASSERT(bNameValid);
    m_wndToolBar.EnableCustomizeButton(TRUE, ID_VIEW_CUSTOMIZE, GET_STR(STR_TOOLBAR_CUSTOMIZE));
    //---------------------------------------------------------------------

    DWORD dwStyle =   WS_CHILD|WS_VISIBLE|CBRS_TOP|CBRS_TOOLTIPS|
                      CBRS_FLYBY|CBRS_HIDE_INPLACE|CBRS_SIZE_DYNAMIC|
                      CBRS_GRIPPER | CBRS_BORDER_3D;
    //-------------------------
    // Create Build toolbar:
    //-------------------------
    if (!m_wndToolbarBuild.CreateEx(this, TBSTYLE_FLAT, dwStyle) ||
        !m_wndToolbarBuild.LoadToolBar(IDR_BUILD))
    {
        STD_DBG(_T("Failed to create build toolbar\n"));
        return -1;      // 作成できませんでした。
    }
    m_wndToolbarBuild.SetName(_T("m_wndToolbarBuild"));
    m_wndToolbarBuild.SetWindowText(GET_STR(STR_TOOLBAR_BUILD));
    m_wndToolbarBuild.EnableCustomizeButton(TRUE, ID_VIEW_CUSTOMIZE, GET_STR(STR_TOOLBAR_CUSTOMIZE));
    
    //-------------------------
    // 線ディフォルト値ツールバー::
    //-------------------------
    if (!m_wndToolbarLine.Create(this, dwStyle, ID_VIEW_LINE_TOOLBAR) ||
        !m_wndToolbarLine.LoadToolBar(IDR_LINE))
    {
        STD_DBG(_T("Failed to create build toolbar\n"));
        return -1;      // 作成できませんでした。
    }
    m_wndToolbarLine.SetName(_T("m_wndToolbarLine"));
    m_wndToolbarLine.SetWindowText(GET_STR(STR_TOOLBAR_LINE));
    m_wndToolbarLine.EnableCustomizeButton(TRUE, ID_VIEW_CUSTOMIZE, GET_STR(STR_TOOLBAR_CUSTOMIZE));


    //-------------------------
    // 図形編集ツールバー::
    //-------------------------
    if (!m_wndToolbarEdit.Create(this, dwStyle, ID_VIEW_EDIT_TOOLBAR) ||
        !m_wndToolbarEdit.LoadToolBar(IDR_EDIT))
    {
        STD_DBG(_T("Failed to create build toolbar\n"));
        return -1;      // 作成できませんでした。
    }
    m_wndToolbarEdit.SetName(_T("m_wndToolbarEdit"));
    m_wndToolbarEdit.SetWindowText(GET_STR(STR_TOOLBAR_EDIT));
    m_wndToolbarEdit.EnableCustomizeButton(TRUE, ID_VIEW_CUSTOMIZE, GET_STR(STR_TOOLBAR_CUSTOMIZE));

    //-------------------------
    // 検索ツールバー::
    //-------------------------
    if (!m_wndToolbarFind.Create(this, dwStyle, ID_VIEW_FIND_TOOLBAR) ||
        !m_wndToolbarFind.LoadToolBar(IDR_FIND))
    {
        STD_DBG(_T("Failed to create build toolbar\n"));
        return -1;      // 作成できませんでした。
    }
    m_wndToolbarFind.SetName(_T("m_wndToolbarFind"));
    m_wndToolbarFind.SetWindowText(GET_STR(STR_TOOLBAR_FIND));
    m_wndToolbarFind.EnableCustomizeButton(TRUE, ID_VIEW_CUSTOMIZE, GET_STR(STR_TOOLBAR_CUSTOMIZE));

    //-------------------------
    // スナップツールバー:
    if (!m_wndToolbarSnap.Create(this, dwStyle, ID_VIEW_SNAP_TOOLBAR) ||
        !m_wndToolbarSnap.LoadToolBar(IDR_SNAP))
    {
        STD_DBG(_T("Failed to create build toolbar\n"));
        return -1;      // 作成できませんでした。
    }
    m_wndToolbarSnap.SetName(_T("m_wndToolbarSnap"));
    m_wndToolbarSnap.SetWindowText(GET_STR(STR_TOOLBAR_SNAP));
    m_wndToolbarSnap.EnableCustomizeButton(TRUE, ID_VIEW_CUSTOMIZE, GET_STR(STR_TOOLBAR_CUSTOMIZE));

    //-------------------------
    // 表示位置ツールバー
    if (!m_wndToolbarShowPos.Create(this, dwStyle, ID_VIEW_SHOW_TOOLBAR) ||
        !m_wndToolbarShowPos.LoadToolBar(IDR_SHOW))
    {
        STD_DBG(_T("Failed to create build toolbar\n"));
        return -1;      // 作成できませんでした。
    }
    m_wndToolbarShowPos.SetName(_T("m_wndToolbarShowPos"));
    m_wndToolbarShowPos.SetWindowText(GET_STR(STR_TOOLBAR_SHOW));
    m_wndToolbarShowPos.EnableCustomizeButton(TRUE, ID_VIEW_CUSTOMIZE, GET_STR(STR_TOOLBAR_CUSTOMIZE));
     //-------------------------


    // ユーザー定義のツール バーの操作を許可します:
    InitUserToolbars(NULL, uiFirstUserToolBarId, uiLastUserToolBarId);

    if (!m_wndStatusBar.Create(this))
    {
        STD_DBG(_T("Failed to create Statusbar\n"));
        return -1;      // 作成できない場合
    }
    m_wndStatusBar.SetIndicators(indicators, sizeof(indicators)/sizeof(UINT));


    //フォーマットバー
    if (!CreateFormatBar())
    {
        STD_DBG(_T("Failed to create Formatbar\n"));
        return -1;
    }

    if (!CreateDrawBar())
    {
        STD_DBG(_T("Failed to create Drawbar\n"));
        return -1;
    }           



    m_wndMenuBar.EnableDocking(CBRS_ALIGN_ANY);     // 
    m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);     // 標準
    m_wndFormatBar.EnableDocking(CBRS_ALIGN_ANY);   // 文字フォーマット
    m_wndDrawBar.EnableDocking(CBRS_ALIGN_ANY);     // 描画
    m_wndToolbarBuild.EnableDocking(CBRS_ALIGN_ANY);// ビルド
    m_wndToolbarLine.EnableDocking(CBRS_ALIGN_ANY); // 線定義 
    m_wndToolbarEdit.EnableDocking(CBRS_ALIGN_ANY); // 図形編集
    m_wndToolbarFind.EnableDocking(CBRS_ALIGN_ANY); // 検索
    m_wndToolbarSnap.EnableDocking(CBRS_ALIGN_ANY);
    m_wndToolbarShowPos.EnableDocking(CBRS_ALIGN_ANY);

    EnableDocking(CBRS_ALIGN_ANY);

    DockPane(&m_wndMenuBar);
    m_wndMenuBar.ShowPane(TRUE, FALSE, TRUE);

    //========================
    DockPane(&m_wndToolbarFind);
    m_wndToolbarFind.ShowPane(TRUE, FALSE, TRUE);

    DockPaneLeftOf(&m_wndFormatBar, &m_wndToolbarFind);
    m_wndFormatBar.ShowPane(TRUE, FALSE, TRUE);

    DockPaneLeftOf(&m_wndToolBar, &m_wndFormatBar);
    m_wndToolBar.ShowPane(TRUE, FALSE, TRUE);
   
    //========================
    DockPane(&m_wndToolbarLine);
    m_wndToolbarLine.ShowPane(TRUE, FALSE, TRUE);

    DockPaneLeftOf(&m_wndToolbarEdit, &m_wndToolbarLine);
    m_wndToolbarEdit.ShowPane(TRUE, FALSE, TRUE);

    DockPaneLeftOf(&m_wndDrawBar, &m_wndToolbarEdit);
    m_wndDrawBar.ShowPane(TRUE, FALSE, TRUE);

    //========================
    DockPane(&m_wndToolbarSnap);
    m_wndToolbarSnap.ShowPane(TRUE, FALSE, TRUE);

    //DockPane(&m_wndToolbarBuild);
    DockPaneLeftOf(&m_wndToolbarBuild, &m_wndToolbarSnap);
    m_wndToolbarBuild.ShowPane(TRUE, FALSE, TRUE);

    DockPaneLeftOf(&m_wndToolbarShowPos, &m_wndToolbarSnap);
    m_wndToolbarShowPos.ShowPane(TRUE, FALSE, TRUE);
    //========================


    m_lstWnd.push_back(&m_wndMenuBar);
    m_lstWnd.push_back(&m_wndToolBar);
    m_lstWnd.push_back(&m_wndFormatBar);
    m_lstWnd.push_back(&m_wndDrawBar);
    m_lstWnd.push_back(&m_wndToolbarBuild);
    m_lstWnd.push_back(&m_wndToolbarLine);
    m_lstWnd.push_back(&m_wndToolbarEdit);
    m_lstWnd.push_back(&m_wndToolbarFind);
    m_lstWnd.push_back(&m_wndToolbarSnap);
    m_lstWnd.push_back(&m_wndToolbarShowPos);

    // Visual Studio 2005 スタイルのドッキング ウィンドウ動作を有効にします
    CDockingManager::SetDockingMode(DT_SMART);
    // Visual Studio 2005 スタイルのドッキング ウィンドウの自動非表示動作を有効にします
    EnableAutoHidePanes(CBRS_ALIGN_ANY);

    // メニュー項目イメージ (どの標準ツール バーにもないイメージ) を読み込みます:
    CMFCToolBar::AddToolBarForImageCollection(IDR_MENU_IMAGES, theApp.m_bHiColorIcons ? IDB_MENU_IMAGES_24 : 0);

    // ドッキング ウィンドウを作成します
    if (!CreateDockingWindows())
    {
        STD_DBG(_T("Failed to create DockingWindows\n"));
        return -1;
    }

    CDockablePane* pTabbedBar = NULL;
    m_wndLayer.EnableDocking(CBRS_ALIGN_ANY);
    DockPane(&m_wndLayer);


        m_wndCObject.EnableDocking(CBRS_ALIGN_ANY);
        m_wndLibView.EnableDocking(CBRS_ALIGN_ANY);
        m_wndCObject.DockToWindow(&m_wndLayer, CBRS_ALIGN_TOP);


        m_wndLibView.AttachToTabWnd(&m_wndCObject, DM_SHOW, TRUE , &pTabbedBar);
        m_wndCObject.ShowPane(TRUE, FALSE, TRUE);


    //=================================
    //=================================
    m_wndZoom.EnableDocking(CBRS_ALIGN_ANY);

    m_wndZoom.AttachToTabWnd(&m_wndLayer, DM_SHOW, TRUE , &pTabbedBar);
    m_wndLayer.ShowPane(TRUE, FALSE, TRUE);
    //=================================

    m_wndOutput.EnableDocking(CBRS_ALIGN_ANY);
    m_wndCallstack.EnableDocking(CBRS_ALIGN_ANY);
    m_wndWatch.EnableDocking(CBRS_ALIGN_ANY);
    m_wndPropertyDef.EnableDocking(CBRS_ALIGN_ANY);
    m_wndIoSet.EnableDocking(CBRS_ALIGN_ANY);

    DockPane(&m_wndOutput);

    m_wndCallstack.AttachToTabWnd(&m_wndOutput, DM_SHOW, TRUE , &pTabbedBar);
    m_wndWatch.AttachToTabWnd(&m_wndOutput, DM_SHOW, TRUE , &pTabbedBar);
    m_wndPropertyDef.AttachToTabWnd(&m_wndOutput, DM_SHOW, TRUE , &pTabbedBar);
    m_wndIoSet.AttachToTabWnd(&m_wndOutput, DM_SHOW, TRUE , &pTabbedBar);
    m_wndOutput.ShowPane(TRUE, FALSE, TRUE);



    //=================================
    //=================================
    m_wndProperties.EnableDocking(CBRS_ALIGN_ANY);
    for(int iRef = 0; iRef < m_iIoRefMax; iRef++)
    {
        m_wndIoRef[iRef].EnableDocking(CBRS_ALIGN_ANY);
    }
    m_wndExecMonitor.EnableDocking(CBRS_ALIGN_ANY);


    DockPane(&m_wndProperties);
    m_wndProperties.DockToWindow(&m_wndLayer, CBRS_ALIGN_TOP);


    for(int iRef = 0; iRef < m_iIoRefMax; iRef++)
    {
        m_wndIoRef[iRef].AttachToTabWnd(&m_wndProperties, DM_SHOW, TRUE , &pTabbedBar);

    }
    m_wndExecMonitor.AttachToTabWnd(&m_wndProperties, DM_SHOW, TRUE , &pTabbedBar);
    m_wndProperties.ShowPane(TRUE, FALSE, TRUE);
    //=================================


    // 固定値に基づいてビジュアル マネージャと visual スタイルを設定します
    OnApplicationLook(theApp.m_nAppLook);

    // 拡張ウィンドウ管理ダイアログ ボックスを有効にします
    EnableWindowsDialog(ID_WINDOW_MANAGER, IDS_WINDOWS_MANAGER, TRUE);

    // ツール バーとドッキング ウィンドウ メニューの配置変更を有効にします
    EnablePaneMenu(TRUE, ID_VIEW_CUSTOMIZE, GET_STR(STR_TOOLBAR_CUSTOMIZE), ID_VIEW_TOOLBAR);

    // ツール バーのクイック (Alt キーを押しながらドラッグ) カスタマイズを有効にします
    CMFCToolBar::EnableQuickCustomization();
    if (CMFCToolBar::GetUserImages() == NULL)
    {
        // ユーザー定義のツール バー イメージを読み込みます
        if (m_UserImages.Load(_T(".\\UserImages.bmp")))
        {
            m_UserImages.SetImageSize(CSize(16, 16), FALSE);
            CMFCToolBar::SetUserImages(&m_UserImages);
        }
    }
    // メニューのパーソナル化 (最近使用されたコマンド) を有効にします
    // メニューをクリックしたときに基本コマンドが 1 つ以上表示されるようにします。
    CList<UINT, UINT> lstBasicCommands;

    lstBasicCommands.AddTail(ID_FILE_NEW);
    lstBasicCommands.AddTail(ID_FILE_OPEN);
    lstBasicCommands.AddTail(ID_FILE_SAVE);
    lstBasicCommands.AddTail(ID_FILE_PRINT);
    lstBasicCommands.AddTail(ID_APP_EXIT);

    lstBasicCommands.AddTail(ID_EDIT_CUT);
    lstBasicCommands.AddTail(ID_EDIT_PASTE);
    lstBasicCommands.AddTail(ID_EDIT_UNDO);
    lstBasicCommands.AddTail(ID_APP_ABOUT);

    lstBasicCommands.AddTail(ID_VIEW_STATUS_BAR);
    lstBasicCommands.AddTail(ID_VIEW_TOOLBAR);
    lstBasicCommands.AddTail(ID_VIEW_APPLOOK_OFF_2003);
    lstBasicCommands.AddTail(ID_VIEW_APPLOOK_VS_2005);
    lstBasicCommands.AddTail(ID_VIEW_APPLOOK_OFF_2007_BLUE);
    lstBasicCommands.AddTail(ID_VIEW_APPLOOK_OFF_2007_SILVER);
    lstBasicCommands.AddTail(ID_VIEW_APPLOOK_OFF_2007_BLACK);
    lstBasicCommands.AddTail(ID_VIEW_APPLOOK_OFF_2007_AQUA);
    lstBasicCommands.AddTail(ID_SORTING_SORTALPHABETIC);

    lstBasicCommands.AddTail(ID_SORTING_SORTBYTYPE);
    lstBasicCommands.AddTail(ID_SORTING_SORTBYACCESS);
    lstBasicCommands.AddTail(ID_SORTING_GROUPBYTYPE);

    //FormatBar
    lstBasicCommands.AddTail (ID_CHAR_COLOR);   
    lstBasicCommands.AddTail (ID_FORMAT_FONT);  //ID_FORMAT_FONTは、Afxres.hにて定義

    //DrawBar
    lstBasicCommands.AddTail (ID_MNU_LINE_COLOR);   
    lstBasicCommands.AddTail (ID_MNU_BRUSH_COLOR); 

    //DrawBar
    lstBasicCommands.AddTail (ID_MNU_LINE_WIDTH);   
    lstBasicCommands.AddTail (ID_MNU_L_DIM); 


    CMFCToolBar::SetBasicCommands(lstBasicCommands);


	// ウィンドウ タイトル バーでドキュメント名とアプリケーション名の順序を切り替えます。これにより、
	//ドキュメント名を縮小版で表示できるため、タスク バーの使用性が向上します。
	ModifyStyle(0, FWS_PREFIXTITLE);
 	//LoadBarState(_T("General"));

    return 0;
}


/**
* @brief   フォーマットバー生成
* @param   なし      
* @return	なし	
* @Note
*/
BOOL CMainFrame::CreateFormatBar()
{
    if (!m_wndFormatBar.Create(this,
        WS_CHILD|WS_VISIBLE|CBRS_TOP|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE|CBRS_SIZE_DYNAMIC|
        CBRS_GRIPPER | CBRS_BORDER_3D,
        ID_VIEW_FORMATBAR) ||
        !m_wndFormatBar.LoadToolBar (IDR_FORMATBAR))
    {
        DB_PRINT(_T("Failed to create FormatBar\n"));
        return FALSE;      // fail to create
    }

    CString str;

    str = GET_SYS_STR(STR_MNU_SET_FORMATTING).c_str();
    m_wndFormatBar.SetWindowText(str);

    m_wndFormatBar.EnableCustomizeButton (TRUE, ID_VIEW_CUSTOMIZE, 
        GET_STR(STR_TOOLBAR_CUSTOMIZE));
    return TRUE;
}

/**
* @brief   描画バー生成
* @param    	 	  
* @return		
* @note	
*/
BOOL CMainFrame::CreateDrawBar()
{
    if (!m_wndDrawBar.Create(this,
        WS_CHILD|WS_VISIBLE|CBRS_TOP|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE|CBRS_SIZE_DYNAMIC|
        CBRS_GRIPPER | CBRS_BORDER_3D,
        ID_VIEW_DRAWBAR))
    {
        DB_PRINT(_T("Failed to create DrawBar\n"));
        return FALSE;      // fail to create
    }
    m_wndDrawBar.LoadToolBar (IDR_DRAW_BAR);

    CString str;
    str = GET_SYS_STR(STR_MNU_SET_DRAWING).c_str();
    m_wndDrawBar.SetWindowText(str);

    m_wndDrawBar.EnableCustomizeButton (TRUE, ID_VIEW_CUSTOMIZE, 
        GET_STR(STR_TOOLBAR_CUSTOMIZE));
    return TRUE;
}

/**
* @brief   ウィンドウ作成前処理要求
* @param   [in]    cs	 	  
* @return	TRUE:成功	
* @note
* @exception   なし
*/
BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
    if( !CMDIFrameWndEx::PreCreateWindow(cs) )
        return FALSE;
    // TODO: この位置で CREATESTRUCT cs を修正して Window クラスまたはスタイルを
    //  修正してください。

    return TRUE;
}

/**
* @brief   ドッキングウインドウ生成
* @return		
* @note	 
* @exception   なし
*/
BOOL CMainFrame::CreateDockingWindows()
{
    // ライブラリ ビューを作成します
    DWORD dwStyle = WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN |  CBRS_FLOAT_MULTI;
    if (!m_wndLibView.Create(GET_STR(STR_LIB_VIEW), this, CRect(0, 0, 200, 200), TRUE, ID_VIEW_LIB_VIEW, 
        dwStyle | CBRS_LEFT))
    {
        STD_DBG(_T("m_wndLibView.Create"));
        return FALSE; // 作成できませんでした
    }
    m_lstWnd.push_back(&m_wndLibView);


    // オブジェクト ビューを作成します
    if (!m_wndCObject.Create(GET_STR(STR_OBJECT_VIEW), this, CRect(0, 0, 200, 200), TRUE, ID_VIEW_OBJECT_VIEW, 
        dwStyle | CBRS_LEFT))
    {
        STD_DBG(_T("m_wndCObject.Create"));
        return FALSE; // 作成できませんでした
    }
    m_lstWnd.push_back(&m_wndCObject);

    // 出力ウィンドウを作成します
    CString strOutputWnd;
    strOutputWnd = GET_SYS_STR(STR_WND_OUTPUT).c_str();
    ASSERT(strOutputWnd != _T(""));
    if (!m_wndOutput.Create(strOutputWnd, this, CRect(0, 0, 100, 100), TRUE, ID_VIEW_OUTPUTWND, 
        dwStyle | CBRS_BOTTOM))
    {
        STD_DBG(_T("m_wndOutput.Create"));
        return FALSE; // 作成できませんでした
    }
    m_lstWnd.push_back(&m_wndOutput);

    // プロパティ ウィンドウを作成します
    CString strPropertiesWnd;
    strPropertiesWnd = GET_STR(STR_WND_PROPERTIES);
    ASSERT(strPropertiesWnd != _T(""));
    if (!m_wndProperties.Create(strPropertiesWnd, this, CRect(0, 0, 200, 200), TRUE, IDR_PROPERTIES, 
        dwStyle |CBRS_RIGHT))
    {
        STD_DBG(_T("m_wndProperties.Create"));
        return FALSE; // 作成できませんでした
    }
    m_lstWnd.push_back(&m_wndProperties);

    // レイヤーウインドウを作成します
    CString strLayerWnd;
    strLayerWnd = GET_STR(STR_WND_LAYER);
    ASSERT(strLayerWnd != _T(""));
    if (!m_wndLayer.Create(strLayerWnd, this, 
        CRect(0, 0, 200, 200), TRUE, ID_VIEW_LAYER, 
        dwStyle |CBRS_LEFT))
    {
        STD_DBG(_T("m_wndLayer.Create"));
        return FALSE; // 作成できませんでした
    }
    m_lstWnd.push_back(&m_wndLayer);

    // ズームウインドウを作成します
    CString strZoomWnd;
    strZoomWnd = GET_STR(STR_WND_ZOOM);
    ASSERT(strZoomWnd != _T(""));
    if (!m_wndZoom.Create(strZoomWnd, this, 
        CRect(0, 0, 200, 200), TRUE, ID_VIEW_ZOOM, 
        dwStyle |CBRS_LEFT))
    {
        STD_DBG(_T("m_wndZoom.Create"));
        return FALSE; // 作成できませんでした
    }
    m_lstWnd.push_back(&m_wndZoom);

    // IO設定ウインドウを作成します
    CString strIoWnd;
    strIoWnd = GET_STR(STR_WND_IO_SET);
    m_wndIoSet.SetWindowIndex(0);
    ASSERT(strIoWnd != _T(""));
    if (!m_wndIoSet.Create(strIoWnd, this, 
        CRect(0, 0, 200, 200), TRUE, ID_IO_SET, 
        dwStyle |CBRS_BOTTOM))
    {
        STD_DBG(_T("m_wndIoSet.Create"));
        return FALSE; // 作成できませんでした
    }
    m_lstWnd.push_back(&m_wndIoSet);

    // IO参照ウインドウを作成します
    for(int iRef = 0; iRef < m_iIoRefMax; iRef++)
    {
        CString strIoRefWnd;
        if (iRef == 0)
        {
            m_wndIoRef[iRef].SetCurrnetMode();
            strIoRefWnd = GET_STR(STR_WND_IO_CONNECT);
        }
        else
        {
            strIoRefWnd.Format(_T("%s %d"), GET_STR(STR_WND_IO_REF),
                iRef);
            m_wndIoRef[iRef].SetRefMode();
        }
        m_wndIoRef[iRef].SetWindowIndex(iRef + 1);

        if (!m_wndIoRef[iRef].Create(strIoRefWnd, this, 
            CRect(0, 0, 200, 200), TRUE, ID_IO_REF_1 + iRef, 
            dwStyle |CBRS_RIGHT))
        {
            STD_DBG(_T("m_wndIoRef.Create"));
            return FALSE; // 作成できませんでした
        }
        m_lstWnd.push_back(&m_wndIoRef[iRef]);
    }


    // コールスタックウインドウを作成します
    CString strCallStackWnd;
    strCallStackWnd = GET_STR(STR_WND_CALL_STACK);
    ASSERT(strCallStackWnd != _T(""));
    if (!m_wndCallstack.Create(strCallStackWnd, this, 
        CRect(0, 0, 200, 200), TRUE, ID_CALLSTACK, 
        dwStyle |CBRS_BOTTOM))
    {
        STD_DBG(_T("m_wndCallstack.Create"));
        return FALSE; // 作成できませんでした
    }
    m_lstWnd.push_back(&m_wndCallstack);

    // ウォッチウインドウを作成します
    CString strWatchWnd;
    strWatchWnd = GET_STR(STR_WND_WATCH);
    ASSERT(strWatchWnd != _T(""));
    if (!m_wndWatch.Create(strWatchWnd, this, 
        CRect(0, 0, 200, 200), TRUE, ID_DEBUG_WATCH, 
        dwStyle |CBRS_BOTTOM))
    {
        STD_DBG(_T("m_wndWatch.Create"));
        return FALSE; // 作成できませんでした
    }
    m_lstWnd.push_back(&m_wndWatch);

    // 実行モニターウインドウを作成します
    CString strExecMonitorWnd;
    strExecMonitorWnd = GET_STR(STR_WND_EXEC_MONITOR);
    ASSERT(strExecMonitorWnd != _T(""));
    if (!m_wndExecMonitor.Create(strExecMonitorWnd, this, 
        CRect(0, 0, 200, 200), TRUE, ID_EXEC_MONITOR, 
        dwStyle |CBRS_RIGHT))
    {
        STD_DBG(_T("m_wndExecMonitor.Create"));
        return FALSE; // 作成できませんでした
    }
    m_lstWnd.push_back(&m_wndExecMonitor);

    // プロパティ定義ウインドウを作成します
    CString strPropDefWnd;
    strPropDefWnd = GET_STR(STR_WND_PROP_DEF);
    ASSERT(strPropDefWnd != _T(""));
    if (!m_wndPropertyDef.Create(strPropDefWnd, this, 
        CRect(0, 0, 200, 200), TRUE, ID_PROP_DEF, 
        dwStyle |CBRS_RIGHT))
    {
        STD_DBG(_T("m_wndPropertyDef.Create"));
        return FALSE; // 作成できませんでした
    }
    m_lstWnd.push_back(&m_wndPropertyDef);

    SetDockingWindowIcons(theApp.m_bHiColorIcons);
    return TRUE;
}

/**
* @brief   ドッキングウインドウアイコン設定
* @param   [in]    bHiColorIcons	多色アイコン使用有無	 	  
* @return	なし
* @note	 
* @exception   なし
*/
void CMainFrame::SetDockingWindowIcons(BOOL bHiColorIcons)
{
    HICON hFileViewIcon = (HICON) ::LoadImage(::AfxGetResourceHandle(), 
        MAKEINTRESOURCE(bHiColorIcons ? IDI_FILE_VIEW_HC : IDI_FILE_VIEW), 
        IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), 0);
    m_wndCObject.SetIcon(hFileViewIcon, FALSE);

    HICON hClassViewIcon = (HICON) ::LoadImage(::AfxGetResourceHandle(), 
        MAKEINTRESOURCE(bHiColorIcons ? IDI_CLASS_VIEW_HC : IDI_CLASS_VIEW), 
        IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), 0);
    m_wndLibView.SetIcon(hClassViewIcon, FALSE);

    HICON hOutputBarIcon = (HICON) ::LoadImage(::AfxGetResourceHandle(),
        MAKEINTRESOURCE(bHiColorIcons ? IDI_OUTPUT_WND_HC : IDI_OUTPUT_WND), 
        IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), 0);
    m_wndOutput.SetIcon(hOutputBarIcon, FALSE);

    HICON hPropertiesBarIcon = (HICON) ::LoadImage(::AfxGetResourceHandle(), 
        MAKEINTRESOURCE(bHiColorIcons ? IDI_PROPERTIES_WND_HC : IDI_PROPERTIES_WND), 
        IMAGE_ICON, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON), 0);
    m_wndProperties.SetIcon(hPropertiesBarIcon, FALSE);

    UpdateMDITabbedBarsIcons();
}

// CMainFrame 診断

#ifdef _DEBUG
/**
* @brief   オブジェクト内部状態チェック
* @param   なし
* @return  なし
* @note     
*/
void CMainFrame::AssertValid() const
{
    CMDIFrameWndEx::AssertValid();
}

/**
* @brief   ダンプ処理
* @param   [in]    dc	ダンプ用の診断ダンプ コンテキスト 
* @return  なし
* @note    
*/
void CMainFrame::Dump(CDumpContext& dc) const
{
    CMDIFrameWndEx::Dump(dc);
}
#endif //_DEBUG


/**
* @brief   ウインドウマネージャ表示要求
* @return		なし
* @note	フレームワークから
ウインドウ->ウインドウから 
* @exception   なし
*/
void CMainFrame::OnWindowManager()
{
    ShowWindowsDialog();
}

/**
* @brief   表示カスタマイズ要求
* @return		なし
* @note	フレームワークから呼び出し
表示->ツールバーとドッキングウインドウ->カスタマイズ 
* @exception   なし
*/
void CMainFrame::OnViewCustomize()
{
    CMFCToolBarsCustomizeDialog* pDlgCust = new CMFCToolBarsCustomizeDialog(this, TRUE /* メニューをスキャンします*/);

    //------------------------------------------------
    pDlgCust->AddToolBar (GET_SYS_STR(STR_MNU_SET_FORMATTING).c_str(), 
        IDR_FORMATBAR);

    CMFCToolBarFontComboBox* pFontButton = m_wndFormatBar.CreateFontComboButton ();
    pDlgCust->ReplaceButton (IDC_FONTNAME, *pFontButton);
    delete pFontButton;

    CToolBarFontSizeCombo comboButtonFontSize (IDC_FONTSIZE, 
        GetCmdMgr ()->GetCmdImage (IDC_FONTSIZE, FALSE),
        WS_TABSTOP|WS_VISIBLE|WS_TABSTOP|WS_VSCROLL|CBS_DROPDOWN,
        10*m_wndFormatBar.m_szBaseUnits.cx + 10);
    pDlgCust->ReplaceButton (IDC_FONTSIZE, comboButtonFontSize);

    CMFCColorMenuButton* pColorButton = m_wndFormatBar.CreateColorButton ();
    pDlgCust->ReplaceButton (ID_CHAR_COLOR, *pColorButton);
    delete pColorButton;

    CCustomMenuButton* pDatumButton = m_wndFormatBar.CreateDatumButton ();
    pDlgCust->ReplaceButton (ID_CHAR_DATUM_TL, *pDatumButton);
    delete pDatumButton;

    CCustomMenuButton* pAlignButton = m_wndFormatBar.CreateAlignButton ();
    pDlgCust->ReplaceButton (ID_CHAR_ALIGN_L, *pAlignButton);
    delete pAlignButton;

    //------------------------------------------------
    pDlgCust->AddToolBar (GET_STR(STR_MNU_SET_DRAWING), IDR_DRAW_BAR);


    //------------------
    //  DRAW 
    //------------------
    {
	    CCustomMenuButton* pDimArrreyButton   = m_wndDrawBar.CreateDimArrayButton ();
	    pDlgCust->ReplaceButton (ID_MNU_DIM, *pDimArrreyButton);
	    delete pDimArrreyButton;
    }
    //------------------

    pDlgCust->EnableUserDefinedToolbars();
    pDlgCust->Create();

}

/**
* @brief   新しいツール バーを作成
* @param [in]    wp	未使用	 	  
* @param [in]    lp	ツール バーのタイトル バーのテキストへのポインタ	 	  
* @return		
* @note	* @retval
* @note フレームワークから呼び出し 
* @exception   なし
*/
LRESULT CMainFrame::OnToolbarCreateNew(WPARAM wp,LPARAM lp)
{
    LRESULT lres = CMDIFrameWndEx::OnToolbarCreateNew(wp,lp);
    if (lres == 0)
    {
        return 0;
    }

    CMFCToolBar* pUserToolbar = (CMFCToolBar*)lres;
    ASSERT_VALID(pUserToolbar);

    pUserToolbar->EnableCustomizeButton(TRUE, ID_VIEW_CUSTOMIZE, GET_STR(STR_TOOLBAR_CUSTOMIZE));
    return lres;
}



/**
* @brief   アプリケーション外観変更要求
* @param [in]    id	外観ID	ID_VIEW_APPLOOK_WIN_2000〜  ..._OFF_2007_AQUA	 	  
* @return		なし
* @note	 
* @exception   なし
*/
void CMainFrame::OnApplicationLook(UINT id)
{
    CWaitCursor wait;

    theApp.m_nAppLook = id;

    switch (theApp.m_nAppLook)
    {
    case ID_VIEW_APPLOOK_WIN_2000:
        CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManager));
        break;

    case ID_VIEW_APPLOOK_OFF_XP:
        CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerOfficeXP));
        break;

    case ID_VIEW_APPLOOK_WIN_XP:
        CMFCVisualManagerWindows::m_b3DTabsXPTheme = TRUE;
        CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows));
        break;

    case ID_VIEW_APPLOOK_OFF_2003:
        CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerOffice2003));
        CDockingManager::SetDockingMode(DT_SMART);
        break;

    case ID_VIEW_APPLOOK_VS_2005:
        CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerVS2005));
        CDockingManager::SetDockingMode(DT_SMART);
        break;

    default:
        switch (theApp.m_nAppLook)
        {
        case ID_VIEW_APPLOOK_OFF_2007_BLUE:
            CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_LunaBlue);
            break;

        case ID_VIEW_APPLOOK_OFF_2007_BLACK:
            CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_ObsidianBlack);
            break;

        case ID_VIEW_APPLOOK_OFF_2007_SILVER:
            CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_Silver);
            break;

        case ID_VIEW_APPLOOK_OFF_2007_AQUA:
            CMFCVisualManagerOffice2007::SetStyle(CMFCVisualManagerOffice2007::Office2007_Aqua);
            break;
        }

        CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerOffice2007));
        CDockingManager::SetDockingMode(DT_SMART);
    }

    //-----------------------
    // Reload toolbar images:
    //-----------------------
    //CMFCToolBar::ResetAllImages ();

    /*
    m_wndToolBar.LoadBitmap (
    theApp.m_bHiColorIcons && nBitsPerPixel > 16 ? IDB_MAINFRAME_HC : IDR_MAINFRAME);

    m_wndFormatBar.LoadBitmap (IDR_FORMATBAR);

    CMFCToolBar::AddToolBarForImageCollection (IDR_TOOLBAR_IMAGES, 
    theApp.m_bHiColorIcons && nBitsPerPixel > 16 ? IDR_TOOLBAR_IMAGES_HC : 0);
    */
    //CMFCToolBar::AddToolBarForImageCollection (IDR_TEXT_ALIGN, IDR_TEXT_ALIGN);

    //CMFCToolBar::AddToolBarForImageCollection (IDR_LINE_TYPE, IDR_LINE_TYPE);

    //CMFCToolBar::AddToolBarForImageCollection (IDR_DIM_TYPE, IDR_DIM_TYPE);
    
    //RecalcLayout ();

    //-----------------------


    RedrawWindow(NULL, NULL, RDW_ALLCHILDREN | RDW_INVALIDATE | RDW_UPDATENOW | RDW_FRAME | RDW_ERASE);

    theApp.WriteInt(_T("ApplicationLook"), theApp.m_nAppLook);
}

/**
* @brief   アプリケーション外観変更更新通知
* @param [in]    pCmdUI	コマンドのUIに対するID	 	  
* @return		なし
*/
void CMainFrame::OnUpdateApplicationLook(CCmdUI* pCmdUI)
{
    pCmdUI->SetRadio(theApp.m_nAppLook == pCmdUI->m_nID);
}

/**
* @brief   フレーム ウィンドウ作成
* @param [in]    nIDResource	    フレームウインドウ リソースID	 	  
* @param [in]    dwDefaultStyle	フレーム ウィンドウ スタイル	 	  
* @param [in]    pParentWnd    	親ウインドウへのポインタ	 	  
* @param [in]    pContext	CCreateContext 構造体へのポインタ	NULL指定可	 	  
* @return		
* @note	 
* @exception   なし
*/
BOOL CMainFrame::LoadFrame(UINT nIDResource, DWORD dwDefaultStyle, CWnd* pParentWnd, CCreateContext* pContext) 
{
    // 基本クラスが実際の動作を行います。

    if (!CMDIFrameWndEx::LoadFrame(nIDResource, dwDefaultStyle, pParentWnd, pContext))
    {
        return FALSE;
    }

    // すべてのユーザー定義ツール バーのボタンのカスタマイズを有効にします
	BOOL bNameValid;
	CString strCustomize;
	bNameValid = strCustomize.LoadString(IDS_TOOLBAR_CUSTOMIZE);
	ASSERT(bNameValid);

    for (int i = 0; i < iMaxUserToolbars; i ++)
    {
        CMFCToolBar* pUserToolbar = GetUserToolBarByIndex(i);
        if (pUserToolbar != NULL)
        {
            pUserToolbar->EnableCustomizeButton(TRUE, ID_VIEW_CUSTOMIZE, GET_STR(STR_TOOLBAR_CUSTOMIZE));
        }
    }
    return TRUE;
}

/**
* @brief   メッセージ出力要求
* @param   [in]    iWin	    出力ウインドウID
* @param   [in]    pMessage	メッセージ	 	  
* @return	常に０	
* @note	 
* @exception   なし
*/
LRESULT CMainFrame::OnPrintMessage(WPARAM iWin, LPARAM pMessage)
{
    E_OUTPUT_WINDOW eWin = static_cast<E_OUTPUT_WINDOW>(iWin);
    TCHAR* pMsg = reinterpret_cast<TCHAR*>(pMessage);

    m_wndOutput.PrintOut(eWin,  pMsg);
    PopOutputBuffer(eWin);
    return 0;
}


/**
* @brief   View生成通知
* @param   [in]    iWin	    View種別ID
* @param   [in]    pWnd	    ウインドウ	 	  
* @return	常に０	
* @note	View生成完了後に各ウインドウに通知する 
* @exception   なし
*/
LRESULT CMainFrame::OnViewCreate(WPARAM iType, LPARAM pMessage)
{
    return 0;
}

/**
* @brief   View更新通知
* @param   [in]    iWin	    View種別ID
* @param   [in]    pWnd	    ウインドウ	 	  
* @return	常に０	
* @note	Viewで選択変更があった時に通知する 
* @exception   なし
*/
LRESULT CMainFrame::OnViewSelchange(WPARAM iType, LPARAM pItem)
{
    return 0;
}


/**
* @brief   出力ウインドウクリア
* @param   [in]    iWin	    View種別ID
* @param   [in]    lparam	    使用しない	 	  
* @return	常に０	
* @note	
*/
LRESULT CMainFrame::OnOutputClear(WPARAM iWin, LPARAM lparam)
{
    E_OUTPUT_WINDOW eWin = static_cast<E_OUTPUT_WINDOW>(iWin);
    m_wndOutput.ClearWindow(eWin);
    return 0;
}

/**
* @brief   出力ウインドウ選択
* @param   [in]    iWin	    View種別ID
* @param   [in]    lparam	    使用しない	 	  
* @return	常に０	
* @note	
*/
LRESULT CMainFrame::OnOutputSel(WPARAM iWin, LPARAM lparam)
{
    E_OUTPUT_WINDOW eWin = static_cast<E_OUTPUT_WINDOW>(iWin);
    ShowPane(&m_wndOutput, true, false, false);
    m_wndOutput.SelectWindow(eWin);
    return 0;
}

/**
* @brief   ポップアップ メニュー表示発生
* @param   [in]    pMenuPopup	ポップアップメニュー
* @return	ポップアップ メニューが表示されている場合は TRUE。それ以外の場合は FALSE。 
* @note	フレームワークがポップアップ メニューを表示すると、
* @note	このメソッドを派生クラス内でオーバーライドして、
* @note	カスタム コードを実行します。たとえば、ポップアップ 
* @note	メニューのコマンドの背景色を変更する場合に、このメソ
* @note	ッドをオーバーライドします。
ときに、フレームワークによって呼び出されます。 
*/
BOOL CMainFrame::OnShowPopupMenu (CMFCPopupMenu* pMenuPopup)
{
    BOOL bRes = CMDIFrameWndEx::OnShowPopupMenu (pMenuPopup);

    if (pMenuPopup != NULL && !pMenuPopup->IsCustomizePane())
    {
        AdjustObjectSubmenu (pMenuPopup);
        AdjustColorsMenu (pMenuPopup, ID_CHAR_COLOR);
        AdjustColorsMenu (pMenuPopup, ID_MNU_LINE_COLOR);
        AdjustColorsMenu (pMenuPopup, ID_MNU_BRUSH_COLOR);

    }
    return bRes;
}

/**
* @brief   ColorMenu調整
* @param   [in]    pMenuPopup	ポップアップメニュー
* @param   [in]    uiID	    メニューID	 	  
* @return	常に０	
* @note	 
*/
void CMainFrame::AdjustColorsMenu (CMFCPopupMenu* pMenuPopup, UINT uiID)
{
    CMFCPopupMenuBar* pMenuBar = pMenuPopup->GetMenuBar ();
    ASSERT (pMenuBar != NULL);
    if (pMenuBar == NULL)
    {
        return;
    }

    int iIndex = pMenuBar->CommandToIndex (uiID);
    if (iIndex < 0)
    {
        return;
    }

    if (DYNAMIC_DOWNCAST (CMFCColorMenuButton, pMenuBar->GetButton (iIndex)) != NULL)
    {
        return;
    }

    if (uiID == ID_CHAR_COLOR)
    {
        CMFCColorMenuButton* pColorButton = m_wndFormatBar.CreateColorButton ();
        pMenuBar->ReplaceButton (ID_CHAR_COLOR, *pColorButton, TRUE);
        delete pColorButton;
    }
    if (uiID == ID_MNU_LINE_COLOR)
    {
        CMFCColorMenuButton* pColorButton = m_wndDrawBar.CreateColorButton (uiID);
        pMenuBar->ReplaceButton (uiID, *pColorButton, TRUE);
        delete pColorButton;
    }
    if (uiID == ID_MNU_BRUSH_COLOR)
    {
        CMFCColorMenuButton* pColorButton = m_wndDrawBar.CreateColorButton (uiID);
        pMenuBar->ReplaceButton (uiID, *pColorButton, TRUE);
        delete pColorButton;
    }
}


/**
* @brief   SubMenu調整
* @param   [in]    pMenuPopup	ポップアップメニュー
* @return	常に０	
* @note	 
*/
void CMainFrame::AdjustObjectSubmenu (CMFCPopupMenu* pMenuPopup)
{
    ASSERT (pMenuPopup != NULL);
    if (pMenuPopup == NULL)
    {
        return;
    }

    CMFCPopupMenuBar* pMenuBar = pMenuPopup->GetMenuBar ();
    ASSERT (pMenuBar != NULL);
    if (pMenuBar == NULL)
    {
        return;
    }

  
    //-------------------------------------------------------------------
    // Replace all "line style" menu items by CLineStyleMenuItem objects:
    //-------------------------------------------------------------------
    for (UINT uiLineStyleCmd  = ID_LINE_SOLID; 
              uiLineStyleCmd <= ID_LINE_DASHDOTDOT; 
              uiLineStyleCmd++)
    {
        int iIndex = pMenuPopup->GetMenuBar ()->CommandToIndex (uiLineStyleCmd);
        if (iIndex >= 0)
        {
            pMenuBar->m_bDisableSideBarInXPMode = TRUE;
            //----------------------------
            // Obtain item text and style:
            //----------------------------
            CString strText	= pMenuBar->GetButtonText (iIndex);
            UINT uiStyle	= pMenuBar->GetButtonStyle (iIndex);

            //------------------
            // Calc. line width:
            //------------------
            int nWidth = uiLineStyleCmd - ID_LINE_WIDTH_1 + 1;

            //-------------------
            // Replace menu item:
            //-------------------

            pMenuBar->ReplaceButton (uiLineStyleCmd, 
                CLineWidthMenuItem(nWidth,
                uiLineStyleCmd,
                strText,
                uiStyle & TBBS_CHECKED));
        }
    }

    //-------------------------------------------------------------------
    // Replace all "line style" menu items by CLineStyleMenuItem objects:
    //-------------------------------------------------------------------
    for (UINT uiLineStyleCmd  = ID_LINE_WIDTH_1; 
              uiLineStyleCmd <= ID_LINE_WIDTH_10; 
              uiLineStyleCmd++)
    {
        int iIndex = pMenuPopup->GetMenuBar ()->CommandToIndex (uiLineStyleCmd);
        if (iIndex >= 0)
        {
            pMenuBar->m_bDisableSideBarInXPMode = TRUE;
            //----------------------------
            // Obtain item text and style:
            //----------------------------
            CString strText	= pMenuBar->GetButtonText (iIndex);
            UINT uiStyle	= pMenuBar->GetButtonStyle (iIndex);

            //------------------
            // Calc. line width:
            //------------------
            int nWidth = uiLineStyleCmd - ID_LINE_WIDTH_1 + 1;

            //-------------------
            // Replace menu item:
            //-------------------

            pMenuBar->ReplaceButton (uiLineStyleCmd, 
                CLineWidthMenuItem(nWidth,
                uiLineStyleCmd,
                strText,
                uiStyle & TBBS_CHECKED));
        }
    }
}


/**
*  @brief  ウインドウプロシジャ
*  @param    [in]  message  処理される Windows メッセージ
*  @param    [in]  wParam   メッセージの処理で使う付加情報1
*  @param    [in]  lParam   メッセージの処理で使う付加情報2
*  @retval   メッセージに依存する値
*  @note   
*/
LRESULT CMainFrame::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
    if ((message >= WM_VIEW_CREATE) &&
        (message <  WM_VIEW_END))
    {
        //DB_PRINT(_T("CMainFrame::WindowProc(%x) \n"), message);
        //上記メッセージを各Viewに再配信する
        std::vector<CWnd*>::iterator it;
        for (it  = m_lstWnd.begin();
            it != m_lstWnd.end();
            it++)
        {
            if ((*it)->m_hWnd == NULL)
            {
                continue;
            }
            (*it)->SendMessage(message, wParam, lParam);
        }

        foreach(CObjectDefView* pView, *(THIS_APP->GetViewList()))
        {
            auto pWnd = pView->GetWindow();
            if (!pWnd->IsWindowVisible())
            {
                continue;
            }
        
            pWnd->SendMessage(message, wParam, lParam);
        }
    }

    else if (message == WM_SHOW_DRAWING)
    {
        boost::uuids::uuid* pUuid = reinterpret_cast<boost::uuids::uuid*>(wParam);
        THIS_APP->OpenNewView(*pUuid);
        return 0;
    }
    else if (message == WM_SHOW_EDITOR)
    {
        boost::uuids::uuid* pUuid = reinterpret_cast<boost::uuids::uuid*>(wParam);
        StdString*           pScriptName = reinterpret_cast<StdString*>(lParam);
        THIS_APP->OpenNewEditor(*pUuid, pScriptName->c_str());
        delete pScriptName;
        return 0;
    }
    else if (message == WM_UPDATE_EXEC_VIWE)
    {
#ifdef _DEBUG
        UpdateExecView();
#endif
        return 0;
    }
    else if (message == WM_UPDATE_EXEC_PANE)
    {
#ifdef _DEBUG
        UpdateExecPane();
#endif
        return 0;
    }
    else if (message == WM_CLOSE_ALL_EXEC_VIEW)
    {
        THIS_APP->CloseAllExecView();
        return 0;
    }

    if (message == WM_VIEW_SAVE_START)
    {
        std::set<CWnd*> lstEditor;
        THIS_APP->GetEditorList(&lstEditor);
        foreach(CWnd* pWnd, lstEditor)
        {
            if (pWnd->m_hWnd == NULL)
            {
                continue;
            }
            pWnd->SendMessage(message, wParam, lParam);

        }
        return 0;
    }

    return CMDIFrameWndEx::WindowProc(message, wParam, lParam);
}


LRESULT CMainFrame::OnAfterTaskbarActivate(WPARAM, LPARAM lp)
{
    HWND hwndMDIChild = (HWND)lp;
    if (hwndMDIChild != NULL && ::IsWindow(hwndMDIChild))
    {
        ::SetFocus(hwndMDIChild);
    }
return 0;
}

LRESULT CMainFrame::OnResetToolbar(WPARAM wp, LPARAM lp)
{
    UINT uiToolBarId = (UINT) wp;
    if ((uiToolBarId == IDR_MAINFRAME)||
        (uiToolBarId == IDR_MAINFRAME_256))
    {
        //m_wndToolBar.ReplaceButton (ID_TB, CToolBarLineComboBox());

    }
    return 0;
}


/**
*  @brief  メインオブジェクト取得
*  @param  なし
*  @retval メイン描画オブジェクト
*  @note   廃止する方向で検討
*/
/*
CPartsDef* CMainFrame::GetMainObject()
{
return  m_psProject->GetMainObject();
}
*/

/**
 *  @brief  プロジェクト取得
 *  @param  [in] eType プロジェクト種別
 *  @retval プロジェクト
 *  @note   
 */
std::weak_ptr<CProjectCtrl> CMainFrame::GetProject(VIEW_COMMON::E_PROJ_TYPE eType)
{
    if (eType == VIEW_COMMON::E_PROJ_LIB)
    {
        return m_psLibrary;
    }
    else
    {
        return  m_psProject;
    }
}

/**
 *  @brief  プロジェクト取得
 *  @param  [in] eType プロジェクト種別
 *  @retval プロジェクト
 *  @note   
 */
CExecCtrl* CMainFrame::GetExecCtrl()
{
    return m_pCtrl.get();
}


/**
 *  @brief  すべて閉じる
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CMainFrame::CloseAll()
{

    //開いているビューを閉じる
    //
    // MockSketch
    // Editor       ->  
    //
    //  閉じるときの流れ
    //  ビューに対し WM_CLOSEを投げる
    //     CFrameWnd::OnClose
    //           ->pDocument->CanCloseFrame
    //                 XXXDoc->SaveModified() //Viewがあるときに呼ばれる
    //           MainFrameに対してCloseした場合は 終了してしまいます..多分
    //                     pApp->HideApplication();
    //                     pApp->CloseAllDocuments(FALSE);
    //          ドキュメントにm_bAutoDeleteが設定している場合は
    //              他に開いているViewが無ければOnCloseDocumentを呼び出します
    //         pDocument->PreCloseFrameが呼ばれます
    //        DestroyWindowを実行します
    //         
    THIS_APP->CloseAllView();

    SaveUserLibrary();

    //サブウインドウを初期化する
    SendMessage(WM_VIEW_CLEAR_ALL);

    LoadUserLibrary();

}


/**
*  @brief  ファイル保存処理
*  @param  なし
*  @retval なし
*  @note   上書き保存
*/
BOOL CMainFrame::DoProjectSave(VIEW_COMMON::E_PROJ_TYPE eType)
{
    //新規に作成したファイルはファイルパスは未設定 
    //CPartsDef* pMainObj = GetMainObject();
    //StdString strPath = pMainObj->GetFilePath();

    std::shared_ptr<CProjectCtrl> pProject;
    pProject = GetProject(eType).lock();
    StdString strPath =  pProject->GetFilePath();

    DWORD dwAttrib = GetFileAttributes(strPath.c_str());
    if (dwAttrib & FILE_ATTRIBUTE_READONLY)
    {
        if (!DoProjectSaveAs(eType, NULL))
        {
            STD_DBG(_T("Warning: File save with new name failed.\n"));
            return FALSE;
        }
    }
    else
    {
        if (!DoProjectSaveAs(eType, strPath.c_str()))
        {
            STD_DBG(_T("Warning: File save failed.\n"));
            return FALSE;
        }
    }
    return TRUE;
}

//現在のファイルを上書き保存
BOOL CMainFrame::DoCurrentFileSave()
{
    STD_DBG(_T("未実装\n"));
    return FALSE;
}


//名前を変更して現在のファイルを保存
BOOL CMainFrame::DoCurrentFileAs(LPCTSTR lpszPathName)
{
    STD_DBG(_T("未実装\n"));
    return FALSE;
}

//現在のファイル変更時
BOOL CMainFrame::SaveModifiedCurrent()
{
    STD_DBG(_T("未実装\n"));
    return FALSE;
}

//!< 閉じる
void CMainFrame::CloseCurrent()
{
    STD_DBG(_T("未実装\n"));
}

//!< ファイル保存・読み込み表示
BOOL CMainFrame::DoPromptFileName(CString& fileName, 
                          bool bReplace, 
                               DWORD lFlags, 
                               BOOL bOpenFileDialog)
{

    STD_DBG(_T("未実装\n"));
    return FALSE;
}





/**
*  @brief  ファイル保存処理
*  @param  [in] lpszPathName ファイル名 (NULLの場合は名前をつけて保存)
*  @retval なし
*  @note   名前をつけて保存
*/
BOOL CMainFrame::DoProjectSaveAs(VIEW_COMMON::E_PROJ_TYPE eType, LPCTSTR lpszPathName)
// Save the document data to a file
// lpszPathName = path name where to save document file
// if lpszPathName is NULL then the user will be prompted (SaveAs)
// note: lpszPathName can be different than 'm_strPathName'
{
    namespace fs = boost::filesystem;

    std::shared_ptr<CProjectCtrl> pProject = GetProject(eType).lock();
    StdString strPath = pProject->GetFilePath();

    try
    {
        //TODO:ファイル変更のチェック



        StdString strProject;
        StdPath  path;

        StdString newName;

        if (lpszPathName != 0)
        {
            newName = lpszPathName;
        }

        if (newName == _T(""))
        {

            CNewProjectDlg   dlgProject;

            //名前が空のときはプロジェクト選択ダイアログを表示する

            bool bRet;
            StdString strProject;

            strProject = _T("New Project");

            dlgProject.SetProjectName(strProject);
            dlgProject.SetDir(SYS_CONFIG->strPathData);
            
            if(dlgProject.DoModal() != IDOK)
            {
                return false;
            }

            if (!dlgProject.IsSucess())
            {
                return false;
            }

            bool bCreateDir;
            bCreateDir = dlgProject.IsCreateDir();
            pProject->SetSingleFileMode(false);

            bool bCreateAssy;
            bCreateAssy = dlgProject.IsCreateAssy();

			bool bCreateXml;
			bCreateXml = dlgProject.IsCreateXml();


            strProject  = dlgProject.GetProjectName();
            path        = dlgProject.GetDir();


            if (bCreateDir)
            {
                path = path / strProject;
                if (fs::exists(path))
                {
                    int iAns;
                    iAns = ::MessageBox(  NULL, 
                        GET_STR(STR_ERR_NEWPRJ_ALREADY_DIR),
                        GET_STR(STR_MB_MESSAGE),MB_OKCANCEL ); 

                    if (iAns != IDOK)
                    {
                        return false;
                    }
                    else
                    {
                        //TODO:全消去
                    }
                }
                else
                {
                    fs::create_directories(path);
                }
            }

            bRet = pProject->CreateNewProject(strProject,
                                              bCreateAssy,
                               				  bCreateXml,
                                              path.wstring());
            if (!bRet)
            {
                MockException(e_project_create);
            }

			if (bCreateXml)
			{
				newName = strProject + _T(".mspx");
			}
			else
			{
				newName = strProject + _T(".msp");
			}

        }

        StdPath pathNew; 
        pathNew = path / newName;
        CWaitCursor wait;

        if (!pProject->Save(pathNew))
        {
            //失敗した場合はファイルを消す
            if (lpszPathName == NULL)
            {   
                try
                {
                    fs::remove(pathNew);
                }
                catch(boost::system::system_error &e)
                {
                    STD_DBG(_T("Filesystem error %s"), e.what());
                }
            }
            MockException(e_file_write);
        }

        SendMessage(WM_VIEW_UPDATE_PROJ, eType, 0);
        SYS_FILE->AddMruProject(pathNew.wstring());
    }
    catch (MockException& e)
    {
        e.DispMessageBox();
        return FALSE;
    }
    catch (...)
    {
        StdString  strError = GET_ERR_STR(e_project_create);
        AfxMessageBox( strError.c_str(), MB_OK);
        return FALSE;
    }
    return TRUE;        //成功
}


/**
*  @brief  プロジェクト保存・読み込み表示
*  @param  [in] fileName ファイル名 (NULL 名前をつけて保存)
*  @param  [in] bReplace 
*  @param  [in] lFlags   
*  @param  [in] bOpenFileDialog TRUE:ファイルを開く FALSE 保存
*  @retval なし
*  @note   
*
*/
BOOL CMainFrame::DoPromptProjectName(CString& fileName, 
                                  bool bReplace, 
                                  DWORD lFlags, 
                                  BOOL bOpenFileDialog)
{

    /*
    lFlags について
    定数	                 解説
    OFN_ALLOWMULTISELECT 	「ファイル名」リストボックスで複数選択を可能にする
    専用テンプレートを用いてダイアログを作成する場合
    「ファイル名」リストボックスの定義に LBS_EXTENDEDSEL 値を入れる
    このフラグを選択すると lpstrFile メンバが指すバッファに
    ディレクトリへのパスと、選択された全てのファイル名
    そして、ファイル名の間はスペースで区切られ格納される

    OFN_EXPLORER            フラグが設定されている場合は
    それぞれ NULL 文字で区切られ、連続した NULL 文字で終わる

    OFN_CREATEPROMPT 	    現在存在しないファイルを作成するかを求めるプロンプトを表示する
    OFN_PATHMUSTEXIST と OFN_FILEMUSTEXIST フラグも含む

    OFN_ENABLEHOOK 	        lpfnHook で指定されたフック関数を有効にする

    OFN_ENABLETEMPLATE 	     hInstance が lpTemplateName メンバで指定された
    ダイアログテンプレートを含むリソースのインスタンスであることを示す

    OFN_ENABLETEMPLATEHANDLE 	hInstance メンバがロード済みのダイアログボックステンプレートを含む
    メモリブロックを指していることを表す
    このフラグが指定されている場合、lpTemplateName は無視される

    OFN_EXPLORER 	        新しいエクスプローラスタイルのダイアログボックスの
    カスタム化方法を用いることを示す

    OFN_FILEMUSTEXIST 	    既存のファイル名しか入力を許さない

    OFN_PATHMUSTEXIST       フラグも含む

    OFN_HIDEREADONLY 	    「読み取り専用」チェックボックスを隠す

    OFN_LONGNAMES 	        古いダイアログボックスのために、長いファイル名を用いる
    OFN_EXPLORER が設定されている場合は常に長い名前になる
    OFN_NOCHANGEDIR 	    ダイアログボックスは、現在のディレクトリを
    ダイアログボックスが呼び出された時のディレクトリに戻す

    OFN_NODEREFERENCELINKS 	選択されたショートカットファイル(.LNK)のパスとファイル名を
    返すようにダイアログボックスに指示する

    OFN_NOLONGNAMES 	    「ファイル名」リストボックスに長いファイル名を表示しない

    OFN_NONETWORKBUTTON 	「ネットワーク」ボタンを隠す

    OFN_NOREADONLYRETURN 	返されたファイルに対する「読み取り専用」チェックボックスに
    チェックマークを付けない

    OFN_NOTESTFILECREATE 	ダイアログボックスを閉じる前にファイルを作成しない
    このフラグは、「変更不可で作成」ネットワーク共有ポイント上で
    ファイルを保存する場合に指定する

    OFN_NOVALIDATE 	        無効な文字が入ったファイル名を有効とみなす

    OFN_OVERWRITEPROMPT 	保存時に選択されたファイルが存在する場合
    メッセージボックスが表示され上書きをするか確認する

    OFN_PATHMUSTEXIST 	    有効なパス及びファイル名でなければ入力を許さない

    OFN_READONLY 	        ダイアログの「読み取り専用」チェックボックスをチェックすることを表す

    OFN_SHAREAWARE 	        ネットワーク共有違反が原因で OpenFile() 関数呼び出しが失敗した場合に
    エラーを無視して所定のファイル名を返す
    このフラグが指定されている場合には SHAREVISTRING に対する
    登録メッセージが lParam パラメータで指定されたパスおよびファイル名に対する
    NULL で終わる文字列のポインタと共にフック関数に送られる
    フック関数は次のいずれかで応答しなければならない

    OFN_SHAREFALLTHROUGH -   ファイル名を表示する
    OFN_SHARENOWARN          アクションなし
    OFN_SHAREWARN -          標準警告メッセージを出す

    OFN_SHOWHELP            ヘルプボタンを表示する
    このフラグを設定する場合、親ウィンドウを持たなければならな
    */

    CFileDialog dlgFile(bOpenFileDialog,  //  TRUE:[ファイルを開く] FALSE:[ファイル名を付けて保存]
        NULL,             //  規定のファイル拡張子
        NULL,             //  
        OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, 
        NULL, 
        NULL, 
        0, FALSE);

    CString title;

    if (!bOpenFileDialog)
    {
        if (bReplace)
        {
            title = GET_STR(STR_FILE_SAVEFILE);  //名前を付けて保存
        }
        else
        {
            title = GET_STR(STR_FILE_SAVEFILECOPY);
        }
    }
    else
    {
        title = GET_STR(STR_FILE_LOAD_PROJECT);
    }

    dlgFile.m_ofn.Flags |= lFlags;

    CString strFilter;
    CString strDefault;

    strFilter += _T("MockSketch Projcet Files (*.mspx)");
    strFilter += (TCHAR)'\0'; 
    strFilter += _T("*.mspx");
    strFilter += (TCHAR)'\0'; 
    dlgFile.m_ofn.nMaxCustFilter++;

    strFilter += _T("MockSketch Projcet Files (*.msp)");
    strFilter += (TCHAR)'\0'; 
    strFilter += _T("*.msp");
    strFilter += (TCHAR)'\0'; 
    dlgFile.m_ofn.nMaxCustFilter++;

	strFilter += _T("MockSketch Projcet Files (*.mspt)");
	strFilter += (TCHAR)'\0';
	strFilter += _T("*.mspt");
	strFilter += (TCHAR)'\0';
	dlgFile.m_ofn.nMaxCustFilter++;
	
	//=============================================
    // append the "*.*" all files filter
    CString allFilter;
    allFilter = GET_STR(STR_FILE_ALLFILTER);
    strFilter += allFilter;
    strFilter += (TCHAR)'\0';   // next string please
    strFilter += _T("*.*");
    strFilter += (TCHAR)'\0'; 
    strFilter += (TCHAR)'\0';   // last string

    dlgFile.m_ofn.nMaxCustFilter++;

    dlgFile.m_ofn.lpstrFilter = strFilter;
    dlgFile.m_ofn.lpstrTitle = title;
    dlgFile.m_ofn.lpstrFile = fileName.GetBuffer(_MAX_PATH);

    INT_PTR nResult = dlgFile.DoModal();
    fileName.ReleaseBuffer();
    return nResult == IDOK;
}

/**
 *  @brief  変更時プロジェクト保存
 *  @param  なし
 *  @retval true 成功
 *  @note   
 */
BOOL CMainFrame::SaveModifiedProject()
{
    using namespace VIEW_COMMON;

    E_PROJ_TYPE eType = E_PROJ_MAIN;
    std::shared_ptr<CProjectCtrl> pProject;
    pProject = GetProject(eType).lock();

    if (!pProject->IsChange())
    {
        //変更なし
        return TRUE;        // ok to continue
    }

    // get name/title of document
    StdString strName;
    strName = pProject->GetName();


    //_T("%s への変更を保存しますか?");
    StdStringStream  strmPrompt;

    strmPrompt << StdFormat(GET_STR(STR_FILE_SAK_TO_SAVE)) % strName;

    switch (AfxMessageBox(strmPrompt.str().c_str(), MB_YESNOCANCEL))
    {
    case IDCANCEL:
        return FALSE;       // don't continue

    case IDYES:
        // If so, either Save or Update, as appropriate
        if (!DoProjectSave(eType))
        {
            return FALSE;       // don't continue
        }
        break;

    case IDNO:
        // If not saving changes, revert the document
        break;

    default:
        STD_ASSERT(FALSE);
        break;
    }
    return TRUE;    // keep going
}

/**
 *  @brief  タブにショートカット メニュー表示時処理
 *  @param  [in] point          メニューの画面座標での位置
 *  @param  [in] dwAllowedItems 現在のタブで実行できるアクションを示すフラグ
 *  @param  [in] bTabDrop       TRUE :ドラッグ時にメニュー表示  
 *                              FALSE:アクティブなタブに表示
 *  @retval true 成功
 *  @note   
 */
BOOL CMainFrame::OnShowMDITabContextMenu(CPoint point, DWORD dwAllowedItems, BOOL bDrop)
{
    CMenu menu;
    VERIFY(menu.LoadMenu(bDrop ? IDR_POPUP_DROP_MDITABS : IDR_POPUP_MDITABS));

    CMenu* pPopup = menu.GetSubMenu(0);
    ASSERT(pPopup != NULL);

    if (pPopup)
    {
        if ((dwAllowedItems & AFX_MDI_CREATE_HORZ_GROUP) == 0)
        {
            pPopup->DeleteMenu(ID_MDI_NEW_HORZ_TAB_GROUP, MF_BYCOMMAND);
        }

        if ((dwAllowedItems & AFX_MDI_CREATE_VERT_GROUP) == 0)
        {
            pPopup->DeleteMenu(ID_MDI_NEW_VERT_GROUP, MF_BYCOMMAND);
        }

        if ((dwAllowedItems & AFX_MDI_CAN_MOVE_NEXT) == 0)
        {
            pPopup->DeleteMenu(ID_MDI_MOVE_TO_NEXT_GROUP, MF_BYCOMMAND);
        }

        if ((dwAllowedItems & AFX_MDI_CAN_MOVE_PREV) == 0)
        {
            pPopup->DeleteMenu(ID_MDI_MOVE_TO_PREV_GROUP, MF_BYCOMMAND);
        }

        if ((dwAllowedItems & AFX_MDI_CAN_BE_DOCKED) == 0)
        {
            pPopup->DeleteMenu(ID_MDI_TABBED_DOCUMENT, MF_BYCOMMAND);
        }

        CMFCPopupMenu* pPopupMenu = new CMFCPopupMenu;
        if (pPopupMenu)
        {
            pPopupMenu->SetAutoDestroy(FALSE);
            pPopupMenu->Create(this, point.x, point.y, pPopup->GetSafeHmenu());
        }
    }

    return TRUE;
}

/**
 *  @brief  次のタブグループへ移動
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CMainFrame::OnMdiMoveToNextGroup()
{
    MDITabMoveToNextGroup();
}

/**
 *  @brief  前のタブグループへ移動
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CMainFrame::OnMdiMoveToPrevGroup()
{
    MDITabMoveToNextGroup(FALSE);
}

/**
 *  @brief  水平タブグループの新規作成
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CMainFrame::OnMdiNewHorzTabGroup()
{
    MDITabNewGroup(FALSE);
}

/**
 *  @brief  垂直タブグループの新規作成
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CMainFrame::OnMdiNewVertGroup()
{
    MDITabNewGroup();
}

/**
 *  @brief  タブグループキャンセル
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CMainFrame::OnMdiCancel()
{
    // TODO: 必要の有無を考える。Add your command handler code here
}

/**
 *  @brief  タブつきドキュメント
 *  @param  なし
 *  @retval なし
 *  @note   選択したウインドウをタブグループに表示する
 */
void CMainFrame::OnMdiTabbedDocument()
{
    CMDIChildWndEx* pMDIChild = DYNAMIC_DOWNCAST(CMDIChildWndEx, MDIGetActive());
    if (pMDIChild == NULL)
    {
        ASSERT(FALSE);
        return;
    }

    TabbedDocumentToControlBar(pMDIChild);
}

/**
 *  @brief  タブつきドキュメント表示UI更新
 *  @param  [in out] コマンド ユーザー インターフェイス
 *  @retval なし
 *  @note   
 */
void CMainFrame::OnUpdateMdiTabbedDocument(CCmdUI* pCmdUI)
{
    pCmdUI->SetCheck();
}



//!< 水平タブグループの新規作成UI更新
/**
 *  @brief  次のタブグループへ移動UI更新
 *  @param  [in out] コマンド ユーザー インターフェイス
 *  @retval なし
 *  @note   
 */
void CMainFrame::OnUpdateMdiNewHorzTabGroup(CCmdUI* pCmdUI)
{
    BOOL  bEnable;
    DWORD dwSts;
    dwSts = GetMDITabsContextMenuAllowedItems();
    bEnable = (AFX_MDI_CREATE_HORZ_GROUP & dwSts);
    pCmdUI->Enable(bEnable);

}

//!< 垂直タブグループの新規作成UI更新
/**
 *  @brief  次のタブグループへ移動UI更新
 *  @param  [in out] コマンド ユーザー インターフェイス
 *  @retval なし
 *  @note   
 */
void CMainFrame::OnUpdateMdiNewVertTabGroup(CCmdUI* pCmdUI)
{
    BOOL  bEnable;
    DWORD dwSts;
    dwSts = GetMDITabsContextMenuAllowedItems();
    bEnable = (AFX_MDI_CREATE_VERT_GROUP & dwSts);
    pCmdUI->Enable(bEnable);
}

/**
 *  @brief  次のタブグループへ移動UI更新
 *  @param  [in out] コマンド ユーザー インターフェイス
 *  @retval なし
 *  @note   
 */
void CMainFrame::OnUpdateMdiMoveToNextGroup(CCmdUI* pCmdUI)
{
    BOOL  bEnable;
    DWORD dwSts;
    dwSts = GetMDITabsContextMenuAllowedItems();
    bEnable = (AFX_MDI_CAN_MOVE_NEXT & dwSts);
    pCmdUI->Enable(bEnable);
}

/**
 *  @brief  ビルド
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CMainFrame::OnMenuBuild(UINT id)
{
    //boost::thread thBuild(&DummyBuild);
    std::shared_ptr<CObjectDef> pObjectDef;
    pObjectDef = THIS_APP->GetCurrentObjectDef().lock();
    try
    {
        if (pObjectDef)
        {
            ChangeOutputWindow (WIN_BUILD);
            ClearOutputWindow  (WIN_BUILD);

            VisiblePane(VIEW_COMMON::OUT_WINDOW);

            bool bRebuild = false;
            if (id == ID_MNU_SCRIPT_REBUILD)
            {         
                bRebuild = true;
            }
            pObjectDef->Build(bRebuild);
        }
    }
    catch(MockException &e)
    {
        STD_DBG(_T("Error[%d] %s"), e.GetErr(), e.GetErrMsg().c_str());
        e.DispMessageBox();
        return;
    }
}


/**
 *  @brief  ビルド表示UI更新
 *  @param  [in out] コマンド ユーザー インターフェイス
 *  @retval なし
 *  @note   
 */
//
void CMainFrame::OnUpdateMenuBuild(CCmdUI* pCmdUI)
{
    bool bEnable = false;
    E_EXEC_STS eSts = m_pCtrl->GetExecStatus();

    if(eSts == EX_EDIT)
    {
        bEnable = true;
    }
    pCmdUI->Enable(bEnable);
}

/**
 *  @brief  ビルド中止
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CMainFrame::OnMenuStopBuild()
{
}

/**
 *  @brief  ビルド中止表示UI更新
 *  @param  [in out] コマンド ユーザー インターフェイス
 *  @retval なし
 *  @note   
 */
void CMainFrame::OnUpdateMenuStopBuild(CCmdUI* pCmdUI)
{
    //pCmdUI->Enable(GET_APP_MODE() == MODE_COMPILE);
}

/**
 *  @brief  実行
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CMainFrame::OnMenuExecute()
{
    std::shared_ptr<CObjectDef> pObjectDef;
    pObjectDef = THIS_APP->GetCurrentObjectDef().lock();

    if (!pObjectDef)
    {
       return;
    }

    //--------------------------
    //コンパイル済みチェック
    //--------------------------
    bool bCompile;
    std::vector<StdString> lstName;
    bCompile = pObjectDef->IsCompiled( &lstName);

    if (!bCompile)
    {
        CProperty* pProp;
        pProp = pObjectDef->GetProperty();
        bool bCompile = true;
        if ((!pProp->bAutoCompile) &&
            (lstName.size() !=0))
        {
            StdString strDisp;
            strDisp = GET_STR(STR_MB_COMPILE);
            for(StdString strFile: lstName)
            {
                strDisp = strFile + _T("\n");
            }
            int iAns;
            iAns = ::MessageBox(  NULL, 
                strDisp.c_str(),
                GET_STR(STR_MB_MESSAGE),MB_OKCANCEL ); 

            if (iAns != IDOK)
            {
                bCompile = false;
            }
        }

        if (bCompile)
        {
            OnMenuBuild(ID_MNU_SCRIPT_BUILD);
        }

        bCompile = pObjectDef->IsCompiled( &lstName);

        if (!bCompile)
        {
            return;
        }
    }
    //--------------------------


    try
    {
        if(pObjectDef->ExecScripts())
        {
           m_pExecObjectDef = pObjectDef;
        }
        else
        {
            m_pExecObjectDef.reset();
            STD_DBG(_T("FAIL:  %s \n"), DB_FUNC_NAME.c_str());
        }
    }
    catch(MockException &e)
    {
        STD_DBG(_T("Error[%d] %s"), e.GetErr(), e.GetErrMsg().c_str());
        e.DispMessageBox();
        return;
    }
}

/**
 *  @brief  実行表示UI更新
 *  @param  [in out] コマンド ユーザー インターフェイス
 *  @retval なし
 *  @note   
 */
void CMainFrame:: OnUpdateMenuExecute(CCmdUI* pCmdUI)
{
    bool bEnable = true;
    E_EXEC_STS eSts = m_pCtrl->GetExecStatus();

    if(eSts == EX_EXEC)
    {
        bEnable = false;
    }
    pCmdUI->Enable(bEnable);
}

/**
 *  @brief  中断
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CMainFrame::OnMenuAbort()
{
    E_EXEC_STS eSts = m_pCtrl->GetExecStatus();

    if ( eSts != EXEC_COMMON::EX_EXEC)
    {
        return;
    }

    std::shared_ptr<CObjectDef> pObjectExec;
    pObjectExec = m_pExecObjectDef.lock();
    if (!pObjectExec)
    {
        STD_DBG(_T("FAIL:  %s (!pObjectExec)\n"), DB_FUNC_NAME.c_str());
        return;
    }

    try
    {
        if(!m_pCtrl->AbortScripts())
        {
           STD_DBG(_T("FAIL:  %s (pObjectExec->AbortScripts)\n"), DB_FUNC_NAME.c_str());
        }
        CloseExecView();

    }
    catch(MockException &e)
    {
        STD_DBG(_T("Error[%d] %s"), e.GetErr(), e.GetErrMsg().c_str());
        e.DispMessageBox();
    }
}

/**
 *  @brief  中断表示UI更新
 *  @param  [in out] コマンド ユーザー インターフェイス
 *  @retval なし
 *  @note   
 */
void CMainFrame::OnUpdateMenuAbort(CCmdUI* pCmdUI)
{
    E_EXEC_STS eSts = m_pCtrl->GetExecStatus();
    pCmdUI->Enable(eSts == EX_EXEC);
}

/**
 *  @brief  一時停止
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CMainFrame::OnMenuPause()
{

    std::shared_ptr<CObjectDef> pObjectExec;
    pObjectExec = m_pExecObjectDef.lock();

    if (!pObjectExec)
    {
        STD_DBG(_T("FAIL:  %s (!pObjectExec)\n"), DB_FUNC_NAME.c_str());
        return;
    }

    try
    {
        if(!THIS_APP->GetExecCtrl()->PauseScripts())
        {
            STD_DBG(_T("FAIL:  %s (!pObjectExec->PauseScripts)\n"), DB_FUNC_NAME.c_str());
        }
    }
    catch(MockException &e)
    {
        STD_DBG(_T("Error[%d] %s"), e.GetErr(), e.GetErrMsg().c_str());
        e.DispMessageBox();
    }
}

/**
 *  @brief  一時停止表示UI更新
 *  @param  [in out] コマンド ユーザー インターフェイス
 *  @retval なし
 *  @note   
 */
void CMainFrame::OnUpdateMenuPause(CCmdUI* pCmdUI)
{
    E_EXEC_STS eSts = m_pCtrl->GetExecStatus();

    //pCmdUI->Enable(eSts == EX_EXEC);
}

//!< デバッグモード
/*
void CMainFrame::OnMenuDebug()
{
    std::shared_ptr<CObjectDef> pDef;
    pDef = THIS_APP->GetCurrentObjectDef().lock();
    if (pDef)
    {
        pDef->SetDebugMode(EXEC_COMMON::EDB_DEBUG);
    }
}

//!< デバッグモード表示UI更新
void CMainFrame::OnUpdateMenuDebug(CCmdUI* pCmdUI)
{
    std::shared_ptr<CObjectDef> pDef;
    pDef = THIS_APP->GetCurrentObjectDef().lock();
    if (pDef)
    {
        pCmdUI->Enable(TRUE);
        if(pDef->GetDebugMode() == EXEC_COMMON::EDB_DEBUG)
        {
            pCmdUI->SetRadio(TRUE);
        }
    }
    else
    {
        pCmdUI->Enable(FALSE);
    }
}

//!< リリースモード
void CMainFrame::OnMenuRelease()
{
    std::shared_ptr<CObjectDef> pDef;
    pDef = THIS_APP->GetCurrentObjectDef().lock();
    if (pDef)
    {
        pDef->SetDebugMode(EXEC_COMMON::EDB_RELEASE);
    }
}

//!< リリースモード表示UI更新
void CMainFrame::OnUpdateMenuRelease(CCmdUI* pCmdUI)
{
    std::shared_ptr<CObjectDef> pDef;
    pDef = THIS_APP->GetCurrentObjectDef().lock();
    if (pDef)
    {
        pCmdUI->Enable(TRUE);
        if(pDef->GetDebugMode() == EXEC_COMMON::EDB_RELEASE)
        {
            pCmdUI->SetRadio(TRUE);
        }
    }
    else
    {
        pCmdUI->Enable(FALSE);
    }
}
*/

/**
 *  @brief  オプション
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CMainFrame::OnMenuOption()
{
    CDlgSetting dlgSetting;

    if(dlgSetting.DoModal() != IDOK)
    {

    }
}

//!< オプション表示UI更新
afx_msg void CMainFrame::OnUpdateMenuOption(CCmdUI* pCmdUI)
{


}

/**
 *  @brief  実行中ウインドウ終了
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void  CMainFrame::CloseExecView()
{
    //TODO:実装
    //CObjectDefView* pView;
    foreach(CObjectDefView* pView, *(THIS_APP->GetViewList()))
    {
        if (pView)
        {

        }
    }
}
#ifdef _DEBUG
/**
 *  @brief  実行中ビュー更新
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CMainFrame::UpdateExecView()
{
    //実行中のスレッドから呼び出される
    LARGE_INTEGER nFreq, nBefore, nAfter;
    memset(&nFreq,   0x00, sizeof nFreq);
    memset(&nBefore, 0x00, sizeof nBefore);
    memset(&nAfter,  0x00, sizeof nAfter);
    CExecCtrl* pCtrl = THIS_APP->GetExecCtrl();
    if (pCtrl->IsMeasureExecTime())
    {
        ::QueryPerformanceFrequency(&nFreq);
        ::QueryPerformanceCounter(&nBefore);	
        //dwStartTime = ::GetTickCount();
    }


    CWnd* pWnd;
    foreach(CObjectDefView* pView, *(THIS_APP->GetViewList()))
    {
        pWnd = pView->GetWindow();
        if (!pWnd->IsWindowVisible())
        {
            continue;
        }
        
        if(!pView->IsDrawiingScript())
        {
            continue;
        }

        pView->UpdateView();
    }

    if (pCtrl->IsMeasureExecTime())
    {
        double dExecTime;
        ::QueryPerformanceCounter(&nAfter);

        dExecTime = (nAfter.QuadPart - nBefore.QuadPart) * 1000.0 / nFreq.QuadPart;

        //dExexTime = CUtil::DiffTime(dwStartTime, ::GetTickCount());
        pCtrl->AddExecTime(EXEC_COMMON::TH_DISP, pCtrl->GetDispCycleTime(), dExecTime);
    }
}

/**
 *  @brief  実行中ペイン更新
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CMainFrame::UpdateExecPane()
{
    LARGE_INTEGER nFreq, nBefore, nAfter;
    memset(&nFreq,   0x00, sizeof nFreq);
    memset(&nBefore, 0x00, sizeof nBefore);
    memset(&nAfter,  0x00, sizeof nAfter);
    CExecCtrl* pCtrl = THIS_APP->GetExecCtrl();
    if (pCtrl->IsMeasureExecTime())
    {
        ::QueryPerformanceFrequency(&nFreq);
        ::QueryPerformanceCounter(&nBefore);	
        //dwStartTime = ::GetTickCount();
    }

    if (m_wndExecMonitor.IsVisible())
    {
        m_wndExecMonitor.PostMessage(WM_UPDATE_EXEC,0,0);
    }

    if (pCtrl->IsMeasureExecTime())
    {
        double dExecTime;
        ::QueryPerformanceCounter(&nAfter);

        dExecTime = (nAfter.QuadPart - nBefore.QuadPart) * 1000.0 / nFreq.QuadPart;

        //dExexTime = CUtil::DiffTime(dwStartTime, ::GetTickCount());
        pCtrl->AddExecTime(EXEC_COMMON::TH_PANE, pCtrl->GetDispCycleTime(), dExecTime);
    }

}
#endif
/**
 *  @brief  ペイン表示
 *  @param  eType ペイン種別
 *  @retval なし
 *  @note   
 */
void CMainFrame::VisiblePane(VIEW_COMMON::UI_TYPE eType)
{
    using namespace VIEW_COMMON;
    CDockablePane* pPain = NULL;
    switch(eType)
    {
    case OBJ_WINDOW:
    case LIB_WINDOW:
    case OUT_WINDOW:
    case PROP_WINDOW:
    case LAYER_WINDOW:
    case IO_WINDOW:
    case IO_REF_WINDOW_1:
    case IO_REF_WINDOW_2:
    case IO_REF_WINDOW_3:
    case IO_REF_WINDOW_4:
    case IO_REF_WINDOW_5:
    case STACK_WINDOW:
    case WATCH_WINDOW:
    case MONITOR_WINDOW:
        pPain = reinterpret_cast<CDockablePane*>(GetUi(eType));
        pPain->ShowPane(TRUE, TRUE, TRUE);
        break;
    default:
        break;
    }
}


/**
 *  @brief  新規プロジェクトの作成
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CMainFrame::OnMenuNewProject()
{
    if(!DoProjectSaveAs(VIEW_COMMON::E_PROJ_MAIN ,NULL))
    {
        STD_DBG(_T("Warning: File save-as failed.\n"));
    }
}

/**
 *  @brief  ライブラリ読み込み
 *  @param  なし
 *  @retval なし
 *  @note   
 */
bool CMainFrame::LoadUserLibrary()
{
    namespace fs = boost::filesystem;

    StdPath pathFile = SYS_CONFIG->GetLibraryPath();

    //ロード開始
    ::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_VIEW_LOAD_START, 
        (WPARAM)0, (LPARAM) 0);

    if (!fs::exists(pathFile))
    {
        // ライブラリファイルがない場合は作成する。
        // 通常は、ディフォルトのライブラリを設定している筈

        bool bRet;
        StdPath   pathLib = pathFile.parent_path();
        bRet = m_psLibrary->CreateNewProject(SYS_CONFIG->GetLibraryName(),
                                          false,
			                              false,
                                          pathLib.wstring());
        if (!bRet)
        {
            MockException(e_library_create);
        }
    }
    else if(!m_psLibrary->Load(pathFile))
    {
        //TODO:ERROR
        //ライブラリ%dの読み込みに失敗しました
        return false;
    }
    CWnd* pWnd = AfxGetApp()->m_pMainWnd;

    if (pWnd)
    {
        ::SendMessage(pWnd->m_hWnd, WM_VIEW_LOAD_END, 
            (WPARAM)m_psLibrary.get(), 
            (LPARAM) VIEW_COMMON::E_PROJ_LIB);
    }
    return true;
}



/**
 *  @brief  ライブラリ保存
 *  @param  なし
 *  @retval なし
 *  @note   
 */
bool CMainFrame::SaveUserLibrary()
{
    SYS_CONFIG->CreateLibraryPath();
    StdPath pathFile = SYS_CONFIG->GetLibraryPath();
    if(!m_psLibrary->Save(pathFile))
    {
        //TODO:ERROR
        //ライブラリ%dの書き込みに失敗しました
        return false;
    }

    if(!m_psLibrary->SaveAllObject())
    {
        //TODO:ERROR
        //ライブラリ%dの書き込みに失敗しました
        return false;
    }
    return true;
}

/**
 *  @brief  プロジェクトを開く
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CMainFrame::OnMenuOpenProject()
{
    CString  fileName;

    if(!SaveModifiedProject())
    {
        return;
    }

    CloseAll();

    if(!DoPromptProjectName(fileName, 
        false, 
        0, 
        TRUE))
    {
        return;
    }

    try
    {
        //ロード開始
        ::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_VIEW_LOAD_START, 
            (WPARAM)0, (LPARAM) 0);

        StdString strFileName = fileName;
        StdPath pathFile(strFileName);

        m_psProject->Load(pathFile);

        //ロード完了
        ::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_VIEW_LOAD_END, 
            (WPARAM)m_psProject.get(), (LPARAM) VIEW_COMMON::E_PROJ_MAIN);
        SYS_FILE->AddMruProject(pathFile.wstring());
    }
    catch(MockException &e)
    {
        STD_DBG(_T("Error[%d] %s"), e.GetErr(), e.GetErrMsg().c_str());
        e.DispMessageBox();

    }
    catch(...)
    {
        STD_DBG(_T("Unknow Error"));
        AfxMessageBox(GET_STR(STR_ERROR_OPEN_FILE));
    }
}

/**
 *  @brief  ファイルの保存
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CMainFrame::OnMenuSeveFile()
{
    CView* pView;

    CMDIChildWnd *pChild = (CMDIChildWnd*)GetActiveFrame();
    if (!pChild)
    {
        return;
    }


    pView = pChild->GetActiveView();
    CMockSketchDoc* pDoc = dynamic_cast<CMockSketchDoc*>(pChild->GetActiveDocument());

    try
    {
        pView->SendMessage(WM_VIEW_SAVE_START);
        if(!pDoc->DoFileSave())
        {
            //TODO:エラー処理
        }
    }
    catch(MockException &e)
    {
        STD_DBG(_T("Error[%d] %s"), e.GetErr(), e.GetErrMsg().c_str());
        e.DispMessageBox();

    }
    catch(...)
    {
        STD_DBG(_T("Unknow Error"));
        //TODO:文字追加
        //AfxMessageBox(GET_STR(STR_ERROR_OPEN_FILE));
    }
    return;
}

//!< 名前をつけけて保存
void CMainFrame::OnMenuSeveAs()
{
    int a = 10;
}

/**
 *  @brief  すべてを保存
 *  @param  なし
 *  @retval なし
 *  @note   メニュー操作
 */
void CMainFrame::OnMenuSeveAll()
{
    //プロジェクト上書き保存
    BOOL bRet;
    bRet = DoProjectSave(VIEW_COMMON::E_PROJ_MAIN);

    try
    {
        //開いているファイルを保存
        m_psProject->SaveAllObject();
    }
    catch(MockException &e)
    {
        STD_DBG(_T("Error[%d] %s"), e.GetErr(), e.GetErrMsg().c_str());
        e.DispMessageBox();

    }
    catch(...)
    {
        STD_DBG(_T("Unknow Error"));
        //TODO:文字追加
        //AfxMessageBox(GET_STR(STR_ERROR_OPEN_FILE));
    }
}


/**
 *  @brief  ポップアップメニュー表示
 *  @param  なし
 *  @retval なし
 *  @note   メニュー操作
 */
void CMainFrame::OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu)
{
    CMDIFrameWndEx::OnInitMenuPopup(pPopupMenu, nIndex, bSysMenu);
    ChangeMenu(pPopupMenu->m_hMenu);

}

/**
 *  @brief  メニュー変更
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CMainFrame::ChangeMenu(HMENU hMenu)
{
    CMenu menu;
    MENUITEMINFO  info;

    if(!menu.Attach(hMenu))
    {
        return;
    }

    int iMenuCount = menu.GetMenuItemCount();
    for (int iCnt =0; iCnt < iMenuCount; iCnt++)
    {
        memset(&info, 0, sizeof(info));
        info.cbSize = sizeof(info);
        info.fMask = MIIM_TYPE | MIIM_SUBMENU | MIIM_ID;
        menu.GetMenuItemInfo(iCnt, &info, TRUE);
        if( info.fType != MFT_STRING)
        {
            continue;
        }

        
        if (info.wID == ID_MOCKSKETCH_MRU_FILE)
        {
            //最近使用したファイル
            m_mnuMruFiles.DestroyMenu();
            m_mnuMruFiles.CreateMenu();

            int iFileNum = SYS_FILE->GetMruFileNum();
            CString strItem;

            if (iFileNum > 0)
            {
                for (int iFileCnt = 0; iFileCnt < iFileNum; iFileCnt++)
                {
                    strItem.Format(_T("&%d %s"), iFileCnt + 1, 
                            SYS_FILE->GetMruFile(iFileCnt).c_str());
                    m_mnuMruFiles.AppendMenu(MF_STRING, 
                        ID_MOCKSKETCH_MRU_FILE1 + iFileCnt,
                        strItem);
                }
                CString strMenuItem;
                menu.GetMenuString(iCnt, strMenuItem, MF_BYPOSITION);
                info.dwTypeData = (LPWSTR)(LPCTSTR)strMenuItem;
                info.cch        = strMenuItem.GetLength();
                info.wID = 0;
                info.hSubMenu = m_mnuMruFiles.m_hMenu;
                menu.SetMenuItemInfo(iCnt, &info, TRUE);
            }
            else
            {
                menu.EnableMenuItem(iCnt, MF_BYPOSITION|MF_GRAYED);
            }

        }
        else if (info.wID == ID_PROJECT_MRU_FILE)
        {
            //最近使用したプロジェクト
            m_mnuMruProjects.DestroyMenu();
            m_mnuMruProjects.CreateMenu();

            int iFileNum = SYS_FILE->GetMruProjectNum();
            CString strItem;

            if (iFileNum > 0)
            {
                for (int iFileCnt = 0; iFileCnt < iFileNum; iFileCnt++)
                {
                    strItem.Format(_T("&%d %s"), iFileCnt + 1, 
                            SYS_FILE->GetMruProject(iFileCnt).c_str());
                    m_mnuMruProjects.AppendMenu(MF_STRING, 
                        ID_PROJECT_MRU_FILE1 + iFileCnt,
                        strItem);
                }
 
                CString strMenuItem;
                menu.GetMenuString(iCnt, strMenuItem, MF_BYPOSITION);
                info.dwTypeData = (LPWSTR)(LPCTSTR)strMenuItem;
                info.cch        = strMenuItem.GetLength();
                info.wID = 0;
                info.hSubMenu = m_mnuMruProjects.m_hMenu;
                menu.SetMenuItemInfo(iCnt, &info, TRUE);
            }
            else
            {
                menu.EnableMenuItem(iCnt, MF_BYPOSITION|MF_GRAYED);
            }
        }
        else if ((info.wID == ID_MNU_SAVE_FILE) ||
                 (info.wID == ID_MNU_SAVE_AS_FILE))
        {
            //%sの保存
            CString strItem;
            StdString strFileName;
            CString strMenuItem;
            menu.GetMenuString(iCnt, strMenuItem, MF_BYPOSITION);

            try
            {
                strFileName = CMainFrame::GetCurrentFileName();
                if (strFileName !=_T(""))
                {
                    strItem.Format((LPCTSTR)strMenuItem, strFileName.c_str());
                }
                else
                {
                    if(info.wID == ID_MNU_SAVE_FILE)
                    {
                        strItem = GET_STR(STR_MNU_SAVE_FILE_NONAME);
                    }
                    else
                    {
                        strItem = GET_STR(STR_MNU_SAVE_AS_FILE_NONAME);
                    }
                }

                info.dwTypeData = (LPWSTR)(LPCTSTR)strItem;
                info.cch        = strItem.GetLength();
                menu.SetMenuItemInfo(iCnt, &info, TRUE);
            }
            catch(...)
            {

            }
        }
    }
    menu.Detach();
}


/**
 *  @brief  現在表示中のファイル名取得
 *  @param  [in]  nID
 *  @param  [out] rMessage
 *  @retval なし
 *  @note
 */
StdString CMainFrame::GetCurrentFileName()
{
    CView* pView;
    StdString strRet;

    CMDIChildWnd *pChild = (CMDIChildWnd*)GetActiveFrame();
    if (!pChild)
    {
        return strRet;
    }


    pView = pChild->GetActiveView();
    CMockSketchDoc* pDoc = dynamic_cast<CMockSketchDoc*>(pChild->GetActiveDocument());

    if (!pView)
    {
        return strRet;
    }
    const type_info& info = typeid( *pView );  // 実行時型情報を取得


    if( info == typeid( CEditorView ) )
    {
        CScriptData* pData = pDoc->GetScript();
        strRet = pData->GetName();
        strRet += _T(".as");
    }
    else if(( info == typeid( CMockSketchView)  )||
            ( info == typeid( CElementView )    ))
    {
        CObjectDefView* pDefView = dynamic_cast<CObjectDefView*>(pView);

        std::shared_ptr<CObjectDef> pDef;
        pDef = pDoc->GetObjectDef().lock();

        if (pDef)
        {
            strRet = pDef->GetName();

			CProjectCtrl* pProj;
			pProj = pDef->GetProject();

			if (!pProj)
			{
				return _T("");
			}

			auto eType = pProj->GetFileType();
			if (eType == CProjectCtrl::E_BIN)
			{
				strRet += _T(".msd");
			}
			else if (eType == CProjectCtrl::E_XML)
			{
				strRet += _T(".msdx");
			}
			else if (eType == CProjectCtrl::E_TXT)
			{
				strRet += _T(".msdt");
			}
		}
    }
    return strRet;
}

/**
 *  @brief  ステータスバー表示メッセージ
 *  @param  [in]  nID
 *  @param  [out] rMessage
 *  @retval なし
 *  @note
 */
void CMainFrame::GetMessageString(UINT nID, CString& rMessage) const
{
    CString strTip;
    if (strTip.LoadString(nID))
    {
        // first newline terminates actual string
        rMessage = GET_STR((LPCTSTR)strTip);
        LPTSTR lpsz = rMessage.GetBuffer(255);
        lpsz = _tcschr(lpsz, '\n');
        if (lpsz != NULL)
        {
            *lpsz = '\0';
        }
        rMessage.ReleaseBuffer();
    }
}

/**
 *  @brief  MRUファイル選択
 *  @param  [in]  id
 *  @retval なし
 *  @note
 */
void CMainFrame::OnMruFile(UINT id)
{
    int iNum = id - ID_MOCKSKETCH_MRU_FILE1;
    StdString strFile;
    strFile = SYS_FILE->GetMruFile(iNum);

    //TODO:実装

}

/**
 *  @brief  MRUファイル選択UI更新
 *  @param  [in]  pCmdUI
 *  @retval なし
 *  @note
 */
void CMainFrame::OnUpdateMruFile(CCmdUI* pCmdUI)
{

}

/**
 *  @brief  プロジェクト選択
 *  @param  [in]  id
 *  @retval なし
 *  @note
 */
void CMainFrame::OnMruProject(UINT id)
{
    if(!SaveModifiedProject())
    {
        return;
    }
    CloseAll();

    int iNum = id - ID_PROJECT_MRU_FILE1;
    StdString strPorjct;
    strPorjct = SYS_FILE->GetMruProject(iNum);


    try
    {
        //ロード開始
        ::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_VIEW_LOAD_START, 
            (WPARAM)0, (LPARAM) 0);

        StdPath pathFile(strPorjct);

        m_psProject->Load(pathFile);

        //ロード完了
        ::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_VIEW_LOAD_END, 
            (WPARAM)m_psProject.get(), (LPARAM) VIEW_COMMON::E_PROJ_MAIN);
        SYS_FILE->AddMruProject(pathFile.wstring());
    }
    catch(MockException &e)
    {
        STD_DBG(_T("Error[%d] %s"), e.GetErr(), e.GetErrMsg().c_str());
        e.DispMessageBox();

    }
    catch(...)
    {
        STD_DBG(_T("Unknow Error"));
        AfxMessageBox(GET_STR(STR_ERROR_OPEN_FILE));
    }
}

/**
 *  @brief  プロジェクト選択
 *  @param  [in]  id
 *  @retval なし
 *  @note
 */
void CMainFrame::OnUpdateMruProject(CCmdUI* pCmdUI)
{
    pCmdUI->Enable(GET_APP_MODE() != EX_EXEC);
}

/**
 *  @brief  プロパティグループ
 *  @param  [in]  id
 *  @retval なし
 *  @note
 */
//!< 
afx_msg void CMainFrame::OnMenuPropertyGroup()
{

    PropertyGroupDlg dlgPropertyGroup;

    dlgPropertyGroup.DoModal();
}

//!< オプション表示UI更新
afx_msg void CMainFrame::OnUpdateMenuPropertyGroup(CCmdUI* pCmdUI)
{
    pCmdUI->Enable(GET_APP_MODE() != EX_EXEC);
}

void CMainFrame::OnClose()
{

	SaveBarState(_T("General"));
    /*
	AfxGetApp()->WriteProfileInt(
		_T("General"),_T("Columns"),m_wndPaletteBar.GetColumns());
	AfxGetApp()->WriteProfileInt(
		_T("General"),_T("Color"),(m_bColor!=0));
	AfxGetApp()->WriteProfileInt(
		_T("General"),_T("ToolTips"),(m_bToolTips!=0));
    */


    CloseAll();
    m_psProject.reset();
    m_psLibrary.reset();
    CMDIFrameWndEx::OnClose();
}


//!< ズーム描画
BOOL CMainFrame::DrawZoom(DRAWING_MODE eMode)
{
    CView* pActive = GetActiveView();


    //本来は画面のロックが必要だが現状はテスト用のため不要
    CMockSketchView* pView;
    pView = dynamic_cast<CMockSketchView*>(pActive);

    if (!pView)
    {
        return FALSE;
    }

    CDC* pDc = m_wndZoom.GetDC();

    if (!pDc)
    {
        return false;
    }

    HDC hDc = pDc->m_hDC;

    CRect rcZoom;
    m_wndZoom.GetClientRect(&rcZoom);

    int iWidth  = rcZoom.Width();
    int iHeight = rcZoom.Height();

    pView->DrawTo(hDc, eMode, iWidth, iHeight);
    return TRUE;
}



