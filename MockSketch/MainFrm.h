/**
 * @brief			MainFrmヘッダーファイル
 * @file			MainFrm.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *	CMainFrame クラスのインターフェイス
 *
 * $
 * $
 * 
 */

#pragma once

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
//#include "FileView.h"
#include "SubWindow/ObjectWnd.h"
#include "SubWindow/OutputWnd.h"
#include "SubWindow/PropertiesWnd.h"
#include "SubWindow/LayerWnd.h"
#include "SubWindow/IoSetWnd.h"
#include "SubWindow/ExecMonitorWnd.h"
#include "SubWindow/DebugWatchWnd.h"
#include "SubWindow/StackTraceWnd.h"
#include "SubWindow/IoSetWnd.h"
#include "SubWindow/PropDefWnd.h"
#include "SubWindow/ZoomWnd.h"
#include "Utility/ID_MAP.h"
#include "ToolBar/CFormatBar.h"
#include "ToolBar/CDrawBar.h"
#include "ToolBar/CLineBar.h"
#include "ToolBar/CFindBar.h"
#include "ToolBar/CShowBar.h"
#include "SubWindow/CustomMenuBar.h"
#include "View/ViewCommon.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#define MAIN_FRAME  (((CMockSketchApp*)AfxGetApp())->GetMainFrame())

/*---------------------------------------------------*/
/*  class                                            */
/*---------------------------------------------------*/
class CPartsDef;
class CProjectCtrl;
class CIoTable;
class CExecCtrl;



/**
 * @class   CMainFrame
 * @brief                        
 */
class CMainFrame : public CMDIFrameWndEx
{
    enum E_FILE_TYPE
    {
        FT_PROJECT,
        FT_FUNC_OBJ,
        FT_SCRIPT,
        FT_BITMAP
    };


	DECLARE_DYNAMIC(CMainFrame)

    static const int m_iIoRefMax = 5;
public:
    //!< コンストラクター
    CMainFrame();


    // 属性
public:

    // 操作
public:

    // オーバーライド
public:
    //!< ウィンドウ作成前処理要求
    virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

    //!< フレーム ウィンドウ作成
    virtual BOOL LoadFrame(UINT nIDResource, 
                            DWORD dwDefaultStyle = WS_OVERLAPPEDWINDOW | 
                                                   FWS_ADDTOTITLE, 
                            CWnd* pParentWnd = NULL, 
                            CCreateContext* pContext = NULL);

    //!< ポップアップ メニュー表示発生
    virtual BOOL OnShowPopupMenu (CMFCPopupMenu* pMenuPopup);

	// For customizing the default messages on the status bar
	virtual void GetMessageString(UINT nID, CString& rMessage) const;

    //!< プロジェクト取得
    std::weak_ptr<CProjectCtrl> GetProject(VIEW_COMMON::E_PROJ_TYPE eType);

    //!< IOテーブル取得
    ID_MAP<CIoTable>* GetIoTableMap(){return &m_mapIoTable;}

    //コールスタックウインドウ
    CStackTraceWnd* GetCallStackWnd(){return &m_wndCallstack;}

    //!< 実行コントロール
    CExecCtrl* GetExecCtrl();

    //!< ズーム描画
    BOOL DrawZoom(DRAWING_MODE eMode);

    // 実装
public:
    //!< デストラクター
    virtual ~CMainFrame();
#ifdef _DEBUG

    //!< オブジェクト内部状態チェック
    virtual void AssertValid() const;

    //!< ダンプ処理
    virtual void Dump(CDumpContext& dc) const;

    //!< ウォッチウインドウ
    CDebugWatchWnd* GetDebugWatchWnd(){return &m_wndWatch;}

    //!< 実行中ビュー更新
    void UpdateExecView();

    //!< 実行中ペイン更新
    void UpdateExecPane();
#endif
    //!< ペイン表示
    void VisiblePane(VIEW_COMMON::UI_TYPE eType);

    //!< UI取得
    void* GetUi(VIEW_COMMON::UI_TYPE eType)
    {
        using namespace VIEW_COMMON;
        switch(eType)
        {      
        case NEMU_BAR:        return &m_wndMenuBar;  
        case TOOL_BAR:        return &m_wndToolBar;
        case BUILD_BAR:       return &m_wndToolbarBuild;
        case STATUS_BAR:      return &m_wndStatusBar;
        case USER_IMAGE:      return &m_UserImages;
        case FORMAT_BAR:      return &m_wndFormatBar;
        case DRAW_BAR:        return &m_wndDrawBar;  
        case LINE_BAR:        return &m_wndToolbarLine;
        case EDIT_BAR:        return &m_wndToolbarEdit;
        case FIND_BAR:        return &m_wndToolbarFind;
        case SNAP_BAR:        return &m_wndToolbarSnap;
        case SHOW_BAR:        return &m_wndToolbarShowPos;
        case OBJ_WINDOW:      return &m_wndCObject;
        case LIB_WINDOW:      return &m_wndLibView;
        case OUT_WINDOW:      return &m_wndOutput;
        case PROP_WINDOW:     return &m_wndProperties;
        case ZOOM_WINDOW:     return &m_wndZoom;
        case LAYER_WINDOW:    return &m_wndLayer;
        case IO_WINDOW:       return &m_wndIoSet;
        case IO_REF_WINDOW_1: return &m_wndIoRef[0];
        case IO_REF_WINDOW_2: return &m_wndIoRef[1];
        case IO_REF_WINDOW_3: return &m_wndIoRef[2];
        case IO_REF_WINDOW_4: return &m_wndIoRef[3];
        case IO_REF_WINDOW_5: return &m_wndIoRef[4];
        case STACK_WINDOW:    return &m_wndCallstack;
        case WATCH_WINDOW:    return &m_wndWatch;
        case MONITOR_WINDOW:  return &m_wndExecMonitor;
        case DEF_WINDOW:      return &m_wndPropertyDef;
        }
        return NULL;
    }


    //!< ライブラリ読み込み
    bool LoadUserLibrary();

    //!< ライブラリ書き込み
    bool SaveUserLibrary();

protected:  
    //!< 最近使用したプロジェクト
    CMenu               m_mnuMruProjects;

    //!< 最近使用したファイル
    CMenu               m_mnuMruFiles;

    //!< メニューバー
    CCustomMenuBar       m_wndMenuBar;

    //!< ツールバー
    CCustomToolBar       m_wndToolBar;

    //!< ビルドツールバー
	CCustomToolBar       m_wndToolbarBuild;

    //!< ラインバー
	CLineBar       m_wndToolbarLine;

    //!< 図形編集
	CCustomToolBar       m_wndToolbarEdit;

    //!< 検索ツールバー
	CFindBar       m_wndToolbarFind;

    //!< スナップツールバー
	CCustomToolBar       m_wndToolbarSnap;

    //!< 表示位置ツールバー
	CShowBar       m_wndToolbarShowPos;

    //!< ステータスバー
    CMFCStatusBar     m_wndStatusBar;

    //!< ツール バーイメージ
    CMFCToolBarImages m_UserImages;

    //!< フォーマットバー
    CFormatBar        m_wndFormatBar;

    //!< 描画バー
    CDrawBar          m_wndDrawBar;  

    //!< オブジェクト一覧
    CObjectWnd       m_wndCObject;

    //!< エレメント一覧
    //CElementWnd      m_wndElement;

    //!< ライブラリ一覧
    CObjectWnd       m_wndLibView;

    //!< 出力ウインドウ
    COutputWnd        m_wndOutput;

    //!< プロパティウインドウ
    CPropertiesWnd    m_wndProperties;

    //!< レイヤーウインドウ
    CLayerWnd         m_wndLayer;

    //!< レイヤーウインドウ
    CZoomWnd         m_wndZoom;

    //!< IO設定ウインドウ
    CIoSetWnd         m_wndIoSet;

    //!< IO参照ウインドウ
    CIoSetWnd         m_wndIoRef[m_iIoRefMax];

    //!< コールスタックウインドウ
    CStackTraceWnd         m_wndCallstack;

    //!< ウォッチウインドウ
    CDebugWatchWnd         m_wndWatch;

    //!< 実行モニター
    CExecMonitorWnd        m_wndExecMonitor;

    //!< プロパティ定義
    CPropDefWnd            m_wndPropertyDef;

    //!< 子ウインドウリスト
    std::vector<CWnd*>      m_lstWnd;

    //!< メインプロジェクト
    std::shared_ptr<CProjectCtrl> m_psProject;

    //!< ライブラリ
    std::shared_ptr<CProjectCtrl> m_psLibrary;

    //!< IOテーブルマップ
    ID_MAP<CIoTable>                m_mapIoTable;

    //!< 実行中のオブジェクト定義
    std::weak_ptr<CObjectDef>  m_pExecObjectDef;

    //bool                         m_bExec;
    std::unique_ptr<CExecCtrl>     m_pCtrl;

protected:

    //!< 生成時処理要求
    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);

    //!< ウインドウマネージャ表示要求
    afx_msg void OnWindowManager();

    //!< 表示カスタマイズ要求
    afx_msg void OnViewCustomize();

    //!< 新しいツール バーを作成
    afx_msg LRESULT OnToolbarCreateNew(WPARAM wp, LPARAM lp);

    //!< アプリケーション外観変更要求
    afx_msg void OnApplicationLook(UINT id);

    //!< アプリケーション外観変更更新通知
    afx_msg void OnUpdateApplicationLook(CCmdUI* pCmdUI);

    //ユーザー定義
    afx_msg LRESULT OnPrintMessage(WPARAM iWin, LPARAM pMessage);

    //!< View生成通知
    afx_msg LRESULT OnViewCreate(WPARAM iType, LPARAM pMessage);

    //!< View選択変更通知
    afx_msg LRESULT OnViewSelchange(WPARAM iType, LPARAM pItem);

    //!< 出力ウインドウクリア
    afx_msg LRESULT OnOutputClear(WPARAM iWin, LPARAM lparam);

    //!< 出力ウインドウ選択
    afx_msg LRESULT OnOutputSel(WPARAM iWin, LPARAM lparam);

    //!< 新規プロジェクトの作成
    afx_msg void OnMenuNewProject();

    //!< プロジェクトを開く
    afx_msg void OnMenuOpenProject();

    //!< ファイルの保存
    afx_msg void OnMenuSeveFile();

    //!< 名前をつけけて保存
    afx_msg void OnMenuSeveAs();

    //!< すべてを保存
    afx_msg void OnMenuSeveAll();

    //!<===================
    //!< タブクループ
    //!<===================

    //!< 次のタブグループへ移動
    afx_msg void OnMdiMoveToNextGroup();

    //!< 前のタブグループへ移動
    afx_msg void OnMdiMoveToPrevGroup();

    //!< 水平タブグループの新規作成
    afx_msg void OnMdiNewHorzTabGroup();

    //!< 垂直タブグループの新規作成
    afx_msg void OnMdiNewVertGroup();

    //!< タブグループキャンセル
    afx_msg void OnMdiCancel();

    //タブつきドキュメント
    afx_msg void OnMdiTabbedDocument();

    //タブつきドキュメント表示UI更新
    afx_msg void OnUpdateMdiTabbedDocument(CCmdUI* pCmdUI);

    //!< 水平タブグループの新規作成UI更新
    afx_msg void OnUpdateMdiNewHorzTabGroup(CCmdUI* pCmdUI);

    //!< 垂直タブグループの新規作成UI更新
    afx_msg void OnUpdateMdiNewVertTabGroup(CCmdUI* pCmdUI);

    //!< 次のタブグループへ移動UI更新
    afx_msg void OnUpdateMdiMoveToNextGroup(CCmdUI* pCmdUI);


    //!<===================
    //!< ビルド
    //!<===================

    //!< ビルド
    afx_msg void OnMenuBuild(UINT id);

    //!< ビルド表示UI更新
    afx_msg void OnUpdateMenuBuild(CCmdUI* pCmdUI);

    //!< ビルド中止
    afx_msg void OnMenuStopBuild();

    //!< ビルド中止表示UI更新
    afx_msg void OnUpdateMenuStopBuild(CCmdUI* pCmdUI);

    //!< 実行
    afx_msg void OnMenuExecute();

    //!< 実行表示UI更新
    afx_msg void OnUpdateMenuExecute(CCmdUI* pCmdUI);

    //!< 中断
    afx_msg void OnMenuAbort();

    //!< 中断表示UI更新
    afx_msg void OnUpdateMenuAbort(CCmdUI* pCmdUI);

    //!< 一時停止
    afx_msg void OnMenuPause();

    //!< 一時停止表示UI更新
    afx_msg void OnUpdateMenuPause(CCmdUI* pCmdUI);

    //!< デバッグモード
    //afx_msg void OnMenuDebug();

    //!< デバッグモード表示UI更新
    //afx_msg void OnUpdateMenuDebug(CCmdUI* pCmdUI);

    //!< リリースモード
    //afx_msg void OnMenuRelease();

    //!< リリースモード表示UI更新
    //afx_msg void OnUpdateMenuRelease(CCmdUI* pCmdUI);

    //!< オプション
    afx_msg void OnMenuOption();

    //!< オプション表示UI更新
    afx_msg void OnUpdateMenuOption(CCmdUI* pCmdUI);

    //!< オプション
    afx_msg void OnMenuPropertyGroup();

    //!< オプション表示UI更新
    afx_msg void OnUpdateMenuPropertyGroup(CCmdUI* pCmdUI);

    afx_msg LRESULT OnAfterTaskbarActivate(WPARAM, LPARAM lp);

    afx_msg LRESULT OnResetToolbar(WPARAM, LPARAM lp);

    DECLARE_MESSAGE_MAP()

    //!< ドッキングウインドウ生成
    BOOL CreateDockingWindows();

    //!< ドッキングウインドウアイコン設定
    void SetDockingWindowIcons(BOOL bHiColorIcons);

    //!< メニュー修正
    void AdjustColorsMenu (CMFCPopupMenu* pMenuPopup, UINT uiID);
    void AdjustObjectSubmenu (CMFCPopupMenu* pMenuPopup);

    //!< フォーマットバー作成
    BOOL CreateFormatBar();

    //!< 描画バー作成
    BOOL CreateDrawBar();


protected:  
    //------------------
    //   ファイル
    //------------------

    //プロジェクト上書き保存
    BOOL DoProjectSave(VIEW_COMMON::E_PROJ_TYPE eType);

    //プロジェクト保存
    BOOL DoProjectSaveAs(VIEW_COMMON::E_PROJ_TYPE eType, LPCTSTR lpszPathName);

    //変更時プロジェクト保存
    BOOL SaveModifiedProject();

    //!< すべてを閉じる
    void CloseAll();

    //!< プロジェクト保存・読み込み表示
    BOOL DoPromptProjectName(CString& fileName, 
                                   bool bReplace, 
                                   DWORD lFlags, 
                                   BOOL bOpenFileDialog);

    //!< 現在表示中のファイル名を取得
    StdString GetCurrentFileName();

    //------------------
    //現在のファイルを上書き保存
    BOOL DoCurrentFileSave();

    //名前を変更して現在のファイルを保存
    BOOL DoCurrentFileAs(LPCTSTR lpszPathName);

    //現在のファイル変更時
    BOOL SaveModifiedCurrent();

    //!< 閉じる
    void CloseCurrent();

    //!< ファイル保存・読み込み表示
    BOOL DoPromptFileName(CString& fileName, 
                              bool bReplace, 
                                   DWORD lFlags, 
                                   BOOL bOpenFileDialog);

    //------------------


    virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);

	virtual BOOL OnShowMDITabContextMenu(CPoint point, DWORD dwAllowedItems, BOOL bDrop);

    //!< 実行中ウインドウ終了
    void  CloseExecView();

    //!< メニュー変更
    void ChangeMenu(HMENU hMenu);

public:
    afx_msg void OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu);

    afx_msg void OnMruFile(UINT id);
    afx_msg void OnUpdateMruFile(CCmdUI* pCmdUI);
    afx_msg void OnMruProject(UINT id);
    afx_msg void OnUpdateMruProject(CCmdUI* pCmdUI);
    afx_msg void OnClose();
};


