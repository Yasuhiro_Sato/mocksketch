/**
 * @brief       MockDef実装ファイル
 * @file        MockDef.cpp
 * @author      Yasuhiro Sato
 * @date        01-2-2009 10:36:26
 * 
 * @note
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks        
 *
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./mockdef.h"
#include <shlobj.h> //!< use shell32.lib

/*---------------------------------------------------*/
/*  Defines                                          */
/*---------------------------------------------------*/
#undef new

bool g_ErrorAboat = false;

//TRACEの代わりに使用
void DB_PRINT(const TCHAR* pStr, ...)
{
    TCHAR szBuf[4096]={0};

    va_list arglist; 
    va_start(arglist, pStr); 
    _vstprintf_s(szBuf, 4096, pStr, arglist);
    va_end(arglist); 

    OutputDebugString(szBuf);
}


void DB_PRINT_TIME(const TCHAR* pStr, ...)
{
    TCHAR szBuf[4096]={0};
            SYSTEMTIME      vTime;


    va_list arglist; 
    va_start(arglist, pStr); 
    _vstprintf_s(szBuf, 4096, pStr, arglist);
    va_end(arglist); 

    GetLocalTime( &vTime );
    StdStringStream  strmOut;
    strmOut.str(_T(""));
    strmOut << StdFormat(_T("%02d/%02d %02d:%02d:%02d.%03d:  %s"))  
        % vTime.wMonth % vTime.wDay
        % vTime.wHour % vTime.wMinute % vTime.wSecond 
        % vTime.wMilliseconds % szBuf << std::endl;

    OutputDebugString(strmOut.str().c_str());

}

/**
 * @class   _CDbgLog
 * @brief   デバッグ用のログ 
 */
class _CDbgLog
{
public:

    //!< コンストラクタ
    _CDbgLog():
    m_bDebugLog ( true),
    m_nLogLine  (  -1 ),
    m_nLogFile  (  -1 )
    {

        m_strLogDir = GetDataPath();
        m_strLogDir /= _T("DbgLog");
        Init();
    }

    //!<  コンストラクタ(ディレクトリ指定)
    _CDbgLog(StdString strLogDir):
    m_bDebugLog ( true),
    m_nLogLine  (  -1 ),
    m_nLogFile  (  -1 )
    {

        m_strLogDir = GetDataPath();
        m_strLogDir /= strLogDir;
        Init();
    }

    //!< デストラクタ
    ~ _CDbgLog()
    {

        // 出力アーカイブの作成
        StdStreamOut ofs(m_pathHead);
        StdTextArchiveOut oa(ofs);

        // ファイルに書き出し
        oa << (const _CDbgLog&)*this;

        // 出力を閉じる
        ofs.close();

        if( m_fileOut)
        {
            m_fileOut.close();
        }
    }

    //!< 出力
    void  OutputV(const StdChar * lpszFormat, va_list argptr)
    {
        const static int iSize = 4096;
        StdChar    szMsg[iSize];
        
        if (!m_bDebugLog)
        {
            return;
        }
        _vstprintf_s(szMsg, iSize, lpszFormat, argptr);
        OutputData(szMsg);
    }

    //!< 書式つき出力
    void  Output(const StdChar * lpszFormat, ...)
    {
        if (!m_bDebugLog)
        {
            return;
        }
        va_list argList;
        va_start(argList, lpszFormat);
        OutputV(lpszFormat, argList);
        va_end(argList);
        
    }

    //!< 書式つき出力（ストリーム）
    void  Output(StdStringStream *pStream)
    {
        if (!m_bDebugLog)
        {
            return;
        }
        OutputData(pStream->str().c_str());
    }

    //!< 出力
    void  OutputData(const StdChar * strOut)
    {
        namespace fs = boost::filesystem;
        SYSTEMTIME      vTime;
        if (!m_bDebugLog)
        {
            return;
        }
        
        GetLocalTime( &vTime );

        m_strmOut.str(_T(""));
        m_strmOut << StdFormat(_T("%02d/%02d %02d:%02d:%02d.%03d:  %s"))  
            % vTime.wMonth % vTime.wDay
            % vTime.wHour % vTime.wMinute % vTime.wSecond 
            % vTime.wMilliseconds % strOut << std::endl;

        m_fileOut <<  m_strmOut.str();
        m_fileOut.flush();

        m_nLogLine++;
        if (m_nLogLine > m_nLineMax)
        {
            m_nLogLine = 0;
            m_nLogFile++;

            if (m_nLogFile > m_nFileMax)
            {
                m_nLogFile = 0;
            }

            m_fileOut.close();

            StdStringStream strmFileName;
            StdPath         stdPath;
            strmFileName << StdFormat(_T("dbg_log_%03d"))  % m_nLogFile;
            stdPath = m_strLogDir / strmFileName.str();
            fs::remove(stdPath);
            m_fileOut.open(stdPath, std::ios::out | std::ios::app );

        }
    }
    //!< 出力文字取得
    StdString GetOutData()
    {
        return m_strmOut.str();
    }

protected:
    //!< 初期化
    void  Init()
    {
        namespace fs = boost::filesystem;
        if (!fs::exists(m_strLogDir) )
        {
            fs::create_directory(m_strLogDir);
        }

        //日本語出力にはこれが必要
        //std::locale jp(GetLocal());

        //std::locale::global(jp);        //これはだめ

        m_nLogLine = 0;
        m_nLogFile = 0;
        
        StdStringStream strmFileName;
        //行数とファイル番号を取得
        m_pathHead = m_strLogDir / _T("dbg_log");
        if (fs::exists(m_pathHead))
        {
            StdStreamIn ifs(m_pathHead);
            StdTextArchiveIn ia(ifs);
            ia >> *this;
        }

        strmFileName << StdFormat(_T("dbg_log_%03d")) % m_nLogFile;
        StdPath stdPath = m_strLogDir / strmFileName.str();
        m_fileOut.open(stdPath, std::ios::out | std::ios::app );
    }



protected:
    //!<最大行数
    const static int        m_nLineMax = 2000;

    //!<最大ファイル数
    const static int        m_nFileMax = 10;

    //!< 出力行数
    int         m_nLogLine;

    //!< 出力ファイル数
    int         m_nLogFile;

    //!< 出力フラグ
    bool        m_bDebugLog;

    //!< 出力ディレクトリ
    StdPath     m_strLogDir;

    StdStringStream  m_strmOut;

    //!< 出力ファイルストリーム
    StdFStream    m_fileOut;

    StdPath     m_pathHead;

private:
    friend class boost::serialization::access;
    template<class Archive> 
    void serialize(Archive & ar, const unsigned int version)
    {
        ar & boost::serialization::make_nvp("Line", m_nLogLine);
        ar & boost::serialization::make_nvp("File", m_nLogFile);
    }

};

_CDbgLog* g_pDbgLog = NULL;

//パス名の取得
StdString GetPathStr(StdPath& pathStd)
{
    #ifdef  UNICODE             
        return pathStd.wstring();
    #else
        return pathStd.string();
    #endif
}

/**
 * @brief   アサーション時のアボート設定.
 * @param   [in] bAboat    true:アサーション時にアボートする  
 */
void SetErrAboat(bool bAboat)
{
    g_ErrorAboat = bAboat;
}

/**
 * @brief   条件付きメッセージ.
 * @param   [in] bFail      フラグ
 * @param   [in] pMessage   表示メッセージ
 * @param   [in] pCond      演算式
 * @param   [in] pLine      行に関する表示
 */
void FailIf (long bFail, const TCHAR* pMessage, const TCHAR* pCond, const TCHAR* pLine)
{
    TCHAR szBuf[4098];

    if(bFail){
        _sntprintf_s(szBuf, sizeof(szBuf), _T("%s :assert %s (%s)\n"), pLine, pMessage, pCond);
        DbgLog(szBuf);
#ifdef _DEBUG
#ifdef _WIN32
        OutputDebugString(szBuf);
#else
        printf(szBuf);
#endif
#endif

#ifndef UNIT_TEST 
        if (g_ErrorAboat)
        {
            abort();
        }
#endif //UNIT_TEST
    }
}

void FailIf2(long bFail, const TCHAR* pMessage, const TCHAR* pCond1, TCHAR* pCond2, const TCHAR* pLine)
{
	TCHAR szBuf[4098];
	_sntprintf_s(szBuf, sizeof(szBuf), _T("%s == %s"),  pCond1, pCond2);
	FailIf(bFail, pMessage, szBuf, pLine);
}


/**
 * @brief   ソース行表示.
 * @param   [in] pFile   ファイル名
 * @param   [in] lLine   行数
 * 
 * @return  メッセージ処理の結果
 */
TCHAR* SourceLine( const TCHAR* pFile, long lLine )
{
    // スレッドセーフではない。
    static TCHAR szLine[1024];
    _sntprintf_s(szLine, sizeof(szLine), _T("%s(%d)"),   pFile, lLine);
    return szLine;
}

//!< ファイルパスからファイル名を削除
StdString DeleteFileName(StdString strFileName)
{
    StdChar             szDrive[_MAX_DRIVE];
    StdChar             szDir[2049 /*2048*//*_MAX_DIR*/];
    StdString           strResult;
    StdStringStream       strm;
    StdChar                    cLast;
    StdString             sTmp;


    if (strFileName.size() < 1)    {
        return strFileName;
    }


    while(1) {
        if (strFileName.size() == 0) {
            break;
        }
        cLast = strFileName.at(strFileName.size() -1);
        if ((cLast == _T('/')) ||
            (cLast == _T('\\'))) {
            sTmp = strFileName.substr(0,  strFileName.size() - 1);
            strFileName = sTmp;
        }
        else {
            break;
        }
    }
    _tsplitpath(strFileName.c_str(), szDrive, szDir, NULL, NULL);
    strm << szDrive << szDir;
    strResult = strm.str();
    return  strResult;     
}


/**
 *  データパス取得
 * 
 * @return	アプリケーションデータのパス
 */
StdPath GetDataPath()
{
    namespace fs =  boost::filesystem;

    StdPath pathRet;
    StdString strPath;
    TCHAR szPath[2051/*2048*/] = { 0 };
    LPITEMIDLIST pidl;

    IMalloc *pMalloc;
    if (SHGetMalloc(&pMalloc) != S_OK)
    {
        throw (_T("memory alloc error"));
    }

    if( SUCCEEDED(SHGetSpecialFolderLocation(NULL,CSIDL_LOCAL_APPDATA,&pidl)) )
    { 
        SHGetPathFromIDList(pidl,szPath); // パスに変換する
        pMalloc->Free(pidl);              // 取得したIDLを解放する (CoTaskMemFreeでも可)

        //直接StdPathに設定すると 文字長の設定が不正となる。
        strPath = szPath;
        pathRet =  strPath; 

        //fs::path p1("ディレクトリ\ファイル.txt", fs::native)
        // インストーラでこのディレクトリをつっくておく
        pathRet /= _T("/MockSketchData");
#ifndef _DEBUG
        if(!fs::exists( pathRet ))
        {
            //なければ作成する
            fs::create_directory( pathRet );
        }
#endif
    }
    
#ifdef _DEBUG
    if ( (szPath[0] == 0) ||
         (!fs::exists( pathRet )))
    {
        if (::GetModuleFileName(NULL, szPath, 2051/*2048*/)) 
        {
            pathRet = DeleteFileName(szPath);
        }
        else
        {
            pathRet = fs::initial_path<StdPath>();
        }
    }
#endif


    pMalloc->Release();
    return pathRet;
}

StdString GetCurrentTimeString()
{
    SYSTEMTIME      vTime;
        
    GetLocalTime( &vTime );

    StdStringStream strmOut;
        strmOut << StdFormat(_T("%02d:%02d:%02d.%03d:  "))  
        //% vTime.wMonth % vTime.wDay
        % vTime.wHour % vTime.wMinute % vTime.wSecond 
        % vTime.wMilliseconds;

    return strmOut.str();
}

/**
 * @brief   スリープ.
 * @param   [in] bFail      フラグ
 * @param   [in] pLine      行に関する表示
 * @return  なし
 */
void StdSleep(DWORD msec)
{
#ifdef WIN32
    Sleep(msec);
#else
    boost::xtime xt;
    boost::xtime_get(&xt, boost::TIME_UTC);

    if(msec < 1000) 
    { 
        xt.nsec += msec * 1000000;
    }
    else
    {
        xt.sec  += msec / 1000;
        xt.nsec += (msec % 1000) * 1000000;
    }

    boost::thread::sleep(xt);
#endif

}


//!< デバッグ出力
void DbgLog (const StdChar * lpszFormat, ...)
{
    static   boost::mutex   mtxGuard;
    boost::mutex::scoped_lock lk(mtxGuard);
    if (g_pDbgLog == NULL)
    {
        g_pDbgLog = new _CDbgLog;
    }

    va_list argList;
    va_start(argList, lpszFormat);
    g_pDbgLog->OutputV(lpszFormat, argList);
    va_end(argList);
}


//!< デバッグ出力
void DbgLog2 (const TCHAR* pFile, long lLine , const StdChar * lpszFormat, ...)
{
    static const int iSize = 4096;
    TCHAR szMsg [iSize];

    static   boost::mutex   mtxGuard;
    //boost::mutex::scoped_lock lk(mtxGuard);
    if (g_pDbgLog == NULL)
    {
        g_pDbgLog = new _CDbgLog;
    }

    va_list argList;
    va_start(argList, lpszFormat);
    _vstprintf_s(szMsg, iSize, lpszFormat, argList);

    StdStringStream strm;
    strm << StdFormat(_T("%s(%d) %s")) % pFile % lLine % szMsg;

    g_pDbgLog->OutputData (strm.str().c_str());
    va_end(argList);


#ifdef _DEBUG
    OutputDebugString(g_pDbgLog->GetOutData().c_str());
#endif

}

//!< エラー表示
void DispErrorV(bool bDlg, const StdChar * lpszFormat, va_list argptr)
{
    const static int iSize = 4096;
    StdChar    szMsg[iSize];

    _vstprintf_s(szMsg, iSize, lpszFormat, argptr);

    ChangeOutputWindow (WIN_DEBUG);
    PrtintOut(WIN_DEBUG, szMsg);
    DbgLog(szMsg);
    if (bDlg)
    {
        AfxMessageBox(szMsg);
    }

#ifdef _DEBUG
    OutputDebugString(szMsg);
#endif
}
        

int DoubleToInt( double  what )
{
    if( what > INT_MAX ) 
    {
        throw std::out_of_range("double > INT_MAX");
    }
    if( what < INT_MIN ) 
    {
        throw std::out_of_range("double < INT_MAX");
    }
    return static_cast<int>( what );
}

//!< Size_t->int変換
int SizeToInt( size_t what )
{
    if( what > INT_MAX ) 
    {
       throw std::out_of_range("size_t > INT_MAX");
    }
    return static_cast<int>( what );
}

UINT SizeToUInt( size_t what )
{
    if( what > UINT_MAX ) 
    {
       throw std::out_of_range("size_t > UINT_MAX");
    }
    return static_cast<UINT>( what );
}

int IntptrToInt( INT_PTR what )
{
    if( what > INT_MAX ) 
    {
       throw std::out_of_range("size_t > INT_MAX");
    }
    return static_cast<int>( what );
}

int DwordptrToInt( DWORD_PTR what )
{
    if( what > INT_MAX ) 
    {
       throw std::out_of_range("size_t > INT_MAX");
    }
    return static_cast<int>( what );
}


void DispError(bool bDlg, const StdChar * lpszFormat, ...)
{
    va_list argList;
    va_start(argList, lpszFormat);
    DispErrorV(bDlg, lpszFormat, argList);
    va_end(argList);
}


//!< 終了処理
void Finish()
{
    if (g_pDbgLog)
    {
        delete g_pDbgLog;
    }

}

//=========================
struct FILE_DATA
{
    std::wstring file;
    int iLine;
};

std::map<HGDIOBJ, FILE_DATA> gMapFile;


HGDIOBJ DbgSelectObject(const TCHAR* pFile, long lLine , HDC hdc, HGDIOBJ h)
{
    FILE_DATA fData;
    fData.file = pFile;
    fData.iLine = lLine;

    HGDIOBJ hRet;
    hRet = :: SelectObject( hdc, h);
    gMapFile[h] = fData;
    return hRet;
}

BOOL DbgDeleteObject(HGDIOBJ ho)
{
    bool bRet;
    bRet = (::DeleteObject( ho) == TRUE);

    auto ite = gMapFile.find(ho);

    if (ite != gMapFile.end())
    {
        gMapFile.erase(ite);
    }
    return bRet;
}

void ShowLeak()
{
    if (gMapFile.size() != 0)
    {
        DB_PRINT(_T("---- Leak detected ---- \n"));
        for(auto ite: gMapFile)
        {
            HGDIOBJ hObj =  ite.first;
            FILE_DATA* pData = &ite.second; 
            DB_PRINT(_T("%s(%d) %I64x \n"),pData->file.c_str(), 
                                        pData->iLine,
                                        hObj);
        }
    }
}
