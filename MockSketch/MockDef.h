/**
 * @brief			MockDef ヘッダーファイル
 * @file			MockDef.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

#ifndef __MOCK_DEF_H__
#define __MOCK_DEF_H__
#define BOOST_FILESYSTEM_VERSION 3

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include <list>
#include <deque>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <stdexcept>
#include <algorithm>
#include <cctype>
#include <cstdio>
#include <regex>
#include <boost/thread.hpp>
#include <boost/thread/xtime.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/foreach.hpp>
#include <boost/signals2.hpp>
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <boost/any.hpp>
#include <boost/range.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/algorithm/find_if.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/format.hpp>
#include <boost/asio.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/concepts.hpp>

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/sequenced_index.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/bimap/bimap.hpp>
#include <boost/bimap/multiset_of.hpp>

#include <boost/serialization/serialization.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/deque.hpp>
#include <boost/serialization/set.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/weak_ptr.hpp>
#include <boost/serialization/version.hpp>

#include <boost/signals2.hpp>

#pragma warning (push) 
#pragma warning( disable : 4244 ) 
#include <boost/archive/binary_iarchive.hpp> 
#include <boost/archive/binary_oarchive.hpp> 
#pragma warning (pop) 

#include <boost/lambda/lambda.hpp>
#include <boost/test/unit_test.hpp>
#include <boost/xpressive/xpressive.hpp>
//#include <boost/regex.hpp> >> std::regxを使用
#include <boost/assign.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/format.hpp>
#include <boost/smart_ptr/make_shared.hpp>
#include <boost/static_assert.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid_generators.hpp>

#include <Eigen/Core>
#include <Eigen/Sparse>
#include <Eigen/Dense>
#include <Eigen/SparseQR>
#include <Eigen/SparseLU>



#ifdef  UNICODE             
#include <boost/archive/text_woarchive.hpp> // テキスト形式アーカイブに書き込み
#include <boost/archive/text_wiarchive.hpp> // テキスト形式アーカイブから読み込み
#include <boost/archive/xml_wiarchive.hpp> 
#include <boost/archive/xml_woarchive.hpp> 
#else
#include <boost/archive/text_oarchive.hpp> // テキスト形式アーカイブに書き込み
#include <boost/archive/text_iarchive.hpp> // テキスト形式アーカイブから読み込み
#include <boost/archive/xml_iarchive.hpp> 
#include <boost/archive/xml_oarchive.hpp> 
#endif


//0と判定する値
#define  NEAR_ZERO   (1e-10)

#define  TOLERANCE_MAX (1e-6)

// 描画領域最大値
#define  MAX_AREA   (1e+40)

// オブジェクト最大数
#define  MAX_OBJ    (200) //(テスト用)   //(2147483647)

#define DEG2RAD  (1.74532925199432957692e-02)
                   
#define RAD2DEG  (5.72957795130823208768e+01)

#define MM2TWIP  (5.66929133858267716535e+01)

#define TWIP2MM  (1.76388888888888888888e-02)

// オブジェクトID 最大数
#define ID_DRAWING_MAX  (0xFFFF00)

// 特殊オブジェクトID
#define ID_DRAWING          (ID_DRAWING_MAX+1) //一時的な描画用
#define ID_NO_ID_DRAWING    (ID_DRAWING_MAX+2) //一時的な描画用でINDEXバッファに描画しない
#define ID_TEMP_GROUP  (ID_DRAWING_MAX+3)  //一時的なグループ
#define ID_PAPER_FRAME (ID_DRAWING_MAX+4)  //印刷枠(紙)
#define ID_PRINT_FRAME (ID_DRAWING_MAX+5)  //印刷枠(印刷範囲)
#define ID_GRID_DOT    (ID_DRAWING_MAX+6)  //グリッド

//エラーコード
// 戻り値にEXCEPTION_TYPEを使うときに使用
// エラー無しが０(e_no_error)となる
typedef DWORD E_ERR;
#define E_NO_ERR  (0)

//ラムダ式で構造体のアクセスに使用
//  以下のような場合に使用(リスト内の構造体のデータを参照する）
//  struct LAMBDA_DATA {
//  std::string Name;
//  int         iId; };
//
//  std::vector<LAMBDA_DATA> lstLambda;
//
//
//  std::vector<LAMBDA_DATA>::iterator ite;
//  ite = std::find_if(lstLambda.begin(), 
//                   lstLambda.end(), 
//                   &boost::lambda::_1 ->* &LAMBDA_DATA::iId == 2);
//
//  これをこのように書けるようにする
//  ite = std::find_if(lstLambda.begin(), 
//                   lstLambda.end(), 
//                   GET_FIRST(LAMBDA_DATA::iId) == 2);
//
#define GET_FIRST(x) &boost::lambda::_1 ->* &x

enum E_WINDOW_MESSAGE
{
    WM_OUTPUT_PRINT = (WM_APP + 1),
    WM_VAL_INPUT    = (WM_APP + 2),
    WM_VIEW_CREATE  = (WM_APP + 3),     
    WM_VIEW_SEL_CHANGEE  = (WM_APP + 4),    // アイテム選択変更

    WM_VIEW_LOAD_START   = (WM_APP + 5),    // ロード完了通知
    WM_VIEW_LOAD_END     = (WM_APP + 6),    // ロード完了通知
    WM_VIEW_SAVE_START   = (WM_APP + 7),    // セーブ開始通知
    WM_VIEW_PARTS_DELETE = (WM_APP + 8),    // 部品削除通知
    WM_VIEW_ACTIVE       = (WM_APP + 9),    // VIEWアクティブ
    WM_VIEW_UPDATE_NAME  = (WM_APP +10),    // 名称変更   OBJCTVIEW->
    WM_VIEW_UPDATE_PROJ  = (WM_APP +11),    // プロジェクト名変更 MAINFRME->
    WM_VIEW_ADD_OBJECT   = (WM_APP +12)  ,  // オブジェクト追加   VIEW->
    WM_VIEW_CLEAR_ALL    = (WM_APP +13),    // 全ウインドウクリア MAINFRME->
    
    WM_CHG_OBJECT_STS  = (WM_APP +14),      // オブジェクトステータス変更
    WM_UPDATE_OBJECT   = (WM_APP +15),      // オブジェクト変更

    WM_UPDATE_PORPERTY_SET = (WM_APP +16),    // プロパティセット変更

    WM_OUTPUT_CLEAR  = (WM_APP + 21),       // 出力ウインドウクリア
    WM_OUTPUT_SEL    = (WM_APP + 22),       // 出力ウインドウ選択

    WM_BREAK_START  = (WM_APP + 23),        // ブレークポイント開始
    WM_BREAK_END    = (WM_APP + 24),        // ブレークポイント終了

    WM_DELETE_WATCH = (WM_APP + 25),        // ウォッチ削除

    WM_MODE_CHANGE  = (WM_APP + 26),        // モード変更
    WM_UPDATE_EXEC  = (WM_APP + 27),        // 実行時更新

    WM_UPDATE_PROPERTY = (WM_APP + 28),     // プロパティウインドウ更新要求

    WM_UPDATE_PRINTER  = (WM_APP + 29),     // プリンター設定変更通知

    WM_UPDATE_CURRENT_LAYER_NUM  = (WM_APP + 30),       // カレントレイヤ番号

    WM_VIEW_END,                            //VIEWに再配信するメッセージの終端


    WM_DROP_ENTER   =  (WM_VIEW_END +1),    // ドロップ通過開始知
    WM_DROP_OVER    =  (WM_VIEW_END +2),    // ドロップ通過通知
    WM_DROP_END     =  (WM_VIEW_END +3),    // ドロップ完了通知
    WM_DROP_LEAVE   =  (WM_VIEW_END +4),    // ドロップ通過完了

    WM_SHOW_DRAWING  =  (WM_VIEW_END +7),    //  DRAWING VIEW 表示
    WM_SHOW_EDITOR   =  (WM_VIEW_END +8),    //  EDITOR表示

    WM_CHG_CONTEXT   =  (WM_VIEW_END +9),    //  
    WM_USER_PASTE_END = (WM_VIEW_END +10),   //  CUTしたあとにPASTEした時CUT元に通知する

    WM_APP_END,     //


    // 以下は再配信なし
    WM_UPDATE_EXEC_VIWE   =  (WM_APP_END +1),   //  UpdateExecView
    WM_UPDATE_EXEC_PANE   =  (WM_APP_END +2),   //  UpdateExecPane
    WM_CLOSE_ALL_EXEC_VIEW=  (WM_APP_END +3),   //  CloseAllExecView
};

enum E_OUTPUT_WINDOW
{
    WIN_BUILD   = 0,
    WIN_DEBUG   = 1,
    WIN_FIND    = 2,

    WIN_OUTPUT_MAX,
};


enum E_TIMER_EVENT
{
    ONT_PAINE       = (0x01000),
    ONT_PROPERTY    = (0x01001),
    ONT_IO          = (0x01002),

};

//MOUSE_MOVE_POS で使用する 拡張コマンド
#define MK_ON_MARKER 0x1000  ///マーカ上にマウスがある
#define MK_ACTION1   0x2000
#define MK_ACTION2   0x4000
#define MK_ACTION3   0x8000

struct MOUSE_MOVE_POS
{
    POINT ptSel;
    UINT nFlags;
};

#define foreach BOOST_FOREACH
#define STATIC_ASSERT BOOST_STATIC_ASSERT

#define DB_FUNC_NAME  CUtil::CharToString(__FUNCTION__)
#define DB_FUNC_PRINT                                     \
    DB_PRINT(_T("%s\n"), CUtil::CharToString(__FUNCTION__).c_str())  \

/**
 * 文字定義
 */
#ifdef  UNICODE             
#define StdChar             wchar_t
#define StdString           std::wstring
#define StdStringStream     std::wstringstream
#define StdStreamBuf        std::wstreambuf
#define StdOut              std::wostream
#define StdFormat           boost::wformat
#define StdPath             boost::filesystem::wpath
#define StdFStream          boost::filesystem::wfstream
#define StdStreamOut        boost::filesystem::wofstream
#define StdStreamIn         boost::filesystem::wifstream
//#define StdFileError        boost::filesystem::wfilesystem_error
#define StdTextArchiveOut   boost::archive::text_woarchive
#define StdTextArchiveIn    boost::archive::text_wiarchive
#define StdXmlArchiveOut    boost::archive::xml_woarchive
#define StdXmlArchiveIn     boost::archive::xml_wiarchive
#define StdXregex           boost::xpressive::wcregex
#define StdStrXregex        boost::xpressive::wsregex
#define StdXMatchResult     boost::xpressive::wcmatch
#define StdStrXMatchResult  boost::xpressive::wsmatch
#define StdRegex            std::wregex
#define StdRegexMatch       std::wsmatch
#else
#define StdChar             char
#define StdString           std::string
#define StdStringStream     std::stringstream
#define StdStreamBuf        std::streambuf
#define StdOut              std::ostream
#define StdFormat           boost::format
#define StdPath             boost::filesystem::path
#define StdFStream          boost::filesystem::fstream
#define StdStreamOut        boost::filesystem::ofstream
#define StdStreamIn         boost::filesystem::ifstream
//#define StdFileError        boost::filesystem::filesystem_error
#define StdTextArchiveOut   boost::archive::text_oarchive
#define StdTextArchiveIn    boost::archive::text_iarchive
#define StdXmlArchiveOut    boost::archive::xml_oarchive
#define StdXmlArchiveIn     boost::archive::xml_iarchive
#define StdXregex           boost::xpressive::cregex
#define StdStrXregex        boost::xpressive::sregex
#define StdXMatchResult     boost::xpressive::cmatch
#define StdStrXMatchResult  boost::xpressive::smatch
#define StdRegex            std::regex
#define StdRegexMatch       std::smatch
#endif


//パス名の取得
StdString GetPathStr(StdPath& pathStd);

void SetErrAboat(bool bAboat);

/**
 * アサーション
 */
void FailIf (long bFail, const TCHAR* pMessage, const TCHAR* pCond, const TCHAR* pLine);
void FailIf2(long bFail, const TCHAR* pMessage, const TCHAR* pCond1, TCHAR* pCond2, const TCHAR* pLine);
TCHAR* SourceLine( const TCHAR* pFile, long lLine );
#define TEST_SOURCELINE() SourceLine( _T( __FILE__ ), __LINE__ )

// DBG_ASSERT は リリースモードではコンパイルされない
#ifndef _DEBUG
#define DBG_ASSERT(condition)
#define DBG_ABORT()
#else
#define DBG_ASSERT(condition)                        \
  ( FailIf( !(condition),                            \
              _T(""),       _T( #condition ),        \
                                 TEST_SOURCELINE() ) )
#define DBG_ABORT()                                  \
    (abort())

#endif //_DEBUG

// 以下のASSERT は リリースモードでもログを残す
#define COMMENT_ASSERT(condition, str)               \
  ( FailIf( !(condition),                            \
              str,       _T( #condition ),           \
                                 TEST_SOURCELINE() ) )

#define STD_ASSERT(condition)                        \
  ( FailIf( !(condition),                            \
              _T(""),       _T( #condition ),        \
                                 TEST_SOURCELINE() ) )
#define STD_ASSERT_DBL_EQ(condition1, condition2)            \
  ( FailIf2( !(fabs (condition1 - condition2) < NEAR_ZERO),   \
  _T(""),       _T(#condition1),  _T(#condition2), \
                                 TEST_SOURCELINE() ) )
//標準デバッグ出力
#define STD_DBG(fmt, ...)                                \
    DbgLog2 (_T( __FILE__ ), __LINE__ , fmt, __VA_ARGS__)\


//リソースリーク検出
HGDIOBJ DbgSelectObject(const TCHAR* pFile, long lLine , HDC hdc, HGDIOBJ h);
BOOL DbgDeleteObject(HGDIOBJ ho);
void ShowLeak();

#ifndef _DEBUG
#define SELECT_OBJECT(hdc, h)   ::SelectObject (hdc, h)
#else
#define SELECT_OBJECT(hdc, h)   DbgSelectObject (_T( __FILE__ ), __LINE__ , hdc, h)
#endif

#ifndef _DEBUG
#define DELETE_OBJECT(hdc)  ::DeleteObject (hdc)
#else
#define DELETE_OBJECT(hdc)   DbgDeleteObject (hdc)
#endif

void DB_PRINT(const TCHAR* pStr, ...);
void DB_PRINT_TIME(const TCHAR* pStr, ...);

//!< ファイルパスからファイル名を削除
StdString DeleteFileName(StdString strFileName);

//!< データパス取得
StdPath GetDataPath();

//現在時刻取得
StdString GetCurrentTimeString();

#define DB_FUNC_PRINT_TIME                                     \
    DB_PRINT(_T("%s%s\n"), GetCurrentTimeString().c_str(), CUtil::CharToString(__FUNCTION__).c_str())  \

//!< スリープ
void StdSleep(DWORD msec);

//!< エラー表示
void DispError(bool bDlg, const StdChar * lpszFormat, ...);

//!< デバッグ出力
void DbgLog(const StdChar * lpszFormat, ...);

//!< デバッグ出力2
void DbgLog2 (const TCHAR* pFile, long lLine , const StdChar * lpszFormat, ...);

//!< Size_t->int変換
int SizeToInt( size_t what );

UINT SizeToUInt( size_t what );

int IntptrToInt( INT_PTR what );

int DwordptrToInt( DWORD_PTR what );

int DoubleToInt( double what );

//!< 終了処理
void Finish();

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#endif //__MOCK_DEF_H__
