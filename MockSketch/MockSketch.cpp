/**
 * @brief       MockSketch実装ファイル
 * @file        CDrawingView.cpp
 * @author      Yasuhiro Sato
 * @date        01-2-2009 10:36:26
 * 
 * @note
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks    アプリケーションメイン
 *
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "afxwinappex.h"
#include "MockSketch.h"
#include "MainFrm.h"

#include "ChildFrm.h"
#include "AboutDlg.h"
#include "CProjectCtrl.h"
#include "MultiDocReloadableTemplate.h"
#include "ExecCommon.h"

#include "DrawingObject/CDrawingScriptBase.h"

#include "DefinitionObject/ObjectDef.h"
#include "DefinitionObject/CPartsDef.h"

#include "DefinitionObject/View/MockSketchDoc.h"
#include "DefinitionObject/View/ObjectDefView.h"
#include "DefinitionObject/View/MockSketchView.h"
#include "DefinitionObject/View/CElementView.h"
#include "Script/EditorView.h"
#include "Script/CScriptEngine.h"
#include "Script/CScript.h"
#include "Script/CExecCtrl.h"

#include "System/CSystem.h"
#include "Utility/CUtility.h"
#include <angelscript.h>

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#define MOCK_SKETCH_MUTEX_NAME	_T("MockSketchApp")

// 唯一の CMockSketchApp オブジェクトです。

CMockSketchApp theApp;


/*---------------------------------------------------*/
/*  Test                                             */
/*---------------------------------------------------*/
#ifdef _DEBUG
extern void TEST_NearPoint();
extern void TEST_POINT2D();
extern void TEST_LINE2D();
extern void TEST_RECT2D();
extern void TEST_CIRCLE2D();
extern void TEST_ELLIPSE2D();
extern void TEST_HYPERBOLA2D();
extern void TEST_PARABOLA2D();
extern void TEST_SPLINE2D();
extern void TEST_MAT2D();
extern void TEST_CUtil();
extern void TEST_PROPERTY_GRID_DATA();
extern void TEST_CIdObj();
extern void TEST_CImageData();
extern void TEST_CVirtualCheckList();
extern void TEST_ID_MAP();
extern void TEST_LIST_BLOCK();
extern void TEST_TREE_DATA();
extern void TEST_ExecCommon();
extern void TEST_DRAWING_TYPE();
extern void TEST_CPropertyGroup();

extern void TEST_NameTreeData();

extern void TEST_CObjectDef();
extern void TEST_CElementDef();
extern void TEST_CFieldDef();
extern void TEST_CReferenceDef();

extern void TEST_CProjectCtrl();
extern void TEST_CPartsDef();
extern void TEST_CDrawingObject();
extern void TEST_CDrawingParts();

extern void TEST_CDrawingCompositionLine();
extern void TEST_CDrawingCircle();
extern void TEST_CDrawingDim();
extern void TEST_CDrawingDimH();
extern void TEST_CDrawingDimL();
extern void TEST_CDrawingDimV();
extern void TEST_CDrawingDimA();
extern void TEST_CDrawingDimD();
extern void TEST_CDrawingDimR();
extern void TEST_CDrawingDimC();
extern void TEST_CDrawingImage();
extern void TEST_CDrawingEllipse();
extern void TEST_CDrawingLine();
extern void TEST_CDrawingPoint();
extern void TEST_CDrawingSpline();
extern void TEST_CDrawingText();
extern void TEST_CDrawingElement();
extern void TEST_CDrawingNode();
extern void TEST_CDrawingGroup();
extern void TEST_CDrawingReference();
extern void TEST_CNodeBound();
extern void TEST_CNodeBound();


//SYSTEM
extern void TEST_CSystemLibrary();
extern void TEST_CUnit();
extern void TEST_CUnitCtrl();
extern void TEST_CSystemLocal();
extern void TEST_CSystemFile();
extern void TEST_CSystemProperty();


//ACTION
extern void TEST_CActionOffset();
extern void TEST_CUndoAction();
extern void TEST_CActionLine();
extern void TEST_CActionCircle();

//IO
extern void TEST_CIoAccessTcp();
extern void TEST_CIoAccessBoard();
extern void TEST_CIoProperty();
extern void TEST_CIoPropertyBlock();
extern void TEST_CIoDef();
extern void TEST_CIoRef();
extern void TEST_CIoConnect();
extern void TEST_CIoAccess();
extern void TEST_CIoDefBase();

//SubWindow
extern void TEST_CDebugWatchWnd();
extern void TEST_CObjectWnd();

//Script
extern void TEST_CScriptEngine();

void test_utlity()
{
    TEST_POINT2D();
    TEST_LINE2D();
    TEST_RECT2D();
    TEST_CIRCLE2D();
    TEST_ELLIPSE2D();
    TEST_HYPERBOLA2D();
    TEST_PARABOLA2D();
    TEST_SPLINE2D();
    TEST_MAT2D();
    TEST_PROPERTY_GRID_DATA();
    TEST_CUtil();
    //TEST_CIdObj();
    TEST_ID_MAP();
    TEST_LIST_BLOCK();
    TEST_TREE_DATA();
    TEST_ExecCommon();
    TEST_NameTreeData();
    TEST_DRAWING_TYPE();
    TEST_CPropertyGroup();

    if(true)     {TEST_CImageData();}         //DLG
    if(false)     {TEST_CVirtualCheckList();}  //DLG
}

//---------------------------
void test_view()
{
    TEST_CObjectDef();
    TEST_CElementDef();
    TEST_CFieldDef();
    TEST_CReferenceDef();

    TEST_CDrawingObject();
    TEST_CDrawingParts();
    TEST_CProjectCtrl();
    TEST_CPartsDef();

    TEST_CDrawingCompositionLine();
    TEST_CDrawingCircle();

    TEST_CDrawingDim();
    TEST_CDrawingDimL();
    TEST_CDrawingDimH();
    TEST_CDrawingDimV();
    TEST_CDrawingDimA();
    TEST_CDrawingDimD();
    TEST_CDrawingDimR();
    TEST_CDrawingDimC();

    TEST_CDrawingImage();

    TEST_CDrawingEllipse();
    TEST_CDrawingLine();
    TEST_CDrawingPoint();
    TEST_CDrawingSpline();
    TEST_CDrawingText();
    TEST_CDrawingElement();
    TEST_CDrawingNode();
    TEST_CDrawingGroup();
    TEST_CDrawingReference();

	TEST_CNodeBound();
}

//---------------------------
void test_sub_window()
{
    TEST_CDebugWatchWnd();
    TEST_CObjectWnd();
}

//---------------------------
void test_system()
{
    TEST_CUnit();
    TEST_CUnitCtrl();
    TEST_CSystemLibrary();
    TEST_CSystemLocal();
    TEST_CSystemFile();
    TEST_CSystemProperty();
}


//---------------------------
void test_action()
{
    TEST_CActionOffset();
    TEST_CActionLine();
    TEST_CActionCircle();
    TEST_CUndoAction();
}
//---------------------------

void test_io()
{
    TEST_CIoAccess();
    TEST_CIoAccessTcp();
    TEST_CIoAccessBoard();
    TEST_CIoProperty();
    TEST_CIoPropertyBlock();
    if(false)     {TEST_CIoDef();}
    if(false)     {TEST_CIoRef();}
    TEST_CIoConnect();
    TEST_CIoDefBase();
}

void test_script()
{
    TEST_CScriptEngine();
}


void test()
{
    SetErrAboat(false);

    DB_PRINT(_T("TEST_NearPoint\n"));
    TEST_NearPoint();

    test_utlity();

    test_action();

    test_view();

    test_sub_window();
    
    test_io();

    test_system();

    //SetErrAboat(true);

    test_script();
}
#endif

#if defined(_DEBUG) && (_MSC_VER >= 1000 && _MSC_VER <= 1699)
//
// Debug memory block header:
//    o  Borrowed from the Microsoft CRT to fix the false "memory leak" report
//       when using typeinfo 'name' accessor in a _DEBUG build of the library.  
//
struct _CrtMemBlockHeader
   {
   struct _CrtMemBlockHeader * pBlockHeaderNext;
   struct _CrtMemBlockHeader * pBlockHeaderPrev;
   char *                      szFileName;
   int                         nLine;
   #ifdef _WIN64
   int                         nBlockUse;
   size_t                      nDataSize;
   #else
   size_t                      nDataSize;
   int                         nBlockUse;
   #endif
   long                        lRequest;
   unsigned char               gap[4];
   };

static void __cdecl _FixTypeInfoBlockUse(void)
   {
   __type_info_node* pNode = __type_info_root_node.next;

   while(pNode != NULL)
      {
      __type_info_node* pNext = pNode->next;

      (((_CrtMemBlockHeader*)pNode) - 1)->nBlockUse = _CRT_BLOCK;

      if (pNode->memPtr != NULL)
         (((_CrtMemBlockHeader*)pNode->memPtr) - 1)->nBlockUse = _CRT_BLOCK;

      pNode = pNext;
      }
   }

#endif//defined(_DEBUG) && (_MSC_VER >= 1000 && _MSC_VER <= 1699)

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
BEGIN_MESSAGE_MAP(CMockSketchApp, CWinAppEx)
    ON_COMMAND(ID_APP_ABOUT, &CMockSketchApp::OnAppAbout)
    // 標準のファイル基本ドキュメント コマンド
    ON_COMMAND(ID_FILE_NEW, OnFileNew)
    ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
    // 標準の印刷セットアップ コマンド
    ON_COMMAND(ID_FILE_PRINT_SETUP, &CMockSketchApp::OnFilePrintSetup)
END_MESSAGE_MAP()


/**
 * @brief   コンストラクタ
 * @param   なし         
 * @retval  なし
 * @note	
 */
CMockSketchApp::CMockSketchApp(BOOL bResourceSmartUpdate):
m_psScriptEngine(0),
m_df(DF_DISABLE)
{
    //_crtBreakAlloc = 414822;

    _set_se_translator(se_translator_function);

	SetAppID(_T("MockTools.MockSketch.000"));
	
	m_bHiColorIcons = TRUE;
    //OSVERSIONINFOW verInfo;
    //verInfo.dwOSVersionInfoSize = sizeof(OSVERSIONINFOW);
    //::GetVersionEx(&verInfo);

    OSVERSIONINFOEX OSver;
    ULONGLONG condition = 0;
    OSver.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
    OSver.dwMajorVersion = 6;
    OSver.dwMinorVersion = 1;
    VER_SET_CONDITION( condition, VER_MAJORVERSION, VER_GREATER_EQUAL);
    VER_SET_CONDITION( condition, VER_MINORVERSION, VER_GREATER_EQUAL);

    m_bWin4 = ::VerifyVersionInfo( &OSver, VER_MAJORVERSION | VER_MINORVERSION, condition);


    m_nDefFont = DEFAULT_GUI_FONT;

    m_dcScreen.Attach(::GetDC(NULL));

    memset(m_psSketchTemplate, 0, sizeof(m_psSketchTemplate));

    m_psEditorTemplate = NULL;
}


/**
 * @brief   デストラクタ
 * @param   なし         
 * @retval  なし
 * @note	
 */
CMockSketchApp::~CMockSketchApp()
{
    CSystem::DeleteInstance();
    if (m_dcScreen.m_hDC != NULL)
    {
        ::ReleaseDC(NULL, m_dcScreen.Detach());
    }

    if (m_psScriptEngine)
    {
        delete m_psScriptEngine;
    }
    Finish();
}

/**
 * @brief   全ユーザ共通設定問い合わせ
 * @param   なし         
 * @retval  true 全ユーザ共通,false 個別保存
 * @note    レジストリにはインストーラで書き込む
 */
bool CMockSketchApp::IsSettingAllUser()
{
    bool bRet;
    bRet = (GetProfileInt(_T("System"), _T("AllUser"), 1) == TRUE);
    return bRet;
}

/**
 * @brief   CMockSketchApp 初期化
 * @param   なし         
 * @retval  true 成功
 * @note	
 */
BOOL CMockSketchApp::InitInstance()
{
    //TODO: call AfxInitRichEdit2() to initialize richedit2 library.

    HANDLE hMutex = CreateMutex (NULL, FALSE, MOCK_SKETCH_MUTEX_NAME);
    if (ERROR_ALREADY_EXISTS == GetLastError ())
    {
        // 二重起動は禁止
        return FALSE;
    }

    if (!m_bWin4)
    {
        // Windows 7 以降
        return false;
    }

    // アプリケーション マニフェストが visual スタイルを有効にするために、
    // ComCtl32.dll Version 6 以降の使用を指定する場合は、
    // Windows XP に InitCommonControlsEx() が必要です。さもなければ、ウィンドウ作成はすべて失敗します。
    INITCOMMONCONTROLSEX InitCtrls;
    InitCtrls.dwSize = sizeof(InitCtrls);
    // アプリケーションで使用するすべてのコモン コントロール クラスを含めるには、
    // これを設定します。
    InitCtrls.dwICC = ICC_WIN95_CLASSES;
    InitCommonControlsEx(&InitCtrls);

    CWinAppEx::InitInstance();

    // OLE ライブラリを初期化します。
    if (!AfxOleInit())
    {
        AfxMessageBox(IDP_OLE_INIT_FAILED);
        return FALSE;
    }
    AfxEnableControlContainer();

    EnableTaskbarInteraction();

    _tsetlocale ( LC_ALL, _T("japanese") ); 
    //std::locale::global( std::locale( "japanese" ) );//数値出力が３桁カンマ区切りになる
    std::locale::global( std::locale( "Japanese", std::locale::ctype ) );
    

    // 標準初期化
    // 設定が格納されているレジストリ キーを変更します。

    SetRegistryKey(_T("MockTools"));

    LoadStdProfileSettings(10);  // 標準の INI ファイルのオプションをロードします (MRU を含む)

    InitContextMenuManager();

    //TODO:６９．ショートカットキーの位置がずれるので一時削除
    InitKeyboardManager();

    InitTooltipManager();

    //カレントディレクトリ・呼び出し/記憶
    boost::filesystem::initial_path<StdPath>();

    //TODO: インストーラでは以下の項目を設定する必要がある

    //ローケールはユーザごとに設定する
    
    //TODO: Ticket#86 システム定義読み込み失敗
    // システム定義読み込み
    CSystem::GetInstance()->Load();

    CMFCToolTipInfo ttParams;
    ttParams.m_bVislManagerTheme = TRUE;
    theApp.GetTooltipManager()->SetTooltipParams(AFX_TOOLTIP_TYPE_ALL,
        RUNTIME_CLASS(CMFCToolTipCtrl), &ttParams);

    // アプリケーション用のドキュメント テンプレートを登録します。ドキュメント テンプレート
    //  はドキュメント、フレーム ウィンドウとビューを結合するために機能します。
    m_psSketchTemplate[0] = new CMultiDocReloadableTemplate(IDR_MockSketchTYPE,
        RUNTIME_CLASS(CMockSketchDoc),
        RUNTIME_CLASS(CChildFrame), // カスタム MDI 子フレーム
        RUNTIME_CLASS(CMockSketchView));
    if (!m_psSketchTemplate[0])
    {
        return FALSE;
    }
    AddDocTemplate(m_psSketchTemplate[0]);

    
    m_psSketchTemplate[1] = new CMultiDocReloadableTemplate(IDR_MockSketchTYPE, //IDR_FIELD_VIEW_TYPE
        RUNTIME_CLASS(CMockSketchDoc),
        RUNTIME_CLASS(CChildFrame), // カスタム MDI 子フレーム
        RUNTIME_CLASS(CMockSketchView));
    if (!m_psSketchTemplate[1])
    {
        return FALSE;
    }
    AddDocTemplate(m_psSketchTemplate[1]);

    m_psSketchTemplate[2] = new CMultiDocReloadableTemplate(IDR_ELEMENT_VIEW_TYPE,
        RUNTIME_CLASS(CMockSketchDoc),
        RUNTIME_CLASS(CChildFrame), // カスタム MDI 子フレーム
        RUNTIME_CLASS(CElementView));
    if (!m_psSketchTemplate[2])
    {
        return FALSE;
    }
    AddDocTemplate(m_psSketchTemplate[2]);

    m_psSketchTemplate[3] = new CMultiDocReloadableTemplate(IDR_MockSketchTYPE, //IDR_REFERENCE_VIEW_TYPE
        RUNTIME_CLASS(CMockSketchDoc),
        RUNTIME_CLASS(CChildFrame), // カスタム MDI 子フレーム
        RUNTIME_CLASS(CMockSketchView));
    if (!m_psSketchTemplate[3])
    {
        return FALSE;
    }
    AddDocTemplate(m_psSketchTemplate[3]);


    //============================== これを追加
    m_psEditorTemplate = new CMultiDocReloadableTemplate(IDR_EDIT_VIEW_TYPE,
        RUNTIME_CLASS(CMockSketchDoc),
        RUNTIME_CLASS(CChildFrame), 
        RUNTIME_CLASS(CEditorView)
        );

    if (!m_psEditorTemplate)
    {
        return FALSE;
    }
    AddDocTemplate(m_psEditorTemplate);
    //============================== これを追加

    //リッチエディット初期化
    AfxInitRichEdit2();

    // メイン MDI フレーム ウィンドウを作成します。
    CMainFrame* pMainFrame = new CMainFrame;
    if (!pMainFrame || !pMainFrame->LoadFrame(IDR_MAINFRAME))
    {
        delete pMainFrame;
        return FALSE;
    }

    m_pMainWnd  = pMainFrame;

    // 接尾辞が存在する場合にのみ DragAcceptFiles を呼び出します。
    //  MDI アプリケーションでは、この呼び出しは、m_pMainWnd を設定した直後に発生しなければなりません。
    // ドラッグ/ドロップ オープンを許可します。
    m_pMainWnd->DragAcceptFiles();

    // DDE Execute open を使用可能にします。
    EnableShellOpen();
    RegisterShellFileTypes(TRUE);

    // DDE、file open など標準のシェル コマンドのコマンド ラインを解析します。
    CCommandLineInfo cmdInfo;
    ParseCommandLine(cmdInfo);


    // コマンド ラインで指定されたディスパッチ コマンドです。アプリケーションが
    // /RegServer、/Register、/Unregserver または /Unregister で起動された場合、False を返します。
    if (!ProcessShellCommand(cmdInfo))
    {
        return FALSE;
    }

    //スクリプトエンジン作成
    m_psScriptEngine = new CScriptEngine;

    if (!m_psScriptEngine->GetEngine())
    {
        return FALSE;
    }

    pMainFrame->LoadUserLibrary();

    // メイン ウィンドウが初期化されたので、表示と更新を行います。
    pMainFrame->ShowWindow(m_nCmdShow);
    pMainFrame->UpdateWindow();

#ifdef _DEBUG
    if(CSystemInit::GetTest())
    {
        test();
    }
#endif
    
    Gdiplus::GdiplusStartupInput gdiplusStartupInput;
    Gdiplus::GdiplusStartup(&m_gdiplusToken, &gdiplusStartupInput, NULL);

    InitPaperSize();

	return TRUE;
}

//------------------------------------------------

//------------------------------------------------

// ダイアログを実行するためのアプリケーション コマンド
void CMockSketchApp::OnAppAbout()
{
    CAboutDlg aboutDlg;
    aboutDlg.DoModal();
}

/**
 *  レジストリ読み込み前処理.
 *  @param      なし
 *  @retval     なし
 *  @note       フレームワークは、レジストリからアプリケーションの
 *  @note       状態を読み込む直前に、このメソッドを呼び出します。
 */
void CMockSketchApp::PreLoadState()
{
    BOOL bNameValid;
    CString strName;

    HMENU hMenu;
    HMENU hPopupMenu;


    //-----------------------
    bNameValid = strName.LoadString(IDS_EDIT_MENU);
    ASSERT(bNameValid);
    GetContextMenuManager()->AddMenu(strName, IDR_POPUP_EDIT);
    //-----------------------
    //=======================
    //レイヤーポップアップ
    //=======================
    GetContextMenuManager()->AddMenu(GET_STR(STR_MNU_LAYER), IDR_POPUP_LAYER);

    hMenu = GetContextMenuManager()->GetMenuById(IDR_POPUP_LAYER);
    STD_ASSERT(hMenu != NULL);
    
    hPopupMenu = ::GetSubMenu(hMenu, 0);

    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_LAYER_ADD), ID_MNU_LAYER_ADD , NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_LAYER_DEL), ID_MNU_LAYER_DEL , NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_LAYER_SCL), ID_MNU_LAYER_SCL , NULL);

    //ダミーを削除
    DeleteMenu(hPopupMenu , ID_MNU_DUMMY , MF_BYCOMMAND );

    //=======================
    //描画ポップアップ
    //=======================
    GetContextMenuManager()->AddMenu(GET_STR(STR_MNU_DRAW), IDR_POPUP_VIEW);

    hMenu = GetContextMenuManager()->GetMenuById(IDR_POPUP_VIEW);
    STD_ASSERT(hMenu != NULL);
    
    hPopupMenu = ::GetSubMenu(hMenu, 0);

    //選択
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_SELECT), ID_MNU_SELECT , NULL);

    //CUTモード
    CUtil::AddMenuCheck(hPopupMenu, GET_STR(STR_MNU_CUT_MODE), ID_MNU_CUT_MODE , true);

    //COPYモード
    CUtil::AddMenuCheck(hPopupMenu, GET_STR(STR_MNU_COPY_MODE), ID_MNU_COPY_MODE , true);

    CUtil::AddMenuItem(hPopupMenu, NULL, 0, NULL);

    //点
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_POINT), ID_MNU_POINT , NULL);

    //直線
    //HMENU hSubMenuLine;
    //hSubMenuLine = CreatePopupMenu();
    //    CUtil::AddMenuItem(hSubMenuLine, GET_STR(STR_MNU_H_LINE),   ID_MNU_H_LINE , NULL);
    //    CUtil::AddMenuItem(hSubMenuLine, GET_STR(STR_MNU_V_LINE),   ID_MNU_V_LINE , NULL);
    //    CUtil::AddMenuItem(hSubMenuLine, GET_STR(STR_MNU_A_LINE),   ID_MNU_A_LINE , NULL);
    //    CUtil::AddMenuItem(hSubMenuLine, GET_STR(STR_MNU_LINE_SEG), ID_MNU_LINE   , NULL);
    //CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_LINE), ID_MNU_LINE , hSubMenuLine);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_LINE_SEG), ID_MNU_LINE   , NULL);

    //円
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_CIRCLE), ID_MNU_CIRCLE , NULL);

    //寸法
    hMenu = GetContextMenuManager()->GetMenuById(IDR_DIM_PALETTE);

    HMENU hSubMenuDim;

    hSubMenuDim = CreatePopupMenu();
    CUtil::AddMenuItem(hSubMenuDim, GET_STR(STR_MNU_L_DIM),   ID_MNU_L_DIM , NULL);
    CUtil::AddMenuItem(hSubMenuDim, GET_STR(STR_MNU_H_DIM),   ID_MNU_H_DIM , NULL);
    CUtil::AddMenuItem(hSubMenuDim, GET_STR(STR_MNU_V_DIM),   ID_MNU_V_DIM , NULL);
    CUtil::AddMenuItem(hSubMenuDim, GET_STR(STR_MNU_A_DIM),   ID_MNU_A_DIM , NULL);
    CUtil::AddMenuItem(hSubMenuDim, GET_STR(STR_MNU_D_DIM),   ID_MNU_D_DIM , NULL);
    CUtil::AddMenuItem(hSubMenuDim, GET_STR(STR_MNU_R_DIM),   ID_MNU_R_DIM , NULL);
    CUtil::AddMenuItem(hSubMenuDim, GET_STR(STR_MNU_C_DIM),   ID_MNU_C_DIM , NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_DIM),   ID_MNU_DIM , hSubMenuDim);

    //節点
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_NODE), ID_MNU_NODE , NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_IMAGE), ID_MNU_IMAGE , NULL);

    CUtil::AddMenuItem(hPopupMenu, NULL, 0, NULL);

    //オフセット
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_OFFSET), ID_MNU_OFFSET , NULL);

    //コーナー
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_CORNER), ID_MNU_CORNER , NULL);

    //面取り
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_CHAMFER), ID_MNU_CHAMFER , NULL);

    //トリム
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_TRIM), ID_MNU_TRIM , NULL);

    //分割
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_DEVIDE), ID_MNU_DEVIDE , NULL);

    //移動
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_ITEM_MOVE), ID_MNU_MOVE , NULL);

    //移動
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_STRETCH_MODE), ID_MNU_STRETCH_MODE , NULL);

    //ストレッチモード
    //CUtil::AddMenuCheck(hPopupMenu, GET_STR(STR_MNU_STRETCH_MODE)   , ID_MNU_STRETCH_MODE,
    //    DRAW_CONFIG->GetStretchMode());

    //複写
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_ITEM_COPY), ID_MNU_COPY , NULL);

    //
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_COMPOSITION_LINE), ID_MNU_COMPOSITION_LINE , NULL);

    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_CONNECTION_LINE), ID_MNU_CONNECTION_LINE , NULL);

    
    //ダミーを削除
    DeleteMenu(hPopupMenu , ID_MNU_DUMMY , MF_BYCOMMAND );

    //=======================


    UINT nMenuResId;
    //---------------------
    // オブジェクトビュー
    //---------------------
    nMenuResId = IDR_POPUP_OBJECT_VIEW;
    GetContextMenuManager()->AddMenu( GET_STR(STR_OBJECT_VIEW), nMenuResId);
    hMenu = GetContextMenuManager()->GetMenuById(nMenuResId);

    STD_ASSERT(hMenu != NULL);
    
    hPopupMenu = ::GetSubMenu(hMenu, 0);
    STD_ASSERT(hPopupMenu != NULL);


    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_FILE_OPEN), ID_MNU_OBJ_OPEN , NULL);
    CUtil::AddMenuItem(hPopupMenu, NULL, 0, NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_OBJ_FOLDER), ID_MNU_OBJ_FOLDER , NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_OBJ_NAME)  , ID_MNU_OBJ_NAME   , NULL);
    CUtil::AddMenuItem(hPopupMenu, NULL, 0, NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_OBJ_SAVE)  , ID_MNU_OBJ_SAVE   , NULL);
    CUtil::AddMenuItem(hPopupMenu, NULL, 0, NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_OBJ_PARTS)      , ID_MNU_OBJ_PARTS  , NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_OBJ_REFERENCE)  , ID_MNU_OBJ_REFERENCE  , NULL);

    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_OBJ_ELEMENT), ID_MNU_OBJ_ELEMENT, NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_OBJ_FIELD)  , ID_MNU_OBJ_FIELD  , NULL);
    CUtil::AddMenuItem(hPopupMenu, NULL, 0, NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_OBJ_SCRIPT), ID_MNU_OBJ_SCRIPT , NULL);
    CUtil::AddMenuItem(hPopupMenu, NULL, 0, NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_OBJ_CUT)   , ID_MNU_OBJ_CUT   , NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_OBJ_COPY)  , ID_MNU_OBJ_COPY   , NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_OBJ_PASTE) , ID_MNU_OBJ_PASTE  , NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_OBJ_DELETE), ID_MNU_OBJ_DELETE , NULL);
    //---------------------




    //ダミーを削除
    DeleteMenu(hPopupMenu , ID_MNU_DUMMY , MF_BYCOMMAND );


    //---------------------
    // エディターポップアップ
    //---------------------
    nMenuResId = IDR_POPUP_EDITOR;
    GetContextMenuManager()->AddMenu( GET_STR(STR_EDITOR_VIEW), nMenuResId);
    hMenu = GetContextMenuManager()->GetMenuById(nMenuResId);

    STD_ASSERT(hMenu != NULL);
    hPopupMenu = ::GetSubMenu(hMenu, 0);
    STD_ASSERT(hPopupMenu != NULL);


    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_EDITOR_CUT)    , ID_EDIT_CUT, NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_EDITOR_PASTE)  , ID_EDIT_COPY, NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_EDITOR_COPY)   , ID_MNU_OBJ_NAME   , NULL);

    CUtil::AddMenuItem(hPopupMenu, NULL, 0, NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_SCRIPT_BUILD) , ID_MNU_SCRIPT_BUILD  , NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_SCRIPT_REBUILD) , ID_MNU_SCRIPT_REBUILD  , NULL);

    CUtil::AddMenuItem(hPopupMenu, NULL, 0, NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_SCRIPT_STOP_BUILD) , ID_MNU_SCRIPT_STOP_BUILD  , NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_SCRIPT_EXEC), ID_MNU_SCRIPT_EXEC , NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_SCRIPT_ABORT), ID_MNU_SCRIPT_ABORT , NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_SCRIPT_PAUSE), ID_MNU_SCRIPT_PAUSE , NULL);

    CUtil::AddMenuItem(hPopupMenu, NULL, 0, NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_EDITOR_BREAKPOINT_SET)   , ID_MNU_TOGGLEBREAKPOINT   , NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_EDITOR_CLEAR_BREAKPOINT) , ID_MNU_REMOVE_ALL_BREAKPOINTS , NULL);
    CUtil::AddMenuItem(hPopupMenu, NULL, 0, NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_SCRIPT_STEP_IN)          , ID_MNU_STEP_IN , NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_SCRIPT_STEP_OVER)        , ID_MNU_STEP_OVER , NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_SCRIPT_EXEC_CUROSR)      , ID_MNU_SCRIPT_EXEC_CURSOR , NULL);
    //CUtil::AddMenuItem(hPopupMenu, NULL, 0, NULL);
    //CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_SCRIPT_DEBUG)        , ID_MNU_SCRIPT_DEBUG , NULL);
    //CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_SCRIPT_RELEASE)      , ID_MNU_SCRIPT_RELEASE , NULL);


    //CUtil::AddMenuCheck(hPopupMenu, GET_STR(STR_MNU_SCRIPT_DEBUG)     , ID_MNU_SCRIPT_DEBUG, true);
    //CUtil::AddMenuCheck(hPopupMenu, GET_STR(STR_MNU_SCRIPT_RELEASE)   , ID_MNU_SCRIPT_RELEASE, true);


    //ダミーを削除
    DeleteMenu(hPopupMenu , ID_MNU_DUMMY , MF_BYCOMMAND );


    //---------------------
    // ウォッチポップアップ
    //---------------------
    nMenuResId = IDR_POPUP_WATCH;
    GetContextMenuManager()->AddMenu( GET_STR(STR_WATCH), nMenuResId);
    hMenu = GetContextMenuManager()->GetMenuById(nMenuResId);

    STD_ASSERT(hMenu != NULL);
    hPopupMenu = ::GetSubMenu(hMenu, 0);
    STD_ASSERT(hPopupMenu != NULL);

    //メニューの置換
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_WATCH_COPY)      , ID_EDIT_COPY, NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_WATCH_PASTE)     , ID_EDIT_PASTE, NULL);
    CUtil::AddMenuItem(hPopupMenu, NULL, 0, NULL);

    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_WATCH_EDIT)     , ID_MNU_WATCH_EDIT   , NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_WATCH_SEL)      , ID_MNU_WATCH_SEL   , NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_WATCH_CLEAER)   , ID_MNU_WATCH_CLEAR   , NULL);
    CUtil::AddMenuItem(hPopupMenu, NULL, 0, NULL);

    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_WATCH_HEX)   , ID_MNU_WATCH_HEX   , NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_CONTRACT)    , ID_MNU_WATCH_CONTRACT   , NULL);


    //ダミーを削除
    DeleteMenu(hPopupMenu , ID_MNU_DUMMY , MF_BYCOMMAND );


    //=======================
    //プロパティエディタポップアップ
    //=======================
    GetContextMenuManager()->AddMenu(GET_STR(STR_MNU_PROPED), IDR_POPUP_PROP_EDITOR);

    hMenu = GetContextMenuManager()->GetMenuById(IDR_POPUP_PROP_EDITOR);
    STD_ASSERT(hMenu != NULL);
    
    hPopupMenu = ::GetSubMenu(hMenu, 0);

    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_EDITOR_CUT)       , ID_EDIT_CUT, NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_EDITOR_COPY)      , ID_EDIT_COPY, NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_EDITOR_PASTE)     , ID_EDIT_PASTE, NULL);
    CUtil::AddMenuItem(hPopupMenu, NULL, 0, NULL);

    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_PROPED_UP)      , ID_ITEM_UP, NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_PROPED_DOWN)    , ID_ITEM_DOWN, NULL);
    CUtil::AddMenuItem(hPopupMenu, NULL, 0, NULL);

    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_PROPED_ADD), ID_MNU_PROPED_ADD , NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MNU_PROPED_DEL), ID_MNU_PROPED_DEL , NULL);

    //ダミーを削除
    DeleteMenu(hPopupMenu , ID_MNU_DUMMY , MF_BYCOMMAND );


    //=======================
    //タブグループポップアップ
    //=======================
    GetContextMenuManager()->AddMenu(GET_STR(STR_MNU_MDITABS), IDR_POPUP_MDITABS);

    hMenu = GetContextMenuManager()->GetMenuById(IDR_POPUP_MDITABS);
    STD_ASSERT(hMenu != NULL);
    
    hPopupMenu = ::GetSubMenu(hMenu, 0);

    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_FILE_SAVE), ID_FILE_SAVE , NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_FILE_CLOSE), ID_FILE_CLOSE , NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MDI_TABBED_DOCUMENT), ID_MDI_TABBED_DOCUMENT , NULL);
    CUtil::AddMenuItem(hPopupMenu, NULL, 0, NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MDI_NEW_HORZ_TAB_GROUP), ID_MDI_NEW_HORZ_TAB_GROUP , NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MDI_NEW_VERT_GROUP), ID_MDI_NEW_VERT_GROUP , NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MDI_MOVE_TO_NEXT_GROUP), ID_MDI_MOVE_TO_NEXT_GROUP , NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MDI_MOVE_TO_PREV_GROUP), ID_MDI_MOVE_TO_PREV_GROUP , NULL);

    //ダミーを削除
    DeleteMenu(hPopupMenu , ID_MNU_DUMMY , MF_BYCOMMAND );


    //=======================
    //タブグループドロップダウン
    //=======================
    GetContextMenuManager()->AddMenu(GET_STR(STR_MNU_DROP_MDITABS), IDR_POPUP_DROP_MDITABS);

    hMenu = GetContextMenuManager()->GetMenuById(IDR_POPUP_DROP_MDITABS);
    STD_ASSERT(hMenu != NULL);
    
    hPopupMenu = ::GetSubMenu(hMenu, 0);

    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MDI_NEW_HORZ_TAB_GROUP), ID_MDI_NEW_HORZ_TAB_GROUP , NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MDI_NEW_VERT_GROUP), ID_MDI_NEW_VERT_GROUP , NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MDI_MOVE_TO_NEXT_GROUP), ID_MDI_MOVE_TO_NEXT_GROUP , NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MDI_MOVE_TO_PREV_GROUP), ID_MDI_MOVE_TO_PREV_GROUP , NULL);
    CUtil::AddMenuItem(hPopupMenu, NULL, 0, NULL);
    CUtil::AddMenuItem(hPopupMenu, GET_STR(STR_MDI_CLOSE), ID_MDI_CANCEL , NULL);

    //ダミーを削除
    DeleteMenu(hPopupMenu , ID_MNU_DUMMY , MF_BYCOMMAND );

}


/**
 *  レジストリ読み込み後処理.
 *  @param      なし
 *  @retval     なし
 *  @note       フレームワークは、レジストリからアプリケーションの
 *  @note       状態を読み込み後に、このメソッドを呼び出します。
 */
void CMockSketchApp::LoadCustomState()
{

    //バージョンが変更されていた場合はリセットしておく
    // タイミングはもう一度調べておく
    //GetContextMenuManager()->ResetState();
}

/**
 *  レジストリ保存後処理.
 *  @param      なし
 *  @retval     なし
 *  @note       フレームワークは、レジストリからアプリケーションの
 *  @note       状態を保存した後に、このメソッドを呼び出します。
 */
void CMockSketchApp::SaveCustomState()
{
}

/**
 *  アプリケーション終了時呼び出し.
 *  @param      なし
 *  @retval     アプリケーションの終了コードを返します。エラーがなかった場合は 0 を
 *              返します。
 *              エラーが発生した場合は 0 より大きい値を返します。この値は、WinMain 
 *              からの戻り値に使われます
 *  @note       この関数の既定の実装では、アプリケーションの .INI ファイルに
 *              フレームワークのオプションを書き込みます。アプリケーション終了時の
 *              後処理をするときは、この関数をオーバーライドします。
 */
int CMockSketchApp::ExitInstance()
{
    if (m_psScriptEngine)
    {
        delete m_psScriptEngine;
        m_psScriptEngine = NULL;
    }

    CSystem::DeleteInstance();
    SYS_LIBRARY->UnregisterAddins(CSystemLibrary::E_ADDIN_IO);
    SYS_LIBRARY->UnregisterAddins(CSystemLibrary::E_ADDIN_AS);

    CScriptEngine::ReleaseTextConvMap();

    Gdiplus::GdiplusShutdown(m_gdiplusToken);

    int iRet;
    iRet = CWinAppEx::ExitInstance();
    //_FixTypeInfoBlockUse();
    return iRet;
}


/**
 * @brief   新規作成
 * @param   なし 
 * @return  なし
 * @note    起動時に呼ばれる
 * @note    
 */
void CMockSketchApp::OnFileNew()
{
    CMainFrame* pMainFrm =  dynamic_cast<CMainFrame*>(m_pMainWnd);

    // 既にViewがあるときは作成できない
    // (MainとなるObject, Viewは１つだけ)
    if (m_lstView.size() != 0)
    {
        STD_DBG(_T("OnFileNew Error size=%d"), m_lstView.size());
        return;
    }
    //ロード開始
    ::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_VIEW_LOAD_START, 
        (WPARAM)0, (LPARAM) 0);

    //!< プロジェクト取得
    std::shared_ptr<CProjectCtrl> pProject;
    pProject = pMainFrm->GetProject(VIEW_COMMON::E_PROJ_MAIN).lock();


    pProject->Load(_T(""));

    //ロード完了を通知
    pMainFrm->SendMessage( WM_VIEW_LOAD_END, 
                           (WPARAM)pProject.get(), 
                           (LPARAM)VIEW_COMMON::E_PROJ_MAIN);


    //内部的には以下の動作が行われる
    /*
    CMockSketchApp::OnFileNew()
        CWinAppEx::OnFileNew()
            CDocManager::OnFileNew();
                CMultiDocTemplate::OpenDocumentFile(NULL);
                {
                    //ここでドキュメント生成
                    CDocument* pDocument = CreateNewDocument();
                    {
                       CDocument* pDocument = (CDocument*)m_pDocClass->CreateObject();
                    }
                    //ここでフレーム生成
                    CFrameWnd* pFrame = CreateNewFrame(pDocument, NULL);
                        OnSizeイベント発生
                    SetDefaultTitle(pDocument);
                }
    */

}


/**
 * @brief   ファイルを開く
 * @param       なし 	  
 * @return		なし
 * @note	 
 * @exception   なし
 */
void CMockSketchApp::OnFileOpen()
{
    //CWinAppEx::OnFileOpen();

    //ロード完了を通知
    //SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_VIEW_LOAD_END, (WPARAM)this, 0);

}

/**
 * @brief   ビューデータ変更
 * @param   [in]   pPartsCtrl  パーツオブジェクト
 * @return  なし
 * @note    
 */
/*
void CMockSketchApp::ChangeViewData(CPartsDef* pPartsCtrl)
{
    CMainFrame* pMainFrame = dynamic_cast<CMainFrame*>(m_pMainWnd);
    STD_ASSERT(pMainFrame != NULL);
}
*/

/**
 * @brief   プロジェクト取得
 * @param   なし
 * @return  プロジェクトへのポインタ
 * @note	 
 */
std::weak_ptr<CProjectCtrl> CMockSketchApp::GetProject(VIEW_COMMON::E_PROJ_TYPE eType) const
{
    std::weak_ptr<CProjectCtrl> pRet;
    CMainFrame* pMainFrame = dynamic_cast<CMainFrame*>(m_pMainWnd);
    if (pMainFrame)
    {
        return pMainFrame->GetProject(eType);
    }
    else
    {
        return pRet;
    }
}

/**
 * @brief   プロジェクト取得
 * @param   なし
 * @return  プロジェクトへのポインタ
 * @note	 
 */
CExecCtrl* CMockSketchApp::GetExecCtrl() const
{
    CMainFrame* pMainFrame = dynamic_cast<CMainFrame*>(m_pMainWnd);
    if (pMainFrame)
    {
        return pMainFrame->GetExecCtrl();
    }
    else
    {
        return NULL;
    }
}

/**
 * @brief   スクリプトエンジン取得
 * @param   なし
 * @return  スクリプトエンジンへのポインタ
 * @note	 
 */
CScriptEngine* CMockSketchApp::GetScriptEngine()
{
    return m_psScriptEngine;
}

/**
 * @brief   メインフレームの有無
 * @param   なし
 * @return  true メインフレーム生成済み
 * @note	 
 */
bool CMockSketchApp::IsExistMain()
{
    CMainFrame* pMainFrame = dynamic_cast<CMainFrame*>(m_pMainWnd);
    if(pMainFrame != NULL)
    {
        return true;
    }
    return false;
}

/**
 * @brief   メインフレームの取得
 * @param   なし
 * @return  メインフレームへのポインタ
 * @note	 
 */
CMainFrame* CMockSketchApp::GetMainFrame()
{
    CMainFrame* pMainFrame = dynamic_cast<CMainFrame*>(m_pMainWnd);
    return pMainFrame;
}

/**
 * @brief   メインフレームの取得
 * @param   なし
 * @return  メインフレームのハンドル
 * @note    
 */
HWND CMockSketchApp::GetMainFrameHandle()
{
    return m_pMainWnd->m_hWnd;
}

/**
 * @brief   新しいビューを開く
 * @param   [in]   pPartsCtrl  パーツオブジェクト
 * @return  なし
 * @note	 
 */
void CMockSketchApp::OpenNewView(boost::uuids::uuid uuidParts)
{
DB_PRINT(_T("OpenNewView  Start\n"));
    CMainFrame* pMainFrame = dynamic_cast<CMainFrame*>(m_pMainWnd);
    STD_ASSERT(pMainFrame != NULL);

    //まずはViewの中から検索(Viewがある場合)
    CString   strTitle;

    StdString strName = GetObjectDefTabName(uuidParts);

    CWnd* pWmd;
    CView* pView;
    foreach(CObjectDefView* pDefView,  m_lstView)
    {
        pWmd = pDefView->GetWindow();
        pView = dynamic_cast<CView*>(pWmd);
        if (pView == NULL)
        {
            STD_DBG(_T("Select tab error %s"), strName);
            continue;
        }


        strTitle = pView->GetDocument()->GetTitle();
        strTitle = CMockSketchDoc::SplitTitleMarker(strTitle);

        if (strTitle ==  strName.c_str())
        {
            //MDIタブを選択
            pView->GetParentFrame()->ActivateFrame();
            return;
        }
    }

DB_PRINT(_T("OpenNewView 1\n"));

    //-----------------------------------------
    //新しいドキュメントを生成し開く
    // CMultiDocTemplate::OpenDocumentFile の処理を参考に
    //CDocument* pDoc = m_psSketchTemplate->OpenDocumentFile(NULL);
    //-----------------------------------------

    //ドキュメントを生成
    std::shared_ptr<CObjectDef> pObjDef;
    pObjDef = GetObjectDefByUuid(uuidParts).lock();

    if (!pObjDef)
    {
        return;
    }

    VIEW_COMMON::E_OBJ_DEF_TYPE eObjType = pObjDef->GetObjectType();
    CMultiDocTemplate* pTemplate = NULL;

    if (eObjType == VIEW_COMMON::E_PARTS)
    {
        pTemplate = m_psSketchTemplate[0];
    }   
    else if (eObjType == VIEW_COMMON::E_FIELD)
    {
        pTemplate = m_psSketchTemplate[1];
    }   
    else if (eObjType == VIEW_COMMON::E_ELEMENT)
    {
        pTemplate = m_psSketchTemplate[2];
    }   

    else if (eObjType == VIEW_COMMON::E_REFERENCE)
    {
        pTemplate = m_psSketchTemplate[3];
    }   
    STD_ASSERT(pTemplate);


    CDocument* pDocument = pTemplate->CreateNewDocument();
    if (pDocument == NULL)
    {
        STD_DBG(_T("CDocTemplate::CreateNewDocument returned NULL."));
        AfxMessageBox(AFX_IDP_FAILED_TO_CREATE_DOC);
        return;
    }

    ASSERT_VALID(pDocument);
DB_PRINT(_T("OpenNewView 2\n"));

    //フレームの生成
    BOOL bAutoDelete = pDocument->m_bAutoDelete;
    pDocument->m_bAutoDelete = FALSE;   // don't destroy if something goes wrong

    CFrameWnd* pFrame = pTemplate->CreateNewFrame(pDocument, NULL);
    pDocument->m_bAutoDelete = bAutoDelete;
    if (pFrame == NULL)
    {
        STD_DBG(_T("CDocTemplate::CreateNewFrame returned NULL."));
        AfxMessageBox(AFX_IDP_FAILED_TO_CREATE_DOC);
        delete pDocument;       // explicit delete on error
        return;
    }
    ASSERT_VALID(pFrame);
DB_PRINT(_T("OpenNewView 3\n"));

    pDocument->m_bEmbedded = TRUE;

    if (!pDocument->OnNewDocument())
    {
        // user has be alerted to what failed in OnNewDocument
        TRACE(traceAppMsg, 0, "CDocument::OnNewDocument returned FALSE.\n");
        pFrame->DestroyWindow();
        return;
    }

    // OpenDocumentFileと動作をあわせるため
    // フレームが作成されてからドキュメントに pPartsCtrlを設定する


    CMockSketchDoc* pSketchDoc = dynamic_cast<CMockSketchDoc*>(pDocument);

    pSketchDoc->SetObjectDef(uuidParts);
DB_PRINT(_T("OpenNewView 4\n"));

    pTemplate->InitialUpdateFrame(pFrame, pDocument, TRUE);
DB_PRINT(_T("InitialUpdateFrame  End\n"));
        //CFrameWnd::InitialUpdateFrameが内部で呼ばれる
        //   ドキュメントに設定しているタイトルが表示される
        //    CFrameWnd::UpdateFrameTitleForDocument(pDocument->GetTitle());
}

/**
 * @brief   新しい実行ビューを開く
 * @param   [in]   pScriptBase  パーツオブジェクト
 * @return  なし
 * @note	 
 */
void CMockSketchApp::OpenNewExecView(std::weak_ptr<CObjectDef> pExecDef)
{
    CMainFrame* pMainFrame = dynamic_cast<CMainFrame*>(m_pMainWnd);
    STD_ASSERT(pMainFrame != NULL);

    std::shared_ptr<CObjectDef> pObjDef;
    pObjDef = pExecDef.lock();

    if (!pObjDef)
    {
        STD_DBG(_T("OpenNewExecView::pObjDef NULL."));
        AfxMessageBox(AFX_IDP_FAILED_TO_CREATE_DOC);
        return;
    }

    //まずはViewの中から検索(Viewがある場合)
    CString   strTitle;
    StdString strName;

    auto pBase = pObjDef->GetViewObject();

    strName = GetScriptBaseName(pBase.get());

    CWnd* pWmd;
    CView* pView;
    foreach(CObjectDefView* pDefView,  m_lstView)
    {
        pWmd = pDefView->GetWindow();
        pView = dynamic_cast<CView*>(pWmd);
        if (pView == NULL)
        {
            STD_DBG(_T("Select tab error %s"), strName);
            continue;
        }

        strTitle = pView->GetDocument()->GetTitle();
        strTitle = CMockSketchDoc::SplitTitleMarker(strTitle);

        if (strTitle ==  strName.c_str())
        {
            //MDIタブを選択
            pView->GetParentFrame()->ActivateFrame();
            return;
        }
    }


    //-----------------------------------------
    //新しいドキュメントを生成し開く
    // CMultiDocTemplate::OpenDocumentFile の処理を参考に
    //CDocument* pDoc = m_psSketchTemplate->OpenDocumentFile(NULL);
    //-----------------------------------------

    //ドキュメントを生成

    VIEW_COMMON::E_OBJ_DEF_TYPE eObjType = pObjDef->GetObjectType();
    CMultiDocTemplate* pTemplate = NULL;

    if (eObjType == VIEW_COMMON::E_PARTS)
    {
        pTemplate = m_psSketchTemplate[0];
    }   
    else if (eObjType == VIEW_COMMON::E_FIELD)
    {
        pTemplate = m_psSketchTemplate[1];
    }   
    else if (eObjType == VIEW_COMMON::E_ELEMENT)
    {
        pTemplate = m_psSketchTemplate[2];
    }   
    else if (eObjType == VIEW_COMMON::E_REFERENCE)
    {
        pTemplate = m_psSketchTemplate[3];
    }   

    STD_ASSERT(pTemplate);


    CDocument* pDocument = pTemplate->CreateNewDocument();
    if (pDocument == NULL)
    {
        STD_DBG(_T("CDocTemplate::CreateNewDocument returned NULL."));
        AfxMessageBox(AFX_IDP_FAILED_TO_CREATE_DOC);
        return;
    }

    ASSERT_VALID(pDocument);

    //フレームの生成
    BOOL bAutoDelete = pDocument->m_bAutoDelete;
    pDocument->m_bAutoDelete = FALSE;   // don't destroy if something goes wrong

    CFrameWnd* pFrame = pTemplate->CreateNewFrame(pDocument, NULL);
    pDocument->m_bAutoDelete = bAutoDelete;
    if (pFrame == NULL)
    {
        STD_DBG(_T("CDocTemplate::CreateNewFrame returned NULL."));
        AfxMessageBox(AFX_IDP_FAILED_TO_CREATE_DOC);
        delete pDocument;       // explicit delete on error
        return;
    }
    ASSERT_VALID(pFrame);

    pDocument->m_bEmbedded = TRUE;

    if (!pDocument->OnNewDocument())
    {
        // user has be alerted to what failed in OnNewDocument
        TRACE(traceAppMsg, 0, "CDocument::OnNewDocument returned FALSE.\n");
        pFrame->DestroyWindow();
        return;
    }

    // OpenDocumentFileと動作をあわせるため
    // フレームが作成されてからドキュメントに pPartsCtrlを設定する


    CMockSketchDoc* pSketchDoc = dynamic_cast<CMockSketchDoc*>(pDocument);
    pSketchDoc->SetExecDef(pExecDef);

    pTemplate->InitialUpdateFrame(pFrame, pDocument, TRUE);


        //CFrameWnd::InitialUpdateFrameが内部で呼ばれる
        //   ドキュメントに設定しているタイトルが表示される
        //    CFrameWnd::UpdateFrameTitleForDocument(pDocument->GetTitle());
}


/**
 * @brief   プロジェクト設定
 * @param   [in]   pathPrj プロジェクトパス
 * @param   [in]   strName プロジェクト名
 * @return  なし
 * @note    
 * @exception   なし
 */
void CMockSketchApp::SetProject(StdPath pathPrj, StdString strName)
{


}

/**
 * @brief   エディタを開く
 * @param       なし 	  
 * @return		なし
 * @note	 エディタのタブ名称は(スクリプト名(パーツ))
 *           
 */
bool CMockSketchApp::ReloadEditorFile(StdPath Path, bool bSimulation)
{
    //まずはViewの中から検索(Viewがある場合)
    CString strTitle;
    CMockSketchDoc* pSketchDoc;
    StdString strScriptName;
    StdString strTabObjectName;
    CScriptData* pScriptData;

    StdPath pathScript;
    
    foreach(CWnd* pWmd,  m_lstEditor)
    {
        CEditorView* pView = dynamic_cast<CEditorView*>(pWmd);
        if (pView == NULL)
        {
            continue;
        }
        
        pSketchDoc = dynamic_cast<CMockSketchDoc*>(pView->GetDocument());

        if(!pSketchDoc)
        {
            continue;
        }

        strTitle = pView->GetDocument()->GetTitle();

    
        if(! CObjectDef::DivideScriptTabName(&strScriptName, 
                                               &strTabObjectName, 
                                               (LPCTSTR)strTitle))
        {
            continue;
        }


        std::shared_ptr<CObjectDef> pDef;
        pDef = GetObjectDefByTabName(strTabObjectName).lock();
        if (!pDef)
        {
            continue;
        }

        pScriptData = pDef->GetScriptInstance(strScriptName);

        STD_ASSERT(pScriptData);
        if (!pScriptData)
        {
            continue;
        }

        pathScript = pScriptData->GetCurrentPath();

        if (pathScript != Path )
        {
            continue;
        }
        
        if (!bSimulation)
        {
            pScriptData->LoadScript(Path);
        }
        return true;
    }
    return false;
}


/**
 * @brief   エディタを開く
 * @param       なし 	  
 * @return		なし
 * @note	 エディタのタブ名称は(スクリプト名(パーツ))
 *           
 */
void CMockSketchApp::OpenNewEditor(boost::uuids::uuid uuidParts, 
                                   LPCTSTR strScriptName,
                                   bool bReload)
{
	STD_ASSERT(m_psEditorTemplate != NULL);
	// = pMainFrame;

    CMainFrame* pMainFrame = dynamic_cast<CMainFrame*>(m_pMainWnd);
    STD_ASSERT(pMainFrame != NULL);

    StdString strTabName; 

    strTabName = GetObjectDefTabName(uuidParts, strScriptName);

    //まずはViewの中から検索(Viewがある場合)
    CString strTitle;
    foreach(CWnd* pWmd,  m_lstEditor)
    {
        CEditorView* pView = dynamic_cast<CEditorView*>(pWmd);
        if (pView == NULL)
        {
            STD_DBG(_T("Select tab error %s"), strTabName);
            continue;
        }

        strTitle = pView->GetDocument()->GetTitle();
        strTitle = CMockSketchDoc::SplitTitleMarker(strTitle);

        if (strTitle == strTabName.c_str())
        {
            //MDIタブを選択
            pView->GetParentFrame()->ActivateFrame();

            if (bReload)
            {
                pView->Reload();
            }
            return;
        }
    }

    std::shared_ptr<CObjectDef> pObjDef;
    
    
    pObjDef = GetObjectDefByUuid(uuidParts).lock();

    if (!pObjDef)
    {
        STD_DBG(_T("CDocTemplate::CreateNewDocument (!pObjDef)"));
        AfxMessageBox(AFX_IDP_FAILED_TO_CREATE_DOC);
        return;
    }

    //-----------------------------------------
    //新しいドキュメントを生成し開く
    // CMultiDocTemplate::OpenDocumentFile の処理を参考に
    //CDocument* pDoc = m_psSketchTemplate->OpenDocumentFile(NULL);
    //-----------------------------------------

    //ドキュメントを生成
    CDocument* pDocument = m_psEditorTemplate->CreateNewDocument();
    if (pDocument == NULL)
    {
        STD_DBG(_T("CDocTemplate::CreateNewDocument returned NULL."));
        AfxMessageBox(AFX_IDP_FAILED_TO_CREATE_DOC);
        return;
    }

    ASSERT_VALID(pDocument);

    //フレームの生成
    BOOL bAutoDelete = pDocument->m_bAutoDelete;
    pDocument->m_bAutoDelete = FALSE;   // don't destroy if something goes wrong

    CFrameWnd* pFrame = m_psEditorTemplate->CreateNewFrame(pDocument, NULL);
    pDocument->m_bAutoDelete = bAutoDelete;
    if (pFrame == NULL)
    {
        STD_DBG(_T("CDocTemplate::CreateNewFrame returned NULL."));
        AfxMessageBox(AFX_IDP_FAILED_TO_CREATE_DOC);
        delete pDocument;       // explicit delete on error
        return;
    }
    ASSERT_VALID(pFrame);

    pDocument->m_bEmbedded = TRUE;

    if (!pDocument->OnNewDocument())
    {
        // user has be alerted to what failed in OnNewDocument
        TRACE(traceAppMsg, 0, "CDocument::OnNewDocument returned FALSE.\n");
        pFrame->DestroyWindow();
        return;
    }

    // OpenDocumentFileと動作をあわせるため
    // フレームが作成されてからドキュメントに pPartsCtrlを設定する
    CMockSketchDoc* pSketchDoc = dynamic_cast<CMockSketchDoc*>(pDocument);

    //!< スクリプトを取得
    CScriptData* pScriptData = pObjDef->GetScriptInstance(strScriptName);

    if (bReload)
    {
        pScriptData->Reload();
    }

    DB_PRINT(_T("OpenNewEditor %s %I64x\n"), strScriptName, pScriptData);

    STD_ASSERT(pScriptData != NULL);

    pSketchDoc->SetObjectDef(uuidParts, pScriptData);

    int iType = pObjDef->GetObjectType();

    if (iType != -1)
    {
        m_psSketchTemplate[iType]->
            InitialUpdateFrame(pFrame, pDocument, TRUE);
    }
        //CFrameWnd::InitialUpdateFrameが内部で呼ばれる
        //   ドキュメントに設定しているタイトルが表示される
        //    CFrameWnd::UpdateFrameTitleForDocument(pDocument->GetTitle());
}

/**
 * @brief   エディタを開く
 * @param       なし 	  
 * @return		なし
 * @note	 エディタのタブ名称は(スクリプト名(パーツ))
 */
void CMockSketchApp::OpenNewEditor(LPCTSTR strTabName)
{
	
    //まずはViewの中から検索(Viewがある場合)
    CString strTitle;
    foreach(CWnd* pWmd,  m_lstEditor)
    {
        CEditorView* pView = dynamic_cast<CEditorView*>(pWmd);
        if (pView == NULL)
        {
            STD_DBG(_T("Select tab error %s"), strTabName);
            continue;
        }

        strTitle = pView->GetDocument()->GetTitle();
        strTitle = CMockSketchDoc::SplitTitleMarker(strTitle);

        if (strTitle == strTabName)
        {
            //MDIタブを選択
            pView->GetParentFrame()->ActivateFrame();
            return ;
        }
    }

    //ドキュメントを生成
    CDocument* pDocument = m_psEditorTemplate->CreateNewDocument();
    if (pDocument == NULL)
    {
        STD_DBG(_T("CDocTemplate::CreateNewDocument returned NULL."));
        AfxMessageBox(AFX_IDP_FAILED_TO_CREATE_DOC);
        return;
    }

    ASSERT_VALID(pDocument);

    //フレームの生成
    BOOL bAutoDelete = pDocument->m_bAutoDelete;
    pDocument->m_bAutoDelete = FALSE;   // don't destroy if something goes wrong

    CFrameWnd* pFrame = m_psEditorTemplate->CreateNewFrame(pDocument, NULL);
    pDocument->m_bAutoDelete = bAutoDelete;
    if (pFrame == NULL)
    {
        STD_DBG(_T("CDocTemplate::CreateNewFrame returned NULL."));
        AfxMessageBox(AFX_IDP_FAILED_TO_CREATE_DOC);
        delete pDocument;       // explicit delete on error
        return;
    }
    ASSERT_VALID(pFrame);

    pDocument->m_bEmbedded = TRUE;

    if (!pDocument->OnNewDocument())
    {
        // user has be alerted to what failed in OnNewDocument
        TRACE(traceAppMsg, 0, "CDocument::OnNewDocument returned FALSE.\n");
        pFrame->DestroyWindow();
        return;
    }

    // OpenDocumentFileと動作をあわせるため
    // フレームが作成されてからドキュメントに pPartsCtrlを設定する
    CMockSketchDoc* pSketchDoc = dynamic_cast<CMockSketchDoc*>(pDocument);


    CMainFrame* pMainFrm =  dynamic_cast<CMainFrame*>(m_pMainWnd);

    StdString strScriptName;
    StdString strObjectName;
    StdString strTabObjectName;

    bool bRet;
    std::vector<StdString> lstName;
    bRet = CObjectDef::DivideScriptTabName(&strScriptName, 
                                           &strTabObjectName, 
                                           strTabName);

    if (!bRet)
    {
        STD_DBG(_T("DivideScriptTabName Fail"));
    }

    std::shared_ptr<CObjectDef> pDef;
    pDef = GetObjectDefByTabName(strTabObjectName).lock();
    if (!pDef)
    {
        STD_DBG(_T("GetParts %s"),strObjectName.c_str());
        return;
    }

    CScriptData* pScriptData = pDef->GetScriptInstance(strScriptName);
    pSketchDoc->SetObjectDef(pDef->GetPartsId(), pScriptData);
    int iType = pDef->GetObjectType();

    if (iType != -1)
    {
        m_psSketchTemplate[iType]->
            InitialUpdateFrame(pFrame, pDocument, TRUE);
    }
        //CFrameWnd::InitialUpdateFrameが内部で呼ばれる
        //   ドキュメントに設定しているタイトルが表示される
        //    CFrameWnd::UpdateFrameTitleForDocument(pDocument->GetTitle());
}

/**
 * @brief   全ウインドウ破棄
 * @param   なし
 * @return  なし
 * @note    すべてのウインドを閉じる
 *           CMainFrame::CloseAllから   
 */
void CMockSketchApp::CloseAllView()
{
    CWnd* pWnd;
    CWnd* pParentWnd;
    std::set<CObjectDefView*>::iterator iteView;
    std::set<CWnd*>::iterator ite;

    std::set<CObjectDefView*> lstView;
    std::set<CWnd*> lstEditor;

    // ウインドウの破棄に伴い自動的にリストが消去されるため
    // あらかじめリストをコピーしておく
    lstView   = m_lstView;
    lstEditor = m_lstEditor;

    for ( iteView  = lstView.begin();
          iteView != lstView.end();
          iteView++)
    {
        pWnd = (*iteView)->GetWindow();;
        if (pWnd == NULL)
        {
            STD_DBG(_T("m_lstView"));
            continue;
        }
        pParentWnd = pWnd->GetParent();
        if (pParentWnd == NULL)
        {
            STD_DBG(_T("m_lstView Parent"));
            continue;
        }
        pParentWnd->SendMessage(WM_CLOSE);
    }


    for ( ite  = lstEditor.begin();
          ite != lstEditor.end();
          ite++)
    {
        pWnd = *ite;
        if (pWnd == NULL)
        {
            STD_DBG(_T("m_lstEditor"));
            continue;
        }
        pParentWnd = pWnd->GetParent();
        if (pParentWnd == NULL)
        {
            STD_DBG(_T("m_lstView Parent"));
            continue;
        }
        pParentWnd->SendMessage(WM_CLOSE);
    }
}

/**
 * @brief   全ての実行ビューを閉じる
 * @param   なし
 * @return  なし
 * @note    すべてのウインドを閉じる
 *           CMainFrame::CloseAllから   
 */
void CMockSketchApp::CloseAllExecView()
{
    CWnd* pWnd;
    CWnd* pParentWnd;
    std::set<CObjectDefView*>::iterator ite;

    std::set<CObjectDefView*> lstView;

    // ウインドウの破棄に伴い自動的にリストが消去されるため
    // あらかじめリストをコピーしておく
    lstView   = m_lstView;

    for ( ite  = lstView.begin();
          ite != lstView.end();
          ite++)
    {
        pWnd = (*ite)->GetWindow();
        if (pWnd == NULL)
        {
            STD_DBG(_T("m_lstView"));
            continue;
        }

        pParentWnd = pWnd->GetParent();
        if (pParentWnd == NULL)
        {
            STD_DBG(_T("m_lstView Parent"));
            continue;
        }

        CMockSketchView* pSketchView;
        pSketchView = dynamic_cast<CMockSketchView*>(pWnd);
        if (pSketchView == NULL)
        {
            continue;
        }

        if (pSketchView->IsDrawiingScript())
        {
            pParentWnd->SendMessage(WM_CLOSE);
        }
    }
}

/**
 * @brief   ビュー追加
 * @param   [in] pWnd  登録ビューウインドウ
 * @return  なし
 * @note    Viewを管理するメソッドがフレームワークに無いため
 */
void CMockSketchApp::AddView(CObjectDefView* pView)
{
    m_lstView.insert(pView);
}

/**
 * @brief   ビュー管理情報削除
 * @param   [in] pWnd  削除ビューウインドウ
 * @return  なし
 * @note    
 */
void CMockSketchApp::DelView(CObjectDefView* pView)
{
    std::set<CObjectDefView*>::iterator ite;

    ite = m_lstView.find(pView);
    if (ite != m_lstView.end())
    {
        m_lstView.erase(ite);
    }

    if ((m_lstView.size() == 0) &&
        (m_lstEditor.size() == 0)) 
    {
        SetAvtiveTabNmae(_T(""));
        GetMainFrame()->SetActiveView(NULL, FALSE);
    }
}


/**
 * @brief   エディタ管理情報追加
 * @param   [in] pWnd  登録エディタウインドウ
 * @return  なし
 * @note    
 */
void CMockSketchApp::AddEditor(CWnd* pWnd)
{
    m_lstEditor.insert(pWnd);
}

/**
 * @brief   エディタ管理情報削除
 * @param   [in] pWnd  削除エディタウインドウ
 * @return  なし
 * @note    
 */
void CMockSketchApp::DelEditor(CWnd* pWnd)
{
    std::set<CWnd*>::iterator ite;

    ite = m_lstEditor.find(pWnd);
    if (ite != m_lstEditor.end())
    {
        m_lstEditor.erase(ite);
    }

    if ((m_lstView.size() == 0) &&
        (m_lstEditor.size() == 0)) 
    {
        SetAvtiveTabNmae(_T(""));
        GetMainFrame()->SetActiveView(NULL, FALSE);
    }
}

/**
 * @brief   ビュー取得
 * @param   [in] strName タブ名
 * @return  ビューへのポインタ
 * @note    
 */
CWnd* CMockSketchApp::GetView(StdString strName) const
{
    CWnd* pWnd;
    CView* pView;
    CString strTitle;
    foreach(CObjectDefView* pDefView, m_lstView)
    {
        pWnd = pDefView->GetWindow();
        pView = reinterpret_cast<CView*>(pWnd);

        strTitle = pView->GetDocument()->GetTitle();
        strTitle = CMockSketchDoc::SplitTitleMarker(strTitle);

        if (strTitle == strName.c_str())
        {
            return pWnd;
        }
    }
    return NULL;
}


/**
 * @brief   ビュー取得
 * @param   [in] strName タブ名
 * @return  ビューへのポインタ
 * @note    
 */
CWnd* CMockSketchApp::GetView(boost::uuids::uuid uuidParts) const
{
    CMockSketchDoc* pSketchDoc;
    CWnd* pWnd;
    CView* pView;
    foreach(CObjectDefView* pDefView, m_lstView)
    {
        pWnd = pDefView->GetWindow();
        pView = reinterpret_cast<CView*>(pWnd);
        pSketchDoc = reinterpret_cast<CMockSketchDoc*>(pView->GetDocument());

        if (pSketchDoc->GetPartsId() == uuidParts)
        {
            return pWnd;
        }
    }
    return NULL;
}

/**
 * @brief   ビューリスト取得
 * @param   [out] pLstView ビューリスト
 * @return  なし
 * @note    
 */
std::set<CObjectDefView*>* CMockSketchApp::GetViewList()
{
    return &m_lstView;
}

/**
 * @brief   エディタービュー取得
 * @param   [in] strName タブ名
 * @return  エディタービューへのポインタ
 * @note    
 */
CWnd* CMockSketchApp::GetEditor(StdString strName) const
{
    CString strTitle;
    foreach(CWnd* pWnd, m_lstEditor)
    {
        CEditorView* pView = reinterpret_cast<CEditorView*>(pWnd);

        strTitle = pView->GetDocument()->GetTitle();
        strTitle = CMockSketchDoc::SplitTitleMarker(strTitle);

        if (strTitle == strName.c_str())
        {
            return pWnd;
        }
    }
    return NULL;
}

/**
 * @brief   エディターリスト取得
 * @param   [out] pLstView エディターリスト
 * @return  なし
 * @note    
 */
void CMockSketchApp::GetEditorList(std::set<CWnd*>* pLstEditor)
{
    *pLstEditor = m_lstEditor;
}


/**
 * @brief   MDIタブ変更(View)
 * @param   [in] strTabName タブ名
 * @return  なし
 * @note    
 */
void CMockSketchApp::SelectViewTab(LPCTSTR strTabName)
{
    std::shared_ptr<CObjectDef> pDef;
    pDef = GetObjectDefByTabName(strTabName).lock();
    if (!pDef)
    {
        STD_DBG(_T("GetParts %s"),strTabName);
        return;
    }

    //新たにビューを作成して開く
    //（既に存在する場合はそれが開かれる)
DB_PRINT(_T("SelectViewTab %s\n"), strTabName);
    OpenNewView(pDef->GetPartsId());
}

/**
 * @brief   エディタを表示する
 * @param   [in] pCtrl 
 * @param   [in] pStr  
 * @return  なし
 * @note    pStrはdeleteされるので注意
 * @note    スレッドからは SelectEditorTabを呼び出せないため
 * @note    このメソッドを作成
 */
void CMockSketchApp::OpenNewEditorFromThread(boost::uuids::uuid uuidParts, StdString* pStr)
{
    ::SendMessage(m_pMainWnd->m_hWnd, WM_SHOW_EDITOR, (WPARAM)&uuidParts,
                                                      (LPARAM)pStr);
}


/**
 * @brief   実行中のView更新
 * @param   なし 
 * @return  なし
 * @note    UpdateExecView
 */
void CMockSketchApp::UpdateExecViewFromThread()
{
    ::PostMessage(m_pMainWnd->m_hWnd, WM_UPDATE_EXEC_VIWE, 0,0);
}

/**
 * @brief   実行中のペイン更新
 * @param   なし 
 * @return  なし
 * @note    UpdateExecPane
 */
void CMockSketchApp::UpdateExecPaneFromThread()
{
    ::PostMessage(m_pMainWnd->m_hWnd, WM_UPDATE_EXEC_PANE, 0,0);
}

/**
 * @brief   実行中のペイン更新
 * @param   なし 
 * @return  なし
 * @note 
 */
void CMockSketchApp::CloseAllExecViewFromThread()
{
    ::PostMessage(m_pMainWnd->m_hWnd, WM_CLOSE_ALL_EXEC_VIEW, 0,0);
}

/**
 * @brief   エディタを表示する
 * @param   [in] strTabName 
 * @return  なし
 * @note    スレッドからは SelectEditorTabを呼び出せないため
 * @note    このメソッドを作成
 */
void CMockSketchApp::OpenNewEditorFromThread(LPCTSTR strTabName)
{

    using namespace boost;

    bool bRet;
    StdString strScript;
    StdString strParts;
    StdString strTab = strTabName;

    bRet = CObjectDef::DivideScriptTabName(&strScript, 
                                           &strParts, 
                                           strTab);

    if (!bRet)
    {
        return;
    }

    uuids::uuid  uuidParts;
    uuidParts = GetPartsIdByTabName(strParts);

    if (uuidParts == boost::uuids::nil_uuid())
    {
        STD_DBG(_T("GetParts %s"),strParts.c_str());
        return;
    }

    if ( GetAvtiveTabNmae() != strTab)
    {
        //エディターを表示する
        StdString* pStrScript = new  StdString;
        *pStrScript = strScript;

        //pStrScriptは呼び出し先で削除する
        OpenNewEditorFromThread(uuidParts, pStrScript);
    }
}


/**
 * @brief   DrawingViewを表示する
 * @param   [in] pCtrl 
 * @return  なし
 * @note    スレッドからは SelectViewTabを呼び出せないため
 * @note    このメソッドを作成
 */
void CMockSketchApp::ShowDrawingView(boost::uuids::uuid uuidParts)
{
    ::SendMessage(m_pMainWnd->m_hWnd, WM_SHOW_DRAWING, (WPARAM)&uuidParts, 0);
}


/**
 * @brief   MDIタブ変更(Editor)
 * @param   [in] strPartsName パーツ名
 * @param   [in] strScript    スクリプト名
 * @param   [in] bReload      ファイル再読み込み
 * @return  なし
 * @note    
 */
void CMockSketchApp::SelectEditorTab(LPCTSTR strPartsName, LPCTSTR strScript, bool bReload)
{
    boost::uuids::uuid uuidParts;
    uuidParts = GetPartsIdByTabName(strPartsName);

    if (uuidParts == boost::uuids::nil_uuid())
    {
        STD_DBG(_T("Select tab error %s"), strScript);
        return;
    }

    //新たにエディタを作成して開く
    //（既に存在する場合はそれが開かれる)
    OpenNewEditor(uuidParts, strScript, bReload);
}

/**
 *  @brief  全テキスト保存
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CMockSketchApp::SaveAllTextToScript()
{
    CEditorView* pEditor;

    foreach(CWnd* pWnd, m_lstEditor)
    {
        pEditor = reinterpret_cast<CEditorView*>(pWnd);
        pEditor->SaveBufferToDocument();
    }
}

/**
 *  @brief  アクティブTab名設定
 *  @param  [in]  strTabName 設定タブ名称
 *  @retval なし
 *  @note   CEditView, CMocksketchViewの OnActivateView以外から
 *  @note   呼び出さないこと
 */
void CMockSketchApp::SetAvtiveTabNmae(StdString strTabName)
{
    m_strActiveTab = strTabName;
}

/**
 *  @brief  アクティブTab名取得
 *  @param  なし
 *  @retval なし
 *  @note   
 */
const StdString& CMockSketchApp::GetAvtiveTabNmae() const
{
    return m_strActiveTab;
}


/**
 *  @brief  定義オブジェクト取得
 *  @param  [in] uuidObject  オブジェクトID
 *  @retval 定義オブジェクト
 *  @note   
 */
std::weak_ptr<CObjectDef> CMockSketchApp::GetObjectDefByUuid(boost::uuids::uuid uuidObject) const
{
    std::weak_ptr<CObjectDef> pRet;
    std::shared_ptr<CProjectCtrl> pProject;

    pProject = GetProject(VIEW_COMMON::E_PROJ_MAIN).lock();
    if (pProject)
    {
        pRet = pProject->GetParts(uuidObject);
    }

    if (pRet.expired())
    {
       std::shared_ptr<CProjectCtrl> pLib;
       pLib = GetProject(VIEW_COMMON::E_PROJ_LIB).lock();
       if (pLib)
       {
           pRet = pLib->GetParts(uuidObject);
       }
    }

    return pRet;
}

/**
 *  @brief  表示中オブジェクト定義取得
 *  @param  なし
 *  @retval オブジェクト定義
 *  @note   
 */
std::weak_ptr<CObjectDef> CMockSketchApp::GetCurrentObjectDef() const
{
    std::weak_ptr<CObjectDef> pRet;
    StdString strTabName;
    strTabName = GetAvtiveTabNmae();

    CWnd*   pWnd;
    CView*  pView;
    CDocument* pDoc;

    pWnd    = GetView(strTabName);
    pView   = dynamic_cast<CView*>(pWnd);

    if (!pView)
    {
        pWnd    = GetEditor(strTabName);
        pView   = dynamic_cast<CView*>(pWnd);
    }

    if (!pView)
    {
        return pRet;
    }

    CMockSketchDoc* pMockSketch;
    pDoc = pView->GetDocument();
    pMockSketch = dynamic_cast<CMockSketchDoc*>(pDoc);

    if (!pMockSketch)
    {
        return pRet;
    }

    pRet = pMockSketch->GetObjectDef();

    return pRet;
}


/**
 *  @brief  部品変更カウント取得
 *  @param  [in] uuidParts 部品ID
 *  @retval 変更カウント
 *  @note   
 */
int CMockSketchApp::GetPartsChangeCount(boost::uuids::uuid uuidParts) const
{
    int iRet = -1;
    std::shared_ptr<CProjectCtrl> pProject;
    pProject = GetProject(VIEW_COMMON::E_PROJ_MAIN).lock();

    if (pProject)
    {
        iRet = pProject->GetPartsChangeCount(uuidParts);
    }

    if (iRet == -1)
    {

       std::shared_ptr<CProjectCtrl> pLib;
       pLib = GetProject(VIEW_COMMON::E_PROJ_MAIN).lock();
       if (pLib)
       {
           iRet = pLib->GetPartsChangeCount(uuidParts);
       }
    }
    return iRet;
}

/**
 *  @brief  定義オブジェクト所属種別取得
 *  @param  [out] pObjName  オブジェクト名称
 *  @param  [in]  strName   オブジェクトID
 *  @retval 定義オブジェクト所属プロジェクト
 *  @note   ライブラリーは、LIB:オブジェクト名
 */
VIEW_COMMON::E_PROJ_TYPE CMockSketchApp::GetObjectDefOwner
                                            (StdString* pObjName, 
                                            StdString strName) const
{
    using namespace VIEW_COMMON;

    StdString    strRet;
    std::vector<StdString> lstName;
    E_PROJ_TYPE eRet = E_PROJ_UNKNOWN;

    CUtil::TokenizeCsv(&lstName, strName, _T(':'));

    if (lstName.size() == 1)
    {
        strRet = lstName[0];
        eRet   = E_PROJ_MAIN;
    }
    else  if (lstName.size() == 2)
    {
        if (lstName[0] == _T("LIB"))
        {
            strRet = lstName[1];
            eRet   = E_PROJ_LIB;
        }
    }

    if (pObjName)
    {
        *pObjName = strRet;
    }
    return eRet;
}


/**
 *  @brief  定義オブジェクト取得
 *  @param  [in] strName  オブジェクトID
 *  @retval 定義オブジェクト
 *  @note   
 */
std::weak_ptr<CObjectDef> CMockSketchApp::GetObjectDefByTabName(StdString strTabName) const
{
    using namespace VIEW_COMMON;
 
    std::weak_ptr<CObjectDef>   pRet;

    StdString       strObjName;
    E_PROJ_TYPE eType = GetObjectDefOwner(&strObjName, strTabName);
    std::shared_ptr<CProjectCtrl> pProject;
    pProject = GetProject(eType).lock();
    if (pProject)
    {
        pRet = pProject->GetPartsByName(strObjName);
    }
    return pRet;
}


/**
 *  @brief  部品定義ID取得
 *  @param  [in] strName  オブジェクト名
 *  @retval 部品定義ID
 *  @note   
 */
boost::uuids::uuid CMockSketchApp::GetPartsIdByTabName(StdString strTabName) const
{
    using namespace VIEW_COMMON;
 
    boost::uuids::uuid   uuidRet;
    StdString       strObjName;
    E_PROJ_TYPE eType = GetObjectDefOwner(&strObjName, strTabName);

    std::shared_ptr<CProjectCtrl> pProject;
    pProject = GetProject(eType).lock();
    if (pProject)
    {
        uuidRet = pProject->GetPartsIdByName(strObjName);
    }
    else
    {
        uuidRet = boost::uuids::nil_uuid();
    }
    return uuidRet;
}


/**
 *  @brief  定義オブジェクトタブ名取得
 *  @param  [in] uuidObject  オブジェクトID
 *  @param  [in] strScript   スクリプト名
 *  @retval 定義オブジェクト名
 *  @note   
 */
StdString CMockSketchApp::GetObjectDefTabName(boost::uuids::uuid uuidObject, 
                                           StdString strScript) const
{
    std::shared_ptr<CObjectDef> pDef;
    StdString strRet;

    std::shared_ptr<CProjectCtrl> pProject;
    pProject = GetProject(VIEW_COMMON::E_PROJ_MAIN).lock();
    if (pProject)
    {
        pDef = pProject->GetParts(uuidObject).lock();
    }

    if (!pDef)
    {
        std::shared_ptr<CProjectCtrl> pLib;
        pLib = GetProject(VIEW_COMMON::E_PROJ_LIB).lock();
       if (pLib)
       {
           pDef = pLib->GetParts(uuidObject).lock();
           strRet = VIEW_COMMON::GetProjectPrifx(VIEW_COMMON::E_PROJ_LIB);
       }
    }

    if (pDef)
    {
        strRet += pDef->GetName();
    }

    if (strScript == _T(""))
    {
        return strRet;
    }

    strScript += _T("(");
    strScript += strRet;
    strScript += _T(")");

    return strScript;

}

/**
 *  @brief  実行インスタンス名
 *  @param  [in] pScriptBase    インスタンス
 *  @retval 実行インスタンス名
 *  @note   
 */
StdString CMockSketchApp::GetScriptBaseName(CDrawingScriptBase* pScriptBase) const
{
    std::shared_ptr<CObjectDef> pObjDef;
    pObjDef = pScriptBase->GetObjectDef().lock();
    StdString strName;
    strName = pScriptBase->GetName();
    if (pObjDef)
    {
        strName += _T("(");
        strName += GetObjectDefTabName(pObjDef->GetPartsId());
        strName += _T(")");
    }
    return strName;
}

/**
 *  @brief  定義オブジェクト名称取得
 *  @param  [in] pDef        オブジェクト定義
 *  @param  [in] strScript   スクリプト名
 *  @retval 定義オブジェクト名
 *  @note   
 */
StdString CMockSketchApp::GetObjectDefTabName(CObjectDef* pDef, 
                           StdString strScript) const
{
    return GetObjectDefTabName(pDef->GetPartsId(), strScript);
}

/**
 * @brief   オブジェクト問い合わせ
 * @param   [in] pQuery 問い合わせデータ
 * @param   なし
 * @retval  オブジェクト
 * @note	
 */
CDrawingObject* CMockSketchApp::QueryObject(const QUERY_REF_OBJECT* pQuery) const
{
    CDrawingObject* pRet = NULL;

    std::shared_ptr<CProjectCtrl> pProject;
    pProject = GetProject(VIEW_COMMON::E_PROJ_MAIN).lock();

    if (pProject)
    {
        pRet = pProject->QueryObject(pQuery);
    }

    if (!pRet)
    {
       std::shared_ptr<CProjectCtrl> pLib;
       pLib = GetProject(VIEW_COMMON::E_PROJ_LIB).lock();
       if (pLib)
       {
           pRet = pLib->QueryObject(pQuery);
       }
    }
    return pRet;
}

CPartsDef* CMockSketchApp::QueryDef(const QUERY_REF_OBJECT* pQuery) const
{
	CPartsDef* pRet = NULL;

	std::shared_ptr<CProjectCtrl> pProject;
	pProject = GetProject(VIEW_COMMON::E_PROJ_MAIN).lock();

	if (pProject)
	{
		pRet = pProject->QueryDef(pQuery);
	}

	if (!pRet)
	{
		std::shared_ptr<CProjectCtrl> pLib;
		pLib = GetProject(VIEW_COMMON::E_PROJ_LIB).lock();
		if (pLib)
		{
			pRet = pLib->QueryDef(pQuery);
		}
	}
	return pRet;
}

/**
 * @brief   オブジェクト移動
 * @param   [in] uuidParts   移動データ部品ID
 * @param   [in] eDst        移動先プロジェクト種別
 * @param   [in] iInsertPos  挿入位置TreeId
 * @param   [in] hCutWnd     移動元ウインドウ
 * @param   なし
 * @retval  
 * @note	
 */
bool CMockSketchApp::MoveObject(boost::uuids::uuid uuidParts, 
                VIEW_COMMON::E_PROJ_TYPE eDst, int iTreeId, 
                HWND hCutWnd)
{
    using namespace VIEW_COMMON;
    using namespace boost::assign;

    //実行中は操作不可

    DBG_ASSERT(GET_APP_MODE() == EXEC_COMMON::EX_EDIT);
    if (GET_APP_MODE() != EXEC_COMMON::EX_EDIT)
    {
        return false;
    }

    E_PROJ_TYPE eSrc = E_PROJ_UNKNOWN;
    std::vector<E_PROJ_TYPE> lstProjType;
   
    std::shared_ptr<CObjectDef> pDef;
    
    lstProjType += E_PROJ_MAIN, E_PROJ_LIB;

    std::shared_ptr<CProjectCtrl> pProjSrc;
    foreach(E_PROJ_TYPE eProj, lstProjType)
    {
        pProjSrc = GetProject(eProj).lock();
        STD_ASSERT(pProjSrc);
        pDef = pProjSrc->GetParts(uuidParts).lock();

        if (pDef)
        {
            eSrc = eProj;
            break;
        }
    }
    DBG_ASSERT(eSrc != E_PROJ_UNKNOWN);
    if(eSrc == E_PROJ_UNKNOWN)
    {
        return false;
    }

    bool bRet;
    if (eSrc == eDst)
    {
        // 同一プロジェクト内の移動
        bRet = pProjSrc->MoveParts(pDef, iTreeId);
        return bRet;
    }

    //移動元ディレクトリを保存

    StdString strMoveSrcDef;
    strMoveSrcDef = pProjSrc->GetDir() +_T("\\") + pDef->GetName();


    std::shared_ptr<CProjectCtrl> pProjDst;
    pProjDst = GetProject(eDst).lock();

    DBG_ASSERT(pProjDst);
    if (!pProjDst)
    {
        return false;
    }

    //-------------------------------
    //変更がある場合は一旦保存する
    //-------------------------------

    if (pProjSrc->IsChange())
    {
        if(pProjSrc->InquireSave())
        {
            //TODO:必要なオブジェクトのみ保存するようにする
            pProjSrc->SaveAllObject();
        }
        else
        {
            return false;
        }
    }

    if (pProjDst->IsChange())
    {
        if(pProjDst->InquireSave())
        {
            //TODO:必要なオブジェクトのみ保存するようにする
            pProjDst->SaveAllObject();
        }
        else
        {
            return false;
        }
    }
    //-------------------------------
    CloseWindow(uuidParts);

    //-------------------------------
    // 同一名称のチェック
    //-------------------------------
    StdString strObject;
    strObject = pDef->GetName();
    if (pProjDst->IsDupPartsName(strObject))
    {
        std::vector<StdString> lstName;
        pProjDst->GetPartsNameList(&lstName);
        strObject = CUtil::FindNumberingString( &lstName, strObject, 4);
        pDef->SetName(strObject);
    }
    //-------------------------------

    if (!pProjDst->AddParts(pDef, iTreeId))
    {
        STD_DBG(_T("MoveParts Feil"));
        return false;
    }

    if(!pProjSrc->DeleteParts(uuidParts))
    {
        STD_DBG(_T("Delete Fail 1"));
        return false;
    }

    //ファイルの保存
    //TODO:必要なオブジェクトのみ保存するようにする
    pProjSrc->SaveAllObject();
    pProjDst->SaveAllObject();


    //移動元のファイルを削除
    boost::system::error_code ec;
    boost::filesystem::remove_all(strMoveSrcDef, ec);

    if (hCutWnd)
    {
        ::SendMessage(hCutWnd, WM_USER_PASTE_END, 
                            (WPARAM)0, (LPARAM)0);
    }
    return true;
}

/**
 * @brief   全実行行選択解除
 * @param   なし
 * @param   なし
 * @retval  
 * @note	
 */
void CMockSketchApp::ClearAllLineindicator()
{
    StdString strTab;
    CEditorView* pEditor;

    foreach(CURRENT_BREAK_CONTEXT* pBreakContext, m_lstBreakContext)
    {
        if (pBreakContext == NULL)
        {
            continue;
        }
        strTab = CUtil::CharToString(pBreakContext->strSection.c_str());
        pEditor = dynamic_cast<CEditorView*>(GetEditor(strTab));
        if (pEditor)
        {
            pEditor->ClearExecLine();
        }
        delete pBreakContext;
    }
    m_lstBreakContext.clear();
    if (m_pMainWnd)
    {
        SendMessage(m_pMainWnd->m_hWnd, WM_BREAK_END, 0, 0);
    }
}

#define GET_FIRST(x) &boost::lambda::_1 ->* &x


/**
 * @brief   実行行表示
 * @param   [in] pBreakContext  
 * @retval  
 * @note	
 */
bool CMockSketchApp::ShowLineindicator(CURRENT_BREAK_CONTEXT* pBreakContext)
{
    CEditorView* pEditor;
    StdString strTab;

    strTab = CUtil::CharToString(pBreakContext->strSection.c_str());

    OpenNewEditorFromThread(strTab.c_str());

    pEditor = dynamic_cast<CEditorView*>(GetEditor(strTab));

    if (pEditor)
    {
        pEditor->SetExecLine(pBreakContext->iLineNo);
    }

    return true;
}

/**
 * @brief   指定ウインドウを閉じる
 * @param   [in] uuidParts  
 * @retval  
 * @note	
 */
void CMockSketchApp::CloseWindow(boost::uuids::uuid uuidParts)
{
    CWnd* pWnd;
    pWnd = GetView(uuidParts);;

    if (pWnd)
    {
        CWnd* pParentWnd;
        pParentWnd = pWnd->GetParent();
        if (pParentWnd)
        {
            pParentWnd->SendMessage(WM_CLOSE);
        }
        else
        {
            STD_DBG(_T("GetParent fail"));
        }
    }

    CMockSketchDoc* pDoc;
    std::vector<CWnd*> lstDel;
    foreach(pWnd, m_lstEditor)
    {
        CEditorView* pView = reinterpret_cast<CEditorView*>(pWnd);

        pDoc = pView->GetDocument();
        if (pDoc->GetPartsId() == uuidParts)
        {
            lstDel.push_back(pWnd);
        }
    }

    foreach(pWnd, lstDel)
    {
        CWnd* pParentWnd;
        pParentWnd = pWnd->GetParent();
        if (pParentWnd)
        {
            pParentWnd->SendMessage(WM_CLOSE);
        }
        else
        {
            STD_DBG(_T("GetParent fail"));
        }
    }
 }


/**
 * @brief   サブウインドウ取得
 * @param   [in] eType  
 * @retval  
 * @note	
 */
CWnd* CMockSketchApp::GetSubWindow(VIEW_COMMON::UI_TYPE eType)
{
    CWnd* pWnd;
    pWnd = reinterpret_cast<CWnd*>(GetMainFrame()->GetUi(eType));
    return pWnd;
}

/**
 * @brief   スクリプト保存
 * @param   [in] uuidParts
 * @param   [in] strScript
 * @retval  なし
 * @note	
 */
bool CMockSketchApp::SaveScript(boost::uuids::uuid uuidParts, StdString strScript)
{
    CString strTitle;
    CScriptData* pScript = NULL;
    bool bEditing = false;

    foreach(CWnd* pWnd, m_lstEditor)
    {
        CEditorView* pEditor = reinterpret_cast<CEditorView*>(pWnd);
        CMockSketchDoc* pDoc = reinterpret_cast<CMockSketchDoc*>(pEditor->GetDocument());

        if (pDoc->GetPartsId() != uuidParts)
        {
            continue;
        }

        pScript = pDoc->GetScript();

        if (pScript->GetName() != strScript)
        {
            continue;
        }

        pEditor->SaveBufferToDocument();
        bEditing = true;
        break;
    }

    if (!bEditing)
    {
        std::shared_ptr<CObjectDef> pDef;
        pDef = GetObjectDefByUuid(uuidParts).lock();
        pScript = pDef->GetScriptInstance(strScript);

    }

    if (pScript)
    {
        bool bRet;
        pScript->SetSingleFileMode();
        bRet = pScript->SaveScript(pScript->GetCurrentPath());
        return bRet;
    }

    return false;
}

std::shared_ptr<CDrawingObject> CMockSketchApp::CreateDefaultObject(DRAWING_TYPE eType)
{
	std::shared_ptr<CDrawingObject> pNew(nullptr);
	auto pProj = GetProject().lock();

	if (pProj)
	{
		auto  pObjShared = pProj->GetDefaultObject(eType);
		if (pObjShared)
		{
			pNew = CDrawingObject::CloneShared(pObjShared.get());
		}
		else
		{
			pNew = CDrawingObject::CreateShared(eType);
		}
	}
	return pNew;
}

const CDrawingObject* CMockSketchApp::GetDefaultObject(DRAWING_TYPE eType) const
{
	auto pProj = GetProject().lock();
	auto  pObj = pProj->GetDefaultObject(eType);

	return pObj.get();
}

bool CMockSketchApp::GetDefaultObjectPriorty(DRAWING_TYPE eType)
{
	auto pProj = GetProject().lock();
	if (pProj)
	{
		return pProj->GetDefaultObjectPriorty(eType);
	}
	return false;
}


bool CMockSketchApp::InitPaperSize()
{
    CDC dc;
    UpdatePrinterSelection(FALSE); 
    if(CreatePrinterDC(dc))
    {
        SetPaperSize(&dc);
        return true;
    }

    // 基準単位(m)に直す
    m_dPrintWidth  =  210.0 /1000.0;
    m_dPrintHeight =  287.0 /1000.0;
    return false;
}


void  CMockSketchApp::SetPaperSize(CDC* pDC)
{

    CSize size;
    // sizeに紙サイズを取得する
    //VERIFY(::Escape(hDc,GETPHYSPAGESIZE, 0, NULL, (LPVOID)&size));

    POINT ptPaper; //印刷デバイス用です。ページの物理的な幅をデバイス単位で表します
    ptPaper.x = pDC->GetDeviceCaps( PHYSICALWIDTH);
    ptPaper.y = pDC->GetDeviceCaps( PHYSICALHEIGHT);

    
    int iTec = pDC->GetDeviceCaps( TECHNOLOGY);
    //DT_PLOTTER          0   /* Vector plotter                   */
    //DT_RASDISPLAY       1   /* Raster display                   */
    //DT_RASPRINTER       2   /* Raster printer                   */
    //DT_RASCAMERA        3   /* Raster camera                    */
    //DT_CHARSTREAM       4   /* Character-stream, PLP            */
    //DT_METAFILE         5   /* Metafile, VDM                    */
    //DT_DISPFILE         6   /* Display-file                     */

    POINT ptSize; //ミリメートル（mm）単位の画面の物理的な幅。
    ptSize.x = pDC->GetDeviceCaps( HORZSIZE);
    ptSize.y = pDC->GetDeviceCaps( VERTSIZE);

    POINT ptPixel; //ピクセル単位の画面の幅。
    ptPixel.x = pDC->GetDeviceCaps( HORZRES);
    ptPixel.y = pDC->GetDeviceCaps( VERTRES);


    //印刷デバイス用です。物理的なページの左端から印刷可能領域の左端までの距離をデバイス単位で表します
    POINT ptOffset;
    ptOffset.x = pDC->GetDeviceCaps( PHYSICALOFFSETX);
    ptOffset.y = pDC->GetDeviceCaps( PHYSICALOFFSETY);

    // 1インチあたりのピクセル数を取得
    m_dPpiX = (double )pDC->GetDeviceCaps(LOGPIXELSX);
    m_dPpiY = (double )pDC->GetDeviceCaps(LOGPIXELSY);
   
    /*
   xOffsetOfRightMargin = xOffset +
                          GetDeviceCaps (hPrnDC, HORZRES) -
                          pt.x -
                          GetDeviceCaps (hPrnDC, LOGPIXELSX) *
                          wInchesWeWant;

   yOffsetOfBottomMargin = yOffset +
                           GetDeviceCaps (hPrnDC, VERTRES) -
                           pt.y -
                           GetDeviceCaps (hPrnDC, LOGPIXELSY) *
                           wInchesWeWant;
    */


    // 基準単位(m)に直す
    m_dPaperWidth =   double(ptPaper.x / m_dPpiX) * 25.4 / 1000.0;
    m_dPaperHeight =  double(ptPaper.y / m_dPpiY) * 25.4 / 1000.0;

    m_dPrintWidth =   double(ptPixel.x / m_dPpiX) * 25.4 / 1000.0;
    m_dPrintHeight =  double(ptPixel.y / m_dPpiY) * 25.4 / 1000.0;

    m_dPrintLeftMargin =  double(ptOffset.x / m_dPpiX) * 25.4 / 1000.0;
    m_dPrintTopMargin  =  double(ptOffset.y / m_dPpiY) * 25.4 / 1000.0;


}


void CMockSketchApp::OnFilePrintSetup()
{
	CPrintDialog pd(TRUE);
	DoPrintDialog(&pd);

    CDC dc;
    if(CreatePrinterDC(dc))
    {
        SetPaperSize(&dc);
    }
    ::SendMessage(m_pMainWnd->m_hWnd, WM_UPDATE_PRINTER, 0, 0);
}
