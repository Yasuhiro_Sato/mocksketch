/**
 * @brief       MockSketchヘッダーファイル
 * @file        MockSketch.h
 * @author      Yasuhiro Sato
 * @date        01-2-2009 10:36:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks   MockSketch アプリケーションのメイン ヘッダー ファイル
 *			
 *
 * $
 * $
 * 
 */

#pragma once

#ifndef __AFXWIN_H__
	#error "PCH に対してこのファイルをインクルードする前に 'stdafx.h' をインクルードしてください"
#endif

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "resource.h"       // メイン シンボル
#include "resource2.h"

#include "View/ViewCommon.h"
#include "View/CURRENT_BREAK_CONTEXT.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#define THIS_APP  ((CMockSketchApp*)AfxGetApp())
#define GET_APP_MODE  ((CMockSketchApp*)AfxGetApp())->GetExecCtrl()->GetExecStatus

/*---------------------------------------------------*/
/*  classes                                          */
/*---------------------------------------------------*/
class CPartsDef;
class CMockSketchDoc;
class CProjectCtrl;
class CScriptEngine;
class CMainFrame;
class CObjectDef;
class CDrawingObject;
class CDrawingScriptBase;
class CObjectDefView;
class CMultiDocReloadableTemplate;
class asIScriptContext;
class CExecCtrl;
enum DRAWING_TYPE;
struct QUERY_REF_OBJECT;

enum DEBUG_FLAG
{
	DF_DISABLE = 0,
	DF_1 = 1,
	DF_2 = 2,
	DF_3 = 3,
	DF_4 = 4,
	DF_5 = 5,
};

/**
 * @class   CMockSketchApp
 * @brief
 */
class CMockSketchApp : public CWinAppEx
{
public:

public:
    CMockSketchApp(BOOL bResourceSmartUpdate = FALSE);
    ~CMockSketchApp();

    // オーバーライド
public:
    virtual BOOL InitInstance();

    // 実装
    UINT  m_nAppLook;
    BOOL  m_bHiColorIcons;
    ULONG_PTR m_gdiplusToken;

    virtual void PreLoadState();
    virtual void LoadCustomState();
    virtual void SaveCustomState();

    //!< 新規作成
    virtual void OnFileNew();

    //!< ファイルを開く
    virtual void OnFileOpen();

    afx_msg void OnAppAbout();


    //!< プロジェクト取得
    std::weak_ptr<CProjectCtrl> GetProject(VIEW_COMMON::E_PROJ_TYPE eType = VIEW_COMMON::E_PROJ_MAIN) const;

    //!< 実行コントロール
    CExecCtrl* GetExecCtrl() const;

    //!< スクリプトエンジン取得
    CScriptEngine* GetScriptEngine();

    //!< 定義オブジェクト所属プロジェクト取得
    VIEW_COMMON::E_PROJ_TYPE CMockSketchApp::GetObjectDefOwner
                                            (StdString* pObjName, 
                                            StdString strName) const;

    //!< 定義オブジェクト取得
    std::weak_ptr<CObjectDef> GetObjectDefByUuid(boost::uuids::uuid uuidObject) const;

    //!< 定義オブジェクト変更カウント取得
    int GetPartsChangeCount(boost::uuids::uuid uuidParts) const;

    //!< 名称による定義オブジェクト取得
    std::weak_ptr<CObjectDef>  GetObjectDefByTabName(StdString strTabName) const;

    //!< 名称による定義オブジェクトID取得
    boost::uuids::uuid  GetPartsIdByTabName(StdString strTabName) const;

    //!< 表示中オブジェクト定義取得
    std::weak_ptr<CObjectDef> CMockSketchApp::GetCurrentObjectDef() const;

    //!< 実行インスタンス名取得
    StdString GetScriptBaseName(CDrawingScriptBase* pScriptBase) const;

    //!< 定義オブジェクトタブ名取得
    StdString GetObjectDefTabName(boost::uuids::uuid uuidObject, 
                                  StdString strScript = _T("")) const;
 
    //!< 定義オブジェクトタブ名取得
    StdString GetObjectDefTabName(CObjectDef* pDef, 
                               StdString strScript = _T("")) const;

    //!< オブジェクト問い合わせ
    CDrawingObject* QueryObject(const QUERY_REF_OBJECT* pQuery) const;
	CPartsDef* QueryDef(const QUERY_REF_OBJECT* pQuery) const;

    //!< オブジェクト移動
    bool MoveObject(boost::uuids::uuid uuidParts, 
                VIEW_COMMON::E_PROJ_TYPE eDst, int iTreeId, 
                HWND hCutWnd = NULL);

    //TODO: VIEW関連を統合する

    
    //!< 全てのビューを閉じる
    void CloseAllView();

    //!< 全ての実行ビューを閉じる
    void CloseAllExecView();

    //!< ビュー追加
    void AddView(CObjectDefView* pView);

    //!< ビュー削除
    void DelView(CObjectDefView* pView);

    //!< ビュー取得
    CWnd* GetView(StdString strName) const;

    //!< ビュー取得
    CWnd* GetView(boost::uuids::uuid uuidParts) const;

    //!< ビューリスト取得
    std::set<CObjectDefView*>* GetViewList();

    //!< エディタ追加
    void AddEditor(CWnd* pWnd);

    //!< エディタ削除
    void DelEditor(CWnd* pWnd);

    //!< エディタ取得
    CWnd* GetEditor(StdString strName) const;

    //!< エディタリスト取得
    void GetEditorList(std::set<CWnd*>* pLstEditor);

    //!< ビューデータ変更
    //void ChangeViewData(CObjectDef* pObjDef);

    //!< 新しいビューを開く
    void OpenNewView(boost::uuids::uuid uuidParts);

    //!< 新しい実行ビューを開く
    void OpenNewExecView(std::weak_ptr<CObjectDef> pExecDef);

    //!< 新しいエディタを開く
    void OpenNewEditor(boost::uuids::uuid uuidParts, LPCTSTR strScriptName, bool bReload = false);

    //!< 新しいエディタを開く
    void OpenNewEditor(LPCTSTR strTabName);

    //!< エディタを表示する
    void OpenNewEditorFromThread(boost::uuids::uuid uuidParts, StdString* pStr);

    //!< エディタを表示する
    void OpenNewEditorFromThread(LPCTSTR strTabName);

    //!< エディタファイルのリロード
    bool ReloadEditorFile(StdPath Path, bool bSimulation);

    //!< DrawingViewを表示する
    void ShowDrawingView(boost::uuids::uuid uuidParts);

    //!< MDIタブ変更(View)
    void SelectViewTab(LPCTSTR strTabName);

    //!< MDIタブ変更(Editor)
    void SelectEditorTab(LPCTSTR strPartsName, LPCTSTR strScript, bool bReload = false);

    //!< プロジェクト設定
    void SetProject(StdPath pathPrj, StdString strName);
    
    //!< メインフレームの有無
    bool IsExistMain();

    //!< メインフレームの取得
    CMainFrame* GetMainFrame();

    //!< メインフレームのハンドル取得
    HWND GetMainFrameHandle();

    //!< 全テキスト保存
    void SaveAllTextToScript();

    //!< アクティブTab名設定
    void SetAvtiveTabNmae(StdString strTabName);

    //!< アクティブTab名取得
    const StdString& GetAvtiveTabNmae() const;

    //!< 全ユーザ共通設定問い合わせ
    bool IsSettingAllUser();

    //!< スクリプト保存
    bool SaveScript(boost::uuids::uuid uuidParts, StdString strScript);

	std::shared_ptr<CDrawingObject> CreateDefaultObject(DRAWING_TYPE eType);
	const CDrawingObject* GetDefaultObject(DRAWING_TYPE eType) const;


	bool GetDefaultObjectPriorty(DRAWING_TYPE eType);

    //--------------------------
    // スレッドからの呼び出し用
    //--------------------------
    void UpdateExecViewFromThread();

    void UpdateExecPaneFromThread();

    void CloseAllExecViewFromThread();

    //------------------------
    // ステップ実行位置表示
    //------------------------
    //!< 全ステップ実行位置クリア
    void ClearAllLineindicator();

    //!< ステップ実行位置クリア
    /*
    void ClearLineindicator(const CScriptContext* pContext, 
                            bool bNextBreakpointSet = true);

    //!< ステップ実行位置表示
    bool SetLineindicator(  CScriptContext* pContext,
                            asIScriptContext *pCtx,
                            bool bNew);
    */

    //------------------------
    // ウインドウ取得
    //------------------------
    CWnd* GetSubWindow(VIEW_COMMON::UI_TYPE eType);
 

    //!< ウインドを閉じる
    void CloseWindow(boost::uuids::uuid uuidParts);

    //------------------------
    // ステップ実行位置表示
    //------------------------
    bool ShowLineindicator(CURRENT_BREAK_CONTEXT* pBreakContext);

    //------------------------
    // 印刷関連
    //------------------------
    bool InitPaperSize();
    void  SetPaperSize(CDC* pDC);

    double GetPaperWidth(){return m_dPaperWidth;}
    double GetPaperHeight(){return m_dPaperHeight;}

    double GetPrintWidth ()  {return m_dPrintWidth;}
    double GetPrintHeight()  {return m_dPrintHeight;}

    double GetPrintLeftMargin()  {return  m_dPrintLeftMargin;}
    double GetPrintTopMargin()   {return  m_dPrintTopMargin;}

    double GetPrintPpiX()  {return  m_dPpiX;}
    double GetPrintPpiY()  {return  m_dPpiY;}

	//------------------------
	// デバッグ用
	//------------------------
	void SetDebugFlag(DEBUG_FLAG df) { m_df = df; }
	DEBUG_FLAG GetDebugFlag() { return m_df; }
	void SetDebugCount(int dc) { m_iDc = dc; }
	int GetDebugCount() { return m_iDc; }
	int IncDebugCount() { return ++m_iDc; }


    DECLARE_MESSAGE_MAP()
    afx_msg void OnFilePrintSetup();

protected:
    virtual int ExitInstance();


protected:
    //!< ドキュメントテンプレート（図形)
    CMultiDocReloadableTemplate* m_psSketchTemplate[4];


    //!< ドキュメントテンプレート（テキスト)
    CMultiDocReloadableTemplate* m_psEditorTemplate;

public:
    //================
    // CFormatBar用
    //================
    CDC m_dcScreen;
	LOGFONT m_lf;
	int m_nDefFont;
	BOOL m_bWin4;
    //================


protected:
    //!< ビューリスト
    std::set<CObjectDefView*> m_lstView;

    //!< エディタリスト
    std::set<CWnd*> m_lstEditor;

    //!< スクリプトエンジン
    CScriptEngine* m_psScriptEngine;

    //!< アクティブタブ名称
    StdString m_strActiveTab;

    //!< 実行中オブジェクト
    CObjectDef*  m_pExecDef;

    std::deque< CURRENT_BREAK_CONTEXT* >   m_lstBreakContext;

    //-----------------------------
    //!< 印刷サイズ（基準単位系(m)）
    double                          m_dPaperWidth = 0.0;
    double                          m_dPaperHeight = 0.0;

    double                          m_dPrintWidth = 0.0;
    double                          m_dPrintHeight = 0.0;

    double                          m_dPrintLeftMargin = 0.0;
    double                          m_dPrintTopMargin = 0.0;

    double                          m_dPpiX = 0.0;
    double                          m_dPpiY = 0.0;
    //-----------------------------

	DEBUG_FLAG	m_df  = DF_DISABLE;
	int         m_iDc = 0;
};

extern CMockSketchApp theApp;
