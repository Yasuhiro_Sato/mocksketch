/**
 * @brief			CMultiDocReloadableTemplate実装ファイル
 * @file			CMultiDocReloadableTemplate.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./MultiDocReloadableTemplate.h"
#include "./SYSTEM/CSystem.h"
/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/**
 * @brief   コンストラクタ
 * @param   なし
 * @retval  なし
 * @note	
 */
CMultiDocReloadableTemplate::CMultiDocReloadableTemplate(UINT nIDResource, CRuntimeClass* pDocClass,
	CRuntimeClass* pFrameClass, CRuntimeClass* pViewClass)
		: CMultiDocTemplate(nIDResource, pDocClass, pFrameClass, pViewClass)
{
}

/**
 * @brief   デストラクタ
 * @param   なし         
 * @retval  なし
 * @note	
 */
CMultiDocReloadableTemplate::~CMultiDocReloadableTemplate()
{

}

/**
 * @brief   ドキュメントテンプレート読み込み
 * @param   なし         
 * @retval  なし
 * @note    メニューを
 */
void CMultiDocReloadableTemplate::LoadTemplate()
{
	//CDocTemplate::LoadTemplate();
    //=====================================================
	if (m_strDocStrings.IsEmpty() && 
        !m_strDocStrings.LoadString(m_nIDResource))
	{
		TRACE(traceAppMsg, 0, "Warning: no document names in string for template #%d.\n",
			m_nIDResource);
	}

	if (m_nIDEmbeddingResource != 0 && m_hMenuEmbedding == NULL)
	{
		// load menu to be used while editing an embedding (as a server)
		HINSTANCE hInst = AfxFindResourceHandle(
			MAKEINTRESOURCE(m_nIDEmbeddingResource), RT_MENU);
		m_hMenuEmbedding =
			::LoadMenu(hInst, MAKEINTRESOURCE(m_nIDEmbeddingResource));
		m_hAccelEmbedding =
			::LoadAccelerators(hInst, MAKEINTRESOURCE(m_nIDEmbeddingResource));
	}
	if (m_nIDServerResource != 0 && m_hMenuInPlaceServer == NULL)
	{
		// load menu to be used while editing in-place (as a server)
		HINSTANCE hInst = AfxFindResourceHandle(
			MAKEINTRESOURCE(m_nIDServerResource), RT_MENU);
		m_hMenuInPlaceServer = ::LoadMenu(hInst,
			MAKEINTRESOURCE(m_nIDServerResource));
		m_hAccelInPlaceServer = ::LoadAccelerators(hInst,
			MAKEINTRESOURCE(m_nIDServerResource));
	}

	if (m_nIDContainerResource != 0 && m_hMenuInPlace == NULL)
	{
		// load menu to be used while in-place editing session (as a container)
		HINSTANCE hInst = AfxFindResourceHandle(
			MAKEINTRESOURCE(m_nIDContainerResource), RT_MENU);
		m_hMenuInPlace = ::LoadMenu(hInst,
			MAKEINTRESOURCE(m_nIDContainerResource));
		m_hAccelInPlace = ::LoadAccelerators(hInst,
			MAKEINTRESOURCE(m_nIDContainerResource));
	}
    //=====================================================

	if (m_nIDResource != 0 && m_hMenuShared == NULL)
	{
		HINSTANCE hInst = AfxFindResourceHandle(
			MAKEINTRESOURCE(m_nIDResource), RT_MENU);
		m_hMenuShared = ::LoadMenu(hInst, MAKEINTRESOURCE(m_nIDResource));
		m_hAccelTable =
			::LoadAccelerators(hInst, MAKEINTRESOURCE(m_nIDResource));
	}

    CUtil::ConvertMenuString(m_hMenuShared);


#ifdef _DEBUG
	// warnings about missing components (don't bother with accelerators)
	if (m_hMenuShared == NULL)
		TRACE(traceAppMsg, 0, "Warning: no shared menu for document template #%d.\n",
			m_nIDResource);
#endif //_DEBUG
}


/**
 * @brief   テンプレート再読み込み
 * @param   なし
 * @retval  なし
 * @note	
 */
void CMultiDocReloadableTemplate::ReloadTemplate()
{
    // Destroy the resources from CDocTemplate
    m_strDocStrings.Empty();

    // Delete OLE resources from CDocTemplate
    if (m_hMenuInPlace != NULL) {
    ::DestroyMenu(m_hMenuInPlace);
    m_hMenuInPlace = NULL;
    } 

    if (m_hAccelInPlace != NULL) {
    ::FreeResource(m_hAccelInPlace);
    m_hAccelInPlace = NULL;
    }

    if (m_hMenuEmbedding != NULL) {
    ::DestroyMenu(m_hMenuEmbedding);
    m_hMenuEmbedding = NULL;
    }

    if (m_hAccelEmbedding != NULL) {
    ::FreeResource(m_hAccelEmbedding);
    m_hAccelEmbedding = NULL;
    }

    if (m_hMenuInPlaceServer != NULL) {
    ::DestroyMenu(m_hMenuInPlaceServer);
    m_hMenuInPlaceServer = NULL;
    }

    if (m_hAccelInPlaceServer != NULL) {
    ::FreeResource(m_hAccelInPlaceServer);
    m_hAccelInPlaceServer = NULL;
    } 

    // Delete shared components from CMultiDocTemplate
    if (m_hMenuShared != NULL) {
    ::DestroyMenu(m_hMenuShared);
    m_hMenuShared = NULL;
    }

    if (m_hAccelTable != NULL) {
    ::FreeResource((HGLOBAL)m_hAccelTable);
    m_hAccelTable = NULL;
    }

    // Now, reload the resources
    LoadTemplate(); 
} 



/**
 * @brief   メニュー項目変更
 * @param   [in] hMenu
 * @retval  なし
 * @note	
 */
/*
void CMultiDocReloadableTemplate::ConvertMenuString(HMENU hMenu)
{
    CMenu menu;
    MENUITEMINFO  info;

    if(!menu.Attach(hMenu))
    {
        return;
    }

    int iMenuCount = menu.GetMenuItemCount();
    for (int iCnt =0; iCnt < iMenuCount; iCnt++)

    {
        memset(&info, 0, sizeof(info));
        info.cbSize = sizeof(info);
        info.fMask = MIIM_TYPE | MIIM_SUBMENU;
        menu.GetMenuItemInfo(iCnt, &info, TRUE);

        StdString strMenu;
        if( info.fType == MFT_STRING)
        {
            CString strMenuItem;
            menu.GetMenuString(iCnt, strMenuItem, MF_BYPOSITION);
            

            strMenu = GET_SYS_STR((LPCTSTR)strMenuItem);
            
            info.dwTypeData = const_cast<StdChar*>(strMenu.c_str());
            info.cch        = strMenu.length();
            menu.SetMenuItemInfo(iCnt, &info, TRUE);
        }

        if (info.hSubMenu != NULL)
        {
            ConvertMenuString(info.hSubMenu);
        }
    }
    menu.Detach();

}
*/

IMPLEMENT_DYNAMIC( CMultiDocReloadableTemplate, CMultiDocTemplate)
