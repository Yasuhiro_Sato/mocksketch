/**
 * @brief			CMultiDocReloadableTemplateヘッダーファイル
 * @file			CMultiDocReloadableTemplate.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

#pragma once

/**
 * @class   CMultiDocReloadableTemplate
 * @brief
 */
class CMultiDocReloadableTemplate : public CMultiDocTemplate
{
	DECLARE_DYNAMIC(CMultiDocReloadableTemplate)

// Constructors
public:
	CMultiDocReloadableTemplate(UINT nIDResource, CRuntimeClass* pDocClass,
		CRuntimeClass* pFrameClass, CRuntimeClass* pViewClass);

public:
    virtual ~CMultiDocReloadableTemplate();

    virtual void LoadTemplate();

    //メニュー項目を変更
    //static void ConvertMenuString(HMENU hMenu);

    void ReloadTemplate();
};

