#include "stdafx.h"
#include "Utility/CUtility.h"
#include "./CExecContextThread.h"
#include "./CExecCtrl.h"
#include "./CScriptObject.h"
#include "./CScriptEngine.h"
#include "./CScriptObject.h"
#include "./CModule.h"
#include "./TCB.h"
#include "DrawingObject/CDrawingScriptBase.h"
#include <angelscript.h>
#include <algorithm>
#include <chrono>
#include <memory>
#include "Mocksketch.h"

#ifdef _DEBUG
#undef new 
#define new DEBUG_NEW
#endif 

CExecContextThread::CExecContextThread(EXEC_COMMON::E_THREAD_TYPE  eThreadType, CExecCtrl *pCtrl):
m_bStop( false),
m_bDebug(true),
m_pExecCtrl(pCtrl),
m_eThreadType(eThreadType),
m_pCurrentTcb(NULL)
{
	memset(&m_lastBreak, 0, sizeof(BreakData));
}


CExecContextThread::~CExecContextThread()
{
    End();
    _DeleteAllQue();
}

bool CExecContextThread::BreakCallback(asIScriptContext *pCtx)
{

	const char *tmp = 0;
    int iCol;
	int lineNbr = pCtx->GetLineNumber(0, &iCol, &tmp);

	std::string strSection = tmp ? tmp : "";

    TCB* pTcb = GetCurrentTcb();

    bool bBreak = false;
    bool bPause = false;

	if( m_action == RESUME )
	{
        bBreak = CheckBreakPoint(pCtx, strSection, lineNbr, iCol);     
	}
	else if( m_action == STEP_OVER )
	{
         //ステップオーバー (関数内部に入らない)
		if( pCtx->GetCallstackSize() > m_lastBreak.iStackLevel )
		{
            bBreak = CheckBreakPoint(pCtx, strSection, lineNbr, iCol);     
		}
        else
        {
            if (pTcb)
            {
                if (pTcb == m_lastBreak.pTcb)
                {
                    bPause = true;
                }
            }
            else
            {
                bPause = true;
            }
        }
	}
	else if( m_action == STEP_OUT )
	{
        //ステップアウト (今実行している関数の外（呼び出し元）に出るまでプログラムを進める)
		if( pCtx->GetCallstackSize() >= m_lastBreak.iStackLevel )
		{
            bBreak = CheckBreakPoint(pCtx, strSection, lineNbr, iCol);     
		}
        else
        {
            if (pTcb)
            {
                if (pTcb == m_lastBreak.pTcb)
                {
                    bPause = true;
                }
            }
            else
            {
                bPause = true;
            }
        }
	}
	else if( m_action == STEP_INTO )
	{
        bBreak = CheckBreakPoint(pCtx, strSection, lineNbr, iCol);
        if (pTcb)
        {
            if (pTcb == m_lastBreak.pTcb)
            {
                bPause = true;
            }
        }
        else
        {
            bPause = true;
        }
	}

    bool bRet = false;
    if (bBreak || bPause)
    {
        if ((m_currentBreakData.iLineNo != lineNbr) ||
            (m_currentBreakData.iColum  != iCol)||
            bBreak)
        {
            /*
                PrepareScriptFunction
                asBC_SUSPEND
                asCContext::Execute
                この３箇所でCallbackが呼び出される

                現状の問題点 ブレークポイントで止まった後
                再開すると、もう一度同じ所で停止する。

            */

            CExecCtrl* pCtrl = THIS_APP->GetExecCtrl();
            pCtrl->NotifyBreakpoint(strSection, lineNbr);
            m_currentBreakData.iLineNo = lineNbr;
            m_currentBreakData.iColum = iCol;
            m_currentBreakData.strSection = strSection;
            m_currentBreakData.pAsContext = pCtx;
            bRet = true;
        }
 
    }

    return bRet;
}

bool CExecContextThread::CheckBreakPoint(asIScriptContext *pCtx, 
                                         const std::string& strFile, 
                                         int lineNbr,
                                         int iCol)
{
	// TODO: Should cache the break points in a function by checking which possible break points
	//       can be hit when entering a function. If there are no break points in the current function
	//       then there is no need to check every line.
	//const char *tmp = 0;
	//int lineNbr = pCtx->GetLineNumber(0, 0, &tmp);


    TCB* pTcb = GetCurrentTcb();

    CScriptObject* pObj = NULL;
    CModule*       pModule = NULL;
    std::vector<CModule::BreakPoint>* pLstBreakPoint;

    if(!pTcb)
    {

        CDrawingScriptBase* pScriptBase;
        pScriptBase = reinterpret_cast<CDrawingScriptBase*>(pCtx->GetUserData());
        if (!pScriptBase)
        {
            return false;
        }
        pObj =  pScriptBase->GetScriptObject();
    }
    else
    {
        pObj = pTcb->GetScriptObject();
    }

    pModule = pObj->GetModule();
    pLstBreakPoint = pModule->GetBreakPoints();
    CModule::BreakPoint* pBreak;

	// 新しい関数内に入ったか？
    asIScriptFunction *func = pCtx->GetFunction();
	
    if( m_lastBreak.pFunc != func )
	{
        // ブレークポイントに調整が必要かチェックする
		for( size_t n = 0; n < pLstBreakPoint->size(); n++ )
		{
            pBreak = &pLstBreakPoint->at(n);

            // 関数に入るときにブレークポイントをチェックする必要がある
			if( pLstBreakPoint->at(n).func )
			{
                //関数名を設定する場合
                // pBreak->name <- 設定時にはファイル名
				if( pBreak->name == func->GetName() )
				{
					// Transform the function breakpoint into a file breakpoint
					pBreak->name           = strFile;
					pBreak->lineNbr        = lineNbr;
                    //pBreak->iColum         = iCol;
					pBreak->func           = false;
					pBreak->needsAdjusting = false;
				}
			}
			// Check if a given breakpoint fall on a line with code or else adjust it to the next line
			else if( pBreak->needsAdjusting &&
					 pBreak->name == strFile )
			{
				int line = func->FindNextLineWithCode(pBreak->lineNbr);
				if( line >= 0 )
				{
					pBreak->needsAdjusting = false;
					if( line != pBreak->lineNbr )
					{
    					// Move the breakpoint to the next line
						pBreak->lineNbr = line;
					}
				}
			}
		}
	}
	m_lastBreak.pFunc = func;

	// Determine if there is a breakpoint at the current line
	for( size_t n = 0; n < pLstBreakPoint->size(); n++ )
	{
		// TODO: do case-less comparison for file name
        pBreak = &pLstBreakPoint->at(n);
		// Should we break?
		if( !pBreak->func &&
			pBreak->lineNbr == lineNbr &&
			pBreak->name == strFile )
		{
            m_lastBreak.pTcb = pTcb;
            return true;
		}
	}
	return false;
}

bool CExecContextThread::Break()
{
    //CExecCtrl::Pause()から呼び出し

    asIScriptContext* pCtx;
    if (m_pCurrentTcb)
    {
        pCtx = m_pCurrentTcb->GetCurrentContext();
        if (pCtx)
        {
            if(pCtx->GetState() == asEXECUTION_ACTIVE)
            {
                pCtx->Suspend();
            }
        }
    }
    m_bBreak = true;
    m_bStepStart = false;

    return true;
}
/**
 *  @brief  例外コールバック
 *  @param  [in]msg   メッセージ情報
 *  @param  [in]param 付帯情報
 *  @retval なし     
 *  @note   
 *          SetExceptionCallbackの第2引数が返る
 */
void CExecContextThread::ExceptionCallback(asIScriptContext *pCtx, void *contextThread)
{
    const char* pSectionName;
    int iLineNo;
    int iCol;

    //asIScriptFunction* pFunc;

    iLineNo = pCtx->GetExceptionLineNumber(&iCol, &pSectionName);
    StdString strException =  CUtil::CharToString(pCtx->GetExceptionString());
    StdString strSection   =  CUtil::CharToString(pSectionName);
    
    StdString strOut;
    strOut = CUtil::StrFormat(_T("Exception %s (line %d:%d) : %s\n"), 
                                    strSection.c_str(), 
                                    iLineNo,
                                    iCol,
                                    strException.c_str());

    PrtintOut(WIN_DEBUG, strOut.c_str());
}

void CExecContextThread::LineCallbackS(asIScriptContext *pCtx, void *contextThread)
{
	if( pCtx->GetState() != asEXECUTION_ACTIVE )
    {
		return;
    }

    if (!contextThread){return;}

    CExecContextThread* pExecScript;
    pExecScript = reinterpret_cast<CExecContextThread*>(contextThread);

    bool bBreak = false;

    if (pExecScript->IsDebugMode())
    {
        bBreak = pExecScript->BreakCallback(pCtx);
    }

    TCB* pTcb = NULL;
    bool bTask = true;
    CModule* pScriptModule = NULL;
    CScriptObject* pScriptObj = NULL;


    if (pCtx == pExecScript->GetNonThreadContext())
    {
        bTask = false;
    }
    else
    {
        pTcb = pExecScript->GetCurrentTcb();
    }

    if (pTcb)
    {
        pScriptObj = pTcb->pScriptObject;
    }
    else
    {
        //TCBなしの場合もある
        CDrawingScriptBase* pScriptBase;
        pScriptBase = reinterpret_cast<CDrawingScriptBase*>(pCtx->GetUserData());
        if (!pScriptBase)
        {
            return;
        }

        pScriptObj =  pScriptBase->GetScriptObject();

        if (!pScriptObj)
        {
            return;
        }
        pScriptModule = pScriptObj->GetModule();
    }


    CExecCtrl* pCtrl = THIS_APP->GetExecCtrl();
    
    if (bBreak)
    {
        pCtrl->Pause();
        pCtrl->SetBreakPointThread(pExecScript->m_eThreadType);
        if (bTask )
        {
            if (pTcb)
            {
                pTcb->SetStatus(TS_BREAK);
            }
        }
        else
        {
            //エディット時のスクリプト
            int iRet;
            iRet = pCtx->Suspend();
            //pExecScript->m_bBreak = true;
            //pExecScript->m_bStepStart = false;

        }
        pExecScript->m_currentBreakData.pScriptObj = pScriptObj;
        THIS_APP->ShowLineindicator(&pExecScript->m_currentBreakData);
    }
    else
    {
        /*
        pCtrl->Resume();
        pExecScript->m_bBreak = false;
        pExecScript->m_bStepStart = false;
        */
    }


    if (bTask && pTcb)
    {
        if ( (pTcb->GetCmd() & TCMD_SUSPEND_MASK) )
        {
            pCtx->Suspend();
        }
    }

}

TCB* CExecContextThread::GetCurrentTcb()
{
    return m_pCurrentTcb;
}

asIScriptContext* CExecContextThread::GetContextLock()
{
    asIScriptContext* pRet;
    m_MtxContext.lock();

    if (m_stackContextCache.empty())
    {
        asIScriptEngine* pEngine = THIS_APP->GetScriptEngine()->GetEngine();
        std::unique_ptr<AsScriptContext> pContext( new AsScriptContext(pEngine) );
        asIScriptContext* pCurrentContext;
        pCurrentContext = pContext->Get();
        pCurrentContext->SetLineCallback(asFUNCTION(LineCallbackS), this, asCALL_CDECL);
        pCurrentContext->SetExceptionCallback(asFUNCTION(ExceptionCallback), this, asCALL_CDECL);
        m_stackContextCache.push(std::move(pContext));
    }
    pRet = m_stackContextCache.top()->Get();
    m_MtxContext.unlock();
    return pRet;
}

//編集時に実行する場合のコンテキスト
asIScriptContext* CExecContextThread::GetNonThreadContext()
{
    //m_MtxContext.lock();
    asIScriptContext* pRet = NULL;

    if(!m_nonThreadContext)
    {
        asIScriptEngine* pEngine = THIS_APP->GetScriptEngine()->GetEngine();
        m_nonThreadContext = std::make_unique<AsScriptContext>(pEngine);
        pRet = m_nonThreadContext->Get();
        pRet->SetLineCallback(asFUNCTION(LineCallbackS), this, asCALL_CDECL);
        pRet->SetExceptionCallback(asFUNCTION(ExceptionCallback), this, asCALL_CDECL);

    }
    else
    {
        pRet = m_nonThreadContext->Get();
    }

    return pRet;
    //m_MtxContext.unlock();

}

void CExecContextThread::_PushContext(std::unique_ptr<AsScriptContext> pContext)
{
    m_MtxContext.lock();
    m_stackContextCache.push(std::move(pContext));
    m_MtxContext.unlock();
}

std::unique_ptr<AsScriptContext> CExecContextThread::MoveContext()
{
    if (m_stackContextCache.empty())
    {
        asIScriptEngine* pEngine = THIS_APP->GetScriptEngine()->GetEngine();
        std::unique_ptr<AsScriptContext> pRet(new AsScriptContext(pEngine));
        pRet->SetLineCallback(asFUNCTION(LineCallbackS), this, asCALL_CDECL);
        return std::move(pRet);
    }

    std::unique_ptr<AsScriptContext> pRet = std::move(m_stackContextCache.top());
    m_MtxContext.lock();
    m_stackContextCache.pop();
    m_MtxContext.unlock();
    return std::move(pRet);
}

bool CExecContextThread::Resume()
{
    if (!m_bBreak)
    {
        return false;
    }
    m_bStepStart = true;
    m_action = RESUME;
    m_condBreakStart.notify_one();
    return false;
}

bool CExecContextThread::StepOut()
{
    if (!m_bBreak)
    {
        return false;
    }
    m_bStepStart = true;
    m_action = STEP_OUT;
    m_condBreakStart.notify_one();
    m_lastBreak.iStackLevel = m_currentBreakData.pAsContext->GetCallstackSize();
    return false;
}


bool CExecContextThread::StepInto()
{
    if (!m_bBreak)
    {
        return false;
    }
    m_bStepStart = true;
    m_action = STEP_INTO;
    m_condBreakStart.notify_one();
    return false;
}

bool CExecContextThread::StepOver()
{
    if (!m_bBreak)
    {
        return false;
    }
    m_bStepStart = true;
    m_action = STEP_OVER;
    m_lastBreak.iStackLevel = m_currentBreakData.pAsContext->GetCallstackSize();
    m_condBreakStart.notify_one();
    return false;
}


bool CExecContextThread::Start()
{
    //TODO:スレッドプライオリティの設定
    using namespace std;
    _DeleteAllQue();

    if (!m_psThread)
    {
        m_bExec = false;
        m_psThread = make_unique< thread >(&CExecContextThread::ExecQue, this);
        HANDLE hThreadHandle = m_psThread->native_handle();
        ::SetThreadPriority(hThreadHandle, THREAD_PRIORITY_ABOVE_NORMAL);

        if (m_psEndThread)
        {
            m_psEndThread->join();
            m_psEndThread.reset();
        }
    }
    return true;
}

bool CExecContextThread::StartDisp()
{
    //TODO:スレッドプライオリティの設定
    using namespace std;
    using namespace EXEC_COMMON;
    if (!m_psThread)
    {
        m_bExec = false;
        m_psThread = make_unique< thread >(&CExecContextThread::ExecOhter, this, TH_DISP);
        HANDLE hThreadHandle = m_psThread->native_handle();
        ::SetThreadPriority(hThreadHandle, THREAD_PRIORITY_ABOVE_NORMAL);
    }
    return true;
}

bool CExecContextThread::StartPane()
{
    //TODO:スレッドプライオリティの設定
    using namespace std;
    using namespace EXEC_COMMON;
    if (!m_psThread)
    {
        m_bExec = false;
        m_psThread = make_unique< thread >(&CExecContextThread::ExecOhter, this, TH_PANE);
        HANDLE hThreadHandle = m_psThread->native_handle();
        ::SetThreadPriority(hThreadHandle, THREAD_PRIORITY_ABOVE_NORMAL);
    }
    return true;
}

bool CExecContextThread::End()
{
    using namespace std;
    if (m_psThread)
    {
        m_bStart = true;
        m_bStop = true;
        m_condLoopStart.notify_one();
    }
    m_psEndThread = make_unique< thread >(&CExecContextThread::_EndThread, this);
    return true;
}

bool CExecContextThread::_EndThread()
{
    if (m_psThread)
    {
        //TODO: 待ち時間パラメータ化
        const auto rel_time = std::chrono::seconds(5);

DB_PRINT_TIME(_T("CExecContextThread::End(%d)\n"), m_eThreadType);

        std::unique_lock<std::mutex> lkEnd(m_MtxEnd);

        bool ret;
        ret = m_condEnd.wait_for(lkEnd, rel_time, [=]{return m_bExitExecQue;});
        if (!ret)
        {
DB_PRINT_TIME(_T("CExecContextThread::End(%d Fail)\n"), m_eThreadType);

            //std::lock_guard<std::mutex> lock(m_Mtx);
            if (m_pCurrentTcb)
            {
               //TODO:要テスト
                STD_DBG(_T("Time out 1\n"));
                asIScriptContext* pCtx;
                pCtx = m_pCurrentTcb->GetCurrentContext();
                if (pCtx)
                {
                    pCtx->Abort();
                }
            }
            ret = m_condEnd.wait_for(lkEnd, rel_time, [=]{return m_bExitExecQue;});
            if (!ret)
            {
DB_PRINT_TIME(_T("CExecContextThread::End(%d Fail2)\n"), m_eThreadType);

                //TODO:要テスト
                STD_DBG(_T("Time out 2\n"));
                HANDLE hThreadHandle = m_psThread->native_handle();
                BOOL bRet = ::TerminateThread(hThreadHandle , FALSE);
            }
        }
        m_psThread->join();
        m_psThread.reset();

        _DeleteAllQue();

DB_PRINT_TIME(_T("CExecContextThread::End(%d End)\n"), m_eThreadType);

    }
    return true;
}

void CExecContextThread::_DeleteAllQue()
{
    m_reqInsertTcb.clear();
    m_stackExecTcb.clear();
    while(!m_stackContextCache.empty())
    {
        m_stackContextCache.pop();
    }
    m_nonThreadContext.reset();
}



bool CExecContextThread::EndThreadWait()
{
    if (m_psEndThread)
    {
        m_psEndThread->join();
        m_psEndThread.reset();
    }
    return true;
}

bool CExecContextThread::SuspendTcb(std::shared_ptr<TCB> pTcb)
{
    std::lock_guard<std::mutex> lock(m_Mtx);
    if (m_pCurrentTcb)
    {
        if(m_pCurrentTcb == pTcb.get())
        {
            m_pCurrentTcb->SetCmd( TCMD_SUSPEND );
            return true;
        }
    }

    CMD_TRANSITION cmdRet;
    cmdRet = pTcb->IsSetCmd(TCMD_SUSPEND);

    if ((cmdRet == CT_NG) || 
        (cmdRet == CT_ERROR))
    {
        return false;
    }

    pTcb->SetCmd( TCMD_SUSPEND );
    pTcb->SetStatus( TS_SUSPEND );

    return true;
}

bool CExecContextThread::TerminateTcb(std::shared_ptr<TCB> pTcb)
{
    std::lock_guard<std::mutex> lock(m_Mtx);
    if (m_pCurrentTcb)
    {
        if(m_pCurrentTcb == pTcb.get())
        {
            m_pCurrentTcb->SetCmd( TCMD_TERMINATE);
            return true;
        }
    }
    pTcb->_SetExecType(ET_ONE_SHOT);
    DeleteTcb(pTcb, true);
    return true;
}



bool CExecContextThread::AbortTcb(std::shared_ptr<TCB> pTcb)
{
    bool bRet;
    //pTcb->AddDebugCount();
    //DB_PRINT(_T("AbortTcb[%d] %d %I64x %s\n"), pTcb->GetId(), pTcb->GetDebugCount(), pTcb.get(), pTcb->GetTaskName().c_str());
    bRet = _StartTcb(pTcb, true);
    //DB_PRINT(_T("AbortTcb End[%d] %d %I64x %s\n"), pTcb->GetId(), pTcb->GetDebugCount(), pTcb.get(), pTcb->GetTaskName().c_str());
    return bRet;
}


bool CExecContextThread::StartTcb(std::shared_ptr<TCB> pTcb)
{
    bool bRet;

#ifdef _DEBUGX
StdString strTcb = pTcb->GetStr();
DB_PRINT(_T("--------StartTcb-----\n"));
DB_PRINT(_T("TCB %s \n"), strTcb.c_str());
DB_PRINT(_T("-----------------\n"));
#endif
 
    //pTcb->AddDebugCount();
    //DB_PRINT(_T("StartTcb[%d] %d %I64x %s\n"), pTcb->GetId(), pTcb->GetDebugCount(), pTcb.get(), pTcb->GetTaskName().c_str());
    bRet = _StartTcb(pTcb, false);
    //DB_PRINT(_T("StartTcb End[%d] %d %I64x %s\n"), pTcb->GetId(), pTcb->GetDebugCount(), pTcb.get(), pTcb->GetTaskName().c_str());
    return bRet;
}


bool CExecContextThread::_StartTcb(std::shared_ptr<TCB> pTcb, bool bAbort)
{
    if (!pTcb)
    {
        return false;
    }

    //std::unique_lock<std::mutex>(m_MtxExec);
    std::lock_guard<std::mutex> lock(m_Mtx);

    EXEC_CMD eCmd;
    if (!bAbort)
    {
        eCmd = TCMD_RUN;
    }
    else
    {
        eCmd = TCMD_ABORT;
    }

    CMD_TRANSITION cmdRet = CT_OK;

    if ((pTcb->eThread == EXEC_COMMON::TH_DISP) ||
        (pTcb->eThread == EXEC_COMMON::TH_PANE))
    {
        pTcb->SetCmd(eCmd);

        {
        std::lock_guard<std::mutex> lock(m_MtxStackExec);
        m_stackExecTcb.push_front(pTcb);
        }

        m_bStart = true;
        m_condLoopStart.notify_one();
        return true;
    }

    cmdRet = pTcb->IsSetCmd(eCmd);

    if ((cmdRet == CT_NG) || 
        (cmdRet == CT_ERROR))
    {
        DB_PRINT(_T("_StartTcb ERROR %I64x %s %d->%d\n"), pTcb.get(), 
                                                   pTcb->GetTaskName().c_str(),
                                                   pTcb->GetCmd(), eCmd);

        return false;
    }
    else if (cmdRet == CT_NEXT)
    {
        bool bRet;
        bRet = pTcb->PushCmd(eCmd);

        //DB_PRINT(_T("PushCmd %I64x %s %d->%d\n"), pTcb.get(), 
        //                                           pTcb->GetTaskName().c_str(),
        //                                           pTcb->GetCmd(), eCmd);

        return bRet;
    }

    //プライオリティが高い場合は実行中の関数を
    //中断させる

    if (m_pCurrentTcb)
    {
        if (m_pCurrentTcb->GetPriority() > pTcb->GetPriority())
        {
            //割り込み要求を設定
            if (m_pCurrentTcb->IsSetCmd(TCMD_INTRRUPT) == CT_OK)
            {
                //DB_PRINT(_T("_StartTcb TCMD_INTRRUPT %I64x %s\n"), pTcb.get(), pTcb->GetTaskName().c_str());
                m_pCurrentTcb->SetCmd( TCMD_INTRRUPT);
            }

            pTcb->SetCmd(eCmd);
            {
            std::lock_guard<std::mutex> lock(m_MtxStackExec);
            m_stackExecTcb.push_front(pTcb);
            }
            return true;
        }
    }

    pTcb->SetCmd(eCmd);

    bool bInsert = false;
    {
        //std::lock_guard<std::mutex> lock(m_MtxInsert);
        if (!m_reqInsertTcb.empty())
        {
            std::list<  std::shared_ptr<TCB> >::iterator ite;
            std::list<  std::shared_ptr<TCB> >::iterator ite2;

            for(ite = m_reqInsertTcb.begin();
                ite != m_reqInsertTcb.end();
                ite++)
            {
                ite2 = ite;
                if ( (*ite)->GetPriority() >  pTcb->GetPriority())
                {
                    //DB_PRINT(_T("_StartTcb insert %I64x %s\n"), pTcb.get(), pTcb->GetTaskName().c_str());
                    ite = m_reqInsertTcb.insert(ite, pTcb);
                    bInsert = true;
                    break;
                }
            }
        }

        if(!bInsert)
        {
            m_reqInsertTcb.push_back(pTcb);
        }
    }

    m_bStart = true;
    m_condLoopStart.notify_one();

    return true;
}

bool CExecContextThread::_InsertReqToStackExecTcb()
{
    //std::lock_guard<std::mutex> lock(m_MtxInsert);
    if (m_reqInsertTcb.empty())
    {
        return false;

    }

    std::shared_ptr<TCB> pTcb = *m_reqInsertTcb.begin();



    if (m_stackExecTcb.empty())
    {
        std::lock_guard<std::mutex> lock(m_MtxStackExec);
        m_stackExecTcb.push_back(pTcb);
        m_reqInsertTcb.pop_front();
        return true;
    }

    for(auto ite  = m_stackExecTcb.begin();
             ite != m_stackExecTcb.end();
             ite++)
    {
        if ((*ite)->GetPriority() > pTcb->GetPriority())
        {
            std::lock_guard<std::mutex> lock(m_MtxStackExec);
            ite = m_stackExecTcb.insert(ite, pTcb);
            m_reqInsertTcb.pop_front();
            return true;
        }
    }

    {
    std::lock_guard<std::mutex> lock(m_MtxStackExec);
    m_stackExecTcb.push_back(pTcb);
    }
    m_reqInsertTcb.pop_front();
    return true;

}

bool CExecContextThread::IsCurrrentContext(asIScriptContext *ctx)
{
    std::lock_guard<std::mutex> lock(m_Mtx);
    if (!m_pCurrentTcb)
    {
        return false;
    }

    if (m_pCurrentTcb->GetCurrentContext() == ctx)
    {
        return true;
    }
    return false;
}

bool CExecContextThread::ExitCurrent()
{
    std::lock_guard<std::mutex> lock(m_Mtx);
    if (!m_pCurrentTcb)
    {
        return false;
    }
    
    if (m_pCurrentTcb->IsSetCmd(TCMD_EXIT) == CT_OK)
    {
        m_pCurrentTcb->SetCmd( TCMD_EXIT);
        return true;
    }
    return false;
}

bool CExecContextThread::EndCycleCurrent()
{
    std::lock_guard<std::mutex> lock(m_Mtx);
    if (!m_pCurrentTcb)
    {
        return false;
    }
    m_pCurrentTcb->SetExecType(ET_ONE_SHOT);
    return true;
}


bool CExecContextThread::SuspendCurrent()
{
    std::lock_guard<std::mutex> lock(m_Mtx);
    if (!m_pCurrentTcb)
    {
        return false;
    }

    //割り込みがあった場合は処理は上書きとなる
    STD_ASSERT(m_pCurrentTcb->GetStatus() == TS_RUN ) ;

#if 0
    if (m_pCurrentTcb->GetCmd() == TCMD_INTRRUPT)
    {
       DB_PRINT(_T("SuspendCurrent Intrrupt Tcb  %I64x  Id%d \n"), m_pCurrentTcb, m_pCurrentTcb->GetId());

    }
#endif

    m_pCurrentTcb->SetCmd( TCMD_SUSPEND );
    //m_pCurrentTcb->pCurrentContext->Get()->Suspend();
    return true;
}

bool CExecContextThread::SleepCurrent(DWORD dwTimeOut)
{
    std::lock_guard<std::mutex> lock(m_Mtx);
    if (!m_pCurrentTcb)
    {
        return false;
    }


    //割り込みがあった場合は処理は上書きとなる
    STD_ASSERT(m_pCurrentTcb->GetStatus() == TS_RUN ) ;

#if 0
    if (m_pCurrentTcb->GetCmd() == TCMD_INTRRUPT)
    {
       DB_PRINT(_T("SleepCurrent Intrrupt Tcb  %I64x  Id%d \n"), m_pCurrentTcb, m_pCurrentTcb->GetId());

    }
#endif
    m_pCurrentTcb->SetSleepStartTime();
    m_pCurrentTcb->SetSleepTime(dwTimeOut);
    m_pCurrentTcb->SetCmd( TCMD_SLEEP );

    return true;
}

void CExecContextThread::YieldCurrent()
{
    std::lock_guard<std::mutex> lock(m_Mtx);

    if (!m_pCurrentTcb)
    {
        return;
    }

    //DB_PRINT(_T("Yield{%d} %I64x  Start3 \n"), m_eThreadType, m_pCurrentTcb);

    //割り込み要求を設定
    //割り込みがあった場合は処理は上書きとなる
    STD_ASSERT(m_pCurrentTcb->GetStatus() == TS_RUN ) ;

#ifdef _DEBUG
    if (m_pCurrentTcb->GetCmd() == TCMD_INTRRUPT)
    {
       DB_PRINT(_T("YieldCurrent Intrrupt Tcb  %I64x  Id%d \n"), m_pCurrentTcb, m_pCurrentTcb->GetId());

    }
#endif

    m_pCurrentTcb->SetCmd( TCMD_YIELD );


    //Queの再配置はExecQue内で行う
}

#define PRINTSTS (0)


bool CExecContextThread::ExecOhter (EXEC_COMMON::E_THREAD_TYPE eType)
{
    TCB tcbOld; //デバッグ用
    std::unique_lock<std::mutex> lk(m_MtxThreadLoop); 
    std::unique_lock<std::mutex> lkStep(m_MtxThreadStep); 
    m_bExitExecQue = false;
    THIS_APP->GetExecCtrl()->AddThreadCount();
    try{
    while(1)
    {
         m_bExec = false;

         if ( m_reqInsertTcb.empty() &&
              m_stackExecTcb.empty())
         {
             m_condLoopStart.wait(lk,  [=]{return m_bStart;});
         }

         if (m_bBreak)
         {
             m_condBreakStart.wait(lkStep,  [=]{return m_bStepStart;});
         }

        std::shared_ptr<TCB> pTcb;
        {
            std::lock_guard<std::mutex> lock(m_Mtx);

            m_bExec = true;

            if (m_bStop)
            {
                m_bExec = false;
                break;
            }

            if (!m_reqInsertTcb.empty())
            {
                _InsertReqToStackExecTcb();
            }

            if (m_stackExecTcb.empty())
            {
                //ここは通らないはず
                DB_PRINT(_T("m_stackExecTcb.empty\n"));
                continue;
            }

            auto iteTop = m_stackExecTcb.begin();
            pTcb = (*iteTop);

        }

        if (eType == EXEC_COMMON::TH_PANE)
        {
            THIS_APP->UpdateExecPaneFromThread();
        }
        else if (eType == EXEC_COMMON::TH_DISP)
        {
            THIS_APP->UpdateExecViewFromThread();
        }

        if (pTcb)
        {
            DeleteTcb(pTcb, true);
        }
    }
    }
    catch(...)
    {
        asThreadCleanup();
        //m_Mtx.unlock();
        m_bExec = false;
    }
    m_bExitExecQue = true;
    m_condEnd.notify_all();
    m_bStart = false;
    m_bStop = false;
    THIS_APP->GetExecCtrl()->SubThreadCount();
    return false;
}

bool CExecContextThread::ExecQue()
{
    TCB tcbOld; //デバッグ用
    std::unique_lock<std::mutex> lk(m_MtxThreadLoop); 
    std::unique_lock<std::mutex> lkStep(m_MtxThreadStep); 
    m_bExitExecQue = false;
    THIS_APP->GetExecCtrl()->AddThreadCount();
    try{
    while(1)
    {
//DB_PRINT(_T("ExecQue{%d} req[%d] exec[%d] Start0 \n"), m_eThreadType, 
//                                                       m_reqInsertTcb.size(),
//                                                       m_stackExecTcb.size());
         m_bExec = false;
        DWORD dwStartTime = 0;
        LARGE_INTEGER nFreq, nBefore, nAfter;
        memset(&nFreq,   0x00, sizeof nFreq);
        memset(&nBefore, 0x00, sizeof nBefore);
        memset(&nAfter,  0x00, sizeof nAfter);


         if ( m_reqInsertTcb.empty() &&
              m_stackExecTcb.empty())
         {
             m_condLoopStart.wait(lk,  [=]{return m_bStart;});
         }

         if (m_bBreak)
         {
             m_condBreakStart.wait(lkStep,  [=]{return m_bStepStart;});
         }

        std::shared_ptr<TCB> pTcb;
        {
            std::lock_guard<std::mutex> lock(m_Mtx);

 
             m_bExec = true;

            if (m_bStop)
            {
                //スレッド内でスクリプトを実行した場合
                //内部でスレッドローカルストレージ (TLS) が作成される
                // asCThreadManager::GetLocalData
                // これを削除するためには、スレッド上で
                // asThreadCleanupを呼び出す必要がある。
                // 
                asThreadCleanup();
                m_bExec = false;
                break;
            }

            if (!m_reqInsertTcb.empty())
            {
                _InsertReqToStackExecTcb();
            }

            if (m_stackExecTcb.empty())
            {
                //ここは通らないはず
                DB_PRINT(_T("m_stackExecTcb.empty\n"));
                m_bStart = false;
                continue;
            }


            auto iteTop = m_stackExecTcb.begin();
            pTcb = (*iteTop);
    //DB_PRINT(_T("ExecQue{%d} %I64x  Start1 \n"), m_eThreadType, pTcb);

            if (!pTcb)
            {
                //TODO:エラー表示
                m_stackExecTcb.pop_front();
                //m_Mtx.unlock();
                continue;
            }
            tcbOld = *pTcb;
#ifdef _DEBUGX
    StdString strTcb = pTcb->GetStr();
    DB_PRINT(_T("--------ExecQue-----\n"));
    DB_PRINT(_T("TCB %s \n"), strTcb.c_str());
    DB_PRINT(_T("--------------------------\n"));
#endif
            CMD_RETURN eRet;
            eRet = pTcb->ApplyCommand(this);

            if (eRet == RET_DELETE)
            {
                DeleteTcb(pTcb, true);
                //m_Mtx.unlock();
                continue;
            }
            else if (eRet == RET_SKIP)
            {
                //m_Mtx.unlock();
                continue;
            }
            else if (eRet == RET_NG)
            {

            }
            //m_Mtx.unlock();



            //EXEC_CMD eCmd = pTcb->GetCmd();
            //ASSERT(eCmd == TCMD_NONE);
            //==========================
            //m_Mtx.lock();
            m_pCurrentTcb = pTcb.get();
            if (m_pExecCtrl->IsMeasureExecTime())
            {
                ::QueryPerformanceFrequency(&nFreq);
                ::QueryPerformanceCounter(&nBefore);	

                //dwStartTime = ::GetTickCount();
            }
    #if 0
            if(!m_pCurrentTcb->GetCurrentContext())
            {
                DB_PRINT(_T("GetCurrentContext is Null {%s} %d %I64x \n"), pTcb->GetTaskName().c_str(), 
                    pTcb->GetDebugCount(),
                    pTcb.get());
            }//DBG
    #endif
            //m_Mtx.unlock();
         }
//DB_PRINT(_T("ExecQue{%d} %I64x  Start2 \n"), m_eThreadType, pTcb);

        int iRet;
        iRet = pTcb->Exec();

        double dExecTime;
        if (m_pExecCtrl->IsMeasureExecTime())
        {
            if (pTcb->eType == ET_CYCLE)
            {

                ::QueryPerformanceCounter(&nAfter);

                dExecTime = (nAfter.QuadPart - nBefore.QuadPart) * 1000.0 / nFreq.QuadPart;

                //dExexTime = CUtil::DiffTime(dwStartTime, ::GetTickCount());
                m_pExecCtrl->AddExecTime(m_eThreadType, pTcb->dwCycleTime, dExecTime);
                pTcb->dExecTime = dExecTime;
            }
        }

        {
            std::lock_guard<std::mutex> lock(m_Mtx);

            m_pCurrentTcb = NULL;
            //==========================
    //DB_PRINT(_T("ExecQue{%d} %I64x  Start3 \n"), m_eThreadType, pTcb);

            _InsertReqToStackExecTcb();

            if (iRet == EX_FINISH)
            {

                if (m_bBreak)
                {
                    CExecCtrl* pCtrl = THIS_APP->GetExecCtrl();
                    pCtrl->Resume();
                    m_action = RESUME;
                    m_bBreak = false;
                    m_bStepStart = false;
                }
    #if 0
                DB_PRINT(_T("Cmd Finish {%s} %d \n"), pTcb->GetTaskName().c_str(), 
                    pTcb->GetDebugCount());
    #endif
                if (pTcb->IsNext())
                {
    #if 0
        DB_PRINT(_T("Cmd Next {%s} %d \n"), pTcb->GetTaskName().c_str(), 
            pTcb->GetDebugCount());
                    pTcb->PopCmd();
    #endif

                }
                else
                {
                    DeleteTcb(pTcb, true);
                }
            }
            else if (iRet == EX_SUSPEND) 
            {
                //if (m_pExecCtrl->GetExecStatus() == )

                if (m_bBreak)
                {
                    //ステップ実行モードに移行

                }



                //コンテキストの保存
                switch(pTcb->GetCmd())
                {
                    case TCMD_INTRRUPT:
                        //通常は現在のスタックの上に割り込み対象
                        pTcb->SetStatus(TS_YIELD);
                        //が乗っているはず
                        break;

                    case TCMD_YIELD:
                    {
                        std::lock_guard<std::mutex> lock(m_MtxStackExec);
                        pTcb->SetStatus(TS_YIELD);
                        //現在のTCBを最後に移す
                        std::shared_ptr<TCB> pCurrent = m_stackExecTcb[0];
                        m_stackExecTcb.push_back(pCurrent);
                        m_stackExecTcb.pop_front();
                    }
                        break;

                    case TCMD_SUSPEND:
//DB_PRINT(_T("TS_SUSPEND {%s} %d\n"), pTcb->GetTaskName().c_str(), pTcb->GetId());
                        pTcb->SetStatus(TS_SUSPEND);
                        DeleteTcb(pTcb, false);
                        break;

                    case TCMD_SLEEP:
                    {
//DB_PRINT(_T("TS_SLEEP {%s} %d\n"), pTcb->GetTaskName().c_str(), pTcb->GetId());
                        pTcb->SetStatus(TS_SLEEP);
                        m_pExecCtrl->SetSleep(pTcb->GetId());
                        DeleteTcb(pTcb, false);
                        break;
                    }

                    case TCMD_TERMINATE:
                    {
                        pTcb->_SetExecType(ET_ONE_SHOT);
                        DeleteTcb(pTcb, true);
                        break;
                    }

                    default:
                        //
                        break;
                }
                pTcb->PopCmd();
            }
            else if (iRet == EX_ERROR)
            {
                if (pTcb)
                {
                    DB_PRINT(_T("pTcb->Exec Error {%s} %I64x \n"), pTcb->GetTaskName().c_str(), pTcb.get());
                    pTcb->PopCmd();
                }
                else
                {
                    DB_PRINT(_T("pTcb->Exec Error {%s} %I64x \n"), pTcb->GetTaskName().c_str(), 0);
                }
            }
            else
            {
                //エラー発生
                //TODO:エラー表示、例外処理
                DB_PRINT(_T("pTcb->Exec Error {%d} %I64x  Start2 \n"), iRet, pTcb);
            }
        }
    }
    }
    catch(...)
    {
        asThreadCleanup();
        //m_Mtx.unlock();
        m_bExec = false;
    }
    m_bExitExecQue = true;
    m_condEnd.notify_all();
    m_bStop = false;
    THIS_APP->GetExecCtrl()->SubThreadCount();
    return true;
}

 bool CExecContextThread::IsTcbOnStack(int iId)
 {
    auto ite =  std::find_if(m_stackExecTcb.begin(),
                            m_stackExecTcb.end(), [=]
                            ( std::shared_ptr<TCB> pSearchTcb )
    {
        return (pSearchTcb->GetId() == iId);
    });

    if (ite == m_stackExecTcb.end())
    {
        return false;
    }

    return true;
 }

bool CExecContextThread::DeleteTcb(std::shared_ptr<TCB> pTcb, bool bExt)
{
    
    auto ite =  std::find_if(m_stackExecTcb.begin(),
                            m_stackExecTcb.end(), [=]
                            ( std::shared_ptr<TCB> pSearchTcb )
    {
        return (pSearchTcb == pTcb);
    });



    if (ite == m_stackExecTcb.end())
    {
        DB_PRINT(_T("Not find DeleteTcb %I64x %d\n"), pTcb.get(),
            pTcb->GetDebugCount());
        return false;
    }

#if 0
    DB_PRINT(_T("DeleteTcb %s %d %d\n"), pTcb->GetTaskName().c_str(), pTcb->GetId(), pTcb->GetDebugCount());
#endif
    if (bExt)
    {
        //Sleep,Suspend時は、コンテキストを保持しておく
        //タスクを削除する場合は、コンテキストを返す
        _PushContext(std::move(pTcb->GetCurrentContextPtr()));
        if (pTcb->GetExecType() == ET_ONE_SHOT)
        {
            pTcb->SetCmd( TCMD_DELETE);
            pTcb->SetStatus(TS_EXT);

        }
        else
        {
//DB_PRINT(_T("TS_WAIT_CYCLE {%s} %d\n"), pTcb->GetTaskName().c_str(), pTcb->GetId());
            pTcb->SetStatus(TS_WAIT_CYCLE);
            pTcb->SetCmd( TCMD_NONE);
        }
    }

    pTcb->bResume = false;

    {
    std::lock_guard<std::mutex> lock(m_MtxStackExec);
    m_stackExecTcb.erase(ite);
    }

    if (m_stackExecTcb.empty())
    {
#if 0
    DB_PRINT(_T("DeleteTcb Empty %I64x %d, sts=%d cmd=%d\n"), pTcb.get(), 
        pTcb->GetDebugCount(), 
        pTcb->GetStatus(), 
        pTcb->GetCmd());
#endif
        m_bStart = false;
    }
    else
    {

        for(int i = 0; i < 10; i++)
        {
            auto ite =  std::find_if(m_stackExecTcb.begin(),
                                    m_stackExecTcb.end(), [=]
                                    ( std::shared_ptr<TCB> pSearchTcb )
            {
                return (pSearchTcb == pTcb);
            });

            if (ite != m_stackExecTcb.end())
            {
                DB_PRINT(_T("Delete fail %I64x %d\n"), pTcb.get(), pTcb->GetDebugCount());
            }
            else
            {
#if 0
DB_PRINT(_T("Delete Sucess %I64x %d\n"), pTcb.get(), pTcb->GetDebugCount());
#endif
                if (m_stackExecTcb.empty())
                {
#if 0
                    DB_PRINT(_T("DeleteTcb Empty %I64x %d\n"), pTcb.get(), pTcb->GetDebugCount());
#endif
                    m_bStart = false;
                }

                break;
            }

            {
            std::lock_guard<std::mutex> lock(m_MtxStackExec);
            m_stackExecTcb.erase(ite);
            }
        }

    }

    if (pTcb->GetCmd() == TCMD_DELETE)
    {
        int iId = pTcb->GetId();
#if 0
DB_PRINT(_T("Req Deltask %I64x %d\n"), pTcb.get(), iId);
#endif
        m_pExecCtrl->DelTsk(iId);
    }
    return true;
}
