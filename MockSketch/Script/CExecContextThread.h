#pragma once

#include <deque>
#include <vector>
#include <functional>
#include <mutex>
#include <thread>
#include <memory>
#include <stack>
#include <list>
#include <map>
#include <deque>
#include <condition_variable>

#include "ExecCommon.h"
#include "view/CURRENT_BREAK_CONTEXT.h"
#include "TCB.h"


class CScriptObject;
class TCB;
class CExecCtrl;
class CModule;
class asIScriptContext;
class asIScriptEngine;
class asIScriptFunction;
class CURRENT_BREAK_CONTEXT;
typedef  std::list < std::shared_ptr <TCB> > TCB_LIST;

/*
class EXEC_BREAK_DATA
{
public:

    std::string strSection;
    CScriptObject* pContext;
    int iLineNo;
    int iColum;
    asIScriptContext* pAsContext;
};
*/

struct TCBTIMER_GROUP
{
    //DWORD iTimerSet;    //周期タイマー値
    DWORD dwStart;      //開始時刻
    std::vector<TCB*> lstTcb;
};


class CExecContextThread
{
public:
	enum DebugAction
	{
		RESUME,  // continue until next break point
		STEP_INTO, // stop at next instruction
		STEP_OVER, // stop at next instruction, skipping called functions
		STEP_OUT   // run until returning from current function
	};

    struct BreakData
    {
        asIScriptFunction *pFunc;
        unsigned int iStackLevel;
        TCB* pTcb;
    };


public:
    virtual ~CExecContextThread();


    //===============================================
    bool CheckBreakPoint(asIScriptContext *ctx,
                         const std::string& strFile,
                         int lineNbr,
                         int iCol);
    bool BreakCallback(asIScriptContext *pCtx);
    static void LineCallbackS(asIScriptContext *pCtx, void *context);
    bool IsDebugMode(){return m_bDebug;}
    void SetDebugMode(bool bDebug){m_bDebug = bDebug;}
    static void ExceptionCallback(asIScriptContext* pContext, void *param);

    //===============================================


    TCB* GetCurrentTcb();

 

    bool ExecGroup(int iGroupNo);

    bool Resume();
    bool StepOut();
    bool StepInto();
    bool StepOver();
    bool Break();


    bool Start();
    bool StartDisp();
    bool StartPane();
    bool End();
    bool EndThreadWait();
    bool ClearContext();

    //bool SetExec(EXEC_BREAK_DATA* pExec);

    //bool SetTask ( EXEC_DATA* pExecData);

    bool ExecQue();
    bool ExecOhter(EXEC_COMMON::E_THREAD_TYPE eType);


    //------------------------
    bool AbortTcb(std::shared_ptr<TCB> pTcb);
    bool StartTcb(std::shared_ptr<TCB> pTcb);
    bool TerminateTcb(std::shared_ptr<TCB> pTcb);
    bool SuspendTcb(std::shared_ptr<TCB> pTcb);


    bool ExitCurrent();
    bool SuspendCurrent();
    void YieldCurrent();
    bool SleepCurrent(DWORD dwTimeOut);
    bool EndCycleCurrent();
    //bool IsProssingTcb(TCB* pTcb);


    bool IsCurrrentContext(asIScriptContext *ctx);

    CExecContextThread(EXEC_COMMON::E_THREAD_TYPE eThreadType, CExecCtrl *ctrl);

    bool DeleteTcb(std::shared_ptr<TCB> pTcb, bool bExt);
    std::unique_ptr<AsScriptContext> MoveContext(); 
    bool IsTcbOnStack(int iId);

    std::list<  std::shared_ptr<TCB> >* GetListInsertTcb()
    {
        return &m_reqInsertTcb;
    };

    asIScriptContext* GetContextLock(); 
    asIScriptContext* GetNonThreadContext();
    CURRENT_BREAK_CONTEXT* GetCurrentBreakData(){ return &m_currentBreakData;}


protected:
//friend CExecCtrl;
//friend CExecContextThread;

    //int _SetExecQue(int iPri);
    //TCB* _FindTsk(std::deque<TCB>* pList, int iTaskId);
    //TCB* _FindTsk(int iTaskId);

    //注意 スクリプト内部からの呼び出しで、コンテキストが
    // 必要な場合は Tcbのcurrentcontextを使用すること
    // 割り込み中は GetGetContextと示すcontextが違うタイミングがある
    void _PushContext(std::unique_ptr<AsScriptContext> pContext);

    bool _StartTcb(std::shared_ptr<TCB> pTcb, bool bAbort);

    

    bool _InsertReqToStackExecTcb();

    bool _EndThread();

    void _DeleteAllQue();

protected:

    CExecCtrl* m_pExecCtrl;
    EXEC_COMMON::E_THREAD_TYPE     m_eThreadType;
    asIScriptEngine* m_pEngine;

    std::deque< std::shared_ptr<TCB> > m_stackExecTcb;
    std::list<  std::shared_ptr<TCB> > m_reqInsertTcb;


    std::mutex m_Mtx;
    std::mutex m_MtxContext;
    std::mutex m_MtxThreadLoop;
    std::mutex m_MtxThreadStep;
    //std::mutex m_MtxInsert;
    std::mutex m_MtxStackExec;
    std::mutex m_MtxEnd;

    std::condition_variable m_condLoopStart;
    std::condition_variable m_condBreakStart;
    std::condition_variable m_condEnd;

    bool m_bStop  = false;
    bool m_bStart = false;

    std::unique_ptr<std::thread> m_psThread;
    std::unique_ptr<std::thread> m_psEndThread;
    
    int m_iPri;

    TCB*      m_pCurrentTcb;


    std::stack< std::unique_ptr<AsScriptContext> >  m_stackContextCache;

    std::unique_ptr<AsScriptContext>                m_nonThreadContext;

    bool               m_bDebug;
    bool               m_bExec = false;
    bool               m_bExitExecQue = false;


    //--------------------
	DebugAction        m_action = RESUME;
    bool               m_bBreak = false;
    bool               m_bStepStart = false;
    int                m_iExecLevel;
	//unsigned int       m_lastCommandAtStackLevel;
	//asIScriptFunction *m_lastFunction;

    //std::deque< EXEC_BREAK_DATA > m_lstBreakData;
    CURRENT_BREAK_CONTEXT m_currentBreakData;
    
    BreakData       m_lastBreak;


};

