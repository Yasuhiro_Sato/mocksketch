/**
 * @brief			CExecCtrl実装ファイル
 * @file		    CExecCtrl.cpp
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "Mocksketch.h"
#include "CExecCtrl.h"
#include "CExecContextThread.h"
#include "CScriptObject.h"
#include "CModule.h"
#include "Utility/CUtility.h"
#include "TCB.h"
#include "ExecCommon.h"

#include "DrawingObject/CDrawingScriptBase.h"
#include "DrawingObject/CDrawingParts.h"
#include "DrawingObject/CDrawingField.h"
#include "DrawingObject/CDrawingElement.h"
#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/CFieldDef.h"

#include <algorithm>
#include <chrono>
#include <mmsystem.h>
#include  <memory>
#include <angelscript.h> 

//FOR DEBUG
//#include "Script/CScript.h"
//#include "Script/EditorView.h"
//FOR DEBUG

using namespace EXEC_COMMON;
#ifdef _DEBUG
#undef new 
#define new DEBUG_NEW
#endif 

CExecCtrl* CExecCtrl::m_pCtrl = NULL;



CExecCtrl::CExecCtrl():
m_psCycleThread(NULL),
m_iCreateId(0)
{
    m_bOneEngine = true;
    //m_pThreadManager = asGetThreadManager();
    //asPrepareMultithread  (m_pThreadManager);

    asIScriptEngine* pEngine = NULL;
    if (m_bOneEngine)
    {
        //pEngine = asCreateScriptEngine(ANGELSCRIPT_VERSION);
        //m_lstEngine.push_back(pEngine);
    }

    using namespace EXEC_COMMON;
    for (int iCnt = TH_GROUP_1; iCnt < TH_MAX; iCnt++)
    {
        EXEC_COMMON::E_THREAD_TYPE eThreadType;
        eThreadType = static_cast<EXEC_COMMON::E_THREAD_TYPE>(iCnt);
        if ((eThreadType == TH_GROUP_MAX) ||
            (eThreadType == TH_FIELD_MAX))
        {
            continue;
        }

        if (!m_bOneEngine)
        {
            //pEngine = asCreateScriptEngine(ANGELSCRIPT_VERSION);
            //m_lstEngine.push_back(pEngine);
        }


        m_mapScriptGroup[eThreadType] = (std::make_unique<CExecContextThread>(eThreadType, this));
    }
}


CExecCtrl::~CExecCtrl()
{
    _EndCycleTask();
    m_mapScriptGroup.clear();
    m_mapTcb.clear();

    /*
    asThreadCleanup();
    for(asIScriptEngine* pEngine: m_lstEngine)
    {
        pEngine->Release();
    }
    asUnprepareMultithread();
    */
}


/*
asIScriptEngine* CExecCtrl::GetEngine(EXEC_COMMON::E_THREAD_TYPE eThread)
{
    // 現状ではスレッドごとのエンジンは生成できない（モジュール毎)
    // 何故なら、実行コンテキスト(ScriptObjcet)毎にスレッドを割り当てるため
    // モジュールとスクリプトは一対一の関係にはならない

    asIScriptEngine* pEngine;
    if ((m_bOneEngine) || (eThread == EXEC_COMMON::TH_ALL))
    {
        pEngine = m_lstEngine[0];
    }
    else
    {
        pEngine = m_lstEngine[eThread];
    }
    return pEngine;
}
*/

asIScriptContext* CExecCtrl::GetContext(EXEC_COMMON::E_THREAD_TYPE eThread)
{
    if(m_mapScriptGroup[eThread])
    {
        return m_mapScriptGroup[eThread]->GetContextLock();
    }

    return NULL;
}

asIScriptContext* CExecCtrl::GetNonThreadContext(EXEC_COMMON::E_THREAD_TYPE eThread)
{
    if(m_mapScriptGroup[eThread])
    {
        return m_mapScriptGroup[eThread]->GetNonThreadContext();
    }
    return NULL;
}

/*
asIScriptContext* CExecCtrl::PushContext(EXEC_COMMON::E_THREAD_TYPE eThread)
{GetContext
    return m_mapScriptGroup[eThread]->_PushContext();

}

bool CExecCtrl::PopContext(EXEC_COMMON::E_THREAD_TYPE eThread)
{
    return m_mapScriptGroup[eThread]->_PopContext();

}
*/


std::unique_ptr<CModule> CExecCtrl::CreateModule(CObjectDef* pDef)
{
    //std::string strModuleName = pStr;
    std::unique_ptr<CModule> pModule (new CModule(pDef));

    return pModule;
}

/*
std::unique_ptr<CScriptObject> CExecCtrl::CreateScriptObject( EXEC_COMMON::E_THREAD_TYPE eThread)
{
    std::unique_ptr<CScriptObject> pContext (new CScriptObject(eThread, this));
    //m_mapScriptObject[pContext->m_pObject] = pContext.get();
    return pContext;
}

CScriptObject* CExecCtrl::GetScriptObject( asIScriptObject* pScroptObj)
{
    auto ite = m_mapScriptObject.find(pScroptObj);

    if (ite != m_mapScriptObject.end())
    {
        return ite->second;
    }
    return NULL;
}

void CExecCtrl::ReleaseScriptObject( asIScriptObject* pScroptObj)
{
    auto ite = m_mapScriptObject.find(pScroptObj);
    if (ite != m_mapScriptObject.end())
    { 
        m_mapScriptObject.erase(ite);
    }
}

int CExecCtrl::GetEnginNum()
{
    return SizeToInt(m_lstEngine.size());
}

asIScriptEngine* CExecCtrl::GetEngine(int iNum)
{
    if (iNum < 0) {return NULL;}
    if (iNum >= m_lstEngine.size()) {return NULL;}
    return m_lstEngine[iNum];
}

*/


bool CExecCtrl::Init()
{

    return false;
}

/*
bool CExecCtrl::ClearContext()
{
    return false;
}

bool CExecCtrl::AddContext(EXEC_COMMON::E_THREAD_TYPE eThreadType, 
                           CScriptObject* pContext)
{
    //pContext->SetExecCtrl(this);

    //TODO: SetUpを登録する

    return true;

}
*/

int CExecCtrl::_CreateNewId()
{
    while(1)
    {
        m_iCreateId++;
        if (m_iCreateId == INT_MAX)
        {
            m_iCreateId = 0;
        }

        if (m_mapTcb.find(m_iCreateId) == m_mapTcb.end())
        {
            return m_iCreateId;
        }
    }
    return -1;
}



int CExecCtrl::AsTaskCreate(TCB* pTcb)
{
    std::lock_guard<std::mutex> lock(m_Mtx);
    int iTaskId = _CreateNewId();

    pTcb->Create(iTaskId);

    m_mapTcb[iTaskId] = std::make_shared<TCB>();
    m_mapTcb[iTaskId]->CopyProerty(*pTcb);

    int iThread;

    iThread = pTcb->GetThreadType();


    if (pTcb->GetExecType() == ET_CYCLE)
    {
        DWORD dwCycleTime = pTcb->GetCycleTime();
        auto iteMap = m_mapTimerGroup.find(dwCycleTime);

        if (iteMap == m_mapTimerGroup.end())
        {
            TIMER_GROUP grp;
            grp.dwStart = 0;
            m_mapTimerGroup[dwCycleTime] = grp;
        }
#if 0
DB_PRINT(_T("m_mapTimerGroup[%d]  add %d\n"), dwCycleTime, iTaskId);
#endif
        m_mapTimerGroup[dwCycleTime].lstTcb.push_back(m_mapTcb[iTaskId]);
    }

#ifdef _DEBUG
StdString strTcb = m_mapTcb[iTaskId]->GetStr();
DB_PRINT(_T("--------AsTaskCreate-----\n"));
DB_PRINT(_T("TCB %s \n"), strTcb.c_str());
DB_PRINT(_T("-----------------\n"));
#endif
    return iTaskId;
}



bool CExecCtrl::AsTaskStart(int iTaskId)
{

    //TODO:ブレーク中の開始は不可
    // 新たに実行した部分で、ブレークが発生した場合
    // 元のブレーク中の状態を解除する方法がなくなってしまう
    // または。ブレークポイントをスタックするようにするか？
    std::lock_guard<std::mutex> lock(m_Mtx);

    auto iteMap = m_mapTcb.find(iTaskId);
    if (iteMap == m_mapTcb.end())
    {
        return false;
    }


    bool bRet = false;
    int iTask = iteMap->second->GetThreadType();

    if(m_mapScriptGroup[iTask])
    {
        bRet = m_mapScriptGroup[iTask]->StartTcb(iteMap->second);
    }
    return bRet;
}


bool CExecCtrl::AsTaskSetPriority(int iTaskId, int iPri)
{
    if (iPri < 0) return false;
    if (iPri > 4) return false;

    std::lock_guard<std::mutex> lock(m_Mtx);
    auto iteMap = m_mapTcb.find(iTaskId);
    if (iteMap == m_mapTcb.end())
    {
        return false;
    }

    TCB* pTcb = iteMap->second.get();
    pTcb->SetPriority(iPri);
    return true;
}

//サイクルタスクを設定
bool CExecCtrl::AsTaskSetCycle(int iTaskId, DWORD dwCycleTime)
{
    std::lock_guard<std::mutex> lock(m_Mtx);
    auto iteMap = m_mapTcb.find(iTaskId);
    if (iteMap == m_mapTcb.end())
    {
        return false;
    }

    std::shared_ptr<TCB> pTcb = iteMap->second;

    
    if (pTcb->GetExecType() == ET_CYCLE)
    {
        auto iteMap = m_mapTimerGroup.find(pTcb->GetCycleTime());
        if (iteMap != m_mapTimerGroup.end())
        {
            TIMER_GROUP* pGroup;
             pGroup = &(*iteMap).second;
             auto iteVec = std::find_if(pGroup->lstTcb.begin(),
                                        pGroup->lstTcb.end(),
                                        [=](std::shared_ptr<TCB> pTcb2)
                                         {
                                             return (pTcb2->GetId() == iTaskId);
                                         });

             if (iteVec != pGroup->lstTcb.end())
             {
                 pGroup->lstTcb.erase(iteVec);
             }
        }
    }

    pTcb->SetExecType(ET_CYCLE);
    pTcb->SetCycleTime(dwCycleTime);
    auto iteMap2 = m_mapTimerGroup.find(dwCycleTime);
    if (iteMap2 == m_mapTimerGroup.end())
    {
        TIMER_GROUP grp;
        grp.dwStart = 0;
        m_mapTimerGroup[pTcb->GetCycleTime()] = grp;
    }

//DB_PRINT(_T("m_mapTimerGroup[%d]  add %d\n"), pTcb->dwCycleTime, iTaskId);
    m_mapTimerGroup[pTcb->GetCycleTime()].lstTcb.push_back(m_mapTcb[iTaskId]);

    return true;
}



bool CExecCtrl::AsTaskSetName(int iTaskId, std::wstring strName)
{
    std::lock_guard<std::mutex> lock(m_Mtx);
    auto iteMap = m_mapTcb.find(iTaskId);
    if (iteMap == m_mapTcb.end())
    {
        return false;
    }

    TCB* pTcb = iteMap->second.get();
    if (pTcb)
    {
        pTcb->SetTaskName(strName);
        return true;
    }
    return false;
}



bool CExecCtrl::AsTaskExit()
{
   //このメソッドが呼び出される時はスクリプト内
    asIScriptContext *ctx = asGetActiveContext();
    CExecContextThread* pThread = _GetExecContextThread(ctx);

    if (pThread)
    {
        return pThread->ExitCurrent();
    }

    return false;
}


bool CExecCtrl::AsTaskEndCycle()
{
   //このメソッドが呼び出される時はスクリプト内
    asIScriptContext *ctx = asGetActiveContext();
    CExecContextThread* pThread = _GetExecContextThread(ctx);

    if (pThread)
    {
        return pThread->EndCycleCurrent();
    }

    return false;
}

bool CExecCtrl::AsTaskSleep(DWORD dwTimeOut)
{
    asIScriptContext *ctx = asGetActiveContext();
    CExecContextThread* pThread = _GetExecContextThread(ctx);

    if (pThread)
    {
        return pThread->SleepCurrent(dwTimeOut);
    }
    return false;
}


bool CExecCtrl::AsTaskAbort(int iTaskId)
{
    std::lock_guard<std::mutex> lock(m_Mtx);
    auto iteMap = m_mapTcb.find(iTaskId);
    if (iteMap == m_mapTcb.end())
    {
        return false;
    }

    
    std::shared_ptr<TCB> pTcb = iteMap->second;
    int iTask = iteMap->second->GetThreadType();

    if(m_mapScriptGroup[iTask])
    {
        return m_mapScriptGroup[iTask]->AbortTcb(pTcb);
    }
    return false;
}

bool CExecCtrl::AsTaskTerminate(int iTaskId)
{
    std::lock_guard<std::mutex> lock(m_Mtx);
    auto iteMap = m_mapTcb.find(iTaskId);
    if (iteMap == m_mapTcb.end())
    {
        return false;
    }

    std::shared_ptr<TCB> pTcb = iteMap->second;
    int iTask = iteMap->second->GetThreadType();

    bool bRet = false;
    if(m_mapScriptGroup[iTask])
    {
        bRet =  m_mapScriptGroup[iTask]->TerminateTcb(pTcb);
    }

    return bRet;
}

bool CExecCtrl::AsTaskSuspend()
{
    asIScriptContext *ctx = asGetActiveContext();
    CExecContextThread* pThread = _GetExecContextThread(ctx);

    if (pThread)
    {
        return pThread->SuspendCurrent();
    }
    return false;
}

bool CExecCtrl::AsTaskSuspend(int iTaskId)
{
    std::lock_guard<std::mutex> lock(m_Mtx);
    auto iteMap = m_mapTcb.find(iTaskId);
    if (iteMap == m_mapTcb.end())
    {
        return false;
    }

    std::shared_ptr<TCB> pTcb = iteMap->second;
    int iTask = iteMap->second->GetThreadType();

    if (pTcb)
    {
        return m_mapScriptGroup[iTask]->SuspendTcb(pTcb);
    }

    return false;
}


bool CExecCtrl::AsTaskWakeUp(int iTaskId)
{
    std::lock_guard<std::mutex> lock(m_Mtx);
    auto iteMap = m_mapTcb.find(iTaskId);
    if (iteMap == m_mapTcb.end())
    {
        return false;
    }

    std::shared_ptr<TCB> pTcb = iteMap->second;
    int iTask = iteMap->second->GetThreadType();

    if (pTcb)
    {
        if (pTcb->GetStatus() == TS_SLEEP) 
        {
                pTcb->ResetSleepTime();
                return true;
        }
        else if (pTcb->GetStatus() == TS_SUSPEND)
        {
            if(m_mapScriptGroup[iTask])
            {
                m_mapScriptGroup[iTask]->StartTcb(pTcb);
                return true;
            }
        }
    }

    return false;
}

int CExecCtrl::AsTaskStatus(int iTaskId)
{
    std::lock_guard<std::mutex> lock(m_Mtx);
    auto iteMap = m_mapTcb.find(iTaskId);
    if (iteMap == m_mapTcb.end())
    {
        return TS_EXT;
    }

    TCB* pTcb = iteMap->second.get();

    if (pTcb)
    {
        int iRet = pTcb->GetStatus();

        if (iRet & TS_RUN)
        {
            return TS_RUN;
        }
        return iRet;
    }
    return TS_EXT;
}

bool CExecCtrl::AsTaskIsExist(int iTaskId)
{
    std::lock_guard<std::mutex> lock(m_Mtx);
    std::map<int, std::shared_ptr<TCB> >::iterator iteMap;
    iteMap = m_mapTcb.find(iTaskId);
    if (iteMap == m_mapTcb.end())
    {
        return false;
    }
    return true;
}

int CExecCtrl::AsTaskGetId(std::wstring strName)
{
    std::map<int, std::shared_ptr<TCB> >::iterator iteMap;
    TCB* pTcb;
    for(iteMap  = m_mapTcb.begin();
        iteMap != m_mapTcb.end();
        iteMap++ )
    {
        pTcb = iteMap->second.get();
        
        if (pTcb)
        {
            if ( strName == pTcb->GetTaskName())
            {
                return iteMap->first;
            }
        }
    }
    return -1;
}



/*
bool CExecCtrl::TaskCommand(int iTaskId, EXEC_CMD eCmd)
{
    std::lock_guard<std::mutex> lock(m_Mtx);
    std::map<int, std::unique_ptr<TCB> >::iterator iteMap;
    iteMap = m_mapTcb.find(iTaskId);
    if (iteMap == m_mapTcb.end())
    {
        return false;
    }
    TCB* pTcb;
    pTcb = iteMap->second.get();
    int iTask = iteMap->second->GetThreadType();

    //コマンドのプライオリティ
    CExecContextThread* pThread;
    pThread = m_mapScriptGroup[iTask].get();


    switch(eCmd)
    {
    case TCMD_TERMINATE:




    }


}
*/

//
bool CExecCtrl::GetTcbMap(COUNT_MAP<int, std::shared_ptr<TCB> >* pMap)
{
    if (!pMap)
    {
        return false;
    }
    std::lock_guard<std::mutex> lock(m_Mtx);

    *pMap = m_mapTcb;

    return true;
}

int CExecCtrl::GetTcbMapCount() const
{
    return m_mapTcb.GetChgCnt();
}


bool CExecCtrl::AddScriptLoop(CDrawingScriptBase* pScriptBase)
{
    TCB exec;
    CScriptObject* pObj;
    asIScriptFunction* pFunc;
    namespace  PLC = std::placeholders;


    pObj = pScriptBase->GetScriptObject();

    pFunc = pObj->GetMethod("int Loop(TCB@)");
    if (!pFunc)
    {
        return false;
    }

    exec.SetAsFunc(pFunc);
    exec.SetFunc (std::bind(&CScriptObject::ExecMethod, pObj, PLC::_1, PLC::_2)); 

    exec.SetThreadType(pScriptBase->GetExecThreadType());
    exec.SetPriority(pScriptBase->GetPriority());
    exec.SetExecType(ET_CYCLE);
    exec.SetCycleTime(pScriptBase->GetCycleTime());
    exec.SetScriptObject(pObj);
    exec.SetTaskName(pScriptBase->GetName());

#ifdef _DEBUG
    StdString strBase = pScriptBase->GetStr();
    StdString strObj = pObj->GetStr();
    DB_PRINT(_T("--------AddScriptLoop-----\n"));
    DB_PRINT(_T("Base %s  \n"), strBase.c_str());
    DB_PRINT(_T("ScriptObject %s \n"), strObj.c_str());
    DB_PRINT(_T("CScriptObject* %I64x CScriptObject* %I64x\n"), pObj );
    DB_PRINT(_T("CScriptObject::m_pDrawingScript * %I64x\n"), pObj );
    DB_PRINT(_T("--------------------------\n"));

#endif

    int iTaskNo = THIS_APP->GetExecCtrl()->AsTaskCreate(&exec);
    m_lstLoop.push_back(iTaskNo);
    return true;
}


bool CExecCtrl::Run(EXEC_COMMON::E_THREAD_TYPE eThreadType)
{
    using namespace EXEC_COMMON; 

    int iThreadEnd = SizeToInt(m_mapScriptGroup.size());

    if (eThreadType == TH_ALL)
    {
        E_THREAD_TYPE eType;
        for(auto& ite : m_mapScriptGroup)
        {
            eType = static_cast<E_THREAD_TYPE>(ite.first);
            Run(eType);
        }
        _StartCycleTask();
        return true;
    }

    if ( (eThreadType >= TH_GROUP_1) &&
         (eThreadType < iThreadEnd))
    {
        if (m_mapScriptGroup[eThreadType])
        {
            if (eThreadType == TH_PANE)
            {
                m_mapScriptGroup[eThreadType]->StartPane();
            }
            else if (eThreadType == TH_DISP)
            {
                m_mapScriptGroup[eThreadType]->StartDisp();
            }
            else
            {
                m_mapScriptGroup[eThreadType]->Start();
            }
        }
        return true;
    }
    return false;
}

void CExecCtrl::AddThreadCount()
{
    m_iExecThreadCount++;
DB_PRINT(_T("AddThreadCount(%d)\n"), m_iExecThreadCount);

}


void CExecCtrl::SubThreadCount()
{
    m_iExecThreadCount--;
DB_PRINT(_T("SubThreadCount(%d)\n"), m_iExecThreadCount);
    if (m_iExecThreadCount == 0)
    {
        m_cvContextThread.notify_all();
    }
}

bool CExecCtrl::Pause()
{
    m_bPauseCycle = true;
    using namespace EXEC_COMMON; 

    int iThreadEnd = SizeToInt(m_mapScriptGroup.size());

    for( int iThreadType = TH_GROUP_1;
             iThreadType < TH_FIELD_MAX;
             iThreadType++)
    {
        if ((iThreadType == TH_DISP) ||
            (iThreadType == TH_PANE)
            )
        {
            continue;
        }

        if (iThreadType == TH_GROUP_MAX) 
        {
            continue;
        }

        if (m_mapScriptGroup[iThreadType])
        {
            m_mapScriptGroup[iThreadType]->Break();
        }
    }

    return true;
}


bool CExecCtrl::Resume()
{
    using namespace EXEC_COMMON; 
    m_bPauseCycle = false;
    m_eBreakPointThread = EXEC_COMMON::TH_NONE;
    m_condCycle.notify_one();

    for( int iThreadType = TH_GROUP_1;
             iThreadType < TH_FIELD_MAX;
             iThreadType++)
    {
        if ((iThreadType == TH_DISP) ||
            (iThreadType == TH_PANE)
            )
        {       
            continue;
        }

        if (iThreadType == TH_GROUP_MAX) 
        {
            continue;
        }

        if (iThreadType >= m_mapScriptGroup.size())
        {
            break;
        }

        if (m_mapScriptGroup[iThreadType])
        {
            m_mapScriptGroup[iThreadType]->Resume();
        }
    }

    return true;
}

//ブレークポイント発生スレッド設定
//Pouse直後に設定する
bool CExecCtrl::SetBreakPointThread(EXEC_COMMON::E_THREAD_TYPE eThreadType)
{
    //ポーズ中 かつ ブレークポイント発生スレッド未設定
    if (m_eBreakPointThread != EXEC_COMMON::TH_NONE)
    {
        return false;
    }

    if (!m_bPauseCycle)
    {
        return false;
    }
    
    m_eBreakPointThread = eThreadType;
    return true;
}

bool CExecCtrl::Abort(EXEC_COMMON::E_THREAD_TYPE eThreadType)
{
    using namespace EXEC_COMMON; 
    int iThreadEnd = SizeToInt(m_mapScriptGroup.size());
    if (eThreadType == TH_ALL)
    {
        /*
        for(int iType = TH_GROUP_1; 
                iType  < iThreadEnd;
                iType++)
        {
            if ((iType == TH_GROUP_MAX)||
                (iType == TH_FIELD_MAX)) 
            {
                continue;
            }

                  (&CExecContextThread::End);
DB_PRINT(_T("CExecCtrl::Abort(%d)\n"), iType);
            m_mapScriptGroup[iType]->End();
DB_PRINT(_T("CExecCtrl::Abort(%d End)\n"), iType);
        }
        */

        _EndCycleTask();
        _DeleteAllQue();
        return true;
    }


    if ( (eThreadType >= TH_GROUP_1) &&
         (eThreadType < iThreadEnd))
    {
        if (m_mapScriptGroup[eThreadType])
        {
            m_mapScriptGroup[eThreadType]->End();
        }
    }

    return true;
}

void CExecCtrl::SetSleep(int iTaskId)
{
    // 他スレッドから呼び出し
    m_lstReqSleep.SetData(iTaskId);
    if (m_bPauseCycle)
    {
        m_condCycle.notify_one();
    }
}

void CExecCtrl::_DeleteAllQue()
{
    m_mapTcb.clear();
    m_lstSleep.clear();
    m_mapTimerGroup.clear();
    m_lstReqSleep.Clear();
    m_lstDelete.Clear();
}


bool CExecCtrl::_StartCycleTask()
{
   m_bStopCycle = false;

   if (!m_psCycleThread)
   {
        m_psCycleThread = new std::thread(&CExecCtrl::_CycleTask, this);

        HANDLE hThreadHandle = m_psCycleThread->native_handle();
        ::SetThreadPriority(hThreadHandle, THREAD_PRIORITY_HIGHEST);
        m_eSts = EXEC_COMMON::EX_EXEC;
        _SetExecStatus(m_eSts);

        TCB exec;

        exec.SetThreadType(TH_DISP);
        exec.SetPriority(3);
        exec.SetExecType(ET_CYCLE);
        exec.SetCycleTime(GetDispCycleTime());
        AsTaskCreate(&exec);

        exec.SetThreadType(TH_PANE);
        exec.SetCycleTime(GetPainCycleTime());
        AsTaskCreate(&exec);
   }
   return true;
}

bool CExecCtrl::_EndCycleTask()
{
DB_PRINT(_T("---CExecCtrl::_EndCycleTask Start--\n"));
    if (m_psCycleThread)
    {
        m_bStopCycle = true;
        m_bPauseCycle = false;
        m_condCycle.notify_one();

        std::unique_lock<std::mutex> lkEnd(mtxEnd);

        if (m_psCycleThread)
        {
DB_PRINT(_T("---CExecCtrl::_EndCycleTask 1--\n"));
            const auto rel_time = std::chrono::seconds(5);
            bool ret = m_cvEnd.wait_for(lkEnd, rel_time, [=]{return m_bExitCycle;});
DB_PRINT(_T("---CExecCtrl::_EndCycleTask 2 ret = (%d)--\n"), ret);
            if (!ret)
            {
                //TODO:要テスト
                //タイムアウト 
                STD_DBG(_T("Time out\n"));
                HANDLE hThreadHandle = m_psCycleThread->native_handle();
                BOOL bRet = ::TerminateThread(hThreadHandle , FALSE);
            }
            m_psCycleThread->join();
            delete m_psCycleThread;
            m_psCycleThread = NULL;
        }

        m_lstReqSleep.Clear();
        m_lstDelete.Clear();
        m_lstSleep.clear();
        _ClearObject();
        _SetExecStatus(EXEC_COMMON::EX_EDIT);
    }
    THIS_APP->ClearAllLineindicator();
DB_PRINT(_T("---CExecCtrl::_EndCycleTask End--\n"));

    return true;
}

void CExecCtrl::DelTsk(int iTaskId)
{
    //std::lock_guard<std::mutex> lock(m_MtxDeleteList);
    m_lstDelete.SetData(iTaskId);
}


bool CExecCtrl::_DelTsk(int iTaskId)
{
#if 0
DB_PRINT(_T("CExecCtrl::_DelTsk[%d]\n"),iTaskId);
#endif 
    {
        std::lock_guard<std::mutex> lock(m_Mtx);
        std::map<DWORD, TIMER_GROUP>::iterator ite;
        for( ite = m_mapTimerGroup.begin();
             ite != m_mapTimerGroup.end();
             ite++)
        {
            std::vector<std::shared_ptr<TCB>>* pLstTcb;
            std::vector<std::shared_ptr<TCB>>::iterator iteTcb;
            pLstTcb = &ite->second.lstTcb;

            for(iteTcb  =  pLstTcb->begin();
                iteTcb !=  pLstTcb->end();
                iteTcb++)
            {
                if ( ((*iteTcb)->GetId() == iTaskId) &&
                     ((*iteTcb)->GetCmd() == TCMD_DELETE))
                {
#if 0
DB_PRINT(_T("m_mapTimerGroup[%d]  _DelTsk %d\n"), (*iteTcb)->GetCycleTime(), iTaskId);
#endif
                    iteTcb = pLstTcb->erase(iteTcb);
                    if (pLstTcb->empty())
                    {
                        break;
                    }
                }
            }

            if (pLstTcb->empty())
            {
                ite = m_mapTimerGroup.erase(ite);
                if (m_mapTimerGroup.empty())
                {
                    break;
                }
            }
        }
    }


    {
        auto iteSleep = std::find(m_lstSleep.begin(), 
                             m_lstSleep.end(), 
                             iTaskId);
        if (iteSleep != m_lstSleep.end())
        {
            m_lstSleep.erase(iteSleep);
        }
    }


    std::lock_guard<std::mutex> lock(m_Mtx);
    std::map<int, std::shared_ptr<TCB> >::iterator iteMap;
    iteMap = m_mapTcb.find(iTaskId);

    //TCB* pTcb = (iteMap->second).get();

    //EXEC_COMMON::E_THREAD_TYPE eThread;
    //eThread = pTcb->GetThreadType();
    //m_mapScriptGroup[eThread]->TerminateTcb(pTcb);

    if (iteMap != m_mapTcb.end())
    {
        m_mapTcb.erase(iteMap);
    }
    else
    {
DB_PRINT(_T("_DelTsk fail %d\n"), iTaskId);
    }
    return true;
}

bool CExecCtrl::_CycleTask()
{
    int iMinTimer;
    iMinTimer = 1;//lcm_n(m_lstTimer);

    DWORD dwOld;
    DWORD dwNow;

    dwOld = timeGetTime();

    std::map<DWORD, TIMER_GROUP>::iterator  iteMap;
    {
        std::lock_guard<std::mutex> lock(m_Mtx);
        for( iteMap = m_mapTimerGroup.begin();
             iteMap != m_mapTimerGroup.end();
             iteMap++)
        {
            if (bMesureExecTime)
            {
                for(auto& iteExec: iteMap->second.mapExec)
                {
                    iteExec.second.dSumOld = 0.0;
                    iteExec.second.dSum = 0.0;
                }

            }
            iteMap->second.dwStart = dwOld ;
        }
    }

    DWORD dwDiff;

    std::vector<std::shared_ptr<TCB>>::iterator iteTcb;
    std::vector<int>::iterator iteSleepTcbId;

    std::unique_lock<std::mutex> lk(m_MtxCycle); 
    m_bExitCycle = false;        

    try
    {
        while(1)
        {
            if (m_bStopCycle)
            {
DB_PRINT_TIME(_T("_CycleTask m_bStopCycle\n"));
                break;
            }

            bool bCycleStart;

            bCycleStart = ((!m_bPauseCycle) ||
                           (!m_lstReqSleep.IsEmpty())||
                           (!m_lstSleep.empty()));

            m_condCycle.wait(lk,  [=]
            {
                return ((!m_bPauseCycle) ||
                           (!m_lstReqSleep.IsEmpty())||
                           (!m_lstSleep.empty()));
                /*
                return (!m_bPauseCycle);   
                */
            });


            Sleep(iMinTimer);
            dwNow = timeGetTime();

            {
                try
                {
                    //std::lock_guard<std::mutex> lock(m_MtxDeleteList);
                    int iDelTaslId;
                    while (m_lstDelete.GetData(&iDelTaslId)) 
                    {
                        _DelTsk(iDelTaslId);
                    }
                }
                catch(...)
                {
    DB_PRINT(_T("_DelTsk Exp  \n"));

                }
            }

            {
                //-----------
                //Sleep処理
                //-----------
                {
                    int iSleepTaskId;
                    while(m_lstReqSleep.GetData(&iSleepTaskId))
                    {
                        m_lstSleep.push_back(iSleepTaskId);
                    }


                    std::vector<int> lstDeleteid;
                    if(!m_lstSleep.empty())
                    {
                        for( iteSleepTcbId  = m_lstSleep.begin();
                             iteSleepTcbId != m_lstSleep.end();
                             iteSleepTcbId++)
                        {
                            auto iteTcb = m_mapTcb.find((*iteSleepTcbId));
                            if (iteTcb != m_mapTcb.end())
                            {

                                dwDiff = CUtil::DiffTime((*iteTcb).second->GetSleepStartTime(), dwNow);
                                if (dwDiff >= (*iteTcb).second->GetSleepTime())
                                {
                                       AsTaskStart((*iteSleepTcbId));
                                       lstDeleteid.push_back(*iteSleepTcbId);
                                }
                            }
                        }

                        for( int iId: lstDeleteid)
                        {
                            auto iteDel = std::find(m_lstSleep.begin(),
                                                    m_lstSleep.end(),
                                                    iId);
                            if (iteDel != m_lstSleep.end())
                            {
    //DB_PRINT(_T("SleepErase [%d]  \n"), iId);
                               m_lstSleep.erase(iteDel);
                            }
                        }
                    }
                }
                //-----------
                if (m_bPauseCycle)
                {
                    continue;
                }

                std::vector<std::vector<std::shared_ptr<TCB>>::iterator> lstErase;
                std::vector<int> lstEraseId;

                for( iteMap = m_mapTimerGroup.begin();
                     iteMap != m_mapTimerGroup.end();
                     iteMap++)
                {
                    dwDiff = CUtil::DiffTime(iteMap->second.dwStart, dwNow);


        //DB_PRINT(_T("Diff %d %d  \n"), dwDiff, iteMap->first);

                    std::vector<std::shared_ptr<TCB>>* pVec;


                    std::shared_ptr<TCB> pTcb;

                    pVec = &iteMap->second.lstTcb;


                    if (dwDiff >= iteMap->first)
                    {
      //DB_PRINT_TIME(_T("Diff Start %d %d  \n"), dwDiff, iteMap->first);

                        if (bMesureExecTime)
                        {
                            for(auto& iteExec: iteMap->second.mapExec)
                            {
                                iteExec.second.dSumOld = iteExec.second.dSum;
                                iteExec.second.dSum = 0.0;
                            }
                        }


                        for(iteTcb  = pVec->begin();
                            iteTcb != pVec->end();
                            iteTcb++)
                        {
                            pTcb = (*iteTcb);
                            TASK_STS eTslSts;
                            eTslSts = pTcb->GetStatus();
                            if((eTslSts == TS_WAIT) ||
                               (eTslSts == TS_WAIT_CYCLE))
                            {
                                if (m_mapScriptGroup[pTcb->GetThreadType()])
                                {
                                    m_mapScriptGroup[pTcb->GetThreadType()]->StartTcb(*iteTcb);
                                }
        //DB_PRINT(_T("Req Tcb  %I64x  TS_INTRRUPT\n"), pTcb);

                                //pTcb->eSts = TS_READY;
                            }
                            else if(pTcb->GetCmd() == TCMD_DELETE)
                            {
                                //削除
                                lstErase.push_back(iteTcb);
                                lstEraseId.push_back(pTcb->GetId());
                            }
                            else
                            {
        //DB_PRINT(_T("Req Tcb  %I64x  %d\n"), pTcb, pTcb->eSts);
                            }
                        }

                        if (!lstErase.empty())
                        {
                            for(auto ite2 : lstErase)
                            {
                                pVec->erase(ite2);
                            }
                        }

                        iteMap->second.dwStart = dwNow;
                    }
                }

                if (!lstEraseId.empty())
                {
    #if 0
       DB_PRINT(_T("StartErase %d \n"), lstEraseId.size());
    #endif
                    std::lock_guard<std::mutex> lock(m_Mtx);


                    //auto ite2
                        //
                    for( int id : lstEraseId)
                    {
    #if 0
       DB_PRINT(_T("StartErase2 iD = %d \n"),id);
    #endif
                        auto iteMap = m_mapTcb.find(id);

                        if (iteMap != m_mapTcb.end())
                        {

    #if 0
       DB_PRINT(_T("Erase Tcb  %I64x  Id%d \n"), &iteMap->second, iteMap->second->GetId());
    #endif
                            m_mapTcb.erase(iteMap);
                        }
                    }
                }
            }
        }
DB_PRINT_TIME(_T("_CycleTask End 1\n"));

        int iThreadEnd = SizeToInt(m_mapScriptGroup.size());
        E_THREAD_TYPE eType;
        for(int iType = TH_GROUP_1; 
                iType  < iThreadEnd;
                iType++)
        {
            eType = static_cast<E_THREAD_TYPE>(iType);
            if(m_mapScriptGroup[iType])
            {
                m_mapScriptGroup[iType]->End();
            }
        }
DB_PRINT_TIME(_T("_CycleTask End 2\n"));

        //ContextThreadの終了を監視
        //TODO: 待ち時間パラメータ化
        bool ret;
        const auto rel_time = std::chrono::seconds(12);
        ret = m_cvContextThread.wait_for(lk, rel_time,  [=]
        {
            return (m_iExecThreadCount == 0);
        });

        for(int iType = TH_GROUP_1; 
                iType  < iThreadEnd;
                iType++)
        {
            eType = static_cast<E_THREAD_TYPE>(iType);
            if(m_mapScriptGroup[iType])
            {
                m_mapScriptGroup[iType]->EndThreadWait();
            }
        }

        
DB_PRINT_TIME(_T("_CycleTask End 3\n"));
        if (!ret)
        {
            STD_DBG(_T("Time out 1\n"));
        }

        //画面を閉じる
        THIS_APP->CloseAllExecViewFromThread();
        _DeleteExecInstacne();

    }catch(...)
    {
DB_PRINT_TIME(_T("_CycleTask End 4\n"));

       DB_PRINT(_T("Error \n"));

    }
DB_PRINT_TIME(_T("_CycleTask End 5\n"));
    m_bExitCycle = true;        
    m_cvEnd.notify_all();
    return true;
}


/**
 * @brief   実行インスタンス消去
 * @param   [in] eThreadType
 * @retval  true 成功
 * @note	
 */
void CExecCtrl::_DeleteExecInstacne()
{
}

EXEC_COMMON::E_EXEC_STS CExecCtrl::GetExecStatus()
{
    return  m_eSts;
}

void  CExecCtrl::_SetExecStatus(EXEC_COMMON::E_EXEC_STS sts)
{
    CWnd* pWnd;
    pWnd = AfxGetApp()->m_pMainWnd;

    m_eSts = sts;
    CDrawingScriptBase* pBase = GetExecScript();

    ::PostMessage(pWnd->m_hWnd, 
        WM_MODE_CHANGE, 
        (WPARAM)m_eSts, 
        (LPARAM)pBase);
}

void CExecCtrl::AsYield(asIScriptContext *ctx)
{
    CExecContextThread* pThread;

    pThread = _GetExecContextThread(ctx);

    if (pThread)
    {
        pThread->YieldCurrent();
    }

}

CExecContextThread* CExecCtrl::_GetExecContextThread(asIScriptContext *ctx)
{
    for (auto ite  = m_mapScriptGroup.begin();
              ite != m_mapScriptGroup.end();
              ite++)
    {
         if ((*ite).second->IsCurrrentContext(ctx))
         {
             return (*ite).second.get();
         }
    }
    return NULL;
}


TCB* CExecCtrl::GetCurrentTcb(asIScriptContext *ctx)
{

    size_t iSize = m_mapScriptGroup.size();
    TCB* pTcb = NULL;

    for (auto ite  = m_mapScriptGroup.begin();
              ite != m_mapScriptGroup.end();
              ite++)
    {
        pTcb = (*ite).second->GetCurrentTcb();
        if (pTcb)
        {

            if (pTcb->GetCurrentContext() == ctx)
            {
                return pTcb;
            }
        }
    }

    return NULL;
}

void CExecCtrl::SetDebugMode(bool bDebug)
{
    for (auto ite  = m_mapScriptGroup.begin();
              ite != m_mapScriptGroup.end();
              ite++)
    {
         (*ite).second->SetDebugMode(bDebug);
    }
}
/////////////////////////////////////////////////

/////////////////////////////////////////////////
/*
bool CExecCtrl::Continue()
{
    if (m_eBreakPointThread == EXEC_COMMON::TH_NONE)
    {
        return false;
    }

    bool bRet;
    bRet = m_mapScriptGroup[m_eBreakPointThread]->Continue();
    return bRet;
}
*/

bool CExecCtrl::StepOut()
{
    if (m_eBreakPointThread == EXEC_COMMON::TH_NONE)
    {
        return false;
    }

    bool bRet = false;
    if(m_mapScriptGroup[m_eBreakPointThread])
    {
        bRet = m_mapScriptGroup[m_eBreakPointThread]->StepOut();
    }
    return bRet;
}

bool CExecCtrl::StepInto()
{
    if (m_eBreakPointThread == EXEC_COMMON::TH_NONE)
    {
        return false;
    }

    bool bRet = false;
    if(m_mapScriptGroup[m_eBreakPointThread])
    {
        bRet = m_mapScriptGroup[m_eBreakPointThread]->StepInto();
    }
    return bRet;
}

bool CExecCtrl::StepOver()
{
    if (m_eBreakPointThread == EXEC_COMMON::TH_NONE)
    {
        return false;
    }

    bool bRet = false;
    if(m_mapScriptGroup[m_eBreakPointThread])
    {
        bRet = m_mapScriptGroup[m_eBreakPointThread]->StepOver();
    }
    return bRet;
}

bool CExecCtrl::SetExecDef( std::shared_ptr<CObjectDef> pDef)
{
    m_psExecDef = pDef;
    return true;
}

/*
bool CExecCtrl::SetExecScript(std::unique_ptr<CDrawingScriptBase>* ppScript)
{
    STD_ASSERT(!m_psExecScript);
    STD_ASSERT(!m_psExecDef);
    m_psExecScript = std::move (*ppScript);
    return true;
}

bool CExecCtrl::SetExecScript(std::unique_ptr<CDrawingElement>* ppScript)
{
    STD_ASSERT(!m_psExecScript);
    STD_ASSERT(!m_psExecDef);
    m_psExecScript = std::move (*ppScript);
    return true;
}
*/


CDrawingScriptBase* CExecCtrl::GetExecScript()
{
    CDrawingScriptBase* pBase = NULL;

    if (m_psExecScript)
    {
        pBase = m_psExecScript.get();
    }
    else if (m_psExecDef)
    {
        CPartsDef* pParts;
        pParts = dynamic_cast<CPartsDef*>(m_psExecDef.get());
        if (pParts)
        {
            pBase = pParts->GetDrawingParts().get();
        }
    }
    return pBase;
}

std::weak_ptr<CObjectDef> CExecCtrl::GetExecDef()
{
    return m_psExecDef;
}

void CExecCtrl::_ClearObject()
{
    if (m_psExecScript)
    {
        m_psExecScript.reset();
    }
    else if (m_psExecDef)
    {
        m_psExecDef.reset();
    }
}


//!< 停止問い合わせ
bool CExecCtrl::PauseScripts()
{

    return false;
}

bool CExecCtrl::InitScript()
{   
    m_lstLoop.clear();
    return false;
}

bool CExecCtrl::IsEndExec()
{
    if (m_bExitCycle)
    {
        return true;
    }
    return false;
}

bool CExecCtrl::StartScript()
{
    // スクリプト実行
    // 事前にAddScriptLoopをしたものを実行する

    //!< 停止問い合わせ
    if(IsEndExec())
    {
        Run();
    }

    for(int iTaskno: m_lstLoop)
    {
        AsTaskStart(iTaskno);
    }
    return true;
}

bool CExecCtrl::AbortScripts()
{
    //bool bRet;
    //!< 全終了
    Abort(EXEC_COMMON::TH_ALL);
    return true;
}


    void  CExecCtrl::AddExecTime(EXEC_COMMON::E_THREAD_TYPE eThreadType, DWORD dwTime, double dAdd)
    {
       auto& ite = m_mapTimerGroup.find(dwTime);
       if (ite != m_mapTimerGroup.end())
       {
           auto iteExec = ite->second.mapExec.find(eThreadType);
           if (iteExec != ite->second.mapExec.end())
           {
               iteExec->second.dSum += dAdd;
           }
           else
           {
               EXEC_TIME execTime;
               execTime.dSum = dAdd;
               ite->second.mapExec[eThreadType] = execTime;
           }
       }
    }

    double CExecCtrl::GetExecTime(EXEC_COMMON::E_THREAD_TYPE eThreadType, DWORD dwTime)
    {
       auto& ite = m_mapTimerGroup.find(dwTime);
       if (ite != m_mapTimerGroup.end())
       {
           auto iteExec = ite->second.mapExec.find(eThreadType);
           if (iteExec != ite->second.mapExec.end())
           {
               return iteExec->second.dSumOld;
           }
       }
       return 0;
    }


void CExecCtrl::NotifyBreakpoint(const std::string& strFileName, int iLineNo)
{
    //CModule * pModule = pObj->GetModule();
    //std::wstring strLine;
    //strLine = pModule->GetLine(strFile.c_str(),lineNbr - 1);
    //std::wstring strFileW = CUtil::CharToString(strFileName.c_str());
    //TRACE(_T(">> Break %s(%d) \n"), strFileW.c_str(), iLineNo);
    _SetExecStatus(EXEC_COMMON::EX_BREAK);
}


CURRENT_BREAK_CONTEXT* CExecCtrl::GetCurrentBreakData()
{
    CURRENT_BREAK_CONTEXT* pRet = NULL;
    if (m_eBreakPointThread == EXEC_COMMON::TH_NONE)
    {
        return pRet;
    }

    if(m_mapScriptGroup[m_eBreakPointThread])
    {
        pRet = m_mapScriptGroup[m_eBreakPointThread]->GetCurrentBreakData();
    }
    return pRet;
}


void CExecCtrl::PrintTcb(TCB* pTcb, const TCHAR* pStr)
{
    static std::map<int, std::wstring> lstGroupName = { 
        {TS_WAIT        ,_T("WAIT")},
        {TS_RUN         ,_T("RUN")},
        {TS_INTRRUPT    ,_T("INTRRUPT")},
        {TS_YIELD       ,_T("YIELD")},
        {TS_SLEEP       ,_T("SLEEP")},
        {TS_SUSPEND     ,_T("SUSPEND")},
        {TS_EXT             ,_T("EXT")}
    };



    /*
    if (pTcb)
    {
        std::wstring strFuncName;

        if (pTcb->pAsFunc)
        {
            strFuncName = CharToString(pTcb->pAsFunc->GetName());
        }

        if (pTcb->eSts != TS_SUSPEND)
        {
            DB_PRINT2(_T("%sid:%d pri:%d sts:%s funcName%s \n"),
                         pStr,   
                         pTcb->iId,
                         pTcb->iPri,
                         lstGroupName[pTcb->eSts].c_str(),
                         strFuncName.c_str());
        }
        else
        {
            DB_PRINT2(_T("%sid:%d pri:%d sts:%s:%s funcName%s \n"),
                         pStr,   
                         pTcb->iId,
                         pTcb->iPri,
                         lstGroupName[pTcb->eSts].c_str(),
                         lstReasonName[pTcb->eSuspendReason].c_str(),
                         strFuncName.c_str());
        }

    }
    else
    {
        DB_PRINT2(_T("NON TCB\n"));
    }
    */
}

void CExecCtrl::PrintTask(const TCHAR* pStr)
{
    std::vector<std::wstring> lstGroupName = {
         _T("GROUP1"), _T("GROUP2"),
         _T("GROUP3"), _T("GROUP4"),
         _T("GROUP5"), _T("FIELD1"),
         _T("FIELD2"), _T("FIELD3"),
         _T("FIELD4"), _T("FIELD5"),
    };
    std::lock_guard<std::mutex> lock(m_Mtx);

    CString strOut;
    int iGroup = 0;
    DB_PRINT(_T("---PrintTask %s----\n"), pStr);

    CExecContextThread* ptr;
    //std::vector<std::unique_ptr <CExecContextThread>>::iterator ite;
    for( auto ite  =  m_mapScriptGroup.begin();
              ite !=  m_mapScriptGroup.end();
              ite++)
    {
        ptr = (*ite).second.get();
        DB_PRINT(_T("%s\n"), lstGroupName[iGroup].c_str());

        auto pInsrList = ptr->GetListInsertTcb();

        if (!pInsrList->empty())
        {
            DB_PRINT(_T("  ---m_lstReq----\n"));

            std::list<  std::shared_ptr<TCB> >::iterator iteList;
            for(iteList  = pInsrList->begin();
                iteList != pInsrList->end();
                iteList++)
            {
                TCB* pTcb = (*iteList).get();
                PrintTcb(pTcb, _T("  "));
            }
        }
        iGroup++;
    }

    DB_PRINT(_T("  ---m_mapTimerGroup----\n"));
    if (!m_mapTimerGroup.empty())
    {
        int iPri = 0;

        for( auto iteTimer: m_mapTimerGroup)
        {
            DB_PRINT(_T("Time %d\n"), iteTimer.first);
            for(auto iteTcb: iteTimer.second.lstTcb)
            {
                PrintTcb(iteTcb.get(), _T("  "));
            }
        }
    }
}





