/**
 * @brief			CExecCtrlヘッダーファイル
 * @file			CExecCtrl.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef _EXEC_CTRL_H_
#define _EXEC_CTRL_H_


/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
//#include "TCB.h"
#include <mutex>
#include <thread>
#include <condition_variable>
#include "ExecCommon.h"
#include "Utility/CUtility.h"


/*---------------------------------------------------*/
/*  class                                            */
/*---------------------------------------------------*/
class CScriptObject;
class CModule;
class CExecContextThread;
class TCB;
class CIdObj;
class CObjectDef;
class CFieldDef;
class CPartsDef;
class CDrawingScriptBase;
class CDrawingParts;
class CDrawingField;
class CDrawingElement;
class asIScriptEngine;
class asIThreadManager;
class asIScriptContext;
class CURRENT_BREAK_CONTEXT;

struct EXEC_TIME
{
    double dSum    = 0.0;  //実行時間積算
    double dSumOld = 0.0;  //前回の実行時間積算
};

struct TIMER_GROUP
{
    //DWORD iTimerSet;    //周期タイマー値
    DWORD dwStart;        //開始時刻
    std::map<int, EXEC_TIME> mapExec;
    std::vector<std::shared_ptr<TCB>> lstTcb;
};


#if 0

//-----------------------
// 実行ステータス
//-----------------------
enum E_EXEC_STATE
{
    STATE_NONE,
    STATE_ABORTNG,
    STATE_BREAK,
    STATE_END,
    STATE_RUN,
    STATE_STEP,
    STATE_PAUSE,
};

//-----------------------
// 要求コマンド
//-----------------------
enum E_CMD_STATE
{
    CMD_NONE,
    CMD_ABORT,
    CMD_RUN,
    CMD_PAUSE,
    CMD_BREAK,

};



//-----------------------
// 実行グループ
//-----------------------
struct EXEC_GROUP
{
    EXEC_COMMON::E_THREAD_TYPE   eThreadType;       // 実行スレッド種別
    int             iTime;             // 1ループの実行時間(msec)
    int             iExecTime;         // 実行時間(msec)
    int             iPri;              // スレッドプライオリティ
    DWORD           dwTime;            // 前回の実行時刻
    E_EXEC_STATE    eState;            // 実行ステート
    E_EXEC_STATE    eBeforeState;      // 前回の実行ステート
    E_CMD_STATE     eCmdState;         // 要求ステート
    int             iExecContextNo;    // 実行コンテキスト番号

    boost::thread*  pThread;                   // 実行スレッド
    std::vector<CScriptContext*>  lstContext;  // 実行コンテキスト
    int             iBreakCnt;          //現在ブレークポイント発生中の数
};
#endif

// Mapに変更があったことを分かる様にする
template<class Key, class T>
class COUNT_MAP
{
public:

    COUNT_MAP(){;}
    virtual ~COUNT_MAP(){;}

    void clear()
    { 
        mapIn.clear(); 
        iCnt++;
    }

    void erase(typename std::map<Key,T>::iterator pos)
    {
        iCnt++;
        mapIn.erase(pos);
    }
    
    typename std::map<Key,T>::iterator find(Key k) 
    {
        return mapIn.find(k);
    }
    
    typename std::map<Key,T>::iterator end()
    {
        return mapIn.end();
    }
    
    typename std::map<Key,T>::iterator begin()
    {
        return mapIn.begin();
    }
    
    typename std::map<Key,T>::iterator insert( typename std::map<Key,T>::iterator pos, 
        const std::pair<Key,T> &val )
    { 
        iCnt++;
        return mapIn.insert(pos, val);
    }
    
    size_t count() const
    {
        return mapIn.count();
    } 

    T& operator[] (const Key& k)
    {
        static T t;
        typename std::map<Key,T>::iterator ite;
        ite = mapIn.find(k);
        if (ite == mapIn.end())
        {
            iCnt++;
            ite = mapIn.insert(std::make_pair(k, T())).first;
        }
        return ite->second;
        //return mapIn[k];
    }

    int GetChgCnt() const{return iCnt;}

private:
    std::map<Key, T> mapIn;
    int iCnt = 0;
};

class CExecCtrl
{
public:


public:
    CExecCtrl();
    virtual ~CExecCtrl();

    //static CExecCtrl* GetInstance();
    //static void DeleteInstance();

    CExecCtrl(const CExecCtrl& r) = delete;			//コピーコンストラクタも delete 指定
	CExecCtrl& operator=(const CExecCtrl& r) = delete;	//代入演算子も delete 指定
 

    //-----------------
    // 構築
    //-----------------


    //!< 初期化
    bool Init();

    //!< 一時停止
    bool Pause();

    //!< 再開
    bool Resume();

    //!< 終了
    bool Abort(EXEC_COMMON::E_THREAD_TYPE eThreadType = EXEC_COMMON::TH_ALL);

    //!< 全中断
    bool BreakAll();

    //ブレークポイント発生スレッド設定
    bool SetBreakPointThread(EXEC_COMMON::E_THREAD_TYPE eThreadType);

    //!< 中断
    bool Break(EXEC_COMMON::E_THREAD_TYPE eThreadType);

    //!< 停止問い合わせ
    bool IsEndExec();

    EXEC_COMMON::E_EXEC_STS GetExecStatus();


    //int GetEnginNum();

    //asIScriptEngine* GetEngine(int iNum);

    std::unique_ptr<CModule> CreateModule(CObjectDef* pDef);

    //std::unique_ptr<CScriptObject> CreateScriptObject(EXEC_COMMON::E_THREAD_TYPE = EXEC_COMMON::TH_GROUP_1);

    //asIScriptEngine* GetEngine(EXEC_COMMON::E_THREAD_TYPE eThread = EXEC_COMMON::TH_ALL);
    asIScriptContext* GetContext(EXEC_COMMON::E_THREAD_TYPE eThread);
    asIScriptContext* GetNonThreadContext(EXEC_COMMON::E_THREAD_TYPE eThread);


    bool Run(EXEC_COMMON::E_THREAD_TYPE eThreadType = EXEC_COMMON::TH_ALL);

    void AddThreadCount();
    void SubThreadCount();

    //--------------
    //CDrawingScriptBase関連
    //--------------
    bool InitScript();
    bool SetExecDef( std::shared_ptr<CObjectDef> pDef);
    //bool SetExecDef( std::shared_ptr<CPartsDef> pDef);
    //bool SetExecScript(std::unique_ptr<CDrawingScriptBase>* ppScript);
    //bool SetExecScript(std::unique_ptr<CDrawingElement>* ppScript);

    CDrawingScriptBase* GetExecScript();
    std::weak_ptr<CObjectDef> GetExecDef();

    bool AddScriptLoop(CDrawingScriptBase* pBase);
    bool StartScript();
    bool AbortScripts();
    bool PauseScripts();
    //--------------


    // FOR AS
    int  AsTaskCreate(TCB* pExec);
    bool AsTaskStart(int iTaskId);
    bool AsTaskSetPriority(int iTaskId, int Pri);
    bool AsTaskSetCycle(int iTaskId, DWORD dwCycleTime);
    bool AsTaskSetName(int iTaskId, std::wstring strName);

    bool AsTaskExit();
    bool AsTaskSleep(DWORD dwTimeOut);
    bool AsTaskAbort(int iTaskId);
    bool AsTaskTerminate(int iTaskId);
    bool AsTaskSuspend(int iTaskId);
    bool AsTaskSuspend();
    bool AsTaskWakeUp(int iTaskId);
    
    bool AsTaskEndCycle();
    bool AsTaskIsExist(int iTaskId);
    int  AsTaskStatus(int iTaskId);

    //bool TaskCommand(int iTaskId, EXEC_CMD eCmd);



    //int  AsGetTaskId(std::string strName);
    int  AsTaskGetId(std::wstring strName);

    void AsYield(asIScriptContext *ctx);

    //TCB* GetCurrentTcb(asIScriptContext *ctx);
    void SetSleep(int iTaskId);

    void DelTsk(int iTaskId);

    void PrintTask(const TCHAR* pStr);
    void PrintTcb(TCB* pTcb, const TCHAR* pStr);
    
    TCB* GetCurrentTcb(asIScriptContext *ctx);

    void SetDebugMode(bool bDebug);

    bool GetTcbMap(COUNT_MAP<int, std::shared_ptr<TCB> >* pMap);

    int GetTcbMapCount() const;

    //----------------------
    //    DEBUG
    //----------------------
    //bool Continue();
    bool StepOut();
    bool StepInto();
    bool StepOver();
    void NotifyBreakpoint(const std::string& strFileName, int iLineNo);
    CURRENT_BREAK_CONTEXT* GetCurrentBreakData();
    //----------------------

    bool IsMeasureExecTime() {return bMesureExecTime;}

    void   AddExecTime(EXEC_COMMON::E_THREAD_TYPE eThreadType, DWORD dwTime, double dAdd);
    double GetExecTime(EXEC_COMMON::E_THREAD_TYPE eThreadType, DWORD dwTime);


    DWORD  GetDispCycleTime(){return m_dwDispCycleTime;}
    DWORD  GetPainCycleTime(){return m_dwPainCycleTime;}

    std::mutex m_Mtx;
    std::mutex mtxEnd;
    std::mutex m_MtxSleepList;
    std::mutex m_MtxThreaCount;

protected:

    bool _CycleTask();
    TCB* _FindTsk(int iTaskId);
    bool _StartCycleTask();
    bool _EndCycleTask();
    bool _DelTsk(int iTaskId);
    void _DeleteExecInstacne();
    void _ClearObject();

    int _CreateNewId();
    void  _SetExecStatus(EXEC_COMMON::E_EXEC_STS sts);
    void _DeleteAllQue();


    CExecContextThread* CExecCtrl::_GetExecContextThread(asIScriptContext *ctx);

    static CExecCtrl* m_pCtrl;

    std::vector<int>    m_lstLoop;


    EXEC_COMMON::E_EXEC_STS          m_eSts = EXEC_COMMON::EX_EDIT;
    //asIThreadManager* m_pThreadManager;
    //std::vector<asIScriptEngine* > m_lstEngine;


    std::mutex m_MtxCycle; 
    std::mutex m_MtxDeleteList; 
    std::condition_variable m_condCycle;
    std::condition_variable m_cvEnd;
    std::condition_variable m_cvContextThread;
    
    bool m_bPauseCycle = false;
    bool m_bExitCycle = true;


    bool m_bStopCycle;
   std::thread* m_psCycleThread;

    std::map<int, std::unique_ptr <CExecContextThread>>     m_mapScriptGroup;
    std::map<DWORD, TIMER_GROUP>       m_mapTimerGroup;

    //std::map<asIScriptObject*, CScriptObject*>       m_mapScriptObject;

    COUNT_MAP<int, std::shared_ptr<TCB> > m_mapTcb;
    std::vector<int> m_lstSleep;
    //std::deque<int>  m_lstDelete;
    SYNC_BUFFER<int> m_lstReqSleep;
    SYNC_BUFFER<int> m_lstDelete;


    std::shared_ptr<CIdObj> m_psIdObj;
    int m_iCreateId;

    bool m_bOneEngine;
    //TCBの実体はこのクラスで管理

    //!< ブレークポイント発生スレッド
    EXEC_COMMON::E_THREAD_TYPE m_eBreakPointThread = EXEC_COMMON::TH_NONE;

    //実行中のオブジェクト定義
    std::shared_ptr<CObjectDef> m_psExecDef;

    //実行中のオブジェクト(廃止予定)
    std::unique_ptr<CDrawingScriptBase> m_psExecScript;

    //実行中のスレッド数
    int m_iExecThreadCount = 0;

    //実行時間の計測
    bool bMesureExecTime = true;


    DWORD m_dwDispCycleTime = 100;

    DWORD m_dwPainCycleTime = 500;

};

#endif //_EXEC_CTRL_H_
