#include "stdafx.h"
#include "./CModule.h"
#include "./CScriptEngine.h"
#include "./CScript.h"
#include "Utility/CUtility.h"
#include "System/CSystem.h"

#include "DefinitionObject/ObjectDef.h"
#include "CProjectCtrl.h"
#include "MockSketch.h"
#include "MainFrm.h"

#include <string> 
#include <angelscript.h>



#include <boost/assign.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/tokenizer.hpp>

#ifdef _DEBUG
#undef new 
#define new DEBUG_NEW
#endif 


CModule::CModule(CObjectDef* pDef):
m_psModule          (0),
m_pProj             (NULL),
m_pDef              (pDef)

{
    //const std::string &strModuleName
    //m_psModule = pEngine->GetModule(strModuleName.c_str(), asGM_ALWAYS_CREATE);
}

CModule::~CModule()
{
    if (m_psModule)
    {
        asIScriptEngine* pEngine = m_psModule->GetEngine();
        pEngine->DiscardModule((const char*)m_psModule);
        m_psModule = NULL;
    }
}
    
/*
bool CModule::LoadScript(const TCHAR *strSectionW)
{
    namespace fs = boost::filesystem;

    std::wstring strFileName = strSectionW;
    strFileName += _T(".as");

    fs::wpath path;

    path = fs::current_path() / strFileName; 


    if (!fs::exists(path))
    {
        return false;
    }

    std::string strFile;
    try
    {
        fs::ifstream inFs;
        inFs.open(path);
        
        if( inFs.fail() )
        {
            return false;
        }

        strFile.assign( std::istreambuf_iterator<char>(inFs),
                                std::istreambuf_iterator<char>());

        inFs.close();
    }
    catch(...)
    {
        return false;
    }

    bool bRet;

    std::string strSection = CUtil::StringToChar(strSectionW);

    bRet = LoadData(strSection.c_str(), strFile);

    return bRet;
}



bool CModule::LoadData(const char *strSection, const std::string& strData)
{
    auto iteMap = m_mapFile.find(strSection);

    if (iteMap != m_mapFile.end())
    {
        return false;
    }

    if (strData.length() ==  0)
    {
        return false;
    }


    int iRet;
	iRet = m_psModule->AddScriptSection(strSection, 
                                        strData.c_str(), 
                                        strData.length(), 0);

    if (iRet != 0)
    {
        return false;
    }
    FILE_DATA fileData;
        
    std::wstring strIn = CUtil::CharToString(strData.c_str());

    CUtil::Tokenize(&fileData.lstLineData, strIn);

    m_mapFile[strSection] = fileData;
    return true;
}

std::wstring  CModule::GetLine(const char *strSection, int iLineNo)
{
    std::wstring strRet;
    auto iteMap = m_mapFile.find(strSection);
    if (iteMap == m_mapFile.end())
    {
        return strRet;
    }
    FILE_DATA* pFileData;
    pFileData = &iteMap->second;

    if (pFileData->lstLineData.size() <= iLineNo)
    {
        return strRet;
    }

    return pFileData->lstLineData[iLineNo];
}
*/

/**
 *  @brief   コンパイル
 *  @param   [in] bDebug  デバッグモード
 *  @retval  true 成功
 *  @note   
 */
bool CModule::Compile(bool bDebug)
{
    if (m_pDef == NULL)
    {
        return false;
    }
    
    //コンパイル済みを設定
    m_eCompile = CS_ERROR;

    int iRet;
    asIScriptEngine* pEngine;

    m_pProj = m_pDef->GetProject();
    
    // モジュール名の設定
    // モジュール名としてユニークな名称を作成する。
    std::stringstream   strmName;
    int iNameID  = m_pProj->GetModuleId();
    StdString strProj = m_pProj->GetName();

    strmName << boost::format("Module_%s_%d") % strProj.c_str() % iNameID << std::endl;
    m_pProj->AddModuleId();



    pEngine = THIS_APP->GetScriptEngine()->GetEngine();

    if (m_psModule)
    {
        iRet = pEngine->DiscardModule(m_psModule->GetName());
        if (iRet < 0)
        {
            //モジュール削除失敗
            m_psModule = NULL;
            PrtintOut(WIN_BUILD, GET_STR( STR_MODULE_DEL_FAIL ));
            return false;
        }
        m_psModule = NULL;
    }

    m_mapScript.clear();

    m_psModule = pEngine->GetModule(strmName.str().c_str(), asGM_ALWAYS_CREATE);

    STD_ASSERT(m_psModule != NULL);


    StdString strDef;

    strDef = THIS_APP->GetObjectDefTabName(m_pDef->GetPartsId());


    //=====================================================
    CScriptData* pData;
    StdString  strSectionNameW;
    std::string strSectionNameA;

    m_pDef->CreateInitialScript();

    int iScriptNum = m_pDef->GetScriptNum();

    //ファイルに保存していない場合はここで保存
    for (int iCnt = 0; iCnt < iScriptNum; iCnt++)
    {
        pData = m_pDef->GetScriptInstance(iCnt);

        if (pData->GetName() == _T("_INITILIZE_SCRIPT_"))
        {
            continue;
        }
        pData->OverWrite();
    }


    for (int iCnt = 0; iCnt < iScriptNum; iCnt++)
    {
        pData = m_pDef->GetScriptInstance(iCnt);

        //この名称からEditorのタブを探す
        //(ファイル名)パーツ名
        strSectionNameW = CUtil::StrFormat(_T("%s(%s)"),pData->GetName().c_str(),
                                                        strDef.c_str());
        strSectionNameA = CUtil::StringToChar(strSectionNameW);
        

        std::shared_ptr<std::string> pStrScript;
        pStrScript =  pData->GetScriptStringInstance().lock();

        STD_ASSERT(pStrScript);
        iRet = m_psModule->AddScriptSection( strSectionNameA.c_str(),
                                             pStrScript->c_str(),
                                             pStrScript->length());

        PrtintOut(WIN_BUILD, GET_STR(STR_MODULE_ADD), strSectionNameW.c_str());
        if (iRet < 0)
        {
            //モジュールを解放
            iRet = pEngine->DiscardModule(m_psModule->GetName());
            m_psModule = NULL;
            PrtintOut(WIN_BUILD, GET_STR(STR_MODULE_ADD_FAIL), strSectionNameW.c_str());
            return false;
        }
        //参照元を設定
        m_mapScript[strSectionNameA] = pData;

    }

    PrtintOut(WIN_BUILD, GET_STR(STR_MODULE_COMPILE), 
        m_pDef->GetName().c_str());
    //=====================================================

    iRet = m_psModule->Build();
    if (iRet < 0)
    {
        //モジュールを解放
        iRet = pEngine->DiscardModule(m_psModule->GetName());
        m_psModule = NULL;
        return false;
    }


    using namespace EXEC_COMMON;

    //-----------------------------------------------------------------------------------

    //クラスを取得
    asIObjectType *type = pEngine->GetObjectTypeById(m_psModule->GetTypeIdByDecl("ExecClass"));

    if (type)
    {
        m_pExecObjectFactory = type->GetFactoryByDecl("ExecClass @ExecClass()");
    }

    if (!m_pExecObjectFactory)
    {
        //コンストラクタなし
        //TODO:文字追加 コンストラクタが設定されていません
        return false;
    }

    if (bDebug)
    {
        m_eCompile = CS_SUCCESS_D;
    }
    else
    {
        m_eCompile = CS_SUCCESS;
    }
    return true;
}


bool CModule::SetBreakPoint(const char* strFileName, int iLineNo)
{
    BreakPoint bp(strFileName, iLineNo, false);
    m_breakPoints.push_back(bp);
    return true;
}

bool CModule::DelBreakPoint(const char* strFileName, int iLineNo)
{
    auto ite = boost::find_if( m_breakPoints, [=] (BreakPoint& bp)
    {
        if ((bp.name == strFileName) &&
            (bp.lineNbr == iLineNo))
        {
            return true;
        }
        return false;
    });

    if (ite != m_breakPoints.end())
    {
        m_breakPoints.erase(ite);
        return true;
    }
    return false;
}

std::vector<CModule::BreakPoint>* CModule::GetBreakPoints()
{
    return &m_breakPoints;
}

/**
 *  @brief  ブレークポイントの有無
 *  @param  [in] strSection セクション名
 *  @param  [in] iLine      行
 *  @retval true: break on
 *  @note
 */
bool CModule::IsBreakPoint(const char* strSection, int iLine)
{
    CScriptData* pData;
    std::map<std::string, CScriptData* >::iterator iteMap;
    if (strSection == NULL)
    {
        return false;
    }

    iteMap = m_mapScript.find(std::string(strSection));

    if (iteMap == m_mapScript.end())
    {
        return false;
    }

    pData = (iteMap->second);
    return pData->IsBreak(iLine);
}


//!< グローバル変数アドレス取得
void* CModule::GetGlobalVarAddress(int* pTypeId, const char * srtVar)
{
    void* pValAddress;
    const char* strGrobalVarName;
    const char* strNameSpace;
    bool bConst;
    int  iTypeId;

    int iVarIndex;
    iVarIndex = m_psModule->GetGlobalVarIndexByName(srtVar);
    if (iVarIndex < 0)
    {
        return NULL;
    }
    m_psModule->GetGlobalVar(iVarIndex, 
                                &strGrobalVarName, 
                                &strNameSpace, 
                                &iTypeId, 
                                &bConst); 
    pValAddress = m_psModule->GetAddressOfGlobalVar(iVarIndex);

    *pTypeId = iTypeId;
    return pValAddress;
}

COMPILE_STS CModule::GetCompileStatus() const
{
    STD_ASSERT(m_pDef);
    if (m_pDef == NULL)
    {
        return CS_NOT_FIND;
    }

    CScriptData* pData;
    int iScriptNum = m_pDef->GetScriptNum();

    for (int iCnt = 0; iCnt < iScriptNum; iCnt++)
    {
        pData = m_pDef->GetScriptInstance(iCnt);
        if (pData->GetName() == _T("_INITILIZE_SCRIPT_"))
        {
            continue;
        }
        if (pData->IsChange())
        {
            return CS_CHANGED;
        }
    }
    return m_eCompile;
}

