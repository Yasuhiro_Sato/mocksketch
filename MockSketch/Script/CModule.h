#include <vector>
#include <map>
#include <string>
#include "COMMON_HEAD.h"
#pragma once

class asIScriptEngine;
class asIScriptModule;
class asIScriptFunction;
class CProjectCtrl;
class CObjectDef;
class CScriptData;

class CModule
{
public:
    struct FILE_DATA
    {
        std::vector<std::wstring>         lstLineData;
    };

	struct BreakPoint
	{
		BreakPoint(std::string f, int n, bool _func) : name(f), lineNbr(n), func(_func), needsAdjusting(true) {}
		std::string name;
		int         lineNbr;
		bool        func;
		bool        needsAdjusting;
	};


    //CModule(const std::string &strModuleName, asIScriptEngine* pEngine);
    CModule(CObjectDef* pDef);

    virtual ~CModule();

    bool Build();

    //!< 解放
    bool Release();

    //!< ブレークポイントの有無
    bool IsBreakPoint(const char* strSection, int iLine);
    
    bool SetBreakPoint(const char* strFileName, int iLineNo);

    bool DelBreakPoint(const char* strFileName, int iLineNo);

    std::vector<CModule::BreakPoint>* GetBreakPoints();

    //!<  実行オブジェクトコンストラクタ取得
    asIScriptFunction* GetExecObjectConstructor(){return m_pExecObjectFactory;} 

    //!< グローバル変数アドレス取得
    void* GetGlobalVarAddress(int* pTypeId, const char * srtVar);

    COMPILE_STS GetCompileStatus() const;

    /*
    bool LoadScript(const wchar_t *strSectionW);

    bool LoadData(const char *strSection, const std::string& strData);

    std::wstring  GetLine(const char *strSection, int iLineNo);
    */

    bool Compile(bool bDebug);

protected:


public:

    //!< プロジェクト
    CProjectCtrl*                  m_pProj;

    //!< 定義データ
    CObjectDef*                    m_pDef;

    asIScriptModule*                m_psModule;

    std::map<std::string, CScriptData* > m_mapScript;

    //!< コンパイル済み
    COMPILE_STS                             m_eCompile;

    std::vector<BreakPoint> m_breakPoints;

    asIScriptFunction *m_pExecObjectFactory;

};