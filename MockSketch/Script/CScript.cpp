/**
 * @brief			CScript実装ファイル
 * @file		    CScript.cpp
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CScript.h"
#include "Utility/CUtility.h"
#include "DefinitionObject/CPartsDef.h"
#include "System/MOCK_ERROR.h"
#include "System/CSystem.h"
#include <conio.h>   // kbhit(), getch()
#include <angelscript.h>


/**
 * @brief   シリアル化(ロード)
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  なし
 * @note	
 */
template<class Archive>
void BREAK_POINT::load(Archive& ar, const unsigned int version)
{
    SERIALIZATION_INIT;
    SERIALIZATION_LOAD("Enable", bEnable);
    MOCK_EXCEPTION_FILE(e_file_read);
}

/**
 * @brief   シリアル化(セーブ)
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  なし
 * @note	
 */
template<class Archive>
void BREAK_POINT::save(Archive& ar, const unsigned int version) const
{
    SERIALIZATION_INIT;
    SERIALIZATION_SAVE("Enable", bEnable);
    MOCK_EXCEPTION_FILE(e_file_write);
}


/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CScriptData::CScriptData():
m_iLine         (0),
m_iCol          (0),
m_iCurLine      (0),
m_iChgCnt       (1),
m_bLockBreakSet (false),
m_bLockBreakGet (false)

{
}

/**
 *  @brief  コンストラクター.
 *  @param  [in] name   スクリプト名
 *  @param  [in] script スクリプト
 *  @retval なし     
 *  @note
 */
CScriptData::CScriptData(StdString name, std::string script):
m_iLine         (0),
m_iCol          (0),
m_iCurLine      (0),
m_iChgCnt       (1),
m_bLockBreakSet (false),
m_bLockBreakGet (false)
{
    m_strName      =  name;
    m_psStrScript = std::make_shared<std::string>(script);
}    


/**
 *  @brief  コピーコンストラクター.
 *  @param  [in] m
 *  @retval なし     
 *  @note
 */
CScriptData::CScriptData(const CScriptData& m)
{
    m_strName      =  m.m_strName;
    m_iLine        =  m.m_iLine;
    m_iCol         =  m.m_iCol;
    m_iCurLine     =  m.m_iCurLine;
    m_bSingleFile  =  m.m_bSingleFile;
    m_pathCurrent  =  m.m_pathCurrent;
    m_iChgCnt       = m.m_iChgCnt;


    if(m.m_psStrScript)
    {
        m_psStrScript = std::make_shared<std::string>();
        *m_psStrScript = *m.m_psStrScript;
    }

    m_bLockBreakSet = false;
    m_bLockBreakGet = false;
}

/**
 *  @brief  デストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CScriptData::~CScriptData()
{
}

/**
 *  @brief  変更の設定
 *  @param  なし
 *  @retval なし     
 *  @note   要保存(Save)判定
 */
void CScriptData::_AddChange()
{
    m_iChgCnt++;
}

/**
 *  @brief  変更の有無
 *  @param  なし
 *  @retval true: 要保存(Save)
 *  @note   
 */
bool CScriptData::IsChange() const
{
    if (m_iChgCnt == 0)
    {
        return false;
    }
    return true;
}



/**
 *  @brief  ファイルパス設定
 *  @param  なし
 *  @retval true: 成功
 *  @note   
 */
void CScriptData::SetCurrentPath(StdPath path)
{ 
    m_pathCurrent = path;
}

/**
 *  @brief  再読み込み
 *  @param  なし
 *  @retval true: 成功
 *  @note   
 */
bool CScriptData::Reload()
{
    bool bRet;
    bRet = LoadScript(m_pathCurrent);
    return bRet;
}

/**
 *  @brief  スクリプト名設定
 *  @param  [in] name クリプト名
 *  @retval true 成功
 *  @note
 */
bool CScriptData::SetName(StdString name)
{
    //TODO:スクリプト名チェック
    m_strName = name;
    return true;
}

/**
 *  @brief  スクリプト名設定
 *  @param  なし
 *  @retval スクリプト名
 *  @note
 */
StdString CScriptData::GetName()
{
    return m_strName;
}

/**  @brief  スクリプト設定
 *  @param  [in] strScript スクリプト
 *  @retval true 成功
 *  @note
 */
bool CScriptData::SetScriptString(std::string strScript)
{
    if(m_psStrScript.get() == NULL)
    {
        m_psStrScript = std::make_shared<std::string>();
    }

    *m_psStrScript = strScript;
    return true;
}

/**
 *  @brief  スクリプト取得
 *  @param  なし
 *  @retval スクリプト
 *  @note
 */
std::string CScriptData::GetScriptString()
{
    if(m_psStrScript.get() != NULL)
    {
        return *m_psStrScript;
    }
    std::shared_ptr<std::string> pStr;
    pStr = GetScriptStringInstance().lock();

    if( !pStr )
    {
        return std::string();
    }

    return *pStr;
}

/**
 *  @brief  スクリプトインスタンス
 *  @param  なし
 *  @retval スクリプト
 *  @note
 */
std::weak_ptr<std::string> CScriptData::GetScriptStringInstance()
{
    std::weak_ptr<std::string> pRet;
    if(m_psStrScript.get() == NULL)
    {
        if(!LoadScript(m_pathCurrent))
        {
            return pRet;
        }
    }
    return m_psStrScript;
}

/**
 *  @brief  スクリプトアンロード
 *  @param  なし
 *  @retval なし
 *  @note
 */
void CScriptData::UnlaodScript()
{
    m_psStrScript.reset();
}

/**
 *  @brief  ブレークポイント取得
 *  @param  [out] pMapBreak ブレークポイント
 *  @retval なし
 *  @note
 */
void CScriptData::GetBreakPoint(std::map<int, BREAK_POINT>* pMapBreak)
{
#ifdef FOR_DEBUG
    DB_PRINT(_T("CScriptData::GetBreakPoint %I64x\n"), this);
#endif
    *pMapBreak = m_mapBreak;
}

/**
 *  @brief  ブレークポイント消去
 *  @param  なし
 *  @retval ブレークポイント
 *  @note
 */
void CScriptData::ClearBreakPoint()
{
    m_mapBreak.clear();
}

/**
 *  @brief  ブレークポイント設定
 *  @param  [in] iLine
 *  @param  [in] breakPoint
 *  @retval なし
 *  @note
 */
void CScriptData::SetBreakPoint(int iLine, BREAK_POINT breakPoint)
{
    //boost::mutex::scoped_lock lock(m_Mtx);
    if (m_bLockBreakGet)
    {
        for(int iCnt = 0; iCnt < 100; iCnt++)
        {
            if (!m_bLockBreakGet)
            {
                break;
            }
            StdSleep(10);
        }
    }
#ifdef FOR_DEBUG
DB_PRINT(_T("SetBreakPoint[%d]_%s\n"), iLine, 
    (breakPoint.bEnable? _T("T"):_T("F")));
#endif
    m_bLockBreakSet = true;
    m_mapBreak[iLine] = breakPoint;
    m_bLockBreakSet = false;

}

/**
 *  @brief  ブレークポイント問い合わせ
 *  @param  [in] iLine
 *  @retval true: break on
 *  @note
 */
bool CScriptData::IsBreak(int iLine)
{
    //boost::mutex::scoped_lock lock(m_Mtx);
    if (m_bLockBreakSet)
    {
        for(int iCnt = 0; iCnt < 100; iCnt++)
        {
            if (!m_bLockBreakSet)
            {
                break;
            }
            StdSleep(10);
        }
    }

    if (m_bLockBreakGet)
    {
        for(int iCnt = 0; iCnt < 100; iCnt++)
        {
            if (!m_bLockBreakGet)
            {
                break;
            }
            StdSleep(10);
        }
    }

    m_bLockBreakGet = true;
    std::map<int, BREAK_POINT>::iterator iteMap;
    iteMap = m_mapBreak.find(iLine);
    if (iteMap == m_mapBreak.end())
    {
        m_bLockBreakGet = false;
        return false;
    }

    bool bEnable = iteMap->second.bEnable;
    m_bLockBreakGet = false;

    return bEnable;

}

/**
 *  @brief  カーソル取得
 *  @param  [out] pLine 行位置
 *  @param  [out] pCol  列位置
 *  @retval なし
 *  @note   カーソル位置保存用
 */
void CScriptData::GetCursor( int* pLine, int* pCol)
{
    *pLine = m_iLine;
    *pCol = m_iCol;
}

/**
 *  @brief  カーソル設定
 *  @param  [in] iLine 行位置
 *  @param  [in] iCol  列位置
 *  @retval なし
 *  @note   カーソル位置保存用
 */
void CScriptData::SetCursor( int iLine, int iColl)
{
    m_iLine = iLine;
    m_iCol  = iColl;
}

/**
 *  @brief  実行位置取得
 *  @param  なし
 *  @retval 実行位置（行）
 *  @note   
 */
int CScriptData::GetCurLine()
{
    return m_iCurLine;
}

/**
 *  @brief  実行行設定
 *  @param  [in] iLine 実行位置（行）
 *  @retval なし
 *  @note   
 */
void CScriptData::SetCurLine( int iLine)
{
    m_iCurLine = iLine;
}

/**
 *  @brief  データクリア
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CScriptData::ClearData()
{
    m_mapBreak.clear();
    m_iCurLine = 0;
    m_iCol     = 0;
    m_iCurLine = 0;

    if(m_psStrScript)
    {
        *m_psStrScript = "";
    }

}

/**
 * @brief   スクリプト読み込み
 * @param   [in] path ファイルパス
 * @retval  true:成功
 * @retval  なし
 * @note	
 */
bool CScriptData::LoadScript(StdPath path)
{
    namespace fs = boost::filesystem;

    if (!fs::exists(path))
    {
        STD_DBG(_T("LoadScript(%s) is not exist."), path.c_str());
        return false;
    }

    try
    {
        fs::ifstream inFs;
        inFs.open(path);
        
        if( inFs.fail() )
        {
            return false;
        }



        //std::streamoff

        /*
        unsigned int eofPos;
        unsigned int begPos;
        unsigned int size;

        inFs.seekg(0, fs::fstream::end);
        eofPos = static_cast<unsigned int>(inFs.tellg());
        inFs.clear();
        inFs.seekg(0, fs::fstream::beg);
        begPos = static_cast<unsigned int>(inFs.tellg());
        size = eofPos - begPos;

        if (size == 0)
        {
            STD_DBG(_T("LoadScript(%s) size zero."), path.c_str());
            return false;
        }
        */
        if (m_psStrScript.get() == NULL)
        {
            m_psStrScript = std::make_shared<std::string>();
        }

        std::vector<char> buf;
        std::string str;

        m_psStrScript->assign( std::istreambuf_iterator<char>(inFs),
                               std::istreambuf_iterator<char>());

        inFs.close();
    }
    catch(...)
    {
        return false;
    }
    return true;
}

/**
 * @brief   スクリプト書き込み
 * @param   [in] path ファイルパス
 * @retval  true:成功
 * @note	
 */
bool CScriptData::SaveScript(StdPath path) const
{
    namespace fs = boost::filesystem;
    try
    {
		if (m_psStrScript)
		{
			fs::ofstream outFs(path);
			outFs << *m_psStrScript;
			outFs.close();
		}

        m_iChgCnt = 0;
    }
    catch(...)
    {
        return false;
    }
    return true;
}

/**
 * @brief   スクリプト上書き保存
 * @param   [in] bForce 強制上書き
 * @retval  true:成功
 * @note	
 */
bool CScriptData::OverWrite(bool bForce) const
{
    StdPath path = GetCurrentPath();

    bool bRet = true;

    if(IsChange())
    {
        bForce = true;
    }

    if (bForce)
    {
        bRet = SaveScript(path);

        if(bRet)
        {
            //TODO:暫定処置
            //ブレークポイントの保存場所を
            m_iChgCnt = 0;
        }
    }
    return bRet;
}


/**
 * @brief   シリアル化(ロード)
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  なし
 * @note	
 */
template<class Archive>
void CScriptData::load(Archive& ar, const unsigned int version)
{
    SERIALIZATION_INIT
    int iSingleFile;

    SERIALIZATION_LOAD("Name"       , m_strName);
    SERIALIZATION_LOAD("BreakPoint" , m_mapBreak);
    SERIALIZATION_LOAD("SingleFile" , iSingleFile);
    m_bSingleFile = (iSingleFile ? true: false);

    if (m_bSingleFile)
    {
        std::string strScript;
        SERIALIZATION_LOAD("Script"       , strScript);
        SetScriptString(strScript);
    }
    else
    {
        bool bRet;
        if (m_psStrScript)
        {
            ;
        }
        StdString strFileName;
        strFileName = m_strName + _T(".as");
        StdPath  path = GetCurrentPath() / strFileName;
        
        SetCurrentPath(path);
        bRet = LoadScript(path);
    }

    m_iChgCnt = 0;
    MOCK_EXCEPTION_FILE(e_file_read);
}

/**
 * @brief   シリアル化(セーブ)
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  なし
 * @note	
 */
template<class Archive>
void CScriptData::save(Archive& ar, const unsigned int version) const
{
    SERIALIZATION_INIT
    int iSingleFile = (m_bSingleFile ? 1 : 0);

	SERIALIZATION_SAVE("Name", m_strName);
    SERIALIZATION_SAVE("BreakPoint", m_mapBreak);
    SERIALIZATION_SAVE("SingleFile", iSingleFile);
    if (m_bSingleFile)
    {
     
        std::string strScript;
        if (m_psStrScript)
        {
            strScript = *m_psStrScript;
        }
        SERIALIZATION_SAVE("Script", strScript);
    }
    else
    {
        bool bRet;
        StdString strFileName;
        strFileName = m_strName + _T(".as");
        StdPath  path = GetCurrentPath() / strFileName;
        bRet = SaveScript(path);
    }

    m_iChgCnt = 0;

    MOCK_EXCEPTION_FILE(e_file_write);
}


//=============================================
//=============================================
//=============================================
//=============================================

//!< コンストラクタ
CScript::CScript():
m_iChgCnt    (1),    
m_bSingleFile(false)
{
}

//!< コピーコンストラクタ
CScript::CScript(const CScript& m)
{
    CScriptData* pData;
    foreach(pData, m.m_lstScript)
    {
        CScriptData* pScript = new CScriptData(*pData);
        m_lstScript.push_back(pScript);
        m_bSingleFile = m.m_bSingleFile;
        m_pathCurrent = m.m_pathCurrent;
    }
    m_iChgCnt   = 1;
}

//!< デストラクタ
CScript::~CScript()
{
    Clear();
}

/**
 * @brief   全スクリプト削除
 * @param   なし
 * @retval  なし
 * @note
 */
void CScript::Clear()
{
    CScriptData* pData;
    foreach(pData, m_lstScript)
    {
        delete pData;
    }
    m_lstScript.clear();
    m_iChgCnt++;
}

/**
 * @brief   スクリプト名チェック
 * @param   [in] スクリプト名
 * @retval  true 使用可能
 * @note	
 */
bool CScript::CheckScriptName(StdString  strName)
{
    using namespace boost::xpressive;

    // 識別子はアルファベットもしは＿（アンダースコア）から始まり、
    // 数字、アンダースコア、アルファベット
    // を含む文字の内 予約語（ KeyWord )を除くもの
    StdStrXregex satatic_id =  + boost::xpressive::set[ alpha |  _T('_')] >> *_w;
 
    if(! regex_match( strName, satatic_id))
    {
        return false;
    }

    if (CheckScriptKeyWord( strName ))
    {
        return false;
    }
    return true;
}

/**
 * @brief   キーワードチェック
 * @param   [in] スクリプト名
 * @retval  true 使用可能
 * @note	
 */
bool CScript::CheckScriptKeyWord(StdString  strName)
{
    using namespace boost::assign;
    
    static std::set<StdString> setKeyWord;

    //TAG:[キーワード確認]
    if (setKeyWord.size() == 0)
    {
        setKeyWord +=    _T("break"), 	_T("case"),      _T("catch"),   _T("class"),
                         _T("clone"),   _T("continue"), _T("default"),   _T("delegate"),
                        _T("delete"), 	_T("else"), 	_T("extends"), 	_T("for"),
                        _T("function"), 	_T("if"), 	_T("in"), 	_T("local"), 	
                        _T("null"), 	_T("resume"),  _T("return"), 	_T("switch"),
                        _T("this"), 	_T("throw"), 	_T("try"), 	_T("typeof"),
                        _T("while"), 	_T("parent"), 	_T("yield"), 	_T("constructor"),
                        _T("vargc"), 	_T("vargv"),    _T("instanceof"), 	_T("true"), 	
                        _T("false"), 	_T("static");
    }

    if (setKeyWord.find(strName) == setKeyWord.end())
    {
        return false;
    }
    return true;

}

/**
 *  @brief  スクリプト名設定
 *  @param  [in]  strName   スクリプト名
 *  @param  [in]  strScript スクリプト
 *  @retval false 設定失敗
 *  @note
 */
bool CScript::SetScript(StdString strName, std::string strScript)
{
    StdString strTmp = CUtil::Trim(strName);
    if (!CheckScriptName(strTmp))
    {
        //使用できない文字が含まれている
        return false;
    }

    CScriptData* pScript;

    pScript = GetScript(strName);

    if (pScript != NULL)
    {
        pScript->SetScriptString(strScript);
    }
    else
    {

        if(m_lstScript.size() + 1 >= INT_MAX)
        {
            return false;
        }

        CScriptData* pNewScript =  new CScriptData(strName, strScript);
        m_lstScript.push_back(pNewScript);
    }
    m_iChgCnt++;
    return true;
}

/**
 *  @brief  スクリプト取得
 *  @param  [in] strScript スクリプト名
 *  @retval スクリプトへのポインタ
 *  @note
 */
CScriptData* CScript::GetScript(StdString strName)
{
    foreach(CScriptData* data, m_lstScript)
    {
        if (data->GetName() == strName)
        {
            return data;
        }
    }
    return NULL;
}

/**
 *  @brief  スクリプト取得
 *  @param  [in] strScript スクリプト名
 *  @retval スクリプトへのポインタ
 *  @note
 */
CScriptData* CScript::GetScript(int nPos)
{
    if (nPos < 0)               {return NULL;}
    if (nPos >= GetScriptNum()) {return NULL;}

    return m_lstScript[nPos];
 }

/**
 *  @brief  スクリプト削除
 *  @param  [in] strName スクリプト名
 *  @retval false 該当なし
 *  @note
 */
bool CScript::DelScript(StdString strName)
{
    std::vector<CScriptData*>::iterator ite;

    CScriptData* pData = NULL;

    for(ite  = m_lstScript.begin();
        ite != m_lstScript.end();
        ite++)
    {
        pData = *ite;
        if ( pData->GetName() == strName)
        {
            break;
        }
    }

    if (ite != m_lstScript.end())
    {
        m_lstScript.erase(ite);
        if (pData)
        {
            delete  pData;
        }
        m_iChgCnt++;
        return true;
    }
    return false;
}

/**
 *  @brief  スクリプト数取得
 *  @param  なし
 *  @retval スクリプト数
 *  @note
 */
int CScript::GetScriptNum()
{
    int iRet = static_cast<int>(m_lstScript.size());

    return iRet;
}

/**
 *  @brief  変更の有無
 *  @param  なし
 *  @retval true 変更あり
 *  @note
 */
bool CScript::IsChange() const
{
    if (m_iChgCnt != 0)
    {
        return false;
    }

    foreach(CScriptData* pData, m_lstScript)
    {
        if(pData->IsChange())
        {
            return true;
        }
    }
    return false;
}

//!< ファイル書き込みモード問い合わせ
bool CScript::IsSingleFileMode() const 
{
    return m_bSingleFile;
}

//!< ファイル書き込みモード設定
void CScript::SetSingleFileMode(bool bSingle)
{
    m_bSingleFile = bSingle;
}

//!< ファイルパス設定
void CScript::SetCurrentPath(StdPath path)
{
    m_pathCurrent = path;
}

//!< ファイルパス取得
StdPath CScript::GetCurrentPath() const 
{
    return m_pathCurrent;
}


/**
 * @brief   シリアル化(ロード)
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  なし
 * @note	
 */
template<class Archive>
void CScript::load(Archive& ar, const unsigned int version)
{
    SERIALIZATION_INIT
    Clear();

    int iSize;
    SERIALIZATION_LOAD("ListSize", iSize);

	//CScriptData  data;
	for(int iCnt = 0; iCnt < iSize; iCnt++)
    {
        std::stringstream   strmName;
        strmName << boost::format("List_%d") % iCnt;
		CScriptData* pData = new CScriptData();
		SERIALIZATION_LOAD(strmName.str(), *pData);
		pData->SetCurrentPath(GetCurrentPath());
        m_lstScript.push_back(pData);
    }
    m_iChgCnt = 0;
    MOCK_EXCEPTION_FILE(e_file_read);
 }

/**
 * @brief   シリアル化(セーブ)
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  なし
 * @note	
 */
template<class Archive>
void CScript::save(Archive& ar, const unsigned int version) const
{
    SERIALIZATION_INIT

	int iSize = SizeToInt(m_lstScript.size());
    SERIALIZATION_SAVE("ListSize", iSize);

    int iCnt = 0;
    foreach(CScriptData* pData, m_lstScript)
    {
        std::stringstream   strmName;
        strmName << boost::format("List_%d") % iCnt;
        pData->SetCurrentPath(GetCurrentPath());
        pData->SetSingleFileMode(IsSingleFileMode());
        SERIALIZATION_SAVE(strmName.str(), *pData);
		iCnt++;
    }

    m_iChgCnt = 0;
    MOCK_EXCEPTION_FILE(e_file_write);

}


//load save のインスタンスが生成されないため
//ダミーとして実装
void CScript::Dummy()
{
    unsigned int iVersion = 0;

    StdStreamOut outFs(_T(""));
    StdStreamIn  inFs(_T(""));
    //boost::filesystem::ofstream outTxtFs(_T(""));
    //boost::filesystem::ifstream inTxtFs(_T(""));

    boost::archive::text_woarchive txtOut(outFs); 
    boost::archive::text_wiarchive txtIn(inFs);

    save<boost::archive::text_woarchive>( txtOut, iVersion);
    load<boost::archive::text_wiarchive>( txtIn,  iVersion);

    StdXmlArchiveOut outXml(outFs);
    StdXmlArchiveIn inXml(inFs);

    save<StdXmlArchiveOut>(outXml, iVersion);
    load<StdXmlArchiveIn>(inXml, iVersion);


    boost::filesystem::ofstream outFsStd(_T(""));
    boost::filesystem::ifstream inFsStd(_T(""));

    boost::archive::binary_oarchive outBin(outFsStd);
    boost::archive::binary_iarchive inBin(inFsStd);

    save<boost::archive::binary_oarchive>(outBin, iVersion);
    load<boost::archive::binary_iarchive>(inBin, iVersion);

}

