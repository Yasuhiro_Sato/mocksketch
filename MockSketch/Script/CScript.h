/**
 * @brief			CScriptヘッダーファイル
 * @file			CScript.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#if !defined(__CSCRIPTL_H_)
#define __CSCRIPT_H_
//CScriptOblectに移行 使用しない

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "COMMON_HEAD.h"

/*---------------------------------------------------*/
/*  class                                            */
/*---------------------------------------------------*/
class asIScriptEngine;
class asIScriptGeneric;
class asIScriptContext;
class CEditorView;
class CScriptModule;


//-------------------------
//  ブレークポイントデータ
//-------------------------
class BREAK_POINT
{
public:
    bool bEnable;   //有効・無効
    int  iTmp;      //未定

public:
    BREAK_POINT():bEnable(false), iTmp(0){;}
    ~BREAK_POINT(){;}


private:
    friend class boost::serialization::access;  
    BOOST_SERIALIZATION_SPLIT_MEMBER();

    template<class Archive>
    void load(Archive& ar, const unsigned int version);

    template<class Archive>
    void save(Archive& ar, const unsigned int version) const;
};


/**
 * @class   CScriptData
 * @brief   スクリプトデータ
 */
class CScriptData
{
public:
    CScriptData();
    CScriptData(StdString name, std::string script);
    CScriptData(const CScriptData& m);
    ~CScriptData();

    //!< スクリプト名設定
    bool SetName(StdString name);

    //!< スクリプト名取得
    StdString GetName();

    //!< スクリプト設定
    bool SetScriptString(std::string strScript);

    //!< スクリプト取得
    std::string GetScriptString();

    //!< スクリプト取得
    std::weak_ptr<std::string> GetScriptStringInstance();

    //!< スクリプトアンロード
    void UnlaodScript();

    //!< ブレークポイント取得
    void GetBreakPoint(std::map<int, BREAK_POINT>* pMapBreak);

    //!< ブレークポイント消去
    void ClearBreakPoint();

    //!< ブレークポイント設定
    void SetBreakPoint(int iLine, BREAK_POINT breakPoint);

    //!< ブレークポイント問い合わせ
    bool IsBreak(int iLine);

    //!< カーソル取得
    void GetCursor( int* pLine, int* pCol);

    //!< カーソル設定
    void SetCursor( int iLine, int iCol);

    //!< カーソル取得
    int GetCurLine();

    //!< カーソル設定
    void SetCurLine( int iLine);

    //!< データクリア
    void ClearData();

    //!< スクリプト読み込み
    bool LoadScript(StdPath path);

    //!< スクリプト書き込み
    bool SaveScript(StdPath path) const;

    //!< 上書き保存
    bool OverWrite(bool bForce = false) const;

    //!< ファイル書き込みモード問い合わせ
    bool IsSingleFileMode() const { return m_bSingleFile;}

    //!< ファイル書き込みモード設定
    void SetSingleFileMode(bool bSingle = true){ m_bSingleFile = bSingle;}

    //!< ファイルパス設定
    void SetCurrentPath(StdPath path);

    //!< ファイルパス取得
    StdPath GetCurrentPath() const { return m_pathCurrent;}

    //!< 変更の有無
    bool IsChange() const;

    //!< 再読み込み
    bool Reload();

protected:
    friend CEditorView;
    friend CScriptModule;

    //!< 変更の設定
    void _AddChange();

protected:
    StdString         m_strName;                 //スクリプト名

    std::shared_ptr<std::string>    m_psStrScript; //!< スクリプト文字
    
    std::map<int, BREAK_POINT>  m_mapBreak;      //!< ブレークポイント

    bool             m_bSingleFile;              //!< 単一ファイルモード

    int m_iLine;          //!< カーソル位置 =（行）

    int m_iCol;           //!< カーソル位置（列）

    int m_iCurLine;       //!< 実行位置（行）

    //同期処理用
    bool m_bLockBreakSet;
    
    //同期処理用
    bool m_bLockBreakGet;
    
    //!< 現在のパス(load/save直前に設定)
    mutable StdPath                                 m_pathCurrent;

    mutable  int                                    m_iChgCnt;

private:
    friend class boost::serialization::access;
    BOOST_SERIALIZATION_SPLIT_MEMBER();

    template<class Archive>
    void load(Archive& ar, const unsigned int version);

    template<class Archive>
    void save(Archive& ar, const unsigned int version) const;
};


/**
 * @class   CScript
 * @brief                       
 */
class CScript
{
public:

    //!<  コンストラクタ
    CScript();


    //!<  コピーコンストラクタ
    CScript(const CScript& m);


    //!< デストラクタ
    ~CScript();

    //!< キーワードチェック
    static bool CheckScriptKeyWord(StdString  strName);

    //--------------------
    //
    //--------------------
    //!< スクリプト名チェック
    static bool CheckScriptName(StdString  strName);

    //!< 全スクリプト削除
    void Clear();

    //!< スクリプト設定
    bool SetScript(StdString strName, std::string strScript);

    //!< スクリプト取得
    CScriptData* GetScript(StdString strName);

    //!< スクリプト取得
    CScriptData* GetScript(int nPos);

    //!< スクリプト削除
    bool DelScript(StdString strScript);

    //!< スクリプト数取得
    int GetScriptNum();

    //!< 変更の有無
    bool IsChange() const;

    //!< ファイル書き込みモード問い合わせ
    bool IsSingleFileMode() const;

    //!< ファイル書き込みモード設定
    void SetSingleFileMode(bool bSingle = true);

    //!< ファイルパス設定
    void SetCurrentPath(StdPath path);

    //!< ファイルパス取得
    StdPath GetCurrentPath() const;
    //--------------------
    // ブレークポイント
    //--------------------


protected:
    friend CScript;

    std::vector<CScriptData*> m_lstScript;

    //!< 単一ファイルモード
    bool                                    m_bSingleFile;

    //!< 現在のパス(load/save直前に設定)
    mutable StdPath                         m_pathCurrent;

protected:
    //!< load save のインスタンス生成用ダミー
    void Dummy();

    mutable int  m_iChgCnt;


private:
    friend class boost::serialization::access;  
    BOOST_SERIALIZATION_SPLIT_MEMBER();

    template<class Archive>
    void load(Archive& ar, const unsigned int version);

    template<class Archive>
    void save(Archive& ar, const unsigned int version) const;
};

#endif //__CSCRIPT_H_

