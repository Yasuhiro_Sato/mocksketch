/**
 * @brief			CScriptEngine実装ファイル
 * @file		    CScriptEngine.cpp
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CScriptEngine.h"
#include "./CScriptObject.h"

#include <angelscript.h>
#include "Utility/Script/scriptArray.h"
#include "Utility/Script/scriptStdstring.h"
#include "Utility/Script/scriptStdwstring.h"
#include "Utility/Script/ScriptBase.h"
#include "Utility/Script/ScriptPOINT2D.h"
#include "Utility/Script/ScriptCIRCLE2D.h"
#include "Utility/Script/ScriptELLIPSE2D.h"
#include "Utility/Script/ScriptSPLINE2D.h"
#include "Utility/Script/ScriptMAT2D.h"
#include "Utility/Script/ScriptLINE2D.h"
#include "Utility/Script/ScriptRECT2D.h"
#include "Utility/Script/ScriptPartsDef.h"
#include "Utility/Script/ScriptDrawingObject.h"
#include "Utility/Script/ScriptDrawingScriptBase.h"
#include "Utility/Script/ScriptDrawingParts.h"
#include "Utility/Script/ScriptDrawingNode.h"
#include "Utility/Script/ScriptDrawingPoint.h"
#include "Utility/Script/ScriptDrawingLine.h"
#include "Utility/Script/ScriptDrawingText.h"
#include "Utility/Script/ScriptDrawingCircle.h"
#include "Utility/Script/ScriptDrawingEllipse.h"
#include "Utility/Script/ScriptDrawingCompositionline.h"
#include "Utility/Script/ScriptDrawingDimL.h"
#include "Utility/Script/ScriptDrawingDimH.h"
#include "Utility/Script/ScriptDrawingDimV.h"
#include "Utility/Script/ScriptDrawingDimA.h"
#include "Utility/Script/ScriptDrawingDimD.h"
#include "Utility/Script/ScriptDrawingDimR.h"
#include "Utility/Script/ScriptDrawingDimC.h"
#include "Utility/Script/ScriptDrawingImage.h"
#include "Utility/Script/ScriptDrawingConnectionLine.h"
#include "Utility/Script/ScriptDrawingSpline.h"
#include "Utility/Script/ScriptDrawingElement.h"
#include "Utility/Script/ScriptDrawingField.h"
#include "Utility/Script/ScriptDrawingReference.h"
#include "Utility/Script/ScriptDrawingGroup.h"
#include "Utility/Script/ScriptApplication.h"
#include "Utility/Script/ScriptNodeMarker.h"
#include "Utility/Script/ScriptDrawingView.h"
#include "Utility/Script/ScriptHandle.h"
#include "Utility/Script/ScriptAny.h"
#include "Utility/Script/ScriptSTL.h"
#include "Utility/Script/ScriptMath.h"
#include "Utility/Script/ScriptTcb.h"
#include "Utility/CUtility.h"
#include "VAL_DATA.h"
#include "DrawingObject/CDrawingScriptBase.h"
#include "DrawingObject/CDrawingParts.h"
#include "DrawingObject/CDrawingField.h"
#include "DrawingObject/CDrawingElement.h"
#include "DefinitionObject/CPartsDef.h"
#include "CExecCtrl.h"
#include "Tcb.h"
#include "CScriptObject.h"

#include "System/Csystem.h"
#include "ScriptAddin.h"
#include "MockSketch.h"




void SPrintfStringV(const std::string &str, va_list argptr)
{
    const static int iSize = 4096;
    char    szMsg[iSize];
        
    //sprintf_s(szMsg, iSize, str.c_str(), argptr);
    _vsnprintf_s(szMsg, iSize, _TRUNCATE, str.c_str(), argptr);

    std::cout << str;

    std::wstring wstr = CUtil::CharToString(szMsg);
    DB_PRINT(wstr.c_str());
}


void SPrintfString(const std::string str, ...)
{
    va_list argList;
    //va_list argCopy;

    va_start(argList, str);
    //va_copy(argCopy,argList);
    SPrintfStringV(str, argList);

    va_end(argList);
    //va_end(argCopy);
}


void ConvUni(asIScriptEngine * pEngine, 
            UniVal64* pUni, 
            void* ref, int refTypeId)
{

	if( refTypeId & asTYPEID_OBJHANDLE )
	{

        pUni->pVal = *(void**)ref;
    }
	else if( refTypeId & asTYPEID_MASK_OBJECT )
	{
        //For String
		asIObjectType *ot = pEngine->GetObjectTypeById(refTypeId);
        if (ot)
        {
            std::string strName = ot->GetName();
            if( strName == "string")
            {
                std::wstring* pStr = reinterpret_cast<std::wstring*>(ref);
                pUni->pcVal = reinterpret_cast<const void*>(pStr->c_str());
            }
        }
    }
    else
    {
        switch(refTypeId)
        {
        case asTYPEID_VOID:
            pUni->pVal = reinterpret_cast<void*>(ref);
            break;

        case asTYPEID_BOOL:
            pUni->bVal = *reinterpret_cast<bool*>(ref);
            break;
        case asTYPEID_INT8:
            pUni->cVal = *reinterpret_cast<char*>(ref);
            break;

	    case asTYPEID_INT16:
            pUni->sVal = *reinterpret_cast<short*>(ref);
            break;

	    case asTYPEID_INT32:
            pUni->iVal = *reinterpret_cast<int*>(ref);
            break;

	    case asTYPEID_INT64:
            pUni->iVal64 = *reinterpret_cast<__int64*>(ref);
            break;

	    case asTYPEID_UINT8:
            pUni->ucVal = *reinterpret_cast<unsigned char*>(ref);
            break;

	    case asTYPEID_UINT16:
            pUni->usVal = *reinterpret_cast<unsigned short*>(ref);
            break;

	    case asTYPEID_UINT32:
            pUni->uiVal = *reinterpret_cast<unsigned int*>(ref);
            break;

        case asTYPEID_UINT64:
            pUni->uiVal64 = *reinterpret_cast<unsigned __int64*>(ref);
            break;

	    case asTYPEID_FLOAT:
            pUni->fVal = *reinterpret_cast<float*>(ref);
            break;

	    case asTYPEID_DOUBLE:
            pUni->dVal = *reinterpret_cast<double*>(ref);
            break;

        default:
            break;

        }
    }
}


void ASprintfWStringV(const std::wstring &str, va_list argptr)
{
    const static int iSize = 4096;
    wchar_t    szMsg[iSize];
        
    _vsnwprintf_s(szMsg, iSize, _TRUNCATE, str.c_str(), argptr);
    DB_PRINT(szMsg);
    PrtintOut(WIN_DEBUG, szMsg);
}

void SPrintfWString(const std::wstring str, ...)
{
    va_list argList;

    va_start(argList, str);
    ASprintfWStringV(str, argList);
    va_end(argList);
}

void AsPrintf0(const std::wstring &str)
{
    SPrintfWString(str);
}


void AsPrintf1(const std::wstring &str, void *ref, int refTypeId)
{
    asIScriptContext *ctx = asGetActiveContext();
    asIScriptEngine *engine = ctx->GetEngine();

    UniVal64 Val[1] = {0};
    ConvUni(engine, &Val[0], ref, refTypeId);
    ASprintfWStringV(str, (char*)Val);
}

void AsPrintf2(const std::wstring &str, 
                 void *ref0, int refTypeId0,
                 void *ref1, int refTypeId1)
{
    asIScriptContext *ctx = asGetActiveContext();
    asIScriptEngine *engine = ctx->GetEngine();

    UniVal64 Val[2] = {0};
    ConvUni(engine, &Val[0], ref0, refTypeId0);
    ConvUni(engine, &Val[1], ref1, refTypeId1);
    ASprintfWStringV(str, (char*)Val);
}


void AsPrintf3(const std::wstring &str, 
                 void *ref0, int refTypeId0,
                 void *ref1, int refTypeId1,
                 void *ref2, int refTypeId2
                 )
{
    asIScriptContext *ctx = asGetActiveContext();
    asIScriptEngine *engine = ctx->GetEngine();

    UniVal64 Val[3] = {0};
    ConvUni(engine, &Val[0], ref0, refTypeId0);
    ConvUni(engine, &Val[1], ref1, refTypeId1);
    ConvUni(engine, &Val[2], ref2, refTypeId2);
    ASprintfWStringV(str, (char*)Val);
}


void AsPrintf4(const std::wstring &str, 
                 void *ref0, int refTypeId0,
                 void *ref1, int refTypeId1,
                 void *ref2, int refTypeId2,
                 void *ref3, int refTypeId3
                 )
{
    asIScriptContext *ctx = asGetActiveContext();
    asIScriptEngine *engine = ctx->GetEngine();

    UniVal64 Val[3] = {0};
    ConvUni(engine, &Val[0], ref0, refTypeId0);
    ConvUni(engine, &Val[1], ref1, refTypeId1);
    ConvUni(engine, &Val[2], ref2, refTypeId2);
    ConvUni(engine, &Val[2], ref2, refTypeId2);
    ASprintfWStringV(str, (char*)Val);


}


void AsPrintf5(const std::wstring &str, 
                 void *ref0, int refTypeId0,
                 void *ref1, int refTypeId1,
                 void *ref2, int refTypeId2,
                 void *ref3, int refTypeId3,
                 void *ref4, int refTypeId4
                 )
{
    asIScriptContext *ctx = asGetActiveContext();
    asIScriptEngine *engine = ctx->GetEngine();

    UniVal64 Val[5] = {0};
    ConvUni(engine, &Val[0], ref0, refTypeId0);
    ConvUni(engine, &Val[1], ref1, refTypeId1);
    ConvUni(engine, &Val[2], ref2, refTypeId2);
    ConvUni(engine, &Val[3], ref3, refTypeId3);
    ConvUni(engine, &Val[4], ref4, refTypeId4);
    ASprintfWStringV(str, (char*)Val);
}


void AsPrintf6(const std::wstring &str, 
                 void *ref0, int refTypeId0,
                 void *ref1, int refTypeId1,
                 void *ref2, int refTypeId2,
                 void *ref3, int refTypeId3,
                 void *ref4, int refTypeId4,
                 void *ref5, int refTypeId5
                 )
{
    asIScriptContext *ctx = asGetActiveContext();
    asIScriptEngine *engine = ctx->GetEngine();

    UniVal64 Val[6] = {0};
    ConvUni(engine, &Val[0], ref0, refTypeId0);
    ConvUni(engine, &Val[1], ref1, refTypeId1);
    ConvUni(engine, &Val[2], ref2, refTypeId2);
    ConvUni(engine, &Val[3], ref3, refTypeId3);
    ConvUni(engine, &Val[4], ref4, refTypeId4);
    ConvUni(engine, &Val[5], ref5, refTypeId5);
    ASprintfWStringV(str, (char*)Val);
}

void AsPrintf7(const std::wstring &str, 
                 void *ref0, int refTypeId0,
                 void *ref1, int refTypeId1,
                 void *ref2, int refTypeId2,
                 void *ref3, int refTypeId3,
                 void *ref4, int refTypeId4,
                 void *ref5, int refTypeId5,
                 void *ref6, int refTypeId6
                 )
{
    asIScriptContext *ctx = asGetActiveContext();
    asIScriptEngine *engine = ctx->GetEngine();

    UniVal64 Val[7] = {0};
    ConvUni(engine, &Val[0], ref0, refTypeId0);
    ConvUni(engine, &Val[1], ref1, refTypeId1);
    ConvUni(engine, &Val[2], ref2, refTypeId2);
    ConvUni(engine, &Val[3], ref3, refTypeId3);
    ConvUni(engine, &Val[4], ref4, refTypeId4);
    ConvUni(engine, &Val[5], ref5, refTypeId5);
    ConvUni(engine, &Val[6], ref6, refTypeId6);
    ASprintfWStringV(str, (char*)Val);


}

void AsPrintf8(const std::wstring &str, 
                 void *ref0, int refTypeId0,
                 void *ref1, int refTypeId1,
                 void *ref2, int refTypeId2,
                 void *ref3, int refTypeId3,
                 void *ref4, int refTypeId4,
                 void *ref5, int refTypeId5,
                 void *ref6, int refTypeId6,
                 void *ref7, int refTypeId7
                 )
{
    asIScriptContext *ctx = asGetActiveContext();
    asIScriptEngine *engine = ctx->GetEngine();

    UniVal64 Val[8] = {0};
    ConvUni(engine, &Val[0], ref0, refTypeId0);
    ConvUni(engine, &Val[1], ref1, refTypeId1);
    ConvUni(engine, &Val[2], ref2, refTypeId2);
    ConvUni(engine, &Val[3], ref3, refTypeId3);
    ConvUni(engine, &Val[4], ref4, refTypeId4);
    ConvUni(engine, &Val[5], ref5, refTypeId5);
    ConvUni(engine, &Val[6], ref6, refTypeId6);
    ConvUni(engine, &Val[7], ref7, refTypeId7);
    ASprintfWStringV(str, (char*)Val);


}

void AsPrintf9(const std::wstring &str, 
                 void *ref0, int refTypeId0,
                 void *ref1, int refTypeId1,
                 void *ref2, int refTypeId2,
                 void *ref3, int refTypeId3,
                 void *ref4, int refTypeId4,
                 void *ref5, int refTypeId5,
                 void *ref6, int refTypeId6,
                 void *ref7, int refTypeId7,
                 void *ref8, int refTypeId8
                 )
{
    asIScriptContext *ctx = asGetActiveContext();
    asIScriptEngine *engine = ctx->GetEngine();

    UniVal64 Val[9] = {0};
    ConvUni(engine, &Val[0], ref0, refTypeId0);
    ConvUni(engine, &Val[1], ref1, refTypeId1);
    ConvUni(engine, &Val[2], ref2, refTypeId2);
    ConvUni(engine, &Val[3], ref3, refTypeId3);
    ConvUni(engine, &Val[4], ref4, refTypeId4);
    ConvUni(engine, &Val[5], ref5, refTypeId5);
    ConvUni(engine, &Val[6], ref6, refTypeId6);
    ConvUni(engine, &Val[7], ref7, refTypeId7);
    ConvUni(engine, &Val[8], ref8, refTypeId8);
    ASprintfWStringV(str, (char*)Val);
}


int CreMethodTsk(std::wstring funcName,  TCB* pTcb)
{

    CScriptObject* pObj = pTcb->GetScriptObject();
    CExecCtrl* pCtrl = THIS_APP->GetExecCtrl();

    std::string strFunc = CUtil::StringToChar(funcName);

    asIScriptFunction* pFunc = NULL;
    if (pObj)
    {
         pFunc = pObj->GetMethod(strFunc.c_str());
    }

    if (!pFunc)
    {
        return -1;
    }

    if (pFunc->GetParamCount() != 1)
    {
        //TODO:エラーコード
        return -2;
    }

    int iTypeId;
	int iRet =  pFunc->GetParam( 0, &iTypeId);

     //TODO: TCBのTypeIDは別に記録しておく
     asIScriptEngine* pEngine = pFunc->GetEngine();
     asIObjectType* pType = pEngine->GetObjectTypeByName("TCB");
     if (!pType)
     {
         //TODO:エラーコード
         return -3;
     }

     int iTypeId2 = pType->GetTypeId();
     iTypeId2 |= asTYPEID_OBJHANDLE;

     if (iTypeId2 != iTypeId)
     {
         //TODO:エラーコード
         return -4;
     }

    pTcb->SetAsFunc(pFunc);
    pTcb->SetFunc(pTcb->GetFunc());
    pTcb->SetThreadType(pTcb->GetThreadType());
    pTcb->SetScriptObject(pTcb->GetScriptObject());
    int iTaskId = pCtrl->AsTaskCreate(pTcb);

    return iTaskId;
}


int CreMethodTsk2(std::wstring funcName)
{
    asIScriptContext *ctx = asGetActiveContext();
    CExecCtrl* pCtrl = THIS_APP->GetExecCtrl();
    TCB* pTcb = pCtrl->GetCurrentTcb(ctx);

    std::wstring funcDef;
    funcDef = _T("void ") + funcName + _T("(TCB@)");

    return CreMethodTsk(funcDef,  pTcb);
}

static void ScriptYield(TCB* pTcb)
{
	asIScriptContext *ctx = asGetActiveContext();
	if( !ctx )
	{
        return;
    }

    CExecCtrl* pCtrl = THIS_APP->GetExecCtrl();


    pCtrl->AsYield(ctx);
}


void ScriptTaskStart(int iTaskId)
{
    CExecCtrl* pCtrl = THIS_APP->GetExecCtrl();
    pCtrl->AsTaskStart(iTaskId);
}

bool ScriptTaskSetPriority(int iTaskId, int iPri)
{
    CExecCtrl* pCtrl = THIS_APP->GetExecCtrl();
    return pCtrl->AsTaskSetPriority(iTaskId, iPri);
}


bool ScriptTaskSetCycle(int iTaskId, DWORD dwCycleTime)
{
    CExecCtrl* pCtrl = THIS_APP->GetExecCtrl();
    return pCtrl->AsTaskSetCycle(iTaskId, dwCycleTime);
}

bool ScriptTaskExit()
{
    CExecCtrl* pCtrl = THIS_APP->GetExecCtrl();
    return pCtrl->AsTaskExit();
}

bool ScriptTaskSleep(DWORD dwTimeOut)
{
    CExecCtrl* pCtrl = THIS_APP->GetExecCtrl();
    return pCtrl->AsTaskSleep(dwTimeOut);
}

bool ScriptTaskAbort(int iTaskId)
{
    CExecCtrl* pCtrl = THIS_APP->GetExecCtrl();
    return pCtrl->AsTaskAbort(iTaskId);
}

bool ScriptTaskTerminate(int iTaskId)
{
    CExecCtrl* pCtrl = THIS_APP->GetExecCtrl();
    return pCtrl->AsTaskTerminate(iTaskId);
}

bool ScriptTaskSuspend()
{
    CExecCtrl* pCtrl = THIS_APP->GetExecCtrl();
    return pCtrl->AsTaskSuspend();
}

bool ScriptTaskWakeUp(int iTaskId)
{
    CExecCtrl* pCtrl = THIS_APP->GetExecCtrl();
    return pCtrl->AsTaskWakeUp(iTaskId);
}

int ScriptTaskStatus(int iTaskId)
{
    CExecCtrl* pCtrl = THIS_APP->GetExecCtrl();
    return pCtrl->AsTaskStatus(iTaskId);
}

bool ScriptTaskSetName(int iTaskId, const std::wstring& strName)
{
    CExecCtrl* pCtrl = THIS_APP->GetExecCtrl();
    return pCtrl->AsTaskSetName(iTaskId, strName);
}

int ScriptTaskGetId(const std::wstring& strName)
{
    CExecCtrl* pCtrl = THIS_APP->GetExecCtrl();
    return pCtrl->AsTaskGetId( strName);
}

bool ScriptEndCycle()
{
    CExecCtrl* pCtrl = THIS_APP->GetExecCtrl();
    return pCtrl->AsTaskEndCycle();
}


/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CScriptEngine::CScriptEngine():
m_pEngine(0),
m_pInputCalcEngine(0)
{
    m_pThreadManager = asGetThreadManager();
    asPrepareMultithread  (m_pThreadManager);

    m_pEngine = asCreateScriptEngine(ANGELSCRIPT_VERSION);
    if( m_pEngine == 0 )
    {
        STD_DBG(_T("CScriptEngine::CScriptEngine Fail"));
        return;
    }

    m_pEngine->SetEngineProperty(asEP_SCRIPT_SCANNER, 0); 
    m_pEngine->SetEngineProperty(asEP_STRING_ENCODING , 0); //1:UTF-16
    m_pEngine->SetEngineProperty(asEP_ALLOW_MULTILINE_STRINGS , 1);
    // The script compiler will send any compiler messages to the callback
    m_pEngine->SetMessageCallback(asFUNCTION(MessageCallback), 0, asCALL_CDECL);


    m_iTemplateId = 0;
    ConfigureEngine(m_pEngine);
    ConfigureTemplate();

    _SetupCalc();
}

/**
 *  @brief  デストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CScriptEngine::~CScriptEngine()
{
    int iRet;
    m_mapTypeFunc.clear();
    m_mapTemplateFunc.clear();
    asThreadCleanup();
    if (m_pEngine)
    {
	    // Release the engine
	    iRet = m_pEngine->Release();
    }
    if (m_pInputCalcEngine)
    {
	    iRet = m_pInputCalcEngine->Release();
    }


    asUnprepareMultithread();
}


/**
 *  @brief  テキスト変換開放
 *  @param  なし
 *  @retval なし
 *  @note   アプリケーション終了時に呼び出す
 */
void CScriptEngine::ReleaseTextConvMap()
{
    //asEndText();
}


/**
 *  @brief  組込関数登録
 *  @param  なし
 *  @retval なし     
 *  @note
 */
void CScriptEngine::ConfigureTemplate()
{
    // テンプレートのTypeIDが取得できないので
    // ここで設定する
    asIObjectType* pType;
    NAME_VALFUNC_MAP::iterator iteMap;
    int iSize = m_pEngine->GetObjectTypeCount();

    std::string strName;
    int iId;

    for (int iCnt = 0; iCnt < iSize; iCnt++)
    {
        pType = m_pEngine->GetObjectTypeByIndex(iCnt);

        if (pType == NULL){continue;}

        if (!(pType->GetFlags() & asOBJ_TEMPLATE))
        {
            continue;
        }
        strName = pType->GetName();
        iteMap = m_mapTemplateFunc.find(strName);
        if (iteMap != m_mapTemplateFunc.end())
        {
            iId = pType->GetTypeId();
            m_mapTypeFunc[iId] = iteMap->second;
            DB_PRINT(_T("SetTmplate[%s] = %x \n"), strName.c_str(), iId);
        }
    }
}


class AsFormat
{
public:

    explicit AsFormat(const std::string &str)
    {
        m_pFmt = new boost::basic_format<char >(str.c_str());
    }

    virtual ~AsFormat()
    {
        delete m_pFmt;
    }

    template<class T> 
    AsFormat&   operator%(const T& x)
    {
       boost::io::detail::feed<char>

    }

    //friend std::basic_ostream<Ch2, Tr2> & 
    //operator<<( std::basic_ostream<Ch2, Tr2> & ,
    //            const basic_format<Ch2, Tr2, Alloc2>& );

    boost::basic_format<char >*   m_pFmt;

};

/**
 *  @brief  スクリプトエンジン設定
 *  @param  なし
 *  @retval なし     
 *  @note
 */
void CScriptEngine::ConfigureEngine(asIScriptEngine*  pEngine)
{
    int r;
    STD_ASSERT(pEngine != NULL);
    SYS_LIBRARY->RegisterAddins(CSystemLibrary::E_ADDIN_AS);

    MockScript::FUNC_RegisterType retgsterType;
    MockScript::FUNC_Register     retgster;

    //組み込み型の登録
    RegisterMockSketchEnum      (pEngine);

    RegisterScriptMath          (pEngine);

    RegisterScriptArray         (pEngine,   m_mapTemplateFunc, true);
    //RegisterStdString           (pEngine,   m_mapTypeFunc);
    RegisterStdWstring          (pEngine,   m_mapTypeFunc);
    RegisterScriptHandleType    (pEngine,   m_mapTypeFunc);

    RegisterMAT2DType    (pEngine,   m_mapTypeFunc);
    RegisterPOINT2DType  (pEngine,   m_mapTypeFunc);
    RegisterLINE2DType   (pEngine,   m_mapTypeFunc);
    RegisterCIRCLE2DType (pEngine,   m_mapTypeFunc);
    RegisterELLIPSE2DType(pEngine,   m_mapTypeFunc);
    RegisterSPLINE2DType (pEngine,   m_mapTypeFunc);
    RegisterRECT2DType   (pEngine,   m_mapTypeFunc);
    RegisterPOINTType    (pEngine,   m_mapTypeFunc);
    RegisterRECTType     (pEngine,   m_mapTypeFunc);
    RegisterMOUSE_MOVE_POSType        (pEngine,   m_mapTypeFunc);

    RegisterTypePOINT2D_VECTOR        (pEngine,   m_mapTypeFunc);
    RegisterTypeDrawingObjectPtr_VECTOR  (pEngine,   m_mapTypeFunc);

    RegisterDrawingObjectType       (pEngine,   m_mapTypeFunc);
    RegisterPartsDefType  (pEngine,   m_mapTypeFunc);
    RegisterDrawingScriptBaseType   (pEngine,   m_mapTypeFunc);
    //RegisterApplicationType         (pEngine,   m_mapTypeFunc);
    RegisterDrawingPointType        (pEngine,   m_mapTypeFunc);
    RegisterDrawingLineType         (pEngine,   m_mapTypeFunc);
    RegisterDrawingTextType         (pEngine,   m_mapTypeFunc);
    RegisterDrawingPartsType        (pEngine,   m_mapTypeFunc);
    RegisterDrawingNodeType         (pEngine,   m_mapTypeFunc);

    RegisterDrawingDimLType          (pEngine,   m_mapTypeFunc);
    RegisterDrawingDimHType          (pEngine,   m_mapTypeFunc);
    RegisterDrawingDimVType          (pEngine,   m_mapTypeFunc);
    RegisterDrawingDimAType          (pEngine,   m_mapTypeFunc);
    RegisterDrawingDimDType          (pEngine,   m_mapTypeFunc);
    RegisterDrawingDimRType          (pEngine,   m_mapTypeFunc);
    RegisterDrawingDimCType          (pEngine,   m_mapTypeFunc);
    RegisterDrawingImageType         (pEngine,   m_mapTypeFunc);

    RegisterDrawingReferenceType     (pEngine,   m_mapTypeFunc);
    RegisterDrawingGroupType         (pEngine,   m_mapTypeFunc);

    RegisterDrawingEllipseType      (pEngine,   m_mapTypeFunc);
    RegisterDrawingCircleType       (pEngine,   m_mapTypeFunc);
    RegisterDrawingConnectionLineType    (pEngine,   m_mapTypeFunc);
    RegisterDrawingSplineType       (pEngine,   m_mapTypeFunc);
    RegisterDrawingCompositionLineType (pEngine,   m_mapTypeFunc);
    RegisterDrawingElementType      (pEngine,   m_mapTypeFunc);
    RegisterDrawingFieldType        (pEngine,   m_mapTypeFunc);
    RegisterNodeMarkerType          (pEngine,   m_mapTypeFunc);
    RegisterDrawingViewType         (pEngine,   m_mapTypeFunc);
    RegisterAnyType                 (pEngine,   m_mapTypeFunc);
    RegisterTcbType                 (pEngine,   m_mapTypeFunc);
#ifdef _DEBUG
    RegisterRefClassType            (pEngine,   m_mapTypeFunc);
#endif //_DEBUG

    int iRet;
    HMODULE hModule;
    std::map<StdString, CSystemLibrary::ADDIN_DATA>::iterator iteMap;
    for(iteMap  = SYS_LIBRARY->m_mapScriptAddins.begin();
        iteMap != SYS_LIBRARY->m_mapScriptAddins.end();
        iteMap++)
    {
        hModule = iteMap->second.hModule;

        retgsterType = reinterpret_cast<MockScript::FUNC_RegisterType>
                          (::GetProcAddress(hModule, "RegisterType"));
        iRet = retgsterType(pEngine);
    }

    RegisterScriptHandle        (pEngine);
    RegisterMAT2D               (pEngine);
    RegisterLINE2D              (pEngine);
    RegisterPOINT2D             (pEngine);
    RegisterCIRCLE2D            (pEngine);
    RegisterELLIPSE2D           (pEngine);
    RegisterSPLINE2D            (pEngine);
    RegisterRECT2D              (pEngine);
    RegisterPOINT               (pEngine);
    RegisterRECT                (pEngine);
    RegisterMOUSE_MOVE_POS      (pEngine);
    RegisterPOINT2D_VECTOR      (pEngine);
    RegisterDrawingObjectPtr_VECTOR(pEngine);

    RegisterDrawingObject       (pEngine);
    RegisterPartsDef  (pEngine);
    RegisterDrawingScriptBase   (pEngine);
    //RegisterApplication         (pEngine);
    RegisterDrawingPoint        (pEngine);
    RegisterDrawingLine         (pEngine);
    RegisterDrawingText         (pEngine);
    RegisterDrawingParts        (pEngine);
    RegisterDrawingNode         (pEngine);
    RegisterDrawingDimH         (pEngine);
    RegisterDrawingDimL         (pEngine);
    RegisterDrawingDimV         (pEngine);
    RegisterDrawingDimA         (pEngine);
    RegisterDrawingDimD         (pEngine);
    RegisterDrawingDimR         (pEngine);
    RegisterDrawingDimC         (pEngine);
    RegisterDrawingImage        (pEngine);

    RegisterDrawingReference    (pEngine);
    RegisterDrawingGroup        (pEngine);

    RegisterDrawingEllipse      (pEngine);
    RegisterDrawingCircle       (pEngine);
    RegisterDrawingConnectionLine    (pEngine);
    RegisterDrawingSpline       (pEngine);
    RegisterDrawingCompositionLine (pEngine);
    RegisterDrawingElement      (pEngine);
    RegisterDrawingField        (pEngine);
    RegisterNodeMarker          (pEngine);
    RegisterDrawingView         (pEngine);
    RegisterAny                 (pEngine);
    RegisterTcb                 (pEngine);
#ifdef _DEBUG
    RegisterRefClass            (pEngine);

#endif //_DEBUG

    for(iteMap  = SYS_LIBRARY->m_mapScriptAddins.begin();
        iteMap != SYS_LIBRARY->m_mapScriptAddins.end();
        iteMap++)
    {
        hModule = iteMap->second.hModule;

        retgster     = reinterpret_cast<MockScript::FUNC_Register>
                           (::GetProcAddress(hModule, "Register"));
        iRet = retgster(pEngine);
    }

    // r = pEngine->RegisterGlobalFunction("void Print(string &in)", asFUNCTION(PrintString), asCALL_CDECL); assert( r >= 0 );
    // r = pEngine->RegisterGlobalFunction("void Print(StdString &in)", asFUNCTION(PrintStringW), asCALL_CDECL); assert( r >= 0 );
    // r = pEngine->RegisterGlobalFunction("StdString _T(string &in)", asFUNCTION(ConvStr), asCALL_CDECL); assert( r >= 0 );
    // r = pEngine->RegisterGlobalFunction("void print(const string &in)", asFUNCTION(PrintWString), asCALL_CDECL); assert( r >= 0 );

    r = pEngine->RegisterGlobalFunction("uint GetSystemTime()", asFUNCTION(GetTickCount), asCALL_STDCALL); assert( r >= 0 );
    r = pEngine->RegisterGlobalFunction("void Assert(bool)", asFUNCTION(Assert), asCALL_GENERIC); assert( r >= 0 );

    r = pEngine->RegisterGlobalFunction("PartsDef@ GetDrawingScriptBase()", asFUNCTION(GetDrawingScriptBase), asCALL_CDECL); assert( r >= 0 );
    r = pEngine->RegisterGlobalFunction("DrawingParts@ GetDrawingParts()", asFUNCTION(GetDrawingParts), asCALL_CDECL); assert( r >= 0 );
    r = pEngine->RegisterGlobalFunction("DrawingField@ GetDrawingField()", asFUNCTION(GetDrawingField), asCALL_CDECL); assert( r >= 0 );
    r = pEngine->RegisterGlobalFunction("DrawingElement@ GetDrawingElement()", asFUNCTION(GetDrawingElement), asCALL_CDECL); assert( r >= 0 );
    r = pEngine->RegisterGlobalFunction("void Redraw()", asFUNCTION(Redraw), asCALL_CDECL); assert( r >= 0 );
    //r = pEngine->RegisterGlobalFunction("Application@ Application()", asFUNCTION(Application), asCALL_CDECL); assert( r >= 0 );

    r = pEngine->RegisterGlobalFunction("int CreMethodTsk(const string &in, TCB@)", 
                                            asFUNCTION(CreMethodTsk), asCALL_CDECL); assert( r >= 0 );
        
    r = pEngine->RegisterGlobalFunction("int CreMethodTsk2(const string &in)", 
                                            asFUNCTION(CreMethodTsk2), asCALL_CDECL); assert( r >= 0 );

    r = pEngine->RegisterGlobalFunction("void Yield()", 
                                            asFUNCTION(ScriptYield), asCALL_CDECL); assert( r >= 0 );

    r = pEngine->RegisterGlobalFunction("void TaskStart(int)", 
                                            asFUNCTION(ScriptTaskStart), asCALL_CDECL); assert( r >= 0 );

    r = pEngine->RegisterGlobalFunction("bool TaskSetPriority(int, int)", 
                                            asFUNCTION(ScriptTaskSetPriority), asCALL_CDECL); assert( r >= 0 );

    r = pEngine->RegisterGlobalFunction("bool TaskSetCycle(int, uint)", 
                                            asFUNCTION(ScriptTaskSetCycle), asCALL_CDECL); assert( r >= 0 );

    r = pEngine->RegisterGlobalFunction("bool TaskExit()", 
                                            asFUNCTION(ScriptTaskExit), asCALL_CDECL); assert( r >= 0 );

    r = pEngine->RegisterGlobalFunction("bool TaskSleep(uint)", 
                                            asFUNCTION(ScriptTaskSleep), asCALL_CDECL); assert( r >= 0 );

    r = pEngine->RegisterGlobalFunction("bool TaskAbort(uint)", 
                                            asFUNCTION(ScriptTaskAbort), asCALL_CDECL); assert( r >= 0 );

    r = pEngine->RegisterGlobalFunction("bool TaskTerminate(uint)", 
                                            asFUNCTION(ScriptTaskTerminate), asCALL_CDECL); assert( r >= 0 );

    r = pEngine->RegisterGlobalFunction("bool TaskSuspend()", 
                                            asFUNCTION(ScriptTaskSuspend), asCALL_CDECL); assert( r >= 0 );

    r = pEngine->RegisterGlobalFunction("bool TaskWakeUp(uint)", 
                                            asFUNCTION(ScriptTaskWakeUp), asCALL_CDECL); assert( r >= 0 );

    r = pEngine->RegisterGlobalFunction("TASK_STS TaskStatus(uint)", 
                                            asFUNCTION(ScriptTaskStatus), asCALL_CDECL); assert( r >= 0 );

    r = pEngine->RegisterGlobalFunction("bool TaskSetName(uint, const string &in)", 
                                            asFUNCTION(ScriptTaskSetName), asCALL_CDECL); assert( r >= 0 );
        
    r = pEngine->RegisterGlobalFunction("int TaskGetId(const string &in)", 
                                            asFUNCTION(ScriptTaskGetId), asCALL_CDECL); assert( r >= 0 );

    r = pEngine->RegisterGlobalFunction("bool TaskEndCycle()", 
                                            asFUNCTION(ScriptEndCycle), asCALL_CDECL); assert( r >= 0 );

    r = pEngine->RegisterGlobalFunction("void Printf(const string &in)", 
                                            asFUNCTION(AsPrintf0), asCALL_CDECL); assert( r >= 0 );
        
    r = pEngine->RegisterGlobalFunction("void Printf(const string &in, ?& in)", 
                                            asFUNCTION(AsPrintf1), asCALL_CDECL); assert( r >= 0 );

    r = pEngine->RegisterGlobalFunction("void Printf(const string &in, ?& in, ?& in)", 
                                            asFUNCTION(AsPrintf2), asCALL_CDECL); assert( r >= 0 );

    r = pEngine->RegisterGlobalFunction("void Printf(const string &in, ?& in, ?& in, ?& in)", 
                                            asFUNCTION(AsPrintf3), asCALL_CDECL); assert( r >= 0 );

    r = pEngine->RegisterGlobalFunction("void Printf(const string &in, ?& in, ?& in, ?& in, ?& in)", 
                                            asFUNCTION(AsPrintf4), asCALL_CDECL); assert( r >= 0 );

    r = pEngine->RegisterGlobalFunction("void Printf(const string &in, ?& in, ?& in, ?& in, ?& in, ?& in)", 
                                            asFUNCTION(AsPrintf5), asCALL_CDECL); assert( r >= 0 );

    r = pEngine->RegisterGlobalFunction("void Printf(const string &in, ?& in, ?& in, ?& in, ?& in, ?& in, ?& in)", 
                                            asFUNCTION(AsPrintf6), asCALL_CDECL); assert( r >= 0 );

    r = pEngine->RegisterGlobalFunction("void Printf(const string &in, ?& in, ?& in, ?& in, ?& in, ?& in, ?& in, ?& in)", 
                                            asFUNCTION(AsPrintf7), asCALL_CDECL); assert( r >= 0 );

    r = pEngine->RegisterGlobalFunction("void Printf(const string &in, ?& in, ?& in, ?& in, ?& in, ?& in, ?& in, ?& in, ?& in)", 
                                            asFUNCTION(AsPrintf8), asCALL_CDECL); assert( r >= 0 );

    r = pEngine->RegisterGlobalFunction("void Printf(const string &in, ?& in, ?& in, ?& in, ?& in, ?& in, ?& in, ?& in, ?& in, ?& in)", 
                                            asFUNCTION(AsPrintf9), asCALL_CDECL); assert( r >= 0 );


    //インターフェイスの登録


    //全体で使いたいものがあればここで登録しておく

    // 今のところグローバル変数,Type等はなし
    // 設定する場合は、CScriptModule::GetVarData等の影響を考えておくこと。
    //LoadAddinScript();
}

/**
 *  @brief  アドインスクリプト読み込み
 *  @param  [in]msg   メッセージ情報
 *  @param  [in]param 付帯情報
 *  @retval なし     
 *  @note   付帯情報は現在未使用
 */
/*
void CScriptEngine::MessageCallback(const asSMessageInfo *msg, void *param)
{

}
*/

/**
 *  @brief  メッセージコールバック
 *  @param  [in]msg   メッセージ情報
 *  @param  [in]param 付帯情報
 *  @retval なし     
 *  @note   付帯情報は現在未使用
 */
void CScriptEngine::MessageCallback(const asSMessageInfo *msg, void *param)
{
	const char *type = "ERR ";
	if( msg->type == asMSGTYPE_WARNING ) 
    {
		type = "WARN";
    }
	else if( msg->type == asMSGTYPE_INFORMATION ) 
    {
		type = "INFO";
    }

    char szBuf[1024];
	sprintf_s(&szBuf[0], sizeof(szBuf), "%s (line %d, %d) : %s : %s\n", msg->section, msg->row, msg->col, type, msg->message);

    StdString strOut =  CUtil::CharToString(szBuf);

    //実際には セクション名から表示Viewを探す必要があるため セクション名は Parts名::スクリプト名にする必要がある
    //出力領域をダブルクリックでウインドウを開く
    /*
        struct asSMessageInfo
        {
	        const char *section;
	        int         row;
	        int         col;
	        asEMsgType  type;
	        const char *message;
        };
    */
    PrtintOut(WIN_BUILD, strOut.c_str());
}

/*
 *  @brief  エンジン取得
 *  @param  なし
 *  @retval スクリプト
 *  @note   
 */
asIScriptEngine* CScriptEngine::GetEngine()
{
    return m_pEngine;
}

/**
 *  @brief  プリント関数(string)
 *  @param  [in] str 出力文字
 *  @retval なし
 *  @note   
 */
void CScriptEngine::PrintScript(const char * lpszFormat, ...)
{
    static const int iSize = 4096;
    char szMsg [iSize];

    va_list argList;
    va_start(argList, lpszFormat);
    vsprintf_s(szMsg, iSize, lpszFormat, argList);
    std::string str(szMsg);
    PrintString(str);
}

/**
 *  @brief  プリント関数(string)
 *  @param  [in] str 出力文字
 *  @retval なし
 *  @note   
 */
void CScriptEngine::PrintString(std::string str)
{
    StdString strOut =  CUtil::CharToString(str.c_str());
	DB_PRINT( strOut.c_str() );
    PrtintOut(WIN_DEBUG, strOut.c_str());
}


/**
 *  @brief  変換(string)
 *  @param  [in] str 
 *  @retval ワイド文字列
 *  @note   
 */
StdString CScriptEngine::ConvStr(std::string &str)
{
    StdString strOut =  CUtil::CharToString(str.c_str());
    return strOut;
}

/**
 *  @brief  プリント関数(StdString)
 *  @param  [in] str 出力文字
 *  @retval なし
 *  @note   
 */
void CScriptEngine::PrintStringW(StdString str)
{
	DB_PRINT( str.c_str() );
    PrtintOut(WIN_DEBUG, str.c_str());
}


void CScriptEngine::Assert(asIScriptGeneric *gen)
{
    bool expr;
	if( sizeof(bool) == 1 )
    {
		expr = gen->GetArgByte(0) ? true : false;
    }
	else
    {
		expr = gen->GetArgDWord(0) ? true : false;
    }

	if( !expr )
	{
		PrintScript("--- Assert failed ---\n");
		asIScriptContext *ctx = asGetActiveContext();
		if( ctx )
		{
			const asIScriptFunction *function = ctx->GetFunction();
			if( function != 0 )
			{
				PrintScript("func   : %s\n", function->GetDeclaration());
				PrintScript("module : %s\n", function->GetModuleName());
				PrintScript("section: %s\n", function->GetScriptSectionName());
			}
			PrintScript("line: %d\n", ctx->GetLineNumber());
			ctx->SetException("Assert failed");
			PrintScript("---------------------\n");
		}
	}
}


/**
 *  @brief  オブジェクトコントロール取得
 *  @param  なし
 *  @retval オブジェクトコントロール
 *  @note   
 */
CDrawingScriptBase* CScriptEngine::GetDrawingScriptBase()
{
    CDrawingScriptBase* pScriptBase;
    asIScriptContext* pCtx = asGetActiveContext();

    pScriptBase = reinterpret_cast<CDrawingScriptBase*>(pCtx->GetUserData());

    pScriptBase->AddRef();
    return pScriptBase;
}

CDrawingParts* CScriptEngine::GetDrawingParts()
{
    CDrawingParts* pParts;
    asIScriptContext* pCtx = asGetActiveContext();

    pParts = reinterpret_cast<CDrawingParts*>(pCtx->GetUserData());
    if (pParts->GetType() != DT_PARTS)
    {
        pParts = NULL;
    }
    else
    {
        pParts->AddRef();
    }

    return pParts;
}

CDrawingField* CScriptEngine::GetDrawingField()
{
    CDrawingField* pField;
    asIScriptContext* pCtx = asGetActiveContext();

    pField = reinterpret_cast<CDrawingField*>(pCtx->GetUserData());

    if (pField->GetType() != DT_FIELD)
    {
        pField = NULL;
    }
    else
    {
        pField->AddRef();
    }
    return pField;
}

CDrawingElement* CScriptEngine::GetDrawingElement()
{
    CDrawingElement* pElement;
    asIScriptContext* pCtx = asGetActiveContext();

    pElement = reinterpret_cast<CDrawingElement*>(pCtx->GetUserData());

    if (pElement->GetType() != DT_ELEMENT)
    {
        pElement = NULL;
    }
    else
    {
        pElement->AddRef();
    }
    return pElement;
}

void CScriptEngine::Redraw()
{
    std::shared_ptr<CObjectDef> pObjectDef;
    pObjectDef = THIS_APP->GetCurrentObjectDef().lock();
    if (!pObjectDef)
    {
        return;
    }

    if((pObjectDef->GetObjectType() != VIEW_COMMON::E_PARTS) &&
       (pObjectDef->GetObjectType() != VIEW_COMMON::E_FIELD))
    {
        return;
    }

    CPartsDef* pCtrl;
    pCtrl = dynamic_cast<CPartsDef*>(pObjectDef.get());

    if (!pCtrl)
    {
        return;
    }

    pCtrl->Redraw();
}

/**
 *  @brief  変数データ変換
 *  @param  [out] pValData      変数データ
 *  @param  [in] strVarName     変数名
 *  @param  [in] iTypeId        型ID
 *  @param  [in] pValAddress    変数アドレス
 *  @retval true: 成功
 *  @note  
 */
bool CScriptEngine::ConvVarData(VAL_DATA* pValData, 
                                LPCTSTR strVarName,
                                int iTypeId, 
                                const void* pValAddress, 
                                CScriptObject* pScriptObject)
{
    pValData->lstVal.clear();

    if (ConvVarDefaultData(pValData, iTypeId, pValAddress))
    {
        pValData->strName = strVarName;
        return true;
    }

    if (ConvUserVarData(pValData, iTypeId, pValAddress, pScriptObject))
    {
        pValData->strName = strVarName;
        return true;
    }

    return false;
}


/**
 *  @brief  基本型変数データ変換
 *  @param  [out] pValData      変数データ
 *  @param  [in] iTypeId        型ID
 *  @param  [in] pValAddress    変数アドレス
 *  @retval true: 成功
 *  @note  
 */
bool CScriptEngine::ConvVarDefaultData(VAL_DATA* pValData, 
                                       int iTypeId, const void* pValAddress)
{
    bool bHex = false;
    if (!pValAddress)
    {
        pValData->strVal = _T("----");
    }

    try
    {
    switch (iTypeId)
    {
    case asTYPEID_VOID:
        pValData->strTypeName = _T("void");
        if (pValAddress)
        {
            pValData->strVal = CUtil::StrFormat(_T("%I64x"), pValAddress);
        }
        return true;
        break;

    case asTYPEID_BOOL:
        {
            pValData->strTypeName = _T("bool");
            if (pValAddress)
            {
                const bool* pBool = reinterpret_cast<const bool*>(pValAddress);
                pValData->strVal = CUtil::StrFormat(_T("%s"), *pBool ? _T("true") : _T("false") );
            }
            return true;
        }
        break;

    case asTYPEID_INT8:
        {
            pValData->strTypeName = _T("int8");
            if (pValAddress)
            {
                const char* pChar = reinterpret_cast<const char*>(pValAddress);
                if (!bHex) { pValData->strVal = CUtil::StrFormat(_T("%d"), *pChar);}
                else       { pValData->strVal = CUtil::StrFormat(_T("%x"), *pChar);}
            }
            return true;
        }
        break;
    case asTYPEID_INT16:
        {
            pValData->strTypeName = _T("int16");
            if (pValAddress)
            {
                const short* pShort = reinterpret_cast<const short*>(pValAddress);
                if (!bHex) { pValData->strVal = CUtil::StrFormat(_T("%d"), *pShort);}
                else       { pValData->strVal = CUtil::StrFormat(_T("%x"), *pShort);}
            }
            return true;
        }
        break;
    case asTYPEID_INT32:
        {
            pValData->strTypeName = _T("int");
            if (pValAddress)
            {
                const int* pInt = reinterpret_cast<const int*>(pValAddress);
                if (!bHex) { pValData->strVal = CUtil::StrFormat(_T("%d"), *pInt);}
                else       { pValData->strVal = CUtil::StrFormat(_T("%x"), *pInt);}
            }
            return true;
        }
        break;
    case asTYPEID_INT64:
        {
            pValData->strTypeName = _T("int64");
            if (pValAddress)
            {
                const long long* pInt = reinterpret_cast<const long long*>(pValAddress);
                if (!bHex) { pValData->strVal = CUtil::StrFormat(_T("%lld"), *pInt);}
                else       { pValData->strVal = CUtil::StrFormat(_T("%I64x"), *pInt);}
            }
            return true;
        }
        break;
    case asTYPEID_UINT8:
        {
            pValData->strTypeName = _T("uint8");
            if (pValAddress)
            {
                const unsigned char* pChar = reinterpret_cast<const unsigned char*>(pValAddress);
                if (!bHex) { pValData->strVal = CUtil::StrFormat(_T("%u"), *pChar);}
                else       { pValData->strVal = CUtil::StrFormat(_T("%x"), *pChar);}
            }
            return true;
        }
        break;

    case asTYPEID_UINT16:
        {
            pValData->strTypeName = _T("uint16");
            if (pValAddress)
            {
                const unsigned short* pShort = reinterpret_cast<const unsigned short*>(pValAddress);
                if (!bHex)  { pValData->strVal = CUtil::StrFormat(_T("%u"), *pShort);}
                else        { pValData->strVal = CUtil::StrFormat(_T("%x"), *pShort);}
            }
            return true;
        }
        break;

    case asTYPEID_UINT32:
        {
            pValData->strTypeName = _T("uint");
            if (pValAddress)
            {
                const unsigned int* pUInt = reinterpret_cast<const unsigned int*>(pValAddress);
                if (!bHex)  { pValData->strVal = CUtil::StrFormat(_T("%u"), *pUInt);}
                else        { pValData->strVal = CUtil::StrFormat(_T("%x"), *pUInt);}
            }
            return true;
        }
        break;

    case asTYPEID_UINT64:
        {
            pValData->strTypeName = _T("uint");
            if (pValAddress)
            {
                const unsigned long long* pUInt = reinterpret_cast<const unsigned long long*>(pValAddress);
                if (!bHex) { pValData->strVal = CUtil::StrFormat(_T("%llu"), *pUInt);}
                else       { pValData->strVal = CUtil::StrFormat(_T("%I64x"), *pUInt);}
            }
            return true;
        }
        break;

    case asTYPEID_FLOAT:
        {
            pValData->strTypeName = _T("float");
            if (pValAddress)
            {
                const float* pFloat = reinterpret_cast<const float*>(pValAddress);
                pValData->strVal = CUtil::StrFormat(_T("%f"), *pFloat);
            }
            return true;
        }
        break;

    case asTYPEID_DOUBLE:
        {
            pValData->strTypeName = _T("double");
            if (pValAddress)
            {
                const double* pDouble = reinterpret_cast<const double*>(pValAddress);
                 pValData->strVal = CUtil::StrFormat(_T("%f"), *pDouble);
            }
            return true;
        }
        break;

    default:
        break;
    }
    }
    catch(...)
    {
        pValData->strVal = _T("Address Error");
        return true;
    }

    return false;
}

/**
 *  @brief  ユーザ定義クラス変数表示データ変換
 *  @param  [out] pValData      変数表示データ
 *  @param  [in]  iTypeId
 *  @param  [in]  pValAddress
 *  @retval String型のId     
 *  @note
 */
bool CScriptEngine::ConvUserVarData(VAL_DATA* pValData, 
                                    int iTypeId, 
                                    const void* pValAddress, 
                                    CScriptObject* pScriptObject)
{
    ID_VALFUNC_MAP::iterator iteMap;
    NAME_VALFUNC_MAP::iterator iteMapName;
    std::string strName;
    asIObjectType* pType;
    bool bRet = false;

    iteMap = m_mapTypeFunc.find(iTypeId);

    pType  = m_pEngine->GetObjectTypeById(iTypeId);
    if (pType == NULL)
    {
        return false;
    }

    if (pValAddress == NULL)
    {
        pValData->strTypeName = CUtil::CharToString(pType->GetName());
        pValData->strVal = _T("NULL");
        return true;
    }

    //----------------------
    //TODO:変換関数キャッシュ
    //----------------------
    HMODULE hModule;
    MockScript::FUNC_GetValData getValData;

    std::map<StdString, CSystemLibrary::ADDIN_DATA>::iterator iteAddinMap;

    int iRet;

    std::vector<StdChar> lstStr;
    lstStr.resize(1024);

    for(iteAddinMap  = SYS_LIBRARY->m_mapScriptAddins.begin();
        iteAddinMap != SYS_LIBRARY->m_mapScriptAddins.end();
        iteAddinMap++)
    {
        hModule = iteAddinMap->second.hModule;


        getValData = reinterpret_cast<MockScript::FUNC_GetValData>
                          (::GetProcAddress(hModule, "GetValData"));


        iRet = getValData(&lstStr[0], lstStr.size(), 
                         iTypeId, pValAddress, pScriptObject);

        if(iRet == -1)
        {
            continue;
        }
        else if(iRet != 0)
        {
            lstStr.resize(iRet + 1);
            iRet = getValData(&lstStr[0], lstStr.size(), 
                             iTypeId, pValAddress, pScriptObject);

            if(iRet != 0)
            {
                continue;
            }
        }
        

        if (VAL_DATA_CONV::StrToValData(&lstStr[0], NULL, pValData))
        {
            return true;
        }
    }
    //----------------------

    if (iteMap == m_mapTypeFunc.end())
    {

        if (!(pType->GetFlags() & asOBJ_TEMPLATE))
        {
            return false;
        }


        strName = pType->GetName();
        iteMapName = m_mapTemplateFunc.find(strName);
        if (iteMapName != m_mapTemplateFunc.end())
        {
            bRet = iteMapName->second(pValData, pValAddress, pScriptObject);
        }
    }
    else
    {
        bRet = iteMap->second(pValData, pValAddress, pScriptObject);
    }
    return bRet;
}


void CScriptEngine::_SetupCalc()
{
 	m_pInputCalcEngine = asCreateScriptEngine(ANGELSCRIPT_VERSION);
    m_pInputCalcEngine->RegisterGlobalProperty("double m_dCalcVal", &m_dCalcVal);
}

bool CScriptEngine::ExecuteString(double* pVal, const StdString& code)
{

    int iLength = SizeToInt(code.length());
    StdString strVal;
    StdString strRet;
    static StdString strValSet = _T("01234567890.");
    StdChar c;
    bool bVal = false;
    bool bExp = false;
    for(int iPos = 0; iPos < iLength; iPos++)
    {
        c = code.at(iPos);
        if (!bVal)
        {
            if ( (c >= _T('0')) &&
                 (c <= _T('9')))
            {
                strVal += c;
                bVal = true;
            }
            else if ( c == _T('.'))
            {
                strVal += c;
                bVal = true;
            }
            else
            {
                strRet += c;
            }
        }
        else
        {
            if ( (c >= _T('0')) &&
                 (c <= _T('9')))
            {
                strVal += c;
                bExp = false;
            }
            else if ( c == _T('.'))
            {
                strVal += c;
                bExp = false;
            }
            else if ( c == _T('e'))
            {
                strVal += c;
                bExp = true;
            }
            else if (( c == _T('+')) && bExp)
            {
                strVal += c;
                bExp = false;
            }
            else if (( c == _T('-')) && bExp)
            {
                strVal += c;
                bExp = false;
            }
            else
            {
                strRet += _T("double(");
                strRet += strVal;
                strRet += _T(")");
                strRet += c;
                strVal.clear();
                bExp = false;
                bVal = false;
            }
        }
    }
    if (bVal)
    {
        strRet += _T("double(");
        strRet += strVal;
        strRet += _T(")");
    }

    std::string str = CUtil::StringToChar(strRet);

    int iRet;
    iRet = _ExecuteString(pVal, m_pInputCalcEngine, str.c_str());
    return (iRet == asEXECUTION_FINISHED);
}
	
int CScriptEngine::_ExecuteString(double* pVal, asIScriptEngine *engine, const char *code)
{
	return _ExecuteString(pVal, engine, code, 0, asTYPEID_VOID, 0, 0);
}

int CScriptEngine::_ExecuteString(double* pVal, 
                                   asIScriptEngine *engine, 
                                   const char *code, 
                                   void *ref, 
                                   int refTypeId, 
                                   asIScriptModule *mod, 
                                   asIScriptContext *ctx)
{
	// Wrap the code in a function so that it can be compiled and executed
	std::string funcCode = " ExecuteString() { m_dCalcVal = \n";
	funcCode += code;
	funcCode += "\n;}";

	// Determine the return type based on the type of the ref arg
	funcCode = engine->GetTypeDeclaration(refTypeId, true) + funcCode;

	// GetModule will free unused types, so to be on the safe side we'll hold on to a reference to the type
	asIObjectType *type = 0;
	if( refTypeId & asTYPEID_MASK_OBJECT )
	{
		type = engine->GetObjectTypeById(refTypeId);
		if( type )
			type->AddRef();
	}

	// If no module was provided, get a dummy from the engine
	asIScriptModule *execMod = mod ? mod : engine->GetModule("ExecuteString", asGM_ALWAYS_CREATE);

	// Now it's ok to release the type
	if( type )
		type->Release();

	// Compile the function that can be executed
	asIScriptFunction *func = 0;
	int r = execMod->CompileFunction("ExecuteString", funcCode.c_str(), -1, 0, &func);
	if( r < 0 )
		return r;

	// If no context was provided, request a new one from the engine
	asIScriptContext *execCtx = ctx ? ctx : engine->CreateContext();
	r = execCtx->Prepare(func);
	if( r < 0 )
	{
		func->Release();
		if( !ctx ) execCtx->Release();
		return r;
	}

	// Execute the function
	r = execCtx->Execute();

	// Unless the provided type was void retrieve it's value
	if( ref != 0 && refTypeId != asTYPEID_VOID )
	{
		if( refTypeId & asTYPEID_OBJHANDLE )
		{
			// Expect the pointer to be null to start with
			assert( *reinterpret_cast<void**>(ref) == 0 );
			*reinterpret_cast<void**>(ref) = *reinterpret_cast<void**>(execCtx->GetAddressOfReturnValue());
			engine->AddRefScriptObject(*reinterpret_cast<void**>(ref), engine->GetObjectTypeById(refTypeId));
		}
		else if( refTypeId & asTYPEID_MASK_OBJECT )
		{
			// Expect the pointer to point to a valid object
			assert( *reinterpret_cast<void**>(ref) != 0 );
			engine->AssignScriptObject(ref, execCtx->GetAddressOfReturnValue(), engine->GetObjectTypeById(refTypeId));
		}
		else
		{
			// Copy the primitive value
			memcpy(ref, execCtx->GetAddressOfReturnValue(), engine->GetSizeOfPrimitiveType(refTypeId));
		}
	}

    *pVal = m_dCalcVal;

	// Clean up
	func->Release();
	if( !ctx ) execCtx->Release();

	return r;
}




//*******************************
//             TEST
//*******************************
#ifdef _DEBUG


void TEST_CScriptEngine()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    CScriptEngine Engine;

    double dVal;
    StdString str;
    bool bRet;
      
    str = _T("1+1");
    bRet = Engine.ExecuteString(&dVal, str);
    STD_ASSERT_DBL_EQ(dVal, 2.0); 
    STD_ASSERT( bRet);

    str = _T("1 + 1");
    bRet =Engine.ExecuteString(&dVal, str);
    STD_ASSERT_DBL_EQ(dVal, 2.0); 
    STD_ASSERT( bRet );

    str = _T("2.0 / 3.0");
    bRet =Engine.ExecuteString(&dVal, str);
    STD_ASSERT_DBL_EQ(dVal, 2.0 / 3.0); 
    STD_ASSERT( bRet );

    str = _T("2 / 3");
    bRet =Engine.ExecuteString(&dVal, str);
    STD_ASSERT_DBL_EQ(dVal, 2.0 / 3.0); 
    STD_ASSERT( bRet );

    str = _T("2 / a");
    bRet = Engine.ExecuteString(&dVal, str);
    STD_ASSERT( !bRet );

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}

#endif //_DEBUG
