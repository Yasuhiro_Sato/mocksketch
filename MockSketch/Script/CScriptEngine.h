/**
 * @brief			CScriptEngineヘッダーファイル
 * @file			CScriptEngine.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef _SCRIPT_ENGINE_H_
#define _SCRIPT_ENGINE_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/

/*---------------------------------------------------*/
/*  class                                            */
/*---------------------------------------------------*/
class asIScriptEngine;
class asIScriptGeneric;
class asIScriptContext;
class asIScriptModule;
class asIThreadManager;
class CDrawingScriptBase;
class CDrawingParts;
class CDrawingField;
class CDrawingElement;
class CScriptObject;
struct asSMessageInfo;
struct VAL_DATA;



/**
 * @class   CScriptEngine
 * @brief   スクリプトエンジン
 */
class CScriptEngine
{
public:
    CScriptEngine();

    virtual ~CScriptEngine();

    asIScriptEngine* GetEngine();

    static void MessageCallback(const asSMessageInfo *msg, void *param);

    static void ReleaseTextConvMap();

    //!< ユーザ定義クラス変数表示データ変換
    bool ConvVarData(VAL_DATA* pValData, 
                     LPCTSTR strVarName,
                    int iTypeId, 
                    const void* pValAddress, 
                    CScriptObject* pScriptObject);

    //!< テンプレートID
    int  GetTemplateId(){return m_iTemplateId;}

    //!< テンプレート追加
    void  AddTemplateId(){m_iTemplateId++;}

    static void PrintScript(const char * lpszFormat, ...);

    bool ExecuteString(double* pVal, const StdString& code);

    //=============
    //組み込み関数
    //=============
    //!< 
    static void PrintString(std::string str);
    static void PrintStringW(StdString str);
    static StdString ConvStr(std::string &str);
    static CDrawingScriptBase* GetDrawingScriptBase();
    static CDrawingParts* GetDrawingParts();
    static CDrawingField* GetDrawingField();
    static CDrawingElement* GetDrawingElement();
    static void Assert(asIScriptGeneric *gen);

    
    static void Redraw();


protected:
    void ConfigureEngine(asIScriptEngine*  pEngine);

    void ConfigureTemplate();

    //!< ディフォルト型変数データ変換
    bool ConvVarDefaultData(VAL_DATA* pValData, int iTypeId, const void* pValAddress);

    //!< ユーザ定義クラス変数表示データ変換
    bool ConvUserVarData(VAL_DATA* pValData, int iTypeId, const void* pValAddress, CScriptObject* pScriptObject);

    void _SetupCalc();

    int _ExecuteString(double* pVal, 
                       asIScriptEngine *engine, 
                       const char *code);

    int _ExecuteString(double* pVal, 
                        asIScriptEngine *engine, 
                        const char *code, 
                        void *ref, 
                        int refTypeId, 
                        asIScriptModule *mod, 
                        asIScriptContext *ctx);


protected:
    //!< スクリプトエンジン
    asIScriptEngine*  m_pEngine;

    //!< 入力値計算用スクリプトエンジン
    asIScriptEngine*  m_pInputCalcEngine;

    //TypeIIdと表示関数
    std::map<int, bool (CALLBACK*)(VAL_DATA*, const void*, void*) > m_mapTypeFunc;

    std::map<std::string, bool (CALLBACK*)(VAL_DATA*, const void*, void*) >  m_mapTemplateFunc;

    asIThreadManager* m_pThreadManager;

    //テンプレートID
    int m_iTemplateId;

    double m_dCalcVal;
};


#endif //_SCRIPT_ENGINE_H_
