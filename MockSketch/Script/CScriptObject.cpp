
#include "stdafx.h"
#include <angelscript.h>
#include <mutex>
#include "Utility/CUtility.h"
#include "./CScriptObject.h"
#include "./CModule.h"
#include "./CExecCtrl.h"
#include "./TCB.h"
#include "./CScriptEngine.h"
#include "./CScriptObject.h"
#include "DrawingObject/CDrawingScriptBase.h"
#include "DefinitionObject/ObjectDef.h"
#include "MockSketch.h"

#define POP_TEST (0)
#ifdef _DEBUG
#undef new 
#define new DEBUG_NEW
#endif 


CScriptObject::CScriptObject(CDrawingScriptBase *pObj):
        m_pModule(0),
        m_psExecObject(0),
        iExecPri(0),
        m_pCurrentTcb(NULL),
        m_pDrawingScript(pObj)
{
    m_pExecCtrl = THIS_APP->GetExecCtrl();
}


CScriptObject::~CScriptObject()
{
    Release();
}

void CScriptObject::Release()
{

#ifdef _DEBUG
    DB_PRINT(_T("CScriptObject::Release %I64x\n"), this);
#endif
    if(m_psExecObject)
    {
        int iRet;
        iRet =m_psExecObject->Release();
        if (iRet == 0)
        {
            m_psExecObject = NULL;
        }
    }
}

bool CScriptObject::IsInit()
{
    return m_bInit;
}

bool CScriptObject::PrepareExec(asIScriptContext* pContext)
{
   if (m_bInit)
   {
       return true;
   }

   DBG_ASSERT(!m_pDrawingScript);

   std::shared_ptr<CObjectDef> def =  m_pDrawingScript->GetObjectDef().lock() ;

   DBG_ASSERT(!def);
    
   if (!def)
   {
       return false;
   }

   //===============
   // 再コンパイル
   //===============
   COMPILE_STS sts;

   sts = def->GetCompileStatus();
   CProperty* pProp;
   pProp = def->GetProperty();

   if (sts == CS_ERROR)
   {
       return false;
   }
   else if ((sts == CS_NONE) ||
            (sts == CS_CHANGED) ||
            (sts == CS_NOT_FIND))
   {
        if (!pProp->bAutoCompile)
        {
            return false;
        }

        bool bDebug = false;
        if ( pProp->eDebug == EXEC_COMMON::EDB_DEBUG)
        {
            bDebug = true;
        }

        bool bRet;
        bRet = def->Compile(bDebug, false);

        if (!bRet)
        {
            return false;
        }
       sts = def->GetCompileStatus();
   }

   if ((sts != CS_SUCCESS) &&
       (sts != CS_SUCCESS_D))
   {
        return false;
   }
   //===============

   if (!m_pModule)
   {
       return false;
   }

   int iRet;
   iRet = _Initialize(pContext);

   if (iRet == asSUCCESS)
   {
       return true;
   }

   return false;
}

int  CScriptObject::_Initialize(asIScriptContext* pContext)
{
    asDWORD dwRet = -1;

    asIScriptModule* pMod = m_pModule->m_psModule;
    if (!pMod)
    {
        return asNO_MODULE;
    }


    //TODO:実行中の排他

    EXEC_COMMON::E_THREAD_TYPE eThreadType = m_pDrawingScript->GetExecThreadType();

    asIScriptEngine* pEngine = pContext->GetEngine();

    asIScriptFunction *pExecClassConstructor;
    pExecClassConstructor = m_pModule->GetExecObjectConstructor();

    if (!pExecClassConstructor)
    {
        return asNO_FUNCTION;
    }

    //コンテキストにユーザーデータ登録
    pContext->SetUserData(m_pDrawingScript);

  
    //コンストラクタ実行
    int r;
    r = pContext->Prepare(pExecClassConstructor);
    if (r < 0)
    {
        return asCONTEXT_NOT_PREPARED;
    }

    r = pContext->Execute();

    if (r != asEXECUTION_FINISHED)
    {
        return asCONTEXT_NOT_FINISHED;
    }
  
    m_psExecObject = *(asIScriptObject**)pContext->GetAddressOfReturnValue();

    m_psExecObject->AddRef();

    int iTypeId =  m_psExecObject->GetTypeId();
    asIObjectType *type = pEngine->GetObjectTypeById(iTypeId);

    using namespace EXEC_COMMON;

    m_lstMethod[METHOD_DRAW]            = type->GetMethodByDecl("int Draw(DrawingView@)");
    m_lstMethod[METHOD_ON_CHANGE_PROPERTY]      = type->GetMethodByDecl("int OnEdit_ChangeProperty(string& in)");
    m_lstMethod[METHOD_ON_INIT_NODE_MARKER]     = type->GetMethodByDecl("int OnEdit_InitNodeMarker(NodeMarker@)");
    m_lstMethod[METHOD_ON_SELECT_NODE_MARKER]   = type->GetMethodByDecl("int OnEdit_SelectNodeMarker(NodeMarker@, string& in)");
    m_lstMethod[METHOD_ON_MOVE_NODE_MARKER]     = type->GetMethodByDecl("int OnEdit_MoveNodeMarker(NodeMarker@, string& in, POINT2D& in, MOUSE_MOVE_POS& in)");
    m_lstMethod[METHOD_ON_RELEASE_NODE_MARKER]  = type->GetMethodByDecl("int OnEdit_ReleaseNodeMarker(NodeMarker@)");
    m_lstMethod[METHOD_ON_CHANGE_NODE]   = type->GetMethodByDecl("int OnEdit_ChangeNode(string& in, NODE_CHANGE_TYPE)");

    m_bInit = true;
    return asSUCCESS;

}

int CScriptObject::Initialize(CModule *mod, bool bThread)
{
    if (m_bInit)
    {
        if (m_pModule == mod)
        {
            return asSUCCESS;
        }
    }
    m_pModule = mod;

    asDWORD dwRet = -1;

    asIScriptModule* pMod = m_pModule->m_psModule;
    if (!pMod)
    {
        return asNO_MODULE;
    }


    //TODO:実行中の排他

    EXEC_COMMON::E_THREAD_TYPE eThreadType = m_pDrawingScript->GetExecThreadType();

    asIScriptContext* pContext ;


    if (bThread)
    {
        pContext = m_pExecCtrl->GetContext(eThreadType);
    }
    else
    {
        pContext = m_pExecCtrl->GetNonThreadContext(eThreadType);
    }

    int iRet;
    iRet = _Initialize(pContext);

    return iRet;
}

CModule * CScriptObject::GetModule()
{
    //ビルド済みのモジュールを割り当てること
    return m_pModule;
}


int CScriptObject::ExecMethod(asIScriptFunction* pFunc, TCB* pTcb)
{
    int iRet;
    EXEC_COMMON::E_THREAD_TYPE eThreadType = m_pDrawingScript->GetExecThreadType();
    iRet = CScriptObject::ExecFunc(pFunc, pTcb, eThreadType, m_psExecObject);

    return iRet;
}


int CScriptObject::ExecFunc(asIScriptFunction* pFunc, TCB* pTcb, 
                    EXEC_COMMON::E_THREAD_TYPE eThreadType,
                    asIScriptObject* pObject)
{
    int r;

    //m_pCurrentTcb = pTcb;  
    //CExecCtrl* pCtrl = THIS_APP->GetExecCtrl();

    //asEContextState eSts;
    asIScriptContext* pContext;
    asIScriptEngine * pEngine;


    //STD_ASSERT(pTcb);
    if (!pTcb)
    {
        return EX_ERROR;
    }

    pContext = pTcb->GetCurrentContext();

    //STD_ASSERT(pContext);
    if (!pContext)
    {
        return EX_ERROR;
    }

    pEngine = pContext->GetEngine();

   if (!pTcb->IsResume())
    {
        r = pContext->Prepare(pFunc)      ; assert(r >= 0);

        if (pObject)
        {
            r = pContext->SetObject(pObject); assert(r >= 0);
        }

        r = pContext->SetArgObject(0, pTcb); assert(r >= 0);
    }


	asUINT gcSize1, gcSize2, gcSize3;
	pEngine->GetGCStatistics(&gcSize1);

   r = pContext->Execute();      

	pEngine->GetGCStatistics(&gcSize2);


    int iRet;
    switch(r)
    {
    case asEXECUTION_FINISHED:
        iRet = EX_FINISH;
        break;
    case asEXECUTION_SUSPENDED:
        iRet = EX_SUSPEND;
        break;
    case asEXECUTION_ABORTED:
    case asEXECUTION_EXCEPTION:
    case asEXECUTION_PREPARED:
    case asEXECUTION_UNINITIALIZED:
    case asEXECUTION_ACTIVE:
    case asEXECUTION_ERROR:
    default:
        iRet = EX_ERROR;
        break;
    }

	if( gcSize2 > gcSize1 )
	{
		pEngine->GarbageCollect(asGC_FULL_CYCLE | asGC_DESTROY_GARBAGE);

		// Determine how many objects were destroyed
 		pEngine->GetGCStatistics(&gcSize3);
	}
    pEngine->GarbageCollect(asGC_ONE_STEP | asGC_DETECT_GARBAGE);

    return iRet;
}


asIScriptFunction*  CScriptObject::GetMethod(const char* pMethodName)
{
    if (!m_psExecObject)
    {
        return NULL;
    }

    int iTypeId =  m_psExecObject->GetTypeId();
    asIScriptEngine* pEngine;
    pEngine = m_psExecObject->GetEngine();;
    asIObjectType *type = pEngine->GetObjectTypeById(iTypeId);
    asIScriptFunction* pFunc;
    pFunc = type->GetMethodByDecl(pMethodName);

    //TODO:引数チェック
    return pFunc;
}

asIScriptFunction*  CScriptObject::_PrepareMethod(EXEC_COMMON::E_SCRIPT_METHOD eMethod,
                                                  asIScriptContext* pContext)
{

    EXEC_COMMON::E_THREAD_TYPE eThreadType = m_pDrawingScript->GetExecThreadType();
    //asIScriptContext* pContext = m_pExecCtrl->GetNonThreadContext(eThreadType);
 
    if (!pContext)
    {
        return NULL;
    }

    asEContextState sts = pContext->GetState() ;

    if (sts == asEXECUTION_ACTIVE)
    {
        //実行中不可
        return NULL;
    }

    if(!PrepareExec(false))
    {
        return NULL;
    }

    if (!m_psExecObject)
    {
        return NULL;
    }

    //コンテキストにユーザーデータ登録
    pContext->SetUserData(m_pDrawingScript);

    asIScriptFunction* pFunction = m_lstMethod[eMethod];

    return pFunction;
}

/**
 *  @brief   コンテキストのプロパティ取得
 *  @param   [out] strScript        スクリプトタブ名
 *  @param   [out] strFunction      
 *  @param   [out] strRootFunction
 *  @param   [in]  pContext
 *  @retval  なし
 *  @note    ASスクリプト実行
 */
/*
bool CScriptObject::GetContextProperty(std::string* pSectionName, 
                                        std::string* pFunctionName, 
                                        std::string* pRootFunctionName,
                                        const CScriptObject* pObj)
{
    asIScriptContext* ctx;
    ctx = pContext->m_psContext;
    int iStackSize = ctx->GetCallstackSize();

    if (iStackSize <= 0)
    {
        return false;
    }

    asIScriptFunction *pFunc = ctx->GetFunction(0);
    asIScriptFunction *pRootFunc = ctx->GetFunction(iStackSize - 1);

    if (pSectionName)
    {
        *pSectionName = pFunc->GetScriptSectionName();
    }

    if (pFunctionName)
    {
        *pFunctionName = pFunc->GetName();
    }

    if (pRootFunctionName)
    {
        *pRootFunctionName = pRootFunc->GetName();
    }

    return true;
}
*/

/**
 *  @brief   描画実行
 *  @param   なし
 *  @retval  なし
 *  @note    ASスクリプト実行
 */
int CScriptObject::DrawScript(CDrawingView* pView)
{
    using namespace EXEC_COMMON;

    std::lock_guard<std::mutex> lock(m_Mtx);
    EXEC_COMMON::E_THREAD_TYPE eThreadType = m_pDrawingScript->GetExecThreadType();
    asIScriptContext* pContext = m_pExecCtrl->GetNonThreadContext(eThreadType);
    asIScriptFunction* pFunction;

    int iRet = FSTS_ERROR;
    pFunction = _PrepareMethod(METHOD_DRAW, pContext);
    if (!pFunction)
    {
        return iRet;
    }


    if(pContext->Prepare(pFunction) )
    {
        pContext->SetObject(m_psExecObject);
        pContext->SetArgObject(0, pView);
       iRet = pContext->Execute();
    }

    return iRet;
}

/**
 *  @brief   プロパティ変更イベント
 *  @param   [in] strPropName プロパティ名
 *  @retval  E_METHOD_STATE
 *  @note    ASスクリプト実行
 */
int CScriptObject::OnChangeProperty(StdString strPropName)
{
    using namespace EXEC_COMMON;

    std::lock_guard<std::mutex> lock(m_Mtx);
    EXEC_COMMON::E_THREAD_TYPE eThreadType = m_pDrawingScript->GetExecThreadType();
    asIScriptContext* pContext = m_pExecCtrl->GetNonThreadContext(eThreadType);
    asIScriptFunction* pFunction;

    int iRet = FSTS_ERROR;
    pFunction = _PrepareMethod(METHOD_ON_CHANGE_PROPERTY, pContext);
    if (!pFunction)
    {
        return iRet;
    }


    if(pContext->Prepare(pFunction) )
    {
        pContext->SetObject(m_psExecObject);
        pContext->SetArgObject(0, &strPropName);
       iRet = pContext->Execute();
    }

    return iRet;
}

/**
 *  @brief   ドラッグ用マーカ初期化イベント
 *  @param   [in] pMarker 
 *  @retval  E_METHOD_STATE
 *  @note    ASスクリプト実行
 */
int CScriptObject::OnInitNodeMarker(CNodeMarker* pMarker)
{
    using namespace EXEC_COMMON;

    std::lock_guard<std::mutex> lock(m_Mtx);
    EXEC_COMMON::E_THREAD_TYPE eThreadType = m_pDrawingScript->GetExecThreadType();
    asIScriptContext* pContext = m_pExecCtrl->GetNonThreadContext(eThreadType);
    asIScriptFunction* pFunction;

    int iRet = FSTS_ERROR;
    pFunction = _PrepareMethod(METHOD_ON_INIT_NODE_MARKER, pContext);
    if (!pFunction)
    {
        return iRet;
    }


    if(pContext->Prepare(pFunction) )
    {
        pContext->SetObject(m_psExecObject);
        pContext->SetArgObject(0, pMarker);
       iRet = pContext->Execute();
    }
    return iRet;
}

/**
 *  @brief   ドラッグ用マーカ選択イベント
 *  @param   [in] pMarker 
 *  @param   [in] iMarkerId 
 *  @retval  E_METHOD_STATE
 *  @note    ASスクリプト実行
 */
int CScriptObject::OnSelectNodeMarker(CNodeMarker* pMarker, 
                                      StdString strMarkerId)
{
    using namespace EXEC_COMMON;

    std::lock_guard<std::mutex> lock(m_Mtx);
    EXEC_COMMON::E_THREAD_TYPE eThreadType = m_pDrawingScript->GetExecThreadType();
    asIScriptContext* pContext = m_pExecCtrl->GetNonThreadContext(eThreadType);
    asIScriptFunction* pFunction;

    int iRet = FSTS_ERROR;
    pFunction = _PrepareMethod(METHOD_ON_SELECT_NODE_MARKER, pContext);
    if (!pFunction)
    {
        return iRet;
    }


    if(pContext->Prepare(pFunction) )
    {
        pContext->SetObject(m_psExecObject);
        pContext->SetArgObject(0, pMarker);
        pContext->SetArgObject(1, &strMarkerId);
        iRet = pContext->Execute();
    }
    return iRet;
}

/**
 *  @brief   ドラッグ用マーカ移動イベント
 *  @param   [in] pMarker 
 *  @param   [in] iMarkerId 
 *  @param   [in] ptRet 
 *  @retval  E_METHOD_STATE
 *  @note    ASスクリプト実行
 */
int CScriptObject::OnMoveNodeMarkerId(CNodeMarker* pMarker,
                            StdString strMarkerId,
                            const POINT2D& ptRet,
                            MOUSE_MOVE_POS posMouse)
{
    using namespace EXEC_COMMON;

    std::lock_guard<std::mutex> lock(m_Mtx);
    EXEC_COMMON::E_THREAD_TYPE eThreadType = m_pDrawingScript->GetExecThreadType();
    asIScriptContext* pContext = m_pExecCtrl->GetNonThreadContext(eThreadType);
    asIScriptFunction* pFunction;

    int iRet = FSTS_ERROR;
    pFunction = _PrepareMethod(METHOD_ON_MOVE_NODE_MARKER, pContext);
    if (!pFunction)
    {
        return iRet;
    }

    POINT2D* pPt = const_cast<POINT2D*>(&ptRet);

    if(pContext->Prepare(pFunction) )
    {
        pContext->SetObject(m_psExecObject);
        pContext->SetArgObject(0, pMarker);
        pContext->SetArgObject(1, &strMarkerId);
        pContext->SetArgObject(2, pPt);
        iRet = pContext->Execute();
    }
    return iRet;

}

/**
 *  @brief   ドラッグ用マーカ開放イベント
 *  @param   [in] pMarker 
 *  @retval  E_METHOD_STATE
 *  @note    ASスクリプト実行
 */
int CScriptObject::OnReleaseNodeMarker(CNodeMarker* pMarker)
{
    using namespace EXEC_COMMON;

    std::lock_guard<std::mutex> lock(m_Mtx);
    EXEC_COMMON::E_THREAD_TYPE eThreadType = m_pDrawingScript->GetExecThreadType();
    asIScriptContext* pContext = m_pExecCtrl->GetNonThreadContext(eThreadType);
    asIScriptFunction* pFunction;

    int iRet = FSTS_ERROR;
    pFunction = _PrepareMethod(METHOD_ON_RELEASE_NODE_MARKER, pContext);
    if (!pFunction)
    {
        return iRet;
    }


    if(pContext->Prepare(pFunction) )
    {
        pContext->SetObject(m_psExecObject);
        pContext->SetArgObject(0, pMarker);
        iRet = pContext->Execute();
    }
    return iRet;
}

/**
 *  @brief   ドラッグ用マーカ変更イベント
 *  @param   [in] pMarker 
 *  @retval  E_METHOD_STATE
 *  @note    ASスクリプト実行
 */
int CScriptObject::OnChangeNode(StdString strMarkerId, E_NODE_CHANGE_TYPE eType)
{
    using namespace EXEC_COMMON;

    std::lock_guard<std::mutex> lock(m_Mtx);
    EXEC_COMMON::E_THREAD_TYPE eThreadType = m_pDrawingScript->GetExecThreadType();
    asIScriptContext* pContext = m_pExecCtrl->GetNonThreadContext(eThreadType);
    asIScriptFunction* pFunction;

    int iRet = FSTS_ERROR;
    pFunction = _PrepareMethod(METHOD_ON_CHANGE_NODE, pContext);
    if (!pFunction)
    {
        return iRet;
    }

    if(pContext->Prepare(pFunction) )
    {
        pContext->SetObject(m_psExecObject);
        pContext->SetArgObject(0, &strMarkerId);
        pContext->SetArgDWord(1, eType);
        iRet = pContext->Execute();
    }
    return iRet;
}

/**
 * @brief   .区切りの名称リストを取得する
 * @param   [in]    strVarName 変数名
 * @param   [out]   pLstName   分解後の変数
 * @retval  なし
 * @note	
 */
void CScriptObject::GetNameList(std::string strVarName, std::vector<std::string>* pLstName)
{
    size_t iPos = strVarName.find_first_of(".");
    if (iPos == std::string::npos)
    {
        pLstName->push_back(strVarName);
        return;
    }
    else
    {
        std::string strFirst  = strVarName.substr(0, iPos);
        std::string strSecond = strVarName.substr(iPos + 1, strVarName.size());
        pLstName->push_back( strFirst);
        GetNameList( strSecond, pLstName);
    }
}

/**
 * @brief   変数データの取得
 * @param   [out]    valData     変数データ
 * @param   [in]     strVarName  変数名
 * @retval  false   該当なし
 * @note	
 */
bool CScriptObject::GetVarData(VAL_DATA* pValData, std::string strVarName)
{
    int iStackLevel = 0;

    int     iVarIndex;
    int     iTypeId;
    void*   pValAddress = NULL;
    
    pValData->bValid = false;
    pValData->lstVal.clear();

    VAL_DATA  valTmp;
    VAL_DATA* pValTmp;

    if (!m_pCurrentTcb)
    {
        return false;
    }
    asIScriptContext* pContext;
    pContext = m_pCurrentTcb->GetCurrentContext();

    //ForDebug
    //PrintValue(strVarName, m_psContext);

    std::vector<std::string> lstName;
    GetNameList(strVarName, &lstName);
    iVarIndex = -1;

    if (lstName.size() == 0)
    {
        return false;
    }
    else if(lstName.size() == 1)
    {
        pValTmp = pValData;
    }
    else
    {
        pValTmp = &valTmp;
    }

    //今のところモジュール外のグローバル変数は考えない

    asIScriptFunction *pFunc = pContext->GetFunction();
    if( !pFunc )
    {
        return false;
    }

    //--------------
    // 変数名検索
    //--------------
    // We start from the end, in case the same name is reused in different scopes

    int iVerCnt = pFunc->GetVarCount();
    std::string strVar;

    asIScriptEngine* pEngine = pContext->GetEngine();


    // 関数内
    for( int iCnt = iVerCnt; iCnt-- > 0; )
    {
        if( pContext->IsVarInScope(iCnt) && 
            lstName[0] == pContext->GetVarName(iCnt) )
        {
            pValAddress = pContext->GetAddressOfVar(iCnt);
            iTypeId = pContext->GetVarTypeId(iCnt);
            break;
        }
    }

    if (!pValAddress && pFunc->GetObjectType())
    {
        if( strVarName == "this" )
        {
            pValAddress = pContext->GetThisPointer();
            iTypeId = pContext->GetThisTypeId();
        }
        else
        {
            asIObjectType* pObjType;
            pObjType = pEngine->GetObjectTypeById(pContext->GetThisTypeId());
            iVerCnt = pObjType->GetPropertyCount();
            for (int iCnt = 0; iCnt < iVerCnt; iCnt++)
            {
                const char *propName = 0;
                int iOffset = 0;
                bool isReference = 0;
                pObjType->GetProperty(iCnt, &propName, &iTypeId, 0, &iOffset, &isReference);
                if( strVarName == propName )
                {
                    pValAddress = (void*)(((asBYTE*)pContext->GetThisPointer())+iOffset);
                    if( isReference )
                    {
                        //TODO: 参照の表示方法を考える
                        pValAddress = *(void**)pValAddress;
                    }
                    break;
                }
            }
        }
    }

    //グローバル変数を探す
    if (!pValAddress)
    {
        asIScriptModule* pMod;
        pMod = pEngine->GetModule(pFunc->GetModuleName(), asGM_ONLY_IF_EXISTS);
        if( pMod )
        {
            iVerCnt = pMod->GetGlobalVarCount();
            for (int iCnt = 0; iCnt < iVerCnt; iCnt++)
            {
                // TODO: Handle namespace too
                const char *varName = 0, *nameSpace = 0;
                pMod->GetGlobalVar(iCnt, &varName, &nameSpace, &iTypeId);
                if( strVarName == varName )
                {
                    pValAddress = pMod->GetAddressOfGlobalVar(iCnt);
                    break;
                }
            }
        }
    }

    if (!pValAddress)
    {
        return false;
    }

    StdString strVarName2;
    strVarName2 = CUtil::CharToString(strVarName.c_str());

    SetValChild(strVarName2.c_str(), iTypeId, pValAddress, pValTmp);


    if (lstName.size() == 1)
    {
        pValData->bValid = true;
        return true;
    }
    else
    {
        int iNameCnt = 1;
        int iNameCntMax = static_cast<int>(lstName.size());
        VAL_DATA* pValChild;
        VAL_DATA* pValNext;

        pValNext = pValTmp;

        for(iNameCnt =1; iNameCnt < iNameCntMax; iNameCnt++)
        {
            StdString strChildName = CUtil::CharToString(lstName[iNameCnt].c_str());
            pValChild = pValNext;

            pValNext = NULL;
            foreach(VAL_DATA& valChild, pValChild->lstVal)
            {
                if ( valChild.strName == strChildName)
                {
                    if(iNameCnt + 1 == iNameCntMax)
                    {
                        pValData->strName = strChildName;
                        pValData->bArray = valChild.bArray;
                        pValData->strTypeName = valChild.strTypeName;
                        pValData->strVal = valChild.strVal;
                        pValData->bValid = true;
                        pValData->lstVal = valChild.lstVal;
                        return true;
                    }
                    else
                    {
                        pValNext = &valChild;
                        break;
                    }
                }
            }

            if (pValNext == NULL)
            {
                return false;
            }
        }
    }
    return false;
}


/**
 * @brief   変数データの取得(子データ)
 * @param   [in]   strVarName  変数名
 * @param   [in]     iTypeId   型ID
 * @param   [in]    pAddress   変数アドレス
 * @param   [out]   pValData   変数データ
 * @retval  なし
 * @note	
 */
void CScriptObject::SetValChild(LPCTSTR strVarName, 
                                 int iTypeId, 
                                 const void* pAddress, 
                                 VAL_DATA* pValData)
{
    pValData->bArray = false;

    bool bRet;

    if (pAddress == NULL)
    {
        pValData->bValid = false;
        return;
    }

    bRet = THIS_APP->GetScriptEngine()->ConvVarData( pValData,
                                       strVarName,
                                       iTypeId, 
                                       pAddress, this);
    if (bRet)
    {
        //基本型の場合
        return ;
    }
    else
    {
        //基本型ではない場合

        pValData->strName = strVarName;
        asIObjectType* pType;
        pType = THIS_APP->GetScriptEngine()->GetEngine()->GetObjectTypeById(iTypeId);
    
        STD_ASSERT(pType != NULL);

        pValData->strVal = _T("{");

        pValData->strTypeName = CUtil::CharToString(pType->GetName());

        VAL_DATA valChild;
        void* pChildAddress;
        void* pNextAddress;

        int iPropMax;
        iPropMax = pType->GetPropertyCount();

        StdString strName;
        const char* strPropName;
        int   iChildTypeId;
        bool  bPrivate;
        int   iOffset;
        int   iRet;

        asIScriptEngine* pEngine;
        bool bReference = false;

        pEngine = THIS_APP->GetScriptEngine()->GetEngine();

        for (int iCnt = 0; iCnt < iPropMax; iCnt++)
        {

            iRet = pType->GetProperty(iCnt, 
                                        &strPropName, 
                                        &iChildTypeId, 
                                        &bPrivate, 
                                        &iOffset, 
                                        &bReference);
            if (iRet < 0)
            {
                STD_DBG(_T("GetProperty Error[%x]"), iRet);
                continue;
            }

            pNextAddress = (void*)(((asBYTE*)pAddress)+iOffset);

            strName = CUtil::CharToString(strPropName);

            bRet = false;


            if  (bReference)
            {
                pChildAddress = *(void**)pNextAddress;
            }
            else
            {
                pChildAddress = pNextAddress;
            }

            if(!(iChildTypeId & asTYPEID_SCRIPTOBJECT))
            {
                bRet = true;
                THIS_APP->GetScriptEngine()->ConvVarData(&valChild, 
                                                         strName.c_str(),
                                                         iChildTypeId, 
                                                         pChildAddress, 
                                                         this);

                pValData->lstVal.push_back(valChild);
            }
            else
            {
                SetValChild(strName.c_str(), iChildTypeId, pChildAddress, &valChild);
                pValData->lstVal.push_back(valChild);
            }
            pValData->strVal += valChild.strVal;
            pValData->strVal += _T(",");
        }
        pValData->strVal += _T("}");

    }
}


/**
 *  @brief   デバッグ用文字
 *  @param   [in] strSpc 
 *  @param   デバッグ用文字
 *  @retval  なし
 *  @note
 */
StdString CScriptObject::GetStr(StdString strSpc) const
{
    StdStringStream strRet;
    strSpc += _T("  ");
    strRet << StdFormat(_T("%s CScriptObject:%s Module:%llx DrawingScript:%llx")) 
        % strSpc.c_str()
        % m_strName.c_str()
        % m_pModule 
        % m_pDrawingScript;
    return strRet.str();
}



/**
 *  @brief   デバッグ用文字
 *  @param   なし
 *  @param   デバッグ用文字
 *  @retval     なし
 *  @note
 */
StdString CScriptObject::GetStr() const
{
    return GetStr(_T(""));
}