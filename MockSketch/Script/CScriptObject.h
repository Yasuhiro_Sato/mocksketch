
/**
 * @brief			CScriptObjectヘッダーファイル
 * @file			CScriptObject.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef _SCRIPT_OBJECTT_H_
#define _SCRIPT_OBJECTT_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/

#include <mutex>
#include <stack>

#include "VAL_DATA.h"
#include "ExecCommon.h"
#include "View/DRAWING_TYPE.h"


/*---------------------------------------------------*/
/*  class                                            */
/*---------------------------------------------------*/
class CModule;
class CExecCtrl;
class asIScriptEngine;
class asIScriptContext;
class asIScriptObject;
class asIScriptFunction;

class TCB;
class POINT2D;
class CDrawingScriptBase;
class CDrawingView;
class CNodeMarker;



// asEContextStateを参照
// 関数実行時の戻り値を再定義
enum E_FUNC_STATE
{
	FSTS_FINISHED      = 0,
	FSTS_SUSPENDED     = 1,
	FSTS_ABORTED       = 2,
	FSTS_EXCEPTION     = 3,
	FSTS_PREPARED      = 4,
	FSTS_UNINITIALIZED = 5,
	FSTS_ACTIVE        = 6,
	FSTS_ERROR         = 7
};



class CScriptObject
{

public:
	struct BreakPoint
	{
		BreakPoint(std::string f, int n, bool _func) : name(f), lineNbr(n), func(_func), needsAdjusting(true) {}
		std::string name;
		int         lineNbr;
		bool        func;
		bool        needsAdjusting;
	};

public:
    //CScriptObject(EXEC_COMMON::E_THREAD_TYPE eThreadType, CExecCtrl *ctrl);
    CScriptObject(CDrawingScriptBase *pObj);

    virtual ~CScriptObject();

    void CScriptObject::Release();

    int Initialize(CModule *mod);

    bool IsInit();

    CModule *GetModule();

    void Setup();
    int Setup(asIScriptFunction* pFunc, TCB* pTcb);


    static int ExecFunc(asIScriptFunction* pFunc, TCB* pTcb,
                        EXEC_COMMON::E_THREAD_TYPE eThreadType,
                        asIScriptObject* pObject = NULL);

    int ExecMethod(asIScriptFunction* pFunc, TCB* pTcb);

    asIScriptFunction*  GetMethod(const char* pMethodName);

    asIScriptObject*    GetObject(){return   m_psExecObject;}
    
    /*
    static bool GetContextProperty(std::string* pSectionName, 
                                std::string* pFunctionName, 
                                std::string* pRootFunctionName,
                                const CScriptObject* pObj);
                                */

    //----------


    int DrawScript(CDrawingView* pView);


    //!<プロパティ変更関数ID取得
    int OnChangeProperty(StdString strPropName);

    //!<ドラッグ用マーカ初期化
    int OnInitNodeMarker(CNodeMarker* pMarker);

    //!<ドラッグ用マーカ選択
    int OnSelectNodeMarker(CNodeMarker* pMarker, 
                           StdString strMarkerId);

    //!<ドラッグ用マーカ移動関数ID取得
    int OnMoveNodeMarkerId(CNodeMarker* pMarker,
                               StdString strMarkerId,
                               const POINT2D& ptRet,
                               MOUSE_MOVE_POS posMouse);

    //!<ドラッグ用マーカ開放関数ID取得
    int OnReleaseNodeMarker(CNodeMarker* pMarker);

    //!<ノード変更通知
    int OnChangeNode(StdString strMarkerId, E_NODE_CHANGE_TYPE eType);

    //!< .区切りの名称リスト取得
    static void GetNameList(std::string strVarName, std::vector<std::string>* pLstName);

    //!< 変数データの取得
    bool GetVarData(VAL_DATA* pValData, std::string strVarName);

    void SetValChild(LPCTSTR strVarName, 
                                 int iTypeId, 
                                 const void* pAddress, 
                                 VAL_DATA* pValData);

    bool PrepareExec(asIScriptContext* pContext);

    int  Initialize(CModule *mod, bool bExec);


    //-------------------
    // デバッグ用
    //-------------------
    virtual StdString GetStr(StdString strSpc) const;

    virtual StdString GetStr() const;

protected:
friend CExecCtrl;

    bool _RequestInterrupt(int iLevel);

    int  _Initialize(asIScriptContext* pContext);

    asIScriptFunction*  _PrepareMethod(EXEC_COMMON::E_SCRIPT_METHOD eMethod,
                                       asIScriptContext* pContext);

protected:
    CModule*                       m_pModule;
    CExecCtrl*                     m_pExecCtrl;
    asIScriptObject*               m_psExecObject;

    //実行インスタンス
    CDrawingScriptBase*            m_pDrawingScript;

    std::mutex m_Mtx;

    std::wstring m_strName;

    std::mutex mtx;
    int iExecPri;

    //実行関数保持
    asIScriptFunction* m_lstMethod[EXEC_COMMON::E_END_OF_METHOD];

    TCB*               m_pCurrentTcb;

    bool               m_bInit;
    //std::vector<BreakPoint> m_breakPoints;

};
#endif