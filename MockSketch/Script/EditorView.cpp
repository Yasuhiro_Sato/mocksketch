/**
* @brief        EditorVie実装ファイル
* @file	        EditorView.h
* @author       Yasuhiro Sato
* @date	        09-2-2009 23:59:08
* 
* @note	
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
* 
* @remarks	        
*			
*
* $
* $
* 
*/

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./EditorView.h"
#include "./CScript.h"
#include "./CModule.h"

#include "DefinitionObject/View/MockSketchDoc.h"

#include "System/CSystem.h"
#include "Utility/CUtility.h"
#include "DefinitionObject/CPartsDef.h"
#include "Script/CExecCtrl.h"
#include "System/CSystem.h"

#include "MockSketch.h"
#include "MainFrm.h"
#include "ExecCommon.h"
#include "View/CURRENT_BREAK_CONTEXT.h"
#include "MtEditor/MtEditorWindow.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
using namespace MT_EDITOR;

IMPLEMENT_DYNCREATE(CEditorView, CView)
BEGIN_MESSAGE_MAP(CEditorView, CView)
    ON_COMMAND(ID_EDIT_COPY, &CEditorView::OnEditCopy)
    ON_COMMAND(ID_EDIT_CUT, &CEditorView::OnEditCut)
    ON_COMMAND(ID_EDIT_PASTE, &CEditorView::OnEditPaste)
    ON_COMMAND(ID_EDIT_UNDO, &CEditorView::OnEditUndo)


    //<<<<<<<手動で追加
    ON_MESSAGE(WM_VIEW_UPDATE_NAME, OnUpdateName)
    ON_MESSAGE(WM_VIEW_SAVE_START, OnViewSaveStart)
    ON_MESSAGE(WM_VIEW_CLEAR_ALL, OnViewClear)

    ON_COMMAND(ID_MNU_TOGGLEBREAKPOINT      , &CEditorView::OnBreakpoint)
    ON_COMMAND(ID_MNU_STEP_IN               , &CEditorView::OnStepIn)
    ON_COMMAND(ID_MNU_STEP_OVER             , &CEditorView::OnStepOver)
    ON_COMMAND(ID_MNU_SCRIPT_EXEC           , &CEditorView::OnStepContinue)

    ON_COMMAND(ID_MNU_SCRIPT_EXEC_CURSOR    , &CEditorView::OnStepCursor)
    ON_COMMAND(ID_MNU_REMOVE_ALL_BREAKPOINTS, &CEditorView::OnClearBreakpoint)
    
    ON_COMMAND_RANGE(ID_MNU_SCRIPT_BUILD , ID_MNU_SCRIPT_PAUSE, &CEditorView::OnMenuBuild)


    //<<<<<<<手動で追加

    ON_WM_RBUTTONUP()
    ON_WM_CONTEXTMENU()
    ON_WM_DESTROY()
END_MESSAGE_MAP()

/**
*  @brief  コンストラクター.
*  @param  なし
*  @retval なし     
*  @note
*/
CEditorView::CEditorView():
m_pScriptData (NULL),
m_iCurLine    (-1),
m_iIndcationLine    (-1)
{
    THIS_APP->AddEditor(this);
    m_psEditor = new CMtEditorWindow;
}

/**
*  @brief  デストラクター.
*  @param  なし
*  @retval なし     
*  @note
*/
CEditorView::~CEditorView()
{
    THIS_APP->DelEditor(this);
    delete m_psEditor;
    m_psEditor = NULL;
}


/**
*  @brief  ドキュメントイメージ描画
*  @param  [in] pDC 描画に使用するデバイス コンテキストへのポインタ
*  @retval なし     
*  @note
*/
void CEditorView::OnDraw(CDC* pDC)
{
    CDocument* pDoc = GetDocument();
}

//------------------
// CEditorView 診断
//------------------
#ifdef _DEBUG
/**
 * @brief   オブジェクト内部状態チェック
 * @param   なし
 * @return  なし
 * @note     
 */
void CEditorView::AssertValid() const
{
	CView::AssertValid();

}

#ifndef _WIN32_WCE
/**
 * @brief   ダンプ処理
 * @param   [in]    dc	ダンプ用の診断ダンプ コンテキスト 
 * @return  なし
 * @note    
 */
void CEditorView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif

CMockSketchDoc* CEditorView::GetDocument() const // デバッグ以外のバージョンはインラインです。
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMockSketchDoc)));
	return (CMockSketchDoc*)m_pDocument;
}

#endif //_DEBUG



//======================== これを追加
/*
void CALLBACK CEditorView::OnMoveCarete(int nID,void *pData,size_t nLine,size_t nColumn)
{
    //char szDebug[256];
    //sprintf(szDebug,"CaretMoved (%d,%d)\n",nLine,nColumn);
    //OutputDebugString(szDebug);
}
*/
//======================== これを追加



/**
 *  @brief  設定更新
 *  @param  なし
 *  @retval true 成功
 *  @note
 */
bool CEditorView::UpdateFontSetting()
{

    bool bRet = true;


    if(!m_fnt.CreatePointFont(SYS_EDITOR->iFontSize,
                          SYS_EDITOR->strFontName.c_str()))
    {
        return false;
    }

    m_psEditor->SetFont(m_fnt);
    return true;
}

/**
 *  @brief  設定更新
 *  @param  なし
 *  @retval true 成功
 *  @note
 */
bool CEditorView::UpdateVisibleSetting()
{

    //行頭の幅
    int iLineNumWidth = 50;
    int iVisivle = 0;
    int iVisibleUnderLine = (SYS_EDITOR->bVisibleUnderLine? TRUE : FALSE);

    if (!SYS_EDITOR->bVisibleLineNum)
    {
        iLineNumWidth = 20;
    }

    /*
    iVisivle |= (SYS_EDITOR->bVisibleSpace   ?    (EDM_HALF_SPACE | EDM_FULL_SPACE) : 0);
    iVisivle |= (SYS_EDITOR->bVisibleTab     ?    EDM_TAB  : 0);
    iVisivle |= (SYS_EDITOR->bVisibleReturn  ?    EDM_LINE : 0);
    iVisivle |= (SYS_EDITOR->bVisibleEof     ?    EDM_EOF  : 0);
    iVisivle |= (SYS_EDITOR->bVisibleLineNum ?    EDM_LINE_NUM : 0);
    */

    return true;
    
}

/**
 *  @brief  コピー
 *  @param  なし
 *  @retval なし
 *  @note
 */
void CEditorView::OnEditCopy()
{
    m_psEditor->Copy();
}

/**
 *  @brief  カット
 *  @param  なし
 *  @retval なし
 *  @note
 */
void CEditorView::OnEditCut()
{
    m_psEditor->Cut();
}

/**
 *  @brief  ペースト
 *  @param  なし
 *  @retval なし
 *  @note
 */
void CEditorView::OnEditPaste()
{
    m_psEditor->Paste();
}

/**
 *  @brief  アンドゥ
 *  @param  なし
 *  @retval なし
 *  @note
 */
void CEditorView::OnEditUndo()
{
    m_psEditor->Undo();
}

/**
 *  @brief  ビルド
 *  @param  [in] nId メニューID
 *  @retval なし
 *  @note   
 */
void CEditorView::OnMenuBuild(UINT nId)
{
    CDocument* pDoc = GetDocument();
    if (pDoc == NULL)
    {
        STD_DBG(_T("GetDocument"));
        return ;
    }

    CMockSketchDoc* pSketchDoc;

    pSketchDoc = reinterpret_cast<CMockSketchDoc*>(pDoc);
    std::shared_ptr<CObjectDef> pDef;
    pDef = pSketchDoc->GetObjectDef().lock();

    STD_ASSERT(pDef);

    if (!pDef)
    {
        return;
    }

    switch(nId)
    {
    case ID_MNU_SCRIPT_BUILD:
        ChangeOutputWindow (WIN_BUILD);
        ClearOutputWindow  (WIN_BUILD);
        pDef->Build(false);
        break;

    case ID_MNU_SCRIPT_REBUILD:
        ChangeOutputWindow (WIN_BUILD);
        ClearOutputWindow  (WIN_BUILD);
        pDef->Build(true);
        break;

    case ID_MNU_SCRIPT_STOP_BUILD:

        break;

    case ID_MNU_SCRIPT_EXEC:
        ChangeOutputWindow (WIN_DEBUG);
        ClearOutputWindow  (WIN_DEBUG);
        pDef->ExecScripts();
        break;

    case ID_MNU_SCRIPT_ABORT:
        THIS_APP->GetExecCtrl()->AbortScripts();
        break;

    case ID_MNU_SCRIPT_PAUSE:
        THIS_APP->GetExecCtrl()->PauseScripts();
        break;
    }
}


/**
 * @brief   ビュー アクティブ状態変更
 * @param   [in]    bActivate       ビューがアクティブ/非アクティブ
 * @param   [in]    pActivateView   アクティブにされているビューへのポインタ
 * @param   [in]    pDeactiveView   非アクティブになるビューへのポインタ
 * @return  なし
 * @note    フレームワークから呼び出し
 */
void CEditorView::OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView)
{
    CView::OnActivateView(bActivate, pActivateView, pDeactiveView);
    if (bActivate)
    {
        //ウインドウの切り替えを通知
        StdString strTitle;
        CDocument* pDoc = GetDocument();
        if (pDoc == NULL)
        {
            STD_DBG(_T("GetDocument"));
            return;
        }

        strTitle = CMockSketchDoc::SplitTitleMarker(pDoc->GetTitle());

        ::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_VIEW_ACTIVE, (WPARAM)&strTitle, (LPARAM)1);
        THIS_APP->SetAvtiveTabNmae(strTitle);
        THIS_APP->GetMainFrame()->SetActiveView(this, FALSE);
    }

#ifdef _DEBUG
#if 0
    CString strWindow;
    CString strAvtive  = _T("Active  : none \n");
    CString strDevtive = _T("Dective : none \n");

    if (pActivateView)
    {
        CDocument* pActiveDoc = pActivateView->GetDocument( );
        if (pActiveDoc)
        {
            strWindow = pActiveDoc->GetTitle();
            strAvtive.Format(_T("Active  : %s %I64x \n"), strWindow, pActivateView);
        }
    }

    if (pDeactiveView)
    {
        CDocument* pDeactiveDoc = pDeactiveView->GetDocument( );
        if (pDeactiveDoc)
        {
            strWindow = pDeactiveDoc->GetTitle();
            strDevtive.Format(_T("Dective : %s %I64x \n"), strWindow, pDeactiveView);
        }
    }
    DB_PRINT(_T("CMockSketchView::OnActivateView bActibe[%d] \n"), bActivate);
    DB_PRINT(_T("    %s"), strAvtive);
    DB_PRINT(_T("    %s"), strDevtive);


    if (bActivate)
    {
        DB_PRINT(_T("ACTIVE---    %s"), strAvtive);
    }
#endif
#endif
}

/**
 * @brief   スクリプト名更新
 * @param   [in] wParam 使用しない
 * @param   [in] lParam 使用しない
 * @return  なし
 * @note    CObjectWnd::OnEndLabelEditから呼び出し
 */
LRESULT CEditorView::OnUpdateName(WPARAM wParam, LPARAM lParam)
{
    // ドキュメントを更新
    CDocument* pDoc = GetDocument();
    if (pDoc == NULL)
    {
        STD_DBG(_T("GetDocument"));
        return 0;
    }
    CMockSketchDoc* pSketchDoc;
    pSketchDoc = reinterpret_cast<CMockSketchDoc*>(pDoc);
    pSketchDoc->UpdateName();
DB_PRINT(_T("CEditorView::OnUpdateName\n"));

    return 0;
}

/**
 * @brief   保存開始前通知
 * @param   [in] wParam 使用しない
 * @param   [in] lParam 使用しない
 * @return  なし
 * @note    
 */
LRESULT CEditorView::OnViewSaveStart(WPARAM pMessage, LPARAM lParam)
{
    //データを保存する
    SaveBufferToDocument();
    return 0;
}

/**
 * @brief   ビュークリア
 * @param   [in] wParam 使用しない
 * @param   [in] lParam 使用しない
 * @return  なし
 * @note    
 */
LRESULT CEditorView::OnViewClear(WPARAM pMessage, LPARAM lParam)
{
    SaveBufferToDocument();
    DestroyWindow();
    return 0;
}

/**
 * @brief   テキスト変更コールバック
 * @param   [in] pData 自身
 * @param   [in] bChg  
 * @return  なし
 * @note    
 */
void CALLBACK CEditorView::SetFuncTextModified(void* pData, bool bChg)
{
    CEditorView* pView = reinterpret_cast<CEditorView*>(pData);
    {
        pView->SetChange();

        ::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_CHG_OBJECT_STS, 
                                    (WPARAM)0, (LPARAM)0);


    }
}

/**
 * @brief   ブレークポイント設定コールバック
 * @param   [in] pData  自身
 * @param   [in] iLine  設定行
 * @param   [in] bSet   設定・解除
 * @return  なし
 * @note    
 */
void CALLBACK CEditorView::SetBreakPointModified(void* pData,
                                                        int iLine,
                                                        bool bSet)
{
    CEditorView* pView = reinterpret_cast<CEditorView*>(pData);
    pView->SetBreakPoint(iLine, bSet, true);
}


/**
 * @brief   変更設定(SetFuncTextModified専用)
 * @param   なし
 * @return  なし
 * @note    
 */
void CEditorView::SetChange()
{
    STD_ASSERT(m_pScriptData != NULL);
    m_pScriptData->_AddChange();
    CDocument* pDoc = GetDocument();
    if (pDoc != NULL)
    {
        CMockSketchDoc* pSketchDoc;
        pSketchDoc = reinterpret_cast<CMockSketchDoc*>(pDoc);
        pSketchDoc->UpdateName();
    }
}

/**
 * @brief   再読み込み
 * @param   なし
 * @return  なし
 * @note    
 */
void CEditorView::Reload()
{
    if (m_pScriptData)
    {
        if (!m_pScriptData->Reload())
        {
            //ファイルの読み込みに失敗しました
            AfxMessageBox(GET_STR(STR_ERROR_FILE_LOAD));
        }
    }
    LoadDocumentToBuffer();
}

/**
 * @brief   テキスト削除
 * @param   なし
 * @return  true 成功
 * @note    
 */
bool CEditorView::ClearText()
{
    m_psEditor->SetString(_T(""));
    return true;
}

/**
 * @brief   テキスト保存
 * @param   なし
 * @return  なし
 * @note    Documentにデータを保存する
 */
bool CEditorView::SaveBufferToDocument()
{
    STD_ASSERT(m_pScriptData != NULL);

    STD_ASSERT(m_psEditor);

    if (!m_psEditor)
    {
        return false;
    }

    m_pScriptData->ClearData();

    std::shared_ptr<std::string> pStr;
    StdString  strScript;

    try
    {
        m_psEditor->ObservChangeDocument(EO_STOP);
        pStr = m_pScriptData->GetScriptStringInstance().lock();
        STD_ASSERT(pStr);
        m_psEditor->GetString(&strScript);

        //TODO:なんとか変換なしで済ませたい。
        *pStr = CUtil::StringToChar(strScript);

        int iIcon = 0;

        m_pScriptData->ClearBreakPoint();
        BREAK_POINT breakPoint;
        breakPoint.bEnable = true;
        breakPoint.iTmp = 0;

        std::vector<MT_EDITOR::BREAK_POINT_DATA> lstBreak;
        m_psEditor->GetBreakPoint(&lstBreak);

        MT_EDITOR::BREAK_POINT_DATA breakData;
        foreach(breakData, lstBreak)
        {
            breakPoint.bEnable = breakData.bEnable;
            m_pScriptData->SetBreakPoint(breakData.iLine, breakPoint);
        }

        m_pScriptData->SetCurLine(1);

        //カーソル位置記憶
        int iCol;
        int iLine;

        m_psEditor->GetCaretPosition(&iLine, &iCol);
        m_pScriptData->SetCursor((int)iLine, (int)iCol);
        m_psEditor->ObservChangeDocument(EO_START);
        return true;
    }
    catch(...)
    {
        STD_DBG(_T("SaveText"));
    }
    m_psEditor->ObservChangeDocument(EO_START);
    return false;
}

/**
 * @brief   テキスト取得
 * @param   なし
 * @return  なし
 * @note    Documentからデータを取得
 */
bool CEditorView::LoadDocumentToBuffer()
{

    STD_ASSERT(m_pScriptData != NULL);
    ClearText();
    std::shared_ptr<std::string> pScript;

    pScript = m_pScriptData->GetScriptStringInstance().lock();

    if (!pScript)
    {
        return true;
    }

    if (pScript->size() == 0)
    {
        return true;
    }

    m_psEditor->ObservChangeDocument(EO_STOP);
    StdString strScript = CUtil::CharToString(pScript->c_str());
    if (strScript.length() == 0)
    {
        STD_DBG(_T("LoadDocumentToBuffer"));
    }


    int iRet = m_psEditor->SetString(strScript);

    if (!iRet)
    {
        STD_DBG(_T("GetText %d"), iRet);
        m_psEditor->ObservChangeDocument(EO_START);
        return false;
    }


    ClearBreakPoint();
    std::map<int, BREAK_POINT> mapBreak;
    std::map<int, BREAK_POINT>::iterator iteMap;
    int iPos;
    BREAK_POINT  breakPoint;

    m_pScriptData->GetBreakPoint(&mapBreak);

    foreach(boost::tie(iPos, breakPoint), mapBreak)
    {
        SetBreakPoint(iPos, true);
    }

    //カーソル位置設定
    int iCol;
    int iLine;

    m_pScriptData->GetCursor( &iLine, &iCol);
    m_psEditor->SetCaretPosition(iLine, iCol);
    m_psEditor->ObservChangeDocument(EO_START);
    return true;
}

/**
 * @brief  ウインドウ生成前処理
 * @param  [in] cs  CREATESTRUCT 構造体。 
 * @retval ウィンドウの作成を継続する場合は、0 以外を返します。
 * @retval それ以外の場合は 0 を返します。
 * @note
 */
BOOL CEditorView::PreCreateWindow(CREATESTRUCT& cs)
{
    return CView::PreCreateWindow(cs);
}

/**
 * @brief   ドキュメント更新
 * @param   [in]    pSender   ドキュメントを変更したビューへのポインタ        
 * @param   [in]    lHint   変更についての情報を持ちます        
 * @param   [in]    pHint   変更についての情報が格納されたオブジェクトへのポインタ。        
 * @retval  なし
 * @note	
 */
void CEditorView::OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/)
{
    CDocument* pDoc = GetDocument();
    if (pDoc == NULL)
    {
        STD_DBG(_T("GetDocument"));
        return;
    }

    CMockSketchDoc* pSketchDoc;
    pSketchDoc = reinterpret_cast<CMockSketchDoc*>(pDoc);
    m_pScriptData = pSketchDoc->GetScript();

    LoadDocumentToBuffer();
}

/**
 * @brief  マウス右ボタン解放
 * @param  [in]    nFlags   仮想キー         
 * @param  [in]    point    マウス座標         
 * @retval なし
 * @note	
 */
void CEditorView::OnRButtonUp(UINT nFlags, CPoint point)
{
    ClientToScreen(&point);
    OnContextMenu(this, point);
}

/**
 *  @brief コンテキストメニュー選択
 *  @param [in] pWnd   マウスの右ボタンがクリックされたウィンドウのハンドル
 *  @param [in] point  クリックされたときの、カーソルの画面座標位置 
 *  @retval           なし
 *  @note
 */
void CEditorView::OnContextMenu(CWnd* pWnd, CPoint point)
{
    /*
    
    //マウス位置をスクリーン座標で記録
    ::GetCursorPos(&m_ptContextMenu);
    HWND hWnd = ::WindowFromPoint(m_ptContextMenu);

    for (int iView = 0; iView < 4; iView++)
    {
        if (hWnd == Footy2GetWnd(m_iEditorId, iView))
        {
            ::SetFocus(hWnd);
        }
    }
    */
	theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDITOR, point.x, point.y, this, TRUE);
}


/**
 *  @brief  実行行解除
 *  @param  [in] bRedraw 再描画
 *  @retval true 成功
 *  @note   
 */
bool CEditorView::ClearExecLine(bool bRedraw)
{

    if (!m_psEditor)
    {
        return false;
    }


    m_iCurLine = -1;

    if (m_psEditor)
    {
        m_psEditor->SetExecLine(-1, false);
        if (bRedraw)
        {
            m_psEditor->Redraw();
        }
    }
    return true;
}

/**
 *  @brief  実行行設定
 *  @param  [in] iLine 実行行(0 org)
 *  @retval なし
 *  @note   1org
 */
bool CEditorView::SetExecLine(int iLine)
{
    if (iLine < 0)
    {
        return false;
    }

    m_iCurLine = iLine;

    if (m_psEditor)
    {
        m_psEditor->SetExecLine(m_iCurLine, true);
    }
    return true;
}


std::string CEditorView::_GetSectionName(CObjectDef* pDef) const
{
    std::string strSectionNameA;
    if (!pDef)
    {
        return strSectionNameA;
    }

    StdString strDef;
    StdString  strSectionNameW;


    strDef = THIS_APP->GetObjectDefTabName(pDef->GetPartsId());

    strSectionNameW = CUtil::StrFormat(_T("%s(%s)"),m_pScriptData->GetName().c_str(),
                                                    strDef.c_str());
    strSectionNameA = CUtil::StringToChar(strSectionNameW);

    return strSectionNameA;
}

/**
 *  @brief  ブレークポイント 設定・解除
 *  @param  [in] iLine   行(0org)
 *  @param  [in] bSet    設定
 *  @param  [in] bDirect 直接ScriptDataに設定
 *  @retval なし
 *  @note  
 */
bool CEditorView::SetBreakPoint(int iLineNo, bool bSet, bool bDirect)
{
    CMockSketchDoc* pDoc = GetDocument();

    std::shared_ptr<CObjectDef> pDef;
    if (pDoc)
    {
        pDef = pDoc->GetObjectDef().lock();
    }

    if (!pDef)
    {
        return false;
    }

    CModule* pModule = pDef->GetModule();

    pDoc->GetObjectDef();
    m_pScriptData->GetName();

    std::string strSectionNameA;
    strSectionNameA = _GetSectionName(pDef.get());
        
    if (bSet)
    {
        pModule->SetBreakPoint(strSectionNameA.c_str(), iLineNo);
    }
    else
    {
        pModule->DelBreakPoint(strSectionNameA.c_str(), iLineNo);
    }

    
    if (bDirect)
    {
        //直接ブレークポイントを設定する
        BREAK_POINT breakPoint;
        breakPoint.bEnable = bSet;
        m_pScriptData->SetBreakPoint(iLineNo, breakPoint);
    }
    else
    {
        if (bSet)
        {
            m_psEditor->AddBreakPoint(iLineNo, true);
        }
        else
        {
            m_psEditor->DelBreakPoint(iLineNo);
        }
    }
    return true;
}

/**
 *  @brief  ブレークポイント 設定・解除要求
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CEditorView::OnBreakpoint()
{
    int iLine;
    int iCol;
    bool bSet;
    m_psEditor->GetCaretPosition(&iLine, &iCol);
    bSet = m_psEditor->IsBreakPoint(iLine);
    SetBreakPoint(iLine, !bSet, false);
}

/**
 *  @brief  ブレークポイント全解除
 *  @param  なし
 *  @retval ture 成功
 *  @note   
 */
bool CEditorView::ClearBreakPoint()
{
    m_psEditor->ClearBreakPoint();
    return true;
}

/**
 *  @brief  ステップイン実行要求
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CEditorView::OnStepIn()
{
    CDocument* pDoc = GetDocument();
    if (pDoc == NULL)
    {
        STD_DBG(_T("GetDocument"));
        return ;
    }

    CMockSketchDoc* pSketchDoc;
    pSketchDoc = reinterpret_cast<CMockSketchDoc*>(pDoc);
    std::shared_ptr<CObjectDef> pDef;
    pDef = pSketchDoc->GetObjectDef().lock();

    STD_ASSERT(pDef);
    if (!pDef)
    {
        return;
    }

    CURRENT_BREAK_CONTEXT* pBreakData;
    pBreakData = THIS_APP->GetExecCtrl()->GetCurrentBreakData();

    if (!pBreakData)
    {
        return;
    }

    std::string strSection = _GetSectionName(pDef.get());
    if (pBreakData->strSection != strSection)
    {
        return;
    }

    THIS_APP->GetExecCtrl()->StepInto();
}


/**
 *  @brief  ステップオーバー実行要求
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CEditorView::OnStepOver()
{
    CDocument* pDoc = GetDocument();
    if (pDoc == NULL)
    {
        STD_DBG(_T("GetDocument"));
        return ;
    }

    CMockSketchDoc* pSketchDoc;
    pSketchDoc = reinterpret_cast<CMockSketchDoc*>(pDoc);
    std::shared_ptr<CObjectDef> pDef;
    pDef = pSketchDoc->GetObjectDef().lock();

    STD_ASSERT(pDef);

    if (!pDef)
    {
        return;
    }

    CURRENT_BREAK_CONTEXT* pBreakData;
    pBreakData = THIS_APP->GetExecCtrl()->GetCurrentBreakData();

    if (!pBreakData)
    {
        return;
    }

    std::string strSection = _GetSectionName(pDef.get());
    if (pBreakData->strSection != strSection)
    {
        return;
    }

    THIS_APP->GetExecCtrl()->StepOver();
}

/**
 *  @brief  カーソル行まで実行要求
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CEditorView::OnStepCursor()
{
    CDocument* pDoc = GetDocument();
    if (pDoc == NULL)
    {
        STD_DBG(_T("GetDocument"));
        return ;
    }

    CMockSketchDoc* pSketchDoc;
    pSketchDoc = reinterpret_cast<CMockSketchDoc*>(pDoc);
    std::shared_ptr<CObjectDef> pDef;
    pDef = pSketchDoc->GetObjectDef().lock();

    STD_ASSERT(pDef);

    if (!pDef)
    {
        return;
    }

    CURRENT_BREAK_CONTEXT* pBreakData;
    pBreakData = THIS_APP->GetExecCtrl()->GetCurrentBreakData();

    if (!pBreakData)
    {
        return;
    }

    std::string strSection = _GetSectionName(pDef.get());
    if (pBreakData->strSection != strSection)
    {
        return;
    }

    STD_ASSERT(m_pScriptData != NULL);

    int iLine;
    int iCol;

    m_psEditor->GetCaretPosition(&iLine, &iCol);

    //TODO:実装
    SetBreakPoint(iLine, true, true);


    //TODO:コンティニュー処理
    /*
    DBG_ASSERT(m_pExecContext);
    if (m_pExecContext)
    {
        m_pExecContext->Continue(EXEC_COMMON::STEP_IN);
    }
    */
    SetBreakPoint(iLine, false, true);
    ClearExecLine(true);
    THIS_APP->GetExecCtrl()->Resume();

}

void CEditorView::OnStepContinue()
{
   CDocument* pDoc = GetDocument();
    if (pDoc == NULL)
    {
        STD_DBG(_T("GetDocument"));
        return ;
    }

    CMockSketchDoc* pSketchDoc;
    pSketchDoc = reinterpret_cast<CMockSketchDoc*>(pDoc);
    std::shared_ptr<CObjectDef> pDef;
    pDef = pSketchDoc->GetObjectDef().lock();

    STD_ASSERT(pDef);

    if (!pDef)
    {
        return;
    }

    CURRENT_BREAK_CONTEXT* pBreakData;
    pBreakData = THIS_APP->GetExecCtrl()->GetCurrentBreakData();

    if (!pBreakData)
    {
        return;
    }

    std::string strSection = _GetSectionName(pDef.get());
    if (pBreakData->strSection != strSection)
    {
        return;
    }

    ClearExecLine(true);
    THIS_APP->GetExecCtrl()->Resume();
}


/**
 *  @brief  全ブレークポイントクリア要求
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CEditorView::OnClearBreakpoint()
{
    ClearBreakPoint();
}

/**
 * @brief   ウインドウ破棄
 * @param   なし
 * @return  なし
 * @note    
 */
void CEditorView::OnDestroy()
{
    SaveBufferToDocument();
    CView::OnDestroy();
}

/**
 *  @brief  ウインドウプロシジャ
 *  @param    [in]  message  処理される Windows メッセージ
 *  @param    [in]  wParam   メッセージの処理で使う付加情報1
 *  @param    [in]  lParam   メッセージの処理で使う付加情報2
 *  @retval   メッセージに依存する値
 *  @note   
 */
LRESULT CEditorView::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
    LRESULT ret;
    BOOL bOver;
    if (m_psEditor)
    {
        ret = m_psEditor->WindowProc(message, wParam, lParam, &bOver);

        if (!bOver)
        {
            return CView::WindowProc(message, wParam, lParam);
        }
        return ret;
    }
    return CView::WindowProc(message, wParam, lParam);
}

/**
 *  @brief コントロールの動的サブクラス化
 *  @param なし
 *  @retval なし
 *  @note   
 */
void CEditorView::PreSubclassWindow()
{
    m_psEditor->Create(m_hWnd);
    m_psEditor->SetBreakPointCallback(this,SetBreakPointModified);
    m_psEditor->SetObservCallback(this,SetFuncTextModified);
    UpdateFontSetting();
    CView::PreSubclassWindow();
}
