/**
 * @brief			CEditorViewヘッダーファイル
 * @file			CEditorView.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *	CEditorView ビュー
 *
 * $
 * $
 * 
 */

#pragma once

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/


/*---------------------------------------------------*/
/*  class                                            */
/*---------------------------------------------------*/
class CScriptObject;
class CMockSketchDoc;
class CScriptData;
class CObjectDef;

namespace MT_EDITOR{
    class CMtEditorWindow;
}

/**
 * @class   CEditorView
 * @brief                        
 */
class CEditorView : public CView
{
	DECLARE_DYNCREATE(CEditorView)

protected:

	CEditorView();           // 動的生成で使用される protected コンストラクタ
	virtual ~CEditorView();

public:
	virtual void OnDraw(CDC* pDC);      // このビューを描画するためにオーバーライドされます。
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif
    CMockSketchDoc* GetDocument() const;

protected:
    CString         m_strFileName;
    CScriptData*    m_pScriptData;
    int             m_iCurLine;
    int             m_iIndcationLine;
    POINT           m_ptContextMenu;
    CFont           m_fnt;

    //!< エディタ本体
    MT_EDITOR::CMtEditorWindow*  m_psEditor;


public:
    //static void CALLBACK OnMoveCarete(int nID,void *pData,size_t nLine,size_t nColumn);

    //!< テキスト変更コールバック
    static void CALLBACK SetFuncTextModified(void* pData, bool bChg);

    //!< ブレークポイント設定コールバック
    static void CALLBACK SetBreakPointModified(void* pData, int iLine, bool bSet);

    //!< スクリプト名更新
    void UpeateScriptName();

    //!< テキスト削除
    bool ClearText();

    //!<  ファイル再読み込み
    bool ReloadFile();

    //!<  テキスト保存;
    bool SaveBufferToDocument();

    //!<  テキスト取得;
    bool LoadDocumentToBuffer();

    //!< フォント設定更新
    bool UpdateFontSetting();

    //!< 表示設定更新
    bool UpdateVisibleSetting();

    //!< ブレークポイント設定
    bool SetBreakPoint(int iLine, bool bSet, bool bDirect = false);

    //!< ブレークポイント全解除
    bool ClearBreakPoint();

    //!< 実行行設定
    bool SetExecLine(int iLine);

    //!< 実行行解除
    bool ClearExecLine(bool bRedraw = true);

    //!< 指示行設定
    //bool SetIndLine(int iLine);

    //!< 指示行解除
    //bool ClearIndLine(bool bRedraw = true);

    //!< 実行スクリプトブジェクト設定
    void SetScriptObject(CScriptObject * pScriptObject);

    //!< 変更設定(SetFuncTextModified専用)
    void SetChange();

    //!< 再読み込み
    void Reload();


    //デバッグ用
#ifdef _DEBUG
    CScriptData*  GetScriptData(){return m_pScriptData;}
#endif

    // 生成された、メッセージ割り当て関数
protected:
    DECLARE_MESSAGE_MAP()

public:
    afx_msg void OnEditCopy();
    afx_msg void OnEditCut();
    afx_msg void OnEditPaste();
    afx_msg void OnEditUndo();

    afx_msg void OnMenuBuild(UINT nId);

    afx_msg void OnClearBreakpoint();
    afx_msg void OnBreakpoint();

    afx_msg void OnStepIn();
    afx_msg void OnStepOver();
    afx_msg void OnStepContinue();
    afx_msg void OnStepCursor();
    

    //!<スクリプト名前更新
    afx_msg LRESULT OnUpdateName(WPARAM pMessage, LPARAM lParam);

    //!<保存開始前通知
    afx_msg LRESULT OnViewSaveStart(WPARAM pMessage, LPARAM lParam);

    //!<ビュークリア
    afx_msg LRESULT OnViewClear(WPARAM pMessage, LPARAM lParam);


protected:
    virtual void OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView);
    virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
    virtual void OnUpdate(CView* /*pSender*/, LPARAM /*lHint*/, CObject* /*pHint*/);
public:
    afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
    afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);
    afx_msg void OnDestroy();
protected:
    virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
    virtual void PreSubclassWindow();

    std::string _GetSectionName(CObjectDef* pDef) const;

};

#ifndef _DEBUG  // MockSketchView.cpp のデバッグ バージョン
inline CMockSketchDoc* CEditorView::GetDocument() const
   { return reinterpret_cast<CMockSketchDoc*>(m_pDocument); }
#endif
