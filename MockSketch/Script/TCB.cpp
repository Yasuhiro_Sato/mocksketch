
#include "stdafx.h"
#include "TCB.h"
#include <angelscript.h>
#include <mmsystem.h>
#include <map>
#include "CExecContextThread.h"
#include "Utility/CUtility.h"

#ifdef _DEBUG
#undef new 
#define new DEBUG_NEW
#endif 

int TCB::isDebugCount = 0;

namespace {

enum CMD_TRANSITION2
{
    O = CT_OK,     //遷移OK
    X = CT_NG,     //遷移NG
    E = CT_ERROR,  //想定外の状態
    N = CT_NEXT,   //次回に遷移
};


static std::vector<std::vector<CMD_TRANSITION2>> g_sts = 
    
   //NON RUN ABT INT SUS YLD SLP TER EXT DEL          
   {{E,  O,  O,  E,  O,  E,  E,  O,  O,  O}, //NON 0
    {E,  E,  N,  O,  O,  O,  O,  O,  O,  O}, //RUN 1
    {X,  X,  X,  O,  O,  O,  O,  O,  O,  O}, //ABT 2
    {X,  X,  N,  X,  E,  E,  E,  O,  O,  O}, //INT 3
    {X,  O,  O,  X,  E,  E,  E,  O,  O,  O}, //SUS 4
    {X,  X,  N,  X,  E,  E,  E,  O,  O,  O}, //YLD 5
    {X,  X,  N,  X,  X,  X,  X,  O,  O,  O}, //SLP 6
    {X,  X,  X,  X,  X,  X,  X,  X,  X,  O}, //TER 7
    {X,  X,  X,  X,  X,  X,  X,  X,  X,  O}, //EXT 8
    {X,  X,  X,  X,  X,  X,  X,  X,  X,  O}, //DEL 9
};
};

AsScriptContext::AsScriptContext()
{

}


AsScriptContext::AsScriptContext(asIScriptEngine* pEngine)
{
    if (!m_pContext)
    {
        m_pContext = pEngine->CreateContext();
    }

}

AsScriptContext::~AsScriptContext()
{
    m_pContext->Release();
    m_pContext = NULL;
}

asIScriptContext* AsScriptContext::Get()
{
    return m_pContext;
}

    
int AsScriptContext::SetLineCallback(asSFuncPtr callback, void *obj, int callConv)
{
    if(m_pContext)
    {
        return m_pContext->SetLineCallback(callback, obj, callConv);
    }

    return -1;
}

//======================
//======================
//======================


TCB::TCB()
{
    isDebugCount++;
    iDebugCount = isDebugCount;
    //pLock = std::make_unique<spinlock>();
}

void   TCB::Create(int iidIn)
{
    eSts = TS_WAIT;
    iId = iidIn;
}


bool TCB::SetPriority(int iPriInput)
{
    if (iPriInput < 0){return false;}
    if (iPriInput > 5){return false;}
    iPri = iPriInput;
    return true;
}

bool TCB::SetTaskName(std::wstring strName)
{
    strTaskName = strName;
    return true;
}

void TCB::SetExecType(EXEC_TYPE eTypeInput)
{
    eType = eTypeInput;
}

void TCB::SetSleepStartTime()
{
    DWORD dwNow = timeGetTime();
    dwSleepStartTime = dwNow;
}

void TCB::SetSleepTime(DWORD dwTime)
{
    dwSleepTime = dwTime;
}

//----------------------------
EXEC_CMD  TCB::GetCmd() 
{
    std::lock_guard<std::mutex> lk(mtxLock);
    if (queCmd.empty())
    {
        return TCMD_NONE;
    }
    return queCmd[0];
}

bool   TCB::SetCmd(EXEC_CMD eCmdSet)
{
    std::lock_guard<std::mutex> lk(mtxLock);
    //チェックなし 
    //IsSetCmdで妥当性チェック
    if (queCmd.empty())
    {
        queCmd.push_back(eCmdSet);
    }
    else
    {
        queCmd[0] = eCmdSet;
    }
    return true;
}

bool   TCB::PushCmd(EXEC_CMD eCmdSet)
{
    std::lock_guard<std::mutex> lk(mtxLock);
    //ABORTコマンドでの使用を想定
    size_t size = queCmd.size();
    if (size < 2)
    {
        queCmd.push_back(eCmdSet);
        return true;
    }
    return false;
}

void  TCB::PopCmd()
{
    std::lock_guard<std::mutex> lk(mtxLock);
    if (queCmd.empty())
    {
        return;
    }
    queCmd.pop_front();
}
//----------------------------


CMD_TRANSITION TCB::IsSetCmd(EXEC_CMD eCmdSet)
{

    /* 
     コマンドは、受け付けてから実行完了までコマンドを保持する
     CT_NEXTで次回に遷移させるのは現状 アボートのみ


     CExecContextThread::ExecQueがコマンドを受け付けるタイミングは
     スクリプト実行中と、スクリプト開始前のみ


     ABORT EXIT TERMINATE DELETEの違い


     ABORT 通常終了
           実行中のスクリプトが終了してから
           Yeld, Sleep を待つ
           STEP ABORT を実行する 


     EXIT 通常終了        
           Yeld, Sleep を待他ない
    

    TERMINATE
          実行中のスクリプトも強制中断する

        
    DELETE 内部でのみ使用
          CExecCtrl内のオブジェクトを削除する

    */






    bool bRet = false;


    int iSrc = GetCmd() & TCMD_MASK;
    int iDst = eCmdSet & TCMD_MASK;


    CMD_TRANSITION eRet;
    try
    {
        eRet = static_cast<CMD_TRANSITION>(g_sts[iSrc].at(iDst));
    }
    catch(...)
    {
        eRet = CT_ERROR;
    }

    return eRet;
}

CMD_RETURN  TCB::ApplyCommand(CExecContextThread* pThread)
{
    CMD_RETURN eRet = RET_OK;
    if (GetStatus( ) == TS_INTRRUPT)
    {
        //割り込みされた状態の復帰
        SetCmd(TCMD_RUN);
    }

    switch(GetCmd())
    {

    case TCMD_NONE:
        if(GetStatus() & TS_SUSPEND_MASK)
        {
            SetStatus(TS_RUN);
            bResume = true;
        }
        break;

    case TCMD_RUN:
    {
        bool bSet = false;
        if(pCurrentContext.get() == NULL)
        {
            //新規もしくはキャッシュ済みのコンテキストを取得
            pCurrentContext = pThread->MoveContext();
            bSet = true;
        }
        else
        {
            bResume = true;
        }

        STD_ASSERT(pCurrentContext.get());
        SetStatus(TS_RUN);
    }
        break;


    case TCMD_DELETE:
        Sleep(1);
        if (!pThread->IsTcbOnStack(iId))
        {
            DB_PRINT(_T("TCMD_EXIT iid{%d} is not exist \n"), iId);
        }

        eRet = RET_SKIP;
        break;

    case TCMD_TERMINATE:
    case TCMD_EXIT:
        break;

    case TCMD_ABORT:

        if(pCurrentContext.get() == NULL)
        {
DB_PRINT(_T("TCMD_ABORT NULL iid{%d}  \n"), iId);
            //新規もしくはキャッシュ済みのコンテキストを取得
            pCurrentContext = pThread->MoveContext();
        }
        else
        {
DB_PRINT(_T("TCMD_ABORT EXIST iid{%d}  \n"), iId);
            //bResume = true;
        }

        STD_ASSERT(pCurrentContext.get());
        SetStatus(TS_RUN);
        SetStep(STP_ABORT);
        SetExecType( ET_ONE_SHOT);
        break;

    default:
        //TODO:エラー表示
        eRet = RET_NG;
        break;
    }
    return eRet;
}

bool  TCB::_SetExecType(EXEC_TYPE eExecType)
{
    eType = eExecType;
    return true;
}
bool  TCB::SetStatus(TASK_STS eStsIn)
{
    eSts = eStsIn;

    switch(eSts)
    {
    case TS_WAIT:
    case TS_RUN:
    case TS_WAIT_CYCLE:
    case TS_INTRRUPT:
    case TS_YIELD:
    case TS_SLEEP:
        break;

    case TS_BREAK:
    case TS_SUSPEND:
    case TS_EXT:
        dExecTime = 0.0;
        break;
    }

    return true;
}


bool TCB::SetCycleTime(DWORD dwTime)
{
    dwCycleTime = dwTime;
    return true;
}

bool TCB::SetThreadType(EXEC_COMMON::E_THREAD_TYPE eType)
{
    eThread = eType;
    return true;
}

void TCB::SetAsFunc(asIScriptFunction* pFunc)
{
    pAsFunc = pFunc;
}

void TCB::SetFunc(std::function<int(asIScriptFunction*, TCB*)> funcInput)
{
    func = funcInput;
}

void TCB::SetScriptObject(CScriptObject* pObject)
{
    pScriptObject = pObject;
}


/**
 *  @brief   デバッグ用文字
 *  @param   [in] strSpc 
 *  @param   デバッグ用文字
 *  @retval  なし
 *  @note
 */
StdString TCB::GetStr(StdString strSpc) const
{
    StdStringStream strRet;
    strSpc += _T("  ");
    strRet << StdFormat(_T("Nemr(%s) Time:%d pScriptObject:%llx Context:%llx")) 
        % strTaskName.c_str()
        % dwTime
        % pScriptObject 
        % pCurrentContext.get();
    return strRet.str();
}

/**
 *  @brief   デバッグ用文字
 *  @param   なし
 *  @param   デバッグ用文字
 *  @retval     なし
 *  @note
 */
StdString TCB::GetStr() const
{
    return GetStr(_T(""));
}