﻿#include "stdafx.h"
#include <functional>
#include <angelscript.h>
#include "ExecCommon.h"
#include <vector>
#include  <memory>
#include  <deque>
#include <mutex>
#include "DrawingObject/RefBase.h"  //CAsRefObj

class spinlock;

#pragma once
extern void DB_PRINT(const TCHAR* pStr, ...);

class CScriptObject;
class CExecContextThread;


class AsScriptContext
{
public:
    AsScriptContext();
    AsScriptContext(asIScriptEngine* pEngine);
    ~AsScriptContext();
    
    int SetLineCallback(asSFuncPtr callback, void *obj, int callConv);

    asIScriptContext* Get();

protected:
    asIScriptContext* m_pContext = NULL;

};

/*
	WAT	RUN	INT	YEL	SLP	SUS	EXT
WAT	-	○	☓	☓	☓	☓	○
RUN	○	-	○	○	○	○	○
INT	☓	○	-	YEL	SLP	SUS	○
YEL	☓	○	☓	-	SLP	SUS	○
SLP	☓	○	☓	☓	-	SUS	○
SUS	☓	○	☓	☓	☓	-	○
EXT	○	○	○	○	○	○	-
*/


//ステータスの優先順位は 
// TS_INTRRUPT < TS_YIELD < TS_SLEEP < TS_SLEEP < TS_SUSPEND
enum TASK_STS
{
    TS_WAIT          = 0x0000, //待ち状態
    TS_RUN           = 0x0100, //実行状態
    TS_WAIT_CYCLE    = 0x0101, //サイクル実行待ち

    TS_SUSPEND_MASK    = 0x1000, //suspend実行マスク
    TS_INTRRUPT        = 0x1101, //INTRRUPT
    TS_YIELD           = 0x1103, //YIELD
    TS_SLEEP           = 0x1104, //SLEEP
    TS_BREAK           = 0x1105, //BREAK状態

    TS_SUSPEND         = 0x1200, //強制待ち状態
    TS_EXT             = 0x8000, //終了
};

enum EXEC_CMD
{
    TCMD_NONE            = 0x000,
    TCMD_RUN             = 0x001,
    TCMD_ABORT           = 0x002,

    TCMD_SUSPEND_MASK    = 0x100, //suspend実行マスク
    TCMD_INTRRUPT        = 0x103,
    TCMD_SUSPEND         = 0x104,
    TCMD_YIELD           = 0x105, //自スクリプトから実行
    TCMD_SLEEP           = 0x106, //自スクリプトから実行
    TCMD_TERMINATE       = 0x307,
    TCMD_EXIT            = 0x408,
    TCMD_DELETE          = 0x409,
    TCMD_MASK            = 0x0FF, //下の２桁をコマンドの通し番号とする
};


/*
enum SUSPEND_REASON
{
    SR_NONE,
    SR_INTRRUPT, //割り込み
    SR_YIELD,    //Yield
    SR_SLEEP,    //
    SR_SUSPEND,  //
};
*/

enum EXEC_RET
{
    EX_FINISH,
    EX_SUSPEND,
    EX_ERROR,

};

enum EXEC_STEP
{
    ES_NONE,           //未実行
    ES_PRE,            //スクリプト実行前
    ES_SCRIPT,         //スクリプト実行中
    ES_POST,           //スクリプト実行後
};


enum E_TASK_STEP_TYPE
{
    STP_SETUP = 0,

    STP_ABORT  = -1,
    STP_TEST_01= -11,
    STP_TEST_02= -12,
    STP_TEST_03= -13,
    STP_TEST_04= -14,
    STP_TEST_05= -15,
};

enum EXEC_TYPE
{
    ET_ONE_SHOT = 0,
    ET_CYCLE    = 1,  
};


enum CMD_TRANSITION
{
    CT_OK,  //遷移OK
    CT_NG,  //遷移NG
    CT_ERROR,  //想定外の状態
    CT_NEXT,  //次回に遷移
};

enum CMD_RETURN
{
    RET_OK,
    RET_NG,
    RET_DELETE,
    RET_SKIP
};

/*
class CAsRefObj
{
public: 
    //!< コンストラクタ
    CAsRefObj():    m_iRefCount (1),
    m_bAppCtrl  (true),
    m_iRefObjId(-1)
    {;}



    //!< デストラクタ
    virtual ~CAsRefObj()
    {
        if (!m_bAppCtrl)
        {
        }
    }



    //<! 参照追加
    void AddRef()
    {
        ++m_iRefCount;
        if (m_iRefObjId != -1)
        {
            DB_PRINT(_T("AddRef %d: %d\n"), m_iRefObjId, m_iRefCount);
        }

    }

    //<! 参照開放
    void Release()
    {
        if (m_iRefCount > 0)
        {
            --m_iRefCount;
        }

        if (m_iRefObjId != -1)
        {
            DB_PRINT(_T("Release %d: %d\n"), m_iRefObjId, m_iRefCount);
        }

        if (!m_bAppCtrl)
        {
            if (m_iRefCount == 0)
            {
                delete this;
            }

        }
    }


    void SetAppCtrl()
    {
        m_bAppCtrl = true;
    }

    void SetScriptCtrl()
    {
        m_bAppCtrl = false;
        m_iRefCount = 0;
    }


    bool IsDelete()
    {
        if (m_bAppCtrl)
        {
            return false;
        }
        return (m_iRefCount == 0);
    }


    int  GetRefId(){return m_iRefObjId;}
    void SetRefId(int id){m_iRefObjId = id;}


protected:

    //! 参照カウント
    int m_iRefCount;

    //!　APP側所有権
    bool m_bAppCtrl;

    //! デバッグ用ID
    int   m_iRefObjId;

};

*/

class TCB: public CAsRefObj
                 
{


protected:
    friend CExecContextThread;


    //EXEC_CMD    eCmd  = TCMD_NONE;     //コマンド
    std::deque<EXEC_CMD >   queCmd;    //コマンド

    EXEC_CMD    eCmdOld  = TCMD_NONE; //前コマンド（デバッグ用）
    TASK_STS    eSts  = TS_WAIT;       //実行ステータス


    bool bResume = false;
    //bool               bIntrrupt = false;                //割り込み実行フラグ
    std::vector<TCB*>  lstChild;                         //子タスク

    //===============
    //プロパティ
    //===============
    int iId  = -1;        //タスクID
    int iPri =  2;        //タスクプライオリティ  0-4
    std::wstring      strTaskName;       
    EXEC_TYPE   eType = ET_ONE_SHOT;   //実行タイプ
    int  iStep = 0;

    //スレッド
    EXEC_COMMON::E_THREAD_TYPE   eThread = EXEC_COMMON::TH_GROUP_1;

    
    DWORD dwTime = 0;
    DWORD dwCycleTime = 100;
    DWORD dwSleepTime = 0;
    DWORD dwSleepStartTime = 0;
    double dExecTime = 0.0;
    CScriptObject* pScriptObject        = NULL;
    asIScriptFunction* pAsFunc          = NULL;
    std::function<int(asIScriptFunction*, TCB*)> func;
    //===============
    static int isDebugCount;
    int iDebugCount;

    //std::mutex m_MtxCmdSet;
    //std::unique_ptr<spinlock> pLock;
    std::mutex mtxLock;
    std::unique_ptr<AsScriptContext> pCurrentContext;

public:


    TCB();
    TCB(TCB const&) = delete;;
    //TCB& operator =(TCB const&) = delete;
    
    void CopyProerty(const TCB& tcb)
    {
        iId     = tcb.iId;
        iPri    = tcb.iPri;  
        eType   = tcb.eType;   
        iStep   = tcb.iStep;
        eThread = tcb.eThread;
        dwTime  = tcb.dwTime;
        dwCycleTime = tcb.dwCycleTime;
        dwSleepTime = tcb.dwSleepTime;
        dwSleepStartTime = tcb.dwSleepStartTime;
        pScriptObject    = tcb.pScriptObject;
        pAsFunc          = tcb.pAsFunc;
        func             = tcb.func;
        strTaskName      = tcb.strTaskName;

        //std::unique_ptr<AsScriptContext> pCurrentContext;
        //std::vector<TCB*>  lstChild;
        //bool               bIntrrupt;
    }

    virtual ~TCB()
    {
    }

    TCB& operator=(const TCB& r)
    {
        CopyProerty(r);
        queCmd  = r.queCmd;     //コマンド
        eCmdOld  = r.eCmdOld; //前コマンド（デバッグ用）
        eSts  = r.eSts;       //実行ステータス

        bResume = r.bResume;
        lstChild = r.lstChild;                         //子タスク
        return *this;
    }

    CMD_RETURN  ApplyCommand(CExecContextThread* pThread);


    void Exit()
    {
        for(TCB* pChild: lstChild)
        {
            pChild->Exit();
        }
        eSts = TS_EXT;
    }

    int Exec()
    {
        return func(pAsFunc, this);
    }

    void   Create(int iidIn);
    

    int    GetId() const    { return iId;}


    int    GetPriority() const   { return iPri;}
    bool   SetPriority(int iPri);

    std::wstring    GetTaskName() const{ return strTaskName;}
    bool            SetTaskName(std::wstring strName);

    EXEC_CMD  GetCmd();
    bool  SetCmd(EXEC_CMD eCmdSet);
    bool  PushCmd(EXEC_CMD eCmdSet);
    void  PopCmd();

    bool IsNext()
    {
        return (queCmd.size() > 1);
    }

    

    TASK_STS  GetStatus() const{ return eSts;}
    bool  SetStatus(TASK_STS eStsIn);

    EXEC_TYPE GetExecType() const{ return eType;}

    void SetExecType(EXEC_TYPE eType);

    DWORD  GetSleepStartTime() const{return dwSleepStartTime;}
    void  SetSleepStartTime();

    DWORD  GetSleepTime() const{return dwSleepTime;}
    void   ResetSleepTime(){dwSleepTime = 0;}
    void   SetSleepTime(DWORD dwTime);

    DWORD  GetCycleTime() const{return dwCycleTime;}
    bool   SetCycleTime(DWORD dwTime);

    double  GetExecTime() const{return dExecTime;}

    EXEC_COMMON::E_THREAD_TYPE GetThreadType() const{return eThread;}
    bool SetThreadType(EXEC_COMMON::E_THREAD_TYPE eType);

    int   GetStep()const{return iStep;}
    void  SetStep(int iStepInput){iStep = iStepInput;}

    int   GetDebugCount()const{return iDebugCount;}
    void  AddDebugCount(){
          isDebugCount++;
          iDebugCount = isDebugCount;
    }

    
    asIScriptFunction* GetAsFunc()const {return pAsFunc;}
    void SetAsFunc(asIScriptFunction* pFunc);

    void SetFunc(std::function<int(asIScriptFunction*, TCB*)> funcInput);
    std::function<int(asIScriptFunction*, TCB*)> GetFunc() const{return func;}

    CScriptObject* GetScriptObject() const{return pScriptObject;}
    void SetScriptObject(CScriptObject* pObject);
    
    asIScriptContext* GetCurrentContext() const 
    {
        if (pCurrentContext.get())
        {
            return pCurrentContext->Get();
        }
        else
        {
            return NULL;
        }
    }

    std::unique_ptr<AsScriptContext>& GetCurrentContextPtr()
    {
        return pCurrentContext;
    }

    bool IsResume() const { return bResume;}
    CMD_TRANSITION IsSetCmd(EXEC_CMD eCmdSet);



    virtual StdString GetStr(StdString strSpc) const;
    virtual StdString GetStr() const;

    //void ExecFsm();
protected:
    bool _SetExecType(EXEC_TYPE eExecType);

protected:
    /*
    TASK_STS _TaskWait();
    TASK_STS _TaskSuspend();
    TASK_STS _TaskRun();
    TASK_STS _TaskIntrrupt();
    TASK_STS _TaskWield(); 
    TASK_STS _TaskSleep(); 
    TASK_STS _TaskExt();
    */
};

