/**
 * @brief			COutputList実装ファイル
 * @file		    COutputList.cpp
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./COutputList.h"
#include "Resource.h"
#include "resource2.h"


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
BEGIN_MESSAGE_MAP(COutputList, CListBox)
	ON_WM_CONTEXTMENU()
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_COMMAND(ID_EDIT_CLEAR, OnEditClear)
	ON_COMMAND(ID_VIEW_OUTPUTWND, OnViewOutput)
	ON_WM_WINDOWPOSCHANGING()
    ON_CONTROL_REFLECT(LBN_DBLCLK, &COutputList::OnLbnDblclk)
    ON_CONTROL_REFLECT(LBN_SELCHANGE, &COutputList::OnLbnSelchange)
END_MESSAGE_MAP()


/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
 COutputList::COutputList():
  m_iListMax ( 50),
  m_bScroll  (  true),
  m_bTagJump ( false)
{
}

/**
 *  @brief  デストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
COutputList::~COutputList()
{
}

/**
*  @brief コンテキストメニュー選択
*  @param [in] pWnd   マウスの右ボタンがクリックされたウィンドウのハンドル
*  @param [in] point  クリックされたときの、カーソルの画面座標位置 
*  @retval           なし
*  @note  フレームワークより呼び出し
*/
void COutputList::OnContextMenu(CWnd* pWnd, CPoint point)
{
    CMenu menu;
    menu.LoadMenu(IDR_OUTPUT_POPUP);

    CMenu* pSubMenu = menu.GetSubMenu(0);

    if (AfxGetMainWnd()->IsKindOf(RUNTIME_CLASS(CMDIFrameWndEx)))
    {
        CMFCPopupMenu* pPopupMenu = new CMFCPopupMenu;

        if (!pPopupMenu->Create(this, point.x, point.y, (HMENU)pSubMenu->m_hMenu, FALSE, TRUE))
        {
            return;
        }

        ((CMDIFrameWndEx*)AfxGetMainWnd())->OnShowPopupMenu(pPopupMenu);
        UpdateDialogControls(this, FALSE);

        //TODO: pPopupMenuはリークしないの？
    }

    SetFocus();
}

/**
*  @brief   コピー要求
*  @param   なし
*  @retval  なし
*  @note    
*/
void COutputList::OnEditCopy()
{
    int iCnt = GetSelCount( );
    if (LB_ERR  == iCnt)
    {
        return;
    }

    if (iCnt == 0)
    {
        return;
    }

    std::vector<int>  lstItemNo;
    lstItemNo.resize(iCnt);

    int iRet;
    iRet = GetSelItems( iCnt, &lstItemNo[0]);


    if (iRet != iCnt)
    {
        STD_DBG(_T("GetSelItems %d != %d"), iCnt, iRet);
        return;
    }

    CString   strLine; 
    StdString strCopy;
    std::vector<int>::iterator ite;

    for (ite  = lstItemNo.begin();
         ite != lstItemNo.end();
         ite++)
    {
        GetText(*ite, strLine);
        strCopy += (LPCTSTR)strLine;
        strCopy += _T("\r\n");
    }

    SetClipboardText(strCopy.c_str());
}

/**
*  @brief   クリア要求
*  @param   なし
*  @retval  なし
*  @note    
*/
void COutputList::OnEditClear()
{
	ResetContent();
}

/**
*  @brief 非表示要求
*  @param   なし
*  @retval  なし
*  @note    
*/
void COutputList::OnViewOutput()
{
	CDockablePane* pParentBar = DYNAMIC_DOWNCAST(CDockablePane, GetOwner());
	CMDIFrameWndEx* pMainFrame = DYNAMIC_DOWNCAST(CMDIFrameWndEx, GetTopLevelFrame());

	if (pMainFrame != NULL && pParentBar != NULL)
	{
		pMainFrame->SetFocus();
		pMainFrame->ShowPane(pParentBar, FALSE, FALSE, FALSE);
		pMainFrame->RecalcLayout();

	}
}

/**
*  @brief クリップボードへコピー
*  @param   なし
*  @retval  なし
*  @note    
*/
BOOL COutputList::SetClipboardText(LPCTSTR lpszText)
{
    CString strText(lpszText);

    // 文字列が空の場合はコピーしない
    if( strText.IsEmpty() )
    {
        return FALSE;
    }

    // クリップボードのオープン
    if( !::OpenClipboard(NULL) )
    {
        return FALSE;
    }

    // ヒープ上にメモリ領域を確保し、文字列をコピー
    int iStrLen = strText.GetLength() + 1;
    HGLOBAL hMem = ::GlobalAlloc(GMEM_FIXED, iStrLen * sizeof(StdChar));

	if (hMem == 0)
	{
		return FALSE;
	}

    LPTSTR pMem = (LPTSTR)hMem;
    ::memset(pMem, 0, iStrLen * sizeof(StdChar));
    ::memcpy(pMem, (LPCTSTR)strText, (iStrLen - 1)* sizeof(StdChar));

    // クリップボードへ文字列をコピーし、クローズ
    ::EmptyClipboard();	// クリップボードの中身を空にする

#ifdef  UNICODE             
    ::SetClipboardData(CF_UNICODETEXT, hMem);
#else
    ::SetClipboardData(CF_TEXT, hMem);
#endif
    ::CloseClipboard();

    return TRUE;
}

/**
*  @brief スクロールの有無設定
*  @param   [in] bEnable true:スクロールあり
*  @retval  なし
*  @note    
*/
void COutputList::EnableScroll(bool bEnable)
{
    m_bScroll = bEnable;
}

/**
*  @brief タグジャンプ有無の設定
*  @param   [in] bEnable true:タグジャンプあり
*  @retval  なし
*  @note    
*/
void COutputList::EnableTagJump(bool bEnable)
{
    m_bTagJump = bEnable;
}

/**
*  @brief 文字追加
*  @param   [in] pMessage 文字列
*  @retval  なし
*  @note    
*/
void  COutputList::PrintOut(const StdChar* pMessage)
{
    AddString(pMessage);
    int iListNum = GetCount();
    if (iListNum == LB_ERR)
    {
        return;
    }

    iListNum--;
    
    int iCntOver = iListNum - m_iListMax;
    if ( iCntOver > 0 )
    {
        for( int iCnt = 0;  iCnt < iCntOver; iCnt++)
        {
            DeleteString(0);
        }
        iListNum = m_iListMax;
    }

    if (m_bScroll)
    {
        SetTopIndex(iListNum);
    }
}

/**
*  @brief ダブルクリック
*  @param   なし
*  @retval  なし
*  @note    
*/
void COutputList::OnLbnDblclk()
{
    if (!m_bTagJump)
    {
        return;
    }

    /*
    using namespace boost::xpressive;
    namespace xpr = boost::xpressive;

    StdStrXMatchResult matchResult;

    StdStrXregex static_mix  =  (s1 = +_) >> _T('(') >> (s2 = +_) >> _T(')') ;


    bRet = regex_match( strTabNmae , matchResult, static_mix);
    iMatchSize =  matchResult.size();

    if (!bRet)
    {
        return false;
    }

    if (iMatchSize != 3 )
    {
        return false;
    }

    StdString strTmp;
    strTmp = matchResult.str(1);
    */


}

void COutputList::OnLbnSelchange()
{
    // TODO: ここにコントロール通知ハンドラ コードを追加します。
}

/**
 * ウインドウ生成前処理
 * @param   [in]    cs	  
 * @return		
 * @note
 */
BOOL COutputList::PreCreateWindow(CREATESTRUCT& cs)
{
    cs.style |= LBS_EXTENDEDSEL;
    return CListBox::PreCreateWindow(cs);
}
