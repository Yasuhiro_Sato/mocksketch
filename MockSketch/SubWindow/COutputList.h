/**
 * @brief			COutputListヘッダーファイル
 * @file			COutputList.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *	
 *
 * $
 * $
 * 
 */

#pragma once

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/

/*---------------------------------------------------*/
/*  class                                            */
/*---------------------------------------------------*/

/**
 * @class   COutputList
 * @brief                        
 */
class COutputList : public CListBox
{
// コンストラクション
public:
    //!< コンストラクタ
	COutputList();

    //!< デストラクタ
	virtual ~COutputList();

    //!< スクロールの有無
	void EnableScroll(bool bEnable);

    //!< スクロールの有無
	void EnableTagJump(bool bEnable);

    //!< 文字追加
    void  PrintOut(const StdChar* pMessage);

protected:
    
    //!< コンテキストメニュー選択
    afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);

    //!< コピー要求
	afx_msg void OnEditCopy();

    //!< クリア要求
	afx_msg void OnEditClear();

    //!< 非表示要求
	afx_msg void OnViewOutput();

    //!< クリップボードへコピー
    BOOL SetClipboardText(LPCTSTR lpszText);


	DECLARE_MESSAGE_MAP()

protected:

    // 最大行数
    int   m_iListMax;

    // スクロールの有無
    bool   m_bScroll;

    // タグジャンプの有無
    bool   m_bTagJump;



public:
    afx_msg void OnLbnDblclk();
    afx_msg void OnLbnSelchange();
protected:
    virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
};
