// CustomDocablePane.cpp : 実装ファイル
//

#include "stdafx.h"
#include "MockSketch.h"
#include "./CustomDocablePane.h"


// CCustomDocablePane

IMPLEMENT_DYNAMIC(CCustomDocablePane, CDockablePane)

CCustomDocablePane::CCustomDocablePane()
{
    m_bMultiThreaded = TRUE;
}

CCustomDocablePane::~CCustomDocablePane()
{
}


BEGIN_MESSAGE_MAP(CCustomDocablePane, CDockablePane)
END_MESSAGE_MAP()



// CCustomDocablePane メッセージ ハンドラ


