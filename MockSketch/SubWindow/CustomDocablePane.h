#pragma once


// CCustomDocablePane

class CCustomDocablePane : public CDockablePane
{
	DECLARE_DYNAMIC(CCustomDocablePane)

public:
	CCustomDocablePane();
	virtual ~CCustomDocablePane();

protected:
	DECLARE_MESSAGE_MAP()
};


