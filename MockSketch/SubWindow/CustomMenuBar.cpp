// CustomMenuBar.cpp : 実装ファイル
//

#include "stdafx.h"
#include "MockSketch.h"
#include "./CustomMenuBar.h"
#include "Utility/Cutility.h"


// CCustomMenuBar

IMPLEMENT_DYNAMIC(CCustomMenuBar, CMFCMenuBar)

CCustomMenuBar::CCustomMenuBar()
{
    CMFCMenuBar::m_bShowAllCommands = TRUE;
    CMFCMenuBar::m_bShowAllMenusDelay = FALSE;
    m_bMultiThreaded = TRUE;
}

CCustomMenuBar::~CCustomMenuBar()
{
}

void CCustomMenuBar::OnReset ()
{
  	//HMENU hMenu = GetHMenu();
    //CUtil::ConvertMenuString(hMenu);
}


void CCustomMenuBar::CreateFromMenu(
   HMENU hMenu,
   BOOL bDefaultMenu,
   BOOL bForceUpdate)
{
  //CVisualStudioDemoApp::ChangeMenu(hMenu);
    CMFCMenuBar::m_bShowAllMenusDelay = FALSE;

   CMFCMenuBar::CreateFromMenu(hMenu, bDefaultMenu, bForceUpdate);
#if 0
   DispMenu(hMenu);
#endif
}



BOOL CCustomMenuBar::LoadState(LPCTSTR lpszProfileName, int nIndex, UINT /*uiID*/)
{
/*
	ENSURE(m_hDefaultMenu != NULL);

	CString strProfileName = ::AFXGetRegPath(strMenuProfile, lpszProfileName);

	// Save current maximize mode(system buttons are not saved!):
	BOOL bMaximizeMode = m_bMaximizeMode;
	SetMaximizeMode(FALSE, NULL, FALSE);

	CDocManager* pDocManager = AfxGetApp()->m_pDocManager;
	if (m_bAutoDocMenus && pDocManager != NULL)
	{
		// Walk all templates in the application:
		for (POSITION pos = pDocManager->GetFirstDocTemplatePosition(); pos != NULL;)
		{
			CMultiDocTemplateEx* pTemplate = (CMultiDocTemplateEx*) pDocManager->GetNextDocTemplate(pos);
			ASSERT_VALID(pTemplate);
			ASSERT_KINDOF(CDocTemplate, pTemplate);

			// We are interested in CMultiDocTemplate objects with the shared menu only....
			if (!pTemplate->IsKindOf(RUNTIME_CLASS(CMultiDocTemplate)) || pTemplate->m_hMenuShared == NULL)
			{
				continue;
			}

			UINT uiMenuResId = pTemplate->GetResId();
			ASSERT(uiMenuResId != 0);

			// Load menubar from registry and associate it with the template shared menu:
			BuildOrigItems(uiMenuResId);
			if (CMFCToolBar::LoadState(strProfileName, nIndex, uiMenuResId) && !m_bResourceWasChanged)
			{
				afxMenuHash.SaveMenuBar(pTemplate->m_hMenuShared, this);
			}
			else if (GetOwner()->GetSafeHwnd() != NULL)
			{
				// The following code was added to ensure that a
				// AFX_WM_RESETMENU message will be sent to the frame the
				// first time the application is loaded
				m_hMenu = NULL;
				CreateFromMenu(pTemplate->m_hMenuShared, FALSE);
				GetOwner()->SendMessage(AFX_WM_RESETMENU, uiMenuResId);
				afxMenuHash.SaveMenuBar(pTemplate->m_hMenuShared, this);
				m_hMenu = pTemplate->m_hMenuShared;
			}
		}
	}

	// Load defualt menubar:
	BuildOrigItems(m_uiDefMenuResId);

	if (CMFCToolBar::LoadState(strProfileName, nIndex, 0) && !m_bResourceWasChanged)
	{
		afxMenuHash.SaveMenuBar(m_hDefaultMenu, this);
	}
	else if (GetOwner()->GetSafeHwnd() != NULL)
	{
		// The following code was added to ensure that a AFX_WM_RESETMENU
		// message will be sent to the frame the first time the application
		// is loaded

		m_hMenu = NULL;
		CreateFromMenu(m_hDefaultMenu, TRUE);

		UINT uiResID = m_uiDefMenuResId;
		if (uiResID == 0)
		{
			// Obtain main window resource ID:
			uiResID = (UINT) GetOwner()->SendMessage(WM_HELPHITTEST);
		}

		GetOwner()->SendMessage(AFX_WM_RESETMENU, uiResID);
		afxMenuHash.SaveMenuBar(m_hDefaultMenu, this);
		m_hMenu = m_hDefaultMenu;
	}

	// Restore current menu:
	BOOL bLoaded = (m_hMenu != NULL && afxMenuHash.LoadMenuBar(m_hMenu, this));

	if (bMaximizeMode)
	{
		RestoreMaximizeMode(!bLoaded); // do not recalc layout if the menu was loaded
	}

	if (bLoaded)
	{
		GetParentFrame()->RecalcLayout();
		Invalidate();
		UpdateWindow();
	}

	AdjustLayout();
	RebuildAccelerationKeys();
*/
	GetParentFrame()->RecalcLayout();
	Invalidate();
	UpdateWindow();
	AdjustLayout();
	RebuildAccelerationKeys();

    CMFCMenuBar::m_bRecentlyUsedMenus = FALSE;
	return TRUE;
}

BOOL CCustomMenuBar::SaveState(LPCTSTR lpszProfileName, int nIndex, UINT /*uiID*/)
{
    //保存はしない

    /*
	ENSURE(m_hDefaultMenu != NULL);

	CString strProfileName = ::AFXGetRegPath(strMenuProfile, lpszProfileName);

	afxMenuHash.SaveMenuBar(m_hMenu, this);

	// Save current maximize mode(system buttons are not saved!):
	BOOL bMaximizeMode = m_bMaximizeMode;
	SetMaximizeMode(FALSE, NULL, FALSE);

	CDocManager* pDocManager = AfxGetApp()->m_pDocManager;
	if (m_bAutoDocMenus && pDocManager != NULL)
	{
		// Walk all templates in the application:
		for (POSITION pos = pDocManager->GetFirstDocTemplatePosition(); pos != NULL;)
		{
			CMultiDocTemplateEx* pTemplate = (CMultiDocTemplateEx*) pDocManager->GetNextDocTemplate(pos);
			ASSERT_VALID(pTemplate);
			ASSERT_KINDOF(CDocTemplate, pTemplate);

			// We are interessing CMultiDocTemplate objects with
			// the sahred menu only....
			if (!pTemplate->IsKindOf(RUNTIME_CLASS(CMultiDocTemplate)) || pTemplate->m_hMenuShared == NULL)
			{
				continue;
			}

			UINT uiMenuResId = pTemplate->GetResId();
			ASSERT(uiMenuResId != 0);

			// Load menubar associated with the template shared menu and
			// save it in the registry:
			if (afxMenuHash.LoadMenuBar(pTemplate->m_hMenuShared, this))
			{
				BuildOrigItems(uiMenuResId);
				CMFCToolBar::SaveState(strProfileName, nIndex, uiMenuResId);
			}
		}
	}

	// Save default menu:
	if (afxMenuHash.LoadMenuBar(m_hDefaultMenu, this))
	{
		BuildOrigItems(m_uiDefMenuResId);
		CMFCToolBar::SaveState(strProfileName, nIndex, 0);
	}

	// Restore current menu:
	BOOL bRestored = (m_hMenu != NULL && afxMenuHash.LoadMenuBar(m_hMenu, this));

	if (bMaximizeMode)
	{
		RestoreMaximizeMode(!bRestored);
	}

	AdjustSizeImmediate();

	if (bRestored)
	{
		GetParentFrame()->RecalcLayout();
		Invalidate();
		UpdateWindow();
	}

	AdjustLayout();
    */
	return TRUE;
}

//メニュー項目を表示する
void CCustomMenuBar::DispMenu(HMENU hMenu, LPCTSTR strSpc)
{
    CMenu menu;
    MENUITEMINFO  info;
    CString strSpcNext = strSpc;
    CString strItem;
    menu.Attach(hMenu);

    int iMenuCount = menu.GetMenuItemCount();
    for (int iCnt =0; iCnt < iMenuCount; iCnt++)

    {
        memset(&info, 0, sizeof(info));
        info.cbSize = sizeof(info);
        info.fMask = MIIM_TYPE | MIIM_SUBMENU;
        menu.GetMenuItemInfo(iCnt, &info, TRUE);
        strItem    = strSpc;

        CString strOut;
        if(info.fType == MFT_SEPARATOR)
        {
            strItem += _T("----------------");

        }
        else if( info.fType == MFT_STRING)
        {
            CString strMenuItem;

            menu.GetMenuString(iCnt, strMenuItem, MF_BYPOSITION);
            strItem += strMenuItem;
            //(LPCTSTR)info.dwTypeData
        }
        else
        {
            strItem += _T("??????");
        }

        DB_PRINT(_T("%s\n"), (LPCTSTR)strItem);

        if (info.hSubMenu != NULL)
        {
            strSpcNext += _T(" ");
            DispMenu(info.hSubMenu, (LPCTSTR) strSpcNext);
        }
    }

    menu.Detach();

}

BOOL CCustomMenuBar::BuildOrigItems(UINT uiMenuResID)
{
    return CMFCMenuBar::BuildOrigItems(uiMenuResID);
}

BOOL CCustomMenuBar::Create(CWnd* pParentWnd, DWORD dwStyle, UINT nID)
{
	return CMFCMenuBar::Create(pParentWnd, dwStyle, nID);
}

BOOL CCustomMenuBar::CreateEx(CWnd* pParentWnd, DWORD dwCtrlStyle, DWORD dwStyle, CRect rcBorders, UINT nID)
{
	return CMFCMenuBar::CreateEx(pParentWnd, dwCtrlStyle, dwStyle, rcBorders, nID);
}

BOOL CCustomMenuBar::RestoreOriginalState()
{
	return CMFCMenuBar::RestoreOriginalState();
}

BOOL CCustomMenuBar::PreTranslateMessage(MSG* pMsg)
{
	return CMFCMenuBar::PreTranslateMessage(pMsg);
}

BOOL CCustomMenuBar::OnSetDefaultButtonText(CMFCToolBarButton* pButton)
{
	return CMFCMenuBar::OnSetDefaultButtonText(pButton);
}

void CCustomMenuBar::OnChangeHot(int iHot)
{
	return CMFCMenuBar::OnChangeHot(iHot);
}

int CCustomMenuBar::FindDropIndex(const CPoint point, CRect& rectDrag) const
{
	return CMFCMenuBar::FindDropIndex(point, rectDrag);
}

void CCustomMenuBar::ResetImages()
{
	return CMFCMenuBar::ResetImages();
}

void CCustomMenuBar::DoPaint(CDC* pDC)
{
	return CMFCMenuBar::DoPaint(pDC);
}


BEGIN_MESSAGE_MAP(CCustomMenuBar, CMFCMenuBar)
END_MESSAGE_MAP()



// CCustomMenuBar メッセージ ハンドラ


