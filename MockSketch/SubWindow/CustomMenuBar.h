#pragma once


// CCustomMenuBar

class CCustomMenuBar : public CMFCMenuBar
{
	DECLARE_DYNAMIC(CCustomMenuBar)

public:
	CCustomMenuBar();
	virtual ~CCustomMenuBar();

    virtual void CreateFromMenu(
       HMENU hMenu,
       BOOL bDefaultMenu = FALSE,
       BOOL bForceUpdate = FALSE
    );
    virtual void OnReset ();

    void DispMenu(HMENU hMenu, LPCTSTR strSpc = _T(" "));

protected:

	virtual BOOL BuildOrigItems(UINT uiMenuResID); // Required for the SmartResourceUpdate

public:
    virtual BOOL Create(CWnd* pParentWnd, DWORD dwStyle = AFX_DEFAULT_TOOLBAR_STYLE, UINT nID = AFX_IDW_MENUBAR);
	virtual BOOL CreateEx(CWnd* pParentWnd, DWORD dwCtrlStyle = TBSTYLE_FLAT, DWORD dwStyle = AFX_DEFAULT_TOOLBAR_STYLE,
		CRect rcBorders = CRect(1, 1, 1, 1), UINT nID = AFX_IDW_MENUBAR);


	//virtual CSize CalcFixedLayout(BOOL bStretch, BOOL bHorz);
	//virtual CSize CalcLayout(DWORD dwMode, int nLength = -1);

	//virtual int CalcMaxButtonHeight();

	//virtual void AdjustLocations();
	//virtual BOOL OnSendCommand(const CMFCToolBarButton* pButton);

	//virtual INT_PTR OnToolHitTest(CPoint point, TOOLINFO* pTI) const;

	virtual BOOL LoadState(LPCTSTR lpszProfileName = NULL, int nIndex = -1, UINT uiID = (UINT) -1);
	virtual BOOL SaveState(LPCTSTR lpszProfileName = NULL, int nIndex = -1, UINT uiID = (UINT) -1);

	//virtual BOOL CanBeRestored() const { return TRUE; }
	//virtual BOOL CanBeClosed() const { return FALSE; }
	//virtual BOOL AllowChangeTextLabels() const { return FALSE; }
	//virtual BOOL IsButtonExtraSizeAvailable() const { return FALSE; }
	//virtual BOOL AllowShowOnPaneMenu() const { return FALSE; }
	//virtual void OnDefaultMenuLoaded(HMENU) {};
    

	virtual BOOL RestoreOriginalState();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
    //virtual int GetRowHeight() const;
	//virtual int GetColumnWidth() const

	virtual BOOL OnSetDefaultButtonText(CMFCToolBarButton* pButton);
	virtual void OnChangeHot(int iHot);
	
	//virtual int GetAvailableExpandSize() const { return m_bExclusiveRow ? 0xFFFF /*unlimited size*/ : CMFCToolBar::GetAvailableExpandSize(); }

protected:
	virtual int FindDropIndex(const CPoint point, CRect& rectDrag) const;
	virtual void ResetImages();
	//virtual BOOL IsPureMenuButton(CMFCToolBarButton* pButton) const;
    virtual void DoPaint(CDC* pDC);

protected:
	DECLARE_MESSAGE_MAP()
};


