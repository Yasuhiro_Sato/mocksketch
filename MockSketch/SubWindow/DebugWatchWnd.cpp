/**
 * @brief			CDebugWatchWnd実装ファイル
 * @file			CDebugWatchWnd.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *	
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/

#include "stdafx.h"

#include "./DebugWatchWnd.h"
#include "Resource.h"
#include "resource2.h"
#include "MainFrm.h"
#include "MockSketch.h"
#include "Script/EditorView.h"
#include "View/CURRENT_BREAK_CONTEXT.h"
#include "Utility/CUtility.h"
#include "System/CSystem.h"
#include "Script/CScriptObject.h"
#include "VAL_DATA.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

BEGIN_MESSAGE_MAP(CDebugWatchWnd, CDockablePane)
	ON_WM_CREATE()
	ON_WM_SIZE()
    
    //ON_MESSAGE(WM_NOTIFY,           OnNotify)
    ON_MESSAGE(WM_BREAK_START,      OnBreakStart)
    ON_MESSAGE(WM_BREAK_END,        OnBreakEnd)
    ON_MESSAGE(WM_DELETE_WATCH,     OnDeleteWatch)

    ON_COMMAND(ID_EDIT_COPY , OnEditCopy)
    ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)

    ON_COMMAND(ID_MNU_WATCH_EDIT,       OnMnuWatchEdit)
    ON_COMMAND(ID_MNU_WATCH_SEL,        OnMnuWatchSel)
    ON_COMMAND(ID_MNU_WATCH_CLEAR,      OnMnuWatchClear)
    ON_COMMAND(ID_MNU_WATCH_HEX,        OnMnuWatchHex)
    ON_COMMAND(ID_MNU_WATCH_CONTRACT,   OnMnuWatchContract)

    ON_UPDATE_COMMAND_UI(ID_EDIT_COPY , OnUpdateEditCopy)
    ON_UPDATE_COMMAND_UI(ID_EDIT_PASTE, OnUpdateEditPaste)

    ON_UPDATE_COMMAND_UI(ID_MNU_WATCH_EDIT,       OnUpdateMnuWatchEdit)
    ON_UPDATE_COMMAND_UI(ID_MNU_WATCH_SEL,        OnUpdateMnuWatchSel)
    ON_UPDATE_COMMAND_UI(ID_MNU_WATCH_CLEAR,      OnUpdateMnuWatchClear)
    ON_UPDATE_COMMAND_UI(ID_MNU_WATCH_HEX,        OnUpdateMnuWatchHex)
    ON_UPDATE_COMMAND_UI(ID_MNU_WATCH_CONTRACT,   OnUpdateMnuWatchContract)


    ON_MESSAGE(WM_MODE_CHANGE, OnModeChange)

    ON_WM_CONTEXTMENU()
END_MESSAGE_MAP()

/**
*  @brief  コンストラクター.
*  @param  なし
*  @retval なし     
*  @note
*/
CDebugWatchWnd::CDebugWatchWnd():
m_pCurBreakContext(NULL)
{
}

/**
*  @brief  デストラクタ.
*  @param  なし
*  @retval なし     
*  @note
*/
CDebugWatchWnd::~CDebugWatchWnd()
{
}


/**
 * @brief       生成時処理要求
 * @param [in]    lpCreateStruct	初期化パラメータへのポインタ	 	  
 * @return		
 * @note	 
 * @exception   なし
 */
int CDebugWatchWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (CDockablePane::OnCreate(lpCreateStruct) == -1)
        return -1;

    RECT rcWin;

    rcWin.top    = lpCreateStruct->y;
    rcWin.bottom = lpCreateStruct->y + lpCreateStruct->cy;
    rcWin.left   = lpCreateStruct->x;
    rcWin.right  = lpCreateStruct->x + lpCreateStruct->cx;
 

    //LVS_EDITLABELS | LVS_ALIGNLEFT |
   //m_Font.CreateStockObject(DEFAULT_GUI_FONT);
    if (!m_lscWatch.Create( WS_CHILD|WS_VISIBLE
                              
                            , rcWin, this, IDC_LSC_WATCH))
    {
        STD_DBG(_T("Watch Create fail"));
        return -1;      // 作成できない場合
    }

    m_lscWatch.SetStyle( TLC_TREELIST| TLC_TREELINE | TLC_BUTTON |
                         TLC_HEADER | TLC_HGRID | TLC_VGRID |TLC_TGRID
                         );

    //m_lscWatch.SetState( TLS_MODIFY  );

    /*
    CFont* pFont1 = &DRAW_CONFIG->fntListCtrl;
    SetFont(&DRAW_CONFIG->fntListCtrl);
	CFont* pFont = GetFont();
    */
    m_lscWatch.SetFont(DRAW_CONFIG->fntListCtrl);
    
    int iWidth;
    int iPos;
    StdString strHeader;

    CUtil::FormatedText(GET_SYS_STR(STR_WAT_NAME), &iWidth, &iPos, &strHeader);
    m_lscWatch.InsertColumn(strHeader.c_str(), TLF_DEFAULT_LEFT, iWidth);

    CUtil::FormatedText(GET_SYS_STR(STR_WAT_VALUE), &iWidth, &iPos, &strHeader);
    m_lscWatch.InsertColumn(strHeader.c_str(), TLF_DEFAULT_LEFT, iWidth);

    CUtil::FormatedText(GET_SYS_STR(STR_WAT_TYPE), &iWidth, &iPos, &strHeader);
    m_lscWatch.InsertColumn(strHeader.c_str(), TLF_DEFAULT_LEFT, iWidth);

    //編集可
    m_lscWatch.SetColumnModify( COL_VAR_NAME, TLM_EDIT );

    m_lscWatch.InsertItem(  _T(""),  TLI_ROOT, TLI_LAST );
    return 0;
}




/**
 *  @brief ウインドウサイズ変更
 *  @param [in] nType   要求されるサイズ変更のタイプ  (SIZE_MAXIMIZED....)
 *  @param [in] cx      クライアント領域の新しい幅 
 *  @param [in] cy      クライアント領域の新しい高さ
 *  @retval           なし
 *  @note
 */
void CDebugWatchWnd::OnSize(UINT nType, int cx, int cy)
{
	CDockablePane::OnSize(nType, cx, cy);

	m_lscWatch.SetWindowPos (NULL, -1, -1, cx, cy, SWP_NOMOVE | SWP_NOACTIVATE | SWP_NOZORDER);
}


/**
 *  @brief リスト最後尾のアイテムを取得
 *  @param なし
 *  @retval           なし
 *  @note  
 */
CTreeListItem* CDebugWatchWnd::GetLastItem()
{
    CTreeListItem* pOld = NULL;
    CTreeListItem* pRoot = m_lscWatch.GetRootItem()->m_pParent;
    CTreeListItem* pNext = m_lscWatch.GetNextItem( pRoot, TLGN_CHILD );

    while(pNext != NULL)
    {
        pOld = pNext;
        pNext = m_lscWatch.GetNextItem( pNext, TLGN_NEXT );
    }
    return pOld;
}

/**
 *  @brief アイテム追加
 *  @param  なし
 *  @retval 追加アイテム
 *  @note   
 */
CTreeListItem* CDebugWatchWnd::AddItem()
{
    CTreeListItem*  pLast;
    CTreeListItem*  pRet = NULL;

    pLast = GetLastItem();

    if (pLast != NULL)
    {
        StdString strName;
        strName = pLast->GetText(COL_VAR_NAME);
        if (strName == _T(""))
        {
            pRet = pLast;
        }
    }

    if (pRet == NULL)
    {
        pRet = m_lscWatch.InsertItem( _T(""),  TLI_ROOT, TLI_LAST );
    }

    return pRet;
}


/**
 *  @brief リスト最後尾に空白行を追加
 *  @param なし
 *  @retval           なし
 *  @note  
 */
void CDebugWatchWnd::AddLast()
{
    CTreeListItem*  pLast;
    pLast = GetLastItem();

    if (pLast != NULL)
    {
        StdString strName;
        strName = pLast->GetText(COL_VAR_NAME);
        if (strName == _T(""))
        {
            return;
        }
    }
    m_lscWatch.InsertItem( _T(""),  TLI_ROOT, TLI_LAST );
}

/**
 *  @brief データ設定
 *  @param [out] pData  設定データ 
 *  @param [in]  pRoot  ルートアイテム
 *  @retval           なし
 *  @note  通常は pRoot = TLI_ROOT;
 */
void CDebugWatchWnd::SetData(VAL_DATA* pData, CTreeListItem* pRoot)
{
    CTreeListItem*  pTreeData;

    if (pRoot == TLI_ROOT)
    {
        pTreeData = AddItem();
    }
    else
    {
        pTreeData = pRoot;
    }

    pTreeData->SetText( pData->strName.c_str()       , COL_VAR_NAME);
    pTreeData->SetText( pData->strVal.c_str()        , COL_VAR_VAL);
    pTreeData->SetText( pData->strTypeName.c_str()   , COL_VAR_TYPE);

    CTreeListItem*  pNext;
    if (pData->lstVal.size() != 0)
    {
        pNext = m_lscWatch.GetChildItem( pTreeData);

        foreach(VAL_DATA& valDat, pData->lstVal)
        {
            if (!pNext)
            {
                pNext = m_lscWatch.InsertItem( _T(""),  pTreeData, TLI_LAST );
            }
            SetData(&valDat, pNext);
            pNext = m_lscWatch.GetNextSiblingItem(pNext);
        }
    }
}

/**
 *  @brief  アイテム名称取得
 *  @param  [in] pItem  アイテム 
 *  @retval アイテム名称
 *  @note  
 */
StdString CDebugWatchWnd::GetItemName(CTreeListItem* pItem)
{
    CTreeListItem* pParent;
    std::string strVarName;
    StdString strText;
    

    STD_ASSERT(pItem != NULL);
    STD_ASSERT(pItem != TLI_ROOT);

    pParent = pItem->m_pParent;
    if (pParent ==  NULL)
    {
        strText = pItem->GetText( COL_VAR_NAME );
    }
    else
    {
        strText = GetItemName(pParent);
        if (strText != _T(""))
        {
            strText += _T(".");
            strText += pItem->GetText( COL_VAR_NAME );
        }
        else
        {
            strText = pItem->GetText( COL_VAR_NAME );
        }
    }
    return strText;
}

/**
 *  @brief アイテムリストアップ
 *  @param [in]  pItem  リストアップを行うアイテム
 *  @param [out] pList  リストアップ
 *  @retval     なし
 *  @note
 */
void CDebugWatchWnd::TraceItem(CTreeListItem* pItem, std::deque<CTreeListItem*>* pList)
{
    CTreeListItem*	pChild;

    if (pItem == NULL)
    {
        return;
    }

    pList->push_back(pItem);

    pChild = pItem->m_pChild;
    if (pChild != NULL)
    {
        TraceItemChild(pChild, pList);
    }
}

/**
 *  @brief アイテムリストアップ
 *  @param [in]  pItem  リストアップを行うアイテム
 *  @param [out] pList  リストアップ
 *  @retval     なし
 *  @note
 */
void CDebugWatchWnd::TraceItemChild(CTreeListItem* pItem, std::deque<CTreeListItem*>* pList)
{

    CTreeListItem*	pNext;
    CTreeListItem*	pChild;


    if (pItem == NULL)
    {
        return;
    }

    pList->push_back(pItem);

    pChild = pItem->m_pChild;
    if (pChild != NULL)
    {
        TraceItemChild(pChild, pList);
    }

    pNext = pItem->m_pNext;
    if ((pNext != TLI_LAST)&&
        (pNext != NULL))
    {
        TraceItemChild(pNext, pList);
    }
}

/**
 *  @brief 子アイテム削除
 *  @param [in] pItem  削除アイテム 
 *  @retval     なし
 *  @note
 */
void CDebugWatchWnd::DeleteChildItem(CTreeListItem* pItem)
{

    std::deque<CTreeListItem*> lstDel;
    TraceItem(pItem, &lstDel);
    
    if (lstDel.size() > 0)
    {
        lstDel.pop_front();
    }
    
    pItem->m_pChild = NULL;

#ifdef _DEBUG
    StdString strName;
#endif

    foreach(CTreeListItem* pDelItem, lstDel)
    {
#ifdef _DEBUG
        strName = pDelItem->GetText( COL_VAR_NAME );
        DB_PRINT(_T("Delete %s \n"), strName.c_str());
#endif
        delete pDelItem;
        pDelItem = NULL;
    }
}


/**
 *  @brief テキスト更新
 *  @param [out] pItem     更新アイテム 
 *  @param [in]  pValData  更新データ 
 *  @retval なし
 *  @note
 */
void CDebugWatchWnd::UpdateText(CTreeListItem* pItem, VAL_DATA*  pValData)
{
    CTreeListItem* pChild;
    CTreeListItem* pNext;
    StdString strVarName;
    pNext = pItem->m_pChild;

    if (pNext == NULL)
    {
        return;
    }

    foreach(VAL_DATA& val, pValData->lstVal)
    {
        strVarName = pNext->GetText( COL_VAR_NAME);

        STD_ASSERT(strVarName == val.strName);

        if (strVarName == val.strName)
        {
            pNext->SetText(val.strVal.c_str(), COL_VAR_VAL);
        }

        pChild =  pNext->m_pChild;

        if (pChild != NULL)
        {
            UpdateText(pNext, &val);
        }

        pNext  = pNext->m_pNext;
        if(pNext == NULL)
        {
            break;
        }
    }
}


/**
 *  @brief データ更新
 *  @param [in] pItem  更新アイテム 
 *  @retval     false pItem削除
 *  @note
 */
void CDebugWatchWnd::UpdateData(CTreeListItem** ppItem)
{
    CTreeListItem* pItem;

    pItem = *ppItem;

    CTreeListItem* pNext;

    StdString strName;
    VAL_DATA  valData;
    strName  = GetItemName(pItem);
    bool bRet = false;


    if (strName == _T(""))
    {
        return;
    }
 
    valData.bValid = false;
    valData.bArray = false;


#ifdef UNICODE
    std::string strNameA;
    strNameA = CUtil::StringToChar(strName);

    if (m_pCurBreakContext != NULL)
    {
        bRet = m_pCurBreakContext->pScriptObj->GetVarData(&valData, strNameA);
    }

#else
    if (m_pCurBreakContext != NULL)
    {
        bRet = m_pCurBreakContext->pScriptObj->GetVarData(&valData, strName);
    }
#endif

    pNext = pItem;
    //bool bReset = false;
    if (bRet)
    {
        SetData(&valData, pItem);

        AddLast();
    }
    else
    {
        if  (pItem->m_pChild != NULL)
        {
            DeleteChildItem(pItem);
        }

        if (strName != _T(""))
        {
            //TODO:テキスト変更
            pItem->SetText(_T("見つかりません"), COL_VAR_VAL);
        }
        else
        {
            pNext = pItem->m_pNext;
            if (pItem->m_pNext != NULL)
            {
                //この呼出内では削除出来ないため後で削除する
                PostMessage(WM_DELETE_WATCH, reinterpret_cast<WPARAM>(pItem), 0);
            }
        }
    }
    m_lscWatch.Invalidate();

    if (pNext)
    {
        pNext = pNext->m_pNext;
        if (pNext != NULL)
        {
            UpdateData(&pNext);
        }
    }
    return;
}


/**
 *  @brief ブレークポイント開始通知
 *  @param [in] pData   
 *  @retval           なし
 *  @note
 */
LRESULT CDebugWatchWnd::OnBreakStart(WPARAM breakContext, LPARAM lparam)
{
    CURRENT_BREAK_CONTEXT* pBreakContext;
    pBreakContext = reinterpret_cast<CURRENT_BREAK_CONTEXT*>(breakContext);

    VAL_DATA    valData;

    if (m_pCurBreakContext != pBreakContext)
    {
        //新しいモジュールが選択された
    }
    m_pCurBreakContext = pBreakContext;
    CTreeListItem* pRoot = m_lscWatch.GetRootItem()->m_pParent;
    if (pRoot != NULL)
    {
        if (pRoot->m_pChild != NULL)
        {
            UpdateData(&pRoot->m_pChild);
        }
    }
    return 0;
}


/**
 *  @brief ブレークポイント終了通知
 *  @param [in] pData   
 *  @retval           なし
 *  @note
 */
LRESULT CDebugWatchWnd::OnBreakEnd(WPARAM breakContext, LPARAM lparam)
{
    CURRENT_BREAK_CONTEXT* pContext;
    pContext = reinterpret_cast<CURRENT_BREAK_CONTEXT*>(breakContext);
    return 0;
}

/**
 *  @brief ウォッチアイテム削除
 *  @param [in] pDeleteItem   
 *  @retval           なし
 *  @note
 */
LRESULT CDebugWatchWnd::OnDeleteWatch(WPARAM pDeleteItem, LPARAM lparam)
{
    CTreeListItem* pItem;
    pItem = reinterpret_cast<CTreeListItem*>(pDeleteItem);
    m_lscWatch.DeleteItem(pItem);
    return 0;

}


/**
 *  @brief モード変更
 *  @param [in] wParam   アプリケーションモード(EXEC_COMMON::E_APP_MODE)
 *  @param [in] lParam   オブジェクト定義      (CObjectDef)
 *  @retval
 *  @note
 */
LRESULT CDebugWatchWnd::OnModeChange(WPARAM wParam, LPARAM lParam)
{
    return 0;
}

/**
 *  @brief  イベント発生通知.
 *  @param  [in]  wParam 
 *  @param  [in]  lParam 
 *  @param  [out] pResult 
 *  @retval なし     
 *  @note
 */
BOOL CDebugWatchWnd::OnNotify(WPARAM wParam, LPARAM pNMHDR, LRESULT* pResult)
{
    BOOL bRet;
    bRet = CDockablePane::OnNotify(wParam, pNMHDR, pResult);
    if (IDC_LSC_WATCH != wParam)
    {
        return bRet;
    }

    *pResult = 0;
    NMTREELIST *pTreeList = reinterpret_cast<NMTREELIST*>(pNMHDR);
    CTreeListItem*  pItem;
    int             iCol;
    DWORD           dwMessage;

    pItem = pTreeList->pItem;
    iCol  = pTreeList->iCol;
    dwMessage = pTreeList->hdr.code;
    
    switch(dwMessage)
    {
        case TLN_ITEMFINISHED:
             UpdateData(&pItem);
             break;

        default:
            break;
    }

    StdString strMsg;

    switch(dwMessage)
    {
        case TLN_SELCHANGING:   {strMsg = _T("SELCHANGING"); break;}
        case TLN_SELCHANGED:    {strMsg = _T("SELCHANGED"); break;}
        case TLN_GETDISPINFO:   {strMsg = _T("GETDISPINFO"); break;}
        case TLN_SETDISPINFO:   {strMsg = _T("SETDISPINFO"); break;}
        case TLN_ITEMEXPANDING: {strMsg = _T("ITEMEXPANDING"); break;}
        case TLN_ITEMEXPANDED:  {strMsg = _T("ITEMEXPANDED"); break;}
        case TLN_BEGINDRAG:     {strMsg = _T("BEGINDRAG"); break;}
        case TLN_BEGINRDRAG:    {strMsg = _T("BEGINRDRAG"); break;}
        case TLN_DELETEITEM:    {strMsg = _T("DELETEITEM"); break;}
        case TLN_BEGINCONTROL:  {strMsg = _T("BEGINCONTROL"); break;}
        case TLN_ENDCONTROL:    {strMsg = _T("ENDCONTROL"); break;}
        case TLN_ITEMUPDATING:  {strMsg = _T("ITEMUPDATING"); break;}
        case TLN_ITEMUPDATED:   {strMsg = _T("ITEMUPDATED"); break;}
        case TLN_ITEMFINISHED:  {strMsg = _T("ITEMFINISHED"); break;}
        case TLN_DRAGENTER:     {strMsg = _T("DRAGENTER"); break;}
        case TLN_DRAGLEAVE:     {strMsg = _T("DRAGLEAVE"); break;}
        case TLN_DRAGOVER:      {strMsg = _T("DRAGOVER"); break;}
        case TLN_DROP:          {strMsg = _T("DROP"); break;}
        case TLN_ITEMCHECK:     {strMsg = _T("ITEMCHECK"); break;}
        case TLN_ITEMLOCK:      {strMsg = _T("ITEMLOCK"); break;}
        case TLN_ITEMIMAGE:     {strMsg = _T("ITEMIMAGE"); break;}
        default:     {strMsg = _T("UNKNOWN "); break;}
    }

    StdString strOut;

    if (pItem)
    {
        strOut = CUtil::StrFormat(_T("Notify %s, Col:%d Text:%s\n"),
                             strMsg.c_str(),
                             iCol,
                             pItem->GetText());
    }
    else
    {
        strOut = CUtil::StrFormat(_T("Notify %s, Col:%d \n"),
                             strMsg.c_str(),
                             iCol);
    }

    DB_PRINT(strOut.c_str());


    return bRet;
}

/**
 *  @brief コンテキストメニュー選択
 *  @param [in] pWnd   マウスの右ボタンがクリックされたウィンドウのハンドル
 *  @param [in] point  クリックされたときの、カーソルの画面座標位置 
 *  @retval           なし
 *  @note
 */
void CDebugWatchWnd::OnContextMenu(CWnd* pWnd, CPoint point)
{

    if (pWnd != &m_lscWatch)
    {
        CDockablePane::OnContextMenu(pWnd, point);
        return;
    }

    CTreeListItem*	pItemHit;
    int iFlags   = 0;
    int iSubItem = 0;

    if (point != CPoint(-1, -1))
    {
        // クリックされた項目の選択:
        CPoint ptClick = point;
        m_lscWatch.ScreenToClient(&ptClick);
        pItemHit = m_lscWatch.HitTest(ptClick, &iFlags, &iSubItem);
        if (pItemHit != NULL)
        {
            m_lscWatch.SelectItem(pItemHit);
        }
    }

    m_lscWatch.SetFocus();
    theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_WATCH, point.x, point.y, this, TRUE);
}



//!< メニュー -> コピー
void CDebugWatchWnd::OnEditCopy()
{

}

    //!< メニュー -> コピー UI更新
void CDebugWatchWnd::OnUpdateEditCopy(CCmdUI* pCmdUI)
{
}


//!< メニュー -> 貼りつけ
void CDebugWatchWnd::OnEditPaste()
{
}


    //!< メニュー -> 貼りつけ UI更新
void CDebugWatchWnd::OnUpdateEditPaste(CCmdUI* pCmdUI)
{
}



    //!< メニュー -> 値の編集
void CDebugWatchWnd::OnMnuWatchEdit()
{
}


    //!< メニュー -> 値の編集 UI更新
void CDebugWatchWnd::OnUpdateMnuWatchEdit(CCmdUI* pCmdUI)
{
}



    //!< メニュー -> 全て選択
void CDebugWatchWnd::OnMnuWatchSel()
{
}


    //!< メニュー -> 全て選択 UI更新
void CDebugWatchWnd::OnUpdateMnuWatchSel(CCmdUI* pCmdUI)
{
}



    //!< メニュー -> 全てクリア
void CDebugWatchWnd::OnMnuWatchClear()
{
}


    //!< メニュー -> 全てクリア UI更新
void CDebugWatchWnd::OnUpdateMnuWatchClear(CCmdUI* pCmdUI)
{
}



    //!< メニュー -> 16進表示
void CDebugWatchWnd::OnMnuWatchHex()
{
}


    //!< メニュー -> 16進表示 UI更新
void CDebugWatchWnd::OnUpdateMnuWatchHex(CCmdUI* pCmdUI)
{
}



    //!< メニュー -> 1レベル上に折りたたむ
void CDebugWatchWnd::OnMnuWatchContract()
{
}


    //!< メニュー -> 1レベル上に折りたたむ UI更新
void CDebugWatchWnd::OnUpdateMnuWatchContract(CCmdUI* pCmdUI)
{
}


//*******************************
//             TEST
//*******************************
#ifdef _DEBUG

#include "MockSketch.h"
void TEST_CDebugWatchWnd()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

#if 0
    CDebugWatchWnd* pDebugWnd;



    VAL_DATA   valData;
    VAL_DATA   tmpValData;
    VAL_DATA   tmpValData2;


    valData.bValid = true;
    valData.bArray = false;
    valData.strName      = _T("test_data");
    valData.strTypeName  = _T("TEST_DATA");
    valData.strVal       = _T("-----");


    tmpValData.bValid = true;
    tmpValData.bArray = false;
    tmpValData.strName      = _T("iData_1");
    tmpValData.strTypeName  = _T("int");
    tmpValData.strVal       = _T("1");

    valData.lstVal.push_back(tmpValData);

    tmpValData.bValid = true;
    tmpValData.bArray = false;
    tmpValData.strName      = _T("dData_1");
    tmpValData.strTypeName  = _T("double");
    tmpValData.strVal       = _T("1.1");

    valData.lstVal.push_back(tmpValData);


    tmpValData2.bValid = true;
    tmpValData2.bArray = false;
    tmpValData2.strName      = _T("test_data2");
    tmpValData2.strTypeName  = _T("TEST_DATA2");
    tmpValData2.strVal       = _T("-----");

        tmpValData.bValid = true;
        tmpValData.bArray = false;
        tmpValData.strName      = _T("iData_2");
        tmpValData.strTypeName  = _T("int");
        tmpValData.strVal       = _T("122");

        tmpValData2.lstVal.push_back(tmpValData);

        tmpValData.bValid = true;
        tmpValData.bArray = false;
        tmpValData.strName      = _T("dData_2");
        tmpValData.strTypeName  = _T("double");
        tmpValData.strVal       = _T("22.1");

        tmpValData2.lstVal.push_back(tmpValData);

    valData.lstVal.push_back(tmpValData2);


    pDebugWnd = THIS_APP->GetMainFrame()->GetDebugWatchWnd();
    valData.strName      = _T("test_data_1");
    pDebugWnd->TEST_SetData(&valData);
    valData.strName      = _T("test_data_2");
    pDebugWnd->TEST_SetData(&valData);
    valData.strName      = _T("test_data_3");
    pDebugWnd->TEST_SetData(&valData);


    CTreeListItem* pRoot      = pDebugWnd->m_lscWatch.GetRootItem()->m_pParent;
    CTreeListItem* pFirstItem = pRoot->m_pChild;

    pDebugWnd->TEST_DeleteChildItem(pFirstItem);

    CTreeListItem* pNextItem;

    StdString strItemName;
    strItemName = pDebugWnd->TEST_GetItemName(pFirstItem);
    DB_PRINT(_T("Name1 %s \n"), strItemName.c_str());

    pNextItem = pFirstItem->m_pNext;
    strItemName = pDebugWnd->TEST_GetItemName(pNextItem);
    DB_PRINT(_T("Name2 %s \n"), strItemName.c_str());

    pNextItem = pNextItem->m_pNext;
    strItemName = pDebugWnd->TEST_GetItemName(pNextItem->m_pChild);
    DB_PRINT(_T("Name3 %s \n"), strItemName.c_str());

    pNextItem = pNextItem->m_pNext;
    if (pNextItem != NULL)
    {
        strItemName = pDebugWnd->TEST_GetItemName(pNextItem);
        DB_PRINT(_T("Name3 %s \n"), strItemName.c_str());
    }

    pFirstItem->m_pNext;

#endif
    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}

#endif //_DEBUG

