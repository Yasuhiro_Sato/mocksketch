/**
 * @brief			CDebugWatchWndヘッダーファイル
 * @file			CDebugWatchWnd.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *	
 *
 * $
 * $
 * 
 */


#pragma once
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "Utility/TreeList/TreeListColumnInfo.h"
#include "Utility/TreeList/TreeListItem.h"
#include "Utility/TreeList/TreeListHeaderCtrl.h"
#include "Utility/TreeList/TreeListTipCtrl.h"
#include "Utility/TreeList/TreeListStaticCtrl.h"
#include "Utility/TreeList/TreeListEditCtrl.h"
#include "Utility/TreeList/TreeListComboCtrl.h"
#include "Utility/TreeList/TreeListCtrl.h"
#include "SubWindow/CustomDocablePane.h"
#include "View/CURRENT_BREAK_CONTEXT.h"

/*---------------------------------------------------*/
/*  class                                            */
/*---------------------------------------------------*/
struct VAL_DATA;

/**
 * @class   CDebugWatchWnd
 * @brief                        
 */
class CDebugWatchWnd : public CCustomDocablePane
{
    // 表示列
    enum ENUM_COL
    {
        COL_VAR_NAME = 0,   //
        COL_VAR_VAL  = 1,
        COL_VAR_TYPE = 2,
    };
    
// コンストラクション
public:
    CDebugWatchWnd();

    virtual ~CDebugWatchWnd();

#ifdef _DEBUG
    void TEST_TraceItem(CTreeListItem* pItem, std::deque<CTreeListItem*>* pList){TraceItem(pItem,pList);}

    void TEST_DeleteChildItem(CTreeListItem* pItem){DeleteChildItem( pItem);}

    void TEST_SetData(VAL_DATA* pData, CTreeListItem* pRoot = TLI_ROOT){ SetData(pData, pRoot);}

    StdString TEST_GetItemName(CTreeListItem* pItem){ return GetItemName(pItem);}

#endif

// 属性
protected:
    CFont m_Font;


protected:
    //カレントモジュール
    CURRENT_BREAK_CONTEXT* m_pCurBreakContext;

// 実装
public:
    CTreeListCtrl   m_lscWatch;

protected:
    void TraceItem(CTreeListItem* pItem, std::deque<CTreeListItem*>* pList);

    void TraceItemChild(CTreeListItem* pItem, std::deque<CTreeListItem*>* pList);

    void DeleteChildItem(CTreeListItem* pItem);

    void SetData(VAL_DATA* pData, CTreeListItem* pRoot = TLI_ROOT);

    void UpdateText(CTreeListItem* pItem, VAL_DATA*  pValData);

    void AddLast();

    //!< アイテム追加
    CTreeListItem* AddItem();

    //!< リスト最後尾アイテム取得
    CTreeListItem* GetLastItem();

    //!< アイテム名取得
    StdString GetItemName(CTreeListItem* pItem);

    //!< データ更新
    void UpdateData(CTreeListItem** pItem);

    //afx_msg void OnLvnEndCtrlLscWatch(NMHDR *pNMHDR, LRESULT *pResult);
    //afx_msg void OnLvnItemFinishedLscWatch(NMHDR *pNMHDR, LRESULT *pResult);

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);

    virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResul);


    //!< ブレークポイント開始通知
    afx_msg LRESULT OnBreakStart(WPARAM wparam, LPARAM lparam);

    //!< ブレークポイント終了通知
    afx_msg LRESULT OnBreakEnd(WPARAM breakModule, LPARAM lparam);

    //!< ウォッチアイテム削除
    afx_msg LRESULT OnDeleteWatch(WPARAM pDeleteItem, LPARAM lparam);

    //!<モード変更
    afx_msg LRESULT OnModeChange(WPARAM wParam, LPARAM lParam);

    //================
    //  メニュー項目
    //================

    //!< メニュー -> コピー
    afx_msg void OnEditCopy();

    //!< メニュー -> コピー UI更新
    afx_msg void OnUpdateEditCopy(CCmdUI* pCmdUI);


    //!< メニュー -> 貼りつけ
    afx_msg void OnEditPaste();

    //!< メニュー -> 貼りつけ UI更新
    afx_msg void OnUpdateEditPaste(CCmdUI* pCmdUI);


    //!< メニュー -> 値の編集
    afx_msg void OnMnuWatchEdit();

    //!< メニュー -> 値の編集 UI更新
    afx_msg void OnUpdateMnuWatchEdit(CCmdUI* pCmdUI);


    //!< メニュー -> 全て選択
    afx_msg void OnMnuWatchSel();

    //!< メニュー -> 全て選択 UI更新
    afx_msg void OnUpdateMnuWatchSel(CCmdUI* pCmdUI);


    //!< メニュー -> 全てクリア
    afx_msg void OnMnuWatchClear();

    //!< メニュー -> 全てクリア UI更新
    afx_msg void OnUpdateMnuWatchClear(CCmdUI* pCmdUI);


    //!< メニュー -> 16進表示
    afx_msg void OnMnuWatchHex();

    //!< メニュー -> 16進表示 UI更新
    afx_msg void OnUpdateMnuWatchHex(CCmdUI* pCmdUI);


    //!< メニュー -> 1レベル上に折りたたむ
    afx_msg void OnMnuWatchContract();

    //!< メニュー -> 1レベル上に折りたたむ UI更新
    afx_msg void OnUpdateMnuWatchContract(CCmdUI* pCmdUI);


	DECLARE_MESSAGE_MAP()
public:
    afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);
};

