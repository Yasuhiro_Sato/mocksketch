﻿// DefaultSetDlg.cpp : 実装ファイル
//

#include "stdafx.h"
#include "MockSketch.h"
#include "CProjectCtrl.h"
#include "DefaultSetDlg.h"
#include "afxdialogex.h"
#include "System/CSystem.h"
#include "DrawingObject/CDrawingObject.h"



// DefaultSetDlg ダイアログ

IMPLEMENT_DYNAMIC(DefaultSetDlg, CDialog)

DefaultSetDlg::DefaultSetDlg(CWnd* pParent /*=nullptr*/)
	: CDialog(IDD_DLG_DEFAULT_OBJ, pParent)
{
	m_bPri = false;
	m_pProj = NULL;
	m_pObj = NULL;

}

DefaultSetDlg::~DefaultSetDlg()
{
}

void DefaultSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CK_HI_PRIORTY_LINE, m_ckHiPriorityLine);
}


BEGIN_MESSAGE_MAP(DefaultSetDlg, CDialog)
	ON_BN_CLICKED(IDC_CK_HI_PRIORTY_LINE, &DefaultSetDlg::OnBnClickedHiPriorityLine)
	ON_BN_CLICKED(IDOK, &DefaultSetDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &DefaultSetDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_PB_RESET, &DefaultSetDlg::OnBnClickedReset)
END_MESSAGE_MAP()


BOOL DefaultSetDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetWindowText(GET_STR(STR_STX_DEFAULT_SET_DLG));
	GetDlgItem(IDC_ST_DEFAULT_SET)->SetWindowText(GET_STR(STR_STX_DEFAULT_SET));
	m_ckHiPriorityLine.SetWindowText(GET_STR(STR_STX_HI_PRIORITY_LINE));

	GetDlgItem(IDC_PB_RESET)->SetWindowText(GET_STR(STR_STX_RESET));
	GetDlgItem(IDCANCEL)->SetWindowText(GET_STR(STR_STX_CANCEL));

	_SetHiPriority(m_bPri);


	return TRUE;  // return TRUE unless you set the focus to a control
}

void DefaultSetDlg::SetProjectAndObject(CProjectCtrl* pProj, CDrawingObject* pObj)
{
	m_pProj = pProj;
	m_pObj = pObj;
}


void DefaultSetDlg::_SetHiPriority(bool bAuto)
{
	int iCheck = BST_UNCHECKED;
	if (bAuto)
	{
		iCheck = BST_CHECKED;
		m_bPri = true;
	}
	else
	{
		m_bPri = false;
	}

	m_ckHiPriorityLine.SetCheck(iCheck);
}


void DefaultSetDlg::SetHiPriority(bool bHIPrioprity)
{
	m_bPri = bHIPrioprity;
}


bool DefaultSetDlg::IsHiPriority()
{
	return (m_bPri);
}


// DefaultSetDlg メッセージ ハンドラー
void DefaultSetDlg::OnBnClickedHiPriorityLine()
{
	UpdateData(TRUE);

	int iCheck;
	iCheck = m_ckHiPriorityLine.GetCheck();

	if (iCheck == BST_CHECKED)
	{
		m_bPri = true;
	}
	else
	{
		m_bPri = false;
	}
	UpdateData(FALSE);
}

void DefaultSetDlg::OnBnClickedOk()
{
	UpdateData(TRUE);

	int iCheck;
	iCheck = m_ckHiPriorityLine.GetCheck();
	m_bPri = (iCheck == BST_CHECKED);

	if (m_pObj)
	{
		m_pProj->StoreDefaultObject(m_pObj, m_bPri);
	}
	CDialog::OnOK();
}

void DefaultSetDlg::OnBnClickedCancel()
{
	CDialog::OnCancel();
}

void DefaultSetDlg::OnBnClickedReset()
{
	if (m_pObj)
	{
		m_pProj->ClearDefaultObject(m_pObj->GetType());
	}

	CDialog::EndDialog(IDC_PB_RESET);
}
