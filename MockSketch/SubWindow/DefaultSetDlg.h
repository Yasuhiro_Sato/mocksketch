﻿#pragma once


// DefaultSetDlg ダイアログ
class CProjectCtrl;
class CDrawingObject;

class DefaultSetDlg : public CDialog
{
	DECLARE_DYNAMIC(DefaultSetDlg)

public:
	DefaultSetDlg(CWnd* pParent = nullptr);   // 標準コンストラクター
	virtual ~DefaultSetDlg();

// ダイアログ データ
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DLG_DEFAULT_OBJ };
#endif

	void SetHiPriority(bool bHIPrioprity);
	bool IsHiPriority();

	void SetProjectAndObject(CProjectCtrl* m_pProj, CDrawingObject* pObj);

	afx_msg void OnBnClickedHiPriorityLine();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedReset();
	virtual BOOL OnInitDialog();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート

	DECLARE_MESSAGE_MAP()

	void _SetHiPriority(bool bHIPrioprity);
	CButton m_ckHiPriorityLine;
	bool m_bPri;
	CProjectCtrl* m_pProj;
	CDrawingObject* m_pObj;


};
