// C:\Backup\Projects\MockTools\MockSketch\Src\mocksketch\MockSketch\SubWindow\DimTextDlg.cpp : 実装ファイル
//

#include "stdafx.h"
#include "MockSketch.h"
#include "./DimTextDlg.h"
#include "afxdialogex.h"
#include "System/CSystem.h"


// CDimTextDlg ダイアログ

IMPLEMENT_DYNAMIC(CDimTextDlg, CDialog)

CDimTextDlg::CDimTextDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDimTextDlg::IDD, pParent)
{

}

CDimTextDlg::~CDimTextDlg()
{
}

void CDimTextDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_CK_AUTO, m_ckAuto);
    DDX_Control(pDX, IDC_ED_TEXT, m_edText);
}

void CDimTextDlg::SetText(LPCTSTR str)
{
    m_Text = str;
}

StdString CDimTextDlg::GetText()
{
    return m_Text;
}

void CDimTextDlg::SetValText(LPCTSTR str)
{
    m_ValText = str;
}

StdString CDimTextDlg::GetValText()
{
    return m_ValText;
}

void CDimTextDlg::_SetAuto(bool bAuto)
{
    int iCheck = BST_UNCHECKED;
    if (bAuto)
    {
        iCheck = BST_CHECKED;
        m_edText.EnableWindow(FALSE);

    }
    else
    {
        m_edText.EnableWindow(TRUE);
    }

    m_ckAuto.SetCheck(iCheck);
}

void CDimTextDlg::SetAuto(bool bAuto)
{
    m_bAuto = bAuto;
}

bool CDimTextDlg::IsAuto()
{
    return (m_bAuto);
}


BEGIN_MESSAGE_MAP(CDimTextDlg, CDialog)
    ON_BN_CLICKED(IDC_CK_AUTO, &CDimTextDlg::OnBnClickedCkAuto)
    ON_BN_CLICKED(IDOK, &CDimTextDlg::OnBnClickedOk)
    ON_BN_CLICKED(IDCANCEL, &CDimTextDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


// CDimTextDlg メッセージ ハンドラー


void CDimTextDlg::OnBnClickedCkAuto()
{
    UpdateData(TRUE);

    int iCheck;
    iCheck = m_ckAuto.GetCheck();

    if (iCheck == BST_CHECKED)
    {
        CString str;
        m_edText.GetWindowText(str);
        m_Text= str;
        m_edText.SetWindowText(m_ValText.c_str());
        m_edText.EnableWindow(FALSE);
        m_bAuto = true;
    }
    else
    {
        m_edText.SetWindowText(m_Text.c_str());
        m_edText.EnableWindow(TRUE);
        m_bAuto = false;
    }
    UpdateData(FALSE);
}


void CDimTextDlg::OnBnClickedOk()
{
    UpdateData(TRUE);
    CString str;
    m_edText.GetWindowText(str);
    m_Text =  (LPCTSTR)str;

    int iCheck;
    iCheck = m_ckAuto.GetCheck();
    m_bAuto = (iCheck == BST_CHECKED);
    
    CDialog::OnOK();
}


void CDimTextDlg::OnBnClickedCancel()
{

    CDialog::OnCancel();
}


BOOL CDimTextDlg::OnInitDialog()
{
    CDialog::OnInitDialog();

    SetWindowText(GET_STR(STR_DTX_DLG));
    GetDlgItem(IDC_CK_AUTO)->SetWindowText(GET_STR(STR_DTX_AUTO));
    m_edText.SetWindowText(m_Text.c_str());
    _SetAuto(m_bAuto);


    return TRUE;  // return TRUE unless you set the focus to a control
}
