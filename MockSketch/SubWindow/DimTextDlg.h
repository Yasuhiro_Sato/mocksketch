#pragma once


// CDimTextDlg ダイアログ
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "Resource.h"
#include "resource2.h"
#include "afxwin.h"


class CDimTextDlg : public CDialog
{
	DECLARE_DYNAMIC(CDimTextDlg)
    StdString     m_Text;
    StdString     m_ValText;
    bool          m_bAuto;

public:

	CDimTextDlg(CWnd* pParent = NULL);   // 標準コンストラクター
	virtual ~CDimTextDlg();

    void SetText(LPCTSTR str);
    StdString GetText();
    void SetValText(LPCTSTR str);
    StdString GetValText();

    void SetAuto(bool bAuto);
    bool IsAuto();


// ダイアログ データ
	enum { IDD = IDD_DLG_DIM_TEXT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート
    void _SetAuto(bool bAuto);

	DECLARE_MESSAGE_MAP()
public:
    afx_msg void OnBnClickedCkAuto();
    CButton m_ckAuto;
    afx_msg void OnBnClickedOk();
    afx_msg void OnBnClickedCancel();
    virtual BOOL OnInitDialog();
    CEdit m_edText;
};
