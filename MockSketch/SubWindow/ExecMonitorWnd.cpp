/**
 * @brief			ExecMonitorWnd実装ファイル
 * @file			ExecMonitorWnd.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *	
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MockSketch.h"
#include "./ExecMonitorWnd.h"
#include "ExecCommon.h"
#include "DefinitionObject/View/MockSketchDoc.h"

#include "DefinitionObject/View/MockSketchView.h"
#include "DefinitionObject/ObjectDef.h"
#include "Utility/CUtility.h"
#include "System/CSystem.h"
#include "Script/CExecCtrl.h"
#include "Script/EditorView.h"
#include "Script/TCB.h"
#include "DrawingObject/CDrawingScriptBase.h"

using namespace EXEC_COMMON;

/*---------------------------------------------------*/
/*  Static                                           */
/*---------------------------------------------------*/
static const E_THREAD_TYPE  gDispThreadType[] ={
                                            TH_GROUP_1 ,
                                            TH_GROUP_2 ,
                                            TH_GROUP_3 ,
                                            TH_GROUP_4 ,
                                            TH_GROUP_5 ,
                                            TH_FIELD_1 ,
                                            TH_FIELD_2 ,
                                            TH_FIELD_3 ,
                                            TH_FIELD_4 ,
                                            TH_FIELD_5 ,
                                            TH_DISP ,
                                            /*TH_PANE ,*/
                                            TH_IO_1 ,
                                            TH_IO_2 ,
                                            TH_IO_3};


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
IMPLEMENT_DYNAMIC(CExecMonitorWnd, CDockablePane)

BEGIN_MESSAGE_MAP(CExecMonitorWnd, CDockablePane)
    ON_WM_CREATE()
    ON_WM_SIZE()
    ON_WM_SETFOCUS()
    ON_WM_SETTINGCHANGE()
    //ON_NOTIFY(TLN_ENDCONTROL, IDC_LSC_MONITOR, &CExecMonitorWnd::OnLvnEndCtrl)
    //ON_NOTIFY(LVN_ENDLABELEDIT, IDC_LST_VIRTUAL_EXEC_MONITOR, OnLvnEndlabeleditVlst)

    
    ON_MESSAGE(WM_VIEW_CREATE,  OnViewCreate)
    ON_MESSAGE(WM_VIEW_ACTIVE, OnViewActivate)
    ON_MESSAGE(WM_VIEW_CLEAR_ALL, OnViewClear)
    //ON_MESSAGE(WM_VIEW_SEL_CHANGEE, OnViewSelchange)
    //ON_MESSAGE(WM_VIEW_PARTS_DELETE, OnPartsDelete)
    ON_MESSAGE(WM_MODE_CHANGE, OnModeChange)
    ON_MESSAGE(WM_UPDATE_EXEC, OnUpdateExec)

    ON_WM_CONTEXTMENU()
END_MESSAGE_MAP()

/**
 * @brief   ツール バーを元の状態に戻します
 * @param   なし
 * @return	なし
 * @note	 既定の実装では、何も行われません。
 * @note	 ツール バーが元の状態に戻るときに置き換える必要がある
 * @note	 ダミー ボタンがツール バーにある場合は、
 * @note	 CMFCToolBar クラスから派生したクラスで 
 * @note	 OnReset をオーバーライドします。
 */
void CExecMonitorToolBar::OnReset ()
{
}


/**
 * @brief   ツールヒント表示
 * @param   [in] pButton
 * @param   [in] pButton
 * @return  
 * @note    ボタンのツールヒントを表示する直前に、フレームワークによって
 * @note    呼び出されます。
 */ 
BOOL CExecMonitorToolBar::OnUserToolTip(CMFCToolBarButton* pButton,
                                  CString& strTTText ) const
{
    //TODO:IO選択、ページでツールヒントが出ない
    //switch(pButton->m_nID)
    //{
    //case ID_IO_DEF_PAGE: strTTText = GET_STR(STR_IOWND_TIP_PAGE); break;
    //case ID_IO_SETTING: strTTText = GET_STR(STR_IOWND_TIP_SETTING); break;
    //case ID_IO_SELECT:  strTTText = GET_STR(STR_IOWND_TIP_SELECT); break;
    //default:
    //    return FALSE;
    //}
    return TRUE;
}

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CExecMonitorWnd::CExecMonitorWnd():
m_pExecCtrl(NULL),
m_iMapChgCnt(-1)
{

}

/**
 *  @brief  デストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CExecMonitorWnd::~CExecMonitorWnd()
{
}

/**
 * @brief   レイアウト調整
 * @param   なし
 * @return	なし
 * @note	 
 */
void CExecMonitorWnd::AdjustLayout()
{
    if (GetSafeHwnd() == NULL)
    {
        return;
    }


    if (m_vlstMonitor.m_hWnd == NULL)
    {
        return;
    }

    STD_ASSERT(m_vlstMonitor.m_hWnd != NULL);

    CRect rectClient;
    GetClientRect(rectClient);
    int cyTlb = m_wndToolBar.CalcFixedLayout(FALSE, TRUE).cy;

    m_wndToolBar.SetWindowPos(NULL, rectClient.left, 
                                    rectClient.top, 
                                    rectClient.Width(), 
                                    cyTlb, SWP_NOACTIVATE | SWP_NOZORDER);

    m_vlstMonitor.SetWindowPos(NULL, rectClient.left,
                                   rectClient.top + cyTlb, 
                                   rectClient.Width(), 
                                   rectClient.Height() - cyTlb, SWP_NOACTIVATE | SWP_NOZORDER);

}


/**
 * @brief   生成時処理要求
 * @param   [in]    lpCreateStruct  初期化パラメータへのポインタ
 * @return  0:生成成功
 * @note	 
 */
int CExecMonitorWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (CDockablePane::OnCreate(lpCreateStruct) == -1)
    {
        return -1;
    }



    //m_vlstMonitor.SetStyle( TLC_TREELIST| TLC_TREELINE | TLC_BUTTON |
    //                     TLC_HEADER | TLC_HGRID | TLC_VGRID |TLC_TGRID
    //                     );

    //TODO:必要に応じてTOOL BARを実装する
    m_wndToolBar.Create(this, AFX_DEFAULT_TOOLBAR_STYLE, IDR_IO_DEF);
    m_wndToolBar.LoadToolBar(IDR_IO_DEF, 0, 0, TRUE /* ロックされています*/);
    m_wndToolBar.CleanUpLockedImages();
    m_wndToolBar.LoadBitmap(/*theApp.m_bHiColorIcons ? IDB_IO_DEF_HC : */IDR_IO_DEF, 0, 0, TRUE /* ロックされました*/);

    m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() | CBRS_TOOLTIPS | CBRS_FLYBY);
    m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() & ~(CBRS_GRIPPER | CBRS_SIZE_DYNAMIC | CBRS_BORDER_TOP | CBRS_BORDER_BOTTOM | CBRS_BORDER_LEFT | CBRS_BORDER_RIGHT));
    m_wndToolBar.SetOwner(this);

    // すべてのコマンドが、親フレーム経由ではなくこのコントロール経由で渡されます:
    m_wndToolBar.SetRouteCommandsViaFrame(FALSE);


    CRect rectClient;
    GetClientRect(rectClient);

    m_vlstMonitor.Create(LVS_REPORT | LVS_ALIGNLEFT | LVS_OWNERDATA | WS_CHILD | 
                       WS_BORDER | WS_VISIBLE |WS_TABSTOP, CRect(), this, IDC_LST_VIRTUAL_EXEC_MONITOR);
    
    //スタイル設定
    ListView_SetExtendedListViewStyleEx(m_vlstMonitor.m_hWnd, LVS_EX_FULLROWSELECT, LVS_EX_FULLROWSELECT);
    ListView_SetExtendedListViewStyleEx(m_vlstMonitor.m_hWnd, LVS_EX_GRIDLINES, LVS_EX_GRIDLINES);

    int iWidth = 300;
    int iPos;
    StdString strHeader;

    CUtil::FormatedText(GET_SYS_STR(STR_EXEC_NAME), &iWidth, &iPos, &strHeader);
    m_vlstMonitor.InsertColumn(0, strHeader.c_str(), LVCFMT_LEFT, 120);

    CUtil::FormatedText(GET_SYS_STR(STR_EXEC_TIME), &iWidth, &iPos, &strHeader);
    m_vlstMonitor.InsertColumn(1, strHeader.c_str(), LVCFMT_LEFT, 140);


    CVirtualCheckList::COL_SET ColSet;

    //名前
    ColSet.bOwnerDraw = true;
    ColSet.eEditType = CVirtualCheckList::NONE;     //編集不可
    m_vlstMonitor.SetCol(0, ColSet);

    //実行時間
    ColSet.bOwnerDraw = true;
    ColSet.eEditType = CVirtualCheckList::NONE;       //編集可
    m_vlstMonitor.SetCol(1, ColSet);

    m_pExecCtrl = THIS_APP->GetExecCtrl();

    //コールバック関数設定
    m_vlstMonitor.SetFunc(Callbackfunc, CallbackChek, this);
    m_vlstMonitor.SetOwnerDrawFunc(CallOwnerDraw);
    m_vlstMonitor.SetOwnerMouseFunc(CallOwnerMouse);
    m_vlstMonitor.SetContextMenuFunc(CallbackContextMenu);
    m_vlstMonitor.SetOnCommandFunc(CallbackOnCommand);

    m_bmpExecSts.Create( IDB_EXEC_IMG, 16, 5, 0xFF00FF );

    //生成及び調整
    AdjustLayout();

    return 0;
}

/**
 *  @brief ウインドウサイズ変更
 *  @param [in] nType   要求されるサイズ変更のタイプ  (SIZE_MAXIMIZED....)
 *  @param [in] cx      クライアント領域の新しい幅 
 *  @param [in] cy      クライアント領域の新しい高さ
 *  @retval           なし
 *  @note
 */
void CExecMonitorWnd::OnSize(UINT nType, int cx, int cy)
{
	CDockablePane::OnSize(nType, cx, cy);
	AdjustLayout();
}

/**
 *  @brief  ウインドウフォーカス設定
 *  @param  [out] pOldWnd  今までフォーカスがあったウインドウ
 *  @retval なし
 *  @note   
 */
void CExecMonitorWnd::OnSetFocus(CWnd* pOldWnd)
{
	CDockablePane::OnSetFocus(pOldWnd);
}

/**
 *  @brief  設定変更
 *  @param  [in] uFlags  変更されたシステム パラメータを示すフラグ
 *  @param  [in] lpszSection   変更されたセクション名を示す文字列へのポインタ
 *  @retval なし
 *  @note   SystemParametersInfo参照
 */
void CExecMonitorWnd::OnSettingChange(UINT uFlags, LPCTSTR lpszSection)
{
	CDockablePane::OnSettingChange(uFlags, lpszSection);
	SetListFont();
}

/**
 *  @brief  フォント変更
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CExecMonitorWnd::SetListFont()
{
}


/**
 *  @brief モード変更
 *  @param [in] wParam   アプリケーションモード(EXEC_COMMON::E_APP_MODE)
 *  @param [in] lParam   オブジェクト定義      (CObjectDef)
 *  @retval
 *  @note
 */
LRESULT CExecMonitorWnd::OnModeChange(WPARAM wParam, LPARAM lParam)
{
    using namespace EXEC_COMMON;

    E_EXEC_STS  eSts;
    CObjectDef* pDef;

    eSts = static_cast<E_EXEC_STS>(wParam);
    pDef = reinterpret_cast<CObjectDef*>(lParam);

    if (eSts == EX_EDIT)
    {
        m_vlstMonitor.SetItemCount(0);
        m_vlstMonitor.EnableWindow(FALSE);
    }
    else if (eSts == EX_EXEC)
    {
        bool bInit = true;
        if (m_vlstMonitor.IsWindowEnabled())
        {
            bInit = false;
        }

        m_vlstMonitor.EnableWindow(TRUE);

        if (bInit)
        {
            InitView();
        }
    }
   return 0;
}


/**
 *  @brief リスト文字変更通知
 *  @param    [in]  pNMHDR 通知メッセージへのポインタ
 *  @param    [out] pResult 
 *  @retval   なし
 *  @note	
 */
/*
void CExecMonitorWnd::OnLvnEndCtrl(NMHDR *pNMHDR, LRESULT *pResult)
{
    NMTREELIST *pTreeList = reinterpret_cast<NMTREELIST*>(pNMHDR);

    CTreeListItem* pItem;
    int            iCol;

    pItem = pTreeList->pItem;
    iCol  = pTreeList->iCol;


    //UpdateData(&pItem);

    *pResult = 0;
}
*/


/**
 * @brief   描画コールバック
 * @param   [in]  pParent 呼び出し元         
 * @param   [in]  nRow    データを取得する行         
 * @param   [in]  nCol    データを取得する列        
 * @param   [out] pData   文字列        
 * @param   [out] pbCheck チェックボックス         
 * @retval  なし
 * @note	仮想リスト用表示関数
 */
void CExecMonitorWnd::Callbackfunc (LPVOID pParent, int nRow, int nCol, _TCHAR* pData, bool* pbCheck)
{
    CExecMonitorWnd* pWnd = (CExecMonitorWnd*)pParent;

    //描画処理
    pWnd->SetText( nRow, nCol, pData,  pbCheck);
}

/**
 * @brief   描画処理
 * @param   [in]  nRow    データを取得する行         
 * @param   [in]  nCol    データを取得する列        
 * @param   [out] pData   文字列        
 * @param   [out] pbCheck チェックボックス         
 * @retval  なし
 * @note	
 */
void CExecMonitorWnd::SetText( int nRow, int nCol, _TCHAR* pData, bool* pbCheck) 
{
    bool                    bCheck = false;
    CString                 sItem;
    //std::wstring            sItem2;
    
    CVirtualTreeCtrlItem* pItem;
    ExecMonitorItem*      pItemData = NULL;
    int                 iItemNo = 0;
    pItem = m_vTreeData.GetItem(nRow);
    if (pItem)
    {
        iItemNo = reinterpret_cast<int>(pItem->pData);
    }
	else
	{
		return;
	}

    if (iItemNo >= m_lstItem.size() )
    {
        return;
    }

    pItemData = &m_lstItem[iItemNo];


    if (nCol == 0)
    {
        sItem = pItemData->strName.c_str();
    }

    *pbCheck = bCheck;
    lstrcpy(pData, sItem);
}

/**
 * @brief   チェックボックスコールバック
 * @param   [in]  nRow    選択時の行         
 * @param   [in]  nCol    選択時の列        
 * @retval  なし
 * @note	仮想リストボタンチェック時に呼び出し
 */
void CExecMonitorWnd::CallbackChek   (LPVOID pParent, int nRow, int nCol)
{
    CExecMonitorWnd* pWnd = (CExecMonitorWnd*)pParent;
}


/**
 * @brief   オーナードロー
 * @param   [in]  pParent    呼び出し元
 * @param   [in]  pCheckList CVirtualCheckList
 * @param   [in]  nRow       行
 * @param   [in]  nCol       列
 * @param   [in]  pDC        デバイスコンテキスト
 * @param   [in]  sData      表示文字列
 * @param   [in]  bCheck     チェックの有無
 * @retval  なし
 * @note    全ての設定を無視して自分で描画
 *          CVirtualCheckList::COL_SET.bOwnerDraw = true;
 */
void CExecMonitorWnd::CallOwnerDraw (LPVOID pParent, 
                    CVirtualCheckList* pCheckList,
                    int nRow, int nCol, 
                    CDC* pDC,  CString sData, 
                    bool bCheck)
{
    //
    using namespace EXEC_COMMON;

    CExecMonitorWnd* pWnd;
    pWnd = reinterpret_cast<CExecMonitorWnd*>(pParent);

    CVirtualTreeCtrlItem* pItem;
    ExecMonitorItem*      pItemData = NULL;
    int iItemNo;

    pItem = pWnd->m_vTreeData.GetItem(nRow);
    if (pItem)
    {
        iItemNo = reinterpret_cast<int>(pItem->pData);
    }
	else
	{
		return;
	}

    if (iItemNo >= pWnd->m_lstItem.size() )
    {
        return;
    }

    pItemData = &pWnd->m_lstItem[iItemNo];


    CRect rcText;
    if(!pCheckList->GetTextRect(nRow, nCol, rcText))
    {
        return;
    }

    CBrush brListback( GetSysColor(COLOR_WINDOW));
    pDC->FillRect(rcText, &brListback);

    if (nCol == 0)
    {
        int iTextX;
        int iRank;

        E_TREE_BUTTON_TYPE eType = TBT_NONE;

        iRank = pItem->iRank;

        if (pItem->psChild != NULL)
        {
            if(pItem->IsExpand())
            {
                eType = TBT_MINUS;
            }
            else
            {
                eType = TBT_PLUS;
            }
        }

        UINT textFormat=0;
        textFormat = DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS | DT_LEFT;

        iTextX = CVirtualTreeCtrl::DrawItemTreeButton( pDC, 
                                   &pWnd->m_vlstMonitor,
                                   nCol, nRow, iRank, eType);
        rcText.left = iTextX;
        pDC->DrawText(sData, &rcText, textFormat);
    }
    else if(nCol == 1)
    {
        if(pItemData->eType == E_GROUP)
        {
            return;
        }

        POINT pt;

        pt.x = rcText.left;
        pt.y = rcText.top;

        CExecCtrl*   pExecCtrl;
        pExecCtrl = THIS_APP->GetExecCtrl();

        CString strDraw;

        CRect rcProgress = rcText;



        rcProgress.left += 16; //アイコン分オフセット
        rcProgress.DeflateRect( 2, 0, 1, 1 );

        if (pItemData->eType == E_TIME)
        {
            int iPercent; 
            int iDefTime;
            double dExecTime;

            iDefTime  = pItemData->dwCycleime;
            dExecTime  = pExecCtrl->GetExecTime(pItemData->eGroup, iDefTime);


            iPercent = int((dExecTime / double(iDefTime)) * 100.0);

            strDraw.Format(_T("%d (%.1f/%d)"), iPercent,
                                             dExecTime,
                                             iDefTime);

            CVirtualTreeCtrl::DrawItemProgressBar(pDC, rcProgress, iPercent, strDraw);
        }
        else
        {
            TCB* pTcb;
            pTcb = pItemData->pTcb;


            UINT textFormat=0;
            textFormat = DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS | DT_RIGHT;

            int iExecTime = CUtil::Round(pTcb->GetExecTime());

            static std::vector<StdString> lstTsk = { _T("NON"), _T("WAT"), _T("RUN"),
                                                     _T("INT"), _T("YLD"), _T("SLP"),
                                                     _T("BRK"), _T("SUS"), _T("EXT")};
            int iStatus = E_IC0_NONE;
            int iTsk = 0;
            switch(pTcb->GetStatus())
            {
            case TS_WAIT:       {iTsk = 1; iStatus = E_IC0_STOP; break;}
            case TS_RUN:        {iTsk = 2; iStatus = E_IC0_RUN;break;}
            case TS_WAIT_CYCLE: {iTsk = 2; iStatus = E_IC0_RUN;break;}
            case TS_INTRRUPT:   {iTsk = 3; iStatus = E_IC0_RUN_PART;break;}
            case TS_YIELD:      {iTsk = 4; iStatus = E_IC0_RUN_PART;break;}
            case TS_SLEEP:      {iTsk = 5; iStatus = E_IC0_RUN_PART;break;}
            case TS_BREAK:      {iTsk = 6; iStatus = E_IC0_RUN_PART;break;}
            case TS_SUSPEND:    {iTsk = 7; iStatus = E_IC0_PAUSE;break;}
            case TS_EXT:        {iTsk = 8; iStatus = E_IC0_STOP;break;}
            }
            pWnd->m_bmpExecSts.Draw( pDC,  iStatus, pt, ILD_TRANSPARENT );
            strDraw.Format(_T("ID:%d Step:%d Time:%d Sts:%s"), pTcb->GetId(),
                                                        pTcb->GetStep(),
                                                        iExecTime,
                                                        lstTsk[iTsk].c_str());
                        
            pDC->DrawText(strDraw, rcProgress, textFormat);
        }
    }
   
}



// オーナーマウス
void CExecMonitorWnd::CallOwnerMouse   (LPVOID pParent, 
                        CVirtualCheckList* pCheckList,
                        int nRow, int nCol, 
                        POINT ptPos, bool bMouseDown)
{
    CExecMonitorWnd* pWnd;
    pWnd = reinterpret_cast<CExecMonitorWnd*>(pParent);

    CVirtualTreeCtrlItem* pItem;
    ExecMonitorItem*      pItemData = NULL;

    pItem = pWnd->m_vTreeData.GetItem(nRow);

    if(!pItem)
    {
        return;
    }

    CRect rcButton;

    if(! CVirtualTreeCtrl::GetItemTreeButtonRect( &rcButton , 
                                                  &pWnd->m_vlstMonitor,
                                                  nRow, pItem->iRank))
    {
        return;
    }

    if(rcButton.left > ptPos.x)
    {
        return;
    }

    if(rcButton.right < ptPos.x)
    {
        return;
    }

    if(rcButton.top > ptPos.y)
    {
        return;
    }

    if(rcButton.bottom < ptPos.y)
    {
        return;
    }

    if (bMouseDown)
    {
        pWnd->m_vTreeData.Expand(pItem, !pItem->IsExpand());
        pCheckList->SetItemCount(pWnd->m_vTreeData.GetVisibleItemNum());
        pCheckList->RedrawWindow();
    }
}


/**
 * @brief   View生成通知
 * @param   [in]    iWin	    View種別ID
 * @param   [in]    pWnd	    ウインドウ	 	  
 * @return	常に０	
 * @note	View生成完了後に各ウインドウに通知する 
 * @exception   なし
 */
LRESULT CExecMonitorWnd::OnViewCreate(WPARAM iType, LPARAM pMessage)
{

    return 0;
}


/**
 * @brief    View活性化を通知
 * @param    [in] pMessage タブタイトル文字
 * @param    [in] lParam   0:MockSletchView 1:EditView
 * @retval   なし
 * @note	
 */
LRESULT CExecMonitorWnd::OnViewActivate(WPARAM pMessage, LPARAM lParam)
{
    return 0;
}

/**
 * @brief   ビュークリア
 * @param   [in] wParam 使用しない
 * @param   [in] lParam 使用しない
 * @return  なし
 * @note    
 */
LRESULT CExecMonitorWnd::OnViewClear(WPARAM pMessage, LPARAM lParam)
{
    return 0;
}

/**
 *  @brief  初期化
 *  @param  [in] pExecCtrl
 *  @retval なし
 *  @note   
 */
void CExecMonitorWnd::InitView()
{
    m_vlstMonitor.SetItemCount(0);

    m_vTreeData.AllClear();

    m_lstItem.clear();

    CExecCtrl*   pExecCtrl;
    pExecCtrl = THIS_APP->GetExecCtrl();

    if (!pExecCtrl)
    {
        m_vlstMonitor.SetItemCount(0);
        return;
    }

    UpdateItem();
}

/**
 *  @brief  実行時画面更新
 *  @param  [in] wParam
 *  @param  [in] lParam
 *  @retval 常に０
 *  @note   
 */
LRESULT CExecMonitorWnd::OnUpdateExec(WPARAM wParam, LPARAM lParam)
{
    UpdateItem();
    m_vlstMonitor.Invalidate(FALSE);
    m_vlstMonitor.UpdateWindow();
    return 0;
}

void CExecMonitorWnd::UpdateItem()
{
    if (!m_pExecCtrl)
    {
        m_pExecCtrl = THIS_APP->GetExecCtrl();
        return;
    }
    int iChgCnt;
    iChgCnt = m_pExecCtrl->GetTcbMapCount();

    if (iChgCnt != m_iMapChgCnt)
    {
        _MakeMapItem();
        m_iMapChgCnt = iChgCnt;
    }
}

void CExecMonitorWnd::_MakeMapItem()
{
    m_pExecCtrl->GetTcbMap(&m_mapTcb);
    m_mapItem.clear();

    //::map< int, std::vector<EstdxecMonitorItem> >::iterator  iteTime;
    std::map<int, std::shared_ptr<TCB> >::iterator ite;

    //=============
    // 準備
    //=============
    for (EXEC_COMMON::E_THREAD_TYPE eThread: gDispThreadType)
    {
        std::map< int, std::vector<TCB*> > mapTimeGroup;
        m_mapItem[eThread] = mapTimeGroup;
    }

    /*
        Group1
           100msec 
               TCB1
               TCB2
               TCB5

           200msec 
               TCB3
        Group2
    */

    for(ite  = m_mapTcb.begin();
        ite != m_mapTcb.end();
        ite++)
    {
        TCB* pTcb;
        pTcb = ite->second.get();

        EXEC_COMMON::E_THREAD_TYPE eType;
        DWORD dwTime;
        dwTime = pTcb->GetCycleTime();
        eType  = pTcb->GetThreadType();



        auto iteTime = m_mapItem.find(eType);
        if (iteTime != m_mapItem.end())
        {
            std::map< int, std::vector<TCB*> >* pMapTime;
            pMapTime = &iteTime->second;
            auto iteItem = pMapTime->find(dwTime);

            if (iteItem != pMapTime->end())
            {
                iteItem->second.push_back(pTcb); 
            }
            else
            {
                std::vector<TCB*> lstItem;
                lstItem.push_back(pTcb); 
                pMapTime->insert(std::make_pair(dwTime, lstItem));
            }
        }
    }
    _UpdateListItem();
}

void CExecMonitorWnd::_UpdateListItem()
{
    using namespace EXEC_COMMON;
    ExecMonitorItem item;
    item.iModuleNo = 0;
    CVirtualTreeCtrlItem* pGroupRoot = NULL;
    m_lstItem.clear();
    m_vTreeData.AllClear();

    size_t nPos = -1;


    for( auto iteGroup: m_mapItem)
    {
        item.eType = E_GROUP;
        E_THREAD_TYPE eType = static_cast<E_THREAD_TYPE>(iteGroup.first);
        item.strName = ::GetThreadType2Name(eType);
        item.pTcb = NULL;
        m_lstItem.push_back(item);
        nPos++;

        if (iteGroup.second.size() != 0)
        {
            if (!pGroupRoot)
            {
                pGroupRoot = m_vTreeData.AddChild(pGroupRoot, reinterpret_cast<void*>(nPos), true, false);
            }
            else
            {
                pGroupRoot = m_vTreeData.AddItem(pGroupRoot,reinterpret_cast<void*>(nPos), true, false);
            }
        }

        CVirtualTreeCtrlItem* pTimeRoot = NULL;
        for( auto iteTime: iteGroup.second)
        {
            CExecCtrl*   pExecCtrl;
            pExecCtrl = THIS_APP->GetExecCtrl();

            StdStringStream strm;
            strm << StdFormat(_T("%d(msec)")) % iteTime.first;
            item.eType = E_TIME;
            item.strName = strm.str();
            item.eGroup =  eType;
            item.dwCycleime = iteTime.first;
            item.dExecTime  = pExecCtrl->GetExecTime(eType, iteTime.first);
            item.pTcb = NULL;
            m_lstItem.push_back(item);
            nPos++;

            
            if (iteTime.second.size() != 0)
            {
                if (!pTimeRoot)
                {
                    pTimeRoot = m_vTreeData.AddChild(pGroupRoot, reinterpret_cast<void*>(nPos), true, false);
                }
                else
                {
                    pTimeRoot = m_vTreeData.AddItem(pTimeRoot, reinterpret_cast<void*>(nPos), true, false);
                }
            }
 
            CVirtualTreeCtrlItem* pTreeItem = NULL;
            for( auto itemMap: iteTime.second)
            {
                item.eType = E_ITEM;
                item.pTcb = itemMap;
                item.strName = itemMap->GetTaskName();
                m_lstItem.push_back(item);
                nPos++;

                if (!pTreeItem)
                {
                    pTreeItem = m_vTreeData.AddChild(pTimeRoot, reinterpret_cast<void*>(nPos), true, false);
                }
                else
                {
                    pTreeItem = m_vTreeData.AddItem(pTreeItem,reinterpret_cast<void*>(nPos), true, false);
                }
            }
        }
    }
    m_vTreeData.UpdateVisibleItemMap();
    m_vlstMonitor.SetItemCount(m_vTreeData.GetVisibleItemNum());
}



/**
 * @brief   コンテキストメニューコールバック
 * @param   [in]  pParent    呼び出し元     
 * @param   [in]  pCheckList リストコントロール     
 * @param   [in]  ptClick クリック位置       
 * @param   [in]  nRow    データを取得する行         
 * @param   [in]  nCol    データを取得する列        
 * @retval   なし
 * @note	
 */
void CExecMonitorWnd::CallbackContextMenu
                        (LPVOID pParent, CVirtualCheckList* pCheckList,
                        POINT ptClick, int nRow, int nCol)
{
    CExecMonitorWnd* pBase = reinterpret_cast<CExecMonitorWnd*>(pParent);
    pBase->OnCustomContextMenu( pCheckList, ptClick, nRow, nCol);
}


/**
 * @brief   コンテキストメニュー表示
 * @param   [in]    point  クリック位置
 * @retval  true メニュー表示あり
 * @note
 */
bool  CExecMonitorWnd::OnCustomContextMenu(CVirtualCheckList* pCheckList,
                                     POINT ptClick, int nRow, int nCol)
{
    CVirtualTreeCtrlItem* pItem;
    ExecMonitorItem*      pItemData = NULL;

    pItem = m_vTreeData.GetItem(nRow);

    int iItemNo = 0;
    if (pItem)
    {
        iItemNo = reinterpret_cast<int>(pItem->pData);
    }

    if (iItemNo >= m_lstItem.size() )
    {
        return false;
    }

    pItemData = &m_lstItem[iItemNo];


    CMenu menu;
    menu.CreatePopupMenu();
    
    UINT nFlagStart = MF_STRING | MF_GRAYED;
    UINT nFlagPause = MF_STRING | MF_GRAYED;
    UINT nFlagAbort = MF_STRING | MF_GRAYED;
    UINT nFlagBreak = MF_STRING | MF_GRAYED;
    

    if (pItemData->eType == E_GROUP)
    {
        return false;
    }
    else if (pItemData->eType == E_TIME)
    {
        return false;
    }

    TCB*  pTcb = pItemData->pTcb;

    if (!pTcb)
    {
        return false;
    }

    switch( pTcb->GetStatus())
    {
    case TS_WAIT:
    case TS_SUSPEND:
        nFlagStart = MF_STRING | MF_ENABLED;
        break;

    case TS_RUN:
    case TS_WAIT_CYCLE:
        nFlagPause = MF_STRING | MF_ENABLED;
        nFlagAbort = MF_STRING | MF_ENABLED;
        nFlagBreak = MF_STRING | MF_ENABLED;
        break;


    case TS_INTRRUPT:
    case TS_YIELD:
    case TS_SLEEP:
        nFlagAbort = MF_STRING | MF_ENABLED;
        nFlagBreak = MF_STRING | MF_ENABLED;
        break;

    case TS_BREAK:
    case TS_EXT:
        break;
    }


    menu.AppendMenu(nFlagStart, ID_MNU_EXEC_PART_START, GET_STR(STR_MNU_EXEC_GROUP_START));
    menu.AppendMenu(nFlagPause, ID_MNU_EXEC_PART_PAUSE,GET_STR(STR_MNU_EXEC_GROUP_PAUSE));
    menu.AppendMenu(nFlagAbort, ID_MNU_EXEC_PART_ABORT, GET_STR(STR_MNU_EXEC_GROUP_ABORT));
    menu.AppendMenu(nFlagBreak, ID_MNU_EXEC_PART_BREAK,GET_STR(STR_MNU_EXEC_GROUP_BREAK));

    menu.AppendMenu(MF_SEPARATOR);
    menu.AppendMenu(MF_STRING, ID_MNU_EXEC_GROUP_CONFIG,GET_STR(STR_MNU_EXEC_GROUP_CONFIG));



    menu.TrackPopupMenu(
        TPM_LEFTALIGN  |    //クリック時のX座標をメニューの左辺にする
        TPM_RIGHTBUTTON,    //右クリックでメニュー選択可能とする
        ptClick.x,ptClick.y,    //メニューの表示位置
        &m_vlstMonitor      //このメニューを所有するウィンドウ
    );
    menu.DestroyMenu();
    return true;
}

/**
 *  @brief コンテキストメニュー選択
 *  @param [in] pWnd   マウスの右ボタンがクリックされたウィンドウのハンドル
 *  @param [in] point  クリックされたときの、カーソルの画面座標位置 
 *  @retval           なし
 *  @note
 */
void CExecMonitorWnd::OnContextMenu(CWnd* pWnd, CPoint point)
{
    if (pWnd->m_hWnd != m_vlstMonitor.m_hWnd)
    {
        return;
    }

    //ヒットテストが失敗する CallbackContextMenuで処理を行う

}


/**
 * @brief   コンテキストメニューコールバック
 * @param   [in]  pParent    呼び出し元     
 * @param   [in]  nCode      コマンドコード
 * @param   [in]  nID        コマンドID
 * @retval  true 処理を実行
 * @note	
 */
bool CExecMonitorWnd::CallbackOnCommand (LPVOID pParent, 
                                CVirtualCheckList* pCheckList,
                                  UINT nCode, UINT nID)
{
    CExecMonitorWnd* pBase = reinterpret_cast<CExecMonitorWnd*>(pParent);
    return pBase->OnCommand( pCheckList, nCode, nID);
}


/**
 * @brief   コマンド
 * @param   [in]  nCode      コマンドコード
 * @param   [in]  nID        コマンドID
 * @retval  true 処理を実行
 * @note
 */
bool  CExecMonitorWnd::OnCommand(CVirtualCheckList* pCheckList, UINT nCode, UINT nID)
{
    bool bRet = false;
    int iRow;
    int iCol;
    CVirtualTreeCtrlItem* pItem;

    pCheckList->GetCurCell(&iRow, &iCol);

    ExecMonitorItem*      pItemData = NULL;
    int                 iItemNo;

    pItem = m_vTreeData.GetItem(iRow);
    if (pItem)
    {
        iItemNo = reinterpret_cast<int>(pItem->pData);
    }
	else
	{
		return false;
	}

    if (iItemNo >= m_lstItem.size() )
    {
        return false;
    }

    pItemData = &m_lstItem[iItemNo];

    if (!pItemData)
    {
        return false;
    }

    if (!m_pExecCtrl)
    {
        return false;
    }

    CExecCtrl* pExecCtrl = THIS_APP->GetExecCtrl();

    EXEC_COMMON::E_THREAD_TYPE eThreadType;
    eThreadType = pItemData->eGroup;
    if (pItemData->eType == E_GROUP)
    {
        /*
        switch(nID)
        {
        case ID_MNU_EXEC_GROUP_START:
            pExecCtrl->Resume(eThreadType);
            break;

        case ID_MNU_EXEC_GROUP_PAUSE:
            pExecCtrl->Pause(eThreadType);
            break;

        case ID_MNU_EXEC_GROUP_ABORT:
            pExecCtrl->Abort(eThreadType);
            break;
        }
        */

        return false;
    }
    else if (pItemData->eType == E_TIME)
    {
        return false;
    }


    TCB* pTcb;
    pTcb = pItemData->pTcb;

    if (!pTcb)
    {
        return false;
    }

    switch(nID)
    {

    case ID_MNU_EXEC_PART_START:
        if (pTcb->IsSetCmd(TCMD_RUN) == CT_OK)
        {
            pExecCtrl->AsTaskWakeUp(pTcb->GetId());
        }
        break;

    case ID_MNU_EXEC_PART_PAUSE:
        if (pTcb->IsSetCmd(TCMD_SUSPEND) == CT_OK )
        {
            pExecCtrl->AsTaskSuspend(pTcb->GetId());
        }
        break;

    case ID_MNU_EXEC_PART_ABORT:
        if (pTcb->IsSetCmd(TCMD_ABORT) == CT_OK )
        {
            pExecCtrl->AsTaskAbort(pTcb->GetId());
        }

        break;
        break;

    case ID_MNU_EXEC_PART_BREAK:
        if (pTcb->IsSetCmd(TCMD_TERMINATE) == CT_OK )
        {
            pExecCtrl->AsTaskTerminate(pTcb->GetId());
        }
        break;

    case ID_MNU_EXEC_GROUP_CONFIG:
        break;

    default:
        break;
    }

    return true;
}