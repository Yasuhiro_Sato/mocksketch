/**
 * @brief			CExecMonitorWndヘッダーファイル
 * @file			CExecMonitorWnd.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#pragma once

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "Utility/VirtualTreeCtrl/VirtualCheckList.h"
#include "Utility/VirtualTreeCtrl/VirtualTreeCtrl.h"
#include "ToolBar/CustomToolBar.h"
#include "SubWindow/CustomDocablePane.h"
#include "ExecCommon.h"
#include "Script/CExecCtrl.h"

/*---------------------------------------------------*/
/*  class                                            */
/*---------------------------------------------------*/
//struct EXEC_GROUP;
class  CExecCtrl;

enum E_EXEC_DSP_TYPE
{
    E_GROUP,
    E_TIME,
    E_ITEM,
};

enum E_EXEC_ICON_STS
{
    E_IC0_NONE     = 0,
    E_IC0_RUN      = 1,
    E_IC0_RUN_PART = 2,
    E_IC0_STOP     = 3,
    E_IC0_PAUSE    = 4,
};

struct ExecMonitorItem
{
    StdString   strName;
    E_EXEC_DSP_TYPE   eType;
    int         iModuleNo;
    EXEC_COMMON::E_THREAD_TYPE eGroup;
    DWORD       dwCycleime; //設定実行時間
    double      dExecTime; //実実行時間
    TCB*        pTcb;
};


/**
 * @class   CIoSetWnd
 * @brief
 */
class CExecMonitorToolBar : public CCustomToolBar
{
public:
    CExecMonitorToolBar():CCustomToolBar(){SetName(_T("CExecMonitorToolBar"));}

    void OnReset ();

	virtual void OnUpdateCmdUI(CFrameWnd* /*pTarget*/, BOOL bDisableIfNoHndler)
	{
		CCustomToolBar::OnUpdateCmdUI((CFrameWnd*) GetOwner(), bDisableIfNoHndler);
	}
    
	virtual BOOL AllowShowOnList() const { return FALSE; }

    virtual BOOL OnUserToolTip(CMFCToolBarButton* pButton,
                               CString& strTTText ) const;
};

/**
 * @class   CExecMonitorWnd
 * @brief                     
 */
class CExecMonitorWnd : public CCustomDocablePane
{
    DECLARE_DYNAMIC(CExecMonitorWnd)
    // 表示列
    enum ENUM_COL
    {
        COL_OBJ_NAME  = 0,   //
        COL_EXEC_TIME = 1,
    };
 
public:
    CExecMonitorWnd();
    virtual ~CExecMonitorWnd();

    //!< 初期化
    void InitView();

    // 描画コールバック
    static void __cdecl Callbackfunc(LPVOID pParent, int nRow, int nCol, _TCHAR* pData, bool* pbCheck);

    // チェックコールバック
    static void __cdecl CallbackChek(LPVOID pParent, int nRow, int nCol);

    //描画処理
    virtual void SetText( int nRow, int nCol, _TCHAR* pData, bool* pbCheck);


    // オーナードロー
    static void __cdecl CallOwnerDraw (LPVOID pParent, 
                    CVirtualCheckList* pCheckList,
                    int nRow, int nCol, 
                    CDC* pDC,  CString sData, 
                    bool bCheck);

    // オーナーマウス
    static void __cdecl CallOwnerMouse   (LPVOID pParent, 
                        CVirtualCheckList* pCheckList,
                        int nRow, int nCol, 
                        POINT ptPos, bool bMouseDown);

    //!< コンテキストメニューコールバック
    static void __cdecl CallbackContextMenu
                        (LPVOID pParent, CVirtualCheckList* pCheckList,
                        POINT ptClick, int nRow, int nCol);

    //!< コンテキストメニュー表示
    bool  OnCustomContextMenu(CVirtualCheckList* pCheckList,
                              POINT ptClick, int nRow, int nCol);

    //!< コマンドコールバック
    static bool __cdecl CallbackOnCommand (LPVOID pParent,
                                  CVirtualCheckList* pCheckList,
                                  UINT nCode, UINT nID);

    //!< コマンド
    bool  OnCommand(CVirtualCheckList* pCheckList,
                    UINT nCode, UINT nID);

    void UpdateItem();

protected:
    void _MakeMapItem();
    void _UpdateListItem();
    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnSetFocus(CWnd* pOldWnd);
    afx_msg void OnSettingChange(UINT uFlags, LPCTSTR lpszSection);
    //!<モード変更
    afx_msg LRESULT OnModeChange(WPARAM wParam, LPARAM lParam);
    //!< View活性化通知
    afx_msg LRESULT OnViewActivate(WPARAM pMessage, LPARAM lParam);
    //!< ビュークリア
    afx_msg LRESULT OnViewClear(WPARAM pMessage, LPARAM lParam);

    //!< View生成通知
    afx_msg LRESULT OnViewCreate(WPARAM iType, LPARAM pMessage);

    //!< 実行時画面更新
    afx_msg LRESULT OnUpdateExec(WPARAM wParam, LPARAM lParam);

    //!< コンテキストメニュー
    afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);

    DECLARE_MESSAGE_MAP()



protected:
    //!< レイアウト調整
    void AdjustLayout();

    //!< フォント変更
    void SetListFont();


protected:
    friend CExecMonitorWnd;
    //!< フォント
    CFont m_Font;

    //!< ツールバー
    CExecMonitorToolBar m_wndToolBar;

    //!< ツリーコントロール
    CVirtualCheckList   m_vlstMonitor;

    //!< ツリーデータ
    CVirtualTreeCtrl  m_vTreeData;

    //!< 実データ
    //std::deque<ExecMonitorItem>  m_lstItem;
    
    //!< 実行インジケータ用イメージ
    CImageList     m_bmpExecSts;

    int m_iMapChgCnt;

    //!< 
    CExecCtrl*      m_pExecCtrl;

    COUNT_MAP<int, std::shared_ptr<TCB> > m_mapTcb;

    //static std::vector<EXEC_COMMON::E_THREAD_TYPE> ms_lstDispThreadType;

    std::map<int , std::map< int, std::vector<TCB*> > > m_mapItem;

    std::vector<ExecMonitorItem>  m_lstItem;
};


