// C:\Backup\Projects\MockTools\MockSketch\Src\mocksketch\MockSketch\SubWindow\ImageCtrlDlg.cpp : 実装ファイル
//

#include "stdafx.h"
#include "MockSketch.h"
#include "./ImageCtrlDlg.h"
#include "./ImagePropertyDlg.h"
#include "DefinitionObject/CPartsDef.h"
#include "System/CSystem.h"
#include "afxdialogex.h"


// CImageCtrlDlg ダイアログ

IMPLEMENT_DYNAMIC(CImageCtrlDlg, CDialog)

BEGIN_MESSAGE_MAP(CImageCtrlDlg, CDialog)
    ON_BN_CLICKED(IDC_PB_ADD, &CImageCtrlDlg::OnBnClickedPbAdd)
    ON_BN_CLICKED(IDC_PB_SET, &CImageCtrlDlg::OnBnClickedPbSet)
    ON_BN_CLICKED(IDC_PB_DEL, &CImageCtrlDlg::OnBnClickedPbDel)
    ON_BN_CLICKED(IDCANCEL, &CImageCtrlDlg::OnBnClickedCancel)
    ON_WM_CREATE()
    ON_BN_CLICKED(IDOK, &CImageCtrlDlg::OnBnClickedOk)
    ON_LBN_SELCHANGE(IDC_LIST1, &CImageCtrlDlg::OnLbnSelchangeList1)
END_MESSAGE_MAP()

CImageCtrlDlg::CImageCtrlDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CImageCtrlDlg::IDD, pParent),
    m_bSelectMode(false)
{
}

CImageCtrlDlg::CImageCtrlDlg(CPartsDef* pDef, CWnd* pParent /*=NULL*/)
	: CDialog(CImageCtrlDlg::IDD, pParent),
    m_bSelectMode(false)
{
    m_pParts = pDef;
}

CImageCtrlDlg::~CImageCtrlDlg()
{
}

void CImageCtrlDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_LIST1, m_lscFile);
}

void CImageCtrlDlg::SetSelectMode()
{
    //ダイアログ表示前に設定する
    m_bSelectMode = true;
}

StdString CImageCtrlDlg::GetSeletFileName()
{
   return m_strSelFileName;
}


int CImageCtrlDlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (CDialog::OnCreate(lpCreateStruct) == -1)
        return -1;

    return 0;
}


void CImageCtrlDlg::OnBnClickedPbAdd()
{
    //ファイル読み込み
    StdString strFileNeme;
    if (m_pParts->LoadImageDlg(&strFileNeme))
    {
        _RedrawList();
    }
}


void CImageCtrlDlg::OnBnClickedPbSet()
{
    StdString strName;
    strName = _GetSelectFileName();

    if (!m_pParts)
    {
        return;
    }


    //イメージデータ取得
    std::shared_ptr<CImageData> pImage = m_pParts->GetImage(strName.c_str()).lock();

    if (!pImage)
    {
        AfxMessageBox(GET_STR(STR_ICD_ERROR_LOAD));
        return;
    }

    CImagePropertyDlg dlgProp;

    dlgProp.SetData(pImage->GetProp());

    if (dlgProp.DoModal() == IDOK)
    {
        CImagePropData prop;
        dlgProp.GetData(&prop);
        pImage->SetProp(prop);
        m_pParts->Redraw();
    }
}

void CImageCtrlDlg::_CheckButten()
{
    StdString strRet;
    int iSel = m_lscFile.GetCurSel();
    BOOL bEnable = TRUE;
    if (iSel == LB_ERR )
    {
        bEnable = FALSE;
    }
    GetDlgItem(IDC_PB_SET)->EnableWindow(bEnable);
    GetDlgItem(IDC_PB_DEL)->EnableWindow(bEnable);
}



void CImageCtrlDlg::OnBnClickedPbDel()
{
    StdString strFileName = _GetSelectFileName();

    m_pParts->DeleteImage(strFileName.c_str());
    _RedrawList();
}


void CImageCtrlDlg::OnBnClickedCancel()
{
    CDialog::OnCancel();
}


StdString CImageCtrlDlg::_GetSelectFileName()
{
    StdString strRet;
    int iSel = m_lscFile.GetCurSel();
    if (iSel != LB_ERR )
    {
        CString str;
        m_lscFile.GetText(iSel, str);
        strRet = str;
    }
    return strRet;
}
void CImageCtrlDlg::OnBnClickedOk()
{

    m_strSelFileName = _GetSelectFileName();

    CDialog::OnOK();
}

void CImageCtrlDlg::_RedrawList()
{
    m_lscFile.ResetContent();
    if (m_pParts)
    {
        std::vector<StdString> lst;
        m_pParts->GetImageList(&lst);

        for( auto dat: lst)
        {
            m_lscFile.AddString(dat.c_str());
        }
    }
}

BOOL CImageCtrlDlg::OnInitDialog()
{
    CDialog::OnInitDialog();


    SetWindowText(GET_STR(STR_ISC_DLG_CTRL));
    GetDlgItem(IDC_PB_ADD)->SetWindowText(GET_STR(STR_ISC_ADD));
    GetDlgItem(IDC_PB_SET)->SetWindowText(GET_STR(STR_ISC_SET));
    GetDlgItem(IDC_PB_DEL)->SetWindowText(GET_STR(STR_ISC_DEL));


    _RedrawList();

    if (m_bSelectMode)
    {
        RECT rcLsc;
        m_lscFile.GetWindowRect(&rcLsc);

        RECT rcMain;
        GetWindowRect(&rcMain);
        long iLeftMargin = rcLsc.left - rcMain.left;
        long iLscWidth = rcLsc.right - rcLsc.left;

        int iWidth  = iLscWidth + 2 * iLeftMargin;
        int iHeigth = rcMain.bottom;

        GetDlgItem(IDC_PB_ADD)->ShowWindow(SW_HIDE);
        GetDlgItem(IDC_PB_SET)->ShowWindow(SW_HIDE);
        GetDlgItem(IDC_PB_DEL)->ShowWindow(SW_HIDE);

        SetWindowPos( NULL, 0,0, iWidth, iHeigth, SWP_NOZORDER | SWP_NOMOVE);
    }
    else
    {
        _CheckButten();
    }
    return TRUE; 
}


void CImageCtrlDlg::OnLbnSelchangeList1()
{
    _CheckButten();
}
