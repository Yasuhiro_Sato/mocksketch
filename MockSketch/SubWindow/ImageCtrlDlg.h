#pragma once
#include "afxwin.h"
#include "resource.h"


// CImageCtrlDlg ダイアログ
class CPartsDef;

class CImageCtrlDlg : public CDialog
{
	DECLARE_DYNAMIC(CImageCtrlDlg)

    CPartsDef* m_pParts;

    bool m_bSelectMode;

    StdString m_strSelFileName;

public:
	CImageCtrlDlg(CPartsDef* pDef, CWnd* pParent = NULL);   // 標準コンストラクター
	virtual ~CImageCtrlDlg();
    void SetSelectMode();
    StdString GetSeletFileName();

// ダイアログ データ
	enum { IDD = IDD_DLG_IMAGE_CTRL };

protected:
	CImageCtrlDlg(CWnd* pParent = NULL);   // 標準コンストラクター
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート
    void _RedrawList();
    void _CheckButten();
    StdString _GetSelectFileName();

	DECLARE_MESSAGE_MAP()
public:
    afx_msg void OnBnClickedPbAdd();
    afx_msg void OnBnClickedPbSet();
    afx_msg void OnBnClickedPbDel();
    afx_msg void OnBnClickedCancel();
    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
    afx_msg void OnBnClickedOk();
    CListBox m_lscFile;
    virtual BOOL OnInitDialog();
    afx_msg void OnLbnSelchangeList1();
};
