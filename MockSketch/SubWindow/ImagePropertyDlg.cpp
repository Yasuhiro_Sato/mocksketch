// ImageProperty.cpp : 実装ファイル
//

#include "stdafx.h"
#include "MockSketch.h"
#include "ImagePropertyDlg.h"
#include "afxdialogex.h"
#include "System/CSystem.h"

// CImageProperty ダイアログ

IMPLEMENT_DYNAMIC(CImagePropertyDlg, CDialog)

BEGIN_MESSAGE_MAP(CImagePropertyDlg, CDialog)
    ON_WM_CREATE()
    ON_BN_CLICKED(IDOK, &CImagePropertyDlg::OnBnClickedOk)
    ON_BN_CLICKED(IDCANCEL, &CImagePropertyDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


/**
 *  @brief   コンストラクター
 *  @param   なし
 *  @retval  なし
 *  @note
 */
CImagePropertyGrid::CImagePropertyGrid()
{

}

/**
 *  @brief   初期設定
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CImagePropertyGrid::Setup()
{

    EnableHeaderCtrl();
    EnableDescriptionArea();
    SetVSDotNetLook(true/*m_bDotNetLook*/);
    MarkModifiedProperties(true/*m_bMarkChanged*/);
    SetAlphabeticMode(false/*!m_bPropListCategorized*/);
    SetShowDragContext(true/*m_bShowDragContext*/);



    std::auto_ptr<CMFCPropertyGridProperty> 
        apDrawConfig(new CMFCPropertyGridProperty(GET_STR(STR_IPG_IMAGE)));



    int iVal = 0;
    //===============
    //X分割数
    //===============
    _SetPorperty(apDrawConfig.get(), &m_Data.m_iXDiv, E_VAL_INT,
                             GET_STR(STR_IPG_XDIV),
                             GET_STR(STR_IPG_INFO_XDIV),
                             iVal++);

    
    //===============
    //Y分割数
    //===============
    _SetPorperty(apDrawConfig.get(), &m_Data.m_iYDiv, E_VAL_INT,
                             GET_STR(STR_IPG_YDIV),
                             GET_STR(STR_IPG_INFO_YDIV),
                             iVal++);

    //===============
    //オフセット
    //===============
    CMFCPropertyGridProperty* pGroupOffset = 
        new CMFCPropertyGridProperty(GET_STR(STR_IPG_OFFSET));

    _SetPorperty(pGroupOffset, &m_Data.m_ptOffset.dX, E_VAL_DOUBLE,
                             GET_STR(STR_IPG_XPOS),
                             GET_STR(STR_IPG_INFO_XPOS),
                             iVal++);

    _SetPorperty(pGroupOffset, &m_Data.m_ptOffset.dY, E_VAL_DOUBLE,
                             GET_STR(STR_IPG_YPOS),
                             GET_STR(STR_IPG_INFO_YPOS),
                             iVal++);

    //===============
    //Dpi
    //===============
    _SetPorperty(apDrawConfig.get(), &m_Data.m_iDpi, E_VAL_INT,
                             GET_STR(STR_IPG_DPI),
                             GET_STR(STR_IPG_INFO_DPI),
                             iVal++);


    //===============
    // 透過有無
    //===============
    StdString strTrans;
    strTrans = GET_STR(STR_IPG_TRP_NONE);
    strTrans += _T(",");
    strTrans += GET_STR(STR_IPG_TRP_COLOR);
    strTrans += _T(",");
    strTrans += GET_STR(STR_IPG_TRP_LT);
    strTrans += _T(",");
    strTrans += GET_STR(STR_IPG_TRP_RB);


    _SetPorperty(apDrawConfig.get(), &m_Data.m_eTranspaent, E_VAL_ENUM,
                            GET_STR(STR_IPG_TRANSPARENT),
                            GET_STR(STR_IPG_INFO_TRANSPARENT),
                            iVal++,
                            strTrans);

    //===============
    // 透過色
    //===============
    _SetPorperty(apDrawConfig.get(), &m_Data.m_crTransparent, E_VAL_COLOR,
                            GET_STR(STR_IPG_TRANSPARENT_COLOR),
                            GET_STR(STR_IPG_INFO_TRANSPARENT_COLOR),
                            iVal++);

	AddProperty(apDrawConfig.release());

}

/**
 *  @brief   データ設定
 *  @param   [in] setting 設定値
 *  @retval  なし
 *  @note
 */
void CImagePropertyGrid::SetData(const CImagePropData& setting)
{
    m_Data = setting;
}

/**
 *  @brief   データ取得
 *  @param   [out]  pSetting 取得データ
 *  @retval  なし
 *  @note
 */
void CImagePropertyGrid::GetData(CImagePropData* pSetting)
{
    *pSetting = m_Data;
}
//-----------------------
//-----------------------




CImagePropertyDlg::CImagePropertyDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CImagePropertyDlg::IDD, pParent)
{

}

CImagePropertyDlg::~CImagePropertyDlg()
{
}

void CImagePropertyDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_FRM_LST, m_frmImage);
}

//!< データ設定
void CImagePropertyDlg::SetData(const CImagePropData& setting)
{
    m_wndImagePorperty.SetData(setting);
}
    
//!< データ取得
void CImagePropertyDlg::GetData(CImagePropData* pSetting)
{
    m_wndImagePorperty.GetData(pSetting);
}


int CImagePropertyDlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (CDialog::OnCreate(lpCreateStruct) == -1)
        return -1;

    // TODO: ここに特定な作成コードを追加してください。
    return 0;
}


void CImagePropertyDlg::OnBnClickedOk()
{
    // TODO: ここにコントロール通知ハンドラー コードを追加します。
    CDialog::OnOK();
}


void CImagePropertyDlg::OnBnClickedCancel()
{
    // TODO: ここにコントロール通知ハンドラー コードを追加します。
    CDialog::OnCancel();
}


BOOL CImagePropertyDlg::OnInitDialog()
{
    CDialog::OnInitDialog();

    SetWindowText(GET_STR(STR_ISP_DLG_PROP));
    
	CRect rectPropList;

	m_frmImage.GetClientRect(&rectPropList);
	m_frmImage.MapWindowPoints(this, &rectPropList);

	m_wndImagePorperty.Create(WS_CHILD |  WS_TABSTOP | WS_BORDER, rectPropList, this, (UINT)-1);
    m_wndImagePorperty.Setup();

    m_wndImagePorperty.ShowWindow(SW_SHOW);

    return TRUE;  // return TRUE unless you set the focus to a control
    // 例外 : OCX プロパティ ページは必ず FALSE を返します。
}
