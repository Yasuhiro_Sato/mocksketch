#pragma once
#include "afxwin.h"
#include "System/CConfig.h"
#include "Utility/CImageData.h"


/**
 * @class   CEditorPropertyGrid
 * @brief   エディタ設定 プロパティグリッド               
 */
class CImagePropertyGrid : public CConfigPropertyGrid
{
public:
    //!<コンストラクタ
    CImagePropertyGrid();

    //!<デストラクタ
    virtual ~CImagePropertyGrid(){;}

    //!<初期設定
    virtual void Setup();

    //!< データ設定
    void SetData(const CImagePropData& setting);
    
    //!< データ取得
    void GetData(CImagePropData* pSetting);

protected:
    mutable CImagePropData m_Data;

};


// CImageProperty ダイアログ

class CImagePropertyDlg : public CDialog
{
	DECLARE_DYNAMIC(CImagePropertyDlg)
    CImagePropertyGrid   m_wndImagePorperty;

public:
	CImagePropertyDlg(CWnd* pParent = NULL);   // 標準コンストラクター
	virtual ~CImagePropertyDlg();

   //!< データ設定
    void SetData(const CImagePropData& setting);
    
    //!< データ取得
    void GetData(CImagePropData* pSetting);

// ダイアログ データ
	enum { IDD = IDD_DLG_IMAGE_PROPERTY };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート

	DECLARE_MESSAGE_MAP()
public:
    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
    CStatic m_frmImage;
    afx_msg void OnBnClickedOk();
    afx_msg void OnBnClickedCancel();

    virtual BOOL OnInitDialog();
};
