// ImageSelDlg.cpp : 実装ファイル
//

#include "stdafx.h"
#include "MockSketch.h"
#include "ImageSelDlg.h"
#include "afxdialogex.h"
#include "System/CSystem.h"


// CImageSelDlg ダイアログ

IMPLEMENT_DYNAMIC(CImageSelDlg, CDialogEx)

CImageSelDlg::CImageSelDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CImageSelDlg::IDD, pParent)
    , m_rdSel(0)
{

}

CImageSelDlg::~CImageSelDlg()
{
}

void CImageSelDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
	DDX_Radio(pDX, IDC_RD_FILE, m_rdSel);

}


BEGIN_MESSAGE_MAP(CImageSelDlg, CDialogEx)
    ON_BN_CLICKED(IDOK, &CImageSelDlg::OnBnClickedOk)
    ON_BN_CLICKED(IDCANCEL, &CImageSelDlg::OnBnClickedCancel)
    ON_WM_CREATE()
END_MESSAGE_MAP()


// CImageSelDlg メッセージ ハンドラー


void CImageSelDlg::OnBnClickedOk()
{
	UpdateData ();
    CDialogEx::OnOK();
}


void CImageSelDlg::OnBnClickedCancel()
{
	UpdateData ();
    CDialogEx::OnCancel();
}


int CImageSelDlg::GetSelect()
{
    return m_rdSel;
}


int CImageSelDlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (CDialogEx::OnCreate(lpCreateStruct) == -1)
    {
        return -1;
    }



    return 0;
}


BOOL CImageSelDlg::OnInitDialog()
{
    CDialogEx::OnInitDialog();

    SetWindowText(GET_STR(STR_ISL_DLG_SEL));
    GetDlgItem(IDC_RD_FILE)->SetWindowText(GET_STR(STR_ISL_FILE));
    GetDlgItem(IDC_RD_STORED)->SetWindowText(GET_STR(STR_ISL_STORED));


    UpdateData(FALSE);

    return TRUE;  // return TRUE unless you set the focus to a control
    // 例外 : OCX プロパティ ページは必ず FALSE を返します。
}
