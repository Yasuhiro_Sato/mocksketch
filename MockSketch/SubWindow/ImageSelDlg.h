#pragma once
#include "afxwin.h"
#include "resource.h"


// CImageSelDlg ダイアログ

class CImageSelDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CImageSelDlg)

public:
	CImageSelDlg(CWnd* pParent = NULL);   // 標準コンストラクター
	virtual ~CImageSelDlg();
    int GetSelect();

// ダイアログ データ
	enum { IDD = IDD_DLG_IMAGE_SEL };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート

	DECLARE_MESSAGE_MAP()
public:
    afx_msg void OnBnClickedOk();
    afx_msg void OnBnClickedCancel();
    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
    int m_rdSel;
    virtual BOOL OnInitDialog();
};
