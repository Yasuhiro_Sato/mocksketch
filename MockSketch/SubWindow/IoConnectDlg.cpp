/**
 * @brief			IoConnectDlg実装ファイル
 * @file			IoConnectDlg.cpp
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MockSketch.h"
#include "./IoConnectDlg.h"
#include "System/CSystem.h"
#include "Utility/CUtility.h"
#include "DefinitionObject/CPartsDef.h"
#include "DrawingObject/CDrawingParts.h"
#include "Io/IIoDef.h"
#include "Io/CIoDef.h"
#include "Io/CIoRef.h"
#include "Io/CIoPropertyBlock.h"
#include "Io/CIoProperty.h"




/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
IMPLEMENT_DYNAMIC(CIoConnectDlg, CDialog)


BEGIN_MESSAGE_MAP(CIoConnectDlg, CDialog)
    ON_BN_CLICKED(IDOK, &CIoConnectDlg::OnBnClickedOk)
    ON_BN_CLICKED(IDCANCEL, &CIoConnectDlg::OnBnClickedCancel)
    ON_CBN_SELCHANGE(IDC_CB_CONNECT, &CIoConnectDlg::OnCbnSelchangeCbConnect)
    ON_CBN_SELCHANGE(IDC_CB_PAGE, &CIoConnectDlg::OnCbnSelchangeCbPage)
    ON_CBN_SELCHANGE(IDC_CB_IO, &CIoConnectDlg::OnCbnSelchangeCbIo)
  END_MESSAGE_MAP()


/**
  *  @brief  コンストラクター.
  *  @param  なし
  *  @return なし
  */
CIoConnectDlg::CIoConnectDlg(CWnd* pParent /*=NULL*/)
    : CDialog(CIoConnectDlg::IDD, pParent),
    m_pIo           (NULL),
    m_pCtrl         (NULL),
    m_iIoId         (-1),
    m_iIdConnect    (-1),
    m_iIoIdConnect  (-1)
{
}

/**
  *  @brief  デストラクタ.
  *  @param  なし
  *  @return なし
  */
CIoConnectDlg::~CIoConnectDlg()
{
}

/**
 *  @brief  フレームワークの自動的なデータ交換
 *  @param  pDX     CDataExchange オブジェクトへのポインタ
 *  @retval なし     
 *  @note   UpdateData メンバ関数から呼び出されます。
 */
void CIoConnectDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_CB_CONNECT, m_cbConnect);
    DDX_Control(pDX, IDC_CB_PAGE, m_cbPage);
    DDX_Control(pDX, IDC_CB_IO, m_cbIo);
}

/**
 *  @brief  Io設定
 *  @param  [in] pIo
 *  @param  [in] iIoId
 *  @retval true 成功
 *  @note   
 */
bool CIoConnectDlg::SetIo(const CIoDefBase* pIo, int iIoId)
{
    m_pIo   = pIo;
    m_iIoId = iIoId;
    return true;
}

/**
 *  @brief  接続情報設定
 *  @param  [in] pId    
 *  @param  [in] pIoId  
 *  @retval true 成功
 *  @note   iId == -1 は未設定を示す
 */
bool CIoConnectDlg::SetConnect(int iId, int iIoId)
{
    m_iIdConnect   = iId;
    m_iIoIdConnect = iIoId;
    return true;
}

/**
 *  @brief  接続情報取得
 *  @param  [out] pId    
 *  @param  [out] pIoId  
 *  @retval true 成功
 *  @note   
 */
bool CIoConnectDlg::GetConnect(int* pId, 
                               int* pIoId) const
{

    *pId   = m_iSelId;
    *pIoId = m_iSelIoId;
    
    return true;
}

/**
 *  @brief  接続対象コンボボックス設定
 *  @param  [in]  pCtrl   描画コントロール
 *  @param  [in]  iSelId  接続対象オブジェクトID
 *  @retval true 成功
 *  @note   
 */
bool CIoConnectDlg::SetConnectCombo(CPartsDef* pCtrl, 
                                    int iSelId)
{
    if (!m_pIo)
    {
        STD_DBG(_T("m_pIo == NULL:  %s \n"), DB_FUNC_NAME.c_str());
        return false;
    }

    if (!pCtrl)
    {
        STD_DBG(_T("pCtrl == NULL:  %s \n"), DB_FUNC_NAME.c_str());
        return false;
    }

    auto pGroup = pCtrl->GetDrawingParts();

    if (!pGroup)
    {
        STD_DBG(_T("pGroup == NULL:  %s \n"), DB_FUNC_NAME.c_str());
        return false;
    }

    //<! マップ取得
    std::deque<std::shared_ptr<CDrawingObject>>* pListGroup;
    
    pListGroup = pGroup->GetList(); 

    int iConnectId;

    iConnectId = m_cbConnect.AddString(pCtrl->GetName().c_str());
    if (iConnectId != CB_ERR)
    {
        m_cbConnect.SetItemData(iConnectId, 0);
    }
    else
    {
        m_cbConnect.SetItemData(iConnectId, -1);
    }

    int iId;
    int iSelPos = -1;
    DRAWING_TYPE eType;
    for(auto pObj: *pListGroup)
    {
        eType = pObj->GetType();
        iId = pObj->GetId();
        if (!(eType & DT_SCRIPTBASE))
        {
            continue;
        }

        
        iConnectId = m_cbConnect.AddString(pObj->GetName().c_str());
        if (iConnectId != CB_ERR)
        {
            m_cbConnect.SetItemData(iConnectId, iId);
            if (iSelId == iId)
            {
                iSelPos = iConnectId;
            }
        }
        else
        {

            m_cbConnect.SetItemData(iConnectId, -1);
        }
    }

    if (iSelPos != -1)
    {
        m_cbConnect.SetCurSel(iSelPos);
    }
    else
    {
        m_cbConnect.SetCurSel(0);
    }

    return true;
}

/**
 *  @brief  ページコンボボックス設定
 *  @param  [in] pIoConnect   描画コントロール
 *  @param  [in] pCtrl        描画コントロール
 *  @retval true 成功
 *  @note   
 */
bool CIoConnectDlg::SetPageCombo(const CIoDefBase* pIoConnect, 
                                       CPartsDef* pCtrl)
{
    m_cbPage.ResetContent();
    
    if (!pCtrl)
    {
        return false;
    }

    CIoPropertyBlock*  pBlock;
    int iPageMax;

    pBlock = pIoConnect->GetBlock();
    iPageMax = pBlock->GetPageMax();
    
    StdString strPage;
    m_cbPage.AddString(GET_STR(STR_DLG_IOCONNECT_ALL));
    for (int iPage = 0; iPage < iPageMax; iPage++)
    {
        strPage = CUtil::StrFormat(GET_STR(STR_DLG_IOCONNECT_DISP_PAGE), iPage);
        m_cbPage.AddString(strPage.c_str());
    }

    m_cbPage.SetCurSel(0);
    return true;
}

/**
 *  @brief  接続IOコンボボックス設定
 *  @param  [in] pIoConnect
 *  @param  [in] iIoId
 *  @param  [in] iPage
 *  @retval true 成功
 *  @note   
 */
bool CIoConnectDlg::SetIoCombo(const CIoDefBase* pIoConnect, 
                               int iIoIdSel, 
                               int iPageSel)
{
    m_cbIo.ResetContent();

    CIoPropertyBlock*  pBlock;

    pBlock = pIoConnect->GetBlock();
    

    StdString strIoName;

    int iRowMax;
    int iIoId;

    int iPage;
    int iPageStart;
    int iPageEnd;

    if (iPageSel == -1)
    {
        iPageStart = 0;
        iPageEnd = pBlock->GetPageMax();
    }
    else
    {
        iPageStart = iPageSel;
        iPageEnd = iPageSel + 1;
    }

    CIoProperty* pProp;
    CIoProperty* pPropConnect;

    int iSel = -1;
    int iCbPos;

    iCbPos = m_cbIo.AddString(GET_STR(STR_DLG_IOCONNECT_NOT));
    m_cbIo.SetItemData(iCbPos, -1);

    pProp = m_pIo->GetBlock()->GetProperty(m_iIoId);

    for (iPage = iPageStart; iPage < iPageEnd; iPage++)
    {
        iRowMax = pBlock->GetRowMax(iPage);
        for(int iRow = 0; iRow < iRowMax; iRow++)
        {
            iIoId = pBlock->GetIoId(iPage, iRow);
            pPropConnect = pBlock->GetProperty(iIoId);

            if (!pProp->IsConnectable(pPropConnect))
            {
                continue;
            }

            strIoName = pPropConnect->GetName();
            iCbPos = m_cbIo.AddString(strIoName.c_str());
 
            if (iCbPos != CB_ERR)
            {
                m_cbIo.SetItemData(iCbPos, iIoId);
                if (iIoIdSel == iIoId)
                {
                    iSel = iCbPos;
                }
            }
            else
            {
                m_cbIo.SetItemData(iCbPos, -1);
            }
        }
    }

    if (iSel != -1)
    {
        m_cbIo.SetCurSel(iSel);
    }
    else
    {
        m_cbIo.SetCurSel(0);
    }

    return true;
}

/**
 *  @brief  Io接続先設定
 *  @param  なし
 *  @retval true 設定成功 
 *  @note   
 */
bool CIoConnectDlg::SetIoConnect()
{
    int iSel;
    int iId;

    iSel = m_cbConnect.GetCurSel();

    if (iSel == CB_ERR)
    {
        return false;
    }

    if (!m_pCtrl)
    {
        return false;
    }

    iId  = DwordptrToInt(m_cbConnect.GetItemData(iSel));

    if (iId == -1)
    {
        return false;
    }

    if (iId == 0)
    {
        m_pIoConnect = m_pCtrl->GetIoDef();
    }
    else
    {

        auto pObj =  m_pCtrl->GetObjectById(iId);
        auto pScriptBase = std::dynamic_pointer_cast<CDrawingScriptBase>(pObj);

        STD_ASSERT(pScriptBase);
        if (!pScriptBase)
        {
            return false;
        }
        m_pIoConnect = pScriptBase->GetIoRef();
    }

    return true;
}

/**
 *  @brief  ウインドウの初期化処理
 *  @param  なし
 *  @retval 常にTRUE     
 *  @note   
 */
BOOL CIoConnectDlg::OnInitDialog()
{
    CDialog::OnInitDialog();

    SetWindowText(GET_STR(STR_DLG_IOCONNECT));
    GetDlgItem(IDCANCEL)      ->SetWindowText(GET_STR(STR_DLG_CANCEL));
    GetDlgItem(IDC_ST_CONNECT)   ->SetWindowText(GET_STR(STR_DLG_NEWPRJ_REF));
    GetDlgItem(IDC_ST_REF_PAGE)  ->SetWindowText(GET_STR(STR_DLG_IOCONNECT_PAGE));
    GetDlgItem(IDC_ST_CONNECT_IO)->SetWindowText(GET_STR(STR_DLG_IOCONNECT_IO));

    m_pCtrl = m_pIo->GetPartsDef();

    if(!SetConnectCombo(m_pCtrl, m_iIdConnect))
    {
        EndDialog(IDCANCEL);
        return TRUE;
    }

    if(!SetIoConnect())
    {
        EndDialog(IDCANCEL);
        return TRUE;
    }

    if(!SetPageCombo(m_pIoConnect, m_pCtrl))
    {
        EndDialog(IDCANCEL);
        return TRUE;
    }

    if (!SetIoCombo(m_pIoConnect, m_iIoIdConnect, -1))
    {
        EndDialog(IDCANCEL);
        return TRUE;
    }

    return TRUE;
}

/**
 *  @brief  OKボタン押下
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CIoConnectDlg::OnBnClickedOk()
{
    int iSel = m_cbConnect.GetCurSel();
    m_iSelId  = DwordptrToInt(m_cbConnect.GetItemData(iSel));

    int iSelIo = m_cbIo.GetCurSel();
    m_iSelIoId  = DwordptrToInt(m_cbIo.GetItemData(iSelIo));

    OnOK();
}

/**
 *  @brief  キャンセルボタン押下
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CIoConnectDlg::OnBnClickedCancel()
{
    OnCancel();
}

/**
 *  @brief  メッセージフィルタ処理
 *  @param  [in] pMsg   ウインドウメッセージ
 *  @retval true これ以上の処理不要 / false 通常処理
 *  @note   
 */
BOOL CIoConnectDlg::PreTranslateMessage(MSG* pMsg)
{
    return CDialog::PreTranslateMessage(pMsg);
}

/**
 *  @brief  接続対象コンボボックス変更処理
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CIoConnectDlg::OnCbnSelchangeCbConnect()
{
    SetIoConnect();
    SetPageCombo(m_pIoConnect, m_pCtrl);
    SetIoCombo(m_pIoConnect, m_iIoIdConnect, -1);
}

/**
 *  @brief  参照ページコンボボックス変更処理
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CIoConnectDlg::OnCbnSelchangeCbPage()
{
    int iSel;
    iSel = m_cbPage.GetCurSel();

    if (iSel != CB_ERR)
    {
        SetIoCombo(m_pIoConnect, m_iIoIdConnect, iSel - 1);
    }
}

/**
 *  @brief  接続IOコンボボックス変更処理
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CIoConnectDlg::OnCbnSelchangeCbIo()
{
}
