/**
 * @brief			IoConnectDlgヘッダーファイル
 * @file			IoConnectDlg.h
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#pragma once
#include "afxwin.h"

/*---------------------------------------------------*/
/*  Class                                            */
/*---------------------------------------------------*/
class CIoDefBase;
class CPartsDef;

// CIoConnectDlg ダイアログ

class CIoConnectDlg : public CDialog
{
	DECLARE_DYNAMIC(CIoConnectDlg)

public:
    //!< コンストラクター
    CIoConnectDlg(CWnd* pParent = NULL);   // 標準コンストラクタ

    //!< デストラクター
    virtual ~CIoConnectDlg();

    //!< Io設定
    bool SetIo(const CIoDefBase* pIo, int iIoId);

    //!< 接続情報設定
    bool SetConnect(int iId, int iIoId);

    //!< 接続情報取得
    bool GetConnect(int* pId, int* pIoId) const;

    // ダイアログ データ
    enum { IDD = IDD_DLG_IO_CONNECT };

protected:

    //!< IO
    const CIoDefBase* m_pIo;

    const CIoDefBase* m_pIoConnect;

    //!< 接続元IOID
    int  m_iIoId;

    //!< 選択オブジェクトID
    int m_iSelId;

    //!< 選択IOID
    int m_iSelIoId;

    CPartsDef* m_pCtrl;


    //!< 接続対象
    CComboBox m_cbConnect;

    //!< 参照ページ
    CComboBox m_cbPage;

    //!< 接続IO
    CComboBox m_cbIo;

    //!<接続対象オブジェクトID
    int m_iIdConnect;

    //!<接続対象I/O ID
    int m_iIoIdConnect;

protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート
    DECLARE_MESSAGE_MAP()

    bool SetIoConnect();
    
    //接続対象コンボボックス設定
    bool SetConnectCombo(CPartsDef* pCtrl,
                   int iSelId);

    //接続IOページコンボボックス設定
    bool SetPageCombo(const CIoDefBase* pIoConnect, 
                       CPartsDef* pCtrl);

    //接続IOコンボボックス設定
    bool SetIoCombo(const CIoDefBase* pIoConnect, 
                    int iIoIdSel, 
                    int iPageSel);


public:
    virtual BOOL OnInitDialog();
    afx_msg void OnBnClickedOk();
    afx_msg void OnBnClickedCancel();
    afx_msg void OnCbnSelchangeCbConnect();
    afx_msg void OnCbnSelchangeCbPage();
    afx_msg void OnCbnSelchangeCbIo();
    virtual BOOL PreTranslateMessage(MSG* pMsg);

protected:
};
