/**
 * @brief			CIoSetWndヘッダーファイル
 * @file			CIoSetWnd.cpp
 * @author			Yasuhiro Sato
 * @date			01-2-2009 10:36:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./IoSetWnd.h"
#include "Resource.h"
#include "resource2.h"
#include "MainFrm.h"
#include "MockSketch.h"
#include "DefinitionObject/View/MockSketchDoc.h"

#include "DefinitionObject/View/MockSketchView.h"
#include "DefinitionObject/View/CDrawingView.h"
#include "DefinitionObject/CPartsDef.h"
#include "DrawingObject/CDrawingScriptBase.h"
#include "DrawingObject/CDrawingParts.h"
#include "Io/CIoDef.h"
#include "Io/CIoRef.h"
#include "Io/IIoDef.h"
#include "./IoSettingDlg.h"
#include "System/CSystem.h"
#include "Utility/CUtility.h"
/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

BEGIN_MESSAGE_MAP(CIoSetWnd, CDockablePane)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_WM_SETTINGCHANGE()
    ON_NOTIFY(LVN_ENDLABELEDIT, IDC_LST_VIRTUAL_IO_SET, OnLvnEndlabeleditVlst)
    ON_MESSAGE(WM_VIEW_CREATE,  OnViewCreate)
    ON_MESSAGE(WM_VIEW_ACTIVE, OnViewActivate)
    ON_MESSAGE(WM_VIEW_CLEAR_ALL, OnViewClear)
    ON_MESSAGE(WM_VIEW_SEL_CHANGEE, OnViewSelchange)
    ON_MESSAGE(WM_VIEW_PARTS_DELETE, OnPartsDelete)
    ON_MESSAGE(WM_MODE_CHANGE, OnModeChange)


    ON_COMMAND_RANGE(ID_IO_DEF_PAGE, ID_IO_DEF_PAGE_5, &CIoSetWnd::OnChangePageComboBox) //機能しない
    ON_COMMAND(ID_IO_SETTING, &CIoSetWnd::OnIoSetting)
    ON_COMMAND_RANGE(ID_IO_SELECT, ID_IO_SELECT_5, &CIoSetWnd::OnIoSelect) //機能しない


    ON_UPDATE_COMMAND_UI(ID_IO_SETTING, &CIoSetWnd::OnUpdateIoSetting)
    ON_UPDATE_COMMAND_UI_RANGE(ID_IO_DEF_PAGE, ID_IO_DEF_PAGE_5, &CIoSetWnd::OnUpdatePageComboBox)
    ON_UPDATE_COMMAND_UI_RANGE(ID_IO_SELECT, ID_IO_SELECT_5, &CIoSetWnd::OnUpdateIoSelect)

END_MESSAGE_MAP()
/*---------------------------------------------------*/
/* Static variables                                  */
/*---------------------------------------------------*/







/**
 * @brief   ツール バーを元の状態に戻します
 * @param   なし
 * @return	なし
 * @note	 既定の実装では、何も行われません。
 * @note	 ツール バーが元の状態に戻るときに置き換える必要がある
 * @note	 ダミー ボタンがツール バーにある場合は、
 * @note	 CMFCToolBar クラスから派生したクラスで 
 * @note	 OnReset をオーバーライドします。
 */
void CIoSetToolBar::OnReset ()
{
    //------------------------------------
    //  ページ設定ボックス挿入
    //------------------------------------
    CMFCToolBarComboBoxButton2  comboIoPage 
        (ID_IO_DEF_PAGE + m_iIndex,                                        // コマンド ID。
        GetCmdMgr ()->GetCmdImage  (ID_IO_DEF_PAGE + m_iIndex, FALSE),     // 関連付けられたイメージのインデックス。
        WS_VISIBLE | WS_TABSTOP | WS_VSCROLL | CBS_DROPDOWNLIST,    // スタイル。
        150 );                              // 幅 (ピクセル単位)。

    comboIoPage.SetToolTipText(_T("表示するページを選択します"));
    ReplaceButton (ID_IO_DEF_PAGE+ m_iIndex, comboIoPage);


    //------------------------------------
    //  IO選択コンボ挿入
    //------------------------------------
    CMFCToolBarComboBoxButton2  comboIoSel 
        (ID_IO_SELECT + m_iIndex,                                        // コマンド ID。
        GetCmdMgr ()->GetCmdImage  (ID_IO_SELECT + m_iIndex, FALSE),     // 関連付けられたイメージのインデックス。
        WS_VISIBLE | WS_TABSTOP | WS_VSCROLL | CBS_DROPDOWNLIST,    // スタイル。
        150 );                              // 幅 (ピクセル単位)。

    comboIoSel.SetToolTipText(_T("表示するIOを選択します"));
    ReplaceButton (ID_IO_SELECT+ m_iIndex, comboIoSel);


    //------------------------------------
    //  カレントオブジェクト選択ボタン挿入
    //------------------------------------
    /*
    CMFCToolBarButton  btnCurrent 
        (ID_IO_CURRENT,                                        // コマンド ID。
        GetCmdMgr ()->GetCmdImage  (ID_IO_CURRENT, FALSE),     // 関連付けられたイメージのインデックス。
        //TODO:テキスト変更
        _T("Current"),    // ボタンのテキスト ラベル。         
        FALSE,     //ユーザー定義のボタンかどうかを示すブール値
        FALSE );   //ボタンをカスタマイズできるかどうかを示すブール値
    btnCurrent.SetStyle(TBBS_CHECKBOX);
    ReplaceButton (ID_IO_CURRENT, btnCurrent);
    */


    int iIndex;
    if (m_bRef)
    {
        iIndex = CommandToIndex(ID_IO_SETTING+ m_iIndex);
        RemoveButton( iIndex);
    }

    if ((!m_bRef) ||
        (m_bCurrentSelMode)) 
    {

        iIndex = CommandToIndex(ID_IO_SELECT+ m_iIndex);
        RemoveButton( iIndex);
    }
}

/**
 * @brief   ツールヒント表示
 * @param   [in] pButton
 * @param   [in] pButton
 * @return  
 * @note    ボタンのツールヒントを表示する直前に、フレームワークによって
 * @note    呼び出されます。
 */ 
BOOL CIoSetToolBar::OnUserToolTip(CMFCToolBarButton* pButton,
                                  CString& strTTText ) const
{
    //TODO:IO選択、ページでツールヒントが出ない
    switch(pButton->m_nID)
    {
    //case ID_IO_DEF_PAGE: strTTText = GET_STR(STR_IOWND_TIP_PAGE); break;
    case ID_IO_SETTING: strTTText = GET_STR(STR_IOWND_TIP_SETTING); break;
    //case ID_IO_SELECT:  strTTText = GET_STR(STR_IOWND_TIP_SELECT); break;


    default:
        return FALSE;
        break;
    }
    return TRUE;
}


/**
 * @brief   コンストラクター
 * @param   なし
 * @return	なし
 * @note	 
 */ 
CIoSetWnd::CIoSetWnd():
m_pIo       (NULL),
//m_pObjDef   (NULL),
m_iId   (-1),
m_iPage (-1),
m_iIndex(-1),
m_bRef(false),

m_bCurrentSelMode(false)
{
}

/**
 * @brief   デストラクター
 * @param   なし
 * @return	なし
 * @note	 
 */
CIoSetWnd::~CIoSetWnd()
{
}

/**
 * @brief   レイアウト調整
 * @param   なし
 * @return	なし
 * @note	 
 */
void CIoSetWnd::AdjustLayout()
{
	if (GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect rectClient;

    GetClientRect(rectClient);


    STD_ASSERT(m_vlstIo.m_hWnd != NULL);

    int cyTlb = m_wndToolBar.CalcFixedLayout(FALSE, TRUE).cy;

    m_wndToolBar.SetWindowPos(NULL, rectClient.left, 
                                    rectClient.top, 
                                    rectClient.Width(), 
                                    cyTlb, SWP_NOACTIVATE | SWP_NOZORDER);

    m_vlstIo.SetWindowPos(NULL, rectClient.left,
                                   rectClient.top + cyTlb, 
                                   rectClient.Width(), 
                                   rectClient.Height() - cyTlb, SWP_NOACTIVATE | SWP_NOZORDER);

    //m_vlstIo.RedrawWindow();
}

/**
 * @brief   設定更新
 * @param   なし
 * @return  なし
 * @note    
 */
void CIoSetWnd::UpdatePageCombo()
{
DB_PRINT(_T("UpdatePageCombo1 %d \n"), m_iIndex);
    //最大ページ数更新
    CMFCToolBarComboBoxButton* pCombo;
    
    pCombo = GetComboBox(ID_IO_DEF_PAGE + m_iIndex);

    if (!m_pIo)    {return;}
    if (!pCombo)   {return;}

    int iPageMax = m_pIo->GetMaxPage();

    StdString strPage;

    pCombo->RemoveAllItems();
    //pCombo->AddItem(GET_STR(STR_DLG_IOCONNECT_ALL));
    for (int iPage = 0; iPage < iPageMax; iPage++)
    {
        strPage = CUtil::StrFormat(GET_STR(STR_DLG_IOCONNECT_DISP_PAGE), iPage);
        pCombo->AddItem(strPage.c_str());
    }

    pCombo->SelectItem(0);
    OnChangePageComboBox(m_iIndex);
    m_wndToolBar.RedrawWindow();
DB_PRINT(_T("UpdatePageCombo2 %d \n"), m_iIndex);
}

/**
 * @brief   Io選択更新
 * @param   なし
 * @return  なし
 * @note    
 */
void CIoSetWnd::UpdateIoSelCombo()
{
    CMFCToolBarComboBoxButton* pCombo;
    pCombo = GetComboBox(ID_IO_SELECT + m_iIndex);
    if (!pCombo)
    {
        return;
    }

    pCombo->RemoveAllItems();


    std::shared_ptr<CObjectDef> pDef;
    pDef = THIS_APP->GetCurrentObjectDef().lock();

    if (!pDef)
    {
        m_vlstIo.EnableWindow(FALSE);
        return;
    }


    CPartsDef* pCtrl;
    if ((pDef->GetObjectType() != VIEW_COMMON::E_PARTS) &&
        (pDef->GetObjectType() != VIEW_COMMON::E_FIELD))
    {
        m_vlstIo.EnableWindow(FALSE);
        return;
    }
    m_vlstIo.EnableWindow(TRUE);

    pCtrl = dynamic_cast<CPartsDef*>(pDef.get());

    if (!pCtrl)
    {
        return;
    }

    auto pGroup = pCtrl->GetDrawingParts();

    if (!pGroup)
    {
        return;
    }




    //<! マップ取得
    auto pMapList = pGroup->GetList(); 

    INT_PTR iConnectId;
                                                  

    iConnectId = pCombo->AddItem(GET_STR(STR_IOWND_NOT_SEL), -1);


    int iId = 0;
    int iSelPos = -1;
    DRAWING_TYPE eType;
    for(auto pObj: *pMapList)
    {
        eType = pObj->GetType();

        if (!(eType & DT_SCRIPTBASE))
        {
            continue;
        }
        pCombo->AddItem(pObj->GetName().c_str(), iId);
        if (m_iId == iId)
        {
            iSelPos = static_cast<int>(iConnectId);
        }
    }

    if (iSelPos != -1)
    {
        pCombo->SelectItem(iSelPos);
    }
    else
    {
        pCombo->SelectItem(0);
    }
    iId++;
}

/**
 * @brief   参照モード設定
 * @param   なし
 * @return  なし
 * @note    生成前に設定する
 */
void CIoSetWnd::SetRefMode()
{
    m_bRef = true;
    m_wndToolBar.m_bRef = true;
}

/**
 * @brief   カレントモード設定
 * @param   なし
 * @return  なし
 * @note    生成前に設定する
 */
void CIoSetWnd::SetCurrnetMode()
{
    SetRefMode();
    m_bCurrentSelMode = true;
    m_wndToolBar.m_bCurrentSelMode = true;
}


/**
 * @brief   ウインドウインデックス設定
 * @param   [in] iIndex ウインドウインデックス
 * @return  なし
 * @note    コンボボックスの識別に必要。
 * @note    生成前に設定しておく
 */
void CIoSetWnd::SetWindowIndex(int iIndex)
{
    STD_ASSERT(iIndex >= 0);
    STD_ASSERT(iIndex <= 5);

    m_iIndex = iIndex;
    m_wndToolBar.m_iIndex = iIndex;
}

/**
 * @brief   コンボボックス取得
 * @param   [in] uiCmd コマンドID
 * @return  コンボボックスへのポインタ
 * @note    通常の方法ではToolBarに設定したコンボボックスが取得できないので
 * @note    表示時にコンボボックスを取得する
 */
CMFCToolBarComboBoxButton* CIoSetWnd::GetComboBox(UINT uiCmd)
{
    CObList listButtons;
    if(CMFCToolBar::GetCommandButtons(uiCmd, listButtons) <= 0)
    {
        return NULL;
    }

    CMFCToolBarComboBoxButton* pCombo = NULL;
    CMFCToolBarComboBoxButton* pRetCombo = NULL;

    POSITION posCombo = listButtons.GetHeadPosition();

    while (posCombo != NULL)
    {
        pCombo = DYNAMIC_DOWNCAST(CMFCToolBarComboBoxButton, 
                listButtons.GetAt(posCombo));

        if (pCombo)
        {
            pRetCombo = pCombo;
            break;
        }

        listButtons.GetNext(posCombo);
    }

    return pRetCombo;
}


/**
 * @brief   生成時処理要求
 * @param   [in]    lpCreateStruct  初期化パラメータへのポインタ
 * @return  0:生成成功
 * @note	 
 */
int CIoSetWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (CDockablePane::OnCreate(lpCreateStruct) == -1)
    {
        return -1;
    }

    if (!m_vlstIo.Create(LVS_REPORT | LVS_ALIGNLEFT | LVS_OWNERDATA | WS_CHILD | 
                       WS_BORDER | WS_VISIBLE |WS_TABSTOP, CRect(), this, IDC_LST_VIRTUAL_IO_SET))
    {
        STD_DBG(_T("m_vlstIo Create fail"));
        return -1;
    }

	m_wndToolBar.Create(this, AFX_DEFAULT_TOOLBAR_STYLE, IDR_IO_DEF + m_iIndex);
	m_wndToolBar.LoadToolBar(IDR_IO_DEF + m_iIndex, 0, 0, TRUE /* ロックされています*/);
	m_wndToolBar.CleanUpLockedImages();
	m_wndToolBar.LoadBitmap(/*theApp.m_bHiColorIcons ? IDB_IO_DEF_HC : */IDR_IO_DEF + m_iIndex, 0, 0, TRUE /* ロックされました*/);

	m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() | CBRS_TOOLTIPS | CBRS_FLYBY);
	m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() & ~(CBRS_GRIPPER | CBRS_SIZE_DYNAMIC | CBRS_BORDER_TOP | CBRS_BORDER_BOTTOM | CBRS_BORDER_LEFT | CBRS_BORDER_RIGHT));
	m_wndToolBar.SetOwner(this);

	// すべてのコマンドが、親フレーム経由ではなくこのコントロール経由で渡されます:
	m_wndToolBar.SetRouteCommandsViaFrame(FALSE);


    //生成及び調整
	AdjustLayout();

    UpdatePageCombo();
	return 0;
}


/**
 *  @brief 列挿入
 *  @param [in] iCol       列番号
 *  @param [in] strItem    文字識別子 
 *  @retval           なし
 *  @note
 */
void CIoSetWnd::InsertColumn(int iCol, StdString strItem)
{
    int iWidth;
    int iPos;
    StdString strHeader;

    CUtil::FormatedText(GET_SYS_STR(strItem), &iWidth, &iPos, &strHeader);
    m_vlstIo.InsertColumn ( iCol,  strHeader.c_str(), iPos, iWidth);
}

/**
 *  @brief ウインドウサイズ変更
 *  @param [in] nType   要求されるサイズ変更のタイプ  (SIZE_MAXIMIZED....)
 *  @param [in] cx      クライアント領域の新しい幅 
 *  @param [in] cy      クライアント領域の新しい高さ
 *  @retval           なし
 *  @note
 */
void CIoSetWnd::OnSize(UINT nType, int cx, int cy)
{
	CDockablePane::OnSize(nType, cx, cy);
	AdjustLayout();
}



/**
 *  @brief  ウインドウフォーカス設定
 *  @param  [out] pOldWnd  今までフォーカスがあったウインドウ
 *  @retval なし
 *  @note   
 */
void CIoSetWnd::OnSetFocus(CWnd* pOldWnd)
{
	CDockablePane::OnSetFocus(pOldWnd);
}


/**
 *  @brief  設定変更
 *  @param  [in] uFlags  変更されたシステム パラメータを示すフラグ
 *  @param  [in] lpszSection   変更されたセクション名を示す文字列へのポインタ
 *  @retval なし
 *  @note   SystemParametersInfo参照
 */
void CIoSetWnd::OnSettingChange(UINT uFlags, LPCTSTR lpszSection)
{
	CDockablePane::OnSettingChange(uFlags, lpszSection);
	SetListFont();
}

/**
 *  @brief  フォント変更
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CIoSetWnd::SetListFont()
{
    /*
	::DeleteObject(m_fntPropList.Detach());


	LOGFONT lf;
	afxGlobalData.fontRegular.GetLogFont(&lf);

	NONCLIENTMETRICS info;
	info.cbSize = sizeof(info);

	afxGlobalData.GetNonClientMetrics(info);

	lf.lfHeight = info.lfMenuFont.lfHeight;
	lf.lfWeight = info.lfMenuFont.lfWeight;
	lf.lfItalic = info.lfMenuFont.lfItalic;


	m_fntPropList.CreateFontIndirect(&lf);
    */
}



/**
 *  @brief   ラベル編集終了
 *  @param    [in]  pNMHDR 通知メッセージへのポインタ
 *  @param    [out] pResult 
 *  @retval   なし
 *  @note	
 */
void CIoSetWnd::OnLvnEndlabeleditVlst(NMHDR *pNMHDR, LRESULT *pResult)
{
    if (m_pIo)
    {
       m_pIo->OnLvnEndlabeleditVlst(&m_vlstIo, pNMHDR, pResult);
    }
}

/**
* @brief    View生成通知
* @param    [in] iType 生成されたViewの種別;
* @param    [in] pMessage CPartsDefへのポインタ;
* @retval   なし
* @note	
*/
LRESULT CIoSetWnd::OnViewCreate(WPARAM iType, LPARAM pMessage)
{
    if (pMessage == 0)
    {
        return 0;
    }

    //TODO:他Viewに対応
    switch(iType)
    {
    case VIEW_COMMON::E_PARTS:
    case VIEW_COMMON::E_FIELD:
    case VIEW_COMMON::E_ELEMENT:
        {
            //m_pObjDef  = reinterpret_cast<CObjectDef*>(pMessage);
            //pIoDef = m_pObjDef->GetIoDef();
            break;
        }

    default:
        return 0;
    }


    if (m_bRef)
    {
        if (!m_bCurrentSelMode)
        {
            UpdateIoSelCombo();
        }
        return 0;
    }

    CIoDef* pIoDef = NULL;

    std::shared_ptr<CObjectDef> pDef;
    pDef = THIS_APP->GetCurrentObjectDef().lock();
    if(!pDef)
    {
        m_vlstIo.EnableWindow(FALSE);
        return 0;
    }

    m_vlstIo.EnableWindow(TRUE);
    pIoDef = pDef->GetIoDef();

    SetIo(pIoDef);


    return 0;
}

/**
 *  @brief  ウインドウプロシジャ
 *  @param    [in]  message  処理される Windows メッセージ
 *  @param    [in]  wParam   メッセージの処理で使う付加情報1
 *  @param    [in]  lParam   メッセージの処理で使う付加情報2
 *  @retval   メッセージに依存する値
 *  @note   
 */
LRESULT CIoSetWnd::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
    //!< 避難的対応 恐らくリフレクションの関連と思われるが
    //!< 同じメッセージが CWnd と CBasePainで発生する
    if (message == WM_NOTIFY)
    {
        if (wParam == IDC_LST_VIRTUAL_IO_SET)
        {
            return CWnd::WindowProc(message, wParam, lParam);

        }
    }

    else if (message == WM_COMMAND)
    {
        UINT nCode = HIWORD(wParam);  //通知コード
        UINT nID   = LOWORD(wParam);  // コントロールＩＤ
        if (nCode == CBN_SELCHANGE)
        {
            // ON_COMMAND(ID_IO_DEF_PAGE, &CIoSetWnd::OnChangeComboBox)
            // が機能しない
            switch(nID)
            {
            case ID_IO_DEF_PAGE:
            case ID_IO_DEF_PAGE_1: 
            case ID_IO_DEF_PAGE_2: 
            case ID_IO_DEF_PAGE_3: 
            case ID_IO_DEF_PAGE_4: 
            case ID_IO_DEF_PAGE_5: 
                {
                    OnChangePageComboBox(nID); 
                    break;
                }

            case ID_IO_SELECT:
            case ID_IO_SELECT_1:
            case ID_IO_SELECT_2:
            case ID_IO_SELECT_3:
            case ID_IO_SELECT_4:
            case ID_IO_SELECT_5:
                {
                    OnIoSelect(nID);
                    break;
                }

            }

        }
    }


    return CDockablePane::WindowProc(message, wParam, lParam);
}


/**
 *  @brief  メッセージフィルタ処理
 *  @param  [in] pMsg ウインドウメッセージ
 *  @retval true/false
 *  @note   
 */
BOOL CIoSetWnd::PreTranslateMessage(MSG* pMsg) 
{
    /*
    if (pMsg->message == WM_COMMAND)
    {
        StdString strWnd;
        if ( pMsg->hwnd == m_hWnd)
        {
            UINT nCode = HIWORD(pMsg->wParam);  //通知コード
            UINT nID   = LOWORD(pMsg->wParam);  // コントロールＩＤ
            if (ID_IO_DEF_PAGE == nID)
            {
                if (nCode == CBN_SELCHANGE)
                {
                    // ON_COMMAND(ID_IO_DEF_PAGE, &CIoSetWnd::OnChangeComboBox)
                    // が機能しない
                    OnChangeComboBox();
                }
            }
        }
    }
    */
    return CDockablePane::PreTranslateMessage(pMsg);
}

/**
 * @brief    View活性化を通知
 * @param    [in] pMessage タブタイトル文字
 * @param    [in] lParam   0:MockSletchView 1:EditView
 * @retval   なし
 * @note	
 */
LRESULT CIoSetWnd::OnViewActivate(WPARAM pMessage, LPARAM lParam)
{
    if (lParam != 0)
    {
        m_vlstIo.EnableWindow(FALSE);
        return 0;
    }


    std::shared_ptr<CObjectDef> pDef;
    pDef = THIS_APP->GetCurrentObjectDef().lock();
    
    if(!pDef)
    {
        m_vlstIo.EnableWindow(FALSE);
        return 0;
    }

    m_vlstIo.EnableWindow(TRUE);
    if (!m_bRef)
    {

        CIoDefBase* pIoDef;
        pIoDef = pDef->GetIoDef();

        SetIo(pIoDef);
        RedrawWindow();
    }
    else
    {
        if (!m_bCurrentSelMode)
        {
            UpdateIoSelCombo();
        }
        Clear();
    }

    return 0;
 
}


/**
 * @brief   ビュークリア
 * @param   [in] wParam 使用しない
 * @param   [in] lParam 使用しない
 * @return  なし
 * @note    
 */
LRESULT CIoSetWnd::OnViewClear(WPARAM pMessage, LPARAM lParam)
{
    m_pIo = NULL;
DB_PRINT(_T("OnViewClear %d \n"), m_iIndex);

    m_vlstIo.SetItemCount( 0 );
    m_vlstIo.UpdateWindow();
    UpdatePageCombo();

    return 0;
}

/**
 * @brief   ページコンボボックス変更
 * @param   [in] iIndex
 * @return  なし
 * @note    
 */
void CIoSetWnd::OnChangePageComboBox(UINT nId)
{
    int iIndex = nId - ID_IO_DEF_PAGE;

    CMFCToolBarComboBoxButton* pSrcCombo = GetComboBox(nId);

    if(iIndex != m_iIndex)  {return;}
    if(pSrcCombo == NULL)   {return;}

    ASSERT_VALID(pSrcCombo);
    int iPage = pSrcCombo->GetCurSel();

    if (iPage < 0)  {return;}
    if (!m_pIo)     {return;}

    m_pIo->SetCurrentPage(iPage);
    m_pIo->UpdateList(&m_vlstIo);
}

/**
 *  @brief   ページコンボボックス UI更新
 *  @param   [in out] コマンド ユーザー インターフェイス
 *  @retval  なし
 *  @note     
 */
void CIoSetWnd::OnUpdatePageComboBox(CCmdUI *pCmdUI)
{
}

/**
 * @brief   設定ボタン押下
 * @param   なし
 * @return  なし
 * @note    
 */
void CIoSetWnd::OnIoSetting()
{
    //TODO:ボードの場合は独自ダイアログを出せるようにする
    int iPageMax;

    if (!m_pIo) {return;}

    CIoDef* pIoDef;
    pIoDef = dynamic_cast<CIoDef*>(m_pIo);

    if (!pIoDef) {return;}

    iPageMax = m_pIo->GetMaxPage();
    CIoSettingDlg dlgIoSeting;
    dlgIoSeting.SetIoPage(iPageMax);

    if( dlgIoSeting.DoModal() == IDOK)
    {
        iPageMax = dlgIoSeting.GetIoPage();
        pIoDef->SetMaxPage(iPageMax);
        UpdatePageCombo();
    }
}

/**
 *  @brief   設定ボタン UI更新
 *  @param   [in out] コマンド ユーザー インターフェイス
 *  @retval  なし
 *  @note     
 */
void CIoSetWnd::OnUpdateIoSetting(CCmdUI *pCmdUI)
{
}


/**
 * @brief   IO選択コンボボックス変更
 * @param   [in] iIndex
 * @return  なし
 * @note    
 */
void CIoSetWnd::OnIoSelect(UINT nID)
{
    CMFCToolBarComboBoxButton* pSrcCombo = GetComboBox(nID);

    int iIndex = nID - ID_IO_SELECT;
    if(pSrcCombo == NULL)   {return;}
    if(iIndex != m_iIndex)  {return;}


    ASSERT_VALID(pSrcCombo);

    int iId;
    int iSel;
    iSel = pSrcCombo->GetCurSel();
    iId  = DwordptrToInt(pSrcCombo->GetItemData());
    m_iId = iId;
    
    if (iId == -1)
    {
        Clear();
    }
    else
    {

        std::shared_ptr<CObjectDef> pDef;
        pDef = THIS_APP->GetCurrentObjectDef().lock();

        if (!pDef)
        {
            m_vlstIo.EnableWindow(FALSE);
            Clear();
            return;
        }


        if ((pDef->GetObjectType() != VIEW_COMMON::E_PARTS)&&
            (pDef->GetObjectType() != VIEW_COMMON::E_FIELD))
        {
            m_vlstIo.EnableWindow(FALSE);
            Clear();
            return;
        }
        m_vlstIo.EnableWindow(TRUE);

        CPartsDef* pCtrl;
        pCtrl = dynamic_cast<CPartsDef*>(pDef.get());
        if (!pCtrl)
        {
            return;
        }
        auto pObj = pCtrl->GetObjectById(iId);

        if (!(pObj->GetType() & DT_SCRIPTBASE))
        {
            Clear();
            return;
        }

        auto pBase = std::dynamic_pointer_cast<CDrawingScriptBase>(pObj);
        if (!pBase)
        {
            Clear();
            return;
        }

        CIoRef* pRef = pBase->GetIoRef();
        SetIo(pRef);
    }
}

/**
 *  @brief   設定ボタン UI更新
 *  @param   [in out] コマンド ユーザー インターフェイス
 *  @retval  なし
 *  @note     
 */
void CIoSetWnd::OnUpdateIoSelect(CCmdUI *pCmdUI)
{
    //TODO 実装
}

/**
 * @brief   View更新通知
 * @param   [in]    iSelList    選択オブジェクトリスト
 * @param   [in]    pWnd	    ウインドウ	 	  
 * @return	常に０	
 * @note	Viewで選択変更があった時に通知する 
 * @exception   なし
 */
LRESULT CIoSetWnd::OnViewSelchange(WPARAM pSelList, LPARAM pItem)
{
//DB_PRINT(_T("OnViewSelchange 2 %d \n"), m_iIndex);
    if (!m_bRef) 
    {
        return 0;
    }
    
    if (!m_bCurrentSelMode) 
    {
        return 0;
    }

    //カレントモードのみ設定
    auto pSetObject = reinterpret_cast<std::vector<std::weak_ptr<CDrawingObject>>*>(pSelList);

    if (pSetObject->size() != 1)
    {
        Clear();
        return 0;
    }

    auto pObj = pSetObject->at(0).lock();

    if (!(pObj->GetType() & DT_SCRIPTBASE))
    {
        Clear();
        return 0;
    }

    auto pBase = std::dynamic_pointer_cast<CDrawingScriptBase>(pObj);
    if (!pBase)
    {
        Clear();
        return 0;
    }

//DB_PRINT(_T("OnViewSelchange 2 %d \n"), m_iIndex);
    auto pRef = pBase->GetIoRef();

    SetIo(pRef);

    return 0;
}

/**
 * @brief   View部品削除
 * @param   [in]    iSelList    選択オブジェクトリスト
 * @param   [in]    pWnd	    ウインドウ	 	  
 * @return	常に０	
 * @note	Viewで選択変更があった時に通知する 
 * @exception   なし
 */
LRESULT CIoSetWnd::OnPartsDelete(WPARAM uuidObjDef, LPARAM pItem)
{

    return 0;
}

/**
 *  @brief   IO設定
 *  @param   [in] pIoDef IO
 *  @retval  なし
 *  @note     
 */
void CIoSetWnd::SetIo(CIoDefBase* pIoDef)
{
    if (m_pIo != pIoDef)
    {
        if (!pIoDef->IsCreated())
        {
            m_pIo = NULL;
            m_vlstIo.SetItemCount( 0 );
            m_vlstIo.UpdateWindow();
            SetTitle(m_pIo);
            return;
        }
        m_pIo = pIoDef;
        m_pIo->InitList(&m_vlstIo);
        UpdatePageCombo();
        SetTitle(m_pIo);
    }
}


/**
 *  @brief   ウィンドウタイトル設定
 *  @param   なし
 *  @retval  なし
 *  @note     
 */
void CIoSetWnd::SetTitle(CIoDefBase* pIoDef)
{
    CString strIoWnd;
    if (m_iIndex == 0)
    {
        strIoWnd = GET_STR(STR_WND_IO_SET);
    }
    else if (m_iIndex == 1)
    {
        strIoWnd = GET_STR(STR_WND_IO_CONNECT);
    }
    else
    {
        strIoWnd.Format(_T("%s %d"), GET_STR(STR_WND_IO_REF),
                                        m_iIndex - 1);
    }

    if ((pIoDef != NULL) && 
        (m_iIndex != 0))
    {
        
        strIoWnd += _T("(");
        strIoWnd += pIoDef->GetName().c_str();
        strIoWnd += _T(")");
    }

    SetWindowText(strIoWnd);
}


/**
 *  @brief   クリア
 *  @param   なし
 *  @retval  なし
 *  @note     
 */
void CIoSetWnd::Clear()
{
    m_pIo = NULL; 
    m_vlstIo.SetItemCount( 0 );
    m_vlstIo.UpdateWindow();
    SetTitle(m_pIo);

    //m_pObjDef = NULL;
}

/**
 *  @brief モード変更
 *  @param [in] wParam   アプリケーションモード(EXEC_COMMON::E_APP_MODE)
 *  @param [in] lParam   オブジェクト定義      (CObjectDef)
 *  @retval
 *  @note
 */
LRESULT CIoSetWnd::OnModeChange(WPARAM wParam, LPARAM lParam)
{
    using namespace EXEC_COMMON;

    E_EXEC_STS  eSts;
    CObjectDef* pDef;

    eSts = static_cast<E_EXEC_STS>(wParam);
    pDef = reinterpret_cast<CObjectDef*>(lParam);

    if (eSts == EX_EDIT)
    {
        if (m_pIo)
        {
            m_pIo->ReleaseList(&m_vlstIo);
        }
        Clear();
    }
    return 0;
}
