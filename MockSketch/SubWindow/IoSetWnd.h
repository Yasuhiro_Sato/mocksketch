/**
 * @brief			IoSetWndヘッダーファイル
 * @file			IoSetWnd.h
 * @author			Yasuhiro Sato
 * @date			01-2-2009 10:36:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef __IO_SET_WND_
#define __IO_SET_WND_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "Utility/VirtualTreeCtrl/VirtualCheckList.h"
#include "ToolBar/CustomToolBar.h"
#include "SubWindow/CustomDocablePane.h"

/*---------------------------------------------------*/
/*  classes                                          */
/*---------------------------------------------------*/
class CIoDefBase;
class CObjectDef;



/**
 * @class   CIoSetWnd
 * @brief
 */
class CIoSetToolBar : public CCustomToolBar
{
public:
    CIoSetToolBar():CCustomToolBar(), 
        m_iIndex(0),
        m_bRef(false),
        m_bCurrentSelMode(false){ SetName(_T("CIoSetToolBar"));}

    void OnReset ();

	virtual void OnUpdateCmdUI(CFrameWnd* /*pTarget*/, BOOL bDisableIfNoHndler)
	{
		CCustomToolBar::OnUpdateCmdUI((CFrameWnd*) GetOwner(), bDisableIfNoHndler);
	}
    
	virtual BOOL AllowShowOnList() const { return FALSE; }

    virtual BOOL OnUserToolTip(CMFCToolBarButton* pButton,
                               CString& strTTText ) const;
    bool m_bRef;

    bool m_bCurrentSelMode;

    int  m_iIndex;
};



/**
 * @class   CIoSetWnd
 * @brief   IO設定ウインドウ 兼 参照ウインドウ
 */
class CIoSetWnd : public CCustomDocablePane
{

// コンストラクション
public:
    CIoSetWnd();

    virtual ~CIoSetWnd();

    //!< レイアウト調整
    void AdjustLayout();

    //!< 参照モード設定
    void SetRefMode();

    //!< カレントモード設定
    void SetCurrnetMode();

    //!< ウインドウインデックス設定
    void SetWindowIndex(int iIndex);


// 属性
public:
    void SetVSDotNetLook(BOOL bSet)
    {
    }

    //!< IOリスト
    CVirtualCheckList m_vlstIo;


protected:
    //!< ウインドウインデックス
    int m_iIndex;

    CFont m_fntPropList;

    //!< ツールバー
    CIoSetToolBar m_wndToolBar;

    //!< 表示IO
    CIoDefBase* m_pIo;

    //!<ページ表示コンボボックス
    CMFCToolBarComboBoxButton* m_psPageComo;

    //!< フォント高さ
    CSize m_szBaseUnits;

    //!< 参照モード
    bool m_bRef;

    //!<  カレントモード(選択中のオブジェクトのIOを表示)
    bool m_bCurrentSelMode;

    //!< 
    //CObjectDef*          m_pObjDef;

    //!< 選択オブジェクト
    int m_iId;

    //!< 選択ページ
    int m_iPage;

// 実装
protected:
    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnSetFocus(CWnd* pOldWnd);
    afx_msg void OnSettingChange(UINT uFlags, LPCTSTR lpszSection);
    DECLARE_MESSAGE_MAP()

    void SetListFont();

    //!< コンボボックス取得
    CMFCToolBarComboBoxButton* GetComboBox(UINT uiCmd);

    //!< エディット完了
    afx_msg void OnLvnEndlabeleditVlst(NMHDR *pNMHDR, LRESULT *pResult);

    //!< View生成通知
    afx_msg LRESULT OnViewCreate(WPARAM iType, LPARAM pMessage);

    //!< View活性化通知
    afx_msg  LRESULT OnViewActivate(WPARAM pMessage, LPARAM lParam);

    //!< Viewクリア
    afx_msg LRESULT OnViewClear(WPARAM pMessage, LPARAM lParam);

    //!< View選択変更
    afx_msg LRESULT OnViewSelchange(WPARAM iType, LPARAM pItem);

    //!< View部品削除
    afx_msg LRESULT OnPartsDelete(WPARAM uuidObjDef, LPARAM pItem);

    //!<モード変更
    afx_msg LRESULT OnModeChange(WPARAM wParam, LPARAM lParam);

    //!< 列挿入
    void InsertColumn(int iCol, StdString strItem);

    virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);

    virtual BOOL PreTranslateMessage(MSG* pMsg);

    //!< IO設定
    void SetIo(CIoDefBase* pIoDef);

    //!< クリア
    void Clear();

    //!< Io選択コンボボックス更新
    void UpdateIoSelCombo();

    //!< ページコンボボックス更新
    void UpdatePageCombo();

    //!< ウィンドウタイトル設定
    void SetTitle(CIoDefBase* pIoDef);


public:
    afx_msg void OnIoSetting();
    afx_msg void OnUpdateIoSetting(CCmdUI *pCmdUI);

    afx_msg void OnChangePageComboBox(UINT nId);

    afx_msg void OnUpdatePageComboBox(CCmdUI *pCmdUI);

    afx_msg void OnIoSelect(UINT nId);
    afx_msg void OnUpdateIoSelect(CCmdUI *pCmdUI);

};

#endif //__LAYER_WND_