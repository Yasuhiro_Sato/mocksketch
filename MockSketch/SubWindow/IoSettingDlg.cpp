/**
 * @brief			IoSettingDlg実装ファイル
 * @file			IoSettingDlg.cpp
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MockSketch.h"
#include "./IoSettingDlg.h"
#include "System/CSystem.h"
#include "Utility/CUtility.h"



/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
IMPLEMENT_DYNAMIC(CIoSettingDlg, CDialog)


BEGIN_MESSAGE_MAP(CIoSettingDlg, CDialog)
    ON_BN_CLICKED(IDOK, &CIoSettingDlg::OnBnClickedOk)
    ON_BN_CLICKED(IDCANCEL, &CIoSettingDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


/**
  *  @brief  コンストラクター.
  *  @param  なし
  *  @return なし
  */
CIoSettingDlg::CIoSettingDlg(CWnd* pParent /*=NULL*/)
    : CDialog(CIoSettingDlg::IDD, pParent)
{
}

/**
  *  @brief  デストラクタ.
  *  @param  なし
  *  @return なし
  */
CIoSettingDlg::~CIoSettingDlg()
{
}

/**
 *  @brief  フレームワークの自動的なデータ交換
 *  @param  pDX     CDataExchange オブジェクトへのポインタ
 *  @retval なし     
 *  @note   UpdateData メンバ関数から呼び出されます。
 */
void CIoSettingDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_ED_PAGE_MAX, m_edPageMax);
    DDX_Control(pDX, IDC_SPN_PAGEMAX, m_spnPageMax);
}



/**
 *  @brief  ウインドウの初期化処理
 *  @param  なし
 *  @retval 常にTRUE     
 *  @note   
 */
BOOL CIoSettingDlg::OnInitDialog()
{
    CDialog::OnInitDialog();

    SetWindowText(GET_STR(STR_DLG_IOSETTING));
    GetDlgItem(IDCANCEL)       ->SetWindowText(GET_STR(STR_DLG_CANCEL));
    GetDlgItem(IDC_ST_PAGE_MAX)->SetWindowText(GET_STR(STR_DLG_IOSETTING_PAGEMAX));

    //スピンボタン
    m_spnPageMax.SetBuddy(&m_edPageMax);

    m_edPageMax.SetRange ( 1, 255);
    m_spnPageMax.SetRange32( 1, 255);

    m_edPageMax.SetVal(m_iPageMax);

    return TRUE;
}


/**
 *  @brief  ページ最大数設定
 *  @param  ページ最大数
 *  @retval なし
 *  @note   
 */
void CIoSettingDlg::SetIoPage(int iPageMax)
{
    m_iPageMax = iPageMax;
}

/**
 *  @brief  ページ最大数取得
 *  @param  なし
 *  @retval ページ最大数
 *  @note   
 */
int  CIoSettingDlg::GetIoPage()
{
    return m_iPageMax;
}

/**
 *  @brief  OKボタン押下
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CIoSettingDlg::OnBnClickedOk()
{
    m_iPageMax = m_edPageMax.GetVal();
    OnOK();
}

/**
 *  @brief  キャンセルボタン押下
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CIoSettingDlg::OnBnClickedCancel()
{
    OnCancel();
}

/**
 *  @brief  メッセージフィルタ処理
 *  @param  [in] pMsg   ウインドウメッセージ
 *  @retval true これ以上の処理不要 / false 通常処理
 *  @note   
 */
BOOL CIoSettingDlg::PreTranslateMessage(MSG* pMsg)
{
    return CDialog::PreTranslateMessage(pMsg);
}

