/**
 * @brief			IoSettingDlgヘッダーファイル
 * @file			IoSettingDlg.h
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#pragma once
#include "afxwin.h"
#include "Utility/IntEdit.h"
#include "afxcmn.h"


// CIoSettingDlg ダイアログ

class CIoSettingDlg : public CDialog
{
	DECLARE_DYNAMIC(CIoSettingDlg)

public:
    //!< コンストラクター
    CIoSettingDlg(CWnd* pParent = NULL);   // 標準コンストラクタ

    //!< デストラクター
    virtual ~CIoSettingDlg();

    //!< ページ最大数設定
    void SetIoPage(int iPageMax);

    //!< ページ最大数取得
    int  GetIoPage();

    // ダイアログ データ
    enum { IDD = IDD_DLG_IO_SETTING };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート


    //void DropFiles(HDROP hDropInfo);

	DECLARE_MESSAGE_MAP()
public:
    virtual BOOL OnInitDialog();
    afx_msg void OnBnClickedOk();
    afx_msg void OnBnClickedCancel();
    virtual BOOL PreTranslateMessage(MSG* pMsg);

protected:
    //!< 最大ページ数
    int         m_iPageMax;

public:
    //!< 最大ページ数エディット
    CIntEdit        m_edPageMax;

    //!< 最大ページ数スピン
    CSpinButtonCtrl m_spnPageMax;
};
