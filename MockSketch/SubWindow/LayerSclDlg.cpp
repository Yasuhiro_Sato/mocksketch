/**
 * @brief			COutputList実装ファイル
 * @file		    COutputList.cpp
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/

#include "stdafx.h"
#include "MockSketch.h"
#include "./LayerSclDlg.h"
#include "System/CSystem.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
BEGIN_MESSAGE_MAP(CLayerSclDlg, CDialog)
    ON_BN_CLICKED(IDOK, &CLayerSclDlg::OnBnClickedOk)
    ON_BN_CLICKED(IDCANCEL, &CLayerSclDlg::OnBnClickedCancel)
    ON_BN_CLICKED(IDC_CK_UNVISIBLE, &CLayerSclDlg::OnBnClickedCkUnvisible)
    ON_BN_CLICKED(IDC_CK_SCL, &CLayerSclDlg::OnBnClickedCkScl)
    ON_BN_CLICKED(IDC_CK_ZERO, &CLayerSclDlg::OnBnClickedCkZero)
END_MESSAGE_MAP()


IMPLEMENT_DYNAMIC(CLayerSclDlg, CDialog)

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
 CLayerSclDlg::CLayerSclDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLayerSclDlg::IDD, pParent)
{
    bUnVisible  = true;
    bSameScl    = true;
    bIgnoreZero = true;

}

/**
 *  @brief  デストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
CLayerSclDlg::~CLayerSclDlg()
{
}

/**
 *  @brief  フレームワークの自動的なデータ交換
 *  @param  pDX     CDataExchange オブジェクトへのポインタ
 *  @retval なし     
 *  @note   UpdateData メンバ関数から呼び出されます。
 */
void CLayerSclDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_CB_SCL1, m_cbScl1);
    DDX_Control(pDX, IDC_CB_SCL2, m_cbScl2);
}




/**
 *  @brief  OKボタン押下
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CLayerSclDlg::OnBnClickedOk()
{
    bUnVisible  = (IsDlgButtonChecked(IDC_CK_UNVISIBLE) == BST_CHECKED); 
    bSameScl    = (IsDlgButtonChecked(IDC_CK_SCL) == BST_CHECKED); 
    bIgnoreZero = (IsDlgButtonChecked(IDC_CK_ZERO) == BST_CHECKED); 

    double dScl1;
    double dScl2;
    CString strScl1;
    CString strScl2;


    m_cbScl1.GetWindowText(strScl1);
    m_cbScl2.GetWindowText(strScl2);

    dScl1 = _tstof(strScl1);
    dScl2 = _tstof(strScl2);

    if (dScl1 < NEAR_ZERO)
    {
        m_cbScl1.SetWindowText(_T("1"));
        AfxMessageBox(GET_STR(STR_LAYER_INVALID_SCL));
        return;
    }

    if (dScl2 < NEAR_ZERO)
    {
        m_cbScl2.SetWindowText(_T("1"));
        AfxMessageBox(GET_STR(STR_LAYER_INVALID_SCL));
        return;
    }

    dScl = dScl1 / dScl2;
    if (dScl < NEAR_ZERO)
    {
        AfxMessageBox(GET_STR(STR_LAYER_INVALID_SCL));
        return;
    }

    OnOK();
}

/**
 *  @brief  キャンセルボタン押下
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CLayerSclDlg::OnBnClickedCancel()
{
    OnCancel();
}

/**
 *  @brief  「非表示のレイヤーは変更しない」チェックボックス押下
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CLayerSclDlg::OnBnClickedCkUnvisible()
{
}

/**
 *  @brief  「倍率が異なるレイヤーは変更しない」チェックボックス押下
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CLayerSclDlg::OnBnClickedCkScl()
{
}

/**
 *  @brief  「０番レイヤーは変更しない」チェックボックス押下
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CLayerSclDlg::OnBnClickedCkZero()
{
}

/**
 *  @brief  ウインドウの初期化処理
 *  @param  なし
 *  @retval 常にTRUE     
 *  @note   
 */
BOOL CLayerSclDlg::OnInitDialog()
{
    CDialog::OnInitDialog();

    SetWindowText(GET_STR(STR_MNU_LAYER_SCL));
    GetDlgItem(IDC_CK_UNVISIBLE)->SetWindowText(GET_STR(STR_LAYER_NO_SEL_UNVISIVLE));
    GetDlgItem(IDC_CK_SCL)->SetWindowText(GET_STR(STR_LAYER_NO_SEL_SCL));
    GetDlgItem(IDC_CK_ZERO)->SetWindowText(GET_STR(STR_LAYER_SEL_ZERO));
    GetDlgItem(IDC_ST_SCL)->SetWindowText(GET_STR(STR_LAYER_SCL));

    CheckDlgButton(IDC_CK_UNVISIBLE ,BST_CHECKED); 
    CheckDlgButton(IDC_CK_SCL       ,BST_CHECKED); 
    CheckDlgButton(IDC_CK_ZERO      ,BST_CHECKED); 

    CComboBox* pCb[2];
    pCb[0] = &m_cbScl1;
    pCb[1] = &m_cbScl2;

    for(int iNo = 0; iNo < 2; iNo++)
    {
        pCb[iNo]->AddString(_T("1"));
        pCb[iNo]->AddString(_T("2"));
        pCb[iNo]->AddString(_T("5"));
        pCb[iNo]->AddString(_T("10"));
        pCb[iNo]->AddString(_T("20"));
        pCb[iNo]->AddString(_T("50"));
        pCb[iNo]->AddString(_T("100"));
        pCb[iNo]->AddString(_T("200"));
        pCb[iNo]->AddString(_T("500"));
        pCb[iNo]->AddString(_T("1000"));
        pCb[iNo]->AddString(_T("2000"));
        pCb[iNo]->AddString(_T("5000"));

        pCb[iNo]->SetCurSel(0);
    }

    return TRUE; 
}
