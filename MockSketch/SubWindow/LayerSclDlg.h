/**
 * @brief			CLayerSclDlgヘッダーファイル
 * @file			CLayerSclDlg.h
 * @author			Yasuhiro Sato
 * @date			07-8-2010 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *	
 *
 * $
 * $
 * 
 */

#pragma once
#include "afxwin.h"

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/

/**
 * @class   CLayerSclDlg
 * @brief                        
 */
class CLayerSclDlg : public CDialog
{
	DECLARE_DYNAMIC(CLayerSclDlg)

public:
	CLayerSclDlg(CWnd* pParent = NULL);   // 標準コンストラクタ
	virtual ~CLayerSclDlg();

public:
    bool bUnVisible;
    bool bSameScl;
    bool bIgnoreZero;
    double dScl;

// ダイアログ データ
	enum { IDD = IDD_DLG_LAYER_SCL };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート

	DECLARE_MESSAGE_MAP()
public:
    afx_msg void OnBnClickedOk();
    afx_msg void OnBnClickedCancel();
    afx_msg void OnBnClickedCkUnvisible();
    afx_msg void OnBnClickedCkScl();
    afx_msg void OnBnClickedCkZero();
    virtual BOOL OnInitDialog();
    CComboBox m_cbScl1;
    CComboBox m_cbScl2;
};
