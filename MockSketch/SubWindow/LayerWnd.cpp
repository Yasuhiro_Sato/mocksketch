/**
 * @brief			CLayerWnd実装ファイル
 * @file			CLayerWnd.cpp
 * @author			Yasuhiro Sato
 * @date			01-2-2009 10:36:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "Resource.h"
#include "MainFrm.h"
#include "MockSketch.h"

#include "./LayerWnd.h"
#include "./LayerSclDlg.h"
#include "View/CLayer.h"
#include "DefinitionObject/View/MockSketchDoc.h"
#include "DefinitionObject/View/MockSketchView.h"
#include "DefinitionObject/View/CDrawingView.h"
#include "DefinitionObject/CPartsDef.h"
#include "Script/CScriptEngine.h"
#include "View/CLayer.h"
#include "System/CSystem.h"
#include "Utility/CUtility.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

/*---------------------------------------------------*/
/* Static variables                                  */
/*---------------------------------------------------*/


/*---------------------------------------------------*/
/* MACROS                                            */
/*---------------------------------------------------*/
BEGIN_MESSAGE_MAP(CLayerWnd, CDockablePane)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_WM_SETTINGCHANGE()
    ON_NOTIFY(LVN_ENDLABELEDIT, IDC_LST_VIRTUAL_IO_SET, OnLvnEndlabeleditVlst)
    ON_MESSAGE(WM_VIEW_CREATE,       OnViewCreate)
    ON_MESSAGE(WM_VIEW_ACTIVE, OnViewActivate)
    ON_MESSAGE(WM_VIEW_CLEAR_ALL, OnViewClear)
    ON_MESSAGE(WM_MODE_CHANGE, OnModeChange)
    ON_COMMAND(ID_MNU_LAYER_ADD, &CLayerWnd::OnMnuLayerAdd)
    ON_UPDATE_COMMAND_UI(ID_MNU_LAYER_ADD, &CLayerWnd::OnUpdateMnuLayerAdd)
    ON_COMMAND(ID_MNU_LAYER_DEL, &CLayerWnd::OnMnuLayerDel)
    ON_UPDATE_COMMAND_UI(ID_MNU_LAYER_DEL, &CLayerWnd::OnUpdateMnuLayerDel)
    ON_COMMAND(ID_MNU_LAYER_SCL, &CLayerWnd::OnMnuLayerScl)
    ON_UPDATE_COMMAND_UI(ID_MNU_LAYER_SCL, &CLayerWnd::OnUpdateMnuLayerScl)
    ON_WM_CONTEXTMENU()
END_MESSAGE_MAP()

/**
 * @brief   コンストラクター
 * @param   なし
 * @return	なし
 * @note	 
 */
CLayerWnd::CLayerWnd()
{
    m_pCtrl = NULL;
}

/**
 * @brief   デストラクター
 * @param   なし
 * @return	なし
 * @note	 
 */
CLayerWnd::~CLayerWnd()
{
}

/**
 * @brief   レイアウト調整
 * @param   なし
 * @return	なし
 * @note	 
 */
void CLayerWnd::AdjustLayout()
{
	if (GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect rectClient;

    GetClientRect(rectClient);


    STD_ASSERT(m_vlstLayer.m_hWnd != NULL);
    int cyTlb = m_wndToolBar.CalcFixedLayout(FALSE, TRUE).cy;

    m_wndToolBar.SetWindowPos(NULL, rectClient.left, 
                                    rectClient.top, 
                                    rectClient.Width(), 
                                    cyTlb, SWP_NOACTIVATE | SWP_NOZORDER);

    m_vlstLayer.SetWindowPos(NULL, rectClient.left,
                                   rectClient.top + cyTlb, 
                                   rectClient.Width(), 
                                   rectClient.Height() - cyTlb, SWP_NOACTIVATE | SWP_NOZORDER);

    //m_vlstLayer.RedrawWindow();
}

/**
 * @brief   レイアウト調整
 * @param   なし
 * @return	なし
 * @note	 
 */
int CLayerWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDockablePane::OnCreate(lpCreateStruct) == -1)
    {
		return -1;
    }


	m_wndToolBar.Create(this, AFX_DEFAULT_TOOLBAR_STYLE, IDR_LAYERBAR);
	m_wndToolBar.LoadToolBar(IDR_LAYERBAR, 0, 0, TRUE /* ロックされています*/);
	m_wndToolBar.CleanUpLockedImages();
	m_wndToolBar.LoadBitmap(IDR_LAYERBAR, 0, 0, TRUE /* ロックされました*/);

	m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() | CBRS_TOOLTIPS | CBRS_FLYBY);
	m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() & ~(CBRS_GRIPPER | CBRS_SIZE_DYNAMIC | CBRS_BORDER_TOP | CBRS_BORDER_BOTTOM | CBRS_BORDER_LEFT | CBRS_BORDER_RIGHT));
	m_wndToolBar.SetOwner(this);

    // すべてのコマンドが、親フレーム経由ではなくこのコントロール経由で渡されます:
	m_wndToolBar.SetRouteCommandsViaFrame(FALSE);


    m_vlstLayer.Create(LVS_REPORT | LVS_ALIGNLEFT | LVS_OWNERDATA | WS_CHILD | 
                       WS_BORDER | WS_VISIBLE |WS_TABSTOP, CRect(), this, IDC_LST_VIRTUAL_IO_SET);



    //生成及び調整
	AdjustLayout();


    //スタイル設定
    ListView_SetExtendedListViewStyleEx(m_vlstLayer.m_hWnd, LVS_EX_FULLROWSELECT, LVS_EX_FULLROWSELECT);
    ListView_SetExtendedListViewStyleEx(m_vlstLayer.m_hWnd, LVS_EX_GRIDLINES, LVS_EX_GRIDLINES);

    //---------------------
    // ヘッダー部の設定
    //---------------------
    CString sTemp;


    m_vlstLayer.InsertColumn ( LAYER_CURRENT,   _T("No"),  LVCFMT_LEFT, 50);
    m_vlstLayer.InsertColumn ( LAYER_ENABLE,     _T(""),  LVCFMT_LEFT, 25);
    m_vlstLayer.InsertColumn ( LAYER_VISIBLE,    _T(""),  LVCFMT_LEFT, 25);
    m_vlstLayer.InsertColumn ( LAYER_USE_COLOR,  _T(""),  LVCFMT_LEFT, 25);
    m_vlstLayer.InsertColumn ( LAYER_COLOR, GET_SYS_STR(STR_LAYER_COLOR).c_str(), LVCFMT_LEFT, 50);
    m_vlstLayer.InsertColumn ( LAYER_NAME,  GET_SYS_STR(STR_LAYER_NAME).c_str(),  LVCFMT_LEFT, 75);
    m_vlstLayer.InsertColumn ( LAYER_SCL,   GET_SYS_STR(STR_LAYER_SCL).c_str(),   LVCFMT_LEFT, 40);
	m_vlstLayer.InsertColumn (LAYER_USE_BOUND, _T(""), LVCFMT_LEFT, 25);


    //-----------
    //項目設定
    //-----------
    CVirtualCheckList::COL_SET ColSet;

    //カレントレイヤー
    ColSet.iStyle = DFCS_BUTTONRADIO;
    ColSet.bDraw = true;            //ボタン描画
    ColSet.eEditType = CVirtualCheckList::NONE;     //編集不可
    m_vlstLayer.SetCol(LAYER_CURRENT, ColSet);

    //カレントレイヤー
    ColSet.bDraw = false;            //ボタン描画
    ColSet.eEditType = CVirtualCheckList::ALLOW;       //編集可
    m_vlstLayer.SetCol(LAYER_NAME, ColSet);

    //操作
    ColSet.iStyle = DFCS_BUTTONCHECK;
    ColSet.bDraw  = true;           //ボタン描画
    ColSet.eEditType = CVirtualCheckList::NONE;     //編集不可
    m_vlstLayer.SetCol(LAYER_ENABLE, ColSet);

    //表示
    m_vlstLayer.SetCol(LAYER_VISIBLE, ColSet);

    //色使用
    
    m_vlstLayer.SetCol(LAYER_USE_COLOR, ColSet);

    //色
    ColSet.iStyle = DFCS_BUTTONPUSH;
    ColSet.bColor  = true;           //背景色使用
    m_vlstLayer.SetCol(LAYER_COLOR, ColSet);

    //倍率
    ColSet.bDraw = false;           //ボタン不描画
    ColSet.bColor  = false;         //ボタン描画
    ColSet.eEditType = CVirtualCheckList::ALLOW;       //編集可
    m_vlstLayer.SetCol(LAYER_SCL, ColSet);


	// バウンディングボックス使用
	ColSet.iStyle = DFCS_BUTTONCHECK;
	ColSet.bDraw = true;           //ボタン描画
	ColSet.eEditType = CVirtualCheckList::ALLOW;     //編集不可
	m_vlstLayer.SetCol(LAYER_USE_BOUND, ColSet);


    //コールバック関数設定
    m_vlstLayer.SetFunc(Callbackfunc, CallbackChek, this);


    //-------------------------------
    // ヘッダ用イメージリストの初期化
    //-------------------------------
    CHeaderCtrl  *headerP = NULL;
    bool bImageError = false;
    HDITEM       hdItem;
    HICON        hIcon = NULL;

    if (!m_imageHeader.Create(16, 16, ILC_COLOR24, 3, 1))
    {
        bImageError = true;
    };

    m_imageHeader.SetBkColor(GetSysColor(COLOR_3DFACE));
    STD_ASSERT(!bImageError);

    hIcon = AfxGetApp()->LoadIcon(IDI_DRAW);
    STD_ASSERT(hIcon != NULL);
    m_imageHeader.Add(hIcon);

    hIcon = AfxGetApp()->LoadIcon(IDI_VISIBLE);
    STD_ASSERT(hIcon != NULL);
    m_imageHeader.Add(hIcon);

    hIcon = AfxGetApp()->LoadIcon(IDI_LAYER);
    STD_ASSERT(hIcon != NULL);
    m_imageHeader.Add(hIcon);

	hIcon = AfxGetApp()->LoadIcon(IDI_BOUND);
	STD_ASSERT(hIcon != NULL);
	m_imageHeader.Add(hIcon);
	
	// イメージリストをヘッダコントロールにセット
    headerP = m_vlstLayer.GetHeaderCtrl();
    STD_ASSERT(headerP != NULL);

    headerP->SetImageList(&m_imageHeader);

    hdItem.mask = HDI_IMAGE | HDI_FORMAT;
    hdItem.fmt = HDF_LEFT | HDF_IMAGE | HDF_STRING;

    hdItem.iImage = 0;
    headerP->SetItem( LAYER_ENABLE, &hdItem);   //操作可・不可

 
    hdItem.iImage = 1;
    headerP->SetItem( LAYER_VISIBLE, &hdItem);  //表示・非表示

    hdItem.iImage = 2;
    headerP->SetItem( LAYER_USE_COLOR, &hdItem);  //表示・非表示

	hdItem.iImage = 3;
	headerP->SetItem(LAYER_USE_BOUND, &hdItem);  //表示・非表示
	
												 //仮想リストの行数を設定する
    m_vlstLayer.SetItemCount( 0);

	return 0;
}

/**
 *  @brief ウインドウサイズ変更
 *  @param [in] nType   要求されるサイズ変更のタイプ  (SIZE_MAXIMIZED....)
 *  @param [in] cx      クライアント領域の新しい幅 
 *  @param [in] cy      クライアント領域の新しい高さ
 *  @retval           なし
 *  @note
 */
void CLayerWnd::OnSize(UINT nType, int cx, int cy)
{
	CDockablePane::OnSize(nType, cx, cy);
	AdjustLayout();
}



/**
 *  @brief  ウインドウフォーカス設定
 *  @param  [out] pOldWnd  今までフォーカスがあったウインドウ
 *  @retval なし
 *  @note   
 */
void CLayerWnd::OnSetFocus(CWnd* pOldWnd)
{
	CDockablePane::OnSetFocus(pOldWnd);
}


/**
 *  @brief  設定変更
 *  @param  [in] uFlags  変更されたシステム パラメータを示すフラグ
 *  @param  [in] lpszSection   変更されたセクション名を示す文字列へのポインタ
 *  @retval なし
 *  @note   SystemParametersInfo参照
 */
void CLayerWnd::OnSettingChange(UINT uFlags, LPCTSTR lpszSection)
{
	CDockablePane::OnSettingChange(uFlags, lpszSection);
	SetListFont();
}

/**
 *  @brief  フォント変更
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CLayerWnd::SetListFont()
{
    /*
	::DeleteObject(m_fntPropList.Detach());


	LOGFONT lf;
	afxGlobalData.fontRegular.GetLogFont(&lf);

	NONCLIENTMETRICS info;
	info.cbSize = sizeof(info);

	afxGlobalData.GetNonClientMetrics(info);

	lf.lfHeight = info.lfMenuFont.lfHeight;
	lf.lfWeight = info.lfMenuFont.lfWeight;
	lf.lfItalic = info.lfMenuFont.lfItalic;


	m_fntPropList.CreateFontIndirect(&lf);
    */
}


/**
 * @brief   描画コールバック
 * @param   [in]  pParent 呼び出し元         
 * @param   [in]  nRow    データを取得する行         
 * @param   [in]  nCol    データを取得する列        
 * @param   [out] pData   文字列        
 * @param   [out] pbCheck チェックボックス         
 * @retval  なし
 * @note	仮想リスト用表示関数
 */
void CLayerWnd::Callbackfunc (LPVOID pParent, int nRow, int nCol, _TCHAR* pData, bool* pbCheck)
{
    CLayerWnd* pDlg = (CLayerWnd*)pParent;

    //描画処理
    pDlg->SetText( nRow, nCol, pData,  pbCheck);
}

/**
 * @brief   描画処理
 * @param   [in]  nRow    データを取得する行         
 * @param   [in]  nCol    データを取得する列        
 * @param   [out] pData   文字列        
 * @param   [out] pbCheck チェックボックス         
 * @retval  なし
 * @note	
 */
void CLayerWnd::SetText( int nRow, int nCol, _TCHAR* pData, bool* pbCheck) 
{
    CLayer*                 pLayer  = NULL;
    long                    lSize;
    //long                    iIndex;
    bool                    bCheck = false;
    CString                 sItem;

    if (m_pCtrl == NULL)
    {
        return;
    }

    lSize = (long)m_pCtrl->GetLayerMax();
    if (nRow >=  lSize)
    {
        return;
    }

    pLayer = m_pCtrl->GetLayer(nRow);

    if (nCol == LAYER_CURRENT)
    {
       sItem.Format(_T("%03d"), nRow);

       if (m_pCtrl->GetCurrentLayerId() == nRow)
       {
           bCheck = true;
       }
    }
    else if (nCol == LAYER_NAME)
    {
        sItem = pLayer->strName.c_str();
    }
    else if (nCol == LAYER_ENABLE)
    {
        bCheck = pLayer->bEnable;
    }
    else if (nCol == LAYER_VISIBLE)
    {
        bCheck = pLayer->bVisible;
    }
    else if (nCol == LAYER_USE_COLOR)
    {
        bCheck = pLayer->bUseLayerColor;
    }
    else if (nCol == LAYER_COLOR)
    {
        sItem.Format(_T("%d"),pLayer->crLayer);
    }
    else if (nCol == LAYER_SCL)
    {
        sItem.Format(_T("%02.2f"), pLayer->dScl);
    }
	else if (nCol == LAYER_USE_BOUND)
	{
		bCheck = pLayer->bUseBoundingBox;
	}

    *pbCheck = bCheck;
    lstrcpy(pData, sItem);
}

/**
 * @brief   チェックボックスコールバック
 * @param   [in]  nRow    選択時の行         
 * @param   [in]  nCol    選択時の列        
 * @retval  なし
 * @note	仮想リストボタンチェック時に呼び出し
 */
void CLayerWnd::CallbackChek   (LPVOID pParent, int nRow, int nCol)
{

    CLayer*                 pLayer  = NULL;
    long                    lSize;

    CLayerWnd* pDlg = (CLayerWnd*)pParent;
    bool bRedraw = false;

    lSize = long(pDlg->m_pCtrl->GetLayerMax());
    if (nRow >=  lSize)
    {
        return;
    }

    if (pDlg->m_pCtrl == NULL)
    {
        return;
    }

    pLayer = pDlg->m_pCtrl->GetLayer(nRow);
    if (pLayer == NULL)
    {
        return;
    }

    if (nCol == LAYER_CURRENT)
    {
        int iOldRow = pDlg->m_pCtrl->GetCurrentLayerId();
        CLayer* pOldLayer = pDlg->m_pCtrl->GetLayer(iOldRow);

        pDlg->m_pCtrl->SetCurrentLayerId( nRow );

        pDlg->m_vlstLayer.RedrawCol(iOldRow, iOldRow, nCol, CVirtualCheckList::RD_CHECK);
        pDlg->m_vlstLayer.RedrawCol(nRow, nRow, nCol, CVirtualCheckList::RD_CHECK);


        if (pOldLayer)
        {
            if (fabs( pOldLayer->dScl - pLayer->dScl) > NEAR_ZERO)
            {
                bRedraw = true;
            }
        }
    }
    else if(nCol == LAYER_ENABLE)
    {
        //On/Off反転
        pLayer->bEnable = (!pLayer->bEnable);
        bRedraw = true;
    }
    else if (nCol == LAYER_VISIBLE)
    {
        //On/Off反転
        pLayer->bVisible = (!pLayer->bVisible);
        pDlg->m_pCtrl->AddGroupChg();
        bRedraw = true;
    }
    else if (nCol == LAYER_USE_COLOR)
    {
        //On/Off反転
        pLayer->bUseLayerColor = (!pLayer->bUseLayerColor);
        bRedraw = true;
    }
    else if (nCol == LAYER_COLOR)
    {
        //現在の位置にカラーピックを表示する
         CColorDialog dlgColor(pLayer->crLayer,        //デフォルトカラー
                               0,           //カスタマイズフラグ
                               pDlg);       //親ウィンドウ

        //dlgColor.SetCurrentColor(pLayer->crLayer);
        if (dlgColor.DoModal() == IDOK )
        {
            pLayer->crLayer = dlgColor.GetColor();
        }
        pDlg->m_vlstLayer.RedrawCol(nRow, nRow, nCol, CVirtualCheckList::RD_TEXT);
        bRedraw = true;
    }
	else if (nCol == LAYER_USE_BOUND)
	{
		//On/Off反転
		pLayer->bUseBoundingBox = (!pLayer->bUseBoundingBox);
		pDlg->m_pCtrl->AddGroupChg();
		bRedraw = true;
	}

    if (bRedraw)
    {
        //再描画
        pDlg->m_pCtrl->Redraw();
    }
}

/**
 *  @brief   ラベル編集終了
 *  @param    [in]  pNMHDR 通知メッセージへのポインタ
 *  @param    [out] pResult 
 *  @retval   なし
 *  @note	
 */
void CLayerWnd::OnLvnEndlabeleditVlst(NMHDR *pNMHDR, LRESULT *pResult)
{
    NMLVDISPINFO *pDispInfo = reinterpret_cast<NMLVDISPINFO*>(pNMHDR);

    *pResult = 0;
	if(pDispInfo->item.pszText == NULL)
	{
        return;
    }

    int nRow = pDispInfo->item.iItem;
    int nCol = pDispInfo->item.iSubItem;

    if (m_pCtrl == NULL)
    {
        return;
    }

    StdString strTmp;
    strTmp = pDispInfo->item.pszText;

    if (nCol == LAYER_NAME)
    {
        m_pCtrl->GetLayer(nRow)->strName = strTmp;
    }
    else if (nCol == LAYER_SCL)
    {
        bool bChange = false;
        double dScl = 1.0;
        double dOldScl = m_pCtrl->GetLayer(nRow)->dScl;
        bool bRet = THIS_APP->GetScriptEngine()->ExecuteString(&dScl, strTmp);

        if (dScl > NEAR_ZERO)
        {
            m_pCtrl->GetLayer(nRow)->dScl = dScl;
            if( fabs(dScl - dOldScl) > NEAR_ZERO)
            {
                m_pCtrl->Redraw();
            }
        }
    }
}

/**
 * @brief   View生成通知
 * @param   [in]    iWin	    View種別ID
 * @param   [in]    pWnd	    ウインドウ	 	  
 * @return	常に０	
 * @note	View生成完了後に各ウインドウに通知する 
 * @exception   なし
 */
LRESULT CLayerWnd::OnViewCreate(WPARAM iType, LPARAM pMessage)
{
    //TODO:他Viewに対応
    if ((iType == VIEW_COMMON::E_PARTS)||
        (iType == VIEW_COMMON::E_FIELD))

    {
        if (pMessage == 0)
        {
            m_pCtrl = 0;
            m_vlstLayer.SetItemCount( 0);
        }
        else
        {
            m_vlstLayer.EnableWindow(TRUE);
            m_pCtrl = reinterpret_cast<CPartsDef*>(pMessage);
            m_vlstLayer.SetItemCount( m_pCtrl->GetLayerMax());
        }
        RedrawWindow();
    }
    return 0;
}

/**
 *  @brief  ウインドウプロシジャ
 *  @param    [in]  message  処理される Windows メッセージ
 *  @param    [in]  wParam   メッセージの処理で使う付加情報1
 *  @param    [in]  lParam   メッセージの処理で使う付加情報2
 *  @retval   メッセージに依存する値
 *  @note   
 */
LRESULT CLayerWnd::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
    //!< 避難的対応 恐らくリフレクションの関連と思われるが
    //!< 同じメッセージが CWnd と CBasePainで発生する
    if (message == WM_NOTIFY)
    {
        if (wParam == IDC_LST_VIRTUAL_IO_SET)
        {
            return CWnd::WindowProc(message, wParam, lParam);

        }
    }

    return CDockablePane::WindowProc(message, wParam, lParam);
}

/**
 * @brief    View活性化を通知
 * @param    [in] pMessage タブタイトル文字
 * @param    [in] lParam   0:MockSletchView 1:EditView
 * @retval   なし
 * @note	
 */
LRESULT CLayerWnd::OnViewActivate(WPARAM pMessage, LPARAM lParam)
{
    if (lParam != 0)
    {
        m_vlstLayer.EnableWindow(FALSE);
        return 0;
    }

    m_vlstLayer.EnableWindow(TRUE);
    StdString* pTitle = reinterpret_cast<StdString*>(pMessage);


    CWnd* pWnd = THIS_APP->GetView(*pTitle);
    CMockSketchView* pView = reinterpret_cast<CMockSketchView*>(pWnd);
    if (pView == NULL)
    {
        STD_DBG(_T("CMockSketchView was deleted."));
        return 0;
    }


    CMockSketchDoc* pDoc = pView->GetDocument();
    std::shared_ptr<CObjectDef> pDef;
    pDef = pDoc->GetObjectDef().lock();
    if (!pDef)
    {
        STD_DBG(_T("CLayerWnd::OnViewActivate CObjectDef."));
        return 0;
    }

    if ((pDef->GetObjectType() != VIEW_COMMON::E_PARTS)&&
        (pDef->GetObjectType() != VIEW_COMMON::E_FIELD))
    {
        m_vlstLayer.EnableWindow(FALSE);
        return 0;
    }


    m_pCtrl = dynamic_cast<CPartsDef*>(pDef.get());

    m_vlstLayer.SetItemCount( m_pCtrl->GetLayerMax());
    RedrawWindow();

    return 0;
 
}


/**
 * @brief   ビュークリア
 * @param   [in] wParam 使用しない
 * @param   [in] lParam 使用しない
 * @return  なし
 * @note    
 */
LRESULT CLayerWnd::OnViewClear(WPARAM pMessage, LPARAM lParam)
{
    return 0;
}

/**
 * @brief   レイヤー追加
 * @param   なし
 * @return  なし
 * @note    
 */
void CLayerWnd::OnMnuLayerAdd()
{
    if (m_pCtrl)
    {
        m_pCtrl->AddLayer();
        m_vlstLayer.SetItemCount( m_pCtrl->GetLayerMax());
        RedrawWindow();
    }
}

/**
 * @brief   レイヤー追加UI更新
 * @param   なし
 * @return  なし
 * @note    
 */
void CLayerWnd::OnUpdateMnuLayerAdd(CCmdUI *pCmdUI)
{


}

/**
 * @brief   レイヤー削除
 * @param   なし
 * @return  なし
 * @note    
 */
void CLayerWnd::OnMnuLayerDel()
{
    if (!m_pCtrl)
    {
        return;
    }

    bool bRet;
    int  iCol = 0;
    int  iRow = 0;

    m_vlstLayer.GetCurCell(&iRow, &iCol);

    if(iRow < 0)
    {
        return;
    }

    bRet = m_pCtrl->DelLayer(iRow, false);

    if (!bRet)
    {

        int iAns = AfxMessageBox(GET_STR(STR_LAYER_DEL_QUESTION), MB_YESNO);
        if (iAns != IDYES)
        {
            return;
        }

        bRet = m_pCtrl->DelLayer(iRow, true);
        m_pCtrl->Redraw();
    }
    
    int iMax = m_pCtrl->GetLayerMax() - 1;
    m_vlstLayer.SetItemCount( iMax + 1);

    if (iRow > iMax)
    {
        m_vlstLayer.Select(iMax, iCol);
    }

}

/**
 * @brief   レイヤー削除UI更新
 * @param   なし
 * @return  なし
 * @note    
 */void CLayerWnd::OnUpdateMnuLayerDel(CCmdUI *pCmdUI)
{
    int  iCol = 0;
    int  iRow = 0;

    if (!m_pCtrl)
    {
        return;
    }

    pCmdUI->Enable(m_pCtrl->GetLayerMax() > 1);
}


/**
 * @brief   レイヤー倍率
 * @param   なし
 * @return  なし
 * @note    
 */
void CLayerWnd::OnMnuLayerScl()
{
    CLayerSclDlg dlgLayer;
    if (dlgLayer.DoModal() != IDOK)
    {
        return;
    }

    int  iCol = 0;
    int  iRow = 0;

    m_vlstLayer.GetCurCell(&iRow, &iCol);

    if(iRow < 0)
    {
        return;
    }

    int iMax = m_pCtrl->GetLayerMax();
    int iStart = 0;
    double dCurScl = 1.0;
    CLayer* pLayer;
    CLayer* pCurLayer;

    if (dlgLayer.bIgnoreZero)
    {
        iStart = 1;
    }

    pCurLayer = m_pCtrl->GetLayer(iRow);
    if (pCurLayer)
    {
        dCurScl = pCurLayer->dScl;
    }


    for (int iNo = iStart; iNo < iMax; iNo++)
    {
        pLayer = m_pCtrl->GetLayer(iNo);
        if (!pLayer)
        {
            continue;
        }

        //同じ倍率のみ
        if (dlgLayer.bSameScl)
        {
            if (fabs(pLayer->dScl - dCurScl) > NEAR_ZERO)
            {
                continue;
            }
        }

        //表示しているもの
        if (dlgLayer.bUnVisible)
        {
            if (!pLayer->bVisible)
            {
                continue;
            }
        }
        pLayer->dScl = dlgLayer.dScl;
    }

    //倍率部再描画
    m_vlstLayer.RedrawCol(LAYER_SCL, CVirtualCheckList::RD_TEXT);
    //再描画
    m_pCtrl->Redraw();
}

/**
 * @brief   レイヤー倍率UI更新
 * @param   なし
 * @return  なし
 * @note    
 */
void CLayerWnd::OnUpdateMnuLayerScl(CCmdUI *pCmdUI)
{


}


/**
*  @brief コンテキストメニュー選択
*  @param [in] pWnd   マウスの右ボタンがクリックされたウィンドウのハンドル
*  @param [in] point  クリックされたときの、カーソルの画面座標位置 
*  @retval           なし
*  @note
*/
void CLayerWnd::OnContextMenu(CWnd* pWnd, CPoint point)
{
    if (pWnd->m_hWnd == m_vlstLayer.m_hWnd)
    {
	    theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_LAYER, point.x, point.y, this, TRUE);
    }
}

/**
 *  @brief モード変更
 *  @param [in] wParam   アプリケーションモード(EXEC_COMMON::E_APP_MODE)
 *  @param [in] lParam   オブジェクト定義      (CObjectDef)
 *  @retval
 *  @note
 */
LRESULT CLayerWnd::OnModeChange(WPARAM wParam, LPARAM lParam)
{
    using namespace EXEC_COMMON;

    E_EXEC_STS  eSts;
    CObjectDef* pDef;

    eSts = static_cast<E_EXEC_STS>(wParam);
    pDef = reinterpret_cast<CObjectDef*>(lParam);

    if (eSts == EX_EDIT)
    {
        m_pCtrl = NULL;
        m_vlstLayer.SetItemCount( 0 );
        RedrawWindow();
    }


    return 0;
}
