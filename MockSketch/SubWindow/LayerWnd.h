/**
 * @brief			CLayerWndヘッダーファイル
 * @file			CLayerWnd.h
 * @author			Yasuhiro Sato
 * @date			01-2-2009 10:36:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef __LAYER_WND_
#define __LAYER_WND_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "Utility/VirtualTreeCtrl/VirtualCheckList.h"
#include "ToolBar/CustomToolBar.h"
#include "SubWindow/CustomDocablePane.h"

/*---------------------------------------------------*/
/*  classes                                          */
/*---------------------------------------------------*/
class CPartsDef;

/**
 * @class   CPropertiesToolBar
 * @brief                        
   
 */
class CLayerToolBar : public CCustomToolBar
{
public:
    CLayerToolBar():CCustomToolBar(){
        SetName(_T("CLayerToolBar"));
    }

	virtual void OnUpdateCmdUI(CFrameWnd* /*pTarget*/, BOOL bDisableIfNoHndler)
	{
		CMFCToolBar::OnUpdateCmdUI((CFrameWnd*) GetOwner(), bDisableIfNoHndler);
	}
    
	virtual BOOL AllowShowOnList() const { return FALSE; }
};


/**
 * @class   CLayerWnd
 * @brief
 */
class CLayerWnd : public CCustomDocablePane
{
    enum LAAYER_COL
    {
        LAYER_CURRENT = 0,
        LAYER_ENABLE  = 1,
        LAYER_VISIBLE = 2,
        LAYER_USE_COLOR = 3,
        LAYER_COLOR     = 4,
        LAYER_NAME      = 5,
        LAYER_SCL       = 6,
		LAYER_USE_BOUND = 7,

    };

// コンストラクション
public:
	CLayerWnd();
	virtual ~CLayerWnd();

	void AdjustLayout();

// 属性
public:
	void SetVSDotNetLook(BOOL bSet)
	{
	}

    //!< レイヤーリスト
    CVirtualCheckList m_vlstLayer;

    //!< カレントレイヤー番号
    int               m_iLayerNo;

protected:
    CLayerToolBar   m_wndToolBar;

	CFont m_fntPropList;

    //ヘッダ
    CImageList  m_imageHeader;

    //!< 表示オブジェクトコントロール
    CPartsDef* m_pCtrl;

    //!< 描画スレッド
    boost::thread  tharedDraw;

// 実装
protected:
    

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnSettingChange(UINT uFlags, LPCTSTR lpszSection);

	DECLARE_MESSAGE_MAP()

    void SetListFont();

    //!< エディット完了
    afx_msg void OnLvnEndlabeleditVlst(NMHDR *pNMHDR, LRESULT *pResult);

    // 描画コールバック
    static void __cdecl Callbackfunc(LPVOID pParent, int nRow, int nCol, _TCHAR* pData, bool* pbCheck);

    // チェックコールバック
    static void __cdecl CallbackChek(LPVOID pParent, int nRow, int nCol);

    //描画処理
    virtual void SetText( int nRow, int nCol, _TCHAR* pData, bool* pbCheck);

    //!< View生成通知
    afx_msg LRESULT OnViewCreate(WPARAM iType, LPARAM pMessage);

    //!< View活性化通知
    afx_msg  LRESULT OnViewActivate(WPARAM pMessage, LPARAM lParam);

    //!<ビュークリア
    afx_msg LRESULT OnViewClear(WPARAM pMessage, LPARAM lParam);

    //!<モード変更
    afx_msg LRESULT OnModeChange(WPARAM wParam, LPARAM lParam);


    virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
public:
    //!< レイヤー追加
    afx_msg void OnMnuLayerAdd();

    //!< レイヤー追加UI更新
    afx_msg void OnUpdateMnuLayerAdd(CCmdUI *pCmdUI);

    //!< レイヤー削除
    afx_msg void OnMnuLayerDel();

    //!< レイヤー削除UI更新
    afx_msg void OnUpdateMnuLayerDel(CCmdUI *pCmdUI);

    //!< レイヤー倍率変更
    afx_msg void OnMnuLayerScl();

    //!< レイヤー倍率変更UI更新
    afx_msg void OnUpdateMnuLayerScl(CCmdUI *pCmdUI);

    afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);
};

#endif //__LAYER_WND_