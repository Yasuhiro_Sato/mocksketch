/**
 * @brief			NewProjectDlg実装ファイル
 * @file			NewProjectDlg.cpp
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MockSketch.h"
#include "./NewProjectDlg.h"
#include "System/CSystem.h"
#include "Utility/CUtility.h"



/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
IMPLEMENT_DYNAMIC(CNewProjectDlg, CDialog)


BEGIN_MESSAGE_MAP(CNewProjectDlg, CDialog)
    ON_BN_CLICKED(IDC_PB_REF, &CNewProjectDlg::OnBnClickedPbRef)
    ON_BN_CLICKED(IDOK, &CNewProjectDlg::OnBnClickedOk)
    ON_BN_CLICKED(IDCANCEL, &CNewProjectDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


/**
  *  @brief  コンストラクター.
  *  @param  なし
  *  @return なし
  */
CNewProjectDlg::CNewProjectDlg(CWnd* pParent /*=NULL*/)
    : CDialog(CNewProjectDlg::IDD, pParent),
    m_bCreateDir(true),
    m_bCreateAssy(true),
	m_bCreateXml(false),
    m_bSucess(false)
     
{
}

/**
  *  @brief  デストラクタ.
  *  @param  なし
  *  @return なし
  */
CNewProjectDlg::~CNewProjectDlg()
{
}

/**
 *  @brief  フレームワークの自動的なデータ交換
 *  @param  pDX     CDataExchange オブジェクトへのポインタ
 *  @retval なし     
 *  @note   UpdateData メンバ関数から呼び出されます。
 */
void CNewProjectDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_CB_DIR, m_cbDir);
}



/**
 *  @brief  ウインドウの初期化処理
 *  @param  なし
 *  @retval 常にTRUE     
 *  @note   
 */
BOOL CNewProjectDlg::OnInitDialog()
{
    CDialog::OnInitDialog();
    SetWindowText(GET_STR(STR_DLG_NEWPRJ));
    GetDlgItem(IDCANCEL)      ->SetWindowText(GET_STR(STR_DLG_CANCEL));
    GetDlgItem(IDC_PB_REF)    ->SetWindowText(GET_STR(STR_DLG_NEWPRJ_REF));
    GetDlgItem(IDC_ST_PROJECT)->SetWindowText(GET_STR(STR_DLG_NEWPRJ_PROJECT));
    GetDlgItem(IDC_ST_DIR)    ->SetWindowText(GET_STR(STR_DLG_NEWPRJ_DIR));
    GetDlgItem(IDC_CK_CREATE_DIR) ->SetWindowText(GET_STR(STR_DLG_NEWPRJ_CREATE_DIR));
    GetDlgItem(IDC_CK_CREATE_ASSY)->SetWindowText(GET_STR(STR_DLG_NEWPRJ_CREATE_ASSY));
	GetDlgItem(IDC_CK_XML)->SetWindowText(GET_STR(STR_DLG_NEWPRJ_XML));
    
    CheckDlgButton(IDC_CK_CREATE_DIR,  m_bCreateDir?BST_CHECKED:BST_UNCHECKED);
    CheckDlgButton(IDC_CK_CREATE_ASSY, m_bCreateAssy?BST_CHECKED:BST_UNCHECKED);
	CheckDlgButton(IDC_CK_XML, m_bCreateXml ? BST_CHECKED : BST_UNCHECKED);

    StdString strItem;
    int iSize = sizeof(SYS_CONFIG->strProjectList)/sizeof(StdString);
    for (int iCnt = 0; iCnt < iSize; iCnt++)
    {
        strItem = SYS_CONFIG->strProjectList[iCnt];

        if (strItem == _T(""))
        {
            continue;
        }
        m_cbDir.InsertString(0, strItem.c_str());
    }

    if (m_cbDir.GetCount() > 0)
    {
        m_cbDir.SetCurSel(0);
    }

    if (m_strPrjectName != _T(""))
    {
        GetDlgItem(IDC_ED_PROJECT)->SetWindowText(m_strPrjectName.c_str());
    }


    if (m_strDir != _T(""))
    {
        m_cbDir.SetWindowText(m_strDir.c_str());
    }

    return TRUE;
}

/**
 *  @brief  参照ボタン押下
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CNewProjectDlg::OnBnClickedPbRef()
{
    //!< ディレクトリ選択ダイアログ
    HWND hWnd = m_hWnd;
    BOOL bRet;
    StdChar szPath[4096];
    bRet = CUtil::FuncSelectFolder( m_hWnd, 
                                    &szPath[0], 
                                    _T(""), 
                                    GET_STR(STR_DLG_SEL_DIR) );

    if (bRet)
    {
        m_cbDir.SetWindowText(&szPath[0]);
    }
}

/**
 *  @brief  OKボタン押下
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CNewProjectDlg::OnBnClickedOk()
{
    namespace fs = boost::filesystem;

    m_bCreateDir  = (IsDlgButtonChecked(IDC_CK_CREATE_DIR) == BST_CHECKED);
    m_bCreateAssy = (IsDlgButtonChecked(IDC_CK_CREATE_ASSY) == BST_CHECKED);
	m_bCreateXml  = (IsDlgButtonChecked(IDC_CK_XML) == BST_CHECKED);

    CString strDir;
    CString strProject;

    m_cbDir.GetWindowText(strDir);
    GetDlgItem(IDC_ED_PROJECT)->GetWindowText(strProject);
    
    
    if (strProject == _T(""))
    {
        AfxMessageBox(GET_STR(STR_ERR_NEWPRJ_NO_PRJ_NAME));
        return;
    }

    if (strDir == _T(""))
    {
        AfxMessageBox(GET_STR(STR_ERR_NEWPRJ_NO_DIR));
        return;
    }

    //プロジェクト名がファイル名として使用できるか
    try
    {
        CUtil::CheckFilename((LPCTSTR)strProject, true);
    }
    catch(MockException &e)
    {
        STD_DBG(_T("Error[%d] %s"), e.GetErr(), e.GetErrMsg().c_str());
        e.DispMessageBox();
        return;
    }

    //----------------------------
    //ディレクトリ作成可能チェック
    //----------------------------
    StdString strStdDir = strDir;
    StdPath pathDir(strStdDir);
    try
    {
        StdPath pathDir(strStdDir);
        fs::create_directory(pathDir);
    }
    catch(...)
    {
        STD_DBG(_T("Cant Create Dir[%s]"), strDir);
        AfxMessageBox(GET_STR(STR_ERROR_CANNOT_CREATE_DIR));
        return;
    }


    int iIndex;
    int iSize = sizeof(SYS_CONFIG->strProjectList)/sizeof(StdString);
    iIndex = m_cbDir.FindStringExact( -1, strDir );
    if (iIndex == CB_ERR)
    {
        // 同じものがなければ
        // リストボックスに追加
        m_cbDir.InsertString(0, strDir);
        if (m_cbDir.GetCount() >= iSize)
        {
            m_cbDir.DeleteString(iSize - 1);
        }
    }
    
    m_strDir = strDir;
    m_strPrjectName = strProject;
    m_bSucess = true;
    
    CString strListBox;
    int iCbSize = m_cbDir.GetCount();
    //リストボックスのデータを保存する
    for (int iCnt = 0; iCnt < iSize; iCnt++)
    {

        strListBox = _T("");
        if (iCnt < iCbSize)
        {
            m_cbDir.GetLBText(iCnt, strListBox);
        }

        SYS_CONFIG->strProjectList[iCnt] = (LPCTSTR)strListBox;
    }

    OnOK();
}

/**
 *  @brief  キャンセルボタン押下
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CNewProjectDlg::OnBnClickedCancel()
{
    OnCancel();
}

/**
 *  @brief  メッセージフィルタ処理
 *  @param  [in] pMsg   ウインドウメッセージ
 *  @retval true これ以上の処理不要 / false 通常処理
 *  @note   
 */
BOOL CNewProjectDlg::PreTranslateMessage(MSG* pMsg)
{
    if(pMsg->message == WM_DROPFILES)
    {
        if (pMsg->hwnd == GetDlgItem(IDC_CB_DIR)->m_hWnd)
        {
            HDROP hDropInfo = reinterpret_cast<HDROP>(pMsg->wParam);
            DropFiles(hDropInfo);
            return TRUE;
        }
    }
    return CDialog::PreTranslateMessage(pMsg);
}

/**
 *  @brief  DropFiles
 *  @param  [in] hDropInfo   ドロップ情報
 *  @retval なし
 *  @note   
 */
void CNewProjectDlg::DropFiles(HDROP hDropInfo) 
{
    namespace fs = boost::filesystem;

    // ドロップされた内容を取得
    int ncnt = 0;
    UINT unSize ;
    StdChar szPath[_MAX_PATH+1];

    unSize = ::DragQueryFile( hDropInfo,
              ncnt++,
              szPath,
               _MAX_PATH);

    if( unSize > 0)
    {
        //ファイル名の場合はフォルダーに変換する
        StdPath pathSel(szPath);
        
        if(!fs::is_directory(pathSel))
        {
            pathSel = GetPathStr(pathSel.parent_path()).c_str();
        }

        m_cbDir.SetWindowText(GetPathStr(pathSel).c_str());
    }
}

/**
 *  @brief  ディレクトリ取得
 *  @param  なし
 *  @retval なし
 *  @note   
 */
StdString   CNewProjectDlg::GetDir()
{
    return m_strDir;
}

/**
 *  @brief  ディレクトリ設定
 *  @param  [in] strDir ディレクトリ名
 *  @retval なし
 *  @note   
 */
void   CNewProjectDlg::SetDir(StdString strDir)
{
    m_strDir = strDir;
}

/**
 *  @brief  プロジェクト名取得
 *  @param  なし
 *  @retval プロジェクト名
 *  @note   
 */
StdString   CNewProjectDlg::GetProjectName()
{
    return m_strPrjectName;
}

/**
 *  @brief  プロジェクト名設定
 *  @param  [in]  strName プロジェクト名1
 *  @retval なし
 *  @note   
 */
void CNewProjectDlg::SetProjectName(StdString strName)
{
    m_strPrjectName = strName;
}

/**
 *  @brief  設定完了判定
 *  @param  なし
 *  @retval true:設定成功  false:設定失敗
 *  @note   
 */
bool CNewProjectDlg::IsSucess()
{
    return m_bSucess;
}


/**
 *  @brief  ディレクトリ作成の有無
 *  @param  なし
 *  @retval true:ディレクトリ作成
 *  @note   
 */
bool   CNewProjectDlg::IsCreateDir()
{
    return m_bCreateDir;
}



/**
 *  @brief  装置定義作成の有無
 *  @param  なし
 *  @retval true:装置定義作成
 *  @note   
 */
bool   CNewProjectDlg::IsCreateAssy()
{
    return m_bCreateAssy;
}

/**
 *  @brief  装置定義作成の有無
 *  @param  なし
 *  @retval true:装置定義作成
 *  @note
 */
bool   CNewProjectDlg::IsCreateXml()
{
	return m_bCreateXml;
}
