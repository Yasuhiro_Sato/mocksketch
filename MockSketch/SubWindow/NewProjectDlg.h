/**
 * @brief			NewProjectDlgヘッダーファイル
 * @file			NewProjectDlg.h
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#pragma once
#include "afxwin.h"


// CNewProjectDlg ダイアログ

class CNewProjectDlg : public CDialog
{
	DECLARE_DYNAMIC(CNewProjectDlg)

public:
    //!< コンストラクター
    CNewProjectDlg(CWnd* pParent = NULL);   // 標準コンストラクタ

    //!< デストラクター
    virtual ~CNewProjectDlg();

    //!< ディレクトリ取得
    StdString   GetDir();

    //!< ディレクトリ設定
    void   SetDir(StdString strDir);

    //!< プロジェクト名取得
    StdString   GetProjectName();

    //!< プロジェクト名設定
    void   SetProjectName(StdString strName);

    //!< 設定完了判定
    bool   IsSucess();

    //!< ディレクトリ作成の有無
    bool   IsCreateDir();

    //!< 装置定義作成の有無
    bool   IsCreateAssy();

	//!<  XMLの有無
	bool   IsCreateXml();
	
	// ダイアログ データ
	enum { IDD = IDD_NEW_PROJECT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート


    void DropFiles(HDROP hDropInfo);

	DECLARE_MESSAGE_MAP()
public:
    virtual BOOL OnInitDialog();
    afx_msg void OnBnClickedPbRef();
    afx_msg void OnBnClickedOk();
    afx_msg void OnBnClickedCancel();
    virtual BOOL PreTranslateMessage(MSG* pMsg);
    CComboBox m_cbDir;

protected:
    StdString   m_strDir;
    StdString   m_strPrjectName;
    bool        m_bSucess;
    bool        m_bCreateDir;
    bool        m_bCreateAssy;
	bool        m_bCreateXml;

};
