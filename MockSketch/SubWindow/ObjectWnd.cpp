/**
* @brief		CObjcetView実装ファイル
* @file			CObjcetView.cpp
* @author			Yasuhiro Sato
* @date			09-2-2009 23:59:08
* 
* @note	
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
* 
* @remarks	        
*			
*
* $4
* $
* 
*/

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "mainfrm.h"
#include "Resource.h"
#include "resource2.h"
#include "MockSketch.h"
#include "CProjectCtrl.h"
#include "./ObjectWnd.h"
#include "./TextInputDlg.h"

#include "DefinitionObject/View/MockSketchView.h"
#include "DefinitionObject/CPartsDef.h"
#include "Utility/CUtility.h"
#include "Utility/DropUtil.h"
#include "Utility/DropSource.h"
#include "System/CSystem.h"
#include "Script/CScript.h"
#include "Script/CExecCtrl.h"

#include "View/ViewCommon.h"

using namespace VIEW_COMMON;


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

BEGIN_MESSAGE_MAP(CObjectWnd, CDockablePane)
    ON_WM_CREATE()
    ON_WM_SIZE()
    ON_WM_CONTEXTMENU()
    ON_COMMAND(ID_PROPERTIES, OnProperties)
    ON_COMMAND(ID_OPEN, OnFileOpen)
    ON_COMMAND(ID_OPEN_WITH, OnFileOpenWith)
    /*
    ON_COMMAND(ID_DUMMY_COMPILE, OnDummyCompile)
    ON_COMMAND(ID_EDIT_CUT, OnEditCut)
    ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
    ON_COMMAND(ID_EDIT_CLEAR, OnEditClear)
    */
    ON_COMMAND(ID_MNU_OBJ_OPEN,     OnOpen)
    ON_COMMAND(ID_MNU_OBJ_FOLDER,   OnCreateFolder)
    ON_COMMAND(ID_MNU_OBJ_NAME,     OnChangeName)
    ON_COMMAND(ID_MNU_OBJ_SAVE  ,   OnObjectSave)
    ON_COMMAND(ID_MNU_OBJ_PARTS,    OnCreateParts)
    ON_COMMAND(ID_MNU_OBJ_REFERENCE,OnCreateReference)

    ON_COMMAND(ID_MNU_OBJ_FIELD,    OnCreateField)
    ON_COMMAND(ID_MNU_OBJ_ELEMENT,  OnCreateElement)
                                        
    ON_COMMAND(ID_MNU_OBJ_SCRIPT,   OnCreateScript)

    ON_COMMAND(ID_MNU_OBJ_CUT,      OnCut)
    ON_COMMAND(ID_MNU_OBJ_COPY,     OnCopy)
    ON_COMMAND(ID_MNU_OBJ_PASTE,    OnPaste)
    ON_COMMAND(ID_MNU_OBJ_DELETE,   OnDelete)

    ON_UPDATE_COMMAND_UI(ID_MNU_OBJ_SCRIPT, OnUpdateMenuScript)
    ON_UPDATE_COMMAND_UI(ID_MNU_OBJ_CUT, OnUpdateMenuCut)
    ON_UPDATE_COMMAND_UI(ID_MNU_OBJ_COPY, OnUpdateMenuCopy)
    ON_UPDATE_COMMAND_UI(ID_MNU_OBJ_PASTE, OnUpdateMenuPaste)
    ON_UPDATE_COMMAND_UI(ID_MNU_OBJ_DELETE, OnUpdateMenuDelete)

    ON_MESSAGE(WM_VIEW_CREATE, OnViewCreate)
    ON_MESSAGE(WM_VIEW_LOAD_END, OnViewLoadEnd)
    ON_MESSAGE(WM_VIEW_ACTIVE, OnViewActivate)
    ON_MESSAGE(WM_VIEW_UPDATE_PROJ, OnViewUpdateProj)
    ON_MESSAGE(WM_VIEW_ADD_OBJECT, OnViewAddObject)
    ON_MESSAGE(WM_VIEW_CLEAR_ALL, OnViewClear)
    
    ON_MESSAGE(WM_CHG_OBJECT_STS, OnChangeObjectStatus)
    ON_MESSAGE(WM_UPDATE_OBJECT,  OnUpdateObject)
        

    ON_WM_PAINT()
    ON_WM_SETFOCUS()
    ON_NOTIFY(NM_DBLCLK, IDC_TREE_OBJ, OnNMDblclk)
    ON_NOTIFY(TVN_ENDLABELEDIT, IDC_TREE_OBJ, OnEndLabelEdit)
    ON_NOTIFY(TVN_BEGINDRAG,  IDC_TREE_OBJ, OnBegindrag)
    ON_NOTIFY(TVN_BEGINRDRAG, IDC_TREE_OBJ, OnBegindragRight)
    ON_WM_LBUTTONUP()
    ON_WM_MOUSEMOVE()
    ON_MESSAGE(WM_DROP_ENTER,  OnDropEnter)
    ON_MESSAGE(WM_DROP_END,    OnDropEnd)
    ON_MESSAGE(WM_DROP_OVER,   OnDropOver)
    ON_MESSAGE(WM_DROP_LEAVE,  OnDropLeave)
    ON_MESSAGE(WM_USER_PASTE_END, OnUserPasteEnd)

END_MESSAGE_MAP()

/**
*  @brief  コンストラクター.
*  @param  なし
*  @retval なし     
*  @note
*/
CObjectWnd::CObjectWnd():
m_pDragImage    (NULL),
m_bDrag         (false),
m_hitemDrag     (NULL),
m_eProjectType  (VIEW_COMMON::E_PROJ_MAIN)
{
    m_wndObjectView.SetDrawCallback(_DrawCallback, this);
}

/**
*  @brief  コンストラクター.
*  @param  なし
*  @retval なし     
*  @note
*/
CObjectWnd::CObjectWnd(VIEW_COMMON::E_PROJ_TYPE eType):
m_pDragImage    (NULL),
m_bDrag         (false),
m_hitemDrag     (NULL),
m_eProjectType  (eType)
{
    m_wndObjectView.SetDrawCallback(_DrawCallback, this);
}

/**
 *  @brief  デストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CObjectWnd::~CObjectWnd()
{
}

/**
 *  @brief  プロジェクト種別設定
 *  @param  [in] eType
 *  @retval なし
 *  @note
 */
void CObjectWnd::SetProjectType(VIEW_COMMON::E_PROJ_TYPE eType)
{
    m_eProjectType = eType;
}

/**
 *  @brief  描画処理コールバック
 *  @param  [in] pCdc
 *  @param  [in] hTree
 *  @param  [in] pVoid
 *  @retval なし
 *  @note
 */
void CObjectWnd::_DrawExpand(CDC* pDc, CRect rcButton, VIEW_COMMON::EXPAND_TYPE eExpand )
{
	CBrush brButton;


    brButton.CreateSolidBrush( DRAW_CONFIG->crCompild );


    CPoint ptCenter( rcButton.left + int(::floor(rcButton.Width() / 2.0)),
                     rcButton.top  + int(::floor(rcButton.Height() / 2.0)));

    // draw plus button [+]
	if( eExpand == EXP_PLUS )
	{
        pDc->FrameRect( &rcButton, &brButton );
		CPen Pen, *pOldPen;
		Pen.CreatePen( PS_SOLID, 1, DRAW_CONFIG->crCompild );
		pOldPen = pDc->SelectObject( &Pen );
        pDc->MoveTo( rcButton.left, ptCenter.y  );
		pDc->LineTo( rcButton.right, ptCenter.y );
		pDc->MoveTo( ptCenter.x, rcButton.top );
		pDc->LineTo( ptCenter.x, rcButton.bottom );
		pDc->SelectObject( pOldPen );
	}
	// draw minus button [-]
	else if( eExpand == EXP_MINUS)
	{
        pDc->FrameRect( &rcButton, &brButton );
		CPen Pen, *pOldPen;
		Pen.CreatePen( PS_SOLID, 1, DRAW_CONFIG->crCompild );
		pOldPen = pDc->SelectObject( &Pen );
		pDc->MoveTo( rcButton.left, ptCenter.y );
		pDc->LineTo( rcButton.right, ptCenter.y );
		pDc->SelectObject( pOldPen );
	}

    ::ExtSelectClipRgn( pDc->m_hDC, NULL, RGN_COPY );

    return;
}

/**
 *  @brief  描画処理コールバック
 *  @param  [in] pCdc
 *  @param  [in] hTree
 *  @param  [in] pVoid
 *  @retval なし
 *  @note
 */
void CObjectWnd::_DrawCallback(CDC* pCdc, HTREEITEM hTree, void* pVoid)
{
    CObjectWnd* pObjWnd;
    pObjWnd = reinterpret_cast<CObjectWnd*>(pVoid);

    StdString strObj;
    StdString strScript;

    E_ITEM_TYPE eImage;


    TVITEM tvItem;
    StdChar strItem[1024];

    memset(&tvItem, 0, sizeof(tvItem));
    tvItem.mask = TVIF_TEXT | TVIF_IMAGE| 
                  TVIF_SELECTEDIMAGE | 
                  TVIF_CHILDREN | 
                  TVIF_PARAM |
                  TVIF_STATE | TVIF_HANDLE;
    tvItem.cchTextMax = 1024;
    tvItem.pszText = &strItem[0];
    tvItem.stateMask = TVIS_EXPANDED;
    tvItem.hItem = hTree;

    if(!pObjWnd->m_wndObjectView.GetItem(&tvItem))
    {
        return;
    }


    eImage = static_cast<E_ITEM_TYPE>(tvItem.iImage);
    
    bool bDef = false;

    switch (eImage)
    {
    case OBJ_FOLDER:
        return;

    case OBJ_ROOT:
        break;

    case OBJ_SCRIPT:
    {
        HTREEITEM hParentTree;
        hParentTree = pObjWnd->m_wndObjectView.GetParentItem(hTree);
        strObj = (LPCTSTR)pObjWnd->m_wndObjectView.GetItemText(hParentTree);
        strScript = tvItem.pszText;
    }
        break;

    case OBJ_PARTS:   
    case OBJ_ELEMENT:
    case OBJ_FIELD:
    case OBJ_REFERENCE:

        strObj = tvItem.pszText;
        bDef   = true;
        break;

    default:
        return;
    }

    std::shared_ptr<CProjectCtrl> pProject;
    pProject = THIS_APP->GetProject(pObjWnd->m_eProjectType).lock();

    if (!pProject)
    {
        return;
    }
    
    boost::uuids::uuid uuidParts;
    uuidParts = pProject->GetPartsIdByName(strObj);

    bool bChange;
    
    if (eImage == OBJ_ROOT)
    {
        bChange = pProject->IsChange();
    }
    else
    {
        bChange = pProject->IsChange(uuidParts, strScript);
    }

#if 1  //FOR DEBUG
    int iIndentPixel;
    iIndentPixel = pObjWnd->m_wndObjectView.GetIndent();

    int iIndexNum;
    iIndexNum = pObjWnd->m_wndObjectView.GetIndentNum(hTree);
#endif

    RECT rcItem;
    RECT rcFill;
    pObjWnd->m_wndObjectView.GetItemRect(hTree, &rcFill, FALSE /*bTextOnly*/);
    pObjWnd->m_wndObjectView.GetItemRect(hTree, &rcItem, TRUE /*bTextOnly*/);


    //-------------------------------------------
    //              +  ■ ☆ Text
    //              |  |  インジケータ 
    //              |  アイコン
    //              Expand
    //                      
    //------------------------------------------

    //コンパイル済みの場合は文字を青くする
    bool bCompiled = false;
    COLORREF crText = DRAW_CONFIG->crText;
    if (bDef)
    {
        COMPILE_STS stsCompile;
        stsCompile = pProject->GetCompileStatus(uuidParts);

        if ((stsCompile == CS_SUCCESS) ||
            (stsCompile == CS_SUCCESS_D))
        {
            bCompiled = true;
            crText = DRAW_CONFIG->crCompild;
        }
        else if(stsCompile == CS_ERROR)
        {
            crText = DRAW_CONFIG->crCompild;
        }
    }


    HBRUSH hBr;
    if (bChange)
    {
        hBr = ::CreateSolidBrush(DRAW_CONFIG->crChange);
    }
    else
    {
        hBr = ::CreateSolidBrush(DRAW_CONFIG->crSaved);
    }
    

    HBRUSH hBack;
    hBack = ::CreateSolidBrush(pCdc->GetBkColor());

    //背景塗りつぶし
    ::FillRect(pCdc->m_hDC, &rcFill, hBack);

    //インジケータ表示
    // インジケータは高さの半分の四角形とする
    int iHeight = rcItem.bottom - rcItem.top;

    RECT rcIndicator;
    int iIndicatorWidth = CUtil::Round(iHeight / 2.0);

    int iMargine = 0; //CUtil::Round(iIndicatorWidth / 10.0);
    int iTopMargine = CUtil::Round((iHeight -  iIndicatorWidth) / 2.0);

    rcIndicator.top    = rcItem.top + iTopMargine;
    rcIndicator.bottom = rcIndicator.top + iIndicatorWidth;
    rcIndicator.right  = rcItem.left   - iMargine;
    rcIndicator.left   = rcIndicator.right   -  iIndicatorWidth;

    ::FillRect(pCdc->m_hDC, &rcIndicator, hBr);

    //アイコン表示
    POINT ptIcon;

    ptIcon.x = rcIndicator.left - iMargine - iHeight;
    ptIcon.y = rcItem.top;

    SIZE szIcon;
    szIcon.cx = 0;
    szIcon.cy = 0;

    pObjWnd->m_ObjectImages.DrawEx(pCdc, eImage, ptIcon, szIcon,
                        CLR_DEFAULT, CLR_DEFAULT, ILD_NORMAL);

    // Expand表示
    // 
    RECT rcExpand;

    //幅を偶数にする
    int iExpandWidth = int(ceil(iIndicatorWidth / 2.0) * 2.0);

    rcExpand.top    = rcIndicator.top;
    rcExpand.bottom = rcExpand.top + iExpandWidth;
    rcExpand.right  = ptIcon.x;
    rcExpand.left   = rcExpand.right - iExpandWidth;

    VIEW_COMMON::EXPAND_TYPE eExpand = EXP_NONE;
    if (tvItem.cChildren != 0)
    {
        if (((tvItem.state & TVIS_EXPANDED) != 0))
        {
            eExpand = VIEW_COMMON::EXP_MINUS;
        }
        else
        {
            eExpand = VIEW_COMMON::EXP_PLUS;
        }
    }

    _DrawExpand(pCdc, rcExpand, eExpand);

    // アイテムのテキストを描画する

    CFont* pOldFnt = NULL;
    CFont fntBold;

    if (bCompiled)
    {
        LOGFONT logfont;
        CFont* pFont = pCdc->GetCurrentFont( );
        pFont->GetLogFont(&logfont);
        logfont.lfWeight = FW_BOLD;

        fntBold.CreateFontIndirect(&logfont);
        pOldFnt = pCdc->SelectObject(&fntBold);
    }

    pCdc->SetTextColor(crText);
    CRect rect;
    StdString strDisp = (LPCTSTR)pObjWnd->m_wndObjectView.GetItemText(hTree);
    pObjWnd->m_wndObjectView.GetItemRect(hTree, &rect, TRUE);
    pCdc->TextOut(rect.left + 2, rect.top + 1, strDisp.c_str());

    if (pOldFnt)
    {
        pCdc->SelectObject(pOldFnt);
    }

}

/**
 *  @brief  作成時処理.
 *  @param  lpCreateStruct
 *  @retval なし     
 *  @note
 */
int CObjectWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (CDockablePane::OnCreate(lpCreateStruct) == -1)
        return -1;


	m_wndToolBar.Create(this, AFX_DEFAULT_TOOLBAR_STYLE, IDR_OBJECTVIEW_BAR);
	m_wndToolBar.LoadToolBar(IDR_OBJECTVIEW_BAR, 0, 0, TRUE /* ロックされています*/);
	m_wndToolBar.CleanUpLockedImages();
	m_wndToolBar.LoadBitmap(IDR_OBJECTVIEW_BAR, 0, 0, TRUE /* ロックされました*/);

	m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() | CBRS_TOOLTIPS | CBRS_FLYBY);
	m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() & ~(CBRS_GRIPPER | CBRS_SIZE_DYNAMIC | CBRS_BORDER_TOP | CBRS_BORDER_BOTTOM | CBRS_BORDER_LEFT | CBRS_BORDER_RIGHT));
	m_wndToolBar.SetOwner(this);

    // すべてのコマンドが、親フレーム経由ではなくこのコントロール経由で渡されます:
	m_wndToolBar.SetRouteCommandsViaFrame(FALSE);

    CRect rectDummy;
    rectDummy.SetRectEmpty();

    // ビューの作成:
    const DWORD dwViewStyle = WS_CHILD | WS_VISIBLE | TVS_HASLINES | TVS_LINESATROOT 
                                       | TVS_HASBUTTONS | TVS_EDITLABELS | TVS_SHOWSELALWAYS;

    //if (!m_wndObjectView.Create(dwViewStyle, rectDummy, this, 4))
    if (!m_wndObjectView.Create(dwViewStyle, rectDummy, this, IDC_TREE_OBJ))
    {
        DB_PRINT(_T("オブジェクト ビューを作成できませんでした\n"));
        return -1;      // 作成できない場合
    }

    //---------------------------
    // ビューのイメージの読み込み:
    //---------------------------

    //TODO:高Dpi対応

    m_iIconWidth = 16;
    m_ObjectImages.Create(IDB_OBJECT_VIEW, m_iIconWidth, 0, RGB(255, 0, 255));
    m_wndObjectView.SetImageList(&m_ObjectImages,  TVSIL_NORMAL );

     OnChangeVisualStyle();

    AdjustLayout();


    m_dropTarget.Register(this);
    m_dropTarget.SetDropWnd(this);

    return 0;
}

/**
*  @brief ウインドウサイズ変更
*  @param [in] nType   要求されるサイズ変更のタイプ  (SIZE_MAXIMIZED....)
*  @param [in] cx      クライアント領域の新しい幅 
*  @param [in] cy      クライアント領域の新しい高さ
*  @retval           なし
*  @note
*/
void CObjectWnd::OnSize(UINT nType, int cx, int cy)
{
    CDockablePane::OnSize(nType, cx, cy);
    AdjustLayout();
}

/**
 *  @brief オブジェクト表示
 *  @param [in] pCtrl   プロジェクトコントロール
 *  @retval           なし
 *  @note
 */
void CObjectWnd::FillObjet()
{
    m_wndObjectView.DeleteAllItems();

    std::shared_ptr<CProjectCtrl> pProject;
    pProject = THIS_APP->GetProject(m_eProjectType).lock();

    if (pProject == NULL)
    {
        return;
    }

    //m_pProject = pProject;
    //ディレクトリを反映させる
    TREE_DATA<VIEW_COMMON::OBJ_TREE_DATA>* pTreeData = 
        pProject->GetFolderData();

    HTREEITEM hRoot;
    HTREEITEM hMain;
    HTREEITEM hScript;


    if (pTreeData == NULL)
    {
        //必要か？
        StdString strName = pProject->GetName();
        hRoot = m_wndObjectView.InsertItem(strName.c_str(), 0, 0);
        m_wndObjectView.SetItemState(hRoot, TVIS_BOLD, TVIS_BOLD);

        hMain = m_wndObjectView.InsertItem(_T("Main"), OBJ_PARTS, OBJ_PARTS, hRoot);
        hScript = m_wndObjectView.InsertItem(_T("Script"), OBJ_SCRIPT, OBJ_SCRIPT, hMain);
    }
    else
    {
        _ReadTreeData();
        hRoot = m_wndObjectView.GetRootItem();
    }

    m_wndObjectView.Expand(hRoot, TVE_EXPAND);

}

/**
 * @brief    Treeデータ取得
 * @param    なし
 * @retval   なし
 * @note	 
 */
void CObjectWnd::_ReadTreeData()
{
    std::shared_ptr<CProjectCtrl> pProject;
    pProject = THIS_APP->GetProject(m_eProjectType).lock();

    if (pProject == NULL)
    {
        return;
    }

    TREE_DATA<VIEW_COMMON::OBJ_TREE_DATA>* pTreeData;

    CTreeCtrl* pWndTree = (CTreeCtrl*) &m_wndObjectView;
    pTreeData = pProject->GetFolderData();

    TREE_DATA_SetData<VIEW_COMMON::OBJ_TREE_DATA>(pWndTree, pTreeData);
}

/**
 *  @brief コンテキストメニュー選択
 *  @param [in] pWnd   マウスの右ボタンがクリックされたウィンドウのハンドル
 *  @param [in] point  クリックされたときの、カーソルの画面座標位置 
 *  @retval           なし
 *  @note
 */
void CObjectWnd::OnContextMenu(CWnd* pWnd, CPoint point)
{
    CTreeCtrl* pWndTree = (CTreeCtrl*) &m_wndObjectView;
    ASSERT_VALID(pWndTree);

    if (pWnd != pWndTree)
    {
        CDockablePane::OnContextMenu(pWnd, point);
        return;
    }

    HTREEITEM hTreeItem = NULL;
    if (point != CPoint(-1, -1))
    {
        // クリックされた項目の選択:
        CPoint ptTree = point;
        pWndTree->ScreenToClient(&ptTree);

        UINT flags = 0;
        hTreeItem = pWndTree->HitTest(ptTree, &flags);
        if (hTreeItem != NULL)
        {
            pWndTree->SelectItem(hTreeItem);
        }
    }

    pWndTree->SetFocus();
    theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_OBJECT_VIEW, point.x, point.y, this, TRUE);

}

/**
*  @brief  表示位置調整
*  @param  なし
*  @retval なし
*  @note
*/
void CObjectWnd::AdjustLayout()
{
    if (GetSafeHwnd() == NULL)
    {
        return;
    }

    CRect rectClient;
    GetClientRect(rectClient);

    int cyTlb = 0;
    cyTlb = m_wndToolBar.CalcFixedLayout(FALSE, TRUE).cy;
    m_wndToolBar.SetWindowPos(NULL, rectClient.left, 
                                    rectClient.top, 
                                    rectClient.Width(), 
                                    cyTlb, SWP_NOACTIVATE | SWP_NOZORDER);
    m_wndObjectView.SetWindowPos(NULL, rectClient.left + 1, 
                                       rectClient.top + cyTlb + 1, 
                                       rectClient.Width() - 2, 
                                       rectClient.Height() - cyTlb - 2, 
                                       SWP_NOACTIVATE | SWP_NOZORDER);
}

/**
*  @brief  プロパティ表示選択
*  @param  なし
*  @retval なし
*  @note
*/
void CObjectWnd::OnProperties()
{
    AfxMessageBox(_T("プロパティ..."));

}

void CObjectWnd::OnFileOpen()
{
    // TODO: ここにコマンド ハンドラ コードを追加します
}

void CObjectWnd::OnFileOpenWith()
{
    // TODO: ここにコマンド ハンドラ コードを追加します
}


/**
 * @brief    描画処理
 * @param    なし
 * @retval   なし
 * @note	 フレームワークより
 */
void CObjectWnd::OnPaint()
{
    CPaintDC dc(this); // 描画のデバイス コンテキスト

    CRect rectTree;
    m_wndObjectView.GetWindowRect(rectTree);
    ScreenToClient(rectTree);

    rectTree.InflateRect(1, 1);
    dc.Draw3dRect(rectTree, ::GetSysColor(COLOR_3DSHADOW), ::GetSysColor(COLOR_3DSHADOW));
}

/**
 * @brief    フォーカス設定
 * @param    [out] pOldWnd フォーカス設定されていたウインドウ
 * @retval   なし
 * @note	 フレームワークより
 */
void CObjectWnd::OnSetFocus(CWnd* pOldWnd)
{
    CDockablePane::OnSetFocus(pOldWnd);

    m_wndObjectView.SetFocus();
}


/**
* @brief    表示スタイル変更
* @param    なし
* @retval   なし
* @note	
*/
void CObjectWnd::OnChangeVisualStyle()
{

}

/**
* @brief    View生成通知
* @param    [in] iType 生成されたViewの種別;
* @param    [in] pMessage CPartsDefへのポインタ;
* @retval   なし
* @note	
*/
LRESULT CObjectWnd::OnViewCreate(WPARAM iType, LPARAM pMessage)
{
    return 0;
}


/**
* @brief    ロード完了を通知
* @param    [in] pMessage CPartsDefへのポインタ;
* @param    [in] lParam 使用しない;
* @retval   なし
* @note	
*/
LRESULT CObjectWnd::OnViewLoadEnd(WPARAM pMessage, LPARAM lParam)
{
    CProjectCtrl* pProject = 0;

    if (lParam == m_eProjectType )
    {
        FillObjet();
        RedrawWindow();
    }
    return 0;
}

/**
 * @brief    View活性化を通知
 * @param    [in] pMessage タブタイトル文字
 * @param    [in] lParam   0:MockSletchView 1:EditView
 * @retval   なし
 * @note	
 */
LRESULT CObjectWnd::OnViewActivate(WPARAM pMessage, LPARAM lParam)
{
    if (pMessage == 0)
    {
        return  0;
    }

    HTREEITEM hTreeSel = 0;

    CTreeCtrl* pWndTree = (CTreeCtrl*)&m_wndObjectView;
    ASSERT_VALID(pWndTree);

    StdString* pTitle = reinterpret_cast<StdString*>(pMessage);
    int        iType  = (int)lParam;


    StdString strObj;

    //CString strText;
    BOOL  bRet;

    VIEW_COMMON::E_PROJ_TYPE eType;

    if ((iType == 0) ||
        (iType == 2))
    {
        eType = THIS_APP->GetObjectDefOwner(&strObj, *pTitle);

        if (eType == m_eProjectType)
        {
            //SketchView
            std::shared_ptr<CObjectDef> pDef;
            std::shared_ptr<CProjectCtrl> pProject;
            pProject = THIS_APP->GetProject(eType).lock();
            E_ITEM_TYPE eItem = OBJ_PARTS;
            if (pProject)
            {
                pDef = pProject->GetPartsByName(strObj).lock();
                if (pDef)
                {
                    E_OBJ_DEF_TYPE eDef = pDef->GetObjectType();
                    eItem = DefToItem(eDef);
                }
            }

            hTreeSel = _GetTreeItem(strObj.c_str(), 0, eItem);
        }
    }
    else if (iType == 1)
    {
        //Editor
        HTREEITEM hObjSel;
        StdString strScript;
        CPartsDef::DivideScriptTabName(&strScript, &strObj, *pTitle); 

        eType = THIS_APP->GetObjectDefOwner(NULL, strObj);

        if (eType == m_eProjectType)
        {

            std::shared_ptr<CObjectDef> pDef;
            std::shared_ptr<CProjectCtrl> pProject;
            pProject = THIS_APP->GetProject(eType).lock();
            E_ITEM_TYPE eItem = OBJ_PARTS;
            if (pProject)
            {
                pDef = pProject->GetPartsByName(strObj).lock();
                if (pDef)
                {
                    E_OBJ_DEF_TYPE eDef = pDef->GetObjectType();
                    eItem = DefToItem(eDef);
                }
            }


            //SketchView
            hObjSel = _GetTreeItem(strObj.c_str(), 0, eItem);
            if (hObjSel != 0)
            {
                //スクリプト名を探す
                hTreeSel = _GetTreeItem(strScript.c_str(), hObjSel, OBJ_SCRIPT);
            }
        }
    }

    bRet = pWndTree->SelectItem( hTreeSel );

    if (hTreeSel)
    {
        pWndTree->EnsureVisible( hTreeSel);
    }
    return 0;
}

/**
 * @brief    選択中アイテムのDrawPartsDef取得
 * @param    なし
 * @retval   DrawPartsDefへのポインタ
 * @note	
 */
std::weak_ptr<CObjectDef>  CObjectWnd::_GetCurrentObjectDef()  const
{
    std::weak_ptr<CObjectDef> pRet;
    CTreeCtrl* pWndTree = (CTreeCtrl*)&m_wndObjectView;
    ASSERT_VALID(pWndTree);

    HTREEITEM hCurrent = pWndTree->GetSelectedItem();

    if (hCurrent == 0)
    {
        return pRet;
    }

    TVITEM ti;
    TCHAR  szName[1024];

    ti.mask = TVIF_IMAGE | TVIF_PARAM | TVIF_TEXT |TVIF_SELECTEDIMAGE;
    ti.hItem        = hCurrent;
    ti.pszText      = &szName[0];
    ti.cchTextMax   = sizeof(szName)/sizeof(TCHAR);

    StdString strObj;
    pWndTree->GetItem(&ti);

    switch(ti.iImage)
    {
        case OBJ_SCRIPT:
        {
            HTREEITEM hParent = pWndTree->GetParentItem(ti.hItem);
            if (hParent == NULL)
            {
                STD_DBG(_T("hParent == NULL %s"), szName);
                return pRet;
            }
            strObj = (LPCTSTR)pWndTree->GetItemText(hParent);
        }

        case OBJ_PARTS:
        case OBJ_ELEMENT:
        case OBJ_FIELD:
        case OBJ_REFERENCE:

        {
            strObj = szName;
        }
        
        default:
            return pRet;
    }
   

    // 部品の取得
    return _GetObjectDef(strObj);
}

/**
 * @brief    CObjectDef取得
 * @param    [int] strObjName オブジェクト名( LIB: 等はついていない)
 * @retval   CObjectDefへのポインタ
 * @note	
 */
std::weak_ptr<CObjectDef>  CObjectWnd::_GetObjectDef(StdString strObjName) const
{
    strObjName = VIEW_COMMON::GetProjectPrifx(m_eProjectType) + strObjName;
    std::weak_ptr<CObjectDef> pDef;
    pDef = THIS_APP->GetObjectDefByTabName(strObjName);
    return pDef;
}

/**
 * @brief    PartsId取得
 * @param    [int] strObjName オブジェクト名( LIB: 等はついていない)
 * @retval   PartsId
 * @note    
 */
boost::uuids::uuid  CObjectWnd::_GetObjectPartsId(StdString strObjName) const
{
    boost::uuids::uuid uuidParts;
    strObjName = VIEW_COMMON::GetProjectPrifx(m_eProjectType) + strObjName;
    uuidParts = THIS_APP->GetPartsIdByTabName(strObjName);
    return uuidParts;
}

/**
 * @brief    Tree検索
 * @param    [in] strItem      検索名
 * @param    [in] hTreeRoot    検索ルートアイテム
 * @param    [in] eType        アイテム種別
 * @retval   Treeハンドル
 * @note	
 */
HTREEITEM  CObjectWnd::_GetTreeItem(LPCTSTR strItem, 
                                    HTREEITEM hTreeRoot, 
                                    VIEW_COMMON::E_ITEM_TYPE eType)
{
    CTreeCtrl* pWndTree = (CTreeCtrl*)&m_wndObjectView;
    ASSERT_VALID(pWndTree);

    if (hTreeRoot == 0)
    {
        hTreeRoot = pWndTree->GetRootItem();
    }

    if (pWndTree->GetItemText(hTreeRoot) == strItem)
    {
        int iImage;
        int iImageSel;
        pWndTree->GetItemImage(hTreeRoot, iImage, iImageSel);
        if (eType == iImage)
        {
            return hTreeRoot;
        }
    }


    HTREEITEM hChild;
    HTREEITEM hNext;
    HTREEITEM hRet = 0;
    hChild = pWndTree->GetChildItem(hTreeRoot);
    if (hChild != 0)
    {
        hRet = _GetTreeItem( strItem, hChild, eType);
        if (hRet != 0)
        {
            return hRet;
        }
    }

    hNext = pWndTree->GetNextItem(hTreeRoot, TVGN_NEXT);
    if (hNext != 0)
    {
        hRet = _GetTreeItem( strItem, hNext, eType);
        if (hRet != 0)
        {
            return hRet;
        }
    }
    return 0;
}

/**
 * @brief    ダブルクリックイベント
 * @param    [in]  pNMHDR 通知メッセージへのポインタ
 * @param    [out] pResult 
 * @retval   なし
 * @note	
 */
void CObjectWnd::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
    OpenFile(false);
    *pResult = -1;
}

/**
 * @brief    開く
 * @param    [in]  bReload 再読み込み
 * @param    [out] pResult 
 * @retval   なし
 * @note	
 */
void CObjectWnd::OpenFile(bool bReload)
{
    CTreeCtrl* pWndTree = (CTreeCtrl*) &m_wndObjectView;
    ASSERT_VALID(pWndTree);

    UINT flags = 0;
    HTREEITEM hTreeItem =pWndTree->GetSelectedItem( );

    TVITEM ti;
    TCHAR  szName[1024];

    ti.mask = TVIF_IMAGE | TVIF_PARAM | TVIF_TEXT |TVIF_SELECTEDIMAGE;
    ti.hItem        = hTreeItem;
    ti.pszText      = &szName[0];
    ti.cchTextMax   = sizeof(szName)/sizeof(TCHAR);

    pWndTree->GetItem(&ti);

    StdString strPriFix;
    strPriFix = VIEW_COMMON::GetProjectPrifx(m_eProjectType);
    StdString   strObject;

    if (hTreeItem != NULL)
    {
        HTREEITEM  hParent = 0;
        if (ti.iImage == OBJ_SCRIPT)
        {
            HTREEITEM  hParts;
            hParts = pWndTree->GetParentItem(hTreeItem);
            if (hParts != NULL)
            {
                strObject = strPriFix;
                strObject += pWndTree->GetItemText(hParts);
            }

            //ファイル再読み込み

            THIS_APP->SelectEditorTab( strObject.c_str(), ti.pszText, bReload);
        }
        else if ((ti.iImage == OBJ_PARTS) ||
                 (ti.iImage == OBJ_REFERENCE)||
                 (ti.iImage == OBJ_ELEMENT)||
                 (ti.iImage == OBJ_FIELD))  
        {
            //名前からタブを探して切り替える
            strObject =  strPriFix;
            strObject += ti.pszText;

            THIS_APP->SelectViewTab(strObject.c_str());
        }
    }
}

/**
 * @brief    開く
 * @param    なし
 * @retval   なし
 * @note	
 */
void CObjectWnd::OnOpen()
{
    OpenFile(true);
}


/**
* @brief    フォルダー作成
* @param    なし
* @retval   なし
* @note	
*/
void CObjectWnd::OnCreateFolder()
{
    CTreeCtrl* pWndTree = (CTreeCtrl*) &m_wndObjectView;
    ASSERT_VALID(pWndTree);

    //選択中のツリー
    HTREEITEM  hTree = pWndTree->GetSelectedItem();

    //フォルダー名設定ダイアログを開く
    CTextInputDlg dlgInput;

    dlgInput.SetInfo(GET_STR(STR_DLG_FOLDER_TITLE), 
        GET_STR(STR_DLG_FOLDER_INFO));

    INT_PTR iRet = dlgInput.DoModal();

    if (iRet != IDOK)
    {
        return;
    }

    StdString strRet = CUtil::Trim(dlgInput.GetText());

    if (strRet == _T(""))
    {
        //文字を入力してください
        AfxMessageBox(GET_STR(STR_ERROR_INPUT_STRING));
        return;
    }

    //フォルダーに使えない文字をチェック(特になし)
    //「/\:*?"<>|#{}%&~.」

    //"/\\:*?\"<>|#{}%&~"
    //find_first_of(const string& str, size_type pos = 0) const;

    TVITEM ti;
    ti.mask = TVIF_IMAGE | TVIF_PARAM | TVIF_SELECTEDIMAGE;
    ti.hItem = hTree;
    pWndTree->GetItem(&ti);


    HTREEITEM  hParent = 0;
    hParent = _GetFolder(hTree);

    HTREEITEM hInsert;
    hInsert = m_wndObjectView.InsertItem(strRet.c_str(), OBJ_FOLDER, OBJ_FOLDER, hParent);
    m_wndObjectView.SelectItem( hInsert);
    m_wndObjectView.EnsureVisible( hInsert);

    //_WriteTreeData();
}

/**
* @brief    親フォルダ取得
* @param    [in] hTree  検索元
* @retval   検索結果
* @note	
*/
HTREEITEM  CObjectWnd::_GetParentFolder(HTREEITEM hTree)
{
    CTreeCtrl* pWndTree = (CTreeCtrl*) &m_wndObjectView;
    ASSERT_VALID(pWndTree);

    TVITEM ti;
    ti.mask = TVIF_IMAGE | TVIF_PARAM | TVIF_SELECTEDIMAGE;
    ti.hItem = hTree;
    pWndTree->GetItem(&ti);
    
    HTREEITEM hParent  = hTree;
    if ((ti.iImage == OBJ_FOLDER) ||
        (ti.iImage == OBJ_ROOT))
    {
        hParent = pWndTree->GetParentItem(hTree);
    }
    return _GetFolder(hParent);

}

/**
* @brief    フォルダ取得
* @param    [in] hTree  検索元
* @retval   検索結果
* @note	
*/
HTREEITEM  CObjectWnd::_GetFolder(HTREEITEM hTree)
{
    CTreeCtrl* pWndTree = (CTreeCtrl*) &m_wndObjectView;
    ASSERT_VALID(pWndTree);

    TVITEM ti;
    ti.mask = TVIF_IMAGE | TVIF_PARAM | TVIF_SELECTEDIMAGE;
    ti.hItem = hTree;
    pWndTree->GetItem(&ti);
    
    if ((ti.iImage == OBJ_FOLDER) ||
        (ti.iImage == OBJ_ROOT))
    {
        return hTree;
    }

    HTREEITEM  hParent = 0;
    HTREEITEM  hRet    = 0;
    hParent = pWndTree->GetParentItem(hTree);

    if (hParent == 0)
    {
        return 0;
    }

    hRet = _GetFolder(hParent);

    return hRet;
}


/**
 * @brief    保存
 * @param    なし
 * @retval   なし
 * @note	
 */
void CObjectWnd::OnObjectSave   ()
{
    if (GET_APP_MODE() != EXEC_COMMON::EX_EDIT)
    {
        return;
    }

    HTREEITEM hTree = m_wndObjectView.GetSelectedItem();
    if (hTree == NULL)
    {
        //ツリーが選択されていない
        return;
    }

    //クリック項目に応じてメニューを更新
    int iImage;
    int iImageSel;
    m_wndObjectView.GetItemImage(hTree, iImage, iImageSel);


    std::shared_ptr<CProjectCtrl> pProject;
    if ((iImage == OBJ_FOLDER) ||
        (iImage == OBJ_UNKNOWN))
    {
        return;
    }

    pProject = THIS_APP->GetProject(m_eProjectType).lock();
    STD_ASSERT(pProject);
    if(!pProject)
    {
        return;
    }

    TREE_DATA<VIEW_COMMON::OBJ_TREE_DATA>* pData;
    pData = pProject->GetFolderData();

    int iData;
    iData = static_cast<int>(m_wndObjectView.GetItemData(hTree));

    OBJ_TREE_DATA* pTreeObj = pData->GetItem(iData);
    STD_ASSERT(pTreeObj);
    if(!pTreeObj)
    {
        return;
    }

    StdString strScript; 
    bool bRet;
    if (iImage == OBJ_ROOT)
    {
        StdPath Path(pProject->GetFilePath());
        bRet = pProject->Save(Path);
    }
    else if (iImage == OBJ_SCRIPT)
    {
        strScript = (LPCTSTR)m_wndObjectView.GetItemText(hTree);
        bRet = THIS_APP->SaveScript(pTreeObj->uuidDef, strScript);
    }
    else
    {
        std::shared_ptr<CObjectDef> pDef; 
        pDef = pProject->GetParts(pTreeObj->uuidDef).lock();

        StdPath Path = pDef->GetCurrentPath();
        bRet = pDef->Save(Path);
    }
}

/**
 *  @brief  保存UI更新
 *  @param  [in out] コマンド ユーザー インターフェイス
 *  @retval なし
 *  @note   
 */
void CObjectWnd::OnUpdateObjectSave(CCmdUI* pCmdUI)
{
    bool bEnable = false;

    HTREEITEM hTree = m_wndObjectView.GetSelectedItem();
    if (hTree == NULL)
    {
        //ツリーが選択されていない
        pCmdUI->Enable(bEnable);
        return;
    }

    //クリック項目に応じてメニューを更新
    int iImage;
    int iImageSel;
    m_wndObjectView.GetItemImage(hTree, iImage, iImageSel);


    if (GET_APP_MODE() == EXEC_COMMON::EX_EDIT)
    {
        std::shared_ptr<CProjectCtrl> pProject;
        if ((iImage != OBJ_FOLDER) &&
            (iImage != OBJ_UNKNOWN))
        {
            pProject = THIS_APP->GetProject(m_eProjectType).lock();
            STD_ASSERT(pProject);
            if(!pProject)
            {
                pCmdUI->Enable(bEnable);
                return;
            }

            TREE_DATA<VIEW_COMMON::OBJ_TREE_DATA>* pData;
            pData = pProject->GetFolderData();

            int iData;
            iData = static_cast<int>(m_wndObjectView.GetItemData(hTree));

            OBJ_TREE_DATA* pTreeObj = pData->GetItem(iData);
            STD_ASSERT(pTreeObj);
            if(!pTreeObj)
            {
                pCmdUI->Enable(bEnable);
                return;
            }

            StdString strScript; 
            if (iImage == OBJ_ROOT)
            {
                bEnable = pProject->IsChange();
            }
            else if (iImage == OBJ_SCRIPT)
            {
                strScript = (LPCTSTR)m_wndObjectView.GetItemText(hTree);
                bEnable = pProject->IsChange(pTreeObj->uuidDef, strScript);
            }
            else
            {
                bEnable = pProject->IsChange(pTreeObj->uuidDef, _T(""));
            }
        }
    }
    pCmdUI->Enable(bEnable);
}


/**
* @brief    名称変更
* @param    なし
* @retval   なし
* @note	
*/
void CObjectWnd::OnChangeName()
{
    CTreeCtrl* pWndTree = (CTreeCtrl*) &m_wndObjectView;
    ASSERT_VALID(pWndTree);


    HTREEITEM  hItem = pWndTree->GetSelectedItem();
    pWndTree->EditLabel(hItem);    // 該当するアイテムを編集する。

    //_WriteTreeData();
}


/**
* @brief    ラベル編集後イベント
* @param    なし
* @retval   なし
* @note	 フレームワークより
*/
void CObjectWnd::OnEndLabelEdit(NMHDR* pNMHDR, LRESULT* pResult)
{
    TV_DISPINFO *ptv_disp;

    ptv_disp = ( TV_DISPINFO* )pNMHDR;
    TV_ITEM* item = &( ptv_disp->item );

    StdString strNewName;
    StdString strCurrentName;
    *pResult = FALSE;

    if (item == NULL)           { return; }
    if (item->pszText == NULL)  { return; }

    strNewName = item->pszText;
    if (strNewName == _T(""))      { return; }

    // ここで受信するイベントは テキストのみ (mask == TVIF_TEXT)
    // なので他の情報は改めて取得し直す必要があります。

    CTreeCtrl* pWndTree = (CTreeCtrl*) &m_wndObjectView;
    ASSERT_VALID(pWndTree);

    TVITEM ti;
    TCHAR  szName[1024];

    ti.mask = TVIF_IMAGE | TVIF_PARAM | TVIF_TEXT |TVIF_SELECTEDIMAGE;
    ti.hItem = item->hItem;
    ti.pszText      = &szName[0];
    ti.cchTextMax   = sizeof(szName)/sizeof(TCHAR);
    pWndTree->GetItem(&ti);
    
    strCurrentName = szName;
    
    std::shared_ptr<CProjectCtrl> pProject;
    pProject = THIS_APP->GetProject(m_eProjectType).lock();

    std::shared_ptr<CObjectDef>  pCurrentObjectDef;
    pCurrentObjectDef = _GetCurrentObjectDef().lock();
    if (!pCurrentObjectDef)
    {
        return;
    }
    
    if (strCurrentName == strNewName)
    {
        return;
    }

    if (ti.iImage == OBJ_SCRIPT)
    {
        //文字チェック
        HTREEITEM hParent = pWndTree->GetParentItem(ti.hItem);
        StdString strParent = pWndTree->GetItemText(hParent);

        if (!pCurrentObjectDef->GetScriptInstance(strNewName))
        {
            //同じ名称が存在します
            AfxMessageBox(GET_ERR_STR(e_same_name));
            return;
        }

        if (!CScript::CheckScriptName(strNewName) )
        {
            //使用できない文字が含まれている
            AfxMessageBox(GET_ERR_STR(e_no_use_char));
            return;
        }
        
        pCurrentObjectDef->ChangeScriptName(strCurrentName, strNewName);
        *pResult = TRUE;

        //Viewに通知
        StdString strTabName;
        strTabName = THIS_APP->GetObjectDefTabName(pCurrentObjectDef->GetPartsId(), 
                                                   strCurrentName);

        CWnd* pWnd = THIS_APP->GetEditor(strTabName);
        if (pWnd == NULL)
        {
            STD_DBG(_T("GetScriptInstance %s"), strCurrentName.c_str());
            return;
        }
        pWnd->SendMessage(WM_VIEW_UPDATE_NAME);
        
    }
    else if ((ti.iImage == OBJ_PARTS) ||
             (ti.iImage == OBJ_FIELD) ||
             (ti.iImage == OBJ_REFERENCE))
    {
        //!< 部品名称チェック(重複)
        if(pProject->IsDupPartsName(strNewName))
        {
            //同じ名称が存在します
            AfxMessageBox(GET_ERR_STR(e_same_name));
            return;
        }


       //!< 部品名称チェック(使用可否)
        if(!pProject->IsPossiblePartsName(strNewName))
        {
            //使用できない文字が含まれている
            AfxMessageBox(GET_ERR_STR(e_no_use_char));
            return;
        }

        pCurrentObjectDef->SetName(strNewName);
        *pResult = TRUE;

         //Viewに通知
        StdString strTabName;
        CWnd* pWnd;
        
        pWnd = THIS_APP->GetView(strCurrentName);
        if (pWnd != NULL)
        {
            pWnd->SendMessage(WM_VIEW_UPDATE_NAME);
        }

        if  ((ti.iImage == OBJ_PARTS)||
             (ti.iImage == OBJ_FIELD))
        {
            //付属しているすべてのスクリプトのタブを更新
            int iScriptNum = pCurrentObjectDef->GetScriptNum();
            CScriptData* pScriptData;
            for (int nCnt = 0; nCnt < iScriptNum; nCnt++)
            {
                pScriptData = pCurrentObjectDef->GetScriptInstance(nCnt);

                StdString strTabName;
                strTabName = THIS_APP->GetObjectDefTabName(pCurrentObjectDef->GetPartsId(), 
                                                           pScriptData->GetName());

                pWnd = THIS_APP->GetEditor(strTabName);
                pScriptData->SetName(strNewName);
                if (pWnd != NULL)
                {
                    pWnd->SendMessage(WM_VIEW_UPDATE_NAME);
                }
            }
        }
    }
    else if (ti.iImage == OBJ_FOLDER)
    {
        if(pWndTree->GetRootItem() == ti.hItem)
        {
            *pResult = FALSE;
            return;
        }
    }
    *pResult = TRUE;    // 変更を有効にさせる 

    //_WriteTreeData();

}

/**
* @brief    部品作成
* @param    [in] eType
* @retval   ture 作成成功
* @note	
*/
bool CObjectWnd::_CreateParts(VIEW_COMMON::E_OBJ_DEF_TYPE eType)
{
    CTreeCtrl* pWndTree = (CTreeCtrl*) &m_wndObjectView;
    ASSERT_VALID(pWndTree);

    std::shared_ptr<CProjectCtrl> pProject;
    pProject = THIS_APP->GetProject(m_eProjectType).lock();

    //部品名を入力

    try
    {
        //選択アイテムを取得
        CTreeCtrl* pWndTree = (CTreeCtrl*) &m_wndObjectView;
        ASSERT_VALID(pWndTree);
        HTREEITEM hTree = pWndTree->GetSelectedItem();

        if (hTree == NULL)
        {
            //ツリーが選択されていない
            return false;
        }

        //---------------------------------------------------
        CTextInputDlg dlgInput;
        StdString strTitle;
        StdString strInfo;
        StdString strError;
        using namespace VIEW_COMMON;

        switch(eType)
        {
        case E_PARTS:
                strTitle = GET_STR( STR_DLG_PARTS_TITLE );             //!< 部品作成
                strInfo  = GET_STR( STR_DLG_PARTS_INFO  );             //!< 部品名を入力してください
                strError = GET_STR( STR_DLG_PARTS_ERROR  );             //!< 部品生成に失敗しました
                break;

        case E_ELEMENT:
                strTitle = GET_STR( STR_DLG_ELEMENT_TITLE );            //!< 要素作成
                strInfo  = GET_STR( STR_DLG_ELEMENT_INFO  );            //!< 要素名を入力してください
                strError = GET_STR( STR_DLG_ELEMENT_ERROR  );           //!< 要素生成に失敗しました
                break;

        case E_FIELD:
                strTitle = GET_STR( STR_DLG_FIELD_TITLE );              //!< フィールド作成
                strInfo  = GET_STR( STR_DLG_FIELD_INFO  );              //!< フィールド名を入力してください
                strError = GET_STR( STR_DLG_FIELD_ERROR  );             //!< フィールド生成に失敗しました
                break;

        case E_REFERENCE:
                strTitle = GET_STR( STR_DLG_REFERENCE_TITLE );              //!< 参照作成
                strInfo  = GET_STR( STR_DLG_REFERENCE_INFO  );              //!< 参照名を入力してください
                strError = GET_STR( STR_DLG_REFERENCE_ERROR  );             //!< 参照生成に失敗しました

        default:
                break;

        }
        dlgInput.SetInfo(strTitle.c_str(), strInfo.c_str());

        //グループ名入力
        StdString  strGroupName = pProject->QueryNewPartsName(eType);
        dlgInput.SetText(strGroupName.c_str());
        dlgInput.SelectAll();

        INT_PTR iRet = dlgInput.DoModal();

        if (iRet != IDOK)
        {
            return false;
        }

        StdString strRet = CUtil::Trim(dlgInput.GetText());
        if (strRet == _T(""))
        {
            //文字を入力してください
            AfxMessageBox(GET_STR(STR_ERROR_INPUT_STRING));
            return false;

        }

        //入力 
        if (!pProject->IsPossiblePartsName(strRet))
        {
            //使用不可能な文字が含まれています
            AfxMessageBox(GET_ERR_STR(e_no_use_char));
            return false;
        }

        //入力 
        if (pProject->IsDupPartsName(strRet))
        {
            //すでに同じ名称が使用されています
            AfxMessageBox(GET_ERR_STR(e_same_name));
            return false;
        }

        TREE_DATA<VIEW_COMMON::OBJ_TREE_DATA>* pData;
        pData = pProject->GetFolderData();
        TREE_DATA_GetExpand<VIEW_COMMON::OBJ_TREE_DATA>(pData, &m_wndObjectView);


        int iSelId = static_cast<int>(pWndTree->GetItemData(hTree));

        std::shared_ptr<CObjectDef> pObjectDef;
        pObjectDef = pProject->CreateParts(strRet, eType, iSelId).lock();
        if (!pObjectDef)
        {
            //バーツが生成できませんでした
            AfxMessageBox(GET_ERR_STR(e_parts_create));
            return false;
        }
        //---------------------------------------------------

        TREE_DATA_SetData<VIEW_COMMON::OBJ_TREE_DATA>(&m_wndObjectView, pData);

        using namespace boost::assign;

        //部品を保存
        pProject->SaveAllObject();

        //パーツを表示
        THIS_APP->OpenNewView(pObjectDef->GetPartsId());

    }
    catch(MockException &e)
    {
        STD_DBG(_T("Error[%d] %s"), e.GetErr(), e.GetErrMsg().c_str());
        e.DispMessageBox();
        return false;
    }
    catch(...)
    {
        STD_DBG(_T("Exception"));
        AfxMessageBox(GET_STR(STR_DLG_PARTS_ERROR));
        return false;
    }
    return true;
}

/**
* @brief    部品作成
* @param    なし
* @retval   なし
* @note	
*/
void CObjectWnd::OnCreateParts()
{
    _CreateParts(VIEW_COMMON::E_PARTS);
}

/**
* @brief    部品作成
* @param    なし
* @retval   なし
* @note	
*/
void CObjectWnd::OnCreateReference()
{
    _CreateParts(VIEW_COMMON::E_REFERENCE);
}


/**
* @brief    フィールド作成
* @param    なし
* @retval   なし
* @note	
*/
void CObjectWnd::OnCreateField()
{
    _CreateParts(VIEW_COMMON::E_FIELD);
}

/**
 * @brief    エレメント作成
 * @param    なし
 * @retval   なし
 * @note	
 */
void CObjectWnd::OnCreateElement()
{
    _CreateParts(VIEW_COMMON::E_ELEMENT);
}

/**
* @brief    スクリプト作成
* @param    なし
* @retval   なし
* @note	
*/
void  CObjectWnd::OnCreateScript()
{
    try
    {
    //選択アイテムを取得
    CTreeCtrl* pWndTree = (CTreeCtrl*) &m_wndObjectView;
    ASSERT_VALID(pWndTree);
    HTREEITEM hTree = pWndTree->GetSelectedItem();

    if (hTree == NULL)
    {
        //ツリーが選択されていない
        return;
    }


    CTextInputDlg dlgInput;

    dlgInput.SetInfo(GET_STR(STR_DLG_SCRIPT_TITLE), 
        GET_STR(STR_DLG_SCRIPT_INFO));

    INT_PTR iRet = dlgInput.DoModal();

    if (iRet != IDOK)
    {
        return;
    }

    StdString strScript = CUtil::Trim(dlgInput.GetText());

    if (strScript == _T(""))
    {
        //文字を入力してください
        AfxMessageBox(GET_STR(STR_ERROR_INPUT_STRING));
        return;

    }

    //入力 
    try
    {
        if (!CScript::CheckScriptName(strScript))
        {
            //使用不可能な文字が含まれています
            AfxMessageBox(GET_ERR_STR(e_no_use_char));
            return;
        }
    }
    catch(MockException& e)
    {
        AfxMessageBox(GET_ERR_STR(e.GetErr()));
    }

    //Partsオブジェクト、若しくはメインオブジェクトを取得する
    TVITEM ti;
    ti.mask = TVIF_IMAGE | TVIF_PARAM | TVIF_SELECTEDIMAGE;
    ti.hItem = hTree;
    pWndTree->GetItem(&ti);

    HTREEITEM  hParent = 0;
    if (ti.iImage == OBJ_SCRIPT)
    {
        HTREEITEM  hFig;
        hFig = pWndTree->GetParentItem(hTree);
        if (hFig != NULL)
        {
            hParent = pWndTree->GetParentItem(hTree);
        }
    }
    else if (ti.iImage == OBJ_PARTS)
    {
        hParent = hTree;
    }
    else if (ti.iImage == OBJ_FIELD)
    {
        hParent = hTree;
    }
    else if (ti.iImage == OBJ_REFERENCE)
    {
        hParent = hTree;
    }
    else if (ti.iImage == OBJ_FOLDER)
    {
        hParent = hTree;
    }
    else
    {
        //想定外
        STD_DBG(_T("ti.iImage = %d"), ti.iImage);
        return;
    }

    if (hParent == 0)
    {
        STD_DBG(_T("OnCreateScript hParent == 0"));
        return;
    }


    StdString strPartsName;
    strPartsName = pWndTree->GetItemText(hParent);

    std::shared_ptr<CProjectCtrl> pProject;
    pProject = THIS_APP->GetProject(m_eProjectType).lock();

    if(!pProject)
    {
        return;
    }

    std::shared_ptr<CObjectDef> pObjectDef;
    pObjectDef   = pProject->GetPartsByName(strPartsName).lock();

    if (!pObjectDef)
    {
        STD_DBG( _T("GetParts(%s) Error\n"), strPartsName.c_str());
        return;
    }

    if(pObjectDef->GetScriptInstance(strScript))
    {
        //すでに同じ名称が使用されています
        AfxMessageBox(GET_ERR_STR(e_same_name));
        return;
    }

    try
    {
        pObjectDef->CreateScript(strScript);
    }
    catch(MockException& e)
    {
        e.DispMessageBox();
        return;
    }

    m_wndObjectView.InsertItem(strScript.c_str(), OBJ_SCRIPT, OBJ_SCRIPT, hParent);

    //エディタを表示
    THIS_APP->OpenNewEditor(pObjectDef->GetPartsId(), strScript.c_str());

    }
    catch(MockException &e)
    {
        STD_DBG(_T("Error[%d] %s"), e.GetErr(), e.GetErrMsg().c_str());
        e.DispMessageBox();
        return;
    }
    catch(...)
    {
        STD_DBG(_T("Exception"));
        AfxMessageBox(GET_STR(STR_DLG_SCRIPT_ERROR));
        return;
    }
    //_WriteTreeData();
}

/**
 * @brief    切り取り
 * @param    なし
 * @retval   なし
 * @note	
 */
void CObjectWnd::OnCut()
{
  HTREEITEM hItem = m_wndObjectView.GetSelectedItem();
  _CutCopy(hItem, true /*bCut*/);
}

/**
 * @brief    コピー
 * @param    なし
 * @retval   なし
 * @note	
 */
void CObjectWnd::OnCopy()
{
  HTREEITEM hItem = m_wndObjectView.GetSelectedItem();
  _CutCopy(hItem, false /*bCut*/);
}

/**
 * @brief    貼り付け
 * @param    なし
 * @retval   なし
 * @note	
 */
void CObjectWnd::OnPaste()
{
  HTREEITEM hItem = m_wndObjectView.GetSelectedItem();
  _Paste(hItem);
}

/**
* @brief    削除
* @param    なし
* @retval   なし
* @note	
*/
void CObjectWnd::OnDelete()
{
    //選択アイテムを取得
    CTreeCtrl* pWndTree = (CTreeCtrl*) &m_wndObjectView;
    ASSERT_VALID(pWndTree);
    HTREEITEM hTree = pWndTree->GetSelectedItem();

    if (hTree == NULL)
    {
        //ツリーが選択されていない
        return;
    }

    //ダイアログ表示
    int iAns = AfxMessageBox(GET_STR(STR_MB_DELETE), MB_YESNO);
    if (iAns != IDYES)
    {
        return;
    }


    //Partsオブジェクト、若しくはメインオブジェクトを取得する
    TVITEM ti;
    TCHAR  szName[1024];

    ti.mask = TVIF_IMAGE | TVIF_PARAM | TVIF_TEXT |TVIF_SELECTEDIMAGE;
    ti.hItem = hTree;
    ti.pszText      = &szName[0];
    ti.cchTextMax   = sizeof(szName)/sizeof(TCHAR);
    pWndTree->GetItem(&ti);
    
    StdString strCurrentName = szName;

    
    //実データの削除
    std::shared_ptr<CObjectDef> pCurrentObjectDef;
    pCurrentObjectDef = _GetCurrentObjectDef().lock();

    HTREEITEM  hParent = 0;
    if (ti.iImage == OBJ_SCRIPT)
    {
        if (!pCurrentObjectDef)
        {
            STD_DBG(_T("pCurrentObjectDef"));
            return;
        }
        _DeleteScript(pCurrentObjectDef.get(), strCurrentName);
    }
    else if (ti.iImage == OBJ_FOLDER)
    {
        // 現在フォルダーにあるものを１つ上のフォルダーに移動した後
        // フォルダーを削除する

        //親フォルダー
        HTREEITEM hParentFolder = _GetParentFolder(hTree);

        std::vector<HTREEITEM>  lstTree;
        // Delete all of the children of hmyItem.
        if (pWndTree->ItemHasChildren(hTree))
        {
           HTREEITEM hNextItem;
           HTREEITEM hChildItem = pWndTree->GetChildItem(hTree);

           //親ディレクトリが現在選択されているフォルダーのものを検索する
           while (hChildItem != NULL)
           {
              if(hTree == pWndTree->GetNextItem(hChildItem, TVGN_PARENT))
              {
                lstTree.push_back(hChildItem);
              }

              hNextItem = pWndTree->GetNextItem(hChildItem, TVGN_NEXT);
              hChildItem = hNextItem;
           }
        }
        
        if (lstTree.size() != 0)
        {
            std::vector<HTREEITEM>::iterator ite;
            for (ite  = lstTree.begin();
                 ite != lstTree.end();
                 ite++)
            {
                _CopyItem(*ite, hParentFolder, TVI_LAST);
            }
        }
        pWndTree->DeleteItem(hTree);
    }
    else //if (ti.iImage == OBJ_PARTS)
    {
        if (pCurrentObjectDef == NULL)
        {
            STD_DBG(_T("pCurrentObjectDef"));
            return;
        }

        //if (pCurrentPartsDef == pMainPartsDef)
        {
            //変更があれば保存を促す
            //if(pCurrentObjectDef->IsChange())
            {
                //保存処理
                //TAG:[保存処理]
            }
            
        }

        _DeleteParts(pCurrentObjectDef.get());
    }
    //_WriteTreeData();
}

/**
* @brief    部品データ削除
* @param    [in] pPartsDef 部品オブジェクト
* @retval   なし
* @note	 
*/
void CObjectWnd::_DeleteParts(CObjectDef*  pObjectDef)
{
    CWnd* pWnd;
    CScriptData* pScriptData;

    CTreeCtrl* pWndTree = (CTreeCtrl*)&m_wndObjectView;
    ASSERT_VALID(pWndTree);

    std::shared_ptr<CProjectCtrl> pProject;
    pProject = THIS_APP->GetProject(m_eProjectType).lock();

    //付属しているスクリプトを先に削除
    int iScriptNum = pObjectDef->GetScriptNum() - 1;

    if (iScriptNum >= 0)
    {
        //後ろから消していく
        for(int nCnt = iScriptNum; nCnt >= 0; --nCnt)
        {
            pScriptData = pObjectDef->GetScriptInstance( nCnt );
            if (pScriptData == NULL)
            {
                continue;
            }
            _DeleteScript(pObjectDef,  pScriptData->GetName());
        }
    }

    
    StdString strParts = pObjectDef->GetName();
 
    //View削除
    boost::uuids::uuid uuidParts;
    uuidParts = pObjectDef->GetPartsId();
    pWnd = THIS_APP->GetView(uuidParts);
    if (pWnd != NULL)
    {
        CWnd* pParent = pWnd->GetParent();
        if (pParent)
        {
            pParent->DestroyWindow();
        }
    }

    //データ削除
    try
    {
        if (! pProject->DeleteParts(uuidParts))
        {
            STD_DBG(_T("DeleteParts(%s)"), strParts.c_str());
        }
    }
    catch(MockException &e)
    {
        int iAns = e.DispMessageBox();
        if (iAns != IDYES)
        {
            return;
        }
        pProject->DeleteParts(uuidParts);
    }

    //Tree削除
    E_OBJ_DEF_TYPE eDef = pObjectDef->GetObjectType();
    E_ITEM_TYPE eItem = DefToItem(eDef);

    HTREEITEM  hParts = _GetTreeItem(strParts.c_str(), NULL, eItem);
    if (hParts == 0)
    {
        STD_DBG(_T("GetTreeItem(%s)"), strParts.c_str());
        return;
    }

    if (!pWndTree->DeleteItem(hParts))
    {
        STD_DBG(_T("DeleteItem(%s)"), strParts.c_str());
        return;
    }
    //_WriteTreeData();
}


/**
 * @brief    Scriptデータ削除
 * @param    [in] pCurrentObjectDef 
 * @param    [in] strScript スクリプト名 
 * @retval   なし
 * @note	 TreeCtrlの構造をCPartsDefに反映させる
 */
void CObjectWnd::_DeleteScript(CObjectDef*  pCurrentObjectDef,
                               StdString strScript)
{
    CWnd* pWnd;
    CScriptData* pScriptData;
    pScriptData = pCurrentObjectDef->GetScriptInstance(strScript);

    CTreeCtrl* pWndTree = (CTreeCtrl*)&m_wndObjectView;
    ASSERT_VALID(pWndTree);
    
    //EditorView削除
    StdString strTabName;
    strTabName = THIS_APP->GetObjectDefTabName(pCurrentObjectDef, pScriptData->GetName());
    pWnd = THIS_APP->GetEditor(strTabName);
    if (pWnd != NULL)
    {
        CWnd* pParent = pWnd->GetParent();
        if (pParent)
        {
            pParent->DestroyWindow();
        }
    }

    E_OBJ_DEF_TYPE eDef = pCurrentObjectDef->GetObjectType();
    E_ITEM_TYPE eItem = DefToItem(eDef);


    //スクリプト削除
    pScriptData->SetName(strScript);
    if (!pCurrentObjectDef->DeleteScript(strScript))
    {
        STD_DBG(_T("DeleteScript(%s)"), strScript.c_str());
    }
    //Tree削除
    HTREEITEM  hParts = _GetTreeItem(pCurrentObjectDef->GetName().c_str(), NULL, eItem);
    if (hParts == 0)
    {
        STD_DBG(_T("GetTreeItem(%s)"), pCurrentObjectDef->GetName().c_str());
        return;
    }

    if ((eDef == E_PARTS)||
        (eDef == E_FIELD) ||
        (eDef == E_ELEMENT))
    {
        HTREEITEM  hScript = _GetTreeItem(strScript.c_str(), hParts, OBJ_SCRIPT);
        if (!pWndTree->DeleteItem(hScript))
        {
            STD_DBG(_T("DeleteItem(%s)"), strScript.c_str());
            return;
        }
    }
    //_WriteTreeData();
}

/**
 * @brief    ドラッグ開始
 * @param    [in]  pNMHDR 通知メッセージへのポインタ
 * @param    [out] pResult 
 * @note	 フレームワークより呼び出し
 */
void CObjectWnd::OnBegindrag(NMHDR* pNMHDR, LRESULT* pResult) 
{
    *pResult = _Begindrag(pNMHDR, true);
}

/**
 * @brief    左ボタンドラッグ開始
 * @param    [in]  pNMHDR 通知メッセージへのポインタ
 * @param    [out] pResult 
 * @note	 フレームワークより呼び出し
 */
void CObjectWnd::OnBegindragRight(NMHDR* pNMHDR, LRESULT* pResult) 
{
    *pResult = _Begindrag(pNMHDR, false);
}

/**
 * @brief    ドラッグ開始
 * @param    [in]  pNMTreeView 通知メッセージへのポインタ
 * @param    [in]  bLeft true:左ボタン 
 * @retval   なし
 * @note	 
 */
LRESULT CObjectWnd::_Begindrag(NMHDR* pNMHDR, bool bLeft)
{
    NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;

    CTreeCtrl* pWndTree = (CTreeCtrl*) &m_wndObjectView;
    ASSERT_VALID(pWndTree);

    // ドラッグするツリーアイテムのハンドルを取得
    //m_hitemDrag = pWndTree->HitTest(pNMTreeView->ptDrag, NULL);
    m_hitemDrag = pNMTreeView->itemNew.hItem;
    int  iDataId = static_cast<int>(pNMTreeView->itemNew.lParam);
    POINT ptDrag = pNMTreeView->ptDrag;

    if (m_hitemDrag == NULL)
    {
        return S_FALSE;
    }

    int iImage;
    int iImageSel;
    StdString strItem;

    pWndTree->GetItemImage(m_hitemDrag, iImage, iImageSel);
    strItem = (LPCTSTR)pWndTree->GetItemText(m_hitemDrag);

    int iDragSrc;
    iDragSrc = static_cast<int>(pWndTree->GetItemData(m_hitemDrag));

    DROP_PARTS_DATA partsData;
    memset (&partsData, 0, sizeof(DROP_PARTS_DATA));


    switch (iImage)
    {
    case OBJ_ROOT:
        //!< 移動不可
        return S_FALSE;


    case OBJ_FOLDER:
    {
        partsData.uiDropType = CF_MOCK_FOLDER;
        partsData.iDragSrc   = iDragSrc;
        partsData.uuidDef    = boost::uuids::nil_uuid();;
        partsData.eProjType  = m_eProjectType;
        partsData.iScript    = -1;
        partsData.effect     = DROPEFFECT_MOVE;
        partsData.bLeftButton   = bLeft;
        partsData.hCutWindow    = m_hWnd;
    }
        break;

    case OBJ_PARTS:  
    case OBJ_REFERENCE:  
    case OBJ_ELEMENT:
    case OBJ_FIELD:
    case OBJ_SCRIPT:
    {
        std::shared_ptr<CObjectDef> pObjectDef;
        pObjectDef = _GetObjectDef(strItem).lock();

        if(!pObjectDef)
        {
            STD_DBG(_T("Obj[%s] isn't find."), strItem.c_str());
            return S_FALSE;
        }

        partsData.uiDropType = CF_MOCK_PARTS;
        partsData.iDragSrc   = iDragSrc;
        partsData.uuidDef    = pObjectDef->GetPartsId();
        partsData.eProjType  = m_eProjectType;
        partsData.iScript    = -1;
        partsData.effect     = DROPEFFECT_MOVE;
        partsData.bLeftButton   = bLeft;
        partsData.hCutWindow    = m_hWnd;

        if (iImage == OBJ_SCRIPT)
        {
            int iScriptPos;
            iScriptPos = pObjectDef->GetScriptPos(strItem);
            partsData.uiDropType = CF_MOCK_SCRIPT;
            partsData.iScript    = iScriptPos;
        }

        break;
    }

    default:
        STD_DBG(_T("Unknown Data type %d \n"), iImage);

        return S_FALSE;
    }

    HGLOBAL	    hData;
    hData = ::GlobalAlloc(GHND, sizeof(DROP_PARTS_DATA));

    void* pData;

	if (hData == 0)
	{
		return S_FALSE;
	}

    pData = ::GlobalLock(hData);

    if (pData == NULL)
    {
        return S_FALSE;
    }

    ::memcpy(pData, &partsData, sizeof(DROP_PARTS_DATA));

    COleDataSource oleDataSource;
    CDropSource ds;
    oleDataSource.CacheGlobalData(partsData.uiDropType, hData); 

    DROPEFFECT dropEffect = oleDataSource.DoDragDrop( DROPEFFECT_COPY | DROPEFFECT_MOVE , NULL, &ds);


    //TODO: 貼り付け側から WM_USER_PASTE_ENDが送られる



    ::GlobalUnlock(hData);

    ::GlobalFree(hData);
    return S_OK;
}

/**
 * @brief   カット,コピー処理
 * @param   [in] hItem   カット,コピー位置         
 * @param   [in] bCut    マウス座標         
 * @note	 フレームワークより呼び出し
 * @note	
 */
bool CObjectWnd::_CutCopy(HTREEITEM hItem, bool bCut)
{
    using namespace VIEW_COMMON;
    int iImage;
    int iImageSel;
    int iDragSrc;
    DROPEFFECT  effect;
    TREE_DATA<OBJ_TREE_DATA>* pData;
    std::shared_ptr<CProjectCtrl> pProject;
    
    pProject = THIS_APP->GetProject(m_eProjectType).lock();

    if (!pProject)
    {
        return false;
    }

    pData = pProject->GetFolderData(); 

    m_wndObjectView.GetItemImage(hItem, iImage, iImageSel);
    iDragSrc = static_cast<int>(m_wndObjectView.GetItemData(hItem));
    effect = (bCut ? DROPEFFECT_MOVE : DROPEFFECT_COPY);

    OBJ_TREE_DATA* pTreeObj = pData->GetItem(iDragSrc);
    DBG_ASSERT(pTreeObj);


    UINT uiDropType;
    boost::uuids::uuid uuidDef;
    switch(iImage)
    {
    case OBJ_ROOT:
        //ROOTはCUT/COPY不可
        DBG_ASSERT(_T("ROOT"));
        return false;

    case OBJ_FOLDER:
        //FOLDERはCOPY不可とする
        DBG_ASSERT(!bCut);
        uiDropType = CF_MOCK_FOLDER;
        uuidDef    = boost::uuids::nil_uuid();
        break;

    case OBJ_PARTS:
    case OBJ_ELEMENT:
    case OBJ_FIELD:
    case OBJ_REFERENCE:
        uiDropType = CF_MOCK_PARTS;
        break;

    case OBJ_SCRIPT:
        uiDropType = CF_MOCK_SCRIPT;
        break;

    default:
        DBG_ASSERT(_T("UNKNOWWN TYPE"));
        return false;
    }


    DROP_PARTS_DATA partsData;
    memset (&partsData, 0, sizeof(DROP_PARTS_DATA));

    partsData.uiDropType    = uiDropType;
    partsData.iDragSrc      = iDragSrc;
    partsData.eProjType     = m_eProjectType;
    partsData.bLeftButton   = false;
    partsData.uuidDef       = pTreeObj->uuidDef;
    partsData.iScript       = pTreeObj->iScriptNo;
    partsData.effect        = effect;

    if (bCut)
    {
        partsData.hCutWindow    = m_hWnd;
    }
    else
    {
        partsData.hCutWindow    = NULL;
    }


    if( !OpenClipboard() )
    {
        return false;
    }

    if (!EmptyClipboard())
    {
        return false;
    }


    int iSize = sizeof(DROP_PARTS_DATA);
    HGLOBAL hMem = ::GlobalAlloc(GMEM_FIXED, iSize);

    if (!hMem)
    {
        return false;
    }

    LPSTR lpMem = (LPSTR)::GlobalLock(hMem);

    if (!lpMem)
    {
        return false;
    }

    memcpy(lpMem, &partsData, iSize);

    ::GlobalUnlock(hMem);

        
    if (::SetClipboardData(uiDropType, hMem))
    {
	    CloseClipboard();
        return true;
    }
    else
    {
        ::GlobalFree(hMem);
        return false;
    } 

    return true;
}

/**
 * @brief   貼り付け処理
 * @param   [in] hItem   選択ツリー位置
 * @param   [in] iDropEffect 移動 or コピー
 *          DROPEFFECT_COPY or DROPEFFECT_MOVE
 * @retval
 * @note    iDropEffect == DROPEFFECT_NONEの時は
 *          クリップボードのデータに従う
 */
bool CObjectWnd::_Paste(HTREEITEM hItem, int iDropEffect)
{
    DROP_PARTS_DATA* pDropData = NULL;
    HANDLE hData;
    bool bRet = false;
    UINT iClipFormat;

    if (::IsClipboardFormatAvailable(CF_MOCK_PARTS))
    {
        iClipFormat = CF_MOCK_PARTS;
    }
    else if (::IsClipboardFormatAvailable(CF_MOCK_FOLDER))
    {
        iClipFormat = CF_MOCK_FOLDER;
    }
    else if (::IsClipboardFormatAvailable(CF_MOCK_SCRIPT))
    {
        iClipFormat = CF_MOCK_SCRIPT;
    }
    else
    {
        return false;
    }

    if (!OpenClipboard())
    {
        return false;
    }

    try
    {
        hData = ::GetClipboardData(iClipFormat);
        if (!hData) 
        {
            throw (MockException(_T("GetClipboardData(%d)"), iClipFormat));
        }

        pDropData = reinterpret_cast<DROP_PARTS_DATA*>(GlobalLock(hData));

        if(!pDropData)
        {
            throw (MockException(_T("GlobalLock")));
        }

        bRet = _PasteDrop(hItem, pDropData, DROPEFFECT_NONE);

        ::GlobalUnlock(hData);

    }
    catch( MockException& e)
    {
        STD_DBG(e.GetErrMsg().c_str());
    }
    
    CloseClipboard();


    return bRet;

}

/**
 * @brief   Drop Paste 共通処理
 * @param   [in] hItem      選択ツリー位置
 * @param   [in] pDropData  
 * @retval
 * @note    
 */
bool CObjectWnd::_PasteDrop(HTREEITEM hItem, DROP_PARTS_DATA* pDropData, int iDropEffect)
{
    bool bRet = false;

    //---------------------------------------------------
    CViewTree* pTreeDst; 
    TREE_DATA<OBJ_TREE_DATA>* pDataDst;


    pTreeDst = &m_wndObjectView;
    std::shared_ptr<CProjectCtrl> pProject;
    pProject = THIS_APP->GetProject(m_eProjectType).lock();

    if (!pProject)
    {
        throw (MockException(_T("GetProject")));
    }

    pDataDst = pProject->GetFolderData(); 


    int iImageDst;
    int iImageSelDst;
    int iDataDst;

    pTreeDst->GetItemImage(hItem, iImageDst, iImageSelDst);
    iDataDst = static_cast<int>(pTreeDst->GetItemData(hItem));

    TREE_DATA<VIEW_COMMON::OBJ_TREE_DATA>* pData;
    pData = pProject->GetFolderData();
    TREE_DATA_GetExpand<VIEW_COMMON::OBJ_TREE_DATA>(pData, &m_wndObjectView);


    if (iDropEffect == DROPEFFECT_NONE)
    {
        iDropEffect = pDropData->effect;
    }
       
    if (iDropEffect == DROPEFFECT_COPY)
    {
        //----------------------
        // コピー
        //----------------------
        if (pDropData->uiDropType == CF_MOCK_FOLDER)
        {
            //フォルダーのコピーはない
            throw (MockException(_T("Folder copy")));
        }
        else if( pDropData->uiDropType ==  CF_MOCK_PARTS)
        {
            bRet = pProject->CopyParts(pDropData->uuidDef ,iDataDst); 
            STD_ASSERT(bRet);
        }
        else if( pDropData->uiDropType ==  CF_MOCK_SCRIPT)
        {
            //スクリプトのコピーは行わない
            throw (MockException(_T("Script copy")));
        }
    }
    else if(iDropEffect == DROPEFFECT_MOVE)
    {
        //----------------------
        // 移動
        //----------------------

        if (pDropData->uiDropType == CF_MOCK_FOLDER)
        {
            //TODO:実装


        }
        else if( pDropData->uiDropType ==  CF_MOCK_PARTS)
        {
            bRet = THIS_APP->MoveObject(pDropData->uuidDef, 
                                        m_eProjectType, 
                                        iDataDst,
                                        pDropData->hCutWindow); 
            STD_ASSERT(bRet);
        }
        else if( pDropData->uiDropType ==  CF_MOCK_SCRIPT)
        {
            //スクリプトの移動も行わない
            throw (MockException(_T("Script Move")));
        }
    }
    TREE_DATA_SetData<VIEW_COMMON::OBJ_TREE_DATA>(&m_wndObjectView, pData);



    return bRet;

}

/**
 * @brief   TreeIdよりHTREEITEMを取得
 * @param   [in]    iId    仮想キー         
 * @param   [in]    hItem  検索開始位置(通常 TVI_ROOT)         
 * @retval
 * @note	 
 */
HTREEITEM CObjectWnd::_GetHitemFromId(int iId, HTREEITEM hItem)
{
    HTREEITEM hItemNext;
    HTREEITEM hItemRet;

    CViewTree* pTree = &m_wndObjectView;

    if (hItem != TVI_ROOT)
    {
        if(pTree->GetItemData(hItem) == iId)
        {
            return hItem;
        }
    }

	if (hItem == NULL)
	{
		return NULL;
	}

    hItemNext = pTree->GetChildItem(hItem);
    if (hItemNext)
    {
        hItemRet = _GetHitemFromId( iId, hItemNext);

        if (hItemRet)
        {
            return hItemRet;
        }
    }

    hItemNext = pTree->GetNextItem(hItem, TVGN_NEXT);
    if (hItemNext)
    {
        hItemRet = _GetHitemFromId(iId, hItemNext);
        return hItemRet;
    }

    return NULL;
}


/**
 * @brief   マウス移動
 * @param   [in]    nFlags   仮想キー         
 * @param   [in]    point    マウス座標         
 * @retval
 * @note	 フレームワークより呼び出し
 * @note	
 */
void CObjectWnd::OnMouseMove(UINT nFlags, CPoint point) 
{
    if (m_bDrag)
    {
  
        CTreeCtrl* pWndTree = (CTreeCtrl*) &m_wndObjectView;
        ASSERT_VALID(pWndTree);
        HTREEITEM hItem = pWndTree->HitTest(point, NULL);


        // CImageList::DragMove関数を実行（座標はスクリーン座標にする必要あり）
        POINT pt = point;
        ClientToScreen( &pt );
        m_pDragImage->DragMove(pt);

        // ドロップ 可・不可判定
        bool bDrop = false;
        int iImage;
        int iImageSel;

        pWndTree->GetItemImage(hItem, iImage, iImageSel);
        if (( iImage == OBJ_FOLDER)||
            ( iImage == OBJ_ROOT))

        {
            bDrop = true;
        }

        if (bDrop)
        {
            CImageList::DragShowNolock(FALSE);
            //pWndTree->SelectDropTarget(hItem);
            CImageList::DragShowNolock(TRUE);
        }
    }
    CDockablePane::OnMouseMove(nFlags, point);

}

/**
 * @brief   マウス左ボタン解放
 * @param   [in]    nFlags   仮想キー         
 * @param   [in]    point    マウス座標         
 * @retval
 * @note	 フレームワークより呼び出し
 * @note	
 */
void CObjectWnd::OnLButtonUp(UINT nFlags, CPoint point) 
{
    CDockablePane::OnLButtonUp(nFlags, point);
}

/**
 * @brief   ツリーアイテムコピー
 * @param   [in]    hItem     コピー元 
 * @param   [in]    hParent   コピー先 
 * @param   [in]    hInsertAfter    インサート先(TVI_FIRST, TVI_LAST なども使用可) 
 * @retval
 * @note
 * @note	
 */
HTREEITEM CObjectWnd::_CopyItem(HTREEITEM hItem, HTREEITEM hParent, HTREEITEM hInsertAfter)
{
DB_PRINT(_T("CObjectWnd::CopyItem [%d] \n"), m_eProjectType);

// コピー元アイテムの情報をTVITEM構造体に取得する
    CTreeCtrl* pWndTree = (CTreeCtrl*) &m_wndObjectView;
    ASSERT_VALID(pWndTree);

    TVITEM tvItem;
    tvItem.hItem = hItem;
    tvItem.mask = TVIF_CHILDREN | TVIF_STATE | TVIF_PARAM |
        TVIF_HANDLE | TVIF_IMAGE | TVIF_SELECTEDIMAGE;
    pWndTree->GetItem(&tvItem);


    // コピー元アイテムのテキスト情報をTVITEM構造体に取得する
    CString strText = pWndTree->GetItemText(hItem);
    tvItem.cchTextMax = strText.GetLength();
    tvItem.pszText = (LPTSTR)(LPCTSTR)strText;

    // アイテム挿入時のマスクを設定
    tvItem.mask = TVIF_STATE | TVIF_PARAM | TVIF_TEXT |
        TVIF_HANDLE | TVIF_IMAGE | TVIF_SELECTEDIMAGE;

    // TVINSERTSTRUCT構造体を用意し、値を設定
    TVINSERTSTRUCT tvInsert;
    tvInsert.hParent = hParent;
    tvInsert.hInsertAfter = hInsertAfter;
    tvInsert.item = tvItem;

    // コピー先の親アイテム下に新しいアイテムを挿入
    HTREEITEM hRetItem = pWndTree->InsertItem(&tvInsert);

    // コピー元アイテムが子孫を持つ場合にはそれらもコピーする必要がある
    HTREEITEM hChild = pWndTree->GetChildItem(hItem);
    while( hChild )
    {
        _CopyItem(hChild, hRetItem, TVI_SORT);
        hChild = pWndTree->GetNextSiblingItem(hChild);
    }

    return hRetItem;
}

/**
 *  @brief  スクリプト生成UI更新
 *  @param  [in out] コマンド ユーザー インターフェイス
 *  @retval なし
 *  @note   
 */
void CObjectWnd::OnUpdateMenuScript(CCmdUI* pCmdUI)
{
    CTreeCtrl* pWndTree = (CTreeCtrl*) &m_wndObjectView;
    ASSERT_VALID(pWndTree);

    HTREEITEM hTreeItem = pWndTree->GetSelectedItem();

    if (hTreeItem == NULL)
    {
        return;
    }
    //クリック項目に応じてメニューを更新
    int iImage;
    int iImageSel;

    //
    pWndTree->GetItemImage(hTreeItem, iImage, iImageSel);
    pCmdUI->Enable((iImage == OBJ_PARTS) ||
                   (iImage == OBJ_REFERENCE) ||
                   (iImage == OBJ_FIELD) ||
                   (iImage == OBJ_SCRIPT));
}

/**
 *  @brief  切り取りUI更新
 *  @param  [in out] コマンド ユーザー インターフェイス
 *  @retval なし
 *  @note   
 */
void CObjectWnd::OnUpdateMenuCut(CCmdUI* pCmdUI)
{
    CTreeCtrl* pWndTree = (CTreeCtrl*) &m_wndObjectView;
    ASSERT_VALID(pWndTree);

    HTREEITEM hTreeItem = pWndTree->GetSelectedItem();

    if (hTreeItem == NULL)
    {
        return;
    }
    //クリック項目に応じてメニューを更新
    int iImage;
    int iImageSel;
    pWndTree->GetItemImage(hTreeItem, iImage, iImageSel);

    bool bEnable = false;
    switch(iImage)
    {
    case OBJ_FOLDER:
    case OBJ_ROOT:
    case OBJ_PARTS:
    case OBJ_REFERENCE:
    case OBJ_ELEMENT:
    case OBJ_FIELD:
        bEnable = true;
        break;

    default:
        break;
    }

    pCmdUI->Enable(bEnable);
}

/**
 *  @brief  コピーUI更新
 *  @param  [in out] コマンド ユーザー インターフェイス
 *  @retval なし
 *  @note   
 */
void CObjectWnd::OnUpdateMenuCopy(CCmdUI* pCmdUI)
{
    CTreeCtrl* pWndTree = (CTreeCtrl*) &m_wndObjectView;
    ASSERT_VALID(pWndTree);

    HTREEITEM hTreeItem = pWndTree->GetSelectedItem();

    if (hTreeItem == NULL)
    {
        return;
    }

    //クリック項目に応じてメニューを更新
    int iImage;
    int iImageSel;

    
    pWndTree->GetItemImage(hTreeItem, iImage, iImageSel);

    bool bEnable = false;

    switch(iImage)
    {
    case OBJ_PARTS:
    case OBJ_REFERENCE:
    case OBJ_ELEMENT:
    case OBJ_FIELD:
        bEnable = true;
        break;

    case OBJ_FOLDER:
    case OBJ_ROOT:
    default:
        break;
    }
    pCmdUI->Enable(bEnable);
}

/**
 *  @brief  貼り付けUI更新
 *  @param  [in out] コマンド ユーザー インターフェイス
 *  @retval なし
 *  @note   
 */
void CObjectWnd::OnUpdateMenuPaste(CCmdUI* pCmdUI)
{
    CTreeCtrl* pWndTree = (CTreeCtrl*) &m_wndObjectView;
    ASSERT_VALID(pWndTree);

    HTREEITEM hTreeItem = pWndTree->GetSelectedItem();

    if (hTreeItem == NULL)
    {
        return;
    }

    //クリック項目に応じてメニューを更新
    int iImage;
    int iImageSel;
    bool bEnable = false;


    //
    pWndTree->GetItemImage(hTreeItem, iImage, iImageSel);
    switch(iImage)
    {
    case OBJ_FOLDER:
    case OBJ_ROOT:
        if ( (::IsClipboardFormatAvailable(CF_MOCK_PARTS)) ||
             (::IsClipboardFormatAvailable(CF_MOCK_FOLDER)))
        {
            bEnable = true;
        }
        break;

    case OBJ_PARTS:
    case OBJ_REFERENCE:
    case OBJ_ELEMENT:
    case OBJ_FIELD:
        if (::IsClipboardFormatAvailable(CF_MOCK_PARTS))
        {
            bEnable = true;
        }
        break;

    default:
        break;
    }
    pCmdUI->Enable(bEnable);
}

/**
 *  @brief  削除UI更新
 *  @param  [in out] コマンド ユーザー インターフェイス
 *  @retval なし
 *  @note   
 */
void CObjectWnd::OnUpdateMenuDelete(CCmdUI* pCmdUI)
{
    CTreeCtrl* pWndTree = (CTreeCtrl*) &m_wndObjectView;
    ASSERT_VALID(pWndTree);

    HTREEITEM hTreeItem = pWndTree->GetSelectedItem();

    if (hTreeItem == NULL)
    {
        return;
    }

    //クリック項目に応じてメニューを更新
    int iImage;
    int iImageSel;

    //
    pWndTree->GetItemImage(hTreeItem, iImage, iImageSel);

    //!< 選択中アイテムのDrawPartsDef取得
    //std::shared_ptr<CObjectDef> pDef;
    //pDef = GetCurrentObjectDef();

    bool bEnable = true;

    if (hTreeItem == pWndTree->GetRootItem())
    {
        bEnable = false;
    }

    pCmdUI->Enable(bEnable);
}


/**
 * @brief    Project名更新
 * @param    なし
 * @retval   0
 * @note	
 */
LRESULT CObjectWnd::OnViewUpdateProj(WPARAM wparam, LPARAM lparam)
{
    CTreeCtrl* pWndTree = (CTreeCtrl*) &m_wndObjectView;
    ASSERT_VALID(pWndTree);

    VIEW_COMMON::E_PROJ_TYPE eType;
    eType = static_cast<VIEW_COMMON::E_PROJ_TYPE>(wparam);

    if (eType != m_eProjectType)
    {
        return 0;
    }

    std::shared_ptr<CProjectCtrl> pProject;
    pProject = THIS_APP->GetProject(m_eProjectType).lock();
    if (!pProject)
    {
        return 0;
    }

    _ReadTreeData();

    StdString strProjName = pProject->GetName();
     
    HTREEITEM hRoot = pWndTree->GetRootItem();
 
    pWndTree->SetItemText(hRoot, strProjName.c_str());

    return 0;
}

/**
 * @brief   ビュークリア
 * @param   [in] wParam 使用しない
 * @param   [in] lParam 使用しない
 * @return  なし
 * @note    
 */
LRESULT CObjectWnd::OnViewClear(WPARAM pMessage, LPARAM lParam)
{
    return 0;
}

/**
 * @brief   ICON種別取得
 * @param   [in] pDef 
 * @return  アイコン種別
 * @note    
 */
E_ITEM_TYPE CObjectWnd::_GetIconType(CObjectDef* pDef)
{
    int iType = pDef->GetObjectType();
    switch (iType)
    {
    case E_PARTS:       {return OBJ_PARTS;}
    case E_FIELD:       {return OBJ_FIELD;}
    case E_ELEMENT:     {return OBJ_ELEMENT;}
    case E_REFERENCE:   {return OBJ_REFERENCE;}
    }

    STD_DBG(_T("type = %d"), iType);
    return OBJ_PARTS;
}

/**
 * @brief   オブジェクト追加通知
 * @param   [in] pObject 追加するオブジェクト
 * @param   [in] lParam 使用しない
 * @return  なし
 * @note    
 */
 LRESULT CObjectWnd::OnViewAddObject(WPARAM pObject, LPARAM lparam)
 {
    std::shared_ptr<CProjectCtrl> pProject;
    pProject = THIS_APP->GetProject(m_eProjectType).lock();

    if (!pProject)
    {
        return 0;
    }

    CTreeCtrl* pWndTree = (CTreeCtrl*) &m_wndObjectView;
    ASSERT_VALID(pWndTree);

    //プロジェクト
    HTREEITEM hRoot = pWndTree->GetRootItem();
    HTREEITEM hCurrent = pWndTree->GetNextItem(hRoot, TVGN_CHILD);


    //!< 部品の取得
    CObjectDef* pDef = reinterpret_cast<CObjectDef*>(pObject);
    STD_ASSERT( pDef != NULL);

    HTREEITEM  hObject = 0;

    E_ITEM_TYPE     eIcon;
    eIcon = _GetIconType(pDef);
 

    hObject = m_wndObjectView.InsertItem(pDef->GetName().c_str(), 
                                              eIcon, eIcon, hRoot, TVI_LAST);

    //!< 部品付属のスクリプト設定
    int iScriptNum = pDef->GetScriptNum();
    StdString strScript;
    for (int iCnt = 0; iCnt < iScriptNum; iCnt++)
    {
        strScript = pDef->GetScriptInstance(iCnt)->GetName();
        m_wndObjectView.InsertItem(strScript.c_str(), OBJ_SCRIPT, OBJ_SCRIPT, hObject);
    }

    RedrawWindow();
    //_WriteTreeData();
    return 0;
 }

/**
 * @brief   オブジェクトステータス変更
 * @param   [in] wParam 使用しない
 * @param   [in] lParam 使用しない
 * @return  なし
 * @note    
 */
 LRESULT CObjectWnd::OnChangeObjectStatus(WPARAM wParam, LPARAM lparam)
 {
    m_wndObjectView.Invalidate();
    m_wndObjectView.UpdateWindow();
    return 0;
 }

/**
 * @brief   オブジェクト更新
 * @param   [in] pObject オブジェクト
 * @param   [in] lParam 使用しない
 * @return  なし
 * @note    
 */
 LRESULT CObjectWnd::OnUpdateObject(WPARAM pObject, LPARAM lparam)
 {

     return 0;
 }


  /**
  * @brief   ド右ボタンドラッグ時のメニュー表示
  * @param   [in] ptTree 表示位置
  * @retval  
  * @note	
  */
int CObjectWnd::_DispDropMenuRightButton(CPoint ptTree)
{
    //-----------------------------------------------
    // 右ボタンでドロップした時は移動かコピーを
    // ポップアップメニューで選択できるようにする。
    //-----------------------------------------------

    ClientToScreen(&ptTree);
    int iDropEffect = DROPEFFECT_NONE;

    CMenu menu;
    menu.CreatePopupMenu();

    UINT nFlagCopy = MF_STRING | MF_ENABLED;
    UINT nFlagMove = MF_STRING | MF_ENABLED;

    //TODO:暫定処理
    menu.AppendMenu(nFlagCopy, 1, _T("ここにコピー"));
    menu.AppendMenu(nFlagMove, 2, _T("ここに移動"));
 
                
    UINT uiMenu;
    uiMenu = menu.TrackPopupMenu(
        TPM_RETURNCMD  |    // メニュー項目を選択しても通知メッセージを送らない
        TPM_NONOTIFY   |    // 関数の戻り値としてメニューＩＤを返す
        TPM_LEFTALIGN  |    // クリック時のX座標をメニューの左辺にする
        TPM_RIGHTBUTTON,    // 右クリックでメニュー選択可能とする

        ptTree.x, 
        ptTree.y, this);


    menu.DestroyMenu();

    if (uiMenu == 1)
    {
        iDropEffect = DROPEFFECT_COPY;
    }
    else if (uiMenu == 2)
    {
        iDropEffect = DROPEFFECT_MOVE;
    }
    //-----------------------------------------------


    return iDropEffect;
}


 /**
  * @brief   ドロップ完了通知
  * @param   [in] wParam 0
  * @param   [in] lParam ドロップデータ (DROP_PARTS_DATA) 
  * @retval  
  * @note	
  */
LRESULT CObjectWnd::OnDropEnd(WPARAM type, LPARAM lParam)
{
    DROP_TARGET_DATA*  pTargetData;
    pTargetData = reinterpret_cast<DROP_TARGET_DATA*>(lParam);

    HGLOBAL hData;
    DROP_PARTS_DATA* pPartsData;


    HTREEITEM hDrop = m_wndObjectView.GetDropHilightItem();
    
    // HTREEITEM hDst;
	// UINT uFlags;
    //hDst = m_wndObjectView.HitTest(pTargetData->ptDrop, &uFlags);
    int iDropEffect = DROPEFFECT_MOVE;

    if (type == CF_MOCK_PARTS)
    {
        m_wndObjectView.SelectDropTarget(NULL);

        hData = pTargetData->pDataObject->GetGlobalData(CF_MOCK_PARTS);

        pPartsData = reinterpret_cast<DROP_PARTS_DATA*>(::GlobalLock(hData));

        if((pTargetData->dwKeyState & MK_RBUTTON))
        {
            iDropEffect = _DispDropMenuRightButton(pTargetData->ptDrop);
        }

        _PasteDrop(hDrop, pPartsData, iDropEffect);
        GlobalUnlock(hData);

    }
    else if (type == CF_MOCK_FOLDER)
    {
        hData = pTargetData->pDataObject->GetGlobalData(CF_MOCK_FOLDER);
        pPartsData = reinterpret_cast<DROP_PARTS_DATA*>(::GlobalLock(hData));
        _PasteDrop(hDrop, pPartsData, iDropEffect);
        GlobalUnlock(hData);
    }
    else if (type == CF_MOCK_SCRIPT)
    {
        hData = pTargetData->pDataObject->GetGlobalData(CF_MOCK_SCRIPT);
        pPartsData = reinterpret_cast<DROP_PARTS_DATA*>(::GlobalLock(hData));
        _PasteDrop(hDrop, pPartsData, iDropEffect);
        GlobalUnlock(hData);
    }
    else
    {

    }

    return S_OK;
}

/**
 * @brief   ドロップ通過開始通知
 * @param   [in] wParam 0
 * @param   [in] lParam ドロップデータ (DROP_PARTS_DATA) 
 * @retval  
 * @note	
 */
LRESULT CObjectWnd::OnDropEnter(WPARAM type, LPARAM lParam)
{
    DROP_TARGET_DATA*  pTargetData;
    pTargetData = reinterpret_cast<DROP_TARGET_DATA*>(lParam);
    return 0;
}


/**
 * @brief   ドロップ通過通知
 * @param   [in] wParam 0
 * @param   [in] lParam ドロップデータ (DROP_PARTS_DATA) 
 * @retval  
 * @note	
 */
LRESULT CObjectWnd::OnDropOver(WPARAM type, LPARAM lParam)
{
    DROP_TARGET_DATA*  pTargetData;
    pTargetData = reinterpret_cast<DROP_TARGET_DATA*>(lParam);

    CViewTree* pTree = &m_wndObjectView;

    if (type == CF_MOCK_PARTS)
    {
        HTREEITEM hitem;

        hitem = pTree->HitTest(pTargetData->ptDrop);

        if (!hitem)
        {
            return DROPEFFECT_NONE;
        }

        int iImage;
        int iImageSel;
        pTree->GetItemImage(hitem, iImage, iImageSel);


        if ((iImage == OBJ_ROOT) ||
            (iImage == OBJ_FOLDER))
        {
            pTree->SelectDropTarget(hitem);
            return DROPEFFECT_MOVE;
        }
        else if ((iImage == OBJ_PARTS)||
                 (iImage == OBJ_FIELD)||
                 (iImage == OBJ_REFERENCE)) 
        {
            pTree->SelectDropTarget(hitem);
            return DROPEFFECT_MOVE;
        }
    }
    else if (type == CF_MOCK_FOLDER)
    {
        if (!OpenClipboard())
        {
            return 0;
        }

        int iRet = DROPEFFECT_NONE;
        try
        {
            HANDLE hData;
            DROP_PARTS_DATA*  pDropData;
            hData = ::GetClipboardData(CF_MOCK_FOLDER);
            if (!hData) 
            {
                throw (MockException(_T("GetClipboardData")));
            }

            pDropData = reinterpret_cast<DROP_PARTS_DATA*>(GlobalLock(hData));

            if(!pDropData)
            {
                throw (MockException(_T("GlobalLock")));
            }

            if (pDropData->hCutWindow == m_hWnd)
            {
                HTREEITEM hitem;

                hitem = pTree->HitTest(pTargetData->ptDrop);

                if (hitem)
                {
                    int iImage;
                    int iImageSel;
                    pTree->GetItemImage(hitem, iImage, iImageSel);


                    if ((iImage == OBJ_ROOT) ||
                        (iImage == OBJ_FOLDER))
                    {
                        pTree->SelectDropTarget(hitem);
                        iRet = DROPEFFECT_MOVE;
                    }
                }
            }
            GlobalUnlock( hData );
        }
        catch( MockException& e)
        {
            STD_DBG(e.GetErrMsg().c_str());
        }
        CloseClipboard();
        return iRet;
    }
    pTree->SelectDropTarget(NULL);

    return 0;
}

/**
 * @brief   ドロップ通過完了
 * @param   [in] wParam 0
 * @param   [in] lParam pWnd
 * @retval  
 * @note	
 */
LRESULT CObjectWnd::OnDropLeave(WPARAM type, LPARAM lParam)
{
    m_wndObjectView.SelectDropTarget(NULL);
    return 0;
}

/**
 * @brief   Paste終了
 * @param   [in] wParam 0
 * @param   [in] lParam 0
 * @retval  
 * @note	
 */
LRESULT CObjectWnd::OnUserPasteEnd(WPARAM type, LPARAM lParam)
{
    _ReadTreeData();
    return 0;
}


/**
 * @brief   インサートマーク処理
 * @param   [in] point   カーソルの座標
 * @param   [in] iType   ドロップデータ種別
 * @retval  
 * @note	
 */
/*
void CObjectWnd::_SetInsertMarkProc(CPoint point, UINT iType)
{
	HTREEITEM hitem;
	UINT uFlags;
	RECT lpRect;

    POINT  ptWinPos;
    POINT  ptCurPos;
    CRect  rcWin;

	//各ウィンドウの座標系に変換
	m_wndObjectView->GetCursorPos(&ptCurPos);
    ptWinPos = ptCurPos;
    ::ScreenToClient(m_hWnd , &ptWinPos);
    ::GetClientRect(m_hWnd , rcWin);

	/
    //マウスカーソル下のアイテム取得
	hitem = HitTest(point, &uFlags);

    int iImage;
    int iImageSel;

    m_wndObjectView->GetItemImage(hitem, iImage, iImageSel);

    if (iImage == OBJ_SCRIPT)
    {
        m_wndObjectView->SetInsert( 0, CViewTree::E_NONE);
        return;
    }

    if ((iType == CF_MOCK_PARTS)||
        (iType == CF_MOCK_FOLDER))
    {
        if ( (iImage == OBJ_FOLDER) ||
             (iImage == OBJ_ROOT))
        {
            m_wndObjectView->SetInsert(hitem, CViewTree::E_SEL);
            return;

        }
    }

    if (iType == CF_MOCK_FOLDER)
    {
        m_wndObjectView->SetInsert( 0, CViewTree::E_NONE);
        return;
    }


        OBJ_FOLDER  = 0,  //!< フォルダー   
        OBJ_PARTS  = 1,  //!< 図形   
        OBJ_SCRIPT  = 2,  //!< スクリプト  
        OBJ_ELEMENT = 3,  //!< エレメント
        OBJ_FIELD   = 4,  //!< フィールド
        OBJ_EDGE    = 5,  //!< エッジ
        OBJ_JOINT   = 6,  //!< ジョイント
        OBJ_INDICATOR   = 7,  //!< インジケータ
        OBJ_ROOT        = 8,  //!< ルート

    pCmdUI->Enable((iImage == OBJ_PARTS) ||
                   (iImage == OBJ_SCRIPT));


	m_wndObjectView->GetItemRect( hitem, &lpRect, true);
	
	if ((lpRect.top <= point.y) && 
		(point.y <= (lpRect.top + (lpRect.bottom - lpRect.top) / 2)))
	{
		//アイテムの上側に挿入マークを表示
		m_wndObjectView->SetInsert(hitem, CViewTree::E_UP);
	}
	else
	{
		//アイテムの下側に挿入マークを表示
		m_wndObjectView->SetInsert(hitem, CViewTree::E_DOWN);
	}

	//リストの末尾より下に挿入の場合
	if(rcWin.PtInRect(ptWinPos) && hitem == NULL)
	{
		//ツリーの末尾に挿入マークを表示
		UINT uiCnt;
		hitem = m_wndObjectView->GetFirstVisibleItem();
		for(uiCnt = 1; uiCnt < GetCount(); uiCnt++)
		{
			hitem = m_wndObjectView->GetNextVisibleItem(hitem);
		}
		m_wndObjectView->SetInsert(hitem, CViewTree::E_DOWN);
	}
}
*/

//*******************************
//             TEST
//*******************************
#ifdef _DEBUG

#include "Utility/TestDlg.h"

namespace TEST_OBJECT_WND
{
    TREE_DATA<VIEW_COMMON::OBJ_TREE_DATA> treeData1;
    TREE_DATA<VIEW_COMMON::OBJ_TREE_DATA> treeData2;
    CTreeCtrl   ctrlTree;
    CTestDlg* g_pDlg;

void TEST_CObjectWnd_Read_Write_OnPbTest(CWnd* pWnd)
{
    //!< Treeデータ反映
    //CObjectWnd::ReadTreeDataDirect(&ctrlTree, &treeData1);
    //CObjectWnd::WriteTreeDataDirect(&treeData2, &ctrlTree);
}

void TEST_CObjectWnd_Read_Write_OnInit(CWnd* pWnd)
{
    CRect rect(0,0,100,200);
    ctrlTree.Create(WS_VISIBLE | WS_TABSTOP | WS_CHILD | WS_BORDER, rect, g_pDlg, 100);
    TEST_CObjectWnd_Read_Write_OnPbTest(pWnd);
    g_pDlg->Exit();
}

/*
void TEST_CObjectWnd_Read_Write()
{
    

    VIEW_COMMON::OBJ_TREE_DATA  objData;
    objData.strObj = _T("PROJECT");
    objData.iImage = OBJ_FOLDER;

    int iRoot = treeData1.AddData(-1, objData);

    objData.strObj = _T("MAIN");
    objData.iImage = OBJ_PARTS;
    int iItem1 = treeData1.AddChild(iRoot, objData);

    objData.strObj = _T("Script");
    objData.iImage = OBJ_SCRIPT;
    int iItem2 = treeData1.AddChild(iItem1, objData);

    objData.strObj = _T("Paers000");
    objData.iImage = OBJ_PARTS;
    int iItem3 = treeData1.AddData(iItem1, objData);

    objData.strObj = _T("Script");
    objData.iImage = OBJ_SCRIPT;
    int iItem4 = treeData1.AddChild(iItem3, objData);

    
    CTestDlg dlgTest;

    dlgTest.SetObj( &ctrlTree);
    dlgTest.SetOnInit( TEST_CObjectWnd_Read_Write_OnInit );
    dlgTest.SetOnPbTest( TEST_CObjectWnd_Read_Write_OnPbTest );
    dlgTest.EnableAutoFit();
    g_pDlg = &dlgTest;
    dlgTest.DoModal();


    STD_ASSERT( treeData2.GetItem(iRoot)->strObj == _T("PROJECT"));
    STD_ASSERT( treeData2.GetItem(iItem1)->strObj == _T("MAIN"));
    STD_ASSERT( treeData2.GetItem(iItem2)->strObj == _T("Script"));
    STD_ASSERT( treeData2.GetItem(iItem3)->strObj == _T("Paers000"));
    STD_ASSERT( treeData2.GetItem(iItem4)->strObj == _T("Script"));
}
*/

};


void TEST_CObjectWnd()
{
    STD_DBG(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    //スキップ
    //TEST_OBJECT_WND::TEST_CObjectWnd_Read_Write();


    STD_DBG(_T("%s End\n"), DB_FUNC_NAME.c_str());
    STD_DBG(_T("\n"));

}

#endif //_DEBUG