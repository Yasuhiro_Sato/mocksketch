/**
 * @brief			CObjectWndヘッダーファイル
 * @file			CObjectWnd.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

#ifndef _OBJCETVIEW_H__
#define _OBJCETVIEW_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "Utility/ViewTree.h"
#include "Utility/TreeData.h"
#include "Utility/DropTarget.h"
#include "View/ViewCommon.h"
#include "ToolBar/CustomToolBar.h"
#include "CustomDocablePane.h"
#include "COMMON_HEAD.h"

/*---------------------------------------------------*/
/*  class                                            */
/*---------------------------------------------------*/
class   CPartsDef;
class   CObjectDef;
class   CProjectCtrl;

/**
 * @class   CObjectWnd
 * @brief                        
 */
class CObjectWndToolBar : public CCustomToolBar
{
public:
    CObjectWndToolBar():CCustomToolBar(){
        SetName(_T("CObjectWndToolBar"));
    }

	virtual void OnUpdateCmdUI(CFrameWnd* /*pTarget*/, BOOL bDisableIfNoHndler)
	{
		CCustomToolBar::OnUpdateCmdUI((CFrameWnd*) GetOwner(), bDisableIfNoHndler);
	}

	virtual BOOL AllowShowOnList() const { return FALSE; }
};

//====================================================================

/**
 * @class   CObjectWnd
 * @brief                        
 */
class CObjectWnd : public CCustomDocablePane
{
public:

    //!< コンストラクタ
	CObjectWnd();
    CObjectWnd(VIEW_COMMON::E_PROJ_TYPE eType);

    void SetProjectType(VIEW_COMMON::E_PROJ_TYPE eType);
	void AdjustLayout();
	void OnChangeVisualStyle();

    //!< オブジェクト表示
    void FillObjet();
    
    //!< Treeデータ取得
    //static void WriteTreeDataDirect(TREE_DATA<VIEW_COMMON::OBJ_TREE_DATA>* pTreeData,
    //                        const CTreeCtrl* pWndTree);


    //!< Treeデータ反映
    //static void ReadTreeDataDirect(CTreeCtrl* pWndTree,
    //      const TREE_DATA<VIEW_COMMON::OBJ_TREE_DATA>* pTreeData);

    void OpenFile(bool bReload);


protected:

    //!< 描画処理コールバック
    static void _DrawCallback(CDC* pCdc, HTREEITEM hTree, void* pVoid);

    static void _DrawExpand(CDC* pDc, CRect rcButton, VIEW_COMMON::EXPAND_TYPE eExpand );

// 属性
protected:

 	CObjectWndToolBar m_wndToolBar;

    CViewTree m_wndObjectView;

	CImageList m_ObjectImages;


    //!< ドラッグイメージを格納するポインタ
	CImageList*  m_pDragImage;  

    //!< ドラッグ中ならTRUE、ドラッグ中でなければFALSE
	bool         m_bDrag;

    //!< ドラッグアイテム（コピー元アイテム）のハンドル
	HTREEITEM    m_hitemDrag;

    //!< 自身へのドラッグ処理
    CDropTarget  m_dropTarget;

    //!< プロジェクト種別
    VIEW_COMMON::E_PROJ_TYPE    m_eProjectType;

    static std::vector<int>     m_lstIcon;

    //!< 表示アイコンの幅(pixl)
    int    m_iIconWidth;

protected:


// 実装
public:
	virtual ~CObjectWnd();

protected:
    friend CObjectWnd;

    //!< Treeデータ取得
    void _ReadTreeData();

    //!< ツリーアイテムコピー
    HTREEITEM _CopyItem(HTREEITEM hItem, HTREEITEM hParent, HTREEITEM hInsertAfter);
    
    //!< ツリー検索
    HTREEITEM  _GetTreeItem(LPCTSTR strItem, HTREEITEM hTreeRoot, VIEW_COMMON::E_ITEM_TYPE eType);

    //!< 選択中アイテムのDrawPartsDef取得
    std::weak_ptr<CObjectDef>  _GetCurrentObjectDef() const;

    //!< CObjectDef取得
    std::weak_ptr<CObjectDef>  _GetObjectDef(StdString strObjName)  const;

    //!< PartsId取得
    boost::uuids::uuid  _GetObjectPartsId(StdString strObjName) const;

    bool _CreateParts(VIEW_COMMON::E_OBJ_DEF_TYPE eType);

    //!< Scriptデータ削除
    void _DeleteScript(CObjectDef*  pCurrentObjectDef,
                               StdString strScript);

    //!< ICON種別取得
    VIEW_COMMON::E_ITEM_TYPE _GetIconType(CObjectDef* pDef);


    //!< 部品データ削除
    void _DeleteParts(CObjectDef*  pObjectDef);

    //!< 親フォルダ
    HTREEITEM  _GetParentFolder(HTREEITEM hTree);

    //!< フォルダ取得
    HTREEITEM  _GetFolder(HTREEITEM hTree);

    //!< ドラッグ開始処理
    LRESULT _Begindrag(NMHDR* pNMHDR, bool bLeft);

    //!< カット,コピー処理
    bool _CutCopy(HTREEITEM hItem, bool bCut);

    //!< ペースト処理
    bool _Paste(HTREEITEM hItem, int iDropEffect = DROPEFFECT_NONE);

    //!< ドロップ、ペースト共通処理
    bool _PasteDrop(HTREEITEM hItem, DROP_PARTS_DATA* pDropData, int iDropEffect);

    //!< 右ボタンドラッグ時のメニュー表示
    int _DispDropMenuRightButton(CPoint ptTree);

    HTREEITEM _GetHitemFromId(int iId, HTREEITEM hItem = TVI_ROOT);


    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
    afx_msg void OnSize(UINT nType, int cx, int cy);
    afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
    afx_msg void OnProperties();
    afx_msg void OnFileOpen();
    afx_msg void OnFileOpenWith();
    /*
    afx_msg void OnDummyCompile();
    afx_msg void OnEditCut();
    afx_msg void OnEditCopy();
    afx_msg void OnEditClear();
    */
    afx_msg void OnCreateFolder();
    afx_msg void OnChangeName   ();
    afx_msg void OnObjectSave   ();
    afx_msg void OnCreateParts  ();
    afx_msg void OnCreateReference  ();
    afx_msg void OnCreateField  ();
    afx_msg void OnCreateElement();

    afx_msg void OnCreateScript();
    afx_msg void OnOpen();
    afx_msg void OnCut();
    afx_msg void OnCopy();
    afx_msg void OnPaste();
    afx_msg void OnDelete();

    afx_msg void OnUpdateMenuScript(CCmdUI* pCmdUI);

    afx_msg void OnUpdateMenuCut(CCmdUI* pCmdUI);
    afx_msg void OnUpdateMenuCopy(CCmdUI* pCmdUI);
    afx_msg void OnUpdateMenuPaste(CCmdUI* pCmdUI);
    afx_msg void OnUpdateMenuDelete(CCmdUI* pCmdUI);
    afx_msg void OnUpdateObjectSave(CCmdUI* pCmdUI);
    


    afx_msg void OnPaint();
    afx_msg void OnSetFocus(CWnd* pOldWnd);

    //!< View生成通知
    afx_msg LRESULT OnViewCreate(WPARAM iType, LPARAM pMessage);

    //!< ロード完了通知
    afx_msg LRESULT OnViewLoadEnd(WPARAM pMessage, LPARAM lParam);

    //!< View活性化通知
    afx_msg LRESULT OnViewActivate(WPARAM pMessage, LPARAM lParam);

    //!< プロジェクト名更新通知
    afx_msg LRESULT OnViewUpdateProj(WPARAM wparam, LPARAM lparam);

    //!< オブジェクト追加通知
    afx_msg LRESULT OnViewAddObject(WPARAM wparam, LPARAM lparam);

    //!< オブジェクト更新
    afx_msg LRESULT OnUpdateObject(WPARAM wparam, LPARAM lparam);
    
    //!< オブジェクトステータス変更
    afx_msg LRESULT OnChangeObjectStatus(WPARAM wparam, LPARAM lparam);

    //!< ダブルクリック
    afx_msg void OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult);

    //!< 編集終了
    afx_msg void OnEndLabelEdit(NMHDR* pNMHDR, LRESULT* pResult);


    //!< マウス右ボタン解放
    afx_msg void OnLButtonUp(UINT nFlags, CPoint point);

    //!< マウス移動
    afx_msg void OnMouseMove(UINT nFlags, CPoint point);

    
    //!<ビュークリア
    afx_msg LRESULT OnViewClear(WPARAM pMessage, LPARAM lParam);

    //-------------------
    // 
    //-------------------

    //!< ドラッグ開始
    afx_msg void OnBegindrag(NMHDR* pNMHDR, LRESULT* pResult);

    //!< 右ボタンドラッグ開始
    afx_msg void OnBegindragRight(NMHDR* pNMHDR, LRESULT* pResult);

    //!<ドロップ開始
    afx_msg  LRESULT OnDropEnter(WPARAM type, LPARAM lParam);

    //!<ドロップ完了
    afx_msg LRESULT OnDropEnd(WPARAM type, LPARAM lParam);

    //!< ドロップ通過
    afx_msg LRESULT OnDropOver(WPARAM pMessage, LPARAM lParam);

    //!< ドロップ通過完了
    afx_msg LRESULT OnDropLeave(WPARAM pMessage, LPARAM lParam);

    //!< Paste終了
    afx_msg LRESULT OnUserPasteEnd(WPARAM pMessage, LPARAM lParam);


	DECLARE_MESSAGE_MAP()
};

#endif // _OBJCETVIEW_H__
