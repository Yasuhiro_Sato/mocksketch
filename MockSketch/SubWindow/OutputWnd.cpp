/**
 * @brief			COutputWnd実装ファイル
 * @file			COutputWnd.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *	
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/

#include "stdafx.h"

#include "./OutputWnd.h"
#include "Resource.h"
#include "resource2.h"
#include "MainFrm.h"
#include "ExecCommon.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

BEGIN_MESSAGE_MAP(COutputWnd, CDockablePane)
	ON_WM_CREATE()
	ON_WM_SIZE()
    ON_MESSAGE(WM_MODE_CHANGE, OnModeChange)
END_MESSAGE_MAP()

/**
*  @brief  コンストラクター.
*  @param  なし
*  @retval なし     
*  @note
*/
COutputWnd::COutputWnd()
{
}

/**
*  @brief  デストラクタ.
*  @param  なし
*  @retval なし     
*  @note
*/
COutputWnd::~COutputWnd()
{
}


/**
 * 生成時処理要求
 * @param [in]    lpCreateStruct	初期化パラメータへのポインタ	 	  
 * @return		
 * @note	 
 * @exception   なし
 */
int COutputWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (CDockablePane::OnCreate(lpCreateStruct) == -1)
        return -1;

    m_Font.CreateStockObject(DEFAULT_GUI_FONT);

    CRect rectDummy;
    rectDummy.SetRectEmpty();

    // タブ付きウィンドウの作成:
    if (!m_wndTabs.Create(CMFCTabCtrl::STYLE_FLAT, rectDummy, this, 1))
    {
        STD_DBG(_T("m_wndTabs Create fail"));
        return -1;      // 作成できない場合
    }

    // 出力ペインの作成:
    const DWORD dwStyle = LBS_NOINTEGRALHEIGHT | WS_CHILD | WS_VISIBLE | WS_HSCROLL | WS_VSCROLL;

    if (!m_wndOutputBuild.Create(dwStyle, rectDummy, &m_wndTabs, 2) ||
        !m_wndOutputDebug.Create(dwStyle, rectDummy, &m_wndTabs, 3) ||
        !m_wndOutputFind.Create(dwStyle, rectDummy, &m_wndTabs, 4))
    {
        STD_DBG(_T("Output Create fail"));
        return -1;      // 作成できない場合
    }

    m_wndOutputBuild.SetFont(&m_Font);
    m_wndOutputDebug.SetFont(&m_Font);
    m_wndOutputFind.SetFont(&m_Font);

    CString strTabName;
    BOOL bNameValid;

    // 一覧ウィンドウをタブに割り当てます:
    bNameValid = strTabName.LoadString(IDS_BUILD_TAB);
    ASSERT(bNameValid);
    m_wndTabs.AddTab(&m_wndOutputBuild, strTabName, (UINT)WIN_BUILD);
    bNameValid = strTabName.LoadString(IDS_DEBUG_TAB);
    ASSERT(bNameValid);
    m_wndTabs.AddTab(&m_wndOutputDebug, strTabName, (UINT)WIN_DEBUG);
    bNameValid = strTabName.LoadString(IDS_FIND_TAB);
    ASSERT(bNameValid);
    m_wndTabs.AddTab(&m_wndOutputFind, strTabName, (UINT)WIN_FIND);

    return 0;
}

/**
 *  @brief ウインドウサイズ変更
 *  @param [in] nType   要求されるサイズ変更のタイプ  (SIZE_MAXIMIZED....)
 *  @param [in] cx      クライアント領域の新しい幅 
 *  @param [in] cy      クライアント領域の新しい高さ
 *  @retval           なし
 *  @note
 */
void COutputWnd::OnSize(UINT nType, int cx, int cy)
{
	CDockablePane::OnSize(nType, cx, cy);

	// タブ コントロールは、クライアント領域全体をカバーする必要があります:
	m_wndTabs.SetWindowPos (NULL, -1, -1, cx, cy, SWP_NOMOVE | SWP_NOACTIVATE | SWP_NOZORDER);
}

/**
 *  @brief 水平スクロール調整
 *  @param [in] wndListBox  
 *  @retval           なし
 *  @note
 */
void COutputWnd::AdjustHorzScroll(CListBox& wndListBox)
{
	CClientDC dc(this);
	CFont* pOldFont = dc.SelectObject(&m_Font);

	int cxExtentMax = 0;

	for (int i = 0; i < wndListBox.GetCount(); i ++)
	{
		CString strItem;
		wndListBox.GetText(i, strItem);

		cxExtentMax = max(cxExtentMax, dc.GetTextExtent(strItem).cx);
	}

	wndListBox.SetHorizontalExtent(cxExtentMax);
	dc.SelectObject(pOldFont);
}


/**
 * @brief   ウインドウ出力
 * @param   [in] iWin  ウインドウID
 * @param   [in] pMessage  出力メッセージ
 * @retval  なし
 * @note	
 */
void COutputWnd::PrintOut(int iWin, TCHAR* pMessage)
{
    switch(iWin)
    {
    case WIN_BUILD:
    	m_wndOutputBuild.PrintOut(pMessage);
        break;

    case WIN_DEBUG:
    	m_wndOutputDebug.PrintOut(pMessage);
        break;

    case WIN_FIND:
    	m_wndOutputFind.PrintOut(pMessage);
        break;
    }
}


/**
 * @brief   ウインドウ選択
 * @param   [in] iWin  ウインドウID
 * @retval  なし
 * @note	
 */
void COutputWnd::SelectWindow(int iWin)
{
    m_wndTabs.SetActiveTab(iWin);
    m_wndTabs.EnsureVisible(iWin);
}

/**
 * @brief   ウインドウクリア
 * @param   [in] iWin  ウインドウID
 * @retval  なし
 * @note	
 */
void COutputWnd::ClearWindow(int iWin)
{
    switch(iWin)
    {
    case WIN_BUILD:
    	m_wndOutputBuild.ResetContent();
        break;

    case WIN_DEBUG:
    	m_wndOutputDebug.ResetContent();
        break;

    case WIN_FIND:
    	m_wndOutputFind.ResetContent();
        break;
    }
}

/**
 *  @brief モード変更
 *  @param [in] wParam   アプリケーションモード(EXEC_COMMON::E_APP_MODE)
 *  @param [in] lParam   オブジェクト定義      (CObjectDef)
 *  @retval
 *  @note
 */
LRESULT COutputWnd::OnModeChange(WPARAM wParam, LPARAM lParam)
{
    using namespace EXEC_COMMON;

    E_EXEC_STS  eSts;
    CObjectDef* pDef;

    eSts = static_cast<E_EXEC_STS>(wParam);
    pDef = reinterpret_cast<CObjectDef*>(lParam);

    if (eSts == EX_EDIT)
    {
        ClearWindow(WIN_BUILD);
        SelectWindow(WIN_BUILD);
    }
    else if (eSts == EX_EXEC)
    {
        ClearWindow(WIN_DEBUG);
        SelectWindow(WIN_DEBUG);
    }
    return 0;
}
