/**
 * @brief			COutputWndヘッダーファイル
 * @file			COutputWnd.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *	
 *
 * $
 * $
 * 
 */


#pragma once
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include    "COutputList.h"

/*---------------------------------------------------*/
/*  class                                            */
/*---------------------------------------------------*/


/**
 * @class   COutputWnd
 * @brief                        
 */
class COutputWnd : public CDockablePane
{
// コンストラクション
public:
    COutputWnd();

    virtual ~COutputWnd();

    //追加
    void    PrintOut(int iWin, TCHAR* pMessage);

    //!< ウインドウ選択
    void    SelectWindow(int iWin);

    //!< ウインドウ内容消去
    void    ClearWindow(int iWin);


// 属性
protected:
    CFont m_Font;

    CMFCTabCtrl	m_wndTabs;

    COutputList m_wndOutputBuild;
    COutputList m_wndOutputDebug;
    COutputList m_wndOutputFind;

protected:

	void AdjustHorzScroll(CListBox& wndListBox);

// 実装
public:

protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);

    //!<モード変更
    afx_msg LRESULT OnModeChange(WPARAM wParam, LPARAM lParam);

    DECLARE_MESSAGE_MAP()
};

