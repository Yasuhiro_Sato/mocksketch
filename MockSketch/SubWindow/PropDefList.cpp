/**
 * @brief			CPropDefList実装ファイル
 * @file			CPropDefList.cpp
 * @author			Yasuhiro Sato
 * @date			01-2-2009 10:36:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./PropDefList.h"
#include "Resource.h"
#include "resource2.h"
#include "MainFrm.h"
#include "MockSketch.h"

#include "DefinitionObject/View/MockSketchDoc.h"
#include "DefinitionObject/View/MockSketchView.h"
#include "DefinitionObject/View/CDrawingView.h"
#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/ObjectDef.h"
#include "System/CSystem.h"
#include "Utility/CUtility.h"
#include "Utility/DropUtil.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

BEGIN_MESSAGE_MAP(CPropDefList, CVirtualCheckList)
    ON_WM_CONTEXTMENU()
    ON_COMMAND(ID_ITEM_UP, &CPropDefList::OnItemUp)
    ON_COMMAND(ID_ITEM_DOWN, &CPropDefList::OnItemDown)
    ON_COMMAND(ID_EDIT_CUT, &CPropDefList::OnEditCut)
    ON_COMMAND(ID_EDIT_COPY, &CPropDefList::OnEditCopy)
    ON_COMMAND(ID_EDIT_PASTE, &CPropDefList::OnEditPaste)
    ON_COMMAND(ID_MNU_PROPED_ADD, &CPropDefList::OnMenuAdd)
    ON_COMMAND(ID_MNU_PROPED_DEL, &CPropDefList::OnMenuDel)
    ON_WM_KILLFOCUS()
    ON_NOTIFY_REFLECT(LVN_ENDLABELEDIT, &CPropDefList::OnLvnEndlabeleditVlst)
    ON_NOTIFY_REFLECT(NM_DBLCLK, &CPropDefList::OnDblclk)


END_MESSAGE_MAP()
/*---------------------------------------------------*/
/* Static variables                                  */
/*---------------------------------------------------*/

namespace
{
    struct STD_PROPERTY_ITEM_CLIP
    {
        CStdPropertyItemDef     ItemDef;
        CPropertySet*           pPropertySet;
        int                     iRow;
        bool                    bCopy;
    };
}

/**
 * @brief   コンストラクター
 * @param   なし
 * @return	なし
 * @note	 
 */
 CPropDefList::CPropDefList():
    iOldRow(-1),
    iOldCol(-1),
    m_pPropertySet(NULL)
{
}

/**
 * @brief   デストラクター
 * @param   なし
 * @return	なし
 * @note	 
 */
CPropDefList::~CPropDefList()
{
}


/**
 * @brief   生成時処理要求
 * @param   [in]    lpCreateStruct  初期化パラメータへのポインタ
 * @return  なし
 * @note	 
 */
void CPropDefList::Setup()
{
    //スタイル設定
    ListView_SetExtendedListViewStyleEx(m_hWnd, LVS_EX_FULLROWSELECT, LVS_EX_FULLROWSELECT);
    ListView_SetExtendedListViewStyleEx(m_hWnd, LVS_EX_GRIDLINES, LVS_EX_GRIDLINES);

    //---------------------
    // ヘッダー部の設定
    //---------------------
    CString sTemp;

    InsertColumn(PRS_TYPE,      _T("STR_PRE_TYPE"      ));
    InsertColumn(PRS_NAME,      _T("STR_PRE_NAME"      ));
    InsertColumn(PRS_VAL_NAME,  _T("STR_PRE_VAL_NAME"  ));
    InsertColumn(PRS_INIT,      _T("STR_PRE_INIT"      ));
    InsertColumn(PRS_NOTE,      _T("STR_PRE_NOTE"      ));
    InsertColumn(PRS_ENABLE,    _T("STR_PRE_ENABLE"    ));
    InsertColumn(PRS_UNIT_KIND, _T("STR_PRE_UNIT_KIND" ));
    InsertColumn(PRS_UNIT,      _T("STR_PRE_UNIT"      ));
    InsertColumn(PRS_MIN,       _T("STR_PRE_MIN"       ));
    InsertColumn(PRS_MAX,       _T("STR_PRE_MAX"       ));
    
    //-----------
    //項目設定
    //-----------
    CVirtualCheckList::COL_SET ColSet;


    //種別
    ColSet.iStyle = 0;
    ColSet.bDraw = false;             //ボタン描画
    ColSet.bColor  = false;           //背景色使用
    ColSet.eEditType = CVirtualCheckList::COMBO;       //コンボボックス
    SetCol(PRS_TYPE, ColSet);

    //名称
    ColSet.bDraw  = false;           //ボタン描画
    ColSet.eEditType = CVirtualCheckList::ALLOW;     //編集可
    SetCol(PRS_NAME, ColSet);

    //初期値
    ColSet.bDraw = false;            //ボタン不描画
    ColSet.eEditType = CVirtualCheckList::ALLOW;       //編集可
    SetCol(PRS_INIT, ColSet);

    //説明
    ColSet.bDraw = false;            //ボタン不描画
    ColSet.eEditType = CVirtualCheckList::ALLOW;       //編集可
    SetCol(PRS_NOTE, ColSet);

    //有効・無効
    ColSet.iStyle = 0;
    ColSet.bDraw   = false;            //ボタン描画
    ColSet.eEditType = CVirtualCheckList::COMBO;       //コンボボックス
    SetCol(PRS_ENABLE, ColSet);

    //物理量
    ColSet.iStyle = 0;
    ColSet.bDraw   = false;            //ボタン描画
    ColSet.eEditType = CVirtualCheckList::COMBO;       //コンボボックス
    SetCol(PRS_UNIT_KIND, ColSet);

    //単位
    ColSet.iStyle = 0;
    ColSet.bDraw   = false;            //ボタン描画
    ColSet.eEditType = CVirtualCheckList::COMBO;       //コンボボックス
    SetCol(PRS_UNIT, ColSet);

    //最大値
    ColSet.bDraw = false;           //ボタン不描画
    ColSet.eEditType = CVirtualCheckList::ALLOW;       //編集可
    SetCol(PRS_MAX, ColSet);

    //最小値
    ColSet.bDraw = false;           //ボタン不描画
    ColSet.eEditType = CVirtualCheckList::ALLOW;       //編集可
    SetCol(PRS_MIN, ColSet);

    //コールバック関数設定
    SetFunc(Callbackfunc, CallbackChek, this);
    SetChgFunc(CallbackChg);
    SetFocusFunc(CallbackFoucs);

    
    //仮想リストの行数を設定する


    int iCnt = 0;
    if (m_pPropertySet)
    {
        iCnt = m_pPropertySet->GetCnt();
    }
    SetItemCount(iCnt);

	return;
}

void CPropDefList::SetPropertySet( CPropertySet*  pPropertySet)
{
    m_pPropertySet = pPropertySet;
    int iCnt = 0;
    if (m_pPropertySet)
    {
        iCnt = m_pPropertySet->GetCnt();
    }
    SetItemCount(iCnt);
    RedrawWindow();
}

/**
 *  @brief 列挿入
 *  @param [in] iCol       列番号
 *  @param [in] strItem    文字識別子 
 *  @retval           なし
 *  @note
 */
void CPropDefList::InsertColumn(int iCol, StdString strItem)
{
    int iWidth;
    int iPos;
    StdString strHeader;

    CUtil::FormatedText(GET_SYS_STR(strItem), &iWidth, &iPos, &strHeader);
    CVirtualCheckList::InsertColumn ( iCol,  strHeader.c_str(), iPos, iWidth);
}


/**
 *  @brief  前データ
 *  @param  [in] pNewWnd  これからフォーカスを設定するウィンドウ
 *  @retval なし
 *  @note   
 */
bool CPropDefList::CheckAll()
{
    bool bRet;
    int                     iNo;
    int                     iCol;
    E_PROPERTY_ITEM_SET     eErrorCol;
    StdString               strError;
    
    if (!m_pPropertySet)
    {
        return false;
    }

    bRet = m_pPropertySet->CheckAll(&iNo, &eErrorCol, &strError);

    int iCurCnt = GetItemCount();
    int iCnt = m_pPropertySet->GetCnt();
    if (iCurCnt != iCnt)
    {
        SetItemCount(iCnt);
        RedrawWindow();
    }

    if(!bRet)
    {
        iCol = static_cast<int>(eErrorCol);
        AfxMessageBox(strError.c_str());
        SetFocusCell(iNo, iCol, iNo, iCol);
        return false;
    }

    return true;
}

/**
 *  @brief  ウインドウフォーカス喪失
 *  @param  [in] pNewWnd  これからフォーカスを設定するウィンドウ
 *  @retval なし
 *  @note   
 */
void CPropDefList::OnKillFocus(CWnd* pNewWnd)
{
    if (pNewWnd == this)
    {
        return;
    }

    bool bRet;
    bRet = CheckAll();

    int iCurCnt = GetItemCount();
    int iCnt = m_pPropertySet->GetCnt();

    if (iCurCnt != iCnt)
    {
        SetItemCount(iCnt);
        RedrawWindow();
    }
}

/**
 * @brief   描画コールバック
 * @param   [in]  pParent 呼び出し元         
 * @param   [in]  nRow    データを取得する行         
 * @param   [in]  nCol    データを取得する列        
 * @param   [out] pData   文字列        
 * @param   [out] pbCheck チェックボックス         
 * @retval  なし
 * @note	仮想リスト用表示関数
 */
void CPropDefList::Callbackfunc (LPVOID pParent, int nRow, int nCol, _TCHAR* pData, bool* pbChec)
{
    CPropDefList* pList = (CPropDefList*)pParent;

    //描画処理
    pList->SetText( nRow, nCol, pData);
}

/**
 * @brief   描画処理
 * @param   [in]  nRow    データを取得する行         
 * @param   [in]  nCol    データを取得する列        
 * @param   [out] pData   文字列        
 * @retval  なし
 * @note	
 */
void CPropDefList::SetText( int nRow, int nCol, _TCHAR* pData) 
{
    CLayer*                 pLayer  = NULL;
    int                     iSize;
    StdString               sItem;

    if (!m_pPropertySet)
    {
        return;
    }

    iSize = m_pPropertySet->GetCnt();
    if (nRow >=  iSize)
    {
        return;
    }

    E_PROPERTY_ITEM_SET eCol = static_cast<E_PROPERTY_ITEM_SET>(nCol);
   
    
    sItem = m_pPropertySet->GetItemString(nRow, eCol);
    lstrcpy(pData, sItem.c_str());
}

/**
 * @brief   チェックボックスコールバック
 * @param   [in]  nRow    選択時の行         
 * @param   [in]  nCol    選択時の列        
 * @retval  なし
 * @note	仮想リストボタンチェック時に呼び出し
 */
void CPropDefList::CallbackChek   (LPVOID pParent, int nRow, int nCol)
{
    CPropDefList* pList = (CPropDefList*)pParent;
}

/**
 *  @brief   ラベル編集終了
 *  @param    [in]  pNMHDR 通知メッセージへのポインタ
 *  @param    [out] pResult 
 *  @retval   なし
 *  @note	
 */
void CPropDefList::OnLvnEndlabeleditVlst(NMHDR *pNMHDR, LRESULT *pResult)
{

    NMLVDISPINFO *pDispInfo = reinterpret_cast<NMLVDISPINFO*>(pNMHDR);

    *pResult = 0;
	if(pDispInfo->item.pszText == NULL)
	{
        return;
    }

    int nRow = pDispInfo->item.iItem;
    int nCol = pDispInfo->item.iSubItem;

    StdString strData;
    strData = pDispInfo->item.pszText;

    if (!m_pPropertySet)
    {
        return;
    }

    try
    {
        bool bRet;
        E_PROPERTY_ITEM_SET eCol = static_cast<E_PROPERTY_ITEM_SET>(nCol);
        bRet = m_pPropertySet->Set(nRow, eCol, strData);

        if (!bRet)
        {
            *pResult = FALSE;
        }
    }
    catch(MockException &e)
    {
        STD_DBG(_T("Error[%d] %s"), e.GetErr(), e.GetErrMsg().c_str());
        e.DispMessageBox();
        return;
    }
    catch(...)
    {
        STD_DBG(_T("Exception"));
        //AfxMessageBox(GET_STR(STR_DLG_SCRIPT_ERROR));
        return;
    }
}

/**
 *  @brief   ラベル編集終了
 *  @param    [in]  pNMHDR 通知メッセージへのポインタ
 *  @param    [out] pResult 
 *  @retval   なし
 *  @note	
 */
void CPropDefList::OnDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
    DWORD dwPos = ::GetMessagePos ();
    CPoint point ((int) LOWORD (dwPos), (int) HIWORD (dwPos));
    
    ScreenToClient (&point);

  	int iRow, iCol;
	bool bCheckCleck;
    bool bRet;
    bRet = HitTest (point, &iRow, &iCol, &bCheckCleck);

    if (!bRet)
    {
        OnMenuAdd();
        *pResult = 0;
    }
    else
    {
        CVirtualCheckList::OnDblClickEx(pNMHDR,pResult);
    }
}


/**
 * @brief    変更前コールバック
 * @param    [in]    pParent       このクラス
 * @param    [in]    pCheckList    VirtualCheckList
 * @param    [in]    nRow          行
 * @param    [in]    nCol          列
 * @param    [inout] pData         セルデータ
 * @retval   なし
 * @note	
 */
void CPropDefList::CallbackChg(LPVOID pParent, CVirtualCheckList* pCheckList,
                              int nRow, int nCol, const _TCHAR* pData)
{
    CPropDefList* pWnd = reinterpret_cast<CPropDefList*>(pParent);

    pWnd->SetChg( nRow, nCol, pData);
}

/**
 * @brief    フォーカス変更コールバック
 * @param    [in]    pParent       このクラス
 * @param    [in]    pCheckList    VirtualCheckList
 * @param    [in]    nRowT          先頭行
 * @param    [in]    nColT          先頭列
 * @param    [in]    nRowB          終端行
 * @param    [in]    nColB          終端列
 * @retval   なし
 * @note	
 */
void CPropDefList::CallbackFoucs(LPVOID pParent, CVirtualCheckList* pCheckList,
                                    int nRowT, int nColT, int nRowB, int nColB)
{
    CPropDefList* pWnd = reinterpret_cast<CPropDefList*>(pParent);

    pWnd->SetCellFocus( nRowT, nColT);
}

/**
 * @brief    変更前処理
 * @param    [in] nRow  行
 * @param    [in] nCol  列
 * @param    [in] pData   変更文字列
 * @retval   なし
 * @note	
 */
void CPropDefList::SetChg( int nRow, int nCol, const _TCHAR* pData)
{

    if (!m_pPropertySet)
    {
        return;
    }

    CVirtualCheckList::COL_SET colData;

    E_PROPERTY_ITEM_SET eCol = static_cast<E_PROPERTY_ITEM_SET>(nCol);

    if(!GetCol (nCol, &colData))
    {
        return;
    }

    if (eCol == PRS_TYPE)
    {
        //コンボボックスの設定
        colData.eEditType = CVirtualCheckList::COMBO;
        colData.lstCombo  = *CStdPropertyItemDef::GetVListTypeList();
    }
    else
    {
       //まだ種別を設定していない場合
        if ( m_pPropertySet->GetItemString(nRow, PRS_TYPE) == CStdPropertyItemDef::PropertyType2Name(PROP_NONE))
        {
            AfxMessageBox(GET_STR(STR_ERROR_SET_BEFORE_TYPE));
            return;
        }
        else if (eCol == PRS_ENABLE)
        {
            colData.eEditType = CVirtualCheckList::COMBO;
            colData.lstCombo.push_back(_T("true"));
            colData.lstCombo.push_back(_T("false"));

        }
        else if (eCol == PRS_UNIT_KIND)
        {
            colData.eEditType = CVirtualCheckList::COMBO;
            CUnitKind* pKind;
            int iSize = SYS_UNIT->GetKindSize();
            for (int iNo = 0; iNo < iSize; iNo++)
            {
                pKind = SYS_UNIT->GetKind(iNo);
                if (pKind)
                {
                    colData.lstCombo.push_back(pKind->GetName());
                }
            }
        }
        else if (eCol == PRS_UNIT)
        {
            colData.eEditType = CVirtualCheckList::COMBO;
            StdString strKind = m_pPropertySet->GetItemString(nRow, PRS_UNIT_KIND);
            SYS_UNIT->GetPhysicalQuantityList(&colData.lstCombo, strKind, false);
        }
    }

    SetCol ( nCol, colData);
}

/**
 * @brief   フォーカス変更処理
 * @param   [in] nRow 行
 * @param   [in] nCol 列
 * @return  なし
 * @note    
 */
void CPropDefList::SetCellFocus( int nRow, int nCol)
{

    if (iOldRow == -1)
    {
        iOldRow = nRow;
        iOldCol = nCol;
        return ;
    }

    StdString           strError;
    E_PROPERTY_ITEM_SET eErrorCol;

    CStdPropertyItemDef* pDefItem;

    if (!m_pPropertySet)
    {
        return;
    }

    if (iOldRow != nRow)
    {
         pDefItem = m_pPropertySet->GetInstance(iOldRow);

         STD_ASSERT(pDefItem);
         pDefItem->CheckItem(&eErrorCol, &strError);
    }

    iOldRow = nRow;
    iOldCol = nCol;
}

/**
 *  @brief コンテキストメニュー選択
 *  @param [in] pWnd   マウスの右ボタンがクリックされたウィンドウのハンドル
 *  @param [in] point  クリックされたときの、カーソルの画面座標位置 
 *  @retval           なし
 *  @note
 */
void CPropDefList::OnContextMenu(CWnd* pWnd, CPoint point)
{
        
    //theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_PROP_EDITOR, 
    //    point.x, point.y, pWnd, FALSE);

    CMenu mnuEdit;

    mnuEdit.CreatePopupMenu();


    UINT nFlagCut       = MF_STRING| (UpdateEditCut() ? MF_ENABLED : MF_DISABLED);
    UINT nFlagCopy      = MF_STRING| (UpdateEditCopy() ? MF_ENABLED : MF_DISABLED);
    UINT nFlagPaste     = MF_STRING| (UpdateEditPaste() ? MF_ENABLED : MF_DISABLED);

    UINT nFlagUp        = MF_STRING| (UpdateItemUp() ? MF_ENABLED : MF_DISABLED);
    UINT nFlagDown      = MF_STRING| (UpdateItemDown() ? MF_ENABLED : MF_DISABLED);

    UINT nFlagAdd       = MF_STRING| (UpdateMenuAdd() ? MF_ENABLED : MF_DISABLED);
    UINT nFlagDel       = MF_STRING| (UpdateMenuDel() ? MF_ENABLED : MF_DISABLED);


    mnuEdit.AppendMenu(nFlagCut,    ID_EDIT_CUT     , GET_STR(STR_MNU_EDITOR_CUT));
    mnuEdit.AppendMenu(nFlagCopy,   ID_EDIT_COPY    , GET_STR(STR_MNU_EDITOR_COPY));
    mnuEdit.AppendMenu(nFlagPaste,  ID_EDIT_PASTE   , GET_STR(STR_MNU_EDITOR_PASTE));
    mnuEdit.AppendMenu(MF_SEPARATOR);
    mnuEdit.AppendMenu(nFlagUp,     ID_ITEM_UP       , GET_STR(STR_MNU_PROPED_UP));
    mnuEdit.AppendMenu(nFlagDown,   ID_ITEM_DOWN     , GET_STR(STR_MNU_PROPED_DOWN));
    mnuEdit.AppendMenu(MF_SEPARATOR);
    mnuEdit.AppendMenu(nFlagAdd,    ID_MNU_PROPED_ADD  , GET_STR(STR_MNU_PROPED_ADD));
    mnuEdit.AppendMenu(nFlagDel,    ID_MNU_PROPED_DEL  , GET_STR(STR_MNU_PROPED_DEL));

                
    UINT uiMenu;
    uiMenu = mnuEdit.TrackPopupMenu(
        TPM_LEFTALIGN  |    //クリック時のX座標をメニューの左辺にする
        TPM_RIGHTBUTTON,    //右クリックでメニュー選択可能とする
        point.x,point.y,    //メニューの表示位置
        this      //このメニューを所有するウィンドウ
    );

    if(uiMenu)
    {
        PostMessage(WM_SYSCOMMAND, uiMenu, 0);
    }
    
    mnuEdit.DestroyMenu();
    
}

/**
 *  @brief   コピー準備
 *  @param   [out] pRow 選択行
 *  @retval  コピーするアイテム
 *  @note     
 */
CStdPropertyItemDef* CPropDefList::_PrepareCopy(int* pRow)
{
    if (!m_pPropertySet)
    {
        return NULL;
    }

    int iRow, iCol;
    GetCurCell(&iRow, &iCol);

    CStdPropertyItemDef* pItemDef;
    if (m_pPropertySet->GetCnt() == 0)
    {
        return NULL;
    }

    pItemDef = m_pPropertySet->GetInstance(iRow);

    if (!pItemDef)
    {
        return NULL;
    }

    if(pItemDef->type == PROP_NONE)
    {
        return NULL;
    }

    *pRow = iRow;
    return pItemDef;
}

/**
 *  @brief   コピー
 *  @param   [in] pItemDef コピーするアイテム
 *  @param   [in] iRow     選択行
 *  @param   [in] bCopy    コピー or カット
 *  @retval  true:成功
 *  @note     
 */
bool CPropDefList::_Copy(const CStdPropertyItemDef* pItemDef, int iRow, bool bCopy)
{
    if (!pItemDef)
    {
        return false;
    }

    // クリップボードのオープン
    if( !::OpenClipboard(NULL) )
    {
        return false;
    }

    // ヒープ上にメモリ領域を確保しコピー
    HGLOBAL hMem = ::GlobalAlloc(GMEM_FIXED, sizeof(STD_PROPERTY_ITEM_CLIP));
	if (hMem == 0)
	{
		return false;
	}

    STD_PROPERTY_ITEM_CLIP* pMem = (STD_PROPERTY_ITEM_CLIP*)hMem;

    ::memcpy(pMem, pItemDef, sizeof(CStdPropertyItemDef));
    pMem->bCopy   = bCopy;
    pMem->iRow    = iRow;
    pMem->pPropertySet = m_pPropertySet;

    // クリップボードへコピーし、クローズ
    ::EmptyClipboard();	// クリップボードの中身を空にする
    ::SetClipboardData(CF_MOCK_PROPERTY, hMem);
    ::CloseClipboard();
    return true;
}

/**
 *  @brief   メニュー -> コピー
 *  @param   なし
 *  @retval  なし
 *  @note     
 */
void CPropDefList::OnEditCopy()
{
    CStdPropertyItemDef* pItemDef;
    int iRow;
    pItemDef = _PrepareCopy(&iRow);

    bool bRet;
    bRet = _Copy(pItemDef, iRow, true);

    STD_ASSERT(bRet);
}

/**
 *  @brief   メニュー -> コピー UI更新
 *  @param   [in out] コマンド ユーザー インターフェイス
 *  @retval  なし
 *  @note     
 */
void CPropDefList::OnUpdateEditCopy(CCmdUI *pCmdUI)
{
    pCmdUI->Enable(UpdateEditCopy());
}

BOOL CPropDefList::UpdateEditCopy()
{
    CStdPropertyItemDef* pItemDef;
    int iRow;
    pItemDef = _PrepareCopy(&iRow);

    if (pItemDef != NULL)
    {
        return TRUE;
    }
    return FALSE;
}

/**
 *  @brief   メニュー -> カット
 *  @param   なし
 *  @retval  なし
 *  @note     
 */
void CPropDefList::OnEditCut()
{
    CStdPropertyItemDef* pItemDef;
    int iRow;
    pItemDef = _PrepareCopy(&iRow);

    bool bRet;
    bRet = _Copy(pItemDef, iRow, false);
}

/**
 *  @brief   メニュー -> カット UI更新
 *  @param   [in out] コマンド ユーザー インターフェイス
 *  @retval  なし
 *  @note     
 */
BOOL CPropDefList::UpdateEditCut()
{
    CStdPropertyItemDef* pItemDef;
    int iRow;
    pItemDef = _PrepareCopy(&iRow);

    if (pItemDef != NULL)
    {
        return TRUE;
    }
    return FALSE;
}

void CPropDefList::OnUpdateEditCut(CCmdUI *pCmdUI)
{
    pCmdUI->Enable(UpdateEditCut());
}

/**
 *  @brief   メニュー -> 張り付け
 *  @param   なし
 *  @retval  なし
 *  @note     
 */
void CPropDefList::OnEditPaste()
{
	if( !::IsClipboardFormatAvailable(CF_MOCK_PROPERTY) )
    {
        return;
    }

    if (!m_pPropertySet)
    {
        return;
    }

    int iRow, iCol;
    GetCurCell(&iRow, &iCol);

    if (iRow < 0)
    {
        return;
    }

    if (!OpenClipboard())
    {
        return;
    }

    HANDLE hData = NULL;
    STD_PROPERTY_ITEM_CLIP* pItemClip;
    try
    {
        hData = ::GetClipboardData(CF_MOCK_PROPERTY);
        if (!hData) 
        {
            throw (MockException(_T("GetClipboardData")));
        }

        pItemClip = reinterpret_cast<STD_PROPERTY_ITEM_CLIP*>(GlobalLock(hData));

        if(!pItemClip)
        {
            throw (MockException(_T("GlobalLock")));
        }

        int iSize = m_pPropertySet->GetCnt();

        //boost::uuids::uuid uuidDefCopy = pItemClip->uuidObjDef;
        CPropertySet* pPropertySet = pItemClip->pPropertySet;
        int  iRowCopy = pItemClip->iRow;
        bool bCopy    = pItemClip->bCopy;
        bool bDelete = false;

        if (!bCopy)
        {
            //カット
            if (pPropertySet == m_pPropertySet)
            {
                if (iRow != iRowCopy)
                {
                    bool bRet;
                    bRet = m_pPropertySet->Move(iRowCopy, iRow);
                    STD_ASSERT(bRet);

                    int iRowStart = iRowCopy;
                    if (iRowCopy > iRow)
                    {
                        iRowStart = iRow;
                    }

                    for (int iRedrawRow = iRowStart; iRedrawRow < iSize - 1; iRedrawRow++)
                    {
                        RedrawRow(iRedrawRow);
                    }

                    RedrawWindow();
                }

                if (hData)
                {
                    ::GlobalUnlock(hData);
                }
                CloseClipboard();
                ::EmptyClipboard();
                return;

            }
            else
            {
                bDelete = true;
            }
        }

        //コピーを生成
        std::shared_ptr<CStdPropertyItemDef> pItemDefCopy;
        pItemDefCopy = std::make_shared <CStdPropertyItemDef>(pItemClip->ItemDef);


        if (bDelete)
        {
            //コピー元を削除
            //TODO:要注意

            if (pPropertySet)
            {
                pPropertySet->Delete(iRowCopy);
            }
        }

        //コピした時に名称が重複する場合があるので 表示名、変数名の末尾に
        //数字をつける
        //名称リストを作成
        std::vector<StdString> lstName;
        std::vector<StdString> lstValName;

        CStdPropertyItemDef* pItemDef;
        for(int iPos = 0; iPos < iSize; iPos++)
        {
            pItemDef =  m_pPropertySet->GetInstance(iPos);

            if (pItemDef)
            {
                lstName.push_back(pItemDef->strDspName);
                lstValName.push_back(pItemDef->strValName);
            }
        }

        //!< 数値つき文字列設定
        StdString strItem;
        strItem = CUtil::FindNumberingString( &lstName, pItemDefCopy->strDspName);
        pItemDefCopy->strDspName = strItem;

        strItem = CUtil::FindNumberingString( &lstValName, pItemDefCopy->strValName);
        pItemDefCopy->strValName = strItem;


        bool bRet;
        bRet = m_pPropertySet->Insert(iRow, pItemDefCopy);

        STD_ASSERT(bRet);

        SetItemCount(iSize + 1);

        for (int iRedrawRow = iRow; iRedrawRow < iSize; iRedrawRow++)
        {
            RedrawRow(iRedrawRow);
        }

        if (hData)
        {
            ::GlobalUnlock(hData);
        }

        ::EmptyClipboard();
    }
    catch( MockException& e)
    {
        STD_DBG(e.GetErrMsg().c_str());
    }

    CloseClipboard();
}

/**
 *  @brief   メニュー -> 貼り付け UI更新
 *  @param   [in out] コマンド ユーザー インターフェイス
 *  @retval  なし
 *  @note     
 */
void CPropDefList::OnUpdateEditPaste(CCmdUI *pCmdUI)
{
    pCmdUI->Enable(UpdateEditPaste());
}

BOOL CPropDefList::UpdateEditPaste()
{
    BOOL bEnable = TRUE;
	// クリップボードにテキストデータが入っているかを調べる
	if( !::IsClipboardFormatAvailable(CF_MOCK_PROPERTY) )
    {
        bEnable = FALSE;
    }
    return bEnable;
}

/**
 *  @brief   追加の可否
 *  @param   なし
 *  @retval  なし
 *  @note     
 */
bool CPropDefList::IsAbleAdd()
{
    if (!m_pPropertySet)
    {
        return false;
    }

   int iCnt = m_pPropertySet->GetCnt();
   if (iCnt != 0)
   {
       CStdPropertyItemDef* pDef;
       pDef = m_pPropertySet->GetInstance(iCnt - 1);
       if (pDef)
       {
           if (pDef->type == PROP_NONE)
           {
               return  false;
           }
       }
   }
   return true;
}

/**
 *  @brief   メニュー -> 追加
 *  @param   なし
 *  @retval  なし
 *  @note     
 */
void CPropDefList::OnMenuAdd()
{
    std::shared_ptr<CStdPropertyItemDef> pPropItem;
    pPropItem = std::make_shared<CStdPropertyItemDef>();

    if (!m_pPropertySet)
    {
        return;
    }

    if (!IsAbleAdd())
    {
        return;
    }

    pPropItem->type = PROP_NONE;
    pPropItem->strDspName  = _T("");
    pPropItem->strValName  = _T("");
    pPropItem->strExp      = _T("");
    pPropItem->anyInit     = StdString(_T(""));
    pPropItem->strUnit     = StdString(_T("NONE"));
    pPropItem->anyMin      = StdString(_T(""));
    pPropItem->anyMax      = StdString(_T(""));
    
    m_pPropertySet->Add(pPropItem);

    int iCnt = m_pPropertySet->GetCnt();
    SetItemCount(iCnt);

    RedrawWindow();
}

/**
 *  @brief   メニュー -> 追加 UI更新
 *  @param   [in out] コマンド ユーザー インターフェイス
 *  @retval  なし
 *  @note     
 */
void CPropDefList::OnUpdateMenuAdd(CCmdUI *pCmdUI)
{
}

BOOL CPropDefList::UpdateMenuAdd()
{
    return TRUE;
}


/**
 *  @brief   メニュー -> 削除
 *  @param   なし
 *  @retval  なし
 *  @note     
 */
void CPropDefList::OnMenuDel()
{
    if (!m_pPropertySet)
    {
        return;
    }

    int iSize = m_pPropertySet->GetCnt();
    if (iSize == 0)
    {
        return;
    }

    int iRow, iCol;
    GetCurCell(&iRow, &iCol);

    if(m_pPropertySet->GetInstance(iRow)->type == PROP_NONE)
    {
        return;
    }

    bool bRet;

    bRet = m_pPropertySet->Delete(iRow);
    STD_ASSERT(bRet);

    for (int iRedrawRow = iRow; iRedrawRow < iSize - 1; iRedrawRow++)
    {
        RedrawRow(iRedrawRow);
    }
}

/**
 *  @brief   メニュー -> 削除 UI更新
 *  @param   [in out] コマンド ユーザー インターフェイス
 *  @retval  なし
 *  @note     
 */
void CPropDefList::OnUpdateMenuDel(CCmdUI *pCmdUI)
{
    pCmdUI->Enable(UpdateMenuDel());
}

BOOL CPropDefList::UpdateMenuDel()
{
    if (!m_pPropertySet)
    {
        return FALSE;
    }

    int iSize = m_pPropertySet->GetCnt();
    if (iSize == 0)
    {
        return FALSE;
    }

    int iRow, iCol;
    GetCurCell(&iRow, &iCol);

    if(m_pPropertySet->GetInstance(iRow)->type == PROP_NONE)
    {
        return FALSE;
    }

    return TRUE;
}

/**
 *  @brief   メニュー -> 上に移動
 *  @param   なし
 *  @retval  なし
 *  @note     
 */
void CPropDefList::OnItemUp()
{
    if (!m_pPropertySet)
    {
        return;
    }

    int iSize = m_pPropertySet->GetCnt();
    if (iSize < 2)
    {
        return;
    }

    int iRow, iCol;
    GetCurCell(&iRow, &iCol);

    if (iRow == 0)
    {
        return;
    }

    if(m_pPropertySet->GetInstance(iRow)->type == PROP_NONE)
    {
        return;
    }

    bool bRet;

    bRet = m_pPropertySet->Swap(iRow, iRow - 1);
    STD_ASSERT(bRet);

    Select(iRow - 1, iCol);
    RedrawRow(iRow);
    RedrawRow(iRow - 1);

    RedrawWindow();
}

/**
 *  @brief   メニュー -> 上に移動
 *  @param   [in out] コマンド ユーザー インターフェイス
 *  @retval  なし
 *  @note     
 */
void CPropDefList::OnUpdateItemUp(CCmdUI *pCmdUI)
{
    pCmdUI->Enable(UpdateItemUp());
}

BOOL CPropDefList::UpdateItemUp()
{
    if (!m_pPropertySet)
    {
        return FALSE;
    }

    int iSize = m_pPropertySet->GetCnt();
    if (iSize < 2)
    {
        return FALSE;
    }

    int iRow, iCol;
    GetCurCell(&iRow, &iCol);

    if (iRow <= 0)
    {
        return FALSE;
    }

    CStdPropertyItemDef* pSelItem;
    pSelItem = m_pPropertySet->GetInstance(iRow);

    BOOL bEnable = TRUE;
    if (pSelItem)
    {
        if(pSelItem->type == PROP_NONE)
        {
            bEnable = FALSE;
        }
    }
    else
    {
        bEnable = FALSE;
    }

    return bEnable;

}
/**
 *  @brief   メニュー -> 下に移動
 *  @param   なし
 *  @retval  なし
 *  @note     
 */
void CPropDefList::OnItemDown()
{
    if (!m_pPropertySet)
    {
        return;
    }

    int iSize = m_pPropertySet->GetCnt();
    if (iSize < 2)
    {
        return;
    }

    int iRow, iCol;
    GetCurCell(&iRow, &iCol);

    if ((iRow + 1) >= iSize)
    {
        return;
    }

    if(m_pPropertySet->GetInstance(iRow)->type == PROP_NONE)
    {
        return;
    }

    if(m_pPropertySet->GetInstance(iRow + 1)->type == PROP_NONE)
    {
        return;
    }

    bool bRet;

    bRet = m_pPropertySet->Swap(iRow, iRow + 1);
    STD_ASSERT(bRet);

    Select(iRow + 1, iCol);
    RedrawRow(iRow);
    RedrawRow(iRow + 1);

    RedrawWindow();

}

/**
 *  @brief   メニュー ->下に移動  UI更新
 *  @param   [in out] コマンド ユーザー インターフェイス
 *  @retval  なし
 *  @note     
 */
void CPropDefList::OnUpdateItemDown(CCmdUI *pCmdUI)
{
    pCmdUI->Enable(UpdateItemDown());
}

BOOL CPropDefList::UpdateItemDown()
{
    if (!m_pPropertySet)
    {
        return FALSE;
    }

    int iSize = m_pPropertySet->GetCnt();
    if (iSize < 2)
    {
        return FALSE;
    }

    int iRow, iCol;
    GetCurCell(&iRow, &iCol);

    if ((iRow + 1) >= iSize)
    {
        return FALSE;
    }

    if(m_pPropertySet->GetInstance(iRow)->type == PROP_NONE)
    {
        return FALSE;
    }

    if(m_pPropertySet->GetInstance(iRow + 1)->type == PROP_NONE)
    {
        return FALSE;
    }

    return (TRUE);

}
