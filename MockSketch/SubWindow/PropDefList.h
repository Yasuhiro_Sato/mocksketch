/**
 * @brief			CPropDefListヘッダーファイル
 * @file			CPropDefList.h
 * @author			Yasuhiro Sato
 * @date			01-2-2009 10:36:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef __PROP_DEF_LIST_
#define __PROP_DEF_LIST_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "Utility/VirtualTreeCtrl/VirtualCheckList.h"
#include "ToolBar/CustomToolBar.h"
#include "SubWindow/CustomDocablePane.h"
#include "Utility/CPropertyGroup.h"

/*---------------------------------------------------*/
/*  classes                                          */
/*---------------------------------------------------*/
class CObjectDef;
struct CStdPropertyItemDef;


/**
 * @class   CPropDefWnd
 * @brief
 */
class CPropDefList : public CVirtualCheckList
{



// コンストラクション
public:
	CPropDefList();
	virtual ~CPropDefList();

	void Setup();

    void SetPropertySet( CPropertySet*  pPropertySet);

// 属性
public:


protected:
    CFont m_fntPropList;

    //!< 表示オブジェクトコントロールUUID
    //boost::uuids::uuid  m_uuidObjDef;

    //!< 前回選択時の行
    int iOldRow;

    //!< 前回選択時の列
    int iOldCol;

    CPropertySet*     m_pPropertySet;

public:
    //!< 列挿入
    void InsertColumn(int iCol, StdString strItem);

// 実装
protected:

	DECLARE_MESSAGE_MAP()


    //!< エディット完了
    afx_msg void OnLvnEndlabeleditVlst(NMHDR *pNMHDR, LRESULT *pResult);

    //!< ダブルクリック
    afx_msg void OnDblclk(NMHDR *pNMHDR, LRESULT *pResult);

    //!< 描画コールバック
    static void __cdecl Callbackfunc(LPVOID pParent, int nRow, int nCol, _TCHAR* pData, bool* pbCheck);

    //!< チェックコールバック
    static void __cdecl CallbackChek(LPVOID pParent, int nRow, int nCol);

    //!< 描画処理
    virtual void SetText( int nRow, int nCol, _TCHAR* pData);

    //!< 変更コールバック
    static void __cdecl CallbackChg(LPVOID pParent, CVirtualCheckList* pCheckList,
                                    int nRow, int nCol, const _TCHAR* pData);

    //!< フォーカスコールバック
    static void __cdecl CallbackFoucs(LPVOID pParent, CVirtualCheckList* pCheckList,
                                    int nRowT, int nColT, int nRowB, int nColB);

    //!< 変更処理
    virtual void SetChg( int nRow, int nCol, const _TCHAR* pData);

    //!< フォーカス変更処理
    virtual void SetCellFocus( int nRow, int nCol);

    //!<ビュークリア
    //afx_msg LRESULT OnViewClear(WPARAM pMessage, LPARAM lParam);

    //!<モード変更
    //afx_msg LRESULT OnModeChange(WPARAM wParam, LPARAM lParam);


    //!< データチェック
    bool CheckAll();

    //!< コピー準備
    CStdPropertyItemDef* _PrepareCopy(int* pRow);

    //!< コピー準備
    bool _Copy(const CStdPropertyItemDef* pItemDef, int iRow, bool bCopy);

    //!< 追加の可否
    bool IsAbleAdd();

public:
    afx_msg void OnEditCut();
    afx_msg void OnUpdateEditCut(CCmdUI *pCmdUI);
    afx_msg void OnEditCopy();
    afx_msg void OnUpdateEditCopy(CCmdUI *pCmdUI);
    afx_msg void OnEditPaste();
    afx_msg void OnUpdateEditPaste(CCmdUI *pCmdUI);
    afx_msg void OnMenuAdd();
    afx_msg void OnUpdateMenuAdd(CCmdUI *pCmdUI);
    afx_msg void OnMenuDel();
    afx_msg void OnUpdateMenuDel(CCmdUI *pCmdUI);
    afx_msg void OnKillFocus(CWnd* pNewWnd);
    afx_msg void OnNMKillfocus(NMHDR *pNMHDR, LRESULT *pResult);
    afx_msg void OnNMSetfocus(NMHDR *pNMHDR, LRESULT *pResult);

    afx_msg void OnItemUp();
    afx_msg void OnUpdateItemUp(CCmdUI *pCmdUI);
    afx_msg void OnItemDown();
    afx_msg void OnUpdateItemDown(CCmdUI *pCmdUI);
    afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);

    BOOL UpdateEditCut();
    BOOL UpdateEditCopy();
    BOOL UpdateEditPaste();
    BOOL UpdateMenuAdd();
    BOOL UpdateMenuDel();
    BOOL UpdateItemUp();
    BOOL UpdateItemDown();


};

#endif //__LAYER_WND_