/**
 * @brief			CPropDefWnd実装ファイル
 * @file			CPropDefWnd.cpp
 * @author			Yasuhiro Sato
 * @date			01-2-2009 10:36:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./PropDefWnd.h"
#include "Resource.h"
#include "resource2.h"
#include "MainFrm.h"
#include "MockSketch.h"

#include "DefinitionObject/View/MockSketchDoc.h"
#include "DefinitionObject/View/MockSketchView.h"
#include "DefinitionObject/View/CDrawingView.h"
#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/ObjectDef.h"
#include "System/CSystem.h"
#include "Utility/CUtility.h"
#include "Utility/DropUtil.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

BEGIN_MESSAGE_MAP(CPropDefWnd, CDockablePane)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_WM_SETTINGCHANGE()
    //ON_NOTIFY(LVN_ENDLABELEDIT, IDC_LST_VIRTUAL_PROP_DEF, OnLvnEndlabeleditVlst)
    ON_MESSAGE(WM_VIEW_CREATE,       OnViewCreate)
    ON_MESSAGE(WM_VIEW_ACTIVE, OnViewActivate)
    ON_MESSAGE(WM_VIEW_CLEAR_ALL, OnViewClear)
    ON_MESSAGE(WM_MODE_CHANGE, OnModeChange)
    /*
    ON_WM_CONTEXTMENU()
    ON_COMMAND(ID_ITEM_UP, &CPropDefWnd::OnItemUp)
    ON_UPDATE_COMMAND_UI(ID_ITEM_UP, &CPropDefWnd::OnUpdateItemUp)
    ON_COMMAND(ID_ITEM_DOWN, &CPropDefWnd::OnItemDown)
    ON_UPDATE_COMMAND_UI(ID_ITEM_DOWN, &CPropDefWnd::OnUpdateItemDown)
    ON_COMMAND(ID_EDIT_CUT, &CPropDefWnd::OnEditCut)
    ON_UPDATE_COMMAND_UI(ID_EDIT_CUT, &CPropDefWnd::OnUpdateEditCut)
    ON_COMMAND(ID_EDIT_COPY, &CPropDefWnd::OnEditCopy)
    ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, &CPropDefWnd::OnUpdateEditCopy)
    ON_COMMAND(ID_EDIT_PASTE, &CPropDefWnd::OnEditPaste)
    ON_UPDATE_COMMAND_UI(ID_EDIT_PASTE, &CPropDefWnd::OnUpdateEditPaste)
    ON_COMMAND(ID_MNU_PROPED_ADD, &CPropDefWnd::OnMenuAdd)
    ON_UPDATE_COMMAND_UI(ID_MNU_PROPED_ADD, &CPropDefWnd::OnUpdateMenuAdd)
    ON_COMMAND(ID_MNU_PROPED_DEL, &CPropDefWnd::OnMenuDel)
    ON_UPDATE_COMMAND_UI(ID_MNU_PROPED_DEL, &CPropDefWnd::OnUpdateMenuDel)
    */

END_MESSAGE_MAP()
/*---------------------------------------------------*/
/* Static variables                                  */
/*---------------------------------------------------*/
/*
namespace
{
    struct STD_PROPERTY_ITEM_CLIP
    {
        CStdPropertyItemDef     ItemDef;
        boost::uuids::uuid      uuidObjDef;
        int                     iRow;
        bool                    bCopy;
    };
}
*/

/**
 * @brief   コンストラクター
 * @param   なし
 * @return	なし
 * @note	 
 */
CPropDefWnd::CPropDefWnd()
{
    m_uuidObjDef = boost::uuids::nil_uuid();
}

/**
 * @brief   デストラクター
 * @param   なし
 * @return	なし
 * @note	 
 */
CPropDefWnd::~CPropDefWnd()
{
}

/**
 * @brief   レイアウト調整
 * @param   なし
 * @return	なし
 * @note	 
 */
void CPropDefWnd::AdjustLayout()
{
	if (GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect rectClient;

    GetClientRect(rectClient);


    STD_ASSERT(m_vlstProp.m_hWnd != NULL);

    int cyTlb = m_wndToolBar.CalcFixedLayout(FALSE, TRUE).cy;

    m_wndToolBar.SetWindowPos(NULL, rectClient.left, 
                                    rectClient.top, 
                                    rectClient.Width(), 
                                    cyTlb, SWP_NOACTIVATE | SWP_NOZORDER);

    m_vlstProp.SetWindowPos(NULL, rectClient.left,
                                   rectClient.top + cyTlb, 
                                   rectClient.Width(), 
                                   rectClient.Height() - cyTlb, SWP_NOACTIVATE | SWP_NOZORDER);
}

/**
 * @brief   生成時処理要求
 * @param   [in]    lpCreateStruct  初期化パラメータへのポインタ
 * @return  0:生成成功
 * @note	 
 */
int CPropDefWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (CDockablePane::OnCreate(lpCreateStruct) == -1)
    {
        return -1;
    }

    m_vlstProp.Create(LVS_REPORT | LVS_ALIGNLEFT | LVS_OWNERDATA | WS_CHILD | 
                       WS_BORDER | WS_VISIBLE |WS_TABSTOP, CRect(), this, IDC_LST_VIRTUAL_PROP_DEF);



	m_wndToolBar.Create(this, AFX_DEFAULT_TOOLBAR_STYLE, IDR_PROPERTIES_DEF);
	m_wndToolBar.LoadToolBar(IDR_PROPERTIES_DEF, 0, 0, TRUE /* ロックされています*/);
	m_wndToolBar.CleanUpLockedImages();
	m_wndToolBar.LoadBitmap( IDR_PROPERTIES_DEF, 0, 0, TRUE /* ロックされました*/);



	m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() | CBRS_TOOLTIPS | CBRS_FLYBY);
	m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() & ~(CBRS_GRIPPER | CBRS_SIZE_DYNAMIC | CBRS_BORDER_TOP | CBRS_BORDER_BOTTOM | CBRS_BORDER_LEFT | CBRS_BORDER_RIGHT));
	m_wndToolBar.SetOwner(this);

	// すべてのコマンドが、親フレーム経由ではなくこのコントロール経由で渡されます:
	m_wndToolBar.SetRouteCommandsViaFrame(FALSE);


    //生成及び調整
	AdjustLayout();

    m_vlstProp.Setup();
	return 0;
}

/**
 *  @brief ウインドウサイズ変更
 *  @param [in] nType   要求されるサイズ変更のタイプ  (SIZE_MAXIMIZED....)
 *  @param [in] cx      クライアント領域の新しい幅 
 *  @param [in] cy      クライアント領域の新しい高さ
 *  @retval           なし
 *  @note
 */
void CPropDefWnd::OnSize(UINT nType, int cx, int cy)
{
	CDockablePane::OnSize(nType, cx, cy);
	AdjustLayout();
}

/**
 *  @brief  ウインドウフォーカス設定
 *  @param  [in] pOldWnd  今までフォーカスがあったウインドウ
 *  @retval なし
 *  @note   
 */
/*
void CPropDefWnd::OnSetFocus(CWnd* pOldWnd)
{
	CDockablePane::OnSetFocus(pOldWnd);
}
*/

/**
 *  @brief  設定変更
 *  @param  [in] uFlags  変更されたシステム パラメータを示すフラグ
 *  @param  [in] lpszSection   変更されたセクション名を示す文字列へのポインタ
 *  @retval なし
 *  @note   SystemParametersInfo参照
 */
void CPropDefWnd::OnSettingChange(UINT uFlags, LPCTSTR lpszSection)
{
	CDockablePane::OnSettingChange(uFlags, lpszSection);
	SetListFont();
}

/**
 *  @brief  フォント変更
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CPropDefWnd::SetListFont()
{
    /*
	::DeleteObject(m_fntPropList.Detach());


	LOGFONT lf;
	afxGlobalData.fontRegular.GetLogFont(&lf);

	NONCLIENTMETRICS info;
	info.cbSize = sizeof(info);

	afxGlobalData.GetNonClientMetrics(info);

	lf.lfHeight = info.lfMenuFont.lfHeight;
	lf.lfWeight = info.lfMenuFont.lfWeight;
	lf.lfItalic = info.lfMenuFont.lfItalic;


	m_fntPropList.CreateFontIndirect(&lf);
    */
}

/**
 *  @brief  ウインドウプロシジャ
 *  @param    [in]  message  処理される Windows メッセージ
 *  @param    [in]  wParam   メッセージの処理で使う付加情報1
 *  @param    [in]  lParam   メッセージの処理で使う付加情報2
 *  @retval   メッセージに依存する値
 *  @note   
 */
LRESULT CPropDefWnd::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
    //!< 避難的対応 恐らくリフレクションの関連と思われるが
    //!< 同じメッセージが CWnd と CBasePainで発生する
    if (message == WM_NOTIFY)
    {
        if (wParam == IDC_LST_VIRTUAL_PROP_DEF)
        {
            return CWnd::WindowProc(message, wParam, lParam);

        }
    }
    return CDockablePane::WindowProc(message, wParam, lParam);
}

/**
 *  @brief  View生成通知
 *  @param    [in]  message  処理される Windows メッセージ
 *  @param    [in]  wParam   メッセージの処理で使う付加情報1
 *  @param    [in]  lParam   メッセージの処理で使う付加情報2
 *  @retval   メッセージに依存する値
 *  @note   
 */
LRESULT CPropDefWnd::OnViewCreate(WPARAM iType, LPARAM pMessage)
{
    if (pMessage == 0)
    {
        m_uuidObjDef =boost::uuids::nil_uuid();
        m_vlstProp.SetPropertySet(NULL);
    }
    else
    {
        m_vlstProp.EnableWindow(TRUE);
        CObjectDef* pDef;

        pDef = reinterpret_cast<CObjectDef*>(pMessage);
        m_uuidObjDef = pDef->GetPartsId();

        CPropertySet* pPropertySet = pDef->GetUserPropertySet();
        m_vlstProp.SetPropertySet(pPropertySet);
        RedrawWindow();
    }

    RedrawWindow();
    return 0;
}


/**
 * @brief    View活性化を通知
 * @param    [in] pMessage タブタイトル文字
 * @param    [in] lParam   0:MockSletchView 1:EditView
 * @retval   なし
 * @note	
 */
LRESULT CPropDefWnd::OnViewActivate(WPARAM pMessage, LPARAM lParam)
{
    int iItemCnt = m_vlstProp.GetItemCount();

    if (iItemCnt > 0)
    {
        m_vlstProp.StopEdit();
        m_vlstProp. StopCombo();
    }

    if (lParam != 0)
    {
        m_vlstProp.EnableWindow(FALSE);
        return 0;
    }

    m_vlstProp.EnableWindow(TRUE);
    StdString* pTitle = reinterpret_cast<StdString*>(pMessage);


    CWnd* pWnd = THIS_APP->GetView(*pTitle);
    CMockSketchView* pView = reinterpret_cast<CMockSketchView*>(pWnd);
    if (pView == NULL)
    {
        STD_DBG(_T("CMockSketchView was deleted."));
        return 0;
    }
    CMockSketchDoc* pDoc = pView->GetDocument();


    std::shared_ptr<CObjectDef> pDef;
    pDef = pDoc->GetObjectDef().lock();
    if (pDef)
    {
        if (m_uuidObjDef != pDef->GetPartsId())
        {
            CPropertySet* pPropertySet = pDef->GetUserPropertySet();
            m_vlstProp.SetPropertySet(pPropertySet);
            m_vlstProp.Select( 0, 0);

            RedrawWindow();
        }
    }

    return 0;
}


/**
 * @brief   ビュークリア
 * @param   [in] wParam 使用しない
 * @param   [in] lParam 使用しない
 * @return  なし
 * @note    
 */
LRESULT CPropDefWnd::OnViewClear(WPARAM pMessage, LPARAM lParam)
{
    return 0;
}


/**
 *  @brief モード変更
 *  @param [in] wParam   アプリケーションモード(EXEC_COMMON::E_APP_MODE)
 *  @param [in] lParam   オブジェクト定義      (CObjectDef)
 *  @retval
 *  @note
 */
LRESULT CPropDefWnd::OnModeChange(WPARAM wParam, LPARAM lParam)
{
    using namespace EXEC_COMMON;

    E_EXEC_STS  eSts;
    CObjectDef* pDef;

    eSts = static_cast<E_EXEC_STS>(wParam);
    pDef = reinterpret_cast<CObjectDef*>(lParam);

    if (eSts == EX_EDIT)
    {
        m_vlstProp.SetPropertySet(NULL);
        RedrawWindow();
    }

    return 0;
}