/**
 * @brief			PropDefWndヘッダーファイル
 * @file			PropDefWnd.h
 * @author			Yasuhiro Sato
 * @date			01-2-2009 10:36:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef __PROP_DEF_WND_
#define __PROP_DEF_WND_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "./PropDefList.h"
#include "ToolBar/CustomToolBar.h"
#include "SubWindow/CustomDocablePane.h"

/*---------------------------------------------------*/
/*  classes                                          */
/*---------------------------------------------------*/
class CObjectDef;
struct CStdPropertyItemDef;


/**
 * @class   CPropDefToolBar
 * @brief
 */
class CPropDefToolBar : public CCustomToolBar
{
public:
    CPropDefToolBar():CCustomToolBar(){
        SetName(_T("CPropDefToolBar"));
    }

	virtual void OnUpdateCmdUI(CFrameWnd* /*pTarget*/, BOOL bDisableIfNoHndler)
	{
		CMFCToolBar::OnUpdateCmdUI((CFrameWnd*) GetOwner(), bDisableIfNoHndler);
	}
    
	virtual BOOL AllowShowOnList() const { return FALSE; }
};



/**
 * @class   CPropDefWnd
 * @brief
 */
class CPropDefWnd : public CCustomDocablePane
{



// コンストラクション
public:
	CPropDefWnd();
	virtual ~CPropDefWnd();

	void AdjustLayout();

// 属性
public:
    void SetVSDotNetLook(BOOL bSet)
    {
    }

    //!< プロパティリスト
    CPropDefList m_vlstProp;


protected:
    CFont m_fntPropList;

    //!< ツールバー
    CPropDefToolBar m_wndToolBar;

    //!< 表示オブジェクトコントロールUUID
    boost::uuids::uuid  m_uuidObjDef;



// 実装
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSettingChange(UINT uFlags, LPCTSTR lpszSection);

	DECLARE_MESSAGE_MAP()

    void SetListFont();

    //!< View生成通知
    afx_msg LRESULT OnViewCreate(WPARAM iType, LPARAM pMessage);

    //!< View活性化通知
    afx_msg  LRESULT OnViewActivate(WPARAM pMessage, LPARAM lParam);

    //!<ビュークリア
    afx_msg LRESULT OnViewClear(WPARAM pMessage, LPARAM lParam);

    //!<モード変更
    afx_msg LRESULT OnModeChange(WPARAM wParam, LPARAM lParam);

     virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
public:

};

#endif //__PROP_DEF_WND_