
#include "stdafx.h"

#include "./PropertiesWnd.h"
#include "Resource.h"
#include "Resource2.h"
#include "MainFrm.h"
#include "MockSketch.h"

#include "DefinitionObject/View/MockSketchDoc.h"
#include "DefinitionObject/View/MockSketchView.h"
#include "DefinitionObject/CPartsDef.h"

#include "DrawingObject/CDrawingObject.h"
#include "System/CSystem.h"
#include "Utility/CustomPropertys.h"
#include "Utility/CStdPropertyTree.h"
#include "Utility/CPropertyGrid.h"
#include "Utility/CUtility.h"



#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


/////////////////////////////////////////////////////////////////////////////
// CResourceViewBar

BEGIN_MESSAGE_MAP(CPropertiesWnd, CDockablePane)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_COMMAND(ID_EXPAND_ALL, OnExpandAllProperties)
	ON_UPDATE_COMMAND_UI(ID_EXPAND_ALL, OnUpdateExpandAllProperties)
	ON_COMMAND(ID_SORTPROPERTIES, OnSortProperties)
	ON_UPDATE_COMMAND_UI(ID_SORTPROPERTIES, OnUpdateSortProperties)
	ON_COMMAND(ID_PROPERTIES1, OnProperties1)
	ON_UPDATE_COMMAND_UI(ID_PROPERTIES1, OnUpdateProperties1)
	ON_COMMAND(ID_PROPERTIES2, OnProperties2)
	ON_UPDATE_COMMAND_UI(ID_PROPERTIES2, OnUpdateProperties2)
	ON_WM_SETFOCUS()
	ON_WM_SETTINGCHANGE()

    ON_MESSAGE(WM_VIEW_SEL_CHANGEE,  OnViewSelchange)
    ON_MESSAGE(WM_VIEW_ACTIVE, OnViewActivate)
    ON_MESSAGE(WM_VIEW_CLEAR_ALL, OnViewClear)
    ON_MESSAGE(WM_MODE_CHANGE, OnModeChange)
    ON_MESSAGE(WM_UPDATE_PROPERTY, OnUpdaatePropertyList)
    ON_MESSAGE(WM_VIEW_LOAD_START, OnLoadStart)


END_MESSAGE_MAP()

/**
 * @brief   コンストラクター
 * @param   なし
 * @return	なし
 * @note	 
 */
CPropertiesWnd::CPropertiesWnd():
 m_pListObject(0)
{
    std::shared_ptr<CStdPropertyTree> Tmp(new CStdPropertyTree);
    m_psMultiObjcetsProperty = Tmp;
    m_uuidParts = boost::uuids::nil_uuid();
}

/**
 * @brief   デストラクター
 * @param   なし
 * @return	なし
 * @note	 
 */
CPropertiesWnd::~CPropertiesWnd()
{
}


/**
 * @brief   レイアウト調整
 * @param   なし
 * @return	なし
 * @note	 
 */
void CPropertiesWnd::AdjustLayout()
{
	if (GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect rectClient,rectCombo;
	GetClientRect(rectClient);

	//m_wndObjectCombo.GetWindowRect(&rectCombo);

	int cyCmb = 0;//rectCombo.Size().cy;
	int cyTlb = m_wndToolBar.CalcFixedLayout(FALSE, TRUE).cy;

	//m_wndObjectCombo.SetWindowPos(NULL, rectClient.left, rectClient.top, rectClient.Width(), 200, SWP_NOACTIVATE | SWP_NOZORDER);
	m_wndToolBar.SetWindowPos(NULL, rectClient.left, rectClient.top + cyCmb, rectClient.Width(), cyTlb, SWP_NOACTIVATE | SWP_NOZORDER);
	m_wndPropGrid.SetWindowPos(NULL, rectClient.left, rectClient.top + cyCmb + cyTlb, rectClient.Width(), rectClient.Height() -(cyCmb+cyTlb), SWP_NOACTIVATE | SWP_NOZORDER);
}

/**
 * @brief   生成時処理要求
 * @param   [in]    lpCreateStruct  初期化パラメータへのポインタ
 * @return  0:生成成功
 * @note	 
 */
int CPropertiesWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDockablePane::OnCreate(lpCreateStruct) == -1)
		return -1;

	CRect rectDummy;
	rectDummy.SetRectEmpty();
/*
	// コンボ ボックスの作成:
	const DWORD dwViewStyle = WS_CHILD | WS_VISIBLE | CBS_DROPDOWNLIST | WS_BORDER | CBS_SORT | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;

	if (!m_wndObjectCombo.Create(dwViewStyle, rectDummy, this, 1))
	{
		DB_PRINT0("プロパティ コンボ ボックスを作成できませんでした\n");
		return -1;      // 作成できない場合
	}

	m_wndObjectCombo.AddString(_T("アプリケーション"));
	m_wndObjectCombo.AddString(_T("プロパティ ウィンドウ"));
	m_wndObjectCombo.SetFont(CFont::FromHandle((HFONT) GetStockObject(DEFAULT_GUI_FONT)));
	m_wndObjectCombo.SetCurSel(0);
*/
	if (!m_wndPropGrid.Create(WS_VISIBLE | WS_CHILD, rectDummy, this, 2))
	{
		DB_PRINT(_T("プロパティ グリッドを作成できませんでした\n"));
		return -1;      // 作成できない場合
	}

	InitPropList();

	m_wndToolBar.Create(this, AFX_DEFAULT_TOOLBAR_STYLE, IDR_PROPERTIES);
	m_wndToolBar.LoadToolBar(IDR_PROPERTIES, 0, 0, TRUE /* ロックされています*/);
	m_wndToolBar.CleanUpLockedImages();
	m_wndToolBar.LoadBitmap(IDR_PROPERTIES, 0, 0, TRUE /* ロックされました*/);

	m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() | CBRS_TOOLTIPS | CBRS_FLYBY);
	m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() & ~(CBRS_GRIPPER | CBRS_SIZE_DYNAMIC | CBRS_BORDER_TOP | CBRS_BORDER_BOTTOM | CBRS_BORDER_LEFT | CBRS_BORDER_RIGHT));
	m_wndToolBar.SetOwner(this);

    m_wndToolBar.SetButtonText(ID_EXPAND_ALL, GET_STR(STR_PORP_SORT_ITEM));
    m_wndToolBar.SetButtonText(ID_SORTPROPERTIES, GET_STR(STR_PORP_SORT_ALPH));
    //m_wndToolBar.SetButtonText(ID_PROPERTIES1, GET_STR(                 ));
    //m_wndToolBar.SetButtonText(ID_PROPERTIES2, GET_STR(                 ));


	// すべてのコマンドが、親フレーム経由ではなくこのコントロール経由で渡されます:
	m_wndToolBar.SetRouteCommandsViaFrame(FALSE);

	AdjustLayout();
	return 0;
}

/**
 *  @brief ウインドウサイズ変更
 *  @param [in] nType   要求されるサイズ変更のタイプ  (SIZE_MAXIMIZED....)
 *  @param [in] cx      クライアント領域の新しい幅 
 *  @param [in] cy      クライアント領域の新しい高さ
 *  @retval           なし
 *  @note
 */
void CPropertiesWnd::OnSize(UINT nType, int cx, int cy)
{
	CDockablePane::OnSize(nType, cx, cy);
	AdjustLayout();
}

/**
 *  @brief  全プロパティ展開通知ボタン押下
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CPropertiesWnd::OnExpandAllProperties()
{
	m_wndPropGrid.ExpandAll();
}

/**
 *  @brief  全プロパティ展開通知ボタンUI更新
 *  @param  [in out] コマンド ユーザー インターフェイス
 *  @retval なし
 *  @note   
 */
void CPropertiesWnd::OnUpdateExpandAllProperties(CCmdUI* pCmdUI)
{
}

/**
 *  @brief  プロパティソートボタン押下
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CPropertiesWnd::OnSortProperties()
{
	m_wndPropGrid.SetAlphabeticMode(!m_wndPropGrid.IsAlphabeticMode());
}

/**
 *  @brief  プロパティソートボタンUI更新
 *  @param  [in out] コマンド ユーザー インターフェイス
 *  @retval  なし
 *  @note
 */
void CPropertiesWnd::OnUpdateSortProperties(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck(m_wndPropGrid.IsAlphabeticMode());
}

/**
 *  @brief  プロパティボタン押下
 *  @param   なし
 *  @retval  なし
 *  @note    未使用
 */
void CPropertiesWnd::OnProperties1()
{
}

/**
 *  @brief  プロパティボタンUI更新
 *  @param  [in out] コマンド ユーザー インターフェイス
 *  @retval  なし
 *  @note    未使用
 */
void CPropertiesWnd::OnUpdateProperties1(CCmdUI* /*pCmdUI*/)
{
}

/**
 *  @brief  イベントボタン押下
 *  @param   なし
 *  @retval  なし
 *  @note    未使用
 */
void CPropertiesWnd::OnProperties2()
{
}

/**
 *  @brief  イベントボタンUI更新
 *  @param  [in out] コマンド ユーザー インターフェイス
 *  @retval  なし
 *  @note    未使用
 */
void CPropertiesWnd::OnUpdateProperties2(CCmdUI* /*pCmdUI*/)
{
}

/**
 * @brief   プロパティリスト消去
 * @param   なし
 * @return  なし
 * @note	
 */
void CPropertiesWnd::ClearPropList()
{
    m_wndPropGrid.RemoveAll();
    m_wndPropGrid.KillTimer(ONT_PROPERTY);
    m_wndPropGrid.RedrawWindow();
}

/**
 * @brief   プロパティリスト初期化
 * @param   なし
 * @return  なし
 * @note	
 */
void CPropertiesWnd::InitPropList()
{
	SetPropListFont();

	m_wndPropGrid.EnableHeaderCtrl(FALSE);
	m_wndPropGrid.EnableDescriptionArea();
	m_wndPropGrid.SetVSDotNetLook();
	m_wndPropGrid.MarkModifiedProperties();
}

/**
 * @brief   フォーカス取得時処理
 * @param   [in] pOldWnd フォーカスが外れるウインドウ
 * @return  なし
 * @note	
 */
void CPropertiesWnd::OnSetFocus(CWnd* pOldWnd)
{
	CDockablePane::OnSetFocus(pOldWnd);
	m_wndPropGrid.SetFocus();
}

/**
 * @brief   システム設定変更通知
 * @param   [in] uFlags      変更されたシステムパラメータを示すフラグ
 * @param   [in] lpszSection 変更されたセクション名を示す文字列へのポインタ
 * @return  なし
 * @note	
 */
void CPropertiesWnd::OnSettingChange(UINT uFlags, LPCTSTR lpszSection)
{
	CDockablePane::OnSettingChange(uFlags, lpszSection);
	SetPropListFont();
}

/**
 * @brief   フォント設定
 * @param   なし
 * @return  なし
 * @note	
 */
void CPropertiesWnd::SetPropListFont()
{
	::DeleteObject(m_fntPropList.Detach());

	LOGFONT lf;
	afxGlobalData.fontRegular.GetLogFont(&lf);

	NONCLIENTMETRICS info;
	info.cbSize = sizeof(info);

	afxGlobalData.GetNonClientMetrics(info);

	lf.lfHeight = info.lfMenuFont.lfHeight;
	lf.lfWeight = info.lfMenuFont.lfWeight;
	lf.lfItalic = info.lfMenuFont.lfItalic;

	m_fntPropList.CreateFontIndirect(&lf);

	m_wndPropGrid.SetFont(&m_fntPropList);
}

/**
 * @brief   リスト更新
 * @param   [in]    iWin	    View種別ID
 * @param   [in]    pWnd	    ウインドウ	 	  
 * @return	常に０	
 * @note	Viewで選択変更があった時に通知する 
 * @exception   なし
 */
void CPropertiesWnd::RedrawList()
{
    CMFCPropertyGridProperty* pProp = m_wndPropGrid.GetCurSel();

    DWORD dw = 0;
    if (pProp)
    {
        dw = pProp->GetData();
    }

    if(!m_pListObject)
    {
        return;
    }

    if (m_pListObject->empty())
    {
        return;
    }

    for(auto wObj: *m_pListObject)
    {
        auto pObj = wObj.lock(); 
        if(pObj)
        {
            pObj->ResetProperty();
        }
    }
    SetObject(m_pListObject);

    if (pProp)
    {
        CMFCPropertyGridProperty* pProp2;
        pProp2 = m_wndPropGrid.FindItemByData(dw);
        if (pProp2)
        {
            m_wndPropGrid.EnsureVisible(pProp2);
            m_wndPropGrid.SetCurSel(pProp2);
        }
    }

}

/**
 * @brief   オブジェクト設定
 * @param   [in]    iWin	    View種別ID
 * @param   [in]    pWnd	    ウインドウ	 	  
 * @return	常に０	
 * @note	Viewで選択変更があった時に通知する 
 * @exception   なし
 */
void CPropertiesWnd::SetObject(std::vector<std::weak_ptr<CDrawingObject>>* pListObject)
{
    size_t lSize;
    //---------------------------------
    //前回選択時のプロパティを解除する
    //---------------------------------
    try
    {
        if (m_pListObject)
        {
            // CPartsDef::ReplaceObject
            // などで既にオブジェクトがない場合があるので
            // 基本的に何もしない
        }
    }
    catch(...)
    {
        STD_DBG(_T("ReleaseProperty Error"));
    }
    //---------------------------------


    //---------------------------------
    //前回選択時のプロパティを設定する
    //---------------------------------
    lSize = pListObject->size();
    if (lSize == 0)
    {
        m_wndPropGrid.RemoveAll();
        m_wndPropGrid.RedrawWindow();
    }
    else if (lSize == 1)
    {
#if 0
DB_PRINT(_T("CPropertiesWnd::SetObject \n"));
#endif
        auto pObject = (*pListObject->begin()).lock();
        if (pObject)
        {
            pObject->SetProperty(&m_wndPropGrid, 0);
        }
    }
    else
    {
        // 複数選択時のプロパティ設定
        
        int iSelPorpFeatuers = CDrawingObject::P_ALL;
        for(auto wObject: *pListObject)
        {
            auto pObject =  wObject.lock();
            if (pObject)
            {
                iSelPorpFeatuers &= pObject->GetPropertyGroupFeatures();
            }
        }
        SetMutiSelectProperty(&m_wndPropGrid, iSelPorpFeatuers);
    }
    //---------------------------------

    m_pListObject = pListObject;
}

/**
 * @brief   プロパティ初期化
 * @param   [in]    iSelPorp
 * @return	
 * @note	 
 * @exception   なし
 */
TREE_GRID_ITEM*  CPropertiesWnd::InitMultiSelectProperty(int iSelPorp)
{
    m_wndPropGrid.RemoveAll();

    boost::tuple< CStdPropertyItem*, void*> pairVal;

    CStdPropertyItem* pItem;
    NAME_TREE<CStdPropertyItem>* pTree;

    NAME_TREE_ITEM<CStdPropertyItem>* pNextItem;
    NAME_TREE_ITEM<CStdPropertyItem>* pGroupItem;

    m_psMultiObjcetsProperty->SetParent(this);
    pTree = m_psMultiObjcetsProperty->GetTreeInstance();

    m_crObj     = DRAW_CONFIG->crFront;
    m_iLayerId  = 0;
    m_iWidth    = 0;
    m_iLineType = 0;

    pTree->Clear();

    //グループ作成
    pGroupItem  = pTree->AddChild(NULL, NULL, GET_STR(STR_PRO_FIG), _T("Property"));

    std::shared_ptr<CObjectDef> pDef;
    pDef = THIS_APP->GetObjectDefByUuid(m_uuidParts).lock();

    CPartsDef* pParts = NULL;
    if (pDef)
    {
        pParts = dynamic_cast<CPartsDef*>(pDef.get());
    }

    //-----------
    //色
    //-----------
    if (iSelPorp & CDrawingObject::P_BASE)
    {
        CStdPropertyItemDef DefColor(PROP_COLOR, 
            GET_STR(STR_PRO_COLOR)   ,
            _T("Color"),
            GET_STR(STR_PRO_INFO_COLOR), 
            true, 
            DISP_NONE,
            DRAW_CONFIG->crFront);

        pItem = new CStdPropertyItem( DefColor, PropColor, m_psMultiObjcetsProperty.get(), NULL, &m_crObj);
        pNextItem = pTree->AddChild(pGroupItem, pItem, DefColor.strDspName, pItem->GetName());

        //-----------
        //レイヤーID
        //-----------
        int iLayerMax = 0;
        if (pParts)
        {
            iLayerMax = pParts->GetLayerMax() - 1;
        }

        CStdPropertyItemDef DefLayer(PROP_INT_RANGE, 
            GET_STR(STR_PRO_LAYER_ID), 
            _T("Layer"),
            GET_STR(STR_PRO_INFO_LAYER_ID),
             true, 
             DISP_NONE,
             0, 0, iLayerMax);

        pItem = new CStdPropertyItem( DefLayer, PropLayer, m_psMultiObjcetsProperty.get(), NULL, &m_iLayerId);
        pNextItem = pTree->AddNext(pNextItem, pItem, DefLayer.strDspName, pItem->GetName());

        if (iSelPorp & CDrawingObject::P_LINE)
        {
            //------------
            //線幅
            //------------
            CStdPropertyItemDef DefLineWidth(PROP_LINE_WIDTH, 
                GET_STR(STR_PRO_LINE_WIDTH)   , 
                _T("Width"),
                GET_STR(STR_PRO_INFO_LINE_WIDTH), 
                true,
                DISP_NONE,
                0);

            pItem = new CStdPropertyItem( DefLineWidth, PropLineWidth, m_psMultiObjcetsProperty.get(), NULL, &m_iWidth);
            pNextItem = pTree->AddNext(pNextItem, pItem, DefLineWidth.strDspName, pItem->GetName());

            //------------
            //線種
            //------------
            CStdPropertyItemDef DefLineType(PROP_LINE_TYPE, 
                GET_STR(STR_PRO_LINE_TYPE)   , 
                _T("Type"),
                GET_STR(STR_PRO_INFO_LINE_TYPE), 
                false,
                DISP_NONE,
                0);

            pItem = new CStdPropertyItem( DefLineType, PropLineType, m_psMultiObjcetsProperty.get(), NULL, &m_iLineType);
            pNextItem = pTree->AddNext(pNextItem, pItem, DefLineType.strDspName, pItem->GetName());
        }
    }
    return pGroupItem;
}

/**
 *  @brief   複数オブジェクトに対するプロパティ設定.
 *  @param   [in] pGrid    オブジェクトコントロールへのポインタ
 *  @param   [in] iSelPorp 
 *  @retval     なし
 *  @note
 */
void CPropertiesWnd::SetMutiSelectProperty(CMFCPropertyGridCtrl *pGrid, int iPropFeatures)
{
    m_wndPropGrid.RemoveAll();

    InitMultiSelectProperty(iPropFeatures);

    m_wndPropGrid.SetGridData(m_psMultiObjcetsProperty.get());
}

/**
 *  @brief   プロパティ変更(色)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CPropertiesWnd::PropColor(CStdPropertyItem* pData , void* pObj)
{
    CPropertiesWnd* pWnd = reinterpret_cast<CPropertiesWnd*>(pObj);
    try
    {
        COLORREF crColor = pData->anyData.GetColor();
        int iId;
        CPartsDef* pCtrl;

        if (!pWnd->m_pListObject)
        {
            return false;
        }

        for(auto wObject: *pWnd->m_pListObject)
        {
            auto pObject = wObject.lock(); 
            if(!pObject)
            {
                continue;
            }
            iId = pObject->GetId();
            pCtrl = pObject->GetPartsDef();
            pObject->SetColor(crColor);
            pCtrl->Draw(iId);
        }
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
    }
    return true;
}


/**
 *  @brief   プロパティ変更(レイヤー)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CPropertiesWnd::PropLayer(CStdPropertyItem* pData, void* pObj)
{

    CPropertiesWnd* pWnd = reinterpret_cast<CPropertiesWnd*>(pObj);
    try
    {
        if (!pWnd->m_pListObject)
        {
            return false;
        }

        int  iLayer = pData->anyData.GetInt();
        int iId;
        CPartsDef* pCtrl;
        for(auto wObject: *pWnd->m_pListObject)
        {
            auto pObject = wObject.lock(); 
            if(!pObject)
            {
                continue;
            }

            iId = pObject->GetId();
            pCtrl = pObject->GetPartsDef();
            pObject->SetLayer(iLayer);
            pCtrl->Draw(iId);
        }
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
    }
    return true;
}

/**
 *  @brief   プロパティ変更(線幅)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CPropertiesWnd::PropLineWidth    (CStdPropertyItem* pData, void* pObj)
{
    CPropertiesWnd* pWnd = reinterpret_cast<CPropertiesWnd*>(pObj);
    try
    {
        if (!pWnd->m_pListObject)
        {
            return false;
        }

        int iId;
        CPartsDef* pCtrl;
        for(auto wObject: *pWnd->m_pListObject)
        {
            auto pObject = wObject.lock(); 
            if(!pObject)
            {
                continue;
            }

            iId = pObject->GetId();
            pCtrl = pObject->GetPartsDef();
            pCtrl->HideObject(iId);
            pObject->SetLineWidth(pData->anyData.GetInt());
            pCtrl->Draw(iId);
        }
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

/**
 *  @brief   プロパティ変更(線種)
 *  @param   [in] pData  変更アイテム
 *  @param   [in] pObj   このオブジェクト
 *  @retval     なし
 *  @note
 */
bool CPropertiesWnd::PropLineType (CStdPropertyItem* pData, void* pObj)
{
    CPropertiesWnd* pWnd = reinterpret_cast<CPropertiesWnd*>(pObj);
    try
    {
        if (!pWnd->m_pListObject)
        {
            return false;
        }

        int iId;
        CPartsDef* pCtrl;
        for(auto wObject: *pWnd->m_pListObject)
        {
            auto pObject = wObject.lock(); 
            if(!pObject)
            {
                continue;
            }

            iId = pObject->GetId();
            pCtrl = pObject->GetPartsDef();
            pCtrl->HideObject(iId);
            pObject->SetLineType(pData->anyData.GetInt());
            pCtrl->Draw(iId);
        }
    }
    catch(...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}


/**
 * @brief   View更新通知
 * @param   [in]    iSelList    選択オブジェクトリスト
 * @param   [in]    pWnd	    ウインドウ	 	  
 * @return	常に０	
 * @note	Viewで選択変更があった時に通知する 
 * @exception   なし
 */
LRESULT CPropertiesWnd::OnViewSelchange(WPARAM iSelList, LPARAM pItem)
{
    auto pSelectObjects = reinterpret_cast<std::vector<std::weak_ptr<CDrawingObject>>*>(iSelList);

    try
    {
        bool bDef = false;
        if (!pSelectObjects)
        {
            bDef = true;
        }
        else 
        {
            if (pSelectObjects->size() == 0)
            {
                bDef = true;
            }
        }

        if (bDef)
        {
            //オブジェクト定義を設定
            std::shared_ptr<CObjectDef> pDef;
            pDef = THIS_APP->GetObjectDefByUuid(m_uuidParts).lock();

            if (pDef)
            {
                pDef->SetProperty(&m_wndPropGrid, 0);
            }
        }
        else
        {
            //オブジェクトを設定
            SetObject(pSelectObjects);
        }
    }
    catch(MockException& e)
    {
        STD_DBG(e.GetErrMsg().c_str());
    }
    catch(...)
    {
        STD_DBG(_T("Unknown Error"));
    }


    return 0;
}


/**
 * @brief   ビュークリア
 * @param   [in] wParam 使用しない
 * @param   [in] lParam 使用しない
 * @return  なし
 * @note    
 */
LRESULT CPropertiesWnd::OnViewClear(WPARAM pMessage, LPARAM lParam)
{
    return 0;
}


/**
 * @brief    View活性化を通知
 * @param    [in] pMessage タブタイトル文字
 * @param    [in] lParam   0:MockSletchView 1:EditView
 * @retval   なし
 * @note	
 */
LRESULT CPropertiesWnd::OnViewActivate(WPARAM pMessage, LPARAM lParam)
{
    if (lParam != 0)
    {
        ClearPropList();
        m_uuidParts = boost::uuids::nil_uuid();
        return 0;
    }

    StdString* pTitle = reinterpret_cast<StdString*>(pMessage);

    CWnd* pWnd = THIS_APP->GetView(*pTitle);
    CMockSketchView* pView = reinterpret_cast<CMockSketchView*>(pWnd);
    if (pView == NULL)
    {
        STD_DBG(_T("CMockSketchView was deleted."));
        return 0;
    }


    CMockSketchDoc* pDoc = pView->GetDocument();
    boost::uuids::uuid uuidParts;
    uuidParts = pDoc->GetPartsId();

    if (m_uuidParts != uuidParts)
    {
        ClearPropList();
        m_uuidParts = uuidParts;
    }
    return 0;
}

/**
 *  @brief モード変更
 *  @param [in] wParam   アプリケーションモード(EXEC_COMMON::E_APP_MODE)
 *  @param [in] lParam   オブジェクト定義      (CObjectDef)
 *  @retval
 *  @note
 */
LRESULT CPropertiesWnd::OnModeChange(WPARAM wParam, LPARAM lParam)
{
    using namespace EXEC_COMMON;

    E_EXEC_STS  eSts;
    CObjectDef* pDef;

    eSts = static_cast<E_EXEC_STS>(wParam);
    pDef = reinterpret_cast<CObjectDef*>(lParam);

    if (eSts == EX_EDIT)
    {
        ClearPropList();
        m_uuidParts = boost::uuids::nil_uuid();
    }

    return 0;
}

/**
 *  @brief プロパティリスト更新
 *  @param [in] wParam   
 *  @param [in] lParam 
 *  @retval
 *  @note   プロパティリストを動的に変更する必要がある場合呼び出す
 */
LRESULT CPropertiesWnd::OnUpdaatePropertyList(WPARAM wParam, LPARAM lParam)
{
    RedrawList();
    return 0;
}


/**
 *  @brief プロパティアイテム更新
 *  @param [in] wObj   
 *  @param [in] itemPos 
 *  @retval
 *  @note  
 */
LRESULT CPropertiesWnd::OnUpdaatePropertyItem(WPARAM wObj, LPARAM itemPos)
{
    /*
    CDrawingObject* pObj = reinterpret_cast<CDrawingObject*>(wObj);
    int             iPos = static_cast<int>(itemPos);


    if (m_pListObject->size() == 1)
    {
        if (m_pListObject->at(0) == pObj)
        {
            CMFCPropertyGridProperty* pGridProp;
            pGridProp = m_wndPropGrid

            pObject->SetProperty(&m_wndPropGrid, 0);
        }
    }
    */
    return 0;
}

LRESULT CPropertiesWnd::OnLoadStart(WPARAM wObj, LPARAM itemPos)
{
    ClearPropList();
    return 0;
}
