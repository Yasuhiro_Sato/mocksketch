/**
 * @brief			CPropertiesWndヘッダーファイル
 * @file			CPropertiesWnd.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

#pragma once

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DrawingObject/CDrawingObject.h"

#include "Utility/CPropertyGrid.h"
#include "ToolBar/CustomToolBar.h"
#include "SubWindow/CustomDocablePane.h"

/*---------------------------------------------------*/
/*  Classess                                         */
/*---------------------------------------------------*/
class CStdPropertyItem;
class CStdPropertyTree;
class CObjectDef;

/**
 * @class   CPropertiesToolBar
 * @brief                        
   
 */
class CPropertiesToolBar : public CCustomToolBar
{
public:
   CPropertiesToolBar():CCustomToolBar(){
        SetName(_T("CPropertiesToolBar"));
    }

	virtual void OnUpdateCmdUI(CFrameWnd* /*pTarget*/, BOOL bDisableIfNoHndler)
	{
		CMFCToolBar::OnUpdateCmdUI((CFrameWnd*) GetOwner(), bDisableIfNoHndler);
	}
    
	virtual BOOL AllowShowOnList() const { return FALSE; }
};

/**
 * @class   CPropertiesWnd
 * @brief                        
   
 */
class CPropertiesWnd : public CCustomDocablePane
{
// コンストラクション
public:
	CPropertiesWnd();

	void AdjustLayout();

    void SetDefaultProperty();

    void SetObject(std::vector<std::weak_ptr<CDrawingObject>>* pListObject);

    void ClearPropList();

    void RedrawList();

// 属性
public:
	void SetVSDotNetLook(BOOL bSet)
	{
		m_wndPropGrid.SetVSDotNetLook(bSet);
		m_wndPropGrid.SetGroupNameFullWidth(bSet);
	}

protected:
	CFont m_fntPropList;
	//CComboBox m_wndObjectCombo;   
	CPropertiesToolBar m_wndToolBar;
	CPropertyGrid      m_wndPropGrid;
    
    std::vector<std::weak_ptr<CDrawingObject>>*  m_pListObject;

// 実装
public:
	virtual ~CPropertiesWnd();

protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnExpandAllProperties();
	afx_msg void OnUpdateExpandAllProperties(CCmdUI* pCmdUI);
	afx_msg void OnSortProperties();
	afx_msg void OnUpdateSortProperties(CCmdUI* pCmdUI);
	afx_msg void OnProperties1();
	afx_msg void OnUpdateProperties1(CCmdUI* pCmdUI);
	afx_msg void OnProperties2();
	afx_msg void OnUpdateProperties2(CCmdUI* pCmdUI);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnSettingChange(UINT uFlags, LPCTSTR lpszSection);
    afx_msg LRESULT OnViewSelchange(WPARAM iType, LPARAM pItem);
    //!<ビュークリア
    afx_msg LRESULT OnViewClear(WPARAM pMessage, LPARAM lParam);
    //!< View活性化通知
    afx_msg  LRESULT OnViewActivate(WPARAM pMessage, LPARAM lParam);

    //!<モード変更
    afx_msg LRESULT OnModeChange(WPARAM wParam, LPARAM lParam);

    //!<プロパティ更新
    afx_msg LRESULT OnUpdaatePropertyList(WPARAM wParam, LPARAM lParam);

    //!<プロパティアイテム更新
    afx_msg LRESULT OnUpdaatePropertyItem(WPARAM wParam, LPARAM lParam);

    afx_msg LRESULT OnLoadStart(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()

	void InitPropList();
	void SetPropListFont();

    //グループプロパティ設定用
    void SetMutiSelectProperty(CMFCPropertyGridCtrl *pGrid, int iPropFeatures);
    TREE_GRID_ITEM*  InitMultiSelectProperty(int iPropFeatures);


    static bool PropColor      (CStdPropertyItem* pData, void* pObj);
    static bool PropLayer      (CStdPropertyItem* pData, void* pObj);
    static bool PropLineWidth  (CStdPropertyItem* pData, void* pObj);
    static bool PropLineType   (CStdPropertyItem* pData, void* pObj);

protected:
    //グループ設定用プロパティ

    //!< 色
    COLORREF m_crObj;

    //!< レイヤーID
    int m_iLayerId;

    //!< 線幅
    int m_iWidth;

    //!< 線種
    int m_iLineType;

    //!< 複数設定用プロパティ
    std::shared_ptr<CStdPropertyTree>      m_psMultiObjcetsProperty;

    //!< 表示中のObjectsCtr
    boost::uuids::uuid m_uuidParts;

};

