/**
 * @brief			PropertyGroupDlg実装ファイル
 * @file			PropertyGroupDlg.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:02:47
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MockSketch.h"
#include "./PropertyGroupDlg.h"
#include "./TextInputDlg.h"
#include "./PropertySetDlg.h"
#include "afxdialogex.h"
#include "System/CSystem.h"
#include "System/MOCK_ERROR.h"
#include "./CProjectCtrl.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
IMPLEMENT_DYNAMIC(PropertyGroupDlg, CDialog)

BEGIN_MESSAGE_MAP(PropertyGroupDlg, CDialog)
    ON_LBN_DBLCLK(IDC_LST_GROUP, &PropertyGroupDlg::OnLbnDblclkLstGroup)
    ON_COMMAND(ID_MNU_PROP_FILE_OPEN    , &PropertyGroupDlg::OnMenuFileOpen)
    ON_UPDATE_COMMAND_UI(ID_MNU_PROP_FILE_OPEN    , &PropertyGroupDlg::OnUpdateMenuFileOpen)
    ON_COMMAND(ID_MNU_PROP_FILE_SAVE    , &PropertyGroupDlg::OnMenuFileSave)
    ON_UPDATE_COMMAND_UI(ID_MNU_PROP_FILE_SAVE    , &PropertyGroupDlg::OnUpdateMenuFileSave)
    ON_COMMAND(ID_MNU_PROP_FILE_SAVE_AS , &PropertyGroupDlg::OnMenuFileSaveAs)
    ON_UPDATE_COMMAND_UI(ID_MNU_PROP_FILE_SAVE_AS , &PropertyGroupDlg::OnUpdateMenuFileSaveAs)
    ON_COMMAND(ID_MNU_PROP_FILE_NEW     , &PropertyGroupDlg::OnMenuFileNew)
    ON_UPDATE_COMMAND_UI(ID_MNU_PROP_FILE_NEW     , &PropertyGroupDlg::OnUpdateMenuFileNew)
    ON_COMMAND(ID_MNU_PROP_FILE_CLOSE   , &PropertyGroupDlg::OnMenuFileClose)
    ON_UPDATE_COMMAND_UI(ID_MNU_PROP_FILE_CLOSE   , &PropertyGroupDlg::OnUpdateMenuFileClose)

    ON_COMMAND(ID_EDIT_COPY   , &PropertyGroupDlg::OnMenuCopy)
    ON_UPDATE_COMMAND_UI(ID_EDIT_COPY   , &PropertyGroupDlg::OnUpdateMenuCopy)
    ON_COMMAND(ID_EDIT_PASTE   , &PropertyGroupDlg::OnMenuPaste)
    ON_UPDATE_COMMAND_UI(ID_EDIT_PASTE   , &PropertyGroupDlg::OnUpdateMenuPaste)
    ON_COMMAND(ID_MNU_PROP_EDIT_ADD         , &PropertyGroupDlg::OnMenuEditAdd)
    ON_UPDATE_COMMAND_UI(ID_MNU_PROP_EDIT_ADD         , &PropertyGroupDlg::OnUpdateMenuEditAdd)
    ON_COMMAND(ID_MNU_PROP_EDIT_DEL         , &PropertyGroupDlg::OnMenuEditDel)
    ON_UPDATE_COMMAND_UI(ID_MNU_PROP_EDIT_DEL         , &PropertyGroupDlg::OnUpdateMenuEditDel)
    ON_COMMAND(ID_MNU_PROP_EDIT_CHG_NAME    , &PropertyGroupDlg::OnMenuEditChgName)
    ON_UPDATE_COMMAND_UI(ID_MNU_PROP_EDIT_CHG_NAME    , &PropertyGroupDlg::OnUpdateMenuEditChgName)

    ON_COMMAND_RANGE(ID_MNU_PROP_FILE_MRU, ID_MNU_PROP_FILE_MRU05, &PropertyGroupDlg::OnMruFile)
    ON_UPDATE_COMMAND_UI_RANGE(ID_MNU_PROP_FILE_MRU, ID_MNU_PROP_FILE_MRU05, &PropertyGroupDlg::OnUpdateMruFile)


    ON_WM_CONTEXTMENU()
    ON_WM_INITMENUPOPUP()
    ON_BN_CLICKED(IDOK, &PropertyGroupDlg::OnBnClickedOk)
    ON_WM_CLOSE()
END_MESSAGE_MAP()

/**
 * コンストラクタ
 */
PropertyGroupDlg::PropertyGroupDlg(CWnd* pParent /*=NULL*/)
	: CDialog(PropertyGroupDlg::IDD, pParent),
    m_bChg(false)
{

}

/**
 * デストラクタ
 */
PropertyGroupDlg::~PropertyGroupDlg()
{
}

/*
*
*  @brief  プロジェクト保存・読み込み表示
*  @param  [in] fileName ファイル名 (NULL 名前をつけて保存)
*  @param  [in] bReplace 
*  @param  [in] lFlags   
*  @param  [in] bOpenFileDialog TRUE:ファイルを開く FALSE 保存
*  @retval なし
*  @note   
*
*/
bool PropertyGroupDlg::DoFiletName(CString& fileName, 
                                  bool bReplace, 
                                  DWORD lFlags, 
                                  BOOL bOpenFileDialog)
{
    CString strPropExt = _T("*.");

    strPropExt += SYS_PROPERTY->GetPropertyExt().c_str();


    CString strFilter;

    strFilter += _T("MockSketch Property Files (");
    strFilter += strPropExt;
    strFilter += _T(")|");
    strFilter += strPropExt;
    strFilter += _T("|");
    strFilter += GET_STR(STR_FILE_ALLFILTER);
    strFilter += _T("|"); 
    strFilter += _T("*.*||");


    CFileDialog dlgFile(
        bOpenFileDialog,    //  TRUE:[ファイルを開く] FALSE:[ファイル名を付けて保存]
        strPropExt, //  規定のファイル拡張子
        fileName,   //  初期ファイル名
        OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, 
        strFilter, //  フィルターを指定する一連の文字列のペア
        NULL,              //  ファイル ダイアログ ボックスの親ウィンドウまたはオーナー ウィンドウへのポインター
        0, FALSE);


    dlgFile.m_ofn.Flags |= lFlags;


    /*
    strFilter += _T("MockSketch Property Files (*.");
    strFilter += SYS_PROPERTY->GetPropertyExt().c_str();
    strFilter += _T(")|");
    strFilter += _T("*.");
    strFilter += SYS_PROPERTY->GetPropertyExt().c_str();

    allFilter = GET_STR(STR_FILE_ALLFILTER);
    strFilter += allFilter;
    strFilter += (TCHAR)'\0';   // next string please
    strFilter += _T("*.*");
    strFilter += (TCHAR)'\0'; 
    strFilter += (TCHAR)'\0';   // last string

    dlgFile.m_ofn.nMaxCustFilter++;

    dlgFile.m_ofn.lpstrFilter = strFilter;
    dlgFile.m_ofn.lpstrTitle = title;
    dlgFile.m_ofn.lpstrFile = fileName.GetBuffer(_MAX_PATH);
    */

    INT_PTR nResult = dlgFile.DoModal();
    if (nResult == IDOK)
    {   
        fileName = dlgFile.GetPathName();
        return true;
    }


    //fileName.ReleaseBuffer();
    return false;
}

/**
 *  @brief  フレームワークの自動的なデータ交換
 *  @param  pDX     CDataExchange オブジェクトへのポインタ
 *  @retval なし     
 *  @note   UpdateData メンバ関数から呼び出されます。
 */
void PropertyGroupDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_LST_GROUP, m_lstGroup);
}

/**
 *  @brief  ウインドウの初期化処理
 *  @param  なし
 *  @retval 常にTRUE     
 *  @note   
 */
BOOL PropertyGroupDlg::OnInitDialog()
{
    SetWindowText(_T("PropertyGroup"));

    CDialog::OnInitDialog();
    std::vector<StdString> lstGroupName;

    if (!m_menu.CreateMenu( ))
    {
		AfxThrowResourceException();
    }
    if (!m_mnuFile.CreateMenu( ))
    {
		AfxThrowResourceException();
    }

    UINT nFlag = MF_STRING | MF_ENABLED;
                                                     //("開く(&O)...");
    m_mnuFile.AppendMenu(nFlag , ID_MNU_PROP_FILE_OPEN, GET_STR(STR_MNU_FILE_OPEN));

                                                     //ファイルの保存
    m_mnuFile.AppendMenu(nFlag , ID_MNU_PROP_FILE_SAVE, GET_STR(STR_MNU_SAVE_FILE_NONAME));

                                                     //名前をつけけて保存
    m_mnuFile.AppendMenu(nFlag , ID_MNU_PROP_FILE_SAVE_AS, GET_STR(STR_MNU_SAVE_AS_FILE_NONAME));
                                                     
                                                     //新規作成(&N
    m_mnuFile.AppendMenu(nFlag , ID_MNU_PROP_FILE_NEW, GET_STR(STR_MNU_FILE_NEW));

                                                    //最近使ったファイル
    m_mnuFile.AppendMenu(nFlag , ID_MNU_PROP_FILE_MRU, GET_STR(STR_MNU_FILE_MRU_FILE));


    m_mnuFile.AppendMenu(MF_SEPARATOR);

                                                     //閉じる
    m_mnuFile.AppendMenu(nFlag , ID_MNU_PROP_FILE_CLOSE, GET_STR(STR_MNU_FILE_CLOSE));

                                                     //ファイル
    m_menu.AppendMenu(nFlag | MF_POPUP, (UINT_PTR)m_mnuFile.m_hMenu, GET_STR(STR_MNU_FILE));



	// Set the menu
	SetMenu(&m_menu);


    //MRU先頭を読み込む
    if(SYS_PROPERTY->GetMruFileNum() > 0)
    {
        std::shared_ptr<CPropertyGroup> pGroup;
        pGroup = SYS_PROPERTY->LoadMruFile(0);
        if (pGroup)
        {
            _SetGroup(pGroup);
        }
    }

    RECT rcWin;

    GetWindowRect(&rcWin);
    int iMenuHeight = GetSystemMetrics(SM_CYMENU);
    rcWin.bottom += iMenuHeight;

    MoveWindow(&rcWin);


    return TRUE;
}

/**
 *  @brief リスト左ダブルクリック
 *  @param なし
 *  @retval          
 *  @note
 */

void PropertyGroupDlg::OnLbnDblclkLstGroup()
{
    int iSel;
    iSel = m_lstGroup.GetCurSel( );
    StdString strProertySet;

    if (iSel == LB_ERR)
    {
        return;
    }
    CString strSel;
    m_lstGroup.GetText(iSel, strSel);
    strProertySet = strSel;
    CPropertySet propertySet;

    if (!m_pGroup->GetPropertySet(&propertySet, (LPCTSTR)strSel))
    {
        return;
    }

    PropertySetDlg dlgPropertySet;
    dlgPropertySet.SetPorpertySet(&propertySet);

    if (dlgPropertySet.DoModal() == IDOK)
    {
        CPropertySet* pSet = m_pGroup->GetPropertySetInstance((LPCTSTR)strSel);
        pSet->DeleteAll();
        dlgPropertySet.GetPorpertySet(pSet);

        if (propertySet != *pSet)
        {
            m_bChg = true;
        }
    }
    m_pCopySet.reset();
}

/**
 *  @brief コンテキストメニュー選択
 *  @param [in] pWnd   マウスの右ボタンがクリックされたウィンドウのハンドル
 *  @param [in] point  クリックされたときの、カーソルの画面座標位置 
 *  @retval           なし
 *  @note
 */
void PropertyGroupDlg::OnContextMenu(CWnd* pWnd, CPoint point)
{
    CMenu mnuEdit;


    mnuEdit.CreatePopupMenu();
    bool bGroup = (bool)m_pGroup;
    bool bSel   = (m_lstGroup.GetCurSel() != LB_ERR);

    UINT nGroup = (bGroup ? MF_ENABLED : MF_DISABLED);
    UINT nSel   = (bSel   ? MF_ENABLED : MF_DISABLED);
    UINT nPaste = (m_pCopySet ? MF_ENABLED : MF_DISABLED);

    UINT nFlagCopy      = MF_STRING| bSel;
    UINT nFlagPaste     = MF_STRING| nPaste;
    UINT nFlagAdd       = MF_STRING| nGroup;
    UINT nFlagDel       = MF_STRING| nSel;
    UINT nFlagChgNmae   = MF_STRING| nSel;

    mnuEdit.AppendMenu(nFlagCopy, ID_EDIT_COPY, GET_STR(STR_MNU_EDITOR_COPY));
    mnuEdit.AppendMenu(nFlagPaste, ID_EDIT_PASTE,GET_STR(STR_MNU_EDITOR_PASTE));
    mnuEdit.AppendMenu(MF_SEPARATOR);
    mnuEdit.AppendMenu(nFlagAdd, ID_MNU_PROP_EDIT_ADD      , GET_STR(STR_MNU_PROP_EDIT_ADD));
    mnuEdit.AppendMenu(nFlagDel, ID_MNU_PROP_EDIT_DEL      , GET_STR(STR_MNU_PROP_EDIT_DEL));
    mnuEdit.AppendMenu(nFlagChgNmae, ID_MNU_PROP_EDIT_CHG_NAME , GET_STR(STR_MNU_PROP_EDIT_CHG_NAME));

                
    UINT uiMenu;
    uiMenu = mnuEdit.TrackPopupMenu(
        TPM_LEFTALIGN  |    //クリック時のX座標をメニューの左辺にする
        TPM_RIGHTBUTTON,    //右クリックでメニュー選択可能とする
        point.x,point.y,    //メニューの表示位置
        this      //このメニューを所有するウィンドウ
    );

    if(uiMenu)
    {
        PostMessage(WM_SYSCOMMAND, uiMenu, 0);
    }
    mnuEdit.DestroyMenu();
}

/**
 * @brief   メニュー -> ファイル開く
 * @param   なし
 * @return  なし
 * @note    
 */
void PropertyGroupDlg::OnMenuFileOpen()
{
    CString fileName;

    if (!m_pGroup)
    {
        //初期ディレクトリを設定
        std::shared_ptr<CProjectCtrl> pPorj;
        pPorj = THIS_APP->GetProject().lock();

        if (pPorj)
        {
            fileName = pPorj->GetDir().c_str();
            fileName += _T("\\*.");
            fileName += SYS_PROPERTY->GetPropertyExt().c_str();

        }
        else
        {
            fileName = SYS_CONFIG->GetLibraryPath().parent_path().c_str();
            fileName += _T("\\*.");
            fileName += SYS_PROPERTY->GetPropertyExt().c_str();
        }
    }
    else
    {
        fileName = m_pGroup->GetFilePath().c_str();
    }

    if (!DoFiletName(fileName, 
                    false, 
                    0, 
                    TRUE))
    {
        return;
    }

    std::shared_ptr<CPropertyGroup> pGroup;
    pGroup  = std::make_shared<CPropertyGroup>();
    
    if (!pGroup->LoadFile((LPCTSTR)fileName, true))
    {
        pGroup.reset();
    }
    else
    {
        _SetGroup(pGroup);
        SYS_PROPERTY->AddMruFile(m_pGroup->GetFilePath().c_str());
    }
}

/**
 * @brief   メニュー -> ファイル開く UI更新
 *  @param  [inout] pCmdUI コマンド ユーザー インターフェイス
 * @return  なし
 * @note    
 */
void PropertyGroupDlg::OnUpdateMenuFileOpen   (CCmdUI *pCmdUI)
{
}

/**
 * @brief   メニュー -> 保存
 * @param   なし
 * @return  なし
 * @note    
 */
void PropertyGroupDlg::OnMenuFileSave()
{
    if(m_pGroup->SaveFile(m_pGroup->GetFilePath(), true))
    {
        SYS_PROPERTY->AddMruFile(m_pGroup->GetFilePath().c_str());

        _ReloadSystemProperty();

        m_bChg = false;
    }
}

/**
 * @brief   メニュー -> 保存 UI更新
 *  @param  [inout] pCmdUI コマンド ユーザー インターフェイス
 * @return  なし
 * @note    
 */
void PropertyGroupDlg::OnUpdateMenuFileSave   (CCmdUI *pCmdUI)
{
}


/**
 * @brief   メニュー -> 名前をつけて保存
 * @param   なし
 * @return  なし
 * @note    
 */
void PropertyGroupDlg::OnMenuFileSaveAs()
{
    CString fileName;
    fileName = m_pGroup->GetFilePath().c_str();
    if (!DoFiletName(fileName, 
                    true,           //bReplace
                    0,              //lFlags
                    FALSE))          //bOpenFileDialog
    {
        return;
    }

    if (m_pGroup->SaveFile((LPCTSTR)fileName, true))
    {
        SYS_PROPERTY->AddMruFile((LPCTSTR)fileName);

        _ReloadSystemProperty();

        m_bChg = false;
    }

}

/**
 * @brief   メニュー -> 名前をつけて保存 UI更新
 *  @param  [inout] pCmdUI コマンド ユーザー インターフェイス
 * @return  なし
 * @note    
 */
void PropertyGroupDlg::OnUpdateMenuFileSaveAs (CCmdUI *pCmdUI)
{
}

/**
 * @brief   メニュー -> 新規作成
 * @param   なし
 * @return  なし
 * @note    
 */
void PropertyGroupDlg::OnMenuFileNew()
{
    CString fileName;

    StdPath path = SYS_CONFIG->GetLibraryPath().parent_path();
    std::vector<StdString> lstFile;
   
    
    StdString strFileStem = _T("PropertyGroup");

    StdString strFileName = _T("*.");
    strFileName += SYS_PROPERTY->GetPropertyExt();

    CUtil::FindFiles( &lstFile, strFileName, path, false);
    strFileStem = CUtil::FindNumberingString( &lstFile, strFileStem);
    strFileStem += _T(".");
    strFileStem += SYS_PROPERTY->GetPropertyExt();

    path /= strFileStem;


    //初期ディレクトリを設定
    fileName = path.c_str();
        
    if (!DoFiletName(fileName, 
                false, 
                0, 
                TRUE))
    {
        return;
    }

    std::shared_ptr<CPropertyGroup> pGroup;
    pGroup  = std::make_shared<CPropertyGroup>();


    //仮のPropertySetを追加しておく
    bool bRet;
    bRet = pGroup->CretePropertySet(_T("PropertySet"));
    STD_ASSERT(bRet);

    path = (LPCTSTR)fileName;
    
    if(pGroup->SaveFile(path, true))
    {
        _SetGroup(pGroup);
        SYS_PROPERTY->AddMruFile(path.c_str());
    }
}

/**
 * @brief   メニュー -> 新規作成 UI更新
 *  @param  [inout] pCmdUI コマンド ユーザー インターフェイス
 * @return  なし
 * @note    
 */
void PropertyGroupDlg::OnUpdateMenuFileNew    (CCmdUI *pCmdUI)
{
}

/**
 * @brief   システムプロパティ変更問い合わせ
 * @param   なし
 * @return  なし
 * @note    
 */
bool PropertyGroupDlg::_ReloadSystemProperty()
{

    StdPath path = m_pGroup->GetFilePath();

    if (!path.has_parent_path())
    {
        return false;
    }

    bool bRet;
    StdPath pathCurrent;
    pathCurrent = path.parent_path();

    if( CUtil::IsPathEqual(pathCurrent , SYS_CONFIG->strPathScriptAddins))
    {
        bRet = SYS_PROPERTY->UnloadDir(pathCurrent, true);
        bRet = SYS_PROPERTY->LoadDir(pathCurrent, true);

        return true;
    }

    StdPath  pathLib;
    pathLib = SYS_CONFIG->GetLibraryPath().parent_path();

    if( CUtil::IsPathEqual(pathCurrent, pathLib))
    {
        bRet = SYS_PROPERTY->UnloadDir(pathCurrent, true);
        bRet = SYS_PROPERTY->LoadDir(pathCurrent, true);
        return true;
    }

    //初期ディレクトリを設定
    std::shared_ptr<CProjectCtrl> pPorj;
    pPorj = THIS_APP->GetProject().lock();

    if (!pPorj)
    {
        return false;
    }

    if( CUtil::IsPathEqual(pathCurrent, pPorj->GetDir()))
    {
        bRet = SYS_PROPERTY->UnloadDir(pathCurrent, true);
        bRet = SYS_PROPERTY->LoadDir(pathCurrent, true);
        return true;
    }

    return false;
}


/**
 * @brief   メニュー -> 終了
 * @param   なし
 * @return  なし
 * @note    
 */
void PropertyGroupDlg::OnMenuFileClose()
{
    if (m_bChg)
    {
        //_T("データが変更されています。\n保存しますか？");
        int iAns = AfxMessageBox(GET_STR(STR_MB_SAVE),MB_OKCANCEL);
        if (iAns == IDOK)
        {
            OnMenuFileSave();

            //プロパティを更新
            if (_ReloadSystemProperty())
            {
                ::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_UPDATE_PORPERTY_SET, 0, 0);
            }
        }
    }
    OnOK();
}

/**
 * @brief   メニュー -> 終了 UI更新
 *  @param  [inout] pCmdUI コマンド ユーザー インターフェイス
 * @return  なし
 * @note    
 */
void PropertyGroupDlg::OnUpdateMenuFileClose  (CCmdUI *pCmdUI)
{

}

/**
 * @brief   メニュー -> 追加
 * @param   なし
 * @return  なし
 * @note    
 */
void PropertyGroupDlg::OnMenuEditAdd()
{
    std::set<StdString>  setList;
    SYS_PROPERTY->GetPropertySetNameList(&setList);
    StdString strName;
    strName = CUtil::FindNumberingString(&setList, _T("PropertySet"));

    //プロパティセット名を入力
    CTextInputDlg dlgPropSet;
    

                        //プロパティセット名を入力してください
    dlgPropSet.SetInfo(GET_STR(STR_PROP_EDIT_INPUT_NAME), strName.c_str());

    if(dlgPropSet.DoModal() != IDOK)
    {
        return;
    }

    strName = dlgPropSet.GetText();

    std::set<StdString>::iterator iteSet;
    iteSet = setList.find(strName);

    if (iteSet != setList.end())
    {
        //すでにそのプロパティセット名は使用されています
        AfxMessageBox(GET_STR(STR_PROP_ERR_INPUT_NAME));
        return;
    }

    if (m_pGroup)
    {
        if(!m_pGroup->CretePropertySet(strName))
        {
            AfxMessageBox(GET_STR(STR_PROP_ERR_INPUT_NAME));
        }
    }
    m_bChg = true;

    m_lstGroup.AddString(strName.c_str());
    m_pCopySet.reset();
}

/**
 * @brief   メニュー ->  追加 UI更新
 *  @param  [inout] pCmdUI コマンド ユーザー インターフェイス
 * @return  なし
 * @note    
 */
void PropertyGroupDlg::OnUpdateMenuEditAdd    (CCmdUI *pCmdUI)
{
    bool bEnable = (bool)m_pGroup;
    pCmdUI->Enable(bEnable);
}

/**
 * @brief   メニュー -> 削除
 * @param   なし
 * @return  なし
 * @note    
 */
void PropertyGroupDlg::OnMenuEditDel()
{
    int iSel;
    iSel = m_lstGroup.GetCurSel();

    if (iSel == LB_ERR)
    {
        return;
    }
    CString strItem;
    m_lstGroup.GetText(iSel, strItem);

    if (m_pGroup)
    {
        if(!m_pGroup->DeletePropertySet((LPCTSTR)strItem))
        {
            AfxMessageBox(GET_STR(STR_PROP_ERR_DEL));
        }
        else
        {
            m_lstGroup.DeleteString(iSel);
        }
    }

    m_bChg = true;

    m_pCopySet.reset();
}

/**
 * @brief   メニュー -> 削除 UI更新
 *  @param  [inout] pCmdUI コマンド ユーザー インターフェイス
 * @return  なし
 * @note    
 */
void PropertyGroupDlg::OnUpdateMenuEditDel    (CCmdUI *pCmdUI)
{
    bool bEnable;
    bEnable = (m_lstGroup.GetCurSel() != LB_ERR);
    pCmdUI->Enable(bEnable);
}

/**
 * @brief   メニュー -> 名称変更
 * @param   なし
 * @return  なし
 * @note    
 */
void PropertyGroupDlg::OnMenuEditChgName()
{
    if (!m_pGroup)
    {
        return;
    }

    int iSel;
    iSel = m_lstGroup.GetCurSel();

    if (iSel == LB_ERR)
    {
        return;
    }
    CString strItem;
    m_lstGroup.GetText(iSel, strItem);

    CTextInputDlg dlgPropSet;

    StdString strName = strItem;
    

                        //プロパティセット名を入力してください
    dlgPropSet.SetInfo(GET_STR(STR_PROP_EDIT_INPUT_NAME), strName.c_str());

    if(dlgPropSet.DoModal() != IDOK)
    {
        return;
    }

    StdString strNewName = dlgPropSet.GetText();


    if(!m_pGroup->ChangeName(strName, strNewName))
    {
        return;
    }

    m_bChg = true;

    m_lstGroup.DeleteString(iSel);
    if(m_lstGroup.InsertString(iSel , strNewName.c_str()) == LB_ERR)
    {
        m_lstGroup.InsertString(-1 , strNewName.c_str());
    }
    m_pCopySet.reset();
}

/**
 * @brief   メニュー -> 名称変更 UI更新
 *  @param  [inout] pCmdUI コマンド ユーザー インターフェイス
 * @return  なし
 * @note    
 */
void PropertyGroupDlg::OnUpdateMenuEditChgName(CCmdUI *pCmdUI)
{
}

/**
 * @brief   メニュー -> コピー
 * @param   なし
 * @return  なし
 * @note    
 */
void PropertyGroupDlg::OnMenuCopy()
{
   int iSel;
    iSel = m_lstGroup.GetCurSel();

    if (iSel == LB_ERR)
    {
        return;
    }
    CString strItem;
    m_lstGroup.GetText(iSel, strItem);

    if (m_pGroup)
    {
        CPropertySet* pSet;
        pSet = m_pGroup->GetPropertySetInstance((LPCTSTR)strItem);

        if (pSet)
        {
            m_pCopySet = std::make_shared<CPropertySet>(*pSet);
            m_strCopySet = (LPCTSTR)strItem;
        }
    }
}

/**
 * @brief   メニュー -> コピー UI更新
 *  @param  [inout] pCmdUI コマンド ユーザー インターフェイス
 * @return  なし
 * @note    
 */
void PropertyGroupDlg::OnUpdateMenuCopy(CCmdUI *pCmdUI)
{
}

/**
 * @brief   メニュー -> 貼り付け
 * @param   なし
 * @return  なし
 * @note    
 */
void PropertyGroupDlg::OnMenuPaste()
{
    std::vector<StdString> lstName;
    m_pGroup->GetPropertySetNameList(&lstName);

    StdString strNewName;
    strNewName = CUtil::FindNumberingString(&lstName, m_strCopySet.c_str());

    if (m_pCopySet)
    {
        CPropertySet* pNewSet;
        m_pGroup->CretePropertySet(strNewName);
        pNewSet= m_pGroup->GetPropertySetInstance(strNewName);
        if (pNewSet)
        {
            *pNewSet = *(m_pCopySet.get());
        }
    }
    m_bChg = true;
    m_pCopySet.reset();
}

/**
 * @brief   メニュー -> 貼り付け UI更新
 *  @param  [inout] pCmdUI コマンド ユーザー インターフェイス
 * @return  なし
 * @note    
 */
void PropertyGroupDlg::OnUpdateMenuPaste(CCmdUI *pCmdUI)
{
}

/**
 *  @brief  MRUファイル選択
 *  @param  [in]  id
 *  @retval なし
 *  @note
 */
void PropertyGroupDlg::OnMruFile(UINT id)
{
    int iNum = id - ID_MNU_PROP_FILE_MRU01;
    std::shared_ptr<CPropertyGroup> pGroup;

    pGroup = SYS_PROPERTY->LoadMruFile(iNum);
    if (pGroup)
    {
        _SetGroup(pGroup);
         SYS_PROPERTY->AddMruFile(pGroup->GetFilePath().c_str());
    }
    else
    {
        //ファイルの読み込みに失敗しました。\nリストから除外しますか
        int iAns = AfxMessageBox(GET_STR(STR_ERROR_FILE_LOAD_AND_DELETE), MB_OKCANCEL);

        if (iAns == IDOK)
        {
            SYS_PROPERTY->DelMruFile(iNum);
        }
    }
}

/**
 *  @brief  プロパティグループ設定
 *  @param  [in]  pGroup
 *  @retval なし
 *  @note
 */
void PropertyGroupDlg::_SetGroup(std::shared_ptr<CPropertyGroup> pGroup)
{
    std::vector<StdString> lstPropertySet;
    
    m_pGroup = pGroup;

    m_pGroup->GetPropertySetNameList(&lstPropertySet);
    m_lstGroup.ResetContent();
    foreach(StdString& str, lstPropertySet)
    {
        m_lstGroup.AddString(str.c_str());
    }

    CString strTitle;
    strTitle.Format(_T("%s"), m_pGroup->GetName().c_str());

    SetWindowText(strTitle);
    m_pCopySet.reset();
}

/**
 *  @brief  MRUファイル選択UI更新
 *  @param  [in]  pCmdUI
 *  @retval なし
 *  @note
 */
void PropertyGroupDlg::OnUpdateMruFile(CCmdUI* pCmdUI)
{
    pCmdUI->Enable(true);
}

/**
 *  @brief  メニュー変更
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void PropertyGroupDlg::_ChangeMenu(HMENU hMenu)
{
    CMenu menu;
    MENUITEMINFO  info;

    if(!menu.Attach(hMenu))
    {
        return;
    }

    int iMenuCount = menu.GetMenuItemCount();

    for (int iCnt =0; iCnt < iMenuCount; iCnt++)
    {
        memset(&info, 0, sizeof(info));
        info.cbSize = sizeof(info);
        info.fMask = MIIM_TYPE | MIIM_SUBMENU | MIIM_ID;
        menu.GetMenuItemInfo(iCnt, &info, TRUE);
        if( info.fType != MFT_STRING)
        {
            continue;
        }

        if (info.wID == ID_MNU_PROP_FILE_MRU)
        {
            //最近使用したファイル
            m_mnuMruFiles.DestroyMenu();
            m_mnuMruFiles.CreateMenu();

            int iFileNum = SYS_PROPERTY->GetMruFileNum();
            CString strItem;

            if (iFileNum > 0)
            {
                for (int iFileCnt = 0; iFileCnt < iFileNum; iFileCnt++)
                {
                    strItem.Format(_T("&%d %s"), iFileCnt + 1, 
                            SYS_PROPERTY->GetMruFile(iFileCnt).c_str());
                    m_mnuMruFiles.AppendMenu(MF_STRING, 
                        ID_MNU_PROP_FILE_MRU01 + iFileCnt,
                        strItem);
                }
                CString strMenuItem;
                menu.GetMenuString(iCnt, strMenuItem, MF_BYPOSITION);
                info.dwTypeData = (LPWSTR)(LPCTSTR)strMenuItem;
                info.cch        = strMenuItem.GetLength();
                info.wID = 0;
                info.hSubMenu = m_mnuMruFiles.m_hMenu;
                menu.SetMenuItemInfo(iCnt, &info, TRUE);
                menu.EnableMenuItem(iCnt, MF_BYPOSITION|MF_ENABLED);
            }
            else
            {
                menu.EnableMenuItem(iCnt, MF_BYPOSITION|MF_GRAYED);
            }
        }
    }
    menu.Detach();
}

void PropertyGroupDlg::OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu)
{
    CDialog::OnInitMenuPopup(pPopupMenu, nIndex, bSysMenu);

    _ChangeMenu(pPopupMenu->m_hMenu);
}


void PropertyGroupDlg::OnBnClickedOk()
{
    OnMenuFileSave();

    //プロパティを更新
    if (_ReloadSystemProperty())
    {
        ::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_UPDATE_PORPERTY_SET, 0, 0);
    }

    OnOK();
}


void PropertyGroupDlg::OnClose()
{
    OnMenuFileClose();
    CDialog::OnClose();
}
