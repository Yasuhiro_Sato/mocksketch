/**
 * @brief			PropertyGroupDlgヘッダーファイル
 * @file			PropertyGroupDlg.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#pragma once

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "afxwin.h"

/*---------------------------------------------------*/
/*  Classes                                          */
/*---------------------------------------------------*/
class CPropertyGroup;
class CPropertySet;

// PropertyGroupDlg ダイアログ

class PropertyGroupDlg : public CDialog
{
	DECLARE_DYNAMIC(PropertyGroupDlg)

public:
	PropertyGroupDlg(CWnd* pParent = NULL);   // 標準コンストラクター
	virtual ~PropertyGroupDlg();

// ダイアログ データ
	enum { IDD = IDD_DLG_PROPERTY_GROUP };

public:
    virtual BOOL OnInitDialog();
    afx_msg void OnLbnDblclkLstGroup();

    bool  DoFiletName(CString& fileName, 
                       bool bReplace, 
                       DWORD lFlags, 
                       BOOL bOpenFileDialog);

    void OnMruFile(UINT id);

protected:

    void _SetGroup(std::shared_ptr<CPropertyGroup> pGroup);

    void _ChangeMenu(HMENU hMenu);

    bool _ReloadSystemProperty();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート

	DECLARE_MESSAGE_MAP()
    afx_msg void OnMenuFileOpen();
    afx_msg void OnUpdateMenuFileOpen   (CCmdUI *pCmdUI);
    afx_msg void OnMenuFileSave();
    afx_msg void OnUpdateMenuFileSave   (CCmdUI *pCmdUI);
    afx_msg void OnMenuFileSaveAs();
    afx_msg void OnUpdateMenuFileSaveAs (CCmdUI *pCmdUI);
    afx_msg void OnMenuFileNew();
    afx_msg void OnUpdateMenuFileNew    (CCmdUI *pCmdUI);
    afx_msg void OnMenuFileClose();
    afx_msg void OnUpdateMenuFileClose  (CCmdUI *pCmdUI);
    afx_msg void OnMenuEditAdd();
    afx_msg void OnUpdateMenuEditAdd    (CCmdUI *pCmdUI);
    afx_msg void OnMenuEditDel();
    afx_msg void OnUpdateMenuEditDel    (CCmdUI *pCmdUI);
    afx_msg void OnMenuEditChgName();
    afx_msg void OnUpdateMenuEditChgName(CCmdUI *pCmdUI);
    afx_msg void OnMenuCopy();
    afx_msg void OnUpdateMenuCopy       (CCmdUI *pCmdUI);
    afx_msg void OnMenuPaste();
    afx_msg void OnUpdateMenuPaste      (CCmdUI *pCmdUI);
    afx_msg void OnUpdateMruFile(CCmdUI* pCmdUI);

public:
    CMenu m_menu;
    CMenu m_mnuFile;
    CMenu m_mnuMruFiles;
    CListBox m_lstGroup;
    std::shared_ptr<CPropertyGroup>  m_pGroup;
    std::shared_ptr<CPropertySet>  m_pCopySet;
    StdString                        m_strCopySet;
    bool                             m_bChg;

    afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);
    afx_msg void OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu);
    afx_msg void OnBnClickedOk();
    afx_msg void OnClose();
};
