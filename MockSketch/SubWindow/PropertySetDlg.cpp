/**
 * @brief			PropertySetDlg実装ファイル
 * @file			PropertySetDlg.cpp
 * @author			Yasuhiro Sato
 * @date			01-2-2009 10:36:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MockSketch.h"
#include "./PropertySetDlg.h"
#include "afxdialogex.h"
#include "System/CSystem.h"
#include "PropertySetDlg.h"


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
IMPLEMENT_DYNAMIC(PropertySetDlg, CDialog)

BEGIN_MESSAGE_MAP(PropertySetDlg, CDialog)
    ON_BN_CLICKED(IDOK, &PropertySetDlg::OnBnClickedOk)
    ON_BN_CLICKED(IDCANCEL, &PropertySetDlg::OnBnClickedCancel)
    ON_WM_SIZE()
END_MESSAGE_MAP()


PropertySetDlg::PropertySetDlg(CWnd* pParent /*=NULL*/)
	: CDialog(PropertySetDlg::IDD, pParent)
{
    m_psPropertySet = new CPropertySet;

}

PropertySetDlg::~PropertySetDlg()
{
    delete m_psPropertySet;

}

void PropertySetDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_LST_PROPERTY_SET, m_vlstPropertySet);
}

void PropertySetDlg::SetPorpertySet(const CPropertySet* pSet)
{
    *m_psPropertySet = *pSet;
}


void PropertySetDlg::GetPorpertySet(CPropertySet* pSet)
{
    *pSet = *m_psPropertySet;
}

// PropertySetDlg メッセージ ハンドラー


BOOL PropertySetDlg::OnInitDialog()
{
    CDialog::OnInitDialog();

    m_vlstPropertySet.SetPropertySet(m_psPropertySet);
    m_vlstPropertySet.Setup();
    AdjustLayout();
    return TRUE; 
}


void PropertySetDlg::OnBnClickedOk()
{
    // TODO: ここにコントロール通知ハンドラー コードを追加します。
    CDialog::OnOK();
}


void PropertySetDlg::OnBnClickedCancel()
{
    // TODO: ここにコントロール通知ハンドラー コードを追加します。
    CDialog::OnCancel();
}


/**
 * @brief   レイアウト調整
 * @param   なし
 * @return	なし
 * @note	 
 */
void PropertySetDlg::AdjustLayout()
{
	if (GetSafeHwnd() == NULL)
	{
		return;
	}

    if(m_vlstPropertySet.m_hWnd == NULL)
    {
        return;
    }

    CRect rectClient;

    GetClientRect(rectClient);


    int cyMenu = 0;
    int cyButton;
    int iButtonMargin = 3;
	CRect rectButton;
    GetDlgItem(IDOK)->GetClientRect(rectButton);
    cyButton = rectButton.Height() + iButtonMargin * 2;

/*
    int cyTlb = m_wndToolBar.CalcFixedLayout(FALSE, TRUE).cy;

    m_wndToolBar.SetWindowPos(NULL, rectClient.left, 
                                    rectClient.top, 
                                    rectClient.Width(), 
                                    cyTlb, SWP_NOACTIVATE | SWP_NOZORDER);
*/
    m_vlstPropertySet.SetWindowPos(NULL, rectClient.left,
                                   rectClient.top + cyMenu, 
                                   rectClient.Width(), 
                                   rectClient.Height() - cyMenu - cyButton,
                                   SWP_NOACTIVATE | SWP_NOZORDER);
    int iButtonTop =  rectClient.Height() - cyButton;
    int iCancelleft = rectClient.Width() - rectButton.Width() - iButtonMargin;

    GetDlgItem(IDCANCEL)->MoveWindow( iCancelleft, iButtonTop,
                                   rectButton.Width(), rectButton.Height(), TRUE);
    iCancelleft -= (rectButton.Width() + iButtonMargin);

    GetDlgItem(IDOK)->MoveWindow(iCancelleft, iButtonTop,
                                   rectButton.Width(), rectButton.Height(), TRUE);
}


void PropertySetDlg::OnSize(UINT nType, int cx, int cy)
{
    CDialog::OnSize(nType, cx, cy);


    AdjustLayout();
}
