#pragma once
#include "afxcmn.h"
#include "./PropDefList.h"


class CPropertySet;

// PropertySetDlg ダイアログ

class PropertySetDlg : public CDialog
{
	DECLARE_DYNAMIC(PropertySetDlg)

public:
	PropertySetDlg(CWnd* pParent = NULL);   // 標準コンストラクター
	virtual ~PropertySetDlg();

    void SetPorpertySet(const CPropertySet* pSet);

    void GetPorpertySet(CPropertySet* pSet);

	void AdjustLayout();

    enum { IDD = IDD_DLG_PROPERTY_SET };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート

	DECLARE_MESSAGE_MAP()
public:
    CPropDefList m_vlstPropertySet;

    CPropertySet* m_psPropertySet;

    virtual BOOL OnInitDialog();
    afx_msg void OnBnClickedOk();
    afx_msg void OnBnClickedCancel();
    afx_msg void OnSize(UINT nType, int cx, int cy);
};
