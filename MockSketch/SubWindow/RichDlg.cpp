// RitchDlg.cpp : 実装ファイル
//

#include "stdafx.h"
#include "MockSketch.h"
#include "./RichDlg.h"


// CRichDlg ダイアログ

IMPLEMENT_DYNAMIC(CRichDlg, CDialog)

CRichDlg::CRichDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CRichDlg::IDD, pParent)
{

}

CRichDlg::~CRichDlg()
{
}

void CRichDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_RICH, m_Rich);
}


BEGIN_MESSAGE_MAP(CRichDlg, CDialog)
    ON_WM_SIZE()
END_MESSAGE_MAP()


/**
 *  @brief ウインドウサイズ変更
 *  @param [in] nType   要求されるサイズ変更のタイプ  (SIZE_MAXIMIZED....)
 *  @param [in] cx      クライアント領域の新しい幅 
 *  @param [in] cy      クライアント領域の新しい高さ
 *  @retval             なし
 *  @note
 */
void CRichDlg::OnSize(UINT nType, int cx, int cy)
{
    CDialog::OnSize(nType, cx, cy);

	CRect rectClient;
	GetClientRect(rectClient);
    if (m_Rich.m_hWnd)
    {
	    m_Rich.SetWindowPos(NULL, rectClient.left, 
                                  rectClient.top,
                                  rectClient.Width(), 
                                  rectClient.Height(), SWP_NOACTIVATE | SWP_NOZORDER);
    }
}


BOOL CRichDlg::PreTranslateMessage(MSG* pMsg)
{
    if(pMsg->message == WM_KEYDOWN)
    {
        if ((pMsg->hwnd == m_Rich.m_hWnd)||
            (pMsg->hwnd == m_hWnd))
        {
            if (pMsg->wParam == VK_ESCAPE)
            {
                CloseWindow();
            }
        }
    }
    return CDialog::PreTranslateMessage(pMsg);
}
