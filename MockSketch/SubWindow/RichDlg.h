#pragma once
#include "afxcmn.h"


// CRichDlg ダイアログ

class CRichDlg : public CDialog
{
	DECLARE_DYNAMIC(CRichDlg)

public:
	CRichDlg(CWnd* pParent = NULL);   // 標準コンストラクタ
	virtual ~CRichDlg();

// ダイアログ データ
	enum { IDD = IDD_DLG_RICH };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート

	DECLARE_MESSAGE_MAP()
public:
    CRichEditCtrl m_Rich;
    afx_msg void OnSize(UINT nType, int cx, int cy);
    virtual BOOL PreTranslateMessage(MSG* pMsg);
};
