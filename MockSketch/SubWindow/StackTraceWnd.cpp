/**
 * @brief			StackTraceWnd実装ファイル
 * @file			StackTraceWnd.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *	
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/

#include "stdafx.h"

#include "./StackTraceWnd.h"
#include "Resource.h"
#include "Resource2.h"
#include "MainFrm.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

BEGIN_MESSAGE_MAP(CStackTraceWnd, CDockablePane)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()

/**
*  @brief  コンストラクター.
*  @param  なし
*  @retval なし     
*  @note
*/
CStackTraceWnd::CStackTraceWnd()
{
}

/**
*  @brief  デストラクタ.
*  @param  なし
*  @retval なし     
*  @note
*/
CStackTraceWnd::~CStackTraceWnd()
{
}

/**
 * @brief   ウインドウ出力
 * @param   [in] pMessage  出力メッセージ
 * @retval  なし
 * @note	
 */
void  CStackTraceWnd::PrintOut(StdString& strMessage)
{
    m_wndOutputTrace.PrintOut(strMessage.c_str());
}

/**
 * @brief   ウインドウクリア
 * @param   なし
 * @retval  なし
 * @note	
 */void  CStackTraceWnd::ClearWindow()
{

    m_wndOutputTrace.ResetContent();
}


/**
 * 生成時処理要求
 * @param [in]    lpCreateStruct	初期化パラメータへのポインタ	 	  
 * @return		
 * @note	 
 * @exception   なし
 */
int CStackTraceWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (CDockablePane::OnCreate(lpCreateStruct) == -1)
        return -1;

    m_Font.CreateStockObject(DEFAULT_GUI_FONT);

    CRect rectDummy;
    rectDummy.SetRectEmpty();

    // 出力ペインの作成:
    const DWORD dwStyle = LBS_NOINTEGRALHEIGHT | WS_CHILD | WS_VISIBLE | WS_HSCROLL | WS_VSCROLL;

    if (!m_wndOutputTrace.Create(dwStyle, rectDummy, this, 0))
    {
        STD_DBG(_T("Output Create fail"));
        return -1;      // 作成できない場合
    }

    m_wndOutputTrace.SetFont(&m_Font);

    return 0;
}

/**
 *  @brief ウインドウサイズ変更
 *  @param [in] nType   要求されるサイズ変更のタイプ  (SIZE_MAXIMIZED....)
 *  @param [in] cx      クライアント領域の新しい幅 
 *  @param [in] cy      クライアント領域の新しい高さ
 *  @retval           なし
 *  @note
 */
void CStackTraceWnd::OnSize(UINT nType, int cx, int cy)
{
	CDockablePane::OnSize(nType, cx, cy);

	m_wndOutputTrace.SetWindowPos (NULL, -1, -1, cx, cy, SWP_NOMOVE | SWP_NOACTIVATE | SWP_NOZORDER);
}

/**
 *  @brief 水平スクロール調整
 *  @param [in] wndListBox  
 *  @retval           なし
 *  @note
 */
void CStackTraceWnd::AdjustHorzScroll(CListBox& wndListBox)
{
	CClientDC dc(this);
	CFont* pOldFont = dc.SelectObject(&m_Font);

	int cxExtentMax = 0;

	for (int i = 0; i < wndListBox.GetCount(); i ++)
	{
		CString strItem;
		wndListBox.GetText(i, strItem);

		cxExtentMax = max(cxExtentMax, dc.GetTextExtent(strItem).cx);
	}

	wndListBox.SetHorizontalExtent(cxExtentMax);
	dc.SelectObject(pOldFont);
}

/**
 *  @brief  ウインドウプロシジャ
 *  @param    [in]  message  処理される Windows メッセージ
 *  @param    [in]  wParam   メッセージの処理で使う付加情報1
 *  @param    [in]  lParam   メッセージの処理で使う付加情報2
 *  @retval   メッセージに依存する値
 *  @note   
 */
LRESULT CStackTraceWnd::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
    return CDockablePane::WindowProc(message, wParam, lParam);
}
