/**
 * @brief			StackTraceWndヘッダーファイル
 * @file			StackTraceWnd.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *	
 *
 * $
 * $
 * 
 */


#pragma once
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include    "COutputList.h"
#include "ToolBar/CustomToolBar.h"
#include "SubWindow/CustomDocablePane.h"

/*---------------------------------------------------*/
/*  class                                            */
/*---------------------------------------------------*/


/**
 * @class   CStackTraceWnd
 * @brief                        
 */
class CStackTraceWnd : public CCustomDocablePane
{
// コンストラクション
public:
    //!< コンストラクタ
    CStackTraceWnd();

    //!< デストラクタ
    virtual ~CStackTraceWnd();

    //追加
    void    PrintOut(StdString& strMessage);

    //!< ウインドウ内容消去
    void    ClearWindow();

protected:
    //!< フォント
    CFont m_Font;

    COutputList m_wndOutputTrace;

protected:
    afx_msg void OnLvnEndlabeleditTrace(NMHDR *pNMHDR, LRESULT *pResult);

	void AdjustHorzScroll(CListBox& wndListBox);

// 実装
public:

protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);

	DECLARE_MESSAGE_MAP()
    virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
};

