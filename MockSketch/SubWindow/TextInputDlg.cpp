/**
 * @brief			CTextInputDlg実装ファイル
 * @file			CTextInputDlg.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MockSketch.h"
#include "./TextInputDlg.h"


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
IMPLEMENT_DYNAMIC(CTextInputDlg, CDialog)

BEGIN_MESSAGE_MAP(CTextInputDlg, CDialog)
    ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()



/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CTextInputDlg::CTextInputDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTextInputDlg::IDD, pParent)
{
    m_bSelAll = false;
}

/**
 *  @brief  デストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CTextInputDlg::~CTextInputDlg()
{
}

/**
 *  @brief  フレームワークの自動的なデータ交換
 *  @param  pDX     CDataExchange オブジェクトへのポインタ
 *  @retval なし     
 *  @note   UpdateData メンバ関数から呼び出されます。
 */
void CTextInputDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

/**
 *  @brief  フレームワークの自動的なデータ交換
 *  @param  [in] strTitle   ダイアログバーに表示する文字
 *  @param  [in] strInfo    説明文字
 *  @retval なし     
 *  @note   UpdateData メンバ関数から呼び出されます。
 */
void CTextInputDlg::SetInfo(LPCTSTR strTitle, LPCTSTR strInfo)
{
    m_strTitle = strTitle;
    m_strInfo  = strInfo;
}


/**
 *  @brief  ウインドウの初期化処理
 *  @param  なし
 *  @retval 常にTRUE     
 *  @note   
 */
BOOL CTextInputDlg::OnInitDialog()
{
    CDialog::OnInitDialog();

    SetWindowText(m_strTitle.c_str());
    GetDlgItem(IDC_ST_TEXT)->SetWindowText(m_strInfo.c_str());
    GetDlgItem(IDC_ED_TEXT)->SetWindowText(m_strRet.c_str());

    if (m_bSelAll)
    {
        CEdit* pEdit;
        pEdit = dynamic_cast<CEdit*>(GetDlgItem(IDC_ED_TEXT));
        if (pEdit)
        {
            pEdit->SetSel(0, -1);
        }
    }
    return TRUE;
}

/**
 *  @brief  OK押し下げ時処理
 *  @param  なし
 *  @retval なし     
 *  @note   入力テキストを記憶 
 */
void CTextInputDlg::OnOK( )
{
    CString strInput;
    GetDlgItem(IDC_ED_TEXT)->GetWindowText(strInput);

    m_strRet = (LPCTSTR)strInput;
    CDialog::OnOK();
}

/**
 *  @brief  テキスト取得
 *  @param  なし
 *  @retval 入力テキスト     
 *  @note   
 */
StdString CTextInputDlg::GetText()
{
    return m_strRet;
}


/**
 *  @brief  ウインドウ表示・非表示イベント
 *  @param  [in] bShow 表示:true・非表示
 *  @param  [in] nStatus  SW_PARENTCLOSING or 
 *                        SW_PARENTOPENING  
 *  @retval なし
 *  @note   フレームワークより呼び出し
 */
void CTextInputDlg::OnShowWindow(BOOL bShow, UINT nStatus)
{
    CDialog::OnShowWindow(bShow, nStatus);
    if(bShow)
    {
        GetDlgItem(IDC_ED_TEXT)->SetFocus();
    }
 }
