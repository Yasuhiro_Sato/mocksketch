/**
 * @brief			CTextInputDlgヘッダーファイル
 * @file			CTextInputDlg.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#pragma once

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "Resource.h"
#include "resource2.h"


/*---------------------------------------------------*/
/*  class                                            */
/*---------------------------------------------------*/

/**
 * @class   CTextInputDlg
 * @brief   テキスト入力                     
 */
class CTextInputDlg : public CDialog
{
	DECLARE_DYNAMIC(CTextInputDlg)

public:
    //!< コンストラクタ
	CTextInputDlg(CWnd* pParent = NULL);   // 標準コンストラクタ

    //!< デストラクタ
    virtual ~CTextInputDlg();

    //!< 文字設定
    void SetInfo(LPCTSTR strTitle, LPCTSTR strInfo);

    //!< テキスト設定
    void SetText(LPCTSTR strINput ){m_strRet = strINput;}

    //!< テキスト取得
    StdString GetText();

    //!< OK選択時処理
    virtual void OnOK();

    //!< 全選択
    void SelectAll(bool bSel = true){m_bSelAll = bSel;}

    // ダイアログ データ
	enum { IDD = IDD_DLG_TEXT_INPUT };

protected:
    //!< 初期化処理
    virtual BOOL OnInitDialog();

    //!< フレームワークの自動的なデータ交換
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート

	DECLARE_MESSAGE_MAP()

protected:
    StdString m_strTitle; //!< タイトル

    StdString m_strInfo;  //!< 説明文

    StdString m_strRet;   //!< 入力文字

    bool m_bSelAll;       //!< 全選択

public:
    afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
};
