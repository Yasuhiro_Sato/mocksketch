/**
 * @brief			CZoomWnd実装ファイル
 * @file			CZoomWnd.cpp
 * @author			Yasuhiro Sato
 * @date			01-2-2009 10:36:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "Resource.h"
#include "MainFrm.h"
#include "MockSketch.h"

#include "./ZoomWnd.h"
#include "DefinitionObject/View/MockSketchDoc.h"
#include "DefinitionObject/View/MockSketchView.h"
#include "DefinitionObject/View/CDrawingView.h"
#include "DefinitionObject/CPartsDef.h"
#include "View/CLayer.h"
#include "System/CSystem.h"
#include "Utility/CUtility.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

/*---------------------------------------------------*/
/* Static variables                                  */
/*---------------------------------------------------*/


/*---------------------------------------------------*/
/* MACROS                                            */
/*---------------------------------------------------*/
BEGIN_MESSAGE_MAP(CZoomWnd, CDockablePane)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_WM_SETTINGCHANGE()
    ON_MESSAGE(WM_VIEW_CREATE,       OnViewCreate)
    ON_MESSAGE(WM_VIEW_ACTIVE, OnViewActivate)
    ON_MESSAGE(WM_VIEW_CLEAR_ALL, OnViewClear)
    ON_MESSAGE(WM_MODE_CHANGE, OnModeChange)

    ON_COMMAND(ID_ZOOM_BACK      , &CZoomWnd::OnZoomBack     )
    ON_UPDATE_COMMAND_UI(ID_ZOOM_BACK  , &CZoomWnd::OnUpdateZoomBack  )
    ON_COMMAND(ID_ZOOM_INDEX      , &CZoomWnd::OnZoomIndex     )
    ON_UPDATE_COMMAND_UI(ID_ZOOM_INDEX  , &CZoomWnd::OnUpdateZoomIndex  )
    ON_COMMAND(ID_ZOOM_SELECT      , &CZoomWnd::OnZoomSelect     )
    ON_UPDATE_COMMAND_UI(ID_ZOOM_SELECT  , &CZoomWnd::OnUpdateZoomSelect  )
    ON_COMMAND(ID_ZOOM_FRONT      , &CZoomWnd::OnZoomFront     )
    ON_UPDATE_COMMAND_UI(ID_ZOOM_FRONT  , &CZoomWnd::OnUpdateZoomFront  )
    ON_COMMAND(ID_ZOOM_TIMER      , &CZoomWnd::OnZoomTimer     )
    ON_UPDATE_COMMAND_UI(ID_ZOOM_TIMER  , &CZoomWnd::OnUpdateZoomTimer  )
    ON_COMMAND(ID_ZOOM_CHAR      , &CZoomWnd::OnZoomChar     )
    ON_UPDATE_COMMAND_UI(ID_ZOOM_CHAR  , &CZoomWnd::OnUpdateZoomChar  )

    /*
    ON_NOTIFY(LVN_ENDLABELEDIT, IDC_LST_VIRTUAL_IO_SET, OnLvnEndlabeleditVlst)
    ON_COMMAND(ID_MNU_LAYER_ADD, &CZoomWnd::OnMnuLayerAdd)
    ON_UPDATE_COMMAND_UI(ID_MNU_LAYER_ADD, &CZoomWnd::OnUpdateMnuLayerAdd)
    ON_COMMAND(ID_MNU_LAYER_DEL, &CZoomWnd::OnMnuLayerDel)
    ON_UPDATE_COMMAND_UI(ID_MNU_LAYER_DEL, &CZoomWnd::OnUpdateMnuLayerDel)
    ON_COMMAND(ID_MNU_LAYER_SCL, &CZoomWnd::OnMnuLayerScl)
    ON_UPDATE_COMMAND_UI(ID_MNU_LAYER_SCL, &CZoomWnd::OnUpdateMnuLayerScl)
    */
    ON_WM_CONTEXTMENU()
    ON_WM_TIMER()
    ON_WM_CLOSE()
END_MESSAGE_MAP()

/**
 * @brief   コンストラクター
 * @param   なし
 * @return	なし
 * @note	 
 */
CZoomWnd::CZoomWnd()
{
    //m_pCtrl = NULL;
}

/**
 * @brief   デストラクター
 * @param   なし
 * @return	なし
 * @note	 
 */
CZoomWnd::~CZoomWnd()
{
}

/**
 * @brief   レイアウト調整
 * @param   なし
 * @return	なし
 * @note	 
 */
void CZoomWnd::AdjustLayout()
{
	if (GetSafeHwnd() == NULL)
	{
		return;
	}

	CRect rectClient;

    GetClientRect(rectClient);

    int cyTlb = m_wndToolBar.CalcFixedLayout(FALSE, TRUE).cy;

    m_wndToolBar.SetWindowPos(NULL, rectClient.left, 
                                    rectClient.top, 
                                    rectClient.Width(), 
                                    cyTlb, SWP_NOACTIVATE | SWP_NOZORDER);


    //m_vlstLayer.RedrawWindow();
}

/**
 * @brief   レイアウト調整
 * @param   なし
 * @return	なし
 * @note	 
 */
int CZoomWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDockablePane::OnCreate(lpCreateStruct) == -1)
    {
		return -1;
    }


	m_wndToolBar.Create(this, AFX_DEFAULT_TOOLBAR_STYLE, IDR_ZOOM);
	m_wndToolBar.LoadToolBar(IDR_ZOOM, 0, 0, TRUE /* ロックされています*/);
	m_wndToolBar.CleanUpLockedImages();
	m_wndToolBar.LoadBitmap(IDR_ZOOM, 0, 0, TRUE /* ロックされました*/);

	m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() | CBRS_TOOLTIPS | CBRS_FLYBY);
	m_wndToolBar.SetPaneStyle(m_wndToolBar.GetPaneStyle() & ~(CBRS_GRIPPER | CBRS_SIZE_DYNAMIC | CBRS_BORDER_TOP | CBRS_BORDER_BOTTOM | CBRS_BORDER_LEFT | CBRS_BORDER_RIGHT));
	m_wndToolBar.SetOwner(this);

    // すべてのコマンドが、親フレーム経由ではなくこのコントロール経由で渡されます:
	m_wndToolBar.SetRouteCommandsViaFrame(FALSE);


    //生成及び調整
	AdjustLayout();


	return 0;
}

/**
 *  @brief ウインドウサイズ変更
 *  @param [in] nType   要求されるサイズ変更のタイプ  (SIZE_MAXIMIZED....)
 *  @param [in] cx      クライアント領域の新しい幅 
 *  @param [in] cy      クライアント領域の新しい高さ
 *  @retval           なし
 *  @note
 */
void CZoomWnd::OnSize(UINT nType, int cx, int cy)
{
	CDockablePane::OnSize(nType, cx, cy);
	AdjustLayout();
}



/**
 *  @brief  ウインドウフォーカス設定
 *  @param  [out] pOldWnd  今までフォーカスがあったウインドウ
 *  @retval なし
 *  @note   
 */
void CZoomWnd::OnSetFocus(CWnd* pOldWnd)
{
	CDockablePane::OnSetFocus(pOldWnd);
}


/**
 *  @brief  設定変更
 *  @param  [in] uFlags  変更されたシステム パラメータを示すフラグ
 *  @param  [in] lpszSection   変更されたセクション名を示す文字列へのポインタ
 *  @retval なし
 *  @note   SystemParametersInfo参照
 */
void CZoomWnd::OnSettingChange(UINT uFlags, LPCTSTR lpszSection)
{
	CDockablePane::OnSettingChange(uFlags, lpszSection);
}


/**
 * @brief   View生成通知
 * @param   [in]    iWin	    View種別ID
 * @param   [in]    pWnd	    ウインドウ	 	  
 * @return	常に０	
 * @note	View生成完了後に各ウインドウに通知する 
 * @exception   なし
 */
LRESULT CZoomWnd::OnViewCreate(WPARAM iType, LPARAM pMessage)
{
    //TODO:他Viewに対応
    return 0;
}

/**
 *  @brief  ウインドウプロシジャ
 *  @param    [in]  message  処理される Windows メッセージ
 *  @param    [in]  wParam   メッセージの処理で使う付加情報1
 *  @param    [in]  lParam   メッセージの処理で使う付加情報2
 *  @retval   メッセージに依存する値
 *  @note   
 */
LRESULT CZoomWnd::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
    //!< 避難的対応 恐らくリフレクションの関連と思われるが
    //!< 同じメッセージが CWnd と CBasePainで発生する
    if (message == WM_NOTIFY)
    {
        if (wParam == IDC_LST_VIRTUAL_IO_SET)
        {
            return CWnd::WindowProc(message, wParam, lParam);

        }
    }

    return CDockablePane::WindowProc(message, wParam, lParam);
}

/**
 * @brief    View活性化を通知
 * @param    [in] pMessage タブタイトル文字
 * @param    [in] lParam   0:MockSletchView 1:EditView
 * @retval   なし
 * @note	
 */
LRESULT CZoomWnd::OnViewActivate(WPARAM pMessage, LPARAM lParam)
{
    if (lParam != 0)
    {
        return 0;
    }

    StdString* pTitle = reinterpret_cast<StdString*>(pMessage);


    CWnd* pWnd = THIS_APP->GetView(*pTitle);
    CMockSketchView* pView = reinterpret_cast<CMockSketchView*>(pWnd);
    if (pView == NULL)
    {
        STD_DBG(_T("CMockSketchView was deleted."));
        return 0;
    }

    CMockSketchDoc* pDoc = pView->GetDocument();
    std::shared_ptr<CObjectDef> pDef;
    pDef = pDoc->GetObjectDef().lock();
    if (!pDef)
    {
        STD_DBG(_T("CZoomWnd::OnViewActivate CObjectDef."));
        return 0;
    }

    if ((pDef->GetObjectType() != VIEW_COMMON::E_PARTS)&&
        (pDef->GetObjectType() != VIEW_COMMON::E_FIELD))
    {
        return 0;
    }


    //m_pCtrl = dynamic_cast<CPartsDef*>(pDef.get());

    //m_vlstLayer.SetItemCount( m_pCtrl->GetLayerMax());
    //RedrawWindow();

    return 0;
 
}


/**
 * @brief   ビュークリア
 * @param   [in] wParam 使用しない
 * @param   [in] lParam 使用しない
 * @return  なし
 * @note    
 */
LRESULT CZoomWnd::OnViewClear(WPARAM pMessage, LPARAM lParam)
{
    return 0;
}

/**
*  @brief コンテキストメニュー選択
*  @param [in] pWnd   マウスの右ボタンがクリックされたウィンドウのハンドル
*  @param [in] point  クリックされたときの、カーソルの画面座標位置 
*  @retval           なし
*  @note
*/
void CZoomWnd::OnContextMenu(CWnd* pWnd, CPoint point)
{
    /*
    if (pWnd->m_hWnd == m_vlstLayer.m_hWnd)
    {
	    theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_LAYER, point.x, point.y, this, TRUE);
    }
    */
}

/**
 *  @brief モード変更
 *  @param [in] wParam   アプリケーションモード(EXEC_COMMON::E_APP_MODE)
 *  @param [in] lParam   オブジェクト定義      (CObjectDef)
 *  @retval
 *  @note
 */
LRESULT CZoomWnd::OnModeChange(WPARAM wParam, LPARAM lParam)
{
    using namespace EXEC_COMMON;

    E_EXEC_STS  eSts;
    CObjectDef* pDef;

    eSts = static_cast<E_EXEC_STS>(wParam);
    pDef = reinterpret_cast<CObjectDef*>(lParam);

    if (eSts == EX_EDIT)
    {
        RedrawWindow();
    }


    return 0;
}


void CZoomWnd::OnZoomBack   ()
{
    THIS_APP->GetMainFrame()->DrawZoom(DRAW_FIG);
    m_eMode = DRAW_FIG;
}

void CZoomWnd::OnZoomIndex  ()
{
    THIS_APP->GetMainFrame()->DrawZoom(DRAW_INDEX);
    m_eMode = DRAW_INDEX;
}

void CZoomWnd::OnZoomSelect ()
{
    THIS_APP->GetMainFrame()->DrawZoom(DRAW_SEL);
    m_eMode = DRAW_SEL;
}

void CZoomWnd::OnZoomFront  ()
{
    THIS_APP->GetMainFrame()->DrawZoom(DRAW_FRONT);
    m_eMode = DRAW_FRONT;
}

void CZoomWnd::OnZoomTimer  ()
{

    if (!m_bTimer)
    {
        m_nTimer = SetTimer( 1, 500, NULL);
        m_bTimer = true;
    }
    else
    {
        KillTimer(m_nTimer);
        m_nTimer = 0;
        m_bTimer = false;
    }
}


void CZoomWnd::OnZoomChar()
{
    THIS_APP->GetMainFrame()->DrawZoom(DRAW_PRINT);
    m_eMode = DRAW_PRINT;
}


void CZoomWnd::OnUpdateZoomBack   (CCmdUI* pCmdUI)
{
}

void CZoomWnd::OnUpdateZoomIndex  (CCmdUI* pCmdUI)
{
}

void CZoomWnd::OnUpdateZoomSelect (CCmdUI* pCmdUI)
{
}

void CZoomWnd::OnUpdateZoomFront  (CCmdUI* pCmdUI)
{
}

void CZoomWnd::OnUpdateZoomTimer  (CCmdUI* pCmdUI)
{
}

void CZoomWnd::OnUpdateZoomChar  (CCmdUI* pCmdUI)
{
}

void CZoomWnd::OnTimer(UINT_PTR nIDEvent)
{
    if (m_nTimer == nIDEvent)
    {
        THIS_APP->GetMainFrame()->DrawZoom(m_eMode);
    }

    CCustomDocablePane::OnTimer(nIDEvent);
}


void CZoomWnd::OnClose()
{
    // TODO: ここにメッセージ ハンドラー コードを追加するか、既定の処理を呼び出します。
    if (m_bTimer)
    {
        KillTimer(m_nTimer);
        m_nTimer = 0;
    }

    CCustomDocablePane::OnClose();
}
