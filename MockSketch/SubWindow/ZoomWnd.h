/**
 * @brief			CZoomWndヘッダーファイル
 * @file			CZoomWnd.h
 * @author			Yasuhiro Sato
 * @date			01-2-2009 10:36:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef __ZOOM_WND_
#define __ZOOM_WND_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "Utility/VirtualTreeCtrl/VirtualCheckList.h"
#include "ToolBar/CustomToolBar.h"
#include "SubWindow/CustomDocablePane.h"

/*---------------------------------------------------*/
/*  classes                                          */
/*---------------------------------------------------*/
class CPartsDef;
class CDrawingView;

/**
 * @class   CPropertiesToolBar
 * @brief                        
   
 */
class CZoomToolBar : public CCustomToolBar
{
public:
    CZoomToolBar():CCustomToolBar(){
        SetName(_T("CZoomToolBar"));
    }

	virtual void OnUpdateCmdUI(CFrameWnd* /*pTarget*/, BOOL bDisableIfNoHndler)
	{
		CMFCToolBar::OnUpdateCmdUI((CFrameWnd*) GetOwner(), bDisableIfNoHndler);
	}
    
	virtual BOOL AllowShowOnList() const { return FALSE; }
};


/**
 * @class   CZoomWnd
 * @brief
 */
class CZoomWnd : public CCustomDocablePane
{

// コンストラクション
public:
	CZoomWnd();
	virtual ~CZoomWnd();

	void AdjustLayout();

// 属性
public:

protected:
    CZoomToolBar   m_wndToolBar;

    std::unique_ptr<CDrawingView*> ms_pView;  

    //!< DC取得
    HDC GetDc();

    bool m_bTimer = false;
    UINT_PTR m_nTimer = 0;;
    DRAWING_MODE m_eMode = DRAW_FRONT;

// 実装
protected:
    
    afx_msg void OnZoomBack   ();
    afx_msg void OnZoomIndex  ();
    afx_msg void OnZoomSelect ();
    afx_msg void OnZoomFront  ();
    afx_msg void OnZoomTimer  ();
    afx_msg void OnZoomChar   ();

    afx_msg void OnUpdateZoomBack   (CCmdUI* pCmdUI);
    afx_msg void OnUpdateZoomIndex  (CCmdUI* pCmdUI);
    afx_msg void OnUpdateZoomSelect (CCmdUI* pCmdUI);
    afx_msg void OnUpdateZoomFront  (CCmdUI* pCmdUI);
    afx_msg void OnUpdateZoomTimer  (CCmdUI* pCmdUI);
    afx_msg void OnUpdateZoomChar  (CCmdUI* pCmdUI);

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnSettingChange(UINT uFlags, LPCTSTR lpszSection);

	DECLARE_MESSAGE_MAP()



    //!< View生成通知
    afx_msg LRESULT OnViewCreate(WPARAM iType, LPARAM pMessage);

    //!< View活性化通知
    afx_msg  LRESULT OnViewActivate(WPARAM pMessage, LPARAM lParam);

    //!<ビュークリア
    afx_msg LRESULT OnViewClear(WPARAM pMessage, LPARAM lParam);

    //!<モード変更
    afx_msg LRESULT OnModeChange(WPARAM wParam, LPARAM lParam);



    virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
public:


    afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);
    afx_msg void OnTimer(UINT_PTR nIDEvent);
    afx_msg void OnClose();
};

#endif //__LAYER_WND_