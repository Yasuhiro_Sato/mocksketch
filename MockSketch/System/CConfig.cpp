/**
 * @brief			CConfig実装ファイル
 * @file			CConfig.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "CConfig.h"
#include "CSystem.h"
#include "CSystem.h"
#include "Utility/CustomPropertys.h"




/**
 * @brief   XML読み込み
 * @param   [in] pXml 
 * @return	なし
 * @note	 
 */
void CConfig::LoadXml(StdXmlArchiveIn* pXml)
{
    *pXml >> boost::serialization::make_nvp("Root", *this);
}

/**
 * @brief   XML書き込み
 * @param   [in] pXml
 * @return	なし
 * @note	 
 */
void CConfig::SaveXml(StdXmlArchiveOut* pXml)
{
    *pXml << boost::serialization::make_nvp("Root", *this);
}

/**
 * @brief   ファイル読み込み
 * @param   [in] path ファイルパス
 * @return	true 読み込み成功
 * @note	 
 */
bool CConfig::Load(StdPath path)
{
    namespace fs = boost::filesystem;

    //デフォルトデータ設定
    StdString strError;
    SetDefault();

    if (!fs::exists(path))
    {
        strError = MockException::FileErrorMessage(true, path,
                   GET_ERR_STR(e_file_exist));
        return false;
    }

    StdStreamIn inFs(path);
    bool bRet = true;

    try
    {
        {
            StdXmlArchiveIn inXml(inFs);
            LoadXml(&inXml);
        }
        inFs.close();
    }
    catch(std::exception& e )
    {
        inFs.close();
        strError = MockException::FileErrorMessage(true, path, 
                                  CUtil::CharToString(e.what()));
        bRet = false;
    }
    catch(...)
    {
        inFs.close();
        strError = MockException::FileErrorMessage(true, path);
        bRet = false;
    }

    if (!bRet)
    {
        AfxMessageBox( strError.c_str());
        ChangeOutputWindow (WIN_DEBUG);
        PrtintOut(WIN_DEBUG, strError.c_str());
    }
    return bRet;
}

/**
 * @brief   ファイル書き込み
 * @param   [in] path 
 * @retval  true 書き込み成功
 * @note 
 */
bool CConfig::Save(StdPath path)
{
    StdStreamOut outFs(path);
    bool bRet = true;
    StdString strError;

    try
    {
        {
            StdXmlArchiveOut outXml(outFs);
            SaveXml(&outXml);
        }
        outFs.close();
    }
    catch(std::exception& e )
    {
        outFs.close();
        strError = MockException::FileErrorMessage(false, path, 
                                  CUtil::CharToString(e.what()));
        ChangeOutputWindow (WIN_DEBUG);
        PrtintOut(WIN_DEBUG, strError.c_str());
        bRet = false;
    }
    catch(...)
    {
        outFs.close();
        strError = MockException::FileErrorMessage(false, path);
        bRet = false;
    }

    if (!bRet)
    {
        AfxMessageBox( strError.c_str());
        ChangeOutputWindow (WIN_DEBUG);
        PrtintOut(WIN_DEBUG, strError.c_str());
    }
    return bRet;
}


/**
 * @brief   ファイル読み込み
 * @param   [in] path ファイルパス
 * @return  true 読み込み成功
 * @note    未初期化時に使用
 *          (ウインドウ未生成,ロケール未設定)
 */
bool CConfig::Load2(StdPath path)
{
    namespace fs = boost::filesystem;

    //デフォルトデータ設定
    StdString strError;
    SetDefault();

    bool bRet = true;
    StdStringStream  strmError;

    if (!fs::exists(path))
    {
        strmError << StdFormat(_T("Setting file not exist (%s)")) %
                     path.c_str();
        STD_DBG(strmError.str().c_str());
        return false;
    }

    StdStreamIn inFs(path);

    try
    {
        {
            StdXmlArchiveIn inXml(inFs);
            LoadXml(&inXml);
        }
        inFs.close();
    }
    catch(std::exception& e )
    {
        inFs.close();
        strmError << StdFormat(_T("File read error %s:%s")) %
                     path.c_str() % CUtil::CharToString(e.what());
        bRet = false;
    }
    catch(...)
    {
        inFs.close();

        strmError << StdFormat(_T("File read error %s")) %
                     path.c_str();
        bRet = false;
    } 

    if (!bRet)
    {
        STD_DBG(strmError.str().c_str());
    }
    return bRet;
}

/**
 * @brief   ファイル書き込み
 * @param   [in] path 
 * @retval  true 書き込み成功
 * @note    未初期化時に使用
 *          (ウインドウ未生成,ロケール未設定)
 */
bool CConfig::Save2(StdPath path)
{
    StdStreamOut outFs(path);
    bool bRet = true;
    StdStringStream  strmError;

    try
    {
        {
            StdXmlArchiveOut outXml(outFs);
            SaveXml(&outXml);
        }
        outFs.close();
    }
    catch(std::exception& e )
    {
        outFs.close();
        strmError << StdFormat(_T("File write error %s:%s")) %
                     path.c_str() % CUtil::CharToString(e.what());
        bRet = false;
    }
    catch(...)
    {
        outFs.close();

        strmError << StdFormat(_T("File write error %s")) %
                     path.c_str();
        bRet = false;
    }

    if (!bRet)
    {
        STD_DBG(strmError.str().c_str());
    }
    return bRet;
}

/**
 *  @brief   コンストラクター
 *  @param   なし
 *  @retval  なし
 *  @note
 */
CConfigPropertyGrid::CConfigPropertyGrid()
{
    
}

/**
 *  @brief   プロパティ設定
 *  @param   [in]  pProp        設定するプロパティグリッド
 *  @param   [in]  pVal         設定値
 *  @param   [in]  eType        設定値の型
 *  @param   [in]  strItemName  設定値の名称
 *  @param   [in]  strItemExplane   設定値の説明
 *  @param   [in]  iVal             設定値のID
 *  @retval  true : 成功
 *  @note
 */
bool CConfigPropertyGrid::_SetPorperty(CMFCPropertyGridProperty* pProp,
                                    void* pVal,
                                    E_VALIANT_TYPE eType,
                                    StdString strItemName,
                                    StdString strItemExplane,
                                    int iVal,
                                    StdString strOption)
{
    COleVariant* pValiant;

    switch(eType)
    {
    case E_VAL_INT:
        pValiant = new COleVariant((long)(*(int*)(pVal)), VT_I4);
        break;

    case E_VAL_DOUBLE:
        pValiant = new COleVariant((double)(*(double*)(pVal)));
        break;

    case E_VAL_BOOL:

        pValiant = new COleVariant((short)
            ((*(bool*)(pVal)) ? VARIANT_TRUE: VARIANT_FALSE), 
            VT_BOOL);
        break;

    case E_VAL_STRING:
        {
        StdString* pStrVal;
        pStrVal = reinterpret_cast<StdString*>(pVal);
        pValiant = new COleVariant(pStrVal->c_str());
        }
        break;

    case E_VAL_COLOR:
    case E_VAL_FONT:
    case E_VAL_ENUM:
        //下で処理
        break;
        
    case  E_VAL_DIRECTORY: //未定義
    default:
        return false;

    }


    if (eType ==E_VAL_FONT)
    {
        //TODO:フォントスタイルが表示されてしまう
         CMFCPropertyGridFontProperty* pFontItem = 
            new CMFCPropertyGridFontProperty(strItemName.c_str(),
            (*(LOGFONT*)(pVal)), /*CF_EFFECTS |*/ CF_SCREENFONTS,
            strItemExplane.c_str());
        pFontItem->SetData(iVal);
	    pProp->AddSubItem(pFontItem);

    }
    else if (eType == E_VAL_COLOR)
    {
	    CMFCPropertyGridColorProperty* pColorItem = 
            new CMFCPropertyGridColorProperty(strItemName.c_str(), 
            (*(COLORREF*)(pVal)),
            NULL, strItemExplane.c_str());
	    pColorItem->EnableOtherButton(_T("Other..."));
	    pColorItem->EnableAutomaticButton(_T("Default"), RGB(128,128,128));
        pColorItem->SetData(iVal);
        pProp->AddSubItem(pColorItem);
    }
    else if (eType == E_VAL_ENUM)
    {
        int iSel = *(int*)(pVal);
         CDropDownExProp* pEnum = 
                new CDropDownExProp( 
                strItemName.c_str(),            // 項目名
                iSel,                           // 選択位置
                strItemExplane.c_str(),         // 項目説明
                iVal,                           // データ識別値
                0,                              // リソースID
                0);                             // リソース画像の分割数

        std::vector<StdString> lstStr;
        CUtil::TokenizeCsv(&lstStr, strOption);


        if (lstStr.size() > 0)
        {
            int id = SizeToInt(lstStr.size()) - 1;
            for(id; id >= 0; id--)
            {
                pEnum->AddOptionId(lstStr[id].c_str(), id);
            }
        }

        pEnum->SetCurSelById(iSel);
	    pProp->AddSubItem(pEnum);
    }
    else
    {
        CMFCPropertyGridProperty* pPropLayerMax = 
            new CMFCPropertyGridProperty(strItemName.c_str(),
            *pValiant, 
            strItemExplane.c_str());
        pPropLayerMax->SetData(iVal);
	    pProp->AddSubItem(pPropLayerMax);
    }

    PROPERTY_VAL vProp;
    vProp.eType = eType;
    vProp.pVal  = pVal;
    vProp.strOption = strOption;
    m_mapVal[iVal] = vProp;
    return true;
}


/**
 *  @brief   プロパティ変更
 *  @param   [in]  pProp        変更されたプロパティグリッド
 *  @retval  なし
 *  @note    フレームワークより呼び出し
 */
void CConfigPropertyGrid::OnPropertyChanged(CMFCPropertyGridProperty* pProp) const
{
    DWORD_PTR dwData;
    dwData = pProp->GetData();

    std::map<int, PROPERTY_VAL>::const_iterator ite;

    ite = m_mapVal.find(DwordptrToInt(dwData));

    if (ite == m_mapVal.end())
    {
        return;
    }

    PROPERTY_VAL val = ite->second;
    switch(val.eType)
    {
    case E_VAL_INT:
        {
        int* pInt;
        pInt = reinterpret_cast<int*>(val.pVal);
        *pInt =  (_variant_t)pProp->GetValue();
        }
        break;

    case E_VAL_DOUBLE:
        {
        double* pDbl;
        pDbl = reinterpret_cast<double*>(val.pVal);
        *pDbl =  (_variant_t)pProp->GetValue();
        }
        break;

    case E_VAL_BOOL:
        {
        bool* pBool;
        pBool = reinterpret_cast<bool*>(val.pVal);
        *pBool =  (_variant_t)pProp->GetValue();
        }
        break;

    case E_VAL_STRING:
        {
        CString* pStr;
        pStr = reinterpret_cast<CString*>(val.pVal);
        *pStr =  (_variant_t)pProp->GetValue();
        }
        break;

    case E_VAL_COLOR:
        {
        COLORREF cr;;
        
        COLORREF* pColor;
        pColor = reinterpret_cast<COLORREF*>(val.pVal);
        CMFCPropertyGridColorProperty* pColorProp;
        pColorProp = dynamic_cast<CMFCPropertyGridColorProperty*>(pProp);
        *pColor =  pColorProp->GetColor();
        cr = (*pColor);
        int a = 10;

        }
        break;

    case E_VAL_FONT:
        {
        LOGFONT* pFont;
        pFont = reinterpret_cast<LOGFONT*>(val.pVal);
        CMFCPropertyGridFontProperty* pFntProp;
        pFntProp = dynamic_cast<CMFCPropertyGridFontProperty*>(pProp);
        *pFont = *pFntProp->GetLogFont();
        }
        break;

    case E_VAL_ENUM:
    {
        int* pInt;
        pInt = reinterpret_cast<int*>(val.pVal);
        CDropDownExProp* pEnum;
        pEnum = dynamic_cast<CDropDownExProp*>(pProp);
        *pInt = pEnum->GetCurId();
    }
    break;



    case E_VAL_DIRECTORY:
    default:
        break;
    }
    return;
}

