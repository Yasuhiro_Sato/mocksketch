/**
 * @brief			CConfigヘッダーファイル
 * @file			CConfig.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef __CONFIG_H__
#define __CONFIG_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "MOCK_ERROR.h"
#include "Utility/CUtility.h"


/**
 * @class   CConfig
 * @brief                        
 */
class CConfig
{
public:

    CConfig(){;}
    ~CConfig(){;}

    virtual void SetDefault(){;}

    virtual void LoadXml(StdXmlArchiveIn* pXml);
    virtual void SaveXml(StdXmlArchiveOut* pXml);

    virtual bool Load(StdPath path);
    virtual bool Save(StdPath path); 

    //--------------
    // エラー文字
    //--------------
    virtual bool Load2(StdPath path);
    virtual bool Save2(StdPath path); 

protected:
private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
    }
};



//==================================
class CConfigPropertyGrid : public CMFCPropertyGridCtrl
{
public:
    enum E_VALIANT_TYPE
    {
        E_VAL_INT,
        E_VAL_DOUBLE,
        E_VAL_BOOL,
        E_VAL_COLOR,
        E_VAL_STRING,
        E_VAL_DIRECTORY,
        E_VAL_FONT,
        E_VAL_ENUM,
    };

    struct  PROPERTY_VAL
    {
        void* pVal;
        E_VALIANT_TYPE eType;
        StdString strOption;
    };

public:
    CConfigPropertyGrid();

    virtual ~CConfigPropertyGrid(){;}

    virtual void Setup() = 0;

protected:

    mutable std::map<int, PROPERTY_VAL> m_mapVal;

protected:
    virtual void OnPropertyChanged(CMFCPropertyGridProperty* pProp) const;

    virtual bool _SetPorperty(CMFCPropertyGridProperty* pProp,
                                    void* pVal,
                                    E_VALIANT_TYPE eType,
                                    StdString strItemName,
                                    StdString strItemExplane,
                                    int iVal,
                                    StdString strOption = _T(""));

};

#endif __CONFIG_H__
