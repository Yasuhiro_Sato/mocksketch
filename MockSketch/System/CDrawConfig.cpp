/**
 * @brief			CDrawConfig実装ファイル
 * @file			CDrawConfig.cpp
 * @author			Yasuhiro Sato
 * @date			01-2-2009 0:56:53
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MOckSketch.h"
#include "./CDrawConfig.h"

#include "./CSystem.h"
#include "View/ViewCommon.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/*---------------------------------------------------*/
/* Static variables                                  */
/*---------------------------------------------------*/

/**
 * コンストラクタ
 */

CDrawConfig::CDrawConfig():
pDispUnit(NULL),
pAngleUnit(NULL)
{
    m_pathFileName = _T("DrawConfig.xml");

}


/**
 * デストラクタ
 */

CDrawConfig::~CDrawConfig()
{

}

/**
 * @brief   ディフォルト値設定
 * @param   なし
 * @return	なし
 * @note	 
 */
void CDrawConfig::SetDefault()
{
    iLayerMax = 256;
    bDisableDifferentScl = false;

    crBack        = RGB( 255, 255, 255);
    crFront       = RGB( 0, 0, 0);
    crOver        = RGB( 255,  75,  75);
    crSelect      = RGB( 255,   0,   0);
    crImaginary   = RGB(   0, 200,   0);
    crAdditional  = RGB(   0, 200, 200);
    crEmphasis    = RGB( 200, 200,   0);
    crConnectable = RGB( 255,   0, 200);

    crPoint       = RGB( 140, 140,   0);
    crDisable     = RGB( 200, 200, 200);
    iPointSize    = 5;
    iPointType    = POINT_CROSS;
    dCloseDistance = 1.0e-6;
    iInputStoreCount = 20;
    bStretchMode     = false;
    dSnapSize        = 2;
    crText       = RGB( 0,   0,   0);
    iFontSize    = 10;
    strFont      = _T("ＭＳ Ｐ明朝");

    crCompild       = RGB(   0,   0, 255);
    crCompilError   = RGB( 255,   0,   0);
    crDrop          = RGB( 255,   0,   0);
    crChange        = RGB( 255,   0,   0);
    crSaved         = RGB(   0, 255,   0);
    crSelcetList    = RGB( 126, 126, 255);


    bAutoNamePoint       = false;
    bAutoNameCircle      = false;
    bAutoNameLine        = false;
    bAutoNameText        = true;
    bAutoNameSpline      = false;
    bAutoNameEllipse     = false;
    bAutoNameCompositionLine = true;
    bAutoNameNode            = true; 
    bAutoNameConnectionLine       = true;
    bAutoNameGroup           = true;
    bAutoNameParts           = true;
    bAutoNameReference       = true;
    bAutoNameImage           = true;
    bAutoNameDim             = false;
    bAutoNameField           = true;
    bAutoNameBoardIo         = true;
    bAutoNameNetIo           = true;
    bAutoNamePartsDef        = true;
    bAutoNameFieldDef        = true;


    iListCtrlFontSize    = 9;
    strListCtrlFont      = _T("ＭＳ Ｐ明朝");

    dDivPolygonAngle = 10.0;
    iDivPolygonSegment = 3;

    dLineMax     = 10000.0;
    bSelPart     = true;

    iAllowClickMoveCount = 5;

    HWND hWnd = THIS_APP->m_pMainWnd->GetSafeHwnd();
    HDC  hDc = ::GetDC(hWnd);

    if (hDc)
    {
        dDpi = ::GetDeviceCaps(hDc,LOGPIXELSX);
    }
    else
    {
        dDpi = 96.0;
    }
    iUndoMax     = 20;
    iDispUnit    = SYS_UNIT->GetUnit(_T("LEN_MM"))->GetNo();
    iDispAngle   = SYS_UNIT->GetUnit(_T("ANGL_DEG"))->GetNo();
    dwSnapType   = (VIEW_COMMON::SNP_ALL ^ (VIEW_COMMON::SNP_GRID | VIEW_COMMON::SNP_ON_LINE));
    dMarkerSize  = 1.5;
    iMaxMoveDisplay = 1000;


    eArrowType           = VIEW_COMMON::AT_OPEN;

    dArrowDotSize        = 1.5;
    dArrowSize           = 3.0;
    dArrowObliqueSize    = 3.0;
    dArrowDatumHeight    = 3.0;

    iDimSettingUnit       = SYS_UNIT->GetUnit(_T("LEN_MM"))->GetNo();
    dDimTextHeight        = 3.2;
    dDimTextSpace         = 0.5;
    iDimWidth             = 1;
    eDimLengthType        = VIEW_COMMON::DLT_DECIMAL;
    iDimLengthPrecision   = 2;
    eDimAngleType         = VIEW_COMMON::DIA_DECIMAL;
    iDimAnglePrecision    = 1;
    dDimGap               = 2.0;
    dDimTop               = 1.0;

    dRotateNodeOffset     = 3.0;

    dMinSnapLength        = 0.5;
    dConnectionStraghtLength = 3.0;

	dGridSize			  = 10.0; //グリッドの１区間のサイズ(表示単位系)
	dMinLine			  = 10.0;//画面上のグリッドの１区間の最小長さ (表示単位系) 
	crGrid              = RGB(200, 200, 200);       //グリッド色  
	eGridType           = POINT_RECT;    //グリッド形状
	iGridMarkerSize     = 1;    //グリッド形状
	bMultiple2          = true;   //区間サイズの 2倍表示を許容する
	bMultiple5          = true;   //区間サイズの 5倍表示を許容する


    strPriFixDiameter     = _T("Φ");
    strPriFixRadius       = _T("R");
    strPriFixChamfer      = _T("C");

    dLineWidthParDot         = 0.3;
    bUsePrintLineWidthOnDisp = true; //false;
    bUseSystemLineType       = false; //true;

    //編集時 分割表示制度(表示単位系)
    dDivPrecisionExec   = 0.2;
    dDivPrecisionDisp   = 0.2;
    dDivPrecisionPrint  = 0.1;


    iImageXMax = 5000;
    iImageYMax = 5000;

    //テキストボックス最小値(表示単位系)
    dTextBoxMinWidth = 40.0;
    dTextBoxMinHeight = 20.0;

    //テキストフレームマージンディフォルト地(寸法設定単位系)
    dTextTopMargin   = 1.0;
    dTextBottomMargin= 1.0;
    dTextLeftMargin  = 2.0;
    dTextRightMargin = 2.0;

    //-------------------------------------------------------------
    STD_ASSERT(fntListCtrl.CreatePointFont(iListCtrlFontSize * 10, strListCtrlFont.c_str()));
    pDispUnit = SYS_UNIT->GetUnit(iDispUnit);
    pAngleUnit = SYS_UNIT->GetUnit(iDispAngle);
    pDimUnit   = SYS_UNIT->GetUnit(iDimSettingUnit);
}

/**
 * @brief   単位保存
 * @param   なし
 * @return	なし
 * @note	 
 */
template<class Archive>
void CDrawConfig::SaveUnit(Archive& ar)
{
    //UnitNoではわかりにくいので文字列で保存する
    StdString strDispUint   = SYS_UNIT->GetUnit(iDispUnit)->GetId();
    StdString strDispAngle  = SYS_UNIT->GetUnit(iDispAngle)->GetId();
    StdString strDimSettingUnit  = SYS_UNIT->GetUnit(iDimSettingUnit)->GetId();

    ar & boost::serialization::make_nvp("DispUnit"   , strDispUint);
    ar & boost::serialization::make_nvp("DispAngle"  , strDispAngle);
    ar & boost::serialization::make_nvp("DimSettingUnit"  , strDimSettingUnit);

}


/**
 * @brief   単位取得
 * @param   なし
 * @return	なし
 * @note	 
 */
template<class Archive>
void CDrawConfig::LoadUnit(Archive& ar)
{
    //UnitNoではわかりにくいので文字列で保存する
    //TODO:エラーが発生しやすくなるので対策が必要
    StdString strDispUint ;
    StdString strDispAngle; 
    StdString strDimSettingUnit; 

    ar & boost::serialization::make_nvp("DispUnit"   , strDispUint);
    ar & boost::serialization::make_nvp("DispAngle"  , strDispAngle);
    ar & boost::serialization::make_nvp("DimSettingUnit"  , strDimSettingUnit);

    iDispUnit   = SYS_UNIT->GetUnit(strDispUint)->GetNo();
    iDispAngle  = SYS_UNIT->GetUnit(strDispAngle)->GetNo();
    iDimSettingUnit  = SYS_UNIT->GetUnit(strDimSettingUnit)->GetNo();

    pDispUnit = SYS_UNIT->GetUnit(iDispUnit);
    pAngleUnit = SYS_UNIT->GetUnit(iDispAngle);
    pDimUnit   = SYS_UNIT->GetUnit(iDimSettingUnit);
}

/**
 * @brief   XML読み込み
 * @param   [in] pXml 
 * @return	なし
 * @note	 
 */
void CDrawConfig::LoadXml(StdXmlArchiveIn* pXml)
{
    *pXml >> boost::serialization::make_nvp("Root", *this);
}

/**
 * @brief   XML書き込み
 * @param   [in] pXml
 * @return	なし
 * @note	 
 */
void CDrawConfig::SaveXml(StdXmlArchiveOut* pXml)
{
    *pXml << boost::serialization::make_nvp("Root", *this);
}

/**
 * @brief   ファイル読み込み
 * @param   なし
 * @return	なし
 * @note	 
 */
void CDrawConfig::Load()
{
    namespace fs = boost::filesystem;

    StdPath  pathLoad = CSystem::GetSystemPath();
    pathLoad /= m_pathFileName;
    CConfig::Load(pathLoad);
}


/**
 * @brief   ファイル書き込み
 * @param   なし          
 * @retval  なし
 * @note    普通は使わない
 */
void CDrawConfig::Save()
{
    StdPath  pathSave =     CSystem::GetSystemPath();
    pathSave /= m_pathFileName;
    CConfig::Save(pathSave);
}


CDrawConfig& CDrawConfig::operator = (const CDrawConfig & obj)
{
    iLayerMax       = obj.iLayerMax;
    crBack          = obj.crBack;
    crText          = obj.crText;

    crSelect        = obj.crSelect;
    crImaginary     = obj.crImaginary;
    crAdditional    = obj.crAdditional;

    bAutoNamePoint       = obj.bAutoNamePoint  ;
    bAutoNameCircle      = obj.bAutoNameCircle ;
    bAutoNameLine        = obj.bAutoNameLine   ;
    bAutoNameText        = obj.bAutoNameText   ;
    bAutoNameSpline      = obj.bAutoNameSpline ;
    bAutoNameEllipse     = obj.bAutoNameEllipse ;
    bAutoNameCompositionLine = obj.bAutoNameCompositionLine;
    bAutoNameNode            = obj.bAutoNameNode           ; 
    bAutoNameConnectionLine       = obj.bAutoNameConnectionLine      ;
    bAutoNameGroup           = obj.bAutoNameGroup          ;
    bAutoNameParts           = obj.bAutoNameParts          ;
    bAutoNameReference       = obj.bAutoNameReference      ;

    bAutoNameImage           = obj.bAutoNameImage          ;
    bAutoNameDim             = obj.bAutoNameDim            ;
    bAutoNameField           = obj.bAutoNameField          ;
    bAutoNameBoardIo         = obj.bAutoNameBoardIo        ;
    bAutoNameNetIo           = obj.bAutoNameNetIo          ;
    bAutoNamePartsDef        = obj.bAutoNamePartsDef       ;
    bAutoNameFieldDef        = obj.bAutoNameFieldDef       ;


    crPoint         = obj.crPoint;

    strFont             = obj.strFont;
    iFontSize           = obj.iFontSize;
    strListCtrlFont     = obj.strListCtrlFont;
    iListCtrlFontSize   = obj.iListCtrlFontSize;
    iPointSize          = obj.iPointSize;
    iPointType          = obj.iPointType;
    dCloseDistance      = obj.dCloseDistance;

    iInputStoreCount    = obj.iInputStoreCount;
    bStretchMode        = obj.bStretchMode;
    dSnapSize           = obj.dSnapSize;
    dLineMax            = obj.dLineMax;
    bSelPart            = obj.bSelPart;
    iUndoMax            = obj.iUndoMax;

    iDispUnit           = obj.iDispUnit;
    iDispAngle          = obj.iDispAngle;

    dwSnapType          = obj.dwSnapType;
    dMarkerSize         = obj.dMarkerSize;

    iImageXMax          = obj.iImageXMax;
    iImageYMax          = obj.iImageYMax;

    dTextBoxMinWidth    = obj.dTextBoxMinWidth;
    dTextBoxMinHeight   = obj.dTextBoxMinHeight;

    return *this;
}


//!< フォントデータ更新
void CDrawConfig::UpdateFont(bool bToLogfont)
{
    if (bToLogfont)
    {
        CFont tmpFnt1;
        tmpFnt1.CreatePointFont(iFontSize, strFont.c_str());
        tmpFnt1.GetLogFont(&lfDefault);

        CFont tmpFnt2;
        tmpFnt2.CreatePointFont(iListCtrlFontSize, strListCtrlFont.c_str());
        tmpFnt2.GetLogFont(&lfListCtrl);
    }
    else
    {
        strFont   = lfDefault.lfFaceName;
        iFontSize = lfDefault.lfHeight;

        strListCtrlFont   = lfListCtrl.lfFaceName;
        iListCtrlFontSize = lfListCtrl.lfHeight;
    }
}


double  CDrawConfig::DimToDispUnit(double dLen)
{
   return DRAW_CONFIG->pDispUnit->ToThisUnit(
             DRAW_CONFIG->pDimUnit->ToBase(dLen));

}


bool CDrawConfig::SetDispUnitNo(int iNo)
{
    CUnit* pUnit;
    pUnit = SYS_UNIT->GetUnit(iNo);
    if (!pUnit)
    {
        return false;
    }

    int iKindNo = SYS_UNIT->GetKindIdByName( _T("Length"));

    if (iKindNo != pUnit->GetKind() )
    {
        return false;
    }
    iDispUnit = iNo;
    pDispUnit = pUnit;
    return true;
}

bool CDrawConfig::SetAngleUnitNo(int iNo)
{
    CUnit* pUnit;
    pUnit = SYS_UNIT->GetUnit(iNo);
    if (!pUnit)
    {
        return false;
    }

    int iKindNo = SYS_UNIT->GetKindIdByName( _T("Angle"));

    if (iKindNo != pUnit->GetKind() )
    {
        return false;
    }
    iDispAngle = iNo;
    pAngleUnit = pUnit;
    return true;


}

bool CDrawConfig::SetDimUnitNo(int iNo)
{
    CUnit* pUnit;
    pUnit = SYS_UNIT->GetUnit(iNo);
    if (!pUnit)
    {
        return false;
    }

    int iKindNo = SYS_UNIT->GetKindIdByName( _T("Length"));

    if (iKindNo != pUnit->GetKind() )
    {
        return false;
    }
    iDimSettingUnit = iNo;
    pDimUnit = pUnit;
    return true;

}

double CDrawConfig::DimSettingToDispUnit(double dVal)
{
    double dRet = pDimUnit->ToBase(dVal);
    dRet = pDispUnit->ToThisUnit(dRet);
    return dRet;
}

int CDrawConfig::DimSettingToDot(double dVal)
{
    double dInch = pDimUnit->ToBase(dVal) * 1000 / 25.4;
    double dRet  = dInch * dDpi;
    return CUtil::Round(dRet);
}

double CDrawConfig::ToMm(double dVal)
{
    return pDispUnit->ToBase(dVal) / 1000;
}

double CDrawConfig::ToInch(double dVal)
{
    return pDispUnit->ToBase(dVal) * 1000 / 25.4;
}

bool CDrawConfig::IsAutoName(DRAWING_TYPE eType)
{
    switch(eType)
    {
        case DT_POINT:  {return bAutoNamePoint;}
        case DT_LINE:   {return bAutoNameLine;}
        case DT_CIRCLE: {return bAutoNameCircle;}
        case DT_TEXT:   {return bAutoNameText;}
        case DT_SPLINE: {return bAutoNameSpline;}
        case DT_ELLIPSE:{return bAutoNameEllipse;}
        case DT_COMPOSITION_LINE:   {return bAutoNameCompositionLine;}
        case DT_NODE:               {return bAutoNameNode;}
        case DT_CONNECTION_LINE:          {return bAutoNameConnectionLine;}
        case DT_GROUP:              {return bAutoNameGroup;}
        case DT_REFERENCE:          {return bAutoNameReference;}
        case DT_IMAGE:              {return bAutoNameImage;}

        case DT_DIM_L:  
        case DT_DIM_H:  
        case DT_DIM_V:  
        case DT_DIM_A:  
        case DT_DIM_D:  
        case DT_DIM_R:  
        case DT_DIM_C:  {return bAutoNameDim;}

        case DT_PARTS:  {return bAutoNameParts;}
        case DT_FIELD:  {return bAutoNameField;}

        case DT_BOARD_IO:   {return bAutoNameBoardIo;}
        case DT_NET_IO:     {return bAutoNameNetIo;}
        case DT_PARTS_DEF:  {return bAutoNamePartsDef;}
        case DT_FIELD_DEF:  {return bAutoNameFieldDef;}
        default:
            DBG_ASSERT(eType);
            return false;
    }
}


//======================================
//======================================
//======================================
/**
 *  @brief   コンストラクター
 *  @param   なし
 *  @retval  なし
 *  @note
 */
CDrawPropertyGrid::CDrawPropertyGrid()
{

}

/**
 *  @brief   初期設定
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CDrawPropertyGrid::Setup()
{

    //TODO:リソース化
    //TODO:未設定項目追加

    EnableHeaderCtrl();
    EnableDescriptionArea();
    SetVSDotNetLook(true/*m_bDotNetLook*/);
    MarkModifiedProperties(true/*m_bMarkChanged*/);
    SetAlphabeticMode(false/*!m_bPropListCategorized*/);
    SetShowDragContext(true/*m_bShowDragContext*/);

    std::auto_ptr<CMFCPropertyGridProperty> 
        apDrawConfig(new CMFCPropertyGridProperty(_T("描画設定")));


    int iVal = 0;

    //=============
    //色設定
    CMFCPropertyGridProperty* pGroupColor = 
        new CMFCPropertyGridProperty(_T("色"));
	apDrawConfig->AddSubItem(pGroupColor);

        _SetPorperty(pGroupColor, &m_Cfg.crBack, E_VAL_COLOR,
                                 _T("背景色"),
                                 _T("画面の背景色を設定します"),
                                 iVal++);


        _SetPorperty(pGroupColor, &m_Cfg.crDisable, E_VAL_COLOR,
                                 _T("無効色"),
                                 _T("画面の背景色を設定します"),
                                 iVal++);

        _SetPorperty(pGroupColor, &m_Cfg.crPoint, E_VAL_COLOR,
                                 _T("点の色"),
                                 _T("画面の背景色を設定します"),
                                 iVal++);
    
    //=============
    //フォント設定
    CMFCPropertyGridProperty* pGroupFont = 
        new CMFCPropertyGridProperty(_T("フォント"));
	apDrawConfig->AddSubItem(pGroupFont);



        _SetPorperty(pGroupFont, &m_Cfg.lfDefault, E_VAL_FONT,
                                 _T("ディフォルト"),
                                 _T("ディフォルトで使用するフォントを設定します"),
                                 iVal++);

        _SetPorperty(pGroupFont, &m_Cfg.lfListCtrl, E_VAL_FONT,
                                 _T("リストコントロール"),
                                 _T("リストコントロールで使用するフォントを設定します"),
                                 iVal++);
    //=============
    //描画
    CMFCPropertyGridProperty* pGroupDrawing = 
        new CMFCPropertyGridProperty(_T("描画"));
	apDrawConfig->AddSubItem(pGroupDrawing);


    //=============
    //スナップ
    CMFCPropertyGridProperty* pGroupSnap = 
        new CMFCPropertyGridProperty(_T("スナップ"));
	apDrawConfig->AddSubItem(pGroupSnap);

       _SetPorperty(pGroupSnap, &m_Cfg.dSnapSize, E_VAL_DOUBLE,
                                 _T("スナップサイズ"),
                                 _T("カーソル中心から図形を探す範囲(mm)"),
                                 iVal++);

        _SetPorperty(pGroupSnap, &m_Cfg.dMarkerSize, E_VAL_DOUBLE,
                                 _T("マーカーサイズ"),
                                 _T("非ビットマップマーカの大きさ"),
                                 iVal++);
     

    //=============
    //単位
    CMFCPropertyGridProperty* pGroupUnit = 
        new CMFCPropertyGridProperty(_T("単位"));
	apDrawConfig->AddSubItem(pGroupUnit);




    //=============
    //機能
    CMFCPropertyGridProperty* pGroupFunc = 
        new CMFCPropertyGridProperty(_T("機能"));
	apDrawConfig->AddSubItem(pGroupFunc);

    //レイヤー最大数
    _SetPorperty(pGroupFunc, &m_Cfg.iLayerMax, E_VAL_INT,
                             _T("レイヤー最大数"),
                             _T("レイヤー最大数の説明"),
                             iVal++);

	AddProperty(apDrawConfig.release());

}

/**
 *  @brief   データ設定
 *  @param   [in] setting 設定値
 *  @retval  なし
 *  @note
 */
void CDrawPropertyGrid::SetData(const CDrawConfig& setting)
{
    m_Cfg = setting;
    m_Cfg.UpdateFont(true);
}

/**
 *  @brief   データ取得
 *  @param   [out]  pSetting 取得データ
 *  @retval  なし
 *  @note
 */
void CDrawPropertyGrid::GetData(CDrawConfig* pSetting)
{
    m_Cfg.UpdateFont(false);
    *pSetting = m_Cfg;
}
