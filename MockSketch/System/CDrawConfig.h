/**
 * @brief			CDrawConfigヘッダーファイル
 * @file			CDrawConfig.h
 * @author			Yasuhiro Sato
 * @date			01-2-2009 0:56:52
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#if !defined(EA_522D248A_2008_4cc6_88AF_42733F5B30B5__INCLUDED_)
#define EA_522D248A_2008_4cc6_88AF_42733F5B30B5__INCLUDED_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CConfig.h"
#include "CUnit.h"
#include "View/ViewCommon.h"
#include "View/DRAWING_TYPE.h"


/*---------------------------------------------------*/
/*  macros                                           */
/*---------------------------------------------------*/


/*---------------------------------------------------*/
/*  Classess                                         */
/*---------------------------------------------------*/
class CUnit;

// 点の種別
enum POINT_TYPE
{
	POINT_DOT,
	POINT_RECT,
	POINT_RECT_FILL,
	POINT_PLUS,
    POINT_CROSS,
    POINT_ROUND,
	POINT_ROUND_FILL
};


/**
 * @class   CDrawConfig
 * @brief   描画設定                     
 */
class CDrawConfig : public CConfig
{
public:


    //=============
    // 色
    //=============

    //!< 背景色
    COLORREF crBack;

    //!< 標準の線色
    COLORREF crFront;

    //!< 無効色
    COLORREF crDisable;

    //!< 点の色
    COLORREF crPoint;

    //!< ディフォルトの文字の色
    COLORREF crText;

    //----------------------
    // TreeCtrl
    //----------------------
    //!< コンパイル済み文字の色(オブジェクトウインドウ)
    COLORREF crCompild;

    //!< コンパイルエラー時の文字色(オブジェクトウインドウ)
    COLORREF crCompilError;

    //!< ドロップ可能ハイライト(オブジェクトウインドウ)
    COLORREF crDrop;

    //!< 変更時(オブジェクトウインドウ)
    COLORREF crChange;

    //!< 保存完了(オブジェクトウインドウ)
    COLORREF crSaved;

    //!< 選択色(オブジェクトウインドウ)
    COLORREF crSelcetList;
    //----------------------

    //!< 選択色
    COLORREF crSelect;

    //!< 仮想物体色 (クリックした時に生成、若しくは選択されるオブジェクトの色)

    COLORREF crImaginary;

    //!< 補助線の色
    COLORREF crAdditional;

    //!< 強調色 (MouseOverで使用 仮想物体色等と関連があることを示すのに使用する)
    COLORREF crEmphasis;

    //!< 接続可能色 (MouseOverで使用 NODEが接続可能であることを示すのに使用する)
    COLORREF crConnectable;

    //!< 通過色
    COLORREF crOver;

    //=============
    // 自動名称設定
    //=============
    bool bAutoNamePoint;
    bool bAutoNameCircle;
    bool bAutoNameLine;
    bool bAutoNameText;
    bool bAutoNameSpline;
    bool bAutoNameEllipse;

    bool bAutoNameCompositionLine;
    bool bAutoNameNode;
    bool bAutoNameConnectionLine;
    bool bAutoNameGroup;
    bool bAutoNameReference;
    bool bAutoNameImage;
    bool bAutoNameDim;
    bool bAutoNameParts;
    bool bAutoNameField;
    bool bAutoNameBoardIo;
    bool bAutoNameNetIo;
    bool bAutoNamePartsDef;
    bool bAutoNameFieldDef;


    //=============
    // フォント
    //=============
    //!< ディフォルトのフォント
    StdString strFont;

    //!< ディフォルトのフォントサイズ
    int iFontSize;

    //!< リストコントロールのフォント
    StdString strListCtrlFont;

    //!< リストコントロールのフォントサイズ
    int iListCtrlFontSize;


    //=============
    // 描画
    //=============
    //!< 点のサイズ
    int  iPointSize;

    //!< 点のタイプ
    int iPointType;


    //!< CompositionLineで始点と終点が閉じていると判定する距離  (表示単位系)
    double   dCloseDistance;

    //編集時 分割表示制度(表示単位系)
    double dDivPrecisionExec;
    double dDivPrecisionDisp;
    double dDivPrecisionPrint;


    //===============
    // ポリゴン
	//===============
	//!< ポリゴン生成時の円弧分割角度
    double dDivPolygonAngle;

    //!< ポリゴン生成時のスプライン分割数
    int iDivPolygonSegment;


    //===============
    // スナップ
    //!< カーソル中心から図形を探す範囲
    double dSnapSize;

    //!< スナップ対象
    DWORD dwSnapType;

    //!< マーカーサイズ(非ビットマップ)(表示単位系)
    double  dMarkerSize;

    //===============
    //マウス
    //!<クリックとドラッグを判別するDot数
    int iAllowClickMoveCount;

    //===============
    //単位
    //!< ディスプレイのDpi
    double dDpi;

	//===============
	//グリッド
	double   dGridSize;      //グリッドの１区間のサイズ(表示単位系)
	double   dMinLine;       //画面上のグリッドの１区間の最小長さ (表示単位系) 
	COLORREF crGrid;       //グリッド色  
	POINT_TYPE  eGridType;    //グリッドマーカー形状
	int   iGridMarkerSize;     //グリッドマーカーサイズ
	bool     bMultiple2;   //区間サイズの 2倍表示を許容する
	bool     bMultiple5;   //区間サイズの 5倍表示を許容する

    //===============
    // 寸法

    //!< 表示角度単位系番号
    int iDimSettingUnit;

    //矢印種別
    VIEW_COMMON::E_ARROW_TYPE eArrowType;

    //黒丸直径(寸法設定単位系)
    double dArrowDotSize;

    //矢印長さ(寸法設定単位系)
    double dArrowSize;

    //斜線長さ(寸法設定単位系)
    double dArrowObliqueSize;

    //データム高さ(寸法設定単位系)
    double dArrowDatumHeight;

    //文字高さ(寸法設定単位系)
    double dDimTextHeight;
    
    //寸法線と文字の間隔(寸法設定単位系)
    double dDimTextSpace;

    //寸法線太さ
    int iDimWidth;

    //寸法長さ表記種別
    VIEW_COMMON::E_DIM_LENGTH_TYPE eDimLengthType;

    // 長さ表記の小数点以下の桁数
    int iDimLengthPrecision;

    VIEW_COMMON::E_DIM_ANGLE_TYPE eDimAngleType;

    // 角度表記の小数点以下の桁数
    int iDimAnglePrecision;

    //ギャップ量(寸法設定単位系)
    double dDimGap;

    //飛び出し量((寸法設定単位系)
    double dDimTop;

    //直径の接頭語
    StdString strPriFixDiameter;

    //半径の接頭語
    StdString strPriFixRadius;

    //チャンファーの接頭語
    StdString strPriFixChamfer;

    //===============
    //!< 回転用ノードのオフセット量(寸法設定単位系)
    double  dRotateNodeOffset;


    //-----------------------------
    //画面上の最小スナップ長((寸法設定単位系)
    double dMinSnapLength;
    
    //接続線の 境界線近傍の直線の長さ((寸法設定単位系)
    double dConnectionStraghtLength;


    //===============
    //
    //!< レイヤー最大数
    int iLayerMax;

    //!< カレントスケールと異なる倍率のレイヤーを無効色で表示する
    bool bDisableDifferentScl;

    //!< 水平、垂直、角度線の長さ
    double dLineMax;

    //!< 図形選択時に一部でも領域に含まれていれば、選択する
    bool bSelPart;

    //!< アンドゥバッファ最大値
    int iUndoMax;

    //!< 入力値記憶数
    int iInputStoreCount;
        
    //!< 移動時に線の選択範囲外の点を固定する
    bool bStretchMode;

     //!< 移動時にオブジェクトを表示する最大数
    int  iMaxMoveDisplay;

    //!< 表示単位系番号
    int iDispUnit;

    //!< 表示角度単位系番号
    int iDispAngle;

    //--------------
    // 印刷関連設定
    //--------------
    //!< 印刷時の1ドットあたりの線の太さ(表示単位系)
    double dLineWidthParDot;

    //!< 印刷時の線の太さを画面で使用する
    bool   bUsePrintLineWidthOnDisp;

    //!< 線幅1dot時の点線等の描画速度向上
    bool bUseSystemLineType;

    //===============
    // 画像
    //!< 画像X方向最大値(画素数)
    int iImageXMax;

    //!< 画像Y方向最大値(画素数)
    int iImageYMax;

    //===============
    // テキストボックス
    //!< テキストボックスX方向最小値(表示単位系)
    double dTextBoxMinWidth;

    //!< テキストボックスY方向最小値(表示単位系)
    double dTextBoxMinHeight;

    //!< ディフォルトのテキストマージン(寸法設定単位系)
    double dTextTopMargin;
    double dTextBottomMargin;
    double dTextLeftMargin;
    double dTextRightMargin;


public:
    //リストコントロールフォント
    CFont   fntListCtrl;


    //!< フォントデータ更新
    void UpdateFont(bool bToLogfont);

    double  DimToDispUnit(double dLen);

public:
    //非保存データ
    LOGFONT lfDefault;

    LOGFONT lfListCtrl;

    CUnit*  pDispUnit;

    CUnit*  pAngleUnit;

    CUnit*  pDimUnit;


private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT

            SERIALIZATION_BOTH("MaxLayer"              , iLayerMax);
            SERIALIZATION_BOTH("DisableDifferentScl"   , bDisableDifferentScl);
        
            SERIALIZATION_BOTH("BackColor"     , crBack);
            SERIALIZATION_BOTH("NormalColor"   , crFront);
            SERIALIZATION_BOTH("TextColor"     , crText);

            SERIALIZATION_BOTH("CompiledColor"    , crCompild);
            SERIALIZATION_BOTH("DropHilightColor" , crDrop);
            SERIALIZATION_BOTH("FileChangeColor"  , crChange);
            SERIALIZATION_BOTH("FileSavedColor"   , crSaved);
            SERIALIZATION_BOTH("ListSelectedColor", crSelcetList);

            SERIALIZATION_BOTH("SelectColor"    , crSelect);
            SERIALIZATION_BOTH("ImaginaryColor" , crImaginary);
            SERIALIZATION_BOTH("AdditionalColor", crAdditional);
            SERIALIZATION_BOTH("EmphasisColor"  , crEmphasis);
            SERIALIZATION_BOTH("ConnectableColor", crConnectable);
            
            SERIALIZATION_BOTH("AutoNamePoint"                 ,bAutoNamePoint);
            SERIALIZATION_BOTH("AutoNameCircle"                ,bAutoNameCircle);
            SERIALIZATION_BOTH("AutoNameLine"                  ,bAutoNameLine);
            SERIALIZATION_BOTH("AutoNameText"                  ,bAutoNameText);
            SERIALIZATION_BOTH("AutoNameSpline"                ,bAutoNameSpline);
            SERIALIZATION_BOTH("AutoNameEllipse"               ,bAutoNameEllipse);
            SERIALIZATION_BOTH("AutoNameCompositionLine"       ,bAutoNameCompositionLine);
            SERIALIZATION_BOTH("AutoNameNode"                  ,bAutoNameNode);
            SERIALIZATION_BOTH("AutoNameConnectionLine"             ,bAutoNameConnectionLine);
            SERIALIZATION_BOTH("AutoNameGroup"                 ,bAutoNameGroup);
            SERIALIZATION_BOTH("AutoNameParts"                 ,bAutoNameParts);
            SERIALIZATION_BOTH("AutoNameReference"             ,bAutoNameReference);
            SERIALIZATION_BOTH("AutoNameImage"                 ,bAutoNameImage);
            SERIALIZATION_BOTH("AutoNameDim"                   ,bAutoNameDim);
            SERIALIZATION_BOTH("AutoNameField"                 ,bAutoNameField);
            SERIALIZATION_BOTH("AutoNameBoardIo"               ,bAutoNameBoardIo);
            SERIALIZATION_BOTH("AutoNameNetIo"                 ,bAutoNameNetIo);
            SERIALIZATION_BOTH("AutoNamePartsDef"              ,bAutoNamePartsDef);
            SERIALIZATION_BOTH("AutoNameFieldDef"              ,bAutoNameFieldDef);


            SERIALIZATION_BOTH("PointColor"    , crPoint);
            
            SERIALIZATION_BOTH("Font"           , strFont);
            SERIALIZATION_BOTH("FontSize"       , iFontSize);
            SERIALIZATION_BOTH("ListCtrlFont"   , strListCtrlFont);
            SERIALIZATION_BOTH("ListCtrlFontSize" , iListCtrlFontSize);
            SERIALIZATION_BOTH("PointSize"     , iPointSize);
            SERIALIZATION_BOTH("PointType"     , iPointType);
            SERIALIZATION_BOTH("CloseDistance" , dCloseDistance);


            SERIALIZATION_BOTH("InputStoreNum" , iInputStoreCount);
            SERIALIZATION_BOTH("StretchMode"   , bStretchMode);
            
            SERIALIZATION_BOTH("SnapSize"      , dSnapSize);
            SERIALIZATION_BOTH("LineMax"       , dLineMax);
            SERIALIZATION_BOTH("SelPart"       , bSelPart);
            SERIALIZATION_BOTH("UndoBufferSize", iUndoMax);

            
            SERIALIZATION_BOTH("AllowClickMoveCount"  , iAllowClickMoveCount);

            SERIALIZATION_BOTH("DPI"            , dDpi);
            SERIALIZATION_BOTH("DispUnitNo"     , iDispUnit);
            SERIALIZATION_BOTH("DispAngleUnitNo", iDispAngle);
            
			SERIALIZATION_BOTH("GridSize"		, dGridSize);   
			SERIALIZATION_BOTH("GridMinSize"	, dMinLine);    
			SERIALIZATION_BOTH("GridColor"		, crGrid);      
			SERIALIZATION_BOTH("GridMarkerType" , eGridType);   
			SERIALIZATION_BOTH("GridMarkerSize" , iGridMarkerSize);
			SERIALIZATION_BOTH("GridMultiple2"  , bMultiple2);  
			SERIALIZATION_BOTH("GridMultiple5"  , bMultiple5);  


            SERIALIZATION_BOTH("ArrowType"          , eArrowType);

            SERIALIZATION_BOTH("ArrowDotSize"       , dArrowDotSize);
            SERIALIZATION_BOTH("ArrowSize"          , dArrowSize);
            SERIALIZATION_BOTH("ArrowObliqueSize"   , dArrowObliqueSize);
            SERIALIZATION_BOTH("ArrowDatumHeight"   , dArrowDatumHeight);

            SERIALIZATION_BOTH("DimTextHeight"      , dDimTextHeight);
            SERIALIZATION_BOTH("DimTextSpace"       , dDimTextSpace);
            SERIALIZATION_BOTH("DimLineWidth"       , iDimWidth);
            SERIALIZATION_BOTH("DimLengthType"      , eDimLengthType);
            SERIALIZATION_BOTH("DimLengthPrecision" , iDimLengthPrecision);
            SERIALIZATION_BOTH("DimAngleType"       , eDimAngleType);
            SERIALIZATION_BOTH("DimAnglePrecision"  , iDimAnglePrecision);
            SERIALIZATION_BOTH("DimGap"             , dDimGap);
            SERIALIZATION_BOTH("DimTop"             , dDimTop);


            SERIALIZATION_BOTH("PriFixDiameter"     , strPriFixDiameter);
            SERIALIZATION_BOTH("PriFixRadius"       , strPriFixRadius);
            SERIALIZATION_BOTH("PriFixChamfer"      , strPriFixChamfer);

            SERIALIZATION_BOTH("RotateNodeOffset"   , dRotateNodeOffset);
            
            SERIALIZATION_BOTH("MinSnapLength"      , dMinSnapLength);
            

            SERIALIZATION_BOTH("LineWidthParDot"         , dLineWidthParDot);
            SERIALIZATION_BOTH("UsePrintLineWidthOnDisp" , bUsePrintLineWidthOnDisp);
            SERIALIZATION_BOTH("UseSystemLineType"       , bUseSystemLineType);



            SERIALIZATION_BOTH("SnapType", dwSnapType);
            SERIALIZATION_BOTH("MarkerSize"    , dMarkerSize);
            SERIALIZATION_BOTH("MaxMoveDisplay"    , iMaxMoveDisplay);


            SERIALIZATION_BOTH("DivPrecisionExec" ,  dDivPrecisionExec);
            SERIALIZATION_BOTH("DivPrecisionDisp" ,  dDivPrecisionDisp);
            SERIALIZATION_BOTH("DivPrecisionPrint", dDivPrecisionPrint);

            SERIALIZATION_BOTH("ImageSizeXMax" , iImageXMax);
            SERIALIZATION_BOTH("ImageSizeYMax" , iImageYMax);

            SERIALIZATION_BOTH("TextBoxMinWidth" , dTextBoxMinWidth);
            SERIALIZATION_BOTH("TextBoxMinHeight", dTextBoxMinHeight);

            SERIALIZATION_BOTH("TextTopMargin"   , dTextTopMargin);
            SERIALIZATION_BOTH("TextBottomMargin", dTextBottomMargin);
            SERIALIZATION_BOTH("TextLeftMargin"  , dTextLeftMargin);
            SERIALIZATION_BOTH("TextRightMargin" , dTextRightMargin);

            if (Archive::is_loading::value)
            {
                fntListCtrl.CreatePointFont(iListCtrlFontSize, strListCtrlFont.c_str());
                LoadUnit(ar);
            }
            else if (Archive::is_saving::value)
            {
                SaveUnit(ar);
            }
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }

public:
	//!< コンストラクタ
	CDrawConfig();

    //!< デストラクタ
	virtual ~CDrawConfig();

    CDrawConfig& operator = (const CDrawConfig & obj);

    //!< レイヤー最大数
    int GetLayerMax()           {return iLayerMax;}

    //!< 点の色取得
    COLORREF GetPointColor()     {return crPoint;}

    //!< 背景色取得
    COLORREF GetBackColor()     {return crBack;}

    //!< 選択色取得
    COLORREF GetSelColor()     {return crSelect;}

    //!< 点サイズ取得
    int GetPointSize()          {return iPointSize;}

    //!< 点タイプ取得
    POINT_TYPE GetPointType()   {return static_cast<POINT_TYPE>(iPointType);}

    //!< 入力値記憶数
    int GetInputStoreCount()    {return iInputStoreCount;}

    //!< ストレッチモード
    bool GetStretchMode()       {return bStretchMode;}
    void SetStretchMode(bool bStretch)       {bStretchMode = bStretch;}

    //!< カーソル中心から図形を探す範囲(mm)
    double GetSnapSize()           {return dSnapSize;}

    //!< ディフォルトの文字の色
    COLORREF GetDefautTextColor()           {return crText;}
    
    //!< ディフォルトのフォント
    const StdChar* GetDefaultFont()             {return strFont.c_str();}

    //!< ディフォルトのフォントサイズ(point)
    int GetFontSize()       {return iFontSize;}

    //!< 水平、垂直、角度線の長さ
    double GetLineMax()       {return dLineMax;}

    //!< 図形選択時に一部でも領域に含まれていれば、選択する
    bool IsSelPart()       {return bSelPart;}

    //!< アンドゥバッファサイズ
    int GetUndoBufferMax()       {return iUndoMax;}

    //!< スナップ種別取得
    DWORD  GetSnapType()       {return dwSnapType;}
    void   SetSnapType(DWORD dw) { dwSnapType = dw; }

    //!< マーカーサイズ取得
    double  GetMarkerSize()       {return dMarkerSize;}

    int  GetMaxMoveDisplay()       {return iMaxMoveDisplay;}


    int  GetDispUnitNo() const {return iDispUnit;}
    bool SetDispUnitNo(int iNo);

    int  GetAngleUnitNo() const{return iDispAngle;}
    bool SetAngleUnitNo(int iNo);

    int  GetDimUnitNo() const{return iDimSettingUnit;}
    bool SetDimUnitNo(int iNo);

    double ToMm(double dVal);  
    double ToInch(double dVal);  

    double DimSettingToDispUnit(double dVal);  
    int    DimSettingToDot(double dVal);  

    bool IsAutoName(DRAWING_TYPE eType);

public:

    template<class Archive>
    void SaveUnit(Archive& ar);

    template<class Archive>
    void LoadUnit(Archive& ar);

    virtual void LoadXml(StdXmlArchiveIn* pXml);

    virtual void SaveXml(StdXmlArchiveOut* pXml);

    virtual void Save();

    virtual void Load();

    //!< 初期値設定
    void SetDefault();

private:
    //
    StdPath m_pathFileName;

};


//==================================
//==================================
//==================================

/**
 * @class   CDrawPropertyGrid
 * @brief   描画設定 プロパティグリッド               
 */
class CDrawPropertyGrid : public CConfigPropertyGrid
{
public:
    //!<コンストラクタ
    CDrawPropertyGrid();

    //!<デストラクタ
    virtual ~CDrawPropertyGrid(){;}

    //!<初期設定
    virtual void Setup();

    //!< データ設定
    void SetData(const CDrawConfig& setting);
    
    //!< データ取得
    void GetData(CDrawConfig* pSetting);

protected:
    mutable CDrawConfig  m_Cfg;
};

#endif // !defined(EA_522D248A_2008_4cc6_88AF_42733F5B30B5__INCLUDED_)
