/**
 * @brief			CEditorConfig実装ファイル
 * @file			CEditorConfig.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "CEditorConfig.h"
#include "CSystem.h"


/**
 * @brief   コンストラクタ
 * @param   なし
 * @return	なし
 * @note	 
 */
CEditorConfig::CEditorConfig()
{
    m_pathFileName = _T("EditorConfig.xml");
}

/**
 * @brief   デストラクタ
 * @param   なし
 * @return	なし
 * @note	 
 */
CEditorConfig::~CEditorConfig()
{
    ;
}

/**
 * @brief   ディフォルト値設定
 * @param   なし
 * @return	なし
 * @note	 
 */
void CEditorConfig::SetDefault()
{
    strFontName = _T("MS Gothic");
    iFontSize   = 9;
    iFontSet    = SHIFTJIS_CHARSET;

    bVisibleSpace   = true;     //スペースの表示
    bVisibleTab     = true;     //タブの表示
    bVisibleReturn  = false;    //改行の表示
    bVisibleEof     = false;    //EOFの表示
    //bVisibleLineNum = false;    //行番号の表示
    bVisibleLineNum =   true;    //行番号の表示
    bVisibleUnderLine = false;

    //eWarpMode         = WRAP_NONE;           // 折返し有無
    eWarpMode         = WRAP_WINDOW;           // 折返し有無
}

/**
 * @brief   XML読み込み
 * @param   [in] pXml 
 * @return	なし
 * @note	 
 */
void CEditorConfig::LoadXml(StdXmlArchiveIn* pXml)
{
    *pXml >> boost::serialization::make_nvp("Root", *this);
}

/**
 * @brief   XML書き込み
 * @param   [in] pXml
 * @return	なし
 * @note	 
 */
void CEditorConfig::SaveXml(StdXmlArchiveOut* pXml)
{
    *pXml << boost::serialization::make_nvp("Root", *this);
}

/**
 * @brief   ファイル読み込み
 * @param   なし
 * @return	なし
 * @note	 
 */
void CEditorConfig::Load()
{
    namespace fs = boost::filesystem;

    StdPath  pathLoad = CSystem::GetSystemPath();
    pathLoad /= m_pathFileName;

    CConfig::Load(pathLoad);
}

/**
 * @brief   ファイル書き込み
 * @param   なし          
 * @retval  なし
 * @note    普通は使わない
 */
void CEditorConfig::Save()
{
    StdPath  pathSave =     CSystem::GetSystemPath();
    pathSave /= m_pathFileName;

    CConfig::Save(pathSave);
}

CEditorConfig& CEditorConfig::operator = (const CEditorConfig & obj)
{
    strFontName     = obj.strFontName;
    iFontSize       = obj.iFontSize;
    iFontSet        = obj.iFontSet;

    bVisibleSpace   = obj.bVisibleSpace;     
    bVisibleTab     = obj.bVisibleTab;       
    bVisibleReturn  = obj.bVisibleReturn;    
    bVisibleEof     = obj.bVisibleEof;       
    bVisibleLineNum = obj.bVisibleLineNum;   
    bVisibleUnderLine   = obj.bVisibleUnderLine; 

    strEmphasis1        = obj.strEmphasis1;
    strEmphasis2        = obj.strEmphasis2;
    iEmphaisiType       = obj.iEmphaisiType;
    crEmphasisColor     = obj.crEmphasisColor;

    //------------------------
    //・折り返し
    eWarpMode       = obj.eWarpMode;       
    bWordBreak      = obj.bWordBreak;      
    bJpnPeriod      = obj.bJpnPeriod;    
    bJpnQuotation   = obj.bJpnQuotation; 

    //------------------------
    //・色
    crText              = obj.crText;        
    crBackGround        = obj.crBackGround;  
    crCrLf              = obj.crCrLf;        
    crHalfSpace         = obj.crHalfSpace;   
    crNormalSpace       = obj.crNormalSpace; 
    crTab               = obj.crTab;         
    crEof               = obj.crEof;         
    crUnderLine         = obj.crUnderLine;   
    crLineNumBorder     = obj.crLineNumBorder;
    crLineNumText       = obj.crLineNumText;  
    crCenterLine        = obj.crCenterLine;   

    return *this;
}

//======================================
//======================================
//======================================
/**
 *  @brief   コンストラクター
 *  @param   なし
 *  @retval  なし
 *  @note
 */
CEditorPropertyGrid::CEditorPropertyGrid()
{

}

/**
 *  @brief   初期設定
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CEditorPropertyGrid::Setup()
{

    EnableHeaderCtrl();
    EnableDescriptionArea();
    SetVSDotNetLook(true/*m_bDotNetLook*/);
    MarkModifiedProperties(true/*m_bMarkChanged*/);
    SetAlphabeticMode(false/*!m_bPropListCategorized*/);
    SetShowDragContext(true/*m_bShowDragContext*/);

    std::auto_ptr<CMFCPropertyGridProperty> 
        apDrawConfig(new CMFCPropertyGridProperty(_T("描画設定")));

    //
    CMFCPropertyGridProperty* pGroupDisp = 
        new CMFCPropertyGridProperty(_T("描画設定Sub1"));
	apDrawConfig->AddSubItem(pGroupDisp);


    //===============
    //レイヤー最大数
    /*
    _SetPorperty(pGroupDisp, &m_Cfg.iLayerMax, E_VAL_INT,
                             _T("レイヤー最大数"),
                             _T("レイヤー最大数の説明"),
                             0);
    */

    
	AddProperty(apDrawConfig.release());


    // BOOL値



    //===============
	//AddProperty(apGroupEditor.release());

}

/**
 *  @brief   データ設定
 *  @param   [in] setting 設定値
 *  @retval  なし
 *  @note
 */
void CEditorPropertyGrid::SetData(const CEditorConfig& setting)
{
    m_Cfg = setting;
}

/**
 *  @brief   データ取得
 *  @param   [out]  pSetting 取得データ
 *  @retval  なし
 *  @note
 */
void CEditorPropertyGrid::GetData(CEditorConfig* pSetting)
{
    *pSetting = m_Cfg;
}
