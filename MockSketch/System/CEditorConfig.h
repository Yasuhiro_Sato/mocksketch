/**
 * @brief			CEditorConfigヘッダーファイル
 * @file			CEditorConfig.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef __EDITOR_CONFIG_H__
#define __EDITOR_CONFIG_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CConfig.h"

enum E_WRAP_MODE
{
    WRAP_NONE,          //折り返しなし
    WRAP_WINDOW,        //画面端で折り返す
};


/**
 * @class   CEditorConfig
 * @brief                        
 */
class CEditorConfig: public CConfig
{
public:

    CEditorConfig();
    ~CEditorConfig();

    void SetDefault();

    //!< XML読み込み
    virtual void LoadXml(StdXmlArchiveIn* pXml);

    //!< XML書き込み
    virtual void SaveXml(StdXmlArchiveOut* pXml);

    virtual void Save();

    virtual void Load();

    CEditorConfig& operator = (const CEditorConfig & obj);

public:

    StdString   strFontName;
    int         iFontSize;
    int         iFontSet;

    bool        bVisibleSpace;     //スペースの表示
    bool        bVisibleTab;       //タブの表示
    bool        bVisibleReturn;    //改行の表示
    bool        bVisibleEof;       //EOFの表示
    bool        bVisibleLineNum;   //行番号の表示
    bool        bVisibleUnderLine; //アンダーラインの表示

    //----------------------------
    StdString   strEmphasis1;
    StdString   strEmphasis2;
    int         iEmphaisiType;
    COLORREF    crEmphasisColor;

    //------------------------
    //・折り返し
    E_WRAP_MODE     eWarpMode;         // 折返モード
    //int            iWarpWidth;         // 折返幅

    bool bWordBreak;       // 英文ワードラップ
    bool bJpnPeriod;       // 日本語の句読点を行頭に持ってこない
    bool bJpnQuotation;    // 日本語のカギ括弧を行頭に持ってこない

    //------------------------
    //・色
    COLORREF crText;        //CP_TEXT       通常テキスト色
    
    
    
    COLORREF crBackGround;  //CP_BACKGROUND 背景色
    COLORREF crCrLf;        //CP_CRLF       改行マーク色
    COLORREF crHalfSpace;   //CP_HALFSPACE  半角スペース色
    COLORREF crNormalSpace; //CP_NORMALSPACE    全角スペース色
    COLORREF crTab;         //CP_TAB            タブ文字色
    COLORREF crEof;         //CP_EOF            [EOF]マーク色(End of File)
    COLORREF crUnderLine;   //CP_UNDERLINE      キャレット位置下線
    COLORREF crLineNumBorder;   //CP_LINENUMBORDER  行番号との境界線
    COLORREF crLineNumText;     //CP_LINENUMTEXT    行番号テキスト
    COLORREF crCenterLine;      //CP_CARETLINE      行番号のキャレットがある行の強調背景


protected:
    StdString m_pathFileName;

private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
            SERIALIZATION_BOTH("FontName"      , strFontName);
            SERIALIZATION_BOTH("FontSize"      , iFontSize);
            SERIALIZATION_BOTH("FontSet"       , iFontSet);

            SERIALIZATION_BOTH("VisibleSpace"         , bVisibleSpace);
            SERIALIZATION_BOTH("VisibleTab"           , bVisibleTab);
            SERIALIZATION_BOTH("VisibleReturn"        , bVisibleReturn);
            SERIALIZATION_BOTH("VisibleEof"           , bVisibleEof);
            SERIALIZATION_BOTH("VisibleLine Number"   , bVisibleLineNum);
            SERIALIZATION_BOTH("VisibleUnderline"     , bVisibleUnderLine);

            SERIALIZATION_BOTH("WarpMode"         , eWarpMode);
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }
};



/**
 * @class   CEditorPropertyGrid
 * @brief   エディタ設定 プロパティグリッド               
 */
class CEditorPropertyGrid : public CConfigPropertyGrid
{
public:
    //!<コンストラクタ
    CEditorPropertyGrid();

    //!<デストラクタ
    virtual ~CEditorPropertyGrid(){;}

    //!<初期設定
    virtual void Setup();

    //!< データ設定
    void SetData(const CEditorConfig& setting);
    
    //!< データ取得
    void GetData(CEditorConfig* pSetting);

protected:
    mutable CEditorConfig  m_Cfg;
};

#endif __EDITOR_CONFIG_H__
