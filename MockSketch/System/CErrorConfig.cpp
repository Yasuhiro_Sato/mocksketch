/**
 * @brief			CErrorConfig実装ファイル
 * @file			CErrorConfig.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "CErrorConfig.h"
#include "CSystem.h"
#include "MOCK_ERROR.h"
#include "Utility/CUtility.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 *  @brief   コンストラクター
 *  @param   なし
 *  @retval  なし
 *  @note
 */
CErrorConfig::CErrorConfig()
{
    m_pathFileName = _T("ErrorConfig.xml");
}

/**
 *  @brief   デストラクタ
 *  @param   なし  
 *  @retval  なし
 *  @note
 */
CErrorConfig::~CErrorConfig()
{


}

/**
 *  @brief   ファイルシステムエラー設定
 *  @param   なし  
 *  @retval  なし
 *  @note
 */
void CErrorConfig::InitError()
{
    m_mapError[e_line_create    ] = _T("直線が作成できません"); 
    m_mapError[e_circle_create  ] = _T("円が作成できません");

    m_mapError[e_line_cross     ] = _T("交点が存在しません"); 

    m_mapError[e_no_answer      ] = _T("解がありません"); 

    m_mapError[e_no_add_open_line      ] = _T("閉じていない複合線を追加することはできません"); 
    m_mapError[e_no_add_type           ] = _T("複合線に追加できる図形は、直線,円,楕円,スプライン,複合線のみです"); 
    m_mapError[e_no_add_cross          ] = _T("図形を閉じるには、先端と終端が交差している必要があります"); 
    m_mapError[e_no_add_intersection   ] = _T("この図形と交差する点がありません"); 
    m_mapError[e_no_add_object         ] = _T("図形が選択されていません"); 

    //------------------
    // 表示関連
    m_mapError[e_view           ] =  _T("表示関連エラー"); 


    //------------------
    // 入力関連
    m_mapError[e_input          ] =  _T("入力関連エラー"); 

    m_mapError[e_same_name      ] =  _T("同じ名称が存在します");  
    m_mapError[e_no_use_char    ] =  _T("英数字と_(アンダースコア)以外の文字は使用できません");
    m_mapError[e_use_keyword    ] =  _T("キーワードが使用されています"); 
    m_mapError[e_no_use_file    ] =  _T("次の文字を使用することはできません「 < > : \" / \\ | ? * 」");
    m_mapError[e_no_use_first_dot]=  _T("先頭に\".\"を(ピリオド)使用することはできません"); 
    m_mapError[e_no_use_end_dot ] =  _T("後端に\".\"(ピリオド)を使用することはできません");

    //------------------
    // Boost::Filesystem
    m_mapError[e_file_system               ] =  _T("ファイルシステム");
    m_mapError[e_file_system_error         ] =  _T("ファイルシステムエラー"); // system generated error; if possible, is translated
    m_mapError[e_file_other_error          ] =  _T("ファイルシステムその他のエラー"); // library generated error
    m_mapError[e_file_security_error       ] =  _T("ファイルセキュリティエラー"); // includes access rights, permissions failures
    m_mapError[e_file_read_only_error      ] =  _T("読み込み専用属性が設定されています");
    m_mapError[e_file_io_error             ] =  _T("IO関連のエラー");
    m_mapError[e_file_path_error           ] =  _T("ファイルパスのエラー");
    m_mapError[e_file_not_found_error      ] =  _T("ファイルが見つかりません");
    m_mapError[e_file_busy_error           ] =  _T("ファイルの応答がありません");  // implies trying again might succeed
    m_mapError[e_file_already_exists_error ] =  _T("すでに存在します");
    m_mapError[e_file_not_empty_error      ] =  _T("空ではありません");
    m_mapError[e_file_is_directory_error   ] =  _T("ディレクトリのエラー");
    m_mapError[e_file_out_of_space_error   ] =  _T("容量が足りません");
    m_mapError[e_file_out_of_memory_error  ] =  _T("メモリが足りません");
    m_mapError[e_file_out_of_resource_error] =  _T("リソースが足りません");

    //------------------
    m_mapError[e_parts_create              ] =  _T("部品の生成に失敗しました");
    m_mapError[e_object_full               ] =  _T("これ以上、図形を作成できません");
    m_mapError[e_gc_request                ] =  _T("ガベージコレクション要求");
    m_mapError[e_main_not_init             ] =  _T("メインフレーム未生成");
    m_mapError[e_object_create             ] =  _T("図形の生成に失敗しました");

    //------------------
    m_mapError[e_cycle_ref                 ] =  _T("循環参照が発生します");
    m_mapError[e_use_parts                 ] =  _T("YESNO:この部品を使用している所があります\n削除しますか？");

    //------------------
    m_mapError[e_val_error                 ] =  _T("不正な値が設定されました");
    m_mapError[e_not_find                  ] =  _T("検索に失敗しました");
    m_mapError[e_type_error                ] =  _T("型の不整合が発生しました");

    //------------------
    m_mapError[e_not_value                 ] =  _T("数値を入力してください");
    m_mapError[e_not_eql_min_max           ] =  _T("最小値と最大値は同じ値に設定しないでください");
    m_mapError[e_io_conv_format1           ] =  _T("区分線形補間は [入力値:出力値], [入力値:出力値], ..と入力してください");
    m_mapError[e_io_conv_format2           ] =  _T("多項式補間は  係数, 係数, ..と入力してください");
    m_mapError[e_io_conv_inc               ] =  _T("区分線形補間の入力値は、単調増加となるように入力してください");
    m_mapError[e_io_conv_inp               ] =  _T("最小値:最大値 又は [入力値:出力値], [入力値:出力値], ....,[入力値:出力値]  若しくは  係数n, 係数n-1,... 係数0 (n>0)と入力してください");
    m_mapError[e_type_err_min_max          ] =  _T("最小値と最大値で設定できるのは、AI,AOのみです");


    m_mapError[e_io_conv_inp               ] =  _T("最小値:最大値 又は [入力値:出力値], [入力値:出力値], ....,[入力値:出力値]  若しくは  係数n, 係数n-1,... 係数0 (n>0)と入力してください");
    m_mapError[e_type_err_min_max          ] =  _T("最小値と最大値で設定できるのは、AI,AOのみです");

    //------------------
    //
    m_mapError[e_file_read                 ] =  _T("ファイル読み込みエラー");
    m_mapError[e_file_write                ] =  _T("ファイル書き込みエラー");
    m_mapError[e_file_read_s               ] =  _T("ファイル%sの読み込みに失敗しました");
    m_mapError[e_file_write_s              ] =  _T("ファイル%sの書き込みに失敗しました");
    m_mapError[e_file_ext                  ] =  _T("想定外のファイル拡張子です");
    m_mapError[e_file_exist                ] =  _T("ファイルが存在しません");

    //------------------
    //Project
    m_mapError[e_project_save              ] =  _T("プロジェクトの保存に失敗しました");
    m_mapError[e_project_create            ] =  _T("プロジェクトの生成に失敗しました");

    //------------------
    //Library
    m_mapError[e_library_create            ] = _T("ライブラリの生成に失敗しました");

    //------------------
    //Library
    m_mapError[e_library_create            ] = _T("ライブラリの生成に失敗しました");

    //------------------
    //System
    m_mapError[e_buffer_full               ] = _T("バッファが一杯");
    m_mapError[e_memory_alloc              ] = _T("メモリの確保を失敗しました");


    //---------------------------
    //PropertyGroup
	m_mapError[e_expect_braces             ] = _T(" \"{\" がありません");
	m_mapError[e_dup_name_s                ] = _T("名称(%s)が重複しています");

    //CStdPropertyItemDef
	m_mapError[e_lack_item                ] = _T("項目数が足りません");
	m_mapError[e_unkonwn_type             ] = _T("不明な型が使用されました");



    //------------------
    m_mapError[e_unknown_error             ]      =  _T("不明なエラー");
    m_mapError[e_error                     ]      =  _T("エラー");//!< 一般的なエラー
}


/**
 *  @brief   表示文字設定
 *  @param   なし  
 *  @retval  なし
 *  @note
 */
void CErrorConfig::InitString()
{
    m_mapStr[e_who]   = _T("発生元");
    m_mapStr[e_path1] = _T("パス1");
    m_mapStr[e_path2] = _T("パス2");
}

/**
 *  @brief   エラー文字取得
 *  @param   [in] iErrorCode  エラーコード
 *  @retval  なし
 *  @note
 */
StdString CErrorConfig::GetErrorStr(int iErrorCode)
{
    std::map<int, StdString>::iterator ite;
    
    ite = m_mapError.find(iErrorCode);

    if (ite == m_mapError.end())
    {
        return m_mapError[e_unknown_error];
    }

    return ite->second;
}

/**
 * @brief   ディフォルト値設定
 * @param   なし
 * @return	なし
 * @note	 
 */
void CErrorConfig::SetDefault()
{
    InitError();
    InitString();
}

/**
 * @brief   XML読み込み
 * @param   [in] pXml 
 * @return	なし
 * @note	 
 */
void CErrorConfig::LoadXml(StdXmlArchiveIn* pXml)
{
    *pXml >> boost::serialization::make_nvp("Root", *this);
}

/**
 * @brief   XML書き込み
 * @param   [in] pXml
 * @return	なし
 * @note	 
 */
void CErrorConfig::SaveXml(StdXmlArchiveOut* pXml)
{
    *pXml << boost::serialization::make_nvp("Root", *this);
}

/**
 * @brief   ファイル読み込み
 * @param   なし          
 * @retval  なし
 * @note	
 */
void CErrorConfig::Load()
{
    //全ユーザ共通設定共通
    StdPath  pathLoad = CSystemLocal::GetLocalDir();
    pathLoad /= m_pathFileName;
    
    CConfig::Load2(pathLoad);
}

/**
 * @brief   ファイル書き込み
 * @param   なし          
 * @retval  なし
 * @note	
 */
void CErrorConfig::Save()
{
    StdPath  pathSave = CSystemLocal::GetLocalDir();
    pathSave /= m_pathFileName;

    CConfig::Save2(pathSave);
}
