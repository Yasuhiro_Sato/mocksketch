/**
 * @brief			CErrorConfigヘッダーファイル
 * @file			CErrorConfig.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef __ERROR_CONFIG_H__
#define __ERROR_CONFIG_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CConfig.h"

/**
 * @class   CErrorConfig
 * @brief                        
 */
class CErrorConfig : public CConfig
{
    enum e_str
    {
        e_who   = 1,
        e_path1 = 2,
        e_path2 = 3,
    };

protected:

    //!< エラーコード変換
    std::map<int, StdString> m_mapError;

    std::map<int, StdString> m_mapStr;

    //!< 保存ファイル名
    StdPath m_pathFileName;

private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        std::string  s;
        namespace ser = boost::serialization;
        try 
        {
            s = "Error"; ar & ser::make_nvp(s.c_str() , m_mapError);
            s = "String"; ar & ser::make_nvp(s.c_str() , m_mapStr);
        }
        catch(MockException& e)
        {
            throw e;
        }
        catch(...)
        {
            StdStringStream strm;
            StdString str = CUtil::CharToString(s.c_str());

            int iErrorNo;
            if (Archive::is_loading::value)
            {
                iErrorNo = e_file_read;
                strm << StdFormat(_T("File read error %s:%s"))
                        % DB_FUNC_NAME
                        % str.c_str();
            }
            else
            {
                iErrorNo = e_file_write;
                strm << StdFormat(_T("File write error %s:%s"))
                        % DB_FUNC_NAME
                        % str.c_str();
            }
            throw MockException(iErrorNo, strm.str().c_str());
        }
    }


public:

    CErrorConfig();
    ~CErrorConfig();

    //!< エラー文字取得
    StdString GetErrorStr(int iErrorCode);

    virtual void SetDefault();

    //!< XML読み込み
    virtual void LoadXml(StdXmlArchiveIn* pXml);

    //!< XML書き込み
    virtual void SaveXml(StdXmlArchiveOut* pXml);

    virtual void Load();

    virtual void Save(); 

protected:
    //!< エラーコード設定
    void InitError();

    //!< 表示文字設定
    void InitString();

};

#endif __ERROR_CONFIG_H__
