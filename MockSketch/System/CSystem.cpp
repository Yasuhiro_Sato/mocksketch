/**
 * @brief			CSystem実装ファイル
 * @file			CSystem.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "CSystem.h"
#include "MockSketch.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifndef _CONSOLE
#ifdef _DEBUG
#define new DEBUG_NEW
#endif
#endif


/*---------------------------------------------------*/
/* Static variables                                  */
/*---------------------------------------------------*/
CSystem* CSystem::ms_pSystem = NULL;

/**
 *  @brief  コンストラクタ.
 *  @param  なし
 *  @retval なし
 *  @note   
 */
CSystem::CSystem()
{
    CheckSystemPath();

    m_psSysInit  = std::make_shared<CSystemInit>();
    m_psSysInit->Load();
    
    m_psSysLocal  = std::make_shared<CSystemLocal>();
    m_psSysLocal->Init();
    m_psSysInit->Load();

    m_psErrorConfig = std::make_shared<CErrorConfig>();
    m_psErrorConfig->Load();

    m_psSysString = std::make_shared<CSystemString>();
    m_psSysString->Load();

    m_psUnitCtrl = std::make_shared<CUnitCtrl>();

    m_psSysConfig = std::make_shared<CSystemConfig>();

    m_psDrawConfig = std::make_shared<CDrawConfig>();

    m_psEditorConfig = std::make_shared<CEditorConfig>();

    m_psSysLibrary = std::make_shared<CSystemLibrary>();

    m_psSysFile = std::make_shared<CSystemFile>();

    m_psSysProperty = std::make_shared<CSystemProperty>();

    m_psSysConfig->Init(m_psSysString.get());

}


/**
 *  @brief  デストラクター
 *  @param  なし
 *  @retval なし
 *  @note   
 */
CSystem::~CSystem()
{
    //Save();
}


/**
 *  @brief  エラー設定
 *  @param  なし
 *  @retval エラー設定へのポインタ
 *  @note   
 */
CErrorConfig* CSystem::GetErrorConfig()
{
    return m_psErrorConfig.get();
}

/**
 *  @brief  システム設定
 *  @param  なし
 *  @retval システム設定へのポインタ
 *  @note   
 */
CSystemConfig* CSystem::GetSystemConfig()
{
    return m_psSysConfig.get();
}

/**
 *  @brief  描画設定
 *  @param  なし
 *  @retval 描画設定へのポインタ
 *  @note   
 */
CDrawConfig* CSystem::GetDrawConfig()
{
    return m_psDrawConfig.get();
}

/**
 *  @brief  文字設定
 *  @param  なし
 *  @retval 文字設定へのポインタ
 *  @note   
 */
CSystemString* CSystem::GetSystemString()
{
    return m_psSysString.get();
}


/**
 *  @brief  単位系設定
 *  @param  なし
 *  @retval 単位系設定へのポインタ
 *  @note   
 */
CUnitCtrl* CSystem::GetUnitCtrl()
{
    return m_psUnitCtrl.get();

}

/**
 *  @brief  エディタ設定
 *  @param  なし
 *  @retval エディタへのポインタ
 *  @note   
 */
CEditorConfig* CSystem::GetEditorConfig()
{
    return m_psEditorConfig.get();
}

/**
 *  @brief  ライブラリ設定
 *  @param  なし
 *  @retval ライブラリへのポインタ
 *  @note   
 */
CSystemLibrary* CSystem::GetSystemLibrary()
{
    return m_psSysLibrary.get();
}

/**
 *  @brief  ロケール設定
 *  @param  なし
 *  @retval ロケールのポインタ
 *  @note   
 */
CSystemLocal* CSystem::GetSystemLocal()
{
    return m_psSysLocal.get();
}

/**
 *  @brief  ファイル設定
 *  @param  なし
 *  @retval ファイル設定のポインタ
 *  @note   
 */
CSystemFile* CSystem::GetSystemFile()
{
    return m_psSysFile.get();
}


/**
 *  @brief  プロパティグループ
 *  @param  なし
 *  @retval プロパティグループのポインタ
 *  @note   
 */
CSystemProperty* CSystem::GetSystemProperty()
{
    return m_psSysProperty.get();
}

/**
 *  @brief  インスタンス削除
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CSystem::DeleteInstance()
{
    if (ms_pSystem != NULL)
    {
        delete ms_pSystem;
        ms_pSystem = NULL;
    }
}

/**
 *  @brief  インスタンス取得
 *  @param  なし
 *  @retval Systemへのポインタ
 *  @note   
 */
CSystem* CSystem::GetInstance()
{
    if (ms_pSystem == NULL)
    {
        ms_pSystem = new CSystem;
    }
    return ms_pSystem;
}


/**
 *  @brief  システムパス取得
 *  @param  なし
 *  @retval システムパス取得へのポインタ
 *  @note   
 */
StdPath CSystem::GetSystemPath()
{
    StdPath pathStd;
    if(THIS_APP->IsSettingAllUser())
    {
        pathStd = GetSystemCommonPath();
    }
    else
    {
        pathStd = GetDataPath();
        pathStd /= _T("System");
    }
    return pathStd;
}

/**
 *  @brief  システム共通パス取得
 *  @param  なし
 *  @retval システムパス取得へのポインタ
 *  @note   
 */
    StdPath  CSystem::GetSystemCommonPath()
{
    StdPath pathStd = GeWindowsPath(CSIDL_COMMON_APPDATA);
    pathStd /= _T("System");
    return pathStd;
}

/**
 *  @brief  ユーザデータパス取得
 *  @param  なし
 *  @retval システムパス取得へのポインタ
 *  @note   
 */
StdPath  CSystem::GetUserPath()
{
    StdPath pathStd = GeWindowsPath(CSIDL_PERSONAL);
    return pathStd;
}


/**
 * @brief   ウインドウ関連パス取得
 * @param   [in] dwCSID
 * @return  パス
 * @note	 
 */
StdPath CSystem::GeWindowsPath(DWORD dwCSID)
{
    namespace fs =  boost::filesystem;

    StdPath pathRet;
    StdString strPath;
    TCHAR szPath[2054/*2048*/] = {0};
    LPITEMIDLIST pidl;

    IMalloc *pMalloc;
    if (SHGetMalloc(&pMalloc) != S_OK)
    {
        MOCK_EXCEPTION(e_memory_alloc);
    }

	if (pMalloc == 0)
	{
		MOCK_EXCEPTION(e_memory_alloc);
	}

    if( SUCCEEDED(SHGetSpecialFolderLocation(NULL,dwCSID,&pidl)) )
    { 
        SHGetPathFromIDList(pidl,szPath); // パスに変換する
        pMalloc->Free(pidl);              // 取得したIDLを解放する (CoTaskMemFreeでも可)

        //直接StdPathに設定すると 文字長の設定が不正となる。
        strPath = szPath;
        pathRet =  strPath; 

        //fs::path p1("ディレクトリ\ファイル.txt", fs::native)
        // インストーラでこのディレクトリをつっくておく
        pathRet /= _T("/MockSketch");
#ifndef _DEBUG
        if(!fs::exists( pathRet ))
        {
            //なければ作成する
            fs::create_directory( pathRet );
        }
#endif
    }
    
#ifdef _DEBUG
    if ( (szPath[0] == 0) ||
         (!fs::exists( pathRet )))
    {
        if (::GetModuleFileName(NULL, szPath, 2054/*2048*/)) 
        {
            pathRet = DeleteFileName(szPath);
        }
        else
        {
            pathRet = fs::initial_path<StdPath>();
        }
    }
#endif

    pMalloc->Release();
    return pathRet;

}


/**
 *  @brief  システムパスチェック
 *  @param  なし
 *  @retval なし
 *  @note   存在しない場合はシステムパスを作成しておく
 */
void CSystem::CheckSystemPath()
{
    StdPath psthSystem = GetSystemPath();

    namespace fs = boost::filesystem;
    
    try
    {
        if (!fs::exists(psthSystem))
        {
            fs::create_directory(psthSystem);
        }
    }
    catch(...)
    {
        STD_DBG(_T("Exception"));
    }
}


/**
 * @brief   ファイル読み込み
 * @param   なし
 * @return	なし
 * @note	 
 */
bool CSystem::Load()
{
    try
    {
        m_psSysLocal->Load();

        m_psErrorConfig->Load();
        
        m_psSysConfig->Load();
        
        m_psUnitCtrl->Load();

        m_psDrawConfig->Load();

        m_psEditorConfig->Load();

        m_psSysLibrary->Load();

        m_psSysInit->Load ();

        m_psSysFile->Load ();

        m_psSysProperty->Load ();

    }
    catch (MockException& e) 
    {
        e;
        return false;
    }
    return true;
}

/**
 * @brief   ファイル書き込み
 * @param   なし          
 * @retval  なし
 * @note    
 */
bool CSystem::Save()
{
    try
    {
        m_psSysLocal->Save();

        m_psErrorConfig->Save();
        
        m_psSysConfig->Save();
        
        m_psUnitCtrl->Save();

        m_psDrawConfig->Save();

        m_psEditorConfig->Save();

        m_psSysLibrary->Save();

        m_psSysInit->Save();

        m_psSysFile->Save();

        m_psSysProperty->Save ();

    }
    catch (MockException& e) 
    {
        e;
        return false;
    }
    return true;

}
