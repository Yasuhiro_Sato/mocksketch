/**
 * @brief			CSystemヘッダーファイル
 * @file			CSystem.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef __SYSTEM_H__
#define __SYSTEM_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CErrorConfig.h"
#include "CSystemConfig.h"
#include "CSystemString.h"
#include "System/CDrawConfig.h"

#include "CEditorConfig.h"
#include "CSystemLibrary.h"
#include "CSystemLocal.h"
#include "CSystemInit.h"
#include "CSystemFile.h"
#include "CSystemProperty.h"
#include "CUnit.h"
#include "MOCK_ERROR.h"


/*---------------------------------------------------*/
/*  class                                            */
/*---------------------------------------------------*/

/*---------------------------------------------------*/
/*  macros                                           */
/*---------------------------------------------------*/

#define  DRAW_CONFIG   CSystem::GetInstance()->GetDrawConfig()
#define  SYS_ERR       CSystem::GetInstance()->GetErrorConfig()
#define  SYS_CONFIG    CSystem::GetInstance()->GetSystemConfig()
#define  SYS_STR       CSystem::GetInstance()->GetSystemString()
#define  SYS_UNIT      CSystem::GetInstance()->GetUnitCtrl()
#define  SYS_EDITOR    CSystem::GetInstance()->GetEditorConfig()
#define  SYS_LIBRARY   CSystem::GetInstance()->GetSystemLibrary()
#define  SYS_LOCAL     CSystem::GetInstance()->GetSystemLocal()
#define  SYS_FILE      CSystem::GetInstance()->GetSystemFile()
#define  GET_SYS_STR(s)     CSystem::GetInstance()->GetSystemString()->GetString( s )
#define  GET_COLOR_STR(s)   CSystem::GetInstance()->GetSystemString()->GetColorName( s )
#define  GET_STR(s)     CSystem::GetInstance()->GetSystemString()->GetString( s ).c_str()
#define  GET_ERR_STR(s) CSystem::GetInstance()->GetErrorConfig()->GetErrorStr( s ).c_str()
#define  SYS_PROPERTY CSystem::GetInstance()->GetSystemProperty()



/**
 * @class   CSystem
 * @brief                        
 */
class CSystem
{
public:

    static CSystem* GetInstance();

    CErrorConfig* GetErrorConfig();

    CSystemConfig* GetSystemConfig();

    CSystemString* GetSystemString();

    CDrawConfig* GetDrawConfig();

    CUnitCtrl* GetUnitCtrl();

    CEditorConfig* GetEditorConfig();

    CSystemLibrary* GetSystemLibrary();

    CSystemLocal* GetSystemLocal();

    CSystemInit* GetSystemInit();

    CSystemFile* GetSystemFile();

    CSystemProperty* GetSystemProperty(); 

    //読込
    bool Load();

    //保存
    bool Save();

    static void DeleteInstance();

    //<! システムパス取得
    static StdPath  GetSystemPath();

    //<! システム共通パス取得
    static StdPath  GetSystemCommonPath();

    //<! ユーザデータパス取得
    static StdPath  GetUserPath();

    virtual ~CSystem();

    //<! ウインドウ関連パス取得
    static StdPath GeWindowsPath(DWORD dwCSID);

    //<! システム設定完了確認
    static bool IsInit();



    StdString GetStrVoid   (){return m_StrVoid;}
    StdString GetStrInt    (){return m_StrInt;}
    StdString GetStrUint   (){return m_StrUint;}
    StdString GetStrLong   (){return m_StrLong;}
    StdString GetStrUlong  (){return m_StrUlong;}
    StdString GetStrFloat  (){return m_StrFloat;}
    StdString GetStrDouble (){return m_StrDouble;}
    StdString GetStrBool   (){return m_StrBool;}
    StdString GetStrLstPt2d(){return m_StrLstPt2d;}
    StdString GetStrColor  (){return m_StrColor;}
    StdString GetStrString (){return m_StrString;}
    StdString GetStrPoint2D(){return m_StrPoint2D;}
    StdString GetStrRect2D (){return m_StrRect2D;}

protected:
    //!< システムパスチェック
    void CheckSystemPath();


protected:
    CSystem();

    static CSystem* ms_pSystem;

    static bool  ms_bInit;

    std::shared_ptr<CErrorConfig> m_psErrorConfig;

    std::shared_ptr<CSystemConfig> m_psSysConfig;

    std::shared_ptr<CSystemString> m_psSysString;

    std::shared_ptr<CDrawConfig> m_psDrawConfig;

    std::shared_ptr<CUnitCtrl> m_psUnitCtrl;

    std::shared_ptr<CEditorConfig> m_psEditorConfig;

    std::shared_ptr<CSystemLibrary> m_psSysLibrary;

    std::shared_ptr<CSystemLocal> m_psSysLocal;

    std::shared_ptr<CSystemInit> m_psSysInit;

    std::shared_ptr<CSystemFile> m_psSysFile;

    std::shared_ptr<CSystemProperty> m_psSysProperty;

protected:
    StdString  m_StrVoid;
    StdString  m_StrInt;
    StdString  m_StrUint;
    StdString  m_StrLong;
    StdString  m_StrUlong;
    StdString  m_StrFloat;
    StdString  m_StrDouble;
    StdString  m_StrBool;
    StdString  m_StrLstPt2d;
    StdString  m_StrColor;
    StdString  m_StrString;
    StdString  m_StrPoint2D;
    StdString  m_StrRect2D;

private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
            ar & boost::serialization::make_nvp( "Local", ms_strLanguageCode)
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }

};

#endif __SYSTEM_H__
