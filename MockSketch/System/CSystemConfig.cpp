/**
 * @brief			CSystemConfig実装ファイル
 * @file			CSystemConfig.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "CSystemConfig.h"
#include "CSystemString.h"
#include "CSystem.h"
#include "Utility/CUtility.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifndef _CONSOLE
#ifdef _DEBUG
#define new DEBUG_NEW
#endif
#endif

/**
 * @brief   コンストラクタ
 * @param   なし
 * @return	なし
 * @note	 
 */
CSystemConfig::CSystemConfig()
{
    m_pathFileName = _T("SystemConfig.xml");
}


/**
 * @brief   デストラクタ
 * @param   なし
 * @return	なし
 * @note	 
 */
CSystemConfig::~CSystemConfig()
{
}

/**
 * @brief   初期化
 * @param   [in]  pSysStr システム文字列へのポインタ
 * @return	なし
 * @note	起動は一回のみ 
 */
void CSystemConfig::Init(const CSystemString* pSysStr)
{

    CSystemString* pSysStrUse = const_cast<CSystemString*>(pSysStr);
    STD_ASSERT(pSysStrUse != NULL);

    AddColor( RGB(0x00, 0x00, 0x00),    pSysStrUse->GetColorName( STR_COLOR_BLACK            ));
    AddColor( RGB(0xA5, 0x2A, 0x00),    pSysStrUse->GetColorName( STR_COLOR_BROWN            ));
    AddColor( RGB(0x00, 0x40, 0x40),    pSysStrUse->GetColorName( STR_COLOR_DARK_OLIVE_GREEN ));
    AddColor( RGB(0x00, 0x55, 0x00),    pSysStrUse->GetColorName( STR_COLOR_DARK_GREEN       ));
    AddColor( RGB(0x00, 0x00, 0x5E),    pSysStrUse->GetColorName( STR_COLOR_DARRK_TEAL       ));
    AddColor( RGB(0x00, 0x00, 0x8B),    pSysStrUse->GetColorName( STR_COLOR_DARK_BLUE        ));
    AddColor( RGB(0x4B, 0x00, 0x82),    pSysStrUse->GetColorName( STR_COLOR_INDIGO           ));
    AddColor( RGB(0x28, 0x28, 0x28),    pSysStrUse->GetColorName( STR_COLOR_GRAY_80          ));

    AddColor( RGB(0x8B, 0x00, 0x00),    pSysStrUse->GetColorName( STR_COLOR_DARK_RED         ));
    AddColor( RGB(0xFF, 0x68, 0x20),    pSysStrUse->GetColorName( STR_COLOR_ORANGE           ));
    AddColor( RGB(0x8B, 0x8B, 0x00),    pSysStrUse->GetColorName( STR_COLOR_DARK_YELLOR      ));
    AddColor( RGB(0x00, 0x93, 0x00),    pSysStrUse->GetColorName( STR_COLOR_GREEN            ));
    AddColor( RGB(0x38, 0x8E, 0x8E),    pSysStrUse->GetColorName( STR_COLOR_TEAL             ));
    AddColor( RGB(0x00, 0x00, 0xFF),    pSysStrUse->GetColorName( STR_COLOR_BLUE             ));
    AddColor( RGB(0x7B, 0x7B, 0xC0),    pSysStrUse->GetColorName( STR_COLOR_BLUE_GRAY        ));
    AddColor( RGB(0x66, 0x66, 0x66),    pSysStrUse->GetColorName( STR_COLOR_GRAY_50          ));

    AddColor( RGB(0xFF, 0x00, 0x00),    pSysStrUse->GetColorName( STR_COLOR_RED              ));
    AddColor( RGB(0xFF, 0xAD, 0x5B),    pSysStrUse->GetColorName( STR_COLOR_LIGHT_ORANGE     ));
    AddColor( RGB(0x32, 0xCD, 0x32),    pSysStrUse->GetColorName( STR_COLOR_LIME             ));
    AddColor( RGB(0x3C, 0xB3, 0x71),    pSysStrUse->GetColorName( STR_COLOR_SEA_GREEN        ));
    AddColor( RGB(51, 204, 204),        pSysStrUse->GetColorName( STR_COLOR_AQUA             ));
    AddColor( RGB(0x7D, 0x9E, 0xC0),    pSysStrUse->GetColorName( STR_COLOR_LIGHT_BLUE       ));
    AddColor( RGB(0x80, 0x00, 0x80),    pSysStrUse->GetColorName( STR_COLOR_VIORET           ));
    AddColor( RGB(0x7F, 0x7F, 0x7F),    pSysStrUse->GetColorName( STR_COLOR_GRAY_40          ));

    AddColor( RGB(0xFF, 0xC0, 0xCB),    pSysStrUse->GetColorName( STR_COLOR_PINK             ));
    AddColor( RGB(0xFF, 0xD7, 0x00),    pSysStrUse->GetColorName( STR_COLOR_GOLD             ));
    AddColor( RGB(0xFF, 0xFF, 0x00),    pSysStrUse->GetColorName( STR_COLOR_YELLOW           ));
    AddColor( RGB(0x00, 0xFF, 0x00),    pSysStrUse->GetColorName( STR_COLOR_BRIGHT_GREEN     ));
    AddColor( RGB(0x00, 0xFF, 0xFF),    pSysStrUse->GetColorName( STR_COLOR_TURQUOISE        ));
    AddColor( RGB(0, 204, 255),         pSysStrUse->GetColorName( STR_COLOR_SKY_BLUE         ));
    AddColor( RGB(234, 128, 102),       pSysStrUse->GetColorName( STR_COLOR_PLUM             ));
    AddColor( RGB(0xC0, 0xC0, 0xC0),    pSysStrUse->GetColorName( STR_COLOR_GRAY_25          ));

    AddColor( RGB(0xFF, 0xE4, 0xE1),    pSysStrUse->GetColorName( STR_COLOR_ROSE             ));
    AddColor( RGB(255, 254, 153),       pSysStrUse->GetColorName( STR_COLOR_TAN              ));
    AddColor( RGB(0xFF, 0xFF, 0xE0),    pSysStrUse->GetColorName( STR_COLOR_LIGHT_YELLOR     ));
    AddColor( RGB(204, 255, 204),       pSysStrUse->GetColorName( STR_COLOR_LIGHT_GREEN      ));
    AddColor( RGB(204, 255, 255),       pSysStrUse->GetColorName( STR_COLOR_TURQUOISE        ));
    AddColor( RGB(153, 204, 255),       pSysStrUse->GetColorName( STR_COLOR_PALE_BLUE        ));
    AddColor( RGB(204, 153, 255),       pSysStrUse->GetColorName( STR_COLOR_LAVENDER         ));
    AddColor( RGB(0xFF, 0xFF, 0xFF),    pSysStrUse->GetColorName( STR_COLOR_WHITE            ));
    InitColorPalette();
}

/**
 * @brief   ディフォルト値設定
 * @param   なし
 * @return	なし
 * @note	 
 */
void CSystemConfig::SetDefault()
{
    strPathData    = GetPathStr(GetDataPath());

    

    iInitIoSize = 8;
    //iMaxExecGroup = 5;
    iIoBufferSize = 1024;
//    editor.SetDefault();
    strDefaultPartsName     = _T("Parts");
    strDefaultReferenceName = _T("Ref");
    strDefaultFieldName     = _T("Field");

    strDefaultMainName  = _T("Main");
    strPathExtIo        = _T("ExtIo");
    strPathScriptAddins = _T("ScriptAddin");
    strPathCommandLib   = _T("CommandLib");

    m_strLibName        = _T("Lib1");

    dwThreadGroup01Time = 100;
    dwThreadGroup02Time = 10;
    dwThreadGroup03Time = 500;
    dwThreadGroup04Time = 100;
    dwThreadGroup05Time = 10;

    dwThreadField01Time = 20;
    dwThreadField02Time = 40;
    dwThreadField03Time = 80;
    dwThreadField04Time = 100;
    dwThreadField05Time = 500;

    dwThreadDispTime = 20;
    dwThreadPaneTime = 1000;

    dwThreadIo01Time = 10;
    dwThreadIo02Time = 10;
    dwThreadIo03Time = 100;

    dwTimerProperty  = 500;
}

/**
 * @brief   ディフォルトスクリプト読み込み
 * @param   なし
 * @return	なし
 * @note	 
 */
void CSystemConfig::LoadDefaultScript()
{
    namespace fs = boost::filesystem;

    StdPath  pathStd = CSystem::GetSystemPath();
    StdPath  pathExp = CSystem::GetSystemPath();
    pathStd /= _T("StdScript.as");
    pathExp /= _T("ExpScript.as");

    bool bRet;
    bRet = CUtil::LoadText(pathStd, &strStdScript);
    bRet = CUtil::LoadText(pathExp, &strExpScript);
}

/**
 * @brief   XML読み込み
 * @param   [in] pXml 
 * @return	なし
 * @note	 
 */
void CSystemConfig::LoadXml(StdXmlArchiveIn* pXml)
{
    *pXml >> boost::serialization::make_nvp("Root", *this);
}

/**
 * @brief   XML書き込み
 * @param   [in] pXml
 * @return	なし
 * @note	 
 */
void CSystemConfig::SaveXml(StdXmlArchiveOut* pXml)
{
    *pXml << boost::serialization::make_nvp("Root", *this);
}

/**
 * @brief   ファイル読み込み
 * @param   なし
 * @return	なし
 * @note	 
 */
void CSystemConfig::Load()
{
    StdPath  pathLoad = CSystem::GetSystemPath();
    pathLoad /= m_pathFileName;

    //デフォルトデータ設定
    LoadDefaultScript();
    CConfig::Load(pathLoad);
}

/**
 * @brief   ファイル書き込み
 * @param   なし          
 * @retval  なし
 * @note    普通は使わない
 */
void CSystemConfig::Save()
{
    StdPath  pathSave =     CSystem::GetSystemPath();
    pathSave /= m_pathFileName;

    CConfig::Save(pathSave);
}



/**
 * @brief   ライブラリ名設定
 * @param   [in] strName
 * @retval  true 設定成功
 * @note
 */
bool CSystemConfig::SetLibraryName(StdString strName)
{
    //TODO:ファイル名チェック
    m_strLibName = strName;
    return true;
}

/**
 * @brief   ライブラリ名取得
 * @param   なし
 * @retval  ライブラリ名
 * @note
 */
StdString CSystemConfig::GetLibraryName() const
{
    return m_strLibName;
}

/**
 * @brief   ライブラリパス取得
 * @param   なし
 * @retval  ライブラリ名
 * @note
 */
StdPath CSystemConfig::GetLibraryPath() const
{
    StdPath pathStd = CSystem::GetSystemPath();
    pathStd /= _T("Library");
    pathStd /= m_strLibName;
    pathStd /= (m_strLibName + _T(".mspx"));
    return pathStd;
}


/**
 * @brief   ライブラリパス生成
 * @param   なし
 * @retval  true 生成成功
 * @note
 */
bool CSystemConfig::CreateLibraryPath()
{
    namespace fs = boost::filesystem;
    StdPath pathStd = CSystem::GetSystemPath();

    try
    {
        pathStd /= _T("Library");
        if (!fs::exists(pathStd))
        {
            fs::create_directory(pathStd);
        }

        pathStd /= m_strLibName;
        if (!fs::exists(pathStd))
        {
            fs::create_directory(pathStd);
        }
        return true;
    }
    catch(...)
    {
        //TODO:エラー表示
    }
    return false;
}

/**
 * @brief   色数取得
 * @param   なし
 * @return	色数
 * @note	 
 */
int CSystemConfig::GetMaxColors()
{
   return (int)m_lstColor.size();
}

/**
 * @brief   色名取得
 * @param   [in] iColorId 色ID
 * @return	色名
 * @note	 
 */
StdString   CSystemConfig::GetColorName(int iColorId)
{
    STD_ASSERT(iColorId >=  0);
    STD_ASSERT(iColorId < (int)m_lstColor.size());

    return m_lstColor[iColorId].strName;
}

/**
 * @brief   RGB色取得
 * @param   [in] iColorId 色ID
 * @return	RGB色
 * @note	 
 */
COLORREF    CSystemConfig::GetColorRgb(int iColorId)
{
    STD_ASSERT(iColorId >=  0);
    STD_ASSERT(iColorId < (int)m_lstColor.size());

    return m_lstColor[iColorId].crColour;
}

/**
 * @brief   パレット取得
 * @param   なし
 * @return	パレットへのポインタ
 * @note	 
 */
LOGPALETTE*  CSystemConfig::GetColorPalette()
{
     return (LOGPALETTE*)&m_Pallette;
}

/**
 * @brief   パレット初期化
 * @param   なし
 * @return	なし
 * @note	 
 */
void CSystemConfig::InitColorPalette()
{
	int nNumColours = GetMaxColors();
	STD_ASSERT(nNumColours <= MAX_COLOURS);
	if (nNumColours > MAX_COLOURS)
    {
		nNumColours = MAX_COLOURS;
    }


	LOGPALETTE* pLogPalette = (LOGPALETTE*) &m_Pallette;
	pLogPalette->palVersion    = 0x300;
	pLogPalette->palNumEntries = (WORD) nNumColours; 

	for (int i = 0; i < nNumColours; i++)
	{
		pLogPalette->palPalEntry[i].peRed   = GetRValue(m_lstColor[i].crColour);
		pLogPalette->palPalEntry[i].peGreen = GetGValue(m_lstColor[i].crColour);
		pLogPalette->palPalEntry[i].peBlue  = GetBValue(m_lstColor[i].crColour);
		pLogPalette->palPalEntry[i].peFlags = 0;
	}
}

/**
 * @brief   色登録
 * @param   [in] crRef          RGB値
 * @param   [in] strColorName   色名
 * @return	色ID値
 * @note	 
 */
int CSystemConfig::AddColor(COLORREF crRef, StdString strColorName)
{
    ColourTableEntry ColorName;

    ColorName.crColour = crRef;
    ColorName.strName  = strColorName;

    m_lstColor.push_back(ColorName);

    return static_cast<int>(m_lstColor.size());
}

CSystemConfig& CSystemConfig::operator = (const CSystemConfig & obj)
{
    strPathData     = obj.strPathData;
    m_strLibName    = obj.m_strLibName;
    strPathExtIo    = obj.strPathExtIo;
    iInitIoSize     = obj.iInitIoSize;
    strStdScript    = obj.strStdScript;
    strExpScript    = obj.strExpScript;


    for(int iCnt = 0; iCnt < MAX_PROJCT_LIST; iCnt++)
    {
        strProjectList[iCnt]    = obj.strProjectList[iCnt];
    }

    strDefaultPartsName         = obj.strDefaultPartsName;
    strDefaultFieldName         = obj.strDefaultFieldName;
    strDefaultReferenceName     = obj.strDefaultReferenceName;
    strDefaultMainName      = obj.strDefaultMainName;
    //iMaxExecGroup           = obj.iMaxExecGroup;
    iIoBufferSize           = obj.iIoBufferSize;

    return *this;
}

//======================================
//======================================
//======================================
/**
 *  @brief   コンストラクター
 *  @param   なし
 *  @retval  なし
 *  @note
 */
CSystemPropertyGrid::CSystemPropertyGrid()
{

}

/**
 *  @brief   初期設定
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CSystemPropertyGrid::Setup()
{

    EnableHeaderCtrl();
    EnableDescriptionArea();
    SetVSDotNetLook(true/*m_bDotNetLook*/);
    MarkModifiedProperties(true/*m_bMarkChanged*/);
    SetAlphabeticMode(false/*!m_bPropListCategorized*/);
    SetShowDragContext(true/*m_bShowDragContext*/);

    std::auto_ptr<CMFCPropertyGridProperty> 
        apDrawConfig(new CMFCPropertyGridProperty(_T("描画設定")));

    //
    CMFCPropertyGridProperty* pGroupDisp = 
        new CMFCPropertyGridProperty(_T("描画設定Sub1"));
	apDrawConfig->AddSubItem(pGroupDisp);


    //===============
    //レイヤー最大数
    /*
    _SetPorperty(pGroupDisp, &m_Cfg.iLayerMax, E_VAL_INT,
                             _T("レイヤー最大数"),
                             _T("レイヤー最大数の説明"),
                             0);
    */

    
	AddProperty(apDrawConfig.release());


    // BOOL値



    //===============
	//AddProperty(apGroupEditor.release());

}

/**
 *  @brief   データ設定
 *  @param   [in] setting 設定値
 *  @retval  なし
 *  @note
 */
void CSystemPropertyGrid::SetData(const CSystemConfig& setting)
{
    m_Cfg = setting;
}

/**
 *  @brief   データ取得
 *  @param   [out]  pSetting 取得データ
 *  @retval  なし
 *  @note
 */
void CSystemPropertyGrid::GetData(CSystemConfig* pSetting)
{
    *pSetting = m_Cfg;
}
