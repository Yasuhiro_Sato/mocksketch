/**
 * @brief			CSystemConfigヘッダーファイル
 * @file			CSystemConfig.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef __SYSTEM_CONFIG_H__
#define __SYSTEM_CONFIG_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CConfig.h"
#include "CEditorConfig.h"

/*---------------------------------------------------*/
/*  Classes                                          */
/*---------------------------------------------------*/
class CSystemString;

/**
 * @class   CSystemConfig
 * @brief                        
 */
class CSystemConfig : public CConfig
{
    static const int MAX_COLOURS = 100;
    static const int MAX_PROJCT_LIST = 10;
    //!< 色指定
    struct ColourTableEntry
    {
        COLORREF    crColour;
        StdString   strName;
    };

    //!< パレット
	struct PALETTE
	{
		LOGPALETTE    LogPalette;
		PALETTEENTRY  PalEntry[MAX_COLOURS];
	};

    //ファイル名
    StdPath  m_pathFileName;

public:


    CSystemConfig();
    ~CSystemConfig();

    //----------------------
    // 色指定
    //----------------------
    LOGPALETTE*  GetColorPalette();

    int         GetMaxColors();
    StdString   GetColorName(int iColorId);
    COLORREF    GetColorRgb(int iColorId);
    //----------------------

    //!< ディフォルトスクリプト読み込み
    void LoadDefaultScript();

    //!< 初期化
    void Init(const CSystemString* pSysStr);

    //!< XML読み込み
    virtual void LoadXml(StdXmlArchiveIn* pXml);

    //!< XML書き込み
    virtual void SaveXml(StdXmlArchiveOut* pXml);

    //!< 読み込み
    virtual void Load();

    //!< 書き込み
    virtual void Save();

    //!< 初期値設定
    void SetDefault();

    //!< ライブラリ名設定
    bool SetLibraryName(StdString strName);

    //!<  ライブラリ名取得
    StdString GetLibraryName() const;

    //!<  ライブラリパス取得
    StdPath GetLibraryPath() const;

    //!< ライブラリパス生成
    bool CreateLibraryPath();

    CSystemConfig& operator = (const CSystemConfig & obj);

protected:

    //!< ライブラリ名
    StdString                        m_strLibName;

public:
    //

    //!< ユーザデータディレクトリパス
    //!< 初回以外は strProjectList[0]と同じ
    StdString                        strPathData;


    //!< スクリプトアドインパス
    StdString                        strPathScriptAddins;

    //!< コマンドスクリプトライブラリディレクトリパス
    StdString                        strPathCommandLib;

    //!< 外部IOアドインディレクトリパス
    StdString                        strPathExtIo;

    //!< 初期IOアドレスサイズ
    int                              iInitIoSize;
    
    //!< 標準スクリプト
    std::string                      strStdScript;

    //!< 拡張スクリプト
    std::string                      strExpScript;

    //!< プロジェクトディレクトリ
    StdString                        strProjectList[MAX_PROJCT_LIST];

    //!< ディフォルト部品名
    StdString                        strDefaultPartsName;

    //!< ディフォルト参照名
    StdString                        strDefaultReferenceName;

    //!< ディフォルトフィールド名
    StdString                        strDefaultFieldName;



    //!< 先頭部品名
    StdString                        strDefaultMainName;

    //!< 実行グループ時間
    DWORD                           dwThreadGroup01Time;
    DWORD                           dwThreadGroup02Time;
    DWORD                           dwThreadGroup03Time;
    DWORD                           dwThreadGroup04Time;
    DWORD                           dwThreadGroup05Time;

    DWORD                           dwThreadField01Time;
    DWORD                           dwThreadField02Time;
    DWORD                           dwThreadField03Time;
    DWORD                           dwThreadField04Time;
    DWORD                           dwThreadField05Time;

    DWORD                           dwThreadDispTime;
    DWORD                           dwThreadPaneTime;

    DWORD                           dwThreadIo01Time;
    DWORD                           dwThreadIo02Time;
    DWORD                           dwThreadIo03Time;

    //タイマー設定値
    DWORD                           dwTimerProperty;    //プロパティウインドウ更新間隔

    //!< IOバッファ初期値
    int                             iIoBufferSize;

private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT

            SERIALIZATION_BOTH("DataPath"      , strPathData);
            SERIALIZATION_BOTH("LibName"       , m_strLibName);
            SERIALIZATION_BOTH("InitIoSize"    , iInitIoSize);
            SERIALIZATION_BOTH("ProjecttList"  , strProjectList);
            SERIALIZATION_BOTH("DefaultPartsName"       , strDefaultPartsName);
            SERIALIZATION_BOTH("DefaultReferenceName"   , strDefaultReferenceName);
            SERIALIZATION_BOTH("DefaultFieldName"       , strDefaultFieldName);
            SERIALIZATION_BOTH("DefaultMainName"   , strDefaultMainName);
            //SERIALIZATION_BOTH("MaxExecGroup"      , iMaxExecGroup);
            SERIALIZATION_BOTH("IoBufferSize"      , iIoBufferSize);

            SERIALIZATION_BOTH("ExecTimeOfThreadGroup01", dwThreadGroup01Time);
            SERIALIZATION_BOTH("ExecTimeOfThreadGroup02", dwThreadGroup02Time);
            SERIALIZATION_BOTH("ExecTimeOfThreadGroup03", dwThreadGroup03Time);
            SERIALIZATION_BOTH("ExecTimeOfThreadGroup04", dwThreadGroup04Time);
            SERIALIZATION_BOTH("ExecTimeOfThreadGroup05", dwThreadGroup05Time);

            SERIALIZATION_BOTH("ExecTimeOfThreadField01", dwThreadField01Time);
            SERIALIZATION_BOTH("ExecTimeOfThreadField02", dwThreadField02Time);
            SERIALIZATION_BOTH("ExecTimeOfThreadField03", dwThreadField03Time);
            SERIALIZATION_BOTH("ExecTimeOfThreadField04", dwThreadField04Time);
            SERIALIZATION_BOTH("ExecTimeOfThreadField05", dwThreadField05Time);

            SERIALIZATION_BOTH("ExecTimeOfThreadDisp", dwThreadDispTime);
            SERIALIZATION_BOTH("ExecTimeOfThreadPane", dwThreadPaneTime);

            SERIALIZATION_BOTH("ExecTimeOfThreadIo01", dwThreadIo01Time);
            SERIALIZATION_BOTH("ExecTimeOfThreadIo02", dwThreadIo02Time);
            SERIALIZATION_BOTH("ExecTimeOfThreadIo03", dwThreadIo03Time);

            SERIALIZATION_BOTH("TimeerPropertyWindow", dwTimerProperty);
            


            
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }

private:
    //!< 色設定
    int AddColor(COLORREF crColor, StdString strName);

    //!< カラーパレット初期化
    void InitColorPalette();

private:
    //!<色
    std::vector<ColourTableEntry>   m_lstColor;

    PALETTE                         m_Pallette;


};

//=========================
/**
 * @class   CSystemPropertyGrid
 * @brief   システム設定 プロパティグリッド               
 */
class CSystemPropertyGrid : public CConfigPropertyGrid
{
public:
    //!<コンストラクタ
    CSystemPropertyGrid();

    //!<デストラクタ
    virtual ~CSystemPropertyGrid(){;}

    //!<初期設定
    virtual void Setup();

    //!< データ設定
    void SetData(const CSystemConfig& setting);
    
    //!< データ取得
    void GetData(CSystemConfig* pSetting);

protected:
    mutable CSystemConfig  m_Cfg;
};

#endif __SYSTEM_CONFIG_H__
