/**
 * @brief			CSystemFile実装ファイル
 * @file			CSystemFile.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "CSystem.h"
#include "CSystemFile.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifndef _CONSOLE
#ifdef _DEBUG
#define new DEBUG_NEW
#endif
#endif

/*---------------------------------------------------*/
/*  Static                                           */
/*---------------------------------------------------*/


/**
 * @brief   コンストラクタ
 * @param   なし
 * @return	なし
 * @note	 
 */
CSystemFile::CSystemFile():
m_iMruFileMax   (3),
m_iMruPorjectMax(3)
{
    m_pathFileName = _T("SystemFile.xml");
}


/**
 * @brief   デストラクタ
 * @param   なし
 * @return	なし
 * @note	 
 */
CSystemFile::~CSystemFile()
{
}

/**
 * @brief   初期化
 * @param   なし 
 * @return	なし
 * @note	起動は一回のみ 
 */
void CSystemFile::Init()
{

}

/**
 * @brief   ディフォルト値設定
 * @param   なし
 * @return	なし
 * @note	 
 */
void CSystemFile::SetDefault()
{
}

/**
 * @brief   XML読み込み
 * @param   [in] pXml 
 * @return	なし
 * @note	 
 */
void CSystemFile::LoadXml(StdXmlArchiveIn* pXml)
{
    *pXml >> boost::serialization::make_nvp("Root", *this);
}

/**
 * @brief   XML書き込み
 * @param   [in] pXml
 * @return	なし
 * @note	 
 */
void CSystemFile::SaveXml(StdXmlArchiveOut* pXml)
{
    *pXml << boost::serialization::make_nvp("Root", *this);
}


/**
 * @brief   ファイル読み込み
 * @param   なし
 * @return	なし
 * @note	 
 */
void CSystemFile::Load()
{

    StdPath  pathLoad = CSystem::GetSystemPath();
    pathLoad /= m_pathFileName;
    CConfig::Load(pathLoad);
}

/**
 * @brief   ファイル書き込み
 * @param   なし          
 * @retval  なし
 * @note    
 */
void CSystemFile::Save()
{
    StdPath  pathSave =     CSystem::GetSystemPath();
    pathSave /= m_pathFileName;
    CConfig::Save(pathSave);
}

/**
 * @brief   MRUファイル追加
 * @param   [in] strFileName ファイル名
 * @retval  なし
 * @note    
 */
void CSystemFile::AddMruFile(StdString strFileName)
{

    std::deque<StdString>::iterator iteFile;

    iteFile = std::find( m_lstMruFile.begin(),
                         m_lstMruFile.end(),
                         strFileName);
    if (iteFile != m_lstMruFile.end())
    {
        m_lstMruFile.erase(iteFile);
    }

    m_lstMruFile.push_front(strFileName);

    if (GetMruFileNum() > m_iMruFileMax)
    {
        m_lstMruFile.pop_back();
    }
    Save();
}

/**
 * @brief   MRUプロジェクトファイル追加
 * @param   [in] strFileName ファイル名
 * @retval  なし
 * @note    
 */
void CSystemFile::AddMruProject(StdString strFileName)
{

    std::deque<StdString>::iterator iteFile;

    iteFile = std::find( m_lstMruProject.begin(),
                         m_lstMruProject.end(),
                         strFileName);
    if (iteFile != m_lstMruProject.end())
    {
        m_lstMruProject.erase(iteFile);
    }

    m_lstMruProject.push_front(strFileName);

    if (GetMruProjectNum() > m_iMruPorjectMax)
    {
        m_lstMruProject.pop_back();
    }
    Save();
}


/**
 * @brief   MRUファイル数取得
 * @param   なし
 * @retval  MRUファイル数
 * @note    
 */
int CSystemFile::GetMruFileNum()
{
    return static_cast<int>(m_lstMruFile.size());
}

/**
 * @brief   MRUプロジェクトファイル数取得
 * @param   なし
 * @retval  MRUプロジェクトファイル数
 * @note    
 */
int CSystemFile::GetMruProjectNum()
{
    return static_cast<int>(m_lstMruProject.size());
}


/**
 * @brief   MRUファイル最大数取得
 * @param   なし
 * @retval  MRUファイル最大数
 * @note    
 */
int CSystemFile::GetMruFileMax()
{
    return m_iMruFileMax;
}

/**
 * @brief   MRUプロジェクト最大数取得
 * @param   なし
 * @retval  MRUプロジェクト最大数
 * @note    
 */
int CSystemFile::GetMruProjectMax()
{
    return m_iMruPorjectMax;
}

/**
 * @brief   MRUファイル最大数設定
 * @param   [in] iNum  MRUファイル最大数
 * @retval  なし
 * @note    
 */
void CSystemFile::SetMruFileMax(int iNum)
{
    if (iNum < 1)
    {
        m_iMruFileMax = 1;
    }
    else if (iNum > 20)
    {
        m_iMruFileMax = 20;
    }

    int iDiff = GetMruFileNum() - iNum;
    if (iDiff > 0)
    {
        for (int iCnt = 0; iCnt < iDiff; iCnt++)
        {
            m_lstMruFile.pop_back();
        }
    }
    m_iMruFileMax = iNum;
}

/**
 * @brief   MRUプロジェクト最大数設定
 * @param   [in] iNum  MRUプロジェクト最大数
 * @retval  なし
 * @note    
 */
void CSystemFile::SetMruProjectMax(int iNum)
{
    if (iNum < 1)
    {
        m_iMruPorjectMax = 1;
    }
    else if (iNum > 20)
    {
        m_iMruPorjectMax = 20;
    }

    int iDiff = GetMruProjectNum() - iNum;
    if (iDiff > 0)
    {
        for (int iCnt = 0; iCnt < iDiff; iCnt++)
        {
            m_lstMruProject.pop_back();
        }
    }

    m_iMruPorjectMax = iNum;
}

/**
 * @brief   MRUファイル名取得
 * @param   [in] iNum ファイル番号
 * @retval  MRUファイル名
 * @note
 */
StdString CSystemFile::GetMruFile(int iNum)
{
    if (iNum < 0)
    {
        return _T("");
    }
    else if (iNum >= GetMruFileNum())
    {
        return _T("");
    }

    return m_lstMruFile[iNum];
}

/**
 * @brief   MRUプロジェクト名取得
 * @param   [in] iNum ファイル番号
 * @retval  MRUプロジェクト名
 * @note    
 */
StdString CSystemFile::GetMruProject(int iNum)
{
    if (iNum < 0)
    {
        return _T("");
    }
    else if (iNum >= GetMruProjectNum())
    {
        return _T("");
    }

    return m_lstMruProject[iNum];
}


//*******************************
//             TEST
//*******************************
#ifdef _DEBUG

//----------------
// テスト
//----------------
void TEST_CSystemFile()
{


    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());


    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));

}
#endif //_DEBUG