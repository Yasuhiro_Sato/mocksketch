/**
 * @brief			CSystemFileヘッダーファイル
 * @file			CSystemFile.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef __SYSTEM_FILE_H__
#define __SYSTEM_FILE_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CConfig.h"

/*---------------------------------------------------*/
/*  Classes                                          */
/*---------------------------------------------------*/
class CElementDef;
class CObjectDef;
class CProjectCtrl;

/**
 * @class   CSystemFile
 * @brief   最初に読み込みを行う設定
 *          (文字列が設定されていない)
 */
class CSystemFile : public CConfig
{
    friend void TEST_CSystemFile();

    //ファイル名
    StdPath  m_pathFileName;


public:
    CSystemFile();
    ~CSystemFile();

    //!< 初期化
    void Init();

    //!< XML読み込み
    virtual void LoadXml(StdXmlArchiveIn* pXml);

    //!< XML書き込み
    virtual void SaveXml(StdXmlArchiveOut* pXml);

    //!< 読み込み
    virtual void Load();

    //!< 書き込み
    virtual void Save();

    //!< 初期値設定
    void SetDefault();

    //!< MRUファイル追加
    void AddMruFile(StdString strFileName);
    
    //!< MRUプロジェクトファイル追加
    void AddMruProject(StdString strFileName);

    //!< MRUファイル数取得
    int GetMruFileNum();
    
    //!< MRUプロジェクトファイル数取得
    int GetMruProjectNum();

    //!< MRUファイル最大数取得
    int GetMruFileMax();
    
    //!< MRUプロジェクト最大数取得
    int GetMruProjectMax();

    //!< MRUファイル最大数設定
    void SetMruFileMax(int iNum);
    
    //!< MRUプロジェクト最大数設定
    void SetMruProjectMax(int iNum);

    //!< MRUファイル名取得
    StdString GetMruFile(int iNum);
    
    //!< MRUプロジェクト名取得
    StdString GetMruProject(int iNum);

protected:

    int m_iMruFileMax;

    int m_iMruPorjectMax;

    std::deque<StdString>      m_lstMruFile;

    std::deque<StdString>      m_lstMruProject;



private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
            SERIALIZATION_BOTH("MruFileMax"      , m_iMruFileMax);
            SERIALIZATION_BOTH("MruProjectMax"   , m_iMruPorjectMax);
            SERIALIZATION_BOTH("MruFileList"     , m_lstMruFile);
            SERIALIZATION_BOTH("MruProjectList"  , m_lstMruProject);

        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )

    }



};

#endif //__SYSTEM_LIBRARY_H__
