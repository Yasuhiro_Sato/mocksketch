/**
 * @brief			CSystemInit実装ファイル
 * @file			CSystemInit.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "CSystem.h"
#include "CSystemInit.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifndef _CONSOLE
#ifdef _DEBUG
#define new DEBUG_NEW
#endif
#endif

/*---------------------------------------------------*/
/*  Static                                           */
/*---------------------------------------------------*/
StdString CSystemInit::ms_strLocal = _T("jp");
bool CSystemInit::ms_bTest = true;


/**
 * @brief   コンストラクタ
 * @param   なし
 * @return	なし
 * @note	 
 */
CSystemInit::CSystemInit()
{
    m_pathFileName = _T("SystemInit.xml");
}


/**
 * @brief   デストラクタ
 * @param   なし
 * @return	なし
 * @note	 
 */
CSystemInit::~CSystemInit()
{
}

/**
 * @brief   初期化
 * @param   [in]  pSysStr システム文字列へのポインタ
 * @return	なし
 * @note	起動は一回のみ 
 */
void CSystemInit::Init()
{

}

/**
 * @brief   ディフォルト値設定
 * @param   なし
 * @return	なし
 * @note	 
 */
void CSystemInit::SetDefault()
{
}

/**
 * @brief   XML読み込み
 * @param   [in] pXml 
 * @return	なし
 * @note	 
 */
void CSystemInit::LoadXml(StdXmlArchiveIn* pXml)
{
    *pXml >> boost::serialization::make_nvp("Root", *this);
}

/**
 * @brief   XML書き込み
 * @param   [in] pXml
 * @return	なし
 * @note	 
 */
void CSystemInit::SaveXml(StdXmlArchiveOut* pXml)
{
    *pXml << boost::serialization::make_nvp("Root", *this);
}


/**
 * @brief   ファイル読み込み
 * @param   なし
 * @return	なし
 * @note	 
 */
void CSystemInit::Load()
{

    StdPath  pathLoad = CSystem::GetSystemPath();
    pathLoad /= m_pathFileName;
    CConfig::Load2(pathLoad);
}

/**
 * @brief   ファイル書き込み
 * @param   なし          
 * @retval  なし
 * @note
 */
void CSystemInit::Save()
{
    StdPath  pathSave =     CSystem::GetSystemPath();
    pathSave /= m_pathFileName;
    CConfig::Save2(pathSave);
}


//*******************************
//             TEST
//*******************************
#ifdef _DEBUG

//----------------
// テスト
//----------------
void TEST_CSystemInit()
{


    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());


    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));

}
#endif //_DEBUG