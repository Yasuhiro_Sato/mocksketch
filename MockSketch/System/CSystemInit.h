/**
 * @brief			CSystemInitヘッダーファイル
 * @file			CSystemInit.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef __SYSTEM_INIT_H__
#define __SYSTEM_INIT_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CConfig.h"

/*---------------------------------------------------*/
/*  Classes                                          */
/*---------------------------------------------------*/
class CElementDef;
class CObjectDef;
class CProjectCtrl;

/**
 * @class   CSystemInit
 * @brief   最初に読み込みを行う設定
 *          (文字列が設定されていない)
 */
class CSystemInit : public CConfig
{
    friend void TEST_CSystemInit();

    //ファイル名
    StdPath  m_pathFileName;


public:
    CSystemInit();
    ~CSystemInit();

    //!< 初期化
    void Init();

    //!< XML読み込み
    virtual void LoadXml(StdXmlArchiveIn* pXml);

    //!< XML書き込み
    virtual void SaveXml(StdXmlArchiveOut* pXml);

    //!< 読み込み
    virtual void Load();

    //!< 書き込み
    virtual void Save();

    //!< 初期値設定
    void SetDefault();

    static StdString GetLocal(){return ms_strLocal;}

    static void SetLocal(StdString str){ms_strLocal = str;}

    static bool GetTest(){return ms_bTest;}

    static void SetTest(bool b){ms_bTest = b;}

protected:
    //!< ロケール
    static StdString  ms_strLocal;

    //!< テスト
    static bool ms_bTest;
    


private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        namespace ser = boost::serialization;
        std::string  s;
        try
        {
           s = "Local";ar & ser::make_nvp( s.c_str() , ms_strLocal);
        }
        catch(MockException& e)
        {
            throw e;
        }
        catch(...)
        {
            StdStringStream strm;
            StdString str = CUtil::CharToString(s.c_str());

            int iErrorNo;
            if (Archive::is_loading::value)
            {
                iErrorNo = e_file_read;
                strm << StdFormat(_T("File read error %s:%s"))
                        % DB_FUNC_NAME
                        % str.c_str();
            }
            else
            {
                iErrorNo = e_file_write;
                strm << StdFormat(_T("File write error %s:%s"))
                        % DB_FUNC_NAME
                        % str.c_str();
            }
            throw MockException(iErrorNo, strm.str().c_str());
        }
    }



};

#endif //__SYSTEM_LIBRARY_H__
