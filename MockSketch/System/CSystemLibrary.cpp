/**
 * @brief			CSystemLibrary実装ファイル
 * @file			CSystemLibrary.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include <DbgHelp.h>
#include "./CSystem.h"
#include "./CSystemLibrary.h"
#include "DefinitionObject/CElementDef.h"
#include "Utility/CUtility.h"
#include "Utility/TreeData.h"
#include "DefinitionObject/ObjectDef.h"

#include "ScriptAddin.h"


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifndef _CONSOLE
#ifdef _DEBUG
#define new DEBUG_NEW
#endif
#endif

/**
 * @brief   コンストラクタ
 * @param   なし
 * @return	なし
 * @note	 
 */
CSystemLibrary::CSystemLibrary()
{
    m_pathFileName = _T("SystemLibrary.xml");
}


/**
 * @brief   デストラクタ
 * @param   なし
 * @return	なし
 * @note	 
 */
CSystemLibrary::~CSystemLibrary()
{
}

/**
 * @brief   初期化
 * @param   [in]  pSysStr システム文字列へのポインタ
 * @return	なし
 * @note	起動は一回のみ 
 */
void CSystemLibrary::Init()
{

}

/**
 * @brief   ディフォルト値設定
 * @param   なし
 * @return	なし
 * @note	 
 */
void CSystemLibrary::SetDefault()
{
}

/**
 * @brief   XML読み込み
 * @param   [in] pXml 
 * @return	なし
 * @note	 
 */
void CSystemLibrary::LoadXml(StdXmlArchiveIn* pXml)
{
    *pXml >> boost::serialization::make_nvp("Root", *this);
}

/**
 * @brief   XML書き込み
 * @param   [in] pXml
 * @return	なし
 * @note	 
 */
void CSystemLibrary::SaveXml(StdXmlArchiveOut* pXml)
{
    *pXml << boost::serialization::make_nvp("Root", *this);
}

/**
 * @brief   ファイル読み込み
 * @param   なし
 * @return	なし
 * @note	 
 */
void CSystemLibrary::Load()
{
    StdPath  pathLoad = CSystem::GetSystemPath();
    pathLoad /= m_pathFileName;

    CConfig::Load(pathLoad);
}

/**
 * @brief   ファイル書き込み
 * @param   なし          
 * @retval  なし
 * @note    普通は使わない
 */
void CSystemLibrary::Save()
{
    StdPath  pathSave =     CSystem::GetSystemPath();
    pathSave /= m_pathFileName;

    CConfig::Save(pathSave);
}

/**
 * @brief   外部IOアドイン読み込み
 * @param   なし          
 * @retval  true 成功
 * @note    
 */
/*
bool CSystemLibrary::RegisterExtrnalIo(StdString strName)
{
    bool bRet;
    StdPath  pathAddin = CSystem::GetSystemPath();
    pathAddin /= SYS_CONFIG->strPathExtIo;

    bRet = RegisterAddins(&m_mapExtIo, strName,  pathAddin, -0-);

    return bRet;
}
*/

/**
 * @brief   外部IOアドイン解除
 * @param   strName アドイン名
 * @retval  true 成功
 * @note    
 */
/*
bool CSystemLibrary::UnregisterExtrnalIo(StdString strName)
{
    bool bRet;
    bRet = UnregisterAddIns(&m_mapExtIo, strName);

    return bRet;
}
*/

/**
 * @brief   DLL読み込み
 * @param   [out] pData        登録データ
 * @param   [in]  pathFile     ファイルパス
 * @param   [in]  eType        アドイン種別
 * @retval  true 成功
 * @note    
 */
bool CSystemLibrary::_LoadDll(ADDIN_DATA* pData, StdPath pathFile, ADDIN_TYPE eType)
{
    HMODULE hAddin;

    StdString strPath = CUtil::CharToString(pathFile.string().c_str()); 
    hAddin = ::LoadLibrary(strPath.c_str());
    if (hAddin == NULL)
    {
        return false;
    }

    // DLL検査
    bool bRet = true;

    FUNC_GetName lpGetName;

    lpGetName = reinterpret_cast<FUNC_GetName>
                (::GetProcAddress(hAddin, "GetName"));
   
    if (lpGetName == NULL)
    {
        bRet = false;
    }


    if (bRet)
    {
        switch (eType)
        {
        case E_ADDIN_IO:
            //TODO:検査項目

            break;

        case E_ADDIN_AS:
            if(!::GetProcAddress(hAddin, "RegisterType")){bRet = false;}
            if(!::GetProcAddress(hAddin, "Register")){bRet = false;}
            break;

        default:
            bRet = false;
        }
    }

    if (!bRet)
    {
        ::FreeLibrary(hAddin);
        return false;
    }

    pData->strName     = lpGetName();
    pData->hModule     = hAddin; 
    pData->pathAddin   = pathFile; 

    return true;
}



/**
 * @brief   アドインデータ属性取得
 * @param   [out] pPathAddins  アドインマップ
 * @param   [out]  pMapAddins    アドイン名
 * @param   [out]  pListAddins アドインパス
 * @param   [in]  eType      アドイン種別
 * @retval  true 成功
 * @note
 */
bool CSystemLibrary::_GetAddinProperty(StdPath* pPathAddins,
                        std::map<StdString, ADDIN_DATA>** ppMapAddins,
                        std::vector<ADDIN_USE>**          ppListAddins,
                        ADDIN_TYPE eType)
{

    switch(eType)
    {
    case  E_ADDIN_IO:
        *ppMapAddins  = &m_mapExtIo;
        *ppListAddins = &m_lstIo;
        *pPathAddins = CSystem::GetSystemPath();
        *pPathAddins /= SYS_CONFIG->strPathExtIo;
        break;
    case E_ADDIN_AS:
        *ppMapAddins = &m_mapScriptAddins;
        *ppListAddins = &m_lstScriptAddins;
        *pPathAddins = CSystem::GetSystemPath();
        *pPathAddins /= SYS_CONFIG->strPathScriptAddins;
        break;
    default:
        return false;
    }

    return true;
}


/**
 * @brief   アドイン登録
 * @param   [out] pMapAddins  アドインマップ
 * @param   [in]  strName    アドイン名    拡張子なし
 * @param   [in]  pathAddins アドインパス
 * @param   [in]  eType      アドイン種別
 * @retval  true 成功
 * @note 
 */
bool CSystemLibrary::_RegisterAddins(StdString strFileName,
                                    StdPath pathAddins,
                                    std::map<StdString, ADDIN_DATA>* pMapAddins,
                                    std::vector<ADDIN_USE>*          pListAddins,
                                    ADDIN_TYPE eType)
{
    std::map<StdString, ADDIN_DATA>::iterator iteMap;
    std::vector<ADDIN_USE>::iterator iteUse;

    //再登録
    iteMap = pMapAddins->find(strFileName);
    if (iteMap != pMapAddins->end())
    {
        return false;
    }


    iteUse  = std::find_if( pListAddins->begin(), 
                             pListAddins->end(), 
                              GET_FIRST(ADDIN_USE)::strFileName == strFileName);


    if (iteUse != pListAddins->end())
    {
        if (!iteUse->bUse)
        {
            return false;
        }
    }
    
    ADDIN_DATA addinData;
    pathAddins /= strFileName;
    if (!_LoadDll(&addinData, pathAddins, eType))
    {
        return false;
    }

    std::pair<StdString, ADDIN_DATA> pairData;
    pairData.first  = strFileName;
    pairData.second = addinData;
    pMapAddins->insert(pairData); 

    if (iteUse == pListAddins->end())
    {
        ADDIN_USE use;
        use.strFileName = strFileName;
        use.bUse = true;
        pListAddins->push_back(use);
    }
    return true;
}


/**
 * @brief   アドイン拡張子取得
 * @retval  アドイン拡張子名
 * @note 
 */
StdString CSystemLibrary::_GetAddinExt(ADDIN_TYPE eType) const
{
    StdString strExt;

    switch (eType)
    {
    case E_ADDIN_IO:{strExt = _T("mai");break;}
    case E_ADDIN_AS:{strExt = _T("maa");break;}
    default:
        break;
    }
    return strExt;
}

/**
 * @brief   アドイン登録
 * @param   [out] pMapAddins  アドインマップ
 * @param   [in]  strName    アドイン)
 * @param   [in]  pathAddins アドインパス
 * @param   [in]  eType      アドイン種別
 * @retval  true 成功
 * @note    アドイン名 == "" で全アドイン登録
 */
bool CSystemLibrary::RegisterAddins(ADDIN_TYPE eType, StdString strFileName)
{

    StdPath pathAddins;
    std::map<StdString, ADDIN_DATA>* pMapAddins;
    std::vector<ADDIN_USE>*          pListAddins; 


    if(!_GetAddinProperty(&pathAddins,
                        &pMapAddins,
                        &pListAddins,
                        eType))
    {
        return false;
    }


    if (strFileName != _T(""))
    {
        return _RegisterAddins(strFileName,
                               pathAddins,
                               pMapAddins,
                               pListAddins,
                               eType);
    }

    std::map<StdString, ADDIN_DATA>::iterator iteMap;
    std::vector<ADDIN_USE>::iterator iteUse;


    WIN32_FIND_DATA fd;
    HANDLE hFind;
    
    StdPath pathSearch = pathAddins;
    StdPath pathFile;

    StdString strExt = _T("*.");

    strExt +=  _GetAddinExt(eType);

    pathSearch /= strExt;

    StdString stFile = CUtil::CharToString(pathSearch.string().c_str());
    hFind = FindFirstFile(stFile.c_str(), &fd);
    if(hFind==INVALID_HANDLE_VALUE)
    {
        return false;
    }

    
    ADDIN_DATA addinData;

    std::pair<StdString, ADDIN_DATA> pairData;
    do
    {
        _RegisterAddins(fd.cFileName,
                        pathAddins,
                        pMapAddins,
                        pListAddins,
                        eType);

    }
    while(::FindNextFile(hFind,&fd) != 0);

    FindClose(hFind);
    return true;
}


//!< 
/**
 * @brief   アドイン開放
 * @param   [out] pMapAddins  アドインマップ
 * @param   [in]  strName    アドイン)
 * @param   [in]  pathAddins アドインパス
 * @param   [in]  eType      アドイン種別
 * @retval  true 成功
 * @note    アドイン名 == "" で全アドイン登録
 */
bool CSystemLibrary::_UnRegisterAddins(
                    StdString strFleName,
                    std::map<StdString, ADDIN_DATA>* pMapAddins)
{
    std::map<StdString, ADDIN_DATA>::iterator iteMap;
    ADDIN_DATA* pAddinData;

    iteMap = pMapAddins->find(strFleName);
    if (iteMap == pMapAddins->end())
    {
        return false;
    }

    pAddinData = &iteMap->second;
    if (pAddinData->hModule == 0)
    {
        return false;
    }
    
    if(!::FreeLibrary(pAddinData->hModule))
    {
        pAddinData->hModule = 0;
        STD_DBG(_T("FreeLibrary Fail %s \n"), pAddinData->strName.c_str());
        return false;
    }
    pAddinData->hModule = 0;
    return true;
}


/**
 * @brief   アドイン解放
 * @param   [in] strName    アドイン名
 * @param   [in] pathAddins アドインパス
 * @param   [in] mapAddins  アドインマップ
 * @retval  true 成功
 * @note    アドイン名 == "" で全アドイン解放
 */
bool CSystemLibrary::UnregisterAddins(ADDIN_TYPE eType, 
                                      StdString strFileName)
{
    std::map<StdString, ADDIN_DATA>::iterator iteMap;

    StdPath pathAddins;
    std::map<StdString, ADDIN_DATA>* pMapAddins;
    std::vector<ADDIN_USE>*          pListAddins; 

    if(!_GetAddinProperty(&pathAddins,
                           &pMapAddins,
                           &pListAddins,
                          eType))
    {
        return false;
    }

    ADDIN_DATA* pAddinData;
    if (strFileName != _T(""))
    {
        return _UnRegisterAddins(strFileName,
                                 pMapAddins);
    }

    for( iteMap  = pMapAddins->begin();
         iteMap != pMapAddins->end();
         iteMap++)
    {
        pAddinData = &iteMap->second;
        if (pAddinData->hModule == 0)
        {
            continue;
        }

        if(!::FreeLibrary(pAddinData->hModule))
        {
            STD_DBG(_T("FreeLibrary Fail %s \n"), pAddinData->strName.c_str());
        }
        pAddinData->hModule = 0;
    }
    return true;
}


/**
 *  @brief  ライブラリデータ取得
 *  @param  なし
 *  @retval 定義オブジェクト
 *  @note   
 */
/*
CProjectCtrl*   CSystemLibrary::GetLibCtrl()
{
    return m_psLib;
}
*/

//*******************************
//             TEST
//*******************************
#ifdef _DEBUG


//----------------
// DLL実行クラス
//----------------
class CTestLib
{
    //名称取得
    typedef LPCTSTR (__stdcall *F_GetName)();
    typedef void    (__stdcall *F_SetName)(LPCTSTR);


public:
    CTestLib():m_hAddin(0),m_GetName(0),m_SetName(0){;}
    virtual ~CTestLib(){;}

    //!< DLL設定
    bool SetDll(HMODULE hModule)
    {
        if (m_hAddin != 0)
        {
            return false;
        }

        m_hAddin = hModule; 
        m_GetName = reinterpret_cast<F_GetName>(::GetProcAddress(m_hAddin, "GetName"));
        m_SetName = reinterpret_cast<F_SetName>(::GetProcAddress(m_hAddin, "SetName"));

        STD_ASSERT(m_GetName != 0);
        STD_ASSERT(m_SetName != 0);


        return true;
    }

    //!< 名称取得
    StdString GetName()
    {
        StdString strRet;
        if (m_GetName == 0)
        {
            return strRet;
        }   

        strRet = m_GetName();

        return strRet; 
    }


protected:
    HMODULE m_hAddin;

    F_GetName m_GetName;
    F_SetName m_SetName;
};




//----------------
// テスト
//----------------
void TEST_CSystemLibrary()
{


    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());
#if 0
    CSystemLibrary SysLib;

    SysLib.RegisterAddins(CSystemLibrary::E_ADDIN_AS);

    std::map<StdString, CSystemLibrary::ADDIN_DATA>::iterator iteMap;

    MockScript::FUNC_RegisterType retgsterType;
    MockScript::FUNC_Register     retgster;

    ID_VALFUNC_MAP mapFunc;
    asIScriptEngine* pEngine = NULL;

    int iRet;
    HMODULE hModule;
    for(iteMap  = SysLib.m_mapScriptAddins.begin();
        iteMap != SysLib.m_mapScriptAddins.end();
        iteMap++)
    {
        hModule = iteMap->second.hModule;

        retgster     = reinterpret_cast<FUNC_RegisterType>(::GetProcAddress(hModule, "RegisterType"));
        retgsterType = reinterpret_cast<FUNC_Register>(::GetProcAddress(hModule, "Register"));


        iRet = retgster(pEngine);
        iRet = retgsterType(pEngine, mapFunc);

    }


    UnregisterAddins();
#endif

    //StdPath  pathAddin = CSystem::GetSystemPath();
    //pathAddin /= _T("ExtIo");



    //------------------------------------
    // 前提条件
    //------------------------------------
    //TODO: C++インターフェイスによるDLLを作成できるようにする
    //現状未実装につき再テストを行うこと
    //動的クラスロードによる簡易プラグインアーキテクチャーについて
    // http://cheesy.dip.jp/tutorialog/archives/7
/*
    bRet = SysLib.RegisterAddIns(&mapTest, _T(""), 
                                  pathAddin );


    STD_ASSERT(bRet);

    std::map<StdString, CSystemLibrary::ADDIN_DATA>::iterator iteMap;

    //----------------
    // アドイン登録
    //---------------
    int nCnt = 1;
    StdString strSetName;
    std::vector<CTestLib*> lstLib;
    for(iteMap  = mapTest.begin();
        iteMap != mapTest.end();
        iteMap++)
    {
        CTestLib* pLib = new  CTestLib;
        lstLib.push_back(pLib);
        pLib->SetDll(iteMap->second.hModule);
        
        DB_PRINT(_T("   >>%s\n"), pLib->GetName().c_str());
        nCnt++;
    }
    //---------------
    
    bRet = SysLib.UnregisterAddIns(&mapTest, _T("MockDevice2") );
    STD_ASSERT(bRet);

    //モジュールの削除を確認
    iteMap = mapTest.find(_T("MockDevice2"));
    STD_ASSERT( iteMap->second.hModule == 0);


    //モジュールを再登録
    iteMap = mapTest.find(_T("MockDevice2"));
    bRet = SysLib.RegisterAddIns(&mapTest, _T("MockDevice2"),  iteMap->second.pathAddin);
    STD_ASSERT(bRet);

    //モジュールの登録を確認
    iteMap = mapTest.find(_T("MockDevice2"));
    STD_ASSERT( iteMap->second.hModule != 0);



    //登録したクラスはどうする？
    bRet = SysLib.UnregisterAddIns(&mapTest, _T("") );
    STD_ASSERT(bRet);


    for(iteMap  = mapTest.begin();
        iteMap != mapTest.end();
        iteMap++)
    {
        STD_ASSERT(iteMap->second.hModule == 0);
    }
    //---------------


    foreach(CTestLib* pLib, lstLib)
    {
        delete pLib;
    }
*/
    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));

}
#endif //_DEBUG