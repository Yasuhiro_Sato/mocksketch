/**
 * @brief			CSystemLibraryヘッダーファイル
 * @file			CSystemLibrary.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef __SYSTEM_LIBRARY_H__
#define __SYSTEM_LIBRARY_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CConfig.h"
#include "Utility/TreeData.h"

/*---------------------------------------------------*/
/*  Classes                                          */
/*---------------------------------------------------*/
class CElementDef;
class CObjectDef;
class CProjectCtrl;


/**
 * @class   CSystemLibrary
 * @brief                        
 */
class CSystemLibrary : public CConfig
{
    friend void TEST_CSystemLibrary();

    //!< アドイン管理
    class ADDIN_USE
    {
    public:
        ADDIN_USE():bUse(false){;}
        virtual ~ADDIN_USE(){;}
     
    public:
        StdString   strFileName;    //!< ファイル名
        bool        bUse;           //!< 使用有無

    private:
        friend class boost::serialization::access;  
        template<class Archive>
        void serialize(Archive& ar, const unsigned int version)
        {
            SERIALIZATION_INIT
                SERIALIZATION_BOTH("FileName"     , strFileName);
                SERIALIZATION_BOTH("Use"      , bUse);
            MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
        }
    };

    //ファイル名
    StdPath  m_pathFileName;

    //名称取得
    typedef LPCTSTR (*FUNC_GetName)();

public:

    //!< アドインデータ
    struct ADDIN_DATA
    {
        HMODULE     hModule;    //!< DLLモジュールハンドル
        StdString   strName;    //!< モジュール名
        StdPath     pathAddin;  //!< DLLパス
    };

    //!< アドイン種別
    enum ADDIN_TYPE
    {
        E_ADDIN_NONE = 0,
        E_ADDIN_IO   = 1, //io定義
        E_ADDIN_AS   = 2, //AngerlSctipt関数定義
    };

public:


    CSystemLibrary();
    ~CSystemLibrary();

    //!< 初期化
    void Init();

    //!< XML読み込み
    virtual void LoadXml(StdXmlArchiveIn* pXml);

    //!< XML書き込み
    virtual void SaveXml(StdXmlArchiveOut* pXml);

    //!< 読み込み
    virtual void Load();

    //!< 書き込み
    virtual void Save();

    //!< 初期値設定
    void SetDefault();

    //!< 外部IOアドイン読み込み
    //bool RegisterExtrnalIo(StdString strName);

    //!< 外部IOアドイン解除
    //bool UnregisterExtrnalIo(StdString strName);

    //!< 定義オブジェクト取得
   // CObjectDef* GetObjectDef(boost::uuids::uuid uuidObject) const;

    //!< ライブラリデータ取得
    //CProjectCtrl*   GetLibCtrl();



    //!< 全アドイン登録
    bool RegisterAddins(ADDIN_TYPE eType, StdString strFileName = _T(""));

    //!< アドイン解放
    bool UnregisterAddins(ADDIN_TYPE eType, StdString strFileName = _T(""));

protected:

    bool _LoadDll(ADDIN_DATA* pData, StdPath pathFile, ADDIN_TYPE eType);


    //!< アドイン登録
    bool _RegisterAddins(StdString strFleName,
                        StdPath pathAddins,
                        std::map<StdString, ADDIN_DATA>* pMapAddins,
                        std::vector<ADDIN_USE>*          pListAddins,
                        ADDIN_TYPE eType);
    
    //!< アドイン開放
    bool _UnRegisterAddins(StdString strFleName,
                        std::map<StdString, ADDIN_DATA>* pMapAddins);

    //!< アドイン拡張子取得
    StdString _GetAddinExt(ADDIN_TYPE eType) const;

public:

    //!< 保存データ
    std::vector<ADDIN_USE> m_lstIo;

    std::vector<ADDIN_USE> m_lstScriptAddins;


    //!< --------------
    //!< 非保存データ
    //!< --------------

    //!< 外部IOアドイン
    std::map<StdString, ADDIN_DATA>  m_mapExtIo;

    //!< スクリプトアドイン
    std::map<StdString, ADDIN_DATA>  m_mapScriptAddins;

private:
    bool _GetAddinProperty(StdPath* pPathAddins,
                            std::map<StdString, ADDIN_DATA>** pMapAddins,
                            std::vector<ADDIN_USE>**          pListAddins,
                            ADDIN_TYPE eType);


private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
            SERIALIZATION_BOTH("Io"      , m_lstIo);
            SERIALIZATION_BOTH("Script"  , m_lstScriptAddins);
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }




};

#endif //__SYSTEM_LIBRARY_H__
