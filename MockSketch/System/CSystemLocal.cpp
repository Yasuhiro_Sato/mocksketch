/**
 * @brief			CSystemLocale実装ファイル
 * @file			CSystemLocale.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include <DbgHelp.h>
#include "CSystem.h"
#include "CSystemLocal.h"
#include "Utility/CUtility.h"
#include <boost/range/algorithm.hpp> //TODO:MockDefに移動
/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifndef _CONSOLE
#ifdef _DEBUG
#define new DEBUG_NEW
#endif
#endif


/*
/**
 * @brief   コンストラクタ
 * @param   なし
 * @return	なし
 * @note	 
 */
CSystemLocal::CSystemLocal():pSysInit(NULL)
{
    m_pathFileName = _T("SystemLocal.xml");
}

/**
 * @brief   デストラクタ
 * @param   なし
 * @return	なし
 * @note	 
 */
CSystemLocal::~CSystemLocal()
{
}

/**
 * @brief   初期化
 * @param   [in]  pSysStr システム文字列へのポインタ
 * @return	なし
 * @note	起動は一回のみ 
 */
void CSystemLocal::Init()
{
    typedef boost::bimaps::bimap<LCID, boost::bimaps::multiset_of<StdString>>::value_type  value_t;


    //同一言語をまとめた
    m_mapLang.insert(value_t(1078 ,_T("af")));
    m_mapLang.insert(value_t(14337,_T("ar")));
    m_mapLang.insert(value_t(15361,_T("ar")));
    m_mapLang.insert(value_t(5121 ,_T("ar")));
    m_mapLang.insert(value_t(3073 ,_T("ar")));
    m_mapLang.insert(value_t(2049 ,_T("ar")));
    m_mapLang.insert(value_t(11265,_T("ar")));
    m_mapLang.insert(value_t(13313,_T("ar")));
    m_mapLang.insert(value_t(12289,_T("ar")));
    m_mapLang.insert(value_t(4097 ,_T("ar")));
    m_mapLang.insert(value_t(6145 ,_T("ar")));
    m_mapLang.insert(value_t(8193 ,_T("ar")));
    m_mapLang.insert(value_t(16385,_T("ar")));
    m_mapLang.insert(value_t(1025 ,_T("ar")));
    m_mapLang.insert(value_t(10241,_T("ar")));
    m_mapLang.insert(value_t(7169 ,_T("ar")));
    m_mapLang.insert(value_t(9217 ,_T("ar")));
    m_mapLang.insert(value_t(1059 ,_T("be")));
    m_mapLang.insert(value_t(1026 ,_T("bg")));
    m_mapLang.insert(value_t(1027 ,_T("ca")));
    m_mapLang.insert(value_t(1029 ,_T("cs")));
    m_mapLang.insert(value_t(1030 ,_T("da")));
    m_mapLang.insert(value_t(1031 ,_T("de")));
    m_mapLang.insert(value_t(3079 ,_T("de")));
    m_mapLang.insert(value_t(2055 ,_T("de")));
    m_mapLang.insert(value_t(5127 ,_T("de")));
    m_mapLang.insert(value_t(4103 ,_T("de")));
    m_mapLang.insert(value_t(1032 ,_T("el")));
    m_mapLang.insert(value_t(3081 ,_T("en")));
    m_mapLang.insert(value_t(10249,_T("en")));
    m_mapLang.insert(value_t(4105 ,_T("en")));
    m_mapLang.insert(value_t(2057 ,_T("en")));
    m_mapLang.insert(value_t(6153 ,_T("en")));
    m_mapLang.insert(value_t(8201 ,_T("en")));
    m_mapLang.insert(value_t(5129 ,_T("en")));
    m_mapLang.insert(value_t(11273,_T("en")));
    m_mapLang.insert(value_t(1033 ,_T("en")));
    m_mapLang.insert(value_t(7177 ,_T("en")));
    m_mapLang.insert(value_t(1034 ,_T("es")));
    m_mapLang.insert(value_t(11274,_T("es")));
    m_mapLang.insert(value_t(16394,_T("es")));
    m_mapLang.insert(value_t(13322,_T("es")));
    m_mapLang.insert(value_t(9226 ,_T("es")));
    m_mapLang.insert(value_t(5130 ,_T("es")));
    m_mapLang.insert(value_t(7178 ,_T("es")));
    m_mapLang.insert(value_t(12298,_T("es")));
    m_mapLang.insert(value_t(4106 ,_T("es")));
    m_mapLang.insert(value_t(18442,_T("es")));
    m_mapLang.insert(value_t(2058 ,_T("es")));
    m_mapLang.insert(value_t(19466,_T("es")));
    m_mapLang.insert(value_t(6154 ,_T("es")));
    m_mapLang.insert(value_t(10250,_T("es")));
    m_mapLang.insert(value_t(20490,_T("es")));
    m_mapLang.insert(value_t(15370,_T("es")));
    m_mapLang.insert(value_t(17418,_T("es")));
    m_mapLang.insert(value_t(14346,_T("es")));
    m_mapLang.insert(value_t(8202 ,_T("es")));
    m_mapLang.insert(value_t(1061 ,_T("et")));
    m_mapLang.insert(value_t(1069 ,_T("eu")));
    m_mapLang.insert(value_t(1065 ,_T("fa")));
    m_mapLang.insert(value_t(1035 ,_T("fi")));
    m_mapLang.insert(value_t(1080 ,_T("fo")));
    m_mapLang.insert(value_t(1036 ,_T("fr")));
    m_mapLang.insert(value_t(2060 ,_T("fr")));
    m_mapLang.insert(value_t(3084 ,_T("fr")));
    m_mapLang.insert(value_t(4108 ,_T("fr")));
    m_mapLang.insert(value_t(5132 ,_T("fr")));
    m_mapLang.insert(value_t(1084 ,_T("gd")));
    m_mapLang.insert(value_t(1037 ,_T("he")));
    m_mapLang.insert(value_t(1081 ,_T("hi")));
    m_mapLang.insert(value_t(1050 ,_T("hr")));
    m_mapLang.insert(value_t(1038 ,_T("hu")));
    m_mapLang.insert(value_t(1057 ,_T("in")));
    m_mapLang.insert(value_t(1039 ,_T("is")));
    m_mapLang.insert(value_t(1040 ,_T("it")));
    m_mapLang.insert(value_t(2064 ,_T("it")));
    m_mapLang.insert(value_t(1041 ,_T("ja")));
    m_mapLang.insert(value_t(1085 ,_T("ji")));
    m_mapLang.insert(value_t(1042 ,_T("ko")));
    m_mapLang.insert(value_t(1063 ,_T("lt")));
    m_mapLang.insert(value_t(1062 ,_T("lv")));
    m_mapLang.insert(value_t(1071 ,_T("mk")));
    m_mapLang.insert(value_t(1086 ,_T("ms")));
    m_mapLang.insert(value_t(1082 ,_T("mt")));
    m_mapLang.insert(value_t(1043 ,_T("nl")));
    m_mapLang.insert(value_t(2067 ,_T("nl")));
    m_mapLang.insert(value_t(1044 ,_T("no")));
    m_mapLang.insert(value_t(1045 ,_T("pl")));
    m_mapLang.insert(value_t(2070 ,_T("pt")));
    m_mapLang.insert(value_t(1046 ,_T("pt")));
    m_mapLang.insert(value_t(1047 ,_T("rm")));
    m_mapLang.insert(value_t(1048 ,_T("ro")));
    m_mapLang.insert(value_t(2072 ,_T("ro")));
    m_mapLang.insert(value_t(1049 ,_T("ru")));
    m_mapLang.insert(value_t(2073 ,_T("ru")));
    m_mapLang.insert(value_t(1070 ,_T("sb")));
    m_mapLang.insert(value_t(1051 ,_T("sk")));
    m_mapLang.insert(value_t(1060 ,_T("sl")));
    m_mapLang.insert(value_t(1052 ,_T("sq")));
    m_mapLang.insert(value_t(3098 ,_T("sr")));
    m_mapLang.insert(value_t(1053 ,_T("sv")));
    m_mapLang.insert(value_t(2077 ,_T("sv")));
    m_mapLang.insert(value_t(1072 ,_T("sx")));
    m_mapLang.insert(value_t(1054 ,_T("th")));
    m_mapLang.insert(value_t(1074 ,_T("tn")));
    m_mapLang.insert(value_t(1055 ,_T("tr")));
    m_mapLang.insert(value_t(1073 ,_T("ts")));
    m_mapLang.insert(value_t(1058 ,_T("uk")));
    m_mapLang.insert(value_t(1056 ,_T("ur")));
    m_mapLang.insert(value_t(1066 ,_T("vi")));
    m_mapLang.insert(value_t(1076 ,_T("xh")));
    m_mapLang.insert(value_t(1077 ,_T("zu")));
    m_mapLang.insert(value_t(2052 ,_T("zh-cn")));
    m_mapLang.insert(value_t(3076 ,_T("zh-hk")));
    m_mapLang.insert(value_t(4100 ,_T("zh-sg")));
    m_mapLang.insert(value_t(1028 ,_T("zh-tw")));


    GetLocale();

}

/**
 * @brief   ディフォルト値設定
 * @param   なし
 * @return  なし
 * @note    
 */
void CSystemLocal::SetDefault()
{
    if ( CSystemInit::GetLocal() != _T("jp") )
    {
        m_mapLocale[_T("af")]    = _T("Afrikaans");
        m_mapLocale[_T("ar")]    = _T("Arabic");
        m_mapLocale[_T("be")]    = _T("Belarusian");
        m_mapLocale[_T("bg")]    = _T("Bulgarian");
        m_mapLocale[_T("ca")]    = _T("Catalan");
        m_mapLocale[_T("cs")]    = _T("Czech");
        m_mapLocale[_T("da")]    = _T("Danish");
        m_mapLocale[_T("de")]    = _T("German");
        m_mapLocale[_T("el")]    = _T("Greek");
        m_mapLocale[_T("en")]    = _T("English");
        m_mapLocale[_T("es")]    = _T("Spanish");
        m_mapLocale[_T("et")]    = _T("Estonian");
        m_mapLocale[_T("eu")]    = _T("Basque");
        m_mapLocale[_T("fa")]    = _T("Farsi");
        m_mapLocale[_T("fi")]    = _T("Finnish");
        m_mapLocale[_T("fo")]    = _T("Faroese");
        m_mapLocale[_T("fr")]    = _T("French");
        m_mapLocale[_T("gd")]    = _T("Scottish Gaelic");
        m_mapLocale[_T("he")]    = _T("Hebrew");
        m_mapLocale[_T("hi")]    = _T("Hindi");
        m_mapLocale[_T("hr")]    = _T("Croatian");
        m_mapLocale[_T("hu")]    = _T("Hungarian");
        m_mapLocale[_T("in")]    = _T("Indonesian");
        m_mapLocale[_T("is")]    = _T("Icelandic");
        m_mapLocale[_T("it")]    = _T("Italian");
        m_mapLocale[_T("ja")]    = _T("Japanese");
        m_mapLocale[_T("ji")]    = _T("Yiddish");
        m_mapLocale[_T("ko")]    = _T("Korean");
        m_mapLocale[_T("lt")]    = _T("Lithuanian");
        m_mapLocale[_T("lv")]    = _T("Latvian");
        m_mapLocale[_T("mk")]    = _T("FYRO Macedonian");
        m_mapLocale[_T("ms")]    = _T("Malay");
        m_mapLocale[_T("mt")]    = _T("Maltese");
        m_mapLocale[_T("nl")]    = _T("Dutch");
        m_mapLocale[_T("no")]    = _T("Norwegian");
        m_mapLocale[_T("pl")]    = _T("Polish");
        m_mapLocale[_T("pt")]    = _T("Portuguese");
        m_mapLocale[_T("rm")]    = _T("Rhaeto");
        m_mapLocale[_T("ro")]    = _T("Romanian");
        m_mapLocale[_T("ru")]    = _T("Russian");
        m_mapLocale[_T("sb")]    = _T("Sorbian");
        m_mapLocale[_T("sk")]    = _T("Slovak");
        m_mapLocale[_T("sl")]    = _T("Slovenian");
        m_mapLocale[_T("sq")]    = _T("Albanian");
        m_mapLocale[_T("sr")]    = _T("Serbian");
        m_mapLocale[_T("sv")]    = _T("Swedish");
        m_mapLocale[_T("sx")]    = _T("Sutu");
        m_mapLocale[_T("th")]    = _T("Thai");
        m_mapLocale[_T("tn")]    = _T("Tswana");
        m_mapLocale[_T("tr")]    = _T("Turkish");
        m_mapLocale[_T("ts")]    = _T("Tsonga");
        m_mapLocale[_T("uk")]    = _T("Ukrainian");
        m_mapLocale[_T("ur")]    = _T("Urdu");
        m_mapLocale[_T("vi")]    = _T("Vietnamese");
        m_mapLocale[_T("xh")]    = _T("Xhosa");
        m_mapLocale[_T("zh-cn")] = _T("Chinese - People's Republic of China");
        m_mapLocale[_T("zh-hk")] = _T("Chinese - Hong Kong SAR");
        m_mapLocale[_T("zh-sg")] = _T("Chinese - Singapore");
        m_mapLocale[_T("zh-tw")] = _T("Chinese - Taiwan");
        m_mapLocale[_T("zu")]    = _T("Zulu");
    }
    else
    {
        m_mapLocale[_T("af")]    = _T("アフリカーンス語");
        m_mapLocale[_T("ar")]    = _T("アラビア語");
        m_mapLocale[_T("be")]    = _T("ベラルーシ語");
        m_mapLocale[_T("bg")]    = _T("ブルガリア語");
        m_mapLocale[_T("ca")]    = _T("カタロニア語");
        m_mapLocale[_T("cs")]    = _T("チェコ語");
        m_mapLocale[_T("da")]    = _T("デンマーク語");
        m_mapLocale[_T("de")]    = _T("ドイツ語");
        m_mapLocale[_T("el")]    = _T("ギリシャ語");
        m_mapLocale[_T("en")]    = _T("英語");
        m_mapLocale[_T("es")]    = _T("スペイン語");
        m_mapLocale[_T("et")]    = _T("エストニア語");
        m_mapLocale[_T("eu")]    = _T("バスク語");
        m_mapLocale[_T("fa")]    = _T("ペルシャ語");
        m_mapLocale[_T("fi")]    = _T("フィンランド語");
        m_mapLocale[_T("fo")]    = _T("フェロー語");
        m_mapLocale[_T("fr")]    = _T("フランス語");
        m_mapLocale[_T("gd")]    = _T("ゲール語 ");
        m_mapLocale[_T("he")]    = _T("ヘブライ語");
        m_mapLocale[_T("hi")]    = _T("ヒンズー語");
        m_mapLocale[_T("hr")]    = _T("クロアチア語");
        m_mapLocale[_T("hu")]    = _T("ハンガリー語");
        m_mapLocale[_T("in")]    = _T("インドネシア語");
        m_mapLocale[_T("is")]    = _T("アイスランド語");
        m_mapLocale[_T("it")]    = _T("イタリア語");
        m_mapLocale[_T("ja")]    = _T("日本語");
        m_mapLocale[_T("ji")]    = _T("イディッシュ語");
        m_mapLocale[_T("ko")]    = _T("韓国語");
        m_mapLocale[_T("lt")]    = _T("リトアニア語");
        m_mapLocale[_T("lv")]    = _T("ラトビア語");
        m_mapLocale[_T("mk")]    = _T("マケドニア語 (FYROM)");
        m_mapLocale[_T("ms")]    = _T("マレー語");
        m_mapLocale[_T("mt")]    = _T("マルタ語");
        m_mapLocale[_T("nl")]    = _T("オランダ語");
        m_mapLocale[_T("no")]    = _T("ノルウェー語");
        m_mapLocale[_T("pl")]    = _T("ポーランド語");
        m_mapLocale[_T("pt")]    = _T("ポルトガル語");
        m_mapLocale[_T("rm")]    = _T("レトロマン語");
        m_mapLocale[_T("ro")]    = _T("ルーマニア語");
        m_mapLocale[_T("ru")]    = _T("ロシア語");
        m_mapLocale[_T("sb")]    = _T("ソルビア語");
        m_mapLocale[_T("sk")]    = _T("スロバキア語");
        m_mapLocale[_T("sl")]    = _T("スロベニア語");
        m_mapLocale[_T("sq")]    = _T("アルバニア語");
        m_mapLocale[_T("sr")]    = _T("セルビア語 (キリル文字)");
        m_mapLocale[_T("sv")]    = _T("スウェーデン語");
        m_mapLocale[_T("sx")]    = _T("ソト語");
        m_mapLocale[_T("th")]    = _T("タイ語");
        m_mapLocale[_T("tn")]    = _T("ツワナ語");
        m_mapLocale[_T("tr")]    = _T("トルコ語");
        m_mapLocale[_T("ts")]    = _T("ツォンガ語");
        m_mapLocale[_T("uk")]    = _T("ウクライナ語");
        m_mapLocale[_T("ur")]    = _T("ウルドゥー語");
        m_mapLocale[_T("vi")]    = _T("ベトナム語");
        m_mapLocale[_T("xh")]    = _T("コーサ語");
        m_mapLocale[_T("zh-cn")] = _T("中国語 (中華人民共和国)");
        m_mapLocale[_T("zh-hk")] = _T("中国語 (香港)");
        m_mapLocale[_T("zh-sg")] = _T("中国語 (シンガポール)");
        m_mapLocale[_T("zh-tw")] = _T("中国語 (台湾)");
        m_mapLocale[_T("zu")]    = _T("ズールー語");
    }
}                    
                     
/**                  
 * @ brief   ロケールの設定
 * @ param   [in] lcid 
 * @ return  true:
 * @ note	         
 */
bool CSystemLocal::SetLocal(LCID lcid)
{
    StdString strLang = GetLanguageCode(lcid);

    if (strLang == _T(""))
    {
        return false;
    }

    if(!IsLocale(strLang))
    {
        return false;
    }

    //TODO:ロケールディレクトリから 
    //     言語コード ロケール名マップを読み込む

    //TODO:ディフォルトのファイルをチェックする。


    //TODO:ロケールの設定
   //std::wcout.imbue(std::locale(std::locale("Japanese"),
   //                   "C", std::locale::numeric));


    CSystemInit::SetLocal(strLang);
    return true;
}

/**                  
 * @ brief   言語コードの取得
 * @ param   [in] lcid 
 * @ return  言語コード (ja, en 等)
 * @ note	         
 */
StdString CSystemLocal::GetLanguageCode(LCID lcid)
{
    using namespace boost::bimaps;
    StdString strRet;
    bimap<LCID, multiset_of<StdString>>::left_map::iterator ite;

    ite = m_mapLang.left.find(lcid);
    if (ite != m_mapLang.left.end())
    {
        strRet = (ite->second);
    }
    return  strRet;
}

/**                  
 * @ brief   ロケール名取得
 * @ param   [in] strLang  言語コード(ja, en 等)
 * @ return  ロケール名 (日本語, )
 * @ note	         
 */
StdString CSystemLocal::GetLocalName(StdString strLang)
{
    StdString strRet;
    std::map<StdString, StdString>::iterator ite;
    ite = m_mapLocale.find(strLang);
    if ( ite != m_mapLocale.end())
    {
        strRet = ite->second;
    }
    return strRet;
}


/**                  
 * @ brief   ロケール判定
 * @ param   [in] strLang  言語コード(strLang  ja, en 等)
 * @ return  true:存在する
 * @ note	         
 */
bool CSystemLocal::IsLocale(StdString strLang)
{
    if ( m_mapLang.right.count(strLang) != 0)
    {
        return true;
    }
    return false;
}

/**
 * @ brief   言語ディレクトリ存在判定
 * @ param   [in] strLang  言語コード(strLang  jp, en-us 等)
 * @ return  true:存在する
 * @ note
 */
bool CSystemLocal::IsExistDir(StdString strLang)
{
    std::vector<StdString>::iterator ite;

    ite = std::find(m_lstExistLang.begin(), 
                    m_lstExistLang.end(),
                    strLang);
    return ( ite != m_lstExistLang.end());
}

/**
 * @ brief   現在のロケールに対するディレクトリ
 * @ param   なし
 * @ return  ディレクトリ
 * @ note
 */
StdPath CSystemLocal::GetLocalDir()
{
    StdPath pathRet;
    StdPath  pathLoad = CSystem::GetSystemPath();
    pathLoad /= _T("Language");
    pathLoad /= CSystemInit::GetLocal();
    pathRet = pathLoad;
    return pathRet;
}

/**
 * @ brief   全ロケール名リストの取得
 * @ param   [out] pLstLocal ロケール名リスト
 * @ return  ディレクトリ
 * @ note
 */
void CSystemLocal::GetAllLocalList(std::vector<StdString>* pLstLocal)
{
    if (pLstLocal == NULL)
    {
        return;
    }

    pLstLocal->clear();

    StdString strLang, strLocale; 
    foreach(  boost::tie(strLang, strLocale), m_mapLocale)
    {
        pLstLocal->push_back(strLocale);
    }
}

//!< 
/**
 * @ brief   設定可能ロケール名リストの取得
 * @ param   [out] pLstLocal ロケール名リスト
 * @ return  ディレクトリ
 * @ note
 */
void CSystemLocal::GetExistLocalList(std::vector<StdString>* pLstLocal)
{
    *pLstLocal = m_lstExistLang;
}

/**                  
 * @ brief  ロケールディレクトリの取得
 * @ param  なし    
 * @ return なし     
 * @ note	         
 */
void CSystemLocal::GetLocale()
{
    namespace fs = boost::filesystem; 

    StdPath  pathLoad = CSystem::GetSystemPath();
    pathLoad /= _T("Language");

    fs::directory_iterator last;

    try
    {
        StdString strLangDir;
        m_lstExistLang.clear();
        for (fs::directory_iterator itePos(pathLoad);
             itePos != last;
             itePos++) 
        {
            if (!fs::is_directory(*itePos)) 
            {
                continue;
            }

            strLangDir = itePos->path().leaf().native();
            if (!IsLocale(strLangDir))
            {
                continue;
            }

            m_lstExistLang.push_back(strLangDir);
        }
    }
    catch(...)
    {
        //TODO: Ticket#86 システム定義読み込み失敗




    }
}

/**
 * @brief   XML読み込み
 * @param   [in] pXml 
 * @return	なし
 * @note	 
 */
void CSystemLocal::LoadXml(StdXmlArchiveIn* pXml)
{
    *pXml >> boost::serialization::make_nvp("Root", *this);
}

/**
 * @brief   XML書き込み
 * @param   [in] pXml
 * @return	なし
 * @note	 
 */
void CSystemLocal::SaveXml(StdXmlArchiveOut* pXml)
{
    *pXml << boost::serialization::make_nvp("Root", *this);
}



/**                  
 * @ brief   ファイル読み込み
 * @ param   なし    
 * @ return	なし     
 * @ note
 */
void CSystemLocal::Load()
{
    StdPath  pathLoad = GetLocalDir();
    pathLoad /= m_pathFileName;
    CConfig::Load2(pathLoad);
}

/**
 * @brief   ファイル書き込み
 * @param   なし          
 * @retval  なし
 * @note 
 */
void CSystemLocal::Save()
{
    StdPath  pathSave =     GetLocalDir();
    pathSave /= m_pathFileName;
    CConfig::Save2(pathSave);
}


//*******************************
//             TEST
//*******************************
#ifdef _DEBUG

//テスト前提条件
void TEST_CSystemLocal_Prerequisite()
{
    namespace fs = boost::filesystem;

    StdPath  pathLoad = CSystem::GetSystemPath();
    pathLoad /= _T("Language");

    StdPath  pathJp = pathLoad / _T("ja");
    StdPath  pathUs = pathLoad / _T("en");

    STD_ASSERT(fs::exists(pathJp));
    STD_ASSERT(fs::exists(pathUs));
}

// bimapについて確認
void TEST_bimap()
{
    using namespace boost::bimaps;

    bimap<int, multiset_of<StdString>> mapTest;
    typedef bimap<int, multiset_of<StdString>>::value_type  value_t;


    mapTest.insert(value_t(1, _T("hello")));
    mapTest.insert(value_t(2, _T("hello")));
    mapTest.insert(value_t(3, _T("bye")));
    mapTest.insert(value_t(4, _T("4")));
    mapTest.insert(value_t(5, _T("5")));
    mapTest.insert(value_t(6, _T("bye")));
    mapTest.insert(value_t(7, _T("bye")));

    bimap<int, multiset_of<StdString>>::right_map::iterator iteRight;
    bimap<int, multiset_of<StdString>>::left_map::iterator iteLeft;

    //--------------------------------------------
    //左側は一意なのでMAPと同じように扱える
    //--------------------------------------------
    StdString strRight;
    STD_ASSERT( mapTest.left.at(4) == _T("4"));
    STD_ASSERT( mapTest.left.at(7) == _T("bye"));

#if 0  //例外が機能しない場合もある
    try
    {
        STD_ASSERT( mapTest.left.at(10) != _T("bye"));
        STD_DBG( _T("ここは通らない"));
    }
    catch( std::exception const & e)
    {
        //範囲外は例外が発生
        e;
    }
#endif

    iteLeft = mapTest.left.find(10);
    STD_ASSERT(iteLeft ==  mapTest.left.end());

    iteLeft = mapTest.left.find(4);
    STD_ASSERT(iteLeft !=  mapTest.left.end());
    //--------------------------------------------

    //--------------------------------------------
    //右側はのでMULTIMAPと同じように扱う
    //--------------------------------------------

    typedef bimap<int, multiset_of<StdString>>::right_map::iterator iterator_t;
    typedef std::pair<iterator_t, iterator_t> pair_t;


    pair_t p = mapTest.right.equal_range(_T("bye"));


    for(iteRight  =p.first; 
        iteRight !=p.second; 
        iteRight++)
    {
         DB_PRINT(_T("RightVal = %d \n"), *iteRight);
         //3,6,7が出力される
    }


    iteRight = mapTest.right.find(_T("bye"));
    STD_ASSERT(iteRight !=  mapTest.right.end());


    //データの有無を調べるだけなら countを使うと良い
    STD_ASSERT(mapTest.right.count(_T("byeb")) == 0);
    STD_ASSERT(mapTest.right.count(_T("bye")) == 3);

}


//----------------
// テスト
//----------------
void TEST_CSystemLocal()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    TEST_bimap();


    TEST_CSystemLocal_Prerequisite();
    CSystemLocal local;

    local.Init();

    STD_ASSERT(local.GetLanguageCode(16394) == _T("es"));
    STD_ASSERT(local.GetLanguageCode(1041) == _T("ja"));


    STD_ASSERT( local.IsExistDir(_T("ja")));
    STD_ASSERT( local.IsExistDir(_T("en")));
    STD_ASSERT(!local.IsExistDir(_T("es")));
    
    local.SetDefault();

    STD_ASSERT(local.GetLocalName(_T("ja")) == _T("Japanese"));


    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));

}
#endif //_DEBUG