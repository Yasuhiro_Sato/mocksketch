/**
 * @brief			CSystemLocalヘッダーファイル
 * @file			CSystemLocaly.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef __SYSTEM_LOCAL_H__
#define __SYSTEM_LOCAL_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CConfig.h"
#include "Utility/TreeData.h"

/*---------------------------------------------------*/
/*  Classes                                          */
/*---------------------------------------------------*/
class CSystemInit;

/**
 * @class   CSystemLocal
 * @brief                        
 */
class CSystemLocal : public CConfig
{
    friend void TEST_CSystemLocal();

    

public:


    CSystemLocal();

    ~CSystemLocal();

    //!< 初期化
    void Init();

    //!< XML読み込み
    virtual void LoadXml(StdXmlArchiveIn* pXml);

    //!< XML書き込み
    virtual void SaveXml(StdXmlArchiveOut* pXml);

    //!< 読み込み
    virtual void Load();

    //!< 書き込み
    virtual void Save();

    //!< 初期値設定
    void SetDefault();

    //!< ロケールの設定
    bool SetLocal(LCID lcid);

    //!< 言語コードの取得
    StdString GetLanguageCode(LCID lcid);

    //!< ロケール名取得
    StdString GetLocalName(StdString strLang);

    //!< 言語ディレクトリ存在判定
    bool IsExistDir(StdString strLang);

    //!< 現在のロケールに対するディレクトリ
    static StdPath   GetLocalDir();

    //!< 全ロケール名リストの取得
    void GetAllLocalList(std::vector<StdString>* pLstLocal);

    //!< 設定可能ロケール名リストの取得
    void GetExistLocalList(std::vector<StdString>* pLstLocal);

    
    CSystemInit*        pSysInit;

protected:

    //!< ロケール判定
    bool IsLocale(StdString strLang);

    //!< ロケールディレクトリの取得
    void GetLocale();

protected:

    //-------------
    //!< 保存データ
    //-------------
    // 言語コード ロケール名マップ
    std::map<StdString, StdString> m_mapLocale;

    //ファイル名
    StdPath  m_pathFileName;

    //-------------
    //!< 非保存データ
    //-------------
    // 言語コード LCID変換マップ
    boost::bimaps::bimap<LCID, boost::bimaps::multiset_of<StdString>> m_mapLang;

    // 設定可能言語コードリスト
    std::vector<StdString> m_lstExistLang;

    // 現在の言語コード
    static StdString  ms_strLanguageCode;


public:


private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        namespace ser = boost::serialization ;
        std::string  s;
        try
        {
            s = "LocalMap"; ar & ser::make_nvp(s.c_str() , m_mapLocale);
        }
        catch(MockException& e)
        {
            throw e;
        }
        catch(...)
        {
            StdStringStream strm;
            StdString str = CUtil::CharToString(s.c_str());

            int iErrorNo;
            if (Archive::is_loading::value)
            {
                iErrorNo = e_file_read;
                strm << StdFormat(_T("File read error %s:%s"))
                        % DB_FUNC_NAME
                        % str.c_str();
            }
            else
            {
                iErrorNo = e_file_write;
                strm << StdFormat(_T("File write error %s:%s"))
                        % DB_FUNC_NAME
                        % str.c_str();
            }
            throw MockException(iErrorNo, strm.str().c_str());
        }
    }


private:


};

#endif //__SYSTEM_LOCAL_H__
