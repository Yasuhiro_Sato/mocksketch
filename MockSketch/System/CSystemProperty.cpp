/**
 * @brief			CSystemProperty実装ファイル
 * @file			CSystemProperty.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "CSystem.h"
#include "CSystemFile.h"
#include "Utility/CPropertyGroup.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifndef _CONSOLE
#ifdef _DEBUG
#define new DEBUG_NEW
#endif
#endif

/*---------------------------------------------------*/
/*  Static                                           */
/*---------------------------------------------------*/


/**
 * @brief   コンストラクタ
 * @param   なし
 * @return	なし
 * @note	 
 */
CSystemProperty::CSystemProperty():
m_iChgCnt(0)
{
    m_pathFileName = _T("SystemProperty.xml");
}


/**
 * @brief   デストラクタ
 * @param   なし
 * @return	なし
 * @note	 
 */
CSystemProperty::~CSystemProperty()
{
}

/**
 * @brief   プロパティセットインスタンス取得
 * @param   [in] strName プロパティセット名
 * @return	なし
 * @note	プロパティ名は グループ名.プロパティセット名
 */
CPropertySet* CSystemProperty::GetPropertySetInstance(StdString strName)
{
    CPropertySet* pRet;

    std::vector<StdString> lstName; 
    CUtil::TokenizeCsv(&lstName, strName,  _T('.'));

    if (lstName.size() != 2)
    {
        return NULL;
    }

    foreach(std::shared_ptr<CPropertyGroup> pGroup, m_lstPropertyGroup)
    {
        if (pGroup->GetName() != lstName[0])
        {
            continue;
        }

        pRet = pGroup->GetPropertySetInstance(lstName[1]);
        return pRet;
    }
    return NULL;
}

/**
 * @brief   プロパティセット名称リスト取得
 * @param   [out] pList プロパティセット名称リスト
 * @return	なし
 * @note	 
 */
void CSystemProperty::GetPropertySetNameList(std::set<StdString>* pList)
{
    std::vector<StdString> lstName;
    StdString strGroup;
    StdString strInstance;
    foreach(std::shared_ptr<CPropertyGroup> pGroup, m_lstPropertyGroup)
    {
         strGroup = pGroup->GetName();
         pGroup->GetPropertySetNameList(&lstName);
         pList->insert(strGroup);
         foreach(StdString& name, lstName)
         {
             strInstance = strGroup;
             strInstance += _T(".");
             strInstance += name;
             pList->insert(strInstance);
         }
    }
}

/**
 * @brief   ディフォルト値設定
 * @param   なし
 * @return	なし
 * @note	 
 */
void CSystemProperty::SetDefault()
{
    m_iMruFileMax = 3;
}

/**
 * @brief   指定ディレクトリ読み込み
 * @param   [in] path 指定ディレクトリ 
 * @param   [in] bSubDir サブディレクトリ読込 
 * @return	true 成功
 * @note	 
 */
bool CSystemProperty::LoadDir(StdPath path, bool bSubDir)
{
    std::vector<StdPath> lstFile;
    if(!_GetFileList(&lstFile, path, bSubDir))
    {
        return false;
    }

    foreach(StdPath& path, lstFile)
    {
        bool bDup = false;
        StdString name = path.stem().c_str();
        if (_HasName(name))
        {
            // グループ名の重複をチェック
            // 名称(%s)が重複しています
            StdString strError = GET_ERR_STR(e_dup_name_s);

            DispError(false, strError.c_str(), name.c_str());

            bDup = true;
        }
        
        //ファイルが重複しても上書きする
        std::shared_ptr<CPropertyGroup> pGroup =  std::make_shared<CPropertyGroup>();
        if (pGroup->LoadFile(path, true))
        {
            if (bDup)
            {
                UnloadPropertyGroup(name);
            }
            m_lstPropertyGroup.push_back(pGroup);
        }
    }
    AddChgCnt();
    return true;
}

/**
 * @brief   ファイルコピー
 * @param   [out] pFileList 指定ディレクトリ 
 * @param   [in]  path      指定ディレクトリ 
 * @return	true 成功
 * @note	使わない
 */
bool CSystemProperty::CopyProperty(StdPath pathSrc, StdPath pathDst)
{
    std::vector<StdPath> lstPath;
    
    if(!_GetFileList(&lstPath, pathSrc, false))
    {
        return false;
    }

    namespace fs = boost::filesystem;
    try 
    {
        foreach(StdPath& path, lstPath)
        {
           StdPath pathFile;
           pathFile = pathDst;
           pathFile /= path.filename();
           fs::copy_file(path, pathFile);
        }
    }
    catch (fs::filesystem_error& ex) 
    {

        STD_DBG(_T("Filesystem error %s"), ex.what());
        return false;
    }
    return true;
}

/**
 * @brief   指定ディレクトリ読み込み
 * @param   [out] pFileList 指定ディレクトリ 
 * @param   [in]  path      指定ディレクトリ 
 * @param   [in]  bSubDir   サブディレクトリも検索 
 * @return	true 成功
 * @note	 
 */
bool CSystemProperty::_GetFileList(std::vector<StdPath>* pFileList, 
                               StdPath path, bool bSubDir)
{
    if (!pFileList)
    {
        return false;
    }

    WIN32_FIND_DATA fd;
    HANDLE hFind;
    
    StdPath pathSearch = path;
    StdPath pathFile;
    StdString strExt = _T(".");

    strExt    += GetPropertyExt();

    pathSearch /= _T("*");

    //pFileList->clear();

    StdString stFile = CUtil::CharToString(pathSearch.string().c_str());
    hFind = FindFirstFile(stFile.c_str(), &fd);
    if(hFind==INVALID_HANDLE_VALUE)
    {
        return false;
    }

    do
    {
        if (lstrcmp(fd.cFileName, _T("..")) != 0 && 
            lstrcmpi(fd.cFileName, _T(".")) != 0) 
        {
            if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) 
            {
                if (bSubDir)
                {
                    StdPath pathNext;
                    pathNext = path;
                    pathNext /= fd.cFileName;
                    _GetFileList(pFileList, pathNext, true);
                }
            }
            else
            {
                pathFile = path;
                pathFile /= fd.cFileName;

                if (pathFile.has_extension())
                {
                    StdString strCurExt = pathFile.extension().wstring();
                    if (lstrcmpi(strExt.c_str(), 
                                 strCurExt.c_str()) == 0)
                    {
                        pFileList->push_back(pathFile);
                    }
                }
            }
        }
    }
    while(::FindNextFile(hFind,&fd) != 0);

    FindClose(hFind);
    return true;
}


/**
 * @brief   指定ディレクトリ解放
 * @param   [in]  path      指定ディレクトリ 
 * @return	true 成功
 * @note	 
 */
bool CSystemProperty::UnloadDir(StdPath path, bool bSubDir)
{
    std::vector<StdPath> lstFile;

    if(!_GetFileList(&lstFile, path, bSubDir))
    {
        return false;
    }

    foreach(StdPath& path, lstFile)
    {
        StdString nameGroup = path.stem().c_str();
        UnloadPropertyGroup(nameGroup);
    }

    AddChgCnt();

    return true;
}

bool CSystemProperty::_HasName(StdString name)
{
    std::vector<std::shared_ptr<CPropertyGroup>>::iterator ite;
    //=========================
    // ラムダ式
    ite = boost::find_if( m_lstPropertyGroup, [=](std::shared_ptr<CPropertyGroup> pGroup)
    {
        if(pGroup->GetName() == name)
        {
            return true;
        }
        return false;
    });
    //=========================

    if (ite == m_lstPropertyGroup.end())
    {
        return false;
    }
    return true;
}

/**
 * @brief   XML読み込み
 * @param   [in] pXml 
 * @return	なし
 * @note	 
 */
void CSystemProperty::LoadXml(StdXmlArchiveIn* pXml)
{
    *pXml >> boost::serialization::make_nvp("Root", *this);
}

/**
 * @brief   XML書き込み
 * @param   [in] pXml
 * @return	なし
 * @note	 
 */
void CSystemProperty::SaveXml(StdXmlArchiveOut* pXml)
{
    *pXml << boost::serialization::make_nvp("Root", *this);
}

void CSystemProperty::LoadMru()
{
    StdPath  pathLoad = CSystem::GetSystemPath();
    pathLoad /= m_pathFileName;
    CConfig::Load(pathLoad);
}

void CSystemProperty::SaveMru()
{
    StdPath  pathSave =     CSystem::GetSystemPath();
    pathSave /= m_pathFileName;
    CConfig::Save(pathSave);
}


/**
 * @brief   ファイル読み込み
 * @param   なし
 * @return	なし
 * @note	 
 */
void CSystemProperty::Load()
{
    //
    namespace fs = boost::filesystem;
    m_lstPropertyGroup.clear();

    StdPath  pathLoad = CSystem::GetSystemPath();

    StdPath  pathAddin = pathLoad;
    pathAddin /= SYS_CONFIG->strPathScriptAddins;
    LoadDir(pathAddin, true);

    StdPath  pathPropertySet;
    pathPropertySet = SYS_CONFIG->GetLibraryPath().parent_path();
    LoadDir(pathPropertySet, true);

    LoadMru();
}

/**
 * @brief   ファイル書き込み
 * @param   なし          
 * @retval  なし
 * @note    普通は使わない
 */
void CSystemProperty::Save()
{
    bool bRet;
    foreach(std::shared_ptr<CPropertyGroup> pGroup, m_lstPropertyGroup)
    {
        bRet = pGroup->SaveFile(pGroup->GetFilePath(),true);
    }

    SaveMru();
}


/**
 * @brief   プロパティグループ数取得
 * @param   なし          
 * @retval  プロパティグループ数
 * @note    
 */
int  CSystemProperty::GetPropertyGroupNum()
{
    return static_cast<int>(m_lstPropertyGroup.size());
}

/**
 * @brief   プロパティグループ取得
 * @param   [in]  プロパティグループ番号          
 * @retval  なし
 * @note    
 */
std::weak_ptr<CPropertyGroup> CSystemProperty::GetPropertyGroupInstance(int iNo)
{
    std::weak_ptr<CPropertyGroup> pRet;
    if (iNo < 0){return pRet;}
    if (iNo >= GetPropertyGroupNum()){return pRet;}
    pRet = m_lstPropertyGroup[iNo];
    return pRet;
}


bool CSystemProperty::UnloadPropertyGroup(int iNo)
{
    std::vector<std::shared_ptr<CPropertyGroup>>::iterator ite;
    if (iNo < 0){return false;}
    if (iNo >= GetPropertyGroupNum()){return false;}

    ite = m_lstPropertyGroup.begin() + iNo;

    m_lstPropertyGroup.erase(ite);

    return true;
}

bool CSystemProperty::UnloadPropertyGroup(StdString nameGroup)
{
    std::vector<std::shared_ptr<CPropertyGroup>>::iterator ite;
    int iNo = 0;
    foreach(std::shared_ptr<CPropertyGroup> pGroup, m_lstPropertyGroup)
    {
        if (pGroup->GetName() == nameGroup)
        {
            ite = m_lstPropertyGroup.begin() + iNo;
            m_lstPropertyGroup.erase(ite);
            return true;
        }

        iNo++;
    }
    return false;
}

/**
 * @brief   MRUファイル追加
 * @param   [in] strFileName ファイル名
 * @retval  なし
 * @note    
 */
void CSystemProperty::AddMruFile(StdString strFileName)
{
    std::deque<StdString>::iterator iteFile;

    iteFile = std::find( m_lstMruFile.begin(),
                         m_lstMruFile.end(),
                         strFileName);
    if (iteFile != m_lstMruFile.end())
    {
        m_lstMruFile.erase(iteFile);
    }

    m_lstMruFile.push_front(strFileName);

    if (GetMruFileNum() > m_iMruFileMax)
    {
        m_lstMruFile.pop_back();
    }
    SaveMru();
}
    
/**
 * @brief   MRUファイル数取得
 * @param   なし
 * @retval  MRUファイル数
 * @note    
 */
int CSystemProperty::GetMruFileNum()
{
    return static_cast<int>(m_lstMruFile.size());
}

/**
 * @brief   MRUファイル最大数取得
 * @param   なし
 * @retval  MRUファイル最大数
 * @note    
 */
int CSystemProperty::GetMruFileMax()
{
    return m_iMruFileMax;
}

/**
 * @brief   MRUファイル最大数設定
 * @param   [in] iNum  MRUファイル最大数
 * @retval  なし
 * @note    
 */
void CSystemProperty::SetMruFileMax(int iNum)
{
    if (iNum < 1)
    {
        m_iMruFileMax = 1;
    }
    else if (iNum > 5)
    {
        m_iMruFileMax = 5;
    }

    int iDiff = GetMruFileNum() - iNum;
    if (iDiff > 0)
    {
        for (int iCnt = 0; iCnt < iDiff; iCnt++)
        {
            m_lstMruFile.pop_back();
        }
    }
    m_iMruFileMax = iNum;
    SaveMru();
}

/**
 * @brief   MRUファイル名取得
 * @param   [in] iNum ファイル番号
 * @retval  MRUファイル名
 * @note
 */
StdString CSystemProperty::GetMruFile(int iNum)
{
    if (iNum < 0)
    {
        return _T("");
    }
    else if (iNum >= GetMruFileNum())
    {
        return _T("");
    }

    return m_lstMruFile[iNum];
}

/**
 * @brief   MRUファイル読み込み
 * @param   [in] iNum ファイル番号
 * @retval  true読み込み成功
 * @note
 */
std::shared_ptr<CPropertyGroup> CSystemProperty::LoadMruFile(int iNum)
{
    std::shared_ptr<CPropertyGroup> pGroupNew;
    if (iNum < 0)
    {
        return pGroupNew;
    }
    else if (iNum >= GetMruFileNum())
    {
        return pGroupNew;
    }


    StdPath path = m_lstMruFile[iNum];

    //保持しているデータから探す
    foreach(std::shared_ptr<CPropertyGroup> pGroup, m_lstPropertyGroup)
    {
        if (path == pGroup->GetFilePath())
        {
            std::swap(m_lstMruFile[0], m_lstMruFile[iNum]);
            return pGroup;
        }
    }

    //ファイルを読み込む
    pGroupNew = std::make_shared<CPropertyGroup>();

    if (pGroupNew->LoadFile(path, false))
    {
        std::swap(m_lstMruFile[0], m_lstMruFile[iNum]);
        return pGroupNew;
    }

    //ファイルが無かった
    DelMruFile(iNum);
    pGroupNew.reset();

    return pGroupNew;
}

/**
 * @brief   MRUファイル名削除
 * @param   [in] iNum ファイル番号
 * @retval  MRUファイル名
 * @note
 */
bool CSystemProperty::DelMruFile(int iNum)
{
    if (iNum < 0)
    {
        return false;
    }
    else if (iNum >= GetMruFileNum())
    {
        return false;
    }

    std::deque<StdString>::iterator ite;
    ite = m_lstMruFile.begin() + iNum;

    m_lstMruFile.erase(ite);

    SaveMru();
    return true;
}

/**
 * @brief  変更番号取得
 * @param  なし
 * @retval 変更番号
 * @note
 */
int CSystemProperty::GetChgCnt()
{
    return m_iChgCnt;
}

/**
 * @brief  変更番号追加
 * @param  なし
 * @retval なし
 * @note
 */
void CSystemProperty::AddChgCnt()
{
    if (m_iChgCnt == INT_MAX)
    {
        m_iChgCnt = -1;
    }
    m_iChgCnt++;
}


//*******************************
//             TEST
//*******************************
#ifdef _DEBUG

//----------------
// テスト
//----------------
void TEST_CSystemProperty()
{


    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());


    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));

}
#endif //_DEBUG