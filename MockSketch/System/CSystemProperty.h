/**
 * @brief			CSystemPropertyヘッダーファイル
 * @file			CSystemProperty.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef __SYSTEM_PROPERTY_H__
#define __SYSTEM_PROPERTYH__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CConfig.h"

/*---------------------------------------------------*/
/*  Classes                                          */
/*---------------------------------------------------*/
class CPropertySet;
class CPropertyGroup;

class CSystemProperty:public CConfig
{
public:
    CSystemProperty();
    ~CSystemProperty();

    virtual void SetDefault();

    CPropertySet* GetPropertySetInstance(StdString strName);

    void GetPropertySetNameList(std::set<StdString>* pList);

    int  GetPropertyGroupNum();

    std::weak_ptr<CPropertyGroup> GetPropertyGroupInstance(int iNo);
    
    bool UnloadPropertyGroup(int iNo);

    bool UnloadPropertyGroup(StdString nameGroup);

    bool AddPropertyGroup(int iNo);

    //!< XML読み込み
    virtual void LoadXml(StdXmlArchiveIn* pXml);

    //!< XML書き込み
    virtual void SaveXml(StdXmlArchiveOut* pXml);

    virtual void SaveMru();

    virtual void LoadMru();

    virtual void Save();

    virtual void Load();

    //!< MRUファイル追加
    void AddMruFile(StdString strFileName);
    
    //!< MRUファイル数取得
    int GetMruFileNum();

    //!< MRUファイル最大数取得
    int GetMruFileMax();

    //!< MRUファイル最大数設定
    void SetMruFileMax(int iNum);

    //!< MRUファイル名取得
    StdString GetMruFile(int iNum);

    //!< MRUファイル削除
    bool DelMruFile(int iNum);

    //!< MRUファイル読み込み
    std::shared_ptr<CPropertyGroup> LoadMruFile(int iNum);

    StdString GetPropertyExt(){return _T("mps");}

    int GetChgCnt();

    void AddChgCnt();

    bool LoadDir(StdPath path, bool bSubDir);

    bool UnloadDir(StdPath path, bool bSubDir);

    bool CopyProperty(StdPath pathSrc, StdPath pathDst);

private:

    bool _GetFileList(std::vector<StdPath>* pFileList, 
                               StdPath path,
                               bool bSubDir = true);

    bool _UnLoadDir(StdPath path);

    bool _HasName(StdString name);
private:

    std::deque<StdString>      m_lstMruFile;

    std::vector<std::shared_ptr<CPropertyGroup>> m_lstPropertyGroup;

    int     m_iMruFileMax;

    StdPath m_pathFileName;

    int     m_iChgCnt;

private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
            SERIALIZATION_BOTH("MruFileMax"      , m_iMruFileMax);
            SERIALIZATION_BOTH("MruFileList"     , m_lstMruFile);
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }

};

#endif //__SYSTEM_PROPERTYH__
