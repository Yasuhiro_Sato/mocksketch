/**
 * @brief			CSystemString実装ファイル
 * @file			CSystemString.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "CSystem.h"
#include "CSystemString.h"
#include "Utility/CUtility.h"



/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifndef _CONSOLE
#ifdef _DEBUG
#define new DEBUG_NEW
#endif
#endif

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CSystemString::CSystemString()
{
    m_pathFileName = _T("SystemString.xml");
}

/**
 *  @brief  デストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CSystemString::~CSystemString()
{

}

/**
 *  @brief  ビューモード取得
 *  @param  [in] eMode  ビューモード
 *  @retval ビューモード文字表記
 *  @note
 */
StdString CSystemString::StrViewMode(VIEW_MODE eMode)
{
    std::map<VIEW_MODE, StdString>::iterator itFind;

    itFind = m_mapViewMode.find(eMode);

    if (itFind == m_mapViewMode.end())
    {
        StdString StrErr = CUtil::StrFormat(_T("ERROR:(%d)"), eMode);
        STD_DBG(StrErr.c_str());
        return StrErr;
    }
    return itFind->second;
}

/**
 *  @brief  システム文字取得
 *  @param  [in] strType  取得文字インデックス
 *  @retval 取得文字列
 *  @note
 */
StdString CSystemString::GetString(const StdString& strType)
{
    std::map<StdString, StdString>::iterator itFind;

    itFind = m_mapString.find(strType);

    if (itFind == m_mapString.end())
    {
        StdString StrErr = CUtil::StrFormat( _T("ERROR:(%s)"), strType.c_str());
        STD_DBG(StrErr.c_str());
        return StrErr;
    }
    return  itFind->second;
}


/**
 *  @brief  システム文字取得
 *  @param  [in] strType  色名インデックス
 *  @retval 色名
 *  @note
 */
StdString CSystemString::GetColorName(const StdString& strType)
{
    std::map<StdString, StdString>::iterator itFind;

    itFind = m_mapColor.find(strType);

    if (itFind == m_mapColor.end())
    {
        StdString StrErr = CUtil::StrFormat( _T("ERROR:(%s)"), strType.c_str());
        STD_DBG(StrErr.c_str());
        return StrErr;
    }
    return  itFind->second;
}

/**
 * @brief   ディフォルト値設定
 * @param   なし          
 * @retval  なし
 * @note	
 */
void CSystemString::SetDefault()
{

    //これは仮 後でファイルに置き換えること
    m_mapString[STR_VIEW_SELECT ] = _T("選択"  );
    m_mapString[STR_VIEW_POINT  ] = _T("点"    );       //!< 点描画
    m_mapString[STR_VIEW_LINE   ] = _T("線分");         //!< 線分
    m_mapString[STR_VIEW_CIRCLE ] = _T("円");           //!< 円
    m_mapString[STR_VIEW_CIRCLE_3P ] = _T("3点指定円"); //!< ３点を通る円
    m_mapString[STR_VIEW_ARC    ] = _T("円弧");         //!< 円弧
    m_mapString[STR_VIEW_NODE   ] = _T("節点");         //!< 節点
    m_mapString[STR_VIEW_IMAGE  ] = _T("画像");         //!< 画像


    m_mapString[STR_VIEW_ELLIPSE ] = _T("楕円");         //!< 楕円
    m_mapString[STR_VIEW_TEXT    ] = _T("文字");         //!< 文字
    m_mapString[STR_VIEW_SPLINE  ] = _T("スプライン");   //!< スプライン

    /*
    m_mapString[STR_VIEW_L_DIM    ] = _T("寸法");         //!< 寸法
    m_mapString[STR_VIEW_H_DIM     ] = _T("水平寸法");    //!< 寸法  //STR_MNU_H_DIMど同じなので整理すること
    m_mapString[STR_VIEW_V_DIM     ] = _T("垂直寸法");    //!< 寸法
    m_mapString[STR_VIEW_A_DIM     ] = _T("角度");        //!< 寸法
    m_mapString[STR_VIEW_D_DIM     ] = _T("直径");        //!< 寸法
    m_mapString[STR_VIEW_C_DIM     ] = _T("チャンファ");        //!< 寸法
    */
    m_mapString[STR_VIEW_GROUP   ] = _T("グループ化");   //!< グループ化
    m_mapString[STR_VIEW_PARTS   ] = _T("部品化");   //!< グループ化
    m_mapString[STR_VIEW_REFERENCE] = _T("参照化");   //!< グループ化


    m_mapString[STR_VIEW_DELETE ] = _T("削除");         //!< 削除
    m_mapString[STR_VIEW_MOVE   ] = _T("移動");         //!< 移動
    m_mapString[STR_VIEW_COPY   ] = _T("複写");         //!< 複写
    m_mapString[STR_VIEW_ROTATE ] = _T("回転");         //!< 回転
    m_mapString[STR_VIEW_SCL    ] = _T("倍率");         //!< 倍率
    m_mapString[STR_VIEW_MIRROR ] = _T("鏡像");         //!< 鏡像

    m_mapString[STR_VIEW_DISASSEMBLY ] = _T("グループ化解除");   //!< グループ化解除
    m_mapString[STR_VIEW_OFFSET ] = _T("オフセット");   //!< オフセット
    m_mapString[STR_VIEW_TRIM   ] = _T("トリム");       //!< トリム
    m_mapString[STR_VIEW_CORNER ] = _T("コーナー");     //!< コーナー
    m_mapString[STR_VIEW_CHAMFER] = _T("面取り");       //!< 面取り
    m_mapString[STR_VIEW_BREAK  ] = _T("分割");         //!< 分割

    m_mapString[STR_VIEW_COMPOSITION_LINE  ] = _T("複合線");        //!< 閉曲線
    m_mapString[STR_VIEW_CONNECTION_LINE  ] = _T("接続線");        //!< 閉曲線
    m_mapString[STR_VIEW_FILL  ] = _T("塗りつぶし");    //!< 塗りつぶし

    //------------------------
    //   EXEC_COMMON
    //------------------------
    m_mapString[STR_EXEC_EDB_FLOOW_SETTING ] = _T("全体の設定に合わせる");

    //-------------------------
    m_mapString[ STR_WND_OUTPUT      ] = _T("出力");
    m_mapString[ STR_WND_PROPERTIES  ] = _T("プロパティ");
    m_mapString[ STR_WND_LAYER       ] = _T("レイヤー");
    m_mapString[ STR_WND_ZOOM        ] = _T("ズーム");
    m_mapString[ STR_WND_IO_SET      ] = _T("IO設定");
    m_mapString[ STR_WND_IO_CONNECT  ] = _T("IO接続");
    m_mapString[ STR_WND_IO_REF      ] = _T("IO参照");
    m_mapString[ STR_WND_EXEC_MONITOR] = _T("実行モニター");
    m_mapString[ STR_WND_WATCH       ] = _T("ウォッチ");
    m_mapString[ STR_WND_CALL_STACK  ] = _T("コールスタック");
    m_mapString[ STR_WND_PROP_DEF    ] = _T("プロパティ定義");

    //------------------------
    // プロパティウインドウ
    m_mapString[ STR_PRO_FIG        ] = _T("図形");
    m_mapString[ STR_PRO_FIG_DEF    ] = _T("図形定義");

    m_mapString[ STR_PRO_TYPE       ] = _T("図形種別");
    m_mapString[ STR_PRO_POINT      ] = _T("点");
    m_mapString[ STR_PRO_CIRCLE     ] = _T("円");
    m_mapString[ STR_PRO_LINE       ] = _T("線");
    m_mapString[ STR_PRO_TEXT       ] = _T("文字");
    m_mapString[ STR_PRO_GROUP      ] = _T("グループ");
    m_mapString[ STR_PRO_PARTS      ] = _T("部品");
    m_mapString[ STR_PRO_REFERENCE  ] = _T("参照");
    m_mapString[ STR_PRO_COMPOSITION_LINE  ] = _T("複合線");
    m_mapString[ STR_PRO_SPLINE     ] = _T("スプライン");
    m_mapString[ STR_PRO_DIM        ] = _T("寸法");
    m_mapString[ STR_PRO_ELLIPSE    ] = _T("楕円");
    m_mapString[ STR_PRO_NODE       ] = _T("節点");
    m_mapString[ STR_PRO_CONNECTION_LINE  ] = _T("接続線");

    m_mapString[ STR_PRO_SCRIPT     ] = _T("スクリプト");
    m_mapString[ STR_PRO_USER       ] = _T("ユーザー定義");
    m_mapString[ STR_PRO_PROPERTY_SET_VAL] = _T("プロパティセット値");

    m_mapString[ STR_PRO_TAG        ] = _T("タグ");
    m_mapString[ STR_PRO_INFO_TAG   ] = _T("タグをカンマ区切りで設定します。図形の分類分けに使用します。");

    m_mapString[ STR_PRO_VISIBLE    ]  = _T("実行時表示");
    m_mapString[ STR_PRO_INFO_VISIBLE] = _T("実行時に図形を表示するかを設定します。");


    m_mapString[ STR_PRO_FIELD       ] = _T("フィールド");
    m_mapString[ STR_PRO_ELEMENT     ] = _T("エレメント");
    m_mapString[ STR_PRO_BOARD_IO    ] = _T("ボードIO");
    m_mapString[ STR_PRO_NET_IO      ] = _T("ネットワークIO");
    m_mapString[ STR_PRO_PARTS_DEF   ] = _T("部品定義");
    m_mapString[ STR_PRO_REFERENCE_DEF] = _T("参照定義");
    m_mapString[ STR_PRO_FIELF_DEF   ] = _T("フィールド定義");
    m_mapString[ STR_PRO_ELEMENT_DEF ] = _T("エレメント定義");

    m_mapString[ STR_PRO_INFO_TYPE  ] = _T("図形の種別を表します。");

    m_mapString[ STR_PRO_NAME       ] = _T("名称");
    m_mapString[ STR_PRO_INFO_NAME  ] = _T("図形に名称を設定します、重複する名称は指定できません。またディフォルトでは名称は設定されていません");

    m_mapString[ STR_PRO_COLOR      ] = _T("色");
    m_mapString[ STR_PRO_INFO_COLOR ] = _T("図形の色を設定します。");
    m_mapString[ STR_PRO_AUTOMATIC  ] = _T("自動");
    m_mapString[ STR_PRO_MORE_COLORS] = _T("その他の色");

    m_mapString[ STR_PRO_LAYER_ID   ] = _T("レイヤーID");
    m_mapString[ STR_PRO_INFO_LAYER_ID] = _T("レイヤーを変更します。");

    m_mapString[ STR_PRO_ID     ] = _T("識別ID");
    m_mapString[ STR_PRO_INFO_ID] = _T("図形を識別するためのIDです。編集はできません。");

    m_mapString[ STR_PROPWIN_X ] = _T("X座標");
    m_mapString[ STR_PROPWIN_Y ] = _T("Y座標");

    m_mapString[ STR_PRO_TOP     ] = _T("上座標");
    m_mapString[ STR_PRO_BOTTOM  ] = _T("下座標");
    m_mapString[ STR_PRO_LEFT    ] = _T("左座標");
    m_mapString[ STR_PRO_RIGHT   ] = _T("右座標");

    m_mapString[ STR_PRO_P1      ] = _T("端点１");
    m_mapString[ STR_PRO_INFO_P1 ] = _T("直線の端点を設定します。");

    m_mapString[ STR_PRO_P2      ] = _T("端点２");
    m_mapString[ STR_PRO_INFO_P2 ] = _T("直線の端点を設定します。");

    m_mapString[ STR_PRO_START     ] = _T("始点");
    m_mapString[ STR_PRO_INFO_START] = _T("始点を設定します。");

    m_mapString[ STR_PRO_END      ] = _T("終点");
    m_mapString[ STR_PRO_INFO_END ] = _T("終点を設定します。");

    m_mapString[ STR_PRO_ARROW_TYPE_START] = _T("始点矢印種別"); 
    m_mapString[ STR_PRO_INFO_ARROW_TYPE_START] = _T("始点側の矢印形状を設定します。");

    m_mapString[ STR_PRO_ARROW_TYPE_END] = _T("終点矢印種別"); 
    m_mapString[ STR_PRO_INFO_ARROW_TYPE_END] = _T("終点側の矢印形状を設定します。");

    m_mapString[ STR_PRO_LINE_FORM] = _T("線形状");
    m_mapString[ STR_PRO_INFO_LINE_FORM] = _T("接続線の形状を設定します。");

    m_mapString[ STR_PRO_LINE_TYPE ]      = _T("線種");
    m_mapString[ STR_PRO_INFO_LINE_TYPE ] = _T("線の種類を設定します。");

    m_mapString[ STR_PRO_LINE_WIDTH ]      = _T("線幅");
    m_mapString[ STR_PRO_INFO_LINE_WIDTH ] = _T("線の太さを設定します。");

    m_mapString[ STR_PRO_POS        ] = _T("位置");
    m_mapString[ STR_PRO_INFO_POS   ] = _T("位置を設定します。");

    m_mapString[ STR_PRO_CENTER     ] = _T("中心点");
    m_mapString[ STR_PRO_INFO_CENTER] = _T("中心点を設定します。");

    m_mapString[ STR_PRO_DATUM     ] = _T("基準点");
    m_mapString[ STR_PRO_INFO_DATUM] = _T("基準点を設定します。");

    m_mapString[ STR_PRO_RAD        ] = _T("半径");
    m_mapString[ STR_PRO_INFO_RAD   ] = _T("半径を設定します。");

    m_mapString[ STR_PRO_RAD1       ] = _T("径１");
    m_mapString[ STR_PRO_INFO_RAD1  ] = _T("角度に対応する軸の径を設定します。");

    m_mapString[ STR_PRO_RAD2       ] = _T("径２");
    m_mapString[ STR_PRO_INFO_RAD2  ] = _T("径１に対し直交(傾斜なしの時)する軸方向の径を設定します。");

    m_mapString[ STR_PRO_ANGLE      ] = _T("角度");
    m_mapString[ STR_PRO_INFO_ANGLE ] = _T("径１方向の角度を設定します。");

    m_mapString[ STR_PRO_SKEW       ] = _T("傾斜");
    m_mapString[ STR_PRO_INFO_SKEW  ] = _T("アフィン変換による傾斜(tan(x))を設定します。");


    m_mapString[ STR_PRO_CENTER_ANGLE      ] = _T("中心角");
    m_mapString[ STR_PRO_INFO_CENTER_ANGLE ] = _T("中心角を設定します。");

    m_mapString[ STR_PRO_ICON_INDEX]       = _T("アイコン番号");
    m_mapString[ STR_PRO_INFO_ICON_INDEX ] = _T("アイコンを選択します。");

    m_mapString[ STR_PRO_DRAW_INDEX]      = _T("図形番号");
    m_mapString[ STR_PRO_INFO_DRAW_INDEX] = _T("画像を分割した時の位置を指定する番号です。右上を0とし左へ進んで行きます。");

    m_mapString[ STR_PRO_ALPHA]      = _T("透過率");
    m_mapString[ STR_PRO_INFO_ALPHA] = _T("図形の透過率を設定します。 0が透明、255で不透明です。");

    m_mapString[ STR_PRO_WIDTH]      = _T("幅");
    m_mapString[ STR_PRO_INFO_WIDTH] = _T("図形の幅を設定します。");

    m_mapString[ STR_PRO_HEIGHT]      = _T("高さ");
    m_mapString[ STR_PRO_INFO_HEIGHT] = _T("図形の高さを設定します。");

    m_mapString[ STR_PRO_FILE_NAME]       = _T("ファイル名");
    m_mapString[ STR_PRO_INFO_FILE_NAME]  = _T(" 図形のファイル名です。");

    m_mapString[ STR_PRO_VALUE]      = _T("値");
    m_mapString[ STR_PRO_INFO_VALUE] = _T("値を設定します。");

    m_mapString[ STR_PRO_CTRL_POINT ]       = _T("制御点");
    m_mapString[ STR_PRO_INFO_CTRL_POINT ]  = _T("制御点を設定します。");

	m_mapString[STR_PRO_SPLINE_TYPE]      = _T("スプライン種別");
	m_mapString[STR_PRO_INFO_SPLINE_TYPE] = _T("スプラインの種別を設定します。");
	
	m_mapString[STR_PRO_EDIT_TYPE] = _T("形状設定方法");
	m_mapString[STR_PRO_INFO_EDIT_TYPE] = _T("スプラインの形状を設定するための方法を設定します");

	m_mapString[STR_PRO_SPLINE_CLOSE] = _T("閉曲線");
	m_mapString[STR_PRO_INFO_SPLINE_CLOSE] = _T("始点と終点をつなげ、閉じた曲線にします");

	m_mapString[ STR_PRO_START_ANGLE      ] = _T("開始角度");
    m_mapString[ STR_PRO_INFO_START_ANGLE ] = _T("弧の開始角度を設定します。角度は反時計回りです。");

    m_mapString[ STR_PRO_END_ANGLE       ] = _T("終了角度");
    m_mapString[ STR_PRO_INFO_END_ANGLE  ] = _T("弧の終了角度を設定します。角度は反時計回りです。");

    m_mapString[ STR_PRO_USE_COLOR     ] = _T("固有色");
    m_mapString[ STR_PRO_INFO_USE_COLOR] = _T("元の色を使用する場合は、FALSE ここで設定している色を使用する場合はTRUEを設定します。");

    m_mapString[ STR_PRO_OPERATION_INHERITANCE] = _T("操作継承"); 
    m_mapString[ STR_PRO_INFO_OPERATION_INHERITANCE] = _T(" 部品を貼り付けた部品でも，節点,文字入力、ボタンの操作を可能とします。"); 

    m_mapString[  STR_PRO_EXEC_THREAD_TYPE]       = _T("実行スレッド種別");
    m_mapString[  STR_PRO_INFO_EXEC_THREAD_TYPE]  = _T("実行に使用する、スレッドの種別を設定します");

    m_mapString[  STR_PRO_EXEC_CYCKE_TIME]        = _T("実行サイクルタイム");
    m_mapString[  STR_PRO_INFO_EXEC_CYCKE_TIME]   = _T("実行間隔をmsec単位で設定します。");

    m_mapString[  STR_PRO_EXEC_PRIORITY]        = _T("実行プライオリティ");
    m_mapString[  STR_PRO_INFO_EXEC_PRIORITY]   = _T("実行するタスクのプライオリティを設定します。1から5までで設定し、1が最も優先度が高くなります。");

    m_mapString[  STR_PRO_DEBUG_MODE]        = _T("実行デバッグモード設定");
    m_mapString[  STR_PRO_INFO_DEBUG_MODE]    = _T("実行時のデバッグモードを設定します。");

    m_mapString[  STR_PRO_DEFINTION_NAME]       = _T("定義名");
    m_mapString[  STR_PRO_INFO_DEFINTION_NAME]  = _T("定義元の名称を表示します。");

    m_mapString[  STR_PRO_AUTO_COMPILE]         = _T("自動コンパイル");
    m_mapString[  STR_PRO_INFO_AUTO_COMPILE]    = _T("編集時に使用する関数（OneditXXXX）を自動的にコンパイルします。");

    m_mapString[  STR_PRO_EDIT_PARMISSION]      = _T("編集許可");
    m_mapString[  STR_PRO_INFO_EDIT_PARMISSION] = _T("ファイル変更の許可・不許可を設定します。");

    m_mapString[  STR_PRO_CONNECT1]             = _T("始点接続先");
    m_mapString[  STR_PRO_INFO_CONNECT1]        = _T("始点の接続先の名称と接点のインデックス値を表示します。");

    m_mapString[  STR_PRO_CONNECT2]             = _T("終点接続先");
    m_mapString[  STR_PRO_INFO_CONNECT2]        = _T("終点の接続先の名称と接点のインデックス値を表示します。");

///////////////////////////////////////////////
//NODE

//    m_mapString[  STR_PRO_DRAGABLE]           = _T("ドラッグ可否");
//    m_mapString[  STR_PRO_INFO_DRAGABLE]      = _T("マーカのドラッグが可能かどうかを設定します。");

    m_mapString[  STR_PRO_MAX_CONNECT]        = _T("最大接続数");
    m_mapString[  STR_PRO_INFO_MAX_CONNECT]   = _T("１つのノードに対して接続できる物の数を設定します。負の値を設定すると無制限に接続できます。");

    m_mapString[  STR_PRO_CONNECTION_TYPE]        = _T("接続タイプ");
    m_mapString[  STR_PRO_INFO_CONNECTION_TYPE]   = _T("接続タイプを設定します。 Activeは自ら接続先を設定し、Passiveは、相手側から接続してもらいます。");

    m_mapString[  STR_PRO_CONNECTION_OBJECT]       = _T("接続対象");
    m_mapString[  STR_PRO_INFO_CONNECTION_OBJECT]  = _T("接続する対象を設定します。 Field, Edge, Objectから選択できます。");

    m_mapString[  STR_PRO_CONNECTION_DIR]         = _T("接続方向");
    m_mapString[  STR_PRO_INFO_CONNECTION_DIR]    = _T("接続に対する入出力方向を設定します。");

    m_mapString[  STR_PRO_CONNECTION_LINE_DIR]         = _T("接続線方向");
    m_mapString[  STR_PRO_INFO_CONNECTION_LINE_DIR]    = _T("接続線を接続する場合の接続線の方向を設定します。");

    m_mapString[  STR_PRO_UNIT_1]        = _T("単位Ch.1");
    m_mapString[  STR_PRO_UNIT_2]        = _T("単位Ch.2");
    m_mapString[  STR_PRO_UNIT_3]        = _T("単位Ch.3");
//    m_mapString[  STR_PRE_UNIT_KIND ]  = _T("物理量");
//    m_mapString[  STR_PRE_UNIT ]       = _T("単位");
    m_mapString[  STR_PRO_INFO_UNIT_KIND]    = _T("物理量を設定します。設定した物理量によって使用可能な単位が変化します。");
    m_mapString[  STR_PRO_INFO_UNIT]         = _T("単位を設定します。");

    m_mapString[  STR_PRO_MARKER]                    = _T("マーカ");

    m_mapString[  STR_PRO_MARKER_SHAPE]               = _T("形状");
    m_mapString[  STR_PRO_INFO_MARKER_SHAPE]          = _T("マーカの形状を設定します。");

    m_mapString[  STR_PRO_SET_NAME]          = _T("プロパティセット名");
    m_mapString[  STR_PRO_INFO_SET_NAME]     = _T("プロパティセットを設定します。使用しなときはNONEを選択します。");

    //マーカー形状
    m_mapString[  STR_DMS_CIRCLE_FILL]    = _T("円塗りつぶし");
    m_mapString[  STR_DMS_CIRCLE]         = _T("円");            
    m_mapString[  STR_DMS_RECT_FILL]      = _T("四角塗りつぶし");
    m_mapString[  STR_DMS_RECT]           = _T("四角");          
    m_mapString[  STR_DMS_DBL_CIRCLE]     = _T("二重丸");        
    m_mapString[  STR_DMS_DBL_RECT]       = _T("二重角"); 

    //マーカー表示種別
    m_mapString[  STR_NDT_SHOW_ALWAYS]          = _T("常に表示する");  
    m_mapString[  STR_NDT_SHOW_NODE_OPERATION]  = _T("ノード操作状態のみ");  

    m_mapString[  STR_PRO_MARKER_COLOR]               = _T("塗りつぶし色");
    m_mapString[  STR_PRO_MARKER_INFO_COLOR]          = _T("マーカ塗りつぶし時の色を設定します。");

    m_mapString[  STR_PRO_MARKER_MOVE_TYPE]           = _T("移動制限");
    m_mapString[  STR_PRO_INFO_MARKER_MOVE_TYPE]      = _T("マーカの移動方向の制限を設定します。");
    
    //移動制限
    m_mapString[  STR_DMT_NOMOVE]             = _T("移動なし");
    m_mapString[  STR_DMT_ALL_MOVE]           = _T("全方向移動可能");
    m_mapString[  STR_DMT_LIMITED_DIRECTION]  = _T("1方向移動可能");
    m_mapString[  STR_DMT_LIMITED_DISTANCE]   = _T("距離固定");

    //接続線方向 
    m_mapString[  STR_DML_ALL]              = _T("全方向");
    m_mapString[  STR_DML_OBJ]              = _T("近傍境界線");
    m_mapString[  STR_DML_TOP]              = _T("上");
    m_mapString[  STR_DML_RIGHT]            = _T("右");
    m_mapString[  STR_DML_BOTTOM]           = _T("下");
    m_mapString[  STR_DML_LEFT]             = _T("左");
    m_mapString[  STR_DML_DIRX]             = _T("左右");
    m_mapString[  STR_DML_DIRY]             = _T("上下");


    m_mapString[  STR_PRO_MARKER_CORD]                = _T("座標系");
    m_mapString[  STR_PRO_INFO_MARKER_CORD]           = _T("マーカーの移動方向の座標系を設定します。");
    m_mapString[  STR_PRO_MARKER_DIR]                 = _T("マーカ方向");
    m_mapString[  STR_PRO_INFO_MARKER_DIR]            = _T("移動制限時のマーカの移動方向を設定します。");
    m_mapString[  STR_PRO_MARKER_DISP_TYPE]           = _T("表示条件種別");
    m_mapString[  STR_PRO_INFO_MARKER_DISP_TYPE]      = _T("マーカーの表示条件を設定します。");

    //プロパティセット
    m_mapString[  STR_PRO_PROPERTY_SET]       = _T("プロパティセット");
    m_mapString[  STR_PRO_INFO_PROPERTY_SET]  = _T("同一のプロパティセットを持つ図形と接続できます。");
    m_mapString[  STR_PROPERTY_SET_INPUT]     = _T("プロパティセット名を入力してください");


    m_mapString[ STR_PRO_CONNECTION]       = _T("接続先");
    m_mapString[ STR_PRO_INFO_CONNECTION]  = _T("ノードの接続先を設定します。");

    m_mapString[ STR_PRO_NODE_SNAP]        = _T("スナップタイプ");
    m_mapString[ STR_PRO_INFO_NODE_SNAP]   = _T("ノード移動時のスナップタイプを設定します。");

    m_mapString[ STR_PRO_CONNECT_PROPERTYSET]        = _T("接続プロパティセット");
    m_mapString[ STR_PRO_INFO_CONNECT_PROPERTYSET]   = _T("接続可能なプロパティセットを設定します。");

    m_mapString[ STR_PRO_NODE_BASE_POINT]  = _T("基点");
    m_mapString[ STR_PRO_INFO_NODE_BASE_POINT]  = _T("スナップタイプが、角度,距離場合の基準となるノードの名称を設定します。");

    //
    m_mapString[ STR_PRO_FILL_COLOR]        = _T("塗りつぶし色");
    m_mapString[ STR_PRO_INFO_FILL_COLOR]   = _T("塗りつぶしを行う色を設定します。");

    m_mapString[ STR_PRO_FILL]       = _T("有無");
    m_mapString[ STR_PRO_INFO_FILL]  = _T("塗りつぶしの有無を設定します。");



///////////////////////////////////////////////
//DIM

    m_mapString[ STR_PRO_DIM_TYPE]              = _T("寸法種別"); 
    m_mapString[ STR_PRO_INFO_DIM_TYPE]         = _T("寸法の種別を設定します。");

    m_mapString[ STR_PRO_DIM_ARROW_TYPE]        = _T("矢印種別");
    m_mapString[ STR_PRO_INFO_DIM_ARROW_TYPE]   = _T("矢印の種別を設定します。");

    m_mapString[ STR_PRO_DIM_ARROW_DIR]         = _T("矢印向き");
    m_mapString[ STR_PRO_INFO_DIM_ARROW_DIR]    = _T("矢印の向きを設定します");

    m_mapString[ STR_PRO_DIM_PRIFIX]            = _T("接頭語");
    m_mapString[ STR_PRO_INFO_DIM_PRIFIX]       = _T("表示するテキストの前に追加する文字を設定します。");

    m_mapString[ STR_PRO_DIM_SUFFIX]            = _T("接尾語");
    m_mapString[ STR_PRO_INFO_DIM_SUFFIX]       = _T("表示するテキストの後に追加する文字を設定します。");

    m_mapString[ STR_PRO_DIM_TEXT]              = _T("表示テキスト");
    m_mapString[ STR_PRO_INFO_DIM_TEXT]         = _T("表示するテキストを設定します\n設定した場合自動寸法記入は解除されます。");

    m_mapString[ STR_PRO_DIM_AUTO]              = _T("自動寸法記入");
    m_mapString[ STR_PRO_INFO_DIM_AUTO]         = _T("寸法を自動で記入します。");

    m_mapString[ STR_PRO_DIM_TEXT_POS]          = _T("文字位置");
    m_mapString[ STR_PRO_INFO_DIM_TEXT_POS]     = _T("文字位置を寸法線の上が下(左か右）を設定します。");

    m_mapString[ STR_PRO_DIM_TEXT_DIR]          = _T("文字向き");
    m_mapString[ STR_PRO_INFO_DIM_TEXT_DIR]     = _T("文字位置を正方向か逆方向か設定します。");

    m_mapString[ STR_PRO_DIM_TEXT_GAP]          = _T("文字隙間");
    m_mapString[ STR_PRO_INFO_DIM_TEXT_GAP]     = _T("寸法から文字下部までの距離を設定します。");

    m_mapString[ STR_PRO_DIM_TEXT_SIZE]         = _T("文字サイズ");
    m_mapString[ STR_PRO_INFO_DIM_TEXT_SIZE]    = _T("文字サイズを設定します。");


    m_mapString[ STR_PRO_DIM_EXT_LINE1]         = _T("寸法補助線1");
    m_mapString[ STR_PRO_INFO_DIM_EXT_LINE1]    = _T("寸法補助線1の表示の有無を設定します。");

    m_mapString[ STR_PRO_DIM_EXT_LINE2]         = _T("寸法補助線2");
    m_mapString[ STR_PRO_INFO_DIM_EXT_LINE2]    = _T("寸法補助線2の表示の有無を設定します。");

    m_mapString[ STR_PRO_DIM_POS1]              = _T("寸法位置1");
    m_mapString[ STR_PRO_INFO_DIM_POS1]         = _T("寸法位置1を設定します。");

    m_mapString[ STR_PRO_DIM_POS2]              = _T("寸法位置2");
    m_mapString[ STR_PRO_INFO_DIM_POS2]         = _T("寸法位置2を設定します。");

    m_mapString[ STR_PRO_DIM_PRECISION]         = _T("小数点以下の桁数");
    m_mapString[ STR_PRO_INFO_DIM_PRECISION]    = _T("小数点以下の桁数を設定します。");

    ///////////////////////////////////////////////
    m_mapString[ STR_PRO_IMAGE_SET]             = _T("画像設定");
    m_mapString[ STR_PRO_INFO_IMAGE_SET]        = _T("画像で使用可能な画像を設定します。");

    m_mapString[ STR_PRO_USE_NODE_BOUNDS]       = _T("バウンディングボックス使用有無");
    m_mapString[ STR_PRO_INFO_USE_NODE_BOUNDS]  = _T("図形が再編集状態(選択状態の図形をもう一度クリックする)でバウンディングボックスの表示の有無を設定します。");

    m_mapString[ STR_PRO_IGNORE_SEL_PART]       = _T("範囲選択設定無視");
    m_mapString[ STR_PRO_INFO_IGNORE_SEL_PART]  = _T("ストレッチモード選択時、矩形選択範囲条件をを強制的に「すべて矩形内に存在する必要がある」ことにする");

	m_mapString[STR_PRO_DEFAULT_SET]			= _T("既定値設定"); 
	m_mapString[STR_PRO_INFO_DEFAULT_SET]		= _T("現在の設定を既定値として設定、または解除します");
	m_mapString[STR_INIT_DEFAULT_SET]			= _T("既定値として設定");

    ///////////////////////////////////////////////

//FILL
    m_mapString[ STR_PRO_FILL_TYPE]             = _T("塗りつぶし種別");
    m_mapString[ STR_PRO_INFO_FILL_TYPE]        = _T("塗りつぶしの有無、方法を設定します。");

    m_mapString[ STR_PRO_FILL_COLOR1]            = _T("塗りつぶし色");
    m_mapString[ STR_PRO_INFO_FILL_COLOR1]       = _T("塗りつぶしの色を設定します。");

    m_mapString[ STR_PRO_HATCHING_TYPE]         = _T("ハッチング種別");
    m_mapString[ STR_PRO_INFO_HATCHING_TYPE]    = _T("ハッチングのを設定します。");

    m_mapString[ STR_PRO_FILL_COLOR2]           = _T("グラデーション移行色");
    m_mapString[ STR_PRO_INFO_FILL_COLOR2]      = _T("グラデーションで変化する色を設定します。");

    m_mapString[ STR_PRO_GRADATION_DIR]         = _T("グラデーション方向");
    m_mapString[ STR_PRO_INFO_GRADATION_DIR]    = _T("グラデーションの方向を設定します。");

    m_mapString[ STR_FILL_NONE]        = _T("塗りつぶしなし");
    m_mapString[ STR_FILL_SOLID]       = _T("塗りつぶし");
    m_mapString[ STR_FILL_HATCHING]    = _T("ハッチング");
    m_mapString[ STR_FILL_GRADATION]   = _T("グラデーション");

    m_mapString[ STR_HATCH_HORIZONTAL]  = _T("水平");
    m_mapString[ STR_HATCH_VERTICAL]    = _T("垂直");
    m_mapString[ STR_HATCH_BDIAGONAL]   = _T("左上から右下");
    m_mapString[ STR_HATCH_FDIAGONAL]   = _T("左下から右上");
    m_mapString[ STR_HATCH_CROSS]       = _T("十字");
    m_mapString[ STR_HATCH_DIAGCROSS]   = _T("Ｘ字");


//PartsDef
    m_mapString[ STR_PRO_PRINT]                 = _T("印刷");

    m_mapString[ STR_PRO_SHOW_PRINT_AREA]       = _T("印刷範囲表示"); 
    m_mapString[ STR_PRO_INFO_SHOW_PRINT_AREA]  = _T("印刷範囲表示を表示します。");
    m_mapString[ STR_PRO_PRINT_ROW]             = _T("印刷行設定"); 
    m_mapString[ STR_PRO_INFO_PRINT_ROW]        = _T("複数印刷するときの行を設定します。\n行、列の順で印刷します。");
    m_mapString[ STR_PRO_PRINT_COL]             = _T("印刷列設定");  
    m_mapString[ STR_PRO_INFO_PRINT_COL]        = _T("複数印刷するときの列を設定します。\n行、列の順で印刷します。");
    m_mapString[ STR_PRO_DISP_PAPER]            = _T("用紙枠表示");  
    m_mapString[ STR_PRO_INFO_DISP_PAPER]       = _T("用紙サイズを表示します\n印刷範囲表示時のみ機能します。");
    m_mapString[ STR_PRO_OFFSET_EQ_PAPER]       = _T("自動印刷オフセット");
    m_mapString[ STR_PRO_INFO_OFFSET_EQ_PAPER]  = _T("印刷行・列に対するオフセット量を用紙サイズに合わせます。");
    m_mapString[ STR_PRO_PRINT_OFFSET_ROW]      = _T("行印刷オフセット");   
    m_mapString[ STR_PRO_INFO_PRINT_OFFSET_ROW] = _T("印刷行に対するオフセット量を設定します。\n自動印刷オフセットがFalseの時機能します。");
    m_mapString[ STR_PRO_PRINT_OFFSET_COL]      = _T("列印刷オフセット");   
    m_mapString[ STR_PRO_INFO_PRINT_OFFSET_COL] = _T("印刷列に対するオフセット量を設定します。\n自動印刷オフセットがFalseの時機能します。");

    m_mapString[ STR_PRO_PAPER]                 = _T("用紙");
    m_mapString[ STR_PRO_PAPER_WIDRH]                 = _T("用紙 幅");
    m_mapString[ STR_PRO_INFO_PAPER_WIDRH]            = _T("用紙の幅を表示します。");
    m_mapString[ STR_PRO_PAPER_HEIGHT]                = _T("用紙 高さ");
    m_mapString[ STR_PRO_INFO_PAPER_HEIGHT]           = _T("用紙の高さを表示します。");
    m_mapString[ STR_PRO_PRINT_WIDTH]                 = _T("印字 幅");
    m_mapString[ STR_PRO_INFO_PRINT_WIDTH]            = _T("印刷可能範囲の幅を表示します");
    m_mapString[ STR_PRO_PRINT_HEIGHT]                = _T("印字 高さ");
    m_mapString[ STR_PRO_INFO_PRINT_HEIGHT]           = _T("印刷可能範囲の高さを表示します");
    m_mapString[ STR_PRO_MARGIN_TOP]                  = _T("マージン 上");
    m_mapString[ STR_PRO_INFO_MARGIN_TOP]             = _T("上方向の印刷不可能範囲を表示します。");
    m_mapString[ STR_PRO_MARGIN_LEFT]                 = _T("マージン 左");
    m_mapString[ STR_PRO_INFO_MARGIN_LEFT]            = _T("左方向の印刷不可能範囲を表示します。");
    
//------------------------
    //編集許可
    m_mapString[  STR_EP_NONE]          = _T("制限なし");
    m_mapString[  STR_EP_SOURCE]        = _T("ソース変更禁止");
    m_mapString[  STR_EP_DESIGN]        = _T("デザイン変更禁止");
    m_mapString[  STR_EP_PROPERTY]      = _T("プロパティ変更禁止");
    m_mapString[  STR_EP_READONRY]      = _T("変更禁止");
    m_mapString[  STR_EP_ENCRYPTION]    = _T("ソースコード暗号化");

//------------------------
    m_mapString[ STR_SEL_POINT  ] = _T("任意の位置、点をクリックしてください");
    m_mapString[ STR_SEL_ANGLE  ] = _T("角度を入力してください");
    m_mapString[ STR_SEL_POINT1 ] = _T("始点を設定します。 任意の位置、点 または図形を選択してください");
    m_mapString[ STR_SEL_POINT2 ] = _T("終点を設定します。 クリック、もしくは 「距離a角度」を入力してくだい");
    m_mapString[ STR_SEL_MARKER ] = _T("マーカを選択で、 点の選択が行えます");

    m_mapString[ STR_SEL_CIRCLE_CENTER ] = _T("円の中心点または円が接する、直線、円を選択してください");
    m_mapString[ STR_SEL_CIRCLE_CENTER1] = _T("円の半径を選択するか入力してください");

    m_mapString[ STR_SEL_CIRCLE_OBJ ] = _T("円の中心点を選択するか半径を入力してください。または、円に接する図形を選択してください");
    m_mapString[ STR_SEL_CIRCLE_LLL ] = _T("３直線に接する円を選択してください");
    m_mapString[ STR_SEL_CIRCLE_CCC ] = _T("３円に接する円を選択してください");

    m_mapString[ STR_SEL_ELLIPSE_CENTER  ] = _T("楕円の中心点を選択してください。");

    m_mapString[ STR_SEL_RAD    ] = _T("半径を設定します。  半径を入力するか、任意の位置、点 または円に接する図形を 選択してください");

    m_mapString[ STR_SEL_3P_1    ] = _T("通過点１を選択してください");
    m_mapString[ STR_SEL_3P_2    ] = _T("通過点２を選択してください");
    m_mapString[ STR_SEL_3P_3    ] = _T("通過点３を選択してください");

    m_mapString[ STR_SEL_OBJECT ] = _T("図形を選択してください。");
    m_mapString[ STR_SEL_OBJECT1 ] = _T("図形1を選択してください");
    m_mapString[ STR_SEL_OBJECT2 ] = _T("図形2を選択してください");
    m_mapString[ STR_SEL_CREATE_POINT] = _T("任意の位置、図形を選択 又は、X座標, Y座標を入力してください。");

    //Offset
    m_mapString[ STR_SEL_OFFSET ]  = _T("オフセット量を入力、または図形を選択してください。");
    m_mapString[ STR_SEL_OFFSET_DIR ] = _T("オフセット方向を選択してください");

    //m_mapString[ STR_SEL_R ]          = _T("半径を入力してください");
    //m_mapString[ STR_SEL_C ]          = _T("面取長を入力してください");
    //m_mapString[ STR_SEL_BREAK_POINT] = _T("分割位置を選択してください");

    //Move
    m_mapString[ STR_SEL_MOVE_POINT1 ] = _T("移動の基準となる点を選択してください");
    m_mapString[ STR_SEL_MOVE_POINT2 ] = _T("移動先の点を選択してください");

    //Copy
    m_mapString[ STR_SEL_COPY_POINT1 ] = _T("複写元の基準となる点を選択してください");
    m_mapString[ STR_SEL_COPY_POINT2 ] = _T("複写先の点を選択してください");

    //ROTATE
    m_mapString[ STR_SEL_ROTATE_CENTER] = _T("回転の中心となる点を選択してください");
    m_mapString[ STR_SEL_ROTATE_ANGLE ] = _T("回転角を入力するか、回転の基準となる線を選択してください");
    m_mapString[ STR_SEL_ROTATE_ANGLE2] = _T("回転の基準からの回転角を入力するか、回転角となる線を選択してください");

    //Mirror
    m_mapString[ STR_SEL_MIRROR_LINE] = _T("鏡像の対称軸となる直線を選択してください");

    //Scl
    m_mapString[ STR_SEL_SCL_CENTER] = _T("拡大、縮小の中心となる点を選択してください");
    m_mapString[ STR_SEL_SCL]        = _T("倍率を入力してください  倍率,[Y軸方向倍率]");

    //TRIM
    m_mapString[ STR_SEL_TRIM ]    = _T("調整を行う図形を選択してください");
    m_mapString[ STR_SEL_TRIM_OBJ] = _T("調整に用いる図形を選択してください");

    //Break
    m_mapString[ STR_SEL_BREAK ]    = _T("分割を行う図形を選択してください");
    m_mapString[ STR_SEL_BREAK_OBJ] = _T("分割に用いる図形、または分割を行う位置を選択してください");

    //Corner
    m_mapString[ STR_SEL_CORNER1 ]    = _T("角を作成する図形１を選択してください");
    m_mapString[ STR_SEL_CORNER2 ]    = _T("角を作成する図形２を選択してください");
    m_mapString[ STR_SEL_CORNER_RAD ] = _T("半径を入力してください。面取りの場合は数値の前に”C”を入力してください");

    //Text
    m_mapString[ STR_SEL_TEXT_POINT ] = _T("文字を入力する位置を指定するか、既に作成された文字を選択してください");

    //Group
    m_mapString[ STR_SEL_GROUP_CENTER] = _T("基準点を選択してください");

    //ELLIPSE
    m_mapString[ STR_SEL_ELLIPSE_VAL] = _T("長径, 短径[, 角度] を入力してください");
    
    //SPLINE
    m_mapString[ STR_SEL_SPLINE_POINT] = _T("スプラインを通る点を選択してください。終点をクリックすると終了します");
    m_mapString[ STR_DLG_SPLINE_REG] = _T("スプラインを登録しますか？");

    //LOOP
    m_mapString[ STR_SEL_COMPOSITION_LINE ]          = _T("追加する図形を選択してください");
    m_mapString[ STR_DLG_LOOP_REG ]      = _T("図形を登録しますか？");
    m_mapString[ STR_DLG_DEL_ORG ]       = _T(" 元図形を削除しますか？;");

    //
    m_mapString[ STR_SEL_TMP] = _T("説明内容が決まっていません");

    //線種
    m_mapString[ STR_PRO_LINE_SOLID]      = _T("実線");
    m_mapString[ STR_PRO_LINE_DASH]       = _T("破線");
    m_mapString[ STR_PRO_LINE_DOT]        = _T("点線");
    m_mapString[ STR_PRO_LINE_DASHDOT]    = _T("１点鎖線");
    m_mapString[ STR_PRO_LINE_DASHDOTDOT] = _T("２点鎖線");

    //線幅
    m_mapString[ STR_ID_LINE_WIDTH_1]      = _T("線幅1");
    m_mapString[ STR_ID_LINE_WIDTH_2]      = _T("線幅2");
    m_mapString[ STR_ID_LINE_WIDTH_3]      = _T("線幅3");
    m_mapString[ STR_ID_LINE_WIDTH_4]      = _T("線幅4");
    m_mapString[ STR_ID_LINE_WIDTH_5]      = _T("線幅5");
    m_mapString[ STR_ID_LINE_WIDTH_6]      = _T("線幅6");
    m_mapString[ STR_ID_LINE_WIDTH_7]      = _T("線幅7");
    m_mapString[ STR_ID_LINE_WIDTH_8]      = _T("線幅8");
    m_mapString[ STR_ID_LINE_WIDTH_9]      = _T("線幅9");
    m_mapString[ STR_ID_LINE_WIDTH_10]     = _T("線幅10");


    //接続線形状
    m_mapString[ STR_PRO_LINE_FORM_STRAIGHT] = _T("直線");
    m_mapString[ STR_PRO_LINE_FORM_MULTI]    = _T("折れ線");
    m_mapString[ STR_PRO_LINE_FORM_SPLINE]   = _T("曲線");

    //------------------------


    //------------------------
    m_mapString[ STR_ERROR_INTERSECT] = _T("交点が存在しません");
    m_mapString[ STR_ERROR_CORNER   ] = _T("角を作成できません");
    m_mapString[ STR_ERROR_BREAK    ] = _T("分割できない位置を指定しています");

    m_mapString[ STR_ERROR_SELECT_BEFORE_OBJECT] = _T("先に図形を選択してください");
    m_mapString[ STR_ERROR_SELECT_OFFSET_OBJECT] = _T("オフセット値を入力するか、オフセット位置(点)をクリックしてください");

    m_mapString[ STR_ERROR_SELECT_BEFORE_CENTER] = _T("先に中心点を選択してください");
    m_mapString[ STR_ERROR_CREATE_OBJECT]        = _T("図形が作成できません");

    m_mapString[ STR_ERROR_INVALID_NUMBER]       = _T("有効な数値ではありません");
    m_mapString[ STR_ERROR_INVALID_FONTSIZE]     = _T("フォントサイズは１〜 1638 までの範囲です");

    m_mapString[ STR_ERROR_INPUT_STRING]     = _T("文字を入力してください");

    m_mapString[ STR_ERROR_SAME_STRING]     = _T("既に設定されている文字があります");

    m_mapString[ STR_ERROR_INVALID_STRING]  = _T("使用できない文字\n「」が含まれています。");

    m_mapString[ STR_ERROR_COPY_FAIL]  = _T("コピーに失敗しました");

    m_mapString[ STR_ERROR_SET_BEFORE_TYPE]    = _T("先に種別を設定してください");

    m_mapString[ STR_ERROR_CANNOT_CREATE_DIR]  = _T("ディレクトリが作成できませんでした");

    m_mapString[ STR_ERROR_OPEN_FILE]          = _T("ファイルを開くことができませんでした");

    m_mapString[ STR_ERROR_FILE_LOAD    ]      = _T("ファイルの読み込みに失敗しました");

    m_mapString[ STR_ERROR_OBJECT_NAME ]  = _T("使用できない文字が設定されています\n名称は、アルファベット若しくはは＿（アンダースコア）から始まり 数字、アンダースコア、アルファベットから成る文字列を設定してください");

    m_mapString[ STR_ERROR_ANGLE_VAL ]  = _T("角度[, 複写回数 ]を入力してください");

    m_mapString[ STR_ERROR_MORE_REDO]  = _T("これ以上やり直しはできません");

    m_mapString[ STR_ERROR_MAX_MIN]   = _T("最大値 >= 最小値 となるように設定してください");

    m_mapString[ STR_ERROR_SET_TYPE]  = _T("型の設定に失敗しました");

    m_mapString[ STR_ERROR_SET_NAME]  = _T("名称を設定してください");

    m_mapString[ STR_ERROR_FILE_LOAD_AND_DELETE] = _T("ファイルの読み込みに失敗しました。\nリストから除外しますか");

    //------------------------
    //------------------------
    m_mapString[ STR_MNU_SET_FORMATTING ] = _T("文字フォーマット");
    m_mapString[ STR_MNU_SET_DRAWING    ] = _T("描画");
    m_mapString[ STR_MNU_SET_CUSTMIZE   ]  = _T("ユーザー設定");

    m_mapString[ STR_MNU_SET_TEXT_COLORS_ITEM]  = _T("文字色...");
    m_mapString[ STR_MNU_SET_LINE_COLORS_ITEM]  = _T("線色...");
    m_mapString[ STR_MNU_SET_FILL_COLORS_ITEM]  = _T("塗りつぶし色...");

    m_mapString[ STR_MNU_SET_AUTOMATIC  ]  = _T("自動");
    m_mapString[ STR_MNU_SET_MORE_COLORS]  = _T("その他の色");
    m_mapString[ STR_MNU_SET_TEXT_COLORS]  = _T("文字の色");
    m_mapString[ STR_MNU_SET_LINE_COLORS]  = _T("線の色");
    m_mapString[ STR_MNU_SET_FILL_COLORS]  = _T("塗りつぶしの色");


    //------------------------
    //レイヤウインドウ
    m_mapString[ STR_LAYER_COLOR]  = _T("色");
    m_mapString[ STR_LAYER_NAME ]  = _T("名称");
    m_mapString[ STR_LAYER_SCL  ]  = _T("倍率");

    //------------------------
    //操作ウインドウ
    m_mapString[ STR_OBJECT_VIEW ]  = _T("オブジェクトビュー");
    m_mapString[ STR_ELEMENT_VIEW]  = _T("エレメントビュー");
    m_mapString[ STR_LIB_VIEW    ]  = _T("ライブラリビュー");

    //ファイル操作
    m_mapString[ STR_FILE_SAVEFILE        ]  = _T( "名前を付けて保存");
    m_mapString[ STR_FILE_SAVEFILECOPY    ]  = _T( "名前を付けて保存");
    m_mapString[ STR_FILE_ALLFILTER       ]  = _T( "すべてのファイル (*.*)");
    m_mapString[ STR_FILE_SAK_TO_SAVE     ]  = _T("%s への変更を保存しますか?");
    m_mapString[ STR_FILE_LOAD_PROJECT    ]  = _T("プロジェクトファイル");
    m_mapString[ STR_FILE_NEW             ]  = _T("新規ファイル");
    m_mapString[ STR_FILE_OPEN            ]  = _T("開く");
    m_mapString[ STR_FILE_CLOSE           ]  = _T("閉じる");
    m_mapString[ STR_FILE_SAVE            ]  = _T("保存");
    m_mapString[ STR_FILE_SAVE_AS         ]  = _T("名前をつけて保存");

    //タブグループ
    m_mapString[ STR_MNU_MDITABS           ]  = _T("タブグループ");
    m_mapString[ STR_MNU_DROP_MDITABS      ]  = _T("タブグループ");
    m_mapString[ STR_MDI_TABBED_DOCUMENT   ]  = _T("タブ付きドキュメント");
    m_mapString[ STR_MDI_NEW_HORZ_TAB_GROUP]  = _T("水平タブグループの新規作成");
    m_mapString[ STR_MDI_NEW_VERT_GROUP    ]  = _T("垂直タブグループの新規作成");
    m_mapString[ STR_MDI_MOVE_TO_NEXT_GROUP]  = _T("次のタブグループへ移動");
    m_mapString[ STR_MDI_MOVE_TO_PREV_GROUP]  = _T("前のタブグループへ移動");
    m_mapString[ STR_MDI_CLOSE             ]  = _T("閉じる");

    //------------------------
    //オブジェクトビュー ポップアップメニュー
    m_mapString[ STR_MNU_OBJ_FOLDER ]  = _T("フォルダー作成");
    m_mapString[ STR_MNU_OBJ_NAME   ]  = _T("名称変更作成");
    m_mapString[ STR_MNU_OBJ_PARTS  ]  = _T("部品作成");
    m_mapString[ STR_MNU_OBJ_REFERENCE  ]  = _T("参照作成");
    m_mapString[ STR_MNU_OBJ_FIELD  ]  = _T("フィールド作成");
    m_mapString[ STR_MNU_OBJ_ELEMENT]  = _T("エレメント作成");
    m_mapString[ STR_MNU_OBJ_SAVE   ]  = _T("保存");
    m_mapString[ STR_MNU_OBJ_SCRIPT ]  = _T("スクリプト作成");

    m_mapString[ STR_MNU_OBJ_CUT    ]  = _T("切り取り");
    m_mapString[ STR_MNU_OBJ_COPY   ]  = _T("コピー");
    m_mapString[ STR_MNU_OBJ_PASTE  ]  = _T("貼り付け");
    m_mapString[ STR_MNU_OBJ_DELETE ]  = _T("削除");

    //オブジェクトビュー フォルダー名入力ダイアログ
    m_mapString[ STR_DLG_FOLDER_TITLE ]  = _T("フォルダー作成");
    m_mapString[ STR_DLG_FOLDER_INFO  ]  = _T("フォルダー名を入力してください");

    //オブジェクトビュー 部品名入力ダイアログ   
    m_mapString[ STR_DLG_PARTS_TITLE  ]  = _T("部品作成");
    m_mapString[ STR_DLG_PARTS_INFO   ]  = _T("部品名を入力してください");
    m_mapString[ STR_DLG_PARTS_ERROR  ]  = _T("部品生成に失敗しました");

    m_mapString[ STR_DLG_ELEMENT_TITLE ]  = _T("要素作成");
    m_mapString[ STR_DLG_ELEMENT_INFO  ]  = _T("要素名を入力してください");
    m_mapString[ STR_DLG_ELEMENT_ERROR ]  = _T("要素生成に失敗しました");

    m_mapString[ STR_DLG_FIELD_TITLE ]  = _T("フィールド作成");
    m_mapString[ STR_DLG_FIELD_INFO  ]  = _T("フィールド名を入力してください");
    m_mapString[ STR_DLG_FIELD_ERROR ]  = _T("フィールド生成に失敗しました");

    m_mapString[ STR_DLG_REFERENCE_TITLE ]  = _T("参照作成");
    m_mapString[ STR_DLG_REFERENCE_INFO  ]  = _T("参照名を入力してください");
    m_mapString[ STR_DLG_REFERENCE_ERROR ]  = _T("参照生成に失敗しました");

    //オブジェクトビュー スクリプト名入力ダイアログ   
    m_mapString[ STR_DLG_SCRIPT_TITLE  ]  = _T("スクリプト作成");
    m_mapString[ STR_DLG_SCRIPT_INFO   ]  = _T("スクリプトを入力してください");
    m_mapString[ STR_DLG_SCRIPT_ERROR  ]  = _T("スクリプト生成に失敗しました");

    //オブジェクトビュー コピー名入力ダイアログ   
    m_mapString[ STR_DLG_COPY_TITLE  ]  = _T("コピー作成");
    m_mapString[ STR_DLG_COPY_INFO  ]   = _T("コピー先の名称を入力してください");

    //=================
    //View メニュー項目
    //=================
    //ファイル
    m_mapString[ STR_MNU_FILE   ]       =_T("ファイル(&F)");

        m_mapString[ STR_MNU_PROJECT_NEW   ]    =_T("新規プロジェクトの作成");
        m_mapString[ STR_MNU_PROJECT_OPEN  ]    =_T("プロジェクトを開く");
    
        m_mapString[ STR_MNU_FILE_CLOSE ]       =_T("閉じる(&C)");
        m_mapString[ STR_MNU_PROJECT_CLOSE ]    =_T("プロジェクトを閉じる");

        m_mapString[ STR_MNU_SAVE_FILE ]            =_T("%sの保存");
        m_mapString[ STR_MNU_SAVE_AS_FILE ]         =_T("%sに名前をつけけて保存");
        m_mapString[ STR_MNU_SAVE_FILE_NONAME ]     =_T("ファイルの保存");
        m_mapString[ STR_MNU_SAVE_AS_FILE_NONAME ]  =_T("名前をつけけて保存");
        m_mapString[ STR_MNU_SAVE_ALL ]             =_T("すべてを保存");

        m_mapString[ STR_MNU_FILE_PRINT ]           =_T("印刷(&P)...");
        m_mapString[ STR_MNU_FILE_PRINT_PREVIEW ]   =_T("印刷プレビュー(&V)");
        m_mapString[ STR_MNU_FILE_PRINT_SETUP]      =_T("プリンタの設定(&R)...");

        m_mapString[ STR_MNU_FILE_MRU_FILE]       =_T("最近使ったファイル");
        m_mapString[ STR_MNU_PROJECT_MRU_FILE]    =_T("最近使ったプロジェクト");

        m_mapString[ STR_MNU_APP_EXIT   ]       =_T("アプリケーションの終了(&X)");

        m_mapString[ STR_MNU_FILE_NEW   ]       =_T("新規作成(&N)");
        m_mapString[ STR_MNU_FILE_OPEN  ]       =_T("開く(&O)...");

   //表示
   m_mapString[ STR_MNU_VIEW   ]               =_T("表示(&V)");
        m_mapString[ STR_MNU_TOOLBAR_WINDOW]        =_T("ツール バーとドッキング ウィンドウ(&T)");
        m_mapString[ STR_MNU_PLACE_HOLDER]      =_T("<プレースホルダ>");
        m_mapString[ STR_MNU_VIEW_STATUS_BAR]   =_T("ステータス バー(&S)");
        m_mapString[ STR_MNU_VIEW_APPLOOK ]     =_T("アプリケーションの外観(&A)");

            m_mapString[ STR_MNU_VIEW_APPLOOK_WIN_2000]  =_T("Windows 2000(&2)");
            m_mapString[ STR_MNU_VIEW_APPLOOK_OFF_XP]    =_T("Office XP(&X)");
            m_mapString[ STR_MNU_VIEW_APPLOOK_WIN_XP]    =_T("Windows XP(&W)");
            m_mapString[ STR_MNU_VIEW_APPLOOK_OFF_2003]  =_T("Office 2003(&3)");
            m_mapString[ STR_MNU_VIEW_APPLOOK_VS_2005]   =_T("Visual Studio .NET 2005(&5)");
            m_mapString[ STR_MNU_VIEW_APPLOOK_OFF_2007]  =_T("Office 2007(&7)");
                m_mapString[ STR_MNU_VIEW_APPLOOK_OFF_2007_BLUE]     =_T("ブルー スタイル(&B)");
                m_mapString[ STR_MNU_VIEW_APPLOOK_OFF_2007_BLACK]    =_T("ブラック スタイル(&L)");
                m_mapString[ STR_MNU_VIEW_APPLOOK_OFF_2007_SILVER]   =_T("シルバー スタイル(&S)");
                m_mapString[ STR_MNU_VIEW_APPLOOK_OFF_2007_AQUA]     =_T("アクア スタイル(&A)");


    //ヘルプ
    m_mapString[ STR_MNU_HELP ] = _T("ヘルプ(&H)");
    m_mapString[ STR_MNU_APP_ABOUT] = _T("バージョン情報 MockSketch(&A)...");


    //編集
    m_mapString[ STR_MNU_EDIT]      = _T("編集(&E)");
        m_mapString[ STR_MNU_UNDO]  = _T("元に戻す(&U)");
        m_mapString[ STR_MNU_REDO]  = _T("やり直し(&Y)");

        //-----------
        //重複あり
        //-----------
        m_mapString[ STR_MNU_CUT]       = _T("切り取り(&T)");
        m_mapString[ STR_MNU_COPY]      = _T("コピー(&C)");
        m_mapString[ STR_MNU_PASTE]     = _T("貼り付け(&P)");
        //-----------


    //描画
    m_mapString[ STR_MNU_DRAW   ]     =_T("描画(&D)");

    m_mapString[ STR_MNU_DRAW_ITEM]     =_T("図形");                         
        m_mapString[ STR_MNU_POINT  ]     =_T("点"); //    ID_MNU_POINT
        m_mapString[ STR_MNU_LINE   ]     =_T("直線"); //    ID_MNU_POINT
        m_mapString[ STR_MNU_LINE_SEG]    =_T("線分");                      

        m_mapString[ STR_MNU_CIRCLE ]       =_T("円");                        
        m_mapString[ STR_MNU_3P_CIRCLE]     =_T("円-3点指定");                

        m_mapString[ STR_MNU_ELLIPSE]       =_T("楕円");                          
        m_mapString[ STR_MNU_SPLINE]        =_T("スプライン");                    

        m_mapString[ STR_MNU_DIM]           =_T("寸法");
        m_mapString[ STR_MNU_L_DIM]         =_T("距離");                      
        m_mapString[ STR_MNU_H_DIM]         =_T("水平");                      
        m_mapString[ STR_MNU_V_DIM]         =_T("垂直");                     
        m_mapString[ STR_MNU_A_DIM]         =_T("角度");                      
        m_mapString[ STR_MNU_D_DIM]         =_T("直径");                      
        m_mapString[ STR_MNU_R_DIM]         =_T("半径");                      
        m_mapString[ STR_MNU_C_DIM]         =_T("面取り");                      
        m_mapString[ STR_MNU_DRAW_TEXT]     =_T("文字");                          
        m_mapString[ STR_MNU_NODE]          =_T("節点");                      
        m_mapString[ STR_MNU_IMAGE]         =_T("画像");                      

    m_mapString[ STR_MNU_DRAW_PROCESS]     =_T("図形加工");                         

        m_mapString[ STR_MNU_OFFSET]        =_T("オフセット");                    
        m_mapString[ STR_MNU_CHAMFER]       =_T("面取り"); 
        m_mapString[ STR_MNU_CORNER]        =_T("コーナー"); 

        m_mapString[ STR_MNU_TRIM]          =_T("トリム");                        
        m_mapString[ STR_MNU_ITEM_MOVE]     =_T("移動");  
        m_mapString[ STR_MNU_STRETCH_MODE]  =_T("ストレッチモード");


    m_mapString[ STR_MNU_DRAW_EDIT]     =_T("図形編集");                         
        m_mapString[ STR_MNU_ITEM_COPY]     =_T("複写");                         
        m_mapString[ STR_MNU_ITEM_ROTATE]   =_T("回転");                          
        m_mapString[ STR_MNU_SCL]           =_T("倍率");                          
        m_mapString[ STR_MNU_MIRROR]        =_T("鏡像");                          
        m_mapString[ STR_MNU_SELECT]        =_T("選択");
        m_mapString[ STR_MNU_CUT_MODE]      =_T("切取りモード");
        m_mapString[ STR_MNU_COPY_MODE]     =_T("複写モード");

        m_mapString[ STR_MNU_DELETE]        =_T("削除");                         
        m_mapString[ STR_MNU_DEVIDE]        =_T("分割");                         
        m_mapString[ STR_MNU_COMPOSITION_LINE]   =_T("複合線");                        
        m_mapString[ STR_MNU_CONNECTION_LINE]    =_T("接続線");                        

        m_mapString[ STR_MNU_GROUP]         =_T("グループ化");
        m_mapString[ STR_MNU_PARTS]         =_T("部品化");
        m_mapString[ STR_MNU_REFERENCE]     =_T("参照化");
        m_mapString[ STR_MNU_REL_GROUP]     =_T("グループ解除");

        m_mapString[ STR_MNU_DISASSEMBLY]   =_T("分解");               
        m_mapString[ STR_MNU_CONVERT_SPLINE]=_T("複合線に変換");
        m_mapString[ STR_MNU_FILL]          =_T("塗りつぶし");                    


    m_mapString[ STR_MNU_DRAW_DEF]     =_T("図形定義");                         
        m_mapString[ STR_MNU_LINE_WIDTH]     =_T("線幅");                          
        m_mapString[ STR_MNU_LINE_STYLE]     =_T("線種");                         

        m_mapString[ STR_MNU_LINE_COLOR]     =_T("線色");                          
        m_mapString[ STR_MNU_BRUSH_COLOR]    =_T("塗りつぶし色");                  

        m_mapString[ STR_MNU_IMAGE_CTRL]    =_T("画像管理");                  

    m_mapString[ STR_MNU_REDRAW]        =_T("再描画");
    m_mapString[ STR_MNU_VIEW_ALL]      =_T("全体表示");

    m_mapString[ STR_MNU_DEL_LAST]      =_T("一つ戻る");

    //フォーマット
    m_mapString[ STR_MNU_FORMAT]    =_T("フォーマット");
        m_mapString[ STR_MNU_FORMAT_FONT]    =_T("フォント");
        m_mapString[ STR_MNU_CHAR_COLOR]     =_T("テキスト色"); 

    //ヘルプ
    m_mapString[ STR_MNU_HELP]     =_T("ヘルプ(&H)");
        m_mapString[ STR_MNU_APP_ABOUT]     =_T("バージョン情報 MockSketch(&A)...");

    //スプライン（ポップアップメニュで使用）
    m_mapString[ STR_MNU_END]        =_T("終了");
    m_mapString[ STR_MNU_CANCEL]     =_T("キャンセル");
	m_mapString[STR_MNU_DEL_THIS_POINT] = _T("この点を削除");

	m_mapString[STR_MNU_CLOSED_CURVE]     = _T("閉曲線");
	m_mapString[STR_MNU_SPLINE_TYPE]      = _T("スプライン種別");
	m_mapString[STR_MNU_SPLINE_EDIT_TYPE] = _T("編集方式");

	//点（コンテキストメニュで使用）
	m_mapString[STR_MNU_ADD_ALL_POINT]   = _T("すべての点を追加する");

	//------------------------
	//   SPLINE_TYPE
	//------------------------
	m_mapString[STR_BEZIER] = _T("ベジェ曲線");
	m_mapString[STR_BSPLINE2] = _T("2次Bスプライン");
	m_mapString[STR_BSPLINE3] = _T("3次Bスプライン");
	m_mapString[STR_NURBS] = _T("NURBS");

	m_mapString[STR_CONTROL_POINT] = _T("制御点");
	m_mapString[STR_PASS_POINT] = _T("通過点");



    //部品選択時 コンテキストメニュー
    m_mapString[ STR_MNU_DSP_IO_DLG]  = _T("IO設定表示");

    m_mapString[ STR_MNU_DSP_ORDER]  = _T("順序");
    m_mapString[ STR_MNU_DSP_ORDER_TOPMOST]  = _T("最前面へ移動する");
    m_mapString[ STR_MNU_DSP_ORDER_TOPBACK]  = _T("最背面へ移動する");
    m_mapString[ STR_MNU_DSP_ORDER_FRONT]  = _T("前面へ移動する");
    m_mapString[ STR_MNU_DSP_ORDER_TBACK]  = _T("背面へ移動する");


    m_mapString[ STR_MNU_VIEW_SETTING]  = _T("表示設定");

    m_mapString[ STR_MNU_VIEW_POSITION]  = _T("表示位置");
    m_mapString[ STR_MNU_SHOW_POS_01]  = _T("位置01");
    m_mapString[ STR_MNU_SHOW_POS_02]  = _T("位置02");
    m_mapString[ STR_MNU_SHOW_POS_03]  = _T("位置03");
    m_mapString[ STR_MNU_SHOW_POS_04]  = _T("位置04");
    m_mapString[ STR_MNU_SHOW_POS_05]  = _T("位置05");
    m_mapString[ STR_MNU_SHOW_POS_06]  = _T("位置06"); 
    m_mapString[ STR_MNU_SHOW_POS_07]  = _T("位置07");
    m_mapString[ STR_MNU_SHOW_POS_08]  = _T("位置08"); 
    m_mapString[ STR_MNU_SHOW_POS_09]  = _T("位置09"); 
    m_mapString[ STR_MNU_SHOW_POS_SET]  = _T("位置設定");

    m_mapString[ STR_SHOW_POS]          = _T("位置");
    m_mapString[ STR_DIAG_SHOW_POS_SET] = _T("設定する位置番号を選択してください"); 
    m_mapString[ STR_TIP_SHOW_POS]      = _T("表示位置\n表示位置番号を選択します");


    m_mapString[ STR_MNU_SNAP]              = _T("スナップ");
    m_mapString[ STR_MNU_SNAP_END_POINT]    = _T("端点");  
    m_mapString[ STR_MNU_SNAP_MID_POINT]    = _T("中点");  
    m_mapString[ STR_MNU_SNAP_CROSS_POINT]  = _T("交点"); 
    m_mapString[ STR_MNU_SNAP_ON_LINE]      = _T("線上");   
    m_mapString[ STR_MNU_SNAP_NEAR_POINT]   = _T("近接点");  
    m_mapString[ STR_MNU_SNAP_CENTER]       = _T("中心点");   
    m_mapString[ STR_MNU_SNAP_TANGENT]      = _T("接線");   
    m_mapString[ STR_MNU_SNAP_FEATURE_POINT]= _T("特徴点");
    m_mapString[ STR_MNU_SNAP_DATUM_POINT]  = _T("基準点");
    m_mapString[ STR_MNU_SNAP_GRID]         = _T("グリッド");     
    m_mapString[ STR_MNU_SNAP_POINT]        = _T("点");      
    m_mapString[ STR_MNU_SNAP_NORM]         = _T("垂線");        
    m_mapString[ STR_MNU_SNAP_NODE]         = _T("ノード");

    m_mapString[ STR_MNU_DRAW_FIGURE]  = _T("図形描画");

    //=======================
    // レイヤー
    //=======================
    m_mapString [STR_MNU_LAYER]            =_T("レイヤー");
    m_mapString [STR_MNU_LAYER_ADD]        =_T("追加");
    m_mapString [STR_MNU_LAYER_DEL]        =_T("削除");
    m_mapString [STR_MNU_LAYER_SCL]        =_T("全体倍率設定");

    m_mapString [STR_LAYER_SCL]                 =_T("倍率");
    m_mapString [STR_LAYER_DEL_QUESTION]        =_T("このレイヤーに図形が含まれていますが、削除しますか？");
    m_mapString [STR_LAYER_NO_SEL_UNVISIVLE]    =_T("非表示のレイヤーは変更しない");
    m_mapString [STR_LAYER_NO_SEL_SCL]          =_T("倍率が異なるレイヤーは変更しない");
    m_mapString [STR_LAYER_SEL_ZERO]            =_T("０番レイヤーは変更しない");
    m_mapString [STR_LAYER_INVALID_SCL]         =_T("正しい倍率を設定してください(倍率>0)");

	//=======================
	// IO入力
	//=======================
	m_mapString[ STR_IOS_ADDRESS ]  = _T("L50,アドレス");
	m_mapString[ STR_IOS_ID   ]     = _T("L50,ID");
	m_mapString[ STR_IOS_TYPE ]     = _T("L60,種 別");
	m_mapString[ STR_IOS_NAME ]     = _T("L80,     名 称");
	m_mapString[ STR_IOS_INIT ]     = _T("R50,初期値");
	m_mapString[ STR_IOS_NOTE ]     = _T("L120,        説   明");
	m_mapString[ STR_IOS_LEN  ]     = _T("R55,データ長");
	m_mapString[ STR_IOS_UNIT ]     = _T("L50,単位");
	m_mapString[ STR_IOS_MAX  ]     = _T("R50,最大値");
	m_mapString[ STR_IOS_MIN  ]     = _T("R50,最小値");
	m_mapString[ STR_IOS_CONV ]     = _T("R50,変換係数");

	m_mapString[ STR_IOS_CONECT ]   = _T("R50,接続先");
	m_mapString[ STR_IOS_VAL ]      = _T("R50,値");
	m_mapString[ STR_IOS_CONV_VAL ] = _T("R50,変換値");

    //=======================
    // IO接続
    //=======================
	m_mapString[ STR_IOC_VAL_DIRECT ]   = _T("R50,直値");
    m_mapString[ STR_IOC_VAL        ]   = _T("R50,値");
	m_mapString[ STR_IOC_CONNECT    ]   = _T("R50,接続先");


    //=======================
    //プロパティウインドウ
    //=======================
    m_mapString[ STR_PORP_SORT_ITEM]    = _T("項目別");
    m_mapString[ STR_PORP_SORT_ALPH]    = _T("アルファベット順");


    //=============================
    // 実行モニター
    //=============================
    //ポップアップメニュー
    m_mapString[ STR_MNU_EXEC_GROUP_START]    = _T("開始");
    m_mapString[ STR_MNU_EXEC_GROUP_PAUSE]    = _T("一時停止");
    m_mapString[ STR_MNU_EXEC_GROUP_ABORT]    = _T("終了");
    m_mapString[ STR_MNU_EXEC_GROUP_BREAK]    = _T("強制終了");

    m_mapString[ STR_MNU_EXEC_GROUP_CONFIG]    = _T("設定");
    //=======================

    /*
    切り取り
    コピー
    切り取り(行)
    コピー(行)
    貼り付け
    挿入
    削除
    */


    //---------
    //エディタ
    //---------
    m_mapString[ STR_EDITOR_VIEW    ]  = _T("エディタ");

    //エディタ ポップアップメニュー
    m_mapString[ STR_MNU_EDITOR_CUT    ]  = _T("切り取り");
    m_mapString[ STR_MNU_EDITOR_PASTE  ]  = _T("貼り付け");
    m_mapString[ STR_MNU_EDITOR_COPY   ]  = _T("コピー");

    m_mapString[ STR_MNU_SCRIPT_BUILD_TOP]      = _T("ビルド (&B)");
    m_mapString[ STR_MNU_SCRIPT_COMPILE]    = _T("コンパイル");
    m_mapString[ STR_MNU_SCRIPT_BUILD]      = _T("ビルド");
    m_mapString[ STR_MNU_SCRIPT_REBUILD]      = _T("リビルド");
    m_mapString[ STR_MNU_SCRIPT_STOP_BUILD] = _T("ビルド停止");
    m_mapString[ STR_MNU_SCRIPT_EXEC]       = _T("実行");
    m_mapString[ STR_MNU_SCRIPT_ABORT]   = _T("停止");
    m_mapString[ STR_MNU_SCRIPT_PAUSE]  = _T("一時停止");
    m_mapString[ STR_MNU_SCRIPT_RESTART]  = _T("再開");
    m_mapString[ STR_MNU_SCRIPT_DEBUG]      = _T("デバッグモード");
    m_mapString[ STR_MNU_SCRIPT_RELEASE]    = _T("リリースモード");

    m_mapString[ STR_MNU_SCRIPT_STEP_IN]          = _T("ステップイン");
    m_mapString[ STR_MNU_SCRIPT_STEP_OVER]        = _T("ステップオーバー");
    m_mapString[ STR_MNU_SCRIPT_EXEC_CUROSR]      = _T("カーソル行の前まで実行");

    m_mapString[ STR_MNU_EDITOR_BREAKPOINT_SET]         = _T("ブレークポイントの設定");
    m_mapString[ STR_MNU_EDITOR_BREAKPOINT_REMOVE]      = _T("ブレークポイントの解除");
    m_mapString[ STR_MNU_EDITOR_CLEAR_BREAKPOINT]       = _T("全ブレークポイントの解除");

    //---------
    // ツール
    //---------
    m_mapString[ STR_MNU_TOOL]          = _T("ツール(&T)");
    m_mapString[ STR_MNU_ADDIN_MANEGER] = _T("アドインマネージャ");
    m_mapString[ STR_MNU_OPTION]        = _T("オプション");
    m_mapString[ STR_MNU_PROPERTY_GROUP]= _T("プロパティグループ");

    //-----------
    // ウインドウ
    //-----------
    m_mapString[ STR_MNU_WINDOW ]       = _T("ウィンドウ(&W)");
    m_mapString[ STR_MNU_WINDOW_NEW ]   = _T("新しいウィンドウを開く(&N)");
    m_mapString[ STR_MNU_WINDOW_SPLIT]  = _T("分割(&P)");

    m_mapString[ STR_MNU_MDI_NEW_HORZ_TAB_GROUP] = _T("水平タブグループの新規作成");
    m_mapString[ STR_MNU_MDI_NEW_VERT_GROUP]     = _T("垂直タブグループの新規作成");
  

    //---------
    //ウォッチ
    //---------
    m_mapString[ STR_WATCH    ]  = _T("ウォッチ");

    //ウォッチ ポップアップメニュー
    m_mapString[ STR_MNU_WATCH_COPY   ]  = _T("コピー");
    m_mapString[ STR_MNU_WATCH_PASTE  ]  = _T("貼り付け");

    m_mapString[ STR_MNU_WATCH_EDIT]    = _T("値の編集");
    m_mapString[ STR_MNU_WATCH_SEL]     = _T("全て選択");
    m_mapString[ STR_MNU_WATCH_CLEAER]  = _T("全てクリア");

    m_mapString[ STR_MNU_WATCH_HEX]     = _T("16進表示");

    m_mapString[ STR_MNU_CONTRACT]      = _T("1レベル上に折りたたむ");

    //------------------------------
    //プロパティグループダイアログ
    //------------------------------
    m_mapString[ STR_MNU_PROP_EDIT_ADD]      = _T("追加");
    m_mapString[ STR_MNU_PROP_EDIT_DEL]      = _T("削除");
    m_mapString[ STR_MNU_PROP_EDIT_CHG_NAME] = _T("名前の変更");
    m_mapString[ STR_PROP_EDIT_INPUT_NAME]   = _T("プロパティセット名を入力してください");
    m_mapString[ STR_PROP_ERR_INPUT_NAME]    = _T("すでにそのプロパティセット名は使用されています");
    m_mapString[ STR_PROP_ERR_DEL]           = _T("プロパティセットの削除に失敗しました");

    //---------------------
    //プロパティ編集
    //---------------------
    m_mapString[ STR_PRE_TYPE ]     = _T("種別");
    m_mapString[ STR_PRE_NAME ]     = _T("名称");
    m_mapString[ STR_PRE_VAL_NAME ] = _T("変数名");
    m_mapString[ STR_PRE_INIT ]     = _T("初期値");
    m_mapString[ STR_PRE_NOTE ]     = _T("説明");
    m_mapString[ STR_PRE_ENABLE]    = _T("有効");
    m_mapString[ STR_PRE_UNIT_KIND] = _T("物理量");
    m_mapString[ STR_PRE_UNIT ]     = _T("単位");
    m_mapString[ STR_PRE_MAX  ]     = _T("最大値");
    m_mapString[ STR_PRE_MIN  ]     = _T("最小値");

    m_mapString[ STR_MNU_PROPED]       = _T("プロパティ編集");
    m_mapString[ STR_MNU_PROPED_UP]    = _T("上に移動");
    m_mapString[ STR_MNU_PROPED_DOWN]  = _T("下に移動");
    m_mapString[ STR_MNU_PROPED_ADD]   = _T("追加");
    m_mapString[ STR_MNU_PROPED_DEL]   = _T("削除");

    m_mapString[ STR_PRE_ERR_COLOR ]     = _T("rrggbb形式の16進数を入力してください");


    //===================================================

    //メッセージ(共通)
    m_mapString[ STR_MB_MESSAGE] = _T("メッセージ");
    m_mapString[ STR_MB_DELETE ] = _T("削除しますか");
    m_mapString[ STR_MB_SAVE ]   = _T("データが変更されています。\n保存しますか？");
    m_mapString[ STR_MB_COMPILE ]= _T("以下のファイルに変更があります。\n コンパイルしますか\n");

    //メッセージ-ビルド
    m_mapString[ STR_MB_BUIDLD_START   ]   = _T("------ ビルド開始 %s-----\n");
    m_mapString[ STR_MB_BUIDLD_SUCCESS ]   = _T("------ ビルド成功 ------\n");
    m_mapString[ STR_MB_BUIDLD_FAILURE ]   = _T("------ ビルド失敗 ------\n");
    m_mapString[ STR_MB_BUIDLD_ABORTED ]   = _T("------ ビルド中止 ------\n");

    //メッセージ-実行
    m_mapString[ STR_MB_SETUP          ]   = _T("セットアップ完了 %s: %d\n");
    m_mapString[ STR_MB_SETUP_FAIL     ]   = _T("セットアップ失敗 %s: %d\n");
    m_mapString[ STR_MB_SETUP_EXEC_FAIL]   = _T("セットアップ実行失敗 %s: %d\n");

    m_mapString[ STR_MB_LOOP_ABORT     ]   = _T("ループ停止 %s: %d\n");

    m_mapString[ STR_MB_ABORT          ]   = _T("アボート完了 %s: %d\n");
    m_mapString[ STR_MB_ABORT_FAIL     ]   = _T("アボート失敗 %s: %d\n");
    m_mapString[ STR_MB_ABORT_EXEC_FAIL]   = _T("アボート実行失敗 %s: %d\n");

    //ファイル
    m_mapString[ STR_MB_FILENAME_s ]       = _T("ファイル名:%s");
    m_mapString[ STR_MB_LINE_NO_d ]        = _T("行:%d");

    //------------------------
    // Action
    //------------------------
    m_mapString[ STR_ACT_CONNECTION ]      = _T("接続");
    m_mapString[ STR_ACT_NO_CONNECTION ]   = _T("未接続");

	//=============================
	// ダイアログ
	//=============================
    m_mapString[ STR_DLG_CANCEL ]             = _T("キャンセル");
    m_mapString[ STR_DLG_SEL_DIR ]            = _T("ディレクトリ選択");

    m_mapString[ STR_DLG_CHG_FILE_s ]         = _T("ファイル%sが変更されました。\n 再度読み込みますか？)");
    m_mapString[ STR_DLG_DELETE_ORGIN_TEXT]   = _T("元の文字列を消去しますか？");

    //-----------------
    // 新規プロジェクト
    //-----------------
    m_mapString[ STR_DLG_NEWPRJ ]                 = _T("新規プロジェクト");
    m_mapString[ STR_DLG_NEWPRJ_REF ]             = _T("参照");
    m_mapString[ STR_DLG_NEWPRJ_PROJECT ]         = _T("プロジェクト名");
    m_mapString[ STR_DLG_NEWPRJ_DIR ]             = _T("ディレクトリ");
    m_mapString[ STR_DLG_NEWPRJ_CREATE_DIR ]      = _T("プロジェクトのディレクトリを作成する");
    m_mapString[ STR_DLG_NEWPRJ_CREATE_ASSY]      = _T("装置定義を作成する");
	m_mapString[ STR_DLG_NEWPRJ_XML]              = _T("XML形式");


    m_mapString[ STR_ERR_NEWPRJ_NO_PRJ_NAME ]     = _T("プロジェクト名を入力してください");
    m_mapString[ STR_ERR_NEWPRJ_NO_DIR ]          = _T("ディレクトリを入力してください");
    m_mapString[ STR_ERR_NEWPRJ_ALREADY_DIR ]     = _T("すでにプロジェクトのディレクトリが存在します。\n上書きしますか？");

    //-----------------
    // IO接続
    //-----------------
    m_mapString[ STR_DLG_IOCONNECT ]            = _T("IO接続");
    m_mapString[ STR_DLG_IOCONNECT_CONNECT ]    = _T("接続対象");
    m_mapString[ STR_DLG_IOCONNECT_PAGE ]       = _T("参照ページ");
    m_mapString[ STR_DLG_IOCONNECT_IO ]         = _T("対象IO");
    m_mapString[ STR_DLG_IOCONNECT_ALL  ]       = _T("全ページ");
    m_mapString[ STR_DLG_IOCONNECT_DISP_PAGE ]  = _T("%04d Page");
    m_mapString[ STR_DLG_IOCONNECT_NOT  ]       = _T("未接続");

    //-----------------
    // IO 設定
    //-----------------
    m_mapString[ STR_DLG_IOSETTING  ]         = _T("IO設定");
    m_mapString[ STR_DLG_IOSETTING_PAGEMAX  ] = _T("最大ページ数");

    //-----------------
    // IO ウインドウ
    //-----------------
    m_mapString[ STR_IOWND_NOT_SEL ]  = _T("未選択");

    //ツールチップ
    m_mapString[ STR_IOWND_TIP_PAGE ]     = _T("表示するページを選択します");
    m_mapString[ STR_IOWND_TIP_SETTING ]  = _T("IOを設定します");
    m_mapString[ STR_IOWND_TIP_SELECT ]   = _T("表示するIOを選択します");

    //------------------------
    // ツールバー
    //------------------------
    m_mapString[ STR_TOOLBAR_CUSTOMIZE ]     = _T("カスタマイズ...");
    m_mapString[ STR_TOOLBAR_STANDARD  ]     = _T("標準");
    m_mapString[ STR_TOOLBAR_BUILD     ]     = _T("ビルド");
    m_mapString[ STR_TOOLBAR_LINE      ]     = _T("線定義");
    m_mapString[ STR_TOOLBAR_FIND      ]     = _T("検索");
    m_mapString[ STR_TOOLBAR_EDIT      ]     = _T("図形編集");
    m_mapString[ STR_TOOLBAR_SNAP      ]     = _T("スナップ");
    m_mapString[ STR_TOOLBAR_SHOW      ]     = _T("表示位置");



    //------------------------
    // モジュール
    //------------------------
    m_mapString[ STR_MODULE_DEL_FAIL  ]     = _T("モジュールの削除に失敗しました");
    m_mapString[ STR_MODULE_ADD       ]     = _T("  -----%s 追加----\n");
    m_mapString[ STR_MODULE_ADD_FAIL  ]     = _T("  スクリプト%sの追加に失敗しました\n");
    m_mapString[ STR_MODULE_COMPILE   ]     = _T("  -----%s コンパイル----\n");
    m_mapString[ STR_MODULE_LOOP_FAIL ]     = _T("  関数 Loop() が見つかりません\n");
    m_mapString[ STR_MODULE_NOT_FIND  ]     = _T("が、見つかりません");

    //------------------------
    // ウォッチ
    //------------------------
    m_mapString[ STR_WAT_NAME ] = _T("L75,名前");
    m_mapString[ STR_WAT_VALUE] = _T("L75,値");
    m_mapString[ STR_WAT_TYPE ] = _T("L75,型");

    //------------------------
    // 実行モニタ
    //------------------------
    m_mapString[ STR_EXEC_NAME] = _T("L75,名前");
    m_mapString[ STR_EXEC_TIME] = _T("L75,実行時間");

    //------------------------
    // スナップ名
    //------------------------
    m_mapString[ STR_END_POINT    ] = _T("端点");
    m_mapString[ STR_MID_POINT    ] = _T("中点");
    m_mapString[ STR_CROSS_POINT  ] = _T("交点");
    m_mapString[ STR_ON_LINE      ] = _T("線上");
    m_mapString[ STR_NEAR_POINT   ] = _T("近接点");
    m_mapString[ STR_CENTER       ] = _T("中心点");
    m_mapString[ STR_TANGENT      ] = _T("接点");
    m_mapString[ STR_FEATURE_POINT] = _T("特徴点");
    m_mapString[ STR_DATUM_POINT  ] = _T("基準点");
    m_mapString[ STR_LENGTH       ] = _T("長さ");
    m_mapString[ STR_ANGLE        ] = _T("角度");
    m_mapString[ STR_BISECTRIX    ] = _T("角２等分線");
    m_mapString[ STR_NORMAL       ] = _T("垂線");
    m_mapString[ STR_GRID         ] = _T("グリッド");
    m_mapString[ STR_POINT        ] = _T("点");
    m_mapString[ STR_INPUT_VAL    ] = _T("入力値");
    m_mapString[ STR_DISTANCE     ] = _T("距離");
    m_mapString[ STR_SNAP_NONE    ] = _T("任意点");
    m_mapString[ STR_SNAP_NODE    ] = _T("接続点");
    m_mapString[ STR_OBJECT       ] = _T("図形");


    m_mapString[ STR_WIDTH        ] = _T("幅");
    m_mapString[ STR_HEIGHT       ] = _T("高さ");
    m_mapString[ STR_SCALE        ] = _T("倍率");

    m_mapString[ STR_SNAP_CIRCLE_CONTACT ]     = _T("円に接する円");
    m_mapString[ STR_SNAP_2CIRCLE_CONTACT ]    = _T("2円に接する円");
    m_mapString[ STR_SNAP_3CIRCLE_CONTACT ]    = _T("3円に接する円");
    m_mapString[ STR_SNAP_2LINE_CONTACT ]      = _T("2直線に接する円");
    m_mapString[ STR_SNAP_3LINE_CONTACT ]      = _T("3直線に接する円");
    m_mapString[ STR_SNAP_3POINT_THROUGH]      = _T("3点を通る円");
    m_mapString[ STR_SNAP_FIRST_POINT]         = _T("1点目");
    m_mapString[ STR_SNAP_SECOND_POINT]        = _T("2点目");

    m_mapString[ STR_SNAP_LINE_CIRCLE_CONTACT] = _T("直線と円に接する円");


    //------------------------
    // 寸法
    //------------------------
    m_mapString[ STR_AT_NONE        ] = _T("なし");
    m_mapString[ STR_AT_DOT_FILL    ] = _T("黒丸");
    m_mapString[ STR_AT_DOT         ] = _T("丸");
    m_mapString[ STR_AT_OBLIQUE     ] = _T("斜線");
    m_mapString[ STR_AT_OPEN        ] = _T("矢印");
    m_mapString[ STR_AT_FILLED      ] = _T("塗潰矢印");
    m_mapString[ STR_AT_DATUM_BLANK ] = _T("基準三角");
    m_mapString[ STR_AT_DATUM_FILL  ] = _T("塗潰基準三角");

    m_mapString[ STR_ARS_NONE      ] = _T("なし");
    m_mapString[ STR_ARS_START     ] = _T("始点");
    m_mapString[ STR_ARS_END       ] = _T("終点");
    m_mapString[ STR_ARS_BOTH      ] = _T("両方");

    m_mapString[ STR_DLT_DECIMAL     ] = _T("10進表記");
    m_mapString[ STR_DLT_FRACTION    ] = _T("分数表記");
    m_mapString[ STR_DLT_ENGINEERING ] = _T("工業図表記");
    m_mapString[ STR_DLT_ARCHTECTUAL ] = _T("建築図面表記");

    m_mapString[ STR_DIA_DECIMAL     ] = _T("10進表記");
    m_mapString[ STR_DIA_DEGMINSEC   ] = _T("度・分・秒");
    m_mapString[ STR_DIA_RADIANS     ] = _T("ラジアン");

    //------------------------
    // ツールチップ
    //------------------------
    m_mapString[ TIP_MNU_POINT      ] = _T("コマンド説明1\nコマンド名1");
    m_mapString[ TIP_MNU_H_LINE     ] = _T("コマンド説明2\nコマンド名2");
    m_mapString[ TIP_MNU_V_LINE     ] = _T("コマンド説明3\nコマンド名3");
    m_mapString[ TIP_MNU_A_LINE     ] = _T("コマンド説明4\nコマンド名4");
    m_mapString[ TIP_MNU_LINE       ] = _T("コマンド説明5\nコマンド名5");
    m_mapString[ TIP_MNU_CIRCLE     ] = _T("コマンド説明6\nコマンド名6");
    m_mapString[ TIP_MNU_ARC        ] = _T("コマンド説明7\nコマンド名7");
    m_mapString[ TIP_MNU_OFFSET     ] = _T("コマンド説明8\nコマンド名8");
    m_mapString[ TIP_MNU_NODE       ] = _T("コマンド説明9\nコマンド名9");
    m_mapString[ TIP_MNU_IMAGE      ] = _T("コマンド説明10\nコマンド名10");
    m_mapString[ TIP_MNU_CORNER     ] = _T("テスト１\n テステス１");
    m_mapString[ TIP_MNU_CHAMFER    ] = _T("テスト１\n テステス１");
    m_mapString[ TIP_MNU_TRIM       ] = _T("テスト１\n テステス１");
    m_mapString[ TIP_MNU_MOVE       ] = _T("テスト１\n テステス１");
    m_mapString[ TIP_MNU_ROTATE     ] = _T("テスト１\n テステス１");
    m_mapString[ TIP_MNU_SELECT     ] = _T("テスト１\n テステス１");
    m_mapString[ TIP_MNU_DELETE     ] = _T("テスト１\n テステス１");
    m_mapString[ TIP_MNU_DEVIDE     ] = _T("テスト１\n テステス１");
    m_mapString[ TIP_MNU_CUT_MODE   ] = _T("テスト１\n テステス１");
    m_mapString[ TIP_MNU_CUT_MODE   ] = _T("テスト１\n テステス１");

                                      
    //=============================
    // 画像選択
    //=============================
    m_mapString[ STR_ISL_DLG_SEL   ] = _T("画像ソース選択");
    m_mapString[ STR_ISL_FILE      ] = _T("ファイル");
    m_mapString[ STR_ISL_STORED    ] = _T("登録済み画像");

    //=============================
    // 画像管理
    //=============================
    m_mapString[ STR_ISC_DLG_CTRL ] = _T("画像管理");
    m_mapString[ STR_ISC_ADD      ] = _T("追加");
    m_mapString[ STR_ISC_SET      ] = _T("設定");
    m_mapString[ STR_ISC_DEL      ] = _T("削除");

    //=============================
    // 画像設定
    //=============================
    m_mapString[ STR_ISP_DLG_PROP  ] = _T("画像管理");

    //=============================
    // 寸法文字
    //=============================
    m_mapString[STR_DTX_DLG   ] = _T("寸法文字");
    m_mapString[STR_DTX_AUTO  ] = _T("寸法値による設定");

	//=============================
	// ディフォルト設定
	//=============================
	m_mapString[STR_STX_DEFAULT_SET_DLG] = _T("既定値設定");
	m_mapString[STR_STX_DEFAULT_SET] = _T("図形の既定値に設定");
	m_mapString[STR_STX_HI_PRIORITY_LINE] = _T("線の設定をレイヤーの既定値より優先する");
	m_mapString[STR_STX_RESET] = _T("リセット");
	m_mapString[STR_STX_CANCEL] = _T("キャンセル");

    //=============================
    // CImageCtrlDlg
    //=============================
    m_mapString[STR_ICD_ERROR_LOAD  ] = _T("画像が読み込めませんでした");

    //=============================
    // CImagePropertyGrid
    //=============================
    m_mapString[STR_IPG_IMAGE       ] = _T("<画素");
    m_mapString[STR_IPG_XDIV        ] = _T("X分割数");
    m_mapString[STR_IPG_INFO_XDIV   ] = _T("X画像を格子状に分割し、番号で表示する図形を設定する\n場合のX方向の分割数");
    m_mapString[STR_IPG_YDIV        ] = _T("X分割数");
    m_mapString[STR_IPG_INFO_YDIV   ] = _T("X画像を格子状に分割し、番号で表示する図形を設定する\n場合のX方向の分割数");
    m_mapString[STR_IPG_OFFSET      ] = _T("オフセット");
    m_mapString[STR_IPG_XPOS        ] = _T("X位置");
    m_mapString[STR_IPG_INFO_XPOS   ] = _T("中心点のX位置(単位:pixel)");
    m_mapString[STR_IPG_YPOS        ] = _T("Y位置");
    m_mapString[STR_IPG_INFO_YPOS   ] = _T("中心点のY位置(単位:pixel)");
    m_mapString[STR_IPG_DPI         ] = _T("Dpi");
    m_mapString[STR_IPG_INFO_DPI    ] = _T("画面１インチ当たりの画素数");
    m_mapString[STR_IPG_TRANSPARENT         ] = _T("透過有無");
    m_mapString[STR_IPG_INFO_TRANSPARENT    ] = _T("透過色の使用有無を設定します");
    m_mapString[STR_IPG_TRANSPARENT_COLOR       ] = _T("透過色");
    m_mapString[STR_IPG_INFO_TRANSPARENT_COLOR  ] = _T("設定した色を透明にします。");

    m_mapString[STR_IPG_TRP_NONE  ] = _T("透過色を使用しない");
    m_mapString[STR_IPG_TRP_COLOR ] = _T("透過色の色を設定する");
    m_mapString[STR_IPG_TRP_LT    ] = _T("画像の右上の点を透過色とする");
    m_mapString[STR_IPG_TRP_RB    ] = _T("画像の左下の点を透過色とする");

    //=======================================
    //=======================================
    m_mapViewMode[VIEW_SELECT ] = GetString(STR_VIEW_SELECT);
    m_mapViewMode[VIEW_POINT  ] = GetString(STR_VIEW_POINT );
    m_mapViewMode[VIEW_LINE   ] = GetString(STR_VIEW_LINE  );
    m_mapViewMode[VIEW_CIRCLE ] = GetString(STR_VIEW_CIRCLE);
    m_mapViewMode[VIEW_CIRCLE_3P] = GetString(STR_VIEW_CIRCLE_3P ); 
    m_mapViewMode[VIEW_ARC    ] = GetString(STR_VIEW_ARC    ); 
    m_mapViewMode[VIEW_NODE   ] = GetString(STR_VIEW_NODE );
    m_mapViewMode[VIEW_IMAGE  ] = GetString(STR_VIEW_IMAGE);

    m_mapViewMode[VIEW_ELLIPSE] = GetString(STR_VIEW_ELLIPSE); 
    m_mapViewMode[VIEW_TEXT   ] = GetString(STR_VIEW_TEXT   ); 
    m_mapViewMode[VIEW_SPLINE ] = GetString(STR_VIEW_SPLINE ); 

    m_mapViewMode[VIEW_L_DIM  ] = GetString(STR_MNU_L_DIM  ); 
    m_mapViewMode[VIEW_H_DIM  ] = GetString(STR_MNU_H_DIM  ); 
    m_mapViewMode[VIEW_V_DIM  ] = GetString(STR_MNU_V_DIM  ); 
    m_mapViewMode[VIEW_A_DIM  ] = GetString(STR_MNU_A_DIM  ); 
    m_mapViewMode[VIEW_D_DIM  ] = GetString(STR_MNU_D_DIM  ); 
    m_mapViewMode[VIEW_R_DIM  ] = GetString(STR_MNU_R_DIM  ); 
    m_mapViewMode[VIEW_C_DIM  ] = GetString(STR_MNU_C_DIM  ); 

    m_mapViewMode[VIEW_GROUP      ] = GetString(STR_VIEW_GROUP ); 
    m_mapViewMode[VIEW_PARTS      ] = GetString(STR_VIEW_PARTS ); 
    m_mapViewMode[VIEW_REFERENCE  ] = GetString(STR_VIEW_REFERENCE); 


    m_mapViewMode[VIEW_DISASSEMBLY  ] = GetString(STR_VIEW_DISASSEMBLY ); 
    m_mapViewMode[VIEW_DELETE     ] = GetString(STR_VIEW_DELETE    ); 
    m_mapViewMode[VIEW_MOVE   ] = GetString(STR_VIEW_MOVE   ); 
    m_mapViewMode[VIEW_COPY   ] = GetString(STR_VIEW_COPY   ); 
    m_mapViewMode[VIEW_SCL    ] = GetString(STR_VIEW_SCL    ); 
    m_mapViewMode[VIEW_MIRROR ] = GetString(STR_VIEW_MIRROR ); 

    m_mapViewMode[VIEW_ROTATE ] = GetString(STR_VIEW_ROTATE ); 
    m_mapViewMode[VIEW_OFFSET ] = GetString(STR_VIEW_OFFSET ); 
    m_mapViewMode[VIEW_TRIM   ] = GetString(STR_VIEW_TRIM   ); 
    m_mapViewMode[VIEW_CORNER ] = GetString(STR_VIEW_CORNER ); 
    m_mapViewMode[VIEW_BREAK  ] = GetString(STR_VIEW_BREAK  ); 

    m_mapViewMode[VIEW_COMPOSITION_LINE ] = GetString(STR_VIEW_COMPOSITION_LINE);
    m_mapViewMode[VIEW_CONNECTION_LINE ] = GetString(STR_VIEW_COMPOSITION_LINE);

    
    m_mapViewMode[VIEW_FILL  ] = GetString(STR_VIEW_FILL  ); 

    //=============================
    // CExtText::POS_TYPE
    //=============================
    m_mapString[STR_POS_TYPE_NONE         ] = _T("なし");
    m_mapString[STR_POS_TYPE_LEFT         ] = _T("左");
    m_mapString[STR_POS_TYPE_RIGHT        ] = _T("右");
    m_mapString[STR_POS_TYPE_MID          ] = _T("左右中央");
    m_mapString[STR_POS_TYPE_TOP          ] = _T("上");
    m_mapString[STR_POS_TYPE_BOTTOM       ] = _T("下");
    m_mapString[STR_POS_TYPE_CENTER       ] = _T("中心");
    m_mapString[STR_POS_TYPE_TOP_LEFT     ] = _T("左上");
    m_mapString[STR_POS_TYPE_TOP_RIGHT    ] = _T("右上");
    m_mapString[STR_POS_TYPE_TOP_MID      ] = _T("上中央");
    m_mapString[STR_POS_TYPE_BOTTTOM_LEFT ] = _T("左下");
    m_mapString[STR_POS_TYPE_BOTTTOM_RIGHT] = _T("右下");
    m_mapString[STR_POS_TYPE_BOTTTOM_MID  ] = _T("下中央");
    m_mapString[STR_POS_TYPE_CENTER_LEFT  ] = _T("中央左");
    m_mapString[STR_POS_TYPE_CENTER_RIGHT ] = _T("中央右");
    m_mapString[STR_POS_TYPE_CENTER_MID   ] = _T("中央");

    //アライメント
    m_mapString[STR_ALIGNMENT_LEFT         ] = _T("左揃え");
    m_mapString[STR_ALIGNMENT_RIGHT        ] = _T("右揃え");   
    m_mapString[STR_ALIGNMENT_CENTER       ] = _T("中央揃え");

    //基準点種別
    m_mapString[STR_BASSE_TYPE_FIRST_LINE  ] = _T("1行目左下");
    m_mapString[STR_BASSE_TYPE_LAST_LINE   ] = _T("最下行目左下");
    m_mapString[STR_BASSE_TYPE_FRAME       ] = _T("外枠");

    //CDrawingText
    m_mapString[STR_PRO_PITCH              ] = _T("文字間隔");
    m_mapString[STR_PRO_INFO_PITCH         ] = _T("文字と文字との間の距離を設定します。");
    m_mapString[STR_PRO_LINE_PITCH         ] = _T("行間隔");
    m_mapString[STR_PRO_INFO_LINE_PITCH    ] = _T("行と行との間の距離を設定します。");
    m_mapString[STR_PRO_X_SCALE            ] = _T("横倍率");   
    m_mapString[STR_PRO_INFO_X_SCALE       ] = _T("横方向の倍率を設定します。"); 
    m_mapString[STR_PRO_Y_SCALE            ] = _T("縦倍率");   
    m_mapString[STR_PRO_INFO_Y_SCALE       ] = _T("縦方向の倍率を設定します。"); 

    m_mapString[STR_PRO_FRAME_ALIGNMENT    ] = _T("枠配置");   
    m_mapString[STR_PRO_INFO_FRAME_ALIGNMENT] = _T("枠の配置方法を設定します。");   

    m_mapString[STR_PRO_AUTO_FIT           ] = _T("自動調整");
    m_mapString[STR_PRO_INFO_AUTO_FIT      ] = _T("文字に合わせて枠のサイズを調整します。");

    m_mapString[STR_PRO_WORD_WRAP          ] = _T("文字折返し");  
    m_mapString[STR_PRO_INFO_WORD_WRAP     ] = _T("文字を枠内で折り返すかどうかを設定します。");

    m_mapString[STR_PRO_DRAW_LINE          ] = _T("枠線の有無");
    m_mapString[STR_PRO_INFO_DRAW_LINE     ] = _T("枠線の描画の有無を設定します。"); 

    m_mapString[STR_PRO_MARGIN_USE         ] = _T("マージン"); 
    m_mapString[STR_PRO_INFO_MARGIN_USE    ] = _T("枠内にマージンを設定するか否かを設定します。"); 

    m_mapString[STR_PRO_FRAME              ] = _T("枠");
    m_mapString[STR_PRO_FRANE_MARGIN       ] = _T("余白");

    m_mapString[STR_PRO_USE_FRAME          ] = _T("枠の有無");
    m_mapString[STR_PRO_INFO_USE_FRAME     ] = _T("文字列を囲む枠の有無を設定します。");

    m_mapString[ STR_PRO_LINE_COLOR        ] = _T("線色");
    m_mapString[ STR_PRO_INFO_LINE_COLOR   ] = _T("枠線の色を設定します。");
    m_mapString[STR_PRO_CORNER_RADIUS      ] = _T("角半径");
    m_mapString[STR_PRO_INFO_CORNER_RADIUS ] = _T("枠の角の丸み半径を設定します。 丸みがない場合は0を設定します。");

    m_mapString[STR_PRO_USE_BACK_COLOR     ] = _T("背景色の有無");
    m_mapString[STR_PRO_INFO_USE_BACK_COLOR] = _T("枠内の背景色の有無を設定します。");

    m_mapString[STR_PRO_BACK_COLOR         ] = _T("背景色");
    m_mapString[STR_PRO_INFO_BACK_COLOR    ] = _T("枠内の背景色を設定します。");

    m_mapString[STR_PRO_FRAME_WIDTH        ] = _T("枠幅");
    m_mapString[STR_PRO_INFO_FRAME_WIDTH   ] = _T("枠の幅をを設定します。");
    m_mapString[STR_PRO_FRAME_HEIGHT       ] = _T("枠高");
    m_mapString[STR_PRO_INFO_FRAME_HEIGHT  ] = _T("枠の高さを設定します。");   
    m_mapString[STR_PRO_FRAME_ALIGNMENT    ] = _T("枠配置");   
    m_mapString[STR_PRO_INFO_FRAME_ALIGNMENT] = _T("枠の配置方法を設定します。");   


    m_mapString[STR_PRO_TEXT_DATUM              ] = _T("基準位置");
    m_mapString[STR_PRO_INFO_TEXT_DATUM         ] = _T("文字の基準点の位置を設定します。");

    m_mapString[STR_PRO_ALIGNMENT          ] = _T("文字配置");
    m_mapString[STR_PRO_INFO_ALIGNMENT     ] = _T("枠内の文字の配置方法を設定します。");

    m_mapString[STR_PRO_DATUM_TYPE         ] = _T("基準点種別");
    m_mapString[STR_PRO_INFO_DATUM_TYPE    ] = _T("基準点の配置方法を設定します。");


    m_mapString[STR_PRO_EDITABLE           ] = _T("編集可");
    m_mapString[STR_PRO_INFO_EDITABLE      ] = _T("実行時に編集できるかを設定します。");
    m_mapString[STR_PRO_REVERSE            ] = _T("反転"); 
    m_mapString[STR_PRO_INFO_REVERSE       ] = _T("文字の方向を反転します。"); 

    m_mapString[STR_PRO_FIX_ROTATE         ] = _T("回転固定");
    m_mapString[STR_PRO_INFO_FIX_ROTATE    ] = _T("回転時に文字列の方向を固定するか設定します。");
    m_mapString[STR_PRO_FIX_SCALE          ] = _T("倍率固定");
    m_mapString[STR_PRO_INFO_FIX_SCALE     ] = _T("図形の倍率変更にかかわらずフォントサイズを一定とします。");
    m_mapString[STR_PRO_FIX_DISP_SCALE     ] = _T("表示倍率固定");
    m_mapString[STR_PRO_INFO_FIX_DISP_SCALE] = _T("画面を拡大、縮小しても画面に表示される文字の大きさを一定とします。");
    m_mapString[STR_PRO_VERTICAL_WRITING   ] = _T("縦書き");
    m_mapString[STR_PRO_INFO_VERTICAL_WRITING] = _T("縦書きにします。");

    m_mapString[STR_PRO_FRAME_MARGIN_TOP          ] = _T("上");
    m_mapString[STR_PRO_INFO_FRAME_MARGIN_TOP     ] = _T("上端のマージンを設定します。");
    m_mapString[STR_PRO_FRAME_MARGIN_BOTTOM       ] = _T("下");
    m_mapString[STR_PRO_INFO_FRAME_MARGIN_BOTTOM  ] = _T("下端のマージンを設定します。");
    m_mapString[STR_PRO_FRAME_MARGIN_LEFT         ] = _T("左");
    m_mapString[STR_PRO_INFO_FRAME_MARGIN_LEFT    ] = _T("左端のマージンを設定します。");
    m_mapString[STR_PRO_FRAME_MARGIN_RIGHT        ] = _T("右");
    m_mapString[STR_PRO_INFO_FRAME_MARGIN_RIGHT   ] = _T("右端のマージンを設定します。");


    //-----------------------------------------------
    m_mapColor[ STR_COLOR_BLACK            ] = _T("Black")             ;
    m_mapColor[ STR_COLOR_BROWN            ] = _T("Brown")             ;
    m_mapColor[ STR_COLOR_DARK_OLIVE_GREEN ] = _T("Dark Olive Green")  ;
    m_mapColor[ STR_COLOR_DARK_GREEN       ] = _T("Dark Green")        ;
    m_mapColor[ STR_COLOR_DARRK_TEAL       ] = _T("Dark Teal")         ;
    m_mapColor[ STR_COLOR_DARK_BLUE        ] = _T("Dark Blue")         ;
    m_mapColor[ STR_COLOR_INDIGO           ] = _T("Indigo")            ;
    m_mapColor[ STR_COLOR_GRAY_80          ] = _T("Gray-80%")          ;

    m_mapColor[ STR_COLOR_DARK_RED         ] = _T("Dark Red")          ;
    m_mapColor[ STR_COLOR_ORANGE           ] = _T("Orange")            ;
    m_mapColor[ STR_COLOR_DARK_YELLOR      ] = _T("Dark Yellow")       ;
    m_mapColor[ STR_COLOR_GREEN            ] = _T("Green")             ;
    m_mapColor[ STR_COLOR_TEAL             ] = _T("Teal")              ;
    m_mapColor[ STR_COLOR_BLUE             ] = _T("Blue")              ;
    m_mapColor[ STR_COLOR_BLUE_GRAY        ] = _T("Blue-Gray")         ;
    m_mapColor[ STR_COLOR_GRAY_50          ] = _T("Gray-50%")          ;

    m_mapColor[ STR_COLOR_RED              ] = _T("Red")               ;
    m_mapColor[ STR_COLOR_LIGHT_ORANGE     ] = _T("Light Orange")      ;
    m_mapColor[ STR_COLOR_LIME             ] = _T("Lime")              ;
    m_mapColor[ STR_COLOR_SEA_GREEN        ] = _T("Sea Green")         ;
    m_mapColor[ STR_COLOR_AQUA	           ] = _T("Aqua")              ;
    m_mapColor[ STR_COLOR_LIGHT_BLUE       ] = _T("Light Blue")        ;
    m_mapColor[ STR_COLOR_VIORET           ] = _T("Violet")            ;
    m_mapColor[ STR_COLOR_GRAY_40          ] = _T("Gray-40%")          ;

    m_mapColor[ STR_COLOR_PINK             ] = _T("Pink")              ;
    m_mapColor[ STR_COLOR_GOLD             ] = _T("Gold")              ;
    m_mapColor[ STR_COLOR_YELLOW           ] = _T("Yellow")            ;    
    m_mapColor[ STR_COLOR_BRIGHT_GREEN     ] = _T("Bright Green")      ;
    m_mapColor[ STR_COLOR_TURQUOISE        ] = _T("Turquoise")         ;
    m_mapColor[ STR_COLOR_SKY_BLUE	       ] = _T("Sky Blue")          ;
    m_mapColor[ STR_COLOR_PLUM	           ] = _T("Plum")              ;
    m_mapColor[ STR_COLOR_GRAY_25          ] = _T("Gray-25%")          ;

    m_mapColor[ STR_COLOR_ROSE             ] = _T("Rose")              ;
    m_mapColor[ STR_COLOR_TAN	           ] = _T("Tan")               ;
    m_mapColor[ STR_COLOR_LIGHT_YELLOR     ] = _T("Light Yellow")      ;
    m_mapColor[ STR_COLOR_LIGHT_GREEN	   ] = _T("Light Green ")      ;
    m_mapColor[ STR_COLOR_TURQUOISE	       ] = _T("Light Turquoise")   ;
    m_mapColor[ STR_COLOR_PALE_BLUE	       ] = _T("Pale Blue")         ;
    m_mapColor[ STR_COLOR_LAVENDER	       ] = _T("Lavender")          ;
    m_mapColor[ STR_COLOR_WHITE            ] = _T("White")             ;

}


/**
 * @brief   XML読み込み
 * @param   [in] pXml 
 * @return	なし
 * @note	 
 */
void CSystemString::LoadXml(StdXmlArchiveIn* pXml)
{
    *pXml >> boost::serialization::make_nvp("Root", *this);
}

/**
 * @brief   XML書き込み
 * @param   [in] pXml
 * @return	なし
 * @note	 
 */
void CSystemString::SaveXml(StdXmlArchiveOut* pXml)
{
    *pXml << boost::serialization::make_nvp("Root", *this);
}

/**
 * @brief   ファイル読み込み
 * @param   [in] pSysLocal
 * @retval  なし
 * @note    CSystem生成時にファイルを読み込むため
 *          pSysLocalを引数とする。
 */
void CSystemString::Load()
{
    namespace fs = boost::filesystem;

    //デフォルトデータ設定

    StdPath  pathLoad;
    pathLoad = CSystemLocal::GetLocalDir();

    pathLoad /= m_pathFileName;

    CConfig::Load2(pathLoad);
}

/**
 * @brief   ファイル書き込み
 * @param   なし          
 * @retval  なし
 * @note    普通は使わない
 */
void CSystemString::Save()
{
    namespace fs = boost::filesystem;

    StdPath  pathSave = CSystemLocal::GetLocalDir();
    if (!fs::exists(pathSave))
    {
       fs::create_directory( pathSave );
    }
    pathSave /= m_pathFileName;

    CConfig::Save2(pathSave);
}
