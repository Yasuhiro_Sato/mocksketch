/**
 * @brief			CSystemStringヘッダーファイル
 * @file			CSystemString.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef __SYSTEM_STRING_H__
#define __SYSTEM_STRING_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CConfig.h"
#include "../VIEW/VIEW_MODE.h"
#define DEF_STR(s)   const StdString  s =  _T( #s )

/*---------------------------------------------------*/
/*  Class                                            */
/*---------------------------------------------------*/
class CSystemLocal;

//------------------------
//   VIEW_MODE
//------------------------
DEF_STR( STR_VIEW_SELECT );    //!< 選択
DEF_STR( STR_VIEW_POINT  );    //!< 点描画
DEF_STR( STR_VIEW_LINE   );    //!< 線分
DEF_STR( STR_VIEW_CIRCLE );    //!< 円
DEF_STR( STR_VIEW_CIRCLE_3P ); //!< ３点を通る円
DEF_STR( STR_VIEW_ARC    );    //!< 円弧

DEF_STR( STR_VIEW_ELLIPSE);    //!< 楕円
DEF_STR( STR_VIEW_TEXT   );    //!< 文字
DEF_STR( STR_VIEW_NODE   );    //!< 節点
DEF_STR( STR_VIEW_IMAGE  );    //!< 画像
DEF_STR( STR_VIEW_SPLINE );    //!< スプライン
//DEF_STR( STR_VIEW_L_DIM  );    //!< 距離        STR_MNU_H_DIM を使用する
//DEF_STR( STR_VIEW_H_DIM  );    //!< 水平寸法
//DEF_STR( STR_VIEW_V_DIM  );    //!< 垂直寸法
//DEF_STR( STR_VIEW_A_DIM  );    //!< 角度
//DEF_STR( STR_VIEW_D_DIM  );    //!< 直径
//DEF_STR( STR_VIEW_R_DIM  );    //!< 半径
//DEF_STR( STR_VIEW_C_DIM  );    //!< チャンファー

DEF_STR( STR_VIEW_GROUP  );     //!< グループ化
DEF_STR( STR_VIEW_PARTS  );     //!< 部品化
DEF_STR( STR_VIEW_REFERENCE);   //!< 参照化


DEF_STR( STR_VIEW_DELETE );    //!< 削除
DEF_STR( STR_VIEW_MOVE   );    //!< 移動
DEF_STR( STR_VIEW_COPY   );    //!< 複写
DEF_STR( STR_VIEW_ROTATE );    //!< 回転
DEF_STR( STR_VIEW_SCL    );    //!< 倍率
DEF_STR( STR_VIEW_MIRROR );    //!< 鏡像

DEF_STR( STR_VIEW_DISASSEMBLY);  //!< グループ化解除

DEF_STR( STR_VIEW_OFFSET );    //!< オフセット
DEF_STR( STR_VIEW_TRIM   );    //!< トリム
DEF_STR( STR_VIEW_CORNER );    //!< コーナー
DEF_STR( STR_VIEW_CHAMFER);    //!< 面取り
DEF_STR( STR_VIEW_BREAK  );    //!< 分割
DEF_STR( STR_VIEW_FILL   );    //!< 塗りつぶし
DEF_STR( STR_VIEW_COMPOSITION_LINE);    //!< 複合線
DEF_STR( STR_VIEW_CONNECTION_LINE);     //!< 接続線

//------------------------
//   EXEC_COMMON
//------------------------
DEF_STR( STR_EXEC_EDB_FLOOW_SETTING );    //!< 全体の設定に合わせる


//------------------------
//ウインドウ
DEF_STR( STR_WND_OUTPUT      );    //!< 出力
DEF_STR( STR_WND_PROPERTIES  );    //!< プロパティ
DEF_STR( STR_WND_LAYER       );    //!< レイヤー
DEF_STR( STR_WND_ZOOM        );    //!< ズーム
DEF_STR( STR_WND_IO_SET      );    //!< IO設定
DEF_STR( STR_WND_IO_CONNECT  );    //!< IO接続
DEF_STR( STR_WND_IO_REF      );    //!< IO参照
DEF_STR( STR_WND_EXEC_MONITOR);    //!< 実行モニター
DEF_STR( STR_WND_WATCH       );    //!< ウォッチ
DEF_STR( STR_WND_CALL_STACK  );    //!< コールスタック
DEF_STR( STR_WND_PROP_DEF    );    //!< プロパティ定義
//------------------------

//------------------------
// プロパティウインドウ 
//TODO VIEW_MODEとの重複

DEF_STR( STR_PRO_FIG        );    //!< 図形
DEF_STR( STR_PRO_FIG_DEF    );    //!< 図形定義

DEF_STR( STR_PRO_TYPE       );    //!< 図形種別
DEF_STR( STR_PRO_POINT      );    //!< 点
DEF_STR( STR_PRO_CIRCLE     );    //!< 円
DEF_STR( STR_PRO_LINE       );    //!< 線
DEF_STR( STR_PRO_TEXT       );    //!< 文字
DEF_STR( STR_PRO_GROUP      );    //!< グループ
DEF_STR( STR_PRO_PARTS      );    //!< 部品
DEF_STR( STR_PRO_REFERENCE  );    //!< 参照


DEF_STR( STR_PRO_COMPOSITION_LINE  );    //!< 複合線
DEF_STR( STR_PRO_SPLINE     );    //!< スプライン
DEF_STR( STR_PRO_DIM        );    //!< 寸法
DEF_STR( STR_PRO_ELLIPSE    );    //!< 楕円
DEF_STR( STR_PRO_NODE       );    //!< 節点
DEF_STR( STR_PRO_CONNECTION_LINE  );    //!< 接続線

DEF_STR( STR_PRO_SCRIPT     );    //!< スクリプト
DEF_STR( STR_PRO_USER       );    //!< ユーザー定義
DEF_STR( STR_PRO_PROPERTY_SET_VAL );    //!< プロパティセット値

DEF_STR( STR_PRO_FIELD      );    //!< フィールド
DEF_STR( STR_PRO_ELEMENT    );    //!< エレメント
DEF_STR( STR_PRO_BOARD_IO   );    //!< ボードIO
DEF_STR( STR_PRO_NET_IO     );    //!< ネットワークIO
DEF_STR( STR_PRO_PARTS_DEF  );    //!< 部品定義
DEF_STR( STR_PRO_REFERENCE_DEF  );//!< 参照定義
DEF_STR( STR_PRO_FIELF_DEF  );    //!< フィールド定義
DEF_STR( STR_PRO_ELEMENT_DEF);    //!< エレメント定義


DEF_STR( STR_PRO_INFO_TYPE  );    //!< 図形の種別を表します。


DEF_STR( STR_PRO_NAME       );    //!< 名称
DEF_STR( STR_PRO_INFO_NAME  );    //!< 図形に名称を設定します、重複する名称は指定できません。またディフォルトでは名称は設定されていません

DEF_STR( STR_PRO_COLOR      );    //!< 色
DEF_STR( STR_PRO_INFO_COLOR );    //!< 図形の色を設定します。
DEF_STR( STR_PRO_AUTOMATIC  );    //!< 自動
DEF_STR( STR_PRO_MORE_COLORS);    //!< その他の色

DEF_STR( STR_PRO_LAYER_ID   );    //!< レイヤーID
DEF_STR( STR_PRO_INFO_LAYER_ID);  //!< レイヤーを変更します。

DEF_STR( STR_PRO_ID   );          //!< 識別ID
DEF_STR( STR_PRO_INFO_ID);        //!< 図形を識別するためのIDです。編集はできません。

DEF_STR( STR_PROPWIN_X );         //!< X座標
DEF_STR( STR_PROPWIN_Y );         //!< Y座標

DEF_STR( STR_PRO_TOP    );        //!< 上座標
DEF_STR( STR_PRO_BOTTOM );        //!< 下座標
DEF_STR( STR_PRO_LEFT   );        //!< 左座標
DEF_STR( STR_PRO_RIGHT  );        //!< 右座標

DEF_STR( STR_PRO_TAG         );   //!< タグ
DEF_STR( STR_PRO_INFO_TAG    );   //!< タグをカンマ区切りで設定します。図形の分類分けに使用します。

DEF_STR( STR_PRO_VISIBLE     );   //!< 実行時表示
DEF_STR( STR_PRO_INFO_VISIBLE);   //!< 実行時に図形を表示するかを設定します。


DEF_STR( STR_PRO_P1   );          //!< 端点１
DEF_STR( STR_PRO_INFO_P1);        //!< 直線の端点を設定します。

DEF_STR( STR_PRO_P2   );          //!< 端点２
DEF_STR( STR_PRO_INFO_P2);        //!< 直線の端点を設定します。

DEF_STR( STR_PRO_START   );        //!< 始点
DEF_STR( STR_PRO_INFO_START);      //!< 始点を設定します。

DEF_STR( STR_PRO_END   );          //!< 終点
DEF_STR( STR_PRO_INFO_END);        //!< 終点を設定します。

DEF_STR( STR_PRO_ARROW_TYPE_START);        //!< 始点矢印種別 
DEF_STR( STR_PRO_INFO_ARROW_TYPE_START);   //!< 始点側の矢印形状を設定します

DEF_STR( STR_PRO_ARROW_TYPE_END);        //!< 終点矢印種別 
DEF_STR( STR_PRO_INFO_ARROW_TYPE_END);   //!< 終点側の矢印形状を設定します

DEF_STR( STR_PRO_LINE_FORM);        //!< 線形状 
DEF_STR( STR_PRO_INFO_LINE_FORM);   //!< 接続線の形状を設定します

DEF_STR( STR_PRO_LINE_TYPE);      //!< 線種
DEF_STR( STR_PRO_INFO_LINE_TYPE); //!< 線の種類を設定します。

DEF_STR( STR_PRO_LINE_WIDTH);     //!< 線幅
DEF_STR( STR_PRO_INFO_LINE_WIDTH);//!< 線の太さを設定します。

DEF_STR( STR_PRO_POS);            //!< 位置
DEF_STR( STR_PRO_INFO_POS);       //!< 位置を設定します。

DEF_STR( STR_PRO_CENTER);         //!< 中心点
DEF_STR( STR_PRO_INFO_CENTER);    //!< 中心点を設定します。

DEF_STR( STR_PRO_DATUM);          //!< 基準点
DEF_STR( STR_PRO_INFO_DATUM);     //!< 基準点を設定します

DEF_STR( STR_PRO_RAD);            //!< 半径
DEF_STR( STR_PRO_INFO_RAD);       //!< 半径を設定します。

DEF_STR( STR_PRO_RAD1);           //!< 径１
DEF_STR( STR_PRO_INFO_RAD1);      //!< 角度に対応する軸の径を設定します。

DEF_STR( STR_PRO_RAD2);           //!< 径２
DEF_STR( STR_PRO_INFO_RAD2);      //!< 径１に対し直行する軸方向の径を設定します。

DEF_STR( STR_PRO_ANGLE);          //!< 角度
DEF_STR( STR_PRO_INFO_ANGLE);     //!< 角度を設定します。

DEF_STR( STR_PRO_SKEW);           //!< 傾斜
DEF_STR( STR_PRO_INFO_SKEW);      //!< アフィン変換による傾斜(tan(x))を設定します。

DEF_STR( STR_PRO_CENTER_ANGLE);         //!< 中心角
DEF_STR( STR_PRO_INFO_CENTER_ANGLE);    //!< 中心角を設定します。

DEF_STR( STR_PRO_ICON_INDEX);           //!< アイコン番号
DEF_STR( STR_PRO_INFO_ICON_INDEX);      //!< アイコンを選択します。

DEF_STR( STR_PRO_DRAW_INDEX);           //!< 図形番号
DEF_STR( STR_PRO_INFO_DRAW_INDEX);      //!< 画像を分割した時の位置を指定する番号です。右上を0とし左へ進んで行きます。

DEF_STR( STR_PRO_ALPHA);                //!< 透過率
DEF_STR( STR_PRO_INFO_ALPHA);           //!< 図形の透過率を設定します。 0が透明、255で不透明です。

DEF_STR( STR_PRO_WIDTH);                //!< 幅
DEF_STR( STR_PRO_INFO_WIDTH);           //!< 図形の幅を設定します。

DEF_STR( STR_PRO_HEIGHT);                //!< 高さ
DEF_STR( STR_PRO_INFO_HEIGHT);           //!< 図形の高さを設定します。

DEF_STR( STR_PRO_FILE_NAME);            //!< ファイル名
DEF_STR( STR_PRO_INFO_FILE_NAME);       //!< 図形のファイル名です。

DEF_STR( STR_PRO_VALUE);                 //!< 値
DEF_STR( STR_PRO_INFO_VALUE);            //!< 値を設定します。

DEF_STR( STR_PRO_CTRL_POINT);           //!< 制御点
DEF_STR( STR_PRO_INFO_CTRL_POINT);      //!< 制御点を設定します。

DEF_STR(STR_PRO_SPLINE_TYPE);           //!< スプライン種別
DEF_STR(STR_PRO_INFO_SPLINE_TYPE);      //!< スプラインの種別を設定します。

DEF_STR(STR_PRO_EDIT_TYPE);             //!< 編集方法
DEF_STR(STR_PRO_INFO_EDIT_TYPE);        //!< スプラインの形状を設定するための方法を設定します

DEF_STR(STR_PRO_SPLINE_CLOSE);          //!< 閉曲線
DEF_STR(STR_PRO_INFO_SPLINE_CLOSE);     //!< 始点と終点をつなげ、閉じた曲線にします

DEF_STR( STR_PRO_START_ANGLE);          //!< 開始角度
DEF_STR( STR_PRO_INFO_START_ANGLE);     //!< 弧の開始角度を設定します。角度は反時計回りです。

DEF_STR( STR_PRO_END_ANGLE);           //!< 終了角度
DEF_STR( STR_PRO_INFO_END_ANGLE);       //!< 弧の終了角度を設定します。角度は反時計回りです。

DEF_STR( STR_PRO_USE_COLOR);           //!< 固有色
DEF_STR( STR_PRO_INFO_USE_COLOR);      //!< 元の色を使用する場合は、FALSE ここで設定している色を使用する場合はTRUEを設定します。

DEF_STR( STR_PRO_OPERATION_INHERITANCE);       //!< 操作継承 
DEF_STR( STR_PRO_INFO_OPERATION_INHERITANCE);  //!< 部品を貼り付けた部品でも，節点,文字入力、ボタンの操作を可能とします。 

DEF_STR( STR_PRO_EXEC_THREAD_TYPE);           //!< 実行スレッド種別
DEF_STR( STR_PRO_INFO_EXEC_THREAD_TYPE);      //!< 実行に使用するスレッドの種別を設定します。

DEF_STR( STR_PRO_EXEC_CYCKE_TIME);           //!< 実行サイクルタイム
DEF_STR( STR_PRO_INFO_EXEC_CYCKE_TIME);      //!< 実行間隔を設定します

DEF_STR( STR_PRO_EXEC_PRIORITY);           //!< 実行プライオリティ
DEF_STR( STR_PRO_INFO_EXEC_PRIORITY);      //!< 実行するタスクのプライオリティを設定します。1から5までで設定し、1が最も優先度が高くなります。

DEF_STR( STR_PRO_DEBUG_MODE);           //!< 実行デバッグモード設定
DEF_STR( STR_PRO_INFO_DEBUG_MODE);      //!< 実行時のデバッグモードを設定します。

DEF_STR( STR_PRO_DEFINTION_NAME);       //!< 定義名
DEF_STR( STR_PRO_INFO_DEFINTION_NAME);  //!< 定義元の名称を表示します。

DEF_STR( STR_PRO_AUTO_COMPILE);         //!< 自動コンパイル
DEF_STR( STR_PRO_INFO_AUTO_COMPILE);    //!< 編集時に使用する関数（OneditXXXX）を自動的にコンパイルします。

DEF_STR( STR_PRO_EDIT_PARMISSION);       //!< 編集許可
DEF_STR( STR_PRO_INFO_EDIT_PARMISSION);  //!< ファイル変更の許可・不許可を設定します。

DEF_STR( STR_PRO_CONNECT1);       //!< 始点接続先
DEF_STR( STR_PRO_INFO_CONNECT1);  //!< 始点の接続先の名称と接点のインデックス値を表示します。

DEF_STR( STR_PRO_CONNECT2);       //!< 終点接続先
DEF_STR( STR_PRO_INFO_CONNECT2);  //!< 終点の接続先の名称と接点のインデックス値を表示します。


///////////////////////////////////////////////
//NODE

//DEF_STR( STR_PRO_DRAGABLE);           //!< ドラッグ可否
//DEF_STR( STR_PRO_INFO_DRAGABLE);      //!< マーカのドラッグが可能かどうかを設定します。

DEF_STR( STR_PRO_MAX_CONNECT);        //!< 最大接続数
DEF_STR( STR_PRO_INFO_MAX_CONNECT);   //!< １つのノードに対して接続できる物の数を設定します。負の値を設定すると無制限に接続できます。

DEF_STR( STR_PRO_CONNECTION_TYPE);        //!< 接続タイプ
DEF_STR( STR_PRO_INFO_CONNECTION_TYPE);   //!< 接続タイプを設定します。 Activeは自ら接続先を設定し、Passiveは、相手側から接続してもらいます。

DEF_STR( STR_PRO_CONNECTION_OBJECT);       //!< 接続対象
DEF_STR( STR_PRO_INFO_CONNECTION_OBJECT);  //!< 接続する対象を設定します。 Field, Edge, Objectから選択できます。

DEF_STR( STR_PRO_CONNECTION_DIR);         //!< 接続方向
DEF_STR( STR_PRO_INFO_CONNECTION_DIR);    //!< 接続に対する入出力方向を設定します。

DEF_STR( STR_PRO_CONNECTION_LINE_DIR);       //!< 接続線方向
DEF_STR( STR_PRO_INFO_CONNECTION_LINE_DIR);  //!< 接続線を接続する場合の接続線の方向を設定します。

DEF_STR( STR_PRO_UNIT_1);        //!< 単位Ch.1
DEF_STR( STR_PRO_UNIT_2);        //!< 単位Ch.2
DEF_STR( STR_PRO_UNIT_3);        //!< 単位Ch.3
//DEF_STR( STR_PRE_UNIT_KIND );  //!< 物理量
//DEF_STR( STR_PRE_UNIT );       //!< 単位
DEF_STR( STR_PRO_INFO_UNIT_KIND);    //!< 物理量を設定します。設定した物理量によって使用可能な単位が変化します。
DEF_STR( STR_PRO_INFO_UNIT);         //!< 単位を設定します。

DEF_STR( STR_PRO_MARKER);                    //!< マーカ

DEF_STR( STR_PRO_MARKER_SHAPE);               //!< 形状
DEF_STR( STR_PRO_INFO_MARKER_SHAPE);          //!< マーカの形状を設定します。

DEF_STR( STR_PRO_SET_NAME);                   //!< プロパティセット名
DEF_STR( STR_PRO_INFO_SET_NAME);              //!< プロパティセットを設定します。使用しなときはNONEを選択します。


//マーカー形状
DEF_STR( STR_DMS_CIRCLE_FILL);      //!< 円塗りつぶし  
DEF_STR( STR_DMS_CIRCLE);           //!< 円            
DEF_STR( STR_DMS_RECT_FILL);        //!< 四角塗りつぶし
DEF_STR( STR_DMS_RECT);             //!< 四角          
DEF_STR( STR_DMS_DBL_CIRCLE);       //!< 二重丸        
DEF_STR( STR_DMS_DBL_RECT);         //!< 二重角 

//マーカー表示種別
DEF_STR( STR_NDT_SHOW_ALWAYS);          //!< 常に表示する 
DEF_STR( STR_NDT_SHOW_NODE_OPERATION);  //!< ノード操作状態のみ 

DEF_STR( STR_PRO_MARKER_COLOR);               //!< 塗りつぶし色
DEF_STR( STR_PRO_MARKER_INFO_COLOR);          //!< マーカ塗りつぶし時の色を設定します。

DEF_STR( STR_PRO_MARKER_MOVE_TYPE);           //!< 移動制限
DEF_STR( STR_PRO_INFO_MARKER_MOVE_TYPE);      //!< マーカの移動方向の制限を設定します。

//移動制限
DEF_STR( STR_DMT_NOMOVE);               //!< 移動なし
DEF_STR( STR_DMT_ALL_MOVE);             //!< 全方向移動可能
DEF_STR( STR_DMT_LIMITED_DIRECTION);    //!< 1方向移動可能
DEF_STR( STR_DMT_LIMITED_DISTANCE);     //!< 距離固定

//接続線方向
DEF_STR( STR_DML_ALL);        //!< 全方向
DEF_STR( STR_DML_OBJ);        //!< 近傍境界線
DEF_STR( STR_DML_TOP);        //!< 上
DEF_STR( STR_DML_RIGHT);      //!< 右
DEF_STR( STR_DML_BOTTOM);     //!< 下
DEF_STR( STR_DML_LEFT);       //!< 左
DEF_STR( STR_DML_DIRX);       //!< 左右
DEF_STR( STR_DML_DIRY);       //!< 上下

DEF_STR( STR_PRO_MARKER_CORD);                //!< 座標系
DEF_STR( STR_PRO_INFO_MARKER_CORD);           //!< マーカーの移動方向の座標系を設定します。

DEF_STR( STR_PRO_MARKER_DIR);                 //!< マーカ方向
DEF_STR( STR_PRO_INFO_MARKER_DIR);            //!< 移動制限時のマーカの移動方向を設定します。

DEF_STR( STR_PRO_MARKER_DISP_TYPE);           //!< 表示種別
DEF_STR( STR_PRO_INFO_MARKER_DISP_TYPE);      //!< マーカーの表示条件を設定します。

DEF_STR( STR_PRO_PROPERTY_SET);               //!< プロパティセット
DEF_STR( STR_PRO_INFO_PROPERTY_SET);          //!< プロパティセットを設定
DEF_STR( STR_PROPERTY_SET_INPUT);             //!< プロパティセット名を入力してください

DEF_STR( STR_PRO_CONNECTION);               //!< 接続先
DEF_STR( STR_PRO_INFO_CONNECTION);          //!< ノードの接続先を設定します

DEF_STR( STR_PRO_NODE_SNAP);                //!< スナップタイプ
DEF_STR( STR_PRO_INFO_NODE_SNAP);           //!< ノード移動時のスナップタイプを設定します

DEF_STR( STR_PRO_CONNECT_PROPERTYSET);      //!< 接続プロパティセット
DEF_STR( STR_PRO_INFO_CONNECT_PROPERTYSET); //!< 接続可能なプロパティセットを設定します。

DEF_STR( STR_PRO_NODE_BASE_POINT);          //!< 基点
DEF_STR( STR_PRO_INFO_NODE_BASE_POINT);     //!< スナップタイプが、角度,距離場合の基準となるノードの名称を設定します。

//
DEF_STR( STR_PRO_FILL_COLOR);       //!< 塗りつぶし色
DEF_STR( STR_PRO_INFO_FILL_COLOR);  //!< 塗りつぶしを行う色を設定します。

DEF_STR( STR_PRO_FILL);             //!< 塗りつぶし有無
DEF_STR( STR_PRO_INFO_FILL);        //!< 塗りつぶしの有無を設定します。


//編集許可
DEF_STR( STR_EP_NONE);          //!< 制限なし
DEF_STR( STR_EP_SOURCE);        //!< ソース変更禁止
DEF_STR( STR_EP_DESIGN);        //!< デザイン変更禁止
DEF_STR( STR_EP_PROPERTY);      //!< プロパティ変更禁止
DEF_STR( STR_EP_READONRY);      //!< 変更禁止
DEF_STR( STR_EP_ENCRYPTION);    //!< ソースコード暗号化


//線種
DEF_STR( STR_PRO_LINE_SOLID);         //!< 実線
DEF_STR( STR_PRO_LINE_DASH);          //!< 破線
DEF_STR( STR_PRO_LINE_DOT);           //!< 点線
DEF_STR( STR_PRO_LINE_DASHDOT);       //!< １点鎖線
DEF_STR( STR_PRO_LINE_DASHDOTDOT);    //!< ２点鎖線

//線幅
DEF_STR( STR_ID_LINE_WIDTH_1); 
DEF_STR( STR_ID_LINE_WIDTH_2); 
DEF_STR( STR_ID_LINE_WIDTH_3); 
DEF_STR( STR_ID_LINE_WIDTH_4); 
DEF_STR( STR_ID_LINE_WIDTH_5); 
DEF_STR( STR_ID_LINE_WIDTH_6); 
DEF_STR( STR_ID_LINE_WIDTH_7); 
DEF_STR( STR_ID_LINE_WIDTH_8); 
DEF_STR( STR_ID_LINE_WIDTH_9); 
DEF_STR( STR_ID_LINE_WIDTH_10);

//接続線形状
DEF_STR( STR_PRO_LINE_FORM_STRAIGHT); //!< 直線
DEF_STR( STR_PRO_LINE_FORM_MULTI);    //!< 折れ線
DEF_STR( STR_PRO_LINE_FORM_SPLINE);   //!< 曲線

//------------------------

///////////////////////////////////////////////
//DIM
DEF_STR( STR_PRO_DIM_TYPE);            //!< 寸法種別 
DEF_STR( STR_PRO_INFO_DIM_TYPE);       //!< 寸法の種別を設定します

DEF_STR( STR_PRO_DIM_ARROW_TYPE);        //!< 矢印種別 
DEF_STR( STR_PRO_INFO_DIM_ARROW_TYPE);   //!< 矢印の種別を設定します
DEF_STR( STR_PRO_DIM_ARROW_DIR);         //!< 矢印向き
DEF_STR( STR_PRO_INFO_DIM_ARROW_DIR);    //!< 寸法を自動で記入します

DEF_STR( STR_PRO_DIM_TEXT);             //!< 表示テキスト
DEF_STR( STR_PRO_INFO_DIM_TEXT);        //!< 表示するテキストを設定します\n設定した場合自動寸法記入は解除されます
DEF_STR( STR_PRO_DIM_PRIFIX);           //!< 接頭語
DEF_STR( STR_PRO_INFO_DIM_PRIFIX);      //!< 表示するテキストの前に追加する文字を設定します
DEF_STR( STR_PRO_DIM_SUFFIX);           //!< 接尾語
DEF_STR( STR_PRO_INFO_DIM_SUFFIX);      //!< 表示するテキストの後に追加する文字を設定します
DEF_STR( STR_PRO_DIM_AUTO);             //!< 自動寸法記入
DEF_STR( STR_PRO_INFO_DIM_AUTO);        //!< 寸法を自動で記入します
DEF_STR( STR_PRO_DIM_TEXT_POS);         //!< 文字位置
DEF_STR( STR_PRO_INFO_DIM_TEXT_POS);    //!< 文字位置を寸法線の上が下(左か右）を設定します
DEF_STR( STR_PRO_DIM_TEXT_DIR);         //!< 文字向き
DEF_STR( STR_PRO_INFO_DIM_TEXT_DIR);    //!< 文字位置を正方向か逆方向か設定します
DEF_STR( STR_PRO_DIM_TEXT_GAP);         //!< 文字隙間
DEF_STR( STR_PRO_INFO_DIM_TEXT_GAP);    //!< 寸法から文字下部までの距離を設定します
DEF_STR( STR_PRO_DIM_TEXT_SIZE);        //!< 文字サイズ
DEF_STR( STR_PRO_INFO_DIM_TEXT_SIZE);   //!< 文字サイズを設定します
DEF_STR( STR_PRO_DIM_EXT_LINE1);        //!< 寸法補助線1
DEF_STR( STR_PRO_INFO_DIM_EXT_LINE1);   //!< 寸法補助線1の表示の有無を設定します
DEF_STR( STR_PRO_DIM_EXT_LINE2);        //!< 寸法補助線2
DEF_STR( STR_PRO_INFO_DIM_EXT_LINE2);   //!< 寸法補助線2の表示の有無を設定します
DEF_STR( STR_PRO_DIM_POS1);             //!< 寸法位置１
DEF_STR( STR_PRO_INFO_DIM_POS1);        //!< 寸法位置１を設定します
DEF_STR( STR_PRO_DIM_POS2);             //!< 寸法位置２
DEF_STR( STR_PRO_INFO_DIM_POS2);        //!< 寸法位置２を設定します
DEF_STR( STR_PRO_DIM_PRECISION);        //!< 小数点以下の桁数
DEF_STR( STR_PRO_INFO_DIM_PRECISION);   //!< 小数点以下の桁数を設定します

///////////////////////////////////////////////
//PartsDef
DEF_STR( STR_PRO_IMAGE_SET);            //!< 画像設定
DEF_STR( STR_PRO_INFO_IMAGE_SET);       //!< 画像で使用可能な画像を設定します

DEF_STR( STR_PRO_USE_NODE_BOUNDS);      //!< バウンディングボックス使用有無
DEF_STR( STR_PRO_INFO_USE_NODE_BOUNDS); //!< 図形が再編集状態(選択状態の図形をもう一度クリックする)でバウンディングボックスの表示の有無を設定します

DEF_STR(STR_PRO_IGNORE_SEL_PART);      //!< 範囲選択設定無視
DEF_STR(STR_PRO_INFO_IGNORE_SEL_PART); //!< ストレッチモード選択時、矩形選択範囲条件をを強制的に「すべて矩形内に存在する必要がある」ことにする


///////////////////////////////////////////////
//DefautlSet
DEF_STR(STR_PRO_DEFAULT_SET);            //!< 既定値設定
DEF_STR(STR_PRO_INFO_DEFAULT_SET);       //!< 現在の設定を既定値として設定、または解除します
DEF_STR(STR_INIT_DEFAULT_SET);            //!< 既定値として設定


///////////////////////////////////////////////
//FILL
DEF_STR( STR_PRO_FILL_TYPE);            //!< 塗りつぶし種別
DEF_STR( STR_PRO_INFO_FILL_TYPE);       //!< 塗りつぶしの有無、方法を設定します

DEF_STR( STR_PRO_FILL_COLOR1);          //!< 塗りつぶし色
DEF_STR( STR_PRO_INFO_FILL_COLOR1);      //!< 塗りつぶしを行う色を設定します

DEF_STR( STR_PRO_HATCHING_TYPE);        //!< ハッチング種別
DEF_STR( STR_PRO_INFO_HATCHING_TYPE);   //!< ハッチングのを設定します

DEF_STR( STR_PRO_FILL_COLOR2);          //!< グラデーション移行色
DEF_STR( STR_PRO_INFO_FILL_COLOR2);     //!< "グラデーションで変化する色を設定します

DEF_STR( STR_PRO_GRADATION_DIR);        //!< グラデーション方向
DEF_STR( STR_PRO_INFO_GRADATION_DIR);   //!< グラデーションの方向を設定します

DEF_STR( STR_FILL_NONE);                //!< 塗りつぶしなし
DEF_STR( STR_FILL_SOLID);               //!< 塗りつぶし
DEF_STR( STR_FILL_HATCHING);            //!< ハッチング
DEF_STR( STR_FILL_GRADATION);           //!< グラデーション

DEF_STR( STR_HATCH_HORIZONTAL);          //!< 水平
DEF_STR( STR_HATCH_VERTICAL);            //!< 垂直
DEF_STR( STR_HATCH_BDIAGONAL);           //!< 左上から右下
DEF_STR( STR_HATCH_FDIAGONAL);           //!< 左下から右上
DEF_STR( STR_HATCH_CROSS);               //!< 十字
DEF_STR( STR_HATCH_DIAGCROSS);           //!< Ｘ字


///////////////////////////////////////////////
//PartsDef
DEF_STR( STR_PRO_PRINT);                //!< 印刷

DEF_STR( STR_PRO_SHOW_PRINT_AREA);      //!< 印刷範囲表示 
DEF_STR( STR_PRO_INFO_SHOW_PRINT_AREA); //!< 印刷範囲表示を表示します
DEF_STR( STR_PRO_PRINT_ROW);            //!< 印刷行設定 
DEF_STR( STR_PRO_INFO_PRINT_ROW);       //!< 複数印刷するときの行を設定します\n行、列の順で印刷します
DEF_STR( STR_PRO_PRINT_COL);            //!< 印刷列設定  
DEF_STR( STR_PRO_INFO_PRINT_COL);       //!< 複数印刷するときの列を設定します\n行、列の順で印刷します
DEF_STR( STR_PRO_DISP_PAPER);           //!< 用紙枠表示  
DEF_STR( STR_PRO_INFO_DISP_PAPER);      //!< 用紙サイズを表示します\n印刷範囲表示時のみ機能します。
DEF_STR( STR_PRO_OFFSET_EQ_PAPER);      //!< 自動印刷オフセット
DEF_STR( STR_PRO_INFO_OFFSET_EQ_PAPER); //!< 印刷行・列に対するオフセット量を用紙サイズに合わせます。
DEF_STR( STR_PRO_PRINT_OFFSET_ROW);     //!< 行印刷オフセット   
DEF_STR( STR_PRO_INFO_PRINT_OFFSET_ROW);//!< 印刷行に対するオフセット量を設定します。\n自動印刷オフセットがFalseの時機能します。
DEF_STR( STR_PRO_PRINT_OFFSET_COL);     //!< 列印刷オフセット   
DEF_STR( STR_PRO_INFO_PRINT_OFFSET_COL);//!< 印刷列に対するオフセット量を設定します。\n自動印刷オフセットがFalseの時機能します。

DEF_STR( STR_PRO_PAPER);                //!< 用紙
DEF_STR( STR_PRO_PAPER_WIDRH);          //!< 用紙 幅
DEF_STR( STR_PRO_INFO_PAPER_WIDRH);     //!< 用紙の幅を表示します。
DEF_STR( STR_PRO_PAPER_HEIGHT);         //!< 用紙 高さ
DEF_STR( STR_PRO_INFO_PAPER_HEIGHT);    //!< 用紙の高さを表示します。
DEF_STR( STR_PRO_PRINT_WIDTH);          //!< 印字 幅
DEF_STR( STR_PRO_INFO_PRINT_WIDTH);     //!< 印刷可能範囲の幅を表示します
DEF_STR( STR_PRO_PRINT_HEIGHT);         //!< 印字 高さ
DEF_STR( STR_PRO_INFO_PRINT_HEIGHT);    //!< 印刷可能範囲の高さを表示します
DEF_STR( STR_PRO_MARGIN_TOP);           //!< マージン 上
DEF_STR( STR_PRO_INFO_MARGIN_TOP);      //!< 上方向の印刷不可能範囲を表示します。
DEF_STR( STR_PRO_MARGIN_LEFT);          //!< マージン 左
DEF_STR( STR_PRO_INFO_MARGIN_LEFT);     //!< 左方向の印刷不可能範囲を表示します。

//------------------------
DEF_STR( STR_SEL_POINT  );    //!< 任意の位置、点をクリックしてください
DEF_STR( STR_SEL_ANGLE );     //!< 角度を入力してください

DEF_STR( STR_SEL_POINT1 );    //!< 始点を設定します。 任意の位置、点 または図形をクリックしてください
DEF_STR( STR_SEL_POINT2 );    //!< 終点を設定します。 クリック、もしくは 「距離a角度」を入力してくだい
DEF_STR( STR_SEL_MARKER );    //!< マーカを選択で、 点の選択が行えます

DEF_STR( STR_SEL_CIRCLE_CENTER  ) ; //!< 円の中心点または円が接する、直線、円を選択してください。
DEF_STR( STR_SEL_CIRCLE_CENTER1 ) ; //!< 円の半径を選択するか入力してください。

DEF_STR( STR_SEL_CIRCLE_OBJ ); //!< 円の中心点を選択するか半径を入力してください。または、円に接する図形を選択してください。
DEF_STR( STR_SEL_CIRCLE_LLL  )   ; //!< ３直線に接する円を選択してください。
DEF_STR( STR_SEL_CIRCLE_CCC  )   ; //!< ３円に接する円を選択してください。
DEF_STR( STR_SEL_CENTER );    //!< 円の中心点または円が接する、直線、円を選択してください。


DEF_STR( STR_SEL_ELLIPSE_CENTER  ) ; //!< 楕円の中心点を選択してください。

DEF_STR( STR_SEL_RAD    );    //!< 半径を設定します。  半径を入力するか、通過点 または円に接する図形を を選択してください
DEF_STR( STR_SEL_3P_1   );    //!< 通過点１を選択してください
DEF_STR( STR_SEL_3P_2   );    //!< 通過点２を選択してください
DEF_STR( STR_SEL_3P_3   );    //!< 通過点３を選択してください


DEF_STR( STR_SEL_OBJECT );        //!< 図形を選択してください。
DEF_STR( STR_SEL_OBJECT1 );       //!< 図形1を選択してください。
DEF_STR( STR_SEL_OBJECT2 );       //!< 図形2を選択してください。

DEF_STR( STR_SEL_OFFSET );        //!< オフセット量を入力、または図形を選択してください。
DEF_STR( STR_SEL_OFFSET_DIR );    //!< オフセット方向を入力してください・

DEF_STR( STR_SEL_CREATE_POINT );  //!< 任意の位置、図形を選択 又は、X座標, Y座標を入力してください。

//DEF_STR( STR_SEL_R );        //!< 半径を入力してください
//DEF_STR( STR_SEL_C );        //!< 面取長を入力してください


//DEF_STR( STR_SEL_BREAK_POINT );      //!< 分割位置を選択してください

DEF_STR( STR_SEL_MOVE_POINT1 );        //!< 移動の基準となる点を選択してください
DEF_STR( STR_SEL_MOVE_POINT2 );        //!< 移動先の点を選択してください

DEF_STR( STR_SEL_COPY_POINT1 );        //!< 複写元の基準となる点を選択してください
DEF_STR( STR_SEL_COPY_POINT2 );        //!< 複写先の点を選択してください

DEF_STR( STR_SEL_ROTATE_CENTER);        //!< 回転の中心となる点を選択してください
DEF_STR( STR_SEL_ROTATE_ANGLE );        //!< 回転角を入力するか、回転の基準となる線を選択してください
DEF_STR( STR_SEL_ROTATE_ANGLE2 );       //!< 回転の基準からの回転角を入力するか、回転角となる線を選択してください

DEF_STR( STR_SEL_MIRROR_LINE);          //!< 鏡像の対称軸となる直線を選択してください

DEF_STR( STR_SEL_SCL_CENTER);           //!< 拡大、縮小の中心となる点を選択してください
DEF_STR( STR_SEL_SCL);                  //!< 倍率を入力してください  倍率,[Y軸方向倍率]


DEF_STR( STR_SEL_TRIM );            //!< 調整を行う図形を選択してください
DEF_STR( STR_SEL_TRIM_OBJ);         //!< 調整に用いる図形を選択してください

DEF_STR( STR_SEL_BREAK );            //!< 分割を行う図形を選択してください
DEF_STR( STR_SEL_BREAK_OBJ);         //!< 分割に用いる図形、または分割を行う位置を選択してください

DEF_STR( STR_SEL_CORNER1 );          //!< 角を作成する図形１を選択してください
DEF_STR( STR_SEL_CORNER2 );          //!< 角を作成する図形２を選択してください
DEF_STR( STR_SEL_CORNER_RAD );       //!< 半径を入力してください。面取りの場合は数値の前に”C”を入力してください


DEF_STR( STR_SEL_TEXT_POINT );       //!< 文字を入力する位置を指定するか、既に作成された文字を選択してください

//Group
DEF_STR( STR_SEL_GROUP_CENTER);      //!< 基準点を選択してください

//ELLIPSE
DEF_STR( STR_SEL_ELLIPSE_VAL);       //!< 長径, 短径[, 角度] を入力してください

//SPLINE
DEF_STR( STR_SEL_SPLINE_POINT);       //!< スプラインを通る点を選択してください。終点をクリックすると終了します
DEF_STR( STR_DLG_SPLINE_REG);         //!< スプラインを登録しますか？

    //LOOP
DEF_STR( STR_SEL_COMPOSITION_LINE );              //!< 追加する図形を選択してください;
DEF_STR( STR_DLG_LOOP_REG);           //!< 図形を登録しますか？
DEF_STR( STR_DLG_DEL_ORG );           //!< 元図形を削除しますか？;

//
DEF_STR( STR_SEL_TMP);               //!< 説明内容が決まっていません

//-----------------
DEF_STR( STR_ERROR_INTERSECT);                  //!< 交点が存在しません
DEF_STR( STR_ERROR_BREAK);                      //!< 分割できない位置を指定しています
DEF_STR( STR_ERROR_CORNER);                     //!< 角を作成できません
DEF_STR( STR_ERROR_SELECT_BEFORE_OBJECT);       //!< 先に図形を選択してください
DEF_STR( STR_ERROR_SELECT_OFFSET_OBJECT);       //!<  オフセット値を入力するか、オフセット位置(点)をクリックしてください    
DEF_STR( STR_ERROR_SELECT_BEFORE_CENTER);       //!< 先に中心点を選択してください
DEF_STR( STR_ERROR_CREATE_OBJECT);              //!< 図形が作成できません
DEF_STR( STR_ERROR_INVALID_NUMBER);             //!< 有効な数値ではありません
DEF_STR( STR_ERROR_INVALID_FONTSIZE);           //!< フォントサイズは１〜 1638 までの範囲です
DEF_STR( STR_ERROR_INPUT_STRING);               //!< 文字を入力してください
DEF_STR( STR_ERROR_SAME_STRING);                //!< "既に設定されている文字があります
DEF_STR( STR_ERROR_INVALID_STRING);             //!< "使用できない文字\n「」が含まれています
DEF_STR( STR_ERROR_COPY_FAIL);                  //!< コピーに失敗しました
DEF_STR( STR_ERROR_SET_BEFORE_TYPE);            //!< 先に種別を設定してください
DEF_STR( STR_ERROR_CANNOT_CREATE_DIR);          //!< ディレクトリが作成できませんでした
DEF_STR( STR_ERROR_OPEN_FILE);                  //!< ファイルを開くことができませんでした
DEF_STR( STR_ERROR_FILE_LOAD);                  //!< ファイルの読み込みに失敗しました
DEF_STR( STR_ERROR_OBJECT_NAME);                //!< 使用できない文字が設定されています\n名称は、アルファベット若しくはは＿（アンダースコア）から始まり 数字、アンダースコア、アルファベットから成る文字列を設定してください
DEF_STR( STR_ERROR_ANGLE_VAL);                  //!< 角度[, 複写回数 ]を入力してください
DEF_STR( STR_ERROR_MORE_REDO);                  //!< これ以上やり直しはできません
DEF_STR( STR_ERROR_MAX_MIN);                    //!< 最大値 >= 最小値 となるように設定してください
DEF_STR( STR_ERROR_SET_TYPE);                   //!< 型の設定に失敗しました
DEF_STR( STR_ERROR_SET_NAME);                   //!< 名称を設定してください
DEF_STR( STR_ERROR_FILE_LOAD_AND_DELETE);       //!< ファイルの読み込みに失敗しました。\nリストから除外しますか

//メニュー関連
DEF_STR( STR_MNU_SET_FORMATTING);              //!< 文字フォーマット
DEF_STR( STR_MNU_SET_DRAWING);                 //!< 描画
DEF_STR( STR_MNU_SET_CUSTMIZE);                //!< ユーザー設定

DEF_STR( STR_MNU_SET_TEXT_COLORS_ITEM);        //!< 文字色...
DEF_STR( STR_MNU_SET_LINE_COLORS_ITEM);        //!< 線色...
DEF_STR( STR_MNU_SET_FILL_COLORS_ITEM);        //!< 塗りつぶし色...

DEF_STR( STR_MNU_SET_AUTOMATIC);               //!< 自動
DEF_STR( STR_MNU_SET_MORE_COLORS);             //!< その他の色
DEF_STR( STR_MNU_SET_TEXT_COLORS);             //!< 文字の色
DEF_STR( STR_MNU_SET_LINE_COLORS);             //!< 線の色
DEF_STR( STR_MNU_SET_FILL_COLORS);             //!< 塗りつぶしの色

//レイヤウインドウ
DEF_STR( STR_LAYER_COLOR);             //!< 色
DEF_STR( STR_LAYER_NAME );             //!< 名称
DEF_STR( STR_LAYER_SCL  );             //!< 倍率
DEF_STR (STR_MNU_LAYER);        //!< レイヤー
DEF_STR (STR_MNU_LAYER_ADD);    //!< 追加
DEF_STR (STR_MNU_LAYER_DEL);    //!< 削除
DEF_STR (STR_MNU_LAYER_SCL);    //!< 全体倍率設定

DEF_STR (STR_LAYER_DEL_QUESTION);       //!< このレイヤーに図形が含まれていますが、削除しますか？
DEF_STR (STR_LAYER_NO_SEL_UNVISIVLE);   //!< 非表示のレイヤーは変更しない
DEF_STR (STR_LAYER_NO_SEL_SCL);         //!< 倍率が異なるレイヤーは変更しない
DEF_STR (STR_LAYER_SEL_ZERO);           //!< ０番レイヤーは変更しない
DEF_STR (STR_LAYER_INVALID_SCL);        //!< 正しい倍率を設定してください(倍率>0)

//操作ウインドウ
DEF_STR( STR_OBJECT_VIEW  );            //!< オブジェクトビュー
DEF_STR( STR_ELEMENT_VIEW );            //!< エレメントビュー
DEF_STR( STR_LIB_VIEW     );            //!< ライブラリビュー

//ファイル操作
DEF_STR( STR_FILE_SAVEFILE        );    //!< 名前を付けて保存
DEF_STR( STR_FILE_SAVEFILECOPY    );    //!< 名前を付けて保存
DEF_STR( STR_FILE_ALLFILTER       );    //!< すべてのファイル (*.*)
DEF_STR( STR_FILE_SAK_TO_SAVE     );    //!< "%s への変更を保存しますか?"
DEF_STR( STR_FILE_LOAD_PROJECT    );    //!< プロジェクトファイルの読み込み
DEF_STR( STR_FILE_NEW             );    //!< 新規ファイル
DEF_STR( STR_FILE_OPEN            );    //!< 開く
DEF_STR( STR_FILE_CLOSE           );    //!< 閉じる
DEF_STR( STR_FILE_SAVE            );    //!< 保存
DEF_STR( STR_FILE_SAVE_AS         );    //!< 名前をつけて保存

//タブグループ
DEF_STR( STR_MNU_MDITABS           );    //!< タブグループ
DEF_STR( STR_MNU_DROP_MDITABS      );    //!< タブグループ
DEF_STR( STR_MDI_TABBED_DOCUMENT   );    //!< タブ付きドキュメント
DEF_STR( STR_MDI_NEW_HORZ_TAB_GROUP);    //!< 水平タブグループの新規作成
DEF_STR( STR_MDI_NEW_VERT_GROUP    );    //!< 垂直タブグループの新規作成
DEF_STR( STR_MDI_MOVE_TO_NEXT_GROUP);    //!< 次のタブグループへ移動
DEF_STR( STR_MDI_MOVE_TO_PREV_GROUP);    //!< 前のタブグループへ移動
DEF_STR( STR_MDI_CLOSE             );    //!< 閉じる

//=======================
//オブジェクトビュー
//=======================

//オブジェクトビュー ポップアップメニュー
DEF_STR( STR_MNU_OBJ_FOLDER );             //!< フォルダー作成
DEF_STR( STR_MNU_OBJ_NAME   );             //!< 名称変更
DEF_STR( STR_MNU_OBJ_PARTS  );             //!< 部品作成
DEF_STR( STR_MNU_OBJ_REFERENCE);           //!< 参照作成
DEF_STR( STR_MNU_OBJ_FIELD  );             //!< フィールド作成
DEF_STR( STR_MNU_OBJ_ELEMENT);             //!< エレメント作成

DEF_STR( STR_MNU_OBJ_SAVE   );             //!< 保存



DEF_STR( STR_MNU_OBJ_SCRIPT );             //!< スクリプト作成
DEF_STR( STR_MNU_OBJ_CUT    );             //!< 切り取り
DEF_STR( STR_MNU_OBJ_COPY   );             //!< コピー
DEF_STR( STR_MNU_OBJ_PASTE  );             //!< 貼り付け
DEF_STR( STR_MNU_OBJ_DELETE );             //!< 削除

//オブジェクトビュー フォルダー名入力ダイアログ
DEF_STR( STR_DLG_FOLDER_TITLE );             //!< フォルダー作成
DEF_STR( STR_DLG_FOLDER_INFO  );             //!< フォルダー名を入力してください

//オブジェクトビュー 部品名入力ダイアログ
DEF_STR( STR_DLG_PARTS_TITLE );             //!< 部品作成
DEF_STR( STR_DLG_PARTS_INFO  );             //!< 部品名を入力してください
DEF_STR( STR_DLG_PARTS_ERROR );             //!< 部品生成に失敗しました

DEF_STR( STR_DLG_ELEMENT_TITLE );            //!< 要素作成
DEF_STR( STR_DLG_ELEMENT_INFO  );            //!< 要素名を入力してください
DEF_STR( STR_DLG_ELEMENT_ERROR );            //!< 要素生成に失敗しました

DEF_STR( STR_DLG_FIELD_TITLE );              //!< フィールド作成
DEF_STR( STR_DLG_FIELD_INFO  );              //!< フィールド名を入力してください
DEF_STR( STR_DLG_FIELD_ERROR );              //!< フィールド生成に失敗しました

DEF_STR( STR_DLG_REFERENCE_TITLE );              //!< 参照作成
DEF_STR( STR_DLG_REFERENCE_INFO  );              //!< 参照名を入力してください
DEF_STR( STR_DLG_REFERENCE_ERROR );              //!< 参照生成に失敗しました


//オブジェクトビュー スクリプト名入力ダイアログ
DEF_STR( STR_DLG_SCRIPT_TITLE);             //!< スクリプト作成
DEF_STR( STR_DLG_SCRIPT_INFO );             //!< スクリプトを入力してください
DEF_STR( STR_DLG_SCRIPT_ERROR);             //!< スクリプト生成に失敗しました

//オブジェクトビュー コピー名入力ダイアログ   
DEF_STR( STR_DLG_COPY_TITLE);             //!< コピー作成
DEF_STR( STR_DLG_COPY_INFO );             //!< コピー先の名称を入力してください



//=================
//View メニュー項目
//=================
//ファイル
DEF_STR (STR_MNU_FILE   );          //!< ファイル(&F)
DEF_STR (STR_MNU_PROJECT_NEW   );   //!< 新規プロジェクトの作成
DEF_STR (STR_MNU_PROJECT_OPEN  );   //!< プロジェクトを開く

DEF_STR (STR_MNU_FILE_CLOSE );      //!< ファイルを閉じる(&C)
DEF_STR (STR_MNU_PROJECT_CLOSE );   //!< プロジェクトを閉じる

DEF_STR (STR_MNU_SAVE_FILE );           //!< %sの保存
DEF_STR (STR_MNU_SAVE_AS_FILE );        //!< %sに名前をつけけて保存
DEF_STR (STR_MNU_SAVE_FILE_NONAME );    //!< ファイルの保存
DEF_STR (STR_MNU_SAVE_AS_FILE_NONAME ); //!< 名前をつけけて保存

DEF_STR (STR_MNU_SAVE_ALL );        //!< すべてのファイルを保存します\nすべてを保存

DEF_STR (STR_MNU_FILE_PRINT );           //!< 印刷(&P)...
DEF_STR (STR_MNU_FILE_PRINT_PREVIEW );   //!< 印刷プレビュー(&V)
DEF_STR (STR_MNU_FILE_PRINT_SETUP); //!< プリンタの設定(&R)...

DEF_STR (STR_MNU_FILE_MRU_FILE);   //!< 最近使ったファイル
DEF_STR (STR_MNU_PROJECT_MRU_FILE);   //!< 最近使ったプロジェクト

DEF_STR (STR_MNU_APP_EXIT   );      //!< アプリケーションの終了(&X)


DEF_STR (STR_MNU_FILE_NEW   );      //!< 新規作成(&N)
DEF_STR (STR_MNU_FILE_OPEN  );      //!< 開く(&O)...


//表示
DEF_STR (STR_MNU_VIEW   );                  //!< 表示(&V)
DEF_STR (STR_MNU_TOOLBAR_WINDOW);           //!< ツール バーとドッキング ウィンドウ(&T)
DEF_STR (STR_MNU_PLACE_HOLDER);     //!< <プレースホルダ>
DEF_STR (STR_MNU_VIEW_STATUS_BAR);          //!< ステータス バー(&S)
DEF_STR (STR_MNU_VIEW_APPLOOK );            //!< アプリケーションの外観(&A)
DEF_STR (STR_MNU_VIEW_APPLOOK_WIN_2000);    //!< Windows 2000(&2)
DEF_STR (STR_MNU_VIEW_APPLOOK_OFF_XP);      //!< Office XP(&X)
DEF_STR (STR_MNU_VIEW_APPLOOK_WIN_XP);      //!< Windows XP(&W)
DEF_STR (STR_MNU_VIEW_APPLOOK_OFF_2003);    //!< Office 2003(&3)
DEF_STR (STR_MNU_VIEW_APPLOOK_VS_2005);     //!< Visual Studio .NET 2005(&5)
DEF_STR (STR_MNU_VIEW_APPLOOK_OFF_2007);    //!< Office 2007(&7)
DEF_STR (STR_MNU_VIEW_APPLOOK_OFF_2007_BLUE);     //!< ブルー スタイル(&B)
DEF_STR (STR_MNU_VIEW_APPLOOK_OFF_2007_BLACK);    //!< ブラック スタイル(&L)
DEF_STR (STR_MNU_VIEW_APPLOOK_OFF_2007_SILVER);   //!< シルバー スタイル(&S)
DEF_STR (STR_MNU_VIEW_APPLOOK_OFF_2007_AQUA);     //!< アクア スタイル(&A)


//ヘルプ
DEF_STR ( STR_MNU_HELP );           //!< ヘルプ(&H)
DEF_STR ( STR_MNU_APP_ABOUT);       //!< バージョン情報 MockSketch(&A)...

//編集
DEF_STR (STR_MNU_EDIT);             //!< 編集(&E)
DEF_STR (STR_MNU_UNDO);             //!< 元に戻す(&U)\tCtrl+Z
DEF_STR (STR_MNU_REDO);             //!< やり直し(&Y)\tCtrl+Y

//-----------
//重複あり
//-----------
DEF_STR (STR_MNU_CUT);              //!< 切り取り(&T)\tCtrl+X
DEF_STR (STR_MNU_COPY);             //!< コピー(&C)\tCtrl+C
DEF_STR (STR_MNU_PASTE);            //!< 貼り付け(&P)\tCtrl+V
//-----------


//描画
DEF_STR (STR_MNU_DRAW   );    //!< 描画

DEF_STR (STR_MNU_DRAW_ITEM);    //!< 図形                         
DEF_STR (STR_MNU_POINT  );    //!< 点 //    ID_MNU_POINT
DEF_STR (STR_MNU_LINE   );    //!< 直線 //    ID_MNU_POINT
DEF_STR (STR_MNU_LINE_SEG);   //!< 線分

DEF_STR (STR_MNU_CIRCLE );      //!< 円                        
DEF_STR (STR_MNU_3P_CIRCLE);    //!< 円-3点指定                

DEF_STR (STR_MNU_ELLIPSE);      //!< 楕円                          
DEF_STR (STR_MNU_SPLINE);       //!< スプライン

DEF_STR (STR_MNU_DIM);          //!< 寸法
DEF_STR (STR_MNU_L_DIM);        //!< 距離                      
DEF_STR (STR_MNU_H_DIM);        //!< 水平                      
DEF_STR (STR_MNU_V_DIM);        //!< 垂直                     
DEF_STR (STR_MNU_A_DIM);        //!< 角度                      
DEF_STR (STR_MNU_D_DIM);        //!< 直径                      
DEF_STR (STR_MNU_R_DIM);        //!< 半径                      
DEF_STR (STR_MNU_C_DIM);        //!< チャンファー                      

DEF_STR (STR_MNU_DRAW_TEXT);    //!< 文字                          
DEF_STR (STR_MNU_NODE);         //!< 節点
DEF_STR (STR_MNU_IMAGE);        //!< 画像

DEF_STR (STR_MNU_DRAW_PROCESS);        //!< 図形加工                         
DEF_STR (STR_MNU_OFFSET);       //!< オフセット                    
DEF_STR (STR_MNU_CORNER);       //!< コーナー                      
DEF_STR (STR_MNU_CHAMFER);      //!< 面取り          
DEF_STR (STR_MNU_TRIM);         //!< トリム                        
DEF_STR (STR_MNU_ITEM_MOVE);    //!< 移動    
DEF_STR (STR_MNU_STRETCH_MODE); //!< ストレッチモード

DEF_STR (STR_MNU_DRAW_EDIT);    //!< 図形編集
DEF_STR (STR_MNU_ITEM_COPY);    //!< 複写                         
DEF_STR (STR_MNU_ITEM_ROTATE);  //!< 回転                          
DEF_STR (STR_MNU_SCL);          //!< 倍率                          
DEF_STR (STR_MNU_MIRROR);       //!< 鏡像                          
DEF_STR (STR_MNU_SELECT);       //!< 選択
DEF_STR (STR_MNU_CUT_MODE);     //!< 切取りモード
DEF_STR (STR_MNU_COPY_MODE);    //!< 複写モード


DEF_STR (STR_MNU_DELETE);       //!< 削除                         
DEF_STR (STR_MNU_DEVIDE);       //!< 分割                         
DEF_STR (STR_MNU_COMPOSITION_LINE);   //!< 複合線                        
DEF_STR (STR_MNU_CONNECTION_LINE);   //!< 接続線                        

DEF_STR (STR_MNU_GROUP);        //!< グループ化 
DEF_STR (STR_MNU_PARTS);        //!< 部品化
DEF_STR (STR_MNU_REFERENCE);    //!< 参照化
DEF_STR (STR_MNU_REL_GROUP);    //!< グループ解除
DEF_STR (STR_MNU_DISASSEMBLY);  //!< 分解

DEF_STR (STR_MNU_CONVERT_SPLINE);  //!< 複合線に変換

DEF_STR (STR_MNU_FILL);         //!< 塗りつぶし                    

DEF_STR (STR_MNU_DRAW_DEF);         //!< 図形定義                    

DEF_STR (STR_MNU_LINE_WIDTH);    //!< 線幅                          
DEF_STR (STR_MNU_LINE_STYLE);    //!< 線種                         

DEF_STR (STR_MNU_LINE_COLOR);    //!< 線色                          
DEF_STR (STR_MNU_BRUSH_COLOR);   //!< 塗りつぶし色                  
DEF_STR (STR_MNU_IMAGE_CTRL);   //!< 画像管理

DEF_STR (STR_MNU_REDRAW);       //!< 再描画
DEF_STR (STR_MNU_VIEW_ALL);     //!< 全体表示

DEF_STR (STR_MNU_DEL_LAST);     //!< 一つ戻る

//フォーマット
DEF_STR (STR_MNU_FORMAT);   //!< フォーマット(&F)
DEF_STR (STR_MNU_FORMAT_FONT);   //!< フォント
DEF_STR (STR_MNU_CHAR_COLOR);    //!< テキスト色 

//スプライン（ポップアップメニュで使用）
DEF_STR (STR_MNU_END);        //!< 終了;
DEF_STR (STR_MNU_CANCEL);     //!< キャンセル;
DEF_STR(STR_MNU_DEL_THIS_POINT);     //!< この点を削除;

DEF_STR(STR_MNU_CLOSED_CURVE);      //!< 閉曲線
DEF_STR(STR_MNU_SPLINE_TYPE);		//!< スプライン種別
DEF_STR(STR_MNU_SPLINE_EDIT_TYPE);  //!< 編集方式

//点（コンテキストメニュで使用）
DEF_STR(STR_MNU_ADD_ALL_POINT);        //!< すべての点を追加する;


//------------------------
//   SPLINE_TYPE
//------------------------
DEF_STR(STR_BEZIER);      //!> _T("ベジェ曲線e")	 },
DEF_STR(STR_BSPLINE2);    //!> _T("2次Bスプライン")  },
DEF_STR(STR_BSPLINE3);    //!> _T("3次Bスプライン")  },
DEF_STR(STR_NURBS);       //!> _T("NURBS")           }

DEF_STR(STR_CONTROL_POINT);  //!> _T("制御点")	 },
DEF_STR(STR_PASS_POINT);     //!> _T("通過点")	 },



//部品選択時 コンテキストメニュー
DEF_STR (STR_MNU_DSP_IO_DLG);     //!< IO設定表示

DEF_STR (STR_MNU_DSP_ORDER);      //!< 順序
DEF_STR (STR_MNU_DSP_ORDER_TOPMOST);    //!< 最前面へ移動する
DEF_STR (STR_MNU_DSP_ORDER_TOPBACK);    //!< 最背面へ移動する
DEF_STR (STR_MNU_DSP_ORDER_FRONT);      //!< 前面へ移動する
DEF_STR (STR_MNU_DSP_ORDER_TBACK);      //!< 背面へ移動する

//
DEF_STR (STR_MNU_VIEW_SETTING);          //!< 表示設定

DEF_STR (STR_MNU_VIEW_POSITION);         //!< 表示位置
DEF_STR (STR_MNU_SHOW_POS_01);   //!< 位置01
DEF_STR (STR_MNU_SHOW_POS_02);   //!< 位置02
DEF_STR (STR_MNU_SHOW_POS_03);   //!< 位置03
DEF_STR (STR_MNU_SHOW_POS_04);   //!< 位置04
DEF_STR (STR_MNU_SHOW_POS_05);   //!< 位置05
DEF_STR (STR_MNU_SHOW_POS_06);   //!< 位置06 
DEF_STR (STR_MNU_SHOW_POS_07);   //!< 位置07
DEF_STR (STR_MNU_SHOW_POS_08);   //!< 位置08 
DEF_STR (STR_MNU_SHOW_POS_09);   //!< 位置09 
DEF_STR (STR_MNU_SHOW_POS_SET);  //!< 位置設定
DEF_STR (STR_SHOW_POS);   //!< 位置 
DEF_STR (STR_DIAG_SHOW_POS_SET);   //!< 設定する位置番号を選択してください 
DEF_STR (STR_TIP_SHOW_POS);        //!< 表示位置\n 表示位置番号を選択します



DEF_STR (STR_MNU_SNAP);                 //!< スナップ
DEF_STR (STR_MNU_SNAP_END_POINT);       //!< 端点  
DEF_STR (STR_MNU_SNAP_MID_POINT);       //!< 中点  
DEF_STR (STR_MNU_SNAP_CROSS_POINT);     //!< 交点 
DEF_STR (STR_MNU_SNAP_ON_LINE);         //!< 線上   
DEF_STR (STR_MNU_SNAP_NEAR_POINT);      //!< 近接点  
DEF_STR (STR_MNU_SNAP_CENTER);          //!< 中心点   
DEF_STR (STR_MNU_SNAP_TANGENT);         //!< 接線   
DEF_STR (STR_MNU_SNAP_FEATURE_POINT);   //!< 特徴点
DEF_STR (STR_MNU_SNAP_DATUM_POINT);     //!< 基準点
DEF_STR (STR_MNU_SNAP_GRID);            //!< グリッド      
DEF_STR (STR_MNU_SNAP_POINT);           //!< 点      
DEF_STR (STR_MNU_SNAP_NORM);            //!< 垂線        
DEF_STR (STR_MNU_SNAP_NODE);            //!< ノード

DEF_STR (STR_MNU_DRAW_FIGURE);         //!< 図形描画

//=======================

//=======================
// IO入力
//=======================
DEF_STR( STR_IOS_ADDRESS );    //!< アドレスオフセット

DEF_STR( STR_IOS_ID   );       //!< ID
DEF_STR( STR_IOS_TYPE );       //!< 種別
DEF_STR( STR_IOS_NAME );       //!< 名称
DEF_STR( STR_IOS_INIT );       //!< 初期値
DEF_STR( STR_IOS_NOTE );       //!< 説明
DEF_STR( STR_IOS_LEN  );       //!< データ長
DEF_STR( STR_IOS_UNIT );       //!< 単位
DEF_STR( STR_IOS_MAX  );       //!< 最大値 (未使用)
DEF_STR( STR_IOS_MIN  );       //!< 最小値 (未使用)
DEF_STR( STR_IOS_CONECT );     //!< 接続先
DEF_STR( STR_IOS_CONV );       //!< 変換係数;
DEF_STR( STR_IOS_CONV_VAL );   //!< 変換値
DEF_STR( STR_IOS_VAL );        //!< 値
//=======================

//=======================
// IO接続
//=======================
DEF_STR( STR_IOC_VAL_DIRECT );  //!< 数値(直値)
DEF_STR( STR_IOC_VAL );         //!< 数値
DEF_STR( STR_IOC_CONNECT );     //!< 接続先
//=======================

//=======================
//プロパティ
//=======================
DEF_STR( STR_PORP_SORT_ITEM);       //!< 項目別
DEF_STR( STR_PORP_SORT_ALPH);       //!< アルファベット順

//=============================
// 実行モニター
//=============================
//ポップアップメニュー
DEF_STR( STR_MNU_EXEC_GROUP_START);     //!< 開始
DEF_STR( STR_MNU_EXEC_GROUP_PAUSE);     //!< 一時停止
DEF_STR( STR_MNU_EXEC_GROUP_ABORT);     //!< 終了
DEF_STR( STR_MNU_EXEC_GROUP_BREAK);     //!< 強制終了

DEF_STR( STR_MNU_EXEC_GROUP_CONFIG);    //!< 設定
//=======================

//---------
//エディタ
//---------
DEF_STR( STR_EDITOR_VIEW    );             //!< エディタ

//エディタ ポップアップメニュー
DEF_STR( STR_MNU_EDITOR_CUT    );          //!< 切り取り
DEF_STR( STR_MNU_EDITOR_PASTE  );          //!< 貼り付け
DEF_STR( STR_MNU_EDITOR_COPY   );          //!< コピー

//---------
// ビルド
//---------
DEF_STR( STR_MNU_SCRIPT_BUILD_TOP);        //!< ビルド (&B)

DEF_STR( STR_MNU_SCRIPT_COMPILE);          //!< コンパイル
DEF_STR( STR_MNU_SCRIPT_BUILD);            //!< ビルド
DEF_STR( STR_MNU_SCRIPT_REBUILD);          //!< リビルド
DEF_STR( STR_MNU_SCRIPT_STOP_BUILD);       //!< ビルド停止
DEF_STR( STR_MNU_SCRIPT_EXEC);             //!< (スクリプトを)実行
DEF_STR( STR_MNU_SCRIPT_ABORT);            //!< (スクリプトを)停止
DEF_STR( STR_MNU_SCRIPT_PAUSE);            //!< (スクリプトを)一時停止
DEF_STR( STR_MNU_SCRIPT_RESTART);          //!< (スクリプトを)再開
DEF_STR( STR_MNU_SCRIPT_STEP_IN);          //!< ステップイン
DEF_STR( STR_MNU_SCRIPT_STEP_OVER);        //!< ステップオーバー
DEF_STR( STR_MNU_SCRIPT_EXEC_CUROSR);      //!< カーソル行の前まで実行

DEF_STR( STR_MNU_SCRIPT_DEBUG);            //!< デバッグモード
DEF_STR( STR_MNU_SCRIPT_RELEASE);          //!< リリースモード

DEF_STR( STR_MNU_EDITOR_BREAKPOINT_SET);       //!< ブレークポイントの設定
DEF_STR( STR_MNU_EDITOR_BREAKPOINT_REMOVE);    //!< ブレークポイントの解除
DEF_STR( STR_MNU_EDITOR_CLEAR_BREAKPOINT); //!< 全ブレークポイントの解除

//---------
// ツール
//---------
DEF_STR( STR_MNU_TOOL);             //!< ツール(&T)
DEF_STR( STR_MNU_ADDIN_MANEGER);    //!< アドインマネージャ
DEF_STR( STR_MNU_OPTION);           //!< オプション
DEF_STR( STR_MNU_PROPERTY_GROUP);   //!< プロパティグループ

//-----------
// ウインドウ
//-----------
DEF_STR( STR_MNU_WINDOW );              //!< ウィンドウ(&W)");
DEF_STR( STR_MNU_WINDOW_NEW );          //!< 新しいウィンドウを開く(&N)");
DEF_STR( STR_MNU_WINDOW_SPLIT);         //!< 分割(&P)");

DEF_STR( STR_MNU_MDI_NEW_HORZ_TAB_GROUP);   //!< 水平タブグループの新規作成");
DEF_STR( STR_MNU_MDI_NEW_VERT_GROUP);       //!< 垂直タブグループの新規作成");

//---------
//ウォッチ
//---------
DEF_STR( STR_WATCH    );        //!< ウォッチ;

//ウォッチ ポップアップメニュー
DEF_STR( STR_MNU_WATCH_COPY);   //!< コピー;
DEF_STR( STR_MNU_WATCH_PASTE);  //!< 貼り付け;

DEF_STR( STR_MNU_WATCH_EDIT);   //!< 値の編集;
DEF_STR( STR_MNU_WATCH_SEL);    //!< 全て選択;
DEF_STR( STR_MNU_WATCH_CLEAER); //!< 全てクリア;

DEF_STR( STR_MNU_WATCH_HEX);    //!< 16進表示;

DEF_STR( STR_MNU_CONTRACT);     //!< 1レベル上に折りたたむ;

//------------------------------
//プロパティグループダイアログ
//------------------------------
DEF_STR( STR_MNU_PROP_EDIT_ADD);        //!< 追加
DEF_STR( STR_MNU_PROP_EDIT_DEL);        //!< 削除
DEF_STR( STR_MNU_PROP_EDIT_CHG_NAME);   //!< 名前の変更

DEF_STR( STR_PROP_EDIT_INPUT_NAME);   //!< プロパティセット名を入力してください
DEF_STR( STR_PROP_ERR_INPUT_NAME);    //!< すでにそのプロパティセット名は使用されています
DEF_STR( STR_PROP_ERR_DEL);           //!< プロパティセットの削除に失敗しました

//=======================
// プロパティ編集
//=======================
DEF_STR( STR_PRE_TYPE );       //!< 種別
DEF_STR( STR_PRE_NAME );       //!< 名称
DEF_STR( STR_PRE_VAL_NAME );   //!< 変数名
DEF_STR( STR_PRE_INIT );       //!< 初期値
DEF_STR( STR_PRE_NOTE );       //!< 説明
DEF_STR( STR_PRE_ENABLE);      //!< 有効
DEF_STR( STR_PRE_UNIT_KIND );  //!< 物理量
DEF_STR( STR_PRE_UNIT );       //!< 単位
DEF_STR( STR_PRE_MAX  );       //!< 最大値
DEF_STR( STR_PRE_MIN  );       //!< 最小値

DEF_STR (STR_MNU_PROPED);        //!< プロパティ編集
DEF_STR (STR_MNU_PROPED_UP);     //!< 上に移動
DEF_STR (STR_MNU_PROPED_DOWN);   //!< 下に移動
DEF_STR (STR_MNU_PROPED_ADD);    //!< 追加
DEF_STR (STR_MNU_PROPED_DEL);    //!< 削除

DEF_STR (STR_PRE_ERR_COLOR);     //!< rrggbb形式の16進数を入力してください


//===================================================


//メッセージ
DEF_STR( STR_MB_MESSAGE);                  //!< メッセージ
DEF_STR( STR_MB_DELETE );                  //!< 削除しますか
DEF_STR( STR_MB_SAVE );                    //!<データが変更されています。\n保存しますか？"
DEF_STR( STR_MB_COMPILE );                 //!<以下のファイルに変更があります。\n コンパイルしますか\n"

//メッセージ-ビルド
DEF_STR( STR_MB_BUIDLD_START );          //!< ------ ビルド開始 %s-----\n
DEF_STR( STR_MB_BUIDLD_SUCCESS );        //!< ------ ビルド成功 ------\n
DEF_STR( STR_MB_BUIDLD_FAILURE );        //!< ------ ビルド失敗 ------\n
DEF_STR( STR_MB_BUIDLD_ABORTED );        //!< ------ ビルド中止 ------\n

//メッセージ-実行
DEF_STR( STR_MB_SETUP          );        //!< セットアップ完了 %s: %d\n
DEF_STR( STR_MB_SETUP_FAIL     );        //!< セットアップ失敗 %s: %d\n
DEF_STR( STR_MB_SETUP_EXEC_FAIL);        //!< セットアップ実行失敗 %s: %d\n
DEF_STR( STR_MB_LOOP_ABORT     );        //!< ループ停止 %s: %d\n

DEF_STR( STR_MB_ABORT          );        //!< アボート完了 %s: %d\n
DEF_STR( STR_MB_ABORT_FAIL     );        //!< アボート失敗 %s: %d\n
DEF_STR( STR_MB_ABORT_EXEC_FAIL);        //!< アボート実行失敗 %s: %d\n

   //ファイル
DEF_STR( STR_MB_FILENAME_s );        //!< ファイル名:%s
DEF_STR( STR_MB_LINE_NO_d);          //!< 行:%d

//------------------------
// Action
//------------------------
DEF_STR( STR_ACT_CONNECTION );           //!< 接続
DEF_STR( STR_ACT_NO_CONNECTION );        //!< 未接続


//=============================
// ダイアログ
//=============================
DEF_STR( STR_DLG_CANCEL  );          // キャンセル
DEF_STR( STR_DLG_SEL_DIR );          // ディレクトリ選択

DEF_STR( STR_DLG_CHG_FILE_s);        //ファイル%sが変更されました。\n 再度読み込みますか？
DEF_STR (STR_DLG_DELETE_ORGIN_TEXT); //元の文字列を消去しますか


//-----------------
// 新規プロジェクト
//-----------------
DEF_STR( STR_DLG_NEWPRJ  );             //!< 新規プロジェクト
DEF_STR( STR_DLG_NEWPRJ_REF  );         //!< 参照
DEF_STR( STR_DLG_NEWPRJ_PROJECT  );     //!< プロジェクト名
DEF_STR( STR_DLG_NEWPRJ_DIR  );         //!< ディレクトリ
DEF_STR( STR_DLG_NEWPRJ_CREATE_DIR  );  //!< プロジェクトのディレクトリを作成する
DEF_STR( STR_DLG_NEWPRJ_CREATE_ASSY );  //!< 装置定義を作成する
DEF_STR( STR_DLG_NEWPRJ_XML);           //!< XML形式


DEF_STR( STR_ERR_NEWPRJ_NO_PRJ_NAME );  //!< プロジェクト名を入力してください
DEF_STR( STR_ERR_NEWPRJ_NO_DIR);        //!< ディレクトリを入力してください
DEF_STR( STR_ERR_NEWPRJ_ALREADY_DIR);  //!< すでにプロジェクトのディレクトリが存在します。\n上書きしますか？

//-----------------
// IO接続
//-----------------
DEF_STR( STR_DLG_IOCONNECT  );          //!< IO接続
DEF_STR( STR_DLG_IOCONNECT_CONNECT  );  //!< 接続対象
DEF_STR( STR_DLG_IOCONNECT_PAGE  );     //!< 参照ページ
DEF_STR( STR_DLG_IOCONNECT_IO  );       //!< 対象IO
DEF_STR( STR_DLG_IOCONNECT_ALL  );      //!< 全ページ
DEF_STR( STR_DLG_IOCONNECT_DISP_PAGE ); //!< %04d Page
DEF_STR( STR_DLG_IOCONNECT_NOT  );      //!< 未接続

//-----------------
// IO 設定
//-----------------
DEF_STR( STR_DLG_IOSETTING  );          //!< IO設定
DEF_STR( STR_DLG_IOSETTING_PAGEMAX  );  //!< 最大ページ数

//-----------------
// IO ウインドウ
//-----------------
DEF_STR( STR_IOWND_NOT_SEL     );  //!< 未選択"

//ツールチップ
DEF_STR( STR_IOWND_TIP_PAGE    );  //!< 表示するページを選択します
DEF_STR( STR_IOWND_TIP_SETTING );  //!< IOを設定します
DEF_STR( STR_IOWND_TIP_SELECT  );  //!< 表示するIOを選択します


//------------------------
// ツールバー
//------------------------
DEF_STR( STR_TOOLBAR_CUSTOMIZE  );         //!< カスタマイズ...
DEF_STR( STR_TOOLBAR_STANDARD  );          //!< 標準
DEF_STR( STR_TOOLBAR_BUILD     );          //!< ビルド
DEF_STR( STR_TOOLBAR_LINE      );          //!< 線定義
DEF_STR( STR_TOOLBAR_FIND      );          //!< 検索
DEF_STR( STR_TOOLBAR_EDIT      );          //!< 図形編集
DEF_STR( STR_TOOLBAR_SNAP      );          //!< スナップ
DEF_STR( STR_TOOLBAR_SHOW      );          //!< 表示位置

//------------------------
// モジュール
//------------------------
DEF_STR( STR_MODULE_DEL_FAIL  );         //!< モジュールの削除に失敗しました
DEF_STR( STR_MODULE_ADD       );         //!<   -----%s 追加----\n
DEF_STR( STR_MODULE_ADD_FAIL  );         //!<   スクリプト%sの追加に失敗しました\n
DEF_STR( STR_MODULE_COMPILE   );         //!<   -----%s コンパイル----\n
DEF_STR( STR_MODULE_LOOP_FAIL );         //!<   関数 Loop() が見つかりません\n
DEF_STR( STR_MODULE_NOT_FIND  );         //!< が、見つかりません

//------------------------
// ウォッチ
//------------------------
DEF_STR( STR_WAT_NAME   );         //!< L75,名前
DEF_STR( STR_WAT_VALUE  );         //!< L75,値
DEF_STR( STR_WAT_TYPE   );         //!< L75,型

//------------------------
// 実行モニタ
//------------------------
DEF_STR( STR_EXEC_NAME   );         //!< L75,名前
DEF_STR( STR_EXEC_TIME   );         //!< L75,実行時間

//------------------------
// スナップ名
//------------------------
DEF_STR( STR_END_POINT    );         //!< 端点
DEF_STR( STR_MID_POINT    );         //!< 中点
DEF_STR( STR_CROSS_POINT  );         //!< 交点
DEF_STR( STR_ON_LINE      );         //!< 線上
DEF_STR( STR_NEAR_POINT   );         //!< 近接点
DEF_STR( STR_CENTER       );         //!< 中心点
DEF_STR( STR_TANGENT      );         //!< 接点
DEF_STR( STR_FEATURE_POINT);         //!< 特徴点
DEF_STR( STR_DATUM_POINT  );         //!< 基準点
DEF_STR( STR_LENGTH       );         //!< 長さ
DEF_STR( STR_ANGLE        );         //!< 角度
DEF_STR( STR_BISECTRIX    );         //!< 角２等分線
DEF_STR( STR_NORMAL       );         //!< 垂線
DEF_STR( STR_GRID         );         //!< グリッド
DEF_STR( STR_POINT        );         //!< 点
DEF_STR( STR_INPUT_VAL    );         //!< 入力値
DEF_STR( STR_SNAP_NONE    );         //!< 任意点
DEF_STR( STR_SNAP_NODE    );         //!< 接続点
DEF_STR( STR_OBJECT       );         //!< 図形



DEF_STR( STR_DISTANCE     );         //!< 距離
DEF_STR( STR_WIDTH        );         //!< 幅
DEF_STR( STR_HEIGHT       );         //!< 高さ
DEF_STR( STR_SCALE        );         //!< 倍率

DEF_STR( STR_SNAP_CIRCLE_CONTACT  ); //!< 円に接する円
DEF_STR( STR_SNAP_2CIRCLE_CONTACT ); //!< 2円に接する円
DEF_STR( STR_SNAP_3CIRCLE_CONTACT ); //!< 3円に接する円
DEF_STR( STR_SNAP_2LINE_CONTACT   ); //!< 2直線に接する円
DEF_STR( STR_SNAP_3LINE_CONTACT   ); //!< 3直線に接する円
DEF_STR( STR_SNAP_3POINT_THROUGH  ); //!< 3点を通る円
DEF_STR( STR_SNAP_FIRST_POINT     ); //!< 1点目
DEF_STR( STR_SNAP_SECOND_POINT    ); //!< 2点目

DEF_STR( STR_SNAP_LINE_CIRCLE_CONTACT ); //!< 直線と円に接する円


//------------------------
// 矢印
//------------------------
DEF_STR(STR_AT_NONE        );         //!<なし
DEF_STR(STR_AT_DOT_FILL    );         //!<黒丸
DEF_STR(STR_AT_DOT         );         //!<丸
DEF_STR(STR_AT_OBLIQUE     );         //!<斜線
DEF_STR(STR_AT_OPEN        );         //!<矢印
DEF_STR(STR_AT_FILLED      );         //!<塗潰矢印
DEF_STR(STR_AT_DATUM_BLANK );         //!<基準三角
DEF_STR(STR_AT_DATUM_FILL  );         //!<塗潰基準三角

DEF_STR(STR_ARS_NONE      );          //!<なし
DEF_STR(STR_ARS_START     );          //!<始点
DEF_STR(STR_ARS_END       );          //!<終点
DEF_STR(STR_ARS_BOTH      );          //!<両方

DEF_STR(STR_DLT_DECIMAL     );        //10進表記
DEF_STR(STR_DLT_FRACTION    );        //分数表記
DEF_STR(STR_DLT_ENGINEERING );        //工業図表記   インチのみ
DEF_STR(STR_DLT_ARCHTECTUAL );        //建築図面表記 インチのみ

DEF_STR(STR_DIA_DECIMAL     );        //10進表記
DEF_STR(STR_DIA_DEGMINSEC   );        //度・分・秒
DEF_STR(STR_DIA_RADIANS     );        //ラジアン


//------------------------
// ツールチップ
//------------------------
DEF_STR( TIP_MNU_POINT      );
DEF_STR( TIP_MNU_H_LINE     );
DEF_STR( TIP_MNU_V_LINE     );
DEF_STR( TIP_MNU_A_LINE     );
DEF_STR( TIP_MNU_LINE       );
DEF_STR( TIP_MNU_CIRCLE     );
DEF_STR( TIP_MNU_ARC        );
DEF_STR( TIP_MNU_NODE       );
DEF_STR( TIP_MNU_IMAGE      );
DEF_STR( TIP_MNU_OFFSET     );
DEF_STR( TIP_MNU_CORNER     );
DEF_STR( TIP_MNU_CHAMFER    );
DEF_STR( TIP_MNU_TRIM       );
DEF_STR( TIP_MNU_MOVE       );
DEF_STR( TIP_MNU_ROTATE     );
DEF_STR( TIP_MNU_SELECT     );
DEF_STR( TIP_MNU_DELETE     );
DEF_STR( TIP_MNU_DEVIDE     );
DEF_STR( TIP_MNU_CUT_MODE   );
DEF_STR( TIP_MNU_COPY_MODE  );

//=============================
// 画像選択
//=============================
DEF_STR(STR_ISL_DLG_SEL   );          //!<画像ソース選択
DEF_STR(STR_ISL_FILE      );          //!<ファイル
DEF_STR(STR_ISL_STORED    );          //!<登録済み画像

//=============================
// 画像管理
//=============================
DEF_STR(STR_ISC_DLG_CTRL  );         //!<画像管理
DEF_STR(STR_ISC_ADD      );          //!<追加
DEF_STR(STR_ISC_SET      );          //!<設定
DEF_STR(STR_ISC_DEL      );          //!<削除

//=============================
// 画像設定
//=============================
DEF_STR(STR_ISP_DLG_PROP  );         //!<画像管理

//=============================
// 寸法文字
//=============================
DEF_STR(STR_DTX_DLG  );              //!<寸法文字
DEF_STR(STR_DTX_AUTO );              //!<寸法値による設定

//=============================
// ディフォルト設定
//=============================
DEF_STR(STR_STX_DEFAULT_SET_DLG);    //!<既定値設定
DEF_STR(STR_STX_DEFAULT_SET);        //!<図形の既定値に設定
DEF_STR(STR_STX_HI_PRIORITY_LINE);   //!<線の設定をレイヤーの既定値より優先する
DEF_STR(STR_STX_RESET);              //!<リセット
DEF_STR(STR_STX_CANCEL);             //!<キャンセル


//=============================
// CImageCtrlDlg
//=============================
DEF_STR(STR_ICD_ERROR_LOAD  );         //!<画像が読み込めませんでした

//=============================
// CImagePropertyGrid
//=============================
DEF_STR(STR_IPG_IMAGE  );              //!<画素
DEF_STR(STR_IPG_XDIV  );               //!<X分割数
DEF_STR(STR_IPG_INFO_XDIV  );          //!<X画像を格子状に分割し、番号で表示する図形を設定する\n場合のX方向の分割数
DEF_STR(STR_IPG_YDIV  );               //!<X分割数
DEF_STR(STR_IPG_INFO_YDIV  );          //!<X画像を格子状に分割し、番号で表示する図形を設定する\n場合のX方向の分割数
DEF_STR(STR_IPG_OFFSET  );             //!<オフセット
DEF_STR(STR_IPG_XPOS  );               //!<X位置
DEF_STR(STR_IPG_INFO_XPOS  );          //!<中心点のX位置(単位:pixel)
DEF_STR(STR_IPG_YPOS  );               //!<Y位置
DEF_STR(STR_IPG_INFO_YPOS  );          //!<中心点のY位置(単位:pixel)
DEF_STR(STR_IPG_DPI  );                //!<Dpi
DEF_STR(STR_IPG_INFO_DPI  );           //!<画面１インチ当たりの画素数
DEF_STR(STR_IPG_TRANSPARENT  );        //!<透過有無
DEF_STR(STR_IPG_INFO_TRANSPARENT  );   //!<透過色の使用有無を設定します
DEF_STR(STR_IPG_TRANSPARENT_COLOR  );        //!<透過色
DEF_STR(STR_IPG_INFO_TRANSPARENT_COLOR  );   //!<設定した色を透明にします。

DEF_STR(STR_IPG_TRP_NONE   );        //!<透過色を使用しない
DEF_STR(STR_IPG_TRP_COLOR  );        //!<透過色の色を設定スr
DEF_STR(STR_IPG_TRP_LT     );        //!<画像の右上の点を透過色とする
DEF_STR(STR_IPG_TRP_RB     );        //!<画像の左下の点を透過色とする


DEF_STR(STR_POS_TYPE_NONE         );        //!<なし 
DEF_STR(STR_POS_TYPE_LEFT         );        //!<左 
DEF_STR(STR_POS_TYPE_RIGHT        );        //!<右 
DEF_STR(STR_POS_TYPE_MID          );        //!<左右中央 
DEF_STR(STR_POS_TYPE_TOP          );        //!<上 
DEF_STR(STR_POS_TYPE_BOTTOM       );        //!<下 
DEF_STR(STR_POS_TYPE_CENTER       );        //!<中心 
DEF_STR(STR_POS_TYPE_TOP_LEFT     );        //!<左上 
DEF_STR(STR_POS_TYPE_TOP_RIGHT    );        //!<右上 
DEF_STR(STR_POS_TYPE_TOP_MID      );        //!<上中央 
DEF_STR(STR_POS_TYPE_BOTTTOM_LEFT );        //!<左下 
DEF_STR(STR_POS_TYPE_BOTTTOM_RIGHT);        //!<右下 
DEF_STR(STR_POS_TYPE_BOTTTOM_MID  );        //!<下中央 
DEF_STR(STR_POS_TYPE_CENTER_LEFT  );        //!<中央左 
DEF_STR(STR_POS_TYPE_CENTER_RIGHT );        //!<中央右 
DEF_STR(STR_POS_TYPE_CENTER_MID   );        //!<中央 

    //アライメント
DEF_STR(STR_ALIGNMENT_LEFT         );        //!<左揃え 
DEF_STR(STR_ALIGNMENT_RIGHT        );        //!<右揃え 
DEF_STR(STR_ALIGNMENT_CENTER       );        //!<中央揃え 

    //基準点種別
DEF_STR(STR_BASSE_TYPE_FIRST_LINE  );        //!<1行目左下
DEF_STR(STR_BASSE_TYPE_LAST_LINE   );        //!<最下行目左下;
DEF_STR(STR_BASSE_TYPE_FRAME       );        //!<外枠

    //CDrawingText
DEF_STR(STR_PRO_PITCH              );        //!<文字間隔 
DEF_STR(STR_PRO_INFO_PITCH         );        //!<文字と文字との間の距離を設定します。 
DEF_STR(STR_PRO_LINE_PITCH         );        //!<行間隔 
DEF_STR(STR_PRO_INFO_LINE_PITCH    );        //!<行と行との間の距離を設定します。 
DEF_STR(STR_PRO_X_SCALE            );        //!<縦倍率
DEF_STR(STR_PRO_INFO_X_SCALE       );        //!<縦方向の倍率を設定します。
DEF_STR(STR_PRO_Y_SCALE            );        //!<横倍率
DEF_STR(STR_PRO_INFO_Y_SCALE       );        //!<横方向の倍率を設定します。

DEF_STR(STR_PRO_FRAME              );        //!<枠
DEF_STR(STR_PRO_FRANE_MARGIN       );        //!<余白 

DEF_STR(STR_PRO_USE_FRAME          );        //!<枠の有無 
DEF_STR(STR_PRO_INFO_USE_FRAME     );        //!<文字列を囲む枠の有無を設定します。 

DEF_STR(STR_PRO_LINE_COLOR         );        //!<線色
DEF_STR(STR_PRO_INFO_LINE_COLOR    );        //!<枠線の色を設定します。
DEF_STR(STR_PRO_CORNER_RADIUS      );        //!<角半径                     未使用
DEF_STR(STR_PRO_INFO_CORNER_RADIUS );        //!<枠の角の半径を設定します。 未使用 

DEF_STR(STR_PRO_USE_BACK_COLOR     );        //!<背景色の有無 
DEF_STR(STR_PRO_INFO_USE_BACK_COLOR);        //!<背景色の有無を設定します。
DEF_STR(STR_PRO_BACK_COLOR         );        //!<背景色 
DEF_STR(STR_PRO_INFO_BACK_COLOR    );        //!<枠内の背景色の有無を設定します 

DEF_STR(STR_PRO_FRAME_WIDTH        );        //!<枠幅
DEF_STR(STR_PRO_INFO_FRAME_WIDTH   );        //!<枠の幅をを設定します。
DEF_STR(STR_PRO_FRAME_HEIGHT       );        //!<枠高"
DEF_STR(STR_PRO_INFO_FRAME_HEIGHT  );        //!<枠の高さを設定します。
DEF_STR(STR_PRO_FRAME_ALIGNMENT    );        //!<枠配置                     未使用
DEF_STR(STR_PRO_INFO_FRAME_ALIGNMENT);       //!<枠の配置方法を設定します   未使用

DEF_STR(STR_PRO_AUTO_FIT           );        //!<自動調整 
DEF_STR(STR_PRO_INFO_AUTO_FIT      );        //!<文字に合わせて枠のサイズを調整します。 

DEF_STR(STR_PRO_WORD_WRAP          );        //!<文字折返し 
DEF_STR(STR_PRO_INFO_WORD_WRAP     );        //!<文字を枠内で折り返すかどうかを設定します。 

DEF_STR(STR_PRO_DRAW_LINE          );        //!<枠線の有無
DEF_STR(STR_PRO_INFO_DRAW_LINE     );        //!<枠線の描画の有無を設定します。 

DEF_STR(STR_PRO_MARGIN_USE         );        //!<マージン 
DEF_STR(STR_PRO_INFO_MARGIN_USE    );        //!<枠内にマージンを設定するか否かを設定します。 

DEF_STR(STR_PRO_TEXT_DATUM              );        //!<基準位置 
DEF_STR(STR_PRO_INFO_TEXT_DATUM         );        //!<文字の基準点の位置を設定します 

DEF_STR(STR_PRO_ALIGNMENT          );        //!<文字配置 
DEF_STR(STR_PRO_INFO_ALIGNMENT     );        //!<枠内の文字の配置方法を設定します 

DEF_STR(STR_PRO_DATUM_TYPE         );        //!<基準点種別
DEF_STR(STR_PRO_INFO_DATUM_TYPE    );        //!<基準点の配置方法を設定します 

DEF_STR(STR_PRO_EDITABLE           );        //!<編集可 
DEF_STR(STR_PRO_INFO_EDITABLE      );        //!<実行時に編集できるかを設定します。 
DEF_STR(STR_PRO_REVERSE            );        //!<反転 
DEF_STR(STR_PRO_INFO_REVERSE       );        //!<文字の方向を反転します。 

DEF_STR(STR_PRO_FIX_ROTATE         );        //!<回転固定 
DEF_STR(STR_PRO_INFO_FIX_ROTATE    );        //!<回転時に文字列の方向を固定するか設定します。 
DEF_STR(STR_PRO_FIX_SCALE          );        //!<倍率固定 
DEF_STR(STR_PRO_INFO_FIX_SCALE     );        //!<図形の倍率変更にかかわらずフォントサイズを一定とします。 
DEF_STR(STR_PRO_FIX_DISP_SCALE     );        //!<表示倍率固定 
DEF_STR(STR_PRO_INFO_FIX_DISP_SCALE);        //!<画面を拡大、縮小しても画面に表示される文字の大きさを一定とします。 
DEF_STR(STR_PRO_VERTICAL_WRITING   );        //!<縦書き
DEF_STR(STR_PRO_INFO_VERTICAL_WRITING);      //!<縦書きにします。

DEF_STR(STR_PRO_FRAME_MARGIN_TOP          );        //!<上
DEF_STR(STR_PRO_INFO_FRAME_MARGIN_TOP     );        //!<上端のマージンを設定します。
DEF_STR(STR_PRO_FRAME_MARGIN_BOTTOM       );        //!<下
DEF_STR(STR_PRO_INFO_FRAME_MARGIN_BOTTOM  );        //!<下端のマージンを設定します。
DEF_STR(STR_PRO_FRAME_MARGIN_LEFT         );        //!<左
DEF_STR(STR_PRO_INFO_FRAME_MARGIN_LEFT    );        //!<左端のマージンを設定します。
DEF_STR(STR_PRO_FRAME_MARGIN_RIGHT        );        //!<右
DEF_STR(STR_PRO_INFO_FRAME_MARGIN_RIGHT   );        //!<右端のマージンを設定します。


//色
DEF_STR( STR_COLOR_BLACK            );      //!> _T("Black")             },
DEF_STR( STR_COLOR_BROWN            );      //!> _T("Brown")             },
DEF_STR( STR_COLOR_DARK_OLIVE_GREEN );      //!> _T("Dark Olive Green")  },
DEF_STR( STR_COLOR_DARK_GREEN       );      //!> _T("Dark Green")        },
DEF_STR( STR_COLOR_DARRK_TEAL       );      //!> _T("Dark Teal")         },
DEF_STR( STR_COLOR_DARK_BLUE        );      //!> _T("Dark Blue")         },
DEF_STR( STR_COLOR_INDIGO           );      //!> _T("Indigo")            },
DEF_STR( STR_COLOR_GRAY_80          );      //!> _T("Gray-80%")          },

DEF_STR( STR_COLOR_DARK_RED         );      //!> _T("Dark Red")          },
DEF_STR( STR_COLOR_ORANGE           );      //!> _T("Orange")            },
DEF_STR( STR_COLOR_DARK_YELLOR      );      //!> _T("Dark Yellow")       },
DEF_STR( STR_COLOR_GREEN            );      //!> _T("Green")             },
DEF_STR( STR_COLOR_TEAL             );      //!> _T("Teal")              },
DEF_STR( STR_COLOR_BLUE             );      //!> _T("Blue")              },
DEF_STR( STR_COLOR_BLUE_GRAY        );      //!> _T("Blue-Gray")         },
DEF_STR( STR_COLOR_GRAY_50          );      //!> _T("Gray-50%")          },

DEF_STR( STR_COLOR_RED              );      //!> _T("Red")               },
DEF_STR( STR_COLOR_LIGHT_ORANGE     );      //!> _T("Light Orange")      },
DEF_STR( STR_COLOR_LIME             );      //!> _T("Lime")              }, 
DEF_STR( STR_COLOR_SEA_GREEN        );      //!> _T("Sea Green")         },
DEF_STR( STR_COLOR_AQUA	            );      //!> _T("Aqua")              },
DEF_STR( STR_COLOR_LIGHT_BLUE       );      //!> _T("Light Blue")        },
DEF_STR( STR_COLOR_VIORET           );      //!> _T("Violet")            },
DEF_STR( STR_COLOR_GRAY_40          );      //!> _T("Gray-40%")          },

DEF_STR( STR_COLOR_PINK             );      //!> _T("Pink")              },
DEF_STR( STR_COLOR_GOLD             );      //!> _T("Gold")              },
DEF_STR( STR_COLOR_YELLOW           );      //!> _T("Yellow")            },    
DEF_STR( STR_COLOR_BRIGHT_GREEN     );      //!> _T("Bright Green")      },
DEF_STR( STR_COLOR_TURQUOISE        );      //!> _T("Turquoise")         },
DEF_STR( STR_COLOR_SKY_BLUE	        );      //!> _T("Sky Blue")          },
DEF_STR( STR_COLOR_PLUM	            );      //!> _T("Plum")              },
DEF_STR( STR_COLOR_GRAY_25          );      //!> _T("Gray-25%")          },

DEF_STR( STR_COLOR_ROSE             );      //!> _T("Rose")              },
DEF_STR( STR_COLOR_TAN	            );      //!> _T("Tan")               },
DEF_STR( STR_COLOR_LIGHT_YELLOR     );      //!> _T("Light Yellow")      },
DEF_STR( STR_COLOR_LIGHT_GREEN	    );      //!> _T("Light Green ")      },
DEF_STR( STR_COLOR_LIGHT_TURQUOISE  );      //!> _T("Light Turquoise")	 },
DEF_STR( STR_COLOR_PALE_BLUE	    );      //!> _T("Pale Blue")         },
DEF_STR( STR_COLOR_LAVENDER	        );      //!> _T("Lavender")          },
DEF_STR( STR_COLOR_WHITE            );      //!> _T("White")             }




/**
 * @class   CSystemString
 * @brief                        
 */
class CSystemString : public CConfig
{
protected:
    //!< ビューモード変換
    std::map<VIEW_MODE, StdString> m_mapViewMode;
    std::map<StdString, StdString> m_mapString;
    std::map<StdString, StdString> m_mapColor;

    StdPath m_pathFileName;
public:

    CSystemString();
    ~CSystemString();

    //!< モード
    StdString StrViewMode(VIEW_MODE eMode);

    //!< 文字列取得
    StdString GetString(const StdString& strType);

    //!< 色名取得
    StdString GetColorName(const StdString& strType);

    //!< XML読み込み
    virtual void LoadXml(StdXmlArchiveIn* pXml);

    //!< XML書き込み
    virtual void SaveXml(StdXmlArchiveOut* pXml);

    virtual void Load();
    virtual void Save(); 

protected:
    //!< ディフォルト値設定
    void SetDefault();

private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT

            SERIALIZATION_BOTH("Mode"      , m_mapViewMode);
            SERIALIZATION_BOTH("String"    , m_mapString);
            SERIALIZATION_BOTH("Color"     , m_mapColor);

        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }

};

#endif __SYSTEM_STRING_H__
