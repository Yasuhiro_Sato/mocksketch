/**
 * @brief			CUnit実装ーファイル
 * @file			CUnit.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "CUnit.h"
#include "CSystem.h"
#include "Utility/CUtility.h"

/*---------------------------------------------------*/
/* Static variables                                  */
/*---------------------------------------------------*/



/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
CUnit::CUnit():
 m_iNo          ( 0),   
 m_strID        (_T("")),
 m_iKind        ( 0),
 m_strUnit      (_T("")),
 m_strNote      (_T("")),
 m_dConv        ( 1.0),
 m_dConv2       ( 0.0),
 m_bUse         ( true)
{
}


/**
 *  @brief  デストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
 CUnit::~CUnit()
{

}

/**
 *  @brief  データ設定
 *  @param  [in]  iNo         No
 *  @param  [in]  strID       ID
 *  @param  [in]  strUnit     単位系
 *  @param  [in]  strKind     種別
 *  @param  [in]  strNote     単位
 *  @param  [in]  eUnit       説明
 *  @param  [in]  dConv       標準量への変換量１ 
 *  @param  [in]  dConv2      標準量への変換量２
 *  @retval なし 
 *  @note
 */
bool CUnit::SetData(int iNo, StdString strID, StdString strUnit, int iKind,  
             StdString strNote, 
             E_SYSTEM_OF_UNITS eUnit, double  dConv, double  dConv2)
{
    if (strID == _T(""))
    {
        return false;
    }

    if (fabs(m_dConv) < 1e-20)
    {
        return false;
    }



   m_iNo            = iNo;
   m_strID          = strID;          //!< ID
   m_iKind          = iKind;          //!< 種別
   m_strUnit        = strUnit;        //!< 単位
   m_strNote        = strNote;        //!< 説明
   m_dConv          = dConv;          //!< 標準量への変換量１ 
   m_dConv2         = dConv2;         //!< 標準量への変換量２

    return true;
}


/**
 *  @brief   代入演算子
 *  @param   [in] obj 代入元 
 *  @retval  代入値
 *  @note 
 */
CUnit& CUnit::operator = (const CUnit & obj)
{
   m_iNo            = obj.m_iNo;
   m_strID          = obj.m_strID;          //!< ID
   m_iKind          = obj.m_iKind;          //!< 種別
   m_strUnit        = obj.m_strUnit;        //!< 単位
   m_strNote        = obj.m_strNote;        //!< 説明
   m_dConv          = obj.m_dConv;          //!< 標準量への変換量１ 
   m_dConv2         = obj.m_dConv2;         //!< 標準量への変換量２
   m_bUse           = obj.m_bUse;
   return *this;
}

/**
 * @brief   等価演算子
 * @param	[in] obj 比較元 
 * @return  ture 同値
 * @note 
 */
bool CUnit::operator == (const CUnit & obj) const
{
    if( m_iNo       != obj.m_iNo    ){return false;}
    if( m_strID     != obj.m_strID  ){return false;}
    if( m_iKind     != obj.m_iKind  ){return false;}
    if( m_strUnit   != obj.m_strUnit){return false;}
    if( m_strNote   != obj.m_strNote){return false;}
    if( m_dConv     != obj.m_dConv  ){return false;}
    if( m_dConv2    != obj.m_dConv2 ){return false;}
    if( m_bUse      != obj.m_bUse   ){return false;}

    return true;
}

//-------------------------------------------
//-------------------------------------------
/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
CUnitCtrl::CUnitCtrl()
{
    m_pathFileName = _T("Units.xml");
}


/**
 *  @brief  デストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
 CUnitCtrl::~CUnitCtrl()
{

}


 /**
 *  @brief  ディフォルト値設定
 *  @param  なし
 *  @retval なし 
 *  @note
 */
void CUnitCtrl::SetDefault()
{
    //物理量

    AddKind( _T("None")                 , _T("なし"));

    //基本
    AddKind( _T("Length")               ,_T("長さ")) ;
    AddKind( _T("Time")                 ,_T("時間")) ;
    AddKind( _T("Temperature")          ,_T("温度")) ;
    AddKind( _T("LuminousIntensity")    ,_T("光度")) ;
    AddKind( _T("Mass")                 ,_T("質量")) ;
    AddKind( _T("Current")              ,_T("電流")) ;
    AddKind( _T("Angle")                ,_T("角度")) ;
    AddKind( _T("Object")               ,_T("物体")) ;



    //組み合わせ単位
    AddKind( _T("Area")                ,_T("面積")); 
    AddKind( _T("Volume")              ,_T("体積")); 
    AddKind( _T("Velocity")            ,_T("速度")); 
    AddKind( _T("Acceleration")        ,_T("加速度"));
    AddKind( _T("Force")               ,_T("力"));
    AddKind( _T("Work")                ,_T("仕事"));
    AddKind( _T("Power")               ,_T("仕事率"));
    AddKind( _T("AngulerVelocity")     ,_T("角速度"));
    AddKind( _T("AngulerAcceleration") ,_T("角加速度"));
    AddKind( _T("MomentOfForce")       ,_T("力のモーメント"));
    AddKind( _T("MomentOfInertia")     ,_T("慣性モーメント"));
    AddKind( _T("Pressure")            ,_T("圧力"));
    AddKind( _T("Density")             ,_T("密度"));
    AddKind( _T("Calirie")             ,_T("熱量"));
    AddKind( _T("HeatCapacity")        ,_T("熱容量"));
    AddKind( _T("SpecificHeat")        ,_T("比熱"));
    AddKind( _T("Frequency")           ,_T("振動数"));
    AddKind( _T("Illuminance")         ,_T("照度"));
    AddKind( _T("Voltage")             ,_T("電圧"));
    AddKind( _T("ElectricResistance")  ,_T("電気抵抗"));
    AddKind( _T("Inductance")          ,_T("インダクタンス"));
    AddKind( _T("ElectricPower")       ,_T("電力"));
    AddKind( _T("ElectrostaticCapacity")  ,_T("静電容量")); 
    AddKind( _T("magnetic field")         ,_T("磁界"));
    AddKind( _T("magnetic flux")          ,_T("磁束"));
    AddKind( _T("FlowRate")               ,_T("流量"));


    //Kind.strId = _T("ElasticModule");       Kind.strName = _T("弾性率")    ; m_unitKind.push_back(Kind);
    //Kind.strId = _T("ElementaryCharge");        Kind.strName = _T("電気量"); m_unitKind.push_back(Kind);
    //Kind.strId = _T("ElectricFluxDensity");     Kind.strName = _T("電束密度"); m_unitKind.push_back(Kind);
    //Kind.strId = _T("magnetic flux density");        Kind.strName = _T("磁束密度"); m_unitKind.push_back(Kind);
    // Kind.strId = _T("permeability");        Kind.strName = _T("透磁率"); m_unitKind.push_back(Kind);

    // 単位なし
    AddUnit(_T("NONE"), _T("None"), _T("") , _T(""), SOU_NONE  , 1.0); 

    // 温度
    AddUnit(_T("TEMP_K"), _T("Temperature"), _T("K") , _T("ケルビン"), SOU_SI  , 1.0); 
    AddUnit(_T("TEMP_C"), _T("Temperature"), _T("C") , _T("摂氏")    , SOU_NONE, 1.0, -273.15); 
    AddUnit(_T("TEMP_F"), _T("Temperature"), _T("F") , _T("華氏")    , SOU_NONE, 5.0/9.0, 5.0 * 459.67 / 9.0); 

    // 長さ
    AddUnit(_T("LEN_M"),  _T("Length"), _T("m")   , _T("メートル")         ,  SOU_SIG            , 1.0); 
    AddUnit(_T("LEN_A"),  _T("Length"), _T("Å")  , _T("オングストローム") ,  SOU_NONE           ,1.0e-10); 
    AddUnit(_T("LEN_NM"), _T("Length"), _T("nm")  , _T("ナノメートル")     ,  SOU_NONE           ,1.0e-9); 
    AddUnit(_T("LEN_UM"), _T("Length"), _T("μm") , _T("マイクロメートル") ,  SOU_NONE           ,1.0e-6); 
    AddUnit(_T("LEN_MM"), _T("Length"), _T("mm")  , _T("ミリメートル")     ,  SOU_NONE           ,1.0e-3); 
    AddUnit(_T("LEN_CM"), _T("Length"), _T("cm")  , _T("センチメートル")   ,  SOU_CGS2G          ,1.0e-2); 
    AddUnit(_T("LEN_KM"), _T("Length"), _T("km")  , _T("キロメートル")     ,  SOU_NONE           ,1.0e+3); 
    AddUnit(_T("LEN_IN"), _T("Length"), _T("in")  , _T("インチ")           ,  SOU_NONE           ,2.54e-2); 
    AddUnit(_T("LEN_FT"), _T("Length"), _T("ft")  , _T("フィート")         ,  SOU_FPS            ,30.48e-2); 
    AddUnit(_T("LEN_YD"), _T("Length"), _T("yd")  , _T("ヤード")           ,  SOU_NONE           ,91.44e-2); 
    AddUnit(_T("LEN_ML"), _T("Length"), _T("ml")  , _T("マイル")           ,  SOU_NONE           ,1.6093e-3); 

    //面積
    AddUnit(_T("AREA_M2"),  _T("Area"), _T("m^2")   , _T("平方メートル")       ,  SOU_SIG  ,  1.0)         ; 
    AddUnit(_T("AREA_MM2"), _T("Area"), _T("mm^2")  , _T("平方ミリメートル")   ,  SOU_NONE ,  1.0e-6)      ; 
    AddUnit(_T("AREA_CM2"), _T("Area"), _T("cm^2")  , _T("平方センチメートル") ,  SOU_CGS2G,  1.0e-4)      ; 
    AddUnit(_T("AREA_KM2"), _T("Area"), _T("km^2")  , _T("平方キロメートル")   ,  SOU_NONE ,  1.0e+6)      ; 
    AddUnit(_T("AREA_IN2"), _T("Area"), _T("in")    , _T("平方インチ")         ,  SOU_NONE ,  6.4516e-4)   ; 
    AddUnit(_T("AREA_FT2"), _T("Area"), _T("ft")    , _T("平方フィート")       ,  SOU_FPS  ,  9.290304e-2) ; 
    AddUnit(_T("AREA_YD2"), _T("Area"), _T("yd")    , _T("平方ヤード")         ,  SOU_NONE ,  0.83612736)  ; 
    AddUnit(_T("AREA_ML2"), _T("Area"), _T("ml")     , _T("平方マイル")        ,  SOU_NONE ,  2.58998811e-6); 

    //体積
    AddUnit(_T("VOL_M3"),  _T("Volume"), _T("m^3")   , _T("立方メートル")      ,  SOU_SIG  ,   1.0)      ; 
    AddUnit(_T("VOL_MM3"), _T("Volume"), _T("mm^3")  , _T("立方ミリメートル")  ,  SOU_NONE ,   1.0e-9)   ; 
    AddUnit(_T("VOL_CM3"), _T("Volume"), _T("cm^3")  , _T("立方センチメートル"),  SOU_CGS2G,   1.0e-6)        ; 
    AddUnit(_T("VOL_IN3"), _T("Volume"), _T("in^3")  , _T("立方インチ")       ,   SOU_NONE ,   1.6387064e-5)  ; 
    AddUnit(_T("VOL_FT3"), _T("Volume"), _T("ft^3")  , _T("立方フィート")     ,   SOU_FPS  ,   2.8316846592e+2)  ; 
    AddUnit(_T("VOL_YD3"), _T("Volume"), _T("yd^3")  , _T("立方ヤード")       ,   SOU_NONE ,   0.764554857984)   ; 
    AddUnit(_T("VOL_L  "), _T("Volume"), _T("L")     , _T("リットル")         ,   SOU_NONE ,   1.0e-3)   ; 

    //質量
    AddUnit(_T("MASS_KG"),  _T("Mass"), _T("kg")  , _T("キログラム")        , SOU_SIG,    1.0); 
    AddUnit(_T("MASS_G") ,  _T("Mass"), _T("g")   , _T("グラム")            , SOU_CGS2G,  1.0e-3); 
    AddUnit(_T("MASS_T") ,  _T("Mass"), _T("t")   , _T("トン")              , SOU_NONE,   1.0e+3); 
    AddUnit(_T("MASS_OZ"),  _T("Mass"), _T("oz")  , _T("オンス")            , SOU_NONE,   2.8349523125e-2); 
    AddUnit(_T("MASS_LB"),  _T("Mass"), _T("lb")  , _T("ポンド")            , SOU_FPS,    0.45359237); 

    //時間
    AddUnit(_T("TIME_S") , _T("Time"), _T("s")    , _T("秒")        , SOU_ALL  ,  1.0); 
    AddUnit(_T("TIME_H") , _T("Time"), _T("h")    , _T("時間")      , SOU_NONE ,  3600); 
    AddUnit(_T("TIME_M") , _T("Time"), _T("min")  , _T("分")        , SOU_NONE ,  60.0); 
    AddUnit(_T("TIME_MS"), _T("Time"), _T("msec") , _T("ミリ秒")    , SOU_NONE ,  1.0e-3); 
    AddUnit(_T("TIME_D") , _T("Time"), _T("d")    , _T("日")        , SOU_NONE ,  86400); 
    AddUnit(_T("TIME_Y") , _T("Time"), _T("y")    , _T("年")        , SOU_NONE ,  31536000); 

    //速度
    AddUnit(_T("VELO_MS") , _T("Velocity"), _T("m/s")      , _T("メートル毎秒")        , SOU_SIG,    1.0); 
    AddUnit(_T("VELO_MM") , _T("Velocity"), _T("m/min")    , _T("メートル毎分")        , SOU_NONE,   1.0/60.0); 
    AddUnit(_T("VELO_MM") , _T("Velocity"), _T("m/h")      , _T("メートル毎時")        , SOU_NONE,   1.0/3600.0); 

    AddUnit(_T("VELO_MMS") , _T("Velocity"), _T("cm/s")    , _T("センチメートル毎秒")  , SOU_CGS2G,   1.0e-2); 
    AddUnit(_T("VELO_MMM") , _T("Velocity"), _T("cm/min")  , _T("センチメートル毎分")  , SOU_NONE,    1.0e-2 / 60.0); 
    AddUnit(_T("VELO_MMH") , _T("Velocity"), _T("cm/h")    , _T("センチメートル毎時")  , SOU_NONE,    1.0e-2 / 3600.0); 

    AddUnit(_T("VELO_MMS") , _T("Velocity"), _T("mm/s")    , _T("ミリメートル毎秒")    , SOU_NONE,  1.0e-3); 
    AddUnit(_T("VELO_MMM") , _T("Velocity"), _T("mm/min")  , _T("ミリメートル毎分")    , SOU_NONE,  1.0e-3 / 60.0); 
    AddUnit(_T("VELO_MMH") , _T("Velocity"), _T("mm/h")    , _T("ミリメートル毎時")    , SOU_NONE,  1.0e-3 / 3600.0)   ; 

    AddUnit(_T("VELO_KMH") , _T("Velocity"), _T("km/h")    , _T("キロメートル毎時")    , SOU_NONE,  1.0/3.6); 
    AddUnit(_T("VELO_MLH") , _T("Velocity"), _T("mph")     , _T("マイル毎時")          , SOU_NONE,  0.44704); 
    AddUnit(_T("VELO_YDS") , _T("Velocity"), _T("yps")    , _T("ヤード毎秒")           , SOU_FPS,   0.9144); 


    //加速度
    AddUnit(_T("ACC_MS") , _T("Acceleration"), _T("m/s^2")      , _T("メートル毎秒毎秒")        ,SOU_SIG   ,   1.0); 
    AddUnit(_T("ACC_CMS") , _T("Acceleration"), _T("cm/s^2")    , _T("センチメートル毎秒毎秒")  ,SOU_CGS2G ,   1.0e-2); 
    AddUnit(_T("ACC_MMS") , _T("Acceleration"), _T("mm/s^2")    , _T("ミリメートル毎秒毎秒")    ,SOU_NONE  ,   1.0e-3); 
    AddUnit(_T("ACC_YDS") , _T("Acceleration"), _T("yd/s^2")    , _T("ヤード毎秒毎秒")          ,SOU_FPS   ,   0.9144); 

    //力
    AddUnit(_T("FCE_N")   , _T("Force"), _T("N")          , _T("ニュートン")        ,SOU_SI    ,   1.0); 
    AddUnit(_T("FCE_DYN") , _T("Force"), _T("dyn")        , _T("ダイン")            ,SOU_CGS   ,   1.0e-5); 
    AddUnit(_T("FCE_PDL") , _T("Force"), _T("pdl")        , _T("バウンダル")        ,SOU_NONE  ,   0.138254954376); 
    AddUnit(_T("FCE_KGF") , _T("Force"), _T("kgf")        , _T("重量キログラム")    ,SOU_MKSG  ,   9.80665); 
    AddUnit(_T("FCE_GG")  , _T("Force"), _T("gf")         , _T("重量グラム")        ,SOU_CGSG  ,   9.80665e-3); 
    AddUnit(_T("FCE_LBF") , _T("Force"), _T("lbf")        , _T("重量ポンド")        ,SOU_FPS   ,   4.4482216152605); 


    //角度
    AddUnit(_T("ANGL_DEG"), _T("Angle"), _T("°")        , _T("度")             ,SOU_SI      ,   1.0); 
    AddUnit(_T("ANGL_RAD"), _T("Angle"), _T("rad")       , _T("ラジアン")       ,SOU_NONE    ,   RAD2DEG); 

}

 /**
 *  @brief  初期化
 *  @param  なし
 *  @retval なし 
 *  @note
 */
void CUnitCtrl::Init()
{
    SetDefault();
}


 /**
 *  @brief  物理量追加
 *  @param  [in] strId 
 *  @param  [in] strName 
 *  @retval なし 
 *  @note
 */
bool CUnitCtrl::AddKind(StdString strId, StdString strName)
{
    CUnitKind Kind;

    int iNo = static_cast<int>(m_unitKind.size());

    Kind.strId   = strId;
    Kind.strName = strName;
    Kind.iNo     = iNo;
    m_unitKind.push_back(Kind);

    return true;
}

/**
 *  @brief  物理量ID取得
 *  @param  [in] strUnitKind 物理量名称
 *  @retval true 成功
 *  @note
 */
int CUnitCtrl::GetKindId(StdString strUnitKind)
{
    int iRet = -1;
    std::vector<CUnitKind>::iterator iteFind;
    iteFind  = std::find_if( m_unitKind.begin(), 
                             m_unitKind.end(), 
         GET_FIRST(CUnitKind)::strId == strUnitKind );
    
    if (iteFind !=  m_unitKind.end())
    {
        iRet = (*iteFind).iNo;
    }

    return iRet;
}

/**
 *  @brief  物理量ID取得
 *  @param  [in] strUnitKind 物理量名称
 *  @retval true 成功
 *  @note
 */
int CUnitCtrl::GetKindIdByName(StdString strUnitName)
{
    int iRet = -1;
    std::vector<CUnitKind>::iterator iteFind;
    iteFind  = std::find_if( m_unitKind.begin(), 
                             m_unitKind.end(), 
         GET_FIRST(CUnitKind)::strName == strUnitName );
    
    if (iteFind !=  m_unitKind.end())
    {
        iRet = (*iteFind).iNo;
    }

    return iRet;
}

/**
 *  @brief  物理量設定数取得
 *  @param  なし 
 *  @retval 物理量設定数 
 *  @note
 */
int CUnitCtrl::GetKindSize()
{
    CUnitKind Kind;

    int iSize = static_cast<int>(m_unitKind.size());

    return iSize;
}

/**
 *  @brief  物理量取得
 *  @param  なし 
 *  @retval 物理量設定数 
 *  @note
 */
CUnitKind* CUnitCtrl::GetKind(int iNo)
{
    bool bRet = true;
    int iMax = static_cast<int>(m_unitKind.size());

    if (iNo < 0)    { bRet = false;}
    if (iNo >= iMax){ bRet = false;}
    STD_ASSERT(bRet);


    return &m_unitKind[iNo];
}

 /**
 *  @brief  単位追加
 *  @param  [in] strId 
 *  @param  [in] strUnit 
 *  @param  [in] strKind 
 *  @param  [in] strNote 
 *  @param  [in] eUnit 
 *  @param  [in] dConv 
 *  @param  [in] dConv2 
 *  @retval なし 
 *  @note
 */
bool CUnitCtrl::AddUnit(StdString strID, 
                        StdString strKind, 
                        StdString strUnit,  
                        StdString strNote, 
                        E_SYSTEM_OF_UNITS eUnit, 
                        double  dConv, 
                        double  dConv2)

{
    bool bRet;
    int iNo;
    int iKindNo;
    iKindNo = GetKindId(strKind);

    iNo = static_cast<int>(m_lstUnit.size());

    if (iKindNo == -1)
    {
        STD_DBG(_T("%s is not find."),strKind.c_str());
        return false;
    }

    CUnit unit;
 
    bRet = unit.SetData(iNo, strID, strUnit, iKindNo,  
                 strNote, eUnit, dConv, dConv2);

    if (bRet)
    {
        bRet = AddUnit(&unit);
    }
    return bRet;
}   

/**
 *  @brief  単位追加
 *  @param  [in] Unit 単位
 *  @retval true 成功
 *  @note
 */
bool CUnitCtrl::AddUnit(CUnit* pUnit)
{

    if( GetUnit(pUnit->GetId()) != NULL)
    {
        //既に存在する
        return false;
    }

    m_lstUnit.push_back(*pUnit);
    return true;
}


 /**
 *  @brief  単位取得
 *  @param  [in] strId 単位ID
 *  @retval 単位へのポインター
 *  @note
 */
 CUnit* CUnitCtrl::GetUnit(StdString strId)
 {
    CUnit* pRet = NULL;
    std::vector<CUnit>::iterator iteFind;
    iteFind  = std::find_if( m_lstUnit.begin(), 
                             m_lstUnit.end(), 
         GET_FIRST(CUnit)::m_strID == strId );
    
    if (iteFind !=  m_lstUnit.end())
    {
        pRet = &(*iteFind);
    }

    return pRet;
}

 /**
 *  @brief  単位取得
 *  @param  [in] iNo 単位ID
 *  @retval 単位へのポインター
 *  @note
 */
CUnit* CUnitCtrl::GetUnit(int iNo)
{
    bool bRet = true;
    int iMax = static_cast<int>(m_lstUnit.size());

    if (iNo < 0)    { bRet = false;}
    if (iNo >= iMax){ bRet = false;}
    //STD_ASSERT(bRet);

    if (bRet)
    {
        return &m_lstUnit[iNo];
    }
    else
    {
        return NULL;
    }
}


/**
 *  @brief  単位取得(単位名より)
 *  @param  [in]  strUnit 単位名(mV, km ...)
 *  @retval 単位
 *  @note
 */
CUnit* CUnitCtrl::GetUnitByUnit(StdString strUnit)
{
    CUnit* pRet = NULL;
    std::vector<CUnit>::iterator iteFind;
    iteFind  = std::find_if( m_lstUnit.begin(), 
                             m_lstUnit.end(), 
         GET_FIRST(CUnit)::m_strUnit == strUnit );
    
    if (iteFind !=  m_lstUnit.end())
    {
        pRet = &(*iteFind);
    }

    return pRet;
}

/**
 *  @brief  単位取得(説明より)
 *  @param  [in]  strNote 単位説明
 *  @retval 単位
 *  @note
 */
CUnit* CUnitCtrl::GetUnitByNote(StdString strNote)
{
    CUnit* pRet = NULL;
    std::vector<CUnit>::iterator iteFind;
    iteFind  = std::find_if( m_lstUnit.begin(), 
                             m_lstUnit.end(), 
         GET_FIRST(CUnit)::m_strNote == strNote );
    
    if (iteFind !=  m_lstUnit.end())
    {
        pRet = &(*iteFind);
    }

    return pRet;
}

 /**
 *  @brief  物理量ごとのリスト取得
 *  @param  [in]  strKindName 物理量名
 *  @param  [out] pList 物理量ごとのリスト
 *  @retval なし
 *  @note
 */
bool CUnitCtrl::GetPhysicalQuantityList(std::vector<CUnit*>* pList,
                                        StdString strKind, 
                                        bool bId)
{

    STD_ASSERT(pList != NULL);

    pList->clear();

    int iKindNo;

    if (bId)
    {
        iKindNo = GetKindId(strKind);

    }
    else
    {
        iKindNo = GetKindIdByName(strKind);
    }

    if (iKindNo == -1)
    {
        return false;
    }

    foreach(CUnit& unit, m_lstUnit)
    {
        if (unit.GetKind() == iKindNo)
        {
            if (unit.IsUse())
            {
                pList->push_back(&unit);
            }
        }
    }
    return true;
 }

 /**
 *  @brief  物理量ごとのリスト取得
 *  @param  [out] pList 物理量ごとのリスト
 *  @param  [in]  strCategory 物理量名
 *  @param  [in]  bId true:Id名使用
 *  @retval なし
 *  @note
 */
 bool CUnitCtrl::GetPhysicalQuantityList(std::vector<StdString>* pList,
                                         StdString strKind, 
                                         bool bId)
 {
    int iKindNo;

    STD_ASSERT(pList != NULL);

    pList->clear();

    if (bId)
    {
        iKindNo = GetKindId(strKind);

    }
    else
    {
        iKindNo = GetKindIdByName(strKind);
    }


    if (iKindNo == -1)
    {
        return false;
    }

    foreach(CUnit& unit, m_lstUnit)
    {
        if (unit.GetKind() == iKindNo)
        {
            if (unit.IsUse())
            {
                pList->push_back(unit.GetUnit());
            }
        }
    }
    return true;
 }

 /**
 *  @brief  物理量取得
 *  @param  [in] strId 単位ID
 *  @retval 物理量
 *  @note   
 */
CUnitKind*  CUnitCtrl::GetPhysicalQuantity(StdString strId)
{
    CUnit* pUnit = GetUnit(strId);
    CUnit* pRet = NULL;

    if (pUnit == NULL)
    {
        return NULL;
    }
    return GetKind(pUnit->GetKind());
}



/**
 *  @brief  単位削除
 *  @param  [in] strId 単位ID
 *  @retval true 成功
 *  @note   
 */
bool CUnitCtrl::DeleteUnit(StdString strId)
{

    CUnit* pRet = NULL;


    std::vector<CUnit>::iterator iteFind;
    std::vector<CUnit>::iterator ite;
    iteFind  = std::find_if( m_lstUnit.begin(), 
                             m_lstUnit.end(), 
         GET_FIRST(CUnit)::m_strID == strId );
    
    if (iteFind !=  m_lstUnit.end())
    {
        pRet = &(*iteFind);
    }

    for(ite  = iteFind;
        ite != m_lstUnit.end();
        ite++)
    {
        (*iteFind).m_iNo--;
    }

    m_lstUnit.erase(iteFind);
    return true;
}


/**
 * @brief   ファイル読み込み
 * @param   なし          
 * @retval  なし
 * @note	
 */
void CUnitCtrl::Load()
{
    namespace fs = boost::filesystem;

    StdPath  pathLoad = CSystem::GetSystemPath();
    pathLoad /= m_pathFileName;
#if 0    //設定が決定するまでは毎回読み込み
    if (!fs::exists(pathLoad))
#endif
    {
        //デフォルトデータ設定
        SetDefault();
        return;
    }

    try
    {
        StdStreamIn inFs(pathLoad);
        {
            StdXmlArchiveIn inXml(inFs);
            inXml >> boost::serialization::make_nvp("Root", *this);
        }
        inFs.close();
    }
    catch(boost::system::system_error &e)
    {
        STD_DBG(_T("Filesystem error %s"), e.what());
        
        return;
    }
    catch(...)
    {
        STD_DBG(_T("Unknoen Error"));
        return;
    }
 }

/**
 * @brief   ファイル書き込み
 * @param   なし          
 * @retval  なし
 * @note    
 */
void CUnitCtrl::Save()
{
    StdPath  pathSave =     CSystem::GetSystemPath();
    pathSave /= m_pathFileName;

    try
    {
        StdStreamOut outFs(pathSave);
        {
            StdXmlArchiveOut outXml(outFs);
            outXml << boost::serialization::make_nvp("Root", *this);
        }
        outFs.close();
    }
    catch(boost::system::system_error &e)
    {
        STD_DBG(_T("Filesystem error %s"), e.what());
        return;
    }
    catch(...)
    {
        STD_DBG(_T("Unknoen Error"));
        return;
    }
}


//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void TEST_CUnit()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());


    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));

}
#endif //_DEBUG

//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void TEST_CUnitCtrl()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());


    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));

}
#endif //_DEBUG