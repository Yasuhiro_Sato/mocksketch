/**
 * @brief			CUnitヘッダーファイル
 * @file			CUnit.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

#ifndef __CUNIT_H__
#define __CUNIT_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CConfig.h"


//単位系  //System of Units
enum E_SYSTEM_OF_UNITS
{
    SOU_NONE  =  0,    //!< 単位系なし
    SOU_SI    =  1,    //!< SI単位系
    SOU_CGS   =  2,    //!< CGS単位系
    SOU_MKSG  =  4,    //!< MKS重力単位系
    SOU_SIG   =  5,    //!< SI単位系 &  MKS重力単位系
    SOU_CGSG  =  8,    //!< CGS重力単位系
    SOU_CGS2G = 10,    //!< CGS単位系 & CGS重力単位系
    SOU_FPS   = 16,    //!< FPS単位系
    SOU_ALL   = 0xFFFF,   
};



/**
 * @class   CUnitKind
 * @brief   物理量(physical quantity)
 */
class CUnitKind
{
public:

    //!< 識別子取得
    StdString  GetId()     { return strId;}

    //!< 名称取得
    StdString  GetName()   { return strName;}

protected:
    friend class CUnitCtrl;  

    int             iNo;        //!< 物理量番号
    
    StdString       strId;      //!< 物理量識別子

    StdString       strName;    //!< 物理量名称

private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        StdString strRead;
        try
        {
           ar & boost::serialization::make_nvp("No"       , iNo);
           ar & boost::serialization::make_nvp("ID"       , strId);
           ar & boost::serialization::make_nvp("Name"     , strName);
        }
        catch(...)
        {
            STD_DBG(_T("Serlization Error %s"), strRead.c_str());
        }
    }
};


/**
 * @class   CUnit
 * @brief
 */
class CUnit
{
protected:
   friend class CUnitCtrl;  
   int                  m_iNo;             //!< iNo
   StdString            m_strID;           //!< ID
   E_SYSTEM_OF_UNITS    m_eUnit;           //!< 単位系
   int                  m_iKind;           //!< 種別(物理量)
   StdString            m_strUnit;         //!< 単位
   StdString            m_strNote;         //!< 説明
   double               m_dConv;           //!< 標準量への変換量１ 
   double               m_dConv2;          //!< 標準量への変換量２
   bool                 m_bUse;            //!< 使用可否

public :
    CUnit();

    virtual ~CUnit();

    bool SetData(int iNo,
                 StdString strID, StdString strUnit, int iKind,  
                 StdString strNote, 
                 E_SYSTEM_OF_UNITS eUnit, double  dConv, double  dConv2 = 0.0);

    //!< 代入演算子
    CUnit& operator = (const CUnit & obj);

    //!< 等価演算子
    bool CUnit::operator == (const CUnit & m) const;

    int       GetNo(){return m_iNo;}

    StdString GetUnit() const {return m_strUnit;}

    StdString GetNote() const {return m_strNote;}

    int       GetKind()  const  {return m_iKind;}

    StdString GetId()  const {return m_strID;}

    bool IsUse(){return m_bUse;}

    void SetUse(bool bUse){m_strUnit = bUse;}

    double ToBase(double dVal){return m_dConv * dVal + m_dConv2;}

    double ToThisUnit(double dVal){return (dVal - m_dConv2)/ m_dConv;}


private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        StdString strRead;
        try
        {
           ar & boost::serialization::make_nvp("No"         , m_iNo);
           ar & boost::serialization::make_nvp("ID"         , m_strID);
           ar & boost::serialization::make_nvp("Kind"       , m_iKind);
           ar & boost::serialization::make_nvp("Unit"       , m_strUnit);
           ar & boost::serialization::make_nvp("Note"       , m_strNote);
           ar & boost::serialization::make_nvp("Alpha"      , m_dConv);
           ar & boost::serialization::make_nvp("Beta"       , m_dConv2);
           ar & boost::serialization::make_nvp("Use"        , m_bUse);
        }
        catch(...)
        {
            STD_DBG(_T("Serlization Error %s"), strRead.c_str());
        }
    }
};



/**
 * @class   CUnit
 * @brief
 */
class CUnitCtrl  : public CConfig
{
protected:
    std::vector<CUnitKind>      m_unitKind;
    std::vector<CUnit>          m_lstUnit;

    StdPath m_pathFileName;

    CUnitCtrl* ms_pUnitCtrl;
public:

    //!< コンストラクター
    CUnitCtrl();


    //!< デストラクター
    ~CUnitCtrl();

    //!< 初期化
    void Init();

    //--------------
    //  物理量
    //--------------

    //!< 物理量設定
    bool AddKind(StdString strId, StdString strName);

    //!< 物理量ID取得
    int GetKindId(StdString strId);

    //!< 物理量ID取得(物理量名)
    int GetKindIdByName(StdString strUnitName);

    //!< 物理量設定数取得
    int GetKindSize();

    //!< 物理量取得
    CUnitKind* GetKind(int iNo);

    //--------------
    //!< 物理量ごとのリスト取得
    bool GetPhysicalQuantityList(std::vector<CUnit*>* pList, StdString strKind, bool bId = true);

    //!< 物理量ごとのリスト取得(単位)
    bool GetPhysicalQuantityList(std::vector<StdString>* pList, StdString strKind, bool bId = true);

    //!< 物理量取得
    CUnitKind*  GetPhysicalQuantity(StdString strId);

    //!< 単位取得
    CUnit* GetUnit(StdString strId);

    //!< 単位取得(単位名より)
    CUnit* GetUnitByUnit(StdString strUnit);

    //!< 単位取得(説明より)
    CUnit* GetUnitByNote(StdString strNote);

    //!< 単位取得
    CUnit* GetUnit(int iNo);

    //!< 単位削除
    bool DeleteUnit(StdString strId);


    //!< 単位追加
    bool AddUnit(StdString strID, 
                        StdString strKind,  
                        StdString strUnit, 
                        StdString strNote, 
                        E_SYSTEM_OF_UNITS eUnit, 
                        double  dConv, 
                        double  dConv2 = 0.0);

    //!< 単位追加
    bool AddUnit(CUnit* pUnit);

    //!< ファイル読込
    virtual void Load();

    //!< ファイル書込
    virtual void Save();



protected:

    //!< ディフォルト値設定
    void SetDefault();



private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        StdString strRead;
        try
        {
            ar & boost::serialization::make_nvp("PhysicalQuantity"      , m_unitKind);
            ar & boost::serialization::make_nvp("Units"                 , m_lstUnit);

            if (Archive::is_loading::value)
            {
            }
        }
        catch(...)
        {
            STD_DBG(_T("Serlization Error %s"), strRead.c_str());
        }
    }
};




#endif //__CUNIT_H__
