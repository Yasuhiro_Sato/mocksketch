/**
 * @brief			CDlgSetting実装ファイル
 * @file			DlgSetting.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MockSketch.h"
#include "DlgSetting.h"


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
IMPLEMENT_DYNAMIC(CDlgSetting, CDialog)


BEGIN_MESSAGE_MAP(CDlgSetting, CDialog)
    ON_BN_CLICKED(IDOK, &CDlgSetting::OnBnClickedOk)
    ON_NOTIFY(TVN_SELCHANGED, ID_TREE_SEL, &CDlgSetting::OnTvnSelchangedTreeSystem)
END_MESSAGE_MAP()

/**
 * コンストラクタ
 */
CDlgSetting::CDlgSetting(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgSetting::IDD, pParent)
{

}

/**
 * デストラクタ
 */
CDlgSetting::~CDlgSetting()
{
}

/**
 *  @brief  フレームワークの自動的なデータ交換
 *  @param  pDX     CDataExchange オブジェクトへのポインタ
 *  @retval なし     
 *  @note   UpdateData メンバ関数から呼び出されます。
 */
void CDlgSetting::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, ID_ST_PROPERTY, m_wndPropListLocation);
	DDX_Control(pDX, ID_TREE_SEL, m_treeSystem);
}



BOOL CDlgSetting::OnInitDialog()
{
    CDialog::OnInitDialog();


    m_wndDrawPorperty.SetData(*DRAW_CONFIG);
    m_wndEditorPorperty.SetData(*SYS_EDITOR);
    m_wndSystemPorperty.SetData(*SYS_CONFIG);



	CRect rectPropList;
	m_wndPropListLocation.GetClientRect(&rectPropList);
	m_wndPropListLocation.MapWindowPoints(this, &rectPropList);

	m_wndDrawPorperty.Create(WS_CHILD |  WS_TABSTOP | WS_BORDER, rectPropList, this, (UINT)-1);
    m_wndDrawPorperty.Setup();

	m_wndEditorPorperty.Create(WS_CHILD |  WS_TABSTOP | WS_BORDER, rectPropList, this, (UINT)-1);
    m_wndEditorPorperty.Setup();

	m_wndSystemPorperty.Create(WS_CHILD |  WS_TABSTOP | WS_BORDER, rectPropList, this, (UINT)-1);
    m_wndSystemPorperty.Setup();

    m_wndDrawPorperty.ShowWindow(SW_HIDE);
    m_wndEditorPorperty.ShowWindow(SW_HIDE);
    m_wndSystemPorperty.ShowWindow(SW_HIDE);




    HTREEITEM hItem;
    HTREEITEM hItemFirst;

    hItem = m_treeSystem.InsertItem(_T("Draw") );
    m_treeSystem.SetItemData(hItem, 0);
    hItemFirst = hItem;

    hItem = m_treeSystem.InsertItem(_T("Editor"));
    m_treeSystem.SetItemData(hItem, 1);

    hItem = m_treeSystem.InsertItem(_T("System"));
    m_treeSystem.SetItemData(hItem, 2);

    m_treeSystem.Expand(hItem, TVE_EXPAND );
    m_treeSystem.Select(hItemFirst, TVGN_CARET);
    return TRUE;
}


void CDlgSetting::OnBnClickedOk()
{

    m_wndDrawPorperty.GetData(DRAW_CONFIG);
    m_wndEditorPorperty.GetData(SYS_EDITOR);
    m_wndSystemPorperty.GetData(SYS_CONFIG);

    OnOK();
}


void CDlgSetting::OnTvnSelchangedTreeSystem(NMHDR *pNMHDR, LRESULT *pResult)
{
    LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);

    StdString   strText;
    DWORD_PTR   data;

	HTREEITEM hItem = pNMTreeView->itemNew.hItem;
    strText = m_treeSystem.GetItemText(hItem);
    data = m_treeSystem.GetItemData(hItem);

    m_wndDrawPorperty.ShowWindow    (data == 0 ? SW_SHOW:SW_HIDE);
    m_wndEditorPorperty.ShowWindow  (data == 1 ? SW_SHOW:SW_HIDE);
    m_wndSystemPorperty.ShowWindow  (data == 2 ? SW_SHOW:SW_HIDE);

    *pResult = 0;
}