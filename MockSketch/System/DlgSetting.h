/**
 * @brief			DlgSttingヘッダーファイル
 * @file			DlgStting.h
 * @author			Yasuhiro Sato
 * @date			30-1-2009 15:53:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

#pragma once
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CSystem.h"

/**
 * @class   CDlgSetting
 * @brief   設定ダイアログ                     
 */
class CDlgSetting : public CDialog
{
	DECLARE_DYNAMIC(CDlgSetting)

public:
	CDlgSetting(CWnd* pParent = NULL);   // 標準コンストラクタ
	virtual ~CDlgSetting();

    // ダイアログ データ
	enum { IDD = IDD_DLG_SETTING };

public:
    virtual BOOL OnInitDialog();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート

    afx_msg void OnBnClickedOk();

    afx_msg void OnTvnSelchangedTreeSystem(NMHDR *pNMHDR, LRESULT *pResult);

    DECLARE_MESSAGE_MAP()

    CTreeCtrl m_treeSystem;

	CStatic m_wndPropListLocation;

    CDrawPropertyGrid     m_wndDrawPorperty;
    CEditorPropertyGrid   m_wndEditorPorperty;
    CSystemPropertyGrid   m_wndSystemPorperty;

    CMFCPropertyGridProperty* m_pGroupFont;
};
