/**
 * @brief			MOCK_ERROR実装ファイル
 * @file			MOCK_ERROR.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MOCK_ERROR.h"
#include "CSystem.h"
#include "Utility/CUtility.h"



/**
 *  @brief  コンストラクタ.
 *  @param  なし
 *  @retval なし
 *  @note   
 */
MockException::MockException(void): exception()
{
    m_error = e_no_error;
}
    
/**
 *  @brief  コンストラクタ.
 *  @param  [in] error エラー番号
 *  @retval なし
 *  @note   
 */
MockException::MockException(long error): exception()
{
    m_error = error;
    m_err_message = CSystem::GetInstance()->GetErrorConfig()->GetErrorStr(m_error);
}

/**
 *  @brief  コンストラクタ.
 *  @param  [in] error  エラー番号
 *  @param  [in] format エラーメッセージ
 *  @retval なし
 *  @note   
 */
MockException::MockException(long error, const StdChar *format, ...): exception()
{
   va_list vl;
   static const int iSize =  2048;
   StdChar buf[iSize];

   va_start( vl, format );
   _vsntprintf( buf, iSize, format, vl );
   m_err_message = buf;
   va_end( vl );
}

/**
 *  @brief  コンストラクタ.
 *  @param  [in] format エラーメッセージ
 *  @retval なし
 *  @note   
 */
MockException::MockException(const StdChar *format, ...): exception()
{
    va_list vl;
   static const int iSize =  2048;
   StdChar buf[iSize];

    va_start( vl, format );
    _vsntprintf( buf, iSize, format, vl );
    m_err_message = buf;
    va_end( vl );
    m_error = e_error;
}

/**
 *  @brief  デストラクタ.
 *  @param  なし
 *  @retval なし
 *  @note   
 */
MockException::~MockException(void) throw()
{
}

/**
 *  @brief  エラー番号取得.
 *  @param  なし
 *  @retval なし
 *  @note   
 */
long  MockException::GetErr()
{
    return m_error;
}

/**
 *  @brief  エラーメッセージ取得.
 *  @param  なし
 *  @retval エラーメッセージ
 *  @note   
 */
StdString  MockException::GetErrMsg()
{
    return m_err_message;
}

/**
 *  @brief  エラーメッセージ取得.
 *  @param  なし
 *  @retval エラーメッセージ
 *  @note   
 */
const char* MockException::what() const throw() 
{
    return CUtil::StringToChar(m_err_message).c_str();
}

/**
 *  @brief  メッセージボックス表示.
 *  @param  なし
 *  @retval ダイアログ選択結果
 *  @note   
 */
int MockException::DispMessageBox()
{
    UINT uiMb;
    StdString strType;
    StdString strDisp;

    //m_err_message

    using namespace boost::xpressive;
    // (.+):(.+)
    StdStrXregex staticRegx  =  (s1 = +_) >> _T(':') >> (s2 = +_) ;

    strDisp = m_err_message;

    StdStrXMatchResult matchResult;
    bool bRet = regex_match( m_err_message , matchResult, staticRegx);

    uiMb = MB_OK;
    if (bRet)
    {
        if (matchResult.size() >= 3)
        {
            strType = matchResult.str(1);
            strDisp = matchResult.str(2);
        }

        if      (strType == _T("OK"))                { uiMb = MB_OK;}
        else if (strType == _T("OKCANCEL"))          { uiMb = MB_OKCANCEL;}
        else if (strType == _T("ABORTRETRYIGNORE"))  { uiMb = MB_ABORTRETRYIGNORE;}
        else if (strType == _T("YESNOCANCEL"))       { uiMb = MB_YESNOCANCEL;}
        else if (strType == _T("YESNO"))             { uiMb = MB_YESNO;}
        else if (strType == _T("RETRYCANCEL"))       { uiMb = MB_RETRYCANCEL;}
        else if (strType == _T("CANCELTRYCONTINUE")) { uiMb = MB_CANCELTRYCONTINUE;}
    }

    int iAns = 0;
#ifndef _CONSOLE
    iAns = AfxMessageBox( strDisp.c_str(), uiMb);
#else
    iAns = MessageBox(NULL, strDisp.c_str(), _T("ERROR"), uiMb);
    STD_DBG(_T("MessageBox[%s]"), strDisp.c_str());
#endif
    return iAns;
}

/**
 *  @brief  ファイルエラーメッセージ
 *  @param  [in] bRead  true:読み込み
 *  @param  [in] path   ファイルパス
 *  @param  [in] str    追加文字列
 *  @retval メッセージ
 *  @note   
 */
StdString MockException::FileErrorMessage(bool bRead, 
                                      StdPath path, 
                                      StdString str)
{
    StdStringStream  strmError;
    StdString        strFormat;
    if (bRead)
    {
        strFormat = GET_ERR_STR(e_file_read_s);
    }
    else
    {
        strFormat = GET_ERR_STR(e_file_write_s);
    }
    strmError << StdFormat(strFormat) % path.c_str();

    if (!str.empty())
    {
        strmError << _T(":");
        strmError << str;
    }
    return strmError.str();
}

void DBG_FILE(std::string  s, int iNo)
{
    StdStringStream strm;
    StdString str = CUtil::CharToString(s.c_str());
    strm << StdFormat(_T("%s %s:%s")) % CSystem::GetInstance()->GetErrorConfig()->GetErrorStr( iNo ).c_str() % DB_FUNC_NAME % str.c_str();
    STD_DBG(_T("%s"), strm.str().c_str());\
}
