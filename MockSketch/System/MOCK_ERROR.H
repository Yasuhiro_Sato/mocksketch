#ifndef __MOCK_ERROR_H_
#define __MOCK_ERROR_H_

#include <boost/interprocess/errors.hpp>

#define MOCK_EXCEPTION(no)\
    STD_DBG(_T("%d: %s"), (no), CSystem::GetInstance()->GetErrorConfig()->GetErrorStr((no)));\
    throw MockException(no)\



template<class T> StdString GetObjectNameT(T t)
{
    StdString s;
    return s;
};

#ifdef _DEBUG
#define MOCK_EXCEPTION_FILE(no)\
 DB_PRINT(_T("End "));DB_FUNC_PRINT_TIME;\
}catch(MockException& e){throw e;}\
catch(...){\
    StdStringStream strm;\
    StdString str;\
    str = GetObjectNameT(this);                     \
    if (!str.empty())                               \
    {                                               \
        str += _T(":");                             \
    }                                               \
    str += CUtil::CharToString(s.c_str());          \
    strm << StdFormat(_T("%s %s:%s")) % CSystem::GetInstance()->GetErrorConfig()->GetErrorStr( no ).c_str() % DB_FUNC_NAME % str.c_str();\
    STD_DBG(_T("%s"), strm.str().c_str());\
    throw MockException(no, strm.str().c_str());}\

#else

#define MOCK_EXCEPTION_FILE(no)\
}catch(MockException& e){throw e;}\
catch(...){\
    StdStringStream strm;\
    StdString str;\
    str = GetObjectNameT(this);                     \
    if (!str.empty())                               \
    {                                               \
        str += _T(":");                             \
    }                                               \
    str += CUtil::CharToString(s.c_str());          \
    strm << StdFormat(_T("%s:%s")) % CSystem::GetInstance()->GetErrorConfig()->GetErrorStr( no ).c_str() %  str.c_str();\
    STD_DBG(_T("%s"), strm.str().c_str());\
    throw MockException(no, strm.str().c_str());}\

#endif

#ifdef _DEBUG3
#define SERIALIZATION_INIT std::string  s; try { DB_PRINT(_T("Start "));DB_FUNC_PRINT_TIME;
#else
#define SERIALIZATION_INIT std::string  s; try {
#endif


#define SERIALIZATION_BOTH(str, var)\
    s = str; ar & boost::serialization::make_nvp(s.c_str() , var)

#define SERIALIZATION_LOAD(str, var)\
    s = str; ar >> boost::serialization::make_nvp(s.c_str() , var)

#define SERIALIZATION_SAVE(str, var)\
    s = str; ar << boost::serialization::make_nvp(s.c_str() , var)

void DBG_FILE(std::string  s, int iNo);

#define MOCK_DISP_EXCEPTION_FILE(no)\
catch(MockException& e){\
    DBG_FILE(s, no)\
    throw MockException(no, strm.str().c_str());}\



/**
 * 例外種別
 * 戻り値として使用するときは 形名を E_ERRとする
 */
enum  EXCEPTION_TYPE
{
    e_no_error = 0,

    e_line_create     = 0x000001, // 直線が作成できない 
    e_circle_create   = 0x000002, // 円が作成できない

    e_line_cross      = 0x000003, // 交点がない 

    e_no_answer       = 0x000010, // 解がない 

    //LOOP
    e_no_add_open_line      = 0x000101, // 閉じていない複合線を追加することはできません
    e_no_add_type           = 0x000102, // 複合線に追加できる図形は、直線,円,楕円,スプライン,複合線のみです
    e_no_add_cross          = 0x000103, // 図形を閉じるには、先端と終端が交差している必要があります
    e_no_add_intersection   = 0x000104, // この図形と交差する点がありません
    e_no_add_object         = 0x000105, // 図形が選択されていません




    //------------------
    // 表示関連
    e_view   = 0x010000, 


    //------------------
    // 入力関連
    e_input   = 0x020000, 

    e_same_name     = 0x020001, // 同じ名称がある 
    e_no_use_char   = 0x020002, // 使用できない文字が含まれている 
    e_use_keyword   = 0x020003, // キーワードが使用されています
    e_no_use_file   = 0x020004, // ファイル名として使用できない文字が含まれている 
    e_no_use_first_dot   = 0x020005, // 先頭に"."を使用することはできません 
    e_no_use_end_dot     = 0x020006, // 後端に"."を使用することはできません 
    e_no_use_id   = 0x020007, // 識別子として使用できない文字が含まれている 


    //------------------
    // Boost::interprocess
    // Boost::interprocessのエラーコードはEnum値なのでそのまま割り当てる
    // boost/interprocess/errors.hppを参照
    e_file_system               = 0x030000,
    e_file_system_error         = e_file_system + boost::interprocess::system_error,     // system generated error; if possible, is translated
    e_file_other_error          = e_file_system + boost::interprocess::other_error,      // library generated error
    e_file_security_error       = e_file_system + boost::interprocess::security_error,    // includes access rights, permissions failures
    e_file_read_only_error      = e_file_system + boost::interprocess::read_only_error,
    e_file_io_error             = e_file_system + boost::interprocess::io_error,
    e_file_path_error           = e_file_system + boost::interprocess::path_error,
    e_file_not_found_error      = e_file_system + boost::interprocess::not_found_error,
    //e_file_not_directory_error  = e_file_system + boost::interprocess::not_directory_error,
    e_file_busy_error           = e_file_system + boost::interprocess::busy_error,         // implies trying again might succeed
    e_file_already_exists_error = e_file_system + boost::interprocess::already_exists_error,
    e_file_not_empty_error      = e_file_system + boost::interprocess::not_empty_error,
    e_file_is_directory_error   = e_file_system + boost::interprocess::is_directory_error,
    e_file_out_of_space_error   = e_file_system + boost::interprocess::out_of_space_error,
    e_file_out_of_memory_error  = e_file_system + boost::interprocess::out_of_memory_error,
    e_file_out_of_resource_error= e_file_system + boost::interprocess::out_of_resource_error,



/* 更に追加
   lock_error,
   sem_error,
   mode_error,
   size_error,
   corrupted_error
*/

    //------------------
    // 生成
    e_create            = 0x040000, 
    e_parts_create      = 0x040001,       //部品の生成に失敗しました 
    e_object_full       = 0x040002,       //これ以上、図形を作成できません 
    e_gc_request        = 0x040003,       //ガベージコレクション要求 
    e_main_not_init     = 0x040004,       //メインフレーム未生成 
    e_object_create     = 0x040005,       //図形の生成に失敗しました 


    //------------------
    // 操作関連
    e_action      = 0x050000, 
    e_cycle_ref   = 0x050001,     //!循環参照が発生します 
    e_use_parts   = 0x050002,     //!この部品を使用している所があります\n削除しますか？
    
    //-----------------
    //ユーティリティ
    e_util        = 0x060000, 
    e_val_error   = 0x060001,     //!不正な値 
    e_not_find    = 0x060002,     //!見つからない 
    e_type_error  = 0x060003,     //!型が違う 
    e_range_over  = 0x060004,     //!範囲外 
    e_any_conv    = 0x060005,     //!Any型変換エラー 

    //------------------
    // IO
    e_io                = 0x070000, 
    e_not_value         = 0x070001,     //!数値を入力してください
    e_not_eql_min_max   = 0x070002,     //!最小値と最大値は同じ値に設定しないでください
    e_io_conv_format1   = 0x070003,     //!区分線形補間は [数値:数値], [数値:数値], ..と入力してください
    e_io_conv_format2   = 0x070004,     //!多項式補間  数値, 数値, ..と入力してください
    e_io_conv_inc       = 0x070005,     //!区分線形補間の入力値は、単調増加となるように入力してください
    e_io_conv_inp       = 0x070006,     //!最小値:最大値 又は [入力値:出力値], [入力値:出力値], ...., 若しくは  係数n, 係数n-1,... 係数0 (n>0)と入力してください
    e_type_err_min_max  = 0x070007,     //!最小値と最大値で設定できるのは、AI,AOのみです

    //------------------
    // ファイル
    e_file                  = 0x080000, 
    e_file_read             = 0x080001,     //! 読み込みエラー
    e_file_write            = 0x080002,     //! 書き込みエラー
    e_file_read_s           = 0x080003,     //! 読み込みエラー
    e_file_write_s          = 0x080004,     //! 書き込みエラー
    e_file_ext              = 0x080008,     //! 拡張子エラー
    e_file_exist            = 0x080010,     //! ファイルが存在しません

    //------------------
    //Project
    e_project_save           = 0x090001,     //! プロジェクトの保存に失敗しました
    e_project_create         = 0x090002,     //プロジェクトの生成に失敗しました 


    //------------------
    //Library
    e_library_create         = 0x0A0001,    //プロジェクトの生成に失敗しました 



    //-----------------
	//System
	e_system = 0xFF00000,
    e_buffer_full   = 0xFF00001,     //バッファが一杯
	e_memory_alloc  = 0xFF00002,     //メモリ確保失敗

    //---------------------------
    //PropertyGroup
	e_expect_braces = 0xFF01001,     // "{" がありません
	e_dup_name_s    = 0xFF01002,     // 名称(%s)が重複しています

    //CStdPropertyItemDef
	e_lack_item    = 0xFF01101,     // 項目数が足りません
	e_unkonwn_type = 0xFF01102,     // 不明な型が使用されました

    //------------------



    e_unknown_error = 0x999999,       //!< 不明エラー
    e_error = 0x1000000,              //!< 一般的なエラー
};

/**
 *  例外クラス.
 */
class MockException :public std::exception
{
public:

    //!< コンストラクター 
    MockException(void);
    
    //!<  コンストラクター (エラー番号設定)
    MockException(long error);

    //!<  コンストラクター (エラー番号設定, エラーメッセージ設定)
    MockException(long error, const TCHAR *format, ...);

    //!<  コンストラクター (エラーメッセージ設定)
    MockException(const TCHAR *format, ...);

    //!< デストラクタ
    virtual ~MockException(void) throw();

    //!< エラー番号取得
    long  GetErr();

    //!< エラーメッセージ取得
    StdString  GetErrMsg();

    int DispMessageBox();

    //
    static StdString FileErrorMessage(bool bRead, 
                                      StdPath Path, 
                                      StdString str = _T(""));

     virtual const char* what() const throw();

protected:
    StdString m_err_message;  //!< メッセージ
    long m_error;               //!< エラー番号
};

#endif __MOCK_ERROR_H_
