/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CCustomMenuButton.h"
#include "Utility/CUtility.h"

CCustomMenuButton::CCustomMenuButton(): CMFCToolBarMenuButton()
{
}

CCustomMenuButton::CCustomMenuButton(UINT uiID, HMENU hMenu, int iImage, LPCTSTR lpszText, BOOL bUserButton)
{
    Initialize(uiID, hMenu, iImage, lpszText, bUserButton);
}


void CCustomMenuButton::Initialize(UINT uiID, HMENU hMenu, int iImage, LPCTSTR lpszText, BOOL bUserButton)
{
	CMFCToolBarMenuButton::Initialize();

	m_nID = uiID;
	m_bUserButton = bUserButton;

	CMFCToolBarMenuButton::SetImage(iImage);
	m_strText = (lpszText == NULL) ? _T("") : lpszText;

	CreateFromMenu(hMenu);
}

void CCustomMenuButton::Serialize(CArchive& ar)
{
	CMFCToolBarButton::Serialize(ar);
    /*
    {
	CObject::Serialize(ar);

	if (ar.IsLoading())
	{
		int iImage;

		ar >> m_nID;
		ar >> m_nStyle;
		ar >> iImage;
		ar >> m_strText;
		ar >> m_bUserButton;
		ar >> m_bDragFromCollection;
		ar >> m_bText;
		ar >> m_bImage;
		ar >> m_bVisible;

		SetImage(iImage);
	}
	else
	{
		ar << m_nID;
		ar << m_nStyle;
		ar << GetImage();
		ar << m_strText;
		ar << m_bUserButton;
		ar << m_bDragFromCollection;
		ar << m_bText;
		ar << m_bImage;
		ar << m_bVisible;
	}
    }
	if (ar.IsLoading())
	{
		while (!m_listCommands.IsEmpty())
		{
			delete m_listCommands.RemoveHead();
		}

		UINT uiTearOffBarID;
		ar >> uiTearOffBarID;

		SetTearOff(uiTearOffBarID);

		ar >> m_bMenuPaletteMode;
		ar >> m_nPaletteRows;
	}
	else
	{
		ar << m_uiTearOffBarID;

		ar << m_bMenuPaletteMode;
		ar << m_nPaletteRows;
	}

	m_listCommands.Serialize(ar);
    */
}

void CCustomMenuButton::OnChangeParentWnd(CWnd* pWndParent)
{
    CMFCToolBarButton::OnChangeParentWnd(pWndParent);

    if (pWndParent != NULL)
    {
        if (pWndParent->IsKindOf(RUNTIME_CLASS(CMFCMenuBar)))
        {
            m_bDrawDownArrow = (m_nID != 0 && !m_listCommands.IsEmpty()) || ((CMFCMenuBar *)pWndParent)->GetForceDownArrows();
            m_bText = TRUE;
            m_bImage = FALSE;
        }
        else
        {
            m_bDrawDownArrow = (m_nID == 0 || !m_listCommands.IsEmpty());
        }

        if (pWndParent->IsKindOf(RUNTIME_CLASS(CMFCPopupMenuBar)))
        {
            m_bMenuMode = TRUE;
            m_bText = TRUE;
            m_bImage = FALSE;
            m_bDrawDownArrow = (m_nID == 0 || !m_listCommands.IsEmpty()) || HasButton();
        }
        else
        {
            m_bMenuMode = FALSE;
        }
    }
}

BOOL CCustomMenuButton::OpenPopupMenu(CWnd* pWnd)
{
    return CMFCToolBarMenuButton::OpenPopupMenu(pWnd);
}

void CCustomMenuButton::OnDraw(CDC* pDC, 
                               const CRect& rect, 
                               CMFCToolBarImages* pImages, 
                               BOOL bHorz, 
                               BOOL bCustomizeMode,
                               BOOL bHighlight,
                               BOOL bDrawBorder,
                               BOOL bGrayDisabledButtons)
{

	if (m_bMenuMode)
	{
		DrawMenuItem(pDC, rect, pImages, bCustomizeMode, bHighlight, bGrayDisabledButtons);
		return;
	}

	BOOL bIsFlatLook = CMFCVisualManager::GetInstance()->IsMenuFlatLook();

	const int nSeparatorSize = 2;

	if (m_bMenuPaletteMode)
	{
		m_nStyle &= ~TBBS_CHECKED;
	}

	//----------------------
	// Fill button interior:
	//----------------------
	FillInterior(pDC, rect, bHighlight || IsDroppedDown());

	CSize sizeImage = CMenuImages::Size();
	if (CMFCToolBar::IsLargeIcons())
	{
		sizeImage.cx *= 2;
		sizeImage.cy *= 2;
	}

	CRect rectInternal = rect;
	CSize sizeExtra = m_bExtraSize ? CMFCVisualManager::GetInstance()->GetButtonExtraBorder() : CSize(0, 0);

	if (sizeExtra != CSize(0, 0))
	{
		rectInternal.DeflateRect(sizeExtra.cx / 2 + 1, sizeExtra.cy / 2 + 1);
	}

	CRect rectParent = rect;
	m_rectArrow = rectInternal;

	const int nMargin = CMFCVisualManager::GetInstance()->GetMenuImageMargin();
	const int nXMargin = bHorz ? nMargin : 0;
	const int nYMargin = bHorz ? 0 : nMargin;

	rectParent.DeflateRect(nXMargin, nYMargin);

	if (m_bDrawDownArrow)
	{
		if (bHorz)
		{
			rectParent.right -= sizeImage.cx + nSeparatorSize - 2 + sizeExtra.cx;
			m_rectArrow.left = rectParent.right + 1;

			if (sizeExtra != CSize(0, 0))
			{
				m_rectArrow.OffsetRect(-sizeExtra.cx / 2 + 1, -sizeExtra.cy / 2 + 1);
			}
		}
		else
		{
			rectParent.bottom -= sizeImage.cy + nSeparatorSize - 1;
			m_rectArrow.top = rectParent.bottom;
		}
	}

	UINT uiStyle = m_nStyle;

	if (bIsFlatLook)
	{
		m_nStyle &= ~(TBBS_PRESSED | TBBS_CHECKED);
	}
	else
	{
		if (m_bClickedOnMenu && m_nID != 0 && m_nID != (UINT) -1 && !m_bMenuOnly)
		{
			m_nStyle &= ~TBBS_PRESSED;
		}
		else if (m_pPopupMenu != NULL)
		{
			m_nStyle |= TBBS_PRESSED;
		}
	}

	BOOL bDisableFill = m_bDisableFill;
	m_bDisableFill = TRUE;

	CMFCToolBarButton::OnDraw(pDC, rectParent, pImages, bHorz, bCustomizeMode, bHighlight, bDrawBorder, bGrayDisabledButtons);

	m_bDisableFill = bDisableFill;

	if (m_bDrawDownArrow)
	{
		if ((m_nStyle &(TBBS_PRESSED | TBBS_CHECKED)) && !bIsFlatLook)
		{
			m_rectArrow.OffsetRect(1, 1);
		}

		if ((bHighlight ||(m_nStyle & TBBS_PRESSED) || m_pPopupMenu != NULL) && m_nID != 0 && m_nID != (UINT) -1 && !m_bMenuOnly)
		{
			//----------------
			// Draw separator:
			//----------------
			CRect rectSeparator = m_rectArrow;

			if (bHorz)
			{
				rectSeparator.right = rectSeparator.left + nSeparatorSize;
			}
			else
			{
				rectSeparator.bottom = rectSeparator.top + nSeparatorSize;
			}

			CMFCVisualManager::AFX_BUTTON_STATE state = CMFCVisualManager::ButtonsIsRegular;

			if (bHighlight ||(m_nStyle &(TBBS_PRESSED | TBBS_CHECKED)))
			{
				//-----------------------
				// Pressed in or checked:
				//-----------------------
				state = CMFCVisualManager::ButtonsIsPressed;
			}

			if (!m_bClickedOnMenu)
			{
				CMFCVisualManager::GetInstance()->OnDrawButtonSeparator(pDC, this, rectSeparator, state, bHorz);
			}
		}

		BOOL bDisabled = (bCustomizeMode && !IsEditable()) || (!bCustomizeMode &&(m_nStyle & TBBS_DISABLED));

		int iImage;
		if (bHorz && !m_bMenuOnly)
		{
			iImage = CMenuImages::IdArrowDown;
		}
		else
		{
			iImage = CMenuImages::IdArrowRight;
		}

		CMenuImages::Draw(pDC, (CMenuImages::IMAGES_IDS) iImage, m_rectArrow, bDisabled ? CMenuImages::ImageGray : CMenuImages::ImageBlack, sizeImage);
	}

	m_nStyle = uiStyle;

	if (!bCustomizeMode)
	{
		if ((m_nStyle &(TBBS_PRESSED | TBBS_CHECKED)) || m_pPopupMenu != NULL)
		{
			//-----------------------
			// Pressed in or checked:
			//-----------------------
			if (!bIsFlatLook && m_bClickedOnMenu && m_nID != 0 && m_nID != (UINT) -1 && !m_bMenuOnly) //JRG
			{
				rectParent.right++;

				CMFCVisualManager::GetInstance()->OnDrawButtonBorder(pDC, this, rectParent, CMFCVisualManager::ButtonsIsHighlighted);
				CMFCVisualManager::GetInstance()->OnDrawButtonBorder(pDC, this, m_rectArrow, CMFCVisualManager::ButtonsIsPressed);
			}
			else
			{
				CMFCVisualManager::GetInstance()->OnDrawButtonBorder(pDC, this, rect, CMFCVisualManager::ButtonsIsPressed);
			}
		}
		else if (bHighlight && !(m_nStyle & TBBS_DISABLED) && !(m_nStyle &(TBBS_CHECKED | TBBS_INDETERMINATE)))
		{
			CMFCVisualManager::GetInstance()->OnDrawButtonBorder(pDC, this, rect, CMFCVisualManager::ButtonsIsHighlighted);
		}
	}
}

BOOL CCustomMenuButton::OnClick(CWnd* pWnd, BOOL bDelay)
{
	ASSERT_VALID(pWnd);

	m_bClickedOnMenu = FALSE;

	if (m_bDrawDownArrow && !bDelay && !m_bMenuMode)
	{
		if (m_nID == 0 || m_nID == (UINT) -1)
		{
			m_bClickedOnMenu = TRUE;
		}
		else
		{
			CPoint ptMouse;
			::GetCursorPos(&ptMouse);
			pWnd->ScreenToClient(&ptMouse);

			m_bClickedOnMenu = m_rectArrow.PtInRect(ptMouse);
			if (!m_bClickedOnMenu)
			{
				return FALSE;
			}
		}
	}

	if (HasButton() && !bDelay)
	{
		CPoint ptMouse;
		::GetCursorPos(&ptMouse);
		pWnd->ScreenToClient(&ptMouse);

		if (m_rectButton.PtInRect(ptMouse))
		{
			return FALSE;
		}
	}

	if (!m_bClickedOnMenu && m_nID > 0 && m_nID != (UINT) -1 && !m_bDrawDownArrow && !m_bMenuOnly)
	{
		return FALSE;
	}

	CMFCMenuBar* pMenuBar = DYNAMIC_DOWNCAST(CMFCMenuBar, m_pWndParent);

	if (m_pPopupMenu != NULL)
	{
		//-----------------------------------------------------
		// Second click to the popup menu item closes the menu:
		//-----------------------------------------------------
		ASSERT_VALID(m_pPopupMenu);

		m_pPopupMenu->m_bAutoDestroyParent = FALSE;
		m_pPopupMenu->DestroyWindow();
		m_pPopupMenu = NULL;

		if (pMenuBar != NULL)
		{
			pMenuBar->SetHot(NULL);
		}
	}
	else
	{
		CMFCPopupMenuBar* pParentMenu = DYNAMIC_DOWNCAST(CMFCPopupMenuBar, m_pWndParent);

		if (bDelay && pParentMenu != NULL && !CMFCToolBar::IsCustomizeMode())
		{
			pParentMenu->StartPopupMenuTimer(this);
		}
		else
		{
			if (pMenuBar != NULL)
			{
				CMFCToolBarMenuButton* pCurrPopupMenuButton = pMenuBar->GetDroppedDownMenu();
				if (pCurrPopupMenuButton != NULL)
				{
					pCurrPopupMenuButton->OnCancelMode();
				}
			}

			if (!OpenPopupMenu(pWnd))
			{
				return FALSE;
			}
		}

		if (pMenuBar != NULL)
		{
			pMenuBar->SetHot(this);
		}
	}

	if (m_pWndParent != NULL)
	{
		CRect rect = m_rect;

		const int nShadowSize = CMFCVisualManager::GetInstance()->GetMenuShadowDepth();

		rect.InflateRect(nShadowSize, nShadowSize);
		m_pWndParent->RedrawWindow(rect, NULL, RDW_FRAME | RDW_INVALIDATE);
	}

	return TRUE;
}

void CCustomMenuButton::CreateFromMenu(HMENU hMenu)
{

    CUtil::ConvertMenuString(hMenu);
    CMFCToolBarMenuButton::CreateFromMenu(hMenu);
}


HMENU CCustomMenuButton::CreateMenu() const
{
    return CMFCToolBarMenuButton::CreateMenu();
}


