
#pragma once
class CCustomMenuButton  : public CMFCToolBarMenuButton
{
public:
	CCustomMenuButton();
	CCustomMenuButton(UINT uiID, HMENU hMenu, int iImage, LPCTSTR lpszText = NULL, BOOL bUserButton = FALSE);

    void Initialize(UINT uiID, HMENU hMenu, int iImage, LPCTSTR lpszText, BOOL bUserButton);

    virtual void Serialize(CArchive& ar);


public:
	virtual BOOL OpenPopupMenu(CWnd* pWnd = NULL);
	//virtual void CopyFrom(const CMFCToolBarButton& src);
	//virtual void Serialize(CArchive& ar);

	virtual void OnDraw(CDC* pDC, const CRect& rect, CMFCToolBarImages* pImages, BOOL bHorz = TRUE, BOOL bCustomizeMode = FALSE,
		BOOL bHighlight = FALSE, BOOL bDrawBorder = TRUE, BOOL bGrayDisabledButtons = TRUE);

	//virtual SIZE OnCalculateSize(CDC* pDC, const CSize& sizeDefault, BOOL bHorz);
	virtual BOOL OnClick(CWnd* pWnd, BOOL bDelay = TRUE);

	virtual void OnChangeParentWnd(CWnd* pWndParent);
	virtual void CreateFromMenu(HMENU hMenu);
	virtual HMENU CreateMenu() const;
	//virtual BOOL HaveHotBorder() const { return FALSE; }
	//virtual void OnCancelMode();
	//virtual BOOL OnContextHelp(CWnd* pWnd) { return OnClick(pWnd, FALSE); }

	//virtual int OnDrawOnCustomizeList(CDC* pDC, const CRect& rect, BOOL bSelected);
	//virtual BOOL IsDroppedDown() const { return m_pPopupMenu != NULL; }

	//virtual CMFCPopupMenu* CreatePopupMenu() { return new CMFCPopupMenu; }
	//virtual void OnAfterCreatePopupMenu() {}

	//virtual BOOL IsEmptyMenuAllowed() const { return FALSE; }

	//virtual BOOL OnBeforeDrag() const;
	//virtual void SaveBarState();

	//void GetImageRect(CRect& rectImage);
	
	//virtual void SetRadio();
	//virtual void ResetImageToDefault();
	//virtual BOOL CompareWith(const CMFCToolBarButton& other) const;

	//virtual BOOL IsBorder() const { return TRUE; }

	//virtual BOOL OnClickMenuItem() { return FALSE; } // Return TRUE for the custom process
	//virtual BOOL IsExclusive() const { return FALSE; }
	//virtual BOOL HasButton() const { return FALSE; }

	//virtual BOOL SetACCData(CWnd* pParent, CAccessibilityData& data);



};
