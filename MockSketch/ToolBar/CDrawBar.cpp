/**
 * @brief			CDrawBar実装ファイル
 * @file			CDrawBar.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "CDrawBar.h"
#include "CLineStyleMenuItem.h"
#include "CLineWidthMenuItem.h"
#include "MockSketch.h"
#include "MainFrm.h"
#include "System/CSystem.h"
#include "DefinitionObject/View/MockSketchView.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

IMPLEMENT_SERIAL(CDrawBar, CCustomToolBar, 1)

BEGIN_MESSAGE_MAP(CDrawBar, CCustomToolBar)
	//{{AFX_MSG_MAP(CDrawBar)
	//}}AFX_MSG_MAP
	// Global help commands
END_MESSAGE_MAP()


/*---------------------------------------------------*/
/* Static variables                                  */
/*---------------------------------------------------*/


/**
 * @brief   コンストラクタ
 * @param   なし
 * @return	なし
 * @note	 
 */
 CDrawBar::CDrawBar():CCustomToolBar()
{
    SetName(_T("CDrawBar"));
}

/**
 * @brief   ツール バーを元の状態に戻します
 * @param   なし
 * @return	なし
 * @note	 既定の実装では、何も行われません。
 * @note	 ツール バーが元の状態に戻るときに置き換える必要がある
 * @note	 ダミー ボタンがツール バーにある場合は、
 * @note	 CMFCToolBar クラスから派生したクラスで 
 * @note	 OnReset をオーバーライドします。
 */
 void CDrawBar::OnReset ()
{
	// 色指定ボタンに置き換え
	CCustomMenuButton* pDimArrreyButton   = CreateDimArrayButton ();
	ReplaceButton (ID_MNU_L_DIM, *pDimArrreyButton);
	delete pDimArrreyButton;


}

/**
 * @brief   ツール バー更新要求
 * @param   [in]    アプリケーションのメイン フレーム ウィンドウへのポインタ。
 *                   このポインタを更新メッセージのルーティングに使います
 * @param   [in]    新ハンドラを持たないコントロールを自動的に使用禁止状態で
 *                   表示するかどうかを示すフラグです。
 * @return	なし
 * @note	 各ボタンまたはペインを更新するには、適切な更新ハンドラを設定するためにメッセージ マップで 
 * @note	 ON_UPDATE_COMMAND_UI マクロを使います。
 * @note	 OnUpdateCmdUI は、アプリケーションが入力待ちのときに、フレームワークから呼び出されます。
 * @note	 ウィンドウは、少なくとも間接的に、表示されているフレーム ウィンドウの子ウィンドウであることが必要です
 * @note	 OnUpdateCmdUI は高度なオーバーライド可能な関数です。
 */
void CDrawBar::OnUpdateCmdUI(CFrameWnd* pTarget, BOOL bDisableIfNoHndler)
{
	USES_CONVERSION;

	CMFCToolBar::OnUpdateCmdUI(pTarget, bDisableIfNoHndler);

}


/**
 * @brief   色選択ボタン生成
 * @param   [in] iID メニューID
 * @return	色選択ボタンへのポインタ
 * @note	 
 */
CMFCColorMenuButton* CDrawBar::CreateColorButton (int iID)
{
	if (m_palColorPicker.GetSafeHandle () == NULL)
	{
        LOGPALETTE* pLogPalette = SYS_CONFIG->GetColorPalette();
		m_palColorPicker.CreatePalette (pLogPalette);
	}


    CMFCColorMenuButton* pColorButton;
    if (iID == ID_MNU_LINE_COLOR )
    {
	    pColorButton = new 
		    CMFCColorMenuButton (iID, 
            GET_SYS_STR(STR_MNU_SET_LINE_COLORS_ITEM).c_str(), &m_palColorPicker);

	    pColorButton->EnableAutomaticButton (GET_SYS_STR(STR_MNU_SET_AUTOMATIC).c_str(), DRAW_CONFIG->crFront);
	    pColorButton->EnableOtherButton     (GET_SYS_STR(STR_MNU_SET_MORE_COLORS).c_str());
	    pColorButton->EnableDocumentColors  (GET_SYS_STR(STR_MNU_SET_LINE_COLORS).c_str());
	    pColorButton->SetColumnsNumber (8);
	    pColorButton->EnableTearOff (ID_COLOR_TEAROFF1, 5, 2);
    }
    else
    {
	    pColorButton = new 
		    CMFCColorMenuButton (iID, 
            GET_SYS_STR(STR_MNU_SET_FILL_COLORS_ITEM).c_str(), &m_palColorPicker);

	    pColorButton->EnableAutomaticButton (GET_SYS_STR(STR_MNU_SET_AUTOMATIC).c_str(), DRAW_CONFIG->crFront);
	    pColorButton->EnableOtherButton     (GET_SYS_STR(STR_MNU_SET_MORE_COLORS).c_str());
	    pColorButton->EnableDocumentColors  (GET_SYS_STR(STR_MNU_SET_FILL_COLORS).c_str());
	    pColorButton->SetColumnsNumber (8);
	    pColorButton->EnableTearOff (ID_COLOR_TEAROFF2, 5, 2);
    }
    //pColorButton->m_bDefaultClick = true;

    /*
	// Initialize color names:
    int nNumColours  = SYS_CONFIG->GetMaxColors();
	for (int i = 0; i < nNumColours; i++)
	{
		CMFCColorMenuButton::SetColorName (SYS_CONFIG->GetColorRgb(i), SYS_CONFIG->GetColorName(i).c_str());
	}
    */
	return pColorButton;
}



/**
 * @brief   線種選択ボタン生成
 * @param   [in] iID メニューID
 * @return	線種選択ボタンへのポインタ
 * @note	 
 */
/*
CCustomMenuButton * CDrawBar::CreateLineTypeButton ()
{
	CMenu menu;
	VERIFY(menu.LoadMenu (IDR_LINE_TYPE_POPUP));
	CCustomMenuButton* pLineType = NULL;

    int iImgIndex = GetCmdMgr ()->GetCmdImage (ID_MNU_LINE_STYLE, FALSE);
    //iImgIndex = -1;
	pLineType = new CCustomMenuButton (
                        (UINT)-1,            //ディフォルトのコマンドID
                        menu.GetSafeHmenu (),  //メニューのハンドル
                        iImgIndex,  //ボタン イメージのインデックス
                        _T("LineTypes"));
	return pLineType;
}
*/


/**
 * @brief   線幅選択ボタン生成
 * @param   なし
 * @return	線幅選択ボタンへのポインタ
 * @note	 
 */
CCustomMenuButton * CDrawBar::CreateWidthButton ()
{
	CMenu menu;
	VERIFY(menu.LoadMenu (IDR_LINE_WIDTH_POPUP));

    CCustomMenuButton* pLineWidth = NULL;

    int iImgIndex = GetCmdMgr ()->GetCmdImage (ID_MNU_LINE_WIDTH, FALSE);

    pLineWidth = new CCustomMenuButton (
        (UINT)-1, // No default command
	    menu.GetSafeHmenu (),
	    iImgIndex,
	    _T("&Line Width"));

    return pLineWidth;
}


/**
 * @brief   選択ボタン生成
 * @param   [in] iID メニューID
 * @return	線種選択ボタンへのポインタ
 * @note	 
 */
CCustomMenuButton * CDrawBar::CreateDimArrayButton ()
{
   CMenu menu;
	VERIFY(menu.LoadMenu (IDR_DIM_PALETTE));


    VIEW_MODE eViewMode = VIEW_L_DIM;

    CMainFrame* pMainFrame;
    pMainFrame = THIS_APP->GetMainFrame();

    if (pMainFrame)
    {
        CView* pView;
        pView = pMainFrame->GetActiveView();
        if (pView)
        {
            CMockSketchView* pMockView;
            pMockView = dynamic_cast<CMockSketchView*>(pView);
            if (pMockView)
            {
                eViewMode = pMockView->GetViewMode();
            }
        }
    }


    int iIdMenu = ID_MNU_L_DIM;
    StdString strId  = STR_MNU_L_DIM;

    switch(eViewMode)
    {
    case VIEW_H_DIM:  {iIdMenu = ID_MNU_H_DIM; strId = STR_MNU_H_DIM; break;}
    case VIEW_V_DIM:  {iIdMenu = ID_MNU_V_DIM; strId = STR_MNU_V_DIM; break;}
    case VIEW_A_DIM:  {iIdMenu = ID_MNU_A_DIM; strId = STR_MNU_A_DIM; break;}
    case VIEW_D_DIM:  {iIdMenu = ID_MNU_D_DIM; strId = STR_MNU_D_DIM; break;}
    case VIEW_R_DIM:  {iIdMenu = ID_MNU_R_DIM; strId = STR_MNU_R_DIM; break;}
    case VIEW_C_DIM:  {iIdMenu = ID_MNU_C_DIM; strId = STR_MNU_C_DIM; break;}
    default: break;
    }

    ;

	CCustomMenuButton* pDimType = NULL;
	CMenu* pPopup = menu.GetSubMenu(0);
	ASSERT(pPopup != NULL);
	if (pPopup != NULL)
	{
        int iImgIndex = GetCmdMgr ()->GetCmdImage (ID_MNU_L_DIM/*iIdMenu*/, FALSE);

		pDimType = new CCustomMenuButton 
                        (
                         ID_MNU_L_DIM /*iIdMenu*/,                     // ユーザーがボタンをクリックしたときに実行するコマンドの ID
                         pPopup->GetSafeHmenu (),     // メニューのハンドル。メニューがないボタンの場合は NULL。
                         iImgIndex,                   //ボタン イメージのインデックス。このボタンにアイコンがないか、uiID で指定されたコマンドのアイコンを使用する場合は -1。アプリケーション内のどの CMFCToolBarImages オブジェクトでも、インデックスは同じです。  
                         GET_STR(strId),              //ツール バー メニュー ボタンのテキス
                         FALSE);                      //ボタンにユーザー定義のイメージを表示する場合は TRUE。uiID で指定されたコマンドに関連付けられている定義済みのイメージをボタンに表示する場合は FALSE。

		pDimType->SetMenuPaletteMode (TRUE, 1 );// Rows number 
        pDimType->SetTearOff (ID_DIM_TYPE_TEAROFF);

	}
    return pDimType;

}

