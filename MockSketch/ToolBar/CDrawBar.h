/**
 * @brief			CDrawBarヘッダー
 * @file			CDrawBar.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef __DRAWBAR_H__
#define __DRAWBAR_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CustomToolBar.h"
#include "CCustomMenuButton.h"

/**
 * @class   CDrawBar
 * @brief   作図ツールバー                     
 */
class CDrawBar : public CCustomToolBar
{
	DECLARE_SERIAL(CDrawBar)

// Construction
public:
	CDrawBar();

// Operations
public:
	virtual void CDrawBar::OnUpdateCmdUI(CFrameWnd* pTarget, BOOL bDisableIfNoHndler);

	CMFCColorMenuButton* CreateColorButton (int iID);
    //CCustomMenuButton * CreateLineTypeButton ();
    CCustomMenuButton * CreateWidthButton ();
    CCustomMenuButton * CreateLineTypeArrayButton ();
    CCustomMenuButton * CreateDimArrayButton();

// Attributes
public:

protected:
	CPalette	m_palColorPicker;	// Palette for color picker

// Implementation
protected:
	virtual void OnReset ();

	// Generated message map functions
	//{{AFX_MSG(CDrawBar)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif

/////////////////////////////////////////////////////////////////////////////
