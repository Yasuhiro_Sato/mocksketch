/**
 * @brief			CDrawBar実装ファイル
 * @file			CDrawBar.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "CFindBar.h"
#include "MockSketch.h"
#include "MainFrm.h"
#include "System/CSystem.h"
#include "DefinitionObject/View/MockSketchView.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

IMPLEMENT_SERIAL(CFindBar, CCustomToolBar, 1)

BEGIN_MESSAGE_MAP(CFindBar, CCustomToolBar)
	//{{AFX_MSG_MAP(CDrawBar)
	//}}AFX_MSG_MAP
	// Global help commands
END_MESSAGE_MAP()


/*---------------------------------------------------*/
/* Static variables                                  */
/*---------------------------------------------------*/


/**
 * @brief   コンストラクタ
 * @param   なし
 * @return	なし
 * @note	 
 */
 CFindBar::CFindBar():CCustomToolBar()
{
    SetName(_T("CFindBar"));
}

/**
 * @brief   ツール バーを元の状態に戻します
 * @param   なし
 * @return	なし
 * @note	 既定の実装では、何も行われません。
 * @note	 ツール バーが元の状態に戻るときに置き換える必要がある
 * @note	 ダミー ボタンがツール バーにある場合は、
 * @note	 CMFCToolBar クラスから派生したクラスで 
 * @note	 OnReset をオーバーライドします。
 */
 void CFindBar::OnReset ()
{
    CMFCToolBarComboBoxButton2  combo 
        (ID_MNU_FIND,                                        // コマンド ID。
        GetCmdMgr ()->GetCmdImage  (ID_MNU_FIND, FALSE),     // 関連付けられたイメージのインデックス。
        WS_VISIBLE | WS_TABSTOP | WS_VSCROLL | CBS_DROPDOWNLIST,    // スタイル。
        150 );                              // 幅 (ピクセル単位)。

    combo.SetToolTipText(_T("表示するページを選択します"));
    ReplaceButton (ID_MNU_FIND, combo);

    auto pComboButton = GetComboButton(ID_MNU_FIND);

    if (!pComboButton)
    {
        return;
    }

    auto pCombo =pComboButton->GetComboBox();

    if (!pCombo)
    {
        return;
    }

}

/**
 * @brief   ツール バー更新要求
 * @param   [in]    アプリケーションのメイン フレーム ウィンドウへのポインタ。
 *                   このポインタを更新メッセージのルーティングに使います
 * @param   [in]    新ハンドラを持たないコントロールを自動的に使用禁止状態で
 *                   表示するかどうかを示すフラグです。
 * @return	なし
 * @note	 各ボタンまたはペインを更新するには、適切な更新ハンドラを設定するためにメッセージ マップで 
 * @note	 ON_UPDATE_COMMAND_UI マクロを使います。
 * @note	 OnUpdateCmdUI は、アプリケーションが入力待ちのときに、フレームワークから呼び出されます。
 * @note	 ウィンドウは、少なくとも間接的に、表示されているフレーム ウィンドウの子ウィンドウであることが必要です
 * @note	 OnUpdateCmdUI は高度なオーバーライド可能な関数です。
 */
void CFindBar::OnUpdateCmdUI(CFrameWnd* pTarget, BOOL bDisableIfNoHndler)
{
	USES_CONVERSION;

	CMFCToolBar::OnUpdateCmdUI(pTarget, bDisableIfNoHndler);

}

LRESULT CFindBar::OnToolbarReset(WPARAM wp,LPARAM)
{
    UINT uiToolBarId = (UINT) wp;
    if (uiToolBarId == IDR_FIND)
    {


    }

    return 0;
}



LRESULT CFindBar::OnToolbarCreateNew(WPARAM wp,LPARAM)
{
  return 0;
}


void CFindBar::OnChangeComboBox(void)
{
    auto pCombo = GetComboButton(ID_MNU_FIND);
    if(pCombo == NULL)
    {
        return;
    }
    int iIndex = pCombo->GetCurSel();
    DWORD_PTR iId = pCombo->GetItemData(iIndex);
    GetOwner()->SendMessage(WM_COMMAND, iId);
}
