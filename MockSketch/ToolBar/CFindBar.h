/**
 * @brief			CDrawBarヘッダー
 * @file			CDrawBar.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef __FINDBAR_H__
#define __FINDBAR_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CustomToolBar.h"
#include "CCustomMenuButton.h"

/**
 * @class   CFindBar
 * @brief   検索ツールバー                     
 */
class CFindBar : public CCustomToolBar
{
	DECLARE_SERIAL(CFindBar)

// Construction
public:
	CFindBar();

// Operations
public:
	virtual void OnUpdateCmdUI(CFrameWnd* pTarget, BOOL bDisableIfNoHndler);


// Attributes
public:

protected:

// Implementation
protected:
	virtual void OnReset ();
	afx_msg LRESULT OnToolbarReset(WPARAM,LPARAM);
	afx_msg LRESULT OnToolbarCreateNew(WPARAM,LPARAM);
    afx_msg void OnChangeComboBox(void);

	DECLARE_MESSAGE_MAP()
};

#endif

/////////////////////////////////////////////////////////////////////////////
