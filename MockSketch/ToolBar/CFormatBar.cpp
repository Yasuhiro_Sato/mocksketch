/**
 * @brief			CFormatBar実装ファイル
 * @file			CFormatBar.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CFormatBar.h"
#include "./CToolBarFontSizeCombo.h"
#include "MockSketch.h"
#include "SYSTEM/CSystem.h"
#include "DefinitionObject/View/MockSketchView.h"
#include "CCustomMenuButton.h"
/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


IMPLEMENT_SERIAL(CFormatBar, CMFCToolBar, 1)

BEGIN_MESSAGE_MAP(CFormatBar, CMFCToolBar)
	//{{AFX_MSG_MAP(CFormatBar)
	//}}AFX_MSG_MAP
	// Global help commands
END_MESSAGE_MAP()


static CSize GetBaseUnits(CFont* pFont)
{
	ASSERT(pFont != NULL);
	if (pFont != NULL)
	{
		ASSERT(pFont->GetSafeHandle() != NULL);

		pFont = theApp.m_dcScreen.SelectObject(pFont);
		TEXTMETRIC tm;
		VERIFY(theApp.m_dcScreen.GetTextMetrics(&tm));

		theApp.m_dcScreen.SelectObject(pFont);
		return CSize(tm.tmAveCharWidth, tm.tmHeight);
	}
	return CSize(0,0);
}

/*---------------------------------------------------*/
/* Static variables                                  */
/*---------------------------------------------------*/

/**
 * @brief   コンストラクタ
 * @param   なし
 * @return	なし
 * @note	 
 */
 CFormatBar::CFormatBar()
{
    //コンボボックスのサイズを決める    
	CFont fnt;
	fnt.Attach(GetStockObject(theApp.m_nDefFont));

    m_szBaseUnits = GetBaseUnits(&fnt);
	CMFCToolBarFontComboBox::m_nFontHeight = m_szBaseUnits.cy;
}

/**
 * @brief   ツール バーを元の状態に戻します
 * @param   なし
 * @return	なし
 * @note	 既定の実装では、何も行われません。
 * @note	 ツール バーが元の状態に戻るときに置き換える必要がある
 * @note	 ダミー ボタンがツール バーにある場合は、
 * @note	 CMFCToolBar クラスから派生したクラスで 
 * @note	 OnReset をオーバーライドします。
 */
 void CFormatBar::OnReset ()
{
    //------------------------------------
	//  フォント名コンボボックス挿入
    //------------------------------------
	CMFCToolBarFontComboBox* pFontButton = CreateFontComboButton ();
	ASSERT_VALID (pFontButton);

	int iIndex = CommandToIndex (IDC_FONTNAME);
	RemoveButton (iIndex);

	if (iIndex > GetCount ())
	{
		iIndex = GetCount ();
	}

    //ディフォルトフォント設定
	InsertButton (*pFontButton, iIndex);
	delete pFontButton;

    //------------------------------------
	//  フォントサイズコンボボックス挿入
    //------------------------------------
	CToolBarFontSizeCombo comboButtonFontSize (IDC_FONTSIZE, 
		GetCmdMgr ()->GetCmdImage  (IDC_FONTSIZE, FALSE),
		WS_VISIBLE | WS_TABSTOP | WS_VSCROLL | CBS_DROPDOWN,
		10*m_szBaseUnits.cx + 10);

	iIndex = CommandToIndex (IDC_FONTSIZE);
	RemoveButton (iIndex);

	if (iIndex > GetCount ())
	{
		iIndex = GetCount ();
	}

	InsertButton (comboButtonFontSize, iIndex);

    //------------------------------------

    //------------------------------------
    //  文字色選択ボタン
    //------------------------------------
	CMFCColorMenuButton* pColorButton = CreateColorButton ();
	ReplaceButton (ID_CHAR_COLOR, *pColorButton);
	delete pColorButton;

    // 文字位置
    CCustomMenuButton* pDatumButton = CreateDatumButton ();
    ReplaceButton (ID_CHAR_DATUM_TL, *pDatumButton);
    delete pDatumButton;


    // 文字位置
	CCustomMenuButton* pAlignButton   = CreateAlignButton ();
	ReplaceButton (ID_CHAR_ALIGN_L, *pAlignButton);
	delete pAlignButton;
    //------------------------------------


}

/**
 * @brief   ツール バー更新要求
 * @param   [in]    アプリケーションのメイン フレーム ウィンドウへのポインタ。
 *                   このポインタを更新メッセージのルーティングに使います
 * @param   [in]    新ハンドラを持たないコントロールを自動的に使用禁止状態で
 *                   表示するかどうかを示すフラグです。
 * @return	なし
 * @note	 各ボタンまたはペインを更新するには、適切な更新ハンドラを設定するためにメッセージ マップで 
 * @note	 ON_UPDATE_COMMAND_UI マクロを使います。
 * @note	 OnUpdateCmdUI は、アプリケーションが入力待ちのときに、フレームワークから呼び出されます。
 * @note	 ウィンドウは、少なくとも間接的に、表示されているフレーム ウィンドウの子ウィンドウであることが必要です
 * @note	 OnUpdateCmdUI は高度なオーバーライド可能な関数です。
 */
void CFormatBar::OnUpdateCmdUI(CFrameWnd* pTarget, BOOL bDisableIfNoHndler)
{
	USES_CONVERSION;

	CMFCToolBar::OnUpdateCmdUI(pTarget, bDisableIfNoHndler);

    CMDIFrameWndEx *pFrame = (CMDIFrameWndEx*)AfxGetApp()->m_pMainWnd;

    if (pFrame == NULL)
    {
        return;
    }

    //TAG:[メインウインドウがない場合エラーになる]
    //    [LOAD キャンセル時に発生]
    CMDIChildWnd *pChild = pFrame->MDIGetActive();
    if (pChild == NULL)
    {
        return;
    }

    CMockSketchView *pView = (CMockSketchView*)pChild->GetActiveView();

    if (pView == 0)
    {
        return;
    }

    CRichEditCtrl* pRich = pView->GetRichEdit();

    if (pRich->m_hWnd != ::GetFocus ())
    {
		return;
    }
    
    if (pRich->m_hWnd == 0)
    {
        return;
    }


    //------------------
    //   フォント名
    //------------------
	CString strFontName;

	// get the current font from the view and update
	CHARFORMAT cf;
    memset(&cf, 0, sizeof(cf));
    cf.cbSize = sizeof(cf);
    cf.dwMask = CFM_FACE|CFM_CHARSET|CFM_SIZE;
    pRich->GetSelectionCharFormat (cf);
    
    
	CObList listButtons;
	if (CMFCToolBar::GetCommandButtons (IDC_FONTNAME, listButtons) > 0)
	{
		for (POSITION posCombo = listButtons.GetHeadPosition (); posCombo != NULL;)
		{
			CMFCToolBarFontComboBox* pCombo = 
				DYNAMIC_DOWNCAST (CMFCToolBarFontComboBox, listButtons.GetNext (posCombo));

			if (pCombo != NULL && !pCombo->HasFocus ())
			{
				if ((cf.dwMask & (CFM_FACE|CFM_CHARSET)) == (CFM_FACE|CFM_CHARSET))
				{
					strFontName = cf.szFaceName;
					pCombo->SetFont (strFontName, DEFAULT_CHARSET, TRUE);
				}
				else
				{
					pCombo->SetText(_T(""));
				}
			}
		}
	}

	if (CMFCToolBar::GetCommandButtons (IDC_FONTSIZE, listButtons) > 0)
	{
		for (POSITION posCombo = listButtons.GetHeadPosition (); posCombo != NULL;)
		{
			CToolBarFontSizeCombo* pCombo = 
				DYNAMIC_DOWNCAST (CToolBarFontSizeCombo, listButtons.GetNext (posCombo));

			if (pCombo != NULL && !pCombo->HasFocus ())
			{
				if (!strFontName.IsEmpty () && pCombo->GetCount () == 0)
				{
					pCombo->RebuildFontSizes (strFontName);
				}

				pCombo->SetTwipSize( (cf.dwMask & CFM_SIZE) ? cf.yHeight : -1);
			}
		}
	}
	pRich->SetFocus ();
}

/**
 * @brief   フォントコンボボタン生成
 * @param   なし
 * @return	フォントコンボボタンへのポインタ
 * @note	 
 */
CMFCToolBarFontComboBox* CFormatBar::CreateFontComboButton ()
{
	return new CMFCToolBarFontComboBox (IDC_FONTNAME, 
		GetCmdMgr ()->GetCmdImage  (IDC_FONTNAME, FALSE),
		TRUETYPE_FONTTYPE | DEVICE_FONTTYPE,
		DEFAULT_CHARSET,
		WS_VISIBLE | WS_TABSTOP | WS_VSCROLL | CBS_DROPDOWN |
		CBS_AUTOHSCROLL | CBS_HASSTRINGS | CBS_OWNERDRAWFIXED,
		(3*LF_FACESIZE*m_szBaseUnits.cx)/2);
}


/**
 * @brief   色選択ボタン生成
 * @param   なし
 * @return	色選択ボタンへのポインタ
 * @note	 
 */
CMFCColorMenuButton* CFormatBar::CreateColorButton ()
{
	if (m_palColorPicker.GetSafeHandle () == NULL)
	{
        LOGPALETTE* pLogPalette = SYS_CONFIG->GetColorPalette();
		m_palColorPicker.CreatePalette (pLogPalette);
	}

    //メニュー表示時のテキスト
	CMFCColorMenuButton* pColorButton = new 
		CMFCColorMenuButton (ID_CHAR_COLOR, 
        GET_SYS_STR(STR_MNU_SET_TEXT_COLORS_ITEM).c_str(), &m_palColorPicker);


	pColorButton->EnableAutomaticButton (GET_SYS_STR(STR_MNU_SET_AUTOMATIC).c_str(), DRAW_CONFIG->GetDefautTextColor());
	pColorButton->EnableOtherButton     (GET_SYS_STR(STR_MNU_SET_MORE_COLORS).c_str());
	pColorButton->EnableDocumentColors  (GET_SYS_STR(STR_MNU_SET_TEXT_COLORS).c_str());
	pColorButton->SetColumnsNumber (8);
	pColorButton->EnableTearOff (ID_COLOR_TEAROFF, 5, 2);

	// Initialize color names:
    int nNumColours  = SYS_CONFIG->GetMaxColors();
	for (int i = 0; i < nNumColours; i++)
	{
		CMFCColorMenuButton::SetColorName (SYS_CONFIG->GetColorRgb(i), SYS_CONFIG->GetColorName(i).c_str());
	}
	return pColorButton;
}

/**
 * @brief   文字位置ボタン生成
 * @param   なし
 * @return	文字位置ボタンへのポインタ
 * @note	 
 */
CCustomMenuButton* CFormatBar::CreateDatumButton()
{
    CMenu menu;
	VERIFY(menu.LoadMenu (IDR_CHAR_DATUM_PALETTE));

	CCustomMenuButton* pAlignType = NULL;
	CMenu* pPopup = menu.GetSubMenu(0);
	ASSERT(pPopup != NULL);
	if (pPopup != NULL)
	{
        int iImgIndex = GetCmdMgr ()->GetCmdImage (ID_CHAR_DATUM_TL, FALSE);
        //iImgIndex = -1;



		pAlignType = new CCustomMenuButton (ID_CHAR_DATUM_TL, pPopup->GetSafeHmenu (),
                         iImgIndex, 
                         _T("Text Datum Type"),
                         FALSE);

        pAlignType->SetMenuPaletteMode (TRUE, 3 );// Rows number 
		//pAlignType->SetTearOff (ID_CHAR_DATUM_TEAROFF);
	}

    return pAlignType;
}

CCustomMenuButton* CFormatBar::CreateAlignButton()
{
    CMenu menu;
    VERIFY(menu.LoadMenu (IDR_CHAR_ALIGN_PALETTE));

    CCustomMenuButton* pAlignType = NULL;
    CMenu* pPopup = menu.GetSubMenu(0);
    ASSERT(pPopup != NULL);
    if (pPopup != NULL)
    {
        int iImgIndex = GetCmdMgr ()->GetCmdImage (ID_CHAR_ALIGN_L, FALSE);
        //iImgIndex = -1;



        pAlignType = new CCustomMenuButton (ID_CHAR_ALIGN_L, pPopup->GetSafeHmenu (),
            iImgIndex, 
            _T("Text Align Type"),
            FALSE);

        //pAlignType->SetMenuPaletteMode (TRUE, 3 );// Rows number 
                                                  //pAlignType->SetTearOff (ID_CHAR_DATUM_TEAROFF);
    }

    return pAlignType;
}

