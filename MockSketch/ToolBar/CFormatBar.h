/**
 * @brief			CFormatBarヘッダー
 * @file			CFormatBar.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */


#ifndef __FORMATBAR_H__
#define __FORMATBAR_H__
#include "CCustomMenuButton.h"

/**
 * @class   CFormatBar
 * @brief   文字フォーマット設定ツールバー                     
 */
class CFormatBar : public CMFCToolBar
{
	DECLARE_SERIAL(CFormatBar)

// Construction
public:
	CFormatBar();

// Operations
public:
	virtual void CFormatBar::OnUpdateCmdUI(CFrameWnd* pTarget, BOOL bDisableIfNoHndler);

	CMFCColorMenuButton* CreateColorButton ();
	CMFCToolBarFontComboBox* CreateFontComboButton ();
    CCustomMenuButton* CreateAlignButton();
    CCustomMenuButton* CreateDatumButton();

// Attributes
public:
	CSize m_szBaseUnits;

protected:
	CPalette	m_palColorPicker;	// Palette for color picker

// Implementation
protected:
	virtual void OnReset ();

	// Generated message map functions
	//{{AFX_MSG(CFormatBar)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif

/////////////////////////////////////////////////////////////////////////////
