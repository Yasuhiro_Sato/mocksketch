/**
 * @brief			CDrawBar実装ファイル
 * @file			CDrawBar.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "CLineBar.h"
#include "CLineStyleMenuItem.h"
#include "CLineWidthMenuItem.h"
#include "MockSketch.h"
#include "MainFrm.h"
#include "System/CSystem.h"
#include "DefinitionObject/View/MockSketchView.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

IMPLEMENT_SERIAL(CLineBar, CCustomToolBar, 1)

BEGIN_MESSAGE_MAP(CLineBar, CCustomToolBar)
	//{{AFX_MSG_MAP(CDrawBar)
	//}}AFX_MSG_MAP
	// Global help commands
END_MESSAGE_MAP()


/*---------------------------------------------------*/
/* Static variables                                  */
/*---------------------------------------------------*/


/**
 * @brief   コンストラクタ
 * @param   なし
 * @return	なし
 * @note	 
 */
 CLineBar::CLineBar():CCustomToolBar()
{
    SetName(_T("CLineBar"));
}

/**
 * @brief   ツール バーを元の状態に戻します
 * @param   なし
 * @return	なし
 * @note	 既定の実装では、何も行われません。
 * @note	 ツール バーが元の状態に戻るときに置き換える必要がある
 * @note	 ダミー ボタンがツール バーにある場合は、
 * @note	 CMFCToolBar クラスから派生したクラスで 
 * @note	 OnReset をオーバーライドします。
 */
 void CLineBar::OnReset ()
{
	// 色指定ボタンに置き換え
	CMFCColorMenuButton* pPenColorButton = CreateColorButton (ID_MNU_LINE_COLOR);
	ReplaceButton (ID_MNU_LINE_COLOR, *pPenColorButton);
	delete pPenColorButton;

	CMFCColorMenuButton* pBrushColorButton = CreateColorButton (ID_MNU_BRUSH_COLOR);
	ReplaceButton (ID_MNU_BRUSH_COLOR, *pBrushColorButton);
	delete pBrushColorButton;
    
    ReplaceButton (ID_MNU_LINE_STYLE, CToolBarLineComboBox());
	
	ReplaceButton (ID_MNU_LINE_WIDTH, CToolBarLineWidthComboBox());
}

/**
 * @brief   ツール バー更新要求
 * @param   [in]    アプリケーションのメイン フレーム ウィンドウへのポインタ。
 *                   このポインタを更新メッセージのルーティングに使います
 * @param   [in]    新ハンドラを持たないコントロールを自動的に使用禁止状態で
 *                   表示するかどうかを示すフラグです。
 * @return	なし
 * @note	 各ボタンまたはペインを更新するには、適切な更新ハンドラを設定するためにメッセージ マップで 
 * @note	 ON_UPDATE_COMMAND_UI マクロを使います。
 * @note	 OnUpdateCmdUI は、アプリケーションが入力待ちのときに、フレームワークから呼び出されます。
 * @note	 ウィンドウは、少なくとも間接的に、表示されているフレーム ウィンドウの子ウィンドウであることが必要です
 * @note	 OnUpdateCmdUI は高度なオーバーライド可能な関数です。
 */
void CLineBar::OnUpdateCmdUI(CFrameWnd* pTarget, BOOL bDisableIfNoHndler)
{
	USES_CONVERSION;

	CMFCToolBar::OnUpdateCmdUI(pTarget, bDisableIfNoHndler);

}

/**
 * @brief   色選択ボタン生成
 * @param   [in] iID メニューID
 * @return	色選択ボタンへのポインタ
 * @note	 
 */
CMFCColorMenuButton* CLineBar::CreateColorButton (int iID)
{
	if (m_palColorPicker.GetSafeHandle () == NULL)
	{
        LOGPALETTE* pLogPalette = SYS_CONFIG->GetColorPalette();
		m_palColorPicker.CreatePalette (pLogPalette);
	}


    CMFCColorMenuButton* pColorButton;
    if (iID == ID_MNU_LINE_COLOR )
    {
	    pColorButton = new 
		    CMFCColorMenuButton (iID, 
            GET_SYS_STR(STR_MNU_SET_LINE_COLORS_ITEM).c_str(), &m_palColorPicker);

	    pColorButton->EnableAutomaticButton (GET_SYS_STR(STR_MNU_SET_AUTOMATIC).c_str(), DRAW_CONFIG->crFront);
	    pColorButton->EnableOtherButton     (GET_SYS_STR(STR_MNU_SET_MORE_COLORS).c_str());
	    pColorButton->EnableDocumentColors  (GET_SYS_STR(STR_MNU_SET_LINE_COLORS).c_str());
	    pColorButton->SetColumnsNumber (8);
	    pColorButton->EnableTearOff (ID_COLOR_TEAROFF1, 5, 2);
    }
    else
    {
	    pColorButton = new 
		    CMFCColorMenuButton (iID, 
            GET_SYS_STR(STR_MNU_SET_FILL_COLORS_ITEM).c_str(), &m_palColorPicker);

	    pColorButton->EnableAutomaticButton (GET_SYS_STR(STR_MNU_SET_AUTOMATIC).c_str(), DRAW_CONFIG->crFront);
	    pColorButton->EnableOtherButton     (GET_SYS_STR(STR_MNU_SET_MORE_COLORS).c_str());
	    pColorButton->EnableDocumentColors  (GET_SYS_STR(STR_MNU_SET_FILL_COLORS).c_str());
	    pColorButton->SetColumnsNumber (8);
	    pColorButton->EnableTearOff (ID_COLOR_TEAROFF2, 5, 2);
    }
	return pColorButton;
}



