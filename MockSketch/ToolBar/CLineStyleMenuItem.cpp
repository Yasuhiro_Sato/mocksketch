// This is a part of the Microsoft Foundation Classes C++ library.
// Copyright (C) Microsoft Corporation
// All rights reserved.
//
// This source code is only intended as a supplement to the
// Microsoft Foundation Classes Reference and related
// electronic documentation provided with the library.
// See these sources for detailed information regarding the
// Microsoft Foundation Classes product.

#include "stdafx.h"
#include "CLineStyleMenuItem.h"
#include "System/CSystem.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#include "Resource.h"

IMPLEMENT_DYNCREATE(CLineStyleMenuItem, CMFCToolBarMenuButton)

const int nHorzMargin = 50;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CLineStyleMenuItem::CLineStyleMenuItem(int nStyle, UINT uiCmdID, LPCTSTR lpszText,
									   BOOL bIsChecked) :
	CMFCToolBarMenuButton (uiCmdID, NULL /* HMENU */, -1 /*iImage*/, lpszText)
{
	m_nLineStyle = nStyle;
	if (bIsChecked)
	{
		m_nStyle |= TBBS_CHECKED;
	}
}

int CLineStyleMenuItem::GetIdToLineStyle (UINT id)
{
    int iLineType;
    switch(id)
    {
    case ID_LINE_SOLID      :{iLineType = PS_SOLID; break;}
    case ID_LINE_DASH       :{iLineType = PS_DASH; break;}
    case ID_LINE_DOT        :{iLineType = PS_DOT; break;}
    case ID_LINE_DASHDOT    :{iLineType = PS_DASHDOT; break;}
    case ID_LINE_DASHDOTDOT :{iLineType = PS_DASHDOTDOT; break;}
    default:{iLineType = PS_SOLID;}
    }
    return iLineType;
}

UINT CLineStyleMenuItem::GetLineStyleToId (int iLineType)
{
    int id;
    switch(iLineType)
    {
    case PS_SOLID      :{id = ID_LINE_SOLID; break;}
    case PS_DASH       :{id = ID_LINE_DASH; break;}
    case PS_DOT        :{id = ID_LINE_DOT; break;}
    case PS_DASHDOT    :{id = ID_LINE_DASHDOT; break;}
    case PS_DASHDOTDOT :{id = ID_LINE_DASHDOTDOT; break;}
    default:{id = ID_LINE_SOLID;}
    }
    return id;
}

LPCTSTR CLineStyleMenuItem::GetIdToLineStyleName (UINT id)
{
    LPCTSTR str;
    switch(id)
    {
    case ID_LINE_SOLID      :{str = GET_STR(STR_PRO_LINE_SOLID); break;}
    case ID_LINE_DASH       :{str = GET_STR(STR_PRO_LINE_DASH); break;}
    case ID_LINE_DOT        :{str = GET_STR(STR_PRO_LINE_DOT); break;}
    case ID_LINE_DASHDOT    :{str = GET_STR(STR_PRO_LINE_DASHDOT); break;}
    case ID_LINE_DASHDOTDOT :{str = GET_STR(STR_PRO_LINE_DASHDOTDOT); break;}
    default:{str = GET_STR(STR_PRO_LINE_SOLID);}
    }
    return str;
}

//*****************************************************************************************
CLineStyleMenuItem::~CLineStyleMenuItem()
{
}
//*****************************************************************************************

int CLineStyleMenuItem::GetMaxTextWidth(CDC* pDC)
{
    int iMax = -1;
    CSize sz;
    for (int id  = ID_LINE_SOLID;
             id <= ID_LINE_DASHDOTDOT;
             id++)
    {
        CString str = GetIdToLineStyleName (id);
        sz = pDC->GetTextExtent(str);
        if (iMax < sz.cx)
        {
            iMax = sz.cx;
        }
    }
    return iMax;
}

void CLineStyleMenuItem::OnDraw (CDC* pDC, const CRect& rect, CMFCToolBarImages* pImages,
			BOOL bHorz, BOOL bCustomizeMode, BOOL bHighlight,
			BOOL bDrawBorder, BOOL bGrayDisabledButtons)
{
	ASSERT_VALID (pDC);


	CRect rectItem = rect;
	rectItem.DeflateRect (1, 1);

	//--------------------
	// Fill item interior:
	//--------------------
	FillInterior (pDC, rectItem, bHighlight);

	//-----------------
	// Draw text label:
	//-----------------
	CRect rectText = rectItem;
    int iTextWidth = GetMaxTextWidth(pDC);
	//rectText.DeflateRect (nHorzMargin+10, 0);
    rectText.left = rectItem.left + pImages->GetImageSize().cx + 5;
	rectText.right = rectText.left + iTextWidth; 

	pDC->DrawText (m_strText, rectText, DT_SINGLELINE | DT_VCENTER);

	//-----------
	// Draw line:
	//-----------
	CRect rectLine = rectItem;
	rectLine.left = rectText.right;

    int y = (rect.top + rect.bottom) / 2; 

    int iStyle  = GetIdToLineStyle (m_nStyle);

    CPen pen;
    CPen* pOldPen;
    pen.CreatePen(iStyle , 1, GetSysColor (COLOR_WINDOWTEXT));
    pOldPen = pDC->SelectObject(&pen);

    pDC->MoveTo(rect.left, y);
    pDC->LineTo(rect.right, y);

    pDC->SelectObject(pOldPen);
}
//**************************************************************************************
void CLineStyleMenuItem::CopyFrom (const CMFCToolBarButton& s)
{
	CMFCToolBarButton::CopyFrom (s);

	const CLineStyleMenuItem& src = (const CLineStyleMenuItem&) s;
	m_nLineStyle = src.m_nLineStyle;
}
//**************************************************************************************
SIZE CLineStyleMenuItem::OnCalculateSize (CDC* pDC, const CSize& /*sizeDefault*/, BOOL /*bHorz*/)
{
	TEXTMETRIC tm;
	pDC->GetTextMetrics (&tm);

     int iTextWidth = GetMaxTextWidth(pDC);
	return CSize (
		iTextWidth + 50,	// Text + line
		tm.tmHeight + 4);
}


//===================================================
//===================================================
//===================================================
IMPLEMENT_SERIAL(CToolBarLineComboBox, CMFCToolBarComboBoxButton, 1)

 BOOL CToolBarLineComboBox::m_bHasFocus = FALSE;

CToolBarLineComboBox::CToolBarLineComboBox():CMFCToolBarComboBoxButton(ID_MNU_LINE_STYLE, GetCmdMgr()->GetCmdImage(ID_MNU_LINE_WIDTH), CBS_DROPDOWN)
{
    m_nLineStyle = ID_LINE_SOLID;
}

CToolBarLineComboBox::CToolBarLineComboBox(UINT uiID, int iImage, DWORD dwStyle, int iWidth ):
CMFCToolBarComboBoxButton(uiID,iImage, dwStyle, iWidth)
{
    m_nLineStyle = ID_LINE_SOLID;
}


CToolBarLineComboBox::~CToolBarLineComboBox()
{
}


void CToolBarLineComboBox::SetLineStyle(int iStyle)
{
    m_nLineStyle = iStyle;
    auto pCb = GetComboBox();

    auto pLc = dynamic_cast<CLineComboBox*>(pCb);
    if(pLc)
    {
        CString strItem;
        int iIndex =  m_nLineStyle - ID_LINE_SOLID;
        pLc->SetCurSel(iIndex);
        pLc->GetLBText(iIndex, strItem); 
        SetText(strItem);

        if(m_pWndParent)
        {
            m_pWndParent->RedrawWindow();
        }
    }
}


BOOL CToolBarLineComboBox::NotifyCommand(int iNotifyCode)
{
	BOOL bRes = CMFCToolBarComboBoxButton::NotifyCommand(iNotifyCode);

	switch (iNotifyCode)
	{
	case CBN_KILLFOCUS:
		m_bHasFocus = FALSE;
		bRes = TRUE;
		break;

	case CBN_SETFOCUS:
		m_bHasFocus = TRUE;
		bRes = TRUE;

    case CBN_SELCHANGE:

		bRes = TRUE;
		break;
	}

	return bRes;
}


/**
 *  @brief  コンボボックス生成
 *  @param  [in] pWndParent 親ウィンドウ 
 *  @param  [in] rect コンボボックスサイズ 
 *  @retval コンボボックスへのポインタ
 *  @note
 */
CComboBox* CToolBarLineComboBox::CreateCombo(CWnd* pWndParent, const CRect& rect)
{
	CLineComboBox* pLineCombo = new CLineComboBox;

	if (!pLineCombo->Create(m_dwStyle | CBS_HASSTRINGS | CBS_OWNERDRAWFIXED, rect, pWndParent, m_nID))
	{
		delete pLineCombo;
		return NULL;
	}

    pLineCombo->InsertString(0,GET_STR(STR_PRO_LINE_SOLID)); 
    pLineCombo->InsertString(1,GET_STR(STR_PRO_LINE_DASH));
    pLineCombo->InsertString(2,GET_STR(STR_PRO_LINE_DOT));
    pLineCombo->InsertString(3,GET_STR(STR_PRO_LINE_DASHDOT));
    pLineCombo->InsertString(4,GET_STR(STR_PRO_LINE_DASHDOTDOT));

    pLineCombo->SetItemData(0, (DWORD_PTR)ID_LINE_SOLID);
    pLineCombo->SetItemData(1, (DWORD_PTR)ID_LINE_DASH);
    pLineCombo->SetItemData(2, (DWORD_PTR)ID_LINE_DOT);
    pLineCombo->SetItemData(3, (DWORD_PTR)ID_LINE_DASHDOT);
    pLineCombo->SetItemData(4, (DWORD_PTR)ID_LINE_DASHDOTDOT);

	return pLineCombo;
}


//===================================================
//===================================================
//===================================================

//^^^^^^^^^^^^^^^^^^^
BEGIN_MESSAGE_MAP(CLineComboBox, CComboBox)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_MESSAGE(WM_MFC_INITCTRL, &CLineComboBox::OnInitControl)
    ON_CONTROL_REFLECT(CBN_SELCHANGE, &CLineComboBox::OnCbnSelchange)
END_MESSAGE_MAP()




CLineComboBox::CLineComboBox()
{

}

CLineComboBox::~CLineComboBox()
{
}


int CLineComboBox::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CComboBox::OnCreate(lpCreateStruct) == -1)
		return -1;

	Init();
	return 0;
}

int CLineComboBox::Init()
{
	CWnd* pWndParent = GetParent();
	ASSERT_VALID(pWndParent);
    m_iTextMaxWidth = -1;

	m_bToolBarMode = pWndParent->IsKindOf(RUNTIME_CLASS(CMFCToolBar));
	if (!m_bToolBarMode)
	{
		Setup();
	}
    return 0;
}

BOOL CLineComboBox::Setup()
{
	ASSERT_VALID(this);
	ENSURE(::IsWindow(m_hWnd));

	if (m_bToolBarMode)
	{
		ASSERT(FALSE);
		return FALSE;
	}

	CleanUp();
	return TRUE;
}


void CLineComboBox::CleanUp()
{
	ASSERT_VALID(this);
	ENSURE(::IsWindow(m_hWnd));

    m_iTextMaxWidth = -1;
	if (m_bToolBarMode)
	{
		// Font data will be destroyed by CMFCToolBarFontComboBox object
		return;
	}


	for (int i = 0; i < GetCount(); i++)
	{
		//delete(CMFCFontInfo*) GetItemData(i);
	}

	ResetContent();
}



BOOL CLineComboBox::Create(DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID)
{
	// Remove the CBS_SIMPLE and CBS_DROPDOWN styles and add the one I'm designed for
	dwStyle &= ~0xF;
	dwStyle |= CBS_DROPDOWNLIST;

	// Make sure to use the CBS_OWNERDRAWVARIABLE style
	dwStyle |= CBS_OWNERDRAWVARIABLE;

	// Use default strings. We need the itemdata to store checkmarks
	dwStyle |= CBS_HASSTRINGS;

   BOOL bRet = CComboBox::Create(dwStyle, rect, pParentWnd, nID);

   return bRet;

}

void CLineComboBox::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	int nTextWidth = 50;
	int nTextHeight = 0;

	nTextHeight = afxGlobalData.GetTextHeight();


	lpMeasureItemStruct->itemWidth =  nTextWidth + 2;
	lpMeasureItemStruct->itemHeight = nTextHeight;
}



int CLineComboBox::DeleteString(UINT nIndex)
{
	return CComboBox::DeleteString(nIndex);
}

int CLineComboBox::CompareItem(LPCOMPAREITEMSTRUCT lpCIS)
{
	ASSERT(lpCIS->CtlType == ODT_COMBOBOX);

	int id1 = (int)(WORD)lpCIS->itemID1;
	if (id1 == -1)
	{
		return -1;
	}

	CString str1;
	GetLBText(id1, str1);

	int id2 = (int)(WORD)lpCIS->itemID2;
	if (id2 == -1)
	{
		return 1;
	}

	CString str2;
	GetLBText(id2, str2);

	return str1.Collate(str2);
}

    //----------------
    //  最大の文字幅を取得する
    //---------------
int CLineComboBox::GetMaxTextWidth(CDC* pDC)
{
    int iMax = GetCount( );
 	CString strText;
    int iWidth = -1;
    for (int id = 0;  id < iMax; id++)
    {
	    GetLBText(id, strText);
        CSize sz = pDC->GetTextExtent(strText);
        if(iWidth  < sz.cx)
        {
            iWidth = sz.cx;
        }
    }
    return iWidth;
}

void CLineComboBox::DrawItem(LPDRAWITEMSTRUCT lpDIS)
{
	if (m_Images.GetSafeHandle() == NULL)
	{
		m_Images.Create(IDB_AFXBARRES_FONT, nImageWidth, 0, RGB(255, 255, 255));
	}

	ASSERT(lpDIS->CtlType == ODT_COMBOBOX);

	CDC* pDC = CDC::FromHandle(lpDIS->hDC);
	ASSERT_VALID(pDC);

	CRect rc = lpDIS->rcItem;

	if (lpDIS->itemState & ODS_FOCUS)
	{
		pDC->DrawFocusRect(rc);
	}

	int nIndexDC = pDC->SaveDC();

	CBrush brushFill;
	if (lpDIS->itemState & ODS_SELECTED)
	{
		brushFill.CreateSolidBrush(GetGlobalData()->clrHilite);
		pDC->SetTextColor(GetGlobalData()->clrTextHilite);
	}
	else
	{
		brushFill.CreateSolidBrush(pDC->GetBkColor());
	}

	pDC->SetBkMode(TRANSPARENT);
    pDC->FillRect(rc, &brushFill);

    //----------------
    //  最大の文字幅を取得する
    //---------------
    if(m_iTextMaxWidth == -1)
    {
        m_iTextMaxWidth = GetMaxTextWidth(pDC);
    }

	CRect rectLine = rc;
	rectLine.left = rc.left + m_iTextMaxWidth;

	//rectLine.DeflateRect (nHorzMargin, 0);

	int id = (int)lpDIS->itemID;
	if (id >= 0)
	{
		CString strText;
		GetLBText(id, strText);

		pDC->DrawText(strText, rc, DT_SINGLELINE | DT_VCENTER);

        int y = (rectLine.top + rectLine.bottom) / 2; 

        int iStyleId = GetItemData(id);
        int iStyle  = CLineStyleMenuItem::GetIdToLineStyle (iStyleId);

        CPen pen;
        CPen* pOldPen;
        pen.CreatePen(iStyle , 1, GetSysColor (COLOR_WINDOWTEXT));
        pOldPen = pDC->SelectObject(&pen);

        pDC->MoveTo(rectLine.left, y);
        pDC->LineTo(rectLine.right, y);

        pDC->SelectObject(pOldPen);

	}
	pDC->RestoreDC(nIndexDC);
}

BOOL CLineComboBox::PreTranslateMessage(MSG* pMsg)
{
	if (m_bToolBarMode && pMsg->message == WM_KEYDOWN && !CToolBarLineComboBox::IsFlatMode())
	{
		CMFCToolBar* pBar = (CMFCToolBar*) GetParent();

		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			if (AFXGetTopLevelFrame(this) != NULL)
			{
				AFXGetTopLevelFrame(this)->SetFocus();
			}
			return TRUE;

		case VK_TAB:
			if (pBar != NULL)
			{
				pBar->GetNextDlgTabItem(this)->SetFocus();
			}
			return TRUE;

		case VK_UP:
		case VK_DOWN:
			if ((GetKeyState(VK_MENU) >= 0) &&(GetKeyState(VK_CONTROL) >=0) && !GetDroppedState())
			{
				ShowDropDown();
				return TRUE;
			}
		}
	}
	return CComboBox::PreTranslateMessage(pMsg);
}

void CLineComboBox::PreSubclassWindow()
{
    CComboBox::PreSubclassWindow();

    _AFX_THREAD_STATE* pThreadState = AfxGetThreadState();
    if (pThreadState->m_pWndInit == NULL)
    {
	    Init();
    }
}

void CLineComboBox::OnDestroy()
{
	CleanUp();
	CComboBox::OnDestroy();
}


LRESULT CLineComboBox::OnInitControl(WPARAM wParam, LPARAM lParam)
{
	DWORD dwSize = (DWORD)wParam;
	BYTE* pbInitData = (BYTE*)lParam;


	CString strDst;
	CMFCControlContainer::UTF8ToString((LPSTR)pbInitData, strDst, dwSize);

	//CTagManager tagManager(strDst);

	Setup();
	return 0;
}

void CLineComboBox::OnCbnSelchange()
{
    // TODO: ここにコントロール通知ハンドラー コードを追加します。
    int iIndex = GetCurSel();

    DWORD_PTR iId = GetItemData(iIndex);
    GetOwner()->SendMessage(WM_COMMAND, iId);

}
