// This is a part of the Microsoft Foundation Classes C++ library.
// Copyright (C) Microsoft Corporation
// All rights reserved.
//
// This source code is only intended as a supplement to the
// Microsoft Foundation Classes Reference and related
// electronic documentation provided with the library.
// See these sources for detailed information regarding the
// Microsoft Foundation Classes product.

#pragma once
#include "afxtoolbarcomboboxbutton.h"
#include "resource.h"


class CLineStyleMenuItem : public CMFCToolBarMenuButton  
{
	DECLARE_DYNCREATE(CLineStyleMenuItem)

public:
	CLineStyleMenuItem(int nStyleId = ID_LINE_SOLID, UINT uiCmdID = 0, LPCTSTR lpszText = NULL,
						BOOL bIsChecked = FALSE);
	virtual ~CLineStyleMenuItem();
    static int GetIdToLineStyle (UINT id);
    static UINT GetLineStyleToId (int iLineType);
    static LPCTSTR GetIdToLineStyleName (UINT id);
    
    static int GetMaxTextWidth(CDC* pDC);


protected:
	virtual void OnDraw (CDC* pDC, const CRect& rect, CMFCToolBarImages* pImages,
						BOOL bHorz = TRUE, BOOL bCustomizeMode = FALSE,
						BOOL bHighlight = FALSE,
						BOOL bDrawBorder = TRUE,
						BOOL bGrayDisabledButtons = TRUE);

	virtual void CopyFrom (const CMFCToolBarButton& src);
	virtual SIZE OnCalculateSize (CDC* pDC, const CSize& sizeDefault, BOOL bHorz);
	int	m_nLineStyle;	// Line Style
    CString m_strMax;
};

//===================================================
//===================================================
//===================================================



class CToolBarLineComboBox:
    public CMFCToolBarComboBoxButton
{
    DECLARE_SERIAL(CToolBarLineComboBox)
public:
	CToolBarLineComboBox();
    CToolBarLineComboBox(UINT uiID, int iImage, DWORD dwStyle = CBS_DROPDOWN, int iWidth = 0);
    ~CToolBarLineComboBox();

    void SetLineStyle(int iLineStyle);

    //!<カレントレイヤ変更
    afx_msg LRESULT OnLayerChange    (WPARAM wParam, LPARAM lParam);


public:
	static BOOL HasFocus()
	{
		return m_bHasFocus;
	}


protected:
	static BOOL m_bHasFocus;
    int	 m_nLineStyle;	// Line width


protected:
	virtual CComboBox* CreateCombo(CWnd* pWndParent, const CRect& rect);
    virtual BOOL NotifyCommand(int iNotifyCode);
#if 0
 	virtual void OnDraw (CDC* pDC, const CRect& rect, CMFCToolBarImages* pImages,
						BOOL bHorz = TRUE, BOOL bCustomizeMode = FALSE,
						BOOL bHighlight = FALSE,
						BOOL bDrawBorder = TRUE,
						BOOL bGrayDisabledButtons = TRUE);
#endif

};



class CLineComboBox: public CComboBox
{

public:
	CLineComboBox();
    virtual ~CLineComboBox();
    virtual BOOL Create(DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID);


protected:
    int Init();
	void CleanUp();
	BOOL Setup();

// Operations
public:
	int DeleteString(UINT nIndex);
	virtual int CompareItem(LPCOMPAREITEMSTRUCT lpCompareItemStruct);
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	virtual void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg LRESULT OnInitControl(WPARAM wParam, LPARAM lParam);

    int GetMaxTextWidth(CDC* pDC);
protected:
    virtual void PreSubclassWindow();

protected:
    int  m_nLineStyle;
    COLORREF color;
    CBrush* pBrush;

    CImageList m_Images;
 	BOOL       m_bToolBarMode;
    int m_iTextMaxWidth = 0;

    int nImageHeight = 16;
    int nImageWidth = 16;


    DECLARE_MESSAGE_MAP()
public:
    afx_msg void OnCbnSelchange();
};
