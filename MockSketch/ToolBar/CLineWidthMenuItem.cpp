// This is a part of the Microsoft Foundation Classes C++ library.
// Copyright (C) Microsoft Corporation
// All rights reserved.
//
// This source code is only intended as a supplement to the
// Microsoft Foundation Classes Reference and related
// electronic documentation provided with the library.
// See these sources for detailed information regarding the
// Microsoft Foundation Classes product.

#include "stdafx.h"
#include "CLineWidthMenuItem.h"
#include "System/CSystem.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#include "Resource.h"

IMPLEMENT_DYNCREATE(CLineWidthMenuItem, CMFCToolBarMenuButton)

const int nHorzMargin = 50;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CLineWidthMenuItem::CLineWidthMenuItem(int nWidth, UINT uiCmdID, LPCTSTR lpszText,
									   BOOL bIsChecked) :
	CMFCToolBarMenuButton (uiCmdID, NULL /* HMENU */, -1 /*iImage*/, lpszText)
{
	m_nWidth = nWidth;
	if (bIsChecked)
	{
		m_nStyle |= TBBS_CHECKED;
	}
}
//*****************************************************************************************
CLineWidthMenuItem::~CLineWidthMenuItem()
{
}
//*****************************************************************************************

int CLineWidthMenuItem::GetMaxTextWidth(CDC* pDC)
{
    CSize sz = pDC->GetTextExtent(m_strMax);
    return sz.cx;
}

void CLineWidthMenuItem::OnDraw (CDC* pDC, const CRect& rect, CMFCToolBarImages* pImages,
			BOOL bHorz, BOOL bCustomizeMode, BOOL bHighlight,
			BOOL bDrawBorder, BOOL bGrayDisabledButtons)
{
	ASSERT_VALID (pDC);


	CRect rectItem = rect;
	rectItem.DeflateRect (1, 1);

	//--------------------
	// Fill item interior:
	//--------------------
	FillInterior (pDC, rectItem, bHighlight);

	//-----------------
	// Draw text label:
	//-----------------
	CRect rectText = rectItem;
    int iTextWidth = GetMaxTextWidth(pDC);
	//rectText.DeflateRect (nHorzMargin+10, 0);
    rectText.left = rectItem.left + pImages->GetImageSize().cx + 5;
	rectText.right = rectText.left + iTextWidth; 

	pDC->DrawText (m_strText, rectText, DT_SINGLELINE | DT_VCENTER);

	//-----------
	// Draw line:
	//-----------
	CRect rectLine = rectItem;
	rectLine.left = rectText.right;
	//rectLine.DeflateRect (nHorzMargin, 0);

	rectLine.top = (rect.top + rect.bottom - m_nWidth) / 2;
	rectLine.bottom = rectLine.top + m_nWidth;

	pDC->FillSolidRect (rectLine, GetSysColor (COLOR_WINDOWTEXT));

/*
	//-------------
	// Draw border:
	//-------------
	if (m_nStyle & TBBS_CHECKED)
	{
		pDC->Draw3dRect (rectItem, GetSysColor (COLOR_3DSHADOW), GetSysColor (COLOR_3DHILIGHT));
	}
	else if (bHighlight)
	{
		pDC->Draw3dRect (rectItem, GetSysColor (COLOR_3DHILIGHT), GetSysColor (COLOR_3DSHADOW));
	}
*/
}
//**************************************************************************************
void CLineWidthMenuItem::CopyFrom (const CMFCToolBarButton& s)
{
	CMFCToolBarButton::CopyFrom (s);

	const CLineWidthMenuItem& src = (const CLineWidthMenuItem&) s;
	m_nWidth = src.m_nWidth;
}
//**************************************************************************************
SIZE CLineWidthMenuItem::OnCalculateSize (CDC* pDC, const CSize& /*sizeDefault*/, BOOL /*bHorz*/)
{
	TEXTMETRIC tm;
	pDC->GetTextMetrics (&tm);

     int iTextWidth = GetMaxTextWidth(pDC);
	return CSize (
		iTextWidth + 50,	// Text + line
		max (m_nWidth, tm.tmHeight) + 4);
}


//===========================================
IMPLEMENT_SERIAL(CToolBarLineWidthComboBox, CMFCToolBarComboBoxButton, 1)

 BOOL CToolBarLineWidthComboBox::m_bHasFocus = FALSE;

CToolBarLineWidthComboBox::CToolBarLineWidthComboBox():
CMFCToolBarComboBoxButton(ID_MNU_LINE_WIDTH, GetCmdMgr()->GetCmdImage(ID_MNU_LINE_WIDTH), CBS_DROPDOWN)
{
    m_nWidth = 1;
}

CToolBarLineWidthComboBox::CToolBarLineWidthComboBox(UINT uiID, int iImage, DWORD dwStyle, int iWidth ):
CMFCToolBarComboBoxButton(uiID,iImage, dwStyle, iWidth)
{
    m_nWidth = 1;
}


CToolBarLineWidthComboBox::~CToolBarLineWidthComboBox()
{
}


void CToolBarLineWidthComboBox::SetWidth(int iWidth)
{
    m_nWidth = iWidth;
    auto pCb = GetComboBox();

    auto pLc = dynamic_cast<CLineWidthComboBox*>(pCb);
    if(pLc)
    {
        CString strItem;
        int iIndex =  m_nWidth - 1;
        pLc->SetCurSel(iIndex);
        pLc->GetLBText(iIndex, strItem); 
        SetText(strItem);

        if(m_pWndParent)
        {
            m_pWndParent->RedrawWindow();
        }
    }
}


BOOL CToolBarLineWidthComboBox::NotifyCommand(int iNotifyCode)
{
	BOOL bRes = CMFCToolBarComboBoxButton::NotifyCommand(iNotifyCode);

	switch (iNotifyCode)
	{
	case CBN_KILLFOCUS:
		m_bHasFocus = FALSE;
		bRes = TRUE;
		break;

	case CBN_SETFOCUS:
		m_bHasFocus = TRUE;
		bRes = TRUE;

    case CBN_SELCHANGE:

		bRes = TRUE;
		break;
	}

	return bRes;
}


/**
 *  @brief  コンボボックス生成
 *  @param  [in] pWndParent 親ウィンドウ 
 *  @param  [in] rect コンボボックスサイズ 
 *  @retval コンボボックスへのポインタ
 *  @note
 */
CComboBox* CToolBarLineWidthComboBox::CreateCombo(CWnd* pWndParent, const CRect& rect)
{
	CLineWidthComboBox* pLineCombo = new CLineWidthComboBox;

	if (!pLineCombo->Create(m_dwStyle | CBS_HASSTRINGS | CBS_OWNERDRAWFIXED, rect, pWndParent, m_nID))
	{
		delete pLineCombo;
		return NULL;
	}

    pLineCombo->InsertString(0, GET_STR(STR_ID_LINE_WIDTH_1));
    pLineCombo->InsertString(1, GET_STR(STR_ID_LINE_WIDTH_2));
    pLineCombo->InsertString(2, GET_STR(STR_ID_LINE_WIDTH_3));
    pLineCombo->InsertString(3, GET_STR(STR_ID_LINE_WIDTH_4));
    pLineCombo->InsertString(4, GET_STR(STR_ID_LINE_WIDTH_5));
    pLineCombo->InsertString(5, GET_STR(STR_ID_LINE_WIDTH_6));
    pLineCombo->InsertString(6, GET_STR(STR_ID_LINE_WIDTH_7));
    pLineCombo->InsertString(7, GET_STR(STR_ID_LINE_WIDTH_8));
    pLineCombo->InsertString(8, GET_STR(STR_ID_LINE_WIDTH_9));
    pLineCombo->InsertString(9, GET_STR(STR_ID_LINE_WIDTH_10));

    pLineCombo->SetItemData(0, (DWORD_PTR)ID_LINE_WIDTH_1);
    pLineCombo->SetItemData(1, (DWORD_PTR)ID_LINE_WIDTH_2);
    pLineCombo->SetItemData(2, (DWORD_PTR)ID_LINE_WIDTH_3);
    pLineCombo->SetItemData(3, (DWORD_PTR)ID_LINE_WIDTH_4);
    pLineCombo->SetItemData(4, (DWORD_PTR)ID_LINE_WIDTH_5);
    pLineCombo->SetItemData(5, (DWORD_PTR)ID_LINE_WIDTH_6);
    pLineCombo->SetItemData(6, (DWORD_PTR)ID_LINE_WIDTH_7);
    pLineCombo->SetItemData(7, (DWORD_PTR)ID_LINE_WIDTH_8);
    pLineCombo->SetItemData(8, (DWORD_PTR)ID_LINE_WIDTH_9);
    pLineCombo->SetItemData(9, (DWORD_PTR)ID_LINE_WIDTH_10);

	return pLineCombo;
}

//^^^^^^^^^^^^^^^^^^^
BEGIN_MESSAGE_MAP(CLineWidthComboBox, CComboBox)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_MESSAGE(WM_MFC_INITCTRL, &CLineWidthComboBox::OnInitControl)
    ON_CONTROL_REFLECT(CBN_SELCHANGE, &CLineWidthComboBox::OnCbnSelchange)
END_MESSAGE_MAP()




CLineWidthComboBox::CLineWidthComboBox()
{

}

CLineWidthComboBox::~CLineWidthComboBox()
{
}


int CLineWidthComboBox::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CComboBox::OnCreate(lpCreateStruct) == -1)
		return -1;

	Init();
	return 0;
}

int CLineWidthComboBox::Init()
{
	CWnd* pWndParent = GetParent();
	ASSERT_VALID(pWndParent);
    m_iTextMaxWidth = -1;

	m_bToolBarMode = pWndParent->IsKindOf(RUNTIME_CLASS(CMFCToolBar));
	if (!m_bToolBarMode)
	{
		Setup();
	}
    return 0;
}

BOOL CLineWidthComboBox::Setup()
{
	ASSERT_VALID(this);
	ENSURE(::IsWindow(m_hWnd));

	if (m_bToolBarMode)
	{
		ASSERT(FALSE);
		return FALSE;
	}

	CleanUp();
	return TRUE;
}


void CLineWidthComboBox::CleanUp()
{
	ASSERT_VALID(this);
	ENSURE(::IsWindow(m_hWnd));

    m_iTextMaxWidth = -1;
	if (m_bToolBarMode)
	{
		// Font data will be destroyed by CMFCToolBarFontComboBox object
		return;
	}


	for (int i = 0; i < GetCount(); i++)
	{
		//delete(CMFCFontInfo*) GetItemData(i);
	}

	ResetContent();
}



BOOL CLineWidthComboBox::Create(DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID)
{
	// Remove the CBS_SIMPLE and CBS_DROPDOWN styles and add the one I'm designed for
	dwStyle &= ~0xF;
	dwStyle |= CBS_DROPDOWNLIST;

	// Make sure to use the CBS_OWNERDRAWVARIABLE style
	dwStyle |= CBS_OWNERDRAWVARIABLE;

	// Use default strings. We need the itemdata to store checkmarks
	dwStyle |= CBS_HASSTRINGS;

   BOOL bRet = CComboBox::Create(dwStyle, rect, pParentWnd, nID);

   return bRet;

}

void CLineWidthComboBox::MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	int nTextWidth = 50;
	int nTextHeight = 0;

	nTextHeight = afxGlobalData.GetTextHeight();


	lpMeasureItemStruct->itemWidth =  nTextWidth + 2;
	lpMeasureItemStruct->itemHeight = nTextHeight;
}



int CLineWidthComboBox::DeleteString(UINT nIndex)
{
	return CComboBox::DeleteString(nIndex);
}

int CLineWidthComboBox::CompareItem(LPCOMPAREITEMSTRUCT lpCIS)
{
	ASSERT(lpCIS->CtlType == ODT_COMBOBOX);

	int id1 = (int)(WORD)lpCIS->itemID1;
	if (id1 == -1)
	{
		return -1;
	}

	CString str1;
	GetLBText(id1, str1);

	int id2 = (int)(WORD)lpCIS->itemID2;
	if (id2 == -1)
	{
		return 1;
	}

	CString str2;
	GetLBText(id2, str2);

	return str1.Collate(str2);
}

    //----------------
    //  最大の文字幅を取得する
    //---------------
int CLineWidthComboBox::GetMaxTextWidth(CDC* pDC)
{
    int iMax = GetCount( );
 	CString strText;
    int iWidth = -1;
    for (int id = 0;  id < iMax; id++)
    {
	    GetLBText(id, strText);
        CSize sz = pDC->GetTextExtent(strText);
        if(iWidth  < sz.cx)
        {
            iWidth = sz.cx;
        }
    }
    return iWidth;
}

void CLineWidthComboBox::DrawItem(LPDRAWITEMSTRUCT lpDIS)
{
	if (m_Images.GetSafeHandle() == NULL)
	{
		m_Images.Create(IDB_AFXBARRES_FONT, nImageWidth, 0, RGB(255, 255, 255));
	}

	ASSERT(lpDIS->CtlType == ODT_COMBOBOX);

	CDC* pDC = CDC::FromHandle(lpDIS->hDC);
	ASSERT_VALID(pDC);

	CRect rc = lpDIS->rcItem;

	if (lpDIS->itemState & ODS_FOCUS)
	{
		pDC->DrawFocusRect(rc);
	}

	int nIndexDC = pDC->SaveDC();

	CBrush brushFill;
	if (lpDIS->itemState & ODS_SELECTED)
	{
		brushFill.CreateSolidBrush(GetGlobalData()->clrHilite);
		pDC->SetTextColor(GetGlobalData()->clrTextHilite);
	}
	else
	{
		brushFill.CreateSolidBrush(pDC->GetBkColor());
	}

	pDC->SetBkMode(TRANSPARENT);
    pDC->FillRect(rc, &brushFill);

    //----------------
    //  最大の文字幅を取得する
    //---------------
    if(m_iTextMaxWidth == -1)
    {
        m_iTextMaxWidth = GetMaxTextWidth(pDC);
    }

	CRect rectLine = rc;
	rectLine.left = rc.left + m_iTextMaxWidth;

	//rectLine.DeflateRect (nHorzMargin, 0);

	int id = (int)lpDIS->itemID;
	if (id >= 0)
	{
		CString strText;
		GetLBText(id, strText);

		pDC->DrawText(strText, rc, DT_SINGLELINE | DT_VCENTER);


        int iLineWidth = id + 1;
	    rectLine.top = (rc.top + rc.bottom - iLineWidth) / 2;
	    rectLine.bottom = rectLine.top + iLineWidth;

	    pDC->FillSolidRect (rectLine, GetSysColor (COLOR_WINDOWTEXT));
	}
	pDC->RestoreDC(nIndexDC);
}

BOOL CLineWidthComboBox::PreTranslateMessage(MSG* pMsg)
{
	if (m_bToolBarMode && pMsg->message == WM_KEYDOWN && !CToolBarLineWidthComboBox::IsFlatMode())
	{
		CMFCToolBar* pBar = (CMFCToolBar*) GetParent();

		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			if (AFXGetTopLevelFrame(this) != NULL)
			{
				AFXGetTopLevelFrame(this)->SetFocus();
			}
			return TRUE;

		case VK_TAB:
			if (pBar != NULL)
			{
				pBar->GetNextDlgTabItem(this)->SetFocus();
			}
			return TRUE;

		case VK_UP:
		case VK_DOWN:
			if ((GetKeyState(VK_MENU) >= 0) &&(GetKeyState(VK_CONTROL) >=0) && !GetDroppedState())
			{
				ShowDropDown();
				return TRUE;
			}
		}
	}
	return CComboBox::PreTranslateMessage(pMsg);
}

void CLineWidthComboBox::PreSubclassWindow()
{
    CComboBox::PreSubclassWindow();

    _AFX_THREAD_STATE* pThreadState = AfxGetThreadState();
    if (pThreadState->m_pWndInit == NULL)
    {
	    Init();
    }
}

void CLineWidthComboBox::OnDestroy()
{
	CleanUp();
	CComboBox::OnDestroy();
}


LRESULT CLineWidthComboBox::OnInitControl(WPARAM wParam, LPARAM lParam)
{
	DWORD dwSize = (DWORD)wParam;
	BYTE* pbInitData = (BYTE*)lParam;


	CString strDst;
	CMFCControlContainer::UTF8ToString((LPSTR)pbInitData, strDst, dwSize);

	//CTagManager tagManager(strDst);

	Setup();
	return 0;
}

void CLineWidthComboBox::OnCbnSelchange()
{
    // TODO: ここにコントロール通知ハンドラー コードを追加します。
    int iIndex = GetCurSel();

    DWORD_PTR iId = GetItemData(iIndex);
    GetOwner()->SendMessage(WM_COMMAND, iId);

}
