/**
 * @brief			CDrawBar実装ファイル
 * @file			CDrawBar.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "CShowBar.h"
#include "MockSketch.h"
#include "MainFrm.h"
#include "System/CSystem.h"
#include "DefinitionObject/View/MockSketchView.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

IMPLEMENT_SERIAL(CShowBar, CCustomToolBar, 1)

BEGIN_MESSAGE_MAP(CShowBar, CCustomToolBar)
	//{{AFX_MSG_MAP(CDrawBar)
	ON_REGISTERED_MESSAGE(AFX_WM_RESETTOOLBAR, OnToolbarReset)
	ON_REGISTERED_MESSAGE(AFX_WM_CREATETOOLBAR, OnToolbarCreateNew)

    ON_CBN_SELENDOK(ID_SHOW_POS, OnChangeComboBox)
    ON_COMMAND(ID_SHOW_POS, OnChangeComboBox)

	//}}AFX_MSG_MAP
	// Global help commands
END_MESSAGE_MAP()


/*---------------------------------------------------*/
/* Static variables                                  */
/*---------------------------------------------------*/


/**
 * @brief   コンストラクタ
 * @param   なし
 * @return	なし
 * @note	 
 */
 CShowBar::CShowBar():CCustomToolBar()
{
    SetName(_T("CShowBar"));
    m_iPos = 0;
}

/**
 * @brief   ツール バーを元の状態に戻します
 * @param   なし
 * @return	なし
 * @note	 既定の実装では、何も行われません。
 * @note	 ツール バーが元の状態に戻るときに置き換える必要がある
 * @note	 ダミー ボタンがツール バーにある場合は、
 * @note	 CMFCToolBar クラスから派生したクラスで 
 * @note	 OnReset をオーバーライドします。
 */
 void CShowBar::OnReset ()
{
    CMFCToolBarComboBoxButton2  combo 
        (ID_SHOW_POS,                                        // コマンド ID。
        GetCmdMgr ()->GetCmdImage  (ID_SHOW_POS, FALSE),     // 関連付けられたイメージのインデックス。
        WS_VISIBLE | WS_TABSTOP | WS_VSCROLL | CBS_DROPDOWNLIST,    // スタイル。
        150 );                              // 幅 (ピクセル単位)。

    combo.SetToolTipText(GET_STR(STR_DIAG_SHOW_POS_SET));
    ReplaceButton (ID_SHOW_POS, combo);

    auto pCombo = GetComboButton(ID_SHOW_POS);

    if (!pCombo)
    {
        return;
    }

    pCombo->ClearData();
    int iSize = ID_MNU_SHOW_POS_MAX - ID_MNU_SHOW_POS_01;

    for (int iCnt = 0; iCnt < iSize; iCnt++)
    {
        StdStringStream  strm;
        strm << StdFormat( _T("%s %d")) % GET_STR(STR_SHOW_POS) % (iCnt + 1);
        pCombo->AddSortedItem(strm.str().c_str(), 
                               (DWORD_PTR)ID_MNU_SHOW_POS_01 + iCnt) ;
    }
    pCombo->SelectItem(0);
}

/**
 * @brief   ツール バー更新要求
 * @param   [in]    アプリケーションのメイン フレーム ウィンドウへのポインタ。
 *                   このポインタを更新メッセージのルーティングに使います
 * @param   [in]    新ハンドラを持たないコントロールを自動的に使用禁止状態で
 *                   表示するかどうかを示すフラグです。
 * @return	なし
 * @note	 各ボタンまたはペインを更新するには、適切な更新ハンドラを設定するためにメッセージ マップで 
 * @note	 ON_UPDATE_COMMAND_UI マクロを使います。
 * @note	 OnUpdateCmdUI は、アプリケーションが入力待ちのときに、フレームワークから呼び出されます。
 * @note	 ウィンドウは、少なくとも間接的に、表示されているフレーム ウィンドウの子ウィンドウであることが必要です
 * @note	 OnUpdateCmdUI は高度なオーバーライド可能な関数です。
 */
void CShowBar::OnUpdateCmdUI(CFrameWnd* pTarget, BOOL bDisableIfNoHndler)
{
	USES_CONVERSION;

	CMFCToolBar::OnUpdateCmdUI(pTarget, bDisableIfNoHndler);

}

LRESULT CShowBar::OnToolbarReset(WPARAM wp,LPARAM)
{
    UINT uiToolBarId = (UINT) wp;
    if (uiToolBarId == IDR_SHOW)
    {


    }

    return 0;
}

LRESULT CShowBar::OnToolbarCreateNew(WPARAM wp,LPARAM)
{
  return 0;
}

BOOL CShowBar::OnCommand (int iId, int iCode, HWND hWnd)
{
    auto pComboButton = GetComboButton(ID_SHOW_POS);
    if (pComboButton)
    {
        auto pCombo =  pComboButton->GetComboBox();
        HWND hC = pCombo->m_hWnd;
    }
   DB_PRINT(_T("CCustomToolBar::OnCommand %d-%d \n"), iId, iCode);

    return FALSE;
}

void CShowBar::OnChangeComboBox(void)
{
    auto pCombo = GetComboButton(ID_SHOW_POS);
    if(pCombo == NULL)
    {
        return;
    }
    int iIndex = pCombo->GetCurSel();
    DWORD_PTR iId = pCombo->GetItemData(iIndex);
    GetOwner()->SendMessage(WM_COMMAND, iId);
}


void CShowBar::SetShowPos(int iNo)
{
    m_iPos =  iNo;
    auto pCombo = GetComboButton(ID_SHOW_POS);
    if(pCombo == NULL)
    {
        return;
    }
    pCombo->SelectItem(m_iPos);
}

int  CShowBar::GetShowPos() const
{
    return m_iPos;

}

