/**
 * @brief			CDrawBarヘッダー
 * @file			CDrawBar.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef __SHOWBAR_H__
#define __SHOWBAR_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "CustomToolBar.h"
#include "CCustomMenuButton.h"




/**
 * @class   CShowBar
 * @brief   表示位置ツールバー                     
 */
class CShowBar : public CCustomToolBar
{
	DECLARE_SERIAL(CShowBar)

// Construction
public:
	CShowBar();

// Operations
public:
	virtual void CShowBar::OnUpdateCmdUI(CFrameWnd* pTarget, BOOL bDisableIfNoHndler);
// Attributes
public:
    void SetShowPos(int iNo);
    int  GetShowPos() const;

     virtual BOOL OnCommand (int iId, int iCode, HWND hWnd);

protected:
    int m_iPos;

// Implementation
protected:
	virtual void OnReset ();

	// Generated message map functions
	//{{AFX_MSG(CDrawBar)
	afx_msg LRESULT OnToolbarReset(WPARAM,LPARAM);
	afx_msg LRESULT OnToolbarCreateNew(WPARAM,LPARAM);
    afx_msg void OnChangeComboBox(void);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif

/////////////////////////////////////////////////////////////////////////////
