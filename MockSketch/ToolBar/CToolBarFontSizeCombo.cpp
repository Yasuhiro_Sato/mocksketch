/**
 * @brief			CDrawingView実装ファイル
 * @file			CDrawingView.cpp
 * @author			Yasuhiro Sato
 * @date			01-2-2009 10:36:26
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
// This is a part of the Microsoft Foundation Classes C++ library.
// Copyright (C) Microsoft Corporation
// All rights reserved.
//
// This source code is only intended as a supplement to the
// Microsoft Foundation Classes Reference and related
// electronic documentation provided with the library.
// See these sources for detailed information regarding the
// Microsoft Foundation Classes product.

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "afxcontrolbarutil.h"
#include "afxtoolbar.h"
#include "afxribbonres.h"
#include "afxtoolbarfontcombobox.h"
#include "afxfontcombobox.h"
#include "CToolBarFontSizeCombo.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

IMPLEMENT_SERIAL(CToolBarFontSizeCombo, CMFCToolBarComboBoxButton, 1)

/*---------------------------------------------------*/
/* Static variables                                  */
/*---------------------------------------------------*/
static int nFontSizes[] = {8, 9, 10, 11, 12, 14, 16, 18, 20, 22, 24, 26, 28, 36, 48, 72};


/**
 * @brief   コンストラクタ
 * @param   なし         
 * @retval  なし
 * @note
 */
CToolBarFontSizeCombo::CToolBarFontSizeCombo()
{
	m_nLogVert = 0;
}

/**
 * @brief   コンストラクタ
 * @param   [in] uiID
 * @param   [in] iImage
 * @param   [in] dwStyle
 * @param   [in] iWidth
 * @retval  なし
 * @note
 */
CToolBarFontSizeCombo::CToolBarFontSizeCombo(UINT uiID, int iImage, DWORD dwStyle, int iWidth) :
	CMFCToolBarComboBoxButton(uiID, iImage, dwStyle, iWidth)
{
	m_nLogVert = 0;
}

/**
 * @brief   デストラクタ
 * @param   なし         
 * @retval  なし
 * @note
 */
CToolBarFontSizeCombo::~CToolBarFontSizeCombo()
{
}

/**
 *  @brief  フォントサイズ再構築
 *  @param  [in] strFontName フォント名 
 *  @retval なし
 *  @note
 */
void CToolBarFontSizeCombo::RebuildFontSizes(const CString& strFontName)
{
	if (strFontName.IsEmpty())
	{
		return;
	}

	CString strText = m_strEdit;

	if (m_pWndCombo != NULL)
	{
		m_pWndCombo->SetRedraw(FALSE);
	}

	CWindowDC dc(NULL);

	RemoveAllItems();

	m_nLogVert = dc.GetDeviceCaps(LOGPIXELSY);
	::EnumFontFamilies(dc.GetSafeHdc(), strFontName, (FONTENUMPROC) EnumSizeCallBack, (LPARAM) this);

	if (!SelectItem(strText))
	{
		m_strEdit = strText;
		if (m_pWndCombo != NULL)
		{
			m_pWndCombo->SetWindowText(m_strEdit);
		}
	}

	// Synchronize context with other comboboxes with the same ID:
	CObList listButtons;
	if (CMFCToolBar::GetCommandButtons(m_nID, listButtons) > 0)
	{
		for (POSITION posCombo = listButtons.GetHeadPosition(); posCombo != NULL;)
		{
			CMFCToolBarComboBoxButton* pCombo = DYNAMIC_DOWNCAST(CMFCToolBarComboBoxButton, listButtons.GetNext(posCombo));

			if (pCombo != NULL && pCombo != this)
			{
				if (pCombo->GetComboBox() != NULL)
				{
					pCombo->GetComboBox()->SetRedraw(FALSE);
				}

				pCombo->RemoveAllItems();

				POSITION pos;
				POSITION posData;

				for (pos = m_lstItems.GetHeadPosition(), posData = m_lstItemData.GetHeadPosition(); pos != NULL && posData != NULL;)
				{
					pCombo->AddItem(m_lstItems.GetNext(pos), m_lstItemData.GetNext(posData));
				}

				if (pCombo->GetComboBox() != NULL)
				{
					pCombo->GetComboBox()->SetRedraw();
				}
			}
		}
	}

	if (m_pWndCombo != NULL)
	{
		m_pWndCombo->SetRedraw();
	}
}

/**
 *  @brief  サイズ取得コールバック
 *  @param  [in] lpntm 
 *  @param  [in] FontType 
 *  @param  [in] FontType 
 *  @retval TRUE 再度呼出し FALSE 呼出完了
 *  @note
 */
BOOL FAR PASCAL CToolBarFontSizeCombo::EnumSizeCallBack(LOGFONT FAR* /*lplf*/, LPNEWTEXTMETRIC lpntm,int FontType, LPVOID lpv)
{
	CToolBarFontSizeCombo* pThis = (CToolBarFontSizeCombo*) lpv;
	ASSERT_VALID(pThis);

	if ((FontType & TRUETYPE_FONTTYPE) || !((FontType & TRUETYPE_FONTTYPE) ||(FontType & RASTER_FONTTYPE))) // if truetype or vector font
	{
		// this occurs when there is a truetype and nontruetype version of a font
		for (int i = 0; i < 16; i++)
		{
			CString strSize;
			strSize.Format(_T("%d"), nFontSizes[i]);

			pThis->AddItem(strSize);
		}

		return FALSE; // don't call me again
	}

	// calc character height in pixels
	pThis->InsertSize(MulDiv(lpntm->tmHeight-lpntm->tmInternalLeading, 1440, pThis->m_nLogVert));

	return TRUE; // call me again
}

/**
 *  @brief  Twipサイズ文字変換
 *  @param  [in] nTwips Twipサイズ
 *  @retval Twipサイズ文字
 *  @note
 */
CString CToolBarFontSizeCombo::TwipsToPointString(int nTwips)
{
	CString str;
	if (nTwips >= 0)
	{
		// round to nearest half point
		nTwips = (nTwips + 5) / 10;

		if ((nTwips % 2) == 0)
		{
			str.Format(_T("%ld"), nTwips/2);
		}
		else
		{
			str.Format(_T("%.1f"), (float) nTwips / 2.F);
		}
	}

	return str;
}

/**
 *  @brief  Twipサイズ設定
 *  @param  [in] nTwips Twipサイズ
 *  @retval なし
 *  @note
 */
void CToolBarFontSizeCombo::SetTwipSize(int nTwips)
{
	SetText(TwipsToPointString(nTwips));
}

/**
 *  @brief  Twipサイズ取得
 *  @param  なし 
 *  @retval Twipで表したフォントサイズ
 *          -2 エラー 
 *          -1 エディットボックスは空
 *  @note
 */
int CToolBarFontSizeCombo::GetTwipSize() const
{
	// return values
	// -2 -- error
	// -1 -- edit box empty
	// >=0 -- font size in twips

	CString str = GetItem() == NULL ? m_strEdit : GetItem();
	if (m_lstItems.Find(m_strEdit) == NULL)
	{
		str = m_strEdit;
	}

	LPCTSTR lpszText = str;

	while (*lpszText == ' ' || *lpszText == '\t')
		lpszText++;

	if (lpszText[0] == NULL)
		return -1; // no text in control

	double d = _tcstod(lpszText, (LPTSTR*)&lpszText);
	while (*lpszText == ' ' || *lpszText == '\t')
		lpszText++;

	if (*lpszText != NULL)
		return -2;   // not terminated properly

	return(d<0.) ? 0 :(int)(d*20.);
}

/**
 *  @brief  サイズ追加
 *  @param  [in] nSize サイズ 
 *  @retval なし
 *  @note
 */
void CToolBarFontSizeCombo::InsertSize(int nSize)
{
	ASSERT(nSize > 0);
	AddItem(TwipsToPointString(nSize), (DWORD) nSize);
}

/**
 *  @brief  コンボボックス生成
 *  @param  [in] pWndParent 親ウィンドウ 
 *  @param  [in] rect コンボボックスサイズ 
 *  @retval コンボボックスへのポインタ
 *  @note
 */
CComboBox* CToolBarFontSizeCombo::CreateCombo(CWnd* pWndParent, const CRect& rect)
{
	CMFCFontComboBox* pWndCombo = new CMFCFontComboBox;
	if (!pWndCombo->Create(m_dwStyle, rect, pWndParent, m_nID))
	{
		delete pWndCombo;
		return NULL;
	}

	return pWndCombo;
}
