// This is a part of the Microsoft Foundation Classes C++ library.
// Copyright (C) Microsoft Corporation
// All rights reserved.
//
// This source code is only intended as a supplement to the
// Microsoft Foundation Classes Reference and related
// electronic documentation provided with the library.
// See these sources for detailed information regarding the
// Microsoft Foundation Classes product.

#pragma once
class CToolBarFontSizeCombo : public CMFCToolBarComboBoxButton  
{
	DECLARE_SERIAL(CToolBarFontSizeCombo)

// Construction/Destruction
public:
	CToolBarFontSizeCombo(UINT uiID, int iImage, DWORD dwStyle = CBS_DROPDOWN, int iWidth = 0);
	virtual ~CToolBarFontSizeCombo();

protected:
	CToolBarFontSizeCombo();

protected:
	int m_nLogVert;

// Operations:
public:
	void RebuildFontSizes(const CString& strFontName);

	void SetTwipSize(int nSize);
	int GetTwipSize() const;

protected:
	CString TwipsToPointString(int nTwips);
	void InsertSize(int nSize);
	static BOOL FAR PASCAL EnumSizeCallBack(LOGFONT FAR* lplf, LPNEWTEXTMETRIC lpntm,int FontType, LPVOID lpv);

// Overrides:
protected:
	virtual CComboBox* CreateCombo(CWnd* pWndParent, const CRect& rect);
};
