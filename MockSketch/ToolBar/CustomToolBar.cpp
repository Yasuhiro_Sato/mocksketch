// CustomToolBar.cpp : 実装ファイル
//
/**
 * @brief			CustomToolBar実装ファイル
 * @file			CCustomToolBar.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *	
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MockSketch.h"
#include "CustomToolBar.h"
#include "./SYSTEM/CSystem.h"
#include "./SYSTEM/CSystem.h"
#include "Utility/CUtility.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
IMPLEMENT_DYNAMIC(CCustomToolBar, CMFCToolBar)
BEGIN_MESSAGE_MAP(CCustomToolBar, CMFCToolBar)
END_MESSAGE_MAP()

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CCustomToolBar::CCustomToolBar()
{
    m_bMultiThreaded = TRUE;
}

/**
 *  @brief  デストラクタ
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CCustomToolBar::~CCustomToolBar()
{
}

/**
 *  @brief  名称設定
 *  @param  [in] strName
 *  @retval なし     
 *  @note   デバッグ用に名前を設定する
 */
void CCustomToolBar::SetName(LPCTSTR strName)
{
    m_strName = strName;
}

/**
 *  @brief  ユーザツールチップ
 *  @param  [in]    pButton
 *  @param  [inout] strTTText
 *  @retval TRUEでstrTTTextをツールチップに設定する
 *  @note
 */
BOOL CCustomToolBar::OnUserToolTip(CMFCToolBarButton* pButton, CString& strTTText) const
{
    CString strTip;
    if (strTip.LoadString(pButton->m_nID))
    {
        strTip = GET_STR((LPCTSTR)strTip);
        AfxExtractSubString(strTTText, strTip, 1, '\n');
        return TRUE;
    }
    return FALSE;

}

/**
 * @brief   ツール バーを元の状態に戻します
 * @param   なし
 * @return	なし
 * @note	 既定の実装では、何も行われません。
 * @note	 ツール バーが元の状態に戻るときに置き換える必要がある
 * @note	 ダミー ボタンがツール バーにある場合は、
 * @note	 CMFCToolBar クラスから派生したクラスで 
 * @note	 OnReset をオーバーライドします。
 */
 void CCustomToolBar::OnReset ()
{
    CMenu* pMenu = GetMenu();;
    HMENU hMenu = pMenu->m_hMenu;
    CUtil::ConvertMenuString(hMenu);
}

BOOL CCustomToolBar::OnCommand (int iId, int iCode, HWND hWnd)
{
     DB_PRINT(_T("CCustomToolBar::OnCommand %d-%d \n"), iId, iCode);

    return FALSE;
}

// CCustomToolBar メッセージ ハンドラ

LRESULT CCustomToolBar::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
    // TODO: ここに特定なコードを追加するか、もしくは基本クラスを呼び出してください。
#ifndef FOR_DEBUG
    if (message == WM_ERASEBKGND)
    {
        HDC hDc = reinterpret_cast<HDC>(wParam);

        // 想定外のメモリーリークーに対する対応
        // 寸法ツールバーにアイコンが表示された時リソースリークが発生する
        //DB_PRINT(_T("WM_ERASEBKGND Cancel %s(%x) %s(%d) \n"), m_strName.c_str(), this, CUtil::ConvWinMessage(message), message);
        return 1;
    }

    else if(message == WM_COMMAND)
    {
        WORD wID         =  wParam & 0xFFFF;
        WORD wNotifyCode = (wParam >> 16) & 0xFFFF;
        HWND hwndControl =  (HWND)lParam;
        if(OnCommand (wID, wNotifyCode, hwndControl))
        {
            return 1;
        }


       //DB_PRINT(_T("%s(%x) %s(%d) \n"), m_strName.c_str(), this, CUtil::ConvWinMessage(message), message);

    }
   //DB_PRINT(_T("%s(%x) %s(%d) \n"), m_strName.c_str(), this, CUtil::ConvWinMessage(message), message);
#endif
    return CMFCToolBar::WindowProc(message, wParam, lParam);
}

CMFCToolBarComboBoxButton* CCustomToolBar::GetComboButton(UINT id)  const
{
    CObList listButtons;
    if(CMFCToolBar::GetCommandButtons(id, listButtons) <= 0)
    {
        return NULL;
    }

    CMFCToolBarComboBoxButton* pCombo = NULL;
    CMFCToolBarComboBoxButton* pRetCombo = NULL;

    POSITION posCombo = listButtons.GetHeadPosition();

    while (posCombo != NULL)
    {
        pCombo = DYNAMIC_DOWNCAST(CMFCToolBarComboBoxButton, 
                listButtons.GetAt(posCombo));

        if (pCombo)
        {
            pRetCombo = pCombo;
            break;
        }

        listButtons.GetNext(posCombo);
    }

    return pRetCombo;
}

CComboBox* CCustomToolBar::GetComboBox(UINT id)  const
{
  auto pButton = GetComboButton(id);

 if(pButton)
 {
    return pButton->GetComboBox();
 }
 return NULL;
}


//ComboBox のSetItemDataに設定したコマンドを送信する
bool CCustomToolBar::SendCurSel(UINT id) const
{
    auto pCombo = GetComboBox(id);
    if (!pCombo)
    {
        return false;
    }
 
    int iIndex = pCombo->GetCurSel();
    DWORD_PTR iId = pCombo->GetItemData(iIndex);
    GetOwner()->SendMessage(WM_COMMAND, iId);

    return true;
}
