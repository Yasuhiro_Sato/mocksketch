#pragma once


class  CMFCToolBarComboBoxButton2: public  CMFCToolBarComboBoxButton
{
public:
    	CMFCToolBarComboBoxButton2(UINT uiID, int iImage, DWORD dwStyle = CBS_DROPDOWNLIST, int iWidth = 0)
            :CMFCToolBarComboBoxButton( uiID, iImage, dwStyle, iWidth){;}

    void SetToolTipText(LPCTSTR strText){m_strToolTip = strText;}

    virtual BOOL OnGetCustomToolTipText(CString& strToolTip)
    {
        strToolTip = m_strToolTip;
        return FALSE;
    }
protected:
    CString m_strToolTip;
};

// CCustomToolBar

class CCustomToolBar : public CMFCToolBar
{
	DECLARE_DYNAMIC(CCustomToolBar)

public:
	CCustomToolBar();
	virtual ~CCustomToolBar();

    virtual void OnReset ();

	virtual BOOL OnUserToolTip(CMFCToolBarButton* pButton, CString& strTTText) const;

    virtual void SetName(LPCTSTR strName);

    CMFCToolBarComboBoxButton* GetComboButton(UINT id) const;
    CComboBox* GetComboBox(UINT id) const;
    bool SendCurSel(UINT id) const;

    virtual BOOL OnCommand (int iId, int iCode, HWND hWnd);

protected:
	DECLARE_MESSAGE_MAP()
    virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);

protected:
    StdString m_strName;
};


