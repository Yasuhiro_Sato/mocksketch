/**
 * @brief			CAny実装ファイル
 * @file		    CAny.cpp
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CAny.h"
#include "Utility/CUtility.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#ifndef _CONSOLE
#define new DEBUG_NEW
#endif
#endif


/**
 *  @brief   値取得
 *  @retval  設定値
 *  @note
 */
bool CAny::GetBool() const
{
    bool bRet;
    try
    {
        bRet = boost::any_cast<bool>(anyData);
    }
    catch(...)
    {
        throw MockException(e_type_error);
    }
    return bRet;
}

/**
 *  @brief   値取得
 *  @retval  設定値
 *  @note
 */
int CAny::GetInt() const
{
    int iRet;
    try
    {
        iRet = (boost::any_cast<int>(anyData));
    }
    catch(...)
    {
        throw MockException(e_type_error);
    }
    return iRet;
}

/**
 *  @brief   値取得
 *  @retval  設定値
 *  @note
 */
double CAny::GetDouble   () const
{
    double dRet;
    try
    {
        dRet = boost::any_cast<double>(anyData);
    }
    catch(...)
    {
        throw MockException(e_type_error);
    }
    return dRet;
}

/**
 *  @brief   値取得
 *  @retval  設定値
 *  @note
 */
COLORREF   CAny::GetColor    () const
{
    COLORREF crRet;
    try
    {
        crRet = boost::any_cast<COLORREF>(anyData);
    }
    catch(...)
    {
        throw MockException(e_type_error);
    }
    return crRet;
}

/**
 *  @brief   値取得
 *  @retval  設定値
 *  @note
 */
POINT2D    CAny::GetPoint    () const
{
    POINT2D ptRet;
    try
    {
        ptRet = boost::any_cast<POINT2D>(anyData);
    }
    catch(...)
    {
        throw MockException(e_type_error);
    }
    return ptRet;
}

/**
 *  @brief   値取得
 *  @retval  設定値
 *  @note
 */
RECT2D    CAny::GetRect    () const
{
    RECT2D rcRet;
    try
    {
        rcRet = boost::any_cast<RECT2D>(anyData);
    }
    catch(...)
    {
        throw MockException(e_type_error);
    }
    return rcRet;
}

/**
 *  @brief   値取得
 *  @retval  設定値
 *  @note
 */
StdString  CAny::GetString   () const
{
    StdString stRet;
    try
    {
        if (anyData.type() == typeid(StdString))
        {
            stRet = boost::any_cast<StdString>(anyData);
        }
        else if (anyData.type() == typeid(const StdChar* ))
        {
            stRet = boost::any_cast<const StdChar*>(anyData);
        }
    }
    catch(...)
    {
        throw MockException(e_type_error);
    }
    return stRet;
}

/**
 *  @brief   値取得
 *  @retval  設定値
 *  @note
 */
std::vector<POINT2D> CAny::GetPointList() const
{
    std::vector<POINT2D> vecRet;
    try
    {
        vecRet = boost::any_cast< std::vector<POINT2D> >(anyData);
    }
    catch(...)
    {
        throw MockException(e_type_error);
    }
    return vecRet;
}


/**
*  @brief   値取得
*  @retval  設定値
*  @note
*/
LINE2D CAny::GetLine() const
{
    LINE2D aRet;
    try
    {
        aRet = boost::any_cast<LINE2D>(anyData);
    }
    catch(...)
    {
        throw MockException(e_type_error);
    }
    return aRet;
}

CIRCLE2D CAny::GetCircle() const
{
    CIRCLE2D aRet;
    try
    {
        aRet = boost::any_cast<CIRCLE2D>(anyData);
    }
    catch(...)
    {
        throw MockException(e_type_error);
    }
    return aRet;
}

ELLIPSE2D CAny::GetEllipse() const
{
    ELLIPSE2D aRet;
    try
    {
        aRet = boost::any_cast<ELLIPSE2D>(anyData);
    }
    catch(...)
    {
        throw MockException(e_type_error);
    }
    return aRet;
}

HYPERBOLA2D CAny::GetHyperbola() const
{
    HYPERBOLA2D aRet;
    try
    {
        aRet = boost::any_cast<HYPERBOLA2D>(anyData);
    }
    catch(...)
    {
        throw MockException(e_type_error);
    }
    return aRet;
}

PARABOLA2D CAny::GetParabola() const
{
    PARABOLA2D aRet;
    try
    {
        aRet = boost::any_cast<PARABOLA2D>(anyData);
    }
    catch(...)
    {
        throw MockException(e_type_error);
    }
    return aRet;
}

SPLINE2D CAny::GetSpline() const
{
    SPLINE2D aRet;
    try
    {
        aRet = boost::any_cast<SPLINE2D>(anyData);
    }
    catch(...)
    {
        throw MockException(e_type_error);
    }
    return aRet;
}

MAT2D CAny::GetMat() const
{
    MAT2D aRet;
    try
    {
        aRet = boost::any_cast<MAT2D>(anyData);
    }
    catch(...)
    {
        throw MockException(e_type_error);
    }
    return aRet;
}

/**
 *  @brief   値取得(変換あり)
 *  @retval  設定値
 *  @note
 */
bool CAny::ToBool() const
{
    bool bRet = false;
    if (anyData.type() == typeid(bool))
    {
        bRet = boost::any_cast<bool>(anyData);
    }
    else
    {
        bRet = (ToInt() != 0 ? 1 : 0);
    }
    return bRet;
}

/**
 *  @brief   値取得(変換あり)
 *  @retval  設定値
 *  @note
 */
int CAny::ToInt() const
{
    int iRet = 0;

    if(anyData.type() == typeid(int))
    {
        iRet = boost::any_cast<int>(anyData);
    }
    else if (anyData.type() == typeid(bool))
    {
        iRet = (boost::any_cast<bool>(anyData) ? 1 : 0);
    }
    else if(anyData.type() == typeid(double))
    {
        iRet = static_cast<int>(boost::any_cast<double>(anyData));
    }
    else if(anyData.type() == typeid(COLORREF))
    {
        iRet = static_cast<int>(boost::any_cast<COLORREF>(anyData));
    }
    else if(anyData.type() == typeid(StdString))
    {
        StdString strInt;
        strInt = boost::any_cast<StdString>(anyData);
        iRet = _tstoi(strInt.c_str());
    }
    return iRet;
}

/**
 *  @brief   値取得(変換あり)
 *  @retval  設定値
 *  @note
 */
double CAny::ToDouble() const
{
    double dRet = 0.0;
    if(anyData.type() == typeid(double))
    {
        dRet = boost::any_cast<double>(anyData);
    }
    else if(anyData.type() == typeid(StdString))
    {
        StdChar*   pStop;
        StdString strDouble;
        strDouble = boost::any_cast<StdString>(anyData);
        dRet = _tcstod(strDouble.c_str(), &pStop);
    }
    else
    {
        dRet = static_cast<double>(ToInt());
    }
    return dRet;
}

/**
 *  @brief   値取得(変換あり)
 *  @retval  設定値
 *  @note
 */
COLORREF CAny::ToColor() const
{
    COLORREF crRet;
    if (anyData.type() == typeid(COLORREF))
    {
        crRet = boost::any_cast<COLORREF>(anyData);
    }
    else
    {
        crRet = static_cast<COLORREF>(ToInt());
    }
    return crRet;
}

/**
 *  @brief   値取得(変換あり)
 *  @retval  設定値
 *  @note
 */
POINT2D CAny::ToPOINT2D() const
{
    POINT2D ptRet;
    if (anyData.type() == typeid(POINT2D))
    {
        ptRet = boost::any_cast<POINT2D>(anyData);
    }
    return ptRet;
}

/**
 *  @brief   値取得(変換あり)
 *  @retval  設定値
 *  @note
 */
RECT2D CAny::ToRECT2D() const
{
    RECT2D rcRet;
    if (anyData.type() == typeid(RECT2D))
    {
        rcRet = boost::any_cast<RECT2D>(anyData);
    }
    return rcRet;
}

LINE2D CAny::ToLINE2D() const
{
    LINE2D ret;
    if (anyData.type() == typeid(LINE2D))
    {
        ret = boost::any_cast<LINE2D>(anyData);
    }
    return ret;
}

CIRCLE2D CAny::ToCIRCLE2D() const
{
    CIRCLE2D ret;
    if (anyData.type() == typeid(CIRCLE2D))
    {
        ret = boost::any_cast<CIRCLE2D>(anyData);
    }
    return ret;
}

ELLIPSE2D CAny::ToELLIPSE2D() const
{
    ELLIPSE2D ret;
    if (anyData.type() == typeid(ELLIPSE2D))
    {
        ret = boost::any_cast<ELLIPSE2D>(anyData);
    }
    return ret;
}

HYPERBOLA2D CAny::ToHYPERBOLA2D() const
{
    HYPERBOLA2D ret;
    if (anyData.type() == typeid(HYPERBOLA2D))
    {
        ret = boost::any_cast<HYPERBOLA2D>(anyData);
    }
    return ret;
}

PARABOLA2D CAny::ToPARABOLA2D() const
{
    PARABOLA2D ret;
    if (anyData.type() == typeid(PARABOLA2D))
    {
        ret = boost::any_cast<PARABOLA2D>(anyData);
    }
    return ret;
}


SPLINE2D CAny::ToSPLINE2D() const
{
    SPLINE2D ret;
    if (anyData.type() == typeid(SPLINE2D))
    {
        ret = boost::any_cast<SPLINE2D>(anyData);
    }
    return ret;
}

MAT2D CAny::ToMAT2D() const
{
    MAT2D ret;
    if (anyData.type() == typeid(MAT2D))
    {
        ret = boost::any_cast<MAT2D>(anyData);
    }
    return ret;
}

/**
 *  @brief   値取得(変換あり)
 *  @retval  設定値
 *  @note
 */
StdString CAny::ToString() const
{
    StdString strRet;
    strRet = CUtil::AnyToString(anyData, false);
    strRet = CUtil::TrimQuotation(strRet);
    return strRet;
}

/**
 *  @brief   値取得(変換あり)
 *  @retval  設定値
 *  @note
 */
std::vector<POINT2D> CAny::ToPointList() const
{
    std::vector<POINT2D> lstRet;
    if (anyData.type() == typeid(std::vector<POINT2D>))
    {
        lstRet = boost::any_cast<std::vector<POINT2D>>(anyData);
    }
    else if(anyData.type() == typeid(StdString))
    {
        StdChar*   pStop;
        std::vector<StdString> strData;
        StdString  strAny;
        strAny = boost::any_cast<StdString>(anyData);
        CUtil::TokenizeCsv(&strData, strAny);
        size_t nSize;
        nSize = strData.size();
        POINT2D pt2D;
        int iMax = int(floor(nSize / 2.0));
        for (int iCnt = 0; iCnt < iMax; iCnt++)
        {
            pt2D.dX = _tcstod(strData[iCnt * 2].c_str(), &pStop);
            pt2D.dY = _tcstod(strData[iCnt * 2 + 1].c_str(), &pStop);
            lstRet.push_back(pt2D);
        }
    }
    return lstRet;
}

/**
 *  @brief   AngerScript用 形名取得
 *  @retval  形名
 *  @note    
 */
StdString CAny::GetAsTypeName() const
{
    StdString strRet;
    if(anyData.type() == typeid(int))
    {
        strRet = _T("int");
    }
    else if (anyData.type() == typeid(bool))
    {
        strRet = _T("bool");
    }
    else if(anyData.type() == typeid(double))
    {
        strRet = _T("double");
    }
    else if(anyData.type() == typeid(COLORREF))
    {
        //unsigned long
        strRet = _T("uint");
    }
    else if(anyData.type() == typeid(POINT2D))
    {
        strRet = _T("POINT2D");
    }
    else if(anyData.type() == typeid(RECT2D))
    {
        strRet = _T("RECT2D");
    }
    else if(anyData.type() == typeid(LINE2D))
    {
        strRet = _T("LINE2D");
    }
    else if(anyData.type() == typeid(CIRCLE2D))
    {
        strRet = _T("CIRCLE2D");
    }
    else if(anyData.type() == typeid(ELLIPSE2D))
    {
        strRet = _T("ELLIPSE2D");
    }
    else if(anyData.type() == typeid(HYPERBOLA2D))
    {
        strRet = _T("HYPERBOLA2D");
    }
    else if(anyData.type() == typeid(PARABOLA2D))
    {
        strRet = _T("PARABOLA2D");
    }
    else if(anyData.type() == typeid(SPLINE2D))
    {
        strRet = _T("SPLINE2D");
    }
    else if(anyData.type() == typeid(StdString))
    {
        strRet = _T("string");
    }
    else if(anyData.type() == typeid(std::vector<POINT2D>))
    {
        strRet = _T("POINT2D_VECTOR");
    }
    else
    {
        strRet = _T("UNKNOWN_TYPE");
    }
    return strRet;
}


#ifdef _DEBUG
void TEST_CAny()
{
    CUtil::DbgOut(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    CAny any1;
    CAny any2;

    any1 = 10;

    any2 = _T("Hello");


    int i1 = any1.ToInt();

    StdString str1 = any1.ToString();
    StdString str2 = any2.ToString();

    STD_ASSERT(i1 == 10);
    STD_ASSERT(str1 == _T("10"));
    STD_ASSERT(str2 == _T("Hello"));

    CUtil::DbgOut(_T("%s End\n"), DB_FUNC_NAME.c_str());
    CUtil::DbgOut(_T("\n"));
}
#endif