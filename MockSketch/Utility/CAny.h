
/**
 * @brief			CAnyヘッダーファイル
 * @file		    CAny.h
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#if !defined (__ANY_H__)
#define __ANY_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DrawingObject/BaseObj.h"
#include "DrawingObject/Primitive/POINT2D.H"
#include "DrawingObject/Primitive/RECT2D.H"
#include "DrawingObject/Primitive/LINE2D.H"
#include "DrawingObject/Primitive/CIRCLE2D.H"
#include "DrawingObject/Primitive/ELLIPSE2D.H"
#include "DrawingObject/Primitive/HYPERBOLA2D.H"
#include "DrawingObject/Primitive/PARABOLA2D.H"
#include "DrawingObject/Primitive/SPLINE2D.H"

/*---------------------------------------------------*/
/*  classes                                          */
/*---------------------------------------------------*/


class CAny : public CBaseObj
{

public:
    boost::any anyData ;

public:
    CAny(){;}

    CAny(const boost::any & other)
    {
        anyData = other;
    }
    
    CAny(const CAny & other)
    {
        anyData = other.anyData;
    }

    template<typename ValueType>
    CAny(const ValueType & value)
    {
        anyData = value;
    }

    virtual ~CAny(){;}

    CAny & operator=(const boost::any& rhs)
    {
        boost::any(rhs).swap(anyData);
        return *this;
    }

    CAny & operator=(const CAny& rhs)
    {
        boost::any(rhs.anyData).swap(anyData);
        return *this;
    }

    CAny & operator=(CAny& rhs)
    {
        boost::any(rhs.anyData).swap(anyData);
        return *this;
    }

    template <class ValueType>
    CAny & operator=(ValueType&& rhs)
    {
        boost::any(static_cast<ValueType&&>(rhs)).swap(anyData);

        if (anyData.type() == typeid(const StdChar* ))
        {
            StdString str = boost::any_cast<const StdChar* >(anyData);
            anyData = str;
        }

        return *this;
    }


    //AngerScript用形名取得
    StdString GetAsTypeName() const;

public:
    //-----------------
    // 値取得(型チェックあり)
    //-----------------
    bool        GetBool     () const;
    int         GetInt      () const;
    double      GetDouble   () const;
    COLORREF    GetColor    () const;
    POINT2D     GetPoint    () const;
    RECT2D      GetRect     () const;

    LINE2D      GetLine     () const;
    CIRCLE2D    GetCircle   () const;
    ELLIPSE2D   GetEllipse  () const;
    HYPERBOLA2D GetHyperbola() const;
    PARABOLA2D  GetParabola () const;
    SPLINE2D    GetSpline   () const;
    MAT2D       GetMat      () const;

    StdString   GetString   () const;
    std::vector<POINT2D> GetPointList() const;


    //-----------------
    // 値取得(変換あり)
    //-----------------
    bool        ToBool      () const;
    int         ToInt       () const;
    double      ToDouble    () const;
    COLORREF    ToColor     () const;
    POINT2D     ToPOINT2D   () const;
    RECT2D      ToRECT2D    () const;
    LINE2D      ToLINE2D    () const;
    CIRCLE2D    ToCIRCLE2D  () const;
    ELLIPSE2D   ToELLIPSE2D () const;
    HYPERBOLA2D ToHYPERBOLA2D() const;
    PARABOLA2D  ToPARABOLA2D() const;
    SPLINE2D    ToSPLINE2D  () const;
    MAT2D       ToMAT2D  () const;

    StdString   ToString    () const;
    std::vector<POINT2D> ToPointList() const;


    //!< 値設定(AS用)
    void SetVal      (bool bVal)                      { anyData = bVal;}
    void SetVal      (int  iVal)                      { anyData = iVal;}
    void SetVal      (double  dVal)                   { anyData = dVal;}
    void SetVal      (COLORREF  crVal)                { anyData = crVal;}
    void SetVal      (StdString   strVal)             { anyData = strVal;}
    void SetVal      (POINT2D   ptVal)                { anyData = ptVal;}
    void SetVal      (RECT2D    rcVal)                { anyData = rcVal;}
    void SetVal      (std::vector<POINT2D>   lstPtVal){ anyData = lstPtVal;}
    void SetVal      (LINE2D      lineVal)            { anyData = lineVal;}
    void SetVal      (CIRCLE2D    circleVal)          { anyData = circleVal;}
    void SetVal      (ELLIPSE2D   ellipseVal)         { anyData = ellipseVal;}
    void SetVal      (HYPERBOLA2D hyperbolaVal)       { anyData = hyperbolaVal;}
    void SetVal      (PARABOLA2D  parabolaVal)        { anyData = parabolaVal;}
    void SetVal      (SPLINE2D    splineVal)          { anyData = splineVal;}
    void SetVal      (MAT2D       matVal)             { anyData = matVal;}


    void SetVal      (void *ref, int refTypeId);


private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        StdString strAny;
        try
        {
            if (Archive::is_saving::value)
            {
                strAny = CUtil::AnyToString(anyData);
            }

            ar & boost::serialization::make_nvp("Val"          ,     strAny);

            if (Archive::is_loading::value)
            {
                anyData = CUtil::StringToAny(strAny);
            }
        }
        catch(...)
        {
            STD_DBG(_T("Serlization Error"));
        }
    }
};
#endif __ANY_H__