/**
 * @brief			CDelayExecインクルードファイル
 * @file			CDelayExec.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */


#ifndef  _DELAY_EXEC_H__
#define  _DELAY_EXEC_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/




/**
 * @class   CDelayExec
 * @brief   遅延実行処理
 *          一定時間後に関数の実行を行う。
 *          遅延時間内に、何度実行を呼び出さしても１度しか
 *          実行しない。
 */
class CDelayExec
{
    //!< スレッド
    boost::thread* m_pThread;

    //!< 開始フラグ
    bool m_bStart;

    //!< 終了フラグ
    bool m_bEnd;

    //!< 遅延時間
    int  m_iTime;

    //-----------------------
    boost::function<void (void*)> m_pFunc;
    void*                         m_pData;

public:
    /**
     * @brief   コンストラクタ
     * @param   なし 
     * @retval  なし
     * @note	
     */
    CDelayExec()
    {
        m_pThread = NULL;
        m_iTime   = 100;
        m_bStart  = false;
        m_bEnd    = false;
    }

    /**
     * @brief   デストラクタ
     * @param   なし 
     * @retval  なし
     * @note	
     */
    ~CDelayExec()
    {
        if (m_pThread)
        {
            m_pThread->join();
            delete m_pThread;
        }
    }


    /**
     * @brief   関数設定
     * @param   iTime       遅延時間(msec)
     * @param   func        遅延実行関数
     * @param   pData       thisを設定
     * @retval  なし
     * @note	
     */
    void SetFunc(int iTime, boost::function<void (void*)> func, void* pData)
    {
        m_iTime = iTime;
        m_pFunc = func;
        m_pData = pData;
    }

    /**
     * @brief   実行
     * @param   なし 
     * @retval  なし
     * @note    
     */
    void Exec()
    {
        STD_ASSERT(m_pFunc != NULL);
        STD_ASSERT(m_pData != NULL);

        if (m_bEnd)
        {
            if (m_pThread)
            {
                m_pThread->join();
                delete m_pThread;
                m_pThread = NULL;
            }
        }

        m_bStart = true;
        if (!m_pThread) 
        {
            m_bEnd   = false;
            m_pThread = new boost::thread(&ThreadFunc, this);
        }
    }

    //!< 開始判定
    bool  IsStart()      { return m_bStart; }

    //!< 終了
    bool  isEnd()        { return m_bEnd;   }

    //!< 開始
    void  CheckStart()   { m_bStart = false;}

    //!< 終了
    void  CheckEnd()     { m_bEnd   = true; }

    //!< 遅延時間取得
    int   GetSleepTime() { return m_iTime;  }

    //!< 遅延関数取得
    boost::function<void (void*)> GetFunction(){return m_pFunc;}

    //!< データ取得
    void* GetData(){return m_pData;}


    /**
     * @brief   遅延実行
     * @param   pDelay 
     * @retval  なし
     * @note    Exec()から呼び出される
     */
    static void ThreadFunc(CDelayExec* pDelay)
    {
        while(1)
        {
            pDelay->CheckStart();
            StdSleep(pDelay->GetSleepTime());
            if(pDelay->IsStart() == true)
            {
                continue;
            }
            break;
        }

        //実行する
        pDelay->GetFunction()(pDelay->GetData());
        pDelay->CheckEnd();
    }
};

#endif  //_DELAY_EXEC_H__
