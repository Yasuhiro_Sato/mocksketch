/**
 * @brief			CExtText実装ファイル
 * @file		    CExtText.cpp
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "CIdObj.h"
#include "CUtility.h"
#include "SYSTEM/MOCK_ERROR.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


/**
 *  @brief   コンストラクター
 *  @param   なし
 *  @retval  なし
 *  @note
 */
CIdObj::CIdObj():
m_iIdNext(1),
m_iIdMax(ID_DRAWING_MAX - 10)
{
}


/**
 *  @brief   コンストラクター
 *  @param   なし
 *  @retval  なし
 *  @note
 */
CIdObj::CIdObj(int iIdMax):
m_iIdNext(1),
m_iIdMax(iIdMax)
{
}

/**
 *  @brief   デストラクタ
 *  @param   なし
 *  @retval  なし
 *  @note
 */
CIdObj::~CIdObj()
{

}

/**
 *  @brief   代入演算子
 *  @param   [in] obj 代入元 
 *  @retval  代入値
 *  @note    Mutexがあるため代入演算子を定義
 */
CIdObj& CIdObj::operator = (const CIdObj & obj)
{
    m_lstDelId = obj.m_lstDelId;

    m_iIdNext  = obj.m_iIdNext;

    m_iIdMax   = obj.m_iIdMax;

    return *this;
}


/**
 *  @brief   ID追加
 *  @param   なし
 *  @retval  なし
 *  @note    呼び出し元はガベージコレクションの処理を書いておくこと
 */
int CIdObj::CreateId()
{
    int iRet;
    if (m_iIdNext <= m_iIdMax)
    {
        iRet = m_iIdNext;
        m_iIdNext++;
        if (m_iIdNext == (m_iIdMax + 1))
        {
            //ガベージコレクション要求
            //（未使用IDの回収)
            throw MockException(e_gc_request); 
        }
    }
    else
    {   
        if (m_lstDelId.empty())
        {
            throw MockException(e_object_full); 
        }

        {
        //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        boost::mutex::scoped_lock lk(m_Mtx);
        std::set<int>::iterator ite;
        ite = m_lstDelId.begin();
        iRet = *ite;
        m_lstDelId.erase(ite);
        }
        //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    }
    return iRet;
}

/**
 *  @brief   ID削除
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CIdObj::DeleteId(int iId)
{
    //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    {
    boost::mutex::scoped_lock lk(m_Mtx);
    m_lstDelId.insert(iId);
    }
    //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
}

/**
 *  @brief   クリア
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CIdObj::Clear()
{
    m_lstDelId.clear();
    m_iIdNext = 0;
}

//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void TEST_CIdObj()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    CIdObj IdObj(10);


    STD_ASSERT( IdObj.CreateId() == 1 );
    STD_ASSERT( IdObj.CreateId() == 2 );
    STD_ASSERT( IdObj.CreateId() == 3 );
    STD_ASSERT( IdObj.CreateId() == 4 );

    IdObj.DeleteId(1);
    IdObj.DeleteId(3);
    IdObj.DeleteId(3);
    IdObj.DeleteId(3);

    STD_ASSERT( IdObj.CreateId() == 5 );
    STD_ASSERT( IdObj.CreateId() == 6 );
    STD_ASSERT( IdObj.CreateId() == 7 );
    STD_ASSERT( IdObj.CreateId() == 8 );
    STD_ASSERT( IdObj.CreateId() == 9 );

    try
    {
        int iRet;
        iRet = IdObj.CreateId();
    }
    catch (MockException& e)
    {
        //ガベージコレクション要求
        STD_ASSERT( e.GetErr() == e_gc_request );
        DB_PRINT(_T("Error Success [%s]\n"), e.GetErrMsg().c_str());
    }

    STD_ASSERT( IdObj.CreateId() == 1 );
    STD_ASSERT( IdObj.CreateId() == 3 );

    try
    {
        int iRet;
        iRet = IdObj.CreateId();
    }
    catch (MockException& e)
    {
        //もう書き込めません
        STD_ASSERT( e.GetErr() == e_object_full );
        DB_PRINT(_T("Error Success [%s]\n"), e.GetErrMsg().c_str());
    }

    IdObj.Clear();
    STD_ASSERT( IdObj.CreateId() == 0 );


    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));

}
#endif //_DEBUG