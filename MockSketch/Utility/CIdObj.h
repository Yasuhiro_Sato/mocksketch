/**
 * @brief			ID_DATAヘッダーファイル
 * @file		    ID_DATA.h
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef IDOBJ_H__
#define IDOBJ_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "SYSTEM/MOCK_ERROR.h"

/**
 * @class   CIdObj
 * @brief   ユニークなIDの生成を行う
 *          IDを使い切ったあとは 、例外が発生する
 *          その後、使用していないIDを登録することで再度IDの生成を
 *          行えるようにする。
 *          ガベージコレクションはスレッドで行うことを想定する 
 */
class CIdObj
{   
public:

    //!< コンストラクタ
    CIdObj();

    //!< コンストラクタ
    CIdObj(int iIdMax);

    //!< デストラクタ
    ~CIdObj();

    CIdObj& operator = (const CIdObj & obj);

    //!< ID追加 
    int CreateId();

    //!<ID削除
    void DeleteId(int iId);

    //!< クリア
    void Clear();

public:
    //!<ミューテックス
    boost::mutex m_Mtx; 

protected:
    std::set<int> m_lstDelId;

    int m_iIdNext;

    int m_iIdMax;


private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
            SERIALIZATION_BOTH("IdNext"    , m_iIdNext);
            SERIALIZATION_BOTH("IdMax"     , m_iIdMax);
            SERIALIZATION_BOTH("IdDelete"  , m_lstDelId);
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }
};

#endif //IDOBJ_H__