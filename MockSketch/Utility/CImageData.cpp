/**
 * @brief			CImageData実装ファイル
 * @file		    CImageData.cpp
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MockSketch.h"
#include "CImageData.h"
#include "Utility/CUtility.h"
#include "System/CSystem.h"
//#include "DefinitionObject/CPartsDef.h"


#include <gdiplus.h>
#include <afximageeditordialog.h>
#include <boost/serialization/binary_object.hpp>


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif



BOOL WINAPI PlgBlt2( HDC hdcDest, const POINT *lpPoint,
                        HDC hdcSrc, INT nXSrc, INT nYSrc, INT nWidth,
                        INT nHeight, HDC hdcMask, INT xMask, INT yMask)
{
   /* we need to use floating-point precision when calculating the
     * XFORM matrix to avoid loss of precision due to rounding */
    typedef struct tagPOINTLF
    {
        double x, y;
    } POINTLF;

    int oldgMode;
    /* parallelogram coords */
    POINTLF plg[3];
    /* rect coords */
    POINTLF rect[3];
    XFORM xf;
    XFORM SrcXf;
    XFORM oldDestXf;
    int ii;

    /* save actual mode, set GM_ADVANCED */
    oldgMode = SetGraphicsMode(hdcDest,GM_ADVANCED);
    if (oldgMode == 0)
        return FALSE;

    for (ii = 0; ii < 3; ii++) {
        plg[ii].x = lpPoint[ii].x;
        plg[ii].y = lpPoint[ii].y;
    }
    rect[0].x = nXSrc;
    rect[0].y = nYSrc;
    rect[1].x = nXSrc + nWidth;
    rect[1].y = nYSrc;
    rect[2].x = nXSrc;
    rect[2].y = nYSrc + nHeight;
    /* calc XFORM matrix to transform hdcDest -> hdcSrc (parallelogram to rectangle) */
    /* determinant */

    /* X components */
    xf.eM11 = -(rect[1].y * plg[2].x + 
                rect[0].y *(plg[1].x-plg[2].x)
               -rect[2].y * plg[1].x+plg[0].x * (rect[2].y-rect[1].y))
                /( rect[0].y * ( rect[2].x-rect[1].x) - rect[1].y * rect[2].x +
                   rect[2].y * rect[1].x + 
                   (rect[1].y-rect[2].y) * rect[0].x);

    xf.eM21 = (plg[0].x*(rect[2].x-rect[1].x)
              -plg[1].x*rect[2].x
              +plg[2].x*rect[1].x
              +(plg[1].x-plg[2].x)*rect[0].x)
              /(rect[0].y*(rect[2].x-rect[1].x)
               -rect[1].y*rect[2].x
               +rect[2].y*rect[1].x+
               (rect[1].y-rect[2].y)*rect[0].x);

    xf.eDx = -(rect[0].y*(plg[2].x*rect[1].x
               -plg[1].x*rect[2].x)
               +plg[0].x*(rect[1].y*rect[2].x
               -rect[2].y*rect[1].x)
              +(rect[2].y*plg[1].x
              -rect[1].y*plg[2].x)*rect[0].x)
              /(rect[0].y*(rect[2].x-rect[1].x)
              -rect[1].y*rect[2].x+rect[2].y
              *rect[1].x+(rect[1].y-rect[2].y)*rect[0].x);

    /* Y components */
    xf.eM12 =  -(rect[1].y*(plg[2].y-plg[0].y)+rect[0].y*(plg[1].y
                -plg[2].y)+rect[2].y*(plg[0].y-plg[1].y))/(rect[0].y*(rect[2].x
                -rect[1].x)-rect[1].y*rect[2].x+rect[2].y*rect[1].x+(rect[1].y
                -rect[2].y)*rect[0].x);

    xf.eM22 = ((plg[0].y-plg[1].y)*rect[2].x+(plg[2].y-plg[0].y)*rect[1].x
              +(plg[1].y-plg[2].y)*rect[0].x) /(rect[0].y*(rect[2].x-rect[1].x)
              -rect[1].y*rect[2].x+rect[2].y*rect[1].x+(rect[1].y-rect[2].y)
              *rect[0].x);

    xf.eDy = (rect[0].y*(plg[1].y*rect[2].x-plg[2].y*rect[1].x)
             -rect[1].y*plg[0].y*rect[2].x+rect[2].y*plg[0].y*rect[1].x
            +(rect[1].y*plg[2].y-rect[2].y*plg[1].y)*rect[0].x)/(rect[0].y
            *(rect[2].x-rect[1].x)-rect[1].y*rect[2].x+rect[2].y*rect[1].x
            +(rect[1].y-rect[2].y)*rect[0].x);

    HBITMAP  bmpTmp = NULL;
    HDC      dcTmp = NULL;
    HBITMAP  bmpTmpOld = NULL;
    if (hdcMask)
    {

        bmpTmp = ::CreateCompatibleBitmap(hdcDest, nWidth, nHeight);
        dcTmp  = ::CreateCompatibleDC(hdcDest);
        bmpTmpOld  = HBITMAP(::SelectObject(dcTmp, bmpTmp));
    
        ::BitBlt( dcTmp, 0,0, 
                    nWidth, 
                    nHeight,
                    hdcSrc, nXSrc, nYSrc, SRCCOPY);

        ::BitBlt(dcTmp,0,0,nWidth,nHeight,
                hdcMask, nXSrc,nYSrc,
                MERGEPAINT);
    }

    GetWorldTransform(hdcSrc,&SrcXf);
    CombineTransform(&xf,&xf,&SrcXf);

    /* save actual dest transform */
    GetWorldTransform(hdcDest,&oldDestXf);

    SetWorldTransform(hdcDest,&xf);

    /* now destination and source DCs use same coords */



    if (hdcMask)
    {
        BitBlt(hdcDest,nXSrc,nYSrc,nWidth,nHeight,
                hdcMask, nXSrc,nYSrc,
                SRCPAINT);

        BitBlt(hdcDest,nXSrc,nYSrc,nWidth,nHeight,
                dcTmp, 0,0,
                SRCAND);
    }
    else
    {
        BitBlt(hdcDest,nXSrc,nYSrc,nWidth,nHeight,
                hdcSrc, nXSrc,nYSrc,
                SRCCOPY);
    }
    
    /* restore dest DC */
    SetWorldTransform(hdcDest,&oldDestXf);
    SetGraphicsMode(hdcDest,oldgMode);



    if (dcTmp)
    {
        SelectObject(dcTmp, bmpTmpOld);
        ::DeleteDC(dcTmp);
    }

    if (bmpTmp)
    {
        ::DeleteObject(bmpTmp);
    }
    return TRUE;
}

BOOL WINAPI AlphaPlgBlt2( HDC hdcDest, const POINT *lpPoint,
                        HDC hdcSrc, INT nXSrc, INT nYSrc, INT nWidth,
                        INT nHeight, HDC dcMMask, INT xMask, INT yMask, BYTE btAlpha)
{
    /* we need to use floating-point precision when calculating the
     * XFORM matrix to avoid loss of precision due to rounding */
    typedef struct tagPOINTLF
    {
        double x, y;
    } POINTLF;

    int oldgMode;
    /* parallelogram coords */
    POINTLF plg[3];
    /* rect coords */
    POINTLF rect[3];
    XFORM xf;
    XFORM MaskXf;
    XFORM oldMaskXf;
    XFORM SrcXf;
    XFORM oldDestXf;
    int ii;

    /* save actual mode, set GM_ADVANCED */
    oldgMode = SetGraphicsMode(hdcDest,GM_ADVANCED);
    if (oldgMode == 0)
        return FALSE;


    POINTLF ptLT;
    POINTLF ptRB;

    ptRB.x = ptLT.x = lpPoint[0].x;
    ptRB.y = ptLT.y = lpPoint[0].y;

    for (ii = 0; ii < 3; ii++) 
    {
        plg[ii].x = lpPoint[ii].x;
        plg[ii].y = lpPoint[ii].y;

        if (ptLT.x > plg[ii].x){ptLT.x = plg[ii].x;}
        if (ptLT.y > plg[ii].y){ptLT.y = plg[ii].y;}
        if (ptRB.x < plg[ii].x){ptRB.x = plg[ii].x;}
        if (ptRB.y < plg[ii].y){ptRB.y = plg[ii].y;}
    }

    rect[0].x = nXSrc;
    rect[0].y = nYSrc;
    rect[1].x = nXSrc + nWidth;
    rect[1].y = nYSrc;
    rect[2].x = nXSrc;
    rect[2].y = nYSrc + nHeight;
    /* calc XFORM matrix to transform hdcDest -> hdcSrc (parallelogram to rectangle) */
    /* determinant */

    /* X components */
    xf.eM11 = -(rect[1].y * plg[2].x + 
                rect[0].y *(plg[1].x-plg[2].x)
               -rect[2].y * plg[1].x+plg[0].x * (rect[2].y-rect[1].y))
                /( rect[0].y * ( rect[2].x-rect[1].x) - rect[1].y * rect[2].x +
                   rect[2].y * rect[1].x + 
                   (rect[1].y-rect[2].y) * rect[0].x);

    xf.eM21 = (plg[0].x*(rect[2].x-rect[1].x)
              -plg[1].x*rect[2].x
              +plg[2].x*rect[1].x
              +(plg[1].x-plg[2].x)*rect[0].x)
              /(rect[0].y*(rect[2].x-rect[1].x)
               -rect[1].y*rect[2].x
               +rect[2].y*rect[1].x+
               (rect[1].y-rect[2].y)*rect[0].x);

    xf.eDx = -(rect[0].y*(plg[2].x*rect[1].x
               -plg[1].x*rect[2].x)
               +plg[0].x*(rect[1].y*rect[2].x
               -rect[2].y*rect[1].x)
              +(rect[2].y*plg[1].x
              -rect[1].y*plg[2].x)*rect[0].x)
              /(rect[0].y*(rect[2].x-rect[1].x)
              -rect[1].y*rect[2].x+rect[2].y
              *rect[1].x+(rect[1].y-rect[2].y)*rect[0].x);

    /* Y components */
    xf.eM12 =  -(rect[1].y*(plg[2].y-plg[0].y)+rect[0].y*(plg[1].y
                -plg[2].y)+rect[2].y*(plg[0].y-plg[1].y))/(rect[0].y*(rect[2].x
                -rect[1].x)-rect[1].y*rect[2].x+rect[2].y*rect[1].x+(rect[1].y
                -rect[2].y)*rect[0].x);

    xf.eM22 = ((plg[0].y-plg[1].y)*rect[2].x+(plg[2].y-plg[0].y)*rect[1].x
              +(plg[1].y-plg[2].y)*rect[0].x) /(rect[0].y*(rect[2].x-rect[1].x)
              -rect[1].y*rect[2].x+rect[2].y*rect[1].x+(rect[1].y-rect[2].y)
              *rect[0].x);

    xf.eDy = (rect[0].y*(plg[1].y*rect[2].x-plg[2].y*rect[1].x)
             -rect[1].y*plg[0].y*rect[2].x+rect[2].y*plg[0].y*rect[1].x
            +(rect[1].y*plg[2].y-rect[2].y*plg[1].y)*rect[0].x)/(rect[0].y
            *(rect[2].x-rect[1].x)-rect[1].y*rect[2].x+rect[2].y*rect[1].x
            +(rect[1].y-rect[2].y)*rect[0].x);


    MaskXf.eM11 = xf.eM21;
    MaskXf.eM21 = xf.eM21;
    MaskXf.eM12 = xf.eM12;
    MaskXf.eM22 = xf.eM22;
    MaskXf.eDx  = 1.0;
    MaskXf.eDy  = 1.0;

    int iBackWidth  = ptRB.x - ptLT.x;
    int iBackHeight = ptRB.y - ptLT.y;

    // マスクをカラービットマップに変換
    HBITMAP  bmpColorMask = ::CreateCompatibleBitmap(hdcDest, iBackWidth, iBackHeight);
    HDC      dcColorMask  = ::CreateCompatibleDC(hdcDest);
    HBITMAP  bmpColorOld  = HBITMAP(::SelectObject(dcColorMask, bmpColorMask));
    
    RECT rc;
    rc.top    = 0;
    rc.left   = 0;
    rc.bottom = iBackHeight;
    rc.right  = iBackWidth;
    ::FillRect(dcColorMask, &rc, HBRUSH(GetStockObject(BLACK_BRUSH)));

    
    HDC      dcBack  =  CreateCompatibleDC(NULL);
    HBITMAP  bmpBack = CreateCompatibleBitmap(hdcSrc,
                                iBackWidth,
                                iBackHeight);
    HGDIOBJ oldBack;
    oldBack = SelectObject( dcBack,  bmpBack);

    //背景を退避
    BitBlt(dcBack,0,0,nWidth,nHeight,
            hdcSrc, ptLT.x, ptLT.y,
            SRCCOPY);
    /*

    //背景にマスクを掛ける
    BitBlt(dcBack,0,0,nWidth,nHeight,
            dcColorMask, 0,0,
            SRCCOPY);
    */

 
    GetWorldTransform(hdcSrc,&SrcXf);
    CombineTransform(&xf,&xf,&SrcXf);

    CombineTransform(&MaskXf,&MaskXf,&SrcXf);

    {
        SetGraphicsMode(dcColorMask,GM_ADVANCED);
        GetWorldTransform(dcColorMask,&oldMaskXf);
        SetWorldTransform(dcColorMask,&MaskXf);


        //カラーマスクを作成する
        ::BitBlt(dcColorMask, 0, 0, iBackWidth, iBackHeight, dcMMask, 0, 0, SRCCOPY);


        SetWorldTransform(dcColorMask,&oldMaskXf);
        SetGraphicsMode(dcColorMask,oldgMode);
    }



    //退避した背景を戻す
    BitBlt(hdcDest,ptLT.x, ptLT.y, iBackWidth, iBackHeight,
            dcColorMask, 0, 0,
            SRCCOPY);



    /* save actual dest transform */
    GetWorldTransform(hdcDest,&oldDestXf);
    SetWorldTransform(hdcDest,&xf);


    /*
    //アルファブレンド
	BLENDFUNCTION bf;
	bf.AlphaFormat = 0;
	bf.BlendFlags = 0;
	bf.BlendOp = AC_SRC_OVER;
	bf.SourceConstantAlpha = btAlpha;

    AlphaBlend(
      hdcDest,
      nXSrc,
      nYSrc,
      nWidth,
      nHeight,
      hdcSrc,
      nXSrc,
      nYSrc,
      nWidth,
      nHeight,
      bf);


    //アルファブレンドの背景部をマスクする
    BitBlt(hdcDest,0,0,nWidth,nHeight,
            dcColorMask, nXSrc,nYSrc,
            MERGEPAINT);

    //退避した背景を戻す
    BitBlt(hdcDest,0,0,nWidth,nHeight,
            dcBack, 0, 0,
            SRCAND);
 
    */


    /* restore dest DC */
    SetWorldTransform(hdcDest,&oldDestXf);
    SetGraphicsMode(hdcDest,oldgMode);


    if (dcColorMask)
    {
        SelectObject(dcColorMask, bmpColorOld);
        ::DeleteDC(dcColorMask);
    }

    if (bmpColorMask)
    {
        ::DeleteObject(bmpColorMask);
    }


    if (dcBack)
    {

        SelectObject(dcBack, oldBack);
        ::DeleteDC(dcBack);
    }

    if (bmpBack)
    {
        ::DeleteObject(bmpBack);
    }

    /*
    if (dcMask)
    {
        SelectObject(dcMask, oldMask);
        ::DeleteDC(dcMask);
    }
    */

    return TRUE;
}


BOOL WINAPI AlphaPlgBlt( HDC hdcDest, const POINT *lpPoint,
                        HDC hdcSrc, INT nXSrc, INT nYSrc, INT nWidth,
                        INT nHeight, BYTE btAlpha)
{
   /* we need to use floating-point precision when calculating the
     * XFORM matrix to avoid loss of precision due to rounding */
    typedef struct tagPOINTLF
    {
        double x, y;
    } POINTLF;

    int oldgMode;
    /* parallelogram coords */
    POINTLF plg[3];
    /* rect coords */
    POINTLF rect[3];
    XFORM xf;
    XFORM SrcXf;
    XFORM oldDestXf;
    int ii;

    /* save actual mode, set GM_ADVANCED */
    oldgMode = SetGraphicsMode(hdcDest,GM_ADVANCED);
    if (oldgMode == 0)
        return FALSE;

    for (ii = 0; ii < 3; ii++) {
        plg[ii].x = lpPoint[ii].x;
        plg[ii].y = lpPoint[ii].y;
    }
    rect[0].x = nXSrc;
    rect[0].y = nYSrc;
    rect[1].x = nXSrc + nWidth;
    rect[1].y = nYSrc;
    rect[2].x = nXSrc;
    rect[2].y = nYSrc + nHeight;
    /* calc XFORM matrix to transform hdcDest -> hdcSrc (parallelogram to rectangle) */
    /* determinant */

    /* X components */
    xf.eM11 = -(rect[1].y * plg[2].x + 
                rect[0].y *(plg[1].x-plg[2].x)
               -rect[2].y * plg[1].x+plg[0].x * (rect[2].y-rect[1].y))
                /( rect[0].y * ( rect[2].x-rect[1].x) - rect[1].y * rect[2].x +
                   rect[2].y * rect[1].x + 
                   (rect[1].y-rect[2].y) * rect[0].x);

    xf.eM21 = (plg[0].x*(rect[2].x-rect[1].x)
              -plg[1].x*rect[2].x
              +plg[2].x*rect[1].x
              +(plg[1].x-plg[2].x)*rect[0].x)
              /(rect[0].y*(rect[2].x-rect[1].x)
               -rect[1].y*rect[2].x
               +rect[2].y*rect[1].x+
               (rect[1].y-rect[2].y)*rect[0].x);

    xf.eDx = -(rect[0].y*(plg[2].x*rect[1].x
               -plg[1].x*rect[2].x)
               +plg[0].x*(rect[1].y*rect[2].x
               -rect[2].y*rect[1].x)
              +(rect[2].y*plg[1].x
              -rect[1].y*plg[2].x)*rect[0].x)
              /(rect[0].y*(rect[2].x-rect[1].x)
              -rect[1].y*rect[2].x+rect[2].y
              *rect[1].x+(rect[1].y-rect[2].y)*rect[0].x);

    /* Y components */
    xf.eM12 =  -(rect[1].y*(plg[2].y-plg[0].y)+rect[0].y*(plg[1].y
                -plg[2].y)+rect[2].y*(plg[0].y-plg[1].y))/(rect[0].y*(rect[2].x
                -rect[1].x)-rect[1].y*rect[2].x+rect[2].y*rect[1].x+(rect[1].y
                -rect[2].y)*rect[0].x);

    xf.eM22 = ((plg[0].y-plg[1].y)*rect[2].x+(plg[2].y-plg[0].y)*rect[1].x
              +(plg[1].y-plg[2].y)*rect[0].x) /(rect[0].y*(rect[2].x-rect[1].x)
              -rect[1].y*rect[2].x+rect[2].y*rect[1].x+(rect[1].y-rect[2].y)
              *rect[0].x);

    xf.eDy = (rect[0].y*(plg[1].y*rect[2].x-plg[2].y*rect[1].x)
             -rect[1].y*plg[0].y*rect[2].x+rect[2].y*plg[0].y*rect[1].x
            +(rect[1].y*plg[2].y-rect[2].y*plg[1].y)*rect[0].x)/(rect[0].y
            *(rect[2].x-rect[1].x)-rect[1].y*rect[2].x+rect[2].y*rect[1].x
            +(rect[1].y-rect[2].y)*rect[0].x);

    GetWorldTransform(hdcSrc,&SrcXf);
    CombineTransform(&xf,&xf,&SrcXf);

    /* save actual dest transform */
    GetWorldTransform(hdcDest,&oldDestXf);

    SetWorldTransform(hdcDest,&xf);

    /* now destination and source DCs use same coords */

	BLENDFUNCTION bf;
	bf.AlphaFormat = 0;
	bf.BlendFlags = 0;
	bf.BlendOp = AC_SRC_OVER;
	bf.SourceConstantAlpha = btAlpha;

    BOOL bRet;
    bRet = AlphaBlend(
      hdcDest,
      nXSrc,
      nYSrc,
      nWidth,
      nHeight,
      hdcSrc,
      nXSrc,
      nYSrc,
      nWidth,
      nHeight,
      bf);

    COMMENT_ASSERT(bRet,_T("AlphaBlend"));

    /* restore dest DC */
    SetWorldTransform(hdcDest,&oldDestXf);
    SetGraphicsMode(hdcDest,oldgMode);

    return TRUE;
}

void PtintDib(HBITMAP hBmp, HDC hdc)
{
    BITMAPINFOHEADER* pBmh;
    int iBytes;

    DB_PRINT(_T("\n"));
    DB_PRINT(_T("HBITMAP           : %x\n"), hBmp);

    DIBSECTION  dib;
    iBytes = ::GetObject( hBmp, sizeof(DIBSECTION), &dib);

    int iWidth;
    int iHeight;
    int iBpp;
    int iPich;
    void* pBits;

	if( iBytes == sizeof( DIBSECTION ) )
	{
        //DIB
        pBmh = &dib.dsBmih;
        DB_PRINT(_T("biSize           : %d\n"), pBmh->biSize);
        DB_PRINT(_T("biWidth          : %d\n"), pBmh->biWidth);
        DB_PRINT(_T("biHeight         : %d\n"), pBmh->biHeight);
        DB_PRINT(_T("biPlanes         : %d\n"), pBmh->biPlanes);
        DB_PRINT(_T("biBitCount       : %d\n"), pBmh->biBitCount);
        DB_PRINT(_T("biCompression    : %d\n"), pBmh->biCompression);
        DB_PRINT(_T("biSizeImage      : %d\n"), pBmh->biSizeImage);
        DB_PRINT(_T("biXPelsPerMeter  : %d\n"), pBmh->biXPelsPerMeter);
        DB_PRINT(_T("biYPelsPerMeter  : %d\n"), pBmh->biYPelsPerMeter);
        DB_PRINT(_T("biClrUsed        : %d\n"), pBmh->biClrUsed);
        DB_PRINT(_T("biClrImportant   : %d\n"), pBmh->biClrImportant);

        iWidth      =    pBmh->biWidth;
        iHeight     =    pBmh->biHeight;
        iBpp        =    pBmh->biBitCount;
        iPich       =    CImageData::CalcPitch(iWidth, iBpp);

    }
    else
    {
        BITMAP bitmap;

        iBytes = ::GetObject( hBmp, sizeof(BITMAP), &bitmap);

        DB_PRINT(_T("bmType         : %d\n"), bitmap.bmType);
        DB_PRINT(_T("bmWidth        : %d\n"), bitmap.bmWidth);
        DB_PRINT(_T("bmHeight       : %d\n"), bitmap.bmHeight);
        DB_PRINT(_T("bmWidthBytes   : %d\n"), bitmap.bmWidthBytes);
        DB_PRINT(_T("bmPlanes       : %d\n"), bitmap.bmPlanes);
        DB_PRINT(_T("bmBitsPixel    : %d\n"), bitmap.bmBitsPixel);

        iWidth = bitmap.bmWidth; 
        iHeight = bitmap.bmHeight; 
        iBpp    = bitmap.bmBitsPixel;

        //DDB
        COLORREF cr;
        for (int iY = 0; iY <  iHeight; iY++)
        {
            for (int iX = 0; iX <  iWidth; iX++)
            {

                cr = GetPixel(hdc, iX, iY);

                if (iBpp == 1)
                {
                    DB_PRINT( _T("%d"), cr == 0 ? 0 :1 );
                }
                else
                {
                    DB_PRINT( _T("%08x "), cr);
                }
            }
            DB_PRINT( _T("\n"));
        }
        return;
    }


    if (pBmh->biWidth <= 1 )
    {
        void* pBits     =    dib.dsBm.bmBits;

        BYTE* pLineHead;

        DWORD pixelsPerDW;  // DWORD 内の画素数
        if (iBpp <= 1)
        {
            for (int iY = 0; iY <  iHeight; iY++)
            {
                pLineHead = (BYTE*)(pBits) + iY * iPich;
                int iBitWidth = 1;

                iBitWidth = iWidth / 8;
                for (int iX = 0; iX <  iBitWidth; iX++)
                {
                    BYTE* pVal = (BYTE*)(pLineHead +iX);
                    std::stringstream ss;
                    ss << static_cast<std::bitset<8> >(*pVal);
                    DB_PRINT( _T("%s "), ss.str().c_str());
                }
                DB_PRINT( _T("\n"));
            }

        }
        else if (iBpp <= 4)
        {
            pixelsPerDW = 8 * sizeof(DWORD) / 4;



        }
        else if (iBpp <= 8)
        {
            pixelsPerDW = 8 * sizeof(DWORD) / 8;



        }
        else
        {
            DWORD* pVal;
            int idev = INT_MAX;
            for (int iY = 0; iY <  iHeight; iY++)
            {
                pLineHead = (BYTE*)(pBits) + iY * iPich;
                for (int iX = 0; iX <  iWidth; iX++)
                {
                    pVal = (DWORD*)(pLineHead + ((iX*iBpp)/8));

                    DB_PRINT( _T("%08x "), *pVal);
                }
                DB_PRINT( _T("\n"));
            }
        }
    }
    DB_PRINT(_T("\n"));
}


/**
 *  @brief   コンストラクタ
 *  @param   なし
 *  @retval  なし
 *  @note
 */
CImageData::CImageData():
m_dAngle        (0),
m_maskDc        (0),
m_dSclX         (1.0),
m_dSclY         (1.0),
m_bChg          (false),
m_maskBitmap    (0),
m_hDc           (0),
m_hBitmap       (0),
m_hOldBitmap    (0),
m_hOldMaskBitmap(0),
m_dcAlpha       (0),
m_hAlphaBitmap  (0),
m_bMirror       (false),
m_dwTransmiittance(255),
m_iBitMapRefarenceCnt(0),
m_bUseFile           (false),
m_pPartsDef(NULL)

{
    m_hDc = 0;
    memset(&m_bmpInfo, 0, sizeof(m_bmpInfo));
    
    m_Prop.m_iXDiv         = 1;
    m_Prop.m_iYDiv         = 1;
    m_Prop.m_iDpi          = 0;
    m_Prop.m_eTranspaent   = E_TRP_NONE;
    m_Prop.m_crTransparent = RGB(0, 128, 128);
    m_Prop.m_ptOffset.dX = 0.0;
    m_Prop.m_ptOffset.dY = 0.0;
    m_ptLT.x = 0;
    m_ptLT.y = 0;
    m_ptRB.x = 0;
    m_ptRB.y = 0;
}

CImageData::CImageData(const CImageData & obj)
{
    *this = obj;
}


/**
 *  @brief   デストラクタ
 *  @param   なし
 *  @retval  なし
 *  @note
 */
CImageData::~CImageData()
{
    if (m_iBitMapRefarenceCnt == 0)
    {
        DeleteBitmap();
        DeleteBitmask();
        DeleteAlpha();
    }
}


/**
 * @brief   ビットマスク削除
 * @param   なし
 * @retval  なし
 * @note	
 */
void CImageData::DeleteBitmask()
{
    if (m_maskDc)
    {
        HBITMAP     hBitmap;
        hBitmap = HBITMAP(::SelectObject( m_maskDc,  m_hOldMaskBitmap));
        ::DeleteDC(m_maskDc);
        m_maskDc = 0;
    }

    if (m_maskBitmap)
    {
        ::DeleteObject(m_maskBitmap);
        m_maskBitmap = 0;
    }
}


/**
 * @brief   アルファブレンド用マップ削除
 * @param   なし
 * @retval  なし
 * @note	
 */
void CImageData::DeleteAlpha()
{
    if (m_dcAlpha)
    {
        ::DeleteDC(m_dcAlpha);
        m_dcAlpha = 0;
    }

    if (m_hAlphaBitmap)
    {
        ::DeleteObject(m_hAlphaBitmap);
        m_hAlphaBitmap = 0;
    }
}


/**
 * @brief   ビットマップ削除
 * @param   なし
 * @retval  なし
 * @note	
 */
void CImageData::DeleteBitmap()
{
    ReturnDC();
    if (m_hBitmap)
    {
        DeleteObject(m_hBitmap);
        m_hBitmap = 0;
    }
}

/**
 *  @brief   代入演算子
 *  @param   [in] obj 代入元 
 *  @retval  代入値
 *  @note    通常使用することはないが
 *           CElementDef  std::vector<CImageData> Load時
 *           内部で CImageData生成-> push_back が行われる
 *           と思われる。 その時デストラクタでビットマップを
 *           削除してしまうとvector内のビットマップも無効になって
 *           しまうためここで参照カウントもどきを設ける
 */
CImageData& CImageData::operator = (const CImageData & obj)
{
    m_dAngle        = obj.m_dAngle;
    m_dSclX         = obj.m_dSclX;
    m_dSclY         = obj.m_dSclY;
    m_maskDc        = obj.m_maskDc;
    m_maskBitmap    = obj.m_maskBitmap;
    m_hDc           = obj.m_hDc;
    m_hBitmap       = obj.m_hBitmap;
    m_bmpInfo       = obj.m_bmpInfo;
    m_pPixel        = obj.m_pPixel;
    m_bMirror       = obj.m_bMirror;
    m_point[0]      = obj.m_point[0];
    m_point[1]      = obj.m_point[1];
    m_point[2]      = obj.m_point[2];
    m_bChg          = obj.m_bChg;
    m_Prop          = obj.m_Prop;
    m_bUseFile      = obj.m_bUseFile;   

    m_strFileName         = obj.m_strFileName;
    m_dwTransmiittance    = obj.m_dwTransmiittance;
    m_iBitMapRefarenceCnt = obj.m_iBitMapRefarenceCnt;

    //load時のpush_backのみ想定
    STD_ASSERT(m_iBitMapRefarenceCnt == 0);
    obj.m_iBitMapRefarenceCnt++;

    return *this;
}


/**
 * @brief   ファイル読み込み
 * @param   [in] path  ファイルパス
 * @retval  true 成功
 * @note	
 */
bool CImageData::LoadFile(StdPath path)
{
    CImage      image;

    HRESULT hRes;
    hRes = image.Load(GetPathStr(path).c_str());

    if (hRes != S_OK)
    {
        return false;
    }

    if(!Create(image.GetWidth(), image.GetHeight(), 24))
    {
        return false;
    }

    HDC hDc = RentalDC();
    image.Draw( hDc, 0, 0 );
    CreateBitmask(hDc);
    ReturnDC();

    m_Prop.m_ptOffset.dX = image.GetWidth() / 2.0;
    m_Prop.m_ptOffset.dY = image.GetHeight() / 2.0;

    CalcPoint();

    m_Prop.m_iDpi = DRAW_CONFIG->dDpi;

    m_bUseFile = true;
    return true;
}


/**
 * @brief   ビットマップ編集
 * @param   なし
 * @retval  true 成功
 * @note	テスト用
 */
bool CImageData::Edit()
{

    if (m_hDc)
    {
        // m_hBitmap が selectされている状態では
        // pBitmapに書き込めない
        ReturnDC();
    }

    CBitmap* pBitmap;
    pBitmap = CBitmap::FromHandle(m_hBitmap);

    CMFCImageEditorDialog dlg(pBitmap, NULL);
    if (dlg.DoModal() == IDOK)
	{
        HDC hDc = RentalDC();
        CreateBitmask(hDc);
        ReturnDC();

        return true;
    }
    return false;
}

/**
 * @brief   透過有無設定
 * @param   [in] bTransparent 透過          
 * @retval  なし
 * @note	
 */
void CImageData::SetUseTranparent(E_TRNSPAENT_TYPE eTransparent)
{
    if (m_Prop.m_eTranspaent != eTransparent)
    {
        m_Prop.m_eTranspaent = eTransparent;
        HDC hDc = RentalDC();
        CreateBitmask(hDc);
        ReturnDC();

        m_bChg = true;
    }
}

/**
 * @brief   透過色設定
 * @param   [in] crTransparent 透過色          
 * @retval  なし
 * @note	
 */
void CImageData::SetTranparentColor(COLORREF crTransparent)
{
    if (m_Prop.m_crTransparent != crTransparent)
    {
        m_Prop.m_crTransparent = crTransparent;
        m_bChg = true;

        if (m_Prop.m_eTranspaent )
        {
            HDC hDc = RentalDC();
            CreateBitmask(hDc);
            ReturnDC();
        }
    }
}

/**
 * @brief   オフセット設定
 * @param   [in] pt オフセット          
 * @retval  なし
 * @note	
 */
void CImageData::SetOffset(POINT2D pt)
{
    if (m_Prop.m_ptOffset.Distance(pt) < NEAR_ZERO)
    {
        m_Prop.m_ptOffset = pt;
        m_bChg = true;
    }
}

/**
 * @brief   オフセット設定
 * @param   [in] pt オフセット          
 * @retval  なし
 * @note	
 */
POINT2D CImageData::GetOffset() const
{
    return m_Prop.m_ptOffset;
}

/**
 * @brief   回転角設定
 * @param   [in] dAngle 回転角
 * @retval  なし
 * @note	
 */
void CImageData::SetAngle(double dAngle)
{
    if ( fabs(m_dAngle-dAngle) > NEAR_ZERO)
    {
        m_dAngle = dAngle;
        m_bChg = true;
    }
}

/**
 * @brief   倍率設定
 * @param   [in] dScl 倍率
 * @retval  なし
 * @note	
 */
void CImageData::SetScl(double dScl)
{
    SetScl(dScl, dScl);
}

/**
 * @brief   倍率設定
 * @param   [in] dSclX 倍率
 * @param   [in] dSclY 倍率
 * @retval  なし
 * @note	
 */
void CImageData::SetScl(double dSclX, double dSclY)
{
    if ( fabs(m_dSclX-dSclX) > NEAR_ZERO)
    {
        m_dSclX = dSclX;
        m_bChg = true;
    }
 
    if ( fabs(m_dSclY-dSclY) > NEAR_ZERO)
    {
        m_dSclY = dSclY;
        m_bChg = true;
    }
}

/**
 * @brief   透過率設定
 * @param   [in] dScl 倍率
 * @retval  なし
 * @note	現状未使用
 */
void CImageData::SetTransmittance(DWORD dwTrans)
{
    if ( (m_dwTransmiittance - dwTrans) != 0)
    {
        m_dwTransmiittance = dwTrans;
        m_bChg = true;
    }
}

/**
 * @brief   デバイスコンテキスト取得
 * @param   なし
 * @retval  なし
 * @note    ビットマップ用メモリDC取得
 *          m_hDc がある時は常にBitMapを選択
 *          した状態にしておく
 */
HDC CImageData::RentalDC()
{
    if (!m_hDc)
    {
        m_hDc =  ::CreateCompatibleDC(NULL);
        m_hOldBitmap = HBITMAP(::SelectObject( m_hDc,  m_hBitmap));
    }

    return m_hDc;
}

/**
 * @brief   デバイスコンテキスト開放
 * @param   なし
 * @retval  なし
 * @note   
 */
 void CImageData::ReturnDC()
{
    if (!m_hDc)
    {
        return;
    }

    HBITMAP     hBitmap;
    hBitmap = HBITMAP(::SelectObject( m_hDc,  m_hOldBitmap));
    ::DeleteDC(m_hDc);
    m_hDc = NULL;
}




/**
 * @brief   ビットマスク作成
 * @param   [in] hDc デバイスコンテキスト
 * @retval  なし
 * @note	
 */
void CImageData::CreateBitmask(HDC hDc)
{
    DeleteBitmask();


    if (m_Prop.m_eTranspaent != E_TRP_NONE)
    {
        COLORREF crTranspaent = 0;
        HDC hdc = RentalDC();


        if (m_Prop.m_eTranspaent == E_TRP_COLOR)
        {
            crTranspaent = m_Prop.m_crTransparent;
        }
        else if (m_Prop.m_eTranspaent == E_TRP_LT)
        {
            crTranspaent = ::GetPixel(hdc, 0,0);;
        }
        else if (m_Prop.m_eTranspaent == E_TRP_RB)
        {
            crTranspaent = ::GetPixel(hdc, m_bmpInfo.bmiHeader.biWidth - 1,
                                           m_bmpInfo.bmiHeader.biHeight - 1);
        }


        COLORREF oldBkColor = SetBkColor( hDc, crTranspaent);

        m_maskDc =  ::CreateCompatibleDC(NULL);
        m_maskBitmap = ::CreateBitmap(m_bmpInfo.bmiHeader.biWidth,
                                      m_bmpInfo.bmiHeader.biHeight, 1,1, NULL);

        m_hOldMaskBitmap = HBITMAP(::SelectObject( m_maskDc,  m_maskBitmap));

        ::BitBlt( m_maskDc, 0,0, 
                  m_bmpInfo.bmiHeader.biWidth, 
                  m_bmpInfo.bmiHeader.biHeight,
                  hDc, 0,0, NOTSRCCOPY);

        ::SetBkColor(hDc,oldBkColor);
    }

    //PtintDib(m_maskBitmap, m_maskDc);
}


void CImageData::CreateAlpha(HDC hDc)
{
    //TODO:キャッシュ化
    DeleteAlpha();

    int iWidth  = m_ptRB.x - m_ptLT.x;
    int iHeight = m_ptRB.y - m_ptLT.y;


    m_dcAlpha =  CreateCompatibleDC(NULL);
    m_hAlphaBitmap = CreateCompatibleBitmap(hDc,
                                iWidth,
                                iHeight);

    SelectObject( m_dcAlpha,  m_hAlphaBitmap);
}




//!< X分割数設定
void CImageData::SetXdiv(int iNum)
{
    if (iNum < 1)
    {
        m_Prop.m_iXDiv = 1;
    }
    else
    {
        m_Prop.m_iXDiv = iNum;
    }

     m_Prop.m_ptOffset.dX = GetWidth() / 2.0;

}

//!< X分割数取得
int CImageData::GetXdiv() const
{
    return  m_Prop.m_iXDiv;
}

//!< Y分割数設定
void CImageData::SetYdiv(int iNum)
{
    if (iNum < 1)
    {
         m_Prop.m_iYDiv = 1;
    }
    else
    {
         m_Prop.m_iYDiv = iNum;
    }

     m_Prop.m_ptOffset.dY = GetHeight() / 2.0;

}

//!< Y分割数取得
int CImageData::GetYdiv() const
{
    return m_Prop.m_iYDiv;
}

//!< 総分割数
int CImageData::Getdiv() const
{
    return m_Prop.m_iXDiv * m_Prop.m_iYDiv;
}

//!< DPI設定
void CImageData::SetDpi(int iDpi)
{
    m_Prop.m_iDpi = iDpi;
}

//!< DPI取得
int CImageData::GetDpi() const
{
    return m_Prop.m_iDpi;
}

//!< 鏡像設定
void CImageData::SetMirror(bool bMirror)
{
    m_bMirror = bMirror;
}

//!< 鏡像取得
bool CImageData::GetMirror() const
{
    return m_bMirror;
}

/**
 * @brief   頂点計算
 * @param   なし
 * @retval  なし
 * @note	
 */
void CImageData::CalcFramePoint(std::vector<POINT2D>* lstPt) const
{
    lstPt->resize(4);

    POINT2D ptCenter;

    double dW = GetWidth();
    double dH = GetHeight();

    //画面座標系
    //左上  
    lstPt->at(0).dX =  - CUtil::Round(m_Prop.m_ptOffset.dX * m_dSclX);
    lstPt->at(0).dY =  - CUtil::Round(m_Prop.m_ptOffset.dY * m_dSclY);

    //右上
    lstPt->at(1).dX = lstPt->at(0).dX + dW * m_dSclX;
    lstPt->at(1).dY = lstPt->at(0).dY;

    //左下
    lstPt->at(2).dX = lstPt->at(0).dX;
    lstPt->at(2).dY = lstPt->at(0).dY + dH * m_dSclY;

    //右下
    lstPt->at(3).dX = lstPt->at(1).dX;
    lstPt->at(3).dY = lstPt->at(2).dY;

    ptCenter.dX = 0.0;
    ptCenter.dY = 0.0;

    lstPt->at(0).Rotate(ptCenter, -m_dAngle);
    lstPt->at(1).Rotate(ptCenter, -m_dAngle);
    lstPt->at(2).Rotate(ptCenter, -m_dAngle);
    lstPt->at(3).Rotate(ptCenter, -m_dAngle);
}

void CImageData::CalcPoint()
{
    std::vector<POINT2D> lstPt;
    CalcFramePoint(&lstPt);
    
    m_point[0].x = CUtil::Round(lstPt[0].dX);
    m_point[0].y = CUtil::Round(lstPt[0].dY);

    m_ptLT = m_ptRB = m_point[0];

    m_point[1].x = CUtil::Round(lstPt[1].dX);
    m_point[1].y = CUtil::Round(lstPt[1].dY);

    m_point[2].x = CUtil::Round(lstPt[2].dX);
    m_point[2].y = CUtil::Round(lstPt[2].dY);

    m_point[3].x = CUtil::Round(lstPt[3].dX);
    m_point[3].y = CUtil::Round(lstPt[3].dY);



    for (int i = 1 ; i < 4 ; i++)
    {
        if (m_ptLT.x > m_point[i].x){m_ptLT.x = m_point[i].x;}
        if (m_ptLT.y > m_point[i].y){m_ptLT.y = m_point[i].y;}
        if (m_ptRB.x < m_point[i].x){m_ptRB.x = m_point[i].x;}
        if (m_ptRB.y < m_point[i].y){m_ptRB.y = m_point[i].y;}
    }
    m_bChg = false;
}


/**
 * @brief   描画
 * @param   [in] hDestDC デバイスコンテキスト
 * @param   [in] pt      描画中心
 * @retval  なし
 * @note	
 */
void CImageData::Draw(HDC hDestDC, POINT pt, int iNo) 
{
    //bool bRet;
    if (m_bChg)
    {
        CalcPoint();
    }

    POINT       point[3];

    if (iNo > Getdiv())
    {
        iNo = Getdiv() - 1;
    }

    POINT ptOffset;

    int iRow = iNo / GetXdiv();
    int iCol= iNo - (iRow *  GetXdiv());

    ptOffset.x = iCol * GetWidth();
    ptOffset.y = iRow * GetHeight();

    int iW = GetWidth();
    int iH = GetHeight();
    
    if (iW > DRAW_CONFIG->iImageXMax)
    {
        return;
    }

    if (iH > DRAW_CONFIG->iImageYMax)
    {
        return;
    }


    HDC hDC;
    hDC = RentalDC();

    BOOL bResult;


    point[0] = CUtil::AddPoint (m_point[0], pt);
    point[1] = CUtil::AddPoint (m_point[1], pt);
    point[2] = CUtil::AddPoint (m_point[2], pt);

    if(CUtil::EqPoint(point[0],point[1]))
    {
        return;
    }

    if(CUtil::EqPoint(point[1],point[2]))
    {
        return;
    }

    if(CUtil::EqPoint(point[0],point[2]))
    {
        return;
    }

    HDC         maskDc = NULL;
    if (m_Prop.m_eTranspaent != E_TRP_COLOR)
    {
        maskDc = m_maskDc;
    }

    if(m_dwTransmiittance >= 255)
    {
	    bResult = ::PlgBlt2( hDestDC, 
                            &point[0], 
                            hDC, 
                            ptOffset.x,
                            ptOffset.y,           //左上の点
                            iW, 
                            iH, 
                            maskDc,
                            ptOffset.x,
                            ptOffset.y);
    }
    else
    {
        BYTE af = static_cast<BYTE>(m_dwTransmiittance);
	    bResult = ::AlphaPlgBlt( hDestDC, 
                            &point[0], 
                            hDC, 
                            ptOffset.x,
                            ptOffset.y,           //左上の点
                            iW, 
                            iH,
                            af);

    }

    
    if (!bResult)
    {
        //StdString strError = CUtil::GetErrMsg();
        //STD_DBG(_T("%s"), strError.c_str());
    }
    ReturnDC();
    //STD_ASSERT(bResult);
}
//!< 描画
void CImageData::Draw(HDC hDestDC, POINT pt,
            int    iNo,
            double dSclX,
            double dSclY,
            double dAngle,
            DWORD dwTrans)
{
    //現状 dTrans は未使用
    SetOffset(m_Prop.m_ptOffset);
    SetAngle(dAngle);
    SetScl(dSclX, dSclY);
    SetTransmittance(dwTrans);

    Draw(hDestDC, pt, iNo) ;

}

void CImageData::DrawRect(HDC hDestDC, POINT pt) 
{
    if (m_bChg)
    {
        CalcPoint();
    }

    POINT       point[4];
    point[0] = CUtil::AddPoint (m_point[0], pt);
    point[1] = CUtil::AddPoint (m_point[1], pt);
    point[2] = CUtil::AddPoint (m_point[3], pt);
    point[3] = CUtil::AddPoint (m_point[2], pt);


    ::MoveToEx(hDestDC, point[0].x, point[0].y, NULL);
    ::LineTo  (hDestDC, point[1].x, point[1].y);
    ::LineTo  (hDestDC, point[2].x, point[2].y);
    ::LineTo  (hDestDC, point[3].x, point[3].y);
    ::LineTo  (hDestDC, point[0].x, point[0].y);

}


void CImageData::GetFramePoints(
    double dSclX,
    double dSclY,
    double dAngle,
    std::vector<POINT2D>* pList)
{
    SetAngle(dAngle);
    SetScl(dSclX, dSclY);
    CalcFramePoint(pList);
}


void CImageData::DrawRect(HDC hDestDC, POINT pt,
                                double dSclX,
                                double dSclY,
                                double dAngle)
{
    //現状 dTrans は未使用
    SetAngle(dAngle);
    SetScl(dSclX, dSclY);
    DrawRect(hDestDC, pt) ;
}

/**
 * @brief   描画
 * @param   [in] hDestDC デバイスコンテキスト
 * @param   [in] pt      描画中心
 * @param   [in] crFill  塗りつぶし色
 * @retval  なし
 * @note	
 */
void CImageData::DrawFill(HDC hDestDC, POINT pt, COLORREF crFill)
{
    bool bRet;
    if (m_bChg)
    {
        CalcPoint();
    }

    POINT       point[3];
    point[0] = CUtil::AddPoint (m_point[0], pt);
    point[1] = CUtil::AddPoint (m_point[1], pt);
    point[2] = CUtil::AddPoint (m_point[2], pt);

    RECT rcFill;
    rcFill.top      = 0;
    rcFill.bottom   = GetHeight();
    rcFill.left     = 0;
    rcFill.right    = GetWidth();

    HDC     dcFill =  CreateCompatibleDC(NULL);
    HBITMAP bmpFill = CreateCompatibleBitmap(hDestDC,
                                GetWidth(),
                                GetHeight());

    SelectObject( dcFill,  bmpFill);


    HBRUSH brFill = ::CreateSolidBrush(crFill);

    ::FillRect(dcFill, &rcFill, brFill);

    bRet = (::PlgBlt( hDestDC, &point[0], 
                    dcFill,
                    0, 0, 
                    GetWidth(),
                    GetHeight(),
                    m_maskBitmap, 0, 0) == TRUE);


    if (!bRet)
    {
        StdString strMask = _T("Mask OK");
        StdString strDest = _T("Dest OK");
        StdString strFill = _T("Fill OK");

        CDC* pDest;
        pDest = CDC::FromHandle (hDestDC);
        if (!pDest)
        {
            strDest = _T("Dest NG");
        }

        CDC* pFill;
        pFill = CDC::FromHandle (hDestDC);
        if (!pFill)
        {
            strFill = _T("Fill NG");
        }

        DIBSECTION dib;
        int iBytes = ::GetObject( m_maskBitmap, sizeof(DIBSECTION), &dib);
        if( iBytes != sizeof( DIBSECTION ) )
        {
            strMask = _T("Mask NG");
        }

        STD_DBG(_T("%s:%s:%s:Width %d:Height %d:[%d,%d]"), strDest.c_str(),
                                                   strFill.c_str(),
                                                   strMask.c_str(),
                                                   GetWidth(),
                                                   GetHeight(),
                                                   point[0].x,
                                                   point[0].y
                                                   );
    }
    //STD_ASSERT(bRet);

    ::DeleteObject(brFill);
    ::DeleteObject(bmpFill);
    ::DeleteDC(dcFill);


}

void CImageData::DrawFill(HDC hDestDC, POINT pt,
                                COLORREF crFill,
                                double dSclX,
                                double dSclY,
                                double dAngle)
{
    //現状 dTrans は未使用
    SetAngle(dAngle);
    SetScl(dSclX, dSclY);
    DrawFill(hDestDC, pt, crFill);
}


/**
 * @brief   ビットマップ生成
 * @param   [in] iWidth
 * @param   [in] iHeight
 * @param   [in] iBitCnt
 * @retval  true 成功
 * @note	
 */
bool CImageData::Create(int iWidth, int iHeight, int iBitCnt)
{
    if ((iBitCnt != 32)&&
        (iBitCnt != 24))
    {
        return false;
    }

    DeleteBitmap();

    memset( &m_bmpInfo, 0, sizeof( m_bmpInfo ) );

    m_bmpInfo.bmiHeader.biSize  = sizeof (BITMAPINFOHEADER);
    m_bmpInfo.bmiHeader.biWidth     = iWidth;
    m_bmpInfo.bmiHeader.biHeight    = iHeight;
    m_bmpInfo.bmiHeader.biPlanes    = 1;
    m_bmpInfo.bmiHeader.biBitCount  = iBitCnt; 
    m_bmpInfo.bmiHeader.biCompression	= BI_RGB;

    int iPich = CalcPitch(iWidth, iBitCnt);
    int iSize = iHeight * abs(iPich);
    //m_lstBit.resize(iSize);
    //BYTE* pPixel = &m_lstBit[0];
 
    m_hBitmap = ::CreateDIBSection(
              NULL,                     // デバイスコンテキストのハンドル
              &m_bmpInfo,               // ビットマップデータ
              DIB_RGB_COLORS,           // データ種類のインジケータ
              (void**) &m_pPixel,       // ビット値
              NULL,                     // ファイルマッピングオブジェクトのハンドル
              0);                       // ビットマップのビット値へのオフセット

    if (m_hBitmap)
    {
        return true;
    }
    m_bUseFile = false;
    return false;
}

/**
 * @brief   ピッチ計算
 * @param   [in]   iWidth
 * @param   [in]   iBitCnt
 * @retval  ビットマップ一列の大きさ
 * @note    ビットマップの幅を４バイト境界に合わせる
 */
int CImageData::CalcPitch(int iWidth, int iBitCnt)
{
    int iPitch = ( (((iWidth * iBitCnt)+31)/32)*4 );
    return iPitch;
}

int  CImageData::GetWidth() const
{
    double dRet;
    dRet = double(m_bmpInfo.bmiHeader.biWidth) / double(m_Prop.m_iXDiv);

    int    iRet;
    iRet  = int(floor(dRet));
    return iRet;
}

int  CImageData::GetHeight() const
{
    double dRet;
    dRet = double(m_bmpInfo.bmiHeader.biHeight)/double(m_Prop.m_iYDiv);

    int    iRet;
    iRet  = int(floor(dRet));
    return iRet;
}

void CImageData::SetProp(CImagePropData Prop)
{
    SetOffset(Prop.m_ptOffset);
    SetXdiv(Prop.m_iXDiv);
    SetYdiv(Prop.m_iYDiv);
    SetDpi(Prop.m_iDpi);
    SetUseTranparent(Prop.m_eTranspaent);
    SetTranparentColor(Prop.m_crTransparent);
}

CImagePropData CImageData::GetProp() const
{
    return m_Prop;
}



void CImageData::SetFileName(StdString   strFileName)
{
    m_strFileName = strFileName;
}

StdString CImageData::GetFileName() const
{
    return m_strFileName;
}

/*
void CImageData::SetPartsDef(CPartsDef*  pPartsDef)
{
    m_pPartsDef = pPartsDef;
}
*/

void CImageData::LoadAfter()
{
/*
    if (m_bUseFile)
    {
        if(m_pPartsDef)
        {
            m_pPartsDef->GetCurrentPath();
            StdPath path(m_pPartsDef->GetCurrentPath());
            path /= m_strFileName;
            LoadFile(path);
         }
    }
*/
}

/**
 * @brief   シリアル化(ロード)
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  なし
 * @note	
 */
template<class Archive>
void CImageData::load(Archive& ar, const unsigned int version)
{
    SERIALIZATION_INIT;


        /*
        SERIALIZATION_LOAD("Angle"        , m_dAngle);
        SERIALIZATION_LOAD("SclX"         , m_dSclX);
        SERIALIZATION_LOAD("SclY"         , m_dSclY);
        */
    
        SERIALIZATION_LOAD("USeFile"      , m_bUseFile);
        SERIALIZATION_LOAD("Offset"       , m_Prop.m_ptOffset);
        SERIALIZATION_LOAD("DivX"         , m_Prop.m_iXDiv);
        SERIALIZATION_LOAD("DivY"         , m_Prop.m_iYDiv);
        SERIALIZATION_LOAD("Dpi"          , m_Prop.m_iDpi);
        SERIALIZATION_LOAD("UseTranparent" , m_Prop.m_eTranspaent);
        SERIALIZATION_LOAD("TranparentColor" , m_Prop.m_crTransparent);

        if (m_bUseFile)
        {
            SERIALIZATION_LOAD("FileName" , m_strFileName);
            //LoadAfterで読み込み
        }
        else
        {
            SERIALIZATION_LOAD("FileName" , m_strFileName);

            int iWidth;
            int iHeight;
            int iBpp;
            SERIALIZATION_LOAD("Width"        , iWidth);
            SERIALIZATION_LOAD("Height"       , iHeight);
            SERIALIZATION_LOAD("BitsPixel"    , iBpp);
            int iPich = CalcPitch(iWidth, iBpp);
            int iSize = iHeight * abs(iPich);

            Create(iWidth, iHeight, iBpp);
            std::vector<BYTE> lstMem;
            lstMem.resize(iSize);

            SERIALIZATION_LOAD("bmBits"   ,
                  boost::serialization::make_binary_object((void*)&lstMem[0], iSize));

            memcpy(m_pPixel, &lstMem[0], iSize);
        }

        HDC hDc = RentalDC();
        CreateBitmask(hDc);
        ReturnDC();
        CalcPoint();

    MOCK_EXCEPTION_FILE(e_file_read);
}


/**
 * @brief   シリアル化(セーブ)
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  なし
 * @note	
 */
template<class Archive>
void CImageData::save(Archive& ar, const unsigned int version) const
{
    SERIALIZATION_INIT;
        const BITMAPINFOHEADER* pHeader;

        pHeader = &m_bmpInfo.bmiHeader;
        //------
        //BITMAP
        //------
        /*
        SERIALIZATION_SAVE("Angle"        , m_dAngle);
        SERIALIZATION_SAVE("SclX"         , m_dSclX);
        SERIALIZATION_SAVE("SclY"         , m_dSclY);
        */
        SERIALIZATION_SAVE("USeFile"      , m_bUseFile);
        SERIALIZATION_SAVE("Offset"       , m_Prop.m_ptOffset);
        SERIALIZATION_SAVE("DivX"         , m_Prop.m_iXDiv);
        SERIALIZATION_SAVE("DivY"         , m_Prop.m_iYDiv);
        SERIALIZATION_SAVE("Dpi"          , m_Prop.m_iDpi);
        SERIALIZATION_SAVE("UseTranparent" , m_Prop.m_eTranspaent);
        SERIALIZATION_SAVE("TranparentColor" , m_Prop.m_crTransparent);
        if (m_bUseFile)
        {
            SERIALIZATION_SAVE("FileName" , m_strFileName);
        }
        else
        {
            SERIALIZATION_SAVE("FileName" , m_strFileName);
            SERIALIZATION_SAVE("Width"       , pHeader->biWidth);
            SERIALIZATION_SAVE("Height"      , pHeader->biHeight);
            SERIALIZATION_SAVE("BitsPixel"   , pHeader->biBitCount);
            int iSize;
            int iPitch;
            iPitch = CalcPitch(pHeader->biWidth, pHeader->biBitCount);
            iSize = pHeader->biHeight * abs(iPitch);


            std::vector<BYTE> lstMem;
            lstMem.resize(iSize);
            memcpy(&lstMem[0], m_pPixel, iSize);

            SERIALIZATION_SAVE("bmBits"   ,
                  boost::serialization::make_binary_object((void*)&lstMem[0], iSize));
        }

    MOCK_EXCEPTION_FILE(e_file_write);
}

/**
 * @brief   デバッグ用プリント
 * @param   ar          
 * @param   version     バージョン番号
 * @retval  なし
 * @note	
 */
void CImageData::Ptint()
{
    // HBITMAP hBmp;
    //DIBSECTION dib;
    //hBmp = m_Image;

    /*
    if(!::GetObject( hBmp, sizeof(DIBSECTION), &dib)) 
    {
        return;
    }
    */

    BITMAPINFOHEADER* pBmh;
    pBmh = &m_bmpInfo.bmiHeader;


    DB_PRINT(_T("biSize           : %d\n"), pBmh->biSize);
    DB_PRINT(_T("biWidth          : %d\n"), pBmh->biWidth);
    DB_PRINT(_T("biHeight         : %d\n"), pBmh->biHeight);
    DB_PRINT(_T("biPlanes         : %d\n"), pBmh->biPlanes);
    DB_PRINT(_T("biBitCount       : %d\n"), pBmh->biBitCount);
    DB_PRINT(_T("biCompression    : %d\n"), pBmh->biCompression);
    DB_PRINT(_T("biSizeImage      : %d\n"), pBmh->biSizeImage);
    DB_PRINT(_T("biXPelsPerMeter  : %d\n"), pBmh->biXPelsPerMeter);
    DB_PRINT(_T("biYPelsPerMeter  : %d\n"), pBmh->biYPelsPerMeter);
    DB_PRINT(_T("biClrUsed        : %d\n"), pBmh->biClrUsed);
    DB_PRINT(_T("biClrImportant   : %d\n"), pBmh->biClrImportant);


    if (pBmh->biWidth <= 32 )
    {

        StdString strImg = _T(" .,:;ilLC*X&%");


        int iWidth      =    pBmh->biWidth;
        int iHeight     =    pBmh->biWidth;
        int iBpp        =    pBmh->biBitCount;
        int iPich       =    CalcPitch(iWidth, iBpp);
        void* pBits     =    m_pPixel;

        BYTE* pLineHead;

        if (iBpp <= 1)
        {
        }
        else if (iBpp <= 4)
        {



        }
        else if (iBpp <= 8)
        {



        }
        else
        {
            DWORD* pVal;
            //DWORD* pVal2;
            int idev = INT_MAX;
            for (int iY = 0; iY <  iHeight; iY++)
            {
                pLineHead = (BYTE*)(pBits) + iY * iPich;
                for (int iX = 0; iX <  iWidth; iX++)
                {
                    pVal = (DWORD*)(pLineHead + ((iX*iBpp)/8));
                    //pVal2 = (DWORD*) m_Image.GetPixelAddress(iX, iY);
                    //if (pVal != pVal2)
                    //{
                    //
                    //}

                    DB_PRINT( _T("%08x "), *pVal);
                }
                DB_PRINT( _T("\n"));
            }
        }
    }
}




//load save のインスタンスが生成されないため
//ダミーとして実装
void CImageData::Dummy()
{
    unsigned int iVersion = 0;

    StdStreamOut outFs(_T(""));
    StdStreamIn  inFs(_T(""));
    //boost::filesystem::ofstream outTxtFs(_T(""));
    //boost::filesystem::ifstream inTxtFs(_T(""));

    boost::archive::text_woarchive txtOut(outFs); 
    boost::archive::text_wiarchive txtIn(inFs);

    save<boost::archive::text_woarchive>( txtOut, iVersion);
    load<boost::archive::text_wiarchive>( txtIn,  iVersion);


    StdXmlArchiveOut outXml(outFs);
    StdXmlArchiveIn inXml(inFs);

    save<StdXmlArchiveOut>(outXml, iVersion);
    load<StdXmlArchiveIn>(inXml, iVersion);

    boost::filesystem::ofstream outFsStd(_T(""));
    boost::filesystem::ifstream inFsStd(_T(""));

    boost::archive::binary_oarchive outBin(outFsStd);
    boost::archive::binary_iarchive inBin(inFsStd);

    save<boost::archive::binary_oarchive>(outBin, iVersion);
    load<boost::archive::binary_iarchive>(inBin, iVersion);


}


//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
#include "Utility/TestDlg.h"

void TestDraw(HDC hDc, void* pImageData)
{
    POINT pt;
    CImageData* pImg = reinterpret_cast<CImageData*>(pImageData);

    RECT rcFill;

    rcFill.top      = 0;
    rcFill.left     = 0;
    rcFill.right    = 100;
    rcFill.bottom   = 100;

    HBRUSH hBr = ::CreateSolidBrush(RGB(210,210,0));

    ::FillRect(hDc, &rcFill, hBr);

    for (int iCnt = 0; iCnt < 360; iCnt += 10)
    {
        pt.x = int(cos(double(iCnt*DEG2RAD)) * 50.0 + 100.0);
        pt.y = int(sin(double(iCnt*DEG2RAD)) * 50.0 + 100.0);
        pImg->SetAngle(iCnt);
        pImg->Draw(hDc, pt, 0);
    }

    pt.x = 100;
    pt.y = 100;
    pImg->DrawFill(hDc, pt, RGB(255, 0, 0));

    DeleteObject(hBr);
}


void TestTime(HDC hDc, void* pImageData)
{
    POINT pt;
    CImageData* pImg = reinterpret_cast<CImageData*>(pImageData);

    RECT rcFill;

    rcFill.top      = 0;
    rcFill.left     = 0;
    rcFill.right    = 100;
    rcFill.bottom   = 100;

    HBRUSH hBr = ::CreateSolidBrush(RGB(210,210,0));

    ::FillRect(hDc, &rcFill, hBr);

    pt.x = 50;
    pt.y = 100;


    //5000 回の描画時間を計測
    CUtil::MesureTime(0, 0, _T(""));
    for(int iCnt = 0; iCnt < 5000; iCnt++)
    {
        pImg->Draw(hDc, pt, 0);
    }
    CUtil::MesureTime(0, 1, _T("Draw"));

    pt.x = 100;
    pt.y = 100;
 
    CUtil::MesureTime(0, 0, _T(""));
    for(int iCnt = 0; iCnt < 5000; iCnt++)
    {
        pImg->DrawFill(hDc, pt, RGB(255, 0, 0));
    }
    CUtil::MesureTime(0, 1, _T("DrawFill"));

    DeleteObject(hBr);
}

void TEST_CImageData()
{
    CUtil::DbgOut(_T("%s Start\n"), DB_FUNC_NAME.c_str());


#if 0
    StdPath  pathLoad = CSystem::GetSystemPath();
    pathLoad /= _T("Test1.Bmp");

    bool bRet;
    CImageData imgTest1;
    CImageData imgTest2;

    bRet = imgTest1.LoadFile(pathLoad);

    STD_ASSERT(bRet);
    imgTest1.Ptint();
    imgTest1.Edit();

    StdPath  pathSave = CSystem::GetSystemPath();
    pathSave /= _T("Test1.Xml");

    StdStreamOut outFs(pathSave);
    {
        StdXmlArchiveOut outXml(outFs);
        outXml << boost::serialization::make_nvp("Root", imgTest1);
    }
    outFs.close();


    StdStreamIn inFs(pathSave);
    {
        StdXmlArchiveIn inXml(inFs);
        inXml >> boost::serialization::make_nvp("Root", imgTest2);
    }
    inFs.close();

    imgTest2.Edit();
#endif



#if 0
    StdPath  pathLoad = CSystem::GetSystemPath();
    pathLoad /= _T("Test1.Bmp");

    bool bRet;
    CImageData imgTest1;

    bRet = imgTest1.LoadFile(pathLoad);
    imgTest1.Ptint();
    imgTest1.Edit();

    POINT ptOffset;

    ptOffset.x = 16;
    ptOffset.y = 16;

    imgTest1.SetOffset(ptOffset);

    CTestDlg dlgTest;

    dlgTest.SetDrawFunc(TestDraw);
    dlgTest.SetData(&imgTest1);

    dlgTest.DoModal();
#endif

#if 0
    StdPath  pathLoad = CSystem::GetSystemPath();
    pathLoad /= _T("Test1.Bmp");

    bool bRet;
    CImageData imgTest1;

    bRet = imgTest1.LoadFile(pathLoad);
    POINT ptOffset;

    ptOffset.x = 16;
    ptOffset.y = 16;

    imgTest1.SetOffset(ptOffset);

    CTestDlg dlgTest;

    dlgTest.SetDrawFunc(TestTime);
    dlgTest.SetUserData(&imgTest1);

    dlgTest.DoModal();

    // 計測結果
    //Time[0] 141[ms] Draw 
    //Time[0] 249[ms] DrawFill 
#endif



    CUtil::DbgOut(_T("%s End\n"), DB_FUNC_NAME.c_str());
    CUtil::DbgOut(_T("\n"));
}
#endif //_DEBUG