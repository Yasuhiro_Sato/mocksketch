/**
 * @brief        CImageDataヘッダーファイル
 * @file	        CImageData.h
 * @author           Yasuhiro Sato
 * @date	        07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
  *			
 *
 * $
 * $
 * 
 */
#ifndef __IMAGE_DATA_H_
#define __IMAGE_DATA_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DrawingObject/Primitive/POINT2D.h"
//class CPartsDef;

enum E_TRNSPAENT_TYPE
{
    E_TRP_NONE  = 0, 
    E_TRP_COLOR = 1,
    E_TRP_LT    = 2,
    E_TRP_RB    = 3,
};


struct CImagePropData
{

    //<! オフセット
    POINT2D      m_ptOffset;

    //!< X分割薄
    int       m_iXDiv;

    //!< Y分割薄
    int       m_iYDiv;

    //!< DPI
    int       m_iDpi;

    //<! 透過有無
    E_TRNSPAENT_TYPE      m_eTranspaent;

    //<! 透過色
    COLORREF    m_crTransparent;

};


/**
 * @class   CImageData
 * @brief  
 */
class CImageData 
{
friend CImageData;
public:
    //!< コンストラクタ
    CImageData();

    CImageData(const CImageData & obj);

    //!< デストラクタ
    virtual ~CImageData();

    //!< 代入演算子
    virtual CImageData& operator = (const CImageData & obj);

    //!< ファイル読み込み
    bool LoadFile(StdPath path);

    //!< 透過有無
    void SetUseTranparent(E_TRNSPAENT_TYPE eTransparent);

    //!< 透過色設定
    void SetTranparentColor(COLORREF crTransparent);

    //!< オフセット設定
    void SetOffset(POINT2D pt);

    //!< 回転角設定
    void SetAngle(double dAngle);

    //!< 倍率設定
    void SetScl(double dScl);

    //!< 倍率設定
    void SetScl(double dSclX, double dSclY);

    //!< 透過率
    void SetTransmittance(DWORD dwTrans);

    //!< 鏡像設定
    void SetMirror(bool bMirror);

    //!< 描画
    void Draw(HDC hDestDC, POINT pt, int iNo) ;

    //!< 描画
    void Draw(HDC hDestDC, POINT pt,
              int iNo,
              double dSclX,
              double dSclY,
              double dAngle,
              DWORD dwTrans) ;


    //!< 描画
    void DrawFill(HDC hDestDC, POINT pt, COLORREF crFill);

    void DrawFill(HDC hDestDC, POINT pt, COLORREF crFill,
                                          double dSclX,
                                          double dSclY,
                                          double dAngle);

    //!< 描画
    void DrawRect(HDC hDestDC, POINT pt) ;

    void DrawRect(HDC hDestDC, POINT pt,
                                double dSclX,
                                double dSclY,
                                double dAngle);


    void GetFramePoints(
        double dSclX,
        double dSclY,
        double dAngle,
        std::vector<POINT2D>* pList);



    //!< ビットマップ生成
    bool Create(int iWidth, int iHeight, int iBitCnt);

    //!< 幅取得
    int GetWidth() const;

    //!< 高さ取得
    int GetHeight()  const;

    //!< ビットマップ編集
    bool Edit();

    //!< デバッグ用プリント
    void Ptint();

    //!< X分割数設定(アニメーション用)
    void SetXdiv(int iNum);

    //!< X分割数取得(アニメーション用)
    int GetXdiv() const;

    //!< Y分割数設定
    void SetYdiv(int iNum);

    //!< Y分割数取得
    int GetYdiv() const;

    //!< 総分割数
    int Getdiv() const;

    //!< DPI設定
    void SetDpi(int iDpi);

    //!< DPI取得
    int GetDpi() const;

    //!< オフセット取得
    POINT2D GetOffset() const;

    //!< 鏡像設定取得
    bool GetMirror() const;


    void SetProp(CImagePropData m_Prop);

    CImagePropData GetProp() const;

    void SetFileName(StdString   strFileName);

    StdString GetFileName() const;


    //void SetPartsDef(CPartsDef*  pPartsDef);

    void LoadAfter();
    

    //!< ピッチ計算
    static int  CalcPitch(int iWidth, int iBitCnt);

    bool IsUseFile() const {return m_bUseFile;}


private:
    //!< ダミー
    void Dummy();

    //!< ビットマスク作成
    void CreateBitmask(HDC hDc);

    //!< ビットマスク削除
    void DeleteBitmask();

    //!< ビットマップ削除
    void DeleteBitmap();

    //!< アルファビットマスク作成
    void CreateAlpha(HDC hDc);

    //!< アルファビットマップ削除
    void DeleteAlpha();


    //!< 頂点計算
    void CalcPoint();
    void CalcFramePoint(std::vector<POINT2D>* lstPt)const;


    HDC RentalDC();

    void ReturnDC();


protected:
    //CImage      m_Image;



    CImagePropData m_Prop;

protected:
    //----------------
    //  非保存データ
    //----------------
    //<! 回転角
    double      m_dAngle;

    //<! X倍率
    double      m_dSclX;

    //<! Y倍率
    double      m_dSclY;

    //!< 鏡像
    bool        m_bMirror;


    //!< マスクデバイスコンテキスト
    HDC         m_maskDc;

    //!< マスクビットマップ
    HBITMAP     m_maskBitmap;

    //!< ビットマップ
    HBITMAP     m_hOldMaskBitmap;

    //!< デバイスコンテキスト
    HDC         m_hDc;

    //!< ビットマップ
    HBITMAP     m_hBitmap;

    //!< ビットマップ
    HBITMAP     m_hOldBitmap;

    //!< アルファチャンネルコンテキスト
    HDC         m_dcAlpha;

    //!< アルファチャンネルビットマップ
    HBITMAP     m_hAlphaBitmap;

    BITMAPINFO  m_bmpInfo;

    //!< ビットフィールド
    BYTE*       m_pPixel;

    //!< PlgBlt頂点座標
    POINT       m_point[4];

    POINT       m_ptLT;
    POINT       m_ptRB;

    //!< PlgBlt頂点座標
    bool        m_bChg;

    mutable int  m_iBitMapRefarenceCnt;

    //<! 透過率
    DWORD      m_dwTransmiittance;

    //!< ファイル名
    StdString   m_strFileName;

    //!< ファイル使用有無
    bool        m_bUseFile;

    CPartsDef*  m_pPartsDef;

private:
    //==============================
    // ファイル保存
    //==============================
    friend class boost::serialization::access;  

    BOOST_SERIALIZATION_SPLIT_MEMBER();

    template<class Archive>
    void load(Archive& ar, const unsigned int version);

    template<class Archive>
    void save(Archive& ar, const unsigned int version) const;


};

#endif // 