/**
 * @brief			CLog実装ファイル
 * @file			CLog.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "CLog.h"
#include "CUtility.h"




/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
//#define NO_THREAD 


CLog::DataFile::DataFile()
{
    pFileData = NULL;
}

CLog::DataFile::~DataFile()
{
    if(pFileData)
    {
        pFileData->close();
        delete pFileData;
    }
}


void CLog::DataFile::Create()
{
    if(!pFileData)
    {
        pFileData = new boost::filesystem::fstream;
    }
}

/**
 *  @brief   コンストラクター
 *  @param   なし
 *  @retval  なし
 *  @note
 */
CLog::CLog():
    m_bInit         ( false ), 
    m_iMaxFileData  ( 1000  ),
    m_iDivFile      ( 10    ),
    m_bEndThread    ( false ),
    m_pThread       ( NULL  ),
    m_iStart        ( 0 ),
    m_iEnd          ( 0 ),
    m_iSeekOffset   ( sizeof(INDEX_HEAD)),
    m_iIndexSize    ( sizeof(INDEX_DATA))
{
    m_iMaxIndex = m_iMaxFileData * m_iDivFile;
}



/**
 *  @brief   デストラクタ
 *  @param   なし  
 *  @retval  なし
 *  @note
 */
CLog::~CLog()
{
    if (m_pThread)
    {
        m_bEndThread = true;
        m_pThread->join();
        delete m_pThread;
    }


    //インデックスの保存
    INDEX_HEAD Head;

    Head.iVersion = m_iVerasion;
    Head.iStart =   m_iStart;
    Head.iEnd   =   m_iEndFinish;

    m_fileIndex.seekp(0);
    m_fileIndex.write( (char*)&Head, m_iSeekOffset );
    m_fileIndex.flush();
    

    foreach(BUFFER_DATA& Data, m_buffer)
    {
        delete[] Data.pData;
    }
}

/**
 *  @brief   ログファイル分割数設定
 *  @param   [in] iNum      分割数
 *  @retval  なし
 *  @note
 */
bool CLog::SetDivFileNum(int iNum)
{
    //初期前のみ実行可能
    if (m_bInit){ return false;}
        

    if (iNum <= 0)
    {
        return false;
    }
    if (iNum > 10000)
    {
        return false;
    }
    m_iDivFile = iNum;
    m_iMaxIndex = m_iDivFile * m_iMaxFileData;
    return true;
}

/**
 *  @brief   １ファイル当たりのデータ数設定
 *  @param   [in] iNum      １ファイル当たりのデータ数
 *  @retval  なし
 *  @note
 */
bool CLog::SetMaxFileDataNum(int iNum)
{
    //初期前のみ実行可能
    if (m_bInit){ return false;}
        

    if (iNum <= 0)
    {
        return false;
    }

    m_iMaxFileData = iNum;
    m_iMaxIndex = m_iDivFile * m_iMaxFileData;
    return true;
}


/**
 *  @brief   初期化
 *  @param   [in] pathSave      保存パス
 *  @param   [in] strFileName  ファイル名(拡張子なし）
 *  @retval  なし
 *  @note
 */
bool CLog::Create(StdPath pathSave, StdString strFileName)
{

    namespace fs = boost::filesystem;
    INDEX_HEAD   bufHead;
    int          iFileSize = 0;
    StdPath      pathFile;
    std::ios_base::open_mode iMode;

    m_iMaxIndex = m_iDivFile * m_iMaxFileData;
    iMode = std::ios::in | std::ios::out | /*std::ios::app  |*/ std::ios::binary | std::ios::ate;
    int iTotalSize = m_iMaxIndex * m_iIndexSize + m_iSeekOffset;

    try
    {
        bool bCreateIndexFile = true;

        //-------------------------------
        // インデックスファイルの設定
        //-------------------------------
        pathFile = pathSave / (strFileName + _T(".mdx"));
        if (fs::exists(pathFile))
        {
            // ファイルがあればとりあえずヘッダー部を
            // 読んで置く
            iFileSize = (int)fs::file_size(pathFile);
            if (iFileSize >= m_iSeekOffset)
            {
                //
                m_fileIndex.open(pathFile,  std::ios::in);
                if (!m_fileIndex)
                {
                    //これは致命的
                    STD_DBG(_T("File open fail[%s]"), GetPathStr(pathFile).c_str());
                    throw(1);
                }
                m_fileIndex.seekg( 0 );
                m_fileIndex.read( (char*)&bufHead, sizeof(bufHead) );

                //バージョンチェック
                if (bufHead.iVersion != m_iVerasion)
                {
                    //バージョン変更に伴う処理を行う

                }

                //ファイルサイズをチェック
                if (iFileSize != iTotalSize)
                {
                    m_fileIndex.close();
                    STD_DBG(_T("iFileSize != iTotalSize (%d,%d)"), iFileSize, iTotalSize);
                    throw(2);
                }
                m_fileIndex.close();
                bCreateIndexFile = false;

            }
            else
            {
                //throw (_T("ファイルサイズ不正"));
                //->ログファイルを消去するか？
                //  問い合わせが必要か
                STD_DBG(_T("iFileSize(%d) >= m_iSeekOffset "), iFileSize);
                fs::remove(pathFile);
            }
        }
        

        INDEX_HEAD headData;
        INDEX_DATA indexEnd;
        if (bCreateIndexFile)
        {
            // インデックスファイル新規作成
   
            //新しくファイルを作る
            char* m_pBuf = new char[iTotalSize];
            memset(m_pBuf, 0, sizeof(char) * iTotalSize);


            headData.iVersion = m_iVerasion;
            headData.iStart = 0;
            headData.iEnd   = 0;

            memcpy(m_pBuf, &headData, sizeof(headData));

            m_fileIndex.open(pathFile,  std::ios::trunc | std::ios::out );
            if (!m_fileIndex)
            {
                STD_DBG(_T("iFileSize(%d) >= m_iSeekOffset "), iFileSize);
                throw(3);
            }
            m_fileIndex.write( m_pBuf, iTotalSize );
            m_fileIndex.close();
        }
        
        m_fileIndex.open(pathFile,  iMode);
        if (!m_fileIndex)
        {
            STD_DBG(_T("File open fail"));
            throw(4);
        }


        //
        m_fileIndex.seekg(0);
        m_fileIndex.read( (char*)&headData, m_iSeekOffset );

        m_iStart = headData.iStart;
        m_iEnd   = headData.iEnd;
        m_iEndFinish = m_iEnd;
        m_iDummyEnd = 0;

        
        //データファイルオープン

        m_lstDataFile.resize(m_iDivFile + 1);

        for (int iCnt = 0; iCnt <( m_iDivFile + 1); iCnt++)
        {
            StdStringStream strmFile;
            pathFile = _T("");
            strmFile << strFileName << StdFormat(_T("_%d.mdd")) % iCnt;
            pathFile = pathSave / (strmFile.str());

            m_lstDataFile[iCnt].Create();
            m_lstDataFile[iCnt].pathFile = pathFile;

 
            boost::filesystem::fstream*  pFileData  = m_lstDataFile[iCnt].pFileData;

            if (!fs::exists(pathFile))
            {
                pFileData->open(pathFile,  std::ios::trunc | std::ios::out );
                if (!(*pFileData))
                {
                    STD_DBG(_T("File open fail %s"), GetPathStr(pathFile).c_str());
                    throw(5);
                }
                pFileData->close();
            }
            pFileData->open(pathFile,  iMode);



            if(!pFileData->good())
            {
                std::ios_base::iostate ioState = pFileData->rdstate( );
                STD_DBG(_T("pFileData->good() %d"), ioState);
                throw(6);
            }
        }

        if (m_iStart != m_iEnd)
        {
            //最終位置データを取得し次の位置を設定する
            m_fileIndex.seekg(m_iSeekOffset + m_iIndexSize * m_iEnd);
            m_fileIndex.read( (char*)&indexEnd, sizeof(indexEnd) );
            
            
            m_iDummyEnd = indexEnd.iOffset *  m_iMaxFileData + indexEnd.iFileIndex;
        }
        // スレッド開始
#ifndef NO_THREAD 
        m_pThread = new boost::thread(&ThreadFunc, this);
#endif
        m_bInit = true;
    }
    catch(...)
    {
        //全部クローズ
        m_fileIndex.close();
        for (int iCnt = 0; iCnt <( m_iDivFile + 1); iCnt++)
        {
            m_lstDataFile[iCnt].pFileData->close();
        }

        STD_DBG(_T("Exception"));
        throw (_T("create error"));
    }
    return true;
}



/**
 *  @brief   開始インデックス取得
 *  @param   なし  
 *  @retval  なし
 *  @note
 */
int CLog::GetStartIndex()
{
    STD_ASSERT(m_bInit);
    return m_iStart;
}

/**
 *  @brief   最終インデックス取得
 *  @param   なし  
 *  @retval  なし
 *  @note
 */
int CLog::GetEndIndex()
{
    STD_ASSERT(m_bInit);
    return m_iEndFinish;
}
//!< 最大インデックス
/**
 *  @brief   最大インデックス取得
 *  @param   なし  
 *  @retval  なし
 *  @note
 */
int CLog::GetMaxIndex()
{
    STD_ASSERT(m_bInit);
    return m_iMaxIndex;
}

/**
 *  @brief   書き込み
 *  @param   eType ログ種別  
 *  @param   pData 書込データ  
 *  @param   lLen  データ長
 *  @param   bBin  true バイナリデータ false アスキー
 *  @retval  なし
 *  @note
 */
void CLog::SetData(CLog::LOG_TYPE eType, const char* pData, long lLen, bool bBin)
{
    static   boost::mutex   mtxGuard;

    BUFFER_DATA TmpBuff;
    INDEX_DATA* pIndexData;

    TmpBuff.zIndexData.iLen = lLen;
    pIndexData = &TmpBuff.zIndexData;
 
    //<<<<<<<<<<<<<<<
    {
        boost::mutex::scoped_lock lk(mtxGuard);
        // カウントアップ中は排他
        // Endをカウントアップさせるタイミングは
        // 実際にデータを書き込んだ後
        //m_iEnd++;
        
        if ((m_iEnd + 1) >= m_iMaxIndex) {m_iEnd = 0;}
        else                            {m_iEnd++;  }


        if (m_iEnd == m_iStart)
        {
            if ((m_iStart + 1) >= m_iMaxIndex) {m_iStart = 0;}
            else                              {m_iStart++;  }

        }

        pIndexData->iIndex = m_iEnd;
        m_iDummyEnd++;

        if (m_iDummyEnd >= (m_iMaxIndex + m_iMaxFileData))
        {
            m_iDummyEnd = 0;
        }

        std::div_t resDev;

        resDev = std::div( m_iDummyEnd, m_iMaxFileData);
        pIndexData->iOffset       = resDev.quot;
        pIndexData->iFileIndex    = resDev.rem;
    }
    //<<<<<<<<<<<<<<<
 
    //Win32API
    GetSystemTime(&pIndexData->time);
    pIndexData->iType = eType;
    pIndexData->iDType = (bBin ? e_bin : e_asc);

    TmpBuff.pData = new char[lLen];

    memcpy(TmpBuff.pData, pData, lLen);

    boost::mutex::scoped_lock lk(m_mtxGuardSeek);
    //<<<<<<<<<<<<<<<<<<<
    m_buffer.push_back(TmpBuff);

#if 1
    CUtil::DbgOut(_T("S:%d E:%d  Off:%d P:%d\n"), m_iStart, m_iEnd, 
                                               pIndexData->iOffset,
                                               pIndexData->iFileIndex);
#endif
    //ここではデータを詰めるだけ
#ifdef NO_THREAD 
       StepFunc();
#endif
    //<<<<<<<<<<<<<<<<<<<
}

/**
 *  @brief   書き込み(ベクター)
 *  @param   eType ログ種別  
 *  @param   pVec  書込データ  
 *  @retval  なし
 *  @note
 */
template<class D>
void CLog::SetData(CLog::LOG_TYPE eType, const std::vector<D>* pVec)
{

    int iLen  = sizeof(D) * pVec->size();
    SetData( eType, (char*)&pVec[0], iLen, true);
}

/**
 *  @brief   書き込み(ストリーム)
 *  @param   eType    ログ種別  
 *  @param   pStream  書込データ  
 *  @retval  なし
 *  @note
 */
void CLog::SetData(CLog::LOG_TYPE eType, const StdStringStream* pStream)
{
    long iLen  = SizeToInt(sizeof(StdChar) * pStream->str().size());
    SetData( eType, (char*)pStream->str().c_str(), iLen, false);
}

/**
 *  @brief   書き込み(文字)
 *  @param   eType    ログ種別  
 *  @param   pStream  書込データ  
 *  @retval  なし
 *  @note
 */
void CLog::SetData(CLog::LOG_TYPE eType, const StdString* pString)
{
    int iLen  = SizeToInt(sizeof(StdChar) * pString->size());
    SetData( eType, (char*)pString->c_str(), iLen, false);
}

/**
 *  @brief   書き込み(フォーマット形式)
 *  @param   eType    ログ種別  
 *  @param   pStr     書込データ  
 *  @retval  なし
 *  @note
 */
void CLog::SetData(CLog::LOG_TYPE eType, const StdChar* pStr, ...)
{
   va_list vl;
   static const size_t iBufSize = 4096;
   StdChar buf[iBufSize];

   va_start( vl, pStr );
   _vsntprintf_s( buf, iBufSize, pStr, vl );
   long iLen = SizeToInt(sizeof(StdChar) * _tcslen(buf));

   SetData( eType, (char*)buf, iLen, false);
   va_end( vl );
}

/**
 *  @brief   インデックス読込み(バイト列)
 *  @param   pIndex   インデックスデータ
 *  @param   pData    読込データ  
 *  @param   iSize    読込データバッファーサイズ  
 *  @retval  false 読み込み失敗
 *  @note
 */
bool CLog::GetIndex(int iIndex, CLog::INDEX_DATA* pIndex)
{
    //!< インデックス範囲チェック
    if( !CheckIndex( iIndex ))
    {
        return false;
    }

    try
    {
        boost::mutex::scoped_lock lk(m_mtxGuardSeek);
        m_fileIndex.seekg(m_iSeekOffset + iIndex * m_iIndexSize);
        m_fileIndex.read( (char*)pIndex, m_iIndexSize );

    }
    catch(...)
    {
        return false;
    }
    return true;
}

/**
 *  @brief   読込み(バイト列)
 *  @param   pIndex   インデックスデータ
 *  @param   pData    読込データ  
 *  @param   iSize    読込データバッファーサイズ  
 *  @retval  false 読み込み失敗
 *  @note
 */
bool CLog::GetData(int iIndex, CLog::INDEX_DATA* pIndex, char* pData, int iSize)
{
    //!< インデックス範囲チェック
    if( !CheckIndex( iIndex ))
    {
        STD_DBG(_T("Index = %d"), iIndex);
        return false;
    }

    try
    {
        m_fileIndex.seekg(m_iSeekOffset + iIndex * m_iIndexSize);
        m_fileIndex.read( (char*)pIndex, m_iIndexSize );

        int iFileOffset = pIndex->iOffset;
        int iFilePos = pIndex->iFilePos;
        int iLen = pIndex->iLen;

        if (iSize < iLen)
        {
            iLen = iSize;
        }

        m_lstDataFile[iFileOffset].pFileData->seekg(iFilePos);
        m_lstDataFile[iFileOffset].pFileData->read(pData, iLen);
    }
    catch(...)
    {
        STD_DBG(_T("Exception"));
        return false;
    }
    return true;
}

/**
 *  @brief   読込み(ベクター)
 *  @param   pIndex   インデックスデータ
 *  @param   pData    読込データ  
 *  @param   iSize    読込データバッファーサイズ  
 *  @retval  false 読み込み失敗
 *  @note
 */
template<class D>
bool CLog::GetData(int iIndex, CLog::LOG_TYPE* pType, std::vector<D>* pVec)
{
    static const int iBufSize = 4096;
    char szBuf[iBufSize];
    bool bRet;
   
    INDEX_DATA  indexData;

    bRet = GetData(iIndex, &indexData, &szBuf[0], iBufSize);

    if (!bRet)
    {
        STD_DBG(_T("GetData"));
        return false;
    }

    int iLen = int(ceil(indexData.iLen / sizeof(D)));

    *pType = indexData.iType;

    pVec.resize(iLen);

    memcpy(&(*pVec)[0], &szBuf[0], indexData.iLen);

    return true;
}

/**
 *  @brief   読込み(ストリーム)
 *  @param   pIndex   インデックスデータ
 *  @param   pData    読込データ  
 *  @param   iSize    読込データバッファーサイズ  
 *  @retval  false 読み込み失敗
 *  @note
 */
bool CLog::GetData(int iIndex, CLog::LOG_TYPE* pType, StdStringStream* pStream)
{
    static const int iRate  = sizeof(StdChar) / sizeof(char);
    static const int iBufSize = 4096;
    StdChar szBuf[iBufSize];
    bool bRet;

    memset(szBuf, 0, sizeof(szBuf));

    sizeof(StdChar) / sizeof(char);

    INDEX_DATA  indexData;

    bRet = GetData(iIndex, &indexData, (char*)&szBuf[0], iBufSize * iRate);

    if (!bRet)
    {
        STD_DBG(_T("GetData"));
        return false;
    }

    *pType = static_cast<LOG_TYPE>(indexData.iType);
    *pStream << szBuf;
    
    return true;
}


/**
 *  @brief   読込み(文字)
 *  @param   pIndex   インデックスデータ
 *  @param   pString  読込データ  
 *  @retval  false 読み込み失敗
 *  @note
 */
bool CLog::GetData(int iIndex, CLog::LOG_TYPE* pType, StdString* pString)
{
    static const int iRate  = sizeof(StdChar) / sizeof(char);
    static const int iBufSize = 4096;
    StdChar szBuf[iBufSize];
    bool bRet;

    memset(szBuf, 0, sizeof(szBuf));

    sizeof(StdChar) / sizeof(char);

    INDEX_DATA  indexData;

    bRet = GetData(iIndex, &indexData, (char*)&szBuf[0], iBufSize * iRate);

    if (!bRet)
    {
        STD_DBG(_T("GetData"));
        return false;
    }

    *pType = static_cast<LOG_TYPE>(indexData.iType);
    *pString = szBuf;
    
    return true;
}


/**
 *  @brief   インデックス範囲チェック
 *  @param   なし  
 *  @retval  なし
 *  @note
 */
 bool CLog::CheckIndex( int iIndex )
{
    if (iIndex < 0) {return false;} 
    if (iIndex >= m_iMaxIndex) {return false;} 

    //開始位置と終了位置の間にある必要がある
    if (m_iStart < m_iEnd)
    {
        if(m_iStart > iIndex) {return false;}
        if(m_iEnd   < iIndex) {return false;}
        return true;
    }
    else
    {
        if (iIndex >= m_iStart) {return true;}
        if (iIndex <= m_iEnd  ) {return true;}
    }
    return false;
}



/**
 *  @brief   スレッド終了フラグ確認
 *  @param   なし  
 *  @retval  true 終了
 *  @note     スレッド内で使用する
 */
bool CLog::IsThreadEnd()
{
    return m_bEndThread;
}


/**
 *  @brief   書込データの有無
 *  @param   なし  
 *  @retval  true データあり
 *  @note
 */
bool CLog::IsWriteDataExists()
{
    return (m_buffer.size() != 0);
}

/**
 *  @brief   ステップ実行
 *  @param   なし  
 *  @retval  なし
 *  @note    スレッド内部から呼び出しi
 */
void CLog::StepFunc()
{
    namespace fs = boost::filesystem;

    if (m_buffer.size() == 0)
    {
        return;
    }
    
    INDEX_DATA*  pIndex;
    DataFile*    pDataFile;
    BUFFER_DATA*  pBuffer;
    char*        pData = NULL;
    try
    {    
        boost::mutex::scoped_lock lk(m_mtxGuardSeek);
        //インデックス位置はすでに計算済み
        pBuffer = &(*m_buffer.begin());
        if (pBuffer == NULL)
        {
            STD_DBG(_T("pBuffer == NULL"));
            throw(_T(""));
        }

        pIndex =  &pBuffer->zIndexData;
        pDataFile = &m_lstDataFile[pIndex->iOffset];

        if (pIndex->iFileIndex == 0)
        {
            std::ios_base::open_mode iMode;
            iMode = std::ios::in | std::ios::out |std::ios::trunc  | std::ios::binary | std::ios::ate;

            pDataFile->pFileData->close();
            fs::remove(pDataFile->pathFile);
            pDataFile->pFileData->open(pDataFile->pathFile,  iMode);

            if(!(*pDataFile->pFileData))
            {
                STD_DBG(_T("pDataFile->pFileData"));
                throw(_T(""));
            }
        }


        //ファイルの最後に移動し、その値を記憶する
        pDataFile->pFileData->seekp(0, std::ios_base::end);
        pIndex->iFilePos = (int)pDataFile->pFileData->tellp();

        //データの保存
        pData = pBuffer->pData;
        pDataFile->pFileData->write( pData, pIndex->iLen);
        m_fileIndex.flush();


        //インデックスの保存
        m_fileIndex.seekp(m_iSeekOffset + pIndex->iIndex * m_iIndexSize, std::ios::beg );
        m_fileIndex.write( (char*)pIndex, m_iIndexSize );

        m_fileIndex.flush();

        //終了位置を更新
        m_iEndFinish = pIndex->iIndex;
    }
    catch(...)
    {
        // スレッド内で例外を出してもしょうがない
        STD_DBG(_T("Thread Error"));
    }

    delete[] pData;
    m_buffer.pop_front();
}

/**
 *  @brief   データ追加
 *  @param   なし  
 *  @retval  なし
 *  @note
 */
void CLog::ThreadFunc( CLog* pLog )
{
    while(1)
    {
        if(pLog->IsThreadEnd())
        {
            break;
        }

        if (!pLog->IsWriteDataExists())
        {
            StdSleep(5);
            continue;
        }
#ifndef NO_THREAD 
        pLog->StepFunc();
#endif
    }
}
