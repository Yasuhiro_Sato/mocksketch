/**
 * @brief			CLogヘッダーファイル
 * @file			CLog.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

#ifndef __LOG_H__
#define __LOG_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/

/**
 * @class   CLog
 * @brief   数十万件規模のログを扱う 
 */
class CLog
{



public:
    //!< ログ種別
    enum LOG_TYPE
    {
        e_error = 1,
        e_warn  = 2,
        e_info  = 4,
    };

    //!< データ種別
    enum DATA_TYPE
    {
        e_bin  = 0,
        e_asc  = 1,
    };
#pragma pack(1)

    //!< インデックスデータヘッダー
    //!<  ファイル保存時に書き込み
    struct INDEX_HEAD
    {
        int  iVersion;
        int  iStart;
        int  iEnd;
    };

    //!< インデックス
    struct INDEX_DATA
    {
        int         iIndex;     //!<インデックス値
        int         iOffset;    //!< 参照ファイルオフセット
        int         iFileIndex; //!< 参照インデックス
        int         iFilePos;   //!< ファイル内位置
        int         iLen;       //!< データ長
        SYSTEMTIME  time;       //!< 時刻
        int         iType;       //!< ログ種別
        int         iDType;      //!< データ種別
        int         iDummy1;     //!< 予備
        int         iDummy2;     //!< 予備
        int         iDummy3;     //!< 予備
        int         iDummy4;     //!< 予備
    };

    struct BUFFER_DATA
    {
        INDEX_DATA  zIndexData;
        char *      pData;

    };

    struct DataFile
    {
        DataFile();//{pFileData = new boost::filesystem::fstream;}
        ~DataFile();/*
        {
            if(*pFileData)
            {
                pFileData->close();
                delete pFileData;
            }
        }
        */

        void Create();


        StdPath  pathFile;
        boost::filesystem::fstream*  pFileData;
    };

#pragma pack()
    

 protected:

    //!< ファイルバージョン
    static const int m_iVerasion = 1;

    //ファイル名
    StdString       m_strLogName;

    //!< 開始インデックス
    int m_iStart;

    //!< 終了インデックス
    int m_iEnd;

    //!< 終了インデックス
    int m_iEndFinish;

    //!< ファイル位置計算用終了インデックス
    int m_iDummyEnd;

    //!<ログインデックス最大数
    int m_iMaxIndex;

    //!< ログファイル分割数
    int m_iDivFile;

    //!< １ファイル当たりのデータ数
    int m_iMaxFileData;

    //!< 初期化完了フラグ
    int m_bInit;

    //!< オフセット値（ヘッダーサイズ)
    int m_iSeekOffset;

    //!< インデックスサイズ
    int m_iIndexSize;

    //!< 書き込みスレッド
    boost::thread* m_pThread;

    //!< スレッド終了フラグ
    bool m_bEndThread;

    //!< 書き込みバッファー
    std::deque< BUFFER_DATA > m_buffer;

    //!< インデックスファイル
    boost::filesystem::fstream  m_fileIndex; 

    //!<データファイル
    std::vector< DataFile >  m_lstDataFile; 

    //!< ミューティックス
    boost::mutex   m_mtxGuardSeek;

public:
    //!< コンストラクタ
    CLog();

    //!< デストラクタ
    ~CLog();


    //===========
    // 設定関連
    //===========

    //!< ログファイル分割数設定
    bool SetDivFileNum(int iNum);
    
    //!<１ファイル当たりのデータ数設定
    bool SetMaxFileDataNum(int iNum);

    //!< 初期化
    bool Create(StdPath pathSave, StdString strFileName);

    //!< 開始インデックス
    int GetStartIndex();


    //!< 最終のインデックス
    int GetEndIndex();

    //!< 最大インデックス
    int GetMaxIndex();

    //!< 書き込み(バイト列)
    void SetData(CLog::LOG_TYPE eType, const char* pData, long lLen, bool bBin = true);
    
    //!< 書き込み(ベクター)
    template<class D>
    void SetData(CLog::LOG_TYPE eType, const std::vector<D>* pVec);

    //!< 書き込み(ストリーム)
    void SetData(CLog::LOG_TYPE eType, const StdStringStream* pStream);

    //!< 書き込み(文字)
    void SetData(CLog::LOG_TYPE eType, const StdString* pString);

    //!< 書き込み(フォーマット形式)
    void SetData(CLog::LOG_TYPE eType, const StdChar* pStr, ...);

    //!< インデックス読込み(バイト列)
    bool GetIndex(int iIndex, CLog::INDEX_DATA* pIndex);

    //!< 読込み(バイト列)
    bool GetData(int iIndex, CLog::INDEX_DATA* pIndex, char* pData, int iSize);

    //!< 読込み(ベクター)
    template<class D>
    bool GetData(int iIndex, CLog::LOG_TYPE* pType, std::vector<D>* pVec);

    //!< 読込み(ストリーム)
    bool GetData(int iIndex, CLog::LOG_TYPE* pType, StdStringStream* pStream);

    //!< 読込み(文字)
    bool GetData(int iIndex, CLog::LOG_TYPE* pType, StdString* pString);

    //!< スレッド実行部
    static void ThreadFunc( CLog* pLog );

    //!< ステップ実行
    void StepFunc();

    //!< スレッド終了フラグ確認
    bool IsThreadEnd();

    //!< 書き込みデータの有無
    bool IsWriteDataExists();

    //!< インデックス範囲チェック
    bool CheckIndex( int iIndex );

protected:

    /*
    ".IDX"
    ".IDD"      //開始位置を記録
    */

};
#endif //__LOG_H__
