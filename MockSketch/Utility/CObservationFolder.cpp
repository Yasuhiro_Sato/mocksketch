/**
 * @brief       CObservationFolder実装ファイル
 * @file        CObservationFolder.cpp
 * @author      Yasuhiro Sato
 * @date        24-3-2013
 * 
 * @note
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks        
 *
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CObservationFolder.h"


/**
 * @brief   コンストラクタ
 * @param   なし
 * @retval  なし
 * @note	
 */
CObservationFolder::CObservationFolder():
m_hDir              (NULL),
m_bOvservDir        (false),
m_hTerminateEvent   (NULL),
m_hThread           (NULL),
m_dwThresdId        (0),
m_pFuncClass        (NULL)
{
    memset(&m_overLap, 0, sizeof(m_overLap));
}

/**
 * @brief   デストラクタ
 * @param   なし
 * @retval  なし
 * @note	
 */
CObservationFolder::~CObservationFolder()
{
    Stop();
}

/**
 * @brief   ファイル変更監視開始
 * @param   なし          
 * @retval  true 成功
 * @note	
 */
bool CObservationFolder::Start(StdString strPath)
{
    return false;

    //今は使用しない
    Stop();
    m_strPath = strPath;
    m_hDir =  INVALID_HANDLE_VALUE,
    m_hTerminateEvent = CreateEvent(0,FALSE,FALSE,0);
    m_dwThresdId      = 0;

    m_bOvservDir = true;

    m_hDir = CreateFileW(m_strPath.c_str(),   // ファイル名 
                        FILE_LIST_DIRECTORY,        // アクセスモード
                        FILE_SHARE_READ |           // 共有モード
                        FILE_SHARE_DELETE,
                        NULL,                       // セキュリティ記述子
                        OPEN_EXISTING,              // 作成方法
                        FILE_FLAG_BACKUP_SEMANTICS|   // ファイル属性
                        FILE_FLAG_OVERLAPPED,        
                        NULL);                      // テンプレートファイルのハンドル

    if (m_hDir == INVALID_HANDLE_VALUE) 
    {
        return false;
    }

    //m_hThread = INVALID_HANDLE_VALUE;

    m_hThread = (HANDLE)_beginthreadex( 
       NULL,                        // スレッドのセキュリティ
       0,                           // スレッドのスタックサイズ
       _ThreadObserveFileChange,     // スレッドの開始アドレス
       this,                        // スレッドの開始引数
       0,                           // 作成オプション(0またはCREATE_SUSPENDED)
       &m_dwThresdId                // スレッド ID
    );

    if(m_hThread == INVALID_HANDLE_VALUE) 
    {
        return  false;
    }
    return true;
}


/**
 * @brief   ファイル変更監視停止
 * @param   なし          
 * @retval  true 成功
 * @note	
 */
bool CObservationFolder::Stop()
{
    if(!m_bOvservDir)
    {
        return true;
    }

    SetEvent(m_hTerminateEvent);
    m_bOvservDir = false;

    /*
    if (m_hDir != INVALID_HANDLE_VALUE) 
    {
        CancelIoEx( m_hDir, &m_overLap);
    }
    */

    if (m_hTerminateEvent) 
    {
        CloseHandle(m_hTerminateEvent);
        m_hTerminateEvent = NULL;
    }

    bool rc = false;
    if (m_hDir != INVALID_HANDLE_VALUE) 
    {
        //To stop this thread, close handle hFolder;
        //It will cause an error in ReadDirectoryChangesW()
        CloseHandle(m_hDir);
        m_hDir = INVALID_HANDLE_VALUE;
        rc = true;
    }

    
    //スレッドのハンドルを閉じる
    CloseHandle(m_hThread);
    m_hThread = NULL;

    return true;
}

/**
 * @brief   ファイル変更監視停止
 * @param   なし          
 * @retval  true 成功
 * @note	
 */
bool CObservationFolder::_Exec()
{
    
    wchar_t lpBuffer[4096] = {L'\0'};
    DWORD dwBytesReturned;
    DWORD dwNotifyFilter;
    BOOL bRet = true;

    //監視に使うフィルタ条件
    dwNotifyFilter = FILE_NOTIFY_CHANGE_LAST_WRITE;
      //FILE_NOTIFY_CHANGE_FILE_NAME   |  //ファイル名変更
      //FILE_NOTIFY_CHANGE_DIR_NAME    |  //ディレクトリ名変更
      //FILE_NOTIFY_CHANGE_ATTRIBUTES  |  //属性変更
      //FILE_NOTIFY_CHANGE_LAST_WRITE  |  //最終書き込み日時変更
      //FILE_NOTIFY_CHANGE_LAST_ACCESS |  //最終アクセス日時変更
      //FILE_NOTIFY_CHANGE_CREATION;       //作成日時変更

    HANDLE hEvent = ::CreateEvent(0, TRUE, FALSE,0);

	if (hEvent == 0)
	{
		return false;
	}

    HANDLE waitEvents[2];
    waitEvents[0] = m_hTerminateEvent;
    waitEvents[1] = hEvent;

	

    while(m_bOvservDir)
    {
        memset(&lpBuffer[0], 0, sizeof(lpBuffer));
        memset (&m_overLap, 0, sizeof(m_overLap));
        m_overLap.hEvent = hEvent;
        ResetEvent(m_overLap.hEvent);
        dwBytesReturned = 0;

        bRet = ReadDirectoryChangesW(m_hDir,  //ディレクトリのハンドル
                                  lpBuffer, //FILE_NOTIFY_INFORMATION構造体へのポインタ
                                  sizeof(lpBuffer)/sizeof(lpBuffer[0]), //lpBufferのサイズ
                                  TRUE, //サブディレクトリを監視するためのフラグ
                                  dwNotifyFilter, //監視に使うフィルタ条件
                                  &dwBytesReturned, //返されたバイト数
                                  &m_overLap, //重複I/O操作に必要な構造体へのポインタ
                                  NULL);//完了ルーチンへのポインタ
        if (!bRet) 
        {
            break;
        }

        DWORD wWaitStatus;

        wWaitStatus = ::WaitForSingleObject(m_hTerminateEvent, 0);
        if (wWaitStatus == WAIT_OBJECT_0)
        {

            break;
        }

        wWaitStatus = ::WaitForSingleObject(m_overLap.hEvent, 10);
        if (wWaitStatus == WAIT_OBJECT_0)
        {
            //2回連続して
            continue;
        }

        wWaitStatus = ::WaitForMultipleObjects(2, waitEvents, FALSE, INFINITE);

        if (wWaitStatus == WAIT_OBJECT_0 ) 
        {
            //m_hTerminateEventがシグナル状態
            break;
        }

        BOOL bOverRet = ::GetOverlappedResult(m_hDir, &m_overLap, 
                                            &dwBytesReturned, TRUE ); 
        if (!bOverRet) 
        {
            continue;
        }


        int i = 0;

        std::vector<StdString> lstStr;
        while (1)
        {
            FILE_NOTIFY_INFORMATION *lpInfomation
            = (FILE_NOTIFY_INFORMATION *)&lpBuffer[i];

            size_t length = lpInfomation->FileNameLength/sizeof(wchar_t);
            lpInfomation->FileName[length] = _T('\0');
            StdString strFile;
            strFile = &lpInfomation->FileName[0];
            std::wstring strAction;

            if (lpInfomation->Action == FILE_ACTION_MODIFIED)
            {
                if(m_pFuncCallback)
                {
                    boost::thread thSend(boost::bind(&_SendFilechange, strFile, this));
                }
            }

            if (lpInfomation->NextEntryOffset == 0) 
            {
                break;
            }
            i += lpInfomation->NextEntryOffset / 2;
        }

        if (!lstStr.empty())
        {

        }
    }

    if (m_overLap.hEvent)
    {
        CloseHandle(m_overLap.hEvent);
        m_overLap.hEvent = NULL;
    }
    return true;
}

/**
 * @brief   ファイル変更監視スレッド
 * @param   [in]  pProj
 * @retval  なし
 * @note	
 */
unsigned int  WINAPI CObservationFolder::_ThreadObserveFileChange(void * pProj)
{
    CObservationFolder* pDlg = reinterpret_cast<CObservationFolder*>(pProj);
    pDlg->_Exec();
    return 0;
}

  
/**
 * @brief   コールバック関数登録
 * @param   [in]  func      登録関数
 * @param   [in]  pClass    登録元クラス
 * @retval  なし
 * @note   
 */
bool CObservationFolder::SetCallbackFunction(
                    boost::function<void (StdString, void*) > func, 
                    void* pClass)
{
    m_pFuncCallback = func;
    m_pFuncClass    = pClass;

    return true;
}

/**
 * @brief   コールバック関数呼び出し
 * @param   [in]  strChangeFile 
 * @param   [in]  pObsFolder
 * @retval  なし
 * @note   
 */
void CObservationFolder::_SendFilechange(StdString strChangeFile, 
                                        CObservationFolder* pObsFolder)
{
    pObsFolder->m_pFuncCallback(strChangeFile, pObsFolder->m_pFuncClass);
    return;
}
