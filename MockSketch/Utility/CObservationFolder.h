/**
 * @brief       CObservationFolder実装ファイル
 * @file        CObservationFolder.cpp
 * @author      Yasuhiro Sato
 * @date        24-3-2013
 * 
 * @note
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks    アプリケーションメイン
 *
 *
 * $
 * $
 * 
 */
#ifndef  _OVSERVATION_FOLDER_H__
#define  _OVSERVATION_FOLDER_H__
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/

/**
 * @class   CObservationFolder
 * @brief   指定フォルダーの監視

    //以下の様に使用する
    class Class
    {
        CObservationFolder obs;

     public:

         Class()
         {
             obs->SetCallbackFunction(FuncFileChange, this);
         }

         void ObsStart()
         {
            //監視開始
            obs->Start(_T("c:\\VC"));
         }

         void Print(strFileName)
         {
            表示処理
         }

         //登録用関数
         static void Class::Func(StdString strFileName, void* pVoid)
         {
            Class* pClass = reinterpret_cast<Class*>(pVoid);
            pClass->Print(strFileName);
         }
     };
 */
class CObservationFolder
{
protected:
    HANDLE  m_hDir;
    bool    m_bOvservDir;
    HANDLE  m_hTerminateEvent;
    OVERLAPPED  m_overLap;
    HANDLE  m_hThread;
    unsigned int   m_dwThresdId;
    StdString      m_strPath;

    boost::function<void (StdString, void*) >      m_pFuncCallback;
    void*                                          m_pFuncClass;
  
public:
    CObservationFolder();
    virtual ~CObservationFolder();

    bool Start(StdString strPath);

    bool Stop();

    bool SetCallbackFunction(boost::function<void (StdString, void*) > func,
                             void* pClass);
    

protected:
    friend CObservationFolder;
    bool   _Exec();
    static unsigned int  WINAPI _ThreadObserveFileChange(void * pProj);
    static void  _SendFilechange(StdString strChangeFile,
                                 CObservationFolder* pObsFolder);

};

#endif //_OVSERVATION_FOLDER_H__