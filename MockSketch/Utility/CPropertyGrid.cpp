/**
 * @brief			CPropertyGrid実装ファイル
 * @file		    CPropertyGrid.cpp
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CPropertyGrid.h"
#include "./CStdPropertyTree.h"
#include "./CustomPropertys.h"
#include "System/CSystem.h"
#include "Utility/CUtility.h"
#include "DrawingObject/CDrawingScriptBase.h"
#include "resource.h"
#include "resource2.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


BEGIN_MESSAGE_MAP(CPropertyGrid, CMFCPropertyGridCtrl)
    ON_WM_TIMER()
    ON_WM_SHOWWINDOW()
    ON_WM_CLOSE()
END_MESSAGE_MAP()


/**
 *  @brief   コンストラクタ
 *  @param   なし
 *  @retval  なし
 *  @note
 */
CPropertyGrid::CPropertyGrid():m_pGridData(0)
{

}


/**
 *  @brief   デストラクタ
 *  @param   なし
 *  @retval  なし
 *  @note
 */
CPropertyGrid::~CPropertyGrid()
{

}

/**
 *  @brief   アイテム変更後処理
 *  @param   [in] pProp  変更アイテム
 *  @retval  なし
 *  @note    フレームワークより呼び出し
 *           グリッド->データ
 */
BOOL CPropertyGrid::ValidateItemData(CMFCPropertyGridProperty* pProp)
{
    if (!pProp){return FALSE;}
    if (!m_pGridData)
    {
        STD_DBG(_T("m_pGridData is NULL"));
        return FALSE;
    }

    // バグ回避
    // これを入れないと文字の更新が１回遅れる
    if (!pProp->OnUpdateValue())
    {
        return FALSE;
    }

    int iId;
    iId =  (int)pProp->GetData();

    CStdPropertyItem* pItem;

#ifdef _DEBUG
    TRACE(_T("-----------\n"));
    NAME_TREE<CStdPropertyItem>* pTree;
    pTree = m_pGridData->GetTreeInstance();
    pTree->DspTree();
    TRACE(_T("--- %I64x --------\n"), pTree);
#endif

    pItem = m_pGridData->GetTreeInstance()->GetItemData(iId);
    if (!pItem)
    {
        STD_DBG(_T("pItem is NULL"));
        return FALSE;
    }

    PROPERTY_TYPE type;
    COleVariant oleVal = pProp->GetValue();
    type = pItem->type;

    switch (type)
    {
    case PROP_BOOL:
        {
            bool bVal = (oleVal.boolVal?true:false);
            pItem->anyData.SetVal(bVal);
        }
        break;

    case PROP_INT:
    case PROP_INT_RANGE:
        pItem->anyData.SetVal(oleVal.intVal);
        break;

    case PROP_LINE_TYPE:
    {
        CDropDownExProp* pDrop = dynamic_cast<CDropDownExProp*>(pProp);
        if (pDrop)
        {
            int iVal = pDrop->GetCurId();
            pItem->anyData.SetVal(iVal);
        }

    }
        break;

    case PROP_LINE_WIDTH:
        pItem->anyData.SetVal(oleVal.intVal);
        break;

    case PROP_DOUBLE:
        pItem->anyData.SetVal(oleVal.dblVal);
        break;

    case PROP_COLOR:
        pItem->anyData.SetVal(oleVal.ulVal);
        break;

    case PROP_STR:
    case PROP_DROP_DOWN:
    case PROP_CHCEK_LIST:
    case PROP_FILE_NAME:
    case PROP_SETTING:
	case PROP_TWO_BUTTON:
        {
        StdString strVal = (LPCTSTR)oleVal.bstrVal;
        pItem->anyData.SetVal(strVal);
        }
        break;

    case PROP_DROP_DOWN_BMP:
    case PROP_DROP_DOWN_ID:
        {
            CDropDownExProp* pDrop = dynamic_cast<CDropDownExProp*>(pProp);
            if (pDrop)
            {
                StdString strVal = (LPCTSTR)oleVal.bstrVal;
                bool bRet;
                int iId = 0;
                bRet = pDrop->FindId(&iId, strVal);

                STD_ASSERT(bRet);
                pItem->anyData.SetVal(iId);
            }
        }
        break;

 
    case PROP_POINT2D:
        {
            POINT2D pt2D;

            pt2D = pItem->anyData.GetPoint();
            if ( _tcscmp (_T("X"), pProp->GetName()) == 0)
            {
                pt2D.dX = oleVal.dblVal;
            }
            else
            {
                pt2D.dY = oleVal.dblVal;
            }

            pItem->anyData.SetVal(pt2D);
        }
        break;

    case PROP_RECT2D:
        {
            RECT2D rc2D;

            rc2D = pItem->anyData.GetPoint();
            if ( _tcscmp (_T("Top"), pProp->GetName()) == 0)
            {
                rc2D.dTop = oleVal.dblVal;
            }
            else if ( _tcscmp (_T("Bottom"), pProp->GetName()) == 0)
            {
                rc2D.dBottom = oleVal.dblVal;
            }
            else if ( _tcscmp (_T("Left"), pProp->GetName()) == 0)
            {
                rc2D.dLeft = oleVal.dblVal;
            }
            else if ( _tcscmp (_T("Right"), pProp->GetName()) == 0)
            {
                rc2D.dRight = oleVal.dblVal;
            }

            pItem->anyData.SetVal(rc2D);
        }
        break;

    case PROP_POINT_LIST:
        {
            std::vector<POINT2D> list2D;
            list2D = pItem->anyData.GetPointList();
            pItem->anyData.SetVal(list2D);
        }
        break;

    default:
        STD_DBG(_T("UNKNOWN TYPE %d"), type);
        break;
    }

    void* pParent = m_pGridData->GetParent();
    if (!pParent)
    {
        return FALSE;
    }

    if (pItem->func)
    {
        if (pItem->func(pItem, pParent))
        {
            return TRUE;
        }
    }
    return FALSE;
}

/**
 *  @brief   アイテム設定
 *  @param   [in] pDef  アイテム定義
 *  @retval  なし
 *  @note
 */
CMFCPropertyGridProperty* CPropertyGrid::SetGridItem(TREE_GRID_ITEM* pDsp, 
                                CMFCPropertyGridProperty* pBeforeGridProperty)
{
    CStdPropertyItemDef* pDef;
    CStdPropertyItem*    pStdItem;
    pStdItem = pDsp->pData;

    //データが設定されてる場合は項目の設定を行う
    CMFCPropertyGridProperty* pItem;
    pItem = NULL;


    if (pStdItem == NULL)
    {
        pItem = new CMFCPropertyGridProperty(pDsp->m_str[0].c_str());
        BOOL bRet = pBeforeGridProperty->AddSubItem(pItem);
        STD_ASSERT(bRet);
        return pItem;
    }

    pDef = pStdItem->psDef;

    STD_ASSERT(pBeforeGridProperty);
    STD_ASSERT(pDsp);



    CAny* pAny = NULL;

    if (pStdItem->ePropCategoly == PROP_CATE_USER)
    {
        //ユーザープロパティ値の取得
        CDrawingScriptBase* pBase;
        pBase = reinterpret_cast<CDrawingScriptBase*>(pStdItem->pRealData);
        pAny = pBase->GetUserProperty(pStdItem->GetName());
        
        //STD_ASSERT(pAny);
        if (!pAny)
        {
            STD_DBG(_T("User property not find %s"), pStdItem->GetName().c_str());
            return NULL;
        }
    }
    else if (pStdItem->ePropCategoly == PROP_CATE_PROP_SET)
    {
        //プロパティセット値の取得
        CDrawingObject* pBase;
        pBase = reinterpret_cast<CDrawingObject*>(pStdItem->pRealData);
        pAny = pBase->GetPropertySetVal(pStdItem->GetName());
        
        //STD_ASSERT(pAny);
        if (!pAny)
        {
            STD_DBG(_T("PropertySet not find %s"), pStdItem->GetName().c_str());
            return NULL;
        }
    }

    /*
    if (!pAny && pStdItem && !pStdItem->pRealData)
    {
        return;
    }
    */

    switch(pDef->type)
    {
    case PROP_BOOL:
        {
        //bool bVal = boost::any_cast<bool>(pDef->anyInit);
        //pItem = new CDropBoolProp( pDef->strDspName.c_str(),  
        //                                      bVal,  
        //                                      pDef->strExp.c_str()); 

        bool bVal;
        if (pAny)
        {
            bVal = pAny->GetBool();
        }
        else
        {
            bVal = *(reinterpret_cast<bool*>(pStdItem->pRealData));
        }


        COleVariant varItem( (long)(bVal?1:0) , VT_BOOL);
        pItem = new CMFCPropertyGridProperty( pDef->strDspName.c_str(),  
                                              varItem,  
                                              pDef->strExp.c_str()); 
        pDef->bEnableEdit = false;
        STD_ASSERT(pItem);

        }
        break;

    case PROP_INT:
        {
            long nVal;
            if (pAny)
            {
                nVal = pAny->GetInt();
            }
            else
            {
                nVal = *reinterpret_cast<int*>(pStdItem->pRealData);
            }


            COleVariant varItem( nVal, VT_I4);
            pItem = new CMFCPropertyGridProperty( pDef->strDspName.c_str(),  
                                                  varItem,  
                                                  pDef->strExp.c_str()); 
            STD_ASSERT(pItem);
        }
        break;

    case PROP_INT_RANGE:
        {
        long nVal;
        if (pAny)
        {
            nVal = pAny->GetInt();
        }
        else
        {
            nVal = *reinterpret_cast<int*>(pStdItem->pRealData);
        }
        COleVariant varItem( nVal, VT_I4);

        int iMax = boost::any_cast<int>(pDef->anyMax);
        int iMin = boost::any_cast<int>(pDef->anyMin);
        pItem = new CBoundedNumberSubProp( pDef->strDspName.c_str(),  
                                              varItem,  
                                              iMin,
                                              iMax,
                                              pDef->strExp.c_str()); 

        
        STD_ASSERT(pItem);
        pItem->EnableSpinControl(TRUE, iMin, iMax);

        }
        break;

    case PROP_LINE_TYPE:
        {

        int iVal;
        if (pAny)
        {
            iVal = pAny->GetInt();
        }
        else
        {
            iVal = *reinterpret_cast<int*>(pStdItem->pRealData);
        }


        CDropDownExProp* pDropItem;
        pDropItem = new CDropDownExProp( pDef->strDspName.c_str(),  
                                              iVal,  
                                              pDef->strExp.c_str(),
                                              0,
                                              IDB_LINE_TYPE,
                                              5); 
        pDropItem->AddOption(GET_STR(STR_PRO_LINE_DASHDOTDOT), 4, PS_DASHDOTDOT);
        pDropItem->AddOption(GET_STR(STR_PRO_LINE_DASHDOT)   , 3, PS_DASHDOT);
        pDropItem->AddOption(GET_STR(STR_PRO_LINE_DOT)       , 2, PS_DOT);
        pDropItem->AddOption(GET_STR(STR_PRO_LINE_DASH)      , 1, PS_DASH);
        pDropItem->AddOption(GET_STR(STR_PRO_LINE_SOLID)     , 0, PS_SOLID);
        /*
        pDropItem->AddOption(GET_STR(STR_PRO_LINE_SOLID)     , 0);
        pDropItem->AddOption(GET_STR(STR_PRO_LINE_DASH)      , 1);
        pDropItem->AddOption(GET_STR(STR_PRO_LINE_DOT)       , 2);
        pDropItem->AddOption(GET_STR(STR_PRO_LINE_DASHDOT)   , 3);
        pDropItem->AddOption(GET_STR(STR_PRO_LINE_DASHDOTDOT), 4);
        */
        pDropItem->SetCurSelById(iVal);
        pItem = pDropItem;
        pDef->bEnableEdit = false;
        STD_ASSERT(pItem);

        }
        break;

    case PROP_LINE_WIDTH:
        {

        long nVal;
        if (pAny)
        {
            nVal = pAny->GetInt();
        }
        else
        {
            nVal = *reinterpret_cast<int*>(pStdItem->pRealData);
        }

        COleVariant varItem( nVal, VT_I4);
        pItem = new CBoundedNumberSubProp( pDef->strDspName.c_str(),  
                                              varItem,  
                                              1,
                                              100,
                                              pDef->strExp.c_str()); 


        STD_ASSERT(pItem);
        }
        break;

    case PROP_DROP_DOWN_BMP:
        {
            // PROP_DROP_DOWN_BMPのMIN値は以下の形で実装する
            // RESOURCEID,Str1:id1, Str2:id2, Str3:id3  
            int iVal;
            if (pAny)
            {
                iVal = pAny->GetInt();
            }
            else
            {
                iVal = *reinterpret_cast<int*>(pStdItem->pRealData);
            }


            StdString strMin = boost::any_cast<StdString>(pDef->anyMin);
            std::vector<StdString> lstStr;

            if (CUtil::Last(strMin) == _T(","))
            {
                strMin = strMin.substr(0,strMin.length()-1);
            }


            CUtil::TokenizeCsv(&lstStr, strMin);

            int iResourceId = boost::lexical_cast<int>(lstStr[0]);
            int iSize = static_cast<int>(lstStr.size()) - 1;

            if (iSize < 0)
            {
                iSize = 0;
                iResourceId = 0;
            }

            CDropDownExProp* pDropItem;
            pDropItem = new CDropDownExProp( pDef->strDspName.c_str(),  
                                                  0,  //ここでは初期値を設定できない
                                                  pDef->strExp.c_str(),
                                                  0,
                                                  iResourceId,
                                                  iSize); 

            for(int iCnt = 1; iCnt <= iSize; iCnt++)
            {
                StdString& strItem = lstStr[iCnt];
                std::vector<StdString> lstPair;
                CUtil::TokenizeCsv(&lstPair, strItem, _T(':'));
                //STD_ASSERT( lstPair.size() == 2 );
                if (lstPair.size() == 2)
                {
                    int iIcon = iCnt - 1;
                    int iId = boost::lexical_cast<int>(lstPair[1]);
                    pDropItem->AddOption(lstPair[0].c_str(), iIcon, iId);
                }
            }

            pDropItem->SetCurSelById(iVal);
            pDef->bEnableEdit = false;
            pItem = pDropItem;
            STD_ASSERT(pItem);
        }
        break;


    case PROP_DOUBLE:
        {
        double dVal;
        if (pAny)
        {
            dVal = pAny->GetDouble();
        }
        else
        {
            dVal = *reinterpret_cast<double*>(pStdItem->pRealData);
        }

        COleVariant varItem( dVal );
        pItem = new CMFCPropertyGridProperty( pDef->strDspName.c_str(),  
                                              varItem,  
                                              pDef->strExp.c_str()); 
        }
        STD_ASSERT(pItem);
        break;

    case PROP_POINT_LIST:
        {
        std::vector<POINT2D> lstVal;
        if (pAny)
        {
            lstVal = pAny->GetPointList();
        }
        else
        {
            lstVal = *reinterpret_cast<std::vector<POINT2D>*>(pStdItem->pRealData);
        }

        pItem = new CPointListProp( pDef->strDspName.c_str(),  
                                          lstVal,  
                                          pDef->strExp.c_str()); 
        STD_ASSERT(pItem);
        }
        break;

    case PROP_COLOR:
        {
        COLORREF crVal;
        if (pAny)
        {
            crVal = pAny->GetColor();
        }
        else
        {
            crVal = *reinterpret_cast<COLORREF*>(pStdItem->pRealData);
        }


        CMFCPropertyGridColorProperty* pColorItem;
        pColorItem = new CMFCPropertyGridColorProperty( pDef->strDspName.c_str(),  
                                              crVal,  
                                              NULL,
                                              pDef->strExp.c_str()); 

        pColorItem->EnableOtherButton(GET_STR(STR_PRO_MORE_COLORS));
        pColorItem->EnableAutomaticButton(GET_STR(STR_PRO_AUTOMATIC), 
            ::GetSysColor(COLOR_3DFACE));

        pItem = pColorItem;

        STD_ASSERT(pItem);
        }
        break;


    case PROP_STR:
        {
        StdString strVal;
        if (pAny)
        {
            strVal = pAny->GetString();
        }
        else
        {
            strVal = *reinterpret_cast<StdString*>(pStdItem->pRealData);
        }

        COleVariant varItem( strVal.c_str());
        pItem = new CMFCPropertyGridProperty( pDef->strDspName.c_str(),  
                                              varItem,  
                                              pDef->strExp.c_str()); 
        STD_ASSERT(pItem);
        }
        break;

    case PROP_POINT2D:
        {
        POINT2D ptVal;
        if (pAny)
        {
            ptVal = pAny->GetPoint();
        }
        else
        {
            ptVal = *reinterpret_cast<POINT2D*>(pStdItem->pRealData);
        }

        //ポイントは２つの項目を持つためここで生成する
        CMFCPropertyGridProperty* pTmpGridProperty;

        pTmpGridProperty = new CMFCPropertyGridProperty(pDsp->m_str[0].c_str());

        //初期値設定

        STD_ASSERT( pBeforeGridProperty != NULL);

        COleVariant varItemX( ptVal.dX );  
        COleVariant varItemY( ptVal.dY );  
        pItem = new CMFCPropertyGridProperty( _T("X"),  
                                              varItemX,  
                                              GET_STR(STR_PROPWIN_X)); 
        pTmpGridProperty->AddSubItem(pItem);
        pItem->AllowEdit(pDef->bEnableEdit);  
        pItem->SetData((DWORD_PTR)pDsp->iId);

        pItem = new CMFCPropertyGridProperty( _T("Y"),  
                                              varItemY,  
                                              GET_STR(STR_PROPWIN_Y)); 
        pTmpGridProperty->AddSubItem(pItem);
        pItem->AllowEdit(pDef->bEnableEdit);  
        pItem->SetData((DWORD_PTR)pDsp->iId + 1);

        CStdPropertyItem* pPropItem;
        pPropItem = m_pGridData->GetTreeInstance()->GetItemData(pDsp->iId);
        pPropItem->anyData.SetVal(ptVal);


        pBeforeGridProperty->AddSubItem(pTmpGridProperty);
        STD_ASSERT(pItem);

        pItem = NULL;

        }
        break;

    case PROP_RECT2D:
        {
        RECT2D rcVal;
        if (pAny)
        {
            rcVal = pAny->GetRect();
        }
        else
        {
            rcVal = *reinterpret_cast<RECT2D*>(pStdItem->pRealData);
        }

        //RECTは４つの項目を持つためここで生成する
        CMFCPropertyGridProperty* pTmpGridProperty;

        pTmpGridProperty = new CMFCPropertyGridProperty(pDsp->m_str[0].c_str());

        //初期値設定

        STD_ASSERT( pBeforeGridProperty != NULL);

        COleVariant varItemTop      ( rcVal.dTop );  
        COleVariant varItemBottom   ( rcVal.dBottom );  
        COleVariant varItemLeft     ( rcVal.dLeft );  
        COleVariant varItemRight    ( rcVal.dRight );  
        pItem = new CMFCPropertyGridProperty( _T("Top"),  
                                              varItemTop,  
                                              GET_STR(STR_PRO_TOP)); 
        STD_ASSERT(pItem);
        pTmpGridProperty->AddSubItem(pItem);
        pItem->AllowEdit(pDef->bEnableEdit);  
        pItem->SetData((DWORD_PTR)pDsp->iId);

        pItem = new CMFCPropertyGridProperty( _T("Bottom"),  
                                              varItemBottom,  
                                              GET_STR(STR_PRO_BOTTOM)); 
        STD_ASSERT(pItem);
        pTmpGridProperty->AddSubItem(pItem);
        pItem->AllowEdit(pDef->bEnableEdit);  
        pItem->SetData((DWORD_PTR)pDsp->iId + 1);


        pItem = new CMFCPropertyGridProperty( _T("Left"),  
                                              varItemLeft,  
                                              GET_STR(STR_PRO_LEFT)); 
        STD_ASSERT(pItem);
        pTmpGridProperty->AddSubItem(pItem);
        pItem->AllowEdit(pDef->bEnableEdit);  
        pItem->SetData((DWORD_PTR)pDsp->iId + 2);

        pItem = new CMFCPropertyGridProperty( _T("Right"),  
                                              varItemRight,  
                                              GET_STR(STR_PRO_RIGHT)); 
        STD_ASSERT(pItem);
        pTmpGridProperty->AddSubItem(pItem);
        pItem->AllowEdit(pDef->bEnableEdit);  
        pItem->SetData((DWORD_PTR)pDsp->iId + 3);

        pBeforeGridProperty->AddSubItem(pTmpGridProperty);

        CStdPropertyItem* pPropItem;
        pPropItem = m_pGridData->GetTreeInstance()->GetItemData(pDsp->iId);
        pPropItem->anyData.SetVal(rcVal);


        pItem = NULL;

        }
        break;

    case PROP_DROP_DOWN:
        {
            StdString strVal;
            if (pAny)
            {
                strVal = pAny->GetString();
            }
            else
            {
                strVal = *reinterpret_cast<StdString*>(pStdItem->pRealData);
            }

            std::vector<StdString> lstStr;

            CDropDownExProp* pDropItem;
            pDropItem = new CDropDownExProp( pDef->strDspName.c_str(),  // 項目名
                                                  0,                    // 選択位置
                                                  pDef->strExp.c_str(), // 項目説明
                                                  0,                    // データ識別値
                                                  0,                    // リソースID
                                                  0);                   // リソース画像の分割数

            //ドロップダウンリストはMINにリスト値を設定する
            StdString strMin = boost::any_cast<StdString>(pDef->anyMin);

            if (CUtil::Last(strMin) == _T(","))
            {
                strMin = strMin.substr(0,strMin.length()-1);
            }

            CUtil::TokenizeCsv(&lstStr, strMin);
            int iCnt = 0;
            int iSel = 0;

            foreach(StdString strItem, lstStr)
            {
                pDropItem->AddOption(strItem.c_str());
            }

            pDropItem->SetStr(strVal.c_str());

            pDef->bEnableEdit = false;
            pItem = pDropItem;
            STD_ASSERT(pItem);
        }
        break;

    case PROP_DROP_DOWN_ID:
        {
            // PROP_DROP_DOWN_IDのMIN値は以下の形で実装する
            // Str1:id1, Str2:id2, Str3:id3  
            int iVal;
            if (pAny)
            {
                iVal = pAny->GetInt();
            }
            else
            {
                iVal = *reinterpret_cast<int*>(pStdItem->pRealData);
            }

            StdString strMin = boost::any_cast<StdString>(pDef->anyMin);
            std::vector<StdString> lstStr;

            CDropDownExProp* pDropItem;
            pDropItem = new CDropDownExProp( pDef->strDspName.c_str(),  
                                                  0,  //ここでは初期値を設定できない
                                                  pDef->strExp.c_str(),
                                                  0,
                                                  0,
                                                  0); 

            if (CUtil::Last(strMin) == _T(","))
            {
                strMin = strMin.substr(0,strMin.length()-1);
            }
            CUtil::TokenizeCsv(&lstStr, strMin);
            int iCnt = 0;
            int iSel = 0;

            foreach(StdString strItem, lstStr)
            {
                std::vector<StdString> lstPair;
                CUtil::TokenizeCsv(&lstPair, strItem, _T(':'));
                //STD_ASSERT( lstPair.size() == 2 );
                if (lstPair.size() == 2)
                {
                    int iId = boost::lexical_cast<int>(lstPair[1]);
                    pDropItem->AddOptionId(lstPair[0].c_str(), iId);
                }
            }

            pDropItem->SetCurSelById(iVal);
            pDef->bEnableEdit = false;
            pItem = pDropItem;
            STD_ASSERT(pItem);
        }
        break;


   case PROP_CHCEK_LIST:
        {
            StdString strVal;
            if (pAny)
            {
                strVal = pAny->GetString();
            }
            else
            {
                strVal = *reinterpret_cast<StdString*>(pStdItem->pRealData);
            }



            CDropCheckList* pDropCheckList;
            pDropCheckList = new CDropCheckList( pDef->strDspName.c_str(),  // 項目名
                                                  0,                    // 選択位置
                                                  pDef->strExp.c_str(), // 項目説明
                                                  0);                   // データ識別値                    // リソースID

            //チェックリストはMINにリスト値を設定する
            StdString strMin = boost::any_cast<StdString>(pDef->anyMin);

            std::vector<StdString> lstStr;
            CUtil::TokenizeCsv(&lstStr, strMin);

            std::vector<StdString> lstSetVal;
            CUtil::TokenizeCsv(&lstSetVal, strVal);

            int iCnt = 0;
            int iSel = 0;

            foreach(StdString strItem, lstStr)
            {
                bool bExist = false;
                auto ite = std::find(lstSetVal.begin(),lstSetVal.end(),strItem);

                if (ite != lstSetVal.end())
                {
                    bExist = true;
                }

                pDropCheckList->AddOption(strItem.c_str(), bExist);
            }


            pDef->bEnableEdit = false;
            pItem = pDropCheckList;
            STD_ASSERT(pItem);
        }
        break;

   case PROP_SETTING:
        {
            StdString strVal;
            if (pAny)
            {
                strVal = pAny->GetString();
            }
            else
            {
                strVal = *reinterpret_cast<StdString*>(pStdItem->pRealData);
            }
            

            //MINにダイアログを設定する
            CDialog* pDialog = boost::any_cast<CDialog*>(pDef->anyMin);


            CCustomDlgProp* pCustonDlg;
            pCustonDlg = new CCustomDlgProp( pDef->strDspName.c_str(),  // 項目名
                                                  strVal.c_str(),           // 値
                                                  pDef->strExp.c_str(),     // 項目説明
                                                  0,                         // データ識別値 
                                                  pDialog
                                                  );                                     


            pDef->bEnableEdit = false;
            pItem = pCustonDlg;
            STD_ASSERT(pItem);
        }
        break;

   case PROP_TWO_BUTTON:
	   {
		   StdString strVal;
		   if (pAny)
		   {
			   strVal = pAny->GetString();
		   }
		   else
		   {
			   strVal = *reinterpret_cast<StdString*>(pStdItem->pRealData);
		   }

		   //MINにコールバックを設定する
		   auto pfunc = boost::any_cast<std::function<void(bool)>>(pDef->anyMin);

		   CTwoButtonsProp* pTwoButton;
		   pTwoButton = new CTwoButtonsProp(pDef->strDspName.c_str(),  // 項目名,
			   strVal.c_str(),           // 値,
			   pDef->strExp.c_str(),     // 項目説明,
			   0,                         // データ識別値 
			   pfunc);

		   pDef->bEnableEdit = false;
		   pItem = pTwoButton;
		   STD_ASSERT(pItem);
   }

    case PROP_FILE_NAME:
        {
        StdString strFile;
        if (pAny)
        {
            strFile = pAny->GetString();
        }
        else
        {
            strFile = *reinterpret_cast<StdString*>(pStdItem->pRealData);
        }
        

        //MINにフィルター値を設定する
        StdString strFilter = boost::any_cast<StdString>(pDef->anyMin);

        //MAXにフォルダーを設定する
        StdString strFolder = boost::any_cast<StdString>(pDef->anyMax);

        pItem = new CFileProperty( pDef->strDspName.c_str(),    // グループの名前
                                   TRUE, //bOpenFileDialog,     // TRUE ファイルを開く
                                   strFile.c_str(),             // ファイル名
                                   strFolder.c_str(),           // フォルダー名
                                   NULL,                        // 規定のファイル名の拡張子 
                                                                // OFN 列挙体フラグ OFN_HIDEREADONLY とか
                                   OFN_NOCHANGEDIR|             // 現在のディレクトリを変更できません
                                   OFN_PATHMUSTEXIST	        // 有効なパスとファイル名のみを入力できます。
                                    ,            
                                   strFilter.c_str(),            // フィルター  例→"JPEG Files (*.jpg;*.jpeg)|*.jpg; *.jpeg||"
                                   pDef->strExp.c_str(),          // プロパティの説明
                                   0) ;                           // アプリケーション固有のデータ 

        STD_ASSERT(pItem);
        }
        break;


    default:
        STD_DBG(_T("type = %d"), pDef->type);
        break;

    }

    if (pItem != NULL)
    {
        pItem->SetData((DWORD_PTR)pDsp->iId);

        if (pDef->type != PROP_BOOL)
        {
            pItem->AllowEdit(pDef->bEnableEdit);
        }

        BOOL bRet;
        //子項目をプロパティに追加します。
        bRet = pBeforeGridProperty->AddSubItem(pItem);
#if 0
DB_PRINT(_T("Group:%s Item %s\n"), pBeforeGridProperty->GetName(),
                                pItem->GetName());
#endif
        STD_ASSERT(bRet);
    }
    return pItem;
}



/**
 *  @brief   グリッドデータ設定
 *  @param   [in] pItem                 追加する グリッドデータ
 *  @param   [in] ppBeforeGridProperty  追加元（親、兄）となるデータ
 *  @retval  なし
 *  @note    再帰
 */

void  CPropertyGrid::SetGridData(TREE_GRID_ITEM* pItem, 
                                 CMFCPropertyGridProperty** ppBeforeGridProperty)
{

    CMFCPropertyGridProperty* pGridProperty = NULL;

    if(pItem == NULL)
    {
        return;
    }

    CMFCPropertyGridProperty* pNewItem;
    if (!ppBeforeGridProperty)
    {
        //ppBeforeGridPropertyが設定されていないときはROOT
        pGridProperty = new CMFCPropertyGridProperty(pItem->m_str[0].c_str());
        pNewItem = pGridProperty;

    }
    else
    {

        pGridProperty = *ppBeforeGridProperty;
        pNewItem = SetGridItem(pItem, pGridProperty);
    }

    if (pItem->spChild)
    {
        if( pNewItem)
        {
            SetGridData(pItem->spChild, &pNewItem);
        }
    }


    if (pItem->spNext)
    {
        SetGridData(pItem->spNext, &pGridProperty);
    }

    if (!ppBeforeGridProperty)
    {
        int iRet;
        iRet = CMFCPropertyGridCtrl::AddProperty(pGridProperty);
    }

}


/**
 *  @brief   グリッドデータ設定
 *  @param   [in] pData  グリッドデータ
 *  @retval  なし
 *  @note
 */
bool  CPropertyGrid::SetGridData(CStdPropertyTree* pData)
{
    boost::recursive_mutex::scoped_lock lk(m_mtx);
    STD_ASSERT(pData != NULL);
    m_pGridData = pData;

    NAME_TREE<CStdPropertyItem>* pTree = pData->GetTreeInstance(); 


    std::deque<  CMFCPropertyGridProperty* >  listCurItem;

    NAME_TREE_ITEM<CStdPropertyItem>*  pDsp;

    pDsp = pTree->GetRoot();

    //ppBeforeGridPropertyが設定されていないときはROOT

    SetGridData(pDsp, NULL);
    return true;
}

/**
 *  @brief   プロパティ更新
 *  @param   
 *  @retval  なし
 *  @note
 */
void  CPropertyGrid::UpdateProperty()
{
    boost::recursive_mutex::scoped_lock lk(m_mtx);

    if (m_pGridData)
    {
        m_pGridData->UpdatePropertyGrid(this, true /*bSetVisibleOnly*/);
    }
}

void CPropertyGrid::OnTimer(UINT_PTR nIDEvent)
{
    CMFCPropertyGridCtrl::OnTimer(nIDEvent);
    if (nIDEvent != ONT_PROPERTY)
    {
        return;
    }

    UpdateProperty();
}


void CPropertyGrid::OnShowWindow(BOOL bShow, UINT nStatus)
{
    CMFCPropertyGridCtrl::OnShowWindow(bShow, nStatus);

    if (bShow)
    {
        SetTimer(ONT_PROPERTY, SYS_CONFIG->dwTimerProperty, NULL);
    }
    else
    {
        KillTimer(ONT_PROPERTY);
    }
    // TODO: ここにメッセージ ハンドラー コードを追加します。
}


void CPropertyGrid::OnClose()
{
    KillTimer(ONT_PROPERTY);
    CMFCPropertyGridCtrl::OnClose();
}


BOOL CPropertyGrid::EditItem(CMFCPropertyGridProperty* pProp, LPPOINT lptClick)
{
    BOOL bRet;
    bRet = CMFCPropertyGridCtrl::EditItem(pProp, lptClick);
    KillTimer(ONT_PROPERTY);
    return bRet;
}

BOOL CPropertyGrid::EndEditItem(BOOL bUpdateData)
{
    BOOL bRet;
    bRet = CMFCPropertyGridCtrl::EndEditItem(bUpdateData);

    SetTimer(ONT_PROPERTY, SYS_CONFIG->dwTimerProperty, NULL);

    return bRet;
}
