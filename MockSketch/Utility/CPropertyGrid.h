/**
 * @brief			CPropGridインクルードファイル
 * @file			CPropGrid.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */


#ifndef  _PROPERTY_GRID_H__
#define  _PROPERTY_GRID_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "Utility/CStdPropertyTree.h"             

/*---------------------------------------------------*/
/*  Class                                            */
/*---------------------------------------------------*/
class CStdPropertyTree;

/**
 * @class   CPropertyGrid
 * @brief                        
 */
class CPropertyGrid : public CMFCPropertyGridCtrl
{
public:

    CPropertyGrid();

    virtual ~CPropertyGrid();

    CMFCPropertyGridProperty*  SetGridItem(TREE_GRID_ITEM* pDsp, CMFCPropertyGridProperty* pGridProperty);

    void  SetGridData(TREE_GRID_ITEM* pItem, CMFCPropertyGridProperty** ppGridProperty);

    bool  SetGridData(CStdPropertyTree* pData);

    void  UpdateProperty();

    void  PauseUpdate();

    void  ResumeUpdate();

    virtual BOOL EditItem(CMFCPropertyGridProperty* pProp, LPPOINT lptClick = NULL);

    virtual BOOL EndEditItem(BOOL bUpdateData = TRUE);

protected:
    virtual BOOL ValidateItemData(CMFCPropertyGridProperty* pProp);


protected:
    CStdPropertyTree* m_pGridData;

    mutable boost::recursive_mutex   m_mtx;
public:
    DECLARE_MESSAGE_MAP()
    afx_msg void OnTimer(UINT_PTR nIDEvent);
    afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
    afx_msg void OnClose();
};

#endif //_PROPERTY_GRID_H__