/**
 * @brief			CPropertyGroup実装ファイル
 * @file			CPropertyGroup.cpp
 * @author			Yasuhiro Sato
 * @date			01-2-2009 0:56:53
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CPropertyGroup.h"

#include "System/CSystem.h"
#include "System/MOCK_ERROR.h"
#include "View/ViewCommon.h"
#include <boost/archive/text_oarchive.hpp> // テキスト形式アーカイブに書き込み

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif
 
/*---------------------------------------------------*/
/* Static variables                                  */
/*---------------------------------------------------*/


////////////////////////////////////////////////////////////////////
// CPropertySet

/**
 *  @brief   コピーコンストラクタ
 *  @param   [in] m コピー元
 *  @retval  なし
 *  @note
 */
CPropertySet::CPropertySet(const CPropertySet& m):
m_iChgPropUserCnt(0)
{
    foreach(std::shared_ptr<CStdPropertyItemDef> pDef, m.m_lstPropertyItem)
    {
        std::shared_ptr<CStdPropertyItemDef> pNewDef;
        pNewDef = std::make_shared<CStdPropertyItemDef>(*pDef);
        m_lstPropertyItem.push_back(pNewDef);
    }
}

/**
 *  @brief   デストラクタ
 *  @param   なし
 *  @retval  なし
 *  @note
 */
CPropertySet::~CPropertySet()
{
    m_lstPropertyItem.clear();

}

CPropertySet& CPropertySet::operator = (const CPropertySet & m)
{
    m_lstPropertyItem.clear();
    foreach(std::shared_ptr<CStdPropertyItemDef> pDef, m.m_lstPropertyItem)
    {
        std::shared_ptr<CStdPropertyItemDef> pNewDef;
        pNewDef = std::make_shared<CStdPropertyItemDef>(*pDef);
        m_lstPropertyItem.push_back(pNewDef);
    }
    m_iChgPropUserCnt = 0;

    return *this;
}

bool CPropertySet::operator == (const CPropertySet & m) const
{
    if(m_lstPropertyItem.size() != m.m_lstPropertyItem.size())
    {
        return false;
    }

    int iCnt = 0;
    foreach(std::shared_ptr<CStdPropertyItemDef> pDef, m.m_lstPropertyItem)
    {
        if((*m_lstPropertyItem[iCnt].get() != *(pDef.get())))
        {
            return false;
        }
        iCnt++;
    }

    return true;
}

bool CPropertySet::operator != (const CPropertySet & m) const
{
    return !(*this == m);
}

/**
 * @brief   定義データ設定
 * @param   pPropDef    定義データ
 * @retval  true 成功
 * @note    定義データはこのクラスで保持する
 */
bool CPropertySet::Add(std::shared_ptr<CStdPropertyItemDef> pPropDef)
{
    if (pPropDef == NULL)
    {
        return false;
    }
    m_lstPropertyItem.push_back(pPropDef);
    AddChgCnt();
    return true;
}

/**
 * @brief   定義データ挿入
 * @param   [in] iRow     挿入行
 * @param   [in] pPropDef 定義データ
 * @retval  true 成功
 * @note    定義データはこのクラスで保持する
 *          iRowの前に挿入
 */
bool CPropertySet::Insert(int iRow, std::shared_ptr<CStdPropertyItemDef> pPropDef)
{
    if (pPropDef == NULL)
    {
        return false;
    }

    if(iRow < 0)
    {
        return false;
    }

    if (iRow >= static_cast<int>(m_lstPropertyItem.size()))
    {
        return false;
    }
    std::vector<std::shared_ptr<CStdPropertyItemDef>>::iterator ite;
    
    ite = m_lstPropertyItem.begin() + iRow;

    m_lstPropertyItem.insert(ite, pPropDef);
    AddChgCnt();
    return true;
}

/**
 * @brief   定義データ取得
 * @param   [in] iNo
 * @retval  定義データへのポインタ
 * @note	
 */
CStdPropertyItemDef* CPropertySet::GetInstance(int iNo)
{
    STD_ASSERT(iNo >=  0);
    STD_ASSERT(iNo < static_cast<int>(m_lstPropertyItem.size()));

    if (iNo < 0){return NULL;}
    if (iNo >= static_cast<int>(m_lstPropertyItem.size())){return NULL;}
    return m_lstPropertyItem[iNo].get();
}

/**
 * @brief   定義データ取得
 * @param   [in] iNo
 * @param   [in] eCol
 * @retval  定義データへのポインタ
 * @note	
 */
StdString CPropertySet::GetItemString(int iNo, E_PROPERTY_ITEM_SET eCol)
{
    StdString strRet;
    std::shared_ptr<CStdPropertyItemDef> pDef;
    if (iNo <  0)                 {return NULL;}
    if (iNo >= static_cast<int>(m_lstPropertyItem.size())) {return NULL;}

    pDef = m_lstPropertyItem[iNo];
    strRet = pDef->GetItemString( eCol);
    return strRet;
}

/**
 * @brief   定義データ設定
 * @param   [in] iNo
 * @param   [in] eCol
 * @param   [in] strDef
 * @retval  設定成功
 * @note	
 */
bool CPropertySet::Set(int iNo, E_PROPERTY_ITEM_SET eCol, StdString strDef)
{
    bool bRet;
    std::shared_ptr<CStdPropertyItemDef> pItemDef;
    if (iNo <  0)                 {return false;}
    if (iNo >= static_cast<int>(m_lstPropertyItem.size())) {return false;}

    pItemDef = m_lstPropertyItem[iNo];

    AddChgCnt();

    bRet = pItemDef->SetItemString( eCol, strDef);

    return bRet;
}

/**
 * @brief   定義データ削除
 * @param   [in] iNo
 * @retval  true 成功
 * @note	
 */
bool CPropertySet::Delete(int iNo)
{
    if (iNo <  0)                 {return false;}
    if (iNo >= static_cast<int>(m_lstPropertyItem.size())) {return false;}

    std::vector<std::shared_ptr<CStdPropertyItemDef>>::iterator ite;

    ite = m_lstPropertyItem.begin() + iNo;

    m_lstPropertyItem.erase(ite);

    AddChgCnt();
    return true;
}

/**
 * @brief   定義データ削除
 * @param   なし
 * @retval  定義データへのポインタ
 * @note	
 */
void CPropertySet::DeleteAll()
{
    m_lstPropertyItem.clear();

    AddChgCnt();
}

/**
 * @brief   ユーザ定義位置交換
 * @param   [in] iSrc
 * @param   [in] iDst
 * @retval  true 成功
 * @note	
 */
bool CPropertySet::Swap(int iSrc, int iDst)
{
    STD_ASSERT(iSrc >=  0);
    STD_ASSERT(iSrc < static_cast<int>(m_lstPropertyItem.size()));
    STD_ASSERT(iDst >=  0);
    STD_ASSERT(iDst < static_cast<int>(m_lstPropertyItem.size()));

    if (iSrc < 0){return false;}
    if (iSrc >= static_cast<int>(m_lstPropertyItem.size())){return false;}
    if (iDst < 0){return false;}
    if (iDst >= static_cast<int>(m_lstPropertyItem.size())){return false;}
 
    if (m_lstPropertyItem[iSrc]->type == PROP_NONE){return false;}
    if (m_lstPropertyItem[iDst]->type == PROP_NONE){return false;}

    std::swap(m_lstPropertyItem[iSrc], m_lstPropertyItem[iDst]);
    AddChgCnt();
    return true;
}

/**
 * @brief   ユーザ定義位置移動
 * @param   [in] iSrc
 * @param   [in] iDst
 * @retval  true 成功
 * @note	
 */
bool CPropertySet::Move(int iSrc, int iDst)
{
    STD_ASSERT(iSrc >=  0);
    STD_ASSERT(iSrc < static_cast<int>(m_lstPropertyItem.size()));
    STD_ASSERT(iDst >=  0);
    STD_ASSERT(iDst < static_cast<int>(m_lstPropertyItem.size()));

    if (iSrc < 0){return false;}
    if (iSrc >= static_cast<int>(m_lstPropertyItem.size())){return false;}
    if (iDst < 0){return false;}
    if (iDst >= static_cast<int>(m_lstPropertyItem.size())){return false;}
 
    if (m_lstPropertyItem[iSrc]->type == PROP_NONE){return false;}


    std::shared_ptr<CStdPropertyItemDef> pPropDef;
    pPropDef = m_lstPropertyItem[iSrc];
    std::vector<std::shared_ptr<CStdPropertyItemDef>>::iterator ite;
    
    ite = m_lstPropertyItem.begin() + iDst;

    m_lstPropertyItem.insert(ite, pPropDef);

    if (iSrc > iDst)
    {
        iSrc++;
    }

    ite = m_lstPropertyItem.begin() + iSrc;
    m_lstPropertyItem.erase(ite);

    AddChgCnt();
    return true;
}

/**
*  @brief  ユーザ定義チェック
*  @param  なし
*  @retval なし     
*  @note   最終行が設定されたとき、新たに入力用の行を追加する
*/
bool CPropertySet::Check()
{
    //!< 最終行を変更した場合
    int iMax = static_cast<int>(m_lstPropertyItem.size()) - 1;

    if (iMax >= 0)
    {
        if (m_lstPropertyItem[iMax]->type == PROP_NONE)
        {
            return true;
        }
    }
 
    std::shared_ptr<CStdPropertyItemDef> pDef = std::make_shared <CStdPropertyItemDef>();
    m_lstPropertyItem.push_back(pDef);

    return false;
}

/**
 *  @brief  全ユーザ定義チェック
 *  @param  [out] pNo         エラー発生アイテム番号
 *  @param  [out] pErrorCol   エラー発生列
 *  @param  [out] pError      エラー文字列
 *  @retval false エラーあり 
 *  @note   
 */
bool CPropertySet::CheckAll(int* pNo, 
                                 E_PROPERTY_ITEM_SET* pErrorCol, 
                                 StdString* pError)
{
    Check();

    int iMax = static_cast<int>(m_lstPropertyItem.size()) - 1;
 
    std::shared_ptr<CStdPropertyItemDef> pDef;

    for (int iNo = 0; iNo < iMax; iNo++)
    {
        pDef = m_lstPropertyItem[iNo];

        if (!pDef->CheckItem(pErrorCol, pError))
        {
            *pNo = iNo;
            return false;
        }
    }
    return true;
}

/**
 * @brief   ユーザ定義数取得
 * @param   なし
 * @retval  定義数
 * @note	
 */
int CPropertySet::GetCnt() const
{
    return static_cast<int>(m_lstPropertyItem.size());
}

/**
 * @brief   ユーザ変更カウント取得
 * @param   なし
 * @retval  定義数
 * @note	
 */
int CPropertySet::GetChgCnt() const
{
    return m_iChgPropUserCnt;
}

//!< 変更カウント取得
bool CPropertySet::IsChgEachItem() const
{

    foreach(const std::shared_ptr<CStdPropertyItemDef> pDef, m_lstPropertyItem)
    {
        if( pDef->GetChgCnt() != 0)
        {
            return true;
        }
    }
    return false;
}


/**
 * @brief   ユーザ変更カウント追加
 * @param   なし
 * @retval  定義数
 * @note	
 */
int CPropertySet::AddChgCnt()
{
    m_iChgPropUserCnt++;
    if (m_iChgPropUserCnt >= INT_MAX)
    {
        m_iChgPropUserCnt = 0;
    }
    return m_iChgPropUserCnt;
}




////////////////////////////////////////////////////////////
CPropertyGroup::CPropertyGroup()
{
}

CPropertyGroup::~CPropertyGroup()
{

}

/**
 * @brief   プロパティセット取得
 * @param   [out] pListDef
 * @param   [in]  strName
 * @return	true:成功
 * @note	 
 */
bool CPropertyGroup::GetPropertySet(CPropertySet* pListDef,
                    StdString strName) const
{

    std::map<StdString, CPropertySet >::const_iterator iteMap;
    
    iteMap = m_mapPropertyGrop.find(strName);

    if (iteMap == m_mapPropertyGrop.end())
    {
        return false;
    }

    *pListDef = (iteMap->second);

    return true;
}


bool CPropertyGroup::SaveFile(StdPath path, bool bDispErrorDialog)
{
    StdString m_strName;
    StdPath   m_pathFileName;

    bool bRet = true;
    StdString strError;
    boost::filesystem::ofstream outFs;
    try
    {
        outFs.open(path);
        //boost::archive::text_oarchive oa(outFs);

        // ファイルに書き出し
        std::map<StdString, CPropertySet >::iterator ite;
        CPropertySet* pSet;


        for(ite  = m_mapPropertyGrop.begin();
            ite != m_mapPropertyGrop.end();
            ite++)
        {
            outFs << CUtil::StringToChar(ite->first);
            outFs << std::endl;
            outFs << "{";
            outFs << std::endl;
            pSet = &(ite->second);
            int iSize = pSet->GetCnt();

            CStdPropertyItemDef* pDef;
            for (int iNo = 0; iNo < iSize; iNo++)
            {
               pDef = pSet->GetInstance(iNo);
               if (pDef->type == PROP_NONE)
               {
                   continue;
               }
               StdString strDef;
               std::string strOut; 
               strDef = pDef->ToString();
               strOut = CUtil::StringToChar(strDef);
               outFs <<  strOut;
               //outFs <<  CUtil::StringToChar(pDef->ToString());
               //outFs << std::endl;
            }
            outFs << "}";
            outFs << std::endl;
        }
        m_pathFileName = path;
    }
    catch(std::exception& e )
    {
        //ファイル%sの書き込みに失敗しました;
        strError = MockException::FileErrorMessage(false, path, 
                                    CUtil::CharToString(e.what()));

        DispError(bDispErrorDialog, strError.c_str());
        bRet = false;
    }
    catch(...)
    {
        strError = MockException::FileErrorMessage(false, path);
        DispError(bDispErrorDialog, strError.c_str());
        bRet = false;
    }
    outFs.close();
    return bRet;
}

/**
 * @brief   File読み込み
 * @param   [in] path
 * @return	なし
 * @note	 
 */
bool CPropertyGroup::LoadFile(StdPath path, bool bDispErrorDialog)
{
    /*
    Name
    {
        CStdPropertyItemDef
        CStdPropertyItemDef
        CStdPropertyItemDef
        CStdPropertyItemDef
    }

    Name
    {

    }
    */

    boost::filesystem::ifstream inFs;
    StdStringStream strStream;
    std::string  strLine;
    StdString    strwLine;
    bool bRet = true;
    
    inFs.open( path ); 
    if( inFs.fail() ) 
    { 
        return false; 
    } 
    
    std::map<StdString, CPropertySet > m_mapTmpPropertyGrop;
    std::map<StdString, CPropertySet >::iterator iteFind;

    int iLine = 0;
    StdString strTrim;
    StdString strName;
    StdString strErr;

    int iSts = 0;
    try
    {
        while( !inFs.eof() ) 
        {
            iLine++;
            std::getline(inFs, strLine);
            strwLine = CUtil::CharToString(strLine.c_str());
            strTrim = CUtil::Trim(strwLine);
            if (strTrim == _T(""))
            {
                continue;
            }

            if (strTrim[0] == _T(';'))
            {
                continue;
            }
            
            switch(iSts)
            {
            case 0:
            {
                strName = strTrim;
                CPropertySet porpSet;


                //重複チェック
                bool bDuf = false;
                iteFind = m_mapPropertyGrop.find(strName);
                if (iteFind != m_mapPropertyGrop.end())
                {
                    bDuf = true;
                }

                //重複しているファイルは上書きする
                if (bDuf)
                {
                    StdString strError;
                    strError = GET_ERR_STR(e_dup_name_s);



                    //MockException(e_dup_name_s, strError.c_str(), strName.c_str());
                }

                m_mapTmpPropertyGrop[strName] = porpSet;
                iSts = 1;
            }
            break;

            case 1:
                if (strTrim != _T("{"))
                {
                    MOCK_EXCEPTION(e_expect_braces);
                }
                iSts = 2;
                break;

            case 2:
            {
                if (strTrim == _T("}"))
                {
                    iSts = 0;
                }
                else
                {
                    std::shared_ptr<CStdPropertyItemDef> pPropDef;
                    pPropDef = std::make_shared<CStdPropertyItemDef>();
                    E_ERR eno;
                    eno = pPropDef->FromString(strTrim);
                    if (eno != e_no_error)
                    {
                        MOCK_EXCEPTION(eno);
                    }
                    else
                    {
                        m_mapTmpPropertyGrop[strName].Add(pPropDef);
                    }
                }
                break;
            }
            default:
                break;
            }
        }
    }
    catch(MockException& e)
    {
        //ファイルの読み込みに失敗しました    
        strErr += GET_STR(STR_ERROR_FILE_LOAD);
        strErr += _T("\n");

        strErr += e.GetErrMsg();
        strErr += _T("\n");

        //T("ファイル名:%s");
        strErr += CUtil::StrFormat( GET_STR(STR_MB_FILENAME_s),  path.c_str());
        strErr += _T("\n");

        //_T("行:%d");
        strErr += CUtil::StrFormat( GET_STR(STR_MB_LINE_NO_d),  iLine);

        DispError(bDispErrorDialog, strErr.c_str());
        bRet = false;

    }
    catch(boost::system::system_error &e)
    {
        //ファイルの読み込みに失敗しました    
        strErr += GET_STR(STR_ERROR_FILE_LOAD);
        strErr += _T("\n");

        strErr +=  CUtil::CharToString(e.what());
        strErr += _T("\n");

        //T("ファイル名:%s");
        strErr += CUtil::StrFormat( GET_STR(STR_MB_FILENAME_s),  path.c_str());
        strErr += _T("\n");

        //_T("行:%d");
        strErr += CUtil::StrFormat( GET_STR(STR_MB_LINE_NO_d),  iLine);

        DispError(bDispErrorDialog, strErr.c_str());

        bRet = false;
    }
    catch(...)
    {
        //ファイルの読み込みに失敗しました    
        strErr += GET_STR(STR_ERROR_FILE_LOAD);
        strErr += _T("\n");

        //T("ファイル名:%s");
        strErr += CUtil::StrFormat( GET_STR(STR_MB_FILENAME_s),  path.c_str());
        strErr += _T("\n");

        //_T("行:%d");
        strErr += CUtil::StrFormat( GET_STR(STR_MB_LINE_NO_d),  iLine);

        DispError(bDispErrorDialog, strErr.c_str());

        bRet = false;
    }

    std::map<StdString, CPropertySet >::iterator ite;
    if (bRet)
    {
        m_pathFileName = path;
        m_mapPropertyGrop.clear();
        for(ite  = m_mapTmpPropertyGrop.begin();
            ite != m_mapTmpPropertyGrop.end();
            ite++)
        {
            m_mapPropertyGrop[ite->first] = ite->second;
        }
    }
    return bRet;
}

void CPropertyGroup::GetPropertySetNameList(std::vector<StdString>* pList) const
{
    STD_ASSERT(pList);
    std::map<StdString, CPropertySet >::const_iterator iteMap;

    pList->clear();
         
    for (iteMap  = m_mapPropertyGrop.begin();
         iteMap != m_mapPropertyGrop.end();
         iteMap++)
    {
        pList->push_back(iteMap->first);
    }
}

CPropertySet* CPropertyGroup::GetPropertySetInstance(StdString strName)
{
    std::map<StdString, CPropertySet >::iterator iteMap;
    
    iteMap = m_mapPropertyGrop.find(strName);

    if (iteMap == m_mapPropertyGrop.end())
    {
        return NULL;
    }

    return &(iteMap->second);

}

bool CPropertyGroup::CretePropertySet(StdString strName)
{
    std::map<StdString, CPropertySet >::iterator iteMap;
    
    iteMap = m_mapPropertyGrop.find(strName);

    if (iteMap != m_mapPropertyGrop.end())
    {
        //重複あり
        return false;
    }

    CPropertySet lstSet;

    m_mapPropertyGrop[strName] = lstSet;
    return true;
}

bool CPropertyGroup::DeletePropertySet(StdString strName)
{
    std::map<StdString, CPropertySet >::iterator iteMap;
    
    iteMap = m_mapPropertyGrop.find(strName);

    if (iteMap == m_mapPropertyGrop.end())
    {
        return false;
    }
    m_mapPropertyGrop.erase(iteMap);
    return true;
}

bool CPropertyGroup::ChangeName(StdString strSrc, StdString strDst)
{
    std::map<StdString, CPropertySet >::iterator iteMap;
    
    iteMap = m_mapPropertyGrop.find(strSrc);

    if (iteMap == m_mapPropertyGrop.end())
    {
        return false;
    }

    m_mapPropertyGrop[strDst] = iteMap->second;
    m_mapPropertyGrop.erase(iteMap);
    return true;
}




//*******************************
//             TEST
//*******************************
#ifdef _DEBUG

//----------------
// テスト
//----------------
void TEST_CPropertyGroup()
{


    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());


    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));

}
#endif //_DEBUG