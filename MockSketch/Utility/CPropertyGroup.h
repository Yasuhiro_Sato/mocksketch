/**
 * @brief			CPropertyGroup ヘッダーファイル
 * @file			CPropertyGroup.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

#ifndef  _PROPERTY_GROUP_H__
#define  _PROPERTY_GROUP_H__
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "Utility/CStdPropertyTree.h"

/*---------------------------------------------------*/
/*  Class                                            */
/*---------------------------------------------------*/

/*---------------------------------------------------*/
/*  Enums                                            */
/*---------------------------------------------------*/








class CPropertySet
{
public:
    CPropertySet():m_iChgPropUserCnt(0){;}

    //!< コピーコンストラクター
    CPropertySet(const CPropertySet& m);

    ~CPropertySet();
    
    CPropertySet& operator = (const CPropertySet & m);

    bool operator == (const CPropertySet & m) const;

    bool operator != (const CPropertySet & m) const;

    //!< データ設定
    virtual bool Add(std::shared_ptr<CStdPropertyItemDef> pPropDef);

    //!< データ挿入
    virtual bool Insert(int iRow, std::shared_ptr<CStdPropertyItemDef> pPropDef);

    //!< データ取得
    virtual  StdString GetItemString(int iNo, E_PROPERTY_ITEM_SET eCol);

    virtual  CStdPropertyItemDef* GetInstance(int iNo);
    
    //!< データ設定
    virtual bool Set(int iNo, E_PROPERTY_ITEM_SET eCol, StdString strDef);

    //!< 削除
    virtual bool Delete(int iNo);

    //!< 全削除
    virtual void DeleteAll();

    //!< 定義数取得
    virtual int GetCnt() const;

    //!< チェック
    virtual bool Check() ;

    //!< 全チェック
    virtual bool CheckAll(int* pNo, E_PROPERTY_ITEM_SET* pErrorCol, StdString* pError);

    //!< 変更カウント取得
    virtual int GetChgCnt() const;

    //!< 変更カウント取得
    virtual bool IsChgEachItem() const;

    //!< 変更カウント追加
    virtual int AddChgCnt();

    //!< 位置交換
    virtual bool Swap(int iSrc, int iDst);

    //!< 位置移動
    virtual bool Move(int iSrc, int iDst);

protected:
    std::vector<std::shared_ptr<CStdPropertyItemDef>>      m_lstPropertyItem;

    int m_iChgPropUserCnt;


private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
            SERIALIZATION_BOTH("PropertySet"      , m_lstPropertyItem);
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
        m_iChgPropUserCnt = 0;
    }
};

//--------------------------------------
//--------------------------------------
//--------------------------------------

class CPropertyGroup
{
public:
    CPropertyGroup();

    virtual ~CPropertyGroup();

    void GetPropertySetNameList(std::vector<StdString>* pList) const;

    bool GetPropertySet(CPropertySet* pListDef,
                        StdString strName) const;

    CPropertySet* GetPropertySetInstance(StdString strName);

    bool CretePropertySet(StdString strName);

    bool DeletePropertySet(StdString strName);

    bool ChangeName(StdString strSrc, StdString strDst);

    virtual bool SaveFile(StdPath path, bool bDispErrorDialog);

    virtual bool LoadFile(StdPath path, bool bDispErrorDialog);

    StdPath GetFilePath(){return m_pathFileName;}

    StdString GetName(){return m_pathFileName.stem().c_str();}

private:
    //
    std::map<StdString, CPropertySet > m_mapPropertyGrop;
    StdPath   m_pathFileName;
};                  


#endif //_PROPERTY_GROUP_H__
