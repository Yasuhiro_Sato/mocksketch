/**
 * @brief			CStdPropertyTree実装ファイル
 * @file		    CStdPropertyTree.cpp
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CStdPropertyTree.h"
#include "Utility/CUtility.h"
#include "Utility/CustomPropertys.h"
#include "System/MOCK_ERROR.h"
#include "System/CSystem.h"
#include "DrawingObject/CDrawingScriptBase.h"
#include <boost/serialization/export.hpp> 

//基底クラスが無い場合は設定しない
//BOOST_CLASS_EXPORT(CStdPropertyItemDef);

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#ifndef _CONSOLE
#define new DEBUG_NEW
#endif
#endif

/*---------------------------------------------------*/
/* Static variables                                  */
/*---------------------------------------------------*/
bool  CStdPropertyItemDef::ms_bInit = false;

std::map<PROPERTY_TYPE, StdString> CStdPropertyItemDef::ms_mapType2Name;
std::map<StdString, PROPERTY_TYPE> CStdPropertyItemDef::ms_mapName2Type;
std::map<PROPERTY_TYPE, StdString> CStdPropertyItemDef::ms_mapType2Type;
std::vector<StdString>             CStdPropertyItemDef::ms_lstCombo;

/**
 * @brief   型がレンジを持っているか判定
 * @param   [in] eType プロパティ形
 * @return	true レンジあり
 * @note	 
 */
bool CStdPropertyItemDef::IsRange( PROPERTY_TYPE eType)
{
    if (eType == PROP_INT_RANGE)
    {
        return true;
    }

    return false;
}

/**
 * @brief   アイテムを文字列に変換
 * @param   [in] eCol アイテム種別
 * @return	変換後文字列
 * @note	 
 */
StdString CStdPropertyItemDef::GetItemString( E_PROPERTY_ITEM_SET eCol)
{
   PropertyInit();
   StdString strRet;

   bool bUseMax;
   bool bUseMin;

   bUseMax = bUseMin = IsRange( type);

   if ((type == PROP_DROP_DOWN) ||
       (type == PROP_DROP_DOWN_ID) ||
       (type == PROP_CHCEK_LIST))
   {
       //ドロップダウンリストは最小値にリストの値を設定する
       bUseMin = true;
   }
   
   if (type == PROP_SETTING)
   {
       bUseMin = false;
       bUseMax = false;
   }

   if (type == PROP_TWO_BUTTON)
   {
	   bUseMin = false;
	   bUseMax = false;
   }

   switch(eCol)
    {
    case PRS_TYPE:       //!< 種別
        return PropertyType2Name(type);
        break;

    case PRS_NAME:       //!< 名称
        return  strDspName;
        break;

    case PRS_VAL_NAME:   //!< 変数名
        return  strValName;
        break;

    case PRS_INIT:       //!< 初期値
        if (type == PROP_COLOR)
        {
            COLORREF colorCurr = boost::any_cast<COLORREF>(anyInit);
	        strRet = CUtil::StrFormat(_T("%02x%02x%02x"), GetRValue(colorCurr),
                                                          GetGValue(colorCurr),
                                                          GetBValue(colorCurr));
        }
        else
        {
            strRet = CUtil::AnyToString(anyInit, false);
            strRet = CUtil::TrimQuotation(strRet);
        }
        return strRet;
        break;

    case PRS_NOTE:       //!< 説明
        return strExp;
        break;

    case PRS_ENABLE:     //!< 有効・無効
        if (bEnableEdit)
        {
            return _T("true");
        }
        else
        {
            return _T("false");
        }
        break;

    case PRS_UNIT_KIND:       //!< 物理量
        {
            CUnitKind* pKind = SYS_UNIT->GetPhysicalQuantity(strUnit);
            if (pKind)
            {
                return  pKind->GetName();
            }
            return _T("");
        }

    case PRS_UNIT:       //!< 単位
        {
            CUnit* pUnit = SYS_UNIT->GetUnit(strUnit);
            if (pUnit)
            {
                return  pUnit->GetUnit();
            }
            else
            {
                    return _T("");
            }
        }
        break;

    case PRS_MAX:        //!< 最大値
        if (bUseMax)
        {
            strRet = CUtil::AnyToString(anyMax, false);
            strRet = CUtil::TrimQuotation(strRet);
            return strRet;
        }
        else
        {
            return _T("");
        }
        break;

    case PRS_MIN:        //!< 最小値
        if (bUseMin)
        {
            strRet = CUtil::AnyToString(anyMin, false);
            strRet = CUtil::TrimQuotation(strRet);
            return strRet;
        }
        else
        {
            return _T("");
        }
        break;

    default:
        break;
    }


    return strRet;
}

/**
 * @brief   型設定時処理
 * @param   [in] typeProp  型
 * @return	true 成功
 * @note	 
 */
bool CStdPropertyItemDef::SetType(PROPERTY_TYPE typeProp)
{
    bool bRet = false;
    switch(typeProp)
    {
    case PROP_NONE:
        break;

    case PROP_BOOL:
        anyInit = bool(false);
        anyMin  = bool(false);
        anyMax  = bool(false);
        bRet = true;
        break;

    case PROP_INT:
    case PROP_INT_RANGE:
        anyInit = int(0);
        anyMin  = int(0);
        anyMax  = int(0);
        bRet = true;
        break;

    case PROP_DOUBLE:
        anyInit = double(0.0);
        anyMin  = double(0.0);
        anyMax  = double(0.0);
        bRet = true;
        break;

    case PROP_COLOR:
        anyInit = DRAW_CONFIG->crFront;
        anyMin  = DRAW_CONFIG->crFront;
        anyMax  = DRAW_CONFIG->crFront;
        bRet = true;
        break;

    case PROP_STR:
        {
        StdString strInit;
        anyInit = strInit;
        anyMin  = strInit;
        anyMax  = strInit;
        bRet = true;
        }
        break;

    case PROP_POINT2D:
        {
        POINT2D pt2D;
        anyInit = pt2D;
        anyMin  = pt2D;
        anyMax  = pt2D;
        bRet = true;
        }
        break;

    case PROP_RECT2D:
        {
        RECT2D rc2D;
        anyInit = rc2D;
        anyMin  = rc2D;
        anyMax  = rc2D;
        bRet = true;
        }
        break;

    case PROP_DROP_DOWN:
        {
        StdString strInit;
        anyInit = strInit;
        anyMin  = strInit;
        anyMax  = strInit;
        bRet = true;
        }
        break;

    case PROP_FILE_NAME:
        {
        StdString strInit;
        anyInit = strInit;
        anyMin  = strInit;
        anyMax  = strInit;
        bRet = true;
        }
        break;



    default:
        break;
    }

    if (bRet)
    {
        type = typeProp;
    }
    return bRet;
}


/**
 * @brief   文字列をアイテムに変換
 * @param   [in] eCol    アイテム種別
 * @param   [in] strItem アイテム文字列
 * @return	true 成功
 * @note	 
 */
bool CStdPropertyItemDef::SetItemString( E_PROPERTY_ITEM_SET eCol, StdString strItem, bool bMessageBox )
{
    PropertyInit();


    StdStringStream strmRet;
    StdString strTypeName;
    std::map<PROPERTY_TYPE, StdString>::iterator iteFindTypeName;
    boost::any anyType;

    if ((eCol == PRS_INIT) ||
        (eCol == PRS_MAX) ||
        (eCol == PRS_MIN))
    {
        iteFindTypeName = ms_mapType2Type.find(type);
        if (iteFindTypeName == ms_mapType2Type.end())
        {
            STD_DBG(_T("SetItemString fail(%d)"),eCol);
            return false;
        }


        if ( type == PROP_DROP_DOWN)
        {
            anyType = strItem;
        }
        else if (type == PROP_COLOR)
        {
            size_t iPos;
            iPos = strItem.find_first_not_of(_T("0123456789abcdefABCDEF"));
            if(iPos != std::string::npos)
            {
                //rrggbb形式の16進数を入力してください
                if(bMessageBox)
                {
                    AfxMessageBox(GET_STR(STR_PRE_ERR_COLOR));
                }
                return false;
            }

            int nR = 0, nG = 0, nB = 0;
            _stscanf_s(strItem.c_str(), _T("%2x%2x%2x"), &nR, &nG, &nB);
            COLORREF colorCurr = RGB(nR, nG, nB);
            anyType = colorCurr;
        }
        else if (( type == PROP_STR) ||
                 ( type == PROP_POINT2D) ||
                 ( type == PROP_RECT2D) ||
                 ( type == PROP_POINT_LIST))
        {
            strTypeName = iteFindTypeName->second;
            strmRet << _T("\"") << strTypeName << _T("\",") ;
            strmRet << _T("\"");
            strmRet << strItem;
            strmRet << _T("\"");
            anyType = CUtil::StringToAny(strmRet.str());

        }
        else
        {
            strTypeName = iteFindTypeName->second;
            strmRet << _T("\"") << strTypeName << _T("\",") ;
            strmRet << strItem;
            anyType = CUtil::StringToAny(strmRet.str());
        }
    }


   switch(eCol)
    {
    case PRS_TYPE:       //!< 種別
        {
        PROPERTY_TYPE typeProp;
        typeProp = PropertyName2Type(strItem);

        if (typeProp == PROP_NONE)
        {
            return false;
        }
        SetType(typeProp);
        }
        break;

    case PRS_NAME:       //!< 名称

        //識別子チェック
        strDspName = strItem;
        break;


    case PRS_VAL_NAME:       //!< 名称

        //識別子チェック
        if (!CUtil::IsIdentifer(strItem))
        {
            MockException(e_no_use_id);
            return false;
        }
        strValName = strItem;
        break;


    case PRS_NOTE:       //!< 説明
        strExp = strItem;
        break;

    case PRS_ENABLE:     //!< 有効・無効

        if (strItem == _T("true"))
        {
            bEnableEdit = true;
        }
        else
        {
            bEnableEdit = false;
        }
        break;

    case PRS_UNIT_KIND:
        {
            StdString strCurKind;
            CUnitKind* pKind  = SYS_UNIT->GetPhysicalQuantity(strUnit);

            if (pKind)
            {
                strCurKind = pKind->GetName();
            }

            if (strCurKind != strItem)
            {
                //種別変更時
                std::vector<CUnit*> lstUnit;
                SYS_UNIT->GetPhysicalQuantityList(&lstUnit, strItem, false);

                if (lstUnit.size() != 0)
                {   
                    strUnit =  lstUnit[0]->GetId();
                }
            }
        }
        break;

    case PRS_UNIT:       //!< 単位
        {
            CUnit* pUnit = SYS_UNIT->GetUnitByUnit(strItem);
            if (pUnit)
            {
                strItem = pUnit->GetId();
            }
            else
            {
                strItem = _T("NONE");
            }
        }
        break;

    case PRS_INIT:       //!< 初期値
        anyInit = anyType;
        break;

    case PRS_MIN:        //!< 最小値
        anyMin = anyType;
        break;

    case PRS_MAX:        //!< 最大値
        anyMax = anyType;
        break;

    default:
        return false;
    }
    
    return true;
}


/**
 * @brief   型リスト取得
 * @param   なし
 * @return	型リスト
 * @note	 
 */
const std::vector<StdString>* CStdPropertyItemDef::GetVListTypeList()
{
    PropertyInit();
    return &ms_lstCombo;
}

/**
 * @brief   初期化
 * @param   なし
 * @return	なし
 * @note	 
 */
void CStdPropertyItemDef::PropertyInit()
{
    if (ms_bInit){return;}

    SetMap(PROP_NONE        ,  _T("NONE"),      CUtil::strString);
    SetMap(PROP_BOOL        ,  _T("BOOL"),      CUtil::strBool);
    SetMap(PROP_INT         ,  _T("INT"),       CUtil::strInt);
    SetMap(PROP_INT_RANGE   ,  _T("INT_RANGE"), CUtil::strInt);
    SetMap(PROP_DOUBLE      ,  _T("DOUBLE"),    CUtil::strDouble);
    SetMap(PROP_COLOR       ,  _T("COLOR"),     CUtil::strColor);
    SetMap(PROP_STR         ,  _T("STRING"),    CUtil::strString);
    SetMap(PROP_POINT2D     ,  _T("POINT2D"),   CUtil::strPoint2D);
    SetMap(PROP_RECT2D      ,  _T("RECT2D"),    CUtil::strRect2D);
    SetMap(PROP_FILE_NAME   ,  _T("FILENAME"),  CUtil::strString);
 
    //SetMap(PROP_LINE_TYPE   ,  _T("LINE_TYPE"));
    //SetMap(PROP_LINE_WIDTH  ,  _T("LINE_WIDTH"));
    //SetMap(PROP_POINT_LIST  ,  _T("POINT_LIST"));

    SetMap(PROP_DROP_DOWN     ,  _T("DROPDOWN"),       CUtil::strString);
    //SetMap(PROP_DROP_DOWN_ID  ,  _T("DROPDOWN_ID"),    CUtil::strInt);
    //SetMap(PROP_CHCEK_LIST  ,  _T("CHECK_LIST"),    CUtil::strInt);

    SetMap(PROP_SETTING     , _T("SETTING"),       CUtil::strString);
	SetMap(PROP_TWO_BUTTON  , _T("TWO_BUTTON"), CUtil::strString);

	
    
    ms_bInit = true;
}

/**
 * @brief   マップ設定
 * @param   [in] eType      型
 * @param   [in] strName    型名
 * @param   [in] strType    anyの型
 * @return	なし
 * @note	 
 */
void CStdPropertyItemDef::SetMap(PROPERTY_TYPE eType, StdString strName, StdString strType)
{
    ms_mapType2Name[eType]   = strName;
    ms_mapName2Type[strName] = eType;
    ms_mapType2Type[eType]   = strType;
    ms_lstCombo.push_back(strName);
}


/**
 * @brief   プロパティ型->名称
 * @param   [in] eType      型
 * @return	型名
 * @note	 
 */
StdString CStdPropertyItemDef::PropertyType2Name(PROPERTY_TYPE eType)
{
    PropertyInit();
    StdString strRet;

    std::map<PROPERTY_TYPE, StdString>::iterator ite;
    ite = ms_mapType2Name.find(eType);

    if (ite != ms_mapType2Name.end())
    {
        return ite->second;
    }
    return strRet;
}

/**
 * @brief   名称->プロパティ型
 * @param   [in] プロパティ名
 * @return	プロパティ型
 * @note	 
 */
PROPERTY_TYPE CStdPropertyItemDef::PropertyName2Type(StdString strType)
{
    PropertyInit();
    StdString strRet;

    std::map<StdString, PROPERTY_TYPE>::iterator ite;
    ite = ms_mapName2Type.find(strType);

    if (ite != ms_mapName2Type.end())
    {
        return ite->second;
    }
    return PROP_NONE;
}

/**
 * @brief   アイテムチェック
 *  @param  [out] pErrorCol   エラー発生列
 *  @param  [out] pError      エラー文字列
 *  @retval false エラーあり 
 *  @note    
 */
bool CStdPropertyItemDef::CheckItem(E_PROPERTY_ITEM_SET* pErrorCol, StdString* pError)
{
    bool bRange = false;
    bool bAutoCorrect = true;

    //TODO:タブを変更したときエラーが出続ける。
    //名称チェック
    if (strDspName == _T(""))
    {
        /*
        *pErrorCol = PRS_NAME;
        *pError = GET_STR(STR_ERROR_SET_NAME);
        return false;
        */
        //名称がない場合はダミーの名称を設定する
    }

    //TODO:タブを変更したときエラーが出続ける。
    /*
    if (!CUtil::IsIdentifer(strDspName))
    {
        *pErrorCol = PRS_NAME;
        *pError = GET_STR(STR_ERROR_OBJECT_NAME);
        return false;
    }
    */


    bool bRet = true;

    //!< 型チェック
    switch(type)
    {
    case PROP_NONE:
        break;

    case PROP_BOOL:
        if(anyInit.type() != typeid(bool))
        {
            *pError = GET_STR(STR_ERROR_SET_TYPE);
            STD_DBG( _T("Init Type %s %d \n"), CUtil::CharToString(anyInit.type().name()).c_str(), type);
            *pErrorCol = PRS_INIT;

            if (bAutoCorrect)
            {
                anyInit = true;
            }
            return false;
        }
        break;


    case PROP_INT_RANGE:

        if(anyMax.type() != typeid(int))
        {
            *pError = GET_STR(STR_ERROR_SET_TYPE);
            STD_DBG( _T("Max Type %s %d \n"), CUtil::CharToString(anyMax.type().name()).c_str(), type);
            *pErrorCol = PRS_MAX;
            bRet = false;
            if (bAutoCorrect)
            {
                anyMax = int(100);
            }
        }

        if(anyMin.type() != typeid(int))
        {
            *pError = GET_STR(STR_ERROR_SET_TYPE);
            STD_DBG( _T("Min Type %s %d \n"), CUtil::CharToString(anyMin.type().name()).c_str(), type);
            *pErrorCol = PRS_MIN;
            bRet = false;
            if (bAutoCorrect)
            {
                anyMin = int(0);
            }
        }

        {
            int iMin, iMax;

            iMax = boost::any_cast<int>(anyMax);
            iMin = boost::any_cast<int>(anyMin);

            if (iMin > iMax)
            {
                *pError = GET_STR(STR_ERROR_MAX_MIN);
                *pErrorCol = PRS_MIN;
                bRet = false;
                if (bAutoCorrect)
                {
                    anyMin = int(iMin + 1);
                }
            }
        }

        //下へ

    case PROP_INT:
        if(anyInit.type() != typeid(int))
        {
            STD_DBG( _T("Init Type %s %d \n"), CUtil::CharToString(anyInit.type().name()).c_str(), type);
            *pError = GET_STR(STR_ERROR_SET_TYPE);
            *pErrorCol = PRS_INIT;
            bRet = false;
            if (bAutoCorrect)
            {
                anyInit = int(0);
            }
        }
        break;

 
    case PROP_DOUBLE:
        if(anyInit.type() != typeid(double))
        {
            STD_DBG( _T("Init Type %s %d \n"), CUtil::CharToString(anyInit.type().name()).c_str(), type);
            *pError = GET_STR(STR_ERROR_SET_TYPE);
            *pErrorCol = PRS_INIT;
            bRet = false;
            if (bAutoCorrect)
            {
                anyInit = double(0.0);
            }
        }
        break;

    case PROP_COLOR:
        if(anyInit.type() != typeid(COLORREF))
        {
            STD_DBG( _T("Init Type %s %d \n"), CUtil::CharToString(anyInit.type().name()).c_str(), type);
            *pError = GET_STR(STR_ERROR_SET_TYPE);
            *pErrorCol = PRS_INIT;
            bRet = false;
            if (bAutoCorrect)
            {
                anyInit = COLORREF(0);
            }
        }
        break;

    case PROP_STR:
        if(anyInit.type() != typeid(StdString))
        {
            STD_DBG( _T("Init Type %s %d \n"), CUtil::CharToString(anyInit.type().name()).c_str(), type);
            *pError = GET_STR(STR_ERROR_SET_TYPE);
            *pErrorCol = PRS_INIT;
            bRet = false;
            if (bAutoCorrect)
            {
                anyInit = StdString(_T(""));
            }
        }
        break;

    case PROP_POINT2D:
        if(anyInit.type() != typeid(POINT2D))
        {
            STD_DBG( _T("Init Type %s %d \n"), CUtil::CharToString(anyInit.type().name()).c_str(), type);
            *pError = GET_STR(STR_ERROR_SET_TYPE);
            *pErrorCol = PRS_INIT;
            bRet = false;
            if (bAutoCorrect)
            {
                anyInit = POINT2D();
            }
        }
        break;

    case PROP_RECT2D:
        if(anyInit.type() != typeid(RECT2D))
        {
            STD_DBG( _T("Init Type %s %d \n"), CUtil::CharToString(anyInit.type().name()).c_str(), type);
            *pError = GET_STR(STR_ERROR_SET_TYPE);
            *pErrorCol = PRS_INIT;
            bRet = false;
            if (bAutoCorrect)
            {
                anyInit = RECT2D();
            }
        }
        break;
    case PROP_DROP_DOWN:
        if(anyInit.type() != typeid(StdString))
        {
            STD_DBG( _T("Init Type %s %d \n"), CUtil::CharToString(anyInit.type().name()).c_str(), type);
            *pError = GET_STR(STR_ERROR_SET_TYPE);
            *pErrorCol = PRS_INIT;
            bRet = false;
            if (bAutoCorrect)
            {
                anyInit = StdString(_T(""));
            }
        }
        if(anyMin.type() != typeid(StdString))
        {
            *pError = GET_STR(STR_ERROR_SET_TYPE);
            STD_DBG( _T("Min Type %s %d \n"), CUtil::CharToString(anyMin.type().name()).c_str(), type);
            *pErrorCol = PRS_MIN;
            bRet = false;
            if (bAutoCorrect)
            {
                anyMin = StdString(_T(""));
            }
        }
        break;

    case PROP_FILE_NAME:
        if(anyInit.type() != typeid(StdString))
        {
            STD_DBG( _T("Init Type %s %d \n"), CUtil::CharToString(anyInit.type().name()).c_str(), type);
            *pError = GET_STR(STR_ERROR_SET_TYPE);
            *pErrorCol = PRS_INIT;
            bRet = false;
            if (bAutoCorrect)
            {
                anyInit = StdString(_T(""));
            }
        }
        break;
    default:
        STD_DBG( _T("Type fail %d \n"), type);
        *pError = GET_STR(STR_ERROR_SET_TYPE);
        *pErrorCol = PRS_INIT;
        bRet = false;
        break;
    }
    return bRet;
}

/**
 * @brief   データ表示
 * @param   なし
 * @return  なし
 * @note    
 */
void CStdPropertyItemDef::Print()
{

    StdString   strEnable;
    StdString   strType =   PropertyType2Name(type);
    StdString   strInit =   CUtil::AnyToString(anyInit, false);

    if (bEnableEdit)
    {
        strEnable =  _T("true");
    }
    else
    {
        strEnable =  _T("false");
    }
    StdString   strMax;
    StdString   strMin;
    strMax = CUtil::AnyToString(anyMax, false);
    strMin = CUtil::AnyToString(anyMin, false);


    STD_DBG(_T("[Type    :%s \n"), strType.c_str());
    STD_DBG(_T("  Name   :%s \n"), strDspName.c_str());
    STD_DBG(_T("  ValName:%s \n"), strValName.c_str());
    STD_DBG(_T("  Explain:%s \n"), strExp.c_str());
    STD_DBG(_T("  Unit   :%s \n"), strUnit.c_str());
    STD_DBG(_T("  Edit   :%s \n"), strEnable.c_str());
    STD_DBG(_T("  Init   :%s \n"), strInit.c_str());
    STD_DBG(_T("  Max    :%s \n"), strMax.c_str());
    STD_DBG(_T("  Min    :%s \n"), strMin.c_str());
    STD_DBG(_T("]\n"));

}

/**
 * @brief   文字列変換
 * @param   なし
 * @return  変換後文字列
 * @note    CStdPropertyItemDefの値を文字列に変換する
 */
StdString CStdPropertyItemDef::ToString()
{
    StdStringStream strm;
    StdString strInit = CUtil::AnyToString(anyInit, false);
    StdString strMax = CUtil::AnyToString(anyMax, false);
    StdString strMin = CUtil::AnyToString(anyMin, false);

    if (strInit == _T(""))
    {
        strInit = _InitStr(type);
    }

    strm << PropertyType2Name(type) << _T(",");  //0   型
    strm << strDspName              << _T(",");  //1   データ表示名
    strm << strValName              << _T(",");  //2   変数名

    strm << _T("\"") << strExp      << _T("\",");//3   データ説明
    strm << strUnit  << _T(",");                 //4   単位
    strm << (bEnableEdit ? "true" : "false") << _T(","); //5  編集可、不可
    strm << strInit  << _T(",");                 //6   初期値
    strm << strMin   << _T(",");                 //7   最小値
    strm << strMax   << std::endl;;              //8   最大値

    return strm.str();
}

//!< 文字列変換
/**
 * @brief   文字列変換
 * @param   [in] 変換する文字列
 * @return  エラー番号
 * @note    文字をCStdPropertyItemDefに変換する
 */
E_ERR CStdPropertyItemDef::FromString(StdString& str)
{
    std::vector<StdString> lstStr;
    CUtil::TokenizeCsv(&lstStr, str);

    if (lstStr.size() < 6)
    {
        return e_lack_item;
    }

    CStdPropertyItemDef tmpDef;


    tmpDef.type        = PropertyName2Type(lstStr[0]);  // 型
    tmpDef.strDspName  = lstStr[1];                     // データ表示名
    tmpDef.strValName  = lstStr[2];                     // データ表示名

    tmpDef.strExp      = lstStr[3];                     // データ説明
    tmpDef.strUnit     = lstStr[4];                     // 単位

    StdString strBool = CUtil::ToLower(lstStr[5]);      // 編集可、不可
    if (strBool == _T("true")){ tmpDef.bEnableEdit = true;}
    else                      { tmpDef.bEnableEdit = false;} 



    StdString strTypeName;

    StdString strInitType;
    StdString strMaxType;
    StdString strMinType;
    StdString strIntType;

    strIntType = CUtil::CharToString(typeid(int).name());
    bool bSameTypeMaxMin = true;

    bool bIgnoralMin = true;
    bool bIgnoralMax = true;

    switch (tmpDef.type)
    {
    case PROP_INT_RANGE:
        bIgnoralMin = false;
        bIgnoralMax = false;
    case PROP_INT:
    case PROP_LINE_TYPE:
    case PROP_LINE_WIDTH:
         strInitType = strIntType;
         break;

    case PROP_DROP_DOWN:
         strInitType = CUtil::CharToString(typeid(StdString).name());
         lstStr[6] = CUtil::QuotString(lstStr[6]);
         if (lstStr.size() >= 8){lstStr[7] = CUtil::QuotString(lstStr[7]);}
         bIgnoralMin = false;
         break;

    case PROP_DROP_DOWN_BMP:
    case PROP_DROP_DOWN_ID:
         bSameTypeMaxMin = false;
         strInitType = strIntType;
         strMinType  = CUtil::CharToString(typeid(StdString).name());
         if (lstStr.size() >= 8){lstStr[7] = CUtil::QuotString(lstStr[7]);}
         bIgnoralMin = false;
         break;

    case PROP_CHCEK_LIST:
         strInitType = CUtil::CharToString(typeid(StdString).name());
         lstStr[5] = CUtil::QuotString(lstStr[5]);
         if (lstStr.size() >= 8){lstStr[7] = CUtil::QuotString(lstStr[7]);}
         bIgnoralMin = false;
         break;


    case PROP_BOOL:
         strInitType = CUtil::CharToString(typeid(bool).name());
         break;

    case PROP_DOUBLE:
         strInitType = CUtil::CharToString(typeid(double).name());
         break;

    case PROP_COLOR:
         strInitType = CUtil::CharToString(typeid(COLORREF).name());
         break;

    case PROP_STR:
         strInitType = CUtil::CharToString(typeid(StdString).name());
         lstStr[6] = CUtil::QuotString(lstStr[6]);  //初期値

         break;

    case PROP_POINT2D:
         strInitType = CUtil::CharToString(typeid(POINT2D).name());
         lstStr[6] = CUtil::QuotString(lstStr[6]);   //初期値
         break;

    case PROP_RECT2D:
         strInitType = CUtil::CharToString(typeid(RECT2D).name());
         lstStr[6] = CUtil::QuotString(lstStr[6]);    //初期値
         break;

    case PROP_POINT_LIST:
         strInitType = CUtil::CharToString(typeid(std::vector<POINT2D>).name());
         lstStr[6] = CUtil::QuotString(lstStr[6]);   //初期値
         break;

    case PROP_FILE_NAME:
         strInitType = CUtil::CharToString(typeid(StdString).name());
         lstStr[6] = CUtil::QuotString(lstStr[6]);  //初期値

    case PROP_SETTING:
         strInitType = CUtil::CharToString(typeid(StdString).name());

	case PROP_TWO_BUTTON:
		strInitType = CUtil::CharToString(typeid(StdString).name());
		


    default:
        return e_unkonwn_type;
    }

    if (bSameTypeMaxMin)
    {
        strMinType = strInitType;
        strMaxType = strInitType;
    }


    StdStringStream strmInit;
    StdStringStream strmMax;;
    StdStringStream strmMin;;


    strmInit << _T("\"") << strInitType << _T("\",") ;

    StdString strInit;
    if (lstStr[6] == _T(""))
    {
        strInit = _InitStr(tmpDef.type);
    }
    else
    {
        strInit = lstStr[6];
    }
    strmInit << strInit;


    tmpDef.anyInit = CUtil::StringToAny(strmInit.str());

    if ((lstStr.size() >= 8) && (!bIgnoralMin))
    {
        strmMin << _T("\"") << strMinType << _T("\",") ;
        strmMin << lstStr[7];
        tmpDef.anyMin = CUtil::StringToAny(strmMin.str());
    }
    else
    {
        tmpDef.anyMin = 0;
        tmpDef.anyMax = 0;
    }

    if ((lstStr.size() >= 9) && (!bIgnoralMax))
    {
        strmMax << _T("\"") << strMaxType << _T("\",") ;
        strmMax << lstStr[8];
        tmpDef.anyMax = CUtil::StringToAny(strmMax.str());
    }
    else
    {
        tmpDef.anyMax = 0;
    }

    type          = tmpDef.type;        
    strDspName    = tmpDef.strDspName;  
    strValName    = tmpDef.strValName;  
    strExp        = tmpDef.strExp;      
    strUnit       = tmpDef.strUnit;     
    bEnableEdit   = tmpDef.bEnableEdit; 
    anyInit       = tmpDef.anyInit;     
    anyMin        = tmpDef.anyMin;      
    anyMax        = tmpDef.anyMax; 
    m_iChgCnt = 0;

    return e_no_error;
}


//!< 文字列変換
StdString CStdPropertyItemDef::_InitStr(PROPERTY_TYPE type) const
{
    StdString strRet;

    switch (type)
    {
    case PROP_INT:
    case PROP_INT_RANGE:
    case PROP_LINE_TYPE:
    case PROP_LINE_WIDTH:
    case PROP_DROP_DOWN_ID:
    case PROP_DROP_DOWN_BMP:
    case PROP_BOOL:
    case PROP_COLOR:
         strRet = _T("0");
         break;

    case PROP_DOUBLE:
         strRet = _T("0.0");
         break;

    case PROP_STR:
    case PROP_DROP_DOWN:
    case PROP_CHCEK_LIST:
    case PROP_FILE_NAME:
    case PROP_SETTING:
	case PROP_TWO_BUTTON:
		
        
         break;

    case PROP_POINT2D:
    {
         POINT2D pt;
         boost::any anyVal;
         anyVal =  pt;
         strRet = CUtil::AnyToString(anyVal, false);
    }
         break;

    case PROP_RECT2D:
    {
         RECT2D rc;
         boost::any anyVal;
         anyVal =  rc;
         strRet = CUtil::AnyToString(anyVal, false);
    }
         break;

    case PROP_POINT_LIST:
    {
         std::vector<POINT2D> lst;
         boost::any anyVal;
         anyVal =  lst;
         strRet = CUtil::AnyToString(anyVal, false);
    }
         break;
    }
    return strRet;
}



/*---------------------------------------------------*/
/*---------------------------------------------------*/
/*---------------------------------------------------*/


/*---------------------------------------------------*/
/*---------------------------------------------------*/
/*---------------------------------------------------*/


/**
 *  @brief   コンストラクタ
 *  @param   なし
 *  @retval  なし
 *  @note
 */
CStdPropertyTree::CStdPropertyTree():
m_pParent(0)
{

}

/**
 *  @brief   コンストラクタ
 *  @param   なし
 *  @retval  なし
 *  @note
 */
CStdPropertyTree::CStdPropertyTree(CStdPropertyTree &m)
{
    m_pParent = m.m_pParent;
    m_Tree    = m.m_Tree;
}

/**
 *  @brief   デストラクタ
 *  @param   なし
 *  @retval  なし
 *  @note
 */
CStdPropertyTree::~CStdPropertyTree()
{

}

/**
 *  @brief   コールバック設定元設定
 *  @param   [in] pParent  設定元
 *  @retval  なし
 *  @note
 */
void CStdPropertyTree::SetParent(void* pParent)
{
    STD_ASSERT(pParent != NULL);

    m_pParent = pParent;
}

/**
 *  @brief   コールバック設定元設定
 *  @param   なし
 *  @retval  設定元へのポインタ
 *  @note
 */
void* CStdPropertyTree::GetParent()
{
    return m_pParent;
}

/**
 *  @brief   アイテム取得
 *  @param   [in] strName     アイテム名称
 *  @param   [in] iNo         名称種別  0:表示名 1:変数名
 *  @retval  アイテムへのポインタ
 *  @note     
 */
CStdPropertyItem* CStdPropertyTree::GetItem(StdString strName, int iNo)
{
    return m_Tree.GetItemData(strName, iNo);
}

//アイテム取得
/**
 *  @brief   アイテム取得
 *  @param   [in] iId        アイテムID
 *  @retval  アイテムへのポインタ
 *  @note     
 */
CStdPropertyItem* CStdPropertyTree::GetItem(int iId)
{
    return m_Tree.GetItemData(iId);
}

/**
 *  @brief   最終グループ取得
 *  @param   なし
 *  @retval  最終グループに対応するアイテム
 *  @note    
 */
NAME_TREE_ITEM<CStdPropertyItem>* CStdPropertyTree::GetLastGroup()
{
    NAME_TREE_ITEM<CStdPropertyItem>* pNext = NULL;
    NAME_TREE_ITEM<CStdPropertyItem>* pNextLast = NULL;
    
    pNextLast = pNext = m_Tree.GetRoot()->spNext;

    while (pNext != NULL)
    {
        pNextLast = pNext;
        pNext = pNext->spNext;
    }
    return pNextLast;
}

/**
 *  @brief   インスタンス取得
 *  @retval  Treeインスタンス
 *  @note
 */
NAME_TREE<CStdPropertyItem>* CStdPropertyTree::GetTreeInstance()
{
    return &m_Tree;
}

/**
 *  @brief   オブジェクトデータからプロパティグリッドへの登録
 *  @param   [in] pGridCtrl
 *  @param   [in] pStdItem     
 *  @retval  
 *  @note    
 */
void CStdPropertyTree::_UpdateStdItemToGrid(CMFCPropertyGridCtrl*   pGridCtrl, 
      NAME_TREE_ITEM<CStdPropertyItem>* pStdItem,
      bool bSetVisibleOnly)
{
    if (pStdItem->spChild)
    {
        _UpdateStdItemToGrid(pGridCtrl, pStdItem->spChild, bSetVisibleOnly);
    }

    if (pStdItem->spNext)
    {
        _UpdateStdItemToGrid(pGridCtrl, pStdItem->spNext, bSetVisibleOnly);
    }

    CMFCPropertyGridProperty* pGridProperty;
    CStdPropertyItem* pItemData;

    
    pGridProperty = pGridCtrl->FindItemByData((DWORD_PTR)pStdItem->iId);
    pItemData = pStdItem->pData;

    if (pGridProperty == NULL)
    {
        return;
    }

    if (pItemData == NULL)
    {
        return;
    }

    if (bSetVisibleOnly)
    {
        if(!pGridProperty->IsVisible())
        {
            return;
        
        }
    }

    if (!pItemData->pRealData)
    {
        return;
    }

    CAny* pAny = NULL;
    if (pItemData->ePropCategoly == PROP_CATE_USER)
    {
        CDrawingScriptBase* pBase;
        pBase = reinterpret_cast<CDrawingScriptBase*>(pItemData->pRealData);
        pAny = pBase->GetUserProperty(pItemData->GetName());
        
        STD_ASSERT(pAny);

        if (!pAny)
        {
            return;
        }
    }
    else if (pItemData->ePropCategoly == PROP_CATE_PROP_SET)
    {
        CDrawingObject* pBase;
        pBase = reinterpret_cast<CDrawingObject*>(pItemData->pRealData);
        pAny = pBase->GetPropertySetVal(pItemData->GetName());
        
        STD_ASSERT(pAny);

        if (!pAny)
        {
            return;
        }
    }

    PROPERTY_TYPE type = pItemData->type;

    _UpdateGridVal(pAny, pItemData->pRealData, type, pGridCtrl, pStdItem->iId);

}



/**
 *  @brief   オブジェクトデータからプロパティグリッドへの登録
 *  @param   [in] pGridCtrl
 *  @param   [in] pStdItem     
 *  @retval  
 *  @note    スクリプト側からの 
 */
bool  CStdPropertyTree::_UpdateGridVal(
      CAny* pAny,
      void* pData,
      PROPERTY_TYPE type,
      CMFCPropertyGridCtrl*   pGridCtrl,
      int                     iId,
      bool bFoceUpdate
      )
{

    CMFCPropertyGridProperty* pGridProperty;
    pGridProperty = pGridCtrl->FindItemByData((DWORD_PTR)iId);

    COleVariant vValOrg;

    COleVariant TmpVal;
    switch (type)
    {
    case PROP_BOOL:
        {
            if(pAny)
            {
                TmpVal = COleVariant((long)(pAny->GetBool()?1:0), VT_BOOL);
                pGridProperty->SetValue( TmpVal );
            }
            else
            {
                bool* pbData = (bool*)pData;
                TmpVal = COleVariant((long)((*pbData)?1:0), VT_BOOL);

            }
            if(!bFoceUpdate)
            {
                vValOrg = pGridProperty->GetValue();
                if (TmpVal == vValOrg)
                {
                    return true;
                }
            }
            pGridProperty->SetValue( TmpVal );
        } break;

    case PROP_INT:
    case PROP_INT_RANGE:
    case PROP_LINE_WIDTH:
        {
            if(pAny)
            {
                TmpVal = COleVariant((long)(pAny->GetInt()), VT_I4);
            }
            else
            {
                int* pInt = (int*)pData;
                TmpVal = COleVariant ((long)(*pInt), VT_I4);
            }

            if(!bFoceUpdate)
            {
                vValOrg = pGridProperty->GetValue();
                if (TmpVal == vValOrg)
                {
                    return true;
                }
            }
            pGridProperty->SetValue( TmpVal );

        } break;

    case PROP_LINE_TYPE:
        {
            CDropDownExProp* pPropDrop;
            pPropDrop = dynamic_cast<CDropDownExProp*>(pGridProperty);
            int iCurSel = pPropDrop->GetCurId();
            int iReqSel;

            if(pAny)
            {
                iReqSel = pAny->GetInt();
            }
            else
            {
                iReqSel = *(int*)pData;
            }

            if(!bFoceUpdate)
            {
                if (iReqSel == iCurSel)
                {
                    return true;
                }
            }
            pPropDrop->SetCurSelById(iReqSel);

        } break;

    case PROP_POINT_LIST:
        {
            CPointListProp* pPropPoint;
            pPropPoint = dynamic_cast<CPointListProp*>(pGridProperty);
            std::vector<POINT2D>* pList =  pPropPoint->GetList();
            std::vector<POINT2D>* pReqList;
            if(pAny)
            {
                pReqList = &pAny->GetPointList();
            }
            else
            {
                pReqList = (std::vector<POINT2D>*)pData;
               
            }

            if(!bFoceUpdate)
            {
                if (*pReqList == *pList)
                {
                    return true;
                }
            }
            pPropPoint->SetList(*pReqList);
        }
        break;

    case PROP_DOUBLE:
        {
            if(pAny)
            {
                TmpVal = COleVariant(pAny->GetDouble());
            }
            else
            {
                double* pDouble = (double*)pData;
                TmpVal = COleVariant (*pDouble);
            }

            if(!bFoceUpdate)
            {
                vValOrg = pGridProperty->GetValue();
                if (TmpVal == vValOrg)
                {
                    return true;
                }
            }
            pGridProperty->SetValue( TmpVal );
        } break;

    case PROP_COLOR:
        {

            CMFCPropertyGridColorProperty* pPropColor;
            pPropColor = dynamic_cast<CMFCPropertyGridColorProperty*>(pGridProperty);
            COLORREF crOrg = pPropColor->GetColor();
            COLORREF crReq;
            if (pPropColor)
            {

                if(pAny)
                {
                    crReq = pAny->GetColor();
                }
                else
                {
                    crReq = *(COLORREF*)pData;
                }

                if(!bFoceUpdate)
                {
                    if (crOrg == crReq)
                    {
                        return true;
                    }
                }
                pPropColor->SetColor( crReq );

            }
            else
            {
                STD_DBG(_T("SetColor fail"));
            }
        } break;

    case PROP_STR:
    case PROP_DROP_DOWN:
    case PROP_CHCEK_LIST:
    case PROP_FILE_NAME:
    case PROP_SETTING:
	case PROP_TWO_BUTTON:


        {
            if(pAny)
            {
                TmpVal = COleVariant( pAny->GetString().c_str() );
            }
            else
            {
                StdString* pStr = (StdString*)pData;
                TmpVal = COleVariant(pStr->c_str());
            }

            if(!bFoceUpdate)
            {
                vValOrg = pGridProperty->GetValue();
                if (TmpVal == vValOrg)
                {
                    return true;
                }
            }
            pGridProperty->SetValue( TmpVal );

        } break;

    case PROP_DROP_DOWN_BMP:
    case PROP_DROP_DOWN_ID:
        {
            CDropDownExProp* pPropDrop;
            pPropDrop = dynamic_cast<CDropDownExProp*>(pGridProperty);
            int iIdOrg = pPropDrop->GetCurId();
            int iVal;

            if(pAny)
            {
                iVal = pAny->GetInt();
            }
            else
            {
                iVal = *(int*)pData;
            }
            if(!bFoceUpdate)
            {
                vValOrg = pGridProperty->GetValue();
                if (iVal == vValOrg.iVal)
                {
                    return true;
                }
            }
            pPropDrop->SetCurSelById(iVal);
        }
        break;


    case PROP_POINT2D:
        {
            POINT2D ptData;
            
            if (pAny)
            {
                ptData = pAny->GetPoint();
            }
            else
            {
                ptData = *(POINT2D*)pData;
            }




            CMFCPropertyGridProperty* pGridProperty2;
            pGridProperty2 = pGridCtrl->FindItemByData((DWORD_PTR)(iId + 1));

            POINT2D ptOrg;
            if(!bFoceUpdate)
            {
                COleVariant vValOrg2;
                 vValOrg  = pGridProperty->GetValue();
                 vValOrg2 = pGridProperty2->GetValue();

                 ptOrg.dX = vValOrg.dblVal;
                 ptOrg.dY = vValOrg2.dblVal;

                 if (ptOrg == ptData)
                 {
                     return true;
                 }
            }


            COleVariant TmpValX(ptData.dX);
            COleVariant TmpValY(ptData.dY);

            pGridProperty->SetValue( TmpValX );
            if (pGridProperty2)
            {
                pGridProperty2->SetValue( TmpValY );
            }
            else
            {
                STD_DBG(_T("pGridProperty2 fail"));
            }
        } break;

    case PROP_RECT2D:{

            RECT2D rcData;
            
            if (pAny)
            {
                rcData = pAny->GetRect();
            }
            else
            {
                rcData = *(RECT2D*)pData;
            }

            CMFCPropertyGridProperty* pGridProperty2 = pGridCtrl->FindItemByData((DWORD_PTR)(iId + 1));
            CMFCPropertyGridProperty* pGridProperty3 = pGridCtrl->FindItemByData((DWORD_PTR)(iId + 2));
            CMFCPropertyGridProperty* pGridProperty4 = pGridCtrl->FindItemByData((DWORD_PTR)(iId + 3));


           RECT2D rcOrg;
            if(!bFoceUpdate)
            {
                COleVariant vValOrg2;
                COleVariant vValOrg3;
                COleVariant vValOrg4;

                 vValOrg  = pGridProperty->GetValue();
                 vValOrg2 = pGridProperty2->GetValue();
                 vValOrg2 = pGridProperty3->GetValue();
                 vValOrg2 = pGridProperty4->GetValue();

                 rcOrg.dTop    = vValOrg.dblVal;
                 rcOrg.dBottom = vValOrg2.dblVal;
                 rcOrg.dLeft   = vValOrg3.dblVal;
                 rcOrg.dRight  = vValOrg4.dblVal;

                 if (rcOrg == rcData)
                 {
                     return true;
                 }
            }

            COleVariant TmpValTop   (rcData.dTop);
            COleVariant TmpValBottom(rcData.dBottom);
            COleVariant TmpValLeft  (rcData.dLeft);
            COleVariant TmpValRight (rcData.dRight);

            pGridProperty->SetValue( TmpValTop );

            if (pGridProperty2) {pGridProperty2->SetValue( TmpValBottom );}
            else                {STD_DBG(_T("pGridProperty2 fail")); }
            if (pGridProperty3) {pGridProperty3->SetValue( TmpValLeft );}
            else                {STD_DBG(_T("pGridProperty3 fail")); }
            if (pGridProperty4) {pGridProperty4->SetValue( TmpValRight );}
            else                {STD_DBG(_T("pGridProperty4 fail")); }

        } break;

    default:
        STD_DBG(_T("UNKNOWN TYPE %d"), type);
        break;
    }

    return true;
}


//直接データ値を設定
bool  CStdPropertyTree::SetDataDirect(
      StdString strValName,
      const CAny* pAny)
{

    CStdPropertyItem* pItem;

    pItem = GetItem(strValName, 1); 


    if (!pItem)
    {
        return false;
    }

    if (!pItem->pRealData)
    {
        return false;
    }

    CAny* pAnyDst = NULL;
    switch(pItem->ePropCategoly )
    {
    case PROP_CATE_NOMAL:
        break;

    case PROP_CATE_USER:
        {
            CDrawingScriptBase* pBase;
            pBase = reinterpret_cast<CDrawingScriptBase*>(pItem->pRealData);
            if (pBase)
            {
                pAnyDst = pBase->GetUserProperty(pItem->GetName());
            }
        }
        break;

    case PROP_CATE_PROP_SET:
        {
            CDrawingObject* pBase;
            pBase = reinterpret_cast<CDrawingObject*>(pItem->pRealData);
            if (pBase)
            {
                pAnyDst = pBase->GetPropertySetVal(pItem->GetName());
            }
        }
        break;

    default:

        return false;
    }


    if(pAnyDst)
    {
        if( pAnyDst->anyData.type() != pAny->anyData.type())
        {
            return false;
        }

       pAnyDst->anyData = pAny->anyData;
       return true;
    }

    try
    {

    switch (pItem->psDef->type)
    {
    case PROP_BOOL:
        {
            bool* pData = (bool*)pItem->pRealData;
            *pData = pAny->GetBool();
        } 
        break;

    case PROP_INT:
    case PROP_INT_RANGE:
    case PROP_LINE_WIDTH:
    case PROP_LINE_TYPE:
    case PROP_DROP_DOWN_BMP:
        {
            int* pData = (int*)pItem->pRealData;
            *pData = pAny->GetInt();
        } break;

   case PROP_POINT_LIST:
        {
            std::vector<POINT2D>* pLstPoint = (std::vector<POINT2D>*)pItem->pRealData;
            *pLstPoint = pAny->GetPointList();
        }
        break;

    case PROP_DOUBLE:
        {
            double* pData = (double*)pItem->pRealData;
            *pData = pAny->GetDouble();
        } break;

    case PROP_COLOR:
        {
            COLORREF* pData = (COLORREF*)pItem->pRealData;
            *pData = pAny->GetColor();
        } break;

    case PROP_STR:
    case PROP_DROP_DOWN:
    case PROP_CHCEK_LIST:
    case PROP_FILE_NAME:
    case PROP_SETTING:
	case PROP_TWO_BUTTON:

        {
            StdString* pData = (StdString*)pItem->pRealData;
            *pData = pAny->GetString();
        } break;

    case PROP_DROP_DOWN_ID:
        {
            int* pData = (int*)pItem->pRealData;
            *pData = pAny->GetInt();
        }
        break;


    case PROP_POINT2D:
        {
            POINT2D* pData = (POINT2D*)pItem->pRealData;
            *pData = pAny->GetPoint();
        } break;

    case PROP_RECT2D:{

            RECT2D* pData = (RECT2D*)pItem->pRealData;
            *pData = pAny->GetRect();
        } break;

    default:
        STD_DBG(_T("UNKNOWN TYPE %d"), pItem->psDef->type);
        return false;
    }
    }
    catch(MockException& e)
    {
        STD_DBG(_T("CStdPropertyTree error %s"), e.what());
        return false;
    }
    return true;
}


/**
 *  @brief   プロパティ更新
 *  @param   [in] pGridCtrl     
 *  @retval  
 *  @note    
 */
void CStdPropertyTree::UpdatePropertyGrid(CMFCPropertyGridCtrl*   pGridCtrl,
                                          bool bSetVisibleOnly)
{
    NAME_TREE_ITEM<CStdPropertyItem>* pTreeItem;
    pTreeItem = m_Tree.GetRoot();

    _UpdateStdItemToGrid(pGridCtrl, pTreeItem, bSetVisibleOnly);
}


/**
 *  @brief   デバッグ用出力
 *  @param   
 *  @retval  
 *  @note    
 */
void CStdPropertyTree::Print(NAME_TREE_ITEM<CStdPropertyItem>* pCur, StdString strIndent)
{
    if (pCur == NULL)
    {
        pCur = m_Tree.GetRoot();
        DB_PRINT(_T("CStdPropertyTree \n"));
    }

    CStdPropertyItem* pItem;
    while(pCur)
    {
        pItem = pCur->pData;

        if (pItem)
        {
            DB_PRINT( _T("%s"), strIndent.c_str(), pItem->GetName().c_str());

            if (pCur->spChild != NULL)
            {
                StdString strNewIndent = strIndent + _T(" ");
                Print(pCur->spChild, strNewIndent);
            }
        }
        pCur = pCur->spNext;
    }
}


//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void Test_CStdPropertyItemDef()
{
    CStdPropertyItemDef DefCenter(PROP_POINT2D, 
        GET_STR(STR_PRO_CENTER)   , 
         _T("Center"),
        GET_STR(STR_PRO_INFO_CENTER), 
        true,
        DISP_UNIT,
        POINT2D());

    StdString strOut;

    strOut = DefCenter.ToString();

    CStdPropertyItemDef DefCenter2;

    DefCenter2.FromString(strOut);

    STD_ASSERT(DefCenter == DefCenter2);



    CStdPropertyItemDef DefString(PROP_STR, 
        GET_STR(STR_PRO_CENTER)   , 
        _T("Center"),
        GET_STR(STR_PRO_INFO_CENTER), 
        true,
        DISP_UNIT,
        _T("Hello"), StdString(_T("")), _T(""));



    strOut = DefString.ToString();








}


void TEST_PROPERTY_GRID_DATA()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());


    Test_CStdPropertyItemDef();

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}
#endif //_DEBUG