/**
 * @brief			CStdPropertyTreeインクルードファイル
 * @file			CStdPropertyTree.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */


#ifndef  _STD_PROPERTY_TREE_H__
#define  _STD_PROPERTY_TREE_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DrawingObject/BaseObj.h"
#include "DrawingObject/Primitive/POINT2D.H"
#include "DrawingObject/Primitive/RECT2D.H"
#include "Utility/CAny.h"
#include "Utility/CUtility.h"
#include "NameTreeData.h"

/*---------------------------------------------------*/
/*  Classes                                          */
/*---------------------------------------------------*/
class CStdPropertyTree;
class CMFCPropertyGridCtrl;
class CStdPropertyItem;

typedef NAME_TREE_ITEM<CStdPropertyItem> TREE_GRID_ITEM;
typedef  boost::tuple< CStdPropertyItem*, void*>  PAIR_ITEM_VAL;



//!< VirtualList用 表示項目(設定用) 
enum E_PROPERTY_ITEM_SET
{
    PRS_TYPE,       //!< 種別
    PRS_NAME,       //!< 名称
    PRS_VAL_NAME,   //!< 変数名
    PRS_INIT,       //!< 初期値
    PRS_NOTE,       //!< 説明
    PRS_ENABLE,     //!< 有効・無効
    PRS_UNIT_KIND,  //!< 物理量
    PRS_UNIT,       //!< 単位
    PRS_MIN,        //!< 最小値
    PRS_MAX,        //!< 最大値
};

enum PROPERTY_TYPE
{
    PROP_NONE,
    PROP_BOOL,
    PROP_INT,
    PROP_INT_RANGE,
    PROP_DOUBLE,
    //PROP_DOUBLE_RANGE,
    PROP_COLOR,
    PROP_STR,
    PROP_POINT2D,
    PROP_RECT2D,
    PROP_LINE_TYPE,
    PROP_LINE_WIDTH,
    PROP_POINT_LIST,
    PROP_DROP_DOWN,
    PROP_DROP_DOWN_ID,
    PROP_DROP_DOWN_BMP,
    PROP_CHCEK_LIST,
    PROP_FILE_NAME,
    PROP_SETTING,
	PROP_TWO_BUTTON,
};

enum E_PRPERTY_CATEGORY
{
    PROP_CATE_NOMAL,
    PROP_CATE_USER,
    PROP_CATE_PROP_SET,
};

/**
 * @struct  CStdPropertyItemDef
 * @brief   プロパティ定義データ                     
 */
struct  CStdPropertyItemDef: public CBaseObj
{
public:
    CStdPropertyItemDef(): 
    type        (PROP_NONE),
    strDspName  (_T("")),
    strValName  (_T("")),
    strExp      ( _T("")),
    anyInit     ( StdString(_T("")) ),
    bEnableEdit (true),
    strUnit     ( StdString(_T("NONE")) ),
    anyMin      ( StdString(_T("")) ),
    anyMax      ( StdString(_T("")) ),
    m_iChgCnt   ( 0 )
    {;}


    CStdPropertyItemDef(PROPERTY_TYPE    typeInput,
                         StdString        strDspNameInput,
                         StdString        strValNameInput,
                         StdString        strExpInput,
                         bool             bEnble,
                         StdString        strUnitInput,
                         boost::any       anyInitInput,
                         boost::any       anyMinInput = 0,
                         boost::any       anyMaxInput = 0
                         )
    {
        type        = typeInput;
        strDspName  = strDspNameInput;
        strValName  = strValNameInput;
        strExp      = strExpInput;
        strUnit     = strUnitInput;
        anyInit     = anyInitInput;
        anyMax      = anyMaxInput;
        anyMin      = anyMinInput;
        bEnableEdit = bEnble;
        m_iChgCnt   = 1;
    }

    CStdPropertyItemDef(const CStdPropertyItemDef& def)
    {
        *this = def;
        /*
        type        = def.type;
        strDspName  = def.strDspName;
        strExp      = def.strExp;
        strUnit     = def.strUnit;
        anyInit     = def.anyInit;
        anyMin      = def.anyMin;
        anyMax      = def.anyMax;
        bEnableEdit = def.bEnableEdit;
        m_iChgCnt   = def.m_iChgCnt;
        */
    }

    CStdPropertyItemDef& operator = (const CStdPropertyItemDef& def)
    {
        type        = def.type;
        strDspName  = def.strDspName;
        strValName  = def.strValName;
        strExp      = def.strExp;
        strUnit     = def.strUnit;
        anyInit     = def.anyInit;
        anyMin      = def.anyMin;
        anyMax      = def.anyMax;
        bEnableEdit = def.bEnableEdit;
        m_iChgCnt   = def.m_iChgCnt;
        return *this;
    }

    bool operator == (const CStdPropertyItemDef& def) const
    {
        if(type        != def.type)     {return false;}
        if(strDspName  != def.strDspName){return false;}
        if(strValName  != def.strValName){return false;}
        if(strExp      != def.strExp)   {return false;}
        if(strUnit     != def.strUnit)  {return false;}

        if(!CUtil::IsAnyEqual(anyInit, def.anyInit)) {return false;}
        if(!CUtil::IsAnyEqual(anyMin,  def.anyMin))  {return false;}
        if(!CUtil::IsAnyEqual(anyMax,  def.anyMax))  {return false;}

        if(bEnableEdit != def.bEnableEdit)  {return false;}
        //m_iChgCnt   = def.m_iChgCnt;
        return true;
    }

    bool operator != (const CStdPropertyItemDef& def) const
    {
        return !(*this == def);
    }

    //!< アイテムを文字列で取得
    StdString GetItemString( E_PROPERTY_ITEM_SET eCol);
    
    //!< アイテムを文字列で設定
    bool SetItemString( E_PROPERTY_ITEM_SET eCol, StdString strItem, bool bMessageBox = true);

    static const std::vector<StdString>* GetVListTypeList();

    bool CheckItem(E_PROPERTY_ITEM_SET* pErrorItem, StdString* pError);

    //------------
    //変更カウント
    //------------
    void AddChgCnt(){m_iChgCnt++;}

    void SubChgCnt(){m_iChgCnt--;}

    int  GetChgCnt()const{return m_iChgCnt;}

public:
    PROPERTY_TYPE    type;          //型
    StdString        strDspName;    //データ表示名
    StdString        strValName;    //変数名
    StdString        strExp;        //データ説明
    StdString        strUnit;       //単位
    bool             bEnableEdit;   //編集可、不可
    boost::any       anyInit;       //初期値
    boost::any       anyMin;        //最小値
    boost::any       anyMax;        //最大値

protected:
    static std::map<PROPERTY_TYPE, StdString> ms_mapType2Name;
    static std::map<StdString, PROPERTY_TYPE> ms_mapName2Type;
    static std::map<PROPERTY_TYPE, StdString> ms_mapType2Type;
    static std::vector<StdString>             ms_lstCombo;
    static bool ms_bInit;

    bool SetType(PROPERTY_TYPE typeProp);

    int  m_iChgCnt;

public:

    //!< プロパティ初期化
    static void          PropertyInit();

    //!< プロパティ型名取得
    static StdString     PropertyType2Name(PROPERTY_TYPE eType);

    //!< プロパティ型名からプロパティ型を取得
    static PROPERTY_TYPE PropertyName2Type(StdString strType);

    static void SetMap(PROPERTY_TYPE eType, StdString strName,StdString strType);

    static bool IsRange( PROPERTY_TYPE eType);


    //!< データ表示
    void Print();

    //!< 文字列変換
    StdString ToString();

    //!< 文字列変換
    E_ERR FromString(StdString& str);

protected:
    //!< 
    StdString _InitStr(PROPERTY_TYPE trpe) const;

    StdString _QuotStr(const StdString& stdStr) const;

private:
    friend class boost::serialization::access;  
    template<class Archive>


    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
            SERIALIZATION_BOTH("Type"      , type);
            SERIALIZATION_BOTH("DispName"  , strDspName);
            SERIALIZATION_BOTH("ValName"  , strValName);
            SERIALIZATION_BOTH("Explain"   , strExp);
            SERIALIZATION_BOTH("Edit"      , bEnableEdit);
            SERIALIZATION_BOTH("Unit"      , strUnit);
            SERIALIZATION_BOTH("InitialVal", anyInit);
            SERIALIZATION_BOTH("MinVal", anyMin);
            SERIALIZATION_BOTH("MaxVal", anyMax);
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }
};

BOOST_CLASS_VERSION(CStdPropertyItemDef, 0);

/**
 * @struct  CStdPropertyItem
 * @brief   プロパティデータ項目                     
 */
class  CStdPropertyItem : public CBaseObj
{
public:

    //!< コンストラクタ
    CStdPropertyItem():m_pParent(0),
                       func(NULL),
                       psDef(NULL),
                       type(PROP_NONE),
                       ePropCategoly(PROP_CATE_NOMAL),
                       pRealData(NULL){;}

    //!< コンストラクタ
    CStdPropertyItem( CStdPropertyItemDef defInput,        //定義データ
                      bool (*funcInput)(CStdPropertyItem* pData, void*),   //入力時コールバック
                      CStdPropertyTree* pParent,                           //親グリッド
                      bool (*funcUpdate)(CStdPropertyItem* pData, void*),  //更新時コールバック
                      void* pData,                                         //実データ 
                      E_PRPERTY_CATEGORY eCategoly = PROP_CATE_NOMAL)                              //ユーザープロパティ      
    {
        //psStrDataName = new StdString(strName);
        psDef       = new CStdPropertyItemDef(defInput);
        func        = funcInput;
        funcAfter   = funcUpdate;
        m_pParent   = pParent;

        type        = defInput.type;
        pRealData   = pData;
        ePropCategoly   = eCategoly;



        anyData     = defInput.anyInit;


    }

    ~CStdPropertyItem()
    {
        if(psDef)
        {
            delete psDef;
            psDef = NULL;
        }
    }

    StdString  GetName()
    {
        if (psDef)
        {
            return psDef->strValName;
        }
        else
        {
            StdString strRet;
            return strRet;
        }
    }
public:

    // AS側から単体で使用することを考え
    // データ名、定義がなくても機能するようにする
    //StdString*        psStrDataName;       //データ名
    CStdPropertyItemDef* psDef;        //定義
    CAny                 anyData;      //値
    PROPERTY_TYPE        type;         //型
    void*                pRealData;      //実データ
    E_PRPERTY_CATEGORY   ePropCategoly;  //プロパティ分類
    bool (*func)(CStdPropertyItem* pData, void*);
    bool (*funcAfter)(CStdPropertyItem* pData, void*);


protected:
    CStdPropertyTree*      m_pParent;
};



class CStdPropertyTree
{
public:
    //コンストラクタ
    CStdPropertyTree();

    CStdPropertyTree(CStdPropertyTree &m);

    //デストラクタ
    virtual ~CStdPropertyTree();



    //設定元
    void SetParent(void* pParent);

    //設定元取得
    void* GetParent();

    //アイテム取得
    CStdPropertyItem* GetItem(StdString strName, int iNo); 

    //アイテム取得
    CStdPropertyItem* GetItem(int iId); 

    //プロパティ更新
    void UpdatePropertyGrid(CMFCPropertyGridCtrl*   pGridCtrl,
                            bool bSetVisibleOnly);

    //インスタンス
    NAME_TREE<CStdPropertyItem>* GetTreeInstance(); 

    //最終グループ取得
    NAME_TREE_ITEM<CStdPropertyItem>* GetLastGroup(); 

    //デバッグ用出力
    void Print(NAME_TREE_ITEM<CStdPropertyItem>* pCur = NULL,
               StdString strIndent =_T(""));

    //アイテムデータ設定
    bool SetItemData(CMFCPropertyGridCtrl* pGridCtrl, StdString strName, const CAny* pAny); 


    bool  SetDataDirect(StdString strValName,  const CAny* pAny);

protected:

    void _UpdateStdItemToGrid(CMFCPropertyGridCtrl*   pGridCtrl, 
          NAME_TREE_ITEM<CStdPropertyItem>* pStdItem,
          bool bSetVisibleOnly);


    bool  _UpdateGridVal(
      CAny* pAny,
      void* pData,
      PROPERTY_TYPE type,
      CMFCPropertyGridCtrl*   pGridCtrl,
      int                     iId,
      bool bFoceUpdate = false
      );


protected:

    void*   m_pParent;

    NAME_TREE<CStdPropertyItem>  m_Tree;
};



#endif //_PROPERTY_GRID_DATA_H__