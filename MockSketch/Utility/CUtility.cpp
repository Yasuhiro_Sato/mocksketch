/**
 * @brief			CUtility実装ファイル
 * @file			CUtility.cpp
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "CUtility.h"
#include <shlobj.h> //!< use shell32.lib
#include "System/MOCK_ERROR.h"
#include "System/CSystem.h"  //GET_SYS_STR
#include "DrawingObject/Primitive/RECT2D.h"
#include "DrawingObject/Primitive/MAT2D.H"
#include "DrawingObject/Primitive/LINE2D.H"
#include "DrawingObject/Primitive/CIRCLE2D.H"
#include "DrawingObject/Primitive/ELLIPSE2D.H"
#include "DrawingObject/Primitive/HYPERBOLA2D.H"
#include "DrawingObject/Primitive/PARABOLA2D.H"
#include "DrawingObject/Primitive/SPLINE2D.H"
#include <boost/tokenizer.hpp>
#include "MtEditor/MtParse.h"
#include <angelscript.h> //ConvUni
/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define PI  3.14159265358979323846264  /* 円周率 */

/*---------------------------------------------------*/
/* Static variables                                  */
/*---------------------------------------------------*/
StdString CUtil::strVoid    = CUtil::CharToString(typeid(void).name());
StdString CUtil::strInt     = CUtil::CharToString(typeid(int).name());
StdString CUtil::strUint    = CUtil::CharToString(typeid(unsigned int).name());
StdString CUtil::strLong    = CUtil::CharToString(typeid(long).name());
StdString CUtil::strUlong   = CUtil::CharToString(typeid(unsigned long).name());
StdString CUtil::strFloat   = CUtil::CharToString(typeid(float).name());
StdString CUtil::strDouble  = CUtil::CharToString(typeid(double).name());
StdString CUtil::strBool    = CUtil::CharToString(typeid(bool).name());
StdString CUtil::strLstPt2d = CUtil::CharToString(typeid(std::vector<POINT2D>).name());
StdString CUtil::strColor   = CUtil::CharToString(typeid(COLORREF).name());
StdString CUtil::strString  = CUtil::CharToString(typeid(StdString).name());
StdString CUtil::strPoint2D = CUtil::CharToString(typeid(POINT2D).name());
StdString CUtil::strRect2D  = CUtil::CharToString(typeid(RECT2D).name());
StdString CUtil::strLine2D  = CUtil::CharToString(typeid(LINE2D).name());
StdString CUtil::strCircle2D  = CUtil::CharToString(typeid(CIRCLE2D).name());
StdString CUtil::strEllipse2D = CUtil::CharToString(typeid(ELLIPSE2D).name());
StdString CUtil::strHyperbola2D  = CUtil::CharToString(typeid(HYPERBOLA2D).name());
StdString CUtil::strParabola2D   = CUtil::CharToString(typeid(PARABOLA2D).name());
StdString CUtil::strSpline2D     = CUtil::CharToString(typeid(SPLINE2D).name());
StdString CUtil::strMat2D     = CUtil::CharToString(typeid(MAT2D).name());

LPCTSTR CUtil::ConvVK(WPARAM param)
{
    static StdString ch;
    switch (param)
    {
    case VK_LBUTTON: ch = _T("VK_LBUTTON"); break;
    case VK_RBUTTON: ch = _T("VK_RBUTTON"); break;
    case VK_CANCEL: ch = _T("VK_CANCEL"); break;
    case VK_MBUTTON: ch = _T("VK_MBUTTON"); break;
    case VK_XBUTTON1: ch = _T("VK_XBUTTON1"); break;
    case VK_XBUTTON2: ch = _T("VK_XBUTTON2"); break;
    case VK_BACK: ch = _T("VK_BACK"); break;
    case VK_TAB: ch = _T("VK_TAB"); break;
    case VK_CLEAR: ch = _T("VK_CLEAR"); break;
    case VK_RETURN: ch = _T("VK_RETURN"); break;
    case VK_SHIFT: ch = _T("VK_SHIFT"); break;
    case VK_CONTROL: ch = _T("VK_CONTROL"); break;
    case VK_MENU: ch = _T("VK_MENU"); break;
    case VK_PAUSE: ch = _T("VK_PAUSE"); break;
    case VK_CAPITAL: ch = _T("VK_CAPITAL"); break;
    case VK_KANA: ch = _T("VK_KANA"); break;
        //case VK_HANGEUL: ch = _T("VK_HANGEUL"); break;
        //case VK_HANGUL: ch = _T("VK_HANGUL"); break;
    case VK_JUNJA: ch = _T("VK_JUNJA"); break;
    case VK_FINAL: ch = _T("VK_FINAL"); break;
        //case VK_HANJA: ch = _T("VK_HANJA"); break;
    case VK_KANJI: ch = _T("VK_KANJI"); break;
    case VK_ESCAPE: ch = _T("VK_ESCAPE"); break;
    case VK_CONVERT: ch = _T("VK_CONVERT"); break;
    case VK_NONCONVERT: ch = _T("VK_NONCONVERT"); break;
    case VK_ACCEPT: ch = _T("VK_ACCEPT"); break;
    case VK_MODECHANGE: ch = _T("VK_MODECHANGE"); break;
    case VK_SPACE: ch = _T("VK_SPACE"); break;
    case VK_PRIOR: ch = _T("VK_PRIOR"); break;
    case VK_NEXT: ch = _T("VK_NEXT"); break;
    case VK_END: ch = _T("VK_END"); break;
    case VK_HOME: ch = _T("VK_HOME"); break;
    case VK_LEFT: ch = _T("VK_LEFT"); break;
    case VK_UP: ch = _T("VK_UP"); break;
    case VK_RIGHT: ch = _T("VK_RIGHT"); break;
    case VK_DOWN: ch = _T("VK_DOWN"); break;
    case VK_SELECT: ch = _T("VK_SELECT"); break;
    case VK_PRINT: ch = _T("VK_PRINT"); break;
    case VK_EXECUTE: ch = _T("VK_EXECUTE"); break;
    case VK_SNAPSHOT: ch = _T("VK_SNAPSHOT"); break;
    case VK_INSERT: ch = _T("VK_INSERT"); break;
    case VK_DELETE: 
        ch = _T("VK_DELETE"); 
        break;
    case VK_HELP: ch = _T("VK_HELP"); break;
    case VK_LWIN: ch = _T("VK_LWIN"); break;
    case VK_RWIN: ch = _T("VK_RWIN"); break;
    case VK_APPS: ch = _T("VK_APPS"); break;
    case VK_SLEEP: ch = _T("VK_SLEEP"); break;
    case VK_NUMPAD0: ch = _T("VK_NUMPAD0"); break;
    case VK_NUMPAD1: ch = _T("VK_NUMPAD1"); break;
    case VK_NUMPAD2: ch = _T("VK_NUMPAD2"); break;
    case VK_NUMPAD3: ch = _T("VK_NUMPAD3"); break;
    case VK_NUMPAD4: ch = _T("VK_NUMPAD4"); break;
    case VK_NUMPAD5: ch = _T("VK_NUMPAD5"); break;
    case VK_NUMPAD6: ch = _T("VK_NUMPAD6"); break;
    case VK_NUMPAD7: ch = _T("VK_NUMPAD7"); break;
    case VK_NUMPAD8: ch = _T("VK_NUMPAD8"); break;
    case VK_NUMPAD9: ch = _T("VK_NUMPAD9"); break;
    case VK_MULTIPLY: ch = _T("VK_MULTIPLY"); break;
    case VK_ADD: ch = _T("VK_ADD"); break;
    case VK_SEPARATOR: ch = _T("VK_SEPARATOR"); break;
    case VK_SUBTRACT: ch = _T("VK_SUBTRACT"); break;
    case VK_DECIMAL: ch = _T("VK_DECIMAL"); break;
    case VK_DIVIDE: ch = _T("VK_DIVIDE"); break;
    case VK_F1: ch = _T("VK_F1"); break;
    case VK_F2: ch = _T("VK_F2"); break;
    case VK_F3: ch = _T("VK_F3"); break;
    case VK_F4: ch = _T("VK_F4"); break;
    case VK_F5: ch = _T("VK_F5"); break;
    case VK_F6: ch = _T("VK_F6"); break;
    case VK_F7: ch = _T("VK_F7"); break;
    case VK_F8: ch = _T("VK_F8"); break;
    case VK_F9: ch = _T("VK_F9"); break;
    case VK_F10: ch = _T("VK_F10"); break;
    case VK_F11: ch = _T("VK_F11"); break;
    case VK_F12: ch = _T("VK_F12"); break;
    case VK_F13: ch = _T("VK_F13"); break;
    case VK_F14: ch = _T("VK_F14"); break;
    case VK_F15: ch = _T("VK_F15"); break;
    case VK_F16: ch = _T("VK_F16"); break;
    case VK_F17: ch = _T("VK_F17"); break;
    case VK_F18: ch = _T("VK_F18"); break;
    case VK_F19: ch = _T("VK_F19"); break;
    case VK_F20: ch = _T("VK_F20"); break;
    case VK_F21: ch = _T("VK_F21"); break;
    case VK_F22: ch = _T("VK_F22"); break;
    case VK_F23: ch = _T("VK_F23"); break;
    case VK_F24: ch = _T("VK_F24"); break;
    case VK_NAVIGATION_VIEW: ch = _T("VK_NAVIGATION_VIEW"); break;
    case VK_NAVIGATION_MENU: ch = _T("VK_NAVIGATION_MENU"); break;
    case VK_NAVIGATION_UP: ch = _T("VK_NAVIGATION_UP"); break;
    case VK_NAVIGATION_DOWN: ch = _T("VK_NAVIGATION_DOWN"); break;
    case VK_NAVIGATION_LEFT: ch = _T("VK_NAVIGATION_LEFT"); break;
    case VK_NAVIGATION_RIGHT: ch = _T("VK_NAVIGATION_RIGHT"); break;
    case VK_NAVIGATION_ACCEPT: ch = _T("VK_NAVIGATION_ACCEPT"); break;
    case VK_NAVIGATION_CANCEL: ch = _T("VK_NAVIGATION_CANCEL"); break;
    case VK_NUMLOCK: ch = _T("VK_NUMLOCK"); break;
    case VK_SCROLL: ch = _T("VK_SCROLL"); break;
        //case VK_OEM_NEC_EQUAL: ch = _T("VK_OEM_NEC_EQUAL"); break;
    case VK_OEM_FJ_JISHO: ch = _T("VK_OEM_FJ_JISHO"); break;
    case VK_OEM_FJ_MASSHOU: ch = _T("VK_OEM_FJ_MASSHOU"); break;
    case VK_OEM_FJ_TOUROKU: ch = _T("VK_OEM_FJ_TOUROKU"); break;
    case VK_OEM_FJ_LOYA: ch = _T("VK_OEM_FJ_LOYA"); break;
    case VK_OEM_FJ_ROYA: ch = _T("VK_OEM_FJ_ROYA"); break;
    case VK_LSHIFT: ch = _T("VK_LSHIFT"); break;
    case VK_RSHIFT: ch = _T("VK_RSHIFT"); break;
    case VK_LCONTROL: ch = _T("VK_LCONTROL"); break;
    case VK_RCONTROL: ch = _T("VK_RCONTROL"); break;
    case VK_LMENU: ch = _T("VK_LMENU"); break;
    case VK_RMENU: ch = _T("VK_RMENU"); break;
    case VK_BROWSER_BACK: ch = _T("VK_BROWSER_BACK"); break;
    case VK_BROWSER_FORWARD: ch = _T("VK_BROWSER_FORWARD"); break;
    case VK_BROWSER_REFRESH: ch = _T("VK_BROWSER_REFRESH"); break;
    case VK_BROWSER_STOP: ch = _T("VK_BROWSER_STOP"); break;
    case VK_BROWSER_SEARCH: ch = _T("VK_BROWSER_SEARCH"); break;
    case VK_BROWSER_FAVORITES: ch = _T("VK_BROWSER_FAVORITES"); break;
    case VK_BROWSER_HOME: ch = _T("VK_BROWSER_HOME"); break;
    case VK_VOLUME_MUTE: ch = _T("VK_VOLUME_MUTE"); break;
    case VK_VOLUME_DOWN: ch = _T("VK_VOLUME_DOWN"); break;
    case VK_VOLUME_UP: ch = _T("VK_VOLUME_UP"); break;
    case VK_MEDIA_NEXT_TRACK: ch = _T("VK_MEDIA_NEXT_TRACK"); break;
    case VK_MEDIA_PREV_TRACK: ch = _T("VK_MEDIA_PREV_TRACK"); break;
    case VK_MEDIA_STOP: ch = _T("VK_MEDIA_STOP"); break;
    case VK_MEDIA_PLAY_PAUSE: ch = _T("VK_MEDIA_PLAY_PAUSE"); break;
    case VK_LAUNCH_MAIL: ch = _T("VK_LAUNCH_MAIL"); break;
    case VK_LAUNCH_MEDIA_SELECT: ch = _T("VK_LAUNCH_MEDIA_SELECT"); break;
    case VK_LAUNCH_APP1: ch = _T("VK_LAUNCH_APP1"); break;
    case VK_LAUNCH_APP2: ch = _T("VK_LAUNCH_APP2"); break;
    case VK_OEM_1: ch = _T("VK_OEM_1"); break;
    case VK_OEM_PLUS: ch = _T("VK_OEM_PLUS"); break;
    case VK_OEM_COMMA: ch = _T("VK_OEM_COMMA"); break;
    case VK_OEM_MINUS: ch = _T("VK_OEM_MINUS"); break;
    case VK_OEM_PERIOD: ch = _T("VK_OEM_PERIOD"); break;
    case VK_OEM_2: ch = _T("VK_OEM_2"); break;
    case VK_OEM_3: ch = _T("VK_OEM_3"); break;

    case VK_GAMEPAD_A: ch = _T("VK_GAMEPAD_A"); break;
    case VK_GAMEPAD_B: ch = _T("VK_GAMEPAD_B"); break;
    case VK_GAMEPAD_X: ch = _T("VK_GAMEPAD_X"); break;
    case VK_GAMEPAD_Y: ch = _T("VK_GAMEPAD_Y"); break;
    case VK_GAMEPAD_RIGHT_SHOULDER: ch = _T("VK_GAMEPAD_RIGHT_SHOULDER"); break;
    case VK_GAMEPAD_LEFT_SHOULDER: ch = _T("VK_GAMEPAD_LEFT_SHOULDER"); break;
    case VK_GAMEPAD_LEFT_TRIGGER: ch = _T("VK_GAMEPAD_LEFT_TRIGGER"); break;
    case VK_GAMEPAD_RIGHT_TRIGGER: ch = _T("VK_GAMEPAD_RIGHT_TRIGGER"); break;
    case VK_GAMEPAD_DPAD_UP: ch = _T("VK_GAMEPAD_DPAD_UP"); break;
    case VK_GAMEPAD_DPAD_DOWN: ch = _T("VK_GAMEPAD_DPAD_DOWN"); break;
    case VK_GAMEPAD_DPAD_LEFT: ch = _T("VK_GAMEPAD_DPAD_LEFT"); break;
    case VK_GAMEPAD_DPAD_RIGHT: ch = _T("VK_GAMEPAD_DPAD_RIGHT"); break;
    case VK_GAMEPAD_MENU: ch = _T("VK_GAMEPAD_MENU"); break;
    case VK_GAMEPAD_VIEW: ch = _T("VK_GAMEPAD_VIEW"); break;
    case VK_GAMEPAD_LEFT_THUMBSTICK_BUTTON: ch = _T("VK_GAMEPAD_LEFT_THUMBSTICK_BUTTON"); break;
    case VK_GAMEPAD_RIGHT_THUMBSTICK_BUTTON: ch = _T("VK_GAMEPAD_RIGHT_THUMBSTICK_BUTTON"); break;
    case VK_GAMEPAD_LEFT_THUMBSTICK_UP: ch = _T("VK_GAMEPAD_LEFT_THUMBSTICK_UP"); break;
    case VK_GAMEPAD_LEFT_THUMBSTICK_DOWN: ch = _T("VK_GAMEPAD_LEFT_THUMBSTICK_DOWN"); break;
    case VK_GAMEPAD_LEFT_THUMBSTICK_RIGHT: ch = _T("VK_GAMEPAD_LEFT_THUMBSTICK_RIGHT"); break;
    case VK_GAMEPAD_LEFT_THUMBSTICK_LEFT: ch = _T("VK_GAMEPAD_LEFT_THUMBSTICK_LEFT"); break;
    case VK_GAMEPAD_RIGHT_THUMBSTICK_UP: ch = _T("VK_GAMEPAD_RIGHT_THUMBSTICK_UP"); break;
    case VK_GAMEPAD_RIGHT_THUMBSTICK_DOWN: ch = _T("VK_GAMEPAD_RIGHT_THUMBSTICK_DOWN"); break;
    case VK_GAMEPAD_RIGHT_THUMBSTICK_RIGHT: ch = _T("VK_GAMEPAD_RIGHT_THUMBSTICK_RIGHT"); break;
    case VK_GAMEPAD_RIGHT_THUMBSTICK_LEFT: ch = _T("VK_GAMEPAD_RIGHT_THUMBSTICK_LEFT"); break;
    case VK_OEM_4: ch = _T("VK_OEM_4"); break;
    case VK_OEM_5: ch = _T("VK_OEM_5"); break;
    case VK_OEM_6: ch = _T("VK_OEM_6"); break;
    case VK_OEM_7: ch = _T("VK_OEM_7"); break;
    case VK_OEM_8: ch = _T("VK_OEM_8"); ; break;
    default:
        ch = _T("UNKNOWN"); break;
    }
    return ch.c_str();
}

//特殊文字を表示できる形式に変換
StdString CUtil::ConvAscii(StdChar* pChar)
{
    StdString strRet;
    switch(*pChar)
    {

        case 1:	    strRet = _T("SOH"); break;	
        case 2:	    strRet = _T("STX"); break;	
        case 3:		strRet = _T("ETX"); break;	
        case 4:		strRet = _T("EOT"); break;	
        case 5:		strRet = _T("ENQ"); break;	
        case 6:		strRet = _T("ACK"); break;	
        case 7:		strRet = _T("BEL"); break;	
        case 8:		strRet = _T("BS"); break;	
        case 9:		strRet = _T("TAB"); break;	
        case 10:	strRet = _T("LF"); break;	
        case 11:	strRet = _T("VT"); break;	
        case 12:	strRet = _T("FF"); break;	
        case 13:	strRet = _T("CR"); break;	
        case 14:	strRet = _T("SO"); break;	
        case 15:	strRet = _T("SI"); break;	
        case 16:	strRet = _T("DLE"); break;	
        case 17:	strRet = _T("DC1"); break;	
        case 18:	strRet = _T("DC2"); break;	
        case 19:	strRet = _T("DC3"); break;	
        case 20:	strRet = _T("DC4"); break;	
        case 21:	strRet = _T("NAK"); break;	
        case 22:	strRet = _T("SYN"); break;	
        case 23:	strRet = _T("ETB"); break;	
        case 24:	strRet = _T("CAN"); break;	
        case 25:	strRet = _T("EM"); break;
        case 26:	strRet = _T("SUB"); break;
        case 27:	strRet = _T("ESC"); break;
        case 28:	strRet = _T("FS"); break;
        case 29:	strRet = _T("GS"); break;
        case 30:	strRet = _T("RS"); break;
        case 31:	strRet = _T("US"); break;
        case 127:	strRet = _T("DEL"); break;
        default:
        {
            if((*pChar >= 32) &&
               (*pChar <= 126))
               {
                strRet =  *pChar;
               }
            else
            {
                strRet = StrFormat( _T("%x02"),  *pChar);
            }
        }
    }
    return  strRet;
}



/**
  *  4Byteエンディアン変換.
  *  @param [in] data 変換元D
  *  @return 変換結果
  */
unsigned long CUtil::Swap4Byte(void* data)
{
    unsigned char* pChar;
    pChar = (unsigned char*)data;
    return (DWORD)( ((*pChar)<<24) | ( (*(pChar+1))<<16)| ( (*(pChar+2))<<8) | (*(pChar+3)));
}

/**
  *  Floatエンディアン変換.
  *  @param [in] data 変換元D
  *  @return 変換結果
  */
float CUtil::SwapFloat(void* pdata)
{
    float* pfdata;
    float   data;
    pfdata = (float*)pdata;
    data = *pfdata;
     *((unsigned long*) &(data)) = (((unsigned long)(*((unsigned long*) &(data)))   ) << 24) | \
                         (((unsigned long)(*((unsigned long*) &(data))) & 0x0000ff00) <<  8) | \
                         (((unsigned long)(*((unsigned long*) &(data))) & 0x00ff0000) >>  8) | \
                         (((unsigned long)(*((unsigned long*) &(data)))             ) >> 24);
    return ( data );
}

/**
  *  2Byteエンディアン変換.
  *  @param [in] data 変換元D
  *  @return 変換結果
  */
unsigned short CUtil::Swap2Byte(void* data)
{
    unsigned char* pChar;
    pChar = (unsigned char*)data;
    return WORD(( *(pChar)<<8) | *(pChar+1));
}

/**
  *  実行時間計測.
  *  @param [in] nID  計測用ID
  *  @param [in] nAct 0:Start 1:End
  *  @param [in] sOut 表示文字列 
  */
void CUtil::MesureTime(int nID, int nAct, LPCTSTR sOut)
{
#ifdef _DEBUG
    static DWORD dwTime[100];
    if ((nID < 0) || (nID > 100))
    {
        return;
    }

    if (nAct == 0)
    {
        //Start
        dwTime[nID] = GetTickCount();
    }
    else if (nAct == 1)
    {
        DWORD dwDiff;
        dwDiff = GetTickCount() - dwTime[nID];
        CUtil::DbgOut(_T("Time[%d] %d[ms] %s \n"), nID, dwDiff, sOut);
    }
#endif
}

/**
  *  デバック出力
  *  @return エラーメッセージ
  */
void CUtil::DbgOut( const TCHAR *format, ... )
{
   va_list vl;
   static const size_t iBufSize = 2055/*2048*/;
   TCHAR buf[iBufSize];

   va_start( vl, format );
   _vsntprintf_s( buf, iBufSize, format, vl );
   OutputDebugString( (LPCTSTR)buf );
   va_end( vl );
   return;
}


/**
  *  エラーメッセージ取得.
  *  @return エラーメッセージ
  */
StdString CUtil::GetErrMsg()
{
    StdString ret;
    LPTSTR lpBuffer = NULL;

	DWORD dwError = GetLastError();

	DWORD rdwRet = FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
        NULL,
		dwError,
        LANG_USER_DEFAULT,
        (LPTSTR)&lpBuffer,
        0,
        NULL );

	if (rdwRet == 0)
	{
		StdStringStream strRet;
		STD_DBG(CUtil::GetErrMsg().c_str());

		strRet << StdFormat(_T("Last Error %d")) % dwError;
		return strRet.str();
	}
	else
	{
		ret = lpBuffer;
		LocalFree(lpBuffer);
	}

    return ret;
}


/**
 * @brief	バイナリダンプ
 * @param	[in] pData  バイナリデータ
 * @param	[in] lLen  データ長
 * @return  なし
 * @note    ログ出力用
 */
void CUtil::BinDump(const TCHAR* pData, long lLen)
{
    long lCnt;
    long lLineLen;
    long lLineCnt;
    long lWidth = 16;
    TCHAR szBuf[1024];
    TCHAR c;

    for (lCnt = 0; lCnt < lLen; lCnt += lWidth )
    {
        lLineLen = lLen - lCnt;
        if ( lLineLen > lWidth)  {lLineLen = lWidth;}
        memset(szBuf, 0, sizeof(szBuf));

		for(lLineCnt = 0; lLineCnt < lLineLen; lLineCnt++)
        {
			_sntprintf_s( &szBuf[lLineCnt * 3], sizeof(szBuf), _TRUNCATE, _T( "%02x "), (CHAR)pData[lCnt + lLineCnt]);
		}

		for(lLineCnt = lLineLen; lLineCnt < lWidth; lLineCnt++)
        {
			_sntprintf_s( &szBuf[lLineCnt * 3],  _TRUNCATE, sizeof(szBuf), _T("   "));
        }

		for(lLineCnt = 0; lLineCnt < lLineLen; lLineCnt++)
        {
            c = pData[lCnt + lLineCnt];
            c = (isprint(c) ? c : _T('.') );
			_sntprintf_s( &szBuf[lWidth * 3 + lLineCnt], sizeof(szBuf),  _TRUNCATE,_T("%c"), c );
		}
        OutputDebugString(szBuf);
        OutputDebugString(_T("\n"));
	}
}

/**
  *  @brief   char->StdString変換.
  *  @param   [in] str 入力文字列
  *  @return  出力文字列
  */
StdString CUtil::CharToString(const char* str)
{
    StdString strRet = _T("");

    if (!str)
    {
        return strRet;
    }

#ifdef  UNICODE             
  	wchar_t*	psToSemStringBuf;
    size_t iConvCnt;

    errno_t iErrorNo;
	iErrorNo = mbstowcs_s( &iConvCnt, NULL , 0,
                            str,
                            _TRUNCATE );

    if ( iErrorNo != 0 )
    {
        CString strData;
        strData = str;
        strRet = (LPCTSTR)strData;
        return strRet;
    }


    size_t iBufSize =  (iConvCnt + 1) * sizeof(wchar_t);
	psToSemStringBuf = (wchar_t*)malloc( iBufSize );
    
    if (psToSemStringBuf == 0)
    {
        return strRet;
    }
    

	iErrorNo = mbstowcs_s( NULL, psToSemStringBuf , iConvCnt + 1,
               str,
               _TRUNCATE );

    if ( iErrorNo == 0 )
	{
		strRet = psToSemStringBuf;
	}
    else
    {
        ;
    }

	free( psToSemStringBuf );
 	return strRet;

#else
     strRet = str;
     return strRet;
#endif
}

/**
  *  @brief   StdString->char 変換.
  *  @param   [in] strString 変換元文字列 
  *  @return               変換後文字列
  */
std::string CUtil::StringToChar(StdString strString)
{
#ifdef  UNICODE      
    std::string strRet;

  	char*	psToStringBuf;

    size_t iLen = strString.length();
    size_t iRet;
    errno_t iErrorNo;

    if (iLen > 0)
    {
	    psToStringBuf = (char*)malloc( iLen * 2 );

        if (psToStringBuf == 0)
        {
            return strRet;
        }

        iErrorNo = wcstombs_s(&iRet, psToStringBuf, iLen * 2, strString.c_str(), _TRUNCATE);

	    if ( iErrorNo == 0 )
	    {
		    strRet = psToStringBuf;
	    }

	    free( psToStringBuf );
    }
    return strRet;
    
#else
     return strString;   
#endif

}

/**
  *  @brief   空白除去.
  *  @param   [in] str 空白を除去する文字列
  *  @return  空白を除去後の文字列
  */
StdString CUtil::Trim(const StdString& str) 
{
    if(str.length() == 0){
        return str;
    }
    size_t start = str.find_first_not_of(_T(" \t\r\n"));
    size_t end = str.find_last_not_of(_T(" \t\r\n"));
    if(start == str.npos){
        return _T("");
    }
    return StdString(str, start, end - start + 1);
}

/**
  *  @brief   引用符除去.
  *  @param   [in] str 引用符を除去する文字列
  *  @return  引用符を除去後の文字列
  */
StdString CUtil::TrimQuotation(const StdString& str) 
{                
    if(str.length() == 0){
        return str;
    }
    size_t start = str.find_first_not_of(_T("\"\'"));
    size_t end = str.find_last_not_of(_T("\"\'"));
    if(start == str.npos){
        return _T("");
    }
    return StdString(str, start, end - start + 1);
}

/**
  *  @brief   書式指定 string
  *  @param   [in] format 書式指定文字列
  *  @return  文字列
  */ 
StdString CUtil::StrFormat( const TCHAR *format, ... )
{
   va_list vl;
   static const size_t iBufSize = 4096;
   TCHAR buf[iBufSize];

   va_start( vl, format );
   _vsntprintf_s( buf, iBufSize, format, vl );
   StdString ret( buf );
   va_end( vl );
   return ret;
}

StdString CUtil::Last(const StdString& str)
{
    StdString ret;
    auto pt = str.end();
    if (pt != str.begin())
    {
        pt--;
        ret =  *pt;
    }
    return ret;
}

/**
 * @brief   経過時間取得.
 * @param   [in] 前回の時間[msec]
 * @param   [in] 今回の時間[msec]
 * @return  時間差 [msec]
 */ 
DWORD CUtil::DiffTime( DWORD old_time, DWORD cur_time)
{
    if (old_time > cur_time) {
        return  0xFFFFFFFF - old_time +  cur_time;
    } else {
        return cur_time - old_time;
    }
}

/**
 * @brief   四捨五入.
 * @param   [in] 値
 * @return  四捨五入結果
 */ 
long CUtil::Round( double val)
{
    return long(floor( val + 0.5));
}



// 点 p が反時計回りの三角形 abc の内側にあるかどうか判定
bool CUtil::IsInnerTriangle(const POINT2D& p, 
                     const POINT2D& a,
                     const POINT2D& b,
                     const POINT2D& c) 
{
    // p が ab の右側にあれば三角形の外側にある
    if ((p - a).Cross(b - a) < 0.0) return false;
    // p が bc の右側にあれば三角形の外側にある
    if ((p - b).Cross(c - b) < 0.0) return false;
    // p が ca の右側にあれば三角形の外側にある
    if ((p - c).Cross(a - c) < 0.0) return false;
    return true;
}

bool CUtil::IsInnerTriangle2(const POINT2D& p, 
                     const POINT2D& a,
                     const POINT2D& b,
                     const POINT2D& c) 
{
    if (!IsInnerTriangle(p, a, b, c))
    {
        return IsInnerTriangle(p, a, c, b);
    }
    return true;
}


//!<角度拡張
//!< Start Endは左周りに設定 (deg)
bool CUtil::UpdateeAngle(double* pStart, double* pEnd, double dAngle)
{
    double dAngleN   = CUtil::NormAngle(dAngle); 
    double dTmpStart = CUtil::NormAngle(*pStart);
    double dTmpEnd   = CUtil::NormAngle(*pEnd);


    if (fabs(dTmpStart - dTmpEnd) < NEAR_ZERO)
    {
        return false;
    }
   
    if ((dTmpEnd - dTmpStart) >= 0)
    {

        if ( (dAngleN >= dTmpStart) &&
             (dAngleN <  dTmpEnd))
        {
            return false;
        }
    }
    else
    {
        if ( (dAngleN >= dTmpStart) &&
             (dAngleN <= 360.0))
        {
            return false;
        }

        if ( (dAngleN >= 0) &&
             (dAngleN <  dTmpEnd))
        {
            return false;
        }
    }

    double dS = CUtil::DiffAngle(dAngleN, dTmpStart);
    double dE = CUtil::DiffAngle(dAngleN, dTmpEnd);

    if (dS < dE)
    {
        *pStart = dAngle;
    }
    else
    {
        *pEnd = dAngle;
    }
    return true;
}

//3点から角度を計算する
bool CUtil::CalcAngle(double* pAngle,
               const POINT2D& pt1,
               const POINT2D& pt2,
               const POINT2D& ptCenter)
                
{
    POINT2D p1 = pt1 - ptCenter;
    POINT2D p2 = pt2 - ptCenter;

    double dLen1 = p1.Length();
    double dLen2 = p2.Length();

    double dL = dLen1 * dLen2;

    if (dL < NEAR_ZERO)
    {
        *pAngle = 0.0;
        return false;
    }

    double dA1 = p1.Angle();
    double dA2 = p2.Angle();


    double dAngle = CUtil::NormAngle(dA2 - dA1);

    *pAngle = dAngle;

    return true;
}

/**
 * @brief   点加算.
 * @param [in] pt1 点1
 * @param [in] pt2 点2
 * @return 加算結果
 */ 
POINT CUtil::AddPoint(const POINT& pt1, const POINT& pt2)
{
    POINT ptRet;
    ptRet.x = pt1.x + pt2.x;
    ptRet.y = pt1.y + pt2.y;
    return ptRet;
}

/**
 * @brief   点減算
 * @param [in] pt1 点1
 * @param [in] pt2 点2
 * @return 減算結果
 */ 
POINT CUtil::SubPoint(const POINT& pt1, const POINT& pt2)
{
    POINT ptRet;
    ptRet.x = pt1.x - pt2.x;
    ptRet.y = pt1.y - pt2.y;
    return ptRet;
}

/**
 * @brief   点等価
 * @param [in] pt1 点1
 * @param [in] pt2 点2
 * @return 減算結果
 */ 
bool CUtil::EqPoint(const POINT& pt1, const POINT& pt2)
{
    if (pt1.x != pt2.x){return false;}
    if (pt1.y != pt2.y){return false;}
    return true;
}

/**
 * @brief   点等価
 * @param [in] pt1 点1
 * @param [in] pt2 点2
 * @return 減算結果
 */ 
double CUtil::LenPoint(const POINT& pt1, const POINT& pt2)
{
    POINT pt = SubPoint(pt1, pt2);
    return LenPoint(pt);
}

/**
 * @brief   点等価
 * @param [in] pt1 点1
 * @param [in] pt2 点2
 * @return 減算結果
 */ 
double CUtil::LenPoint(const POINT& pt)
{
    return sqrt(pt.x * pt.x +  pt.y *  pt.y);
}

//!<   交点(直線パラメータ)
bool CUtil::CalcCrossPoint(POINT2D* pPt,
                       double dA1, double dB1, double dC1,
                       double dA2, double dB2, double dC2)
{
    double dD = dA1 * dB2 - dA2 * dB1;

    if ((dD * dD) < NEAR_ZERO)
    {
        //解がない
        return false;
    }
    
    POINT2D ptRet;

    ptRet.dX = (dB1 * dC2 - dB2 * dC1) / dD;
    ptRet.dY = (dA2 * dC1 - dA1 * dC2) / dD;
    
    *pPt = ptRet;
    return true;
}

//!< 三角関数取得 dAngle:degree
void CUtil::GetTriFunc(double dAngle, double* pSin, double* pCos)
{
    //切りの良い角度の時三角関数の計算誤差を修正する

    double dSin;
    double dCos;

    if (( dAngle <  NEAR_ZERO) &&
        ( dAngle > -NEAR_ZERO))
    {
        dSin = 0.0;
        dCos = 1.0;
    }
    else if (( (dAngle - 90.0) <  NEAR_ZERO) &&
             ( (dAngle - 90.0) > -NEAR_ZERO))
    {
        dSin = 1.0;
        dCos = 0.0;
    }
    else if (( (dAngle - 180.0) <  NEAR_ZERO) &&
             ( (dAngle - 180.0) > -NEAR_ZERO))
    {
        dSin =  0.0;
        dCos = -1.0;
    }
    else if (( (dAngle - 270.0) <  NEAR_ZERO) &&
             ( (dAngle - 270.0) > -NEAR_ZERO))
    {
        dSin = -1.0;
        dCos =  0.0;
    }
    else
    {
        double dTheta;
        dTheta = DEG2RAD * dAngle;
        dSin = sin(dTheta);
        dCos = cos(dTheta);
    }
    *pSin = dSin;
    *pCos = dCos;
}

//!< 三角関数取得 dAngle:degree
/*
void GetTriFuncL(double dAngle, double* pSinl, double* pCosl)
{
    //切りの良い角度の時三角関数の計算誤差を修正する

    double dSinl;
    double dCosl;

    if (( dAngle <  NEAR_ZERO) &&
        ( dAngle > -NEAR_ZERO))
    {
        dSinl = 0.0;
        dCosl = 1.0;
    }
    else if (( (dAngle - 90.0) <  NEAR_ZERO) &&
             ( (dAngle - 90.0) > -NEAR_ZERO))
    {
        dSinl = 0.0;
        dCosl = 1.0;
    }
    else if (( (dAngle - 180.0) <  NEAR_ZERO) &&
             ( (dAngle - 180.0) > -NEAR_ZERO))
    {
        dSinl = 0.0;
        dCosl = 1.0;
    }
    else if (( (dAngle - 270.0) <  NEAR_ZERO) &&
             ( (dAngle - 270.0) > -NEAR_ZERO))
    {
        dSinl = 0.0;
        dCosl = 1.0;
    }
    else
    {
        double dTheta;
        dTheta = DEG2RAD * dAngle;
        dSin = sinl(dTheta);
        dCos = cosl(dTheta);
    }
    *pSinl = dSinl;
    *pCosl = dCosl;
}
*/

//!< 角度計算
double CUtil::GetAngle(const POINT2D& pPt1, const POINT2D& pPt2)
{
    double dAngle;

    dAngle = atan2(pPt2.dY - pPt1.dY,
                   pPt2.dX - pPt1.dX) * RAD2DEG; 
    return dAngle;
}

//!<角度判定 
//!< Start Endは左周りに設定 (deg)
bool CUtil::IsInsideAngle(double dStart, double dEnd, double dAngle)
{
    double dAngleN   = CUtil::NormAngle(dAngle); 
    double dTmpStart = CUtil::NormAngle(dStart);
    double dTmpEnd   = CUtil::NormAngle(dEnd);


    if (fabs(dStart - dEnd) < NEAR_ZERO)
    {
        return true;
    }
   
    if ((dTmpEnd - dTmpStart) >= 0)
    {

        if ( (dAngleN >= dTmpStart) &&
             (dAngleN <  dTmpEnd))
        {
            return true;
        }
        return false;
    }
    else
    {
        if ( (dAngleN >= dTmpStart) &&
             (dAngleN <= 360.0))
        {
            return true;
        }

        if ( (dAngleN >= 0) &&
             (dAngleN <  dTmpEnd))
        {
            return true;
        }
        return false;
    }
    return false;
}



//!<鋭角計算
double CUtil::DiffAngle(double dAngle1, double dAngle2)
{
    double d1   = CUtil::NormAngle(dAngle1); 
    double d2   = CUtil::NormAngle(dAngle2);

    double dAngle;

    dAngle = fabs(d1 - d2);

    if (dAngle > 180.0)
    {
        dAngle = 360.0 - dAngle;
    }
    return dAngle;
}

                       
//角度の差を±９０°に変換
double CUtil::DiffAngle90(double dAngle1, double dAngle2)
{
    double d1   = CUtil::NormAngle(dAngle1); 
    double d2   = CUtil::NormAngle(dAngle2);

    double dDiff = d2 - d1;
    double dRet = 0.0;
    //角度の差を±９０°に変換
    if (dDiff < -270.0)
    {
        dRet =  dDiff  + 360.0;
    }
    else if ((dDiff < -90.0) && (dDiff >= -270.0))
    {
        dRet =  - dDiff  - 180.0;
    }
    else if ((dDiff < 90.0) && (dDiff >= -90))
    {
        dRet =  dDiff;
    }
    else if ((dDiff < 270.0) && (dDiff >= 90))
    {
        dRet =  180.0 - dDiff;
    }
    else if ((dDiff < 360.0) && (dDiff >= 270.0))
    {
        dRet =  dDiff - 360.0;
    }

    return dRet;
}


//!< スケーリングによる角度変換(deg)
double CUtil::SclAngle(double dAngle, double dSclX, double dSclY)
{
    double dRet;
    double dC;
    double dS;
    CUtil::GetTriFunc(dAngle, &dS, &dC);


    dRet = atan2(dSclY * dS, dSclX * dC) * RAD2DEG;
    return dRet;
}

/**
// 点 pt が有向線分 e(pt1, pt2) の左右どちら側にあるか調べる。
// 点 pt が有向線分 e の　左側にある場合  1 を、
// 　    　有向線分 e の直線上にある場合  0 を、
// 　    　有向線分 e の　右側にある場合 -1 を返す。
*/
int CUtil::Side(const IPoint2D& pt1, const IPoint2D& pt2, const IPoint2D& pt)
{

    // 有向線分 ( の外積の z 成分を求める
    double d  = pt.GetX() * (pt1.GetY()  - pt2.GetY()) +
        pt1.GetX()  * (pt2.GetY()  - pt.GetY()) + 
        pt2.GetX()  * (pt.GetY()   - pt1.GetY());
    if (fabs(d) < NEAR_ZERO)   {return 0;}
    else if ( d > 0 )          {return  1;} // 左
    else if ( d < 0 )          {return -1;}// 右
    return 0;
}

//------------------------------------------------------------
// ２点で示される直線をクリッピングする
//------------------------------------------------------------
bool CUtil::Clipping(RECT Rec, POINT *P1, POINT *P2)
{
    POINT P;
    int   Cl1,Cl2,C;
    int   i;
    
    P.x = 0;
    P.y = 0;

    for ( i = 0; i < 3; i++)
    { 
        Cl1 = Cval3( P1, Rec);   //点１領域情報
        Cl2 = Cval3( P2, Rec);   //点２領域情報
        
        //２点がクリッピング領域内にあるときは、クリッピングなし
        if ( (Cl1 == 0) && (Cl2 == 0) )
        {
            return true;
        }
        else if  (Cl1 == Cl2) 
        {
            //同じ領域の場合は書込まない
            return false;
        }
        //
        if      (Cl1 == 0) {C = Cl2;}
        else if (Cl2 == 0) {C = Cl1;}


        if ((Cl1 == 0 ) || (Cl2 == 0 ))
        {
            if        (C & POS_LEFT)  {P = ClipLeft  (Rec, P1,P2);}
            else if   (C & POS_RIGHT) {P = ClipRight (Rec, P1,P2);} 
            else if   (C & POS_TOP)   {P = ClipTop   (Rec, P1,P2);} 
            else if   (C & POS_BOTTOM){P = ClipBottom(Rec, P1,P2);} 
            if (C == Cl1) {*P1 = P; Cl1 = Cval3( P1, Rec);}
            else          {*P2 = P; Cl2 = Cval3( P2, Rec);}

            if ( (Cl1 == 0) && (Cl2 == 0) )
            {
                return true;
            }
        }

        if        (Cl1 & POS_LEFT)  {P = ClipLeft  (Rec, P1,P2);}
        else if   (Cl1 & POS_RIGHT) {P = ClipRight (Rec, P1,P2);} 
        else if   (Cl1 & POS_TOP)   {P = ClipTop   (Rec, P1,P2);} 
        else if   (Cl1 & POS_BOTTOM){P = ClipBottom(Rec, P1,P2);} 
        *P1 = P;

        if        (Cl2 & POS_LEFT)  {P = ClipLeft  (Rec, P1,P2);}
        else if   (Cl2 & POS_RIGHT) {P = ClipRight (Rec, P1,P2);} 
        else if   (Cl2 & POS_TOP)   {P = ClipTop   (Rec, P1,P2);} 
        else if   (Cl2 & POS_BOTTOM){P = ClipBottom(Rec, P1,P2);}
        *P2 = P;

        Cl1 = Cval3( P1, Rec);   //点１領域情報
        Cl2 = Cval3( P2, Rec);   //点２領域情報
        if ( (Cl1 == 0) && (Cl2 == 0) )
        {
            return true;
        }
    }    
    return false;
}


//------------------------------------------------------------
// 点がクリッピング領域のどちらにあるか4bitの値で示す
//------------------------------------------------------------
int CUtil::Cval3(POINT *P, RECT Rec)
{
    int c = 0;

    if (P->x < Rec.left)   {c |= POS_LEFT  ;}
    if (P->x > Rec.right)  {c |= POS_RIGHT ;} 
    if (P->y < Rec.top)    {c |= POS_TOP   ;}
    if (P->y > Rec.bottom) {c |= POS_BOTTOM;}
    return c;
}

//------------------------------------------------------------
// 左領域でクリッピング
//------------------------------------------------------------
POINT CUtil::ClipLeft(RECT Rec, POINT* P1, POINT* P2)
{
    POINT P;
    int dx;
    int dy;

    dx = P2->x - P1->x;
    dy = P2->y - P1->y;
    
    if (dx != 0)
    {
        P.y = P1->y + dy * (Rec.left - P1->x) / dx;
        P.x = Rec.left;
    }
    else
    {
        if      (P2->y < Rec.top)   {P.y = Rec.top;}
        else if (P2->y > Rec.bottom){P.y = Rec.bottom;}

        if      (P1->y < Rec.top)   {P.y = Rec.top;}
        else if (P1->y > Rec.bottom){P.y = Rec.bottom;}
        
        P.x = P1->x;
   }
   return P;
}

//------------------------------------------------------------
// 右領域でクリッピング
//------------------------------------------------------------
POINT CUtil::ClipRight(RECT Rec, POINT* P1, POINT* P2)
{
    POINT P;
    int dx;
    int dy;

    dx = P2->x -P1->x;
    dy = P2->y -P1->y;
    
    if (dx != 0)
    {
        P.y = P1->y + dy * (Rec.right - P1->x) / dx;
        P.x = Rec.right;
    }
    else
    {
        if      (P2->y < Rec.top)   {P.y = Rec.top;}
        else if (P2->y > Rec.bottom){P.y = Rec.bottom;}

        if      (P1->y < Rec.top)   {P.y = Rec.top;}
        else if (P1->y > Rec.bottom){P.y = Rec.bottom;}
        
        P.x = P1->x;
   }
   return P;
}

//------------------------------------------------------------
// 上領域でクリッピング
//------------------------------------------------------------
POINT CUtil::ClipTop(RECT Rec, POINT* P1, POINT* P2)
{
    POINT P;
    int dx;
    int dy;

    dx = P2->x - P1->x;
    dy = P2->y - P1->y;
    
    if (dy != 0)
    {
        P.x = P1->x + dx * (Rec.top - P1->y) / dy;
        P.y = Rec.top;
    }
    else
    {
        if      (P2->x < Rec.left)   {P.x = Rec.left;}
        else if (P2->x > Rec.right)  {P.x = Rec.right;}

        if      (P1->x < Rec.left)   {P.x = Rec.left;}
        else if (P1->x > Rec.right)  {P.x = Rec.right;}
        
        P.y = P1->y;
   }
   return P;
}

//------------------------------------------------------------
// 下領域でクリッピング
//------------------------------------------------------------
POINT CUtil::ClipBottom(RECT Rec, POINT* P1, POINT* P2)
{
    POINT P;
    int dx;
    int dy;

    dx = P2->x - P1->x;
    dy = P2->y - P1->y;
    
    if (dy != 0)
    {
        P.x = P1->x + dx * (Rec.bottom - P1->y) / dy;
        P.y = Rec.bottom;
    }
    else
    {
        if      (P2->x < Rec.left)   {P.x = Rec.left;}
        else if (P2->x > Rec.right)  {P.x = Rec.right;}

        if      (P1->x < Rec.left)   {P.x = Rec.left;}
        else if (P1->x > Rec.right)  {P.x = Rec.right;}
        
        P.y = P1->y;
   }
   return P;
}

//!< 角度正規化
double CUtil::NormAngle(double dAngle)
{
    double dRet;
    dRet = std::fmod( dAngle, 360.0); 

    if (fabs (dRet) < NEAR_ZERO)
    {
        dRet = 0.0;
    }
    else
    {
        if (dRet < 0)
        {
            dRet = 360.0 + dRet;
        }
    }
    return dRet;
}

//!< 角度正規化(0-180)
double CUtil::NormAngle2(double dAngle)
{
    double dRet;
    dRet = std::fmod( dAngle, 180.0); 

    if (fabs (dRet) < NEAR_ZERO)
    {
        dRet = 0.0;
    }
    else
    {
        if (dRet < 0)
        {
            dRet = 180.0 + dRet;
        }
    }
    return dRet;
}


//!< 象限取得
int CUtil::GetQuadrant(double dAngle)
{
    int ret;
    dAngle = NormAngle(dAngle);

    if     ((dAngle >=   0.0) && (dAngle <  90.0)){ret = 1;}
    else if((dAngle >=  90.0) && (dAngle < 180.0)){ret = 2;}
    else if((dAngle >= 180.0) && (dAngle < 270.0)){ret = 3;}
    else  {ret = 4;}
    return ret;
}


double CUtil::GetDirection(const POINT2D& ptCenter,
    const POINT2D& ptDir,
    const POINT2D& pt) 
{
    //ptCenter->ptDirとptCenter->ptが同一方向であれば１
    //逆方向なら-1を返す
    //同一直線上かどうかの判定は行わない

    if( ptCenter.Distance(ptDir) < NEAR_ZERO)
    {
        return 0.0f;
    }

    if( ptCenter.Distance(pt) < NEAR_ZERO)
    {
        return 0.0f;
    }


    double dX = ptDir.dX - ptCenter.dX;
    double dY = ptDir.dY - ptCenter.dY;

    double dX2 = pt.dX - ptCenter.dX;
    double dY2 = pt.dY - ptCenter.dY;

    double dN;
    if (fabs(dX) > fabs(dY))
    {

        dN = dX * dX2;
    }
    else
    {
        dN = dY * dY2;
    }

    if (dN > 0.0)
    { 
        return 1.0;
    }

    return -1.0;
}

//!< 拡張子取得
StdString CUtil::GetExt(StdPath path)
{
	StdString ret;
    size_t iPos = GetPathStr(path.filename()).rfind(_T('.'));

    if(std::string::npos != iPos)
    {
    	ret = GetPathStr(path.filename()).substr(iPos);
    }
    return ret;
}


void CUtil::SPrintfWStringV(const std::wstring &str, va_list argptr)
{
    const static int iSize = 4096;
    wchar_t    szMsg[iSize];
        
    _vsnwprintf_s(szMsg, iSize, _TRUNCATE, str.c_str(), argptr);
    DB_PRINT(szMsg);
}

void CUtil::FormatV(std::wstring &str, const std::wstring &fmt, va_list argptr)
{
    const static int iSize = 4096;
    wchar_t    szMsg[iSize];
        
    _vsnwprintf_s(szMsg, iSize, _TRUNCATE, fmt.c_str(), argptr);
    str = szMsg;
}

void CUtil::SPrintfWString(const std::wstring str, ...)
{
    va_list argList;

    va_start(argList, str);
    SPrintfWStringV(str, argList);

    va_end(argList);
}

void CUtil::Tokenize(std::vector<std::wstring>* pParsedData, std::wstring strIn)
{
    if (pParsedData == NULL)
    {
        return;
    }

    //typedef boost::escaped_list_separator<wchar_t> esc_sep;
    typedef boost::char_separator<wchar_t>  separator;
    typedef boost::tokenizer<separator ,std::wstring::const_iterator, std::wstring> tokenDef;



    separator sep(_T("\n\r") ,              //デリミタの集合
                 _T(""),                    //解析結果にトークンとして残すデリミタの集合
                 boost::keep_empty_tokens); //区切り文字が連続した場合の挙動 
                                            

    //esc_sep sep(charSep);
    //esc_sep sep(_T('\\'), charSep, _T('\"'));
    //strIn += _T("");
    tokenDef tokens( strIn, sep);


    std::wstring strLine;
    tokenDef::iterator tok_iter;
    for (tok_iter =  tokens.begin();
         tok_iter != tokens.end();
         ++tok_iter) 
    {
        pParsedData->push_back(*tok_iter);
    }
}

void CUtil::ConvUni(asIScriptEngine * pEngine, 
            UniVal64* pUni, 
            void* ref, int refTypeId)
{

	if( refTypeId & asTYPEID_OBJHANDLE )
	{

        pUni->pVal = *(void**)ref;
    }
	else if( refTypeId & asTYPEID_MASK_OBJECT )
	{
        //For String
		asIObjectType *ot = pEngine->GetObjectTypeById(refTypeId);
        if (ot)
        {
            std::string strName = ot->GetName();
            if( strName == "string")
            {
                std::wstring* pStr = reinterpret_cast<std::wstring*>(ref);
                pUni->pcVal = reinterpret_cast<const void*>(pStr->c_str());
            }
        }
    }
    else
    {
        switch(refTypeId)
        {
        case asTYPEID_VOID:
            pUni->pVal = reinterpret_cast<void*>(ref);
            break;

        case asTYPEID_BOOL:
            pUni->bVal = *reinterpret_cast<bool*>(ref);
            break;
        case asTYPEID_INT8:
            pUni->cVal = *reinterpret_cast<char*>(ref);
            break;

	    case asTYPEID_INT16:
            pUni->sVal = *reinterpret_cast<short*>(ref);
            break;

	    case asTYPEID_INT32:
            pUni->iVal = *reinterpret_cast<int*>(ref);
            break;

	    case asTYPEID_INT64:
            pUni->iVal64 = *reinterpret_cast<__int64*>(ref);
            break;

	    case asTYPEID_UINT8:
            pUni->ucVal = *reinterpret_cast<unsigned char*>(ref);
            break;

	    case asTYPEID_UINT16:
            pUni->usVal = *reinterpret_cast<unsigned short*>(ref);
            break;

	    case asTYPEID_UINT32:
            pUni->uiVal = *reinterpret_cast<unsigned int*>(ref);
            break;

        case asTYPEID_UINT64:
            pUni->uiVal64 = *reinterpret_cast<unsigned __int64*>(ref);
            break;

	    case asTYPEID_FLOAT:
            pUni->fVal = *reinterpret_cast<float*>(ref);
            break;

	    case asTYPEID_DOUBLE:
            pUni->dVal = *reinterpret_cast<double*>(ref);
            break;

        default:
            break;

        }
    }
}



//!< 大文字変換
StdString CUtil::ToUpper(StdString Str)
{
    StdString strRet;
    strRet = Str;
    std::wstring::iterator itBegin;
    std::wstring::iterator itEnd;

    itBegin = strRet.begin();
    itEnd   = strRet.end();

	std::transform(itBegin, itEnd, itBegin, std::towupper);
	return strRet;
}

//!< 小文字変換
StdString CUtil::ToLower(StdString Str)
{
    StdString strRet;
    strRet = Str;
    std::wstring::iterator itBegin;
    std::wstring::iterator itEnd;

    itBegin = strRet.begin();
    itEnd   = strRet.end();

    std::transform( itBegin, itEnd, itBegin, std::towlower); 
    return strRet;
}

//!< メニューアイテム追加
void CUtil::AddMenuItem(HMENU hmenu, const StdChar* lpszItemName, int nId, HMENU hmenuSub)
{
	MENUITEMINFO mii;
	
	mii.cbSize = sizeof(MENUITEMINFO);
	mii.fMask  = MIIM_ID | MIIM_TYPE;
	mii.wID    = nId;
    static const int iNameSize = 1024;
    StdChar    szName[iNameSize];


	if (lpszItemName != NULL) 
    {
        memset(szName, 0 ,sizeof(szName));
        _tcscpy_s(szName, iNameSize, lpszItemName);
		mii.fType      = MFT_STRING;
		mii.dwTypeData = szName;
	}
	else
    {
		mii.fType = MFT_SEPARATOR;
    }

	if (hmenuSub != NULL) 
    {
		mii.fMask   |= MIIM_SUBMENU;
		mii.hSubMenu = hmenuSub;
	}

	InsertMenuItem(hmenu, nId, FALSE, &mii);
}

//!< メニューチェックアイテム追加
void CUtil::AddMenuCheck(HMENU hmenu, const StdChar* lpszItemName, int nId, bool bRadio)
{
    MENUITEMINFO mii;

    mii.cbSize = sizeof(MENUITEMINFO);
    mii.fMask  = MIIM_ID | MIIM_TYPE | MIIM_CHECKMARKS;
    mii.wID    = nId;
    static const int iNameSize = 1024;
    StdChar    szName[iNameSize];


    memset(szName, 0 ,sizeof(szName));
    _tcscpy_s(szName, iNameSize, lpszItemName);
    mii.fType      = MFT_STRING;

    if (bRadio)
    {
        mii.fType |= MFT_RADIOCHECK;
    }
    mii.dwTypeData = szName;

    InsertMenuItem(hmenu, nId, FALSE, &mii);
}

/**
 * @brief   メニュー項目変更
 * @param   [in] hMenu
 * @retval  なし
 * @note	
 */
void CUtil::ConvertMenuString(HMENU hMenu)
{
    CMenu menu;
    MENUITEMINFO  info;

    if (!::IsMenu(hMenu))
    {
        return;
    }

    if(!menu.Attach(hMenu))
    {
        return;
    }

    int iMenuCount = menu.GetMenuItemCount();
    for (int iCnt =0; iCnt < iMenuCount; iCnt++)

    {
        memset(&info, 0, sizeof(info));
        info.cbSize = sizeof(info);
        info.fMask = MIIM_TYPE | MIIM_SUBMENU;
        menu.GetMenuItemInfo(iCnt, &info, TRUE);

        StdString strMenu;
        if( info.fType == MFT_STRING)
        {
            CString strMenuItem;
            menu.GetMenuString(iCnt, strMenuItem, MF_BYPOSITION);
            
            strMenu = GET_SYS_STR((LPCTSTR)strMenuItem);
            
            info.dwTypeData = const_cast<StdChar*>(strMenu.c_str());
            info.cch        = SizeToUInt(strMenu.length());
            menu.SetMenuItemInfo(iCnt, &info, TRUE);
        }

        if (info.hSubMenu != NULL)
        {
            CUtil::ConvertMenuString(info.hSubMenu);
        }
    }
    menu.Detach();
}

//!< 書式指定テキスト
bool CUtil::FormatedText(StdString strText,int* pWidth, int* pPos, StdString* pHeaderText)
{
    // このような書式を分解して取り出す 
    // リストコントロールのヘッダーの書式指定に使用する
    // L100,HELLO

    using namespace boost::xpressive;
    size_t iMatchSize;
    StdStrXMatchResult matchResult;

    if (pWidth)
    {
        *pWidth   = 100;
    }

    if (pPos)
    {
        *pPos     = LVCFMT_LEFT;
    }

    if (pHeaderText)
    {
        *pHeaderText = strText;
    }


    StdStrXregex satatic_reg = (s1 = (as_xpr(_T('L')) | _T('R') |_T('C'))) >> 
                                     (s2 =+_d) >> *_s >> _T(",") >> /**_s >>*/ 
                                     (s3 = *_) /*>> *_s*/; 

    bool bRet = regex_match( strText , matchResult, satatic_reg);

    if (!bRet)
    {
        STD_DBG(_T("FormatedText regex_match Error %s"), strText.c_str());
        return false;
    }

    iMatchSize =  matchResult.size();
 
    if (iMatchSize != 4)
    {
        STD_DBG(_T("FormatedText Size Error %s"), strText.c_str());
        return false;
    }

    if (pWidth)
    {
        *pWidth = _tstoi(matchResult.str(2).c_str());
    }

    if (pPos)
    {
        StdString StrPos = matchResult.str(1);
        if      (StrPos == _T("L")) {*pPos = 0x0000;} // LVCFMT_LEFT
        else if (StrPos == _T("R")) {*pPos = 0x0001;} // LVCFMT_RIGHT
        else if (StrPos == _T("C")) {*pPos = 0x0002;} // LVCFMT_CENTER
    }

    if (pHeaderText)
    {
        *pHeaderText = matchResult.str(3);
    }
    return true;
}



//!< ファイル名使用できない文字チェック
bool CUtil::CheckFilename(StdString strFilename, bool bExecption)
{
    size_t iPos;
    iPos = strFilename.find_first_of(_T("<>:\"/\\|?*"));
    if (iPos != StdString::npos)
    {
        if (bExecption) {throw  MockException(e_no_use_file);}
        return false;
    }

    iPos = strFilename.find_first_of(_T("."));
    if (iPos == 0)
    {
        if (bExecption) {throw  MockException(e_no_use_first_dot);}
        return false;
    }

    iPos = strFilename.find_last_of(_T("."));
    if (iPos == strFilename.length() - 1)
    {
        if (bExecption) {throw  MockException(e_no_use_first_dot);}
        return false;
    }

    return true;
}

//!< ディレクトリ選択ダイアログコールバック
int CALLBACK CUtil::FuncCallBackProc( HWND hWnd, UINT uMsg, LPARAM lParam, LPARAM lpData )
{
    if ( uMsg == BFFM_INITIALIZED )
    {
        if ( HIWORD(lpData) )
        {
            SendMessage( hWnd, BFFM_SETSELECTION, (WPARAM)TRUE, (LPARAM)lpData );
        }
    }
    UNREFERENCED_PARAMETER( lParam );
    return( 0 );
            }
                                
//!< ディレクトリ選択ダイアログ
BOOL CUtil::FuncSelectFolder( HWND hWnd, LPTSTR lpGetPath, LPCTSTR lpDefPath, LPCTSTR lpTitle )
{
	LPITEMIDLIST lpIDlist;
    LPMALLOC lpMalloc;
    BROWSEINFO bi;

    if ( SUCCEEDED(SHGetMalloc(&lpMalloc)) )
    {
        if ( HIWORD(lpDefPath) )
        {
            bi.pidlRoot = NULL;
        }
    else
    {
        bi.pidlRoot = (LPCITEMIDLIST)lpDefPath;
    }
    bi.hwndOwner = hWnd;
    bi.pszDisplayName = NULL;
    bi.lpszTitle = lpTitle;
    bi.ulFlags = BIF_RETURNONLYFSDIRS;
    bi.lpfn = CUtil::FuncCallBackProc;
    bi.lParam = (LPARAM)lpDefPath;
    bi.iImage = 0;

	lpIDlist = SHBrowseForFolder(&bi);

    if ( lpIDlist != NULL )
    {
        SHGetPathFromIDList( lpIDlist, lpGetPath );
        lpMalloc->Free( lpIDlist );
        lpMalloc->Release();
        return( TRUE );
    }
        lpMalloc->Release();
    }
    lstrcpy( lpGetPath, _T("") );
    return( FALSE );
}


//後端数値分離
//strName の 後端に数値がある場合分離する
//ex) TEST_001 ->  Base = "TEST_" Val = 1 Digit = 3
bool CUtil::DevideLastNum( StdString* pBase,
                           int*       pDigit,
                           int*       pVal,
                           const StdString& strName)
{
    int iLen = SizeToInt(strName.length());
    size_t start = strName.find_last_not_of(_T("0123456789"));


    if (iLen == 0)
    {
        return false;
    }

    if (start == strName.npos)
    {
        return false;
    }

    StdString sVal = strName.substr(start + 1, iLen);
    int iVal = _tstoi(sVal.c_str());


    if (pBase)
    {
        *pBase = strName.substr(0, start + 1);;
    }

    if (pVal)
    {
        *pVal = iVal;
    }

    if (pDigit)
    {
        *pDigit = SizeToInt(sVal.length());
    }
    return true;
}

/**
 * @brief   数値つき文字列設定
 * @param   [in] * pList 名称リスト
 * @param   [in] strBase 名称
 * @param   [in] iDigit  桁数
 * @retval  true 使用可能
 * @note	名称リストに  strBase_XXXX  があった場合 XXXXの最大値+1を
            strBaseにつけて返す
            桁数に負の値を設定した場合 名称リストに名称が見つからなかった
            場合 strBaseを返す。それ以外は
            strBase0000を返す
 */
StdString CUtil::FindNumberingString( const std::vector<StdString>* pList, 
                                       StdString strBase, 
                                       int iDigit)
{
    int iMax = -1;
    int iDigitMax = INT_MAX;
    int iVal = 0;
    StdString strVal;
    StdString strSepalator;
    StdStringStream strmResult;
    StdStrXMatchResult matchResult;

    StdString strRegex = strBase + _T("(_*)([0-9]*)");
    StdStrXregex reStr = StdStrXregex::compile(strRegex); 

    bool bMatch = false;
    foreach(StdString strLine, *const_cast< std::vector<StdString>* >(pList) )
    {
        if(boost::xpressive::regex_match(strLine, matchResult, reStr))
        {
            if (strLine == strBase)
            {
                bMatch = true;
                continue;
            }

            else if (matchResult.size() != 3){continue;}
            strVal = matchResult.str(2);
            iVal = _tstoi(strVal.c_str());
            
            if (!matchResult.str(1).empty())
            {
                strSepalator = matchResult.str(1); 
            }
            
            if (iVal > iMax)
            {
                iMax = iVal;
            }
        }
    }

    if ( (!bMatch) &&
         (iDigit < 0))
    {
        return strBase;
    }

    if (iDigit > 0)
    {
        iDigitMax = iDigit * 10  - 1;
    }
    
    if (iMax >= iDigitMax)
    {
        iMax = 0;
    }
    else
    {
        iMax++;
    }

    strmResult <<  strBase << strSepalator;
    if (iDigit > 0)
    {
        strmResult.width(iDigit);
        strmResult.fill(_T('0'));
    }
    strmResult << iMax;
    
    return strmResult.str();
}

StdString CUtil::FindNumberingString( const std::set<StdString>* pList, 
                                       StdString strBase, 
                                       int iDigit)
{
    std::vector<StdString> lstStr;
    foreach(StdString str,*pList)
    {
        lstStr.push_back(str);
    }
    return FindNumberingString(&lstStr, strBase, iDigit);
}

/**
 * @brief   ファイル検索
 * @param   [out] pFileList ファイル名リスト
 * @param   [in]  strFileName 検索ファイル名(ワイルドカード使用可)
 * @param   [in]  path  検索パス
 * @param   [in]  bExt  ファイル名リストに拡張子を含めるか
 * @retval  true ファイルあり
 * @note	
 */
bool CUtil::FindFiles( std::vector<StdString>* pFileList,
                       StdString strFileName,
                       StdPath path,
                       bool bExt) 
{
    WIN32_FIND_DATA fd;
    HANDLE hFind;
    
    StdPath pathSearch = path;
    StdPath pathFile;

    StdString strExt = strFileName;

    pathSearch /= strExt;

    StdString stFile = CUtil::CharToString(pathSearch.string().c_str());
    hFind = FindFirstFile(stFile.c_str(), &fd);
    if(hFind==INVALID_HANDLE_VALUE)
    {
        return false;
    }

    StdString strFile;
    do
    {
        //pathFile = path;

        if (bExt)
        {
            strFile = fd.cFileName;
        }
        else
        {
            pathFile = fd.cFileName;
            strFile = pathFile.stem().c_str();
        }
        pFileList->push_back(strFile);

    }
    while(::FindNextFile(hFind,&fd) != 0);

    FindClose(hFind);
    return true;
}

/**
 * @brief   識別子チェック(使用可否)
 * @param   [in] 部品名
 * @retval  true 使用可能
 * @note	
 */
bool CUtil::IsIdentifer(StdString strName)
{
    using namespace MT_EDITOR;
    return MtParserAS::IsIdentfire(strName);
}


/**
 * @brief   コンマまでを分割
 * @param   [in,out] strName  分割文字
 * @param   [out]    pRes     抽出した文字
 * @retval  true  分割あり
 * @note	
 */
bool CUtil::SeparateComma(StdString& strName, StdString* pRes)
{
    size_t iPos;

    iPos = strName.find_first_of(_T("."));
    if (iPos == StdString::npos)
    {
        if (pRes)
        {
            *pRes    = _T("");
        }
        return false;
    }

    if (pRes)
    {
        *pRes    = strName.substr(0, iPos);
    }
    strName  = strName.substr(iPos + 1);
    return true;
}

/**
 * @brief   最後のコンマを削除
 * @param   [in,out] strName 削除対象文字列
 * @retval  true  削除あり
 * @note	
 */
bool CUtil::DeleteLastComma(StdString& strName)
{
    if (strName.empty())
    {
        return false;
    }

    if (strName[strName.size()-1] == _T(','))
    {
        strName = strName.substr(0, strName.size()- 1);
        return true;
    }
    return false;
}
/**
 * @brief   CSV分割
 * @param   [out]    pParsedData   抽出した文字列
 * @param   [in]     strIn         分割文字
 * @param   [in]     charSep       セパレータ
 * @retval  
 * @note	
 */
void CUtil::TokenizeCsv(std::vector<StdString>* pParsedData, StdString strIn, StdChar charSep)
{
    if (pParsedData == NULL)
    {
        return;
    }

    typedef boost::escaped_list_separator<StdChar> esc_sep;
    typedef boost::tokenizer<esc_sep, StdString::const_iterator, StdString > tokenizer;

    //esc_sep sep(charSep);
    esc_sep sep(_T('\\'), charSep, _T('\"'));
    strIn += _T("");
    tokenizer tokens(strIn, sep);

    StdString strRet;
    tokenizer::iterator tok_iter;
    for (tok_iter =  tokens.begin();
         tok_iter != tokens.end();
         ++tok_iter) 
    {
        //strRet = StripQuoat(*tok_iter);
        pParsedData->push_back(*tok_iter);
    }
}
/**
 *  @brief   引用符追加
 *  @param   [in] stdStr 追加元
 *  @retval  引用符追加後の文字列
 *  @note   
 */
StdString CUtil::QuotString(const StdString& stdStr)
{
    StdString strRet;
    size_t iLen = stdStr.length();
    
    if (iLen == 0)
    {
        strRet = _T("\"\"");
    }
    else
    {
        if ((stdStr[0] != _T('\"')) &&
            (stdStr[iLen - 1] != _T('\"')))
        {
            strRet = _T("\"");
            strRet += stdStr;
            strRet += _T("\"");
        }
        else
        {
            strRet = stdStr;
        }
    }

    return strRet;
}

/**
 *  @brief   引用符削除
 *  @param   [in] stdStr 削除元
 *  @retval  引用符削除後の文字列
 *  @note   
 */
StdString CUtil::StripQuoat(const StdString& stdStr)
{
    StdString strRet;
    size_t iLen = stdStr.length();
    
    if (iLen != 0)
    {
        if ( ((stdStr[0] != _T('\"')) &&
             (stdStr[iLen - 1] != _T('\"'))) ||
             ((stdStr[0] != _T('\'')) &&
             (stdStr[iLen - 1] != _T('\''))))
        {
            strRet = stdStr.substr(1, iLen - 2);
            return strRet;
        }
    }
    strRet = stdStr;

    return strRet;
}

//括弧の除去 StringToAnyで使用
void CUtil::ReplaceParenthesis(StdString* pStr)
{
    size_t iSize = pStr->size();
    for(int iCnt = 0; iCnt < iSize; iCnt++)
    {

        if( (pStr->at(iCnt) == _T('(')) ||
            (pStr->at(iCnt) == _T('{')) ||
            (pStr->at(iCnt) == _T('}')))
        {
            pStr->at(iCnt) = _T(' ');
        }
        else if (pStr->at(iCnt) == _T('('))
        {
            pStr->at(iCnt) = _T(',');
        }
    }
}

/**
 *  @brief   文字列からANY型への変換
 *  @param   [in] anyData 値
 *  @retval  変換後文字列
 *  @note   
*/
boost::any CUtil::StringToAny(const StdString& strAny)
{
    boost::any anyRet;
    std::vector<StdString> strData;
    StdChar*   pStop;
    TokenizeCsv(&strData, strAny);

    size_t nSize;
    nSize = strData.size();
    if (nSize < 2)
    {
        return anyRet;
    }

    if (strData[0] == strInt)
    {
        anyRet = _tstoi(strData[1].c_str());
    }
    else if (strData[0] == strUint)
    {
        anyRet = static_cast<unsigned int>(_tcstoul(strData[1].c_str(), &pStop, 0));
    }
    else if (strData[0] == strLong)
    {
        anyRet = _tstol(strData[1].c_str());
    }   
    else if (strData[0] == strUlong)
    {
        anyRet =_tcstoul(strData[1].c_str(), &pStop, 0);
    }  
    else if (strData[0] == strFloat)
    {
        anyRet = static_cast<float>(_tcstod(strData[1].c_str(), &pStop));

    }  
    else if (strData[0] == strDouble)
    {
        anyRet = _tcstod(strData[1].c_str(), &pStop);
    }
    else if (strData[0] == strBool)
    {
        if (strData[1] == _T("1"))
        {
            anyRet = true;
        }
        else
        {
            anyRet = false;
        }
    }
    else if (strData[0] == strLstPt2d)
    {
        std::vector<POINT2D> ret;
        StdStringStream strmIn;
        strmIn << strData[1];
        boost::archive::text_wiarchive txtIn(strmIn);
        txtIn >> ret;
        anyRet = ret;

    }
    else if (strData[0] == strColor)
    {
        anyRet =_tcstoul(strData[1].c_str(), &pStop, 0);
    }
    else if (strData[0] == strString)
    {
        anyRet =strData[1];
    }
    else if (strData[0] == strPoint2D)
    {
        POINT2D ret;
        ret.FromString(strData[1]);
        anyRet = ret;
    }
    else if (strData[0] == strRect2D)
    {
        RECT2D ret;
        ret.FromString(strData[1]);
        anyRet = ret;
    }
    else if (strData[0] == strLine2D)
    {
        LINE2D ret;
        ret.FromString(strData[1]);
        anyRet = ret;
    }
    else if (strData[0] == strCircle2D)
    {
        CIRCLE2D ret;
        ret.FromString(strData[1]);
        anyRet = ret;
    }
    else if (strData[0] == strHyperbola2D)
    {
        HYPERBOLA2D ret;
        ret.FromString(strData[1]);
        anyRet = ret;
    }
    else if (strData[0] == strParabola2D)
    {
        PARABOLA2D ret;
        ret.FromString(strData[1]);
        anyRet = ret;
    }
    else if (strData[0] == strSpline2D)
    {
        SPLINE2D ret;
        ret.FromString(strData[1]);
        anyRet = ret;
    }
    else if (strData[0] == strMat2D)
    {
        MAT2D ret;
        ret.FromString(strData[1]);
        anyRet = ret;
    }
    else if (strData[0] == strVoid)
    {

    }
    return anyRet;
}

/**
 *  @brief   ANY型から文字列への変換
 *  @param   [in] anyData 値
 *  @param   [in] bType   型の有無（通常 true)
 *  @retval  変換後文字列
 *  @note   
 */
StdString CUtil::AnyToString(const boost::any& anyData, bool bType)
{
    StdStringStream strmRet;
    StdString strTypeName = CUtil::CharToString(anyData.type().name());


    if (bType)
    {
        strmRet << _T("\"") << strTypeName << _T("\",") ;
    }

    if (anyData.type() == typeid(int))
    {
         strmRet << boost::any_cast<int>(anyData);
    }
    else if (anyData.type() == typeid(unsigned int))
    {
        strmRet << boost::any_cast<unsigned int>(anyData);
    }
    else if (anyData.type() == typeid(long))
    {
        strmRet << boost::any_cast<long>(anyData);
    }
    else if (anyData.type() == typeid(unsigned long))
    {
        strmRet << boost::any_cast<unsigned long>(anyData);
    }
    else if (anyData.type() == typeid(float))
    {
        strmRet << boost::any_cast<float>(anyData);
    }
    else if (anyData.type() == typeid(double))
    {
        strmRet << boost::any_cast<double>(anyData);
    }
    else if (anyData.type() == typeid(bool))
    {
        strmRet << boost::any_cast<bool>(anyData);
    }
    else if (anyData.type() == typeid(std::vector<POINT2D>))
    {
        std::vector<POINT2D> lstPt;
        lstPt = boost::any_cast< std::vector<POINT2D> >(anyData);
        boost::archive::text_woarchive txtOut(strmRet);
        txtOut << lstPt;
    }
    else if (anyData.type() == typeid(COLORREF))
    {
        strmRet << boost::any_cast<COLORREF>(anyData);
    }
    else if (anyData.type() == typeid(StdString))
    {
        StdString strOut;
        strOut = boost::any_cast<StdString>(anyData);
        strmRet << _T("\"") << strOut << _T("\"");
    }
    else if (anyData.type() == typeid(LPCTSTR))
    {
        strmRet << _T("\"") << boost::any_cast<LPCTSTR>(anyData) << _T("\"");
    }
    else if (anyData.type() == typeid(POINT2D))
    {
        strmRet << (boost::any_cast<POINT2D>(anyData)).ToString();
    }
    else if (anyData.type() == typeid(RECT2D))
    {
        strmRet << (boost::any_cast<RECT2D>(anyData)).ToString();
    }
    else if (anyData.type() == typeid(LINE2D))
    {
        strmRet << (boost::any_cast<LINE2D>(anyData)).ToString();
    }
    else if (anyData.type() == typeid(CIRCLE2D))
    {
        strmRet << (boost::any_cast<CIRCLE2D>(anyData)).ToString();
    }
    else if (anyData.type() == typeid(ELLIPSE2D))
    {
        strmRet << (boost::any_cast<ELLIPSE2D>(anyData)).ToString();
    }
    else if (anyData.type() == typeid(HYPERBOLA2D))
    {
        strmRet << (boost::any_cast<HYPERBOLA2D>(anyData)).ToString();
    }
    else if (anyData.type() == typeid(PARABOLA2D))
    {
        strmRet << (boost::any_cast<PARABOLA2D>(anyData)).ToString();
    }
    else if (anyData.type() == typeid(SPLINE2D))
    {
        strmRet << (boost::any_cast<SPLINE2D>(anyData)).ToString();
    }
    else if (anyData.type() == typeid(MAT2D))
    {
        strmRet << (boost::any_cast<MAT2D>(anyData)).ToString();
    }
    else if (anyData.type() == typeid(void))
    {
         strmRet << _T("");
    }
    //strmRet << std::endl;
    return strmRet.str();
}

//ANY型の比較
bool CUtil::IsAnyEqual(const boost::any& any1, const boost::any& any2)
{
    if (any1.type() != any2.type())
    {
        return false;
    }
    if (any1.type() == typeid(int))
    {
         if (boost::any_cast<int>(any1) == 
             boost::any_cast<int>(any2))
         {
            return true;   
         }
    }
    else if (any1.type() == typeid(unsigned int))
    {
         if (boost::any_cast<unsigned int>(any1) == 
             boost::any_cast<unsigned int>(any2))
         {
            return true;   
         }
    }
    else if (any1.type() == typeid(long))
    {
         if (boost::any_cast<long>(any1) == 
             boost::any_cast<long>(any2))
         {
            return true;   
         }
    }
    else if (any1.type() == typeid(unsigned long))
    {
         if (boost::any_cast<unsigned long>(any1) == 
             boost::any_cast<unsigned long>(any2))
         {
            return true;   
         }
    }
    else if (any1.type() == typeid(float))
    {
        float fBal =
        boost::any_cast<float>(any1) - 
        boost::any_cast<float>(any2);
        if (fabs(fBal) < NEAR_ZERO)
        {
        return true;   
        }
    }
    else if (any1.type() == typeid(double))
    {
        double dBal =
        boost::any_cast<double>(any1) - 
        boost::any_cast<double>(any2);

        if (fabs(dBal) < NEAR_ZERO)
        {
        return true;   
        }
    }
    else if (any1.type() == typeid(bool))
    {
         if (boost::any_cast<bool>(any1) == 
             boost::any_cast<bool>(any2))
         {
            return true;   
         }
    }
    else if (any1.type() == typeid(std::vector<POINT2D>))
    {
         if (boost::any_cast<std::vector<POINT2D>>(any1) == 
             boost::any_cast<std::vector<POINT2D>>(any2))
         {
            return true;   
         }
    }
    else if (any1.type() == typeid(COLORREF))
    {
         if (boost::any_cast<COLORREF>(any1) == 
             boost::any_cast<COLORREF>(any2))
         {
            return true;   
         }
    }
    else if (any1.type() == typeid(StdString))
    {
         if (boost::any_cast<StdString>(any1) == 
             boost::any_cast<StdString>(any2))
         {
            return true;   
         }
    }
    else if (any1.type() == typeid(POINT2D))
    {
         if (boost::any_cast<POINT2D>(any1) == 
             boost::any_cast<POINT2D>(any2))
         {
            return true;   
         }
    }
    else if (any1.type() == typeid(RECT2D))
    {
         if (boost::any_cast<RECT2D>(any1) == 
             boost::any_cast<RECT2D>(any2))
         {
            return true;   
         }
    }
    else
    {
        STD_DBG(_T("Unknown type %s"), CUtil::CharToString(any1.type().name()).c_str());
    }

    return false;
}
//Pathの比較
// StdPathでは大文字、小文字は別に扱われる
bool CUtil::IsPathEqual(const StdPath& path1, const StdPath& path2)
{
    return (ToUpper(path1.c_str()) == ToUpper(path2.c_str()));
}


/**
 * @brief   GUIDから文字列への変換
 * @param   [in]    guid  GUID
 * @retval  変換文字列
 * @note	
 */
StdString CUtil::GuidToString(const GUID& guid)
{
    StdStringStream strmRet;

    strmRet << std::hex;
    strmRet << std::setw(8) << std::setfill(_T('0')) << guid.Data1;
    strmRet << _T("-");
    strmRet << std::setw(4) << std::setfill(_T('0')) << guid.Data2;
    strmRet << _T("-");
    strmRet << std::setw(4) << std::setfill(_T('0')) << guid.Data3;
    strmRet << _T("-");
    strmRet << std::setw(2) << std::setfill(_T('0')) << guid.Data4[0];
    strmRet << std::setw(2) << std::setfill(_T('0')) << guid.Data4[1];
    strmRet << std::setw(2) << std::setfill(_T('0')) << guid.Data4[2];
    strmRet << std::setw(2) << std::setfill(_T('0')) << guid.Data4[3];
    strmRet << std::setw(2) << std::setfill(_T('0')) << guid.Data4[4];
    strmRet << std::setw(2) << std::setfill(_T('0')) << guid.Data4[5];
    strmRet << std::setw(2) << std::setfill(_T('0')) << guid.Data4[6];
    strmRet << std::setw(2) << std::setfill(_T('0')) << guid.Data4[7];

    return strmRet.str();
}


/**
 * @brief   浮動小数点値チェック
 * @param   [in]    path  ファイルパス
 * @param   [out]   pStr  
 * @retval  true    成功
 * @note	
 */
bool CUtil::IsFloatVal(const StdString& strVal)
{
    using namespace boost::xpressive;
    StdStrXMatchResult matchResult;

    StdString strVal2;

    strVal2 = strVal;
    boost::trim(strVal2);

    StdStrXregex satatic_reg = 
        !(set= _T('+'),_T('-')) >>                           // [+-]?
         (s1= range( _T('1'),_T('9')) >> *range( _T('0'),_T('9'))) >>  // ([1-9][0-9]*)
        !(_T('.') >> +_d) >>                                            // (?:\.\d+)?
        !((set= _T('e'),_T('E')) >> !(set= _T('+'),_T('-')) >> +_d);   // (?:[Ee][+-]?\d+)?

    bool bRet = regex_match( strVal2 , matchResult, satatic_reg);

    return bRet;
}

/**
 * @brief   ファイル読み込み
 * @param   [in]    path  ファイルパス
 * @param   [out]   pStr  
 * @retval  true    成功
 * @note	
 */
bool CUtil::LoadText(StdPath path, StdString* pStr)
{
    StdStreamIn inFs;
    StdStringStream strStream;
    StdString  strLine;
    bool bRet = true;
    
    inFs.open( path ); 
    if( inFs.fail() ) 
    { 
        return false; 
    } 
    
    try
    {
        while( !inFs.eof() ) 
        {
            std::getline(inFs,strLine);
            (*pStr) += strLine;
            (*pStr) += _T("\n");
        }
    }
    catch(boost::system::system_error &e)
    {
        bRet = false;
        STD_DBG(_T("Filesystem error %s"), e.what());
    }
    catch(...)
    {
        bRet = false;
        STD_DBG(_T("Unknoen Error"));
    }
    return bRet;
}


/**
 * @brief   ファイル読み込み
 * @param   [in]    path  ファイルパス
 * @param   [out]   pStr  
 * @retval  true    成功
 * @note	
 */
bool CUtil::LoadText(StdPath path, std::string* pStr)
{
    if (pStr == NULL)
    {
        return false;
    }

    namespace fs = boost::filesystem;
    std::ios::pos_type         ullFilePos;
 
    fs::ifstream inFs;
    std::string  strLine;
    bool bRet = true;
    
    inFs.open( path , std::ios::binary); 
    if( inFs.fail() ) 
    { 
        return false; 
    } 
    
    inFs.seekg(0, std::ios::end);
    ullFilePos = inFs.tellg();
    pStr->resize(static_cast<size_t>(ullFilePos));
	inFs.seekg(0, std::ios::beg);
    try
    {
	    inFs.read((char*)&pStr->at(0), ullFilePos);
    }
    catch(fs::filesystem_error &e)
    {
      bRet = false;
      STD_DBG(_T("Filesystem error %s"), e.what());
    }
    catch(...)
    {
      bRet = false;
      STD_DBG(_T("Unknoen Error"));
    }
    return bRet;
}

/**
 * @brief   ウインドウメッセージ名取得
 * @param   [in]    UINT msg
 * @retval  変換文字列
 * @note	
 */
CString CUtil::ConvWinMessage(UINT msg)
{   
    CString strRet = _T("UNKONWN_MSG");
#ifdef _DEBUG
    static std::map<UINT, CString> mapMsg;
    if(mapMsg.size() == 0)
    {
        mapMsg[0x02A1 ] = _T("WM_MOUSEHOVER");
        mapMsg[0x02A3 ] = _T("WM_MOUSELEAVE");
        mapMsg[0x0000 ] = _T("WM_NULL");
        mapMsg[0x0001 ] = _T("WM_CREATE");
        mapMsg[0x0002 ] = _T("WM_DESTROY");
        mapMsg[0x0003 ] = _T("WM_MOVE");
        mapMsg[0x0005 ] = _T("WM_SIZE");
        mapMsg[0x0006 ] = _T("WM_ACTIVATE");
        mapMsg[0x0007 ] = _T("WM_SETFOCUS");
        mapMsg[0x0008 ] = _T("WM_KILLFOCUS");
        mapMsg[0x000A ] = _T("WM_ENABLE");
        mapMsg[0x000B ] = _T("WM_SETREDRAW");
        mapMsg[0x000C ] = _T("WM_SETTEXT");
        mapMsg[0x000D ] = _T("WM_GETTEXT");
        mapMsg[0x000E ] = _T("WM_GETTEXTLENGTH");
        mapMsg[0x000F ] = _T("WM_PAINT");
        mapMsg[0x0010 ] = _T("WM_CLOSE");
        mapMsg[0x0011 ] = _T("WM_QUERYENDSESSION");
        mapMsg[0x0013 ] = _T("WM_QUERYOPEN");
        mapMsg[0x0016 ] = _T("WM_ENDSESSION");
        mapMsg[0x0012 ] = _T("WM_QUIT");
        mapMsg[0x0014 ] = _T("WM_ERASEBKGND");
        mapMsg[0x0015 ] = _T("WM_SYSCOLORCHANGE");
        mapMsg[0x0018 ] = _T("WM_SHOWWINDOW");
        mapMsg[0x001A ] = _T("WM_WININICHANGE");
        mapMsg[0x001A ] = _T("WM_SETTINGCHANGE");
        mapMsg[0x001B ] = _T("WM_DEVMODECHANGE");
        mapMsg[0x001C ] = _T("WM_ACTIVATEAPP");
        mapMsg[0x001D ] = _T("WM_FONTCHANGE");
        mapMsg[0x001E ] = _T("WM_TIMECHANGE");
        mapMsg[0x001F ] = _T("WM_CANCELMODE");
        mapMsg[0x0020 ] = _T("WM_SETCURSOR");
        mapMsg[0x0021 ] = _T("WM_MOUSEACTIVATE");
        mapMsg[0x0022 ] = _T("WM_CHILDACTIVATE");
        mapMsg[0x0023 ] = _T("WM_QUEUESYNC");
        mapMsg[0x0024 ] = _T("WM_GETMINMAXINFO");
        mapMsg[0x0026 ] = _T("WM_PAINTICON");
        mapMsg[0x0027 ] = _T("WM_ICONERASEBKGND");
        mapMsg[0x0028 ] = _T("WM_NEXTDLGCTL");
        mapMsg[0x002A ] = _T("WM_SPOOLERSTATUS");
        mapMsg[0x002B ] = _T("WM_DRAWITEM");
        mapMsg[0x002C ] = _T("WM_MEASUREITEM");
        mapMsg[0x002D ] = _T("WM_DELETEITEM");
        mapMsg[0x002E ] = _T("WM_VKEYTOITEM");
        mapMsg[0x002F ] = _T("WM_CHARTOITEM");
        mapMsg[0x0030 ] = _T("WM_SETFONT");
        mapMsg[0x0031 ] = _T("WM_GETFONT");
        mapMsg[0x0032 ] = _T("WM_SETHOTKEY");
        mapMsg[0x0033 ] = _T("WM_GETHOTKEY");
        mapMsg[0x0037 ] = _T("WM_QUERYDRAGICON");
        mapMsg[0x0039 ] = _T("WM_COMPAREITEM");
        mapMsg[0x003D ] = _T("WM_GETOBJECT");
        mapMsg[0x0041 ] = _T("WM_COMPACTING");
        mapMsg[0x0044 ] = _T("WM_COMMNOTIFY");
        mapMsg[0x0046 ] = _T("WM_WINDOWPOSCHANGING");
        mapMsg[0x0047 ] = _T("WM_WINDOWPOSCHANGED");
        mapMsg[0x0048 ] = _T("WM_POWER");
        mapMsg[0x004A ] = _T("WM_COPYDATA");
        mapMsg[0x004B ] = _T("WM_CANCELJOURNAL");
        mapMsg[0x004E ] = _T("WM_NOTIFY");
        mapMsg[0x0050 ] = _T("WM_INPUTLANGCHANGEREQUEST");
        mapMsg[0x0051 ] = _T("WM_INPUTLANGCHANGE");
        mapMsg[0x0052 ] = _T("WM_TCARD");
        mapMsg[0x0053 ] = _T("WM_HELP");
        mapMsg[0x0054 ] = _T("WM_USERCHANGED");
        mapMsg[0x0055 ] = _T("WM_NOTIFYFORMAT");
        mapMsg[0x007B ] = _T("WM_CONTEXTMENU");
        mapMsg[0x007C ] = _T("WM_STYLECHANGING");
        mapMsg[0x007D ] = _T("WM_STYLECHANGED");
        mapMsg[0x007E ] = _T("WM_DISPLAYCHANGE");
        mapMsg[0x007F ] = _T("WM_GETICON");
        mapMsg[0x0080 ] = _T("WM_SETICON");
        mapMsg[0x0081 ] = _T("WM_NCCREATE");
        mapMsg[0x0082 ] = _T("WM_NCDESTROY");
        mapMsg[0x0083 ] = _T("WM_NCCALCSIZE");
        mapMsg[0x0084 ] = _T("WM_NCHITTEST");
        mapMsg[0x0085 ] = _T("WM_NCPAINT");
        mapMsg[0x0086 ] = _T("WM_NCACTIVATE");
        mapMsg[0x0087 ] = _T("WM_GETDLGCODE");
        mapMsg[0x0088 ] = _T("WM_SYNCPAINT");
        mapMsg[0x00A0 ] = _T("WM_NCMOUSEMOVE");
        mapMsg[0x00A1 ] = _T("WM_NCLBUTTONDOWN");
        mapMsg[0x00A2 ] = _T("WM_NCLBUTTONUP");
        mapMsg[0x00A3 ] = _T("WM_NCLBUTTONDBLCLK");
        mapMsg[0x00A4 ] = _T("WM_NCRBUTTONDOWN");
        mapMsg[0x00A5 ] = _T("WM_NCRBUTTONUP");
        mapMsg[0x00A6 ] = _T("WM_NCRBUTTONDBLCLK");
        mapMsg[0x00A7 ] = _T("WM_NCMBUTTONDOWN");
        mapMsg[0x00A8 ] = _T("WM_NCMBUTTONUP");
        mapMsg[0x00A9 ] = _T("WM_NCMBUTTONDBLCLK");
        mapMsg[0x00AB ] = _T("WM_NCXBUTTONDOWN");
        mapMsg[0x00AC ] = _T("WM_NCXBUTTONUP");
        mapMsg[0x00AD ] = _T("WM_NCXBUTTONDBLCLK");
        mapMsg[0x00FE ] = _T("WM_INPUT_DEVICE_CHANGE");
        mapMsg[0x00FF ] = _T("WM_INPUT");
        mapMsg[0x0100 ] = _T("WM_KEYFIRST");
        mapMsg[0x0100 ] = _T("WM_KEYDOWN");
        mapMsg[0x0101 ] = _T("WM_KEYUP");
        mapMsg[0x0102 ] = _T("WM_CHAR");
        mapMsg[0x0103 ] = _T("WM_DEADCHAR");
        mapMsg[0x0104 ] = _T("WM_SYSKEYDOWN");
        mapMsg[0x0105 ] = _T("WM_SYSKEYUP");
        mapMsg[0x0106 ] = _T("WM_SYSCHAR");
        mapMsg[0x0107 ] = _T("WM_SYSDEADCHAR");
        mapMsg[0x0109 ] = _T("WM_UNICHAR");
        mapMsg[0x0109 ] = _T("WM_KEYLAST");
        mapMsg[0x0108 ] = _T("WM_KEYLAST");
        mapMsg[0x010D ] = _T("WM_IME_STARTCOMPOSITION");
        mapMsg[0x010E ] = _T("WM_IME_ENDCOMPOSITION");
        mapMsg[0x010F ] = _T("WM_IME_COMPOSITION");
        mapMsg[0x010F ] = _T("WM_IME_KEYLAST");
        mapMsg[0x0110 ] = _T("WM_INITDIALOG");
        mapMsg[0x0111 ] = _T("WM_COMMAND");
        mapMsg[0x0112 ] = _T("WM_SYSCOMMAND");
        mapMsg[0x0113 ] = _T("WM_TIMER");
        mapMsg[0x0114 ] = _T("WM_HSCROLL");
        mapMsg[0x0115 ] = _T("WM_VSCROLL");
        mapMsg[0x0116 ] = _T("WM_INITMENU");
        mapMsg[0x0117 ] = _T("WM_INITMENUPOPUP");
        mapMsg[0x011F ] = _T("WM_MENUSELECT");
        mapMsg[0x0120 ] = _T("WM_MENUCHAR");
        mapMsg[0x0121 ] = _T("WM_ENTERIDLE");
        mapMsg[0x0122 ] = _T("WM_MENURBUTTONUP");
        mapMsg[0x0123 ] = _T("WM_MENUDRAG");
        mapMsg[0x0124 ] = _T("WM_MENUGETOBJECT");
        mapMsg[0x0125 ] = _T("WM_UNINITMENUPOPUP");
        mapMsg[0x0126 ] = _T("WM_MENUCOMMAND");
        mapMsg[0x0127 ] = _T("WM_CHANGEUISTATE");
        mapMsg[0x0128 ] = _T("WM_UPDATEUISTATE");
        mapMsg[0x0129 ] = _T("WM_QUERYUISTATE");
        mapMsg[0x0132 ] = _T("WM_CTLCOLORMSGBOX");
        mapMsg[0x0133 ] = _T("WM_CTLCOLOREDIT");
        mapMsg[0x0134 ] = _T("WM_CTLCOLORLISTBOX");
        mapMsg[0x0135 ] = _T("WM_CTLCOLORBTN");
        mapMsg[0x0136 ] = _T("WM_CTLCOLORDLG");
        mapMsg[0x0137 ] = _T("WM_CTLCOLORSCROLLBAR");
        mapMsg[0x0138 ] = _T("WM_CTLCOLORSTATIC");
        mapMsg[0x0200 ] = _T("WM_MOUSEFIRST");
        mapMsg[0x0200 ] = _T("WM_MOUSEMOVE");
        mapMsg[0x0201 ] = _T("WM_LBUTTONDOWN");
        mapMsg[0x0202 ] = _T("WM_LBUTTONUP");
        mapMsg[0x0203 ] = _T("WM_LBUTTONDBLCLK");
        mapMsg[0x0204 ] = _T("WM_RBUTTONDOWN");
        mapMsg[0x0205 ] = _T("WM_RBUTTONUP");
        mapMsg[0x0206 ] = _T("WM_RBUTTONDBLCLK");
        mapMsg[0x0207 ] = _T("WM_MBUTTONDOWN");
        mapMsg[0x0208 ] = _T("WM_MBUTTONUP");
        mapMsg[0x0209 ] = _T("WM_MBUTTONDBLCLK");
        mapMsg[0x020A ] = _T("WM_MOUSEWHEEL");
        mapMsg[0x020B ] = _T("WM_XBUTTONDOWN");
        mapMsg[0x020C ] = _T("WM_XBUTTONUP");
        mapMsg[0x020D ] = _T("WM_XBUTTONDBLCLK");
        mapMsg[0x020E ] = _T("WM_MOUSEHWHEEL");
        mapMsg[0x020E ] = _T("WM_MOUSELAST");
        mapMsg[0x020D ] = _T("WM_MOUSELAST");
        mapMsg[0x020A ] = _T("WM_MOUSELAST");
        mapMsg[0x0209 ] = _T("WM_MOUSELAST");
        mapMsg[0x0210 ] = _T("WM_PARENTNOTIFY");
        mapMsg[0x0211 ] = _T("WM_ENTERMENULOOP");
        mapMsg[0x0212 ] = _T("WM_EXITMENULOOP");
        mapMsg[0x0213 ] = _T("WM_NEXTMENU");
        mapMsg[0x0214 ] = _T("WM_SIZING");
        mapMsg[0x0215 ] = _T("WM_CAPTURECHANGED");
        mapMsg[0x0216 ] = _T("WM_MOVING");
        mapMsg[0x0218 ] = _T("WM_POWERBROADCAST");
        mapMsg[0x0219 ] = _T("WM_DEVICECHANGE");
        mapMsg[0x0220 ] = _T("WM_MDICREATE");
        mapMsg[0x0221 ] = _T("WM_MDIDESTROY");
        mapMsg[0x0222 ] = _T("WM_MDIACTIVATE");
        mapMsg[0x0223 ] = _T("WM_MDIRESTORE");
        mapMsg[0x0224 ] = _T("WM_MDINEXT");
        mapMsg[0x0225 ] = _T("WM_MDIMAXIMIZE");
        mapMsg[0x0226 ] = _T("WM_MDITILE");
        mapMsg[0x0227 ] = _T("WM_MDICASCADE");
        mapMsg[0x0228 ] = _T("WM_MDIICONARRANGE");
        mapMsg[0x0229 ] = _T("WM_MDIGETACTIVE");
        mapMsg[0x0230 ] = _T("WM_MDISETMENU");
        mapMsg[0x0231 ] = _T("WM_ENTERSIZEMOVE");
        mapMsg[0x0232 ] = _T("WM_EXITSIZEMOVE");
        mapMsg[0x0233 ] = _T("WM_DROPFILES");
        mapMsg[0x0234 ] = _T("WM_MDIREFRESHMENU");
        mapMsg[0x0281 ] = _T("WM_IME_SETCONTEXT");
        mapMsg[0x0282 ] = _T("WM_IME_NOTIFY");
        mapMsg[0x0283 ] = _T("WM_IME_CONTROL");
        mapMsg[0x0284 ] = _T("WM_IME_COMPOSITIONFULL");
        mapMsg[0x0285 ] = _T("WM_IME_SELECT");
        mapMsg[0x0286 ] = _T("WM_IME_CHAR");
        mapMsg[0x0288 ] = _T("WM_IME_REQUEST");
        mapMsg[0x0290 ] = _T("WM_IME_KEYDOWN");
        mapMsg[0x0291 ] = _T("WM_IME_KEYUP");
        mapMsg[0x02A1 ] = _T("WM_MOUSEHOVER");
        mapMsg[0x02A3 ] = _T("WM_MOUSELEAVE");
        mapMsg[0x02A0 ] = _T("WM_NCMOUSEHOVER");
        mapMsg[0x02A2 ] = _T("WM_NCMOUSELEAVE");
        mapMsg[0x02B1 ] = _T("WM_WTSSESSION_CHANGE");
        mapMsg[0x02c0 ] = _T("WM_TABLET_FIRST");
        mapMsg[0x02df ] = _T("WM_TABLET_LAST");
        mapMsg[0x0300 ] = _T("WM_CUT");
        mapMsg[0x0301 ] = _T("WM_COPY");
        mapMsg[0x0302 ] = _T("WM_PASTE");
        mapMsg[0x0303 ] = _T("WM_CLEAR");
        mapMsg[0x0304 ] = _T("WM_UNDO");
        mapMsg[0x0305 ] = _T("WM_RENDERFORMAT");
        mapMsg[0x0306 ] = _T("WM_RENDERALLFORMATS");
        mapMsg[0x0307 ] = _T("WM_DESTROYCLIPBOARD");
        mapMsg[0x0308 ] = _T("WM_DRAWCLIPBOARD");
        mapMsg[0x0309 ] = _T("WM_PAINTCLIPBOARD");
        mapMsg[0x030A ] = _T("WM_VSCROLLCLIPBOARD");
        mapMsg[0x030B ] = _T("WM_SIZECLIPBOARD");
        mapMsg[0x030C ] = _T("WM_ASKCBFORMATNAME");
        mapMsg[0x030D ] = _T("WM_CHANGECBCHAIN");
        mapMsg[0x030E ] = _T("WM_HSCROLLCLIPBOARD");
        mapMsg[0x030F ] = _T("WM_QUERYNEWPALETTE");
        mapMsg[0x0310 ] = _T("WM_PALETTEISCHANGING");
        mapMsg[0x0311 ] = _T("WM_PALETTECHANGED");
        mapMsg[0x0312 ] = _T("WM_HOTKEY");
        mapMsg[0x0317 ] = _T("WM_PRINT");
        mapMsg[0x0318 ] = _T("WM_PRINTCLIENT");
        mapMsg[0x0319 ] = _T("WM_APPCOMMAND");
        mapMsg[0x031A ] = _T("WM_THEMECHANGED");
        mapMsg[0x031D ] = _T("WM_CLIPBOARDUPDATE");
        mapMsg[0x031E ] = _T("WM_DWMCOMPOSITIONCHANGED");
        mapMsg[0x031F ] = _T("WM_DWMNCRENDERINGCHANGED");
        mapMsg[0x0320 ] = _T("WM_DWMCOLORIZATIONCOLORCHANGED");
        mapMsg[0x0321 ] = _T("WM_DWMWINDOWMAXIMIZEDCHANGE");
        mapMsg[0x033F ] = _T("WM_GETTITLEBARINFOEX");
        mapMsg[0x0358 ] = _T("WM_HANDHELDFIRST");
        mapMsg[0x035F ] = _T("WM_HANDHELDLAST");
        mapMsg[0x0360 ] = _T("WM_AFXFIRST");
        mapMsg[0x037F ] = _T("WM_AFXLAST");
        mapMsg[0x0380 ] = _T("WM_PENWINFIRST");
        mapMsg[0x038F ] = _T("WM_PENWINLAST");
        mapMsg[0x8000 ] = _T("WM_APP");
        mapMsg[0x0400 ] = _T("WM_USER");
    }

    std::map<UINT, CString>::iterator iteMap;
    iteMap = mapMsg.find(msg);
    if (iteMap != mapMsg.end())
    {
        strRet = iteMap->second;
    }

#endif

    return strRet;
}

/******************************************/
/* シンプレックス法                       */
/*      max : 最大繰り返し回数            */
/*      n : 次元                          */
/*      k : 初期値設定係数                */
/*      r1 : 縮小比率                     */
/*      r2 : 拡大比率                     */
/*      y : 最小値                        */
/*      x : x(初期値と答え)               */
/*      snx : 関数値を計算する関数名      */
/*      return : >=0 : 正常終了(収束回数) */
/*               =-1 : 収束せず           */
/******************************************/
/*
int Simplex(int max, int n, double k, double eps, double r1, double r2, double *y, double *x, double (*snx)(double *))
{
	double **xx, *yy, *xg, *xr, *xn, yr, yn, e;
	int i1, i2, fh = -1, fg = -1, fl = -1, count = 0, sw = -1;
					// 初期値の設定
	yy = new double [n+1];
	xg = new double [n];
	xr = new double [n];
	xn = new double [n];
	xx = new double * [n+1];
	for (i1 = 0; i1 < n+1; i1++)
    {
		xx[i1] = new double [n];
    }

	for (i1 = 0; i1 < n+1; i1++) 
    {
		for (i2 = 0; i2 < n; i2++)
        {
			xx[i1][i2] = x[i2];
        }
		if (i1 > 0)
        {
			xx[i1][i1-1] += k;
        }
		yy[i1] = snx(xx[i1]);
	}
					// 最大値，最小値の計算
	for (i1 = 0; i1 < n+1; i1++) 
    {
		if (fh < 0 || yy[i1] > yy[fh])
        {
			fh = i1;
        }
		if (fl < 0 || yy[i1] < yy[fl])
        {
			fl = i1;
        }
	}
	for (i1 = 0; i1 < n+1; i1++) 
    {
		if (i1 != fh && (fg < 0 || yy[i1] > yy[fg]))
        {
			fg = i1;
        }
	}
					// 最小値の計算
	while (count < max && sw < 0) 
    {
		count++;
							// 重心の計算
		for (i1 = 0; i1 < n; i1++)
        {
			xg[i1] = 0.0;
        }
		for (i1 = 0; i1 < n+1; i1++) 
        {
			if (i1 != fh) 
            {
				for (i2 = 0; i2 < n; i2++)
                {
					xg[i2] += xx[i1][i2];
                }
			}
		}
		for (i1 = 0; i1 < n; i1++)
        {
			xg[i1] /= n;
							// 最大点の置き換え
        }
		for (i1 = 0; i1 < n; i1++)
        {
			xr[i1] = 2.0 * xg[i1] - xx[fh][i1];
        }
		yr = snx(xr);
		
        if (yr >= yy[fh]) 
        {   // 縮小
			for (i1 = 0; i1 < n; i1++)
            {
				xr[i1] = (1.0 - r1) * xx[fh][i1] + r1 * xr[i1];
            }
			yr = snx(xr);
		}
		else if (yr < (yy[fl]+(r2-1.0)*yy[fh])/r2) 
        {   // 拡大
			for (i1 = 0; i1 < n; i1++)
            {
				xn[i1] = r2 * xr[i1] - (r2 - 1.0) * xx[fh][i1];
            }
			yn = snx(xn);
			if (yn <= yr) 
            {
				for (i1 = 0; i1 < n; i1++)
                {
					xr[i1] = xn[i1];
                }
				yr = yn;
			}
		}
		for (i1 = 0; i1 < n; i1++)
        {
			xx[fh][i1] = xr[i1];
        }
		yy[fh] = yr;
							// シンプレックス全体の縮小
		if (yy[fh] >= yy[fg]) 
        {
			for (i1 = 0; i1 < n+1; i1++) 
            {
				if (i1 != fl) {
					for (i2 = 0; i2 < n; i2++)
						xx[i1][i2] = 0.5 * (xx[i1][i2] + xx[fl][i2]);
					yy[i1] = snx(xx[i1]);
				}
			}
		}
							// 最大値，最小値の計算
		fh = -1;
		fg = -1;
		fl = -1;
		for (i1 = 0; i1 < n+1; i1++) 
        {
			if (fh < 0 || yy[i1] > yy[fh])
            {
				fh = i1;
            }

			if (fl < 0 || yy[i1] < yy[fl])
            {
				fl = i1;
            }
		}
		for (i1 = 0; i1 < n+1; i1++) 
        {
			if (i1 != fh && (fg < 0 || yy[i1] > yy[fg]))
				fg = i1;
		}
							// 収束判定
		e = 0.0;
		for (i1 = 0; i1 < n+1; i1++) 
        {
			if (i1 != fl) 
            {
				yr  = yy[i1] - yy[fl];
				e  += yr * yr;
			}
		}

		if (e < eps)
        {
			sw = 0;
        }
	}

	if (sw == 0) 
    {
		sw = count;
		*y = yy[fl];
		for (i1 = 0; i1 < n; i1++)
        {
			x[i1] = xx[fl][i1];
        }
	}

	delete [] yy;
	delete [] xg;
	delete [] xr;
	delete [] xn;
	for (i1 = 0; i1 < n+1; i1++)
    {
		delete [] xx[i1];
    }
	delete [] xx;

	return sw;
}
*/

/** 同符号なら真 */
bool CUtil::IsSameSign(double a, double b) 
{
    if ((a > 0) && (b > 0))
    {
        return true;
    }
    else if ((a < 0) && (b < 0))
    {
        return true;
    }
    else if ((a == 0) && (b == 0))
    {
        return true;
    }

    return false;
}

//正なら１ 負なら-1
double CUtil::sgn2(double dVal)
{
    if (dVal >= 0.0)
    {
        return 1.0;
    }
    return -1.0;
}

//!< 実数比較
bool CUtil::IsEqual(double a, double b)
{
    return (fabs(a-b) < NEAR_ZERO);
}

/**
 * @brief 2分法
 * @param [out] pRet    計算結果
 * @param [in ] func    解を求める関数
 * @param [in ] pVoid   解を求める関数に渡すポインタ
 * @param [in ] dA      開始点
 * @param [in ] dB      終了点
 * @param [in ] iMax    計算回数
 * @param [in ] dTolerance    許容誤差
 * @return true 解あり
 */ 
bool  BisectionPart (double* pRet, std::function<double (double, const void*) > func, const void* pVoid, 
                          double dA, double dB, int iMax, double dTolerance)
{
    if (dTolerance < 0.0) 
    {
        dTolerance = 0.0;
    }
    
    if (dB < dA) 
    {  
        std::swap(dA, dB);
    }

    double dFa = func(dA, pVoid);  
    if (fabs(dFa) < NEAR_ZERO) 
    {
        *pRet = dA;
        return true;
    }
   

    double dFb = func(dB, pVoid);  
    if (fabs(dFb) <  NEAR_ZERO)
    {
        *pRet = dB;
        return true;
    }

    if (CUtil::IsSameSign(dFa, dFb))
    {
        //区間の両端で関数値が同符号です
        return false;
    }

    double dC = 0.0;
    double dFc;
    for (int iCnt = 0 ;iCnt < iMax; iCnt++) 
    {

        dC = (dA + dB) / 2.0;
        dFc = func(dC, pVoid);  

#if 0 //INSERT
            DB_PRINT( _T("   BisectionPart:%d  dA:%f=%f dB:%f=%f dC:%f=%f \n"), iCnt, dA, dFa, dB, dFb, dC, dFc);
#endif
        
        if ((dC - dA <= dTolerance) ||
            (dB - dC <= dTolerance))
        {
            *pRet = dC;
            return true;
        }

        if (fabs(dFc) <NEAR_ZERO)
        {
            *pRet = dC;
            return true;
        }

        if (CUtil::IsSameSign(dFc, dFa)) 
        {
            dA  = dC;  
            dFa = func(dA, pVoid);  

        }
        else
        { 
            dB  = dC;  
            dFb = func(dB, pVoid);  
        }
    }
    *pRet = dC;
    return false;
}

/**
 * @brief 2分法
 * @param [out] plstRet    計算結果リスト
 * @param [in ] func       解を求める関数
 * @param [in ] pVoid      解を求める関数に渡すポインタ
 * @param [in ] dMin       最小値
 * @param [in ] dMax       最大値
 * @param [in ] dMax       最大値
 * @param [in ] iMax       計算回数
 * @param [in ] dTolerance    許容誤差
 * @return true 解あり
 */ 
bool  CUtil::Bisection (std::vector<double>* plstRet, 
                        std::function<double (double, const void*) > func,
                        const void* pVoid, 
                        double dMin, double dMax, double dStep, int iMax, double dTolerance)
{
    bool bRet = false;
    //最小値＞最大値なら入れ換える。
    if(dMin > dMax)
    {
        std::swap(dMin, dMax);
    }

    //二分法で解を求める
    double dValNext;
    double dRet;
    double dSign;
    for(double dVal = dMin; dVal <= dMax; dVal += dStep)
    {
        dValNext = dVal + dStep;
        
        //区間の両端で関数の値が異なっているなら解を求める
        double dA = func(dVal, pVoid);
        double dB = func(dValNext, pVoid);

        if (fabs(fabs(dA) - DBL_MAX) < NEAR_ZERO)
        {
            continue;
        }

        if (fabs(fabs(dB) - DBL_MAX) < NEAR_ZERO)
        {
            continue;
        }

        dSign = dA * dB;
        if(dSign <= 0.0)
        {

#if 0 //INSERT
        double dAns1;
        double dAns2; 
        dAns1 = func(dVal, pVoid);
        dAns2 = func(dValNext, pVoid);
        DB_PRINT(_T("  v:%f,%f v2:%f,%f \n"), dVal, dAns1, dValNext, dAns2);
#endif

            if (BisectionPart(&dRet, func, pVoid, dVal, dValNext, iMax, dTolerance))
            {
                auto ite = std::find(plstRet->begin(), plstRet->end(), dRet);

                if (ite == plstRet->end())
                {
                    plstRet->push_back(dRet);
                }
                bRet = true;
            }
        }
    }
    return bRet;
}

 
// MISTより 改変
//! - 参考文献
//!   - Richard P. Brent, "Algorithms for Minimization Without Derivatives", DOVER PUBLICATIONS, Mineola, New York
//! 
//! @note 指定された区間 [a, b] 内での極小値を探索する
//! @note 区間内に極小が存在しない場合は，区間内で最小となる位置を出力する
//! 
//! @param[in]  dMin           … 区間の左端
//! @param[in]  b              … 区間の右端
//! @param[out] x              … 極小を与える座標値
//! @param[in]  f              … 評価関数
//! @param[in]  dTolerance      … 許容誤差
//! @param[out] pIterations     … 実際の反復回数
//! @param[in]  max_iterations … 最大反復回数
//!
//! @return 極小を与える座標値における評価値
double CUtil::CalcMin( double dMin, double dMax, double* pX, 
               std::function<double (double, const void*) > func, const void* pVoid, 
               double dTolerance, int* pIterations, int iMaxCalc )
{
	double u, v, w, fu, fv, fw, fx, e = 0.0, d = 0.0;
	const double c = ( 3.0 - std::sqrt( 5.0 ) ) / 2.0;

	// dMin<= x <= dMaxで極小を囲う区間に変更する
	if( dMin > dMax )
	{
        std::swap(dMin, dMax);
	}

	v = w = *pX = dMin + c * ( dMax- dMin );
	fv = fw = fx = func(*pX, pVoid);

	int ite;
	for( ite = 1 ; ite <= iMaxCalc ; ite++ )
	{
		double m = ( dMin + dMax ) * 0.5;
		double tol = 1.0e-12 * std::abs( *pX ) + dTolerance;
		double t2 = 2.0 * tol;

		// 最後に判定した最小値候補点と，区間 [a, b] の中間との差が許容誤差内になったら終了する
		if( std::abs(*pX - m ) <= t2 - 0.5 * ( dMax- dMin) )
		{
			break;
		}

		double p = 0.0, q = 0.0, r = 0.0;

		if( std::abs( e ) > tol )
		{
			// 放物線補間を行う
			r = (*pX - w ) * ( fx - fv );
			q = (*pX - v ) * ( fx - fw );
			p = (*pX - v ) * q - (*pX - w ) * r;
			q = 2.0 * ( q - r );

			if( q > 0 )
			{
				p = -p;
			}
			else
			{
				q = -q;
			}

			r = e;
			e = d;
		}

		if( std::abs( p ) < std::abs( 0.5 * q * r ) && 
            p > q * ( dMin- *pX) &&
            p < q * ( dMax- *pX) )
		{
			// 放物線補間が適切に行われたので区間を更新する
			d = p / q;
			u = *pX + d;

			if( u - dMin< t2 || dMax- u < t2 )
			{
				d = *pX < m ? tol : -tol;
			}
		}
		else
		{
			// 放物線補間は不適切なので黄金分割する
			// 区間の大きいほうを黄金分割する
			e = (*pX < m ? dMax: dMin) - *pX;
			d = c * e;
		}

		u = *pX + ( std::abs( d ) >= tol ? d : ( d > 0 ? tol : -tol ) );
		fu = func( u , pVoid);

		if( fu <= fx )
		{
			// より小さい値が見つかったので a, b, v, w を更新する
			if( u < *pX)
			{
				// 区間 dMin< u < x に極小値がある
				dMax = *pX;
			}
			else
			{
				// 区間 x < u < dMaxに極小値がある
				dMin = *pX;
			}
			v  = w;
			w  = *pX;
			fv = fw;
			fw = fx;
            *pX = u;
			fx = fu;
		}
		else
		{
			// より大きい値が見つかったので a, dMaxを更新する
			if( u < *pX)
			{
				// 区間 u < x < dMaxに極小値がある
				dMin = u;
			}
			else
			{
				// 区間 dMin< x < u に極小値がある
				dMax = u;
			}

			// 新しく評価した点と従来評価した点の大小関係を調べる
			if( fu <= fw || w == *pX)
			{
				v = w;
				w = u;
				fv = fw;
				fw = fu;
			}
			else if( fu <= fv || v == *pX || v == w )
			{
				v = u;
				fv = fu;
			}
		}
	}

    if (pIterations)
    {
	    *pIterations = ite;
    }
	return( fx );
}


void CUtil::QuadraticEquation(std::vector<double>* pList, double a, double b, double c)
			/* $ax^2+ bx + c = 0$, $a \ne 0$ */
{
    double dD = b*b - 4.0 * a *c;
    double dX;
    if (fabs(dD) < NEAR_ZERO)
    {
        dX = -b/(2.0*a);
        pList->push_back(dX);
    }
    else if (dD > 0)
    {
        dX = (-b+sqrt(dD))/(2.0*a);
        pList->push_back(dX);
        dX = (-b-sqrt(dD))/(2.0*a);
        pList->push_back(dX);
    }
}

double CUtil::Cuberoot(double x)  /* $\sqrt[3]{x}$ */
{
	double s, prev;
	int pos;

	if (x == 0) return 0;
	if (x > 0) pos = 1;  else {  pos = 0;  x = -x;  }
	if (x > 1) s = x;  else s = 1;
	do {
		prev = s;  s = (x / (s * s) + 2 * s) / 3;
	} while (s < prev);
	if (pos) return prev;  else return -prev;
}

void CUtil::Cardano(std::vector<double>* pList, double a, double b, double c, double d)
			/* $ax^3 + bx^2 + cx + d = 0$, $a \ne 0$ */
{
	double p, q, t, a3, b3, x1, x2, x3;

	b /= (3 * a);  c /= a;  d /= a;
	p = b * b - c / 3;
	q = (b * (c - 2 * b * b) - d) / 2;
	a = q * q - p * p * p;
	if (a == 0) {
		q = Cuberoot(q);  x1 = 2 * q - b;  x2 = -q - b;

        //重解
        pList->push_back(x1);
        pList->push_back(x2);

	} else if (a > 0) {
		if (q > 0) a3 = Cuberoot(q + sqrt(a));
		else       a3 = Cuberoot(q - sqrt(a));
		b3 = p / a3;
		x1 = a3 + b3 - b;
        //x2 = -0.5 * (a3 + b3) - b;
		//x3 = fabs(a3 - b3) * sqrt(3.0) / 2;
        pList->push_back(x1);

	} else {
		a = sqrt(p);  t = acos(q / (p * a));  a *= 2;
		x1 = a * cos( t           / 3) - b;
		x2 = a * cos((t + 2 * PI) / 3) - b;
		x3 = a * cos((t + 4 * PI) / 3) - b;

        pList->push_back(x1);
        pList->push_back(x2);
        pList->push_back(x3);
    }
}

// ax^4+bx^3+cx^2+dx+e=0
void CUtil::QuarticEquation(std::vector<double>* pList, double a, double b, double c, double d, double e)
{
    double A3 = b/a;
    double A2 = c/a;
    double A1 = d/a;
    double A0 = e/a;



    double B2= -0.375*A3*A3+A2;
    double B1=  0.125*A3*A3*A3-0.5*A3*A2+A1;
    double B0= -(3.0/256.0)*A3*A3*A3*A3
               +0.0625*A3*A3*A2
               -0.25*A3*A1
               +A0;
    double x;
    if(fabs(B1) < NEAR_ZERO)
    {
        double DD = B2*B2 - 4.0 * B0;

        if (fabs(DD) < NEAR_ZERO)
        {
            if(fabs(0.5*B2) < NEAR_ZERO)
            {
                x = - A3 / 4.0;
                pList->push_back(x);
                return;
            }

            if (B2 > 0)
            {
                //実解なし
                return;
            }

            double y;

            y = sqrt(0.5*(-B2));
            x = y -A3/4.0;
            pList->push_back( x);
            pList->push_back(-x);
        }
        else if (DD < 0)
        {
            //実解なし
            return;
        }
        else
        {
            double y;
            double E[2];
            E[0] =  0.5*(-B2 + sqrt(DD));
            E[1] =  0.5*(-B2 - sqrt(DD));

            for(double& EE: E)
            {
                if (EE > 0)
                {
                    y = sqrt(EE);
                    x = y -A3/4.0;
                    pList->push_back(x);
                    y = -y;
                    x = y -A3/4.0;
                    pList->push_back(x);
                }
            }
            return;
        }
    }

    double C2 = 2*B2;
    double C1 = B2*B2-4.0*B0;
    double C0 = -(B1*B1);


    std::vector<double> list;

    Cardano(&list, 1.0, C2, C1, C0);

    if (list.empty())
    {
        return;
    }

    bool bAns = false;
    for(double &T: list)
    {
        double D2  =  2.0*T;
        if (T > 0)
        {
            double D11 = -2*T*sqrt(T);
            double D01 =  B2*T+T*T+sqrt(T)*B1;

            double D12 = -D11;
            double D02 =  B2*T+T*T-sqrt(T)*B1;

            std ::vector<double> qList;
            QuadraticEquation(&qList, D2, D11, D01);
            QuadraticEquation(&qList, D2, D12, D02);

            double dX;
            for(double dY: qList)
            {
                dX = dY -A3/4.0;
                pList->push_back(dX);
                bAns = true;
            }
        }
        if(bAns)
        {
            break;
        }
    }
}

/**
 *  @brief   クリッピング処理
 *  @param   [inout] pPt1 直線の端点１
 *  @param   [inout] pPt2 直線の端点２ 
 *  @param   [in] rc クリッピングする矩形 
 *  @retval  true 描画あり false 描画なし
 *  @note
 */
bool CUtil::Clip(POINT* pPt1, POINT* pPt2, RECT rc)
{
    return (CUtil::Clip2(pPt1, pPt2, rc) != CLIP_INVISIBLE);
}

CLIP_STS CUtil::Clip2(POINT* pPt1, POINT* pPt2, RECT rc)
{
    enum Edge 
    {
        NONE      = 0,
        LEFT      = 1,
        RIGHT     = 2,
        TOP       = 4,
        BOTTOM    = 8,
    };

    auto GetCode = [&](POINT* pt)
    {
      int code = 0;
      if( pt->x < rc.left )   {code += LEFT;}
      if( pt->x > rc.right )  {code += RIGHT;}
      if( pt->y > rc.top)     {code += TOP;}
      if( pt->y < rc.bottom ) {code += BOTTOM;}
      return code;
    };

    auto CalcX = [&]( POINT p0, POINT p1, int iX, POINT* pPt )
    {
      int cy = ( p1.y - p0.y ) * ( iX - p0.x ) / ( p1.x - p0.x ) + p0.y;

      if ( ( cy < rc.bottom ) || ( cy > rc.top ) )
      {
          return false;
      }

      pPt->x = iX;
      pPt->y = cy;

      return true;
    };

    auto CalcY = [&]( POINT p0, POINT p1, int iY, POINT* pPt )
    {
      int cx = ( p1.x - p0.x ) * ( iY - p0.y ) / ( p1.y - p0.y ) + p0.x;

      if ( ( cx < rc.left ) || ( cx > rc.right ) )
      {
          return false;
      }

      pPt->x = cx;
      pPt->y = iY;

      return true;
    };

    auto CalcPoint = [&] ( int code, POINT p0, POINT p1, POINT* pPt )
    {
      if ( ( code & LEFT ) != 0 )
      {
        if ( CalcX( p0, p1, rc.left, pPt ) )
        {
          return true;
        }
      }

      if ( ( code & RIGHT ) != 0 )
      {
        if ( CalcX( p0, p1, rc.right, pPt ) )
        {
          return true;
        }
      }

      if ( ( code & TOP ) != 0)
      {
        if ( CalcY( p0, p1, rc.top, pPt ) )
        {
          return true;
        }
      }

      if ( ( code & BOTTOM ) != 0 )
      {
        if ( CalcY( p0, p1, rc.bottom, pPt ) )
        {
          return true;
        }
      }

      return false;  /* クリッピングされなかった場合、線分は完全に不可視 */
    };

    int code0, code1; // 端点分類コード 

    code0 = GetCode( pPt1 );  // 始点の端点分類コードを求める 
    code1 = GetCode( pPt2 );  // 終点の端点分類コードを求める 

    // 端点分類コードがどちらも0000ならばクリッピングは必要なし 
    if ( ( code0 == 0 ) && 
         ( code1 == 0 ) )
    {
        return CLIP_NONE;
    }

    /* 始点・終点の端点分類コードの論理積が非０ならば線分は完全に不可視 */
    if ( ( code0 & code1 ) != 0 ) 
    {
        return CLIP_INVISIBLE;
    }

    /* 始点のクリッピング */
    if( code0 != 0 )
    {
        if ( ! CalcPoint( code0, *pPt1, *pPt2, pPt1 ) )
        {
            return CLIP_INVISIBLE;
        }
    }

    /* 終点のクリッピング */
    if( code1 != 0 )
    {
        if ( ! CalcPoint( code1, *pPt1, *pPt2, pPt2 ) )
        {
            return CLIP_INVISIBLE;
        }
    }

  return CLIP_EXIST;
}
//*******************************
//             TEST
//*******************************
#ifdef _DEBUG

void TEST_FindNumberingString()
{
    using namespace boost::assign;

    std::vector<StdString> lstData;
    StdString strResult;

    lstData += _T("Data_001"), _T("Data_02"), _T("Data04"), _T("Datd_05");
    strResult = CUtil::FindNumberingString(&lstData, _T("Data"), 3);

    STD_ASSERT(strResult == _T("Data_005"));

    strResult = CUtil::FindNumberingString(&lstData, _T("Data"));
    STD_ASSERT(strResult == _T("Data"));

    lstData.push_back(_T("Data"));
    strResult = CUtil::FindNumberingString(&lstData, _T("Data"));
    STD_ASSERT(strResult == _T("Data_5"));

    lstData += _T("Data_999");
    strResult = CUtil::FindNumberingString(&lstData, _T("Data"), 3);

    STD_ASSERT(strResult == _T("Data_000"));

    lstData.clear();
    strResult = CUtil::FindNumberingString(&lstData, _T("Data"), 4);

    STD_ASSERT(strResult == _T("Data0000"));

}

void TEST_NormAngle()
{
    CUtil::DbgOut(_T("  %s \n"), DB_FUNC_NAME.c_str());

    double dAngleAns = 0.0;
    dAngleAns = CUtil::NormAngle(0.0);
    STD_ASSERT_DBL_EQ(dAngleAns, 0.0);

    dAngleAns = CUtil::NormAngle(-10.0);
    STD_ASSERT_DBL_EQ(dAngleAns, 350.0);

    dAngleAns = CUtil::NormAngle(-370.0);
    STD_ASSERT_DBL_EQ(dAngleAns, 350.0);

    dAngleAns = CUtil::NormAngle(360.0);
    STD_ASSERT_DBL_EQ(dAngleAns, 0.0);

    dAngleAns = CUtil::NormAngle(370.0);
    STD_ASSERT_DBL_EQ(dAngleAns, 10.0);

}

void TEST_FormatedText()
{

    bool bRet;
    int iWidth;
    int iPos;
    StdString strRet;
    StdString strText = _T("C123,Hello World");
    bRet = CUtil::FormatedText( strText, &iWidth, &iPos, &strRet);
    STD_ASSERT(bRet);
    STD_ASSERT(iWidth == 123);
    STD_ASSERT(iPos == 0x0002/*LVCFMT_CENTER*/);
    STD_ASSERT(strRet == _T("Hello World"));

    strText = _T("L123,  Hello World  ");
    bRet = CUtil::FormatedText( strText, &iWidth, &iPos, &strRet);
    STD_ASSERT(bRet);
    STD_ASSERT(iWidth == 123);
    STD_ASSERT(iPos == 0x0000/*LVCFMT_LEFT*/);
    STD_ASSERT(strRet == _T("  Hello World  "));

    strText = _T("R123  ,  Hello World  ");
    bRet = CUtil::FormatedText( strText, &iWidth, &iPos, &strRet);
    STD_ASSERT(bRet);
    STD_ASSERT(iWidth == 123);
    STD_ASSERT(iPos == 0x0001/*LVCFMT_RIGHT*/);
    STD_ASSERT(strRet == _T("  Hello World  "));

    strText = _T("CA123  ,  Hello World  ");
    bRet = CUtil::FormatedText( strText, &iWidth, &iPos, &strRet);
    STD_ASSERT(!bRet);

}

void TEST_SeparateComma()
{
    StdString strTest;
    StdString strRes;

    strTest = _T("123.4567.890.234");
    
    STD_ASSERT(CUtil::SeparateComma(strTest, &strRes));
    STD_ASSERT(strRes == _T("123"));
    STD_ASSERT(strTest == _T("4567.890.234"));

    STD_ASSERT(CUtil::SeparateComma(strTest, &strRes));
    STD_ASSERT(strRes == _T("4567"));
    STD_ASSERT(strTest == _T("890.234"));

    STD_ASSERT(CUtil::SeparateComma(strTest, &strRes));
    STD_ASSERT(strRes == _T("890"));
    STD_ASSERT(strTest == _T("234"));

    STD_ASSERT(!CUtil::SeparateComma(strTest, &strRes));
}

void TEST_TokenizeCsv()
{

    StdString strSrc = _T("Hello,1,\"コンマ,を入れても\",,   sss");

    StdString strAns1= _T("Hello");
    StdString strAns2= _T("1");
    StdString strAns3= _T("コンマ,を入れても");
    StdString strAns4= _T("");        // コンマが連続している場合は空文字
    StdString strAns5= _T("   sss");  // 空白は評価される
    
    std::vector<StdString> lstStr;

    CUtil::TokenizeCsv(&lstStr, strSrc);

    STD_ASSERT(lstStr.size() == 5);
    STD_ASSERT(lstStr[0] == strAns1);
    STD_ASSERT(lstStr[1] == strAns2);
    STD_ASSERT(lstStr[2] == strAns3);
    STD_ASSERT(lstStr[3] == strAns4);
    STD_ASSERT(lstStr[4] == strAns5);

    /*
    StdString strSrc2 = _T("\"\",\"\",\"\"");

    CUtil::TokenizeCsv(&lstStr, strSrc2);

    StdString strAns= _T("");
    STD_ASSERT(lstStr.size() == 3);
    STD_ASSERT(lstStr[0] == strAns);
    STD_ASSERT(lstStr[1] == strAns);
    STD_ASSERT(lstStr[2] == strAns);
    */

}

void TEST_DiffAngle90()
{
    
    STD_ASSERT_DBL_EQ(CUtil::DiffAngle90(0.0,   10.0),  10.0);
    STD_ASSERT_DBL_EQ(CUtil::DiffAngle90(0.0, -355.0),   5.0);
    STD_ASSERT_DBL_EQ(CUtil::DiffAngle90(0.0, -265.0),  85.0);
    STD_ASSERT_DBL_EQ(CUtil::DiffAngle90(0.0, -115.0), -65.0);
    STD_ASSERT_DBL_EQ(CUtil::DiffAngle90(0.0,  -40.0), -40.0);
    STD_ASSERT_DBL_EQ(CUtil::DiffAngle90(0.0,   10.0),  10.0);
    STD_ASSERT_DBL_EQ(CUtil::DiffAngle90(0.0,   95.0),  85.0);
    STD_ASSERT_DBL_EQ(CUtil::DiffAngle90(0.0,  185.0),  -5.0);
    STD_ASSERT_DBL_EQ(CUtil::DiffAngle90(0.0,  280.0), -80.0);
    STD_ASSERT_DBL_EQ(CUtil::DiffAngle90(0.0, 10.0), 10.0);

}





void TEST_StringToAny()
{
    boost::any  anyAns;
    StdString strRet;
    
    boost::any  anyInt = (int)12; 
    strRet = CUtil::AnyToString(anyInt);
    DB_PRINT(_T("%s \n"),strRet.c_str());
    anyAns = CUtil::StringToAny(strRet);
    STD_ASSERT(boost::any_cast<int>(anyInt) == boost::any_cast<int>(anyAns));

    boost::any  anyLong = (long)100; 
    strRet = CUtil::AnyToString(anyLong);
    DB_PRINT(_T("%s \n"),strRet.c_str());
    anyAns = CUtil::StringToAny(strRet);
    STD_ASSERT(boost::any_cast<long>(anyLong) == boost::any_cast<long>(anyAns));

    boost::any  anyDouble = 123.5; 
    strRet = CUtil::AnyToString(anyDouble);
    DB_PRINT(_T("%s \n"),strRet.c_str());
    anyAns = CUtil::StringToAny(strRet);
    STD_ASSERT(boost::any_cast<double>(anyDouble) == boost::any_cast<double>(anyAns));

    boost::any  anyBool = true; 
    strRet = CUtil::AnyToString(anyBool);
    DB_PRINT(_T("%s \n"),strRet.c_str());
    anyAns = CUtil::StringToAny(strRet);
    STD_ASSERT(boost::any_cast<bool>(anyBool) == boost::any_cast<bool>(anyAns));

    boost::any  anyUint = (unsigned int)100; 
    strRet = CUtil::AnyToString(anyUint);
    DB_PRINT(_T("%s \n"),strRet.c_str());
    anyAns = CUtil::StringToAny(strRet);
    STD_ASSERT(boost::any_cast<unsigned int>(anyUint) == boost::any_cast<unsigned int>(anyAns));

    boost::any  anyUlong = (unsigned long)100; 
    strRet = CUtil::AnyToString(anyUlong);
    DB_PRINT(_T("%s \n"),strRet.c_str());
    anyAns = CUtil::StringToAny(strRet);
    STD_ASSERT(boost::any_cast<unsigned long>(anyUlong) == boost::any_cast<unsigned long>(anyAns));

    boost::any  anyColor = (COLORREF)100; 
    strRet = CUtil::AnyToString(anyColor);
    DB_PRINT(_T("%s \n"),strRet.c_str());
    anyAns = CUtil::StringToAny(strRet);
    STD_ASSERT(boost::any_cast<COLORREF>(anyColor) == boost::any_cast<COLORREF>(anyAns));

    boost::any  anyFloat = float(123.32); 
    strRet = CUtil::AnyToString(anyFloat);
    DB_PRINT(_T("%s \n"),strRet.c_str());
    anyAns = CUtil::StringToAny(strRet);
    STD_ASSERT(boost::any_cast<float>(anyFloat) == boost::any_cast<float>(anyAns));

    boost::any  anyString = StdString(_T("Hello")); 
    strRet = CUtil::AnyToString(anyString);
    DB_PRINT(_T("%s \n"),strRet.c_str());
    anyAns = CUtil::StringToAny(strRet);

    StdString strSrc  = boost::any_cast<StdString>(anyString);
    StdString strAns  = boost::any_cast<StdString>(anyAns);

    STD_ASSERT(boost::any_cast<StdString>(anyString) == boost::any_cast<StdString>(anyAns));
    STD_ASSERT(strSrc == strAns);


    POINT2D pt2D;
    std::vector<POINT2D> lst2D;

    pt2D.dX = 10.1;
    pt2D.dY = 10.2;
    lst2D.push_back(pt2D);

    pt2D.dX = 11.1;
    pt2D.dY = 11.2;
    lst2D.push_back(pt2D);

    pt2D.dX = 12.1;
    pt2D.dY = 12.2;
    lst2D.push_back(pt2D);

    boost::any  anyPt2dList = lst2D; 
    strRet = CUtil::AnyToString(anyPt2dList);
    DB_PRINT(_T("%s \n"),strRet.c_str());
    anyAns = CUtil::StringToAny(strRet);

    std::vector<POINT2D>* pList;
    std::vector<POINT2D>* pAns;
    /* これはうまくいかない
    pList = &boost::any_cast<std::vector<POINT2D> >(anyPt2dList);
    pAns  = &boost::any_cast<std::vector<POINT2D> >(anyAns);
    */

    std::vector<POINT2D> lstPtList;
    std::vector<POINT2D> lstPtAns;
    lstPtList = boost::any_cast<std::vector<POINT2D> >(anyPt2dList);
    lstPtAns  = boost::any_cast<std::vector<POINT2D> >(anyAns);
    pList = &lstPtList;
    pAns  = &lstPtAns;


    STD_ASSERT(pList->size() == pAns->size());
    STD_ASSERT(pList->at(0) == pAns->at(0));
    STD_ASSERT(pList->at(1) == pAns->at(1));
    STD_ASSERT(pList->at(2) == pAns->at(2));

    boost::any  anyPt2 = pt2D; 
    strRet = CUtil::AnyToString(anyPt2);
    DB_PRINT(_T("%s \n"),strRet.c_str());
    anyAns = CUtil::StringToAny(strRet);
    STD_ASSERT(boost::any_cast<POINT2D>(anyPt2) == boost::any_cast<POINT2D>(anyAns));
}

void TEST_Guid()
{   
    StdString strRet;
    HRESULT hres;
    GUID guid;
    hres =  CoCreateGuid(&guid);
    strRet = CUtil::GuidToString(guid);
    DB_PRINT(_T("GUID %s \n"),strRet.c_str());

    int a=10;
}

void TEST_IsFloatVal()
{   
    STD_ASSERT( CUtil::IsFloatVal(_T("123.23")) );
    STD_ASSERT( CUtil::IsFloatVal(_T("-123.33")) );
    STD_ASSERT( CUtil::IsFloatVal(_T("  -123.33  ")) );
    STD_ASSERT( CUtil::IsFloatVal(_T("+21.310")) );
    STD_ASSERT(!CUtil::IsFloatVal(_T("21.21.22")) );
    STD_ASSERT( CUtil::IsFloatVal(_T("-21e23")) );
    STD_ASSERT( CUtil::IsFloatVal(_T("+2.1e-23")) );
    STD_ASSERT(!CUtil::IsFloatVal(_T("+2.1e-0.5")) );
    STD_ASSERT(!CUtil::IsFloatVal(_T("1.232e")) );
    STD_ASSERT(!CUtil::IsFloatVal(_T("-.23")) );
    STD_ASSERT(!CUtil::IsFloatVal(_T("a.23")) );
    STD_ASSERT(!CUtil::IsFloatVal(_T("abc")) );
}


void TEST_Clipping()
{
    RECT rc;
    rc.top      = 30;
    rc.bottom   = -10;
    rc.left     = -50;
    rc.right    = 20;

    POINT p1;
    POINT p2;
    POINT p1Ans;
    POINT p2Ans;

    
    auto EqP = [](POINT p1, POINT p2)
    {
        bool bRet;
        bRet = (p1.x == p2.x) && (p1.y == p2.y);
        return bRet;
    };

    //矩形内
    p1.x = -30;    p1.y =  20;
    p2.x =  10;    p2.y =   0;
    p1Ans = p1;
    p2Ans = p2;

    STD_ASSERT(CUtil::Clip(&p1, &p2, rc));
    STD_ASSERT(EqP(p1, p1Ans));
    STD_ASSERT(EqP(p2, p2Ans));

    //両端クリップ
    p1.x = -60;    p1.y =  50;
    p2.x =  60;    p2.y = -10;

    p1Ans.x = -20;    p1Ans.y =  30;
    p2Ans.x =  20;    p2Ans.y =  10;

    STD_ASSERT(CUtil::Clip(&p1, &p2, rc));
    STD_ASSERT(EqP(p1, p1Ans));
    STD_ASSERT(EqP(p2, p2Ans));

    p1.x =    0;    p1.y =  50;
    p2.x =  -80;    p2.y = -30;

    p1Ans.x = -20;    p1Ans.y =  30;
    p2Ans.x = -50;    p2Ans.y =   0;

    STD_ASSERT(CUtil::Clip(&p1, &p2, rc));
    STD_ASSERT(EqP(p1, p1Ans));
    STD_ASSERT(EqP(p2, p2Ans));

    //------------
    //Y
    //------------
    p1.x =   -20;    p1.y =  50;
    p2.x =   -20;    p2.y = -70;

    p1Ans.x =  -20;    p1Ans.y =   30;
    p2Ans.x =  -20;    p2Ans.y =  -10;

    STD_ASSERT(CUtil::Clip(&p1, &p2, rc));
    STD_ASSERT(EqP(p1, p1Ans));
    STD_ASSERT(EqP(p2, p2Ans));


    p1.x =   -20;    p1.y =  50;
    p2.x =   -20;    p2.y =  10;

    p1Ans.x =  -20;    p1Ans.y =   30;
    p2Ans.x =  -20;    p2Ans.y =   10;

    STD_ASSERT(CUtil::Clip(&p1, &p2, rc));
    STD_ASSERT(EqP(p1, p1Ans));
    STD_ASSERT(EqP(p2, p2Ans));

    p1.x =   -20;    p1.y =   10;
    p2.x =   -20;    p2.y =  -70;

    p1Ans.x =  -20;    p1Ans.y =   10;
    p2Ans.x =  -20;    p2Ans.y =  -10;

    STD_ASSERT(CUtil::Clip(&p1, &p2, rc));
    STD_ASSERT(EqP(p1, p1Ans));
    STD_ASSERT(EqP(p2, p2Ans));

    //------------
    //X
    //------------
    p1.x =   -90;    p1.y =  20;
    p2.x =    50;    p2.y =  20;

    p1Ans.x =  -50;    p1Ans.y =   20;
    p2Ans.x =   20;    p2Ans.y =   20;

    STD_ASSERT(CUtil::Clip(&p1, &p2, rc));
    STD_ASSERT(EqP(p1, p1Ans));
    STD_ASSERT(EqP(p2, p2Ans));

    p1.x =   -90;    p1.y =  20;
    p2.x =     0;    p2.y =  20;

    p1Ans.x =  -50;    p1Ans.y =   20;
    p2Ans.x =    0;    p2Ans.y =   20;

    STD_ASSERT(CUtil::Clip(&p1, &p2, rc));
    STD_ASSERT(EqP(p1, p1Ans));
    STD_ASSERT(EqP(p2, p2Ans));

    p1.x =     0;    p1.y =  20;
    p2.x =    50;    p2.y =  20;

    p1Ans.x =    0;    p1Ans.y =   20;
    p2Ans.x =   20;    p2Ans.y =   20;

    STD_ASSERT(CUtil::Clip(&p1, &p2, rc));
    STD_ASSERT(EqP(p1, p1Ans));
    STD_ASSERT(EqP(p2, p2Ans));

}
void TEST_DevideLastNum()
{
    StdString strBase;
    int iVal;
    bool bRet;
    int iDigit;
    bRet = CUtil::DevideLastNum(&strBase, &iDigit, &iVal, _T("DDD_001"));

    STD_ASSERT(bRet);
    STD_ASSERT(strBase == _T("DDD_"));
    STD_ASSERT(iDigit == 3);
    STD_ASSERT(iVal == 1);
}                                       

void TEST_QuarticEquation()
{
    std::vector<double> lstAns;
    CUtil::QuarticEquation(&lstAns, 1, -7, 5, 31, -30);
    //-2.1.3.5
    STD_ASSERT(lstAns.size() == 4);

}


#endif //_DEBUG


//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void TEST_CUtil()
{
    CUtil::DbgOut(_T("%s Start\n"), DB_FUNC_NAME.c_str());
        TEST_FindNumberingString();
        TEST_NormAngle();
        TEST_FormatedText();
        TEST_SeparateComma();
        TEST_TokenizeCsv();
        TEST_StringToAny();
        TEST_Guid();
        TEST_IsFloatVal();
        TEST_Clipping();
        TEST_DevideLastNum();
        TEST_QuarticEquation();
    CUtil::DbgOut(_T("%s End\n"), DB_FUNC_NAME.c_str());
    CUtil::DbgOut(_T("\n"));
}
#endif //_DEBUG