/**
 * @brief			CUtilヘッダーファイル
 * @file			CUtitlty.h
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#if !defined (__CUTILITY_H__)
#define __CUTILITY_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DrawingObject/Primitive/POINT2D.h"

class asIScriptEngine;
/**
 シルアル化
*/
namespace boost 
{
  namespace serialization 
  {

    //----------------
    //    POINT
    //----------------
    template <class Archive>
    void serialize(Archive& ar, POINT& rpoint, const unsigned int version) 
    {
        try
        {
            ar & boost::serialization::make_nvp("X"     , rpoint.x);
            ar & boost::serialization::make_nvp("Y"     , rpoint.y);
        }
        catch(...)
        {
            STD_DBG(_T("Serlization Error"));
        }
    }

    //----------------
    //    SIZE
    //----------------
    template <class Archive>
    void serialize(Archive& ar, SIZE& rsize, const unsigned int version) 
    {
        try
        {
            ar & boost::serialization::make_nvp("X"     , rsize.cx);
            ar & boost::serialization::make_nvp("Y"     , rsize.cy);
        }
        catch(...)
        {
            STD_DBG(_T("Serlization Error"));
        }
    }

    //--------------------
    //    any
    //--------------------
    template <class Archive>
    void serialize(Archive& ar, boost::any& rAny, const unsigned int version) 
    {
        StdString strAny;
        try
        {
            if (Archive::is_saving::value)
            {
                strAny = CUtil::AnyToString(rAny);
            }

            ar & boost::serialization::make_nvp("Val"          ,     strAny);

            if (Archive::is_loading::value)
            {
                rAny = CUtil::StringToAny(strAny);
            }
        }
        catch(...)
        {
            STD_DBG(_T("Serlization Error"));
        }
    }

    //--------------------
    //    LOGFONT(LOAD)
    //--------------------
    template <class Archive>
    void load(Archive& ar, LOGFONT& rfont, const unsigned int version) 
    {
        StdString strFaceName;
        strFaceName = rfont.lfFaceName;
        ar & boost::serialization::make_nvp("lfHeight"          ,     rfont.lfHeight);
        ar & boost::serialization::make_nvp("lfWidth"           ,     rfont.lfWidth);
        ar & boost::serialization::make_nvp("lfEscapement"      ,     rfont.lfEscapement);
        ar & boost::serialization::make_nvp("lfOrientation"     ,     rfont.lfOrientation);
        ar & boost::serialization::make_nvp("lfWeight"          ,     rfont.lfWeight);
        ar & boost::serialization::make_nvp("lfItalic"          ,     rfont.lfItalic);
        ar & boost::serialization::make_nvp("lfUnderline"       ,     rfont.lfUnderline);
        ar & boost::serialization::make_nvp("lfStrikeOut"       ,     rfont.lfStrikeOut);
        ar & boost::serialization::make_nvp("lfCharSet"         ,     rfont.lfCharSet);
        ar & boost::serialization::make_nvp("lfOutPrecision"    ,     rfont.lfOutPrecision);
        ar & boost::serialization::make_nvp("lfClipPrecision"   ,     rfont.lfClipPrecision);
        ar & boost::serialization::make_nvp("Quality"           ,     rfont.lfQuality);
        ar & boost::serialization::make_nvp("PitchAndFamily"    ,     rfont.lfPitchAndFamily);
        ar & boost::serialization::make_nvp("FaceName"          ,     strFaceName);
    }

    //--------------------
    //    LOGFONT(SAVE)
    //--------------------
    template <class Archive>
    void save(Archive& ar, const LOGFONT& rfont, const unsigned int version) 
    {
        StdString strFaceName;
        ar & boost::serialization::make_nvp("lfHeight"          ,     rfont.lfHeight);
        ar & boost::serialization::make_nvp("lfWidth"           ,     rfont.lfWidth);
        ar & boost::serialization::make_nvp("lfEscapement"      ,     rfont.lfEscapement);
        ar & boost::serialization::make_nvp("lfOrientation"     ,     rfont.lfOrientation);
        ar & boost::serialization::make_nvp("lfWeight"          ,     rfont.lfWeight);
        ar & boost::serialization::make_nvp("lfItalic"          ,     rfont.lfItalic);
        ar & boost::serialization::make_nvp("lfUnderline"       ,     rfont.lfUnderline);
        ar & boost::serialization::make_nvp("lfStrikeOut"       ,     rfont.lfStrikeOut);
        ar & boost::serialization::make_nvp("lfCharSet"         ,     rfont.lfCharSet);
        ar & boost::serialization::make_nvp("lfOutPrecision"    ,     rfont.lfOutPrecision);
        ar & boost::serialization::make_nvp("lfClipPrecision"   ,     rfont.lfClipPrecision);
        ar & boost::serialization::make_nvp("Quality"           ,     rfont.lfQuality);
        ar & boost::serialization::make_nvp("PitchAndFamily"    ,     rfont.lfPitchAndFamily);
        ar & boost::serialization::make_nvp("FaceName"          ,     strFaceName);
        memset (rfont.lfFaceName, 0, sizeof(rfont.lfFaceName));
        _tcscpy_s(rfont.lfFaceName, sizeof(rfont.lfFaceName), strFaceName.c_str());
    }

    template <class Archive>
    void serialize(Archive& ar, LOGFONT& rfont, const unsigned int version)
    {
        boost::serialization::split_free(ar, rfont, version);
    }

    //--------------------
    //    GUID
    //--------------------
    template <class Archive>
    void serialize(Archive& ar, GUID& guid, const unsigned int version) 
    {
        ar & boost::serialization::make_nvp("GUID"   ,
             boost::serialization::make_binary_object(&guid, sizeof(GUID)));
    }

    //--------------------
    //    UUID
    //--------------------
    template <class Archive>
    void load(Archive& ar, boost::uuids::uuid& uuid, const unsigned int version) 
    {
        StdString strUuid;
        ar >> boost::serialization::make_nvp("UUID"  , strUuid);
        uuid = boost::uuids::string_generator()(strUuid);
    }

    template <class Archive>
    void save(Archive& ar, const boost::uuids::uuid& uuid, const unsigned int version) 
    {
        StdStringStream strm;
        StdString strUuid;

        strm << uuid;
        strUuid = strm.str();
        ar << boost::serialization::make_nvp("UUID"  , strUuid);

    }

    template <class Archive>
    void serialize(Archive& ar, boost::uuids::uuid& uuid, const unsigned int version)
    {
        boost::serialization::split_free(ar, uuid, version);
    }
  }
}


/*---------------------------------------------------*/
/*  Enum                                             */
/*---------------------------------------------------*/
enum CLIP_STS
{
    CLIP_NONE = 0,
    CLIP_INVISIBLE = 1,
    CLIP_EXIST = 2,
};

union UniVal
{
    char             cVal;
    unsigned char    ucVal;
    short            sVal;
    unsigned short   usVal;
    int              iVal;
    unsigned int     uiVal;
    float            fVal;
    double           dVal;
    StdChar          strVal;
};

union UniVal64
{
    bool             bVal;
    char             cVal8[8];
    double           dVal;
    char             cVal;
    unsigned char    ucVal;
    short            sVal;
    unsigned short   usVal;
    int              iVal;
    unsigned int     uiVal;
    float            fVal;
    StdChar          strVal;
    __int64          iVal64;
    unsigned __int64          uiVal64;
    void*            pVal;
    const void*      pcVal;
};

/**
 * @class   NULL STRING
 * @brief                        
 */
const StdString gNULL_STR = _T("");

/**
 *  リングバッファ.
 */
template<class D>
class RING_BUFFER
{
public:
    RING_BUFFER()
    {
        Init(1024);
    }

    void Resize(long lSize)
    {
        Init(lSize);
    }

    virtual ~RING_BUFFER(){;}


    void SetData( char* p_data, long len)
    {
        long pos;
        long next;
        long dest;

        //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        m_cs.Lock();
        pos = m_pos[m_set];
        if ((pos + len) >= (long)m_buffer.size())
        {
            pos = 0;
            m_pos[m_set] = 0;
        }

        //データをコピー
        memcpy(&m_buffer[pos], p_data, len);
        m_len[m_set] = len;

        m_set++;
        if (m_set >= ms_ring_buf_cnt) {
            m_set = 0;
        }

        next = pos + len + 1;
        if ( next >= (int)m_buffer.size()) {
            next = 0;
        }

        m_pos[m_set] = next;
        m_cs.Unlock();
        //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

        //データがたまりすぎた場合書込にウェイトをかける
        if( m_get > m_set) {
            dest = ms_ring_buf_cnt - m_get + m_set;
        } else {
            dest = m_set - m_get;
        }

        if (  dest  > (ms_ring_buf_cnt * 0.7))
        {
            Sleep(100);
        }
    }

    /**
     * リングバッファーデータ取得.
     * @param   pData   [out] 設定データ
     * @return  データ長
     */
    long GetData(char* pData)
    {
        long pos;
        long len;

        if (m_set == m_get) {
            return 0;
        }

        //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        m_cs.Lock();

        len = m_len[m_get];
        pos = m_pos[m_get];
        memcpy(pData, &m_buffer[pos], len);

        m_get++;
        if (m_get >= ms_ring_buf_cnt) {
            m_get = 0;
        }
        m_cs.Unlock();
        //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
        return len;
    }


    /**
     * リングバッファーデータ有無取得.
     * @return  ture  新しいデータが設定されていない
     * @return  false 新しいデータが設定された
     */
    bool IsEmpty()
    {
        if (m_set == m_get) {
            return true;
        }
        return false;
    }


    void Init(long lSize){
        m_cs.Lock();
        m_buffer.resize(lSize);
        m_set = 0;
        m_get = 0;
        memset(&m_len[0], 0, sizeof(m_pos));
        memset(&m_pos[0], 0, sizeof(m_len));
        m_cs.Unlock();
    }

protected:
    static const long ms_ring_buf_cnt = 100;        //!< リングバッファデータ最大数
    std::vector< D > m_buffer;                      //!< データバッファ
    size_t           m_set;                         //!< 書込位置
    size_t           m_get;                         //!< 読込位置
    long             m_pos[ms_ring_buf_cnt];        //!< データ位置
    long             m_len[ms_ring_buf_cnt];        //!< データ長
    CCriticalSection m_cs;                          //!< クリティカルセクション
};
    

//スレッド間同期用のリングバッファ
template<class D>
class SYNC_BUFFER
{
private:
    std::vector<D> m_lstData;
    int lBufferSize;
    int lR;
    int lW;

public:
    SYNC_BUFFER()
    {
        _Init(1024);
    }

    virtual ~SYNC_BUFFER(){;}

    void Resize(long lSize)
    {
        _Init(lSize);
    }

    void Clear()
    {
        lR = lW = 0;
    }

    void SetData(D data)
    {
        int next = _GetNext(lW);
        m_lstData[next] = data;
        lW = next;
    }

    bool GetData(D* pData)
    {
        if (lR == lW) 
        {
            return false;
        }

        int next = _GetNext(lR);
        *pData = m_lstData[next];
        lR = next;
        return true;
    }

    bool IsEmpty()
    {
        return (lR == lW);
    }



protected:
    void _Init(int n)
    {
        lBufferSize = n;
        m_lstData.resize(n);
        lR = -1;
        lW = -1;
    }


    int _GetNext(int n)
    {
        int next;
        next = n + 1;
        if (next >= lBufferSize)
        {
            return 0;
        }
        return next;
    }
};

class DbgGuiLeak
{
public:
    explicit DbgGuiLeak ()
    {
        _guiResCount = ::GetGuiResources (::GetCurrentProcess (),
                                          GR_GDIOBJECTS);
    }
    ~DbgGuiLeak ()
    {
        int leaks = ::GetGuiResources (::GetCurrentProcess (), 
                                       GR_GDIOBJECTS) - _guiResCount;
        if (leaks != 0)
        {
            std::cout << "Gui Resources Leaked: " << leaks << std::endl;
        }
    }
private:
    unsigned _guiResCount;
};


/**
 * @class   CUtil
 * @brief                        
 */
class CUtil
{
    static const int POS_LEFT   = 0x01;
    static const int POS_RIGHT  = 0x02;
    static const int POS_TOP    = 0x04;
    static const int POS_BOTTOM = 0x08;

    public:

    //!< GDIチェック
    static HGDIOBJ SelectObject(HDC hdc, HGDIOBJ h);
    static BOOL DeleteObject(HGDIOBJ ho);

    //!< 仮想キー変換
    static LPCTSTR ConvVK(WPARAM param);

    //!< ４バイトエンディアン変換
    static unsigned long  Swap4Byte(void* data);

    //!< FLOATエンディアン変換
    static float          SwapFloat(void* pdata);

    //!< ２バイトエンディアン変換
    static unsigned short Swap2Byte(void* data);

    //!< デバッグ出力
    static void DbgOut( const TCHAR *format, ... );

    //!< 実行時間計測
    static void MesureTime(int nID, int nAct, LPCTSTR sOut);

    //!< エラーメッセージ
    static StdString GetErrMsg();
    
    //!< バイナリダンプ
    static void BinDump(const TCHAR* pData, long lLen);

    //!< char->StdString変換 
    static StdString CharToString(const char* str);

    //!< char->StdString変換 
    static std::string StringToChar(StdString strString);

    //!< 空白除去.
    static StdString Trim(const StdString& str);

    //!< 空白除去.
    static StdString TrimQuotation(const StdString& str);

    //!<  書式指定 string
    static StdString StrFormat( const TCHAR *format, ... );

    static StdString Last(const StdString& str);

    static void SPrintfWStringV(const std::wstring& str, va_list argptr);

    static void FormatV(std::wstring &str, const std::wstring &fmt, va_list argptr);

    static void SPrintfWString(const std::wstring str, ...);

    static void ConvUni(asIScriptEngine * pEngine, 
                        UniVal64* pUni, 
                        void* ref, int refTypeId);

    static void Tokenize(std::vector<std::wstring>* pParsedData, std::wstring strIn);

    //!< 経過時間取得.
    static DWORD DiffTime( DWORD old_time, DWORD cur_time);

    //!< 四捨五入
    static long Round( double val);

    //!< 三角形内部判定
    static bool IsInnerTriangle(const POINT2D& p, 
                     const POINT2D& a,
                     const POINT2D& b,
                     const POINT2D& c);

    static bool IsInnerTriangle2(const POINT2D& p, 
                     const POINT2D& a,
                     const POINT2D& b,
                     const POINT2D& c);
    //!< 角度拡張
    static bool UpdateeAngle(double* pStart, double* pEnd, double dAngle);


    //3点から角度を計算する
    static bool CalcAngle(double* pAngle,
               const POINT2D& pt1,
               const POINT2D& pt2,
               const POINT2D& ptCenter);

    //!< 点加算
    static POINT AddPoint(const POINT& pt1, const POINT& pt2); 

    //!< 点減算
    static POINT SubPoint(const POINT& pt1, const POINT& pt2); 

    //!< 点減算
    static bool EqPoint(const POINT& pt1, const POINT& pt2); 

    //!< 点距離
    static double LenPoint(const POINT& pt1, const POINT& pt2); 

    //!< 点距離
    static double LenPoint(const POINT& pt1); 

    //!< 直線パラメータの交点計算
    static bool CalcCrossPoint(POINT2D* pPt, 
                              double dA1, double dB1, double dC1,
                              double dA2, double dB2, double dC2);

    //!< 三角関数取得
    static void GetTriFunc(double dAngle, double* pSin, double* pCos); 

    //!< 角度計算
    static double GetAngle(const POINT2D& pPt1, const POINT2D& pPt2); 

    //!< 角度正規化
    static double NormAngle(double dAngle); 

    //!< 角度正規化(180度）
    static double NormAngle2(double dAngle); 

    //!< 鋭角計算
    static double DiffAngle(double dAngle1, double dAngle2); 

    //!< 鋭角計算角 （±９０°）
    static double DiffAngle90(double dAngle1, double dAngle2); 

    //!< 角度判定
    static bool IsInsideAngle(double dStart, double dEnd, double dAngle);

    //!< 象限取得
    static int GetQuadrant(double dAngle);

    //!< 同一方向判定
    static double GetDirection(const POINT2D& ptCenter, const POINT2D& ptDir, const POINT2D& pt);
    
    //!< スケーリングによる角度変換
    static double SclAngle(double dAngle, double dSclX, double dSclY); 

    //!<方向判定
    static int Side(const IPoint2D& pt1, const IPoint2D& pt2, const IPoint2D& pt);

    //!< クリッピング
    static bool Clipping(RECT Rec, POINT *P1, POINT *P2);
    
    //!< 拡張子取得
    static StdString GetExt(StdPath path);

    //!< 大文字変換
    static StdString ToUpper(StdString Str);

    //!< 小文字変換
    static StdString ToLower(StdString Str);

    //!< メニューアイテム追加
    static void AddMenuItem(HMENU hmenu, const StdChar* lpszItemName, int nId, HMENU hmenuSub);

    //!< メニューチェックアイテム追加
    static void AddMenuCheck(HMENU hmenu, const StdChar* lpszItemName, int nId, bool bRadio = false);

    //!< メニュー項目変更
    static void ConvertMenuString(HMENU hMenu);


    //!< データパス取得
    static StdPath GetDataPath();

    //!< 書式指定テキスト(ListCtrl用)
    static bool FormatedText(StdString strText,int* pWidth, int* iPos, StdString* pHeaderText);

    //!< ファイル名使用できない文字チェック
    static bool CheckFilename(StdString strFilename, bool bExecption = false);

    //!< ディレクトリ選択ダイアログコールバック
    static int CALLBACK FuncCallBackProc( HWND hWnd, UINT uMsg, LPARAM lParam, LPARAM lpData );

    //!< ディレクトリ選択ダイアログ
    static BOOL FuncSelectFolder( HWND hWnd, LPTSTR lpGetPath, LPCTSTR lpDefPath, LPCTSTR lpTitle );

    //後端数値分離
    static bool DevideLastNum(StdString* pBase,
        int*       pDigit,
        int*       pVal,
        const StdString& strName);

    //!< 数値つき文字列設定
    static StdString FindNumberingString( const std::vector<StdString>* pList, StdString strBase, int iDigit = -1);
    static StdString FindNumberingString( const std::set<StdString>* pList, StdString strBase, int iDigit = -1);

    //!< 識別子チェック
    static bool IsIdentifer(StdString strName);

    //!< ファイル読み込み 
    static bool LoadText(StdPath path, StdString* pStr);

    //!< ファイル読み込み 
    static bool LoadText(StdPath path, std::string * pStr);

    //!< ファイル検索
    static bool FindFiles( std::vector<StdString>* pFileList, StdString strFileName, StdPath path, bool bExt = true); 

    //!< コンマまでを分割
    static bool SeparateComma(StdString& strName, StdString* pRes);

    //!< 最後のコンマを削除
    static bool DeleteLastComma(StdString& strName);

    //!< CSV区切りで分割
    static void TokenizeCsv(std::vector<StdString>* pParsedData, StdString strIn, StdChar charSep = _T(','));

    //文字列からANY型への変換
    static boost::any StringToAny(const StdString& strAny);

    //ANY型から文字列への変換
    static StdString AnyToString(const boost::any& anyData, bool bType = true);

    //ANY型の比較
    static bool IsAnyEqual(const boost::any& any1, const boost::any& any2);

    static void ReplaceParenthesis(StdString* pStr);

    //Pathの比較
    static bool IsPathEqual(const StdPath& path1, const StdPath& path2);

    //GUIDから文字列への変換
    static StdString GuidToString(const GUID& guid);

    //特殊文字を表示できる形式に変換
    static StdString ConvAscii(StdChar* pChar);

    //引用符追加
    static StdString QuotString(const StdString& stdStr);

    //引用符削除
    static StdString StripQuoat(const StdString& stdStr);

    //浮動小数点値チェック
    static bool IsFloatVal(const StdString& strVal);

    //ウインドウメッセージ
    static CString ConvWinMessage(UINT msg);

    //!< 同符号なら真 
    static bool IsSameSign(double a, double b);

    //!< 正なら１ 負なら-1
    static double sgn2(double dVal);

    //!< 実数比較
    static bool IsEqual(double a, double b);

    //!< 2分法
    static bool  Bisection (std::vector <double>* plstRet, 
                 std::function<double (double, const void*) > func,
                 const void* pVoid, 
                 double dMin, double dMax, double dStep, int iMax, double dTolerance);

    //!< 区間内の最小値
    static double CalcMin( double dMin, double dMax, double * pX, 
                           std::function<double (double, const void*) > func, const void* pVoid, 
                           double dTolerance, int* pIterations, int iMaxCalc );

    //!< 2次方程式（実根のみ）
    static void QuadraticEquation(std::vector<double>* pList, double a, double b, double c);

    //!< 立方根
    static double Cuberoot(double x);  /* $\sqrt[3]{x}$ */

    //!< カルダノの公式（実根のみ）
    static void Cardano(std::vector<double>* pList, double a, double b, double c, double d);

    //!< 4次方程式（実根のみ）
    static void QuarticEquation(std::vector<double>* pList, double a, double b, double c, double d, double e);

    //!< クリッピング
    static bool Clip(POINT* pPt1, POINT* pPt2, RECT rc);
    static CLIP_STS Clip2(POINT* pPt1, POINT* pPt2, RECT rc);

    
protected:

    static  int     Cval3(POINT *P, RECT Rec);
    static  POINT   ClipLeft(RECT Rec, POINT* P1, POINT* P2);
    static  POINT   ClipRight(RECT Rec, POINT* P1, POINT* P2);
    static  POINT   ClipTop(RECT Rec, POINT* P1, POINT* P2);
    static  POINT   ClipBottom(RECT Rec, POINT* P1, POINT* P2);

public:
    static  StdString strVoid   ;
    static  StdString strInt    ;
    static  StdString strUint   ;
    static  StdString strLong   ;
    static  StdString strUlong  ;
    static  StdString strFloat  ;
    static  StdString strDouble ;
    static  StdString strBool   ;
    static  StdString strLstPt2d;
    static  StdString strColor  ;
    static  StdString strString ;
    static  StdString strPoint2D;
    static  StdString strRect2D ;
    static  StdString strLine2D ;
    static  StdString strCircle2D ;
    static  StdString strEllipse2D ;
    static  StdString strHyperbola2D ;
    static  StdString strParabola2D ;
    static  StdString strSpline2D ;
    static  StdString strMat2D ;
};
#endif // __CUTILITY_H__
