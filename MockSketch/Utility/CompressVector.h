/**
 * @brief			CompressVectorヘッダーファイル
 * @file		    CompressVector.h
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#if !defined (__COMPRESS_VECTOR_H__)
#define __COMPRESS_VECTOR_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "System/MOCK_ERROR.h"




template <class D>
class COMPRESS_VECTOR
{
protected:
    struct PairData
    {
        D       Data;
        int     iCnt;

    private:
        //==============================
        // ファイル保存
        //==============================
        friend class boost::serialization::access;  
        template<class Archive>
        void serialize(Archive& ar, const unsigned int version)
        {
            SERIALIZATION_INIT;
                SERIALIZATION_BOTH("Data"  , Data);
                SERIALIZATION_BOTH("Cnt"   , iCnt);
            MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
        }
    };


public:
    //* コンストラクター
    COMPRESS_VECTOR() 
    {
        ;
    }

    //* デストラクター
    ~COMPRESS_VECTOR()
    {
        ;
    }

    //!< ベクトル設定
    void SetVector(const std::vector<D>* pVec)
    {
        PairData pair;
        m_lstPair.clear();

        bool bFirst = true;
        foreach(D Data, *pVec)
        {
            if (bFirst)
            {
                pair.Data = Data;
                pair.iCnt = 0;
                bFirst = false;
                continue;
            }

            if (pair.Data == Data)
            {
                pair.iCnt++;
            }
            else
            {
                m_lstPair.push_back(pair);
                pair.Data = Data;
                pair.iCnt = 0;
           }
        }
        m_lstPair.push_back(pair);
    }

    //!< ベクトル設定
    void SetUniqueVector(const std::vector<std::unique_ptr<D>>* pVec)
    {
        PairData pair;
        m_lstPair.clear();

        bool bFirst = true;
        foreach(auto  Data, *pVec)
        {
            if (bFirst)
            {
                pair.Data = *Data;
                pair.iCnt = 0;
                bFirst = false;
                continue;
            }

            if (pair.Data == *Data)
            {
                pair.iCnt++;
            }
            else
            {
                m_lstPair.push_back(pair);
                pair.Data = *Data;
                pair.iCnt = 0;
            }
        }
        m_lstPair.push_back(pair);
    }

    void SetSharedVector(const std::vector<std::shared_ptr<D>>* pVec)
    {
        PairData pair;
        m_lstPair.clear();

        bool bFirst = true;
        foreach(auto Data, *pVec)
        {
            if (bFirst)
            {
                pair.Data = *Data;
                pair.iCnt = 0;
                bFirst = false;
                continue;
            }

            if (pair.Data == *Data)
            {
                pair.iCnt++;
            }
            else
            {
                m_lstPair.push_back(pair);
                pair.Data = *Data;
                pair.iCnt = 0;
            }
        }
        m_lstPair.push_back(pair);
    }

    //!< ベクトル取得
    void GetVector(std::vector<D>* pVec)
    {
        STD_ASSERT(pVec != NULL);

        pVec->clear();
        foreach(PairData pair, m_lstPair)
        {
            for(int iCnt = 0; iCnt <= pair.iCnt; iCnt++)
            {
                pVec->push_back(pair.Data);
            }
        }
     }

    void GetUniqueVector(std::vector<std::unique_ptr<D>>* pVec)
    {
        STD_ASSERT(pVec != NULL);

        pVec->clear();
        foreach(PairData pair, m_lstPair)
        {
            for(int iCnt = 0; iCnt <= pair.iCnt; iCnt++)
            {
                pVec->push_back(std::make_unique<D>(pair.Data));
            }
        }
    }

    void GetSharedVector(std::vector<std::shared_ptr<D>>* pVec)
    {
        STD_ASSERT(pVec != NULL);

        pVec->clear();
        foreach(PairData pair, m_lstPair)
        {
            for(int iCnt = 0; iCnt <= pair.iCnt; iCnt++)
            {
                pVec->push_back(std::make_shared<D>(pair.Data));
            }
        }
    }

protected:

    //!< オブジェクト
    std::vector<PairData>            m_lstPair;


private:

    //==============================
    // ファイル保存
    //==============================
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
            SERIALIZATION_BOTH("Pair"  , m_lstPair);
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }
};

#endif //__COMPRESS_VECTOR_H__