/**
 * @brief			CustomProperty実装ファイル
 * @file			CustomPropertys.cpp
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MockSketch.h"
#include "System/MOCK_ERROR.h"
#include "./CustomPropertys.h"
#include "./CheckComboBox.h"
#include "./CUtility.h"
#include "PointListDlg.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif


////////////////////////////////////////////////////////////////////////////////
// CCheckBoxProp class

CCheckBoxProp::CCheckBoxProp(const CString& strName, BOOL bCheck, LPCTSTR lpszDescr, DWORD dwData) :
	CMFCPropertyGridProperty(strName, COleVariant((long)bCheck), lpszDescr, dwData)
{
	m_rectCheck.SetRectEmpty();
}

void CCheckBoxProp::OnDrawName(CDC* pDC, CRect rect)
{
	m_rectCheck = rect;
	m_rectCheck.DeflateRect(1, 1);

	m_rectCheck.right = m_rectCheck.left + m_rectCheck.Height();

	rect.left = m_rectCheck.right + 1;

	CMFCPropertyGridProperty::OnDrawName(pDC, rect);

	OnDrawCheckBox(pDC, m_rectCheck, (m_varValue.boolVal));
}

void CCheckBoxProp::OnClickName(CPoint point)
{
	if (m_bEnabled && m_rectCheck.PtInRect(point))
	{
		m_varValue.boolVal = !(m_varValue.boolVal);
		m_pWndList->InvalidateRect(m_rectCheck);
	}
}

BOOL CCheckBoxProp::OnDblClk(CPoint point)
{
	if (m_bEnabled && m_rectCheck.PtInRect(point))
	{
		return TRUE;
	}

	m_varValue.boolVal = !(m_varValue.boolVal);
	m_pWndList->InvalidateRect(m_rectCheck);
	return TRUE;
}

void CCheckBoxProp::OnDrawCheckBox(CDC * pDC, CRect rect, BOOL bChecked)
{
	COLORREF clrTextOld = pDC->GetTextColor();

	CMFCVisualManager::GetInstance()->OnDrawCheckBox(pDC, rect, FALSE, bChecked, m_bEnabled);

	pDC->SetTextColor(clrTextOld);
}

BOOL CCheckBoxProp::PushChar(UINT nChar)
{
	if (nChar == VK_SPACE)
	{
		OnDblClk(CPoint(-1, -1));
	}

	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// CPropSliderCtrl

CPropSliderCtrl::CPropSliderCtrl(CSliderProp* pProp, COLORREF clrBack)
{
	m_clrBack = clrBack;
	m_brBackground.CreateSolidBrush(m_clrBack);
	m_pProp = pProp;
}

CPropSliderCtrl::~CPropSliderCtrl()
{
}

BEGIN_MESSAGE_MAP(CPropSliderCtrl, CSliderCtrl)
	//{{AFX_MSG_MAP(CPropSliderCtrl)
	ON_WM_CTLCOLOR_REFLECT()
	ON_WM_HSCROLL_REFLECT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPropSliderCtrl message handlers

HBRUSH CPropSliderCtrl::CtlColor(CDC* pDC, UINT /*nCtlColor*/)
{
	pDC->SetBkColor(m_clrBack);
	return m_brBackground;
}

void CPropSliderCtrl::HScroll(UINT /*nSBCode*/, UINT /*nPos*/)
{
	ASSERT_VALID(m_pProp);

	m_pProp->OnUpdateValue();
	m_pProp->Redraw();
}

////////////////////////////////////////////////////////////////////////////////
// CSliderProp class

CSliderProp::CSliderProp(const CString& strName, long nValue, LPCTSTR lpszDescr, DWORD dwData) :
	CMFCPropertyGridProperty(strName, nValue, lpszDescr, dwData)
{
}

CWnd* CSliderProp::CreateInPlaceEdit(CRect rectEdit, BOOL& bDefaultFormat)
{
	CPropSliderCtrl* pWndSlider = new CPropSliderCtrl(this, m_pWndList->GetBkColor());

	rectEdit.left += rectEdit.Height() + 5;

	pWndSlider->Create(WS_VISIBLE | WS_CHILD, rectEdit, m_pWndList, AFX_PROPLIST_ID_INPLACE);
	pWndSlider->SetPos(m_varValue.lVal);

	bDefaultFormat = TRUE;
	return pWndSlider;
}

BOOL CSliderProp::OnUpdateValue()
{
	ASSERT_VALID(this);
	ASSERT_VALID(m_pWndInPlace);
	ASSERT_VALID(m_pWndList);
	ASSERT(::IsWindow(m_pWndInPlace->GetSafeHwnd()));

	long lCurrValue = m_varValue.lVal;

	CSliderCtrl* pSlider = (CSliderCtrl*) m_pWndInPlace;

	m_varValue = (long) pSlider->GetPos();

	if (lCurrValue != m_varValue.lVal)
	{
		m_pWndList->OnPropertyChanged(this);
	}

	return TRUE;
}

////////////////////////////////////////////////////////////////////////////////
// CBoundedNumberPairProp class

CBoundedNumberPairProp::CBoundedNumberPairProp(const CString& strGroupName, int nMinValue1, int nMaxValue1, int nMinValue2, int nMaxValue2, DWORD_PTR dwData, BOOL bIsValueList) :
	CMFCPropertyGridProperty(strGroupName, dwData, bIsValueList)
{
	m_nMinValue1 = nMinValue1;
	m_nMaxValue1 = nMaxValue1;
	m_nMinValue2 = nMinValue2;
	m_nMaxValue2 = nMaxValue2;
}

BOOL CBoundedNumberPairProp::OnUpdateValue()
{
	ASSERT_VALID(this);
	ASSERT_VALID(m_pWndInPlace);
	ASSERT_VALID(m_pWndList);
	ASSERT(::IsWindow(m_pWndInPlace->GetSafeHwnd()));

	CString strText;
	m_pWndInPlace->GetWindowText(strText);

	BOOL bIsChanged = FormatProperty() != strText;

	if (bIsChanged)
	{
		CString strDelimeter(_T(","));

		for (int i = 0; !strText.IsEmpty() && i < GetSubItemsCount(); i++)
		{
			CString strItem = strText.SpanExcluding(strDelimeter);
			if (strItem.GetLength() + 1 > strText.GetLength())
			{
				strText.Empty();
			}
			else
			{
				strText = strText.Mid(strItem.GetLength() + 1);
			}
			strItem.TrimLeft();
			strItem.TrimRight();

			int nItem = _ttoi(strItem);
			if ((i == 0) && ((nItem < m_nMinValue1) || (nItem > m_nMaxValue1)))
			{
				static BOOL bRecursedHere = FALSE;
				if (bRecursedHere)
					return TRUE;
				bRecursedHere = TRUE;

				CString strMessage;
				strMessage.Format(_T("Height value must be between %d and %d."), m_nMinValue1, m_nMaxValue1);
				AfxMessageBox(strMessage);

				bRecursedHere = FALSE;
				return FALSE;
			}
			else if ((i == 1) && ((nItem < m_nMinValue2) || (nItem > m_nMaxValue2)))
			{
				static BOOL bRecursedHere = FALSE;
				if (bRecursedHere)
					return TRUE;
				bRecursedHere = TRUE;

				CString strMessage;
				strMessage.Format(_T("Width value must be between %d and %d."), m_nMinValue2, m_nMaxValue2);
				AfxMessageBox(strMessage);

				bRecursedHere = FALSE;
				return FALSE;
			}
		}

		return CMFCPropertyGridProperty::OnUpdateValue();
	}

	return TRUE;
}

////////////////////////////////////////////////////////////////////////////////
// CBoundedNumberSubProp class

CBoundedNumberSubProp::CBoundedNumberSubProp(const CString& strName, const COleVariant& varValue, int nMinValue, int nMaxValue, LPCTSTR lpszDescr) :
	CMFCPropertyGridProperty(strName, varValue, lpszDescr)
{
	m_nMinValue = nMinValue;
	m_nMaxValue = nMaxValue;
}

BOOL CBoundedNumberSubProp::OnUpdateValue()
{
	ASSERT_VALID(this);
	ASSERT_VALID(m_pWndInPlace);
	ASSERT_VALID(m_pWndList);
	ASSERT(::IsWindow(m_pWndInPlace->GetSafeHwnd()));

	BOOL bRet = TRUE;
	CString strText;
	m_pWndInPlace->GetWindowText(strText);

	BOOL bIsChanged = FormatProperty() != strText;
	if (bIsChanged)
	{
		int nItem = _ttoi(strText);
		if ((nItem < m_nMinValue) || (nItem > m_nMaxValue))
		{
			static BOOL bRecursedHere = FALSE;
			if (bRecursedHere)
				return TRUE;
			bRecursedHere = TRUE;

			CString strMessage;
			strMessage.Format(_T("Value must be between %d and %d."), m_nMinValue, m_nMaxValue);
			AfxMessageBox(strMessage);

			bRecursedHere = FALSE;
			return FALSE;
		}

		bRet = CMFCPropertyGridProperty::OnUpdateValue();

		if (m_pParent != NULL)
		{
			m_pWndList->OnPropertyChanged(m_pParent);
		}
	}

	return bRet;
}

////////////////////////////////////////////////////////////////////////////////
// CIconListProp class

const int nIconMargin = 3;

CIconListProp::CIconListProp(const CString& strName, const CImageList& imageListIcons, int nSelectedIcon, CStringList* plstIconNames, LPCTSTR lpszDescr, DWORD dwData) :
	CMFCPropertyGridProperty(strName, (long) nSelectedIcon, lpszDescr, dwData)
{
	m_imageListIcons.CreateFromImageList(imageListIcons);
	m_imageListIcons.SetTransparentColor(::GetSysColor(COLOR_3DFACE));

	if (plstIconNames != NULL)
	{
		m_lstIconNames.AddTail(plstIconNames);
		ASSERT(m_lstIconNames.GetCount() == m_imageListIcons.GetCount());
	}

	for (int i = 0; i < m_imageListIcons.GetCount(); i++)
	{
		CString strItem;
		strItem.Format(_T("%d"), i);

		AddOption(strItem);
	}

	AllowEdit(FALSE);
}

CComboBox* CIconListProp::CreateCombo(CWnd* pWndParent, CRect rect)
{
	rect.bottom = rect.top + 400;

	CIconCombo* pWndCombo = new CIconCombo(m_imageListIcons, m_lstIconNames);
	if (!pWndCombo->Create(WS_CHILD | CBS_NOINTEGRALHEIGHT | CBS_DROPDOWNLIST | WS_VSCROLL | CBS_OWNERDRAWFIXED | CBS_HASSTRINGS, rect, pWndParent, AFX_PROPLIST_ID_INPLACE))
	{
		delete pWndCombo;
		return NULL;
	}

	return pWndCombo;
}

void CIconListProp::OnDrawValue(CDC* pDC, CRect rect)
{
	ASSERT_VALID(m_pWndList);
	ASSERT_VALID(pDC);

	CString strVal = (LPCTSTR)(_bstr_t) m_varValue;
	if (strVal.IsEmpty() || m_imageListIcons.GetCount() == 0)
	{
		return;
	}

	int nIndex = _ttoi(strVal);

	if (nIndex < 0)
	{
		return;
	}

	COLORREF clrTextOld = pDC->GetTextColor();

	CFont* pOldFont = pDC->SelectObject(IsModified() && m_pWndList->IsMarkModifiedProperties() ? &m_pWndList->GetBoldFont() : m_pWndList->GetFont());

	CRect rectImage = rect;
	rectImage.right = rectImage.left + rectImage.Height();
	rectImage.DeflateRect(1, 1);

	CAfxDrawState ds;
	m_imageListIcons.PrepareDrawImage(ds, rectImage.Size());
	m_imageListIcons.Draw(pDC, rectImage.left, rectImage.top, nIndex);
	m_imageListIcons.EndDrawImage(ds);

	if (!m_lstIconNames.IsEmpty())
	{
		CString str = m_lstIconNames.GetAt(m_lstIconNames.FindIndex(nIndex));

		rect.left = rectImage.right + 2 * nIconMargin;

		pDC->DrawText(str, rect, DT_SINGLELINE | DT_VCENTER);
	}

	pDC->SetTextColor(clrTextOld);
	pDC->SelectObject(pOldFont);

	m_bValueIsTruncated = FALSE;
}

CWnd* CIconListProp::CreateInPlaceEdit(CRect rectEdit, BOOL& bDefaultFormat)
{
	CWnd* pWnd = CMFCPropertyGridProperty::CreateInPlaceEdit(rectEdit, bDefaultFormat);
	if (pWnd != NULL)
	{
		pWnd->ShowWindow(SW_HIDE);
	}

	return pWnd;
}

/////////////////////////////////////////////////////////////////////////////
// CIconCombo

CIconCombo::CIconCombo(CMFCToolBarImages& imageListIcons, CStringList& lstIconNames) :
	m_imageListIcons(imageListIcons), m_lstIconNames(lstIconNames)
{
}

CIconCombo::~CIconCombo()
{
}

BEGIN_MESSAGE_MAP(CIconCombo, CComboBox)
	//{{AFX_MSG_MAP(CIconCombo)
	ON_WM_DRAWITEM()
	ON_WM_MEASUREITEM()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIconCombo message handlers

void CIconCombo::OnDrawItem(int /*nIDCtl*/, LPDRAWITEMSTRUCT lpDIS)
{
	CDC* pDC = CDC::FromHandle(lpDIS->hDC);
	ASSERT_VALID(pDC);

	CRect rect = lpDIS->rcItem;
	int nIcon = lpDIS->itemID;

	HBRUSH brBackground;
	COLORREF clText;

	if (lpDIS->itemState & ODS_SELECTED)
	{
		brBackground = GetSysColorBrush(COLOR_HIGHLIGHT);
		clText = afxGlobalData.clrTextHilite;
	}
	else
	{
		brBackground = GetSysColorBrush(COLOR_WINDOW);
		clText = afxGlobalData.clrWindowText;
	}

	if (lpDIS->itemAction &(ODA_DRAWENTIRE | ODA_SELECT))
	{
		::FillRect(lpDIS->hDC, &rect, brBackground);
	}

	if (nIcon < 0)
	{
		return;
	}

	CAfxDrawState ds;
	m_imageListIcons.PrepareDrawImage(ds);
	m_imageListIcons.Draw(pDC, rect.left + nIconMargin, rect.top + nIconMargin, nIcon);
	m_imageListIcons.EndDrawImage(ds);

	if (!m_lstIconNames.IsEmpty())
	{
		CString str = m_lstIconNames.GetAt(m_lstIconNames.FindIndex(nIcon));
		CFont* pOldFont = pDC->SelectObject(&afxGlobalData.fontRegular);

		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextColor(clText);

		CRect rectText = rect;
		rectText.left += m_imageListIcons.GetImageSize().cx + 2 * nIconMargin;

		pDC->DrawText(str, rectText, DT_SINGLELINE | DT_VCENTER);

		pDC->SelectObject(pOldFont);
	}
}

void CIconCombo::OnMeasureItem(int /*nIDCtl*/, LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	int nTextWidth = 0;
	int nTextHeight = 0;

	if (!m_lstIconNames.IsEmpty())
	{
		nTextHeight = afxGlobalData.GetTextHeight();

		CClientDC dc(this);
		CFont* pOldFont = dc.SelectObject(&afxGlobalData.fontRegular);

		for (POSITION pos = m_lstIconNames.GetHeadPosition(); pos != NULL;)
		{
			CString str = m_lstIconNames.GetNext(pos);

			nTextWidth = max(nTextWidth, dc.GetTextExtent(str).cx + nIconMargin);
		}

		dc.SelectObject(pOldFont);
	}

	lpMeasureItemStruct->itemWidth = m_imageListIcons.GetImageSize().cx + nTextWidth + 2 * nIconMargin;
	lpMeasureItemStruct->itemHeight = max(nTextHeight, m_imageListIcons.GetImageSize().cy + 2 * nIconMargin);
}

////////////////////////////////////////////////////////////////////////////////
// CComboBoxExProp class

CComboBoxExProp::CComboBoxExProp(const CString& strName, const CString& strValue, LPCTSTR lpszDescr, DWORD dwData, CImageList* pImageList) :
	CMFCPropertyGridProperty(strName, (LPCTSTR) strValue, lpszDescr, dwData), m_pImageList(pImageList)
{
}

CComboBox* CComboBoxExProp::CreateCombo(CWnd* pWndParent, CRect rect)
{
	rect.bottom = rect.top + 400;

	CComboBoxEx* pWndCombo = new CComboBoxEx;

	if (!pWndCombo->Create(WS_CHILD | CBS_DROPDOWNLIST | WS_VSCROLL, rect, pWndParent, AFX_PROPLIST_ID_INPLACE))
	{
		delete pWndCombo;
		return NULL;
	}

	if (m_pImageList != NULL)
	{
		pWndCombo->SetImageList(m_pImageList);
	}

	return pWndCombo;
}

BOOL CComboBoxExProp::OnEdit(LPPOINT lptClick)
{
	if (!CMFCPropertyGridProperty::OnEdit(lptClick))
	{
		return FALSE;
	}

	CComboBoxEx* pWndComboEx = DYNAMIC_DOWNCAST(CComboBoxEx, m_pWndCombo);
	if (pWndComboEx == NULL)
	{
		ASSERT(FALSE);
		return FALSE;
	}

	pWndComboEx->ResetContent();

	int i = 0;

	COMBOBOXEXITEM item;
	memset(&item, 0, sizeof(item));

	item.mask = CBEIF_IMAGE | CBEIF_INDENT | CBEIF_SELECTEDIMAGE | CBEIF_TEXT;

	for (POSITION pos = m_lstOptions.GetHeadPosition(); pos != NULL; i++)
	{
		CString strItem = m_lstOptions.GetNext(pos);

		item.iItem = i;
		item.iSelectedImage = item.iImage = m_lstIcons [i];
		item.iIndent = m_lstIndents [i];
		item.pszText = (LPTSTR)(LPCTSTR) strItem;
		item.cchTextMax = strItem.GetLength();

		pWndComboEx->InsertItem(&item);
	}

	return TRUE;
}

BOOL CComboBoxExProp::AddOption(LPCTSTR lpszOption, int nIcon, int nIndent)
{
	if (!CMFCPropertyGridProperty::AddOption(lpszOption))
	{
		return FALSE;
	}

	m_lstIcons.Add(nIcon);
	m_lstIndents.Add(nIndent);

	return TRUE;
}

COwnerDrawDescrProp::COwnerDrawDescrProp(const CString& strName, const COleVariant& varValue) : CMFCPropertyGridProperty(strName, varValue)
{
}

void COwnerDrawDescrProp::OnDrawDescription(CDC* pDC, CRect rect)
{
	CDrawingManager dm(*pDC);
	dm.FillGradient2(rect, RGB(102, 200, 238), RGB(0, 129, 185), 45);

	CFont* pOldFont = pDC->SelectObject(&afxGlobalData.fontBold);

	CString strText = _T("Custom Text");

	pDC->SetTextColor(RGB(0, 65, 117));
	pDC->DrawText(strText, rect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);

	rect.OffsetRect(-2, -2);

	pDC->SetTextColor(RGB(155, 251, 255));
	pDC->DrawText(strText, rect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);

	pDC->SelectObject(pOldFont);
}

//---------------------------------------------------
CTwoButtonsProp::CTwoButtonsProp(const CString& strName,
	                             const CString& strValue, 
                            	LPCTSTR     lpszDescr,
								DWORD       dwData,
								std::function<void(bool)> func) :	
									CMFCPropertyGridProperty(strName,                   // グループの名前
									(LPCTSTR)strValue,         // プロパティ値
										lpszDescr,                 // プロパティの説明
										dwData)                   // アプリケーション固有のデータ (
{
	CBitmap bmp;
	bmp.LoadBitmap(IDB_BUTTONS);

	m_images.Create(14, 14, ILC_MASK | ILC_COLOR8, 0, 0);
	m_images.Add(&bmp, RGB(255, 0, 255));
	m_func = func;
}

void CTwoButtonsProp::AdjustButtonRect()
{
	CMFCPropertyGridProperty::AdjustButtonRect();
	m_rectButton.left -= m_rectButton.Width();
}

void CTwoButtonsProp::OnClickButton(CPoint point)
{
	BOOL bIsLeft = point.x < m_rectButton.CenterPoint().x;

	if (bIsLeft)
	{
		m_func(true);
	}
	else
	{
		m_func(false);
	}

}

void CTwoButtonsProp::OnDrawButton(CDC* pDC, CRect rectButton)
{
	for (int i = 0; i < 2; i++)
	{
		CMFCToolBarButton button;

		CRect rect = rectButton;

		if (i == 0)
		{
			rect.right = rect.left + rect.Width() / 2;
		}
		else
		{
			rect.left = rect.right - rect.Width() / 2;
		}

		CMFCVisualManager::AFX_BUTTON_STATE state = CMFCVisualManager::ButtonsIsHighlighted;

		CMFCVisualManager::GetInstance()->OnFillButtonInterior(pDC, &button, rect, state);

		m_images.Draw(pDC, i, CPoint(rect.left, rect.top), ILD_NORMAL);

		CMFCVisualManager::GetInstance()->OnDrawButtonBorder(pDC, &button, rect, state);
	}
}
//---------------------------------------------------


/////////////////////////////////////////////////////////////////////////////
// CCustomDlgProp

CCustomDlgProp::CCustomDlgProp(const CString& strName, 
                               const CString& strValue,
                               LPCTSTR     lpszDescr,
                               DWORD       dwData,
                               CDialog* pDlg) : 
	CMFCPropertyGridProperty(strName,                   // グループの名前
                             (LPCTSTR)strValue,         // プロパティ値
                             lpszDescr,                 // プロパティの説明
                             dwData)                   // アプリケーション固有のデータ (
{
    m_pDlg = pDlg;
}

void CCustomDlgProp::OnClickButton(CPoint /*point*/)
{
    INT_PTR iRet = IntptrToInt(m_pDlg->DoModal());
}


/////////////////////////////////////////////////////////////////////////////
// CPointListProp

CPointListProp::CPointListProp(const CString& strName, 
                               const std::vector<POINT2D>& varList,
                               LPCTSTR     lpszDescr, 
                               DWORD       dwData):
	CMFCPropertyGridProperty(strName,                   // グループの名前
                             (LPCTSTR) _T(""),          // プロパティ値
                             lpszDescr,                 // プロパティの説明
                             dwData),                   // アプリケーション固有のデータ (
                             m_lstData(varList)
{
}

void CPointListProp::OnClickButton(CPoint /*point*/)
{
    CPointListDlg dlgList;
    dlgList.m_lstData = m_lstData;

    if(dlgList.DoModal() == IDOK)
    {
        m_lstData = dlgList.m_lstData;
    }
}

/////////////////////////////////////////////////////////////////////////////
// CDropDownExProp

/**
 * @brief   コンストラクター
 * @param   [in] strName    項目名
 * @param   [in] iSel       選択位置
 * @param   [in] lpszDescr  項目説明
 * @param   [in] dwData     データ識別値
 * @param   [in] nIDResource    リソースID(使用しないときは０）
 * @param   [in] iDiv           リソース画像の分割数
 * @return	なし
 * @note
 */
CDropDownExProp::CDropDownExProp(const CString& strName, 
                                 int iSel, 
                                 LPCTSTR lpszDescr, 
                                 DWORD dwData, 
                                 UINT nIDResource,
                                 int iDiv):
	CMFCPropertyGridProperty(strName,                   // グループの名前
                             (LPCTSTR)_T(""),                  // プロパティ値
                             lpszDescr,                 // プロパティの説明
                             dwData),                   // アプリケーション固有のデータ (
                             m_pImageList(NULL),
                             m_bInerImageList(false),
                             m_nSel(iSel)
{
    AllowEdit(false);
    if (nIDResource != 0)
    {
        SetImage( nIDResource, iDiv);
    }
}

/**
 * @brief   デストラクター
 * @param   なし
 * @return	なし
 * @note	 
 */
CDropDownExProp::~CDropDownExProp()
{
    if (m_bInerImageList)
    {
        if (m_pImageList)
        {
            delete m_pImageList;
        }
    }
}

/**
 * @brief   イメージ設定
 * @param   [in]  nIDResource リソースID
 * @param   [in]  iDiv        分割数
 * @return	true 成功
 * @note	 
 */
bool CDropDownExProp::SetImage(UINT nIDResource, int iDiv)
{
    if (m_pImageList != NULL)
    {
        return false;
    }
    

    CBitmap bmp;
    if (!bmp.LoadBitmap(nIDResource))
    {
        return false;
    }
    BITMAP bitMap;

    if (iDiv < 1)
    {
        return false;
    }
    
    if(!bmp.GetBitmap(&bitMap))
    {
        return false;
    }
    int iCx = bitMap.bmWidth / iDiv;
    int iCy = bitMap.bmHeight;

    
    m_pImageList = new CImageList;
    if(!m_pImageList->Create(nIDResource, iCx, 1, RGB( 0, 128, 128)))
    //if(!m_pImageList->Create(iCx, iCy, ILC_COLOR, 0, 0))
    {
        delete m_pImageList;
        m_pImageList = NULL;
        return false;
    }

    /*
    if(!m_pImageList->Add(&bmp, RGB( 0, 128, 128)))
    {
        delete m_pImageList;
        return false;
    }
    */

    m_bInerImageList = true;
    return true;
}

/**
 * @brief   イメージ設定
 * @param   [in]  pImageList  イメージリスト
 * @return  true 成功
 * @note    外部のイメージリストを使用する
 */
bool CDropDownExProp::SetImage(CImageList* pImageList)
{
    if (m_pImageList != NULL)
    {
        return false;
    }
    m_pImageList = pImageList;
    return true;
}

/**
 * @brief   選択位置取得
 * @param   なし
 * @return  ドロップダウンリスト選択位置
 * @note    
 */
int  CDropDownExProp::GetCurSel() const
{
    int nSel = CB_ERR;
    if (m_pWndCombo)
    {
        nSel = m_pWndCombo->GetCurSel();
    }
    return nSel;

}

/**
 * @brief   選択位置設定
 * @param   [in] iSel 選択位置
 * @return  
 * @note    
 */
void  CDropDownExProp::SetCurSel(int iSel)
{
    Select(iSel);
}


/**
 * @brief   ドロップダウン内位置取得
 * @param   [in] iId 選択Id
 * @return  ドロップダウンリスト内の位置 
 * @note    
 */
int  CDropDownExProp::GetSelById(int iId)
{
    int iSel;
    bool bRet;

    StdString strSel;
    bRet = FindStr(&strSel, iId);

    STD_ASSERT(bRet);

    if(!bRet)
    {
        return -1;
    }

    int i = 0;
    for (POSITION pos = m_lstOptions.GetHeadPosition(); pos != NULL; i++)
    {
        CString strItem = m_lstOptions.GetNext(pos);
 
        if (strSel == (LPCTSTR)strItem)
        {
            iSel = i;
            return iSel;
        }
    }
    return -1;
}

/**
 * @brief   Idによる選択位置設定
 * @param   [in] iId 選択Id
 * @return  
 * @note    AddOptionIdで初期値を設定している必要がある
 */
void  CDropDownExProp::SetCurSelById(int iId)
{
    int iSel;
    iSel = GetSelById(iId);

    if (m_pWndCombo)
    {
        Select(iSel);
    }
    else
    {
        StdString strItem;
        FindStr(&strItem, iId);
        SetStr(strItem.c_str());
        COleVariant TmpVal(strItem.c_str());
        SetValue(TmpVal);
        m_nSel = iSel;
    }

}
 int  CDropDownExProp::GetCurId() const
 {
    int iSel = GetCurSel();
    CString strName;



    if (iSel == -1)
    {
        return false;
    }

    CString strItem;
    m_pWndCombo->GetLBText(iSel, strItem);

    /*
    int iCnt = 0;
    bool bFind = false;
	for (POSITION pos = m_lstOptions.GetHeadPosition(); pos != NULL;)
	{
        strName = m_lstOptions.GetNext(pos);
        if (strItem == strName)
        {
            bFind = true; 
            break;
        }
        iCnt++;
	}

    if (!bFind)
    {
        return -1;
    }
    */

    int iId;
    bool bRet;
    bRet =  FindId(&iId, (LPCTSTR) strItem);

    if (bRet)
    {
        return iId;
    }
    
    return -1;
 }

/**
 * @brief   Id取得
 * @param   [out] pId 選択位置
 * @param   [in]  iSel 選択位置
 * @return  
 * @note    AddOptionIdで初期値を設定している必要がある
 */
bool CDropDownExProp::FindId(int* pId, StdString strItem) const
{
    int iRet = -1;
    std::vector< boost::tuple<StdString, int> > ::const_iterator ite;
  
    //=========================
    // ラムダ式
    ite = boost::find_if( m_lstValue, [=](boost::tuple<StdString, int> tpl)
    {
        if(tpl.get<0>() == strItem)
        {
            return true;
        }
        return false;
    });
    //=========================

    if (ite != m_lstValue.end())
    {
        *pId = ite->get<1>();
        return true;
    }

    return false;
}

bool CDropDownExProp::FindStr(StdString* pStrItem, int iId) const
{
    std::vector< boost::tuple<StdString, int> > ::const_iterator ite;
  
    //=========================
    // ラムダ式
    ite = boost::find_if( m_lstValue, [=](boost::tuple<StdString, int> tpl)
    {
        if(tpl.get<1>() == iId)
        {
            return true;
        }
        return false;
    });
    //=========================

    if (ite != m_lstValue.end())
    {
        *pStrItem = ite->get<0>().c_str();
        return true;
    }
    return false;
}


/**
 * @brief   選択位置設定
 * @param   [in] iSel 選択位置
 * @return  
 * @note    
 */
void  CDropDownExProp::Select(int iSel)
{
    if (m_pWndCombo)
    {
        CString strItem;
        m_pWndCombo->GetLBText(iSel, strItem);
        SetStr(strItem);
    }
    m_nSel = iSel;
}

/**
 * @brief   値設定
 * @param   [in] varValue
 * @return  なし
 * @note    オーバライド
 */
void CDropDownExProp::SetValue(const COleVariant& varValue)
{
    CMFCPropertyGridProperty::SetValue(varValue);
    STD_ASSERT(varValue.vt == VT_BSTR);
    const CString strVal = varValue;
    SetStr(strVal);
}

/**
 * @brief   値設定
 * @param   [in] varValue
 * @return  なし
 * @note    オーバライド
 */
void CDropDownExProp::SetStr(const CString strVal)
{
    int iSel = 0;
    if (m_pWndCombo)
    {
        iSel = m_pWndCombo->FindStringExact(-1, strVal);
        if (iSel != CB_ERR)
        {
            m_pWndCombo->SetCurSel(iSel);
            m_nSel = iSel;
        }
    }
    else
    {
        CString strItem;
	    for (POSITION pos = m_lstOptions.GetHeadPosition();
             pos != NULL; 
             iSel++)
	    {
		    strItem = m_lstOptions.GetNext(pos);
            if (strItem == strVal)
            {
                m_nSel = iSel;
                break;
            }
	    }
    }
}


/**
 * @brief   コンボボックス作成
 * @param   [in] pWndParent
 * @param   [in] rect
 * @return  コンボボックス
 * @note    内部呼び出し
 */
CComboBox* CDropDownExProp::CreateCombo(CWnd* pWndParent, CRect rect)
{
    rect.bottom = rect.top + 400;

    DWORD  dwStyle = WS_CHILD | CBS_DROPDOWNLIST | WS_VSCROLL;

    CComboBoxEx* pWndCombo = new CComboBoxEx;

    if (!pWndCombo->Create(dwStyle, rect, pWndParent, AFX_PROPLIST_ID_INPLACE))
    {
        delete pWndCombo;
        return NULL;
    }

    if (m_pImageList != NULL)
    {
        pWndCombo->SetImageList(m_pImageList);
    }
    return pWndCombo;
}


/**
 * @brief   コンボボックス変更
 * @param   なし
 * @return  なし
 * @note    
 */
void CDropDownExProp::OnSelectCombo()
{
    CMFCPropertyGridProperty::OnSelectCombo();
    if (m_pWndCombo)
    {
        int nSel = m_pWndCombo->GetCurSel();
        Select(nSel);
    }
}

/**
 * @brief   編集開始
 * @param   [in] lptClick クリック位置
 * @return  TRUE 編集成功
 * @note    
 */
BOOL CDropDownExProp::OnEdit(LPPOINT lptClick)
{
	if (!CMFCPropertyGridProperty::OnEdit(lptClick))
	{
		return FALSE;
	}

	CComboBoxEx* pWndComboEx = DYNAMIC_DOWNCAST(CComboBoxEx, m_pWndCombo);
	if (pWndComboEx == NULL)
	{
		return FALSE;
	}

	pWndComboEx->ResetContent();


	COMBOBOXEXITEM item;
	memset(&item, 0, sizeof(item));

    if (m_lstIcons.empty())
    {
	    item.mask = CBEIF_TEXT;
    }
    else
    {
        item.mask = CBEIF_TEXT | CBEIF_IMAGE | CBEIF_SELECTEDIMAGE;
    }

    int i = IntptrToInt(m_lstOptions.GetCount()) - 1;
    int iItem = 0;
    int iImage;
    int iId;

    for (POSITION pos = m_lstOptions.GetTailPosition(); pos != NULL; i--)
	{
		CString strItem = m_lstOptions.GetPrev(pos);

		item.iItem = -1;
        if (!m_lstIcons.empty())
        {
            iId = m_lstIcons [i].get<0>();
            iImage  = m_lstIcons [i].get<1>();

		    item.iSelectedImage = item.iImage = iImage;
		    item.lParam = iId;
        }

		item.pszText = (LPTSTR)(LPCTSTR) strItem;
		item.cchTextMax = strItem.GetLength();

        int iRet;
		iRet = pWndComboEx->InsertItem(&item);
        iItem++;
        STD_ASSERT(iRet != -1);
	}
	return TRUE;
}

/**
 * @brief   アイテム追加
 * @param   [in] lpszOption アイテム名称
 * @param   [in] nIcon      アイコン番号
 * @param   [in] nID        Id
 * @return  TRUE 編集成功
 * @note    
 */
BOOL CDropDownExProp::AddOption(LPCTSTR lpszOption, int nIcon, int nId)
{
    if (!CMFCPropertyGridProperty::AddOption(lpszOption))
    {
        return FALSE;
    }

    if (m_nSel == (int)m_lstIcons.size())
    {
        SetValue(lpszOption);
    }
    m_lstIcons.push_back( boost::make_tuple(nId, nIcon) );
    m_lstValue.push_back( boost::make_tuple(StdString(lpszOption) , nId) );
	return TRUE;
}

/**
 * @brief   アイテム,ID追加
 * @param   [in] lpszOption アイテム名称
 * @param   [in] nId        ID
 * @return  TRUE 編集成功
 * @note    
 */
BOOL CDropDownExProp::AddOptionId(LPCTSTR lpszOption, int nId)
{
    if (!CMFCPropertyGridProperty::AddOption(lpszOption))
    {
        return FALSE;
    }

    //m_lstOptionsの末尾に追加

    if (m_nSel == (int)m_lstValue.size())
    {
        SetValue(lpszOption);
    }

    m_lstValue.push_back( boost::make_tuple(StdString(lpszOption) , nId) );

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CDropDownExProp

/**
 * @brief   コンストラクター
 * @param   [in] strName    項目名
 * @param   [in] iSel       項目名値
 * @param   [in] lpszDescr  項目説明
 * @param   [in] dwData     データ識別値
 * @return	なし
 * @note	 
 */
CDropBoolProp::CDropBoolProp(const CString& strName, 
                                 bool    bBool, 
                                 LPCTSTR lpszDescr, 
                                 DWORD dwData):
	CMFCPropertyGridProperty(strName,                   // グループの名前
                             (LPCTSTR) _T(""),          // プロパティ値
                             lpszDescr,                 // プロパティの説明
                             dwData),                   // アプリケーション固有のデータ (
                             m_bSel(bBool)
{
    AllowEdit(false);
    AddOption(_T("TRUE"));
    AddOption(_T("FALSE"));
    SetValue(bBool ? _T("TRUE"):_T("FALSE"));

}

/**
 * @brief   コンボボックス作成
 * @param   [in] pWndParent
 * @param   [in] rect
 * @return  コンボボックス
 * @note    内部呼び出し
 */
CComboBox* CDropBoolProp::CreateCombo(CWnd* pWndParent, CRect rect)
{
	rect.bottom = rect.top + 400;

	CComboBox* pWndCombo = new CComboBox;

	if (!pWndCombo->Create(WS_CHILD | 
        CBS_DROPDOWNLIST |
        WS_VSCROLL, rect, pWndParent, AFX_PROPLIST_ID_INPLACE))
	{
		delete pWndCombo;
		return NULL;
	}

	return pWndCombo;
}

/**
 * @brief   編集開始
 * @param   [in] lptClick クリック位置
 * @return  TRUE 編集成功
 * @note    
 */
BOOL CDropBoolProp::OnEdit(LPPOINT lptClick)
{
	if (!CMFCPropertyGridProperty::OnEdit(lptClick))
	{
		return FALSE;
	}

	if (m_pWndCombo == NULL)
	{
		ASSERT(FALSE);
		return FALSE;
	}

	m_pWndCombo->ResetContent();


	for (POSITION pos = m_lstOptions.GetHeadPosition(); pos != NULL;)
	{
		CString strItem = m_lstOptions.GetNext(pos);
		m_pWndCombo->AddString( (LPTSTR)(LPCTSTR) strItem );
	}

    m_pWndCombo->SetCurSel(m_bSel? 0:1);

	return TRUE;
}


/**
 * @brief   コンボボックス変更
 * @param   なし
 * @return  なし
 * @note    
 */
void CDropBoolProp::OnSelectCombo()
{
    CMFCPropertyGridProperty::OnSelectCombo();
    if (m_pWndCombo)
    {
        if(m_pWndCombo->GetCurSel() == 0)
        {
            m_bSel = true;
        }
        else
        {
            m_bSel = false;
        }
    }
}

/**
 * @brief   設定値取得
 * @param   なし
 * @return  設定値
 * @note    
 */
bool  CDropBoolProp::GetCurSel()
{
    return m_bSel;

}

/**
 * @brief   設定
 * @param   [in] bVal 設定値
 * @return  
 * @note    
 */
void  CDropBoolProp::SetBool(bool bVal)
{
    m_bSel = bVal;
    SetValue(m_bSel ? _T("TRUE"):_T("FALSE"));
}




////////////////////////////////////////////////////////////////////////////////
// CDropCheckList class

CDropCheckList::CDropCheckList(const CString& strName, const CString& strValue, LPCTSTR lpszDescr, DWORD dwData) :
	CMFCPropertyGridProperty(strName, (LPCTSTR) strValue, lpszDescr, dwData)
{
}

CComboBox* CDropCheckList::CreateCombo(CWnd* pWndParent, CRect rect)
{
	rect.bottom = rect.top + 400;

	CCheckComboBox* pWndCombo = new CCheckComboBox;

	if (!pWndCombo->Create(WS_CHILD | CBS_DROPDOWNLIST | WS_VSCROLL,
        rect, pWndParent, AFX_PROPLIST_ID_INPLACE))
	{
		delete pWndCombo;
		return NULL;
	}

    pWndCombo->SetOnChckCallback(CallbackOnCeck, this);

  	return pWndCombo;
}


void CDropCheckList::CallbackOnCeck(void* pClass)
{
	CDropCheckList* pCheckList = reinterpret_cast<CDropCheckList*>(pClass);
    pCheckList->OnUpdateValue();

}

BOOL CDropCheckList::OnEdit(LPPOINT lptClick)
{
	if (!CMFCPropertyGridProperty::OnEdit(lptClick))
	{
		return FALSE;
	}

	CCheckComboBox* pCheckCombo = dynamic_cast<CCheckComboBox*>(m_pWndCombo);
	if (pCheckCombo == NULL)
	{
		return FALSE;
	}

	pCheckCombo->ResetContent();
    int iRet;
    for(auto ite = m_lstCheck.begin();
             ite != m_lstCheck.end();
             ite++)
    {
    	iRet = pCheckCombo->AddString(ite->first.c_str());
        if (iRet != CB_ERR)
        {
            pCheckCombo->SetCheck(iRet, ite->second);
        }
    }
    OnUpdateValue();
	return TRUE;
}

BOOL CDropCheckList::AddOption(LPCTSTR lpszOption, bool nbCheck)
{
	if (!CMFCPropertyGridProperty::AddOption(lpszOption))
	{
		return FALSE;
	}

    StdString strName = lpszOption;
    strName = CUtil::Trim(strName);
    m_lstCheck[strName] = nbCheck;

	return TRUE;
}

void CDropCheckList::RecalcCheck(LPCTSTR str)
{
    std::vector<StdString> lstStr;
    CUtil::TokenizeCsv(&lstStr, str);

    for(auto ite = m_lstCheck.begin();
        ite != m_lstCheck.end();
        ite++)
    {
        bool bFind = false;
        foreach(StdString strItem, lstStr)
        {
            strItem = CUtil::Trim(strItem);
            if (ite->first == strItem)
            {
                bFind = true;
                break;
            }
        }
        ite->second = bFind;
    }
}

BOOL CDropCheckList::OnEndEdit()
{
    CCheckComboBox* pCheckList;
    pCheckList = dynamic_cast<CCheckComboBox*>(m_pWndCombo);
    if (pCheckList)
    {
        CString str;
        pCheckList->GetWindowText(str);
        RecalcCheck(str);
    }
    return CMFCPropertyGridProperty::OnEndEdit();
}

/**
 * @brief   値更新
 * @return	
 * @note	 
 */
BOOL CDropCheckList::OnUpdateValue()
{
	ASSERT_VALID(this);
    CCheckComboBox* pCheckList;
    pCheckList = dynamic_cast<CCheckComboBox*>(m_pWndCombo);
    if (pCheckList)
    {
        CString str;
        pCheckList->GetWindowText(str);
        m_pWndInPlace->SetWindowText(str);

        CMFCPropertyGridProperty::OnUpdateValue();


    	return TRUE;
    }
    return false;
}

///////////////////////////////////////////////////////////////////////////
// CColorProp
//IMPLEMENT_DYNAMIC(CColorProp, CMFCPropertyGridProperty)

/**
 * @brief   コンストラクター
 * @param   [in] strName    項目名
 * @param   [in] color      色
 * @param   [in] pPalette   パレット
 * @param   [in] lpszDescr  項目説明
 * @param   [in] dwData     データ識別値
 * @param   [in] nIDResource    リソースID(使用しないときは０）
 * @param   [in] iDiv           リソース画像の分割数
 * @return	なし
 * @note	 
 */
/*
 CColorProp::CColorProp(const CString&  strName, 
                        const COLORREF& color, 
                        CPalette*       pPalette, 
                        LPCTSTR         lpszDescr, 
                        DWORD_PTR       dwData) :
CMFCPropertyGridProperty(strName, COleVariant(), lpszDescr, dwData), m_Color(color), m_ColorOrig(color)
{
	//CMFCColorBar::InitColors(pPalette, m_Colors);

	m_varValue = (LONG) color;
	m_varValueOrig = (LONG) color;

    //
    // #define AFX_PROP_HAS_LIST 0x0001
    // #define AFX_PROP_HAS_BUTTON 0x0002
    // #define AFX_PROP_HAS_SPIN 0x0004

	m_dwFlags = 0x0001;

	m_pPopup = NULL;
	m_bStdColorDlg = FALSE;
	m_ColorAutomatic = RGB(0, 0, 0);
	m_nColumnsNumber = 5;
}
*/

/**
 * @brief    デストラクター
 * @param   なし
 * @return	なし
 * @note	 
 */
/*
CColorProp::~CColorProp()
{
}
*/
/**
 * @brief    値描画
 * @param   [in] pDC  デバイスコンテキスト
 * @param   [in] rect 描画領域
 * @return	なし
 * @note	 
 */
/*
void CColorProp::OnDrawValue(CDC* pDC, CRect rect)
{
	CRect rectColor = rect;

	rect.left += rect.Height();
	CMFCPropertyGridProperty::OnDrawValue(pDC, rect);

	rectColor.right = rectColor.left + rectColor.Height();
	rectColor.DeflateRect(1, 1);
	rectColor.top++;
	rectColor.left++;

	CBrush br(m_Color == (COLORREF)-1 ? m_ColorAutomatic : m_Color);
	pDC->FillRect(rectColor, &br);
	pDC->Draw3dRect(rectColor, 0, 0);
}
*/

/**
 * @brief    ボタン押下
 * @param   [in] point クリック位置
 * @return	なし
 * @note	 
 */
/*
void CColorProp::OnClickButton(CPoint point)
{
	ASSERT_VALID(this);
	ASSERT_VALID(m_pWndList);

	m_bButtonIsDown = TRUE;
	Redraw();

	CList<COLORREF,COLORREF> lstDocColors;
	m_pPopup = new CMFCColorPopupMenu(NULL, m_Colors, m_Color, NULL, NULL, NULL, lstDocColors, m_nColumnsNumber, m_ColorAutomatic);
	m_pPopup->SetPropList(m_pWndList);

	if (!m_strOtherColor.IsEmpty()) // Other color button
	{
		m_pPopup->m_wndColorBar.EnableOtherButton(m_strOtherColor, !m_bStdColorDlg);
	}

	if (!m_strAutoColor.IsEmpty()) // Automatic color button
	{
		m_pPopup->m_wndColorBar.EnableAutomaticButton(m_strAutoColor, m_ColorAutomatic);
	}

	CPoint pt(m_pWndList->m_rectList.left + m_pWndList->m_nLeftColumnWidth + 1, m_rectButton.bottom + 1);
	m_pWndList->ClientToScreen(&pt);

	if (!m_pPopup->Create(m_pWndList, pt.x, pt.y, NULL, FALSE))
	{
		ASSERT(FALSE);
		m_pPopup = NULL;
	}
	else
	{
		m_pPopup->GetMenuBar()->SetFocus();
	}
}
*/
/**
 * @brief    編集
 * @param   [in] point クリック位置
 * @return	なし
 * @note	 
 */
/*
BOOL CColorProp::OnEdit(LPPOINT lptClick)
{
	ASSERT_VALID(this);
	ASSERT_VALID(m_pWndList);

	m_pWndInPlace = NULL;

	CRect rectEdit;
	CRect rectSpin;

	AdjustInPlaceEditRect(rectEdit, rectSpin);

	CMFCMaskedEdit* pWndEdit = new CMFCMaskedEdit;
	DWORD dwStyle = WS_VISIBLE | WS_CHILD;

	if (!m_bEnabled)
	{
		dwStyle |= ES_READONLY;
	}

	pWndEdit->SetValidChars(_T("01234567890ABCDEFabcdef"));

	pWndEdit->Create(dwStyle, rectEdit, m_pWndList, AFX_PROPLIST_ID_INPLACE);


    m_pWndInPlace = pWndEdit;
	m_pWndInPlace->SetWindowText(FormatProperty());
	//m_pWndInPlace->SetFont(IsModified() && m_pWndList->m_bMarkModifiedProperties ? &m_pWndList->m_fontBold : m_pWndList->GetFont());
	m_pWndInPlace->SetFocus();

	m_bInPlaceEdit = TRUE;
	return TRUE;
}

/**
 * @brief    編集領域調整
 * @param   [in] rectEdit エディット領域
 * @param   [in] rectSpin スピン領域
 * @return	なし
 * @note	 
 */
/*
void CColorProp::AdjustInPlaceEditRect(CRect& rectEdit, CRect& rectSpin)
{
	ASSERT_VALID(this);
	ASSERT_VALID(m_pWndList);

	rectSpin.SetRectEmpty();

	rectEdit = m_Rect;
	rectEdit.DeflateRect(0, 2);

	//int nMargin = m_pWndList->m_bMarkModifiedProperties && m_bIsModified ? m_pWndList->m_nBoldEditLeftMargin : m_pWndList->m_nEditLeftMargin;
	int nMargin =   0; 

	rectEdit.left = m_pWndList->m_rectList.left + 
                    m_pWndList->m_nLeftColumnWidth + 
                    m_Rect.Height() + AFX_TEXT_MARGIN - nMargin + 1;

	AdjustButtonRect();
	rectEdit.right = m_rectButton.left;
}
*/
/**
 * @brief   元の値に戻す
 * @param   なし
 * @return	なし
 * @note	 
 */
/*
void CColorProp::ResetOriginalValue()
{
	CMFCPropertyGridProperty::ResetOriginalValue();
	m_Color = m_ColorOrig;
}
*/

/**
 * @brief   書式
 * @param   
 * @return	
 * @note	 
 */
/*
CString CColorProp::FormatProperty()
{
	ASSERT_VALID(this);

	CString str;
	str.Format(_T("%02x%02x%02x"), GetRValue(m_Color), GetGValue(m_Color), GetBValue(m_Color));

	return str;
}
*/

/**
 * @brief   色設定
 * @param   [in] color 設定色
 * @return	
 * @note	 
 */
/*
void CColorProp::SetColor(COLORREF color)
{
	ASSERT_VALID(this);

	m_Color = color;
	m_varValue = (LONG) color;

	if (::IsWindow(m_pWndList->GetSafeHwnd()))
	{
		CRect rect = m_Rect;
		rect.DeflateRect(0, 1);

		m_pWndList->InvalidateRect(rect);
		m_pWndList->UpdateWindow();
	}

	if (m_pWndInPlace != NULL)
	{
		ASSERT_VALID(m_pWndInPlace);
		m_pWndInPlace->SetWindowText(FormatProperty());
	}
}
*/

/**
 * @brief   行数設定
 * @param   [in] nColumnsNumber 行数
 * @return	
 * @note	 
 */
/*
void CColorProp::SetColumnsNumber(int nColumnsNumber)
{
	ASSERT_VALID(this);
	ASSERT(nColumnsNumber > 0);

	m_nColumnsNumber = nColumnsNumber;
}
*/

/**
 * @brief   自動設定ボタン
 * @param   [in] lpszLabel      ラベル色
 * @param   [in] colorAutomatic 色
 * @param   [in] bEnable        有効
 * @return	
 * @note	 
 */
/*
void CColorProp::EnableAutomaticButton(LPCTSTR lpszLabel, COLORREF colorAutomatic, BOOL bEnable)
{
	ASSERT_VALID(this);

	m_ColorAutomatic = colorAutomatic;
	m_strAutoColor = (!bEnable || lpszLabel == NULL) ? _T("") : lpszLabel;
}
*/

/**
 * @brief   設定ボタン
 * @param   [in] lpszLabel      ラベル色
 * @param   [in] bAltColorDlg   色
 * @param   [in] bEnable        有効
 * @return	
 * @note	 
 */
/*
void CColorProp::EnableOtherButton(LPCTSTR lpszLabel, BOOL bAltColorDlg, BOOL bEnable)
{
	ASSERT_VALID(this);

	m_bStdColorDlg = !bAltColorDlg;
	m_strOtherColor = (!bEnable || lpszLabel == NULL) ? _T("") : lpszLabel;
}
*/

/**
 * @brief   値更新
 * @return	
 * @note	 
 */
/*
BOOL CColorProp::OnUpdateValue()
{
	ASSERT_VALID(this);

	if (m_pWndInPlace == NULL)
	{
		return FALSE;
	}

	ASSERT_VALID(m_pWndInPlace);
	ASSERT(::IsWindow(m_pWndInPlace->GetSafeHwnd()));

	CString strText;
	m_pWndInPlace->GetWindowText(strText);

	COLORREF colorCurr = m_Color;
	int nR = 0, nG = 0, nB = 0;
	_stscanf_s(strText, _T("%2x%2x%2x"), &nR, &nG, &nB);
	m_Color = RGB(nR, nG, nB);

	if (colorCurr != m_Color)
	{
		m_pWndList->OnPropertyChanged(this);
	}

	return TRUE;
}
*/

////////////////////////////////////////////////////////////////////////////
// CMFCPropertyGridFileProperty object

IMPLEMENT_DYNAMIC(CFileProperty, CMFCPropertyGridProperty)

CFileProperty::CFileProperty(const CString& strName, 
                             const CString& strFolderName, 
                             DWORD_PTR dwData, 
                             LPCTSTR lpszDescr) :
CMFCPropertyGridProperty(strName, 
                         COleVariant((LPCTSTR)strFolderName), 
                         lpszDescr, dwData), m_bIsFolder(TRUE)
{
	m_dwFlags = 0x0002;
}

CFileProperty::CFileProperty( const CString& strName,          // グループの名前
                               BOOL bOpenFileDialog,           // TRUE ファイルを開く
                               const CString& strFileName,     //ファイル名
                               const CString & strFolderName,  //フォルダー名
                               LPCTSTR lpszDefExt,             //規定のファイル名の拡張子
                               DWORD dwFileFlags,              // OFN 列挙体フラグ OFN_HIDEREADONLY とか
                               LPCTSTR lpszFilter,             //フィルター  例→"JPEG Files (*.jpg;*.jpeg)|*.jpg; *.jpeg||"
                               LPCTSTR lpszDescr,              // プロパティの説明
                               DWORD_PTR dwData) :             // アプリケーション固有のデータ 

CMFCPropertyGridProperty(strName,                            // グループの名前
                        COleVariant((LPCTSTR)strFileName),   // プロパティ値
                        lpszDescr,                           // プロパティの説明
                        dwData),                              
                        m_bOpenFileDialog(bOpenFileDialog), 
                        m_dwFileOpenFlags(dwFileFlags)
{
	m_dwFlags = 0x0002;
	m_strFolder = strFolderName;
	m_strDefExt = lpszDefExt == NULL ? _T("") : lpszDefExt;
	m_strFilter = lpszFilter == NULL ? _T("") : lpszFilter;

	m_bIsFolder  = FALSE;
}

CFileProperty::~CFileProperty()
{
}

void CFileProperty::OnClickButton(CPoint /*point*/)
{
	ASSERT_VALID(this);
	ASSERT_VALID(m_pWndList);
	ASSERT_VALID(m_pWndInPlace);
	ASSERT(::IsWindow(m_pWndInPlace->GetSafeHwnd()));

	m_bButtonIsDown = TRUE;
	Redraw();

	CString strPath = m_varValue;//.bstrVal;
	strPath.Replace(_T("/"),_T("\\"));
	BOOL bUpdate = FALSE;

	if (m_bIsFolder)
	{
		if (afxShellManager == NULL)
		{
			CWinAppEx* pApp = DYNAMIC_DOWNCAST(CWinAppEx, AfxGetApp());
			if (pApp != NULL)
			{
				pApp->InitShellManager();
			}
		}

		if (afxShellManager == NULL)
		{
			ASSERT(FALSE);
		}
		else
		{
			bUpdate = afxShellManager->BrowseForFolder(strPath, m_pWndList, strPath);
		}
	}
	else
	{
		TCHAR buffer[MAX_PACKAGE_NAME];
		DWORD ret = ::GetCurrentDirectory(MAX_PACKAGE_NAME, buffer);

		if (ret == 0)
		{
			buffer[0] = 0;
			STD_DBG(CUtil::GetErrMsg().c_str());
		}


		CFileDialog dlg(m_bOpenFileDialog, 
                        m_strDefExt, 
                        strPath, 
                        m_dwFileOpenFlags, 
                        m_strFilter, 
                        m_pWndList);
		if (dlg.DoModal() == IDOK)
		{
			bUpdate = TRUE;
			CString path = dlg.GetPathName();
			path.Replace(_T("\\"),_T("/"));

			int work_space_size = m_strFolder.GetLength()+1;
			int path_size = path.GetLength();

			strPath = "";
			for(int i=work_space_size;i<path_size;i++)
			{
				strPath += path[i];
			}
		}
		
//		CString work_space = ParticleEditor::instance().
		::SetCurrentDirectory(buffer);
	}

	if (bUpdate)
	{
		if (m_pWndInPlace != NULL)
		{
			m_pWndInPlace->SetWindowText(strPath);
		}

		m_varValue = (LPCTSTR) strPath;
	}

	m_bButtonIsDown = FALSE;
	Redraw();

	if (m_pWndInPlace != NULL)
	{
		m_pWndInPlace->SetFocus();
	}
	else
	{
		m_pWndList->SetFocus();
	}
}