/**
 * @brief			CustomPropertyインクルードファイル
 * @file			CustomProperty.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */


#ifndef  _CUSTOM_PROPERTY_H__
#define  _CUSTOM_PROPERTY_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DrawingObject/Primitive/POINT2D.H"

/**
 * @class   CCheckBoxProp
 * @brief                        
 */
class CCheckBoxProp : public CMFCPropertyGridProperty
{
public:
	CCheckBoxProp(const CString& strName, BOOL bCheck, LPCTSTR lpszDescr = NULL, DWORD dwData = 0);

protected:
	virtual BOOL OnEdit(LPPOINT /*lptClick*/) { return FALSE; }
	virtual void OnDrawButton(CDC* /*pDC*/, CRect /*rectButton*/) {}
	virtual void OnDrawValue(CDC* /*pDC*/, CRect /*rect*/) {}
	virtual BOOL HasButton() const { return FALSE; }

	virtual BOOL PushChar(UINT nChar);
	virtual void OnDrawCheckBox(CDC * pDC, CRect rectCheck, BOOL bChecked);
	virtual void OnDrawName(CDC* pDC, CRect rect);
	virtual void OnClickName(CPoint point);
	virtual BOOL OnDblClk(CPoint point);

protected:
	CRect m_rectCheck;
};

//===============================================================================
/**
 * @class   CSliderProp
 * @brief                        
 */
class CSliderProp : public CMFCPropertyGridProperty
{
public:
	CSliderProp(const CString& strName, long nValue, LPCTSTR lpszDescr = NULL, DWORD dwData = 0);

	virtual BOOL OnUpdateValue();

protected:
	virtual CWnd* CreateInPlaceEdit(CRect rectEdit, BOOL& bDefaultFormat);
	virtual BOOL OnSetCursor() const { return FALSE; /* Use default */ }
};

//===============================================================================
/**
 * @class   CSliderProp
 * @brief                        
 */
class CBoundedNumberPairProp : public CMFCPropertyGridProperty
{
public:
	CBoundedNumberPairProp(const CString& strGroupName, int nMinValue1, int nMaxValue1, int nMinValue2, int nMaxValue2, DWORD_PTR dwData = 0, BOOL bIsValueList = FALSE);

	virtual BOOL OnUpdateValue();

private:
	int m_nMinValue1;
	int m_nMaxValue1;
	int m_nMinValue2;
	int m_nMaxValue2;
};


//===============================================================================
/**
 * @class   CSliderProp
 * @brief                        
 */
class CBoundedNumberSubProp : public CMFCPropertyGridProperty
{
public:
	CBoundedNumberSubProp(const CString& strName, const COleVariant& varValue, int nMinValue, int nMaxValue, LPCTSTR lpszDescr = NULL);

	virtual BOOL OnUpdateValue();
private:
	int m_nMinValue;
	int m_nMaxValue;
};

//===============================================================================
/**
 * @class   CIconListProp
 * @brief                        
 */
class CIconListProp : public CMFCPropertyGridProperty
{
public:
	CIconListProp(const CString& strName, const CImageList& imageListIcons, int nSelectedIcon, CStringList* plstIconNames = NULL, LPCTSTR lpszDescr = NULL, DWORD dwData = 0);

protected:
	virtual CWnd* CreateInPlaceEdit(CRect rectEdit, BOOL& bDefaultFormat);
	virtual CComboBox* CreateCombo(CWnd* pWndParent, CRect rect);
	virtual void OnDrawValue(CDC* pDC, CRect rect);

	CMFCToolBarImages m_imageListIcons;
	CStringList m_lstIconNames;
};

//===============================================================================
/**
 * @class   CComboBoxExProp
 * @brief                        
 */
class CComboBoxExProp : public CMFCPropertyGridProperty
{
public:
	CComboBoxExProp(const CString& strName, const CString& strValue, LPCTSTR lpszDescr = NULL, DWORD dwData = 0, CImageList* pImageList = NULL);

	BOOL AddOption(LPCTSTR lpszOption, int nIcon = -1, int nIndent = 0);

protected:
	virtual CComboBox* CreateCombo(CWnd* pWndParent, CRect rect);
	virtual BOOL OnEdit(LPPOINT lptClick);

	CImageList* m_pImageList;
	CArray<int, int> m_lstIcons;
	CArray<int, int> m_lstIndents;
};

//===============================================================================
/**
 * @class   COwnerDrawDescrProp
 * @brief                        
 */
class COwnerDrawDescrProp : public CMFCPropertyGridProperty
{
public:
	COwnerDrawDescrProp(const CString& strName, const COleVariant& varValue);

protected:
	virtual void OnDrawDescription(CDC* pDC, CRect rect);
};

//===============================================================================
/**
 * @class   CTwoButtonsProp
 * @brief                        
 */
class CTwoButtonsProp : public CMFCPropertyGridProperty
{
public:
	CTwoButtonsProp(const CString& strName, 
		const CString& strValue,
		LPCTSTR     lpszDescr,
		DWORD       dwData,
		std::function<void(bool)>);

protected:
	virtual BOOL HasButton() const { return TRUE; }
	virtual void AdjustButtonRect();
	virtual void OnClickButton(CPoint point);
	virtual void OnDrawButton(CDC* pDC, CRect rectButton);

	CImageList m_images;
	std::function<void(bool)> m_func;
};

//===============================================================================
/**
 * @class   CCustomDlgProp
 * @brief                        
 */
class CCustomDlgProp : public CMFCPropertyGridProperty
{
    CDialog* m_pDlg;
public:
    CCustomDlgProp(const CString& strName, 
                               const CString& strValue,
                               LPCTSTR     lpszDescr,
                               DWORD       dwData,
                               CDialog* pDlg);

protected:
	virtual BOOL HasButton() const { return TRUE; }
	virtual void OnClickButton(CPoint point);
};


//===============================================================================
//===============================================================================
/**
 * @class   CSliderProp
 * @brief                        
 */
class CPropSliderCtrl : public CSliderCtrl
{
// Construction
public:
	CPropSliderCtrl(CSliderProp* pProp, COLORREF clrBack);

// Attributes
protected:
	CBrush m_brBackground;
	COLORREF m_clrBack;
	CSliderProp* m_pProp;

// Implementation
public:
	virtual ~CPropSliderCtrl();

protected:
	//{{AFX_MSG(CPropSliderCtrl)
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	//}}AFX_MSG
	afx_msg void HScroll(UINT nSBCode, UINT nPos);

	DECLARE_MESSAGE_MAP()
};

//===============================================================================
/**
 * @class   CSliderProp
 * @brief                        
 */
class CIconCombo : public CComboBox
{
// Construction
public:
	CIconCombo(CMFCToolBarImages& imageListIcons, CStringList& lstIconNames);

// Attributes
protected:
	CMFCToolBarImages& m_imageListIcons;
	CStringList& m_lstIconNames;

// Implementation
public:
	virtual ~CIconCombo();

protected:
	//{{AFX_MSG(CIconCombo)
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};



//===============================================================================
/**
 * @class   CPointListProp
 * @brief                        
 */
class CPointListProp : public CMFCPropertyGridProperty
{
public:
    //CListProp(const CString& strName, const COleVariant& varValue);
    CPointListProp(const CString& strName, 
                   const std::vector<POINT2D>& varList,
                   LPCTSTR     lpszDescr, 
                   DWORD       dwData = 0);

    void SetList(std::vector<POINT2D>& lstDbl){m_lstData = lstDbl;}
    std::vector<POINT2D>* GetList(){return &m_lstData;}

protected:
	virtual BOOL HasButton() const { return TRUE; }
	virtual void OnClickButton(CPoint point);
    std::vector<POINT2D>  m_lstData;
};


//===============================================================================
/**
 * @class   CDropBoolProp
 * @brief                        
 */
class CDropBoolProp : public CMFCPropertyGridProperty
{
public:
	CDropBoolProp(const CString&      strName, 
                            bool        bBool, 
                            LPCTSTR     lpszDescr, 
                            DWORD       dwData = 0);

    void  SetBool(bool bVal);
    bool  GetCurSel();

protected:
	virtual CComboBox* CreateCombo(CWnd* pWndParent, CRect rect);
	virtual BOOL OnEdit(LPPOINT lptClick);
    virtual void OnSelectCombo();

    bool              m_bSel;

};


//===============================================================================
/**
 * @class   CDropDownExProp
 * @brief                        
 */
class CDropDownExProp : public CMFCPropertyGridProperty
{
public:
	CDropDownExProp(const CString&      strName, 
                            int         iSel, 
                            LPCTSTR     lpszDescr, 
                            DWORD       dwData = 0, 
                            UINT        nIDResource = 0,
                            int         iDiv = 0);

	BOOL AddOption(LPCTSTR lpszOption, int nIcon = -1, int nIndent = 0);

    BOOL AddOptionId(LPCTSTR lpszOption, int nId);

    //イメージ設定
    bool SetImage(UINT nIDResource, int iDiv);

    //イメージ設定(外部設定）
    bool SetImage(CImageList* pImageList);

    void  Select(int iSel);

    int   GetCurSel() const;

    void  SetCurSel(int iSel);

    int   GetSelById(int iId);

    void  SetCurSelById(int iId);

    int  GetCurId() const;

    virtual void SetValue(const COleVariant& varValue);

    void SetStr(const CString strVal);

    bool FindId(int* pId, StdString strItem) const;

    bool FindStr(StdString* pStrItem, int iId) const;

protected:
    virtual ~CDropDownExProp();
	virtual CComboBox* CreateCombo(CWnd* pWndParent, CRect rect);
	virtual BOOL OnEdit(LPPOINT lptClick);
    virtual void OnSelectCombo();

	CImageList*      m_pImageList;

    std::vector< boost::tuple<int, int> > m_lstIcons;
    std::vector< boost::tuple<StdString, int> > m_lstValue;

	//CArray<int, int> m_lstIcons;
	//CArray<int, int> m_lstIndents;

    int  m_nSel;
    bool m_bInerImageList;
};


//===============================================================================
/**
 * @class   CDropCheckList
 * @brief                        
 */
class CDropCheckList : public CMFCPropertyGridProperty
{
public:
	CDropCheckList(const CString& strName, const CString& strValue, LPCTSTR lpszDescr = NULL, DWORD dwData = 0);

	BOOL AddOption(LPCTSTR lpszOption, bool bCheck);

protected:
	virtual CComboBox* CreateCombo(CWnd* pWndParent, CRect rect);
	virtual BOOL OnEdit(LPPOINT lptClick);

    virtual BOOL OnUpdateValue();
    virtual BOOL OnEndEdit();

    void RecalcCheck(LPCTSTR str);

    static void CallbackOnCeck(void* pClass);

	std::map<StdString, bool> m_lstCheck;
};


//===============================================================================
/**
 * @class   CColorProp
 * @brief                        
 */
/*
class CColorProp : public CMFCPropertyGridProperty
{
	friend class CMFCPropertyGridCtrl;

	DECLARE_DYNAMIC(CColorProp)

// Construction
public:
	CColorProp(const CString& strName, const COLORREF& color, CPalette* pPalette = NULL, LPCTSTR lpszDescr = NULL, DWORD_PTR dwData = 0);
	virtual ~CColorProp();

// Overrides
public:
	virtual void OnDrawValue(CDC* pDC, CRect rect);
	virtual void OnClickButton(CPoint point);
	virtual BOOL OnEdit(LPPOINT lptClick);
	virtual BOOL OnUpdateValue();
	virtual CString FormatProperty();

protected:
	virtual BOOL OnKillFocus(CWnd* pNewWnd) { return pNewWnd->GetSafeHwnd() != m_pPopup->GetSafeHwnd(); }
	virtual BOOL OnEditKillFocus() { return m_pPopup == NULL; }
	virtual BOOL IsValueChanged() const { return m_Color != m_ColorOrig; }

	virtual void AdjustInPlaceEditRect(CRect& rectEdit, CRect& rectSpin);
	virtual void ResetOriginalValue();

// Attributes
public:
	COLORREF GetColor() const { return m_Color; }
	void SetColor(COLORREF color);

	// Color popup attributes:
	void EnableAutomaticButton(LPCTSTR lpszLabel, COLORREF colorAutomatic, BOOL bEnable = TRUE);
	void EnableOtherButton(LPCTSTR lpszLabel, BOOL bAltColorDlg = TRUE, BOOL bEnable = TRUE);
	void SetColumnsNumber(int nColumnsNumber);

// Attributes
protected:
	COLORREF     m_Color;          // Color value
	COLORREF     m_ColorOrig;      // Original color value
	COLORREF     m_ColorAutomatic; // Automatic (default) color value
	CString      m_strAutoColor;   // Atomatic color label
	CString      m_strOtherColor;  // Alternative color label
	CMFCColorPopupMenu* m_pPopup;

	CArray<COLORREF, COLORREF> m_Colors;

	int  m_nColumnsNumber; // Number of columns in dropped-down colors list
	BOOL m_bStdColorDlg;   // Use standard Windows color dialog
};
*/



class CFileProperty : public CMFCPropertyGridProperty
{
	DECLARE_DYNAMIC(CFileProperty)

	// Construction
public:

	CFileProperty(const CString& strName, const CString& strFolderName, DWORD_PTR dwData = 0, LPCTSTR lpszDescr = NULL);
	CFileProperty(const CString& strName, BOOL bOpenFileDialog, const CString& strFileName, const CString& strFolderName, LPCTSTR lpszDefExt = NULL,
		DWORD dwFlags = OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, LPCTSTR lpszFilter = NULL, LPCTSTR lpszDescr = NULL, DWORD_PTR dwData = 0);
	virtual ~CFileProperty();

	// Overrides
public:
	virtual void OnClickButton(CPoint point);

	// Attributes
protected:
	BOOL    m_bIsFolder;

	// File open dialog atributes:
	BOOL    m_bOpenFileDialog; // TRUE - use "File Open/Save" diaog; otherwise - folder selection dialog
	DWORD   m_dwFileOpenFlags;
	CString m_strDefExt;
	CString m_strFilter;
	CString m_strFolder;
};
#endif //_CUSTOM_PROPERTY_H__
