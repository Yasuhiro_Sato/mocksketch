//////////////////////////////////////////////////////////////////////
//
//	 Title					：2406 PC-EPMA
//	 Filename				：CDoubleEdit.h
//	 Author					：SystemV
//	 Belong to				：SystemV
//	 Creation date			：2005/08/31
//	 Copyright				：Copyright(c) 2005 JEOL Ltd.
//	 Abstract				：範囲チェックつき実数値入力エディットボックス
//
//	 Compiler				：VisualC++.NET (2003)
//	 Target system			：Windows XP   (ServicePack-2)
//	 Testing Environment	：Windows XP   (ServicePack-2)
//
//////////////////////////////////////////////////////////////////////

/*---------------------------------------------------------------------------*/
/*      Header files                                                         */
/*---------------------------------------------------------------------------*/
#include "stdafx.h"
#include <math.h>
#include "./DoublEedit.h"


/*---------------------------------------------------------------------------*/
/*      Macros                                                               */
/*---------------------------------------------------------------------------*/
IMPLEMENT_DYNAMIC(CDoubleEdit, CEdit)

BEGIN_MESSAGE_MAP(CDoubleEdit, CEdit)
    ON_CONTROL_REFLECT_EX(EN_KILLFOCUS, OnEnKillfocus)
    ON_CONTROL_REFLECT_EX(EN_CHANGE, OnEnChange)
    ON_WM_TIMER()
END_MESSAGE_MAP()


///////////////////////////////////////////////////////////
//	Method    : CDoubleEdit::CDoubleEdit
//	Summary   : コンストラクター
//	Arguments : なし
//	Return    : なし
//	Exception : なし
//	Note      :
///////////////////////////////////////////////////////////
CDoubleEdit::CDoubleEdit()
{
    m_dMax = 100.0;
    m_dMin = 0.0;
    SetPrecision(1);
    m_bUpdate = false;
    m_dwMsec = 2500;
    m_nTimer = 0;
}

///////////////////////////////////////////////////////////
//	Method    : CDoubleEdit::~CDoubleEdit
//	Summary   : デストラクター
//	Arguments : なし
//	Return    : なし
//	Exception : なし
//	Note      :
///////////////////////////////////////////////////////////
CDoubleEdit::~CDoubleEdit()
{
}

///////////////////////////////////////////////////////////
//	Method    : CDoubleEdit::SetRange
//	Summary   : 表示範囲設定
//	Arguments : dLower (i)下限値
//	          : dUpper (i)上限値
//	Return    : なし
//	Exception : なし
//	Note      :
///////////////////////////////////////////////////////////
void CDoubleEdit::SetRange(double dLower, double dUpper)
{
    m_dMax = dUpper;
    m_dMin = dLower;
}

///////////////////////////////////////////////////////////
//	Method    : CDoubleEdit::Round
//	Summary   : 四捨五入
//	Arguments : dVal   (i) 数値
//              lDegit (i) 小数点桁数
//	Return    : 変換結果
//	Note      : 
///////////////////////////////////////////////////////////
double CDoubleEdit::Round(double dVal, long lDegit)
{
    double dAdd;
    double dExp;
    double dAns;
    double dTmp1, dTmp2;

    if (lDegit < 0)
    {
        return dVal;
    }

    if(dVal >= 0)
    {
        dAdd = 0.5; 
    }
    else
    {
        dAdd = -0.5; 
    }

    dExp = pow(10.0, lDegit);
    dAns = dVal + (dAdd / dExp );
    dTmp1 = dAns * dExp;
    if (dVal >= 0)
    {
        dTmp2 = floor(dTmp1);
    }
    else
    {
        dTmp2 = ceil(dTmp1);
    }

    //割算は使用しない
    dAns = dTmp2 * pow(10.0, -lDegit);
    return dAns;
}


///////////////////////////////////////////////////////////
//	Method    : CDoubleEdit::GetVal
//	Summary   : 値の取得
//	Arguments : なし
//	Return    : 現在の設定値
//	Exception : なし
//	Note      :
///////////////////////////////////////////////////////////
double  CDoubleEdit::GetVal(bool bRangeCheck)
{
     CString strDat;
     UpdateData();
     GetWindowText(strDat);

     int iPos = strDat.Find(_T('.')); 
     if (iPos != -1)
     {
        CString sLeft  = strDat.Left (iPos); 
        CString sRigth = strDat.Mid(iPos + 1, strDat.GetLength() - sLeft.GetLength() - 1 ); 
        sRigth = sRigth.Left(m_lPrecision);
        strDat.Format(_T("%s.%s"), (LPCTSTR)sLeft, (LPCTSTR)sRigth);
     }
     m_dVal = _tstof(strDat);

    //表示誤差解消の為
    if(bRangeCheck)
    {
        if (m_dVal > m_dMax)
        {
            m_dVal = m_dMax;
        }
        else if (m_dVal < m_dMin)
        {
            m_dVal = m_dMin;
        }
    }

    return  m_dVal;
}

///////////////////////////////////////////////////////////
//	Method    : CDoubleEdit::SetVal
//	Summary   : 値の設定
//	Arguments : dVal        (i)設定する値
//              bRangeCheck (i)範囲チェックの有無
//	Return    : なし
//	Exception : なし
//	Note      :
///////////////////////////////////////////////////////////
void    CDoubleEdit::SetVal(double dVal, bool bRangeCheck)
{
    m_dVal = dVal;
    if (bRangeCheck)
    {
        if (m_dVal > m_dMax)
        {
            m_dVal = m_dMax;
        }
        else if (m_dVal < m_dMin)
        {
            m_dVal = m_dMin;
        }
    }

    //指定桁数での四捨五入を行う
    m_dVal = Round(m_dVal, m_lPrecision);

    SetWindowText(GetStr(m_dVal));
    UpdateData(FALSE);
}

///////////////////////////////////////////////////////////
//	Method    : CDoubleEdit::UpdateText
//	Summary   : 表示更新
//	Arguments : なし
//	Return    : なし
//	Exception : なし
//	Note      :
///////////////////////////////////////////////////////////
void    CDoubleEdit::UpdateText()
{
    GetVal();
    SetWindowText(GetStr(m_dVal));
    UpdateData(FALSE);
}

///////////////////////////////////////////////////////////
//	Method    : CDoubleEdit::SetPrecision
//	Summary   : 小数点以下の桁数の設定
//	Arguments : lPrecision(i) 小数点以下の桁数
//	Return    : なし
//	Exception : なし
//	Note      :
///////////////////////////////////////////////////////////
void    CDoubleEdit::SetPrecision(unsigned long lPrecision)
{
    m_sFormat.Format(_T("%%.%df"), lPrecision);
    m_lPrecision = lPrecision;
}

///////////////////////////////////////////////////////////
//	Method    : CDoubleEdit::OnEnKillfocus
//	Summary   : フォーカス失効時の処理
//	Arguments : なし
//	Return    : 真偽
//	Exception : なし
//	Note      :
///////////////////////////////////////////////////////////
BOOL CDoubleEdit::OnEnKillfocus()
{
    //[#176-5th] Returnキーを押すと 連続してデータが更新される
    if(fabs(m_dVal - GetVal()) > 1e-10)
    {
        UpdateText();
    }
    return TRUE;
}

///////////////////////////////////////////////////////////
//	Method    : CDoubleEdit::OnEnChange
//	Summary   : エディットボックス変更時の処理
//	Arguments : なし
//	Return    : なし
//	Exception : なし
//	Note      : 
///////////////////////////////////////////////////////////
BOOL CDoubleEdit::OnEnChange()
{

    //ON_CONTROL_REFLECT_EX を使用し
    // FALSEを返した場合は、親ウインドウで
    // イベントが発生する

    if(m_bUpdate)
    {
        m_bUpdate = false;
        return FALSE;
    }
    else
    {
        //タイマーは使わない
        //StartTimer();
        return TRUE;
    }
}

///////////////////////////////////////////////////////////
//	Method    : CDoubleEdit::SendChange
//	Summary   : チェンジメッセージ発行
//	Arguments : なし
//	Return    : なし
//	Exception : なし
//	Note      : 親ウインドウでOnEnUpdateXXXXを発生させる
///////////////////////////////////////////////////////////
void CDoubleEdit::SendChange()
{
    CWnd* pWNd;
    StopTimer();
    pWNd = GetParent();
    if (pWNd != NULL)
    {
        //EN_CHANGEは自動的に発生するためここでイベントの
        //送信は行わない
        //WORD  nHi, nLo;
        //nHi = EN_CHANGE;
        //nLo = GetDlgCtrlID();
        //DWORD dwChange = (DWORD)MAKELONG(nLo, nHi);
        m_bUpdate = true;
        //pWNd->SendMessage(WM_COMMAND, dwChange, (LPARAM)m_hWnd);
    }
}
///////////////////////////////////////////////////////////
//	Method    : CDoubleEdit::PreTranslateMessage
//	Summary   : メッセージフィルタ処理
//	Arguments : pMsg (i) ウインドウメッセージ
//	Return    : true/false
//	Exception : なし
//	Note      :
///////////////////////////////////////////////////////////
BOOL CDoubleEdit::PreTranslateMessage(MSG* pMsg)
{
    if (pMsg->message == WM_KEYDOWN)
    {
        if(pMsg->wParam == VK_RETURN)
        {
            UpdateText();
            return TRUE;
        }
    }
    return CEdit::PreTranslateMessage(pMsg);
}

///////////////////////////////////////////////////////////
//	Method    : CDoubleEdit::CheckVal
//	Summary   : 表示データ範囲チェック
//	Arguments : pFrVal(o) 変更前の値
//              pToVal(o) 変更後の値
//	Return    : TRUE、範囲変更無し FALSE あり
//	Exception : なし
//	Note      :
///////////////////////////////////////////////////////////
BOOL CDoubleEdit::CheckVal(double* pFrVal, double*pToVal)
{
     CString strDat;
     BOOL bRet = TRUE;
     double ToVal;
     double FrVal;

     UpdateData();
     GetWindowText(strDat);
     m_dVal = _tstof(strDat);
     ToVal = m_dVal;
     FrVal = m_dVal;

     if (m_dVal > m_dMax)
     {
        ToVal = m_dMax;
        //[2005.12.24]#2-22
        //m_dMax = ToVal;
        bRet =  FALSE;
     }
     else if (m_dVal < m_dMin)
     {
        ToVal = m_dMin;
        //[2005.12.24]#2-22
        //m_dMax = ToVal;
        bRet =  FALSE;
     }

    if (pFrVal != NULL)
    {
        *pFrVal = FrVal;
    }

    if (pToVal != NULL)
    {
        *pToVal = ToVal;
    }
    UpdateText();
    return  bRet;
}

///////////////////////////////////////////////////////////
//	Method    : CDoubleEdit::CheckVal
//	Summary   : フォーマットを適用した値を取得する
//	Arguments : dVal(i) 入力値
//              pToVal(o) 変更御の値
//	Return    : 変換後の文字列
//	Exception : なし
//	Note      :
///////////////////////////////////////////////////////////
CString CDoubleEdit::GetStr(double dVal)
{
    CString strDat;
    double dTmp;
    //切捨を行う
    dTmp = dVal * pow(double(10.0), double(m_lPrecision));
    if (dVal > 0)
    {
        dTmp = floor(dTmp);
    }
    else
    {
        dTmp = ceil(dTmp);
    }
    dTmp = dTmp / (double)pow(double(10), double(m_lPrecision));

    strDat.Format(m_sFormat, dTmp);
    return strDat;
}

////////////////////////////////////////////////////////////
//	Method    : CDoubleEdit::StartTimer
//	Summary   : タイマー設定
//	Arguments : なし
//	Return    : 
//	Note      : 
///////////////////////////////////////////////////////////
void CDoubleEdit::StartTimer()
{
    if (m_dwMsec == 0)
    {
        return;
    }
    m_nTimer = SetTimer( 1, m_dwMsec, NULL);
}

////////////////////////////////////////////////////////////
//	Method    : CDoubleEdit::StopTimer
//	Summary   : タイマー解除
//	Arguments : なし
//	Return    : 
//	Note      : 
///////////////////////////////////////////////////////////
void CDoubleEdit::StopTimer()
{
    if (m_nTimer)
    {
        KillTimer(m_nTimer);
        m_nTimer = 0;
    }
}

////////////////////////////////////////////////////////////
//	Method    : CIntEdit::OnTimer
//	Summary   : タイマー処理
//	Arguments : nIDEvent (i) イベントID
//	Return    : なし
//	Note      : 
///////////////////////////////////////////////////////////
void CDoubleEdit::OnTimer(UINT_PTR  nIDEvent)
{
    CEdit::OnTimer(nIDEvent);
    if (m_nTimer)
    {
        if(IsWindowVisible())
        {
            UpdateText();
        }
        StopTimer();
    }
    CEdit::OnTimer(nIDEvent);
}

///////////////////////////////////////////////////////////
//	Method    : CDoubleEdit::WindowProc
//	Summary   : Windows プロシージャ
//	Arguments : message (i) メッセージ
//              wParam (i) パラメータ
//              lParam (i) パラメータ
//	Return    : メッセージに依存する値
//	Exception : なし
//	Note      : メニュー用
///////////////////////////////////////////////////////////
LRESULT CDoubleEdit::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
    //スピンボタン等で変更した場合
    if (message == WM_SETTEXT)
    {
        SendChange();
    }
    return CEdit::WindowProc(message, wParam, lParam);
}
