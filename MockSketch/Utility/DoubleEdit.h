//////////////////////////////////////////////////////////////////////
//
//	 Title					：2406 PC-EPMA
//	 Filename				：DoubleEdit.cpp
//	 Author					：SystemV
//	 Belong to				：SystemV
//	 Creation date			：2005/08/31
//	 Copyright				：Copyright(c) 2005 JEOL Ltd.
//	 Abstract				：範囲チェックつき実数値入力エディットボックス
//
//	 Compiler				：VisualC++.NET (2003)
//	 Target system			：Windows XP   (ServicePack-2)
//	 Testing Environment	：Windows XP   (ServicePack-2)
//
//////////////////////////////////////////////////////////////////////
#pragma once

/*---------------------------------------------------------------------------*/
/*      Header files                                                         */
/*---------------------------------------------------------------------------*/


///////////////////////////////////////////////////////////
//	Class     : CDoubleEdit
//	Summary   : 範囲チェックつき実数値入力エディットボックス
//	Note      :
///////////////////////////////////////////////////////////
class CDoubleEdit : public CEdit
{
	DECLARE_DYNAMIC(CDoubleEdit)

public:
	CDoubleEdit();
	virtual ~CDoubleEdit();

    //レンジ設定
    void    SetRange(double dLower, double dUpper);

    //値の取得
    double  GetVal(bool bRangeCheck = true);

    //値の設定
    void    SetVal(double dVal, bool bRangeCheck = false);

    //小数点以下の桁数
    void    SetPrecision(unsigned long lPrecision);

    //値の更新
    void    UpdateText();

    //四捨五入
    double Round(double dVal, long lDegit);

    //表示データ範囲チェック
    BOOL CheckVal(double* pFrVal, double*pToVal);

    //フォーマットを適用した値を取得する
    CString GetStr(double dVal);

protected:
    //タイマー開始
    void StartTimer();

    //タイマー停止
    void StopTimer();

    //チェンジメッセージ発行
    void SendChange();

protected:
    //小数点以下の桁数
    unsigned long m_lPrecision;

    //最大値
    double m_dMax;

    //最小値
    double m_dMin;

    //現在の値
    double m_dVal;

    //アップデート用フラグ
    bool m_bUpdate;

    //表示フォーマット
    CString m_sFormat;

    //----------------------------
    //ボタンを押し続けた場合の処理
    //----------------------------
    //反応時間
    DWORD       m_dwMsec;

    //タイマーID
    UINT_PTR    m_nTimer;

protected:
	DECLARE_MESSAGE_MAP()
public:
    afx_msg BOOL OnEnKillfocus();
    afx_msg BOOL OnEnChange();
    virtual BOOL PreTranslateMessage(MSG* pMsg);
    afx_msg void OnTimer(UINT_PTR  nIDEvent);
protected:
    virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
};


