/**
 * @brief			DropUtil実装ファイル
 * @file			DropData.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "DropItem.h"
#include <oleidl.h>


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CDropItem::CDropItem():
m_iId       (0),
m_Type      (E_DROP_NONE)
{

}

/**
 *  @brief  デストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CDropItem::~CDropItem()
{


}


/**
 *  @brief  名称取得
 *  @param  なし
 *  @retval 名称
 *  @note
 */
StdString CDropItem::GetName()
{
    return m_strName;
}

/**
 *  @brief  名称設定
 *  @param  [in] 
 *  @retval なし
 *  @note
 */
void CDropItem::SetName(StdString strName)
{
    m_strName = strName;
}
    
/**
 *  @brief  ID取得
 *  @param  なし
 *  @retval ID
 *  @note
 */
int CDropItem::GetId()
{
    return m_iId;
}

/**
 *  @brief  ID設定
 *  @param  [in] iId 
 *  @retval なし
 *  @note
 */
void CDropItem::SetId(int iId)
{
    m_iId = iId;
}

/**
 *  @brief  種別取得
 *  @param  [in] iId 
 *  @retval なし
 *  @note
 */
DROP_TYPE CDropItem::GetType()
{
    return m_Type;
}


