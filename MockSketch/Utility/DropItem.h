/**
 * @brief			CDropItemヘッダーファイル
 * @file			CDropItem.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef _DROP_ITEM_H__
#define _DROP_ITEM_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/

/*---------------------------------------------------*/
/*   Definition                                      */
/*---------------------------------------------------*/

/*---------------------------------------------------*/
/*  class                                            */
/*---------------------------------------------------*/

/*---------------------------------------------------*/
/*      Struct                                       */
/*---------------------------------------------------*/
enum DROP_TYPE
{
    E_DROP_NONE,
    E_DROP_ELEM,
    E_DROP_FIELD,
};

/**
 * @class   CDropItem
 * @brief                        
 */
class CDropItem
{
public:
    //!< コンストラクター
    CDropItem(void);

    //!< デストラクター
    virtual ~CDropItem();

    //!< 名称取得
    StdString GetName();

    //!< 名称設定
    void SetName(StdString strName);
    
    //!< ID取得
    int GetId();

    //!< ID設定
    void SetId(int iId);

    //!< 種別取得
    DROP_TYPE GetType(); 

    virtual void DrawIcon(){;}

    virtual void DrawDrop(){;}

protected:
    //!< ID
    int         m_iId;

    //!< 名称
    StdString   m_strName;

    //!< 種別
    DROP_TYPE   m_Type;
    

};

#endif //_DROP_UTIL_H__