/**
 * @brief			DropSource実装ファイル
 * @file		    DropSource.cpp
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MockSketch.h"
#include "./DropSource.h"
#include <afxole.h>

/*---------------------------------------------------*/
/* Macros                                            */
/*---------------------------------------------------*/
//スタティックリンクではエラーとなる
//IMPLEMENT_DYNAMIC(CDropSource, COleDropSource)

BEGIN_MESSAGE_MAP(CDropSource, COleDropSource)
END_MESSAGE_MAP()


/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CDropSource::CDropSource()
{
}

/**
 *  @brief  デストラクタ.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CDropSource::~CDropSource()
{
}
SCODE CDropSource::QueryContinueDrag(BOOL bEscapePressed,    DWORD dwKeyState) 
{    // TODO: この位置に固有の処理を追加するか、    
    //       または基本クラスを呼び出してください    

    SCODE scode = COleDropSource::QueryContinueDrag(bEscapePressed, dwKeyState);  

    // DRAGDROP_S_CANCEL
    // DRAGDROP_S_DROP

    if  (scode != DRAGDROP_S_DROP)       
    {
        return	(scode); 
    }

DB_PRINT(_T("CDropSource::QueryContinueDrag %d, %d \n"), bEscapePressed, dwKeyState);


    //m_pSourceWnd->PostMessage(WM_APPDROPITEM, m_LastEffect);  
    return  (scode);
}


/**
 *  @brief  マウスカーソル変更処理.
 *  @param  dropEffect  
 *  @retval S_OK 変更成功
 *  @note   
 */
SCODE CDropSource::GiveFeedback(DROPEFFECT dropEffect)
{
    m_LastEffect = dropEffect;

    return COleDropSource::GiveFeedback(dropEffect);
    /*
        if (!m_bDragStarted) 
        {
            return S_OK;
        }
    */
        /*
        switch (dropEffect) 
        {

        case DROPEFFECT_COPY:           // コピー操作中の場合。専用のマウスカーソルに置き換えます
                ::SetCursor(LoadCursor(NULL,IDC_WAIT));
                break;

 

        case DROPEFFECT_MOVE:           // 移動操作中の場合。専用のマウスカーソルに置き換えます
                ::SetCursor(LoadCursor(NULL,IDC_HAND));
                break;
        default:

                return DRAGDROP_S_USEDEFAULTCURSORS;
        }

        return S_OK;
        */
}

/**
 *  @brief  D&D開始処理.
 *  @param  pWnd  ドラッグ開始ウインドウ
 *  @retval S_OK 変更成功
 *  @note   
 */
BOOL CDropSource::OnBeginDrag(CWnd* pWnd)
{
    BOOL bRet;
    VERIFY(pWnd);

    m_pSourceWnd = pWnd;

    bRet = COleDropSource::OnBeginDrag(pWnd);

DB_PRINT(_T("OnBeginDrag %s\n"), bRet ? _T("OK") : _T("NG"));
    return bRet;
}
