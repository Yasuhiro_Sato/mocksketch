/**
* @brief		CDropSourceヘッダーファイル
* @file			CDropSource.h
* @author			Yasuhiro Sato
* @date			09-2-2009 23:59:08
* 
* @note	
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
* 
* @remarks	        
*			
*
* $
* $
* 
*/
//#ifndef __DROP_SOURCE_H__
//#define __DROP_SOURCE_H__
#pragma once
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/


/**
 * @class   CDropSource
 * @brief                        
 */
class CDropSource : public COleDropSource
{
//スタティックリンクではエラーとなる
//	DECLARE_DYNAMIC(CDropSource)

public:
    //!< コンストラクター
	CDropSource();

    //!< デストラクター
	virtual ~CDropSource();

protected:
	DECLARE_MESSAGE_MAP()

public:

    virtual SCODE QueryContinueDrag(BOOL bEscapePressed,DWORD dwKeyState); 

    //!< D&D操作時，マウスカーソル変更処理
    virtual SCODE GiveFeedback(DROPEFFECT dropEffect);

    //!< D&D開始処理
    virtual BOOL OnBeginDrag(CWnd* pWnd);

private:
	CWnd* m_pSourceWnd;	
    DROPEFFECT m_LastEffect;
};
//#endif //__DROP_SOURCE_H__


