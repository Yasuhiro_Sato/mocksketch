/**
 * @brief			CDropTarget実装ファイル
 * @file			DropTarget.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./DropTarget.h"
#include "MockSketch.h"
#include "CProjectCtrl.h"


#include "DefinitionObject/CPartsDef.h"
#include "DefinitionObject/ObjectDef.h"


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
//#ifdef _DEBUG
//#define new DEBUG_NEW
//#endif

//スタティックリンクではエラーとなる
//IMPLEMENT_DYNAMIC( CDropTarget, COleDropTarget)

BEGIN_MESSAGE_MAP( CDropTarget, COleDropTarget)
END_MESSAGE_MAP()

/**
 *  @brief  コンストラクター
 *  @param  なし
 *  @retval なし
 *  @note   
 */
CDropTarget::CDropTarget():
m_dwKeyState    (0),
m_pDropWnd      (NULL),
m_dropEffect    (0)
{
}

/**
 *  @brief  デストラクター
 *  @param  なし
 *  @retval なし
 *  @note   
 */
CDropTarget::~CDropTarget()
{
}

/**
 *  @brief  ドロップ結果受取ウインドウ設定
 *  @param  [in]  pWnd          操作を
 *  @retval なし
 *  @note   ドロップ時に
 *          マウスイベントを送信する
 */
void CDropTarget::SetDropWnd(CWnd* pWnd)
{
    m_pDropWnd = pWnd;
}


/**
 *  @brief  ドロップ処理
 *  @param  [in]  pWnd 
 *  @param  [in]  pDataObject   
 *  @param  [in]  dropEffect   
 *  @param  [in]  point   
 *  @retval TRUE
 *  @note   
 */
BOOL CDropTarget::OnDrop(CWnd* pWnd, 
                         COleDataObject* pDataObject,	
                         DROPEFFECT dropEffect, 
                         CPoint point)
{
    int iType = DropUtil::GetAvailableType(pDataObject);
    DROP_TARGET_DATA dropTarget;
    if (m_pDropWnd)
    {
        dropTarget.pWnd = pWnd;
        dropTarget.pDataObject = pDataObject;
        dropTarget.dropEffect = dropEffect;
        dropTarget.ptDrop = point;
        dropTarget.dwKeyState = m_dwKeyState;

        m_pDropWnd->SendMessage(WM_DROP_END, iType, (LPARAM)&dropTarget);
    }
    return TRUE;
}


/**
 *  @brief  ドロップ開始処理
 *  @param  [in]  pWnd 
 *  @param  [in]  pDataObject   
 *  @param  [in]  dwKeyState   
 *  @param  [in]  point   
 *  @retval 
 *  @note   
 */
DROPEFFECT CDropTarget::OnDragEnter(CWnd* pWnd, 
                                    COleDataObject* pDataObject, 
                                    DWORD dwKeyState, 
                                    CPoint point)
{
    DROP_TARGET_DATA dropTarget;
    int iType;
    if (m_pDropWnd)
    {
        iType = DropUtil::GetAvailableType(pDataObject);

        m_dwKeyState = dwKeyState;
        dropTarget.pWnd = pWnd;
        dropTarget.pDataObject = pDataObject;
        dropTarget.dropEffect = m_dropEffect;
        dropTarget.ptDrop = point;
        dropTarget.dwKeyState = m_dwKeyState;

        m_pDropWnd->SendMessage(WM_DROP_ENTER, iType, (LPARAM)&dropTarget);
        m_dropEffect = dropTarget.dropEffect;

    }
    return COleDropTarget::OnDragEnter(pWnd, pDataObject, dwKeyState, point);
}

/**
 *  @brief  ドロップ開始処理
 *  @param  [in]  pWnd 
 *  @param  [in]  pDataObject   
 *  @param  [in]  dwKeyState   
 *  @param  [in]  point   
 *  @retval 
 *  @note   
 */
DROPEFFECT CDropTarget::OnDragOver(CWnd* pWnd, 
                                    COleDataObject* pDataObject, 
                                    DWORD dwKeyState, 
                                    CPoint point)
{

    // 引数の point を利用して、マウスカーソルがウィンドウの端に近い部分にある場合に
    // 自動スクロールの処理をしてますが、ここでは割愛します。
    DROPEFFECT ret = DROPEFFECT_NONE;
    DROP_TARGET_DATA dropTarget;
    int iType;
 
    if (m_pDropWnd)
    {
        m_dwKeyState = dwKeyState;

        dropTarget.pWnd = pWnd;
        dropTarget.pDataObject = pDataObject;
        dropTarget.dropEffect = m_dropEffect;
        dropTarget.ptDrop = point;
        dropTarget.dwKeyState = m_dwKeyState;
        iType = DropUtil::GetAvailableType(pDataObject);
        ret = IntptrToInt(m_pDropWnd->SendMessage(WM_DROP_OVER, iType, (LPARAM)&dropTarget));
    }
    return  ret;
}


/**
 *  @brief  ドロップ通過完了
 *  @param  [in]  pWnd 
 *  @retval 
 *  @note   
 */
void CDropTarget::OnDragLeave(CWnd* pWnd)
{
    m_dwKeyState = 0;
    if (m_pDropWnd)
    {
        m_pDropWnd->SendMessage(WM_DROP_LEAVE, 0, (LPARAM)pWnd);
    }
}
