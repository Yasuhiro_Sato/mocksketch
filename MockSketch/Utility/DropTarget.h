/**
* @brief		CDropTargetヘッダーファイル
* @file			DropTarget.h
* @author			Yasuhiro Sato
* @date			09-2-2009 23:59:08
* 
* @note	
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
* 
* @remarks	        
*			
*
* $
* $
* 
*/
#ifndef __DROP_TRAGET_H__
#define __DROP_TRAGET_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DropUtil.h"


//---------------
//
//---------------
struct DROP_TARGET_DATA
{
    CWnd*           pWnd;        //現在カーソルがあるウインドウ
    COleDataObject* pDataObject; //ドロップデータ オブジェクトへのポインタ。
    DROPEFFECT      dropEffect;  //ユーザーが選択したドロップ操作の結果
    POINT           ptDrop;      //画面上のカーソルの位置
    DWORD           dwKeyState;  //修飾子キーの状態。MK_CONTROL、MK_SHIFT、MK_ALT、MK_LBUTTON、MK_MBUTTON、および MK_RBUTTON をいくつでも組み合わせて指定できます。
};


/**
 * @class   ptDrop
 * @brief                        
 */
class CDropTarget : public COleDropTarget
{
    //スタティックリンクではエラーとなる
    //DECLARE_DYNAMIC(CDropTarget)

public:
    //!< コンストラクター
	CDropTarget();

    //!< デストラクター
    virtual ~CDropTarget();

    //!< ドロップ結果受取ウインドウ
    void SetDropWnd(CWnd* pWnd);

protected:
    
    //!< ドロップ報告ウインドウ
    CWnd* m_pDropWnd;

    //!< ドロップした部品
    DROP_TARGET_DATA    m_dropTarget;

    //!< キー状態
    DWORD               m_dwKeyState;

    DROPEFFECT          m_dropEffect;
protected:
	DECLARE_MESSAGE_MAP()

public:
    virtual BOOL OnDrop(CWnd* pWnd, COleDataObject* pDataObject,	DROPEFFECT dropEffect, CPoint point);
    virtual DROPEFFECT OnDragEnter(CWnd* pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point);
    virtual DROPEFFECT OnDragOver(CWnd* pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point);
    virtual void OnDragLeave(CWnd* pWnd);
};

#endif //__DROP_TRAGET_H__

