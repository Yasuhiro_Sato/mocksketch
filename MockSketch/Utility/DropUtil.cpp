/**
 * @brief			DropUtil実装ファイル
 * @file			DropData.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "DropUtil.h"
#include "DropTarget.h"
#include "DropSource.h"
#include <oleidl.h>


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

namespace DropUtil
{

	int GetAvailableType(COleDataObject* pDataObject)
	{
		if (pDataObject->IsDataAvailable(CF_MOCK_PARTS))
		{
			return CF_MOCK_PARTS;
		}
		else if (pDataObject->IsDataAvailable(CF_MOCK_FOLDER))
		{
			return CF_MOCK_FOLDER;
		}
		else if (pDataObject->IsDataAvailable(CF_MOCK_SCRIPT))
		{
			return CF_MOCK_SCRIPT;
		}
		else if (pDataObject->IsDataAvailable(CF_MOCK_PROPERTY))
		{
			return CF_MOCK_PROPERTY;
		}
		else if (pDataObject->IsDataAvailable(CF_MOCK_IO))
		{
			return CF_MOCK_IO;
		}
		return -1;
	}

	/**
	 *  @brief  グローバルエリアにテキストを保存
	 *  @param  [in] strText  設定テキスト
	 *  @retval メモリーハンドル
	 *  @note    使い終わったら ::GlobalFree;
	 */
	HGLOBAL CreateGlobalStr(StdString strText)
	{
		HGLOBAL	    hText;
		StdChar* pChar;
		size_t iStrLen = strText.length() + 1;
		hText = ::GlobalAlloc(GHND, iStrLen * sizeof(StdChar));
		if (hText == NULL)
		{
			return hText;
		}

		pChar = (StdChar*)::GlobalLock(hText);

		if (pChar == NULL)
		{
			//メモリーが確保できない
			return NULL;
		}
		else
		{
			::memset(pChar, 0, iStrLen * sizeof(StdChar));
			::memcpy(pChar, (LPCTSTR)strText.c_str(), (iStrLen - 1) * sizeof(StdChar));
			::GlobalUnlock(hText);
			return hText;
		}
	}

	/**
	 *  @brief  パーツドロップデータ作成
	 *  @param  [in] strParts  パーツ名
	 *  @retval
	 *  @note
	 */
	DROPEFFECT DoDorpParts(StdString strParts)
	{
		COleDataSource dropSrc;
		HGLOBAL hText = CreateGlobalStr(strParts);

		if (hText == NULL)
		{
			STD_DBG(_T("Drop Failure\n"));

			return DROPEFFECT_NONE;
		}

		dropSrc.CacheGlobalData(CF_MOCK_PARTS, hText);    // 違うクリップボードフォーマットを指定して何個もデータを登録可能です。

		CDropSource ds;
		DROPEFFECT dropEffect = dropSrc.DoDragDrop(DROPEFFECT_COPY, NULL, &ds);


		::GlobalFree(hText);
		return dropEffect;
	}

}