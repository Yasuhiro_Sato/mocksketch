/**
 * @brief			CDropUtilヘッダーファイル
 * @file			CDropUtil.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef _DROP_UTIL_H__
#define _DROP_UTIL_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "View/ViewCommon.h"

/*---------------------------------------------------*/
/*   Definition                                      */
/*---------------------------------------------------*/
#define CFSTR_MOCK_PARTS	_T("MockParts.Format")
const UINT CF_MOCK_PARTS = RegisterClipboardFormat(CFSTR_MOCK_PARTS);

#define CFSTR_MOCK_FOLDER	_T("MockFolder.Format")
const UINT CF_MOCK_FOLDER = RegisterClipboardFormat(CFSTR_MOCK_FOLDER);

#define CFSTR_MOCK_SCRIPT	_T("MockScript.Format")
const UINT CF_MOCK_SCRIPT = RegisterClipboardFormat(CFSTR_MOCK_SCRIPT);

#define CFSTR_MOCK_PROPERTY	_T("MockProperty.Format")
const UINT CF_MOCK_PROPERTY = RegisterClipboardFormat(CFSTR_MOCK_PROPERTY);

#define CFSTR_MOCK_IO	_T("MockIo.Format")
const UINT CF_MOCK_IO  = RegisterClipboardFormat(CFSTR_MOCK_IO);


/*---------------------------------------------------*/
/*  class                                            */
/*---------------------------------------------------*/
class  CObjectDef;

/*---------------------------------------------------*/
/*      Struct                                       */
/*---------------------------------------------------*/

//FOLDER, PARTS, SCRIPT 共通データ
struct DROP_PARTS_DATA
{
    UINT                        uiDropType;  // CF_MOCK_PARTS等
    int                         iDragSrc;    // ドラッグ元
    boost::uuids::uuid          uuidDef;
    VIEW_COMMON::E_PROJ_TYPE    eProjType;      // ドラッグ元プロジェクト種別
    int                         iScript;        // スクリプトId
    DROPEFFECT                  effect;         // Move or Copy
    bool                        bLeftButton;    // 
    POINT                       ptDrop;         // ドロップ位置
    HWND                        hCutWindow;     // カット元ウインドウ
                                                // PASTE 後に WM_USER_PASTE_ENDを送信する
};


struct DROP_IO_DATA
{
    UINT                 uiDropType;
    POINT                ptMouse;
    boost::uuids::uuid   uuidDef;

};

namespace DropUtil
{

int GetAvailableType(COleDataObject* pDataObject);

HGLOBAL CreateGlobalStr(StdString strText);

//!<< パーツドロップデータ作成
DROPEFFECT DoDorpParts(StdString strParts);

}

#endif //_DROP_UTIL_H__