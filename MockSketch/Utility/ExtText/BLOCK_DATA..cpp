/**
* @brief			BLOCK_DATA実装ファイル
* @file			BLOCK_DATA.cpp
* @author			Yasuhiro Sato
* @date			07-2-2009 20:02:47
* 
* @note	
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
* 
* @remarks	        
*			
*
* $
* $
* 
*/
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MockSketch.h"
#include "./BLOCK_DATA.h"
#include "./FONT_LINE_DATA.h"


BLOCK_DATA::BLOCK_DATA() :
    sameCh(0),
    pParentLine(NULL)
{
#if  DBG_PRINT
    pName = std::make_unique<StdString>();
#endif 
    Clear();
}


BLOCK_DATA::BLOCK_DATA(const BLOCK_DATA& b)
{
    *this = b;
}

BLOCK_DATA& BLOCK_DATA::operator =(const BLOCK_DATA& block)
{
    pParentLine = block.pParentLine;
    sameCh = block.sameCh;
    iWidth = block.iWidth;
    iFront = block.iFront;
    iHeight = block.iHeight;
    iBottom = block.iBottom;


    iBlockNo = block.iBlockNo;
    szModifer = block.szModifer;
    ptEscOffset = block.ptEscOffset;
    //dEscScl = block.dEscScl;
    iOffsetX = block.iOffsetX;
    ptAlign = block.ptAlign;
    eLine = block.eLine;
    bClose = block.bClose;

    lstLine.clear();
    for (auto ite = block.lstLine.begin();
        ite != block.lstLine.end();
        ite++)
    {
        lstLine.push_back(std::make_shared<FONT_LINE_DATA>(*(*ite)));
    }

#if  DBG_PRINT
    if (!pName)
    {
        pName = std::make_unique<StdString>();
    }

    *pName = *block.pName;
#endif 

    return *this;
}

void BLOCK_DATA::AddLine()
{
    lstLine.push_back(std::make_shared<FONT_LINE_DATA>());
    GetCurLine()->pParentBlock = this;
#if  DBG_PRINT
    size_t iCurLine = lstLine.size() - 1;

    StdStringStream strmName; 

    strmName << *this->pName;
    strmName << _T("-");
    strmName << _T("L:");
    strmName << iCurLine;

    *(GetCurLine()->pName) = strmName.str();
#endif

}

std::shared_ptr<FONT_LINE_DATA>& BLOCK_DATA::GetCurLine()
{
    if (lstLine.empty())
    {
        AddLine();
    }
    size_t iCurLine = lstLine.size() - 1;
    return  lstLine[iCurLine];
}

POINT BLOCK_DATA::_GetGlobal() const
{
    POINT ptRet;
    if (lstLine.empty())
    {
        ptRet.x = 0;
        ptRet.y = 0;
        return ptRet;
    }

    ptRet = lstLine[0]->GetTotalOffset();
    ptRet.x -= lstLine[0]->iAlign;
    ptRet.x += iFront;
    return ptRet;
}


POINT BLOCK_DATA::GetGlobalTL() const
{
    POINT ptRet = _GetGlobal();
    ptRet.y -= (iHeight - iBottom);
    return ptRet;
}

POINT BLOCK_DATA::GetGlobalBR() const
{
    POINT ptRet = _GetGlobal();
    ptRet.y += iBottom;
    ptRet.x += iWidth;
    return ptRet;
}

double BLOCK_DATA::GetFontScl() const
{
    double dScl = 1.0;
   if (pParentLine)
   {
       if(pParentLine->pParentBlock)
       {
           dScl = pParentLine->pParentBlock->GetFontScl();
       }
   }
   return dScl * (szModifer.dScl + 1.0);
}

bool BLOCK_DATA::CloseAllLine(int iLinePitch, BASE_TYPE eBase)
{

#if  DBG_PRINT
    DB_PRINT(_T("CloseAllLine B[%s] \n"), this->pName->c_str());
#endif

    int iXMax = INT_MIN;
    int iXMin = INT_MAX;
    int iY = 0;

    /*
    ABCD
    EFGHAAAA
    HIJKEA
    */
    bool bFirst = true;

    //X方向はすでに設定済み

    int iLine = 0;

    int iOldTop = 0;
    int iOldBottom = 0;

    bool bGrid = false;
    if ((szModifer.dwSts & BS_GRID) ||
        (szModifer.dwSts & BS_NO_GRID_LINE))
    {
        bGrid = true;
    }

    std::vector< std::vector<BLOCK_DATA*>> grid;


    this->iBottom = 0;
    for (auto iteLine = lstLine.begin();
        iteLine != lstLine.end();
        iteLine++)
    {
        //ブロックサイズ

        //-------------------------------------------------
        // 各行の大きさに合わせてブロックのサイズを変更する
        //-------------------------------------------------
        if ((*iteLine)->iLineWidth > this->iWidth)
        {
            this->iWidth = (*iteLine)->iLineWidth;
        }

        if ((*iteLine)->iLineFront < this->iFront)
        {
            this->iFront = (*iteLine)->iLineFront;
        }

        if (iteLine == lstLine.begin())
        {
            (*iteLine)->iTotalBottom = (*iteLine)->iLineBottom;
            if ((*iteLine)->iLineHeight > this->iHeight)
            {
                this->iHeight = (*iteLine)->iLineHeight;
            }

            if ((*iteLine)->iLineBottom > this->iBottom)
            {
                this->iBottom = (*iteLine)->iLineBottom;
            }

        }
        else
        {
            int iAddHeight = iLinePitch + (*iteLine)->iLineHeight + (*iteLine)->iLineBottom;
            this->iHeight += iAddHeight;
            this->iBottom += iAddHeight;
            (*iteLine)->iTotalBottom = this->iBottom;
        }

        //-------------------------------------------------

        if(bGrid)
        {
            std::vector<BLOCK_DATA*> lstBlockRow;
            BLOCK_DATA* pBlock;
            for (auto ite = (*iteLine)->lstBlock.begin();
                ite != (*iteLine)->lstBlock.end();
                ite++)
            {
                pBlock = (*ite).get();
                if (!pBlock->bClose)
                {
                    return false;
                }
                lstBlockRow.push_back(pBlock);
            }
            grid.push_back(lstBlockRow);
        }
    }

    //-----------------
    //グリッドの処理
    //-----------------
    std::vector<int> lstWidth;
    std::vector<int> lstHeight;
    std::vector<int> lstAlignment;

    int iRowSize = SizeToInt(grid.size());
    int iColSize = 0;
    bool bGridSizeCheck = true;
    bool bUseGrid = false;
    if (bGrid)
    {
        bool bFirst = true;
        for (auto line : grid)
        {
            if (bFirst)
            {
                iColSize = SizeToInt(line.size());
                bFirst = false;
            }
            if (iColSize != line.size())
            {
                //列のサイズが揃っていないときは処理しない
                bGridSizeCheck = false;
                break;
            }
        }

        if (bGridSizeCheck)
        {
            bUseGrid = true;
            lstWidth.resize(iColSize, 0);
            lstHeight.resize(iRowSize, 0);
            lstAlignment.resize(iColSize, 0);

            for (int iR = 0; iR < iRowSize; iR++)
            {
                for (int iC = 0; iC < iColSize; iC++)
                {
                    BLOCK_DATA* pBlock;
                    pBlock = grid[iR].at(iC);

                    if (lstWidth[iC] < pBlock->iWidth)
                    {
                        lstWidth[iC] = pBlock->iWidth;
                    }
#if  DBG_PRINT

                    int iBottom = -1;
                    if (pBlock->pParentLine)
                    {
                        iBottom = pBlock->pParentLine->iLineBottom;
                    }

                    DB_PRINT(_T("R:%d C:%d W:%d H:%d O:%d B:%d\n"), iR, iC,
                        pBlock->iWidth,
                        pBlock->iHeight,
                        pBlock->iOffsetX,
                        iBottom);
#endif
                    //アライメント未設定の場合
                    //先頭列は左揃え、それ以外は上の列に従う
                    if (!(pBlock->szModifer.dwSts & BSA_LEFT) &&
                        !(pBlock->szModifer.dwSts & BSA_RIGHT) &&
                        !(pBlock->szModifer.dwSts & BSA_MID))
                    {
                        if (iR == 0)
                        {
                            pBlock->szModifer.dwSts |= BSA_LEFT;
                        }
                        else
                        {
                            pBlock->szModifer.dwSts = grid[iR - 1].at(iC)->szModifer.dwSts;
                        }
                    }
                }

                lstHeight[iR] = this->lstLine[iR]->iLineHeight;
            }

            //行の高さの1/10を幅方向のマージンとする
            int iMargin = lstHeight[0] / 10;
            if (iMargin < 0)
            {
                iMargin = 1;
            }

            this->lstRow.clear();
            this->lstCol.clear();
            this->lstRow.push_back(0);
            this->lstCol.push_back(0);
            for (int iR = 0; iR < iRowSize; iR++)
            {
                for (int iC = 0; iC < iColSize; iC++)
                {
                    BLOCK_DATA* pBlock;
                    pBlock = grid[iR].at(iC);

                    int iOffsetX = 0;
                    if (iR == 0)
                    {
                        iOffsetX = this->lstCol[iC] + lstWidth[iC] + (iMargin * 2);
                        this->lstCol.push_back(iOffsetX);
                    }

                    int iAllign = lstWidth[iC] - pBlock->iWidth;

                    if (pBlock->szModifer.dwSts & BSA_RIGHT)
                    {
                        pBlock->ptAlign.x = iAllign;
                    }
                    else if (pBlock->szModifer.dwSts & BSA_MID)
                    {
                        pBlock->ptAlign.x = iAllign / 2;
                    }
                    else  if (pBlock->szModifer.dwSts & BSA_LEFT)
                    {
                        pBlock->ptAlign.x = 0;
                    }
                    pBlock->ptAlign.x += iMargin;
                    pBlock->iWidth = lstWidth[iC];
                    pBlock->ptAlign.y = 0;
                }
                lstRow.push_back(lstRow[iR] + lstHeight[iR]);
            }

            this->iHeight =  lstRow[lstRow.size() - 1];
            this->iWidth = lstCol[lstCol.size() - 1];
#if 0
            DB_PRINT(_T("CloseAllLine::Width1:%d \n"), this->iWidth);
            DB_PRINT(_T("CloseAllLine::Height1:%d \n"), this->iHeight);

#endif
        }
#if  DBG_PRINT
        for (int iR : lstRow)
        {
            DB_PRINT(_T("lstRow:%d \n"), iR);
        }

        for (int iC : lstCol)
        {
            DB_PRINT(_T("lstCol:%d \n"), iC);
        }
#endif
    }
    else
    {
        for (auto pLine : lstLine)
        {
            int iAllign;
            iAllign = this->iWidth - pLine->iLineWidth;
            if (szModifer.dwSts & BSA_RIGHT)
            {
                pLine->iAlign = iAllign;
            }
            else if (szModifer.dwSts & BSA_MID)
            {
                pLine->iAlign = iAllign / 2;
            }
            else  if (szModifer.dwSts & BSA_LEFT)
            {
                pLine->iAlign = 0;
            }
        }
    }
    //-----------------

    if (bUseGrid)
    {
        for (int iR = 0; iR < iRowSize; iR++)
        {
            for (int iC = 0; iC < iColSize; iC++)
            {
                int iOffsetX;
                iOffsetX = lstCol[iC];

                BLOCK_DATA* pBlock;
                pBlock = grid[iR].at(iC);

                pBlock->iWidth = lstWidth[iC];
                pBlock->iOffsetX = lstCol[iC];
            }
        }
    }

#if 0
    DB_PRINT(_T("CloseAllLine::Width2:%d \n"), this->iWidth);
    DB_PRINT(_T("CloseAllLine::Height2:%d \n"), this->iHeight);
#endif

#if DBG_PRINT
    DB_PRINT(_T("----BOLCK START ----\n"));

    for (auto ite = lstLine.begin();
        ite != lstLine.end();
        ite++)
    {

#if 0
        DB_PRINT(_T("%d:  (T:%d,L:%d,B:%d,R:%d)\n"), iCol, (*ite)->rect.top, (*ite)->rect.left,
            (*ite)->rect.bottom, (*ite)->rect.right);

#endif


    }

#if 0
    DB_PRINT(_T("Block (T:%d,L:%d,B:%d,R:%d)\n"), rect.top, rect.left,
        rect.bottom, rect.right);

    DB_PRINT(_T("----BOLCK END ----\n"));
#endif
#endif
    bClose = true;
    return true;
}

void BLOCK_DATA::SetParent()
{

    for (auto iteLine = lstLine.begin();
        iteLine != lstLine.end();
        iteLine++)
    {
        (*iteLine)->pParentBlock = this;
        (*iteLine)->SetParent();
    }
}

void BLOCK_DATA::Clear()
{
    pParentLine = NULL;
    sameCh = 0;

    iWidth = 0;
    iHeight = 0;
    iFront = 0;
    iBottom = 0;

    iBlockNo = 0;
    ptEscOffset.x = 0;
    ptEscOffset.y = 0;
    //dEscScl = 1.0;
    iOffsetX = 0;
    ptAlign.x = 0;
    ptAlign.y = 0;
    eLine = BL_NONE;
    bClose = false;
    lstLine.clear();

    //AddLine();

}

BLOCK_DATA* BLOCK_DATA::GetParentBlock()
{
    if (pParentLine)
    {
        return pParentLine->pParentBlock;
    }
    return NULL;
}

void BLOCK_DATA::Print(StdString str)
{

    StdString strNmae;
#if  DBG_PRINT
    strNmae = *(pName);
#endif

    DB_PRINT(_T("%s Block(%d:%lx:) (W:%d H:%d F:%d B:%d O:%d Esc(%d,%d:%f) mod:%x N:%s)ParentLine=%lx \n"), str.c_str(),
        iBlockNo, this,
        iWidth, iHeight,
        iFront, iBottom,
        iOffsetX,
        ptEscOffset.x,
        ptEscOffset.y,
        szModifer.dScl,
        szModifer.dwSts,
        strNmae.c_str(),
        pParentLine);

    str += _T("  ");
    for (auto line: lstLine)
    {
        line->Print(str);
    }
}



bool BLOCK_DATA::IsGrid() const
{
    return (!lstRow.empty() || !lstCol.empty());
}
