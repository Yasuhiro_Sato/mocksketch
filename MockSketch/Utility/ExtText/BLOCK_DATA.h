/**
* @brief			BLOCK_DATAヘッダーファイル
* @file		    BLOCK_DATA.h
* @author			Yasuhiro Sato
* @date			02-2-2009 03:00:24
* 
* @note	
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
* 
* @remarks	        
*			
*
* $
* $
* 
*/

#if !defined (__BLOCK_DATA_H__)
#define __BLOCK_DATA_H__

#include "System/CSystem.h"
#include "System/MOCK_ERROR.h"
#include "./TEXT_TYPE.h"
#include "./ESC_BLOCK_MODIFER.h"

class FONT_LINE_DATA;


//ブロック情報
class BLOCK_DATA
{   
public:
    BLOCK_DATA();
    BLOCK_DATA(const BLOCK_DATA& b);
    virtual ~BLOCK_DATA(){;}

    BLOCK_DATA& operator =(const BLOCK_DATA& block);


    FONT_LINE_DATA*  pParentLine;
    StdChar sameCh;              //閉じカッコ } or ]
    int     iWidth;  //ブロックの幅
    int     iHeight; //ブロックの全高
    int     iFront;  //先頭文字左端から先端までの距離
    int     iBottom; //行先頭文字下端からの下端までの距離

    int     iBlockNo;
    POINT   ptEscOffset;     //ESCで設定されたオフセット    
    //double  dEscScl;
    int     iOffsetX;        //ブロック開始時のpParentLine終端位置
    POINT   ptAlign;         //ブロック毎のアライメント(GRID使用時)           
    E_BLOCK_LINE eLine;      //枠線
    bool    bClose;
    ESC_BLOCK_MODIFER  szModifer; //ブロック修飾子
    std::unique_ptr<StdString> pName;         //デバッグ用 

    std::vector < std::shared_ptr< FONT_LINE_DATA > > lstLine;

    std::vector < int > lstRow;
    std::vector < int > lstCol;

    //-----------------------
    //std::vector<std::shared_ptr<LINE2D>>  lstRuledLine;      //罫線

    //ESC等の影響は受けない
    void AddLine();
    std::shared_ptr<FONT_LINE_DATA>& GetCurLine();


    bool CloseAllLine(int iLinePich, BASE_TYPE eBase);
    POINT GetGlobalTL() const;
    POINT GetGlobalBR() const;

    BLOCK_DATA* GetParentBlock();
    double GetFontScl() const;
    void SetParent();
    void Clear();
    void Print(StdString str);
    bool IsGrid() const;

protected:
    POINT _GetGlobal() const;
};

#endif //__BLOCK_DATA_H__