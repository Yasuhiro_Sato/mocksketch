﻿/**
* @brief			CExtText実装ファイル
* @file		    CExtText.cpp
* @author			Yasuhiro Sato
* @date			02-2-2009 03:00:24
*
* @note
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
*
* @remarks
*
*
* $
* $
*
*/

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include <tchar.h>
#include "MockSketch.h"
#include "Resource.h"
#include "CExtText.h"
#include "../CompressVector.h"
#include "../CUtility.h"
#include "DrawingObject/Primitive/MAT2D.h"
#include "DrawingObject/Primitive/LINE2D.h"
#include "DrawingObject/Primitive/SPLINE2D.h"
#include "DrawingObject/CDrawingText.h"
#include "DefinitionObject/View/CDrawingView.h"
#include "View/CLayer.h"


#include "./FONT_LINE_DATA.h"
#include "./BLOCK_DATA.h"
#include "./TEXT_FRAME.h"
#include "./FONT_DATA.h"
#include "./TEXT_MARGIN.h"
#include "./FONT_WORK_DATA.h"
#include "./LINE_PROPERTY.h"
#include "./PARSE_DATA.h"


#include "System/CSystem.h"
#include <functional>
#include <ShellScalingAPI.h>
#include <boost/serialization/export.hpp> 
BOOST_CLASS_EXPORT(CExtText);



#define DEBUG_TEXT_LINE 0

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


RECT AppendRect(const RECT& r1, const RECT& r2)
{
    RECT ret;
    if (r1.bottom < r2.bottom)
    {
        ret.bottom = r2.bottom;
    }
    else
    {
        ret.bottom = r1.bottom;
    }

    if (r1.top > r2.top)
    {
        ret.top = r2.top;
    }
    else
    {
        ret.top = r1.top;
    }

    if (r1.left > r2.left)
    {
        ret.left = r2.left;
    }
    else
    {
        ret.left = r1.left;
    }

    if (r1.right < r2.right)
    {
        ret.right = r2.right;
    }
    else
    {
        ret.right = r1.right;
    }

    return ret;
}



/**
*  @brief   コンストラクター
*  @param   なし
*  @retval  なし
*  @note
*/
CExtText::CExtText()
{
    m_psDefautFont = std::make_shared<FONT_DATA>();

    m_psDefautFont->fntData.lfHeight = 0;                  // 文字セルまたは文字の高さ
    m_psDefautFont->fntData.lfWidth = 0;               // 平均文字幅
    m_psDefautFont->fntData.lfEscapement = 0;         // 文字送りの方向とX軸との角度
    m_psDefautFont->fntData.lfOrientation = 0;                     // ベースラインとX軸との角度
    m_psDefautFont->fntData.lfWeight = 0;                     // フォントの太さ
    m_psDefautFont->fntData.lfItalic = 0;                     // イタリック体指定
    m_psDefautFont->fntData.lfUnderline = 0;                     // 下線付き指定
    m_psDefautFont->fntData.lfStrikeOut = 0;                     // 打ち消し線付き指定
    m_psDefautFont->fntData.lfCharSet = DEFAULT_CHARSET;       // キャラクタセット
    m_psDefautFont->fntData.lfOutPrecision = 1;                  // 出力精度
    m_psDefautFont->fntData.lfClipPrecision = 1;                  // クリッピングの精度
    m_psDefautFont->fntData.lfQuality = DEFAULT_QUALITY;    // 出力品質
    m_psDefautFont->fntData.lfPitchAndFamily = 0;                  // ピッチとファミリ
    m_psDefautFont->fntData.lfFaceName[0] = 0;                     // フォント名

    m_psDefautFont->dHeight = 20;
    m_psDefautFont->uiStyle = 0;
    m_psDefautFont->crColor = DRAW_CONFIG->crFront;

    m_dAngle = 0.0;
    m_eDatum = BOTTTOM_LEFT;
    m_eAlignment = AL_LEFT;
    m_eBase = FIRST_BASE1;
    m_bUseFrame = false;
    m_bFixSize = false;
    m_bFixDisp = false;
    m_dSclX = 1.0;
    m_dSclY = 1.0;
    m_dPitch = 0.0;
    m_dLinePitch = 0.0;
    m_bFixRotate = false;
    m_bEditable = true;
    m_bVertical = false;
    m_bReverse  = false;
    m_bFixPitch = false;
    m_psTextFrame = std::make_unique<TEXT_FRAME>();
    m_psTextFrame->Margin()->dTop = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->dTextTopMargin);  
    m_psTextFrame->Margin()->dBottom = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->dTextBottomMargin);  
    m_psTextFrame->Margin()->dLeft = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->dTextLeftMargin);  
    m_psTextFrame->Margin()->dRight = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->dTextRightMargin);  

}

/**
*  @brief   コンストラクター
*  @param   [in]  pFONT フォント
*  @param   [in]  crDef 色
*  @retval  なし
*  @note
*/
CExtText::CExtText(const LOGFONT* pFONT, COLORREF crDef)
{
    m_psDefautFont = std::make_shared<FONT_DATA>();

    //STD_ASSERT( pFONT != NULL);


    memcpy(&m_psDefautFont->fntData, pFONT, sizeof(LOGFONT));

    m_psDefautFont->uiStyle = 0;
    m_psDefautFont->crColor = crDef;

    m_dAngle = 0.0;
    m_eDatum = BOTTTOM_LEFT;
    m_eAlignment = AL_LEFT;
    m_eBase = FIRST_BASE1;
    m_bUseFrame = false;
    m_bFixSize = false;
    m_bFixDisp = false;
    m_dSclX = 1.0;
    m_dSclY = 1.0;
    m_dPitch = 0.0;
    m_dLinePitch = 0.0;
    m_bFixRotate = false;
    m_bEditable = true;
    m_bVertical = false;
    m_bReverse  = false;
    m_bFixPitch = false;
    m_psTextFrame = std::make_unique<TEXT_FRAME>();
    m_psTextFrame->Margin()->dTop = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->dTextTopMargin);  
    m_psTextFrame->Margin()->dBottom = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->dTextBottomMargin);  
    m_psTextFrame->Margin()->dLeft = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->dTextLeftMargin);  
    m_psTextFrame->Margin()->dRight = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->dTextRightMargin);  
}

//!< コピーコンストラクター
CExtText::CExtText(const CExtText& txt)
{
    m_psTextFrame = std::make_unique<TEXT_FRAME>();
    *this = txt;
}


/**
*  @brief   代入演算子
*  @param   [in] m 代入元
*  @retval  代入値
*  @note
*/
CExtText& CExtText::operator = (const CExtText& m)
{
    m_dAngle = m.m_dAngle;
    m_strOut = m.m_strOut;
    m_eAlignment = m.m_eAlignment;
    m_eDatum = m.m_eDatum;
    m_eBase = m.m_eBase;
    m_FontData = m.m_FontData;
    m_bFixSize = m.m_bFixSize;
    m_bFixDisp = m.m_bFixDisp;
    m_dSclX = m.m_dSclX;
    m_dSclY = m.m_dSclY;
    m_dPitch = m.m_dPitch;
    m_dLinePitch = m.m_dLinePitch;
    m_bFixRotate = m.m_bFixRotate;
    m_bEditable = m.m_bEditable;
    m_bVertical = m.m_bVertical;
    m_bUseFrame = m.m_bUseFrame;
    *m_psTextFrame = *(m.m_psTextFrame);
    m_bReverse  = m.m_bReverse; 
    m_bFixPitch = m.m_bFixPitch;


    return *this;
}

// **
// @brief   等価演算子
// @param	m (i) 値
// @return  ture 同値
// @note    
// */
bool CExtText::operator == (const CExtText& m) const
{
    if (m_dAngle != m.m_dAngle) { return false; }
    if (m_strOut != m.m_strOut) { return false; }
    if (m_eDatum != m.m_eDatum) { return false; }
    if (m_eAlignment != m.m_eAlignment) { return false; }
    if (m_eBase != m.m_eBase) { return false; }

    if (m_bFixSize != m.m_bFixSize) { return false; }
    if (m_bFixDisp != m.m_bFixDisp) { return false; }
    if (!CUtil::IsEqual(m_dSclX, m.m_dSclX)) { return false; }
    if (!CUtil::IsEqual(m_dSclY, m.m_dSclY)) { return false; }
    if (!CUtil::IsEqual(m_dPitch, m.m_dPitch)) { return false; }
    if (!CUtil::IsEqual(m_dLinePitch, m.m_dLinePitch)) { return false; }
    if (m_bFixRotate != m.m_bFixRotate) { return false; }
    if (m_bEditable != m.m_bEditable) { return false; }
    if (m_bVertical != m.m_bVertical) { return false; }
    if (m_bReverse != m.m_bReverse) { return false; }
    if (m_bFixPitch != m.m_bFixPitch) { return false; }

    size_t iSize = m_FontData.size();
    if (iSize != m.m_FontData.size()) { return false; }


    for (size_t iPos = 0; iPos < iSize; iPos++)
    {
        if (*m_FontData[iPos] != *m.m_FontData[iPos])
        {
            return false;
        }
    }

    if (m_bUseFrame != m.m_bUseFrame)
    {
        return false;
    }
    else
    {
        if (m_bUseFrame)
        {
            if (*m_psTextFrame != *(m.m_psTextFrame))
            {
                return false;
            }
        }
    }

    return true;
}

// **
// @brief   不等価演算子
// @param	m (i) 値
// @return  false 同値
// @note    
// */
bool CExtText::operator != (const CExtText& m) const
{
    return !(*this == m);
}

/**
*  @brief   テキスト設定
*  @param   [in]  strText 表示文字
*  @retval  なし
*  @note
*/
void CExtText::SetText(const StdChar* strText)
{
    m_strOut = strText;
    size_t len = m_strOut.length();
    m_FontData.resize(len);

    //foreach(FONT_DATA &Data, m_FontData)
    //{
    //    Data = m_DefautFont;
    //}

    for (int iCnt = 0; iCnt < len; iCnt++)
    {
        m_FontData[iCnt] = m_psDefautFont;
    }
}

/**
*  @brief   色設定
*  @param   [in]  iStart 開始位置 (-1で先頭)
*  @param   [in]  iEnd   終了位置 (-1で終端)
*  @param   [in]  crText 設定色
*  @retval  なし
*  @note
*/
void CExtText::SetColor(int iStart, int iEnd, COLORREF crText)
{
    if (iStart == -1) { iStart = 0; }
    if (iEnd == -1)
    {
        iEnd = static_cast<int>(m_strOut.length()) - 1;
        if (iEnd < 0)
        {
            iEnd = 0;
        }
    }

    if (m_FontData.size() == 0)
    {
        return;
    }

    for (int iCnt = iStart; iCnt <= iEnd; iCnt++)
    {
        m_FontData[iCnt]->crColor = crText;
    }
}

/**
*  @brief   フォント
*  @param   [in]  iStart 開始位置 (-1で先頭)
*  @param   [in]  iEnd   終了位置 (-1で終端)
*  @param   [in]  pFONT  フォント
*  @retval  なし
*  @note
*/
void CExtText::SetFont(int iStart, int iEnd, const LOGFONT* pFONT)
{
    if (iStart == -1) { iStart = 0; }
    if (iEnd == -1)
    {
        iEnd = static_cast<int>(m_strOut.length()) - 1;
        if (iEnd < 0)
        {
            iEnd = 0;
        }
    }

    if (m_FontData.size() == 0)
    {
        return;
    }

    for (int iCnt = iStart; iCnt <= iEnd; iCnt++)
    {
        m_FontData[iCnt]->fntData = *pFONT;
    }
}

/**
*  @brief   表示スタイル設定
*  @param   [in]  iStart 開始位置 (-1で先頭)
*  @param   [in]  iEnd   終了位置 (-1で終端)
*  @param   [in]  uiStyle  スタイル(STYLEを参照)
*  @retval  なし
*  @note
*/
void CExtText::SetStyle(int iStart, int iEnd, unsigned int uiStyle)
{
    if (iStart == -1) { iStart = 0; }
    if (iEnd == -1)
    {
        iEnd = static_cast<int>(m_strOut.length()) - 1;
        if (iEnd < 0)
        {
            iEnd = 0;
        }
    }

    if (m_FontData.size() == 0)
    {
        return;
    }

    LOGFONT* pFont;

    for (int iCnt = iStart; iCnt <= iEnd; iCnt++)
    {

        m_FontData[iCnt]->uiStyle = uiStyle;
        pFont = &m_FontData[iCnt]->fntData;

        pFont->lfWeight = ((uiStyle & BOLD) ? FW_BOLD : 0);
        pFont->lfItalic = ((uiStyle & ITALIC) ? 1 : 0);
        pFont->lfUnderline = ((uiStyle & UNDER) ? 1 : 0);
        pFont->lfStrikeOut = ((uiStyle & STRIKE) ? 1 : 0);
    }

}

/**
*  @brief   表示スタイル設定
*  @param   [in]  iStart 開始位置 (-1で先頭)
*  @param   [in]  iEnd   終了位置 (-1で終端)
*  @param   [in]  uiStyle      スタイル(STYLEを参照)
*  @param   [in]  dHight       文字高さ(mm)
*  @param   [in]  color        文字色
*  @param   [in]  strFontName  フォント名
*  @retval  なし
*  @note
*/
void CExtText::SetStyle(int iStart, int iEnd, unsigned int uiStyle,
    double dHight, COLORREF color, StdString strFontName)
{
    if (iStart == -1) { iStart = 0; }
    if (iEnd == -1)
    {
        iEnd = static_cast<int>(m_strOut.length()) - 1;
        if (iEnd < 0)
        {
            iEnd = 0;
        }
    }

    if (m_FontData.size() == 0)
    {
        return;
    }

    LOGFONT* pFont;

    for (int iCnt = iStart; iCnt <= iEnd; iCnt++)
    {

        m_FontData[iCnt]->uiStyle = uiStyle;
        pFont = &m_FontData[iCnt]->fntData;

        m_FontData[iCnt]->dHeight = dHight;
        pFont->lfWeight = ((uiStyle & BOLD) ? FW_BOLD : 0);
        pFont->lfItalic = ((uiStyle & ITALIC) ? 1 : 0);
        pFont->lfUnderline = ((uiStyle & UNDER) ? 1 : 0);
        pFont->lfStrikeOut = ((uiStyle & STRIKE) ? 1 : 0);

        if (strFontName != _T(""))
        {
            _tcscpy(pFont->lfFaceName, strFontName.c_str());
        }
    }
}

/**
*  @brief   文字高さ設定
*  @param   [in]  iStart 開始位置 (-1で先頭)
*  @param   [in]  iEnd   終了位置 (-1で終端)
*  @param   [in]  iHight       文字高さ(point)
*  @retval  なし
*  @note
*/
void CExtText::SetHeight(int iStart, int iEnd, double dHight)
{
    if (iStart == -1) { iStart = 0; }
    if (iEnd == -1)
    {
        iEnd = static_cast<int>(m_strOut.length()) - 1;
        if (iEnd < 0)
        {
            iEnd = 0;
        }
    }

    if (m_FontData.size() == 0)
    {
        return;
    }

    for (int iCnt = iStart; iCnt <= iEnd; iCnt++)
    {
        m_FontData[iCnt]->dHeight = dHight;
    }
}

/**
*  @brief   角度設定
*  @param   [in]  dAngle    角度(deg)
*  @retval  なし
*  @note
*/
void CExtText::SetAngle(double dAngle)
{
    m_dAngle = dAngle;
}

/**
*  @brief   角度取得
*  @param   なし
*  @retval  なし
*  @note
*/
double CExtText::GetAngle() const
{
    return m_dAngle;
}


//!< 反転設定
void CExtText::SetReverse(bool bReverse)
{
    m_bReverse = bReverse;        
}

//!< 反転取得
bool CExtText::IsReverse() const
{
    return m_bReverse;
}

/**
*  @brief   表示領域取得
*  @param   [out] pSize 表示領域
*  @param   [out] pPtTL 先頭文字の右上の点
*  @param   [in]  pt    先頭文字の右上の点
*  @param   [in]  hDc   デバイスコンテキスト
*  @param   [in]  dScl  表示倍率
*  @retval  なし
*  @note
*/
void CExtText::GetDispSize(SIZE* pSize, HDC hDc, const CDrawingView* pView, double dScl) const
{
    HFONT hOldFont = 0;
    FONT_WORK_DATA work;
    hOldFont = DrawTest(&work, hDc, pView, dScl, false);


    *pSize = work.sizeDrawArea;

    if (hOldFont != 0)
    {
        (HFONT)::SelectObject(hDc, hOldFont);
    }

    for (FONT_OUT_DATA& fontData : work.outFont)
    {
        if (fontData.bCreateFont)
        {
            ::DeleteObject(fontData.hFont);
            if (m_dAngle != 0)
            {
                ::DeleteObject(fontData.hFontS);
            }
        }
    }
}

void CExtText::_ConvSpline( std::vector<LINE_LIST>* lstAny, 
                            std::vector<char>* pBuf,
                            const POINT2D& ptOffset,
                            CDrawingView* pView,
                            int iLayerId) const
{
    struct Vec2 
    {
        POINT2D pt;
        Vec2(const POINTFX& pfx) :
            pt((pfx.x.value + pfx.x.fract / 65536.0),
               (pfx.y.value + pfx.y.fract / 65536.0))
        {
        
        }
    };
    
    struct OUT_LINE 
    {  //線の情報を表す構造体
        WORD wType; //線のタイプ
        std::vector<Vec2> points; //線を確定させる点
    }; //


    std::vector<std::vector<OUT_LINE>> result;

    auto itrHeader = reinterpret_cast<LPTTPOLYGONHEADER>(&pBuf->at(0));


    while (static_cast<void*>(itrHeader) < &pBuf->at(0) + pBuf->size())
    {
        std::vector<OUT_LINE> contour; 

        DWORD curveSize = itrHeader->cb;

        POINTFX start = itrHeader->pfxStart;

        auto itrCurve = reinterpret_cast<LPTTPOLYCURVE>(itrHeader + 1);
        while (static_cast<void*>(itrCurve) < reinterpret_cast<char*>(itrHeader) + curveSize)
        {

            std::vector<Vec2> points;
            points.emplace_back(start);
            for (WORD i = 0; i < itrCurve->cpfx; i++) 
            {
                points.emplace_back(itrCurve->apfx[i]);
            }
            
            if (itrCurve->cpfx > 0)
            {
                start = itrCurve->apfx[itrCurve->cpfx - 1]; //apfxの最後の要素が次のstart
            }
            contour.push_back({ itrCurve->wType, std::move(points) });

            itrCurve = reinterpret_cast<LPTTPOLYCURVE>(&itrCurve->apfx + itrCurve->cpfx);
            //apfx[cpfx-1](最大要素)の一個後ろが次のCurveの先頭になることを利用
        }

        result.emplace_back(std::move(contour));

        itrHeader = reinterpret_cast<LPTTPOLYGONHEADER>(reinterpret_cast<char*>(itrHeader) + itrHeader->cb);
    }


    VIEW_COMMON::VIEW_VALUE& vv =  pView->GetViewValue();
    CLayer* pLayer = pView->GetLayer(iLayerId);
    double dDpu = pView->GetDpu();
    double dSclLayer = 1.0;
    //表示スケールを設定
    double dScl;

    if (!IsFixDispSize())
    {
        dSclLayer = pLayer->dScl;
    }
    dScl  = pView->GetViewScale() * dSclLayer;

#if DBG_CONV_SPLINE
    DB_PRINT(_T("_ConvSpline Scl:%f dpu:%f\n"), dScl, dDpu);
#endif 
    auto Scr2World = [=](POINT2D pt)
    {
        POINT2D ptRet2D;
        ptRet2D.dX =  pt.dX / (dDpu  * dScl);
        ptRet2D.dY =  pt.dY / (dDpu  * dScl);
        return ptRet2D;
    };

    SPLINE2D::SPLINE_TYPE wSplineType;
    bool  bFixLayerScl = IsFixSize();
    POINT2D ptLast;
    long    lastType = -1;

    for (auto vd : result) 
    {
        LINE_LIST loop;
        loop.ptOffset = Scr2World(ptOffset);
        //std::vector<CAny> loop;
        for (auto d : vd)
        {
            switch (d.wType)
            {
            case TT_PRIM_LINE:
            {

#if DBG_CONV_SPLINE
DB_PRINT(_T("TT_PRIM_LINE  "));
#endif 
                size_t num =  d.points.size() - 1;
                for(size_t cnt = 0; cnt < num; cnt++)
                {
                    POINT2D pt1, pt2;
#if DBG_CONV_SPLINE
DB_PRINT(_T("  Line(%f,%f)-(%f,%f)\n"),  d.points[cnt].pt.dX, 
                                         d.points[cnt].pt.dY,
                                         d.points[cnt+1].pt.dX, 
                                         d.points[cnt+1].pt.dY);
#endif

                    pt1 = Scr2World( d.points[cnt    ].pt );
                    pt2 = Scr2World( d.points[cnt + 1].pt );
                    LINE2D l(pt1 ,  pt2);
                    loop.lstAny.push_back(l);
                }
            }
                break;
            case TT_PRIM_CSPLINE:
            case TT_PRIM_QSPLINE:
            {
                wSplineType = SPLINE2D::SPT_BSPLINE3;
                std::vector<POINT2D> lstPt;
                SPLINE2D sp; 
                sp.SetType(wSplineType);
                sp.SetEditType(SPLINE2D::EDT_CTRL);
                sp.SetKnotType(SPLINE2D::KNT_OPEN_UNI);

                POINT2D pt;
                for(auto p: d.points)
                {

                    pt = Scr2World( p.pt );
                    lstPt.push_back(pt);
                }
#if DBG_CONV_SPLINE
    DB_PRINT(_T("\n"));
#endif
                bool bResetStartEnd = true;
                sp.SetControlPoints(lstPt, bResetStartEnd);
                loop.lstAny.push_back(sp);
            }
              break;
            default:
               break;


            }
            lastType = d.wType;

        }
        size_t vdSize = vd.size() - 1;
        size_t lastSize = vd.at(vdSize).points.size()-1;
        POINT2D ptStart;
        POINT2D ptEnd;

        ptStart = Scr2World( vd.at(0).points.at(0).pt);
        ptEnd = Scr2World(vd.at(vdSize).points.at(lastSize).pt);

        if(ptStart != ptEnd)
        {
            LINE2D l(ptStart , ptEnd );
            loop.lstAny.push_back(l);
        }
        lstAny->push_back(loop);
    }
}

void CExtText::ConvSpline(
    std::vector<std::vector<LINE_LIST>>* plst,
    CDrawingView* pView,
    int iLayerId)  const
{
    FONT_WORK_DATA workData;
    workData.bPrint = pView->IsPrintMode();
    HDC  hDc = pView->GetDc();

#if DBG_CONV_SPLINE
    DB_PRINT(_T("------------_ConvSpline Start\n"));
#endif 

    HFONT hOldFont = 0;
    //double dDspScl = pView->GetViewScale();


    double dScl;
    dScl = pView->GetViewScale();
    CLayer* pLayer = pView->GetLayer(iLayerId);
    if (!IsFixSize())
    {
        dScl *= pLayer->dScl;
    }

    if (IsFixDispSize())
    {
        dScl = 1.0;
    }

    hOldFont = DrawTest(&workData, hDc, pView, dScl, false);

    //SIZE size;
    //GetDispSize(&size, hDc, pView, 1.0);


    int lCnt;
    long lLen = (long)m_FontData.size();
    HFONT hFont = 0;
    StdChar  chOut[2];

    if (lLen == 0)
    {
        return;
    }

    double dDir = 1.0;
    unsigned int OldAlign;
    if(IsReverse())
    {
        OldAlign = ::SetTextAlign(hDc, TA_BOTTOM | TA_RIGHT);
        workData.ptOffsetToTL.x = -workData.ptOffsetToTL.x;
        dDir = -1.0;
    }
    else
    {
        OldAlign = ::SetTextAlign(hDc, TA_BOTTOM | TA_LEFT);
    }


    POINT ptLineOffset;
    ptLineOffset.x = 0;
    ptLineOffset.y = 0;
    FONT_LINE_DATA* pLine = NULL;

    TEXTMETRIC  tm;
    for (lCnt = 0; lCnt < lLen; lCnt++)
    {
        const FONT_DATA* pData = m_FontData[lCnt].get();
        if (!workData.outFont[lCnt].bDisp)
        {
            continue;
        }

        if (workData.outFont[lCnt].bCreateFont)
        {
            if (m_dAngle != 0)
            {
                hFont = workData.outFont[lCnt].hFontS;
            }
            else
            {
                hFont = workData.outFont[lCnt].hFont;
            }
            ::SelectObject(hDc, hFont);
            ::GetTextMetrics(hDc, &tm);
        }

        chOut[0] = m_strOut[lCnt];
        chOut[1] = 0;

        FONT_OUT_DATA* pOutFont = &(workData.outFont[lCnt]);
        FONT_LINE_DATA* pCurLine = pOutFont->pLine;
        if (pCurLine)
        {
            if (pCurLine != pLine)
            {
                pLine = pCurLine;
                ptLineOffset = pLine->GetTotalOffset();
            }
        }


        POINT2D ptOffset;
        ptOffset.dX = double(dDir * ( ptLineOffset.x + pOutFont->ptOffset.x));
        ptOffset.dY = double(       (-ptLineOffset.y + pOutFont->ptOffset.y));

        double dY = 0.0;

        UINT format = GGO_NATIVE | GGO_BEZIER | GGO_UNHINTED;
        GLYPHMETRICS gm;
        const MAT2 mat2 = {{0,1},{0,0},{0,0},{0,1}};
        DWORD rSize = ::GetGlyphOutline(hDc, chOut[0], format, &gm, 0, NULL, &mat2);

        //原点の位置がずれるための調整
        //
        //ptOffset.dX +=  gm.gmptGlyphOrigin.x;
        ptOffset.dY +=  tm.tmDescent;

        if((rSize != GDI_ERROR) && (rSize > 0))
        {
            std::vector<char> buf;
            buf.resize(rSize);
        
            DWORD rSize2 = ::GetGlyphOutline(hDc, chOut[0], format, &gm, rSize, &buf[0], &mat2);

#if DBG_CONV_SPLINE
            SIZE  sizeTxt;
            BOOL bRet1 = GetTextExtentPoint32(hDc, &chOut[0], 1, &sizeTxt);

            DB_PRINT(_T("_ConvSpline %c\n"), chOut[0]);
            DB_PRINT(_T("  Scl:%f offset(%f,%f) size(%d, %d)\n"), dScl, ptOffset.dX, ptOffset.dY,
                sizeTxt.cx, sizeTxt.cy);
#endif 

            std::vector<LINE_LIST> lstAny;
            _ConvSpline( &lstAny, &buf, ptOffset, pView, iLayerId);              
            plst->push_back(lstAny);

        }
    }

    HFONT hBeforFont = 0;
    if (workData.hOldFont != NULL)
    {
        hBeforFont = (HFONT)::SelectObject(hDc, workData.hOldFont);
    }

    for (lCnt = 0; lCnt < lLen; lCnt++)
    {
        if (workData.outFont[lCnt].bCreateFont)
        {
            ::DeleteObject(workData.outFont[lCnt].hFont);
            if (m_dAngle != 0)
            {
                ::DeleteObject(workData.outFont[lCnt].hFontS);
            }
        }
    }

#if DBG_CONV_SPLINE
    DB_PRINT(_T("------------_ConvSpline End\n"));
    DB_PRINT(_T("\n"));
#endif 

}

/**
*  @brief   文字アライメント設定
*  @param   [in]  ePos    アライメント種別
*  @retval  なし
*  @note
*/
void CExtText::SetAlign(ALIGNMENT_TYPE ePos)
{
    m_eAlignment = ePos;
}

/**
*  @brief   文字アライメント取得
*  @param   なし
*  @retval  アライメント種別
*  @note
*/
ALIGNMENT_TYPE CExtText::GetAlign() const
{
    return m_eAlignment;
}


/**
*  @brief   基準点位置設定
*  @param   [in]  ePos    アライメント種別
*  @retval  なし
*  @note
*/
void CExtText::SetDatum(POS_TYPE ePos)
{
    m_eDatum = ePos;
}

/**
*  @brief   文字基準点位置取得
*  @param   なし
*  @retval  アライメント種別
*  @note
*/
POS_TYPE CExtText::GetDatum() const
{
    return m_eDatum;
}

//!< 基準種別設定
void CExtText::SetBase(BASE_TYPE eBase)
{
    m_eBase = eBase;
}

//!< 基準点種別取得
BASE_TYPE CExtText::GetBase() const
{
    return m_eBase;
}


//!<枠表示可否取得        
bool CExtText::IsEnableFrame() const
{
    return m_bUseFrame;
}

//!<枠表示可否設定        
void CExtText::SetFrame(bool bUse)
{
    m_bUseFrame = bUse;
}

TEXT_FRAME* CExtText::Frame() const
{
    return m_psTextFrame.get();
}


int CExtText::_ConvPitchToScreen(double dPitch, double dScl, const CDrawingView* pView) const
{
    int iPitch;
    iPitch = CUtil::Round(dPitch * (dScl * pView->GetDpu()));
    return iPitch;
}

double CExtText::_ConvScreenToPitch(int iScreen, double dScl, const CDrawingView* pView) const
{
    double dPitch;
    if (fabs(dScl) > NEAR_ZERO)
    {
        dPitch = iScreen / (pView->GetDpu() * dScl);
    }
    else
    {
        dPitch = DBL_MAX;
    }
    return dPitch;
}


/**
*  @brief   フォント作成
*  @param   [in]  pData    フォントデータ
*  @param   [in]  dScl     倍率
*  @param   [in]  hDC      デバイスコンテキスト
*  @retval  フォント
*  @note
*/
HFONT CExtText::CreateFont(const FONT_DATA* pData, double dScl, bool bBold, HDC hDC, bool bPrint, bool bAngle) const
{
    //bBoldは廃止する
    HFONT hFont;
    int iHeight;
    int iAngle;

    double dRatio = 1.0;

    double dY = GetDeviceCaps(hDC, LOGPIXELSY);
    if (bPrint)
    {
        double dPrintY = THIS_APP->GetPrintPpiY(); //1インチあたりのピクセル
        dRatio = (dPrintY / dY);
    }

    if (m_bFixDisp)
    {
        dScl = -1;
    }

    iHeight = int(-dScl * dRatio * pData->dHeight * dY / 72.0/*25.4*/);

 #if DBG_CONV_SPLINE
    DB_PRINT(_T("CreateFont scl:%f dRatio:%f dHeight:%f dY:%f iHeight:%d\n"), dScl, dRatio ,pData->dHeight, dY , iHeight);
 #endif 

    //iHeight = pData->fntData.lfHeight * dScl;
    //上付、下付はテキストの高さを半分にする
    if ((pData->uiStyle & UPPER) ||
        (pData->uiStyle & LOWER))
    {
        iHeight = iHeight / 2;
    }

    pData->fntData.lfOrientation;

    if (bAngle)
    {
        iAngle = int(m_dAngle * 10.0);
    }
    else
    {
        iAngle = 0;
    }


    int iWidth = 0;
    if (!CUtil::IsEqual(m_dSclX, m_dSclY))
    {
        //アスペクト比が１ではないときは幅を設定する
        StdString strFontName = pData->fntData.lfFaceName;
        int iAberage = GetAverageFontWidth(hDC, iHeight, strFontName);
        iWidth = CUtil::Round(iAberage * m_dSclX);
    }
    iHeight = CUtil::Round(iHeight * m_dSclY);

    if (iHeight == 0)
    {
        iHeight = 1;
    }

    int iWeight = 0;
    if (pData->fntData.lfWeight != 0)
    {
        iWeight = pData->fntData.lfWeight + 300;
    }

    hFont = ::CreateFont(
        iHeight,               // フォントの高さ
        iWidth,                // 平均文字幅
        iAngle,                // 文字送り方向の角度
        iAngle,                           // ベースラインの角度
        iWeight,                          // フォントの太さ
        pData->fntData.lfItalic,          // 斜体にするかどうか
        pData->fntData.lfUnderline,       // 下線を付けるかどうか
        pData->fntData.lfStrikeOut,       // 取り消し線を付けるかどうか
        pData->fntData.lfCharSet,         // 文字セットの識別子
        pData->fntData.lfOutPrecision,    // 出力精度
        pData->fntData.lfClipPrecision,   // クリッピング精度
        pData->fntData.lfQuality,         // 出力品質
        pData->fntData.lfPitchAndFamily,  // ピッチとファミリ
        pData->fntData.lfFaceName         // フォント名
    );
    return hFont;
}

void CExtText::_CreateBlock(StdChar ch, int iPitch, 
    const ESC_BLOCK_MODIFER& m, 
    FONT_WORK_DATA* pWork,
    FONT_LINE_DATA** ppL, 
    BLOCK_DATA** ppB) const
{
    FONT_LINE_DATA* pLine;
    pLine = *ppL;
    BLOCK_DATA* pCurBlock = *ppB;


    pLine->lstBlock.push_back(std::shared_ptr<BLOCK_DATA>());
    int iBlock = SizeToInt(pLine->lstBlock.size()) - 1;

    BLOCK_DATA bd;
#if DBG_PRINT
    DB_PRINT(_T("iOffsetX %d Block:%d\n"), bd.iOffsetX, iBlock);
#endif
    bd.sameCh = ch;
    pLine->lstBlock[iBlock] = std::make_shared<BLOCK_DATA>(bd);

    BLOCK_DATA* pNextBlock = (*ppL)->lstBlock[iBlock].get();
    pNextBlock->iBlockNo = iBlock;

    if (!(m.dwSts & BSA_LEFT))
    {
        pNextBlock->iOffsetX = ((*ppL)->iLineWidth + (*ppL)->iLineFront - iPitch);
    }
    else
    {
        if(iBlock == 0)
        {
            pNextBlock->iOffsetX = 0;
        }
        else
        {
            pNextBlock->iOffsetX =  pLine->lstBlock[iBlock-1]->iOffsetX;
        }    
    }

    pNextBlock->szModifer = m;
    pNextBlock->pParentLine = pLine;
    pWork->stackBlock.push_front(pNextBlock);

#if DBG_PRINT

    StdStringStream strmName; 
    if (pNextBlock->pParentLine)
    {
        strmName << *pNextBlock->pParentLine->pName;
        strmName << _T("-");
    }
    strmName << _T("B:");
    strmName << iBlock;
    *(pNextBlock->pName) = strmName.str();

#endif
    pNextBlock->AddLine();

    *ppB = pNextBlock;
    *ppL = pNextBlock->GetCurLine().get();

}


void CExtText::_CloseBlock(ESC_BLOCK_MODIFER* pCm, 
    FONT_WORK_DATA* pWork,
    int iPL, 
    FONT_LINE_DATA** ppL, 
    BLOCK_DATA** ppB) const
{
    //最終行のY座標を確定する

    BLOCK_DATA* pCurBlock = *ppB;

#if  DBG_PRINT
    DB_PRINT(_T("_CloseBlock B[%s][%lx] \n"), (*ppB)->pName->c_str(), pCurBlock);
#endif
    ESC_BLOCK_MODIFER* pM  = &pCurBlock->szModifer;

    STD_ASSERT(pCurBlock->CloseAllLine(iPL, FIRST_BASE1));

    if ((fabs(pM->dY) > NEAR_ZERO) ||
        (fabs(pM->dX) > NEAR_ZERO)||
        (pM->dwSts & BSB_BLOCK))

    {
        //エスケープシーケンスによる位置変更がある場合

        SIZE  szBase; //Escオフセット位置の基準となるサイズ
                      //前のブロックがある場合は前のブロックサイズ
                      //前のブロックがない場合は先頭のフォントを基準とする

        szBase.cx = 0;
        szBase.cy = 0;
        int iOffset = 0;
        int iHeight = pCurBlock->iHeight;

        BLOCK_DATA* pDummyBlock = NULL;
        BLOCK_DATA* pBeforeBlock  = _GetBeforeBlock(pCurBlock, pM->iBeforeOffset);
        if( pBeforeBlock)
        {
            //前のブロックを使用
            szBase.cx = pBeforeBlock->iWidth;
            szBase.cy = pBeforeBlock->iHeight;
        }
        else
        {
            //前のブロックがない場合 X方向側の設定は不可
            //                       Y方向は先頭フォントのスケール変更前の値を使用する 
            szBase = pCurBlock->lstLine[0]->GetTopFontSize();
            if (fabs(pM->dScl) > NEAR_ZERO)
            {
                //フォントスケールを変更している場合は、スケール変更する前のサイズを基準とする
                iHeight = szBase.cy;

                double  dScl = 1 / (pCurBlock->GetFontScl());
                szBase.cx = CUtil::Round(double(szBase.cx) * dScl);
                szBase.cy = CUtil::Round(double(szBase.cy) * dScl);

                pDummyBlock = new  BLOCK_DATA(*pCurBlock);
                pDummyBlock->iHeight =  szBase.cy;
                pBeforeBlock =  pDummyBlock;
            }
        }

        pCurBlock->ptEscOffset.x =  CUtil::Round(szBase.cx * pM->dX);
        pCurBlock->ptEscOffset.y =  CUtil::Round(szBase.cy * pM->dY);

        if(pBeforeBlock)
        {
            if(pM->dwSts & BSB_LEFT)
            {
                pCurBlock->iOffsetX =  pBeforeBlock->iOffsetX;
            }
            else if(pM->dwSts & BSB_MID)
            {
                int iCenter = CUtil::Round((pBeforeBlock->iWidth - pCurBlock->iWidth) / 2.0);
                pCurBlock->iOffsetX =  pBeforeBlock->iOffsetX + iCenter;
            }
            else if(pM->dwSts & BSB_RIGHT)
            {
                pCurBlock->iOffsetX =  pBeforeBlock->iOffsetX + pBeforeBlock->iWidth;
            }

            if(pM->dwSts & BSB_OVER_ROOF)
            {
                pCurBlock->ptEscOffset.y +=   - pBeforeBlock->iHeight;
            }
            else if(pM->dwSts & BSB_ROOF)
            {
                pCurBlock->ptEscOffset.y +=   - pBeforeBlock->iHeight + iHeight;
            }
            else if(pM->dwSts & BSB_V_CENTER)
            {
                int iCenter = CUtil::Round((iHeight - pBeforeBlock->iHeight) / 2.0);

                pCurBlock->ptEscOffset.y +=  iCenter;
            }
            else if(pM->dwSts & BSB_GROUND)
            {
                //pCurBlock->ptEscOffset.y +=  pBeforeBlock->iBottom - pCurBlock->iBottom;
            }
            else if(pM->dwSts & BSB_UNDER_GROUND)
            {
                pCurBlock->ptEscOffset.y +=   iHeight;
            }

            if(pM->dwSts & BSB_X_BLOCK)
            {
                //pCurBlock->ptEscOffset.x -= pBeforeBlock->iFront - pCurBlock->iFront;
                pCurBlock->ptEscOffset.x += pBeforeBlock->ptEscOffset.x;
            }

            if(pM->dwSts & BSB_Y_BLOCK)
            {
                pCurBlock->ptEscOffset.y += pBeforeBlock->iBottom - pCurBlock->iBottom;
                pCurBlock->ptEscOffset.y += pBeforeBlock->ptEscOffset.y;
            }

        }

        if(pDummyBlock)
        {
            delete pDummyBlock;
        }
    }
    else
    {
        BLOCK_DATA* pBeforeBlock  = _GetBeforeBlock(pCurBlock, 1);

        if(pBeforeBlock)
        {



        }
    }

    //このブロックを所有している行のサイズ更新
    if (pCurBlock->pParentLine)
    {
        bool bChangeBase = true;
        pCurBlock->pParentLine->ExpandSize(pCurBlock, bChangeBase);
    }


    FONT_LINE_DATA* pOldLine = *ppL;
    BLOCK_DATA* pOldBlock = pCurBlock;

    pWork->stackBlock.pop_front();

    if(!pWork->stackBlock.empty())
    {
        BLOCK_DATA* pParentBlock;
        FONT_LINE_DATA* pParentBlockLine;
        pParentBlock = pWork->stackBlock[0];
        pParentBlockLine = pParentBlock->GetCurLine().get();


        *pCm = pParentBlock->szModifer;
        *ppB = pParentBlock;
        *ppL = pParentBlockLine;

#if DBG_PRINT
        DB_PRINT(_T(" Next Bloc[%s]d\n"), (*ppB)->pName->c_str());
        DB_PRINT(_T(" Next Line[%s]d\n"), (*ppL)->pName->c_str());
        DB_PRINT(_T("\n"));
#endif

    }

#if DBG_PRINT
    DB_PRINT(_T("START pCurrentX %d:%d\n"), (*ppL)->iLineWidth);
    DB_PRINT(_T("--Close block END --\n"));
    DB_PRINT(_T("\n"));
#endif


}

//
/**
*  @brief   前のブロックを取得
*  @param   [in]  pBlock 現在のブロック
*  @param   [in]  hDC   デバイスコンテキスト
*  @param   [in]  dScl  倍率
*  @retval  元々設定されていたフォント
*  @note    pWorkに入ったフォントは後で削除する必要がある
*/
BLOCK_DATA* CExtText::_GetBeforeBlock(BLOCK_DATA* pBlock,  int iBeforeNum) const
{
    BLOCK_DATA* pRet = NULL;
    auto pParentBlock = pBlock->GetParentBlock();

    if(!pParentBlock)
    {
        return NULL;
    }

    auto pParentLine = pParentBlock->GetCurLine();
    if(!pParentLine)
    {
        return NULL;
    }

    if(iBeforeNum < 0)
    {
        return NULL;
    }


    int iBlockSize = SizeToInt(pParentLine->lstBlock.size());

    int iCallBlock = iBlockSize - iBeforeNum - 1;

    if (( iCallBlock  >= 0 ) &&
        ( iBlockSize  > iCallBlock ))
    {
        //前のブロック
        pRet = pParentLine->lstBlock[iCallBlock].get();
    }
    return pRet;
}



/**
*  @brief   表示テスト
*  @param   [out] pWork 表示データ
*  @param   [in]  hDC   デバイスコンテキスト
*  @param   [in]  dScl  倍率
*  @retval  元々設定されていたフォント
*  @note    pWorkに入ったフォントは後で削除する必要がある
*/
HFONT CExtText::DrawTest(FONT_WORK_DATA* pWork,
    HDC hDc, const CDrawingView* pView, double dScl, bool bEmphasis) const
{
    /*
    現状では文字ごとにフォントデータを持っていて
    書き込みも１文字ごとに行うようになっている
    冗長なので、いずれ修正する予定

    FONT_DATA      文字毎
    FONT_OUT_DATA  文字毎  (フォントの生成は変化があったときのみ)
    FONT_LINE_DATA 行毎

    */

    //１行目を原点とするモード 
    //最下行を原点とするモード
    BASE_TYPE eBase = FIRST_BASE1;
    FONT_DATA OldFont;
    HFONT hFont = 0;
    HFONT hOldFont = 0;
    HFONT hBeforFont = 0;
    StdChar  chOut;
    StdChar  chOld;
    SIZE     sizeTxt;
    long     lLine = 0;
    long     lMaxHeight = 0;

#if DBG_PRINT
    (*pWork->block.pName) = _T("Root");
#endif
    long lFontDataNum = (long)m_FontData.size();

    if (lFontDataNum == 0)
    {
        return hFont;
    }

    pWork->outFont.resize(lFontDataNum);
    pWork->sizeDrawArea.cx = 0;           //全体の表示領域
    pWork->sizeDrawArea.cy = 0;
    lLine = 0;
    ESC_BLOCK_MODIFER eM;
    int iOrgFontHeight = 0;

    bool bEscFontEnd = false;

    eM.Clear();
    chOld = 0;

    ESC_STS eStateOld = ESC_NONE;
    ESC_STS eState = ESC_START;

    /*
    ppL in 現在の行(ここに新しいブロックを追加) 
    out 新しい行(新しいブロック内の行)

    ppB 新しいブロック

    */




    //--------------------------------------
    int iOffsetY = 0;
    int lLineOld = 0;



    int iCurLine = 0;
    FONT_LINE_DATA* pCurLine;
    BLOCK_DATA* pCurBlock;
    pCurBlock = &pWork->block;

    pWork->stackBlock.push_front(pCurBlock);

    int iPitchOrg = _ConvPitchToScreen(m_dPitch, dScl, pView);
    int iPitchLineOrg = _ConvPitchToScreen(m_dLinePitch, dScl, pView);
    int iPitch;
    int iPitchLine = iPitchLineOrg;

    //----------------------
    //折り返し位置の計算
    //----------------------
    bool bWrap = false;
    int  iWrapWidth = 0;
    int  iInnerHeight = 0;
    if (IsEnableFrame())
    {
        double dWrapWidth;
        double dInnerHeight;
        dWrapWidth = m_psTextFrame->GetWidth();
        dInnerHeight = m_psTextFrame->GetHeight();

        if (m_psTextFrame->IsUseMargin())
        {
            dWrapWidth -= m_psTextFrame->Margin()->dLeft;
            dWrapWidth -= m_psTextFrame->Margin()->dRight;

            dInnerHeight -= m_psTextFrame->Margin()->dTop;
            dInnerHeight -= m_psTextFrame->Margin()->dBottom;
        }
        //
        int iMargin = 5;//dot単位 文字サイズのゆらぎに対する余裕分、大きくする

        double dMargin = _ConvScreenToPitch(iMargin, dScl, pView);

        iWrapWidth = _ConvPitchToScreen(dWrapWidth + dMargin, dScl, pView);
        iInnerHeight = _ConvPitchToScreen(dInnerHeight + dMargin, dScl, pView);

        if (m_psTextFrame->IsTextWrap() &&
            !m_psTextFrame->IsAutoFit())
        {
            bWrap = true;
        }
    }
    //----------------------
    bool bCreateFont = true;  //フォント生成


                              //基本のフォントの高さを取得しておく
    int iDefaultFontHeight = 0;
    {
        TEXTMETRIC tm;
        const FONT_DATA* pData = m_FontData[0].get();
        static const bool bAngle = false;
        HFONT hTmpFont = CreateFont(pData, dScl, false, hDc, pWork->bPrint, bAngle);
        hBeforFont = (HFONT)::SelectObject(hDc, hTmpFont);
        ::GetTextMetrics(hDc, &tm);

        hBeforFont = (HFONT)::SelectObject(hDc, hOldFont);
        ::DeleteObject(hTmpFont);
        // 平均の文字の高さを保存
        iDefaultFontHeight = tm.tmHeight;
    }

    GRID_STS eGrid = GRID_NONE;
    bool bContinueEsc = false;
    for (long lFontNo = 0; lFontNo < lFontDataNum; lFontNo++)
    {
        chOut = m_strOut[lFontNo];
        pCurLine = pCurBlock->GetCurLine().get();
        bool bStartBlock = false;


#if DBG_PRINT | DBG_CONV_SPLINE
        DB_PRINT(_T(" [%s]\n"),CUtil::ConvAscii(&chOut).c_str());
#endif
        if (pCurBlock->sameCh == 0)
        {
            iPitch = iPitchOrg;
            iPitchLine = iPitchLineOrg;
        }
        else
        {
            // カッコが設定されていた場合
            iPitch = 0;     //文字間隔
            iPitchLine = 0; //行間隔
        }

        switch (eState)
        {
        case ESC_COMMAND:
        {
            PARSE_DATA p;
            if(p.Parse(lFontNo, &m_strOut, &eM))
            {
                lFontNo = p.GetPos() - 1;
                eState = ESC_AFTER;
                continue;
            }

            break;
        }
        case ESC_NONE_BLOCK:
        case ESC_RETURN:
        case ESC_START:
        case ESC_NONE:
        case ESC_BLOCK:
        case ESC_LBLOCK:
        case ESC_ONEBLOCK:
        case ESC_CLOSE:
        {
            if (chOut == _T('\\'))      
            {
                eM.Clear();      eState = ESC; 
            }
            else if (chOut == _T('%'))  
            { 
                if (eState == ESC_NONE_BLOCK)
                {
                    _CloseBlock(&eM, pWork, iPitchLine, &pCurLine, &pCurBlock);
                }
                else if(!bContinueEsc)
                {
                    eM.Clear();
                }
                eState = ESC; 
            }
            else if (chOut == _T('\"')) { eState = DBL_QUOTE; }
            else if (chOut == _T('\'')) { eState = QUOTE; }
            else if (chOut == _T('\n')) { eState = ESC_RETURN; }
            else if ((chOut == _T('|')) || (chOut == _T(',')))
            {
                if (eGrid == GRID_READY_TO_CREATE)
                {
                    //グリッドブロック内は新たにブロックを生成する
                    eState = ESC_LBLOCK;
                    bContinueEsc = false;
                    //グリッド生成フラグは消しておく
                    eM.dwSts &= ~(BS_GRID | BS_NO_GRID_LINE);
                    _CreateBlock(chOut, iPitch, eM, pWork, &pCurLine, &pCurBlock);
                    eM.Clear();
                    lMaxHeight = 0;
                    eGrid = GRID_INNER_BLOCK;
                }
                else if (eGrid == GRID_INNER_BLOCK) 
                {
                    if(pCurBlock->sameCh == _T(' '))
                    {
                        int a = 10;
                    }
                    eGrid = GRID_READY_TO_CREATE;
                    _CloseBlock(&eM, pWork, iPitchLine, &pCurLine, &pCurBlock);

                    lMaxHeight = pCurLine->iLineHeight;
                    bCreateFont = true;
                    eState = ESC_START;
                }
                else
                {
                    eState = ESC_NONE;
                }
            }
            else if (chOut == _T('{'))
            {
                eState = ESC_PBLOCK;

            }
            else if ((chOut == _T('[')) &&
                (eGrid == GRID_READY_TO_CREATE))
            {
                eState = ESC_LBLOCK;
                bContinueEsc = false;

                //グリッド生成フラグは消しておく
                eM.dwSts &= ~(BS_GRID | BS_NO_GRID_LINE);
                _CreateBlock(']', iPitch, eM, pWork, &pCurLine, &pCurBlock);
                eM.Clear();       //グリッド時はここでクリア
                lMaxHeight = 0;
                eGrid = GRID_INNER_BLOCK;

            }
            else if (chOut == _T(']'))
            {
                bool bCloseMainBlock = false;
                if (chOut == pCurBlock->sameCh)
                {
                    bCloseMainBlock = true;
                }

                if (pCurBlock->sameCh == _T(' '))  
                {
                    if (eGrid == GRID_INNER_BLOCK)
                    {
                        eGrid = GRID_READY_TO_CREATE;
#if DBG_PRINT
                        DB_PRINT(_T("eGrid == GRID_INNER_BLOCK2 \n"));
#endif

                        _CloseBlock(&eM, pWork, iPitchLine, &pCurLine, &pCurBlock);
                        lMaxHeight = pCurLine->iLineHeight;
                        bCreateFont = true;
                        bCloseMainBlock = true;
                    }
                }

                if (bCloseMainBlock)
                {
                    if (eGrid == GRID_INNER_BLOCK)
                    {
                        eGrid = GRID_READY_TO_CREATE;
                    }
                    else
                    {
                        if ((pCurBlock->szModifer.dwSts & BS_GRID) ||
                            (pCurBlock->szModifer.dwSts & BS_NO_GRID_LINE))  
                        {
                            eGrid = GRID_NONE;
                        }
                        else
                        {
                            eGrid = GRID_READY_TO_CREATE;
                        }
                    }
#if DBG_PRINT
                    DB_PRINT(_T("bCloseMainBlock \n"));
#endif

                    _CloseBlock(&eM, pWork, iPitchLine, &pCurLine, &pCurBlock);
                    eState = ESC_CLOSE;

                    lMaxHeight = pCurLine->iLineHeight;

                    bCreateFont = true;
                }
            }
            else
            {

                bool bNone = true;
                if ((pCurBlock->szModifer.dwSts & BS_GRID) ||
                    (pCurBlock->szModifer.dwSts & BS_NO_GRID_LINE))  
                {
                    if (eGrid == GRID_READY_TO_CREATE)
                    {
                        bContinueEsc = false;
                        //グリッド生成フラグは消しておく
                        eM.dwSts &= ~(BS_GRID | BS_NO_GRID_LINE);
                        _CreateBlock( _T(' '), iPitch, eM, pWork, &pCurLine, &pCurBlock);
                        eM.Clear();
                        lMaxHeight = 0;
                        eGrid = GRID_INNER_BLOCK;
                    }
                }
                else if(eState == ESC_START)
                {
                    bContinueEsc = false;
                    _CreateBlock( _T(' '), iPitch, eM, pWork, &pCurLine, &pCurBlock);
                    eM.Clear();
                    lMaxHeight = 0;
                    eState = ESC_NONE_BLOCK;
                    bNone = false;
                }

                if(bNone)
                {
                    eState = ESC_NONE;
                }
            }
        }
        break;

        case ESC_PBLOCK:
            if (chOut == _T('}')) { eState = ESC_NONE; }
            else
            {
                //TODO:パラメータブロック処理
            }
            break;

        case DBL_QUOTE_ESC:
            eState = DBL_QUOTE;
            break;

        case DBL_QUOTE:
            if (chOut == _T('\"'))
            {
                if((chOld != _T('\\')) &&  (chOld != _T('%')))
                {
                    eState = ESC_NONE; 
                }
            }
            else if (chOut == _T('\n'))
            {
                eState = ESC_RETURN;
            }
            else if (chOut == _T('\\'))
            {
                eState = DBL_QUOTE_ESC;
            }
            break;

        case QUOTE_ESC:
            eState = QUOTE;
            break;

        case QUOTE:
            if (chOut == _T('\'')) 
            {
                if((chOld != _T('\\')) &&  (chOld != _T('%')))
                {
                    eState = ESC_NONE; 
                }
            }
            else if (chOut == _T('\n')) 
            { 
                eState = ESC_RETURN; 
            }
            else if(chOut == _T('\\'))
            {
                eState = QUOTE_ESC;
            }
            break;


        case ESC:
        {
            if (!bContinueEsc)
            {
                eState = ESC_AFTER;
            }
            bool bBB = false;
            int iMulti = 1;
            if ((chOut >= _T('1')) && (chOut <= _T('9')))
            {
                iMulti =  chOut - _T('1');
                chOut =  eM.chOffset;

                if(eM.bBeforeOffset)
                {
                    iMulti++;
                    eM.iBeforeOffset = iMulti;
                    iMulti = 1;
                    eM.bBeforeOffset = false;
                    bBB = true;
                }
            }

            ESC_BLOCK_MODIFER e;
            if      (chOut == _T('^')) { e.dScl -= 0.5; e.bUse = true; eM.dwSts |= BSB_ROOF, eM.bBeforeOffset = true;}   //FontSize -50% 上方向 75%
            else if (chOut == _T('.')) { e.dScl -= 0.5; e.bUse = true; eM.dwSts |= BSB_GROUND, eM.bBeforeOffset = true; }   //FontSize -50% 下方向 25%
            else if (chOut == _T('~')) { e.dScl -= 0.5; e.bUse = true; eM.dwSts |= BSB_OVER_ROOF, eM.bBeforeOffset = true; }              //              上方向 100%
            else if (chOut == _T('u')) { e.dY -= 0.25;    e.bUse = true; }              //              上方向 25%
            else if (chOut == _T('U')) { e.dY -= 0.5;     e.bUse = true; }              //              上方向 50%
            else if (chOut == _T('d')) { e.dY += 0.25;    e.bUse = true; }              //              下方向 25%
            else if (chOut == _T('D')) { e.dY += 0.5;     e.bUse = true; }              //              下方向 50%
            else if (chOut == _T('l')) { e.dScl +=0.25;   e.bUse = true; }                 //FontSize +25%
            else if (chOut == _T('L')) { e.dScl +=0.5;    e.bUse = true; }                //FontSize +50%
            else if (chOut == _T('s')) { e.dScl -=0.25;   e.bUse = true; }                 //FontSize -25%
            else if (chOut == _T('S')) { e.dScl -=0.5;    e.bUse = true; }                //FontSize -50%
            else if (chOut == _T('b')) { e.dX   -=0.25;   e.bUse = true; }                   //              後方向 25%   
            else if (chOut == _T('B')) { e.dX   -=0.5;    e.bUse = true; }                  //              後方向 50%
            else if (chOut == _T('f')) { e.dX   +=0.25;   e.bUse = true; }                   //              前方向 25%
            else if (chOut == _T('F')) { e.dX   +=0.5;    e.bUse = true; }                  //              前方向 50%

            //前のブロックに対しての位置指定(数値を指定して、「前の前」のブロック等設定できる)
            else if (chOut == _T('h')) { eM.dwSts |= BSB_LEFT ; eM.bBeforeOffset = true; bBB = true;}           // 左寄せ
            else if (chOut == _T('m')) { eM.dwSts |= BSB_MID  ; eM.bBeforeOffset = true; bBB = true;}           // 中央
            else if (chOut == _T('t')) { eM.dwSts |= BSB_RIGHT; eM.bBeforeOffset = true; bBB = true;}           // 中央

            else if (chOut == _T('R')) { eM.dwSts |= BSB_OVER_ROOF         ; eM.bBeforeOffset = true; bBB = true;}    
            else if (chOut == _T('r')) { eM.dwSts |= BSB_ROOF              ; eM.bBeforeOffset = true; bBB = true;}    
            else if (chOut == _T('c')) { eM.dwSts |= BSB_V_CENTER          ; eM.bBeforeOffset = true; bBB = true;}    
            else if (chOut == _T('g')) { eM.dwSts |= BSB_GROUND            ; eM.bBeforeOffset = true; bBB = true;}    
            else if (chOut == _T('G')) { eM.dwSts |= BSB_UNDER_GROUND      ; eM.bBeforeOffset = true; bBB = true;}    

            if(e.bUse)
            {
                eM.dX += (e.dX *  iMulti);
                eM.dY += (e.dY *  iMulti);
                eM.dScl += (e.dScl *  iMulti);
                eM.bUse = true;
                eM.chOffset = chOut;
            }
            else if(!bBB)
            {
                if (chOut == _T('#'))
                {
                    eM.dwSts |= BS_GRID;
                }           // グリッド描画
                else if (chOut == _T('*')) { eM.dwSts |= BS_NO_GRID_LINE; }   // グリッド枠線なし
                else if (chOut == _T('='))
                {
                    eM.dwSts |= BS_FRAME;
                }          // ブロック枠描画
                else if (chOut == _T('_')) { eM.dwSts |= BS_UNDERLINE; }      // ブロック下線描画
                else if (chOut == _T('<')) { eM.dwSts |= BSA_LEFT; }          // 左寄せ
                else if (chOut == _T('>')) { eM.dwSts |= BSA_RIGHT;}         // 右寄せ
                else if (chOut == _T(':')) { eM.dwSts |= BSA_MID; }           // 中央
                else if (chOut == _T('['))
                { 
                    eState = ESC_LBLOCK;
                    bContinueEsc = false;
                    _CreateBlock(_T(']'), iPitch, eM, pWork, &pCurLine, &pCurBlock);
                    eM.Clear();
                    lMaxHeight = 0;
                    bStartBlock = true;
                }           // 中央
                else if (chOut == _T('n'))
                {
                    chOut = _T('\n');
                    eState = ESC_RETURN;
                }
                else if (chOut == _T('%'))
                {
                    bContinueEsc = true;
                    eState = ESC;
                }
                else if (chOut == _T('$'))
                {
                    eState = ESC_COMMAND;
                }
                else
                {
                    eState = ESC_NONE;
                }
            }
            break;
        }
        case ESC_AFTER:
            //エスケープシーケンスの効果は後の１文字か
            //[]で囲まれた範囲
        {
            char ch;

            if      (chOut == _T('\\')) { eState = ESC; }
            else if (chOut == _T('%'))  { eState = ESC; }
            else if (chOut == _T('['))
            {
                ch = _T(']');
                eState = ESC_LBLOCK;

                if (eGrid == GRID_READY_TO_CREATE)
                {
                    eGrid = GRID_INNER_BLOCK;
                }

                if ((eM.dwSts & BS_GRID) ||
                    (eM.dwSts & BS_NO_GRID_LINE))
                {
                    eGrid = GRID_READY_TO_CREATE;
                }

                bStartBlock = true;
            }
            else
            {
                ch = 0;
                eState = ESC_ONEBLOCK;
                lMaxHeight = 0;
                bStartBlock = true;
            }

            if (bStartBlock)
            {
                bContinueEsc = false;
                _CreateBlock(ch, iPitch, eM, pWork, &pCurLine, &pCurBlock);
                eM.Clear();
                lMaxHeight = 0;
            }
        }
        break;
        }

        bool bS = false;  //文字表示
        bool bT = false;  //行頭
        bool bSetHeight = false;
        switch (eStateOld)
        {
        case ESC_START:
            if (eState == ESC_NONE || eState ==ESC_NONE_BLOCK) { bCreateFont = true; bS = true; bT = true; }
            else if (eState == ESC_RETURN) { bSetHeight = true; bS = true; bT = true; }

            break;

        case ESC_CLOSE:
        case ESC_NONE_BLOCK:
        case ESC_NONE:
            if (eState == ESC_NONE || eState ==ESC_NONE_BLOCK) { bS = true; }
            break;

        case ESC:
            break;

        case ESC_AFTER:
            if (eState == ESC_ONEBLOCK) { bS = true; bT = true; }
            break;

        case QUOTE_ESC:
            if (eState == QUOTE) { bS = true; }
            break;

        case QUOTE:
            if (eState == QUOTE) { bS = true; }
            break;

        case DBL_QUOTE_ESC:
            if (eState == DBL_QUOTE){bS = true; }
            break;

        case DBL_QUOTE:
            if (eState == DBL_QUOTE){bS = true; }
            break;

        case ESC_BLOCK:
            if (eState == ESC_NONE ) { bS = true; bT = true; }
            break;

        case ESC_LBLOCK:
            if (eState == ESC_NONE) { bS = true; bT = true; }
            break;

        case ESC_ONEBLOCK:
            if      (eState == ESC)       { bS = false; bT = true; }
            else if (eState == ESC_START) { bS = false; bT = true; }
            else if (eState == ESC_CLOSE) { bS = false; bT = true; }
            else               { bS = true;  bT = false; }
            break;

        case ESC_RETURN:
            if (eState == ESC_NONE) { bS = true; }
            else if (eState == ESC_RETURN)
            {
                bSetHeight = true; bS = true; bT = true;
            }
            break;
        }

        pWork->outFont[lFontNo].bCreateFont = false;
        pWork->outFont[lFontNo].bDisp = false;
        pWork->outFont[lFontNo].bStartBlock = bStartBlock;
        pWork->outFont[lFontNo].pLine = pCurLine;

        eStateOld = eState;

        if ((!bS) && (chOut != _T('\n')))
        {
            //表示しない場合
            chOld = chOut;
            continue;
        }
        else
        {
            if (chOut != _T('\n'))
            {
                pWork->outFont[lFontNo].bDisp = true;
            }
        }

        //連続改行の場合高さ情報がないなめここで設定しておく
        if (bSetHeight)
        {
            if (pCurLine->iLineHeight == 0)
            {
                pCurLine->iLineHeight = iDefaultFontHeight;
            }
        }


        bool bChange = (pCurBlock->szModifer.bUse || pCurBlock->szModifer.dwSts != 0);
        FONT_DATA* pData = m_FontData[lFontNo].get();

        double dParentBlockScl = 1.0;
        if ((OldFont != *pData) ||
            bChange ||
            (bCreateFont && chOut != _T('\n'))
            )
        {
            if(pCurBlock->szModifer.dwSts & BS_STRIKE)
            {
                pData->fntData.lfStrikeOut = 1;
            }

            if(pCurBlock->szModifer.dwSts & BS_ITALIC)
            {
                pData->fntData.lfItalic = 1;
            }

            if(pCurBlock->szModifer.dwSts & BS_STRIKE)
            {
                pData->fntData.lfUnderline = 1;
            }

            if(pCurBlock->szModifer.dwSts & BS_BOLD)
            {
                pData->fntData.lfWeight = FW_BOLD;
            }

            if(pCurBlock->szModifer.dwSts & BS_COLOR)
            {
                pData->crColor =  pCurBlock->szModifer.cr;
            }

            if(pCurBlock->szModifer.dwSts & BS_FONTNAME)
            {
                _tcscpy( pData->fntData.lfFaceName, pCurBlock->szModifer.strFontName.c_str() );
            }

            double dBlockScl = pCurBlock->GetFontScl();

            double dDispScl = dScl * dBlockScl;
            bool bAngle = true;
            hFont = CreateFont(pData, dDispScl, false, hDc, pWork->bPrint, bAngle);

            if (m_dAngle != 0)
            {
                //フォントのサイズは回転していない状態を取得する
                //(回転したフォントは、角度にかかわらずY方向に1.3～1.4倍になるため）
                bAngle = false;
                HFONT hFontS = CreateFont(pData, dDispScl, false, hDc, pWork->bPrint, bAngle);
                pWork->outFont[lFontNo].hFontS = hFontS;
                hBeforFont = (HFONT)::SelectObject(hDc, hFontS);
            }
            else
            {
                pWork->outFont[lFontNo].hFontS = 0;
                hBeforFont = (HFONT)::SelectObject(hDc, hFont);
            }

            if (lFontNo == 0)
            {
                //最初のフォントを記憶
                hOldFont = hBeforFont;
            }


            pWork->outFont[lFontNo].hFont = hFont;
            pWork->outFont[lFontNo].bCreateFont = true;


            OldFont = *pData;
            bEscFontEnd = false;
            bCreateFont = false;
        }


        bool bLF = false;
        if (chOut == '\n')
        {
            bLF = true;
            sizeTxt.cx = 0;
            sizeTxt.cy = 0;//iDefaultFontHeight;
        }
        else
        {
            //文字の大きさを測る
            BOOL bRet1 = GetTextExtentPoint32(hDc, &chOut, 1, &sizeTxt);

#if DBG_CONV_SPLINE

            double dBlockScl = pCurBlock->GetFontScl();
            double dDispScl = dScl * dBlockScl;

            DB_PRINT(_T("QQ DrawTest %c\n"), chOut);
            DB_PRINT(_T("  Scl:%f dBlockScl:%f dDispScl:%f size(%d, %d)\n"), dScl, dBlockScl, dDispScl, sizeTxt.cx, sizeTxt.cy);
#endif 
            if (bWrap)
            {
                int iNewRight = (pCurLine->iLineWidth) + CUtil::Round(sizeTxt.cx / 2.0);

                if (iNewRight > iWrapWidth)
                {
                    //ラップ処理を行うのは、トップに属している行のみ
                    if (pCurLine->pParentBlock == &pWork->block)
                    {
                        bLF = true;
                    }
                }
            }
        }

        if(bT)
        {
#if DBG_PRINT
            DB_PRINT(_T("Block Top\n"));
#endif
            //先頭位置のフォントサイズを位置調整のため記憶しておく
            pCurLine->SetTopFontSize(sizeTxt);
        }

        //改行
        int iAddPitch = iPitch;
        if (bLF)                                  
        {
#if DBG_PRINT
            DB_PRINT(_T("Add Line\n"));
#endif            //
            pCurBlock->AddLine();
            pCurLine = pCurBlock->GetCurLine().get();
            iAddPitch = 0;
            lMaxHeight = 0;
        }


        if (sizeTxt.cy > lMaxHeight)
        {
            lMaxHeight = sizeTxt.cy;
        }



        //------------------------
        // 文字の大きさを登録
        //------------------------                          

        /*
        BLOCK_DATA* pParentBlock = pCurLine->pParentBlock;
        int iBeforeFront = 0;
        if (pParentBlock)
        {
            FONT_LINE_DATA* pLine = pParentBlock->pParentLine;
            if (pLine)
            {
                size_t szBlockNum = pLine->lstBlock.size();
                if (szBlockNum > 1)
                {
                    BLOCK_DATA* pBeforeBlock = NULL;
                    pBeforeBlock = pLine->lstBlock[szBlockNum - 2].get();
                    if (pBeforeBlock)
                    {
                        iBeforeFront = pBeforeBlock->iFront;
                    }
                }
            }
        }
        */

        pWork->outFont[lFontNo].ptOffset.x = pCurLine->iLineWidth + pCurLine->iLineFront /*- iBeforeFront*/;
        pWork->outFont[lFontNo].size = sizeTxt;
        //------------------------

        pCurLine->iLineWidth += iAddPitch;

        //------------------------
        // 行の大きさを登録
        //------------------------

        if(bT && lMaxHeight != 0)
        {
            pCurLine->iLineHeight = 0;
        }

        if (lMaxHeight > pCurLine->iLineHeight)
        {
            pCurLine->iLineHeight = lMaxHeight;
        }
        pCurLine->iLineWidth += (sizeTxt.cx);
        //------------------------

        //pCurBlock->iWidth += iAddPitch;

        pWork->outFont[lFontNo].hFont = hFont;
        pWork->outFont[lFontNo].pLine = pCurLine;


        if (eState == ESC_ONEBLOCK)
        {
            //最終行のY座標を確定する
#if DBG_PRINT
            DB_PRINT(_T("eState == ESC_ONEBLOCK  _CloseBlock\n"));
#endif
            bCreateFont = true;
            bContinueEsc = false;
            _CloseBlock(&eM, pWork, iPitchLine, &pCurLine, &pCurBlock);
        }

#if DBG_PRINT  | DBG_CONV_SPLINE

        DB_PRINT(_T("%c  H:%d V:%d (%d,%d)\n"), chOut, sizeTxt.cx, sizeTxt.cy,
            pWork->outFont[lFontNo].ptOffset.x,
            pWork->outFont[lFontNo].ptOffset.y);

#endif

#if DBG_PRINT_1
        DB_PRINT(_T("(x,y) = %03d, %03d  %x[%c] \n"), pWork->outFont[lFontNo].ptOffset.x
            , pWork->outFont[lFontNo].ptOffset.y
            , chOut, chOut);
#endif


#if DBG_PRINT
        DB_PRINT(_T("------A------------\n"));

        pWork->block.Print(_T(" "));

        DB_PRINT(_T("-------B-----------\n"));
#endif 

        chOld = chOut;
    }

#if DBG_PRINT_1
    DB_PRINT(_T("Final _CloseBlock W:%d H%d:\n"),  pWork->block.iWidth, pWork->block.iHeight);
#endif

    //pWork->block.CloseAllLine(iPitchLine, FIRST_BASE1);
    while (!pWork->stackBlock.empty())
    {
        _CloseBlock(&eM, pWork, iPitchLine, &pCurLine, &pCurBlock);
    }


    //後処理
#if DBG_PRINT
    DB_PRINT(_T("------------------\n"));

    pWork->block.SetParent();
    pWork->block.Print(_T(" "));

    DB_PRINT(_T("------------------\n"));
#endif



    //----------------------

    int iBlockWidth;
    double dMarginWidth = 0.0;
    double dMarginHeight= 0.0;


    if(!IsEnableFrame())
    {
        m_psTextFrame->SetUseMargin(false);
    }

    if(m_psTextFrame->IsUseMargin())
    {
        dMarginWidth = m_psTextFrame->Margin()->dLeft
            + m_psTextFrame->Margin()->dRight;

        dMarginHeight = m_psTextFrame->Margin()->dTop
            + m_psTextFrame->Margin()->dBottom;
    }

    if (IsEnableFrame() && !m_psTextFrame->IsAutoFit())
    {
        double dWidth = m_psTextFrame->GetWidth()
            - dMarginWidth;

        double dHeighth = m_psTextFrame->GetHeight()
            - dMarginHeight;

        iBlockWidth  = _ConvPitchToScreen(dWidth, dScl, pView);
        int iBlockHeight = _ConvPitchToScreen(dHeighth, dScl, pView);

        pWork->sizeDrawArea.cx = iBlockWidth;
        pWork->sizeDrawArea.cy = iBlockHeight;


#if DBG_CONV_SPLINE
        {
            DB_PRINT(_T("-----Frame--------\n"));
            DB_PRINT(_T("   Scl:%f W:%f h:%f -> W:%d h:%d \n"), dScl, dWidth, dHeighth, iBlockWidth, iBlockHeight);



            double dW;
            dW = _ConvScreenToPitch(pWork->block.iWidth, dScl, pView);

            double dH;
            dH = _ConvScreenToPitch(pWork->block.iHeight, dScl, pView);
            DB_PRINT(_T(" calc  Scl:%f  W:%d h:%d->W:%f h:%f \n"), dScl, pWork->block.iWidth, pWork->block.iHeight
                                                                           , dW, dH);


        }
#endif
    }
    else
    {
        if (fabs(dScl) > NEAR_ZERO)
        {
            double dW;
            dW = _ConvScreenToPitch(pWork->block.iWidth, dScl, pView)
                + dMarginWidth;
            m_psTextFrame->SetWidth(dW);

            double dH;

            dH = _ConvScreenToPitch(pWork->block.iHeight, dScl, pView)
                + dMarginHeight;
            m_psTextFrame->SetHeight(dH);

            pWork->sizeDrawArea.cx = pWork->block.iWidth;
            pWork->sizeDrawArea.cy = pWork->block.iHeight;


            //POINT2D ptOffset;
            //ptOffset.dX =  _ConvScreenToPitch(pWork->block.iFront, dScl, pView);
            //ptOffset.dY =  -_ConvScreenToPitch(pWork->block.iBottom, dScl, pView);
            //m_psTextFrame->SetOffset(ptOffset);

        }
        iBlockWidth = pWork->block.iWidth;
        pWork->sizeDrawArea.cx = iBlockWidth;
        pWork->sizeDrawArea.cy = pWork->block.iHeight;

#if DBG_CONV_SPLINE | DBG_PRINT
        {
            POINT2D ptOffset;
            ptOffset.dX =  _ConvScreenToPitch(pWork->block.iFront, dScl, pView);
            ptOffset.dY =  -_ConvScreenToPitch(pWork->block.iBottom, dScl, pView);


            double dVScl = pView->GetViewScale();

            //pPt->x =  static_cast<LONG>(  CUtil::Round((pt2D.dX  - pLayer->ptOffset.dX ) * m_dDpuX *  dScl ) - m_Vv.ptLook.x + (m_Vv.iWidth / 2.0));



            DB_PRINT(_T("-----Frame-AutoFit--Scl:%f--\n"), dScl);
            DB_PRINT(_T("   Frame(W:%f H:%f) O:%s \n"),
                m_psTextFrame->GetWidth(), m_psTextFrame->GetHeight(),
                ptOffset.Str().c_str());

            DB_PRINT(_T("   Frame Work(W:%d H:%d F:%d B:%d)  \n"),
                pWork->block.iWidth, pWork->block.iHeight,
                pWork->block.iFront, pWork->block.iBottom);

            POINT2D ptWH2;
            POINT2D pt;
            POINT ptWH;
            POINT pt0;
            int iLayerId = pView->GetCurrentLayerId();
            ptWH2.dX = m_psTextFrame->GetWidth();
            ptWH2.dY = m_psTextFrame->GetHeight();
            pView->ConvWorld2Scr(&ptWH, ptWH2, iLayerId);
            pView->ConvWorld2Scr(&pt0, pt, iLayerId);

            ptOffset.dX =  _ConvScreenToPitch(pWork->block.iFront, dScl, pView);
            ptOffset.dY =  -_ConvScreenToPitch(pWork->block.iBottom, dScl, pView);

            int iFw;
            int iFh;

            iFw = _ConvPitchToScreen( m_psTextFrame->GetWidth(), dScl, pView);
            iFh = _ConvPitchToScreen( m_psTextFrame->GetHeight(), dScl, pView);


            DB_PRINT(_T("   Frame Work2(W:%d H:%d) (W:%d H:%d) Scl:%f,%f  \n"),
                ptWH.x - pt0.x, ptWH.y - pt0.y ,iFw, iFh, dScl, dVScl);

            DB_PRINT(_T("------------------\n\n"));
        }
#endif
    }


    //-----------------------------
    //  アライメント調整
    //-----------------------------
    /* 基準点の位置が文字列中心の場合と、枠中心の場合がある
    両方の場合で共通に使えるオフセット量を設定する
    */

    int iBaseWidth = 0;
    if (m_eBase == FIRST_BASE1)
    {
        if (!pWork->block.lstLine.empty())
        {
            iBaseWidth = pWork->block.lstLine[0]->iLineWidth;
        }
    }
    else if (m_eBase == LAST_BASE)
    {
        if (!pWork->block.lstLine.empty())
        {
            size_t iLast = pWork->block.lstLine.size() - 1;
            iBaseWidth = pWork->block.lstLine[iLast]->iLineWidth;
        }
    }
    else
    {
        iBaseWidth = iBlockWidth;
    }

    for (auto pLine : pWork->block.lstLine)
    {
        int iLineWidth = pLine->iLineWidth;
        if (m_eAlignment == AL_RIGHT)
        {
            //文字ブロック左端からのオフセット量
            pLine->iAlign = iBaseWidth - iLineWidth;
        }
        else if (m_eAlignment == AL_MID)
        {
            pLine->iAlign = CUtil::Round((iBaseWidth - iLineWidth) / 2.0);
        }
        else
        {
            //文字ブロック右端からのオフセット量
            pLine->iAlign = 0;
        }
    }

    //-----------------------------------------------------
    int iFirstLineHeight = 0;
    if (!pWork->block.lstLine.empty())
    {
        if (!pWork->block.lstLine[0]->lstBlock.empty())
        {
            if (!pWork->block.lstLine[0]->lstBlock[0]->lstLine.empty())
            {
                iFirstLineHeight = pWork->block.lstLine[0]->lstBlock[0]->lstLine[0]->iLineHeight;
            }
        }
    }

    int iTotalHeight = 0;
    if (!pWork->block.lstLine.empty())
    {
        iTotalHeight = pWork->block.iHeight;
    }

    /*
    *    文字の基準点からフレーム左上の位置まで
    *   */
    POINT* pToTL = &(pWork->ptOffsetToTL);
    GetTL(pToTL, 
        iFirstLineHeight,               //先頭行の高さ
        iTotalHeight, 
        iBaseWidth, 
        pWork->sizeDrawArea.cx,         //テキスト領域の幅（フレーム設定時は フレーム幅-マージン  
        pView, 
        dScl);

    return hOldFont;
}


void CExtText::GetTL(POINT* pToTL, 
    int iFirstLineHeight,               //先頭行の高さ
    int iTotalHeight, 
    int iBaseWidth, 
    int iTextAreaWide,                  //テキスト領域の幅（フレーム設定時は フレーム幅-マージン  
    const CDrawingView* pView, 
    double dScl) const
{


    POINT pMarginLT;
    POINT pMarginRB;

    double dML;
    m_psTextFrame->Margin()->dLeft;

    dML = 0.0;
    pMarginLT.x = 0;
    pMarginLT.y = 0;
    pMarginRB.x = 0;
    pMarginRB.y = 0;

    if(IsEnableFrame()) 
    {
        if(m_psTextFrame->IsUseMargin())
        {
            dML = m_psTextFrame->Margin()->dLeft;
            pMarginLT.x = _ConvPitchToScreen(m_psTextFrame->Margin()->dLeft, dScl, pView);
            pMarginLT.y = _ConvPitchToScreen(m_psTextFrame->Margin()->dTop, dScl, pView);
            pMarginRB.x = _ConvPitchToScreen(m_psTextFrame->Margin()->dRight, dScl, pView);
            pMarginRB.y = _ConvPitchToScreen(m_psTextFrame->Margin()->dBottom, dScl, pView);
        }
    }

    if (m_eBase != TEXT_BLOCK)
    {
        if (m_eDatum & LEFT)       { pToTL->x = 0; }
        else if (m_eDatum & MID)   { pToTL->x = -(CUtil::Round(iBaseWidth / 2.0)); }
        else if (m_eDatum & RIGHT) { pToTL->x = -iBaseWidth; }
    }


    if (m_eBase == FIRST_BASE1)
    {
        if (m_eDatum & TOP)         { pToTL->y = iFirstLineHeight; }
        else if (m_eDatum & BOTTOM) { pToTL->y = 0; }
        else if (m_eDatum & CENTER) { pToTL->y = CUtil::Round(iFirstLineHeight / 2.0); }
    }
    else if (m_eBase == LAST_BASE)
    {
        if (m_eDatum & TOP)         { pToTL->y = iFirstLineHeight; }
        else if (m_eDatum & BOTTOM) { pToTL->y = iFirstLineHeight - iTotalHeight; }
        else if (m_eDatum & CENTER) { pToTL->y = (iFirstLineHeight - CUtil::Round(iTotalHeight/2.0)); }
    }
    else if (m_eBase == TEXT_BLOCK)
    {
        if      (m_eDatum & LEFT)  { pToTL->x =  pMarginLT.x; }
        else if (m_eDatum & RIGHT) { pToTL->x = -pMarginRB.x - iTextAreaWide ; }
        else if (m_eDatum & MID)   
        {
            double dOffset = m_psTextFrame->GetWidth() / 2.0 - dML;
            int iOffset = _ConvPitchToScreen(dOffset, dScl, pView);
            pToTL->x = -iOffset; 

        }  

        if      (m_eDatum & TOP)    { pToTL->y = pMarginLT.y + iFirstLineHeight; }
        else if (m_eDatum & BOTTOM) { pToTL->y = -iTotalHeight - pMarginRB.y + iFirstLineHeight; }
        else if (m_eDatum & CENTER) { pToTL->y = CUtil::Round((pMarginLT.y - pMarginRB.y - iTotalHeight) / 2.0) + iFirstLineHeight; }
    }
}


//!< 文字サイズ固定
void CExtText::SetFixSize(bool dFixSize)
{
    m_bFixSize = dFixSize;
}

bool CExtText::IsFixSize() const
{
    return m_bFixSize;}

//!< 表示サイズ固定
void CExtText::SetFixDispSize(bool dFixSize)
{
    m_bFixDisp = dFixSize;
}

bool CExtText::IsFixDispSize() const
{
    return m_bFixDisp;
}


//!< 行列適用
void CExtText::Matrix(const MAT2D& mat2D)
{
    double dSx, dSy;
    double dAngle = Frame()->Matrix( &dSx, &dSy, mat2D, m_dAngle);
    if (!IsFixSize())
    {

        m_dSclX = fabs(m_dSclX * dSx);
        m_dSclY = fabs(m_dSclY * dSy);
    }

    if((dSx * dSy) < 0)
    {

        bool bReverse = IsReverse();
        SetReverse(!bReverse);
    }

    if(dSy < 0)
    {
        if (IsFixRotate())
        {
            m_dAngle += 180.0;

        }
        else
        {
            dAngle += 180.0;
        }
    }

    if (!IsFixRotate())
    {
        m_dAngle = CUtil::NormAngle(dAngle);
    }
}


void CExtText::MatrixDirect(const MAT2D& mat2D)
{
    double dSclX;
    double dSclY;
    double dAngle;
    double dT;
    POINT2D pt2D;
    mat2D.GetAffineParam(&pt2D, &dAngle, &dSclX, &dSclY, &dT);

    m_dAngle = dAngle;
    m_dSclX = dSclX;
    m_dSclY = dSclY;

}

//!< 拡大率取得
double CExtText::GetSclX() const
{
    return m_dSclX;
}

double CExtText::GetSclY() const
{
    return m_dSclY;
}

void CExtText::SetSclX(double dScl)
{
    m_dSclX = dScl;

}

void CExtText::SetSclY(double dScl)
{
    m_dSclY = dScl;
}

double CExtText::GetPitch() const
{
    return m_dPitch;
}

void CExtText::SetPitch(double dPitch)
{
    m_dPitch = dPitch;
}

//!< 行間
double CExtText::GetLinePitch() const
{
    return m_dLinePitch;
}

void CExtText::SetLinePitch(double dPitch)
{
    m_dLinePitch = dPitch;
}

//!< 回転固定
void CExtText::SetFixRotate(bool dFix)
{
    m_bFixRotate = dFix;
}

bool CExtText::IsFixRotate() const
{
    return m_bFixRotate;
}

//!< 編集
void CExtText::SetEnableEdit(bool dFix)
{
    m_bEditable = dFix;
}

bool CExtText::IsEnableEdite() const
{
    return m_bEditable;
}

std::vector<POINT>* CExtText::GetDispFramePoint() const
{
    if (m_psTextFrame)
    {
        return &m_psTextFrame->lstDispFrame;
    }
    else
    {
        return NULL;
    }
}


std::vector<POINT2D>* CExtText::GetFramePoint() const
{
    if (m_psTextFrame)
    {
        return &m_psTextFrame->lstFrame;
    }
    else
    {
        return NULL;
    }
}

//基準点からフレームの左上の位置(角度０)
POINT2D CExtText::_FramePos(double dBaseWidth,
                            double dFirstLineHeight,
                            double dLastTextLineHeight
                            ) const
{
    TEXT_FRAME* pFrame;
    pFrame = Frame();
    STD_ASSERT(pFrame);

    double dDir = 1.0;
    if(IsReverse())
    {
        dDir = -1.0; 
    }

    double dMarginLeft   = 0.0;
    double dMarginTop    = 0.0;
    double dMarginRight  = 0.0;
    double dMarginBottom = 0.0;

    if(pFrame->IsUseMargin() && IsEnableFrame()) 
    {
        dMarginLeft = pFrame->Margin()->dLeft;
        dMarginTop = pFrame->Margin()->dTop;
        dMarginRight  = pFrame->Margin()->dRight;
        dMarginBottom = pFrame->Margin()->dBottom;
    }

    double dW = pFrame->GetWidth();

    BASE_TYPE eBase = GetBase();
    POS_TYPE  eDatum =GetDatum();
    ALIGNMENT_TYPE eAlignment = GetAlign();

    double dFrameWidth =  pFrame->GetWidth();

    POINT2D ptRet;
    double dML = 0.0; //左マージン
    double dMR = 0.0; //右マージン
    if (eBase != TEXT_BLOCK)
    {
        if (eDatum & LEFT)         {   /*ptO.dX -= dML;*/ }
        else if  (eDatum & MID)    {   ptRet.dX += dDir * (-dBaseWidth / 2.0); }
        else if  (eDatum & RIGHT)  {   ptRet.dX += dDir * (-dBaseWidth); }

        if (eAlignment == AL_LEFT)     { ptRet.dX -= dDir * dMarginLeft; }
        else if (eAlignment == AL_MID) { ptRet.dX += dDir * (-(dFrameWidth - dBaseWidth) / 2.0); }
        else                           { ptRet.dX += dDir * (-(dFrameWidth - dMarginRight - dBaseWidth)); }
    }


    if (eBase == FIRST_BASE1)
    {
        if (eDatum & TOP)          {   ptRet.dY = dMarginTop;    }
        else if (eDatum & BOTTOM)  {   ptRet.dY = dFirstLineHeight + dMarginTop;    }
        else if(eDatum & CENTER)   {   ptRet.dY =(dFirstLineHeight/2.0) + dMarginTop;}
    }
    else if (eBase == LAST_BASE)
    {
        if (eDatum & TOP)          {   ptRet.dY = dMarginTop;    }
        else if (eDatum & BOTTOM)  {   ptRet.dY = dLastTextLineHeight+ dMarginTop;    }
        else if(eDatum & CENTER)   {   ptRet.dY = dLastTextLineHeight/2.0 + dMarginTop;}
    }
    else if (eBase == TEXT_BLOCK)
    {

        if (eDatum & TOP)          {   ptRet.dY = 0;    }
        else if (eDatum & BOTTOM)  {   ptRet.dY = pFrame->GetHeight();    }
        else if(eDatum & CENTER)   {   ptRet.dY = pFrame->GetHeight() / 2.0;}

        if (eDatum & LEFT)         {   /*ptO.dX -= dML;*/ }
        else if  (eDatum & MID)    {   ptRet.dX += (dDir * (-pFrame->GetWidth() / 2.0)); }
        else if  (eDatum & RIGHT)  {   ptRet.dX += (dDir * (-pFrame->GetWidth())); }

    }
    return   ptRet;
}

void CExtText::DrawFrame (CDrawingView* pView,
    HDC hDc,
    const TEXT_PARAM* pParam,
    int iFront,
    int iWidthBase,
    int iFirstLineHeight,
    int iLastTextLineHeight,
    int iLayerId) const
{
    TEXT_FRAME* pFrame;

    pFrame = Frame();
    if (!pFrame)
    {
        return;
    }


    //POINT ptGTL  =  pParam->pWorkData->block.GetGlobalTL();

   
    //文字領域の右上を取得
    double dWidthBase;
    double dFirstLineHeight;               //先頭行の高さ
    double dLastTextLineHeight;
    double dFront;

    pView->ConvScr2World(&dWidthBase, iWidthBase, iLayerId, IsFixDispSize());
    pView->ConvScr2World(&dFirstLineHeight, iFirstLineHeight, iLayerId, IsFixDispSize());
    pView->ConvScr2World(&dLastTextLineHeight, iLastTextLineHeight, iLayerId, IsFixDispSize());
    pView->ConvScr2World(&dFront, iFront, iLayerId, IsFixDispSize());


    

    //フレームの右上位置を計算
    POINT2D pt2dFrameTL;
    pt2dFrameTL = _FramePos(dWidthBase,
                            dFirstLineHeight,
                            dLastTextLineHeight );


    POINT2D pt2dTL = pParam->pt2dSel + pt2dFrameTL;

    pt2dTL.dX += dFront;


    double dC = pFrame->GetCorner();
    bool bR = false;
    if (fabs(dC) > NEAR_ZERO)
    {
        bR = true;
    }

    double dDir = 1.0;
    if(IsReverse())
    {
        dDir = -1.0; 
    }

    pFrame->lstFrame.resize(4);

    pFrame->lstFrame[0].dX =  pt2dTL.dX + pFrame->GetOffset().dX;
    pFrame->lstFrame[0].dY =  pt2dTL.dY + pFrame->GetOffset().dY;

    pFrame->lstFrame[1].dX = pFrame->lstFrame[0].dX + (dDir * pFrame->GetWidth());
    pFrame->lstFrame[1].dY = pFrame->lstFrame[0].dY ;

    pFrame->lstFrame[2].dX = pFrame->lstFrame[0].dX + (dDir * pFrame->GetWidth());
    pFrame->lstFrame[2].dY = pFrame->lstFrame[0].dY - (       pFrame->GetHeight());

    pFrame->lstFrame[3].dX = pFrame->lstFrame[0].dX;
    pFrame->lstFrame[3].dY = pFrame->lstFrame[0].dY - (       pFrame->GetHeight());


    /*
    DB_PRINT(_T("CExtText::DrawFrame2   0f(%f,%f), P0(%f,%f)  Sel+TL(%f,%f))\n"), 
        pFrame->GetOffset().dX, pFrame->GetOffset().dX,
        pFrame->lstFrame[0].dX, pFrame->lstFrame[0].dY,
         pt2dTL.dX, pt2dTL.dY);               
    */

    double dAngle = GetAngle();

    std::vector<LINE2D> lstRound;
    std::vector<POINT2D> lstCenter;

    if (bR)
    {
        POINT2D pt1;
        POINT2D pt2;

        double dRc = dDir * dC;


        pt1 = pFrame->lstFrame[0];
        pt1.dX = pt1.dX + dRc;
        pt2 = pFrame->lstFrame[1];
        pt2.dX = pt2.dX - dRc;
        lstRound.push_back(LINE2D(pt1, pt2));
        lstCenter.push_back(POINT2D(pt1.dX, pt1.dY - dC));

        pt1 = pFrame->lstFrame[1];
        pt1.dY -= dC;
        pt2 = pFrame->lstFrame[2];
        pt2.dY += dC;
        lstRound.push_back(LINE2D(pt1, pt2));
        lstCenter.push_back(POINT2D(pt1.dX - dRc, pt1.dY));

        pt1 = pFrame->lstFrame[2];
        pt1.dX = pt1.dX - dRc;
        pt2 = pFrame->lstFrame[3];
        pt2.dX = pt2.dX + dRc;
        lstRound.push_back(LINE2D(pt1, pt2));
        lstCenter.push_back(POINT2D(pt1.dX, pt1.dY + dC));

        pt1 = pFrame->lstFrame[3];
        pt1.dY += dC;
        pt2 = pFrame->lstFrame[0];
        pt2.dY -= dC;
        lstRound.push_back(LINE2D(pt1, pt2));
        lstCenter.push_back(POINT2D(pt1.dX + dRc, pt1.dY));
    }


    double dMarginLeft   = 0.0;
    double dMarginTop    = 0.0;
    double dMarginRight  = 0.0;
    double dMarginBottom = 0.0;

    if(pFrame->IsUseMargin() && IsEnableFrame()) 
    {
        dMarginLeft   = dDir * pFrame->Margin()->dLeft;
        dMarginRight  = dDir * pFrame->Margin()->dRight;
        dMarginTop    =        pFrame->Margin()->dTop;
        dMarginBottom =        pFrame->Margin()->dBottom;
    }

    std::vector<POINT2D> lstMargin;
    lstMargin.resize(4);

    lstMargin[0].dX = pFrame->lstFrame[0].dX + dMarginLeft;
    lstMargin[0].dY = pFrame->lstFrame[0].dY - dMarginTop;

    lstMargin[1].dX = pFrame->lstFrame[1].dX - dMarginRight;
    lstMargin[1].dY = lstMargin[0].dY;

    lstMargin[2].dX = lstMargin[1].dX;
    lstMargin[2].dY = pFrame->lstFrame[2].dY + dMarginBottom;

    lstMargin[3].dX = lstMargin[0].dX;
    lstMargin[3].dY = lstMargin[2].dY;;


     int iCnt = 0;
    pFrame->lstDispFrame.resize(4);
    pFrame->lstTextArea.resize(4);

    bool  bFixLayerScl = IsFixSize();

    for(POINT2D& ptLine: pFrame->lstFrame)
    {
        if(!pFrame->IsUseMargin())
        {
            pView->ConvWorld2Scr(&pFrame->lstTextArea.at(iCnt), ptLine, iLayerId, bFixLayerScl);
        }

        ptLine.Rotate(pParam->pt2dSel, dAngle);
        pView->ConvWorld2Scr(&pFrame->lstDispFrame[iCnt], ptLine, iLayerId, bFixLayerScl);
        iCnt++;
    }
#if DBG_PRINT
    DB_PRINT(_T("DrawFrame TL(%d,%d) BR(%d,%d) W:%d, H:%d\n"),
        pFrame->lstDispFrame[0].x, pFrame->lstDispFrame[0].y,
        pFrame->lstDispFrame[2].x, pFrame->lstDispFrame[2].y, 
        pFrame->lstDispFrame[2].x - pFrame->lstDispFrame[0].x,
        pFrame->lstDispFrame[2].y - pFrame->lstDispFrame[0].y);

    DB_PRINT(_T("DrawFrame lstFrame TL(%f,%f) BR(%f,%f) W:%f, H:%f\n"),
        pFrame->lstFrame[0].dX, pFrame->lstFrame[0].dY,
        pFrame->lstFrame[2].dX, pFrame->lstFrame[2].dY, 
        pFrame->lstFrame[2].dX - pFrame->lstFrame[0].dX,
        pFrame->lstFrame[2].dY - pFrame->lstFrame[0].dY);



#endif
    if (bR)
    {
        for (int iCnt = 0; iCnt < 4; iCnt++)
        {
            lstRound[iCnt].Rotate(pParam->pt2dSel, dAngle);
            lstCenter[iCnt].Rotate(pParam->pt2dSel, dAngle);
        }
    }

    if(pFrame->IsUseMargin())
    {
        iCnt = 0;
        for(POINT2D& ptLine: lstMargin)
        {
            //枠外の文字を非表示にする計算に用いるため回転はしない
            pView->ConvWorld2Scr(&pFrame->lstTextArea.at(iCnt), ptLine, iLayerId, bFixLayerScl);
            iCnt++;

#ifdef DRAW_MARGIN
            ptLine.Rotate(m_ptTmp, dAngle);
#endif
        }
    }

    //計算の誤差でクリッピングが行われる場合があるので1dot拡張

    if (IsReverse())
    {
        pFrame->lstTextArea.at(0).x++;
        pFrame->lstTextArea.at(1).x--;
        pFrame->lstTextArea.at(2).x--;
        pFrame->lstTextArea.at(3).x++;
    }
    else
    {
        pFrame->lstTextArea.at(0).x--;
        pFrame->lstTextArea.at(1).x++;
        pFrame->lstTextArea.at(2).x++;
        pFrame->lstTextArea.at(3).x--;
    }

    pFrame->lstTextArea.at(0).y--;
    pFrame->lstTextArea.at(1).y--;
    pFrame->lstTextArea.at(2).y++;
    pFrame->lstTextArea.at(3).y++;

    bool bDrawLine = (pFrame->IsEnableLine() || pParam->bSelect);
    int iLineType = PS_SOLID;
    int iLineWidth = 1;

    COLORREF crLine;
    COLORREF crBack;

    if (!IsEnableFrame() && 
        !pParam->bSelect)
    {
        return;
    }


    if (pParam->bIndex)
    {
        crLine = pParam->cr;
        crBack = pParam->cr;
    }
    else
    {
        if (!pParam->bFrameColor)
        {
            crLine = pFrame->LineProperty()->cr;
        }
        else
        {
            crLine = pParam->crFrame;
        }

        iLineType = pFrame->LineProperty()->iLineType;
        iLineWidth = pFrame->LineProperty()->iWidth;

        if (pParam->bEmphasis)
        {
            iLineWidth += 2;
        }
        /*
        if (bDrawLine)
        }
        else
        {
        crLine = pFrame->GetColor();
        }*/

        crBack = pFrame->GetColor();
    }

    bool bSkioFirstPoint = false;
    pFrame->lstFrame.push_back(pFrame->lstFrame[0]);

    if(bDrawLine)
    {
        pView->SetPen(iLineType,  iLineWidth, crLine, pParam->iId);
        if(!bR)
        {
            _LineMulti3( pView, iLayerId, &pFrame->lstFrame, bSkioFirstPoint, bFixLayerScl);
        }
    }

    bool bFill = pFrame->IsUseColor() && (!pParam->bDrag);
    if (bFill)
    {
        pView->SetBrush(crBack);
        if(!bR)
        {
            _FillMulti3(pView, iLayerId, &pFrame->lstFrame, bFixLayerScl);
        }
    }

#ifdef DRAW_MARGIN
    if(pFrame->IsUseMargin())
    {
        lstMargin.push_back(lstMargin[0]);
        pView->SetPen(PS_DASH,  1, crLine, m_nId);
        _LineMulti3( pView, iLayerId, &lstMargin, bSkioFirstPoint, bFixLayerScl);

        if (!bR)
        {
            pView->SetPen(iLineType, iLineWidth, crLine, m_nId);
        }
    }
#endif


    if (bR)
    {
        pView->SetPen(iLineType,  iLineWidth, crLine, pParam->iId);
        int iLoop = 1;
        if (bFill)
        {
            pView->BeginPath();
            iLoop = 2;
        }

        for (int iCnt = 0; iCnt < iLoop; iCnt++)
        {
            if (iLoop == 2)
            {
                //塗りつぶし用の線を背景色で書いてから
                //塗りつぶした後 点線を書く
                if (iCnt == 0)
                {
                    COLORREF crB;
                    crB = DRAW_CONFIG->GetBackColor();
                    pView->SetPen(PS_SOLID, iLineWidth, crB, pParam->iId);
                }
                else
                {
                    pView->SetPen(iLineType, iLineWidth, crLine, pParam->iId);
                }
            }

            POINT p1;
            POINT p2;
            POINT p3;

            POINT c;
            RECT  rc;
            int i = 0;
            for(int i = 0; i < 4 ; i++)
            {
                pView->ConvWorld2Scr(&p1, lstRound[i].GetPt1(), iLayerId, bFixLayerScl);
                pView->ConvWorld2Scr(&p2, lstRound[i].GetPt2(), iLayerId, bFixLayerScl);

                if(i == 0)
                {
                    pView->MoveTo(p1);
                }
                pView->LineTo(p2);

                int i2 = (i+1) % 4;
                pView->ConvWorld2Scr(&c,  lstCenter[i2], iLayerId, bFixLayerScl);
                int iD = CUtil::Round(CUtil::LenPoint(p2, c));
                rc.top    = c.y - iD; 
                rc.left   = c.x - iD; 
                rc.bottom = c.y + iD; 
                rc.right  = c.x + iD; 

                pView->ConvWorld2Scr(&p3, lstRound[i2].GetPt1(), iLayerId, bFixLayerScl);

                bool bClockWise = (!IsReverse()); 
                pView->ArcTo(rc, p2, p3, bClockWise);
            }

            if (iCnt == 0)
            {
                if (bFill)
                {
                    pView->CloseFigure();
                    pView->EndPath();
                    pView->FillPath(crBack, pParam->iId);
                }
            }
        }
    }

}


void CExtText::_LineMulti3( CDrawingView* pView,
    int iLayerId,     
    const std::vector<POINT2D>* pLstPt, bool bSkioFirstPoint,
    bool bFixLayerScl) const
{
    CPoint  Pt1, Pt2;
    LINE2D  line;
    POINT2D pt2DOld;
    std::vector<POINT> lstDot;
    int iSize;

    //-------------------------------
    // 単にスケールを変更させただけだと位置がずれるのでOffsetを計算する
    POINT ptOffset;

    if ( bFixLayerScl)
    {
        POINT ptA, ptB;
        pView->ConvWorld2Scr(&ptA, pLstPt->at(0), iLayerId, true);
        pView->ConvWorld2Scr(&ptB, pLstPt->at(0), iLayerId, false);  //本来の位置

        ptOffset.x =  ptB.x - ptA.x;
        ptOffset.y =  ptB.y - ptA.y;
    }
    else
    {
        ptOffset.x = 0; 
        ptOffset.y = 0;  
    }
    //-------------------------------

    foreach(POINT2D pt, *pLstPt)
    {
        pView->ConvWorld2Scr(&Pt1, pt, iLayerId, bFixLayerScl);
        Pt1.x += ptOffset.x;
        Pt1.y += ptOffset.y;

        iSize = (int)lstDot.size();
        if (iSize != 0)
        {
            Pt2 = lstDot[iSize - 1];
            Pt2.x += ptOffset.x;
            Pt2.y += ptOffset.y;
            if (Pt2 != Pt1)
            {
                lstDot.push_back(Pt1);
            }
        }
        else
        {
            lstDot.push_back(Pt1);
        }
    }

    pView->Polyline(lstDot, bSkioFirstPoint);

}


void CExtText::_FillMulti3( CDrawingView* pView,
    int iLayerId,
    const std::vector<POINT2D>* pLstPt,
    bool bFixLayerScl) const
{
    CPoint  Pt1, Pt2;
    LINE2D  line;
    POINT2D pt2DOld;
    std::vector<POINT> lstDot;
    int iSize;

    //-------------------------------
    // 単にスケールを変更させただけだと位置がずれるのでOffsetを計算する
    POINT ptOffset;

    if ( bFixLayerScl)
    {
        POINT ptA, ptB;
        pView->ConvWorld2Scr(&ptA, pLstPt->at(0), iLayerId, true);
        pView->ConvWorld2Scr(&ptB, pLstPt->at(0), iLayerId, false);  //本来の位置

        ptOffset.x =  ptB.x - ptA.x;
        ptOffset.y =  ptB.y - ptA.y;
    }
    else
    {
        ptOffset.x = 0; 
        ptOffset.y = 0;  
    }
    //-------------------------------


    foreach(POINT2D pt, *pLstPt)
    {

        pView->ConvWorld2Scr(&Pt1, pt, iLayerId, bFixLayerScl);
        iSize = (int)lstDot.size();
        Pt1.x += ptOffset.x;
        Pt1.y += ptOffset.y;

        if (iSize != 0)
        {
            Pt2 = lstDot[iSize - 1];
            Pt2.x += ptOffset.x;
            Pt2.y += ptOffset.y;
            if (Pt2 != Pt1)
            {
                lstDot.push_back(Pt1);
            }
        }
        else
        {
            lstDot.push_back(Pt1);
        }
    }
    pView->FillPolyline(lstDot);

}

/**
*  @brief   表示
*  @param   [in]  hDC   デバイスコンテキスト
*  @param   [in]  pt           表示位置
*  @param   [in]  dOffsetAngle 追加角度
*  @param   [in]  dScl         倍率
*  @param   [in]  bColor       強制色設定の有無
*  @param   [in]  crF          設定色
*  @param   [in]  bBold        太字(
*  @param   [in]  bIndex       インデックス描画
*  @param   [in]  bFrameColor  フレーム強制色設定の有無
*  @param   [in]  bColor       フレーム強制設定色

*  @retval  なし
*  @note
*/
void CExtText::Draw(HDC hDc,
    CDrawingView* pView,
    POINT pt,
    FONT_WORK_DATA* pWorkData,
    const TEXT_PARAM* pParam,
    int iLayerId
)  const
{
    //DbgGuiLeak dgl;

    long lCnt = 0;

    COLORREF crRef;
    COLORREF crOld = 0xfffffffe;
    int  iOldBkMode;
    HFONT hFont = 0;
    HFONT hBeforFont = 0;
    StdChar  chOut[2];
    unsigned int OldAlign;

    long lLen = (long)m_FontData.size();

    if (lLen == 0)
    {
        return;
    }

    double dDir = 1.0;
    if(IsReverse())
    {
        OldAlign = ::SetTextAlign(hDc, TA_BOTTOM | TA_RIGHT);
        pWorkData->ptOffsetToTL.x = -pWorkData->ptOffsetToTL.x;
        dDir = -1.0;
    }
    else
    {
        OldAlign = ::SetTextAlign(hDc, TA_BOTTOM | TA_LEFT);
    }


    iOldBkMode = GetBkMode(hDc);
    SetBkMode(hDc, TRANSPARENT);

    double dAngle = m_dAngle + pParam->dOffsetAngle;

    double dCos;
    double dSin;
    CUtil::GetTriFunc(dAngle, &dSin, &dCos);

    int iDrawBottom = 0;

    //枠線
    int iFirstTextLineHeight = 0;
    int iLastTextLineHeight = 0;
    int iWidthBase = 0;
    int iFront = 0; 

    if (!pWorkData->block.lstLine.empty())
    {
        auto pFistLine = pWorkData->block.lstLine[0];

        std::shared_ptr< FONT_LINE_DATA > pSecondFirstLine;
        if (!pFistLine->lstBlock.empty())
        {
            if (!pFistLine->lstBlock[0]->lstLine.empty())
            {
                pSecondFirstLine = pFistLine->lstBlock[0]->lstLine[0];
            }
        }

        iFront     = pWorkData->block.iFront;
        if (pFistLine->IsGrid())
        {
            //Gridの場合は内部の先頭行を取得する。
            if(pSecondFirstLine)
            {
                iFirstTextLineHeight = pSecondFirstLine->iLineHeight  - pSecondFirstLine->iLineBottom;
            }
            iLastTextLineHeight  = pFistLine->iLineHeight;
            iWidthBase = pWorkData->block.iWidth;
        }
        else
        {

            iLastTextLineHeight = pWorkData->block.iHeight;
            iWidthBase = pWorkData->block.iWidth;

            iFirstTextLineHeight = pFistLine->iLineHeight - pFistLine->iLineBottom;
 
 /*
            if(pSecondFirstLine)
            {
                iFirstTextLineHeight = pSecondFirstLine->iLineHeight - pSecondFirstLine->iLineBottom;
#if DBG_PRINT
                DB_PRINT(_T("CExtText::DrawFrame iFirstTextLineHeight: %d 1st:%d)\n"), 
                    pSecondFirstLine->iLineHeight,  pFistLine->iLineHeight - pFistLine->iLineBottom);               
#endif
            }
            else
            {
                iFirstTextLineHeight = pFistLine->iLineHeight - pFistLine->iLineBottom;
            }
 */
        }
    }

    TEXT_PARAM param;
    param = *pParam;
    param.pWorkData = pWorkData;

    {
#if DBG_PRINT
        DB_PRINT(_T("CExtText::DrawFrame iFront:%d  iWidthBase:%d iFirstTextLineHeight:%d iLastTextLineHeight:%d)\n"), 
            iFront,iWidthBase,iFirstTextLineHeight,iLastTextLineHeight );               
#endif

        DrawFrame(pView, hDc, &param, 
            iFront,
            iWidthBase, 
            iFirstTextLineHeight,
            iLastTextLineHeight,
        
        iLayerId);
        if (pWorkData->bFirstFrameCallback)
        {
            if (!m_psTextFrame->lstDispFrame.empty())
            {
                iDrawBottom = m_psTextFrame->lstDispFrame[2].y;
                if (m_psTextFrame->IsUseMargin())
                {
                    iDrawBottom -= CUtil::Round(m_psTextFrame->Margin()->dBottom);
                }
            }
        }
    }

    int iLine = 0;

    int iFillMode = WINDING;
    HPEN hOldPen;
    HPEN hPen;
    HBRUSH hOldBrush;
    HBRUSH hBrush;

    if (pParam->bIndex)
    {
        iFillMode = ::GetPolyFillMode(hDc);
        ::SetPolyFillMode(hDc, WINDING);
        hPen = ::CreatePen(PS_SOLID, 1, pParam->cr);
        hOldPen = (HPEN)::SelectObject(hDc, (HGDIOBJ)hPen);
        hBrush = ::CreateSolidBrush(pParam->cr);
        hOldBrush = (HBRUSH)::SelectObject(hDc, (HGDIOBJ)hBrush);
    }




    int iOldLine = -1;
    FONT_LINE_DATA* pOldLine = NULL;
    FONT_LINE_DATA* pLine = NULL;
    POINT ptLineOffset;
    ptLineOffset.x = 0;
    ptLineOffset.y = 0;
    std::vector<LINE2D> lstBlockLine;

    POINT2D ptSel;
    ptSel.dX = pt.x;
    ptSel.dY = pt.y;

    for (lCnt = 0; lCnt < lLen; lCnt++)
    {
        const FONT_DATA* pData = m_FontData[lCnt].get();


        if (pWorkData->outFont[lCnt].bStartBlock)
        {
            BLOCK_DATA* pBlock = NULL;
            FONT_LINE_DATA* pLine2 = pWorkData->outFont[lCnt].pLine;
            if (pLine2)
            {
                pBlock = pLine2->pParentBlock;
            }

            if (pBlock)
            {
                POINT ptTL = pBlock->GetGlobalTL();
                POINT ptBR = pBlock->GetGlobalBR();

                if (pBlock->szModifer.dwSts & BS_UNDERLINE)
                {
                    POINT2D ptS;
                    POINT2D ptE;

                    ptS.dX = double(dDir * (ptSel.dX + ptTL.x) + pWorkData->ptOffsetToTL.x);
                    ptS.dY = double(       (ptSel.dY + ptBR.y) + pWorkData->ptOffsetToTL.y);

                    ptE.dX = double(dDir * (ptSel.dX + ptBR.x) + pWorkData->ptOffsetToTL.x);
                    ptE.dY = double(       (ptSel.dY + ptBR.y) + pWorkData->ptOffsetToTL.y);

                    LINE2D l(ptS, ptE);
                    l.Rotate(ptSel, -dAngle);
                    lstBlockLine.push_back(l);

                }

                if (pBlock->szModifer.dwSts & BS_FRAME)
                {
                    POINT2D pt[4];

                    pt[0].dX = double(dDir*(ptSel.dX + ptTL.x) + pWorkData->ptOffsetToTL.x);
                    pt[0].dY = double(     (ptSel.dY + ptTL.y) + pWorkData->ptOffsetToTL.y);

                    pt[1].dX = double(dDir*(ptSel.dX + ptBR.x) + pWorkData->ptOffsetToTL.x);
                    pt[1].dY = pt[0].dY;

                    pt[2].dX = pt[1].dX;
                    pt[2].dY = double(     (ptSel.dY + ptBR.y) + pWorkData->ptOffsetToTL.y);

                    pt[3].dX = pt[0].dX;
                    pt[3].dY = pt[2].dY;


#if DBG_PRINT
                    DB_PRINT(_T("CExtText::Draw BS_FRAME TL(%d,%d) BR(%d,%d) Sel(%f,%f) W:%d H;%d  O(%d,%d)\n"), 
                        ptTL.x, ptTL.y, ptBR.x, ptBR.y, ptSel.dX, ptSel.dY, ptBR.x-ptTL.x, ptTL.y - ptBR.y,
                        pWorkData->ptOffsetToTL.x, pWorkData->ptOffsetToTL.y);               
#endif

                    for (POINT2D& p : pt)
                    {
                        p.Rotate(ptSel, -dAngle);
                    }

                    lstBlockLine.push_back(LINE2D(pt[0], pt[1]));
                    lstBlockLine.push_back(LINE2D(pt[1], pt[2]));
                    lstBlockLine.push_back(LINE2D(pt[2], pt[3]));
                    lstBlockLine.push_back(LINE2D(pt[3], pt[0]));
                }

                if (((pBlock->szModifer.dwSts & BS_GRID) &&
                    !(pBlock->szModifer.dwSts & BS_NO_GRID_LINE)) &&
                    ((pBlock->lstRow.size() > 1) &&
                        (pBlock->lstCol.size() > 1)))
                {
                    std::vector<double> lstX;
                    std::vector<double> lstY;

                    double dX, dY;
                    for (int iX : pBlock->lstCol)
                    {
                        dX = dDir * (ptSel.dX + ptTL.x + iX) + pWorkData->ptOffsetToTL.x;
                        lstX.push_back(dX);
                    }

                    for (int iY : pBlock->lstRow)
                    {
                        dY =        (ptSel.dY + ptTL.y + iY) + pWorkData->ptOffsetToTL.y;
                        lstY.push_back(dY);
                    }

                    POINT2D ptS, ptE;

                    double dXMin = lstX[0];
                    double dYMin = lstY[0];
                    double dXMax = lstX[lstX.size() - 1];
                    double dYMax = lstY[lstY.size() - 1];

                    for (double& dX : lstX)
                    {
                        ptS.dX = dX;
                        ptS.dY = dYMin;

                        ptE.dX = dX;
                        ptE.dY = dYMax;
                        ptS.Rotate(ptSel, -dAngle);
                        ptE.Rotate(ptSel, -dAngle);
                        lstBlockLine.push_back(LINE2D(ptS, ptE));
                    }

                    for (double& dY : lstY)
                    {
                        ptS.dX = dXMin;
                        ptS.dY = dY;

                        ptE.dX = dXMax;
                        ptE.dY = dY;
                        ptS.Rotate(ptSel, -dAngle);
                        ptE.Rotate(ptSel, -dAngle);
                        lstBlockLine.push_back(LINE2D(ptS, ptE));
                    }

                }
            }
        }

        if (!pWorkData->outFont[lCnt].bDisp)
        {
            continue;
        }

        if (!pParam->bIndex)
        {
            if (pParam->bForceColor)
            {
                //色を強制的に設定する
                crRef = pParam->cr;
            }
            else
            {
                crRef = pData->crColor;
            }

            if ((crRef != crOld) || (lCnt == 0))
            {
                SetTextColor(hDc, crRef);
            }
            crOld = crRef;

            if (pWorkData->outFont[lCnt].bCreateFont)
            {
                hFont = pWorkData->outFont[lCnt].hFont;
                ::SelectObject(hDc, hFont);
            }
        }

        chOut[0] = m_strOut[lCnt];
        chOut[1] = 0;

        POINT2D ptOffset;
        POINT2D ptBL;      //右下


        FONT_OUT_DATA* pOutFont = &(pWorkData->outFont[lCnt]);
        FONT_LINE_DATA* pCurLine = pOutFont->pLine;

        if (pCurLine)
        {
            if (pCurLine != pLine)
            {
                pLine = pCurLine;
                ptLineOffset = pLine->GetTotalOffset();
            }
        }
        ptOffset.dX = double(dDir * (ptLineOffset.x + pOutFont->ptOffset.x) + pWorkData->ptOffsetToTL.x);
        ptOffset.dY = double(       (ptLineOffset.y + pOutFont->ptOffset.y) + pWorkData->ptOffsetToTL.y);

#if DBG_PRINT
        DB_PRINT(_T("Draw %s L:(%d,%d) O:(%d,%d), Pt(%f,%f) Sz(%d,%d)[%s]\n"), chOut,
            ptLineOffset.x, ptLineOffset.y,
            pOutFont->ptOffset.x, pOutFont->ptOffset.y,
            ptOffset.dX, ptOffset.dY,
            pOutFont->size.cx,  pOutFont->size.cy,
            pOutFont->pLine->pName->c_str()
        );
#endif

        //---------------------
        // クリッピング処理
        //---------------------
#if 0 //デバッグ用にクリッピングを無効にする
        if (IsEnableFrame())
        {
            int iClipMargin = 0;
            POINT2D ptBase = ptOffset;
            ptBase += ptSel;
            CRect rc(m_psTextFrame->lstTextArea.at(0), m_psTextFrame->lstTextArea.at(2));

            if (IsReverse())
            {
                /*
                if (m_psTextFrame->lstTextArea.at(0).x < ptBase.dX)
                {
                continue;
                }

                int iX = pOutFont->size.cx;
                if (m_psTextFrame->lstTextArea.at(2).x > ((ptBase.dX - iX) + iClipMargin))
                {
                continue;
                }
                */
            }
            else
            {
                if (m_psTextFrame->lstTextArea.at(0).x > ptBase.dX)
                {
                    continue;
                }

                int iX = pOutFont->size.cx;
                if (m_psTextFrame->lstTextArea.at(2).x < ((ptBase.dX + iX) - iClipMargin))
                {
                    continue;
                }
            }

            int iY = pOutFont->size.cy;
            if (m_psTextFrame->lstTextArea.at(0).y > (ptBase.dY - iY))
            {
                continue;
            }


            if (m_psTextFrame->lstTextArea.at(2).y < ptBase.dY)
            {
                continue;
            }

        }
        //---------------------
#endif //デバッグ用
        double dY = 0.0;

#if 0//DBG_PRINT
        DB_PRINT(_T("Draw %s L:(%d,%d) O:(%d,%d), Pt(%d,%d) Sz(%d,%d)\n"), chOut,
            ptLineOffset.x, ptLineOffset.y,
            pOutFont->ptOffset.x, pOutFont->ptOffset.y,
            ptOffset.dX, ptOffset.dY,
            pOutFont->size.cx, pOutFont->size.cy);

        DB_PRINT(_T("Rotate(%f) %s Offset:(%f,%f)"), dAngle, chOut,
            ptOffset.dX, ptOffset.dY);
#endif

        ptOffset.Rotate(-dAngle);
        ptBL = ptSel + ptOffset;


#if 0 //DBG_PRINT
        DB_PRINT(_T("-(%f,%f)\n"), ptOffset.dX, ptOffset.dY);
#endif


        BOOL bRet2;
        if (!pParam->bIndex)
        {
#if DEBUG_TEXT_LINE
            POINT2D ptW;
            ptW.dX = pWorkData->outFont[lCnt].size.cx;
            ptW.dY = 0.0;
            ptW.Rotate(dAngle);

            POINT2D ptH;
            ptH.dX = 0.0;
            ptH.dY = pWorkData->outFont[lCnt].size.cy;
            ptH.Rotate(dAngle);


            CPoint ptRect[4];

            ptRect[0].x = CUtil::Round(ptBL.dX);
            ptRect[0].y = CUtil::Round(ptBL.dY);

            ptRect[1].x = ptRect[0].x + CUtil::Round(ptW.dX);
            ptRect[1].y = ptRect[0].y - CUtil::Round(ptW.dY);

            ptRect[2].x = ptRect[1].x + CUtil::Round(ptH.dX);
            ptRect[2].y = ptRect[1].y - CUtil::Round(ptH.dY);

            ptRect[3].x = ptRect[0].x + CUtil::Round(ptH.dX);
            ptRect[3].y = ptRect[0].y - CUtil::Round(ptH.dY);

            POINT pt;
            ::MoveToEx(hDc, ptRect[0].x, ptRect[0].y, &pt);
            ::LineTo(hDc, ptRect[1].x, ptRect[1].y);
            ::LineTo(hDc, ptRect[2].x, ptRect[2].y);
            ::LineTo(hDc, ptRect[3].x, ptRect[3].y);
            ::LineTo(hDc, ptRect[0].x, ptRect[0].y);

#endif
            POINT ptT;
            ptT.x = CUtil::Round(ptBL.dX);
            ptT.y = CUtil::Round(ptBL.dY);

            bRet2 = ::TextOut(hDc, ptT.x,
                ptT.y, &chOut[0], 1);
            if (pParam->bEmphasis)
            {
                ::TextOut(hDc, ptT.x + 1,
                    ptT.y + 1, &chOut[0], 1);
            }


        }
        else
        {
            POINT2D ptW;
            ptW.dX = pOutFont->size.cx;
            ptW.dY = 0.0;
            ptW.Rotate(dAngle);

            POINT2D ptH;
            ptH.dX = 0.0;
            ptH.dY = pOutFont->size.cy;
            ptH.Rotate(dAngle);


            CPoint ptRect[4];

            ptRect[0].x = CUtil::Round(ptBL.dX);
            ptRect[0].y = CUtil::Round(ptBL.dY);

            ptRect[1].x = ptRect[0].x + CUtil::Round(ptW.dX);
            ptRect[1].y = ptRect[0].y - CUtil::Round(ptW.dY);

            ptRect[2].x = ptRect[1].x + CUtil::Round(ptH.dX);
            ptRect[2].y = ptRect[1].y - CUtil::Round(ptH.dY);

            ptRect[3].x = ptRect[0].x + CUtil::Round(ptH.dX);
            ptRect[3].y = ptRect[0].y - CUtil::Round(ptH.dY);

            BOOL bRet;
            bRet = ::Polygon(hDc, ptRect, 4);
            STD_ASSERT(bRet);
        }
        pOldLine = pWorkData->outFont[lCnt].pLine;
    }

    SetBkMode(hDc, iOldBkMode);
    SetTextAlign(hDc, OldAlign);

    {
        COLORREF crLine;
        if (!pParam->bFrameColor)
        {
            crLine = m_psTextFrame->LineProperty()->cr;
        }
        else
        {
            crLine = pParam->crFrame;
        }


        int iLineType =  m_psTextFrame->LineProperty()->iLineType;
        int iLineWidth = m_psTextFrame->LineProperty()->iWidth;
        pView->SetPen(iLineType,  iLineWidth, crLine, pParam->iId);

        POINT pt;
        POINT pt1;
        POINT pt2;
        for (LINE2D& l : lstBlockLine)
        {
            pt1.x =  CUtil::Round(l.Pt(0).dX);
            pt1.y =  CUtil::Round(l.Pt(0).dY);
            pt2.x =  CUtil::Round(l.Pt(1).dX);
            pt2.y =  CUtil::Round(l.Pt(1).dY);

            ::MoveToEx(hDc, pt1.x, pt1.y, &pt);
            ::LineTo  (hDc, pt2.x, pt2.y);
        }
    }

    if (pParam->bIndex)
    {
        ::SetPolyFillMode(hDc, iFillMode);
        (HPEN)::SelectObject(hDc, (HGDIOBJ)hOldPen);
        (HBRUSH)::SelectObject(hDc, (HGDIOBJ)hOldBrush);

        ::DeleteObject(hPen);
        ::DeleteObject(hBrush);
    }

    if (pWorkData->hOldFont != NULL)
    {
        hBeforFont = (HFONT)::SelectObject(hDc, pWorkData->hOldFont);
    }

    for (lCnt = 0; lCnt < lLen; lCnt++)
    {
        if (pWorkData->outFont[lCnt].bCreateFont)
        {
            ::DeleteObject(pWorkData->outFont[lCnt].hFont);
            if (m_dAngle != 0)
            {
                ::DeleteObject(pWorkData->outFont[lCnt].hFontS);
            }
        }
    }
}

/**
*  @brief   リッチエディットからの文字取得         A
*  @param   [in]  pRich   リッチエディットコントロール
*  @retval  なし
*  @note
*/
void CExtText::SetFromRich(CRichEditCtrl* pRich)
{
    STD_ASSERT(pRich != 0);
    SetText(_T(""));
    //フォーマットを１文字づつ読み込む
    //CString  strOut;
    //pRich->SetSel( 0, -1);
    //long lLen = pRich->GetTextLength();

    //pRich->SetSel( 0, -1);
    //CString strOut = pRich->GetSelText();
    //StdString str2 = strOut;

    //文字を先に設定
    //SetText(strOut);
    int nLineCount = pRich->GetLineCount();
    int nLineIndex;
    int nLineLength;

    // Dump every line of text of the rich edit control. 
    //TCHAR  szBuf[4096];
    for (int i = 0; i < nLineCount; i++)
    {
        CString strText;
        nLineIndex = pRich->LineIndex(i);
        nLineLength = pRich->LineLength(nLineIndex) + 1;
        if (nLineLength != 1)
        {
            //int iRet = pRich->GetLine(i, &szBuf[0],  4096);
            //szBuf[nLineLength] = 0;
            //m_strOut += szBuf;

            int iRet2 = pRich->GetLine(i, strText.GetBufferSetLength(nLineLength), nLineLength);

            //int iLen2 = strText.GetLength();
            strText.SetAt(nLineLength - 1, 0);
            m_strOut += strText;

            strText.ReleaseBuffer(nLineLength);
        }

        if ((i + 1) != nLineCount)
        {
            m_strOut += _T("\n");
        }
    }

    long lLen = SizeToInt(m_strOut.length());

    CHARFORMAT2 cf;
    cf.cbSize = sizeof(CHARFORMAT2);
    cf.dwMask = CFM_BOLD | CFM_ITALIC | CFM_UNDERLINE | CFM_STRIKEOUT |
        CFM_SUBSCRIPT | CFM_SUPERSCRIPT |
        CFM_SIZE | CFM_COLOR | CFM_FACE | CFM_OFFSET | CFM_CHARSET;

    FONT_DATA fnt;
    memset(&fnt.fntData, 0, sizeof(fnt.fntData));

    fnt.fntData.lfHeight = 0;                  // 文字セルまたは文字の高さ
    fnt.fntData.lfWidth = 0;               // 平均文字幅
    fnt.fntData.lfEscapement = 0;         // 文字送りの方向とX軸との角度
    fnt.fntData.lfOrientation = 0;                     // ベースラインとX軸との角度
    fnt.fntData.lfOutPrecision = 1;                  // 出力精度
    fnt.fntData.lfClipPrecision = 1;                  // クリッピングの精度

    m_FontData.clear();
    int iStrCnt = 0;
    for (long lCnt = 0; lCnt < lLen; lCnt++)
    {

        pRich->SetSel(lCnt, lCnt + 1);
        pRich->GetSelectionCharFormat(cf);
        CString str = pRich->GetSelText();

        StdChar ch = m_strOut.at(iStrCnt);

        fnt.fntData.lfWeight = ((cf.dwEffects & CFE_BOLD) ? FW_BOLD : 0);// フォントの太さ
        fnt.fntData.lfItalic = ((cf.dwEffects & CFE_ITALIC) ? 1 : 0);    // イタリック体指定
        fnt.fntData.lfUnderline = ((cf.dwEffects & CFE_UNDERLINE) ? 1 : 0); // 下線付き指定
        fnt.fntData.lfStrikeOut = ((cf.dwEffects & CFE_STRIKEOUT) ? 1 : 0); // 打ち消し線付き指定
        fnt.fntData.lfCharSet = cf.bCharSet;       // キャラクタセット
        fnt.fntData.lfQuality = DEFAULT_QUALITY;    // 出力品質
        fnt.fntData.lfPitchAndFamily = cf.bPitchAndFamily;                  // ピッチとファミリ


        _tcscpy(fnt.fntData.lfFaceName, cf.szFaceName);

        fnt.uiStyle = 0;
        fnt.uiStyle |= ((cf.dwEffects & CFE_BOLD) ? BOLD : 0);     // フォントの太さ
        fnt.uiStyle |= ((cf.dwEffects & CFE_ITALIC) ? ITALIC : 0);     // イタリック体指定
        fnt.uiStyle |= ((cf.dwEffects & CFE_UNDERLINE) ? UNDER : 0);   // 下線付き指定
        fnt.uiStyle |= ((cf.dwEffects & CFE_STRIKEOUT) ? STRIKE : 0);  // 打ち消し線付き指定
        fnt.uiStyle |= ((cf.dwEffects & CFE_SUBSCRIPT) ? LOWER : 0);   // 下付き文字
        fnt.uiStyle |= ((cf.dwEffects & CFE_SUPERSCRIPT) ? UPPER : 0);  // 上付き文字


                                                                        // 1/20point単位
        fnt.dHeight = double(cf.yHeight) / 20.0;
        fnt.crColor = cf.crTextColor;

        m_FontData.push_back(std::make_shared<FONT_DATA>(fnt));
        iStrCnt++;
    }
}


/**
*  @brief   リッチエディットへの文字出力
*  @param   [in out]  pRich   リッチエディットコントロール
*  @retval  なし
*  @note
*/
void CExtText::SetToRich(CRichEditCtrl* pRich) const
{
    STD_ASSERT(pRich != 0);

    CString  strOut;

    //全て削除
    pRich->SetSel(0, -1);
    pRich->Clear();

    //テキスト設定
    pRich->ReplaceSel(m_strOut.c_str());

    size_t lLen = m_strOut.length();
    size_t lLen2 = m_FontData.size();

    STD_ASSERT(lLen == lLen2);


    CHARFORMAT2 cf;
    memset(&cf, 0, sizeof(cf));
    cf.cbSize = sizeof(CHARFORMAT2);
    cf.dwMask = CFM_BOLD | CFM_ITALIC | CFM_UNDERLINE | CFM_STRIKEOUT |
        CFM_SUBSCRIPT | CFM_SUPERSCRIPT |
        CFM_SIZE | CFM_COLOR | CFM_FACE | CFM_OFFSET | CFM_CHARSET;

    unsigned int    uiStyle = 0;
    for (long lCnt = 0; lCnt < lLen2; lCnt++)
    {
        cf.dwEffects = 0;
        uiStyle = m_FontData[lCnt]->uiStyle;
        cf.dwEffects |= ((uiStyle & BOLD) ? CFE_BOLD : 0);
        cf.dwEffects |= ((uiStyle & ITALIC) ? CFE_ITALIC : 0);
        cf.dwEffects |= ((uiStyle & UNDER) ? CFE_UNDERLINE : 0);
        cf.dwEffects |= ((uiStyle & STRIKE) ? CFE_STRIKEOUT : 0);
        cf.dwEffects |= ((uiStyle & LOWER) ? CFE_SUBSCRIPT : 0);
        cf.dwEffects |= ((uiStyle & UPPER) ? CFE_SUPERSCRIPT : 0);

        cf.yHeight = int(m_FontData[lCnt]->dHeight * 20.0);
        cf.bCharSet = m_FontData[lCnt]->fntData.lfCharSet;
        cf.crTextColor = m_FontData[lCnt]->crColor;
        cf.bPitchAndFamily = m_FontData[lCnt]->fntData.lfPitchAndFamily;
        _tcscpy(cf.szFaceName, m_FontData[lCnt]->fntData.lfFaceName);
        pRich->SetSel(lCnt, lCnt + 1);
        pRich->SetSelectionCharFormat(cf);
    }
    pRich->SetSel(0, 0);
    pRich->EmptyUndoBuffer();

}

int CExtText::GetAverageFontWidth(HDC hDc, int iHeight, StdString strFontName) const
{
    LOGFONT         fntData;
    fntData.lfHeight = iHeight;                    // 文字セルまたは文字の高さ
    fntData.lfWidth = 0;                     // 平均文字幅
    fntData.lfEscapement = 0;                     // 文字送りの方向とX軸との角度
    fntData.lfOrientation = 0;                     // ベースラインとX軸との角度
    fntData.lfCharSet = DEFAULT_CHARSET;       // キャラクタセット
    fntData.lfOutPrecision = 1;                  // 出力精度
    fntData.lfClipPrecision = 1;                  // クリッピングの精度
    fntData.lfQuality = DEFAULT_QUALITY;    // 出力品質
    fntData.lfPitchAndFamily = 0;                  // ピッチとファミリ
    fntData.lfFaceName[0] = 0;                     // フォント名
    fntData.lfWeight = 0;
    fntData.lfItalic = 0;
    fntData.lfUnderline = 0;
    fntData.lfStrikeOut = 0;

    CDC dc;
    if (strFontName != _T(""))
    {
        _tcscpy(fntData.lfFaceName, strFontName.c_str());
    }

    HFONT fnt;
    fnt = ::CreateFontIndirect(&fntData);
    //StdString strText(_T("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"));
    //StdString strText(_T("abcAbcEFg"));

    HFONT oldFnt;
    oldFnt = (HFONT)::SelectObject(hDc, fnt);
    //GetTextExtentPoint32(hDc, strText.c_str(), strText.length(), &szText);

    TEXTMETRIC tm;
    ::GetTextMetrics(hDc, &tm);

    ::SelectObject(hDc, oldFnt);

#if 0
    DB_PRINT(_T("Text %s %d\n"), strFontName.c_str(), iHeight);
    DB_PRINT(_T("tmHeight %d\n"), tm.tmHeight);
    DB_PRINT(_T("tmAscent %d\n"), tm.tmAscent);
    DB_PRINT(_T("tmDescent %d\n"), tm.tmDescent);
    DB_PRINT(_T("tmInternalLeading %d\n"), tm.tmInternalLeading);
    DB_PRINT(_T("tmExternalLeading %d\n"), tm.tmExternalLeading);
    DB_PRINT(_T("tmAveCharWidth %d\n"), tm.tmAveCharWidth);
    DB_PRINT(_T("tmMaxCharWidth %d\n"), tm.tmMaxCharWidth);
    DB_PRINT(_T("tmWeight %d\n"), tm.tmWeight);
    DB_PRINT(_T("tmOverhang %d\n"), tm.tmOverhang);
    DB_PRINT(_T("tmDigitizedAspectX %d\n"), tm.tmDigitizedAspectX);
    DB_PRINT(_T("tmDigitizedAspectY %d\n"), tm.tmDigitizedAspectY);

    DB_PRINT(_T("tmFirstChar %c\n"), tm.tmFirstChar);
    DB_PRINT(_T("tmLastChar  %c\n"), tm.tmLastChar);
    DB_PRINT(_T("tmdefaultChar %c\n"), tm.tmDefaultChar);
    DB_PRINT(_T("tmBreakChar  %c\n"), tm.tmBreakChar);

    DB_PRINT(_T("tmItalic         %d\n"), tm.tmItalic);
    DB_PRINT(_T("tmUnderlined     %d\n"), tm.tmUnderlined);
    DB_PRINT(_T("tmStruckOut      %d\n"), tm.tmStruckOut);
    DB_PRINT(_T("tmPitchAndFamily %d\n"), tm.tmPitchAndFamily);
    DB_PRINT(_T("tmCharSet        %d\n"), tm.tmCharSet);

    DB_PRINT(_T("\n"));
#endif
    return tm.tmAveCharWidth;
}


StdString CExtText::GetDatumTypeName(const POS_TYPE& eType)
{
    switch (eType)
    {
    case NONE:          return GET_STR(STR_POS_TYPE_NONE);
    case LEFT:          return GET_STR(STR_POS_TYPE_LEFT);
    case RIGHT:         return GET_STR(STR_POS_TYPE_RIGHT);
    case MID:           return GET_STR(STR_POS_TYPE_MID);
    case TOP:           return GET_STR(STR_POS_TYPE_TOP);
    case BOTTOM:        return GET_STR(STR_POS_TYPE_BOTTOM);
    case CENTER:        return GET_STR(STR_POS_TYPE_CENTER);
    case TOP_LEFT:      return GET_STR(STR_POS_TYPE_TOP_LEFT);
    case TOP_RIGHT:     return GET_STR(STR_POS_TYPE_TOP_RIGHT);
    case TOP_MID:       return GET_STR(STR_POS_TYPE_TOP_MID);
    case BOTTTOM_LEFT:  return GET_STR(STR_POS_TYPE_BOTTTOM_LEFT);
    case BOTTTOM_RIGHT: return GET_STR(STR_POS_TYPE_BOTTTOM_RIGHT);
    case BOTTTOM_MID:   return GET_STR(STR_POS_TYPE_BOTTTOM_MID);
    case CENTER_LEFT:   return GET_STR(STR_POS_TYPE_CENTER_LEFT);
    case CENTER_RIGHT:  return GET_STR(STR_POS_TYPE_CENTER_RIGHT);
    case CENTER_MID:    return GET_STR(STR_POS_TYPE_CENTER_MID);
    }
    return _T("");
}

StdString CExtText::GetAlignmentTypeName(const ALIGNMENT_TYPE& eType)
{
    switch (eType)
    {
    case AL_LEFT:        return GET_STR(STR_ALIGNMENT_LEFT);
    case AL_RIGHT:       return GET_STR(STR_ALIGNMENT_RIGHT);
    case AL_MID:         return GET_STR(STR_ALIGNMENT_CENTER);
    }
    return _T("");
}

StdString CExtText::GetBaseTypeName(const BASE_TYPE& eType)
{
    switch (eType)
    {
    case FIRST_BASE1:        return GET_STR(STR_BASSE_TYPE_FIRST_LINE);
    case LAST_BASE:          return GET_STR(STR_BASSE_TYPE_LAST_LINE);
    case TEXT_BLOCK:         return GET_STR(STR_BASSE_TYPE_FRAME);
    }
    return _T("");
}


TREE_GRID_ITEM* CExtText::CreateStdPropertyTree(CStdPropertyTree* pProperty,
    NAME_TREE<CStdPropertyItem>* pTree,
    NAME_TREE_ITEM<CStdPropertyItem>* pNextItem)
{
    CStdPropertyItem* pItem;
    //------------
    // 基準点位置
    //------------
    std::vector<POS_TYPE> lstPos = { TOP_LEFT,
        TOP_MID,
        TOP_RIGHT,
        CENTER_LEFT,
        CENTER_MID,
        CENTER_RIGHT,
        BOTTTOM_LEFT,
        BOTTTOM_MID,
        BOTTTOM_RIGHT };


    StdString strDatumList;
    //IDR_TEXT_ALIGNの図形の順番に合わせる
    strDatumList += boost::lexical_cast<StdString>(IDR_TEXT_DATUM);
    strDatumList += _T(",");
    for (POS_TYPE eType : lstPos)
    {
        strDatumList += CExtText::GetDatumTypeName(eType);
        strDatumList += _T(":");
        strDatumList += boost::lexical_cast<StdString>(eType);
        strDatumList += _T(",");
    }

    CStdPropertyItemDef DefDatum(PROP_DROP_DOWN_BMP, //型
        GET_STR(STR_PRO_TEXT_DATUM),                      //データ表示名
        _T("Datum"),                                 //名称
        GET_STR(STR_PRO_INFO_TEXT_DATUM),                 //データ説明
        false,                                       //編集可、不可
        DISP_NONE,                                   //単位
        static_cast<int>(m_eDatum),                  //初期値
        strDatumList,                                //最小値・リスト設定
        0);

    pItem = new CStdPropertyItem(
        DefDatum,     //定義データ
        PropDatum,     //入力時コールバック
        pProperty,          //親グリッド
        NULL,                  //更新時コールバック
        &m_eDatum);   //実データ

    pNextItem = pTree->AddNext(pNextItem,
        pItem,
        DefDatum.strDspName,
        pItem->GetName());

    //------------
    // 文字配置
    //------------
    //------------
    // 基準点位置
    //------------
    std::vector<ALIGNMENT_TYPE> lstAlign = { AL_LEFT,
        AL_MID,
        AL_RIGHT };


    StdString strAlignmentList;
    //IDR_TEXT_ALIGNの図形の順番に合わせる
    strAlignmentList += boost::lexical_cast<StdString>(IDR_TEXT_ALIGN);
    strAlignmentList += _T(",");
    for (ALIGNMENT_TYPE eType : lstAlign)
    {
        strAlignmentList += CExtText::GetAlignmentTypeName(eType);
        strAlignmentList += _T(":");
        strAlignmentList += boost::lexical_cast<StdString>(eType);
        strAlignmentList += _T(",");
    }

    CStdPropertyItemDef DefAlign(PROP_DROP_DOWN_BMP, //型
        GET_STR(STR_PRO_ALIGNMENT),                  //データ表示名
        _T("Alignment"),                             //名称
        GET_STR(STR_PRO_INFO_ALIGNMENT),             //データ説明
        false,                                       //編集可、不可
        DISP_NONE,                                   //単位
        static_cast<int>(m_eAlignment),              //初期値
        strAlignmentList,                            //最小値・リスト設定
        0);

    pItem = new CStdPropertyItem(
        DefAlign,      //定義データ
        PropAlign,     //入力時コールバック
        pProperty,     //親グリッド
        NULL,                  //更新時コールバック
        &m_eAlignment);   //実データ

    pNextItem = pTree->AddNext(pNextItem,
        pItem,
        DefAlign.strDspName,
        pItem->GetName());

    //------------
    // 基準点種別
    //------------
    std::vector<BASE_TYPE> lstBaseType = { FIRST_BASE1,
        LAST_BASE,
        TEXT_BLOCK };


    StdString strBaseTypeList;
    for (BASE_TYPE eType : lstBaseType)
    {
        strBaseTypeList += CExtText::GetBaseTypeName(eType);
        strBaseTypeList += _T(":");
        strBaseTypeList += boost::lexical_cast<StdString>(eType);
        strBaseTypeList += _T(",");
    }

    CStdPropertyItemDef DefBaseType(PROP_DROP_DOWN_ID, //型
        GET_STR(STR_PRO_DATUM_TYPE),                //データ表示名
        _T("BaseType"),                             //名称
        GET_STR(STR_PRO_INFO_DATUM_TYPE),           //データ説明
        false,                                      //編集可、不可
        DISP_NONE,                                  //単位
        static_cast<int>(m_eBase),                  //初期値
        strBaseTypeList,                            //最小値・リスト設定
        0);

    pItem = new CStdPropertyItem(
        DefBaseType,    //定義データ
        PropBaseType,   //入力時コールバック
        pProperty,      //親グリッド
        NULL,           //更新時コールバック
        &m_eBase);      //実データ

    pNextItem = pTree->AddNext(pNextItem,
        pItem,
        DefBaseType.strDspName,
        pItem->GetName());

    //------------
    // 回転角
    //------------
    CStdPropertyItemDef DefAngle(PROP_DOUBLE,           //表示タイプ
        GET_STR(STR_PRO_ANGLE),                   //表示名
        _T("Angle"),                                    //変数名
        GET_STR(STR_PRO_INFO_ANGLE),                 //表示説明
        true,                                           //編集可不可
        DISP_ANGLE,                                     //表示単位
        m_dAngle                                        //初期値
    );

    pItem = new CStdPropertyItem(
        DefAngle,
        PropAngle,
        pProperty,
        NULL,
        (void*)&m_dAngle);

    pNextItem = pTree->AddNext(pNextItem,
        pItem,
        DefAngle.strDspName,
        pItem->GetName());

    //------------
    // X倍率
    //------------
    CStdPropertyItemDef DefScaleX(PROP_DOUBLE,           //表示タイプ
        GET_STR(STR_PRO_X_SCALE),                   //表示名
        _T("XScl"),                                    //変数名
        GET_STR(STR_PRO_INFO_X_SCALE),                 //表示説明
        true,                                           //編集可不可
        DISP_UNIT,                                     //表示単位
        m_dSclX                                        //初期値
    );

    pItem = new CStdPropertyItem(
        DefScaleX,
        PropScaleX,
        pProperty,
        NULL,
        (void*)&m_dSclX);

    pNextItem = pTree->AddNext(pNextItem,
        pItem,
        DefScaleX.strDspName,
        pItem->GetName());

    //------------
    // Y倍率
    //------------
    CStdPropertyItemDef DefScaleY(PROP_DOUBLE,           //表示タイプ
        GET_STR(STR_PRO_Y_SCALE),                   //表示名
        _T("YScl"),                                    //変数名
        GET_STR(STR_PRO_INFO_Y_SCALE),                 //表示説明
        true,                                           //編集可不可
        DISP_UNIT,                                     //表示単位
        m_dSclY                                        //初期値
    );

    pItem = new CStdPropertyItem(
        DefScaleY,
        PropScaleY,
        pProperty,
        NULL,
        (void*)&m_dSclY);

    pNextItem = pTree->AddNext(pNextItem,
        pItem,
        DefScaleY.strDspName,
        pItem->GetName());

    //------------
    // 字間
    //------------
    CStdPropertyItemDef DefPitch(PROP_DOUBLE,           //表示タイプ
        GET_STR(STR_PRO_PITCH),                   //表示名
        _T("Pitch"),                                    //変数名
        GET_STR(STR_PRO_INFO_PITCH),                 //表示説明
        true,                                           //編集可不可
        DISP_UNIT,                                     //表示単位
        m_dPitch                                        //初期値
    );

    pItem = new CStdPropertyItem(
        DefPitch,
        PropPitch,
        pProperty,
        NULL,
        (void*)&m_dPitch);

    pNextItem = pTree->AddNext(pNextItem,
        pItem,
        DefPitch.strDspName,
        pItem->GetName());

    //------------
    // 行間
    //------------
    CStdPropertyItemDef DefLinePitch(PROP_DOUBLE,           //表示タイプ
        GET_STR(STR_PRO_LINE_PITCH),                   //表示名
        _T("LinePitch"),                                    //変数名
        GET_STR(STR_PRO_INFO_LINE_PITCH),                 //表示説明
        true,                                           //編集可不可
        DISP_UNIT,                                     //表示単位
        m_dLinePitch                                        //初期値
    );

    pItem = new CStdPropertyItem(
        DefLinePitch,
        PropLinePitch,
        pProperty,
        NULL,
        (void*)&m_dLinePitch);

    pNextItem = pTree->AddNext(pNextItem,
        pItem,
        DefLinePitch.strDspName,
        pItem->GetName());

    //------------
    // 倍率固定
    //------------
    CStdPropertyItemDef DefFixScale(PROP_BOOL,           //表示タイプ
        GET_STR(STR_PRO_FIX_SCALE),                   //表示名
        _T("FixScale"),                                    //変数名
        GET_STR(STR_PRO_INFO_FIX_SCALE),                 //表示説明
        true,                                           //編集可不可
        DISP_NONE,                                     //表示単位
        m_bFixSize                                        //初期値
    );

    pItem = new CStdPropertyItem(
        DefFixScale,
        PropFixScale,
        pProperty,
        NULL,
        (void*)&m_bFixSize);

    pNextItem = pTree->AddNext(pNextItem,
        pItem,
        DefFixScale.strDspName,
        pItem->GetName());


    //------------
    // 表示倍率固定
    //------------
    CStdPropertyItemDef DefFixDispScale(PROP_BOOL,           //表示タイプ
        GET_STR(STR_PRO_FIX_DISP_SCALE),                   //表示名
        _T("FixDispScale"),                                    //変数名
        GET_STR(STR_PRO_INFO_FIX_DISP_SCALE),                 //表示説明
        true,                                           //編集可不可
        DISP_NONE,                                     //表示単位
        m_bFixDisp                                        //初期値
    );

    pItem = new CStdPropertyItem(
        DefFixDispScale,
        PropFixDispScale,
        pProperty,
        NULL,
        (void*)&m_bFixDisp);

    pNextItem = pTree->AddNext(pNextItem,
        pItem,
        DefFixDispScale.strDspName,
        pItem->GetName());

    //------------
    // 回転固定
    //------------
    CStdPropertyItemDef DefFixRotate(PROP_BOOL,           //表示タイプ
        GET_STR(STR_PRO_FIX_ROTATE),                   //表示名
        _T("FixRotate"),                                    //変数名
        GET_STR(STR_PRO_INFO_FIX_ROTATE),                 //表示説明
        true,                                           //編集可不可
        DISP_NONE,                                     //表示単位
        m_bFixRotate                                        //初期値
    );

    pItem = new CStdPropertyItem(
        DefFixRotate,
        PropFixRotate,
        pProperty,
        NULL,
        (void*)&m_bFixRotate);

    pNextItem = pTree->AddNext(pNextItem,
        pItem,
        DefFixRotate.strDspName,
        pItem->GetName());

    //------------
    // 反転
    //------------
    CStdPropertyItemDef DefReverse(PROP_BOOL,           //表示タイプ
        GET_STR(STR_PRO_REVERSE),                        //表示名
        _T("Reversee"),                                  //変数名
        GET_STR(STR_PRO_INFO_REVERSE),                   //表示説明
        true,                                            //編集可不可
        DISP_NONE,                                       //表示単位
        m_bReverse                                       //初期値
    );

    pItem = new CStdPropertyItem(
        DefReverse,
        PropReverse,
        pProperty,
        NULL,
        (void*)&m_bReverse);

    pNextItem = pTree->AddNext(pNextItem,
        pItem,
        DefReverse.strDspName,
        pItem->GetName());


    //------------
    // 編集可
    //------------
    CStdPropertyItemDef DefEditable(PROP_BOOL,           //表示タイプ
        GET_STR(STR_PRO_EDITABLE),                   //表示名
        _T("Editable"),                                    //変数名
        GET_STR(STR_PRO_INFO_EDITABLE),                 //表示説明
        true,                                           //編集可不可
        DISP_NONE,                                     //表示単位
        m_bEditable                                        //初期値
    );

    pItem = new CStdPropertyItem(
        DefEditable,
        PropEditable,
        pProperty,
        NULL,
        (void*)&m_bEditable);

    pNextItem = pTree->AddNext(pNextItem,
        pItem,
        DefEditable.strDspName,
        pItem->GetName());


    //------------
    // 枠の有無
    //------------
    CStdPropertyItemDef DefUseFrame(PROP_BOOL,           //表示タイプ
        GET_STR(STR_PRO_USE_FRAME),                   //表示名
        _T("UseFrame"),                                    //変数名
        GET_STR(STR_PRO_INFO_USE_FRAME),                 //表示説明
        true,                                           //編集可不可
        DISP_NONE,                                     //表示単位
        m_bUseFrame                                        //初期値
    );

    pItem = new CStdPropertyItem(
        DefUseFrame,
        PropUseFrame,
        pProperty,
        NULL,
        (void*)&m_bUseFrame);

    pNextItem = pTree->AddNext(pNextItem,
        pItem,
        DefUseFrame.strDspName,
        pItem->GetName());


    bool bEnableFrameLine = false;
    if (IsEnableFrame())
    {
        pNextItem = m_psTextFrame->CreateStdPropertyTree(pProperty, pTree, pNextItem);
        bEnableFrameLine = m_psTextFrame->IsEnableLine();
    }

    if(!bEnableFrameLine)
    {
        m_psTextFrame->LineProperty()->CreateStdPropertyTree(pProperty, pTree, pNextItem);
    }


    return pNextItem;
}

bool CExtText::PropDatum(CStdPropertyItem* pData, void* pObj)
{
    CDrawingText* pDrawing = reinterpret_cast<CDrawingText*>(pObj);
    try
    {
        CExtText* pTxt = pDrawing->GetTextInstance();
        if (!pTxt)
        {
            return false;
        }

        pDrawing->AddChgCnt();

        pDrawing->Hide();
        POS_TYPE eType;
        eType = static_cast<POS_TYPE>(pData->anyData.GetInt());

        pTxt->SetDatum(eType);
        pDrawing->Redraw();

    }
    catch (...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

bool CExtText::PropAlign(CStdPropertyItem* pData, void* pObj)
{
    CDrawingText* pDrawing = reinterpret_cast<CDrawingText*>(pObj);
    try
    {
        CExtText* pTxt = pDrawing->GetTextInstance();
        if (!pTxt)
        {
            return false;
        }

        pDrawing->AddChgCnt();

        pDrawing->Hide();
        ALIGNMENT_TYPE eType;
        eType = static_cast<ALIGNMENT_TYPE>(pData->anyData.GetInt());

        pTxt->SetAlign(eType);
        pDrawing->Redraw();
    }
    catch (...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

bool CExtText::PropBaseType(CStdPropertyItem* pData, void* pObj)
{
    CDrawingText* pDrawing = reinterpret_cast<CDrawingText*>(pObj);
    try
    {
        CExtText* pTxt = pDrawing->GetTextInstance();
        if (!pTxt)
        {
            return false;
        }

        pDrawing->AddChgCnt();

        pDrawing->Hide();
        BASE_TYPE eType;
        eType = static_cast<BASE_TYPE>(pData->anyData.GetInt());

        pTxt->SetBase(eType);
        pDrawing->Redraw();
    }
    catch (...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

bool CExtText::PropAngle(CStdPropertyItem* pData, void* pObj)
{
    CDrawingText* pDrawing = reinterpret_cast<CDrawingText*>(pObj);
    try
    {
        CExtText* pTxt = pDrawing->GetTextInstance();
        if (!pTxt)
        {
            return false;
        }

        pDrawing->AddChgCnt();

        pDrawing->Hide();
        double dAngle = pData->anyData.GetDouble();
        pTxt->SetAngle(dAngle);
        pDrawing->Redraw();

    }
    catch (...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

bool CExtText::PropPitch(CStdPropertyItem* pData, void* pObj)
{
    CDrawingText* pDrawing = reinterpret_cast<CDrawingText*>(pObj);
    try
    {
        CExtText* pTxt = pDrawing->GetTextInstance();
        if (!pTxt)
        {
            return false;
        }

        pDrawing->AddChgCnt();

        pDrawing->Hide();
        double dPitch = pData->anyData.GetDouble();
        pTxt->SetPitch(dPitch);
        pDrawing->Redraw();
    }
    catch (...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

bool CExtText::PropLinePitch(CStdPropertyItem* pData, void* pObj)
{
    CDrawingText* pDrawing = reinterpret_cast<CDrawingText*>(pObj);
    try
    {
        CExtText* pTxt = pDrawing->GetTextInstance();
        if (!pTxt)
        {
            return false;
        }

        pDrawing->AddChgCnt();

        pDrawing->Hide();
        double dPitch = pData->anyData.GetDouble();
        pTxt->SetLinePitch(dPitch);
        pDrawing->Redraw();
    }
    catch (...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

bool CExtText::PropScaleX(CStdPropertyItem* pData, void* pObj)
{
    CDrawingText* pDrawing = reinterpret_cast<CDrawingText*>(pObj);
    try
    {
        CExtText* pTxt = pDrawing->GetTextInstance();
        if (!pTxt)
        {
            return false;
        }

        pDrawing->AddChgCnt();

        pDrawing->Hide();
        double dScl = pData->anyData.GetDouble();
        pTxt->SetSclX(dScl);
        pDrawing->Redraw();
    }
    catch (...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

bool CExtText::PropScaleY(CStdPropertyItem* pData, void* pObj)
{
    CDrawingText* pDrawing = reinterpret_cast<CDrawingText*>(pObj);
    try
    {
        CExtText* pTxt = pDrawing->GetTextInstance();
        if (!pTxt)
        {
            return false;
        }

        pDrawing->AddChgCnt();

        pDrawing->Hide();
        double dScl = pData->anyData.GetDouble();
        pTxt->SetSclY(dScl);
        pDrawing->Redraw();
    }
    catch (...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

bool CExtText::PropFixScale(CStdPropertyItem* pData, void* pObj)
{
    CDrawingText* pDrawing = reinterpret_cast<CDrawingText*>(pObj);
    try
    {
        CExtText* pTxt = pDrawing->GetTextInstance();
        if (!pTxt)
        {
            return false;
        }

        pDrawing->AddChgCnt();

        pDrawing->Hide();
        bool bFix = pData->anyData.GetBool();
        pTxt->SetFixSize(bFix);
        pDrawing->Redraw();
    }
    catch (...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}
bool CExtText::PropFixDispScale(CStdPropertyItem* pData, void* pObj)
{
    CDrawingText* pDrawing = reinterpret_cast<CDrawingText*>(pObj);
    try
    {
        CExtText* pTxt = pDrawing->GetTextInstance();
        if (!pTxt)
        {
            return false;
        }

        pDrawing->AddChgCnt();

        pDrawing->Hide();
        bool bFix = pData->anyData.GetBool();
        pTxt->SetFixDispSize(bFix);
        pDrawing->Redraw();
    }
    catch (...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

bool CExtText::PropFixRotate(CStdPropertyItem* pData, void* pObj)
{
    CDrawingText* pDrawing = reinterpret_cast<CDrawingText*>(pObj);
    try
    {
        CExtText* pTxt = pDrawing->GetTextInstance();
        if (!pTxt)
        {
            return false;
        }

        pDrawing->AddChgCnt();

        pDrawing->Hide();
        bool bFix = pData->anyData.GetBool();
        pTxt->SetFixRotate(bFix);
        pDrawing->Redraw();
    }
    catch (...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}



bool CExtText::PropEditable(CStdPropertyItem* pData, void* pObj)
{
    CDrawingText* pDrawing = reinterpret_cast<CDrawingText*>(pObj);
    try
    {
        CExtText* pTxt = pDrawing->GetTextInstance();
        if (!pTxt)
        {
            return false;
        }

        pDrawing->AddChgCnt();

        pDrawing->Hide();
        bool bEdit = pData->anyData.GetBool();
        pTxt->SetEnableEdit(bEdit);
        pDrawing->Redraw();
    }
    catch (...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

bool CExtText::PropReverse(CStdPropertyItem* pData, void* pObj)
{
    CDrawingText* pDrawing = reinterpret_cast<CDrawingText*>(pObj);
    try
    {
        CExtText* pTxt = pDrawing->GetTextInstance();
        if (!pTxt)
        {
            return false;
        }

        pDrawing->AddChgCnt();

        pDrawing->Hide();
        bool bReverse = pData->anyData.GetBool();
        pTxt->SetReverse(bReverse);
        pDrawing->Redraw();
    }
    catch (...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}


bool CExtText::PropUseFrame(CStdPropertyItem* pData, void* pObj)
{
    CDrawingText* pDrawing = reinterpret_cast<CDrawingText*>(pObj);
    try
    {
        CExtText* pTxt = pDrawing->GetTextInstance();
        if (!pTxt)
        {
            return false;
        }

        pDrawing->AddChgCnt();

        pDrawing->Hide();
        bool bUse = pData->anyData.GetBool();
        pTxt->SetFrame(bUse);
        if (!bUse)
        {
            TEXT_FRAME* pFrame = pTxt->Frame();
            if (pFrame)
            {
                pFrame->SetAutoFit(false);
                pFrame->SetUseMargin(false);
            }
        }

        pDrawing->Redraw();

        //リスト再描画
        pDrawing->ResetProperty();
        ::PostMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_UPDATE_PROPERTY,
            (WPARAM)0, (LPARAM)0);

    }
    catch (...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}


//!< 文字基準位置->リソースID変換
int  CExtText::ConvDatumToId(POS_TYPE ePos)
{
    int iResId = ID_CHAR_DATUM_BL;
    if     (ePos == TOP_LEFT     ) {iResId = ID_CHAR_DATUM_TL;}
    else if(ePos == TOP_MID      ) {iResId = ID_CHAR_DATUM_TM;}
    else if(ePos == TOP_RIGHT    ) {iResId = ID_CHAR_DATUM_TR;}

    else if(ePos == CENTER_LEFT  ) {iResId = ID_CHAR_DATUM_ML;}
    else if(ePos == CENTER_MID   ) {iResId = ID_CHAR_DATUM_MM;}
    else if(ePos == CENTER_RIGHT ) {iResId = ID_CHAR_DATUM_MR;}

    else if(ePos == BOTTTOM_LEFT ) {iResId = ID_CHAR_DATUM_BL;}
    else if(ePos == BOTTTOM_MID  ) {iResId = ID_CHAR_DATUM_BM;}
    else if(ePos == BOTTTOM_RIGHT) {iResId = ID_CHAR_DATUM_BR;}

    return  iResId;
}

//!< リソースID変換->文字基準位置
POS_TYPE CExtText::ConvIdToDatum(int iResId)
{
    if     (iResId == ID_CHAR_DATUM_TL) {return  TOP_LEFT    ;}
    else if(iResId == ID_CHAR_DATUM_TM) {return  TOP_MID     ;}
    else if(iResId == ID_CHAR_DATUM_TR) {return  TOP_RIGHT   ;}

    else if(iResId == ID_CHAR_DATUM_ML) {return  CENTER_LEFT  ;}
    else if(iResId == ID_CHAR_DATUM_MM) {return  CENTER_MID   ;}
    else if(iResId == ID_CHAR_DATUM_MR) {return  CENTER_RIGHT ;}

    else if(iResId == ID_CHAR_DATUM_BL) {return  BOTTTOM_LEFT ;}
    else if(iResId == ID_CHAR_DATUM_BM) {return  BOTTTOM_MID  ;}
    else if(iResId == ID_CHAR_DATUM_BR) {return  BOTTTOM_RIGHT;}
    return  BOTTTOM_LEFT;
}

//!< 文字表示位置設定
int CExtText::ConvTextAlignToId(ALIGNMENT_TYPE eAlign)
{
    int iResId = ID_CHAR_ALIGN_L;
    if     (eAlign == AL_LEFT     ) {iResId = ID_CHAR_ALIGN_L;}
    else if(eAlign == AL_MID      ) {iResId = ID_CHAR_ALIGN_M;}
    else if(eAlign == AL_RIGHT    ) {iResId = ID_CHAR_ALIGN_R;}

    return  iResId;

}

//!< 文字表示位置取得
ALIGNMENT_TYPE CExtText::ConvIdToTextAlign(int iResId)
{
    if     (iResId == ID_CHAR_ALIGN_L) {return  AL_LEFT    ;}
    else if(iResId == ID_CHAR_ALIGN_M) {return  AL_MID     ;}
    else if(iResId == ID_CHAR_ALIGN_R) {return  AL_RIGHT   ;}
    return AL_LEFT;

}


template<class Archive>
void CExtText::load(Archive& ar, const unsigned int version)
{
    SERIALIZATION_INIT;
    //-----------------------------------
    // m_FontData を直接シリアライズすると
    // サイズが大きくなるため,圧縮する
    //-----------------------------------

    SERIALIZATION_LOAD("Angle"  , m_dAngle);
    SERIALIZATION_LOAD("Datum"  , m_eDatum);
    SERIALIZATION_LOAD("Align"  , m_eAlignment);
    SERIALIZATION_LOAD("Base"   , m_eBase);
    SERIALIZATION_LOAD("Text"   , m_strOut);
    SERIALIZATION_LOAD("SclX"       , m_dSclX);
    SERIALIZATION_LOAD("SclY"       , m_dSclY);
    SERIALIZATION_LOAD("Pitch"      , m_dPitch);
    SERIALIZATION_LOAD("LinePitch"  , m_dLinePitch);
    SERIALIZATION_LOAD("FixSize"    , m_bFixSize);
    SERIALIZATION_LOAD("FixDisp"    , m_bFixDisp);        
    SERIALIZATION_LOAD("FixRotate"  , m_bFixRotate);
    SERIALIZATION_LOAD("Editable"   , m_bEditable);


    SERIALIZATION_LOAD("Vertical"   , m_bVertical);
    SERIALIZATION_LOAD("UseFrame"      , m_bUseFrame);
    TEXT_FRAME frame;
    SERIALIZATION_LOAD("Frame"     , frame);
    m_psTextFrame = std::make_unique<TEXT_FRAME>(frame);

    COMPRESS_VECTOR<FONT_DATA> compressFont;
    SERIALIZATION_LOAD("Format" , compressFont);
    compressFont.GetSharedVector(&m_FontData);
    MOCK_EXCEPTION_FILE(e_file_read);

}


template<class Archive>
void CExtText::save(Archive& ar, const unsigned int version) const
{
    SERIALIZATION_INIT;
    //-----------------------------------
    // m_FontData を直接シリアライズすると
    // サイズが大きくなるため,圧縮する
    //-----------------------------------
    COMPRESS_VECTOR<FONT_DATA> compressFont;
    compressFont.SetSharedVector(&m_FontData);

    SERIALIZATION_SAVE("Angle"  , m_dAngle);
    SERIALIZATION_SAVE("Datum"  , m_eDatum);
    SERIALIZATION_SAVE("Align"  , m_eAlignment);
    SERIALIZATION_SAVE("Base"   , m_eBase);
    SERIALIZATION_SAVE("Text"   , m_strOut);
    SERIALIZATION_SAVE("SclX"       , m_dSclX);
    SERIALIZATION_SAVE("SclY"       , m_dSclY);
    SERIALIZATION_SAVE("Pitch"      , m_dPitch);
    SERIALIZATION_SAVE("LinePitch"  , m_dLinePitch);
    SERIALIZATION_SAVE("FixSize"    , m_bFixSize);
    SERIALIZATION_SAVE("FixDisp"    , m_bFixDisp);        
    SERIALIZATION_SAVE("FixRotate"  , m_bFixRotate);
    SERIALIZATION_SAVE("Editable"   , m_bEditable);


    SERIALIZATION_SAVE("Vertical"   , m_bVertical);
    SERIALIZATION_SAVE("UseFrame"      , m_bUseFrame);
    SERIALIZATION_SAVE("Frame"     , *m_psTextFrame);
    SERIALIZATION_SAVE("Format" , compressFont);

    MOCK_EXCEPTION_FILE(e_file_write);

}