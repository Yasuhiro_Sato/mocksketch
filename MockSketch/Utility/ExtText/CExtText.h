/**
 * @brief			CExtTextヘッダーファイル
 * @file		    CExtText.h
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

#if !defined (__EXT_TEXT_H__)
#define __EXT_TEXT_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "../CompressVector.h"



class MAT2D;
class CDrawingText;
class CDrawingView;
class BLOCK_DATA;
class FONT_LINE_DATA;
class TEXT_FRAME;
struct FONT_DATA;
struct ESC_BLOCK_MODIFER;
struct FONT_OUT_DATA;
struct FONT_WORK_DATA;
class SPLINE2D;

#include "Utility/CStdPropertyTree.h"
#include "System/CSystem.h"
#include "System/MOCK_ERROR.h"
#include "./TEXT_TYPE.h"
#include <memory>



//class TREE_GRID_ITEM;



struct TEXT_PARAM
{
    double dScl;           //倍率 
    bool   bIndex;         //インデックス使用有無
    int    iId;            //インデックス
    //-------------------------
    POINT2D  pt2dSel;           //
    FONT_WORK_DATA* pWorkData;
    bool   bSelect;
    bool   bDrag;

    //int    iBase;
    //int    iLastLineBase;
    //int    iWidthBase;
    //-------------------------
    double dOffsetAngle;   //追加角度
    bool   bForceColor;    //強制色設定の有無
    COLORREF cr;           //設定色
    bool   bEmphasis;      //強調表示
    bool bFrameColor;      //フレーム強制色設定の有無
    COLORREF crFrame;      //フレーム強制設定色
};







class CExtText
{
public:

    
//*------------------------
// 書式データ
//-------------------------

    //ワーク用データ

public:
    struct  LINE_LIST
    {
        POINT2D ptOffset;
        std::vector<CAny> lstAny;
    };

   
protected:
    friend CDrawingText;
    friend CExtText;

    double      m_dAngle;
    StdString   m_strOut;
    
    POS_TYPE            m_eDatum;        //基準点位置
    ALIGNMENT_TYPE      m_eAlignment;    //文字揃え           
    BASE_TYPE           m_eBase;         //基準位置

    std::vector< std::shared_ptr< FONT_DATA > > m_FontData;              //書式データ
    double      m_dSclX;
    double      m_dSclY;

    bool        m_bFixSize;     //図形の拡大縮小で文字サイズを変更しない
    bool        m_bFixDisp;     //表示の拡大縮小で文字サイズを変更しない

    double      m_dPitch;       //文字間隔
    double      m_dLinePitch;   //行間隔
    
    bool        m_bFixRotate;
    bool        m_bEditable;
    bool        m_bVertical;

    bool        m_bUseFrame;  

    bool        m_bReverse;            

    bool        m_bFixPitch;

    std::unique_ptr<TEXT_FRAME> m_psTextFrame;

    //ワーク用データ
    std::shared_ptr<FONT_DATA>   m_psDefautFont;

    //std::vector<FONT_OUT_DATA>  m_FontOutData;      //
    //std::vector<FONT_LINE_DATA> m_FontLineData;     //表示サイズ決定に使用


private:
    friend class boost::serialization::access;  

    BOOST_SERIALIZATION_SPLIT_MEMBER();
    template<class Archive>
    void load(Archive& ar, const unsigned int version);

    template<class Archive>
    void save(Archive& ar, const unsigned int version) const;


public:
    //!< コンストラクター
    CExtText();

    //!< コンストラクター
    CExtText(const LOGFONT* pFONT, COLORREF crDef);

    //!< コピーコンストラクター
    CExtText(const CExtText& txt);

    //!< テキスト取得
    StdString GetText()const {return m_strOut;}

    //!< テキスト設定
    void SetText(const StdChar* strText);

    //!< 色設定
    void SetColor(int iStart, int iEnd, COLORREF crText);

    //!< フォント
    void SetFont(int iStart, int iEnd,const LOGFONT* pFONT);

    //!< 表示スタイル設定
    void SetStyle(int iStart, int iEnd, unsigned int uiStyle);

    //!< 表示スタイル設定
    void SetStyle(int iStart, int iEnd, unsigned int uiStyle, double dHight, COLORREF color, StdString strFontName);

    //!< 文字高さ設定
    void SetHeight(int iStart, int iEnd, double dHight);

    //!< 角度設定
    void SetAngle(double dAngle);

    //!< 角度取得
    double GetAngle() const;


    //!< 文字アライメント設定
    void SetAlign(ALIGNMENT_TYPE ePos);

    //!< 文字アライメント取得
    ALIGNMENT_TYPE GetAlign() const;

    //!< 基準点位置設定
    void SetDatum(POS_TYPE ePos);

    //!< 基準点位置取得
    POS_TYPE GetDatum() const;

    //!< 基準種別設定
    void SetBase(BASE_TYPE ePos);

    //!< 基準点種別取得
    BASE_TYPE GetBase() const;

    //!< 文字基準位置->リソースID変換
    static int  ConvDatumToId(POS_TYPE ePos );

    //!< リソースID変換->文字基準位置
    static POS_TYPE ConvIdToDatum(int iId);

    //!< 文字表示位置設定
    static  int ConvTextAlignToId(ALIGNMENT_TYPE eAlign );

    //!< 文字表示位置取得
    static ALIGNMENT_TYPE   ConvIdToTextAlign(int iId);



    //!<枠表示可否取得        
    bool IsEnableFrame() const;

    //!<枠表示可否設定        
    void SetFrame(bool bUse);
                                                        
    TEXT_FRAME* Frame() const;

    static StdString GetDatumTypeName(const POS_TYPE& eType);
    static StdString GetAlignmentTypeName(const ALIGNMENT_TYPE& eType);
    static StdString GetBaseTypeName(const BASE_TYPE& eType);


    void DrawFrame( CDrawingView* pView, 
        HDC hDc, 
        const TEXT_PARAM* pParam,
        int iFront,
        int iWidthBase,
        int iFirstLineHeight,
        int iLastTextLineHeight,
        int iLayerId
    )const;

    //!< 表示
    void Draw(HDC hDc, 
              CDrawingView* pView,
              POINT pt, 
              FONT_WORK_DATA* pWorkData,
              const TEXT_PARAM* pParam,
              int iLayerId
              ) const;

   //!< 表示領域取得
   void GetFramePoint(std::vector<POINT2D>* lstPt) const;


    //!< 表示領域取得
   void GetDispSize(SIZE* pSize, 
                    HDC hDc, 
                    const CDrawingView* pView,
                    double dScl) const;

   //!< リッチエディットからの文字取得
    void SetFromRich(CRichEditCtrl* pRich);

    //!< リッチエディットへの文字出力
    void SetToRich(CRichEditCtrl* pRich) const;

    //!< 代入演算子
    CExtText& operator = (const CExtText & m);

    //!< 等価演算子
    bool operator == (const CExtText & m) const;

    //!< 不等価演算子
    bool operator != (const CExtText & m) const;

    //!< 文字サイズ固定
    void SetFixSize(bool bFixSize);
    bool IsFixSize() const;

    //!< 表示サイズ固定
    void SetFixDispSize(bool bFixSize);
    bool IsFixDispSize() const;

    //!< 行列適用
    virtual void Matrix(const MAT2D& mat2D);
    void MatrixDirect(const MAT2D& mat2D);

    //!< 拡大率取得
    double GetSclX() const;
    double GetSclY() const;

    void SetSclX(double dScl);
    void SetSclY(double dScl);

    //!< 字間
    double GetPitch() const;
    void SetPitch(double dPitch);

    //!< 行間
    double GetLinePitch() const;
    void SetLinePitch(double dPitch);

    //!< 回転固定
    void SetFixRotate(bool dFixSize);
    bool IsFixRotate() const;

    //!< 編集
    void SetEnableEdit(bool dFixSize);
    bool IsEnableEdite() const;

    //!< 反転
    void SetReverse(bool bReverse);
    bool IsReverse() const;


    //!< フォント平均幅取得
    int GetAverageFontWidth(HDC hDc, int iHeight,  StdString strFontName) const;

    //!< 枠線表示点
    std::vector<POINT>* GetDispFramePoint() const;
    std::vector<POINT2D>* GetFramePoint() const;

    //------------------------------
    TREE_GRID_ITEM*  CreateStdPropertyTree( CStdPropertyTree* pProperty,
                                    NAME_TREE<CStdPropertyItem>* pTree,
                                    NAME_TREE_ITEM<CStdPropertyItem>* pNextItem);

    POINT2D _FramePos(  double dWidthBase,
                        double dFirstLineHeight,
                        double dLastTextLineHeight ) const;


    void GetTL(POINT* pToTL, 
        int iFirstLineHeight, 
        int iTotalHeight, 
        int iBaseWidth, 
        int iTextAreaWide,                  //テキスト領域の幅（フレーム設定時は フレーム幅-マージン  
        const CDrawingView* pView, 
        double dScl) const;


    //!< 表示テスト
    HFONT DrawTest(FONT_WORK_DATA* pWork, 
        HDC hDc, 
        const CDrawingView* pView, 
        double dScl, 
        bool bBold) const;

 
    void ConvSpline(
        std::vector<std::vector<LINE_LIST>>* plst,
        CDrawingView* pView,
        int iLayerId
    ) const;


    static bool PropDatum(CStdPropertyItem* pData, void* pObj);
    static bool PropAlign(CStdPropertyItem* pData, void* pObj);
    static bool PropBaseType(CStdPropertyItem* pData, void* pObj);
    static bool PropAngle(CStdPropertyItem* pData, void* pObj);
    static bool PropScaleX(CStdPropertyItem* pData, void* pObj);
    static bool PropScaleY(CStdPropertyItem* pData, void* pObj);
    static bool PropPitch(CStdPropertyItem* pData, void* pObj);
    static bool PropLinePitch(CStdPropertyItem* pData, void* pObj);

    static bool PropFixScale(CStdPropertyItem* pData, void* pObj);
    static bool PropFixDispScale(CStdPropertyItem* pData, void* pObj);
    static bool PropFixRotate(CStdPropertyItem* pData, void* pObj);
    static bool PropEditable(CStdPropertyItem* pData, void* pObj);
    static bool PropUseFrame(CStdPropertyItem* pData, void* pObj);
    static bool PropReverse(CStdPropertyItem* pData, void* pObj);



protected:


   //!< フォント作成
    HFONT CreateFont(const FONT_DATA* pData, double dScl, bool bBold, HDC hDC, bool bPrint, bool bAngle) const;


    int _ConvPitchToScreen(double dPich, double dScl, const CDrawingView* pView) const;
    double _ConvScreenToPitch(int iPich, double dScl, const CDrawingView* pView) const;

    void _CreateBlock(StdChar ch, int iPitch, 
        const ESC_BLOCK_MODIFER& m, 
        FONT_WORK_DATA* pWork,
        FONT_LINE_DATA** ppL, 
        BLOCK_DATA** ppB) const;

    void _CloseBlock(ESC_BLOCK_MODIFER* pM, 
        FONT_WORK_DATA* pWork,
        int iPL, 
        FONT_LINE_DATA** ppL, 
        BLOCK_DATA** ppB)const;

    void _LineMulti3( CDrawingView* pView,
        int iLayerId,     
        const std::vector<POINT2D>* pLstPt, bool bSkioFirstPoint,
        bool bFixLayerScl) const;

    void _FillMulti3( CDrawingView* pView,
        int iLayerId,
        const std::vector<POINT2D>* pLstPt,
        bool bFixLayerScl) const;

    void _ConvSpline( std::vector<LINE_LIST>* lstAny,
                      std::vector<char>* pBuf,
                      const POINT2D& ptOffset,
                      CDrawingView* pView,
                      int iLayerId) const;

    BLOCK_DATA* _GetBeforeBlock(BLOCK_DATA* pBlock, int iBeforeNum) const;


};
BOOST_CLASS_VERSION(CExtText, 1);

#endif //ExtText