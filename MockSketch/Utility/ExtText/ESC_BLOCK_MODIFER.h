/**
* @brief			CExtTextヘッダーファイル
* @file		    CExtText.h
* @author			Yasuhiro Sato
* @date			02-2-2009 03:00:24
* 
* @note	
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
* 
* @remarks	        
*			
*
* $
* $
* 
*/

#if !defined (__ESC_BLOCK_MODIFER_H__)
#define __ESC_BLOCK_MODIFER_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "./TEXT_TYPE.h"

struct ESC_BLOCK_MODIFER
{
    bool bUse = false;
    double  dX  = 0.0;           //1に対して 前フォントの25% 
    double  dY  = 0.0;           //1に対して 前フォントの25% 
    double  dScl = 0.0;          //1に対して 前フォントの25%
    COLORREF     cr      = 0;
    COLORREF     bgColor = 0;
    DWORD        dwSts = 0;                    //ブロックステータス
    ESC_STS      eEscReturnSts = ESC_NONE;     //ブロックを閉じたときに復帰するステータス
    bool         bBeforeOffset = false;        
    bool         bMulti = false;      
    int          iBeforeOffset = 1;               
    StdString    strFontName;
    StdChar chOffset = 0;   //乗数指定用にフォントを記憶

    void Clear()
    {
        bUse = false;
        bMulti = false;
        dX = 0.0;
        dY = 0.0;
        dScl = 0.0;
        cr      = 0;
        bgColor = 0;
        dwSts   = 0;   
        eEscReturnSts = ESC_NONE;
        chOffset = 0;
        bBeforeOffset = false;
        iBeforeOffset = 1;
        strFontName = _T("");
    }

    ESC_BLOCK_MODIFER& operator =(const ESC_BLOCK_MODIFER& block)
    {
        bUse        = block.bUse     ;
        dX          = block.dX       ;
        dY          = block.dY       ;
        dScl        = block.dScl     ;
        cr          = block.cr       ;
        bgColor     = block.bgColor  ;
        dwSts       = block.dwSts    ;                     
        chOffset    = block.chOffset ;
        iBeforeOffset    = block.iBeforeOffset ;
        strFontName = block.strFontName ;
        return *this;
    }
};

#endif //__ESC_BLOCK_MODIFER_H__