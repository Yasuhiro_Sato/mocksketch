/**
* @brief        FONT_DATAヘッダーファイル
* @file	        FONT_DATA.h
* @author           Yasuhiro Sato
* @date	        07-2-2009 20:03:09
* 
* @note	
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
* 
* @remarks	        
*			
*
* $
* $
* 
*/
#ifndef __FONT_DATA_H_
#define __FONT_DATA_H_
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/

struct FONT_DATA
{
    LOGFONT         fntData;
    unsigned int    uiStyle;
    COLORREF        crColor;
    double          dHeight;    //point
    int             iDummy1;
    int             iDummy2;

    FONT_DATA()
    {
        memset(&fntData, 0, sizeof(LOGFONT));
        uiStyle = 0;
        crColor = 0;
        dHeight = 0.0;
        iDummy1 = 0;
        iDummy2 = 0;
    }

    bool operator == (const FONT_DATA & right) const
    {
        if (memcmp(&fntData, &right.fntData, sizeof(LOGFONT)- sizeof(fntData.lfFaceName)) != 0)
        {
            return false;
        }

        if ((_tcscmp(fntData.lfFaceName, right.fntData.lfFaceName)) != 0)
        {
            return false;
        }

        if (uiStyle != right.uiStyle){return false;}
        if (crColor != right.crColor){return false;}
        if (fabs(dHeight - right.dHeight) > NEAR_ZERO){return false;}
        return true;
    }

    bool operator != (const FONT_DATA & right) const
    {
        return ( !(*this == right));
    }

private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
            StdString       strFont; 
        if (Archive::is_saving::value)
        {
            strFont = fntData.lfFaceName;
        }

        SERIALIZATION_BOTH("Font"     , strFont);
        SERIALIZATION_BOTH("Style"    , uiStyle);
        SERIALIZATION_BOTH("Color"    , crColor);
        SERIALIZATION_BOTH("Height"   , dHeight);

        if (Archive::is_loading::value)
        {
            fntData.lfHeight = 0;                  // 文字セルまたは文字の高さ
            fntData.lfWidth  = 0;               // 平均文字幅
            fntData.lfEscapement  = 0;         // 文字送りの方向とX軸との角度
            fntData.lfOrientation = 0;                     // ベースラインとX軸との角度
            fntData.lfCharSet     = DEFAULT_CHARSET;       // キャラクタセット
            fntData.lfOutPrecision   = 1;                  // 出力精度
            fntData.lfClipPrecision  = 1;                  // クリッピングの精度
            fntData.lfQuality        = DEFAULT_QUALITY;    // 出力品質
            fntData.lfPitchAndFamily = 0;                  // ピッチとファミリ
            fntData.lfFaceName[0] = 0;                     // フォント名


            fntData.lfWeight     = ((uiStyle & BOLD) ?  FW_BOLD :0);
            fntData.lfItalic     = ((uiStyle & ITALIC) ?  1 :0);
            fntData.lfUnderline  = ((uiStyle & UNDER) ?  1 :0);
            fntData.lfStrikeOut  = ((uiStyle & STRIKE) ?  1 :0);
            if (strFont != _T(""))
            {
                _tcscpy( fntData.lfFaceName, strFont.c_str() );
            }
        }
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }
};
#endif //__FONT_DATA_H_
