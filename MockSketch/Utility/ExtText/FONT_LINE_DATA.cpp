/**
* @brief			FONT_LINE_DATA実装ファイル
* @file			FONT_LINE_DATA.cpp
* @author			Yasuhiro Sato
* @date			07-2-2009 20:02:47
* 
* @note	
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
* 
* @remarks	        
*			
*
* $
* $
* 
*/
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MockSketch.h"
#include "./FONT_LINE_DATA.h"
#include "./BLOCK_DATA.h"
#include "./TEXT_TYPE.h"

FONT_LINE_DATA::FONT_LINE_DATA() :
    pParentBlock(NULL),
    iLineHeight(0),
    iLineBottom(0),
    iTotalBottom(0),
    iLineFront(0),
    iLineWidth(0),
    iAlign(0)
{
    szFont.cx = 0;
    szFont.cy = 0;
#if  DBG_PRINT
    pName = std::make_unique<StdString>();
#endif 
}

FONT_LINE_DATA::FONT_LINE_DATA(const FONT_LINE_DATA& line)
{
    *this = line;
}

FONT_LINE_DATA& FONT_LINE_DATA::operator =(const FONT_LINE_DATA& line)
{
    iLineHeight = line.iLineHeight;
    iAlign = line.iAlign;
    iLineBottom = line.iLineBottom;
    iTotalBottom = line.iTotalBottom;
    iLineFront = line.iLineFront;
    iLineWidth = line.iLineWidth;
    iAlign = line.iAlign;

    pParentBlock = line.pParentBlock;
    int iCnt = 0;
    lstBlock.clear();
    for (auto ite = line.lstBlock.begin();
        ite != line.lstBlock.end();
        ite++)
    {
        BLOCK_DATA* pBlock = (*ite).get();
        lstBlock.push_back(std::make_shared<BLOCK_DATA>());
        BLOCK_DATA* pBlockDest = lstBlock.at(iCnt).get();
        *pBlockDest = *pBlock;
        iCnt++;
    }

#if  DBG_PRINT
    if (!pName)
    {
        pName = std::make_unique<StdString>();
    }
    *pName = *line.pName;
#endif 

    return *this;
}

FONT_LINE_DATA* FONT_LINE_DATA::GetFirstLine()
{
    if (!pParentBlock)
    {
        return this;
    }

    FONT_LINE_DATA* pParent;
    pParent = pParentBlock->pParentLine;
    if (!pParent)
    {
        return this;
    }

    return pParent->GetFirstLine();
}


int FONT_LINE_DATA::GetHeight() const
{
    return (iLineHeight);
}

void FONT_LINE_DATA::ExpandSize(BLOCK_DATA* pBlock, bool bChangeBase)
{
#ifdef _DEBUG
    //子ブロックのみ
    bool bChild = false;
    for (auto ite = lstBlock.begin();
        ite != lstBlock.end();
        ite++)
    {
        BLOCK_DATA* pLineBlock = (*ite).get();
        if (pBlock == pLineBlock)
        {
            bChild = true;
            break;
        }
    }
    STD_ASSERT(bChild);
#endif

#if DBG_PRINT
    DB_PRINT(_T("----ExpandSize %s---\n"), this->pName->c_str());
    DB_PRINT(_T("   BLOCK[%d] H:%d,W:%d,F:%d,B:%d,Offset(%d,%d) Scl:%f\n"), 
        pBlock->iBlockNo, pBlock->iHeight, pBlock->iWidth, pBlock->iFront,pBlock->iBottom,
        pBlock->ptEscOffset.x, pBlock->ptEscOffset.y, pBlock->szModifer.dScl);
#endif


    int iStart = pBlock->iFront + pBlock->iOffsetX + pBlock->ptEscOffset.x;
    int iEnd = iStart + pBlock->iWidth;
    int iLineEnd = this->iLineFront + this->iLineWidth;
    if (iStart  < this->iLineFront)
    {
        this->iLineFront = iStart;
    }

    if (iEnd > iLineEnd)
    {
        this->iLineWidth = iEnd - this->iLineFront;
    }
    else
    {
        this->iLineWidth = iLineEnd - this->iLineFront;
    }

    //Y軸は下向きが正（ディスプレイ座標系）
    int iBlockBottom = pBlock->ptEscOffset.y;
    iBlockBottom += pBlock->iBottom;

    int iBlockiTop =   iBlockBottom - pBlock->iHeight;
    int iLineTop =   this->iLineBottom - this->iLineHeight;

    if(iBlockBottom > this->iLineBottom)
    {
        this->iLineBottom  = iBlockBottom;
    }

    if (iLineTop > iBlockiTop)
    {
        iLineTop =  iBlockiTop;
    }



    int iLineHeight =  this->iLineBottom - iLineTop;
    this->iLineHeight =  iLineHeight;

#if DBG_PRINT
    DB_PRINT(_T("   LINE:[%s] H:%d,W:%d,F:%d,B:%d, TB:%d, A:%d\n"),
        pName->c_str(),iLineHeight, iLineWidth, iLineFront, iLineBottom,  iTotalBottom, iAlign);
    DB_PRINT(_T("----ExpandSize End---\n\n"));
#endif

/*
    //この行を所有しているブロックのサイズ更新
    if (this->pParentBlock)
    {
        bool bChangeBase = true;
        this->pParentBlock->ExpandSize(pCurBlock, bChangeBase);
    }
*/    
}

void FONT_LINE_DATA::SetParent()
{
    for (auto ite = lstBlock.begin();
        ite != lstBlock.end();
        ite++)
    {
        (*ite)->pParentLine = this;
        (*ite)->SetParent();
    }
}

POINT   FONT_LINE_DATA::GetTotalOffset() const
{
    POINT ptRet;
    ptRet.x = 0;
    ptRet.y = 0;

    ptRet.x += iAlign;
    //ptRet.x += iLineFront;
    ptRet.y += iTotalBottom;
    ptRet.y -= iLineBottom;

    if (pParentBlock)
    {
        ptRet.x += pParentBlock->ptEscOffset.x;
        ptRet.y += pParentBlock->ptEscOffset.y;
        ptRet.x += pParentBlock->iOffsetX;
        ptRet.x += pParentBlock->ptAlign.x;
        ptRet.y += pParentBlock->ptAlign.y;
        //ptRet.x -= pParentBlock->iFront;

        if (pParentBlock->pParentLine)
        {
            POINT ptParent;
            ptParent = pParentBlock->pParentLine->GetTotalOffset();
            ptRet.x += ptParent.x;
            ptRet.y += ptParent.y;
        }
    }
    return ptRet;
}

void FONT_LINE_DATA::Print(StdString str)
{

    StdString strNmae;
#if  DBG_PRINT
    strNmae = *(pName);
#endif

    DB_PRINT(_T("%s Line(%lx)(W:%d H:%d B:%d TB:%d F:%d A:%d S:%s)ParentBlock=%lx \n"), str.c_str(),
        this,
        iLineWidth, iLineHeight,
        iLineBottom, iTotalBottom, iLineFront,
        iAlign,
        strNmae.c_str(),
        pParentBlock);

        str += _T("  ");

    for (auto block :lstBlock)
    {
        block->Print(str);
    }
}

bool  FONT_LINE_DATA::IsGrid() const
{
    if (!lstBlock.empty())
    {
        return lstBlock[0]->IsGrid();
    }
    return false;
}
