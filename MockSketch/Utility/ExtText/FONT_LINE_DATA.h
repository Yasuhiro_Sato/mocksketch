
/**
* @brief			FONT_LINE_DATAヘッダーファイル
* @file		    FONT_LINE_DATA.h
* @author			Yasuhiro Sato
* @date			02-2-2009 03:00:24
* 
* @note	
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
* 
* @remarks	        
*			
*
* $
* $
* 
*/

#if !defined (__FONT_LINE_DATA_H__)
#define __FONT_LINE_DATA_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
class  BLOCK_DATA;

class FONT_LINE_DATA
{
public:
    FONT_LINE_DATA();
    FONT_LINE_DATA(const FONT_LINE_DATA& line);
    virtual ~FONT_LINE_DATA(){;}
    FONT_LINE_DATA& operator =(const FONT_LINE_DATA& line);
    FONT_LINE_DATA* GetFirstLine();
    int     GetHeight() const;
    void    ExpandSize(BLOCK_DATA* pBlock, bool bChangeBase);
    void    SetParent();
    void    Print(StdString str);
    POINT   GetTotalOffset() const;
    bool    IsGrid() const;
    void SetTopFontSize(SIZE  sz){szFont = sz;}
    SIZE GetTopFontSize(){return szFont;}

public:
    //RECT    rect;      //pParentBlock->pLineを原点とした領域(dot)
    //高さ方向の位置は 親のBLOCK_DATA::CalcArea()
    //実行後に確定される
    //X方向は適宜決定される
    /*
    exampl %:[ABC%U%U%>[123\n4567\n8990123]EFGJ\nIKJL]
    +----------------+
    |   +-------+    |
    |   |    123|    |
    |ABC|   4567|EFGJ|
    |   |8990123|    |
    |   +-------+    |
    |      IJKL      |
    +----------------+
    A〜J,I〜LがLine

    */
    int     iLineWidth;
    int     iLineHeight;  //行の全高
    int     iLineBottom;  //行先頭文字下端からの距離
    int     iTotalBottom; //先頭行文字下端からの距離
    int     iLineFront;  //先頭文字左端から先端までの距離
    int     iAlign;      // 
    std::vector< std::shared_ptr< BLOCK_DATA > > lstBlock;
    BLOCK_DATA* pParentBlock;
    SIZE  szFont;                             //先頭のフォントサイズ                    
    std::unique_ptr<StdString> pName;         //デバッグ用 

protected:
    //SIZE    size;            //最大サイズ(dot)
    //int     iStartX;         //開始位置(dot)
    //int     iStartY;         //開始位置(dot)
    //int     iEndChar;        //終了位置(char)

};

#endif //__FONT_LINE_DATA_H__