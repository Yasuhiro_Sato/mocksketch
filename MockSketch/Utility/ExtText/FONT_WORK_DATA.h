/**
* @brief        FONT_WORK_DATAヘッダーファイル
* @file	        FONT_WORK_DATA.h
* @author           Yasuhiro Sato
* @date	        07-2-2009 20:03:09
* 
* @note	
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
* 
* @remarks	        
*			
*
* $
* $
* 
*/
#ifndef __FONT_WORK_DATA_H_
#define __FONT_WORK_DATA_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "./BLOCK_DATA.H"

class FONT_LINE_DATA;

struct FONT_OUT_DATA
{
    HFONT   hFont;          //フォント(角度付き)
    HFONT   hFontS;         //フォント(0度限定)
    POINT   ptOffset;       //描画位置(LineDataを原点とする)
    bool    bCreateFont;    //フォント生成フラグ
    SIZE    size;           //フォントサイズ
    FONT_LINE_DATA* pLine;
    bool    bDisp;          
    bool    bStartBlock;    //ブロックの先頭

};

struct FONT_WORK_DATA
{
    std::vector<FONT_OUT_DATA>  outFont;      //
                                              //std::vector<FONT_LINE_DATA> lineFont;     //表示サイズ決定に使用
    BLOCK_DATA  block;
    std::deque<BLOCK_DATA*>  stackBlock;

    SIZE        sizeDrawArea;
    POINT       ptOffsetToTL;    //基準位置からみてテキストブロックの左上の位置(dot)
    HFONT       hOldFont;
    bool        bFirstFrameCallback;
    bool        bPrint;

    void Clear()
    {
        outFont.clear();
        block.Clear();
        stackBlock.clear();
        sizeDrawArea.cx = 0;
        sizeDrawArea.cy = 0;
        ptOffsetToTL.x = 0;
        ptOffsetToTL.y = 0;
        hOldFont = 0;
        bPrint   = false;
        //bFirstFrameCallback 初期化しない
    }
};

#endif  //__FONT_WORK_DATA_H_