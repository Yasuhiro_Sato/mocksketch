
/**
* @brief			LINE_PROPERTY実装ファイル
* @file		    LINE_PROPERTY.cpp
* @author			Yasuhiro Sato
* @date			02-2-2009 03:00:24
*
* @note
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
*
* @remarks
*
*
* $
* $
*
*/

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./LINE_PROPERTY.h"
#include "./TEXT_MARGIN.h"
#include "System/CSystem.h"
#include "DrawingObject/CDrawingText.h"
#include "Utility/ExtText/TEXT_FRAME.H"


LINE_PROPERTY::LINE_PROPERTY() :
    iWidth(1),
    iLineType(PS_SOLID)
{
    cr = DRAW_CONFIG->crFront;
}

TREE_GRID_ITEM* LINE_PROPERTY::CreateStdPropertyTree(CStdPropertyTree* pProperty,
    NAME_TREE<CStdPropertyItem>* pTree,
    NAME_TREE_ITEM<CStdPropertyItem>* pNextItem)
{


    NAME_TREE_ITEM<CStdPropertyItem>* pLine;
    NAME_TREE_ITEM<CStdPropertyItem>* pLineNext;
    NAME_TREE_ITEM<CStdPropertyItem>* pParent;
    pParent = pNextItem->pParent;

    //pLine  = pTree->AddNext(pParent, NULL, GET_STR(STR_PRO_LINE), _T("Line"));
    pLine = pTree->AddNext(pNextItem, NULL, GET_STR(STR_PRO_LINE), _T("Line"));

    CStdPropertyItem* pItem;
    //------------
    // 線色
    //------------
    CStdPropertyItemDef DefColor(PROP_COLOR,            //表示タイプ
        GET_STR(STR_PRO_LINE_COLOR),                 //表示名
        _T("LineColor"),                                //変数名
        GET_STR(STR_PRO_INFO_LINE_COLOR),               //表示説明
        true,                                           //編集可不可
        DISP_NONE,                                      //表示単位
        cr                                              //初期値
    );

    pItem = new CStdPropertyItem(
        DefColor,
        PropColor,
        pProperty,
        NULL,
        (void*)&cr);

    pLineNext = pTree->AddChild(pLine,
        pItem,
        DefColor.strDspName,
        pItem->GetName());

    //------------
    // 線幅
    //------------
    CStdPropertyItemDef DefWidth(PROP_LINE_WIDTH,       //表示タイプ
        GET_STR(STR_PRO_LINE_WIDTH),                 //表示名
        _T("LineWidth"),                                //変数名
        GET_STR(STR_PRO_INFO_LINE_WIDTH),               //表示説明
        true,                                           //編集可不可
        DISP_NONE,                                      //表示単位
        iWidth                                          //初期値
    );

    pItem = new CStdPropertyItem(
        DefWidth,
        PropWidth,
        pProperty,
        NULL,
        (void*)&iWidth);

    pLineNext = pTree->AddNext(pLineNext,
        pItem,
        DefWidth.strDspName,
        pItem->GetName());

    //------------
    // 線種
    //------------
    CStdPropertyItemDef DefType(PROP_LINE_TYPE,         //表示タイプ
        GET_STR(STR_PRO_LINE_TYPE),                  //表示名
        _T("LineType"),                                 //変数名
        GET_STR(STR_PRO_INFO_LINE_TYPE),                //表示説明
        true,                                           //編集可不可
        DISP_NONE,                                      //表示単位
        iLineType                                       //初期値
    );

    pItem = new CStdPropertyItem(
        DefType,
        PropType,
        pProperty,
        NULL,
        (void*)&iLineType);

    pLineNext = pTree->AddNext(pLineNext,
        pItem,
        DefType.strDspName,
        pItem->GetName());

    return pLine;
}

bool LINE_PROPERTY::PropColor(CStdPropertyItem* pData, void* pObj)
{
    CDrawingText* pDrawing = reinterpret_cast<CDrawingText*>(pObj);
    try
    {
        CExtText* pTxt = pDrawing->GetTextInstance();
        if (!pTxt)
        {
            return false;
        }

        TEXT_FRAME* pFrame = pTxt->Frame();
        if (!pFrame)
        {
            return false;
        }

        LINE_PROPERTY* pLineProp = pFrame->LineProperty();
        if (!pLineProp)
        {
            return false;
        }

        pDrawing->AddChgCnt();

        pDrawing->Hide();
        COLORREF cr = pData->anyData.GetColor();
        pLineProp->cr = cr;
        pDrawing->Redraw();
    }
    catch (...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

bool LINE_PROPERTY::PropWidth(CStdPropertyItem* pData, void* pObj)
{
    CDrawingText* pDrawing = reinterpret_cast<CDrawingText*>(pObj);
    try
    {
        CExtText* pTxt = pDrawing->GetTextInstance();
        if (!pTxt)
        {
            return false;
        }

        TEXT_FRAME* pFrame = pTxt->Frame();
        if (!pFrame)
        {
            return false;
        }

        LINE_PROPERTY* pLineProp = pFrame->LineProperty();
        if (!pLineProp)
        {
            return false;
        }

        pDrawing->AddChgCnt();

        pDrawing->Hide();
        int iWidth = pData->anyData.GetInt();
        pLineProp->iWidth = iWidth;
        pDrawing->Redraw();
    }
    catch (...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

bool LINE_PROPERTY::PropType(CStdPropertyItem* pData, void* pObj)
{
    CDrawingText* pDrawing = reinterpret_cast<CDrawingText*>(pObj);
    try
    {
        CExtText* pTxt = pDrawing->GetTextInstance();
        if (!pTxt)
        {
            return false;
        }

        TEXT_FRAME* pFrame = pTxt->Frame();
        if (!pFrame)
        {
            return false;
        }

        LINE_PROPERTY* pLineProp = pFrame->LineProperty();
        if (!pLineProp)
        {
            return false;
        }

        pDrawing->AddChgCnt();

        pDrawing->Hide();
        int iLineType = pData->anyData.GetInt();
        pLineProp->iLineType = iLineType;
        pDrawing->Redraw();
    }
    catch (...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

template<class Archive>
void LINE_PROPERTY::load(Archive& ar, const unsigned int version)
{
    SERIALIZATION_INIT;
    SERIALIZATION_LOAD("Color", cr);
    SERIALIZATION_LOAD("Width", iWidth);
    SERIALIZATION_LOAD("Type", iLineType);
    MOCK_EXCEPTION_FILE(e_file_read);
}


template<class Archive>
void LINE_PROPERTY::save(Archive& ar, const unsigned int version) const
{
    SERIALIZATION_INIT;
    SERIALIZATION_SAVE("Color", cr);
    SERIALIZATION_SAVE("Width", iWidth);
    SERIALIZATION_SAVE("Type", iLineType);
    MOCK_EXCEPTION_FILE(e_file_write);
}

//!< load save のインスタンスが生成用ダミー
void LINE_PROPERTY::Dummy()
{
    unsigned int iVersion = 0;

    StdStreamOut outFs(_T(""));
    StdStreamIn  inFs(_T(""));

    boost::archive::text_woarchive txtOut(outFs);
    boost::archive::text_wiarchive txtIn(inFs);

    save<boost::archive::text_woarchive>( txtOut, iVersion);
    load<boost::archive::text_wiarchive>( txtIn,  iVersion);

    StdXmlArchiveOut outXml(outFs);
    StdXmlArchiveIn inXml(inFs);

    save<StdXmlArchiveOut>(outXml, iVersion);
    load<StdXmlArchiveIn>(inXml, iVersion);


    boost::filesystem::ofstream outFsStd(_T(""));
    boost::filesystem::ifstream inFsStd(_T(""));

    boost::archive::binary_oarchive outBin(outFsStd);
    boost::archive::binary_iarchive inBin(inFsStd);

    save<boost::archive::binary_oarchive>(outBin, iVersion);
    load<boost::archive::binary_iarchive>(inBin, iVersion);

}
