/**
* @brief        LINE_PROPERTYヘッダーファイル
* @file	        LINE_PROPERTY.h
* @author           Yasuhiro Sato
* @date	        07-2-2009 20:03:09
* 
* @note	
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
* 
* @remarks	        
*			
*
* $
* $
* 
*/
#ifndef __LINE_PROPERTY_H_
#define __LINE_PROPERTY_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "System/CSystem.h"
#include "Utility/CStdPropertyTree.h"   

class LINE_PROPERTY
{
public:
    LINE_PROPERTY();

    LINE_PROPERTY(const LINE_PROPERTY& m)
    {
        *this = m;
    }

    virtual ~LINE_PROPERTY(){;}

    LINE_PROPERTY& operator =(const LINE_PROPERTY& m)
    {
        cr = m.cr;
        iWidth = m.iWidth;
        iLineType = m.iLineType;
        return *this;
    }

    bool operator ==(const LINE_PROPERTY& m)
    {
        if(cr != m.cr){return false;}
        if(iWidth != m.iWidth){return false;}
        if(iLineType != m.iLineType){return false;}
        return true;
    }

    bool operator !=(const LINE_PROPERTY& m)
    {
        return !(*this == m);
    }

    //-----------------
    TREE_GRID_ITEM*  CreateStdPropertyTree( CStdPropertyTree* pProperty,
        NAME_TREE<CStdPropertyItem>* pTree,
        NAME_TREE_ITEM<CStdPropertyItem>* pNextItem);

    static bool PropColor(CStdPropertyItem* pData, void* pObj);
    static bool PropWidth(CStdPropertyItem* pData, void* pObj);
    static bool PropType(CStdPropertyItem* pData, void* pObj);



public:
    COLORREF    cr;
    int         iWidth;
    int         iLineType;


private:
    friend class boost::serialization::access;  

    BOOST_SERIALIZATION_SPLIT_MEMBER();
    template<class Archive>
    void load(Archive& ar, const unsigned int version);

    template<class Archive>
    void save(Archive& ar, const unsigned int version) const;

    void Dummy();

};

#endif //__LINE_PROPERTY_H_