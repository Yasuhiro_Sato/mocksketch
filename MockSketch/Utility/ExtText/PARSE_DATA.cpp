/**
* @brief			PARSE_DATA実装ファイル
* @file			PARSE_DATA.cpp
* @author			Yasuhiro Sato
* @date			07-2-2009 20:02:47
* 
* @note	
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
* 
* @remarks	        
*			
*
* $
* $
* 
*/
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./PARSE_DATA.h"
#include "./ESC_BLOCK_MODIFER.h"
#include "./TEXT_TYPE.h"
#include "../CUtility.h"


PARSE_DATA::PARSE_DATA():
m_iPos(0),
m_pStr(NULL)
{
}

PARSE_DATA::~PARSE_DATA()
{
}

StdChar PARSE_DATA::_GetCurrent() const
{
    return m_pStr->at(m_iPos);
}


int PARSE_DATA::GetPos() const
{
    return m_iPos;
}


bool PARSE_DATA::_CountUp()
{
    if ((m_iPos+1) >= (int)m_pStr->size())
    {
        return false;
    }
    m_iPos++;
    return true;
}

/*
<sentence> ::=( <command> )*"["
<command>  ::=( <reserved word> [":"(<ColorVal>|<val>|<identifier>)])*
<Hexdigit> ::= ("0"|...|"F")
<ColorVal> ::= ("#" <Hexdigit><Hexdigit><Hexdigit><Hexdigit><Hexdigit><Hexdigit>) 
<digit>    ::= ("0"|...|"9")
<digits>   ::= (<digits>)+
<val>      ::= [("+" | "-")]<digits> 
<alpha>    ::= ("a"|...|"z" or "A"|...|"Z" or "_")
<reserved word> ::("X","Y","SCL","COLOR"......)
<identifier>    ::= (<alpha>( <alpha>| <digit>)*)  

*/
//
bool PARSE_DATA::Parse( int iPos, const StdString* pStr, ESC_BLOCK_MODIFER* pM) 
{
    m_iPos = iPos;
    m_pStr = pStr; 
    m_lstCommand.clear();

    if(!_ParseSentence())
    {
        return false;
    }

    pM->Clear();
    for(auto cmd: m_lstCommand)
    {
        switch(cmd.cid)
        {
        case CID_X:
            pM->bUse = true;
            pM->dX = cmd.iVal / 100.0;
            break;

        case CID_Y:
            pM->bUse = true;
            pM->dY = cmd.iVal / 100.0;
            break;

        case CID_RUBY:        
            pM->bUse = true;
            pM->dY   = -1.0;
            pM->dScl = 0.5;
            break;

        case CID_COLOR: 
            pM->bUse = true;
            pM->dwSts |= BS_COLOR;
            pM->cr = cmd.cVal;
            break;

        case CID_SUPER:       
            pM->bUse = true;
            pM->dY   = -0.75;
            pM->dScl = 0.5;
            break;

        case CID_SUB:         
            pM->bUse = true;
            pM->dY   =  0.25;
            pM->dScl = 0.5;
            break;

        case CID_FRAME:       
            pM->dwSts |= BS_FRAME;
            break;

        case CID_UNDERLINE:   
            pM->dwSts |= BS_UNDERLINE;
            break;

        case CID_STRIKE:      
            pM->dwSts |= BS_STRIKE;
            break;

        case CID_GRID:  
            pM->dwSts |= BS_GRID;
            break;

        case CID_BLOCK_TOP:   
            pM->dwSts |= BSB_LEFT;
            break;

        case CID_BLOCK_MIDDLE:
            pM->dwSts |= BSB_MID;
            break;

        case CID_BLOCK_TAIL:
            pM->dwSts |= BSB_RIGHT;
            break;

        case CID_BLOCK_OVER_ROOF:
            pM->dwSts |= BSB_OVER_ROOF;
            break;

        case CID_BLOCK_ROOF:
            pM->dwSts |= BSB_ROOF;
            break;

        case CID_BLOCK_V_CENTER:
            pM->dwSts |= BSB_V_CENTER;
            break;

        case CID_BLOCK_GROUND:
            pM->dwSts |= BSB_GROUND;
            break;

        case CID_BLOCK_UNDWR_GROUND:
            pM->dwSts |= BSB_UNDER_GROUND;
            break;

        case CID_SCL: 
            pM->bUse = true;
            pM->dScl = cmd.iVal / 100.0;
            break;

        case CID_BOLD:        
            pM->dwSts |= BS_BOLD;
            break;

        case CID_ITALIC:      
            pM->dwSts |= BS_ITALIC;
            break;

        case CID_FONTNAME:    
            pM->dwSts |= BS_FONTNAME;
            pM->strFontName = cmd.sVal;
            break;

        case CID_LEFT:        
            pM->dwSts |= BSA_LEFT;
            break;

        case CID_RIGHT:       
            pM->dwSts |= BSA_RIGHT;
            break;

        case CID_MIDDLE:      
            pM->dwSts |= BSA_MID;
            break;

        default:
            break;
        }
    }
    return true;
}


//---------------------------------------
//   区切り文字
//---------------------------------------
bool PARSE_DATA::_IsDemeliter(StdChar c) const
{
    if ((c == _T(' ')) ||
        (c == _T('\t'))||
        (c == _T('\r'))||        
        (c == _T('\n')))
    {
        return true;
    }
    return false;
}


//---------------------------------------------------------
// <英字>            ::= "a"|"b"|"c"|"d"|"e"|"f"|"g"|"h"|"i"|"j"|"k"|"l"|"m"|
//                       "n"|"o"|"p"|"q"|"r"|"s"|"t"|"u"|"v"|"w"|"x"|"y"|"z"|
//                       "A"|"B"|"C"|"D"|"E"|"F"|"G"|"H"|"I"|"J"|"K"|"L"|"M"|
//                       "N"|"O"|"P"|"Q"|"R"|"S"|"T"|"U"|"V"|"W"|"X"|"Y"|"Z"|"_"
//---------------------------------------------------------
bool PARSE_DATA::_IsAlpha(StdChar c) const
{
    if ((c >= _T('a')) &&
        (c <= _T('z')))
    {
        return true;
    }
    if ((c >= _T('A')) &&
        (c <= _T('Z')))
    {
        return true;
    }
    if (c == _T('_'))
    {
        return true;
    }

    return false;
}


//<Hexdigit> ::= ("0"|...|"F")
bool PARSE_DATA::_IsHexa(StdChar c) const
{
    if(_IsNum(c))
    {
        return true;
    }

    if ((c >= _T('a')) &&
        (c <= _T('f')))
    {
        return true;
    }
    if ((c >= _T('A')) &&
        (c <= _T('F')))
    {
        return true;
    }

    return false;
}

//---------------------------------------------------------
// <数字>            ::= "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
//---------------------------------------------------------
bool PARSE_DATA::_IsNum(StdChar c) const
{
    if ((c >= _T('0')) &&
        (c <= _T('9')))
    {
        return true;
    }
    return false;
}


//空白文字読みとばし
bool PARSE_DATA::_SkipSpace() 
{
    StdChar c;
    while(1)
    {                     
        c = _GetCurrent();
        if (!_IsDemeliter(c))
        {
            return true;
        }

        if (!_CountUp())
        {
            break;
        }
    }
    return false;
}

//<sentence> ::=( <command> )*"["
bool PARSE_DATA::_ParseSentence() 
{

    StdChar c;
    while(1)
    {                     
        if(!_ParseCommand())
        {
            return false;
        }

        if(!_SkipSpace( ))
        {
            return false;
        }

        c = _GetCurrent();
        if( c == _T('['))
        {
            return true;
        }
    }
}

//<command>  ::= <reserved word> [":"(<ColorVal>|<val>|<identifier>)]
bool PARSE_DATA::_ParseCommand() 
{
    COMMAND_DATA cmd;
    cmd.cid = TP_NONE;
    cmd.cVal = RGB(0,0,0);
    cmd.iVal = 0;

    _SkipSpace();
    if(!_ParsReserved(&cmd))
    {
        return false;
    }

    _SkipSpace();

    StdChar c = _GetCurrent();

    if (c == _T(':'))
    {
        DWORD dValType = (cmd.cid & TP_MASK);
        _CountUp();

        if (dValType == TP_COLOR)
        {
            if(!_ParseColor( &cmd))
            {
                m_strError = _T("Invalid color."); 
                return false;
            }
        }
        else if(dValType == TP_STR)
        {
            if(!_ParseStr( &cmd))
            {
                m_strError = _T("Invalid string."); 
                return false;
            }
        }
        else if(dValType == TP_VAL)
        {
            if(!_ParseInt(  &cmd))
            {
                m_strError = _T("Invalid value."); 
                return false;
            }
        }
        else
        {
            m_strError = _T("A Identifer is not find."); 
            return false;
        }
    }

    m_lstCommand.push_back(cmd);
    return true;
}

//予約後検索
bool PARSE_DATA::_ParsReserved(COMMAND_DATA* pCmd) 
{
    StdString strWork;
    StdChar c;
    while(1)
    {                     
        c = _GetCurrent();
        if (_IsDemeliter(c))
        {
            break;
        }

        if (c == _T(':'))
        {
            break;
        }

        if (c == _T('['))
        {
            break;
        }

        strWork += c;
        _CountUp();
    }

    strWork = CUtil::ToUpper(strWork);

    auto it = mapKye2Id.find(strWork);
    if (it == mapKye2Id.end())
    {
        m_strError = _T("A Identifer is not find."); 
        return false;
    }

    pCmd->cid = it->second;
    return true;
}


bool PARSE_DATA::_ParseStr(COMMAND_DATA* pCmd) 
{
    StdString strWork;
    StdChar  c = _GetCurrent();

    bool bQuate = false;

    for(int iCnt = 0; iCnt <256; iCnt++)
    {
        if (c == _T('"'))
        {
           bQuate = (!bQuate);
        }
        else
        {
            if(!bQuate)
            {
                if(_IsDemeliter(c) || c==_T('['))
                {
                    pCmd->sVal =  strWork;
                    return true;
                }
            }

            strWork += c;
        }
        if (!_CountUp()) {return false;}
        c = _GetCurrent();
    }

    return false;
}

//---------------------------------------------------------
//<整数>             ::=[("+"|"-")] <数字>( <数字>)*
//---------------------------------------------------------
bool PARSE_DATA::_ParseInt(COMMAND_DATA* pCmd) 
{

    StdString strWork;
    StdChar  c = _GetCurrent();

    int iMulti = 1;
    if (c == _T('-'))
    {
        iMulti = -1;
        if (!_CountUp()) {return false;}
        c = _GetCurrent();
    }
    else if (c == _T('+'))
    {
        if (!_CountUp()) {return false;}
        c = _GetCurrent();
    }

    if (!_IsNum(c))
    {
        return false;
    }
    strWork = c;

    while(1)
    {
        if (!_CountUp()) {return false;}
        c = _GetCurrent();
        if (_IsNum(c))
        {
            strWork += c;
        }
        else
        {
            pCmd->iVal =  iMulti * _tstoi(strWork.c_str());
            return true;
        }
    }
    return false;

}

//<ColorVal> ::= ("#" <Hexdigit><Hexdigit><Hexdigit><Hexdigit><Hexdigit><Hexdigit>) 
bool PARSE_DATA::_ParseColor( COMMAND_DATA* pCmd)
{
    StdString strWork;
    StdChar  c = _GetCurrent();
    if (c != _T('#'))
    {
        return false;
    }

    if (!_CountUp()) {return false;}

    StdString str;
    int iVal[3];
    for(int i = 0; i < 3; i++)
    {
        str = _T("");
        for(int j = 0; j < 2; j++)
        {
            c = _GetCurrent();
            if(!_IsHexa(c))
            {
                return false;
            }
            str += c;
            _CountUp();
        }

        iVal[i] = _tcstol(str.c_str(), 0, 16);
    }
    pCmd->cVal = RGB(iVal[0], iVal[1], iVal[2]);
    return true;
}
