/**
* @brief        PARSE_DATAヘッダーファイル
* @file	        PARSE_DATA.h
* @author           Yasuhiro Sato
* @date	        07-2-2009 20:03:09
* 
* @note	
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
* 
* @remarks	        
*			
*
* $
* $
* 
*/
#ifndef __PARSE_DATA_H_
#define __PARSE_DATA_H_
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
struct ESC_BLOCK_MODIFER;


class PARSE_DATA
{
    enum COMMAND_ID
    {
        TP_NONE  = 0x00000000,
        TP_COLOR = 0x00010000,
        TP_STR   = 0x00020000,
        TP_VAL   = 0x00030000,

        TP_MASK  = 0xFFFF0000,
        CID_MASK = 0x0000FFFF,

        CID_X           = 0x0001 | TP_VAL,                      
        CID_Y           = 0x0002 | TP_VAL,     
        CID_RUBY        = 0x0003 | TP_VAL,    //ルビ    
        CID_COLOR       = 0x0004 | TP_COLOR,     
        CID_SUPER       = 0x0005 | TP_NONE,   //上付き  
        CID_SUB         = 0x0006 | TP_NONE,   //下付き  
        CID_FRAME       = 0x0007 | TP_NONE,  
        CID_UNDERLINE   = 0x0008 | TP_NONE,  
        CID_STRIKE      = 0x0009 | TP_NONE,   //打ち消し線
        CID_GRID        = 0x000A | TP_NONE,  
   
        CID_BLOCK_TOP   = 0x0010 | TP_VAL,  
        CID_BLOCK_MIDDLE= 0x0011 | TP_VAL,  
        CID_BLOCK_TAIL  = 0x0012 | TP_VAL,  

        CID_BLOCK_OVER_ROOF      = 0x0013 | TP_VAL,  
        CID_BLOCK_ROOF           = 0x0014 | TP_VAL,  
        CID_BLOCK_V_CENTER       = 0x0015 | TP_VAL,  
        CID_BLOCK_GROUND         = 0x0016 | TP_VAL,  
        CID_BLOCK_UNDWR_GROUND   = 0x0017 | TP_VAL,  


        CID_SCL         = 0x0021 | TP_VAL,  
        CID_BOLD        = 0x0022 | TP_NONE,  
        CID_ITALIC      = 0x0023 | TP_NONE,  
        CID_FONTNAME    = 0x0024 | TP_STR,  
        CID_LEFT        = 0x0025 | TP_NONE,  
        CID_RIGHT       = 0x0026 | TP_NONE,  
        CID_MIDDLE      = 0x0027 | TP_NONE,  

    };

    struct COMMAND_DATA
    {
        COMMAND_ID cid;
        StdString  sVal;
        COLORREF   cVal;
        int        iVal;      
    };


    const std::map<StdString, COMMAND_ID> mapKye2Id 
        = { {_T("X")    ,CID_X},
    {_T("Y")    ,CID_Y},
    {_T("RUBY") ,CID_RUBY },       
    {_T("COLOR"),CID_COLOR},       
    {_T("SUPER"),CID_SUPER},       
    {_T("SUB")  ,CID_SUB},        
    {_T("FRAME"),CID_FRAME},       
    {_T("UNDERLINE"),CID_UNDERLINE},   
    {_T("STRIKE")   ,CID_STRIKE},      
    {_T("GRID")     ,CID_GRID},        
    {_T("BLOCK_TOP"),CID_BLOCK_TOP},   
    {_T("BLOCK_MIDDLE") ,CID_BLOCK_MIDDLE},


    {_T("BLOCK_OVER_ROOF")    ,CID_BLOCK_OVER_ROOF},
    {_T("BLOCK_ROOF")         ,CID_BLOCK_ROOF},
    {_T("BLOCK_V_CENTER")     ,CID_BLOCK_V_CENTER},
    {_T("BLOCK_GROUND")       ,CID_BLOCK_GROUND},
    {_T("BLOCK_UNDER_GROUND") ,CID_BLOCK_UNDWR_GROUND},

    {_T("SCL")      ,CID_SCL},         
    {_T("BOLD")     ,CID_BOLD},        
    {_T("ITALIC")   ,CID_ITALIC},      
    {_T("FONTNAME") ,CID_FONTNAME},    
    {_T("LEFT")     ,CID_LEFT},        
    {_T("RIGHT")    ,CID_RIGHT},       
    {_T("MIDDLE")   ,CID_MIDDLE},      
    };
public:
    PARSE_DATA();
    virtual ~PARSE_DATA();

    bool Parse( int iPos, const StdString* pStr, ESC_BLOCK_MODIFER* pM) ;
    int  GetPos() const;

protected:
    int     m_iPos = 0;
    const   StdString* m_pStr; 
    StdString  m_strError; 
    std::vector<COMMAND_DATA> m_lstCommand;

    StdChar _GetCurrent()const;
    bool _CountUp() ;
    bool _IsDemeliter(StdChar c) const;
    bool _IsAlpha(StdChar c) const;
    bool _IsNum(StdChar c) const;
    bool _IsHexa(StdChar c) const;


    bool _ParseSentence() ;
    bool _ParseCommand() ;
    bool _SkipSpace( ) ;
    bool _ParseInt( COMMAND_DATA* pCmd) ;
    bool _ParseColor( COMMAND_DATA* pCmd) ;
    bool _ParseStr( COMMAND_DATA* pCmd) ;
    bool _ParsReserved(  COMMAND_DATA* pCmd) ;

};
#endif //__PARSE_DATA_H_
