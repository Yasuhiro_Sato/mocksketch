
/**
* @brief			CDrawingNode実装ファイル
* @file			CDrawingNode.cpp
* @author			Yasuhiro Sato
* @date			07-2-2009 20:02:47
* 
* @note	
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
* 
* @remarks	        
*			
*
* $
* $
* 
*/
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./TEXT_FRAME.h"
#include "./LINE_PROPERTY.h"
#include "./TEXT_MARGIN.h"
#include "System/CSystem.h"
#include "DrawingObject/CDrawingText.h"



bool TEXT_FRAME::operator ==(const TEXT_FRAME& m)
{
    if(fabs(dWidth     - m.dWidth)    > NEAR_ZERO) {return false;}
    if(fabs(dHeight    - m.dHeight)    > NEAR_ZERO) {return false;}
    if(dHeight != m.dHeight){return false;}
    if(bAutoFit != m.bAutoFit){return false;}
    if(bTextWrap != m.bTextWrap){return false;}
    if(bDrawLine != m.bDrawLine){return false;}
    if(*pPropLine != *(m.pPropLine)){return false;}
    if(bMargin != m.bMargin){return false;}
    if(*pMargin != *(m.pMargin)){return false;}
    if(bUseColor != m.bUseColor){return false;}
    if(bUseColor)
    {
        if(cr != m.cr){return false;}
    }
    if(fabs(dCorner    - m.dCorner)    > NEAR_ZERO) {return false;}
    if(fabs(dSkew - m.dSkew)    > NEAR_ZERO) {return false;}
    if(ptOffset != m.ptOffset){return false;}

    return true;
}


void   TEXT_FRAME::SetWidth(double dW)
{
    dWidth = dW;
}

void   TEXT_FRAME::SetHeight(double dH)
{
    dHeight = dH;
}

void   TEXT_FRAME::SetSkew(double dS)
{
    dSkew = dS;
}

void   TEXT_FRAME::SetOffset(POINT2D pt)
{
    ptOffset = pt;
}

double TEXT_FRAME::Matrix(double* pSx, double* pSy, const MAT2D& mat2D, double dAngle)
{

    double dSx;
    double dSy;

    double dNewAngle;
    double dNewSkew;

    mat2D.GetAxixParam( &dSx, &dSy,  &dNewAngle,  &dNewSkew, 
        dAngle, dSkew);
#if 0  
    DB_PRINT(_T("TEXT_FRAME::Matrix  dSx:%f dSy:%f  dNewAngle:%f  dAngle:%f\n"),
        dSx, dSy, dNewAngle, dNewSkew, dAngle);
#endif

    if (!IsAutoFit())
    {
        SetWidth(fabs(GetWidth() * dSx));
        SetHeight(fabs(GetHeight() * dSy));
    }

    if(pSx)
    {
        *pSx = dSx;
    }

    if(pSy)
    {
        *pSy = dSy;
    }


    return dNewAngle;

}

TREE_GRID_ITEM* TEXT_FRAME::CreateStdPropertyTree(CStdPropertyTree* pProperty,
    NAME_TREE<CStdPropertyItem>* pTree,
    NAME_TREE_ITEM<CStdPropertyItem>* pNextItem)
{
    CStdPropertyItem* pItem;

    NAME_TREE_ITEM<CStdPropertyItem>* pFrame;
    NAME_TREE_ITEM<CStdPropertyItem>* pParent;

    pParent = pNextItem->pParent;
    STD_ASSERT(pParent != NULL);
    if (!pParent)
    {
        return pNextItem;
    }

    pFrame = pTree->AddNext(pParent, NULL, GET_STR(STR_PRO_FRAME), _T("Frame"));


    //------------
    // 自動幅調整
    //------------
    CStdPropertyItemDef DefAutoFit(PROP_BOOL,            //表示タイプ
        GET_STR(STR_PRO_AUTO_FIT),                   //表示名
        _T("AutoFit"),                                  //変数名
        GET_STR(STR_PRO_INFO_AUTO_FIT),                 //表示説明
        true,                                           //編集可不可
        DISP_NONE,                                      //表示単位
        bAutoFit                                          //初期値
    );

    pItem = new CStdPropertyItem(
        DefAutoFit,
        PropAutoFit,
        pProperty,
        NULL,
        (void*)&bAutoFit);

    pNextItem = pTree->AddChild(pFrame,
        pItem,
        DefAutoFit.strDspName,
        pItem->GetName());

    if (!bAutoFit)
    {
        //------------
        // 幅
        //------------
        CStdPropertyItemDef DefWidth(PROP_DOUBLE,            //表示タイプ
            GET_STR(STR_PRO_FRAME_WIDTH),                   //表示名
            _T("FrameWidth"),                               //変数名
            GET_STR(STR_PRO_INFO_FRAME_WIDTH),                 //表示説明
            true,                                           //編集可不可
            DISP_UNIT,                                      //表示単位
            dWidth                                          //初期値
        );

        pItem = new CStdPropertyItem(
            DefWidth,
            PropWidth,
            pProperty,
            NULL,
            (void*)&dWidth);

        pNextItem = pTree->AddNext(pNextItem,
            pItem,
            DefWidth.strDspName,
            pItem->GetName());

        //------------
        // 高さ
        //------------
        CStdPropertyItemDef DefHeight(PROP_DOUBLE,            //表示タイプ
            GET_STR(STR_PRO_FRAME_HEIGHT),                   //表示名
            _T("FrameHeight"),                               //変数名
            GET_STR(STR_PRO_INFO_FRAME_HEIGHT),                 //表示説明
            true,                                           //編集可不可
            DISP_UNIT,                                      //表示単位
            dHeight                                          //初期値
        );

        pItem = new CStdPropertyItem(
            DefHeight,
            PropHeight,
            pProperty,
            NULL,
            (void*)&dHeight);

        pNextItem = pTree->AddNext(pNextItem,
            pItem,
            DefHeight.strDspName,
            pItem->GetName());
    }

    //------------
    // 折り返し
    //------------
    CStdPropertyItemDef DefTextWrap(PROP_BOOL,            //表示タイプ
        GET_STR(STR_PRO_WORD_WRAP),                   //表示名
        _T("TextWrap"),                               //変数名
        GET_STR(STR_PRO_INFO_WORD_WRAP),                 //表示説明
        true,                                           //編集可不可
        DISP_NONE,                                      //表示単位
        bTextWrap                                          //初期値
    );

    pItem = new CStdPropertyItem(
        DefTextWrap,
        PropTextWrap,
        pProperty,
        NULL,
        (void*)&bTextWrap);

    pNextItem = pTree->AddNext(pNextItem,
        pItem,
        DefTextWrap.strDspName,
        pItem->GetName());

    //----------------
    // 枠線描画の有無
    //----------------
    CStdPropertyItemDef DefDrawLine(PROP_BOOL,          //表示タイプ
        GET_STR(STR_PRO_DRAW_LINE),                   //表示名
        _T("DrawLine"),                                 //変数名
        GET_STR(STR_PRO_INFO_DRAW_LINE),                 //表示説明
        true,                                           //編集可不可
        DISP_NONE,                                      //表示単位
        bDrawLine                                         //初期値
    );

    pItem = new CStdPropertyItem(
        DefDrawLine,
        PropDrawLine,
        pProperty,
        NULL,
        (void*)&bDrawLine);

    pNextItem = pTree->AddNext(pNextItem,
        pItem,
        DefDrawLine.strDspName,
        pItem->GetName());

    if (bDrawLine)
    {
        pNextItem = pPropLine->CreateStdPropertyTree(pProperty, pTree, pNextItem);
    }

    //------------
    // 角丸
    //------------
    CStdPropertyItemDef DefCorner(PROP_DOUBLE,          //表示タイプ
        GET_STR(STR_PRO_CORNER_RADIUS),              //表示名
        _T("Corner"),                                   //変数名
        GET_STR(STR_PRO_INFO_CORNER_RADIUS),            //表示説明
        true,                                           //編集可不可
        DISP_UNIT,                                      //表示単位
        dCorner                                         //初期値
    );

    pItem = new CStdPropertyItem(
        DefCorner,
        PropCorner,
        pProperty,
        NULL,
        (void*)&dCorner);

    pNextItem = pTree->AddNext(pNextItem,
        pItem,
        DefCorner.strDspName,
        pItem->GetName());

    //------------
    // マージン
    //------------
    CStdPropertyItemDef DefUseMargin(PROP_BOOL,            //表示タイプ
        GET_STR(STR_PRO_MARGIN_USE),                   //表示名
        _T("UseMargin"),                               //変数名
        GET_STR(STR_PRO_INFO_MARGIN_USE),                 //表示説明
        true,                                           //編集可不可
        DISP_NONE,                                      //表示単位
        bMargin                                          //初期値
    );

    pItem = new CStdPropertyItem(
        DefUseMargin,
        PropUseMargin,
        pProperty,
        NULL,
        (void*)&bMargin);

    pNextItem = pTree->AddNext(pNextItem,
        pItem,
        DefUseMargin.strDspName,
        pItem->GetName());



    if (bMargin)
    {
        pNextItem = pMargin->CreateStdPropertyTree(pProperty, pTree, pNextItem);
    }

    //------------
    // 背景色の有無
    //------------
    CStdPropertyItemDef DefUseColor(PROP_BOOL,            //表示タイプ
        GET_STR(STR_PRO_USE_BACK_COLOR),                     //表示名
        _T("UseColor"),                                   //変数名
        GET_STR(STR_PRO_INFO_USE_BACK_COLOR),                   //表示説明
        true,                                             //編集可不可
        DISP_NONE,                                        //表示単位
        bUseColor                                         //初期値
    );

    pItem = new CStdPropertyItem(
        DefUseColor,
        PropUseColor,
        pProperty,
        NULL,
        (void*)&bUseColor);

    pNextItem = pTree->AddNext(pNextItem,
        pItem,
        DefUseColor.strDspName,
        pItem->GetName());

    if (bUseColor)
    {
        //------------
        // 背景色
        //------------
        CStdPropertyItemDef DefFrameBackColor(PROP_COLOR,           //表示タイプ
            GET_STR(STR_PRO_BACK_COLOR),                     //表示名
            _T("FrameBackColor"),                             //変数名
            GET_STR(STR_PRO_INFO_BACK_COLOR),                   //表示説明
            true,                                             //編集可不可
            DISP_NONE,                                        //表示単位
            cr                                         //初期値
        );

        pItem = new CStdPropertyItem(
            DefFrameBackColor,
            PropColor,
            pProperty,
            NULL,
            (void*)&cr);

        pNextItem = pTree->AddNext(pNextItem,
            pItem,
            DefFrameBackColor.strDspName,
            pItem->GetName());
    }

    return pFrame;
}


/**
*  @brief   プロパティ変更(フレーム幅)
*  @param   [in] pData  変更アイテム
*  @param   [in] pObj   このオブジェクト
*  @retval     なし
*  @note
*/
bool TEXT_FRAME::PropWidth(CStdPropertyItem* pData, void* pObj)
{
    CDrawingText* pDrawing = reinterpret_cast<CDrawingText*>(pObj);
    try
    {
        CExtText* pTxt = pDrawing->GetTextInstance();
        if (!pTxt)
        {
            return false;
        }

        TEXT_FRAME* pFrame = pTxt->Frame();
        if (!pFrame)
        {
            return false;
        }

        pDrawing->AddChgCnt();

        pDrawing->Hide();
        double dWidth = pData->anyData.GetDouble();
        pFrame->SetWidth(dWidth);
        pDrawing->Redraw();

    }
    catch (...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

bool TEXT_FRAME::PropHeight(CStdPropertyItem* pData, void* pObj)
{
    CDrawingText* pDrawing = reinterpret_cast<CDrawingText*>(pObj);
    try
    {
        CExtText* pTxt = pDrawing->GetTextInstance();
        if (!pTxt)
        {
            return false;
        }

        TEXT_FRAME* pFrame = pTxt->Frame();
        if (!pFrame)
        {
            return false;
        }

        pDrawing->AddChgCnt();

        pDrawing->Hide();
        double dHeight = pData->anyData.GetDouble();
        pFrame->SetHeight(dHeight);
        pDrawing->Redraw();

    }
    catch (...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

bool TEXT_FRAME::PropCorner(CStdPropertyItem* pData, void* pObj)
{
    CDrawingText* pDrawing = reinterpret_cast<CDrawingText*>(pObj);
    try
    {
        CExtText* pTxt = pDrawing->GetTextInstance();
        if (!pTxt)
        {
            return false;
        }

        TEXT_FRAME* pFrame = pTxt->Frame();
        if (!pFrame)
        {
            return false;
        }

        pDrawing->AddChgCnt();

        pDrawing->Hide();
        double dOldConner = pFrame->GetCorner() / 2.0;

        double dCorner = pData->anyData.GetDouble();
        pFrame->SetCorner(dCorner);

        bool bSetMargin = false;
        if (!pFrame->IsUseMargin())
        {
            pFrame->SetUseMargin(true);
            bSetMargin = true;
        }
        else
        {
            bSetMargin = true;
            if (fabs(pFrame->Margin()->dBottom - dOldConner) > NEAR_ZERO) { bSetMargin = false; }
            if (fabs(pFrame->Margin()->dTop - dOldConner) > NEAR_ZERO) { bSetMargin = false; }
            if (fabs(pFrame->Margin()->dLeft - dOldConner) > NEAR_ZERO) { bSetMargin = false; }
            if (fabs(pFrame->Margin()->dRight - dOldConner) > NEAR_ZERO) { bSetMargin = false; }
        }


        if (bSetMargin)
        {
            pFrame->Margin()->dBottom = dCorner / 2.0;
            pFrame->Margin()->dTop = dCorner / 2.0;
            pFrame->Margin()->dLeft = dCorner / 2.0;
            pFrame->Margin()->dRight = dCorner / 2.0;
        }

        pDrawing->Redraw();

    }
    catch (...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

bool TEXT_FRAME::PropAutoFit(CStdPropertyItem* pData, void* pObj)
{
    CDrawingText* pDrawing = reinterpret_cast<CDrawingText*>(pObj);
    try
    {
        CExtText* pTxt = pDrawing->GetTextInstance();
        if (!pTxt)
        {
            return false;
        }

        TEXT_FRAME* pFrame = pTxt->Frame();
        if (!pFrame)
        {
            return false;
        }

        bool bOldFit = pFrame->IsAutoFit();

        pDrawing->AddChgCnt();

        pDrawing->Hide();
        bool bAuto = pData->anyData.GetBool();

        pFrame->SetAutoFit(bAuto);
        pDrawing->Redraw();


        //リスト再描画
        pDrawing->ResetProperty();
        ::PostMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_UPDATE_PROPERTY,
            (WPARAM)0, (LPARAM)0);
    }
    catch (...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

bool TEXT_FRAME::PropTextWrap(CStdPropertyItem* pData, void* pObj)
{
    CDrawingText* pDrawing = reinterpret_cast<CDrawingText*>(pObj);
    try
    {
        CExtText* pTxt = pDrawing->GetTextInstance();
        if (!pTxt)
        {
            return false;
        }

        TEXT_FRAME* pFrame = pTxt->Frame();
        if (!pFrame)
        {
            return false;
        }

        pDrawing->AddChgCnt();

        pDrawing->Hide();
        bool bWrap = pData->anyData.GetBool();
        pFrame->SetTextWrap(bWrap);
        pDrawing->Redraw();
    }
    catch (...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

bool TEXT_FRAME::PropDrawLine(CStdPropertyItem* pData, void* pObj)
{
    CDrawingText* pDrawing = reinterpret_cast<CDrawingText*>(pObj);
    try
    {
        CExtText* pTxt = pDrawing->GetTextInstance();
        if (!pTxt)
        {
            return false;
        }

        TEXT_FRAME* pFrame = pTxt->Frame();
        if (!pFrame)
        {
            return false;
        }

        pDrawing->AddChgCnt();

        pDrawing->Hide();
        bool bDraw = pData->anyData.GetBool();
        pFrame->EnableLine(bDraw);
        pDrawing->Redraw();

        //リスト再描画
        pDrawing->ResetProperty();
        ::PostMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_UPDATE_PROPERTY,
            (WPARAM)0, (LPARAM)0);
    }
    catch (...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

bool TEXT_FRAME::PropUseMargin(CStdPropertyItem* pData, void* pObj)
{
    CDrawingText* pDrawing = reinterpret_cast<CDrawingText*>(pObj);
    try
    {
        CExtText* pTxt = pDrawing->GetTextInstance();
        if (!pTxt)
        {
            return false;
        }

        TEXT_FRAME* pFrame = pTxt->Frame();
        if (!pFrame)
        {
            return false;
        }

        pDrawing->AddChgCnt();

        pDrawing->Hide();
        bool bMargin = pData->anyData.GetBool();
        pFrame->SetUseMargin(bMargin);
        pDrawing->Redraw();

        //リスト再描画
        pDrawing->ResetProperty();
        ::PostMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_UPDATE_PROPERTY,
            (WPARAM)0, (LPARAM)0);
    }
    catch (...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

bool TEXT_FRAME::PropUseColor(CStdPropertyItem* pData, void* pObj)
{
    CDrawingText* pDrawing = reinterpret_cast<CDrawingText*>(pObj);
    try
    {
        CExtText* pTxt = pDrawing->GetTextInstance();
        if (!pTxt)
        {
            return false;
        }

        TEXT_FRAME* pFrame = pTxt->Frame();
        if (!pFrame)
        {
            return false;
        }

        pDrawing->AddChgCnt();

        pDrawing->Hide();
        bool bColor = pData->anyData.GetBool();
        pFrame->SetUseColor(bColor);
        pDrawing->Redraw();

        //リスト再描画
        pDrawing->ResetProperty();
        ::PostMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_UPDATE_PROPERTY,
            (WPARAM)0, (LPARAM)0);
    }
    catch (...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

bool TEXT_FRAME::PropColor(CStdPropertyItem* pData, void* pObj)
{
    CDrawingText* pDrawing = reinterpret_cast<CDrawingText*>(pObj);
    try
    {
        CExtText* pTxt = pDrawing->GetTextInstance();
        if (!pTxt)
        {
            return false;
        }

        TEXT_FRAME* pFrame = pTxt->Frame();
        if (!pFrame)
        {
            return false;
        }

        pDrawing->AddChgCnt();

        pDrawing->Hide();
        COLORREF crColor = pData->anyData.GetColor();
        pFrame->SetColor(crColor);
        pDrawing->Redraw();

        //リスト再描画
        pDrawing->ResetProperty();
        ::PostMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_UPDATE_PROPERTY,
            (WPARAM)0, (LPARAM)0);
    }
    catch (...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

bool TEXT_FRAME::PropOffset(CStdPropertyItem* pData, void* pObj)
{
    CDrawingText* pDrawing = reinterpret_cast<CDrawingText*>(pObj);
    try
    {
        CExtText* pTxt = pDrawing->GetTextInstance();
        if (!pTxt)
        {
            return false;
        }

        TEXT_FRAME* pFrame = pTxt->Frame();
        if (!pFrame)
        {
            return false;
        }

        pDrawing->AddChgCnt();

        pDrawing->Hide();
        POINT2D pt = pData->anyData.GetPoint();
        pFrame->SetOffset(pt);
        pDrawing->Redraw();

    }
    catch (...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

template<class Archive>
void TEXT_FRAME::load(Archive& ar, const unsigned int version)
{
    SERIALIZATION_INIT;
    SERIALIZATION_LOAD("Width", dWidth);
    SERIALIZATION_LOAD("Height", dHeight);
    SERIALIZATION_LOAD("Corner", dCorner);
    SERIALIZATION_LOAD("AutoFit", bAutoFit);

    SERIALIZATION_LOAD("UseLine", bDrawLine);
    if (bDrawLine)
    {
        LINE_PROPERTY line;
        SERIALIZATION_LOAD("Line", line);
        pPropLine = std::make_unique<LINE_PROPERTY>(line);
    }

    SERIALIZATION_LOAD("UseMargin", bMargin);
    if (bMargin)
    {
        TEXT_MARGIN margin;
        SERIALIZATION_LOAD("Margin", margin);
        pMargin = std::make_unique<TEXT_MARGIN>(margin);
    }

    SERIALIZATION_LOAD("UseColor", bUseColor);
    SERIALIZATION_LOAD("Color", cr);
    SERIALIZATION_LOAD("Skew", dSkew);
    MOCK_EXCEPTION_FILE(e_file_read);
}

template<class Archive>
void TEXT_FRAME::save(Archive& ar, const unsigned int version) const
{
    SERIALIZATION_INIT;
    SERIALIZATION_SAVE("Width", dWidth);
    SERIALIZATION_SAVE("Height", dHeight);
    SERIALIZATION_SAVE("Corner", dCorner);
    SERIALIZATION_SAVE("AutoFit", bAutoFit);
    SERIALIZATION_SAVE("UseLine", bDrawLine);

    if (bDrawLine)
    {
        SERIALIZATION_SAVE("Line", *pPropLine);
    }

    SERIALIZATION_SAVE("UseMargin", bMargin);
    if (bMargin)
    {
        SERIALIZATION_SAVE("Margin", *pMargin);
    }

    SERIALIZATION_SAVE("UseColor", bUseColor);
    SERIALIZATION_SAVE("Color", cr);
    SERIALIZATION_SAVE("Skew", dSkew);

    MOCK_EXCEPTION_FILE(e_file_write);
}


//load save のインスタンスが生成されないため
//ダミーとして実装
void TEXT_FRAME::_Dummy()
{
    unsigned int iVersion = 0;

    StdStreamOut outFs(_T(""));
    StdStreamIn  inFs(_T(""));

    boost::archive::text_woarchive txtOut(outFs);
    boost::archive::text_wiarchive txtIn(inFs);

    save<boost::archive::text_woarchive>(txtOut, iVersion);
    load<boost::archive::text_wiarchive>(txtIn, iVersion);

    StdXmlArchiveOut outXml(outFs);
    StdXmlArchiveIn inXml(inFs);

    save<StdXmlArchiveOut>(outXml, iVersion);
    load<StdXmlArchiveIn>(inXml, iVersion);

    boost::filesystem::ofstream outFsStd(_T(""));
    boost::filesystem::ifstream inFsStd(_T(""));

    boost::archive::binary_oarchive outBin(outFsStd);
    boost::archive::binary_iarchive inBin(inFsStd);

    save<boost::archive::binary_oarchive>(outBin, iVersion);
    load<boost::archive::binary_iarchive>(inBin, iVersion);

}