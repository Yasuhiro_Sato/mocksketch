/**
* @brief        TEXT_FRAMEヘッダーファイル
* @file	        TEXT_FRAME.h
* @author           Yasuhiro Sato
* @date	        07-2-2009 20:03:09
* 
* @note	
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
* 
* @remarks	        
*			
*
* $
* $
* 
*/
#ifndef __TEXT_FRAME_H_
#define __TEXT_FRAME_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "System/CSystem.h"
#include "Utility/CStdPropertyTree.h"   

class LINE_PROPERTY;
class TEXT_MARGIN;

class TEXT_FRAME
{

public:
    TEXT_FRAME():
        dWidth(0),
        dHeight(0),
        bTextWrap(true),
        bAutoFit(true),
        bDrawLine(true),
        bMargin(true),
        bUseColor(false),
        dCorner(0.0),
        cr(RGB(255,255,255)),
        dSkew(0.0)
    {
        pPropLine = std::make_unique<LINE_PROPERTY>();
        cr = DRAW_CONFIG->crBack;

        pMargin = std::make_unique<TEXT_MARGIN>();
    }

    virtual ~TEXT_FRAME(){;}

    TEXT_FRAME(const TEXT_FRAME& m)
    {
        *this = m;
    }

    TEXT_FRAME& operator =(const TEXT_FRAME& m)
    {
        dWidth = m.dWidth;
        dHeight = m.dHeight;
        bTextWrap = m.bTextWrap;
        bAutoFit = m.bAutoFit;
        bDrawLine = m.bDrawLine;
        bMargin = m.bMargin;
        dCorner = m.dCorner;

        pPropLine.reset();
        if (m.pPropLine)
        {
            pPropLine = std::make_unique<LINE_PROPERTY>(*(m.pPropLine));
        }

        pMargin.reset();
        if (m.pMargin)
        {
            pMargin = std::make_unique<TEXT_MARGIN>(*(m.pMargin));
        }

        bUseColor = m.bUseColor;
        cr = m.cr;

        lstDispFrame = m.lstDispFrame;
        lstFrame = m.lstFrame;
        dSkew = m.dSkew;
        ptOffset = m.ptOffset;
        return *this;
    }

    bool operator ==(const TEXT_FRAME& m);

    bool operator !=(const TEXT_FRAME& m)
    {
        return !(*this == m);
    }

    //幅(表示単位系)
    double GetWidth(){return dWidth;}
    void   SetWidth(double dW);

    double GetHeight(){return dHeight;}
    void   SetHeight(double dH);

    double GetSkew(){return dSkew;}
    void   SetSkew(double dS);

    POINT2D GetOffset(){return ptOffset;}
    void   SetOffset(POINT2D pt);

    bool   IsAutoFit(){return bAutoFit;}
    void   SetAutoFit(bool bUse){bAutoFit = bUse;}

    bool   IsTextWrap(){return bTextWrap;}
    void   SetTextWrap(bool bUse){bTextWrap = bUse;}

    bool   IsUseColor(){return bUseColor;}
    void   SetUseColor(bool bUse){bUseColor = bUse;}

    COLORREF   GetColor(){return cr;}
    void   SetColor(COLORREF c){cr = c;}

    bool   IsUseMargin(){return bMargin;}
    void   SetUseMargin(bool bUse)
    {
        if (bUse)
        {
            if(!pMargin)
            {
                pMargin = std::make_unique<TEXT_MARGIN>();
            }
        }
        else
        {
            pMargin.reset();

        }
        bMargin = bUse;
    }

    double Matrix(double* pSx, double* pSy, const MAT2D& mat2D, double dAngle);


    bool   IsEnableLine(){return bDrawLine;}
    void   EnableLine(bool bUse){bDrawLine = bUse;}

    double GetCorner(){return dCorner;}
    void   SetCorner(double dTmpCorner){dCorner = dTmpCorner;}

    LINE_PROPERTY*   LineProperty(){return (pPropLine?pPropLine.get():NULL);}
    TEXT_MARGIN*     Margin(){return (pMargin?pMargin.get():NULL);}

    //-----------------
    TREE_GRID_ITEM*  CreateStdPropertyTree( CStdPropertyTree* pProperty,
        NAME_TREE<CStdPropertyItem>* pTree,
        NAME_TREE_ITEM<CStdPropertyItem>* pNextItem);

    static bool PropWidth(CStdPropertyItem* pData, void* pObj);
    static bool PropHeight(CStdPropertyItem* pData, void* pObj);
    static bool PropCorner(CStdPropertyItem* pData, void* pObj);
    static bool PropAutoFit(CStdPropertyItem* pData, void* pObj);
    static bool PropTextWrap(CStdPropertyItem* pData, void* pObj);
    static bool PropDrawLine(CStdPropertyItem* pData, void* pObj);
    static bool PropUseMargin(CStdPropertyItem* pData, void* pObj);
    static bool PropUseColor(CStdPropertyItem* pData, void* pObj);
    static bool PropColor(CStdPropertyItem* pData, void* pObj);
    static bool PropSkew(CStdPropertyItem* pData, void* pObj);
    static bool PropOffset(CStdPropertyItem* pData, void* pObj);


public:
    //一時使用
    //unique_ptrを使用しないのはバッファーごとの計算を省略するため
    //枠線座標（画面座標)
    std::vector<POINT>   lstDispFrame;
    std::vector<POINT2D> lstFrame;
    std::vector<POINT>   lstTextArea;

private:
    double dWidth;
    double dHeight;
    double dCorner;

    bool   bAutoFit;    //自動領域調整
    bool   bTextWrap;   //折り返し

    bool   bDrawLine;   //枠線の使用有無
    std::unique_ptr<LINE_PROPERTY> pPropLine;

    bool   bMargin;    //マージンの使用有無
    std::unique_ptr<TEXT_MARGIN> pMargin;

    bool   bUseColor;   //背景色の有無
    COLORREF cr; 

    double dSkew;      // 歪角

    POINT2D ptOffset;   // オフセット




private:
    friend class boost::serialization::access;  

    BOOST_SERIALIZATION_SPLIT_MEMBER();
    template<class Archive>
    void load(Archive& ar, const unsigned int version);

    template<class Archive>
    void save(Archive& ar, const unsigned int version) const;

    void _Dummy();
};

#endif // __TEXT_FRAME_H_

