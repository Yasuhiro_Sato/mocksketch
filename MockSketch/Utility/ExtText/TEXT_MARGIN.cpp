/**
* @brief			CDrawingNode実装ファイル
* @file			CDrawingNode.cpp
* @author			Yasuhiro Sato
* @date			07-2-2009 20:02:47
* 
* @note	
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
* 
* @remarks	        
*			
*
* $
* $
* 
*/
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./TEXT_MARGIN.h"
#include "./TEXT_FRAME.h"
#include "./LINE_PROPERTY.h"
#include "./CExtText.h"
#include "System/CSystem.h"
#include "DrawingObject/CDrawingText.h"


template<class Archive>
void TEXT_MARGIN::load(Archive& ar, const unsigned int version)
{
    SERIALIZATION_INIT;
    SERIALIZATION_LOAD("Top", dTop);
    SERIALIZATION_LOAD("Bottom", dBottom);
    SERIALIZATION_LOAD("Left", dLeft);
    SERIALIZATION_LOAD("Right", dRight);
    MOCK_EXCEPTION_FILE(e_file_read);
}


template<class Archive>
void TEXT_MARGIN::save(Archive& ar, const unsigned int version) const
{
    SERIALIZATION_INIT;
    SERIALIZATION_SAVE("Top", dTop);
    SERIALIZATION_SAVE("Bottom", dBottom);
    SERIALIZATION_SAVE("Left", dLeft);
    SERIALIZATION_SAVE("Right", dRight);
    MOCK_EXCEPTION_FILE(e_file_write);
}

TREE_GRID_ITEM* TEXT_MARGIN::CreateStdPropertyTree(CStdPropertyTree* pProperty,
    NAME_TREE<CStdPropertyItem>* pTree,
    NAME_TREE_ITEM<CStdPropertyItem>* pNextItem)
{
    CStdPropertyItem* pItem;

    NAME_TREE_ITEM<CStdPropertyItem>* pMargin;
    NAME_TREE_ITEM<CStdPropertyItem>* pMarginNext;


    pMargin = pTree->AddNext(pNextItem, NULL, GET_STR(STR_PRO_FRANE_MARGIN), _T("Margin"));

    //------------
    // 上
    //------------
    CStdPropertyItemDef DefTop(PROP_DOUBLE,            //表示タイプ
        GET_STR(STR_PRO_FRAME_MARGIN_TOP),                   //表示名
        _T("MarginTop"),                               //変数名
        GET_STR(STR_PRO_INFO_FRAME_MARGIN_TOP),                 //表示説明
        true,                                           //編集可不可
        DISP_UNIT,                                      //表示単位
        dTop                                          //初期値
    );

    pItem = new CStdPropertyItem(
        DefTop,
        PropTop,
        pProperty,
        NULL,
        (void*)&dTop);

    pMarginNext = pTree->AddChild(pMargin,
        pItem,
        DefTop.strDspName,
        pItem->GetName());

    //------------
    // 下
    //------------
    CStdPropertyItemDef DefBottom(PROP_DOUBLE,          //表示タイプ
        GET_STR(STR_PRO_FRAME_MARGIN_BOTTOM),                   //表示名
        _T("MarginBottom"),                             //変数名
        GET_STR(STR_PRO_INFO_FRAME_MARGIN_BOTTOM),                 //表示説明
        true,                                           //編集可不可
        DISP_UNIT,                                      //表示単位
        dBottom                                         //初期値
    );

    pItem = new CStdPropertyItem(
        DefBottom,
        PropBottom,
        pProperty,
        NULL,
        (void*)&dBottom);

    pMarginNext = pTree->AddNext(pMarginNext,
        pItem,
        DefBottom.strDspName,
        pItem->GetName());

    //------------
    // 左
    //------------
    CStdPropertyItemDef DefLeft(PROP_DOUBLE,            //表示タイプ
        GET_STR(STR_PRO_FRAME_MARGIN_LEFT),                   //表示名
        _T("MarginLeft"),                               //変数名
        GET_STR(STR_PRO_INFO_FRAME_MARGIN_LEFT),                 //表示説明
        true,                                           //編集可不可
        DISP_UNIT,                                      //表示単位
        dLeft                                           //初期値
    );

    pItem = new CStdPropertyItem(
        DefLeft,
        PropLeft,
        pProperty,
        NULL,
        (void*)&dLeft);

    pMarginNext = pTree->AddNext(pMarginNext,
        pItem,
        DefLeft.strDspName,
        pItem->GetName());

    //------------
    // 右
    //------------
    CStdPropertyItemDef DefRight(PROP_DOUBLE,           //表示タイプ
        GET_STR(STR_PRO_FRAME_MARGIN_RIGHT),                   //表示名
        _T("MarginRight"),                              //変数名
        GET_STR(STR_PRO_INFO_FRAME_MARGIN_RIGHT),                 //表示説明
        true,                                           //編集可不可
        DISP_UNIT,                                      //表示単位
        dRight                                          //初期値
    );

    pItem = new CStdPropertyItem(
        DefRight,
        PropRight,
        pProperty,
        NULL,
        (void*)&dRight);

    pMarginNext = pTree->AddNext(pMarginNext,
        pItem,
        DefRight.strDspName,
        pItem->GetName());

    return pMargin;
}

bool TEXT_MARGIN::PropTop(CStdPropertyItem* pData, void* pObj)
{
    CDrawingText* pDrawing = reinterpret_cast<CDrawingText*>(pObj);
    try
    {
        CExtText* pTxt = pDrawing->GetTextInstance();
        if (!pTxt)
        {
            return false;
        }

        TEXT_FRAME* pFrame = pTxt->Frame();
        if (!pFrame)
        {
            return false;
        }

        TEXT_MARGIN* pMargin = pFrame->Margin();
        if (!pMargin)
        {
            return false;
        }

        pDrawing->AddChgCnt();

        pDrawing->Hide();
        double dTop = pData->anyData.GetDouble();
        pMargin->dTop = dTop;
        pDrawing->Redraw();
    }
    catch (...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

bool TEXT_MARGIN::PropBottom(CStdPropertyItem* pData, void* pObj)
{
    CDrawingText* pDrawing = reinterpret_cast<CDrawingText*>(pObj);
    try
    {
        CExtText* pTxt = pDrawing->GetTextInstance();
        if (!pTxt)
        {
            return false;
        }

        TEXT_FRAME* pFrame = pTxt->Frame();
        if (!pFrame)
        {
            return false;
        }

        TEXT_MARGIN* pMargin = pFrame->Margin();
        if (!pMargin)
        {
            return false;
        }

        pDrawing->AddChgCnt();

        pDrawing->Hide();
        double dBottom = pData->anyData.GetDouble();
        pMargin->dBottom = dBottom;
        pDrawing->Redraw();
    }
    catch (...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

bool TEXT_MARGIN::PropLeft(CStdPropertyItem* pData, void* pObj)
{
    CDrawingText* pDrawing = reinterpret_cast<CDrawingText*>(pObj);
    try
    {
        CExtText* pTxt = pDrawing->GetTextInstance();
        if (!pTxt)
        {
            return false;
        }

        TEXT_FRAME* pFrame = pTxt->Frame();
        if (!pFrame)
        {
            return false;
        }

        TEXT_MARGIN* pMargin = pFrame->Margin();
        if (!pMargin)
        {
            return false;
        }

        pDrawing->AddChgCnt();

        pDrawing->Hide();
        double dLeft = pData->anyData.GetDouble();
        pMargin->dLeft = dLeft;
        pDrawing->Redraw();
    }
    catch (...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

bool TEXT_MARGIN::PropRight(CStdPropertyItem* pData, void* pObj)
{
    CDrawingText* pDrawing = reinterpret_cast<CDrawingText*>(pObj);
    try
    {
        CExtText* pTxt = pDrawing->GetTextInstance();
        if (!pTxt)
        {
            return false;
        }

        TEXT_FRAME* pFrame = pTxt->Frame();
        if (!pFrame)
        {
            return false;
        }

        TEXT_MARGIN* pMargin = pFrame->Margin();
        if (!pMargin)
        {
            return false;
        }

        pDrawing->AddChgCnt();

        pDrawing->Hide();
        double dRight = pData->anyData.GetDouble();
        pMargin->dRight = dRight;
        pDrawing->Redraw();
    }
    catch (...)
    {
        STD_DBG(_T("SetError"));
        return false;
    }
    return true;
}

//!< load save のインスタンスが生成用ダミー
void TEXT_MARGIN::Dummy()
{
    unsigned int iVersion = 0;

    StdStreamOut outFs(_T(""));
    StdStreamIn  inFs(_T(""));

    boost::archive::text_woarchive txtOut(outFs);
    boost::archive::text_wiarchive txtIn(inFs);

    save<boost::archive::text_woarchive>( txtOut, iVersion);
    load<boost::archive::text_wiarchive>( txtIn,  iVersion);

    StdXmlArchiveOut outXml(outFs);
    StdXmlArchiveIn inXml(inFs);

    save<StdXmlArchiveOut>(outXml, iVersion);
    load<StdXmlArchiveIn>(inXml, iVersion);


    boost::filesystem::ofstream outFsStd(_T(""));
    boost::filesystem::ifstream inFsStd(_T(""));

    boost::archive::binary_oarchive outBin(outFsStd);
    boost::archive::binary_iarchive inBin(inFsStd);

    save<boost::archive::binary_oarchive>(outBin, iVersion);
    load<boost::archive::binary_iarchive>(inBin, iVersion);

}
