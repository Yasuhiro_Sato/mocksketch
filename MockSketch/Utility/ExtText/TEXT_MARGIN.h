/**
* @brief        TEXT_MARGINヘッダーファイル
* @file	        TEXT_MARGIN.h
* @author           Yasuhiro Sato
* @date	        07-2-2009 20:03:09
* 
* @note	
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
* 
* @remarks	        
*			
*
* $
* $
* 
*/
#ifndef __TEXT_MARGIN_H_
#define __TEXT_MARGIN_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "Utility/CStdPropertyTree.h"       

/*---------------------------------------------------*/
/*  classes                                          */
/*---------------------------------------------------*/
class TEXT_MARGIN
{
public:
    TEXT_MARGIN():dTop(0.0),
        dBottom(0.0),
        dLeft(0.0),
        dRight(0.0){;}
    TEXT_MARGIN(const TEXT_MARGIN& m)
    {
        *this = m;
    }

    virtual ~TEXT_MARGIN(){;}

    TEXT_MARGIN& operator =(const TEXT_MARGIN& m)
    {
        dTop = m.dTop;
        dBottom = m.dBottom;
        dLeft = m.dLeft;
        dRight = m.dRight;
        return *this;
    }

    bool operator ==(const TEXT_MARGIN& m)
    {
        if(dTop != m.dTop){return false;}
        if(dBottom != m.dBottom){return false;}
        if(dLeft != m.dLeft){return false;}
        if(dRight != m.dRight){return false;}
        return true;
    }

    bool operator !=(const TEXT_MARGIN& m)
    {
        return !(*this == m);
    }


    TREE_GRID_ITEM*  CreateStdPropertyTree( CStdPropertyTree* pProperty,
        NAME_TREE<CStdPropertyItem>* pTree,
        NAME_TREE_ITEM<CStdPropertyItem>* pNextItem);

    static bool PropTop(CStdPropertyItem* pData, void* pObj);
    static bool PropBottom(CStdPropertyItem* pData, void* pObj);
    static bool PropLeft(CStdPropertyItem* pData, void* pObj);
    static bool PropRight(CStdPropertyItem* pData, void* pObj);

public:
    double dTop;
    double dBottom;
    double dLeft;
    double dRight;

private:
    friend class boost::serialization::access;  

    BOOST_SERIALIZATION_SPLIT_MEMBER();
    template<class Archive>
    void load(Archive& ar, const unsigned int version);

    template<class Archive>
    void save(Archive& ar, const unsigned int version) const;

    void Dummy();

};

#endif //TEXT_MARGIN