/**
* @brief			TEXT_TYPEヘッダーファイル
* @file		    TEXT_TYPE.h
* @author			Yasuhiro Sato
* @date			02-2-2009 03:00:24
* 
* @note	
* - Copyright:     Copyright (C) 2010 MockTools Ltd.
* - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
* - Target system:	Windows XP Professional(SP3)
* 
* @remarks	        
*			
*
* $
* $
* 
*/

#if !defined (__TEXT_TYPE_H__)
#define __TEXT_TYPE_H__
enum BASE_TYPE
{
    FIRST_BASE1,   //１行目左下を原点
    LAST_BASE,     //最下行左下を原点
    TEXT_BLOCK,    //テキスト枠
};

enum STYLE
{
    ITALIC  = 0x01,     //イタリック体
    BOLD    = 0x02,     //太字
    UNDER   = 0x04,     //下線
    STRIKE  = 0x08,     //打ち消し線
    UPPER   = 0x10,     //上付
    LOWER   = 0x20,     //下付
};

enum ESC_STS
{
    ESC_START = 0x00,
    ESC_NONE  = 0x01,
    ESC       = 0x02,    //%
    ESC_AFTER = 0x03,    //%直後
    QUOTE     = 0x04,
    DBL_QUOTE = 0x05,
    QUOTE_ESC     = 0x06,
    DBL_QUOTE_ESC = 0x07,

    ESC_RETURN = 0x08,
    ESC_NONE_BLOCK  = 0x09,  //ESC_STARTによるBLOCK開始
    ESC_CONTINUITY  = 0x0A,  //未使用
    ESC_COMMAND     = 0x0B,  //%$ コマンド形式
    ESC_CLOSE       = 0x0C,  //]直後

    //ESC_POS    = 0x07,
    //ESC_ALIGN    = 0x06,

    //ESC_UP     = 0x06,
    //ESC_DOWN   = 0x07,
    //ESC_LARGE  = 0x08,
    //ESC_SMALL  = 0x09,
    //ESC_BACK  = 0x0A,

    //ESC_UPPER  = 0x0B,
    //ESC_LOWER  = 0x0C,
    ESC_BLOCK = 0x40,     //未使用
    ESC_LBLOCK = 0x80,    //[直後
    ESC_ONEBLOCK = 0x100,
    ESC_GRID     = 0x200,
    ESC_PBLOCK    = 0x400,
    ESC_ERROR    = 0x1000,
};


/*
%+[123|456|789]
%+[|123|456|789|]
%+[[123][456][789]]
%+[[123]456[789]]
%+[[123]456|789|]
%+[[123]|456|789]
%+[%>[123]|456|789]

上記のような場合においていずれもブロックと認識させたい
グリッド内にグリッドを作成することは考慮しない
*/
enum GRID_STS
{
    GRID_NONE,
    GRID_INNER_BLOCK,     //[から]までの間もしくは表示文字列
    GRID_PREPARE_BLOCK,   //％から[までの間
    GRID_READY_TO_CREATE, //％以外１文字でもあればブロックを生成する
};


/*
enum ESC_STR
{
ESC_QUATE,
ESC_UPPER,
ESC_LOWER,
ESC_UP,
ESC_DOWN,
ESC_LARGE,
ESC_SMALL,
ESC_BS,
};
*/

enum ALIGNMENT_TYPE
{
    AL_NONE         = 0x00,
    AL_LEFT         = 0x01,
    AL_RIGHT        = 0x02,
    AL_MID          = 0x04,
};

enum BLOCK_STS
{
    BS_NONE            = 0x00000000,
    BS_UNDERLINE       = 0x00000001,   //下線
    BS_GRID            = 0x00000002,   //グリッド線
    BS_FRAME           = 0x00000004,   //枠

    BS_COLOR           = 0x00000008,   //色
    BS_STRIKE          = 0x00000010,   //打消し線
    BS_ITALIC          = 0x00000020,   //イタリック
    BS_FONTNAME        = 0x00000040,   //フォント名
    BS_BOLD            = 0x00000080,   //太字

    BS_NO_GRID_LINE    = 0x00000100,   //グリッドに線を引かない

    BS_IGNORE_BLOCK_POSITON  = 0x00000200, // 自ブロック位置を無視 ：

    BSA_LEFT           = 0x00001000,   // 左寄せ ：自ブロックに対して
    BSA_RIGHT          = 0x00002000,   // 右寄せ ：
    BSA_MID            = 0x00004000,   // 中央   ：

    BSB_LEFT           = 0x00010000,   // 左寄せ ：前のブロックに対して 
    BSB_MID            = 0x00020000,   // 中央 ：
    BSB_RIGHT          = 0x00040000,   // 右寄せ ：
    BSB_X_BLOCK        = 0x000F0000,   // 全ブロックに対しての位置設定

    BSB_OVER_ROOF      = 0x00080000,   // 前ブロックの上端と自ブロックの下端 ： 
    BSB_ROOF           = 0x00100000,   // 前ブロックの上端と自ブロックの上端 ：
    BSB_V_CENTER       = 0x00200000,   // 前ブロックの中央と自ブロックの中央 ：
    BSB_GROUND         = 0x00400000,   // 前ブロックの下端と自ブロックの下端 ： 通常は必要なし 
    BSB_UNDER_GROUND   = 0x00800000,   // 前ブロックの下端と自ブロックの上端
    BSB_Y_BLOCK        = 0x00F00000,   // 全ブロックに対しての位置設定
    BSB_BLOCK          = 0x00FF0000,   // 全ブロックに対しての位置設定

};

enum POS_TYPE
{
    NONE         = 0x00,
    LEFT         = 0x01,
    RIGHT        = 0x02,
    MID          = 0x04,

    TOP          = 0x10,
    BOTTOM       = 0x20,
    CENTER       = 0x40,

    TOP_LEFT     = 0x11,
    TOP_RIGHT    = 0x12,
    TOP_MID      = 0x14,

    BOTTTOM_LEFT  = 0x21,
    BOTTTOM_RIGHT = 0x22,
    BOTTTOM_MID   = 0x24,

    CENTER_LEFT  = 0x41,
    CENTER_RIGHT = 0x42,
    CENTER_MID   = 0x44,
};

enum E_BLOCK_LINE
{
    BL_NONE   = 0x09,
    BL_LEFT   = 0x01,
    BL_RIGHT  = 0x02,
    BL_TOP    = 0x04,
    BL_BOTTOM = 0x08,
    BL_ALL    = BL_LEFT|BL_RIGHT|BL_TOP|BL_BOTTOM,
};

#define DBG_PRINT   0
#define DBG_PRINT_1 0
#define DBG_CONV_SPLINE 0

#endif //__TEXT_TYPE_H__