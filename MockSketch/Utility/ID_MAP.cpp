/**
 * @brief			ID_MAP実装ファイル
 * @file		    ID_MAP.cpp
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./ID_MAP.h"
#include "Utility/CUtility.h"



class CIdMapTest
{
public:

    CIdMapTest():m_iId(0){;}
    CIdMapTest(StdString strNmae):m_iId(0){ m_strName = strNmae;}
    ~CIdMapTest(){;}

    int GetId(){return m_iId;}
    void SetId(int iID){ m_iId = iID;}

    StdString GetName(){return m_strName;}
    void SetName(StdString strName){ m_strName = strName;}

protected:

    int m_iId;
    StdString m_strName;
};


//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void TEST_ID_MAP()
{
    CUtil::DbgOut(_T("%s Start\n"), DB_FUNC_NAME.c_str());


    ID_MAP<CIdMapTest> mapTest;

    {
        //mapTestにコピーを作成する

        CIdMapTest T1(_T("Test1"));
        CIdMapTest T2(_T("Test2"));
        CIdMapTest T3(_T("Test3"));
        CIdMapTest T4(_T("Test4"));

        mapTest.SetData(&T1, true);
        mapTest.SetData(&T2, true);
        mapTest.SetData(&T3, true);
        mapTest.SetData(&T4, true);
    }

    CIdMapTest* p1;
    CIdMapTest* p2;
    CIdMapTest* p3;
    CIdMapTest* p4;
    CIdMapTest* p5;

    p1 = mapTest.GetData(1);
    p2 = mapTest.GetData(2);
    p3 = mapTest.GetData(3);
    p4 = mapTest.GetData(4);
    p5 = mapTest.GetData(5);

    STD_ASSERT(p1 != NULL);
    STD_ASSERT(p2 != NULL);
    STD_ASSERT(p3 != NULL);
    STD_ASSERT(p4 != NULL);
    STD_ASSERT(p5 == NULL);


    //ウイークポインター取得
    std::weak_ptr<CIdMapTest> wp1;
    std::weak_ptr<CIdMapTest> wp2;
    std::weak_ptr<CIdMapTest> wp3;
    std::weak_ptr<CIdMapTest> wp4;
    std::weak_ptr<CIdMapTest> wp5;

    mapTest.GetWeak( &wp1, 1);
    mapTest.GetWeak( &wp2, 2);
    mapTest.GetWeak( &wp3, 3);
    mapTest.GetWeak( &wp4, 4);
    mapTest.GetWeak( &wp5, 5);

    STD_ASSERT(!wp1.expired());
    STD_ASSERT(!wp2.expired());
    STD_ASSERT(!wp3.expired());
    STD_ASSERT(!wp4.expired());
    STD_ASSERT( wp5.expired());

    //データ削除
    STD_ASSERT (mapTest.DeleteData(2) );

    //データ削除確認
    STD_ASSERT(wp2.expired());

    //データ削除確認
    STD_ASSERT(mapTest.GetData(2) == NULL);


    CUtil::DbgOut(_T("%s End\n"), DB_FUNC_NAME.c_str());
    CUtil::DbgOut(_T("\n"));
}
#endif //_DEBUG