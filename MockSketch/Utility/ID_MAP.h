/**
 * @brief			ID_MAPヘッダーファイル
 * @file		    ID_MAP.h
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */


#ifndef __ID_MAP_H__
#define __ID_MAP_H__



/**
 * @class   ID_MAP
 * @brief   IDに関連付けられたデータを保持する
 *          データは,SetId,GetIdメソッドを持っている必要が
 *          ある。
 *
 *
 * 目的： 複数のオブジェクトが保持しているデータを一元管理する
 *        相互関係を永続化させる
 *
 *
 *
 */
template <class D>
class ID_MAP
{
public:

    /**
     *  @brief  コンストラクター
     *  @param  なし
     *  @retval なし
     *  @note   
     */
    ID_MAP():iIdMax(10)
    {
        ;
    }

    /**
     *  @brief  デストラクタ
     *  @param  なし
     *  @retval なし
     *  @note   
     */
    ~ID_MAP()
    {
    }

    /**
     *  @brief  データ登録
     *  @param  [in] pData     設定データ
     *  @param  [in] bCreateId ID生成の有無
     *  @retval true 登録成功
     *  @note   
     */
    bool SetData(D* pData, bool bCreateId)
    {
        int iNewId = 1;
        if (bCreateId)
        {
            if (!m_mapId.empty())
            {
                iNewId = SearchId();
                if (iNewId < 0)
                {
                    return false;
                }
            }
        }
        else
        {
            iNewId = pData->GetId();
        }

        std::shared_ptr<D>* pShared;
        pShared = GetShared(iNewId);

        if (pShared != NULL)
        {
            return false;
        }

        std::shared_ptr<D> pTmp1(new D(*pData));
        pTmp1->SetId(iNewId);

        m_mapId[iNewId] = pTmp1;
        return true;
    }



    /**
     *  @brief  データ取得
     *  @param  [in] iId
     *  @retval データ
     *  @note   データが存在しない場合はNULL
     */
    std::shared_ptr<D>* GetShared(int iId)
    {
        std::map<int, std::shared_ptr<D> >::iterator iteMap;
        
        iteMap = m_mapId.find(iId);

        if (iteMap == m_mapId.end())
        {
            return NULL;
        }
        return &iteMap->second;
    }

    /**
     *  @brief  データ取得
     *  @param  [out] pWeak
     *  @param  [in]  iId
     *  @retval false 失敗
     *  @note   
     */
    bool GetWeak(std::weak_ptr<D>* pWeak, int iId)
    {
        std::map<int, std::shared_ptr<D> >::iterator iteMap;
        
        iteMap = m_mapId.find(iId);

        if (iteMap == m_mapId.end())
        {
            return false;
        }
        *pWeak = iteMap->second;
        return true;
    }

    /**
     *  @brief  データ取得
     *  @param  [in] iId
     *  @retval データ
     *  @note   データが存在しない場合はNULL
     */
    std::shared_ptr<D>* GetWeak(int iId)
    {
        std::map<int, std::shared_ptr<D> >::iterator iteMap;
        
        iteMap = m_mapId.find(iId);

        if (iteMap == m_mapId.end())
        {
            return NULL;
        }
        return &iteMap->second;
    }

    /**
     *  @brief  データ削除
     *  @param  [in] iId
     *  @retval true 削除成功
     *  @note  
     */
    bool DeleteData(int iId)
    {
        std::map<int, std::shared_ptr<D> >::iterator iteMap;
        
        iteMap = m_mapId.find(iId);

        if (iteMap == m_mapId.end())
        {
            return false;
        }

        m_mapId.erase(iteMap);
        return true;
    }

    /**
     *  @brief  データ取得
     *  @param  [in] iId
     *  @retval データ
     *  @note   データが存在しない場合はNULL
     */
    D* GetData(int iId)
    {
        std::shared_ptr<D>* pShared;
        pShared = GetShared(iId);

        if (pShared)
        {
            return pShared->get();
        }

        return NULL;
    }

protected:
    /**
     *  @brief  ID検索
     *  @param  なし
     *  @retval 空きID
     *  @note   空きIDがない場合は-1を返す
     */
    int SearchId()
    {
        int iNewId;
        int iSize;
        std::map<int, std::shared_ptr<D> >::iterator iteMap;
        iteMap = m_mapId.begin();
        iSize = static_cast<int>(m_mapId.size()) - 1;
        iteMap = m_mapId.end();
        iteMap--;
        iNewId = iteMap->first + 1;

        if (iNewId >= iIdMax)
        {
            iNewId = -1;
            int iCnt = 1;
            int iId;
            for (iteMap  = m_mapId.begin();
                 iteMap != m_mapId.end();
                 iteMap++)
            {
                iId = iteMap->first;
                if (iId != iCnt)
                {
                    iNewId = iCnt;
                    break;
                }
                iCnt++;
            }
        }
        return iNewId;
    }

protected:
    //!< マップ
    std::map<int, std::shared_ptr<D> > m_mapId;

    //!< ID最大値
    int iIdMax;

private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        StdString strRead;
        try
        {
            ar & boost::serialization::make_nvp("IdMax"    , iIdMax);
            ar & boost::serialization::make_nvp("IdMap"    , m_mapId);
        }
        catch(...)
        {
            STD_DBG(_T("Serlization Error %s"), strRead.c_str());
        }
    }

};


#endif  //__ID_MAP_H__


