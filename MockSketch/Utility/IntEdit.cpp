//////////////////////////////////////////////////////////////////////
//
//	 Title					：2406 PC-EPMA
//	 Filename				：IntEdit.h
//	 Author					：SystemV
//	 Belong to				：SystemV
//	 Creation date			：2005/08/31
//	 Copyright				：Copyright(c) 2005 JEOL Ltd.
//	 Abstract				：囲チェックつき実数値入力エディットボックス
//
//	 Compiler				：VisualC++.NET (2003)
//	 Target system			：Windows XP   (ServicePack-2)
//	 Testing Environment	：Windows XP   (ServicePack-2)
//
//////////////////////////////////////////////////////////////////////

/*---------------------------------------------------------------------------*/
/*      Header files                                                         */
/*---------------------------------------------------------------------------*/
#include "stdafx.h"
#include "./IntEdit.h"


/*---------------------------------------------------------------------------*/
/*      Macros                                                               */
/*---------------------------------------------------------------------------*/
IMPLEMENT_DYNAMIC(CIntEdit, CEdit)

BEGIN_MESSAGE_MAP(CIntEdit, CEdit)
    ON_CONTROL_REFLECT_EX(EN_KILLFOCUS, OnEnKillfocus)
    ON_CONTROL_REFLECT_EX(EN_CHANGE, OnEnChange)
    ON_WM_TIMER()
END_MESSAGE_MAP()


///////////////////////////////////////////////////////////
//	Method    : CIntEdit::CIntEdit
//	Summary   : コンストラクター
//	Arguments : なし
//	Return    : なし
//	Exception : なし
//	Note      :
///////////////////////////////////////////////////////////
CIntEdit::CIntEdit()
{
    m_iMax = 100;
    m_iMin = 0;
    m_sFormat.Format(_T("%%d"));
    m_bUpdate = false;
    m_dwMsec = 1500;
    m_nTimer = 0;
}

///////////////////////////////////////////////////////////
//	Method    : CIntEdit::~CIntEdit
//	Summary   : デストラクター
//	Arguments : なし
//	Return    : なし
//	Exception : なし
//	Note      :
///////////////////////////////////////////////////////////
CIntEdit::~CIntEdit()
{
}


///////////////////////////////////////////////////////////
//	Method    : CIntEdit::SetRange
//	Summary   : 表示範囲設定
//	Arguments : iLower (i)下限値
//	          : iUpper (i)上限値
//	Return    : なし
//	Exception : なし
//	Note      :
///////////////////////////////////////////////////////////
void CIntEdit::SetRange(int iLower, int iUpper)
{
    m_iMax = iUpper;
    m_iMin = iLower;
}

///////////////////////////////////////////////////////////
//	Method    : CIntEdit::GetVal
//	Summary   : 値の取得
//	Arguments : bRangeCheck (i) 範囲チェックの有無
//	Return    : 現在の設定値
//	Exception : なし
//	Note      :
///////////////////////////////////////////////////////////
int  CIntEdit::GetVal(bool bRangeCheck)
{
     CString strDat;
     UpdateData();
     GetWindowText(strDat);
     m_iVal = _tstoi(strDat);

     if (bRangeCheck)
     {
        if (m_iVal > m_iMax)
        {
            m_iVal = m_iMax;
        }
        else if (m_iVal < m_iMin)
        {
            m_iVal = m_iMin;
        }
     }
     return  m_iVal;
}

///////////////////////////////////////////////////////////
//	Method    : CIntEdit::SetVal
//	Summary   : 値の設定
//	Arguments : iVal (i) 設定する値
//              bRangeCheck (i)範囲チェックの有無
//	Return    : なし
//	Exception : なし
//	Note      :
///////////////////////////////////////////////////////////
void CIntEdit::SetVal(int iVal, bool bRangeCheck)
{
    CString strDat;
    m_iVal = iVal;

    if (bRangeCheck)
    {
        if (m_iVal > m_iMax)
        {
        m_iVal = m_iMax;
        }
        else if (m_iVal < m_iMin)
        {
        m_iVal = m_iMin;
        }
    }

    strDat.Format(m_sFormat, m_iVal);
    SetWindowText(strDat);
    UpdateData(FALSE);
}

///////////////////////////////////////////////////////////
//	Method    : CIntEdit::UpdateText
//	Summary   : 表示更新
//	Arguments : なし
//	Return    : なし
//	Exception : なし
//	Note      :
///////////////////////////////////////////////////////////
void    CIntEdit::UpdateText()
{
    CString strDat;
    GetVal();
    strDat.Format(m_sFormat, m_iVal);
    SetWindowText(strDat);
    UpdateData(FALSE);
}


///////////////////////////////////////////////////////////
//	Method    : CIntEdit::OnEnKillfocus
//	Summary   : フォーカス失効時の処理
//	Arguments : なし
//	Return    : 真偽
//	Exception : なし
//	Note      :
///////////////////////////////////////////////////////////
BOOL CIntEdit::OnEnKillfocus()
{
    UpdateText();
    return TRUE;
}

///////////////////////////////////////////////////////////
//	Method    : CIntEdit::OnEnChange
//	Summary   : エディットボックス変更時の処理
//	Arguments : なし
//	Return    : なし
//	Exception : なし
//	Note      : 
///////////////////////////////////////////////////////////
BOOL CIntEdit::OnEnChange()
{

    //ON_CONTROL_REFLECT_EX を使用し
    // FALSEを返した場合は、親ウインドウで
    // イベントが発生する

    // 手入力の場合は最後の入力後 1.5秒経過した後
    // イベントを発生させる
    if(m_bUpdate)
    {
        m_bUpdate = false;
        return FALSE;
    }
    else
    {
        StartTimer();
        return TRUE;
    }
}

///////////////////////////////////////////////////////////
//	Method    : CIntEdit::SendChange
//	Summary   : チェンジメッセージ発行
//	Arguments : なし
//	Return    : なし
//	Exception : なし
//	Note      : 親ウインドウでOnEnUpdateXXXXを発生させる
///////////////////////////////////////////////////////////
void CIntEdit::SendChange()
{
    CWnd* pWNd;
    StopTimer();
    pWNd = GetParent();
    if (pWNd != NULL)
    {
        //EN_CHANGEは自動的に発生するためここでイベントの
        //送信は行わない
        //WORD  nHi, nLo;
        //nHi = EN_CHANGE;
        //nLo = GetDlgCtrlID();
        //DWORD dwChange = (DWORD)MAKELONG(nLo, nHi);
        m_bUpdate = true;
        //pWNd->SendMessage(WM_COMMAND, dwChange, (LPARAM)m_hWnd);
    }
}

///////////////////////////////////////////////////////////
//	Method    : CIntEdit::PreTranslateMessage
//	Summary   : メッセージフィルタ処理
//	Arguments : pMsg (i) ウインドウメッセージ
//	Return    : true/false
//	Exception : なし
//	Note      :
///////////////////////////////////////////////////////////
BOOL CIntEdit::PreTranslateMessage(MSG* pMsg)
{
    if (pMsg->message == WM_KEYDOWN)
    {
        if(pMsg->wParam == VK_RETURN)
        {
            UpdateText();
            return TRUE;
        }
    }
    return CEdit::PreTranslateMessage(pMsg);
}

///////////////////////////////////////////////////////////
//	Method    : CIntEdit::CheckVal
//	Summary   : 表示データ範囲チェック
//	Arguments : pFrVal(o) 変更前の値
//              pToVal(o) 変更後の値
//	Return    : TRUE、範囲変更無し FALSE あり
//	Exception : なし
//	Note      :
///////////////////////////////////////////////////////////
BOOL CIntEdit::CheckVal(int* pFrVal, int*pToVal)
{
     CString strDat;
     BOOL bRet = TRUE;
     int ToVal;
     int FrVal;

     UpdateData();
     GetWindowText(strDat);
     m_iVal = _tstoi(strDat);
     ToVal = m_iVal;
     FrVal = m_iVal;

     if (m_iVal > m_iMax)
     {
        ToVal = m_iMax;
        m_iVal = m_iMax;
        bRet =  FALSE;
     }
     else if (m_iVal < m_iMin)
     {
        ToVal = m_iMin;
        m_iVal = m_iMin;
        bRet =  FALSE;
     }

    if (pFrVal != NULL)
    {
        *pFrVal = FrVal;
    }

    if (pToVal != NULL)
    {
        *pToVal = ToVal;
    }
    UpdateText();
    return  bRet;
}

///////////////////////////////////////////////////////////
//	Method    : CIntEdit::WindowProc
//	Summary   : Windows プロシージャ
//	Arguments : message (i) メッセージ
//              wParam (i) パラメータ
//              lParam (i) パラメータ
//	Return    : メッセージに依存する値
//	Exception : なし
//	Note      : 
///////////////////////////////////////////////////////////
LRESULT CIntEdit::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
    //スピンボタン等で変更した場合
    if (message == WM_SETTEXT)
    {
        SendChange();
    }
    return CEdit::WindowProc(message, wParam, lParam);
}

////////////////////////////////////////////////////////////
//	Method    : CIntEdit::StartTimer
//	Summary   : タイマー設定
//	Arguments : なし
//	Return    : 
//	Note      : 
///////////////////////////////////////////////////////////
void CIntEdit::StartTimer()
{
    if (m_dwMsec == 0)
    {
        return;
    }
    m_nTimer = SetTimer( 1, m_dwMsec, NULL);
}

////////////////////////////////////////////////////////////
//	Method    : CIconTextButton::StopTimer
//	Summary   : タイマー解除
//	Arguments : なし
//	Return    : 
//	Note      : 
///////////////////////////////////////////////////////////
void CIntEdit::StopTimer()
{
    if (m_nTimer)
    {
        KillTimer(m_nTimer);
        m_nTimer = 0;
    }
}

////////////////////////////////////////////////////////////
//	Method    : CIntEdit::OnTimer
//	Summary   : タイマー処理
//	Arguments : nIDEvent (i) イベントID
//	Return    : なし
//	Note      : 
///////////////////////////////////////////////////////////
void CIntEdit::OnTimer(UINT_PTR  nIDEvent)
{
    CEdit::OnTimer(nIDEvent);
    if (m_nTimer)
    {
        if(IsWindowVisible())
        {
            UpdateText();
        }
        StopTimer();
    }
}
