/**
 * @brief			IoConnectDlgヘッダーファイル
 * @file			IoConnectDlg.h
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#pragma once

/**
 * @class   CIntEdit
 * @brief   範囲チェックつき整数値入力エディットボックス
 */class CIntEdit : public CEdit
{
	DECLARE_DYNAMIC(CIntEdit)

public:
	CIntEdit();
	virtual ~CIntEdit();

    //レンジ設定
    void    SetRange(int iLower, int iUpper);

    //値の取得
    int    GetVal(bool bRangeCheck = true);

    //値の設定
    void    SetVal(int iVal,bool bRangeCheck = false);

    //値の更新
    void    UpdateText();

    //表示データ範囲チェック
    BOOL CheckVal(int* pFrVal, int*pToVal);

protected:
    //タイマー開始
    void StartTimer();

    //タイマー停止
    void StopTimer();

    //チェンジメッセージ発行
    void SendChange();

protected:

    //最大値
    int m_iMax;

    //最小値
    int m_iMin;

    //現在の値
    int m_iVal;

    //アップデート用フラグ
    bool m_bUpdate;

    //表示フォーマット
    CString m_sFormat;

    //----------------------------
    //ボタンを押し続けた場合の処理
    //----------------------------
    //反応時間
    DWORD       m_dwMsec;

    //タイマーID
    UINT_PTR    m_nTimer;

protected:

	DECLARE_MESSAGE_MAP()
public:
    afx_msg BOOL OnEnKillfocus();
    afx_msg BOOL OnEnChange();
    virtual BOOL PreTranslateMessage(MSG* pMsg);
protected:
    virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
public:
    afx_msg void OnTimer(UINT_PTR  nIDEvent);
};


