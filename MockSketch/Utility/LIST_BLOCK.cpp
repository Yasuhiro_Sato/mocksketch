/**
 * @brief			ID_MAP実装ファイル
 * @file		    ID_MAP.cpp
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./LIST_BLOCK.h"
#include "Utility/CUtility.h"





//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
/*
#define BOOST_TEST_MODULE "Unit test for hogehoge."
#include <boost/test/unit_test.hpp>
using namespace boost::unit_test;


BOOST_AUTO_TEST_SUITE( test_suite1 )  

BOOST_AUTO_TEST_CASE( test_1 )
{
	BOOST_CHECK_EQUAL( _T("1"), _T("1"));
}

BOOST_AUTO_TEST_CASE( test_2 )
{
	BOOST_CHECK_EQUAL( _T("2"), _T("2"));
}

BOOST_AUTO_TEST_SUITE_END()
*/
using namespace boost::assign;

void TEST_ADD()
{
    LIST_BLOCK< int >  listblock;

    std::vector< int > lstPart;

    lstPart += 0,1,2,3,4,5;
    listblock.AddData( lstPart );

    lstPart.clear();
    lstPart += 6,7,8;
    listblock.AddData( lstPart );


    lstPart.clear();
    lstPart += 16,17,18,19;
    listblock.AddData( lstPart );

    STD_ASSERT( listblock.Size() == 13);

    STD_ASSERT( listblock.Get(0) == 0);
    STD_ASSERT( listblock.Get(4) == 4);
    STD_ASSERT( listblock.Get(5) == 5);
    STD_ASSERT( listblock.Get(6) == 6);
    STD_ASSERT( listblock.Get(8) == 8);
    STD_ASSERT( listblock.Get(9)  == 16);
    STD_ASSERT( listblock.Get(10) == 17);
    STD_ASSERT( listblock.Get(11) == 18);
    STD_ASSERT( listblock.Get(12) == 19);


    //-------------------
    // 削除テスト
    //-------------------
    STD_ASSERT( listblock.Delete(7));
    STD_ASSERT( listblock.Size() == 10);

    STD_ASSERT( listblock.Get(0) == 0);
    STD_ASSERT( listblock.Get(4) == 4);
    STD_ASSERT( listblock.Get(5) == 5);
    STD_ASSERT( listblock.Get(6) == 16);
    STD_ASSERT( listblock.Get(7) == 17);
    STD_ASSERT( listblock.Get(8) == 18);
    STD_ASSERT( listblock.Get(9) == 19);

    STD_ASSERT( !listblock.Delete(20));


    //-------------------
    // 挿入テスト
    //-------------------
    lstPart.clear();
    lstPart += 6,7,8;

    listblock.Insert( 6, lstPart);

    STD_ASSERT( listblock.Size() == 13);

    STD_ASSERT( listblock.Get(0) == 0);
    STD_ASSERT( listblock.Get(4) == 4);
    STD_ASSERT( listblock.Get(5) == 5);
    //---------------------------------
    STD_ASSERT( listblock.Get(6) == 6);
    STD_ASSERT( listblock.Get(8) == 8);
    //---------------------------------
    STD_ASSERT( listblock.Get(9)  == 16);
    STD_ASSERT( listblock.Get(10) == 17);
    STD_ASSERT( listblock.Get(11) == 18);
    STD_ASSERT( listblock.Get(12) == 19);

    //-------------------
    // データ設定
    //-------------------
    STD_ASSERT( listblock.Set(0, 10) );
    STD_ASSERT( listblock.Set(9, 90) );
    STD_ASSERT( listblock.Set(12, 120) );

    STD_ASSERT( listblock.Get(0) == 10);
    STD_ASSERT( listblock.Get(9)  == 90);
    STD_ASSERT( listblock.Get(12) == 120);


}

void TEST_LIST_BLOCK()
{
    CUtil::DbgOut(_T("%s Start\n"), DB_FUNC_NAME.c_str());


    TEST_ADD();


    CUtil::DbgOut(_T("%s End\n"), DB_FUNC_NAME.c_str());
    CUtil::DbgOut(_T("\n"));
}
#endif //_DEBUG