/**
 * @brief			LIST_BLOCKヘッダーファイル
 * @file		    LIST_BLOCK.h
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */


#ifndef __LIST_BLOCK_H__
#define __LIST_BLOCK_H__

/**
 * @class   LIST_BLOCK
 * @brief   vector< vector <D> > の構造に at(n) のように
 *          一次元配列的にアクセスできるようにする          
 *          IO_DEFなどで使用予定
 */
template <class D>
class LIST_BLOCK
{
public:

    /**
     *  @brief  コンストラクター
     *  @param  なし
     *  @retval なし
     *  @note   
     */
    LIST_BLOCK()
    {
        ;
    }

    /**
     *  @brief  デストラクタ
     *  @param  なし
     *  @retval なし
     *  @note   
     */
    ~LIST_BLOCK()
    {
    }

    /**
     *  @brief  データ登録
     *  @param  [in] pData     設定データ
     *  @retval なし
     *  @note   
     */
    virtual void AddData(const std::vector <D>& list)
    {
        boost::mutex::scoped_lock lk(mtxGuard);
        m_lstVector.push_back(list);
        Update();
    }

    /**
     *  @brief  データ挿入
     *  @param  [in] iPos      データ位置
     *  @param  [in] pData     設定データ
     *  @retval true:成功
     *  @note   
     */
    virtual bool Insert(int iPos, const std::vector <D>& list)
    {
        int iBlockPos;
        boost::mutex::scoped_lock lk(mtxGuard);

        iBlockPos = m_lstIndex[iPos].get<0>();

        std::vector< std::vector <D> >::iterator ite;

        ite = m_lstVector.begin();
        ite += iBlockPos;

        m_lstVector.insert(ite , list); 

        Update();
        return true;
    }



    /**
     *  @brief  データ削除
     *  @param  [in] iPos   データ位置
     *  @retval true:成功
     *  @note   
     */
    virtual bool Delete(int iPos)
    {
        int iBlockPos;
        boost::mutex::scoped_lock lk(mtxGuard);

        if (iPos < 0){return false;}
        if (iPos >= static_cast<int>(m_lstIndex.size()))
        {
            return false;
        }

        iBlockPos = m_lstIndex[iPos].get<0>();

        std::vector< std::vector <D> >::iterator ite;

        ite = m_lstVector.begin();
        ite += iBlockPos;

        m_lstVector.erase(ite);
        Update();

        return true;
    }


    /**
     *  @brief  データ数
     *  @param  なし
     *  @retval データの個数
     *  @note   
     */
    int Size() const
    {
        boost::mutex::scoped_lock lk(mtxGuard);
        return static_cast<int>(m_lstIndex.size());   
    }

    /**
     *  @brief  データ取得
     *  @param  なし
     *  @retval 
     *  @note   
     */
    virtual D& Get(int iPos) const
    {
        boost::mutex::scoped_lock lk(mtxGuard);
        return *m_lstIndex[iPos].get<1>();   
    }

    /**
     *  @brief  データ設定
     *  @param  [in] iPos
     *  @param  [in] data
     *  @retval 
     *  @note   
     */
    virtual bool Set(int iPos, D data)
    {
        int iBlockPos;
        boost::mutex::scoped_lock lk(mtxGuard);

        if (iPos < 0){return false;}
        if (iPos >= static_cast<int>(m_lstIndex.size()))
        {
            return false;
        }

        iBlockPos = m_lstIndex[iPos].get<0>();

        int iIntanalPos = 0;

        if (iPos != 0)
        {
            for (int iCnt = iPos - 1; iCnt != 0; iCnt--)
            {
                if(iBlockPos !=  m_lstIndex[iCnt].get<0>())
                {

                    break;
                }
                iIntanalPos++;
            }
        }
        else
        {
            iIntanalPos = 0;
        }

        m_lstVector[iBlockPos].at(iIntanalPos) = data;
        m_lstIndex[iPos].get<1>() = &m_lstVector[iBlockPos].at(iIntanalPos);
        return true;
    }


protected:

    /**
     *  @brief  データ更新
     *  @param  なし
     *  @retval 
     *  @note   
     */
    void Update()
    {

        //TODO:とりあえず実装、あとで最適化
        m_lstIndex.clear();

        int iCnt = 0;
        foreach(std::vector<D>& latMain, m_lstVector)
        {
            foreach(D& val, latMain)
            {
                m_lstIndex.push_back(boost::make_tuple(iCnt, &val));
            }
            iCnt++;
        }
    }

protected:
    //---------------------
    // 保存データ
    //---------------------
    //!< データ本体
    std::vector< std::vector <D> >          m_lstVector;


    //---------------------
    // 非保存データ
    //---------------------
    mutable boost::mutex                    mtxGuard;

    //!< データインデックス
    std::vector< boost::tuple<int, D*> >    m_lstIndex;

private:
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
            SERIALIZATION_BOTH("Block"    , m_lstVector);
            if (Archive::is_loading::value)
            {
                Update();
            }
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }};


#endif  //__LIST_BLOCK_H__


