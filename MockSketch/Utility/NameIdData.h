/**
 * @brief			NameIdDataヘッダーファイル
 * @file		    NameIdData.h
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#if !defined (__NAME_ID_DATA_H__)
#define __NAME_ID_DATA_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "SYSTEM/MOCK_ERROR.h"


template <class D>
class NAME_ID_DATA
{
public:
    struct ID_DATA
    {
        D*          pData;
        int         iId;
        StdString strName;

        //==============================
        // ファイル保存
        //==============================
        friend class boost::serialization::access;  
        template<class Archive>
        void serialize(Archive& ar, const unsigned int version)
        {
            StdString strRead;
            try
            {
                ar & boost::serialization::make_nvp("Data"  , *pData);   strRead += _T("Data,");
                ar & boost::serialization::make_nvp("ID"    , iId);     strRead += _T("ID,");
                ar & boost::serialization::make_nvp("Name"  , strName); strRead += _T("Name,");
            }
            catch(...)
            {
                STD_DBG(_T("Serlization Error %s"), strRead.c_str());
            }
        }
    };

public:
    //* コンストラクター
    NAME_ID_DATA()
    {
        m_iId = 1;
    }

    //* デストラクター
    ~NAME_ID_DATA()
    {
        ID_DATA Obj;
        std::pair<int, ID_DATA> pairMap;
        foreach(pairMap, m_mapObjects)
        {
            Obj = pairMap.second;
            delete Obj.pData;
        }
    }

    //!< マップ取得
    std::map<int, ID_DATA >* GetIdMap()
    {
        return &m_mapObjects;
    }

    //!< マップ再構築
    void ResetNameMap()
    {
        //番号再設定
        m_iId = 1;
        std::pair<int, ID_DATA> pairMap;
        m_mapNameObjects.clear();

        ID_DATA Obj;

        std::map<int, ID_DATA >::iterator iteMap;
        std::map<int, ID_DATA > mapTmp;

        for (iteMap  = m_mapObjects.begin();
             iteMap != m_mapObjects.end();
             iteMap++)
        {
            Obj = iteMap->second;
 
            Obj.iId = m_iId;
            mapTmp[m_iId] = Obj;

            //名称マップ再構築
            if (Obj.strName != _T(""))
            {
                m_mapNameObjects[Obj.strName] = Obj;
            }
            m_iId++;
        }
        m_mapObjects.clear();
        m_mapObjects = mapTmp;
    }


    //!< 名称設定
    bool SetName(int iId, StdString strName)
    {
        if ( strName == _T(""))
        {
            return false;
        }

        if (Find(strName) != NULL)
        {
            return false;
        }
        
        std::map<int, ID_DATA>::iterator ite;

        ID_DATA Data;
        ite = m_mapObjects.find(iId);
        if (ite != m_mapObjects.end())
        {
            ite->second.strName = strName;
            Data = ite->second;
            m_mapNameObjects[strName] = Data;
            return true;
        }
        return false;
    }

    //!< 名称取得
    StdString GetName(int iId)
    {
        StdString strRet;

        std::map<int, ID_DATA>::iterator ite;

        ite = m_mapObjects.find(iId);
        if (ite != m_mapObjects.end())
        {
            return ite->second.strName;
        }
        return strRet;
    }

    //!< 名称変更
    bool ChgName(StdString strNameOrg, StdString strNameChg)
    {
        if ( strNameOrg == _T(""))
        {
            return false;
        }

        std::map<StdString, ID_DATA>::iterator ite;

        ite = m_mapNameObjects.find(strNameOrg);
        if (ite == m_mapNameObjects.end())
        {
            return false;
        }
        
        ID_DATA Data;

        Data = ite->second;
        m_mapNameObjects.erase(ite);
        Data.strName = strNameChg;
        m_mapNameObjects[strNameChg] = Data;

        //IDリスト変更
        std::map<int, ID_DATA>::iterator iteId;

        iteId = m_mapObjects.find(Data.iId);
        STD_ASSERT(iteId != m_mapObjects.end());
        iteId->second.strName = strNameChg;

        return true;
    }

    //!< データ追加(名称つき)
    int AddData(D* pData, StdString strName = _T(""))
    {
        ID_DATA Data;
        Data.pData   = pData;
        Data.iId     = m_iId;
        Data.strName = strName; 
        m_mapObjects[m_iId] = Data;

        if (strName != _T(""))
        {
            m_mapNameObjects[strName] = Data;
        }

        m_iId++;
        if (m_iId >= MAX_OBJ)
        {
            //ResetNameMap();
            throw MockException(e_buffer_full);
        }
        return (m_iId - 1);
    }

    //!< 最大ID取得
    int GetMaxId()
    {
        return m_iId - 1;
    }

    //!< IDによるデータ検索
    D* Find(int iId)
    {
        std::map<int, ID_DATA>::iterator ite;

        ite = m_mapObjects.find(iId);
        if (ite != m_mapObjects.end())
        {
            return ite->second.pData;
        }
        return NULL;
    }

    //!< 名称によるデータ検索
    D* Find(StdString strName)
    {
        std::map<StdString, ID_DATA>::iterator ite;
        if (strName == _T(""))
        {
            return NULL;
        }

        ite = m_mapNameObjects.find(strName);
        if (ite != m_mapNameObjects.end())
        {
            return ite->second.pData;
        }
        return NULL;
    }

    //!< 名称によるID取得
    int GetId(StdString strName)
    {
        std::map<StdString, ID_DATA>::iterator ite;
        if (strName == _T(""))
        {
            return -1;
        }

        ite = m_mapNameObjects.find(strName);
        if (ite != m_mapNameObjects.end())
        {
            return ite->second.iId;
        }
        return -1;
    }

    //!< IDによるデータ削除
    bool Delete(int iId)
    {
        std::map<int, ID_DATA>::iterator ite;

        ID_DATA Data;
        ite = m_mapObjects.find(iId);
        if (ite != m_mapObjects.end())
        {
            Data = ite->second;
            
            delete Data.pData;
            m_mapObjects.erase(ite);
            
            if (Data.strName != _T(""))
            {
                std::map<StdString, ID_DATA>::iterator iteName;
                iteName = m_mapNameObjects.find(Data.strName);
                m_mapNameObjects.erase(iteName);
            }
            return true;
        }
        return false;
    }


    //!< 入れ替え
    bool Replace(int iId, D* pData, StdString strName = _T("")))
    {
        std::map<int, ID_DATA>::iterator ite;

        ID_DATA Data;
        ite = m_mapObjects.find(iId);
        if (ite != m_mapObjects.end())
        {
            if (Data.strName != _T(""))
            {
                std::map<StdString, ID_DATA>::iterator iteName;
                iteName = m_mapNameObjects.find(Data.strName);
                m_mapNameObjects.erase(iteName);
            }


            Data = ite->second;
            delete Data.pData;

            Data.pData = pData;
            Data.strName = strName;
            if (strName != _T(""))
            {
                m_mapNameObjects[strName] = Data;
            }
            return true;
        }
        return false;
    }


protected:

    //!< オブジェクト
    std::map<int, ID_DATA >            m_mapObjects;

    //!< 描画オブジェクト(名前検索用)
    std::map<StdString , ID_DATA >   m_mapNameObjects;

    //ID
    int m_iId;

private:

    //==============================
    // ファイル保存
    //==============================
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT
            SERIALIZATION_BOTH("ObjectMap"  , m_mapObjects);
            SERIALIZATION_BOTH("ID"         , m_iId);
            ResetNameMap();
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }
};

#endif //__NAME_ID_DATA_H__