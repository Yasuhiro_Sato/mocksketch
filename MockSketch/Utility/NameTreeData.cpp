/**
 * @brief			NameTreeData実装ファイル
 * @file		    NameTreeData.cpp
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./NameTreeData.h"


//*******************************
//             TEST
//*******************************
#ifdef _DEBUG

#include "CUtility.h"

void TEST_NameTreeData()
{
    CUtil::DbgOut(_T("%s Start\n"), DB_FUNC_NAME.c_str());


    NAME_TREE<StdString> tree;
    /*
       CPropertyGrid(CMFCPropertyGridCtrl)とCStdPropertyTreeを１対１で対応させる

       class CPropertyGrid : public CMFCPropertyGridCtrl


       CMFCPropertyGridProperty
       +------------------------------------------+ 
       |CStdPropertyTree                         |
       +------------------------------------------+ 
       |    NAME_TREE<CStdPropertyItem>  m_Tree; |
       +------------------------------------------+ 


  CStdPropertyTree -> CMFCPropertyGridCtrl への流れ

       Viewでオブジェクトが選択が変更された。
       CPropertiesWnd::OnViewSelchange

       CPropertiesWnd::SetObject(std::vector<CDrawingObject*>* pListObject)

       CDrawingObject::SetProperty(CMFCPropertyGridCtrl *pGrid, UINT uiMask)
       ------------------------------
                CPropertyGrid::SetGridData(TREE_GRID_ITEM* pItem, CMFCPropertyGridProperty** ppBeforeGridProperty)
                CPropertyGrid::SetGridItem() IDの関連つけ
                CDrawingObject::UpdateProperty(pGrid);
       ------------------------------
                CDrawingObject::UpdateProperty(CMFCPropertyGridCtrl *pGrid, 
                                    CStdPropertyTree    *pProprtyGrid,
                                    std::vector<int>     *pListId)

                                    ここで具体的な処理が行われる

    CMFCPropertyGridCtrl -> CStdPropertyTree への流れ

        CPropertyGrid::ValidateItemData(CMFCPropertyGridProperty* pProp)
        で設定する。
    */




    /* 以下のようなデータを作成する

        DATA (Name1, Name2) id

        + ROOT (root, root-) 0
            +CHILD1 (child1, child-1) 1
            |   +CHILD1_1(child1_1, child-1_1) 2
            |   |CHILD1_2(child1_2, child-1_2) 3
            |   |CHILD1_3(child1_3, child-1_3) 4  (offset 2)
            |   |CHILD1_4(child1_4, child-1_4) 6
            |
            +CHILD2 (child2, child-2) 7
            |   +CHILD2_1(child2_1, child-2_1) 8
            |   |CHILD2_2(child2_2, child-2_2) 9  (offset 2)
            |   |CHILD2_3(child2_3, child-2_3) 11
            |
            +CHILD3 (child3, child-3) 12
                +CHILD3_1(child3_1, child-3_1) 13
                |CHILD3_2(child3_2, child-3_2) 14

        Test2. データの呼び出し
            Test1.1  名称1での呼び出し    
            Test1.2  名称2での呼び出し    
            Test1.3  IDでの呼び出し    

        Test3. データの削除
    */


    //==============================
    // データの作成
    //==============================
    NAME_TREE_ITEM<StdString>* pRoot;
    NAME_TREE_ITEM<StdString>* pChild;
    NAME_TREE_ITEM<StdString>* pNext;

    pRoot  = tree.AddNext (NULL,   new StdString(_T("ROOT")), _T("root"), _T("root-")); 
    pChild = tree.AddChild(pRoot,  new StdString(_T("CHILD1")), _T("child1"), _T("child-1"));

    STD_ASSERT(tree.GetName(pChild, 0) == _T("root.child1"));

    pNext  = tree.AddChild(pChild, new StdString(_T("CHILD1_1")), _T("child1_1"), _T("child-1_1"));
    pNext  = tree.AddNext (pNext,  new StdString(_T("CHILD1_2")), _T("child1_2"), _T("child-1_2"));
    pNext  = tree.AddNext (pNext,  new StdString(_T("CHILD1_3")), _T("child1_3"), _T("child-1_3"), 2);
    pNext  = tree.AddNext (pNext,  new StdString(_T("CHILD1_4")), _T("child1_4"), _T("child-1_4"));

    pChild  = tree.AddNext (pChild, new StdString(_T("CHILD2")), _T("child2"), _T("child-2"));
    pNext   = tree.AddChild(pChild, new StdString(_T("CHILD2_1")), _T("child2_1"), _T("child-2_1"));

    STD_ASSERT(tree.GetName(pNext, 0) == _T("root.child2.child2_1"));
    STD_ASSERT(tree.GetName(pNext, 1) == _T("root-.child-2.child-2_1"));

    pNext   = tree.AddNext (pNext,  new StdString(_T("CHILD2_2")), _T("child2_2"), _T("child-2_2"), 2);
    pNext   = tree.AddNext (pNext,  new StdString(_T("CHILD2_3")), _T("child2_3"), _T("child-2_3"));

    pChild  = tree.AddNext (pChild, new StdString(_T("CHILD3")), _T("child3"), _T("child-3"));
    pNext   = tree.AddChild(pChild, new StdString(_T("CHILD3_1")), _T("child3_1"), _T("child-3_1"));
    pNext   = tree.AddNext (pNext,  new StdString(_T("CHILD3_2")), _T("child3_2"), _T("child-3_2"), 2);
    //==============================

    NAME_TREE_ITEM<StdString>* pItem;

    //------------------------------
    //名称からデータを取り出す
    //------------------------------
    pItem = tree.GetItem(_T("root.child2.child2_2"),0);

    STD_ASSERT(pItem !=  NULL);

    STD_ASSERT( *(pItem->pData) ==  _T("CHILD2_2"));
    //------------------------------

    //------------------------------
    //IDからデータを取り出す
    //------------------------------
    pItem = tree.GetItem(_T("root.child3.child3_1"),0);
    STD_ASSERT(pItem !=  NULL);

    int iId = pItem->iId;

    pItem = tree.GetItem(iId);
    STD_ASSERT( *(pItem->pData) ==  _T("CHILD3_1"));
    //------------------------------





    CUtil::DbgOut(_T("%s End\n"), DB_FUNC_NAME.c_str());
    CUtil::DbgOut(_T("\n"));
}
#endif //_DEBUG
