/**
 * @brief			NameTreeDataヘッダーファイル
 * @file		    NameTreeData.h
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#if !defined (__NAME_TREE_DATA_H__)
#define ___NAME_TREE_DATA_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "SYSTEM/MOCK_ERROR.h"

template <class D>
class NAME_TREE_ITEM
{   
public:
    NAME_TREE_ITEM();
    ~NAME_TREE_ITEM();

    void ClearAll();

public:
    NAME_TREE_ITEM<D>* pParent;
    NAME_TREE_ITEM<D>* pBefore;
    NAME_TREE_ITEM<D>* spNext;  //実体を保持する
    NAME_TREE_ITEM<D>* spChild; //実体を保持する
    D*                      pData;
    StdString               m_str[2];
    int                     iId;

};

template <class D>
class NAME_TREE
{
protected:
    static const int NAME_MAX = 2;
    std::map<StdString, NAME_TREE_ITEM<D>*>    m_mapName[NAME_MAX];
    std::map<int,       NAME_TREE_ITEM<D>*>    m_mapId;
   
    NAME_TREE_ITEM<D>                m_Root;   
    StdString                  m_str[NAME_MAX];
    int                        m_IdMax;

public:
    NAME_TREE();
    ~NAME_TREE(); 




    NAME_TREE_ITEM<D>* AddNext(NAME_TREE_ITEM<D>* pBefore, 
                                        D* pData,
                                        StdString  strName1, 
                                        StdString  strName2 = _T(""),
                                        int iOffset = 1);

    NAME_TREE_ITEM<D>* AddChild(NAME_TREE_ITEM<D>* pParent, 
                                        D* pData,
                                        StdString  strName1, 
                                        StdString  strName2 = _T(""),
                                        int iOffset = 1);

    //アイテム名取得
    StdString GetName(NAME_TREE_ITEM<D>* pItem, int iNo);

    //ルートアイテム取得
    NAME_TREE_ITEM<D>* GetRoot();

    //アイテム取得
    NAME_TREE_ITEM<D>* GetItem(StdString strName, int iNo);

    //アイテム取得(ID)
    NAME_TREE_ITEM<D>* GetItem(int iId);

    //アイテムデータ取得
    D* GetItemData(StdString strName, int iNo);

    //アイテムデータ取得(ID)
    D* GetItemData(int iId);

    //アイテム削除(名称指定)
    bool DeleteItem(StdString strName, int iNo);

    //全アイテム削除
    void Clear();

    //アイテム削除(アイテム指定)
    bool DeleteItem(NAME_TREE_ITEM<D>* pItem);

    //子アイテムの削除
    bool DeleteChild(NAME_TREE_ITEM<D>* pItem);

    //ツリー構造表示
    void DspTree();

    void DspTree(NAME_TREE_ITEM<D>* pItem, int iLevel);

};


/**
 * @brief   コンストラクタ
 * @param   なし
 * @return	なし
 * @note	 
 */
template <class D>
NAME_TREE_ITEM<D>::NAME_TREE_ITEM():   
    pParent(0), 
    pBefore(0), 
    pData(0), 
    spNext(0), 
    spChild(0), 
    iId(0)
{
    ;
}

/**
 * @brief   デスト    //アイテム取得(ID)
    NAME_TREE_ITEM<D>* GetItem(int iId);

    //アイテムデータ取得
    D* GetItemData(StdString strName, int iNo);

    //アイテムデータ取得(ID)
    D* GetItemData(int iId);

    //アイテム削除(名称指定)
    bool DeleteItem(StdString strName, int iNo);

    //全アイテム削除
    void Clear();

    //アイテム削除(アイテム指定)
    bool DeleteItem(NAME_TREE_ITEM<D>* pItem);

ラクタ
 * @param   なし
 * @return	なし
 * @note	 
 */
template <class D>
NAME_TREE_ITEM<D>::~NAME_TREE_ITEM()
{
    if (pData != NULL)
    {
        delete pData;
        pData = NULL;
    }
}

/**
 * @brief   全データ削除
 * @param   なし
 * @return	なし
 * @note	 
 */
template <class D>
void NAME_TREE_ITEM<D>::ClearAll()
{
    //自身に接続するすべてのデータ削除
    if (pData != NULL)
    {
        delete pData;
        pData = NULL;
    }

    if (spNext != NULL)
    {
        spNext->ClearAll();
        delete spNext;
        spNext = NULL;
    }


    if (spChild != NULL)
    {
        spChild->ClearAll();
        delete spChild;
        spChild = NULL;
    }
    pParent = NULL;
    pParent = NULL;

    iId = 0;
    m_str[0] = _T("");
    m_str[1] = _T("");
}


//----------------------------------------------
//----------------------------------------------
//----------------------------------------------

/**
 * @brief   コンストラクタ
 * @param   なし
 * @return	なし
 * @note	 
 */
template <class D>
NAME_TREE<D>::NAME_TREE():m_IdMax(0)
{
    ;
}

/**
 * @brief   デストラクタ
 * @param   なし
 * @return	なし
 * @note	 
 */
template <class D>
NAME_TREE<D>::~NAME_TREE() 
{
    Clear();
}

/**
 * @brief   兄弟アイテム追加
 * @param   [in]  pBefore  兄アイテム
 * @param   [in]  pData    データ
 * @param   [in]  strName1 名称１
 * @param   [in]  strName2 名称２
 * @param   [in]  iOffset  IDオフセット値
 * @return	なし
 * @note	 データの生存はこのクラスで管理する。
 *           オフセットはIDの増加間隔を変更する。
 *          例えばPOINT2Dの様に内部に2つデータがあり、設定項目が
 *          2つある場合はoffset = 2 とする 
 *          ROOTアイテムは pBefore にNULLを設定する
 */
template <class D>
NAME_TREE_ITEM<D>* NAME_TREE<D>::AddNext(NAME_TREE_ITEM<D>* pBefore, 
                                    D* pData,
                                    StdString  strName1, 
                                    StdString  strName2,
                                    int iOffset)
{
    if (pBefore == NULL)
    {
        m_str[0]  = strName1;
        m_str[1]  = strName2;

        for (int iNo = 0; iNo < NAME_MAX; iNo++)
        {
            m_mapName[iNo][strName1] = &m_Root;
            m_Root.m_str[iNo] = m_str[iNo];
        }

        m_IdMax = 0;
        m_Root.iId = m_IdMax;

        for (int iCnt = 0; iCnt < iOffset; iCnt++)
        {
            m_mapId[m_IdMax + iCnt] = &m_Root;
        }

        m_IdMax += iOffset;


        m_Root.pData = pData;
        return &m_Root;
    }
    else
    {
        NAME_TREE_ITEM<D>* itemData =  new NAME_TREE_ITEM<D>;
        pBefore->spNext = itemData;
        itemData->pBefore = pBefore;
        itemData->pParent = pBefore->pParent; 
        itemData->pData = pData;

        itemData->m_str[0] = strName1;
        itemData->m_str[1] = strName2;

        itemData->iId = m_IdMax;

        for (int iCnt = 0; iCnt < iOffset; iCnt++)
        {
            m_mapId[m_IdMax + iCnt] = itemData;
        }
        m_IdMax += iOffset;

        m_mapName[0][GetName(itemData, 0)] = itemData;
        m_mapName[1][GetName(itemData, 1)] = itemData;
        return itemData;
    }
}

/**
 * @brief   子アイテム追加
 * @param   [in]  pBefore  親アイテム
 * @param   [in]  pData    データ
 * @param   [in]  strName1 名称１
 * @param   [in]  strName2 名称２
 * @param   [in]  iOffset  IDオフセット値
 * @return	なし
 * @note	 データの生存はこのクラスで管理する。
 *           オフセットはIDの増加間隔を変更する。
 *          例えばPOINT2Dの様に内部に2つデータがあり、設定項目が
 *          2つある場合はoffset = 2 とする 
 */
template <class D>
NAME_TREE_ITEM<D>* NAME_TREE<D>::AddChild(NAME_TREE_ITEM<D>* pParent, 
                                    D* pData,
                                    StdString  strName1, 
                                    StdString  strName2,
                                    int iOffset)
{
    if (pParent == NULL)
    {
        m_mapName[0][strName1] = &m_Root;
        m_mapName[1][strName2] = &m_Root;

        m_str[0]  = strName1;
        m_str[1]  = strName2;

        m_Root.m_str[0] = m_str[0];
        m_Root.m_str[1] = m_str[1];

        m_IdMax = 0;
        m_Root.iId = m_IdMax;

        for (int iCnt = 0; iCnt < iOffset; iCnt++)
        {
            m_mapId[m_IdMax + iCnt] = &m_Root;
        }
        m_IdMax += iOffset;

        m_Root.pData = pData;
        return &m_Root;
    }
    else
    {
        NAME_TREE_ITEM<D>* itemData = new NAME_TREE_ITEM<D>;
        STD_ASSERT(pParent->spChild == NULL);

        pParent->spChild = itemData;
        itemData->pParent = pParent; 
        itemData->pData = pData;

        itemData->m_str[0] = strName1;
        itemData->m_str[1] = strName2;

        itemData->iId = m_IdMax;
        for (int iCnt = 0; iCnt < iOffset; iCnt++)
        {
            m_mapId[m_IdMax + iCnt] = itemData;
        }
        m_mapName[0][GetName(itemData, 0)] = itemData;
        m_mapName[1][GetName(itemData, 1)] = itemData;

        m_IdMax += iOffset;

        return itemData;
    }
}

/**
 * @brief   アイテム名取得
 * @param   [in]  pItem    アイテム
 * @param   [in]  iNameNo      名称種別(名称１/名称２)
 * @return	アイテム名称
 * @note    親から"."で区切られた名称を取得する
 */
template <class D>
StdString NAME_TREE<D>::GetName(NAME_TREE_ITEM<D>* pItem, int iNameNo)
{
    STD_ASSERT(iNameNo < NAME_MAX);

    StdString strRet;
    strRet = pItem->m_str[iNameNo];

    NAME_TREE_ITEM<D>* pItemParent = pItem->pParent;
    while(pItemParent != NULL)
    {
        strRet = pItemParent->m_str[iNameNo] + _T(".") + strRet;
        pItemParent = pItemParent->pParent;
    }
    return  strRet;
}

/**
 * @brief   ルートアイテム取得
 * @param   なし
 * @return	ルートアイテム
 * @note    
 */
template <class D>
NAME_TREE_ITEM<D>* NAME_TREE<D>::GetRoot()
{
    return &m_Root;
}

/**
 * @brief   アイテム取得
 * @param   [in] strName
 * @param   [in] iNo
 * @return  アイテム
 * @note    
 */
template <class D>
NAME_TREE_ITEM<D>* NAME_TREE<D>::GetItem(StdString strName, int iNo)
{
    STD_ASSERT(iNo < NAME_MAX);

    std::map<StdString, NAME_TREE_ITEM<D>*>::iterator iteFind;
    
    iteFind = m_mapName[iNo].find(strName);
    if (iteFind == m_mapName[iNo].end())
    {
        return NULL;
    }

    return iteFind->second;
}


//アイテム取得(ID)
template <class D>
NAME_TREE_ITEM<D>* NAME_TREE<D>::GetItem(int iId)
{
    std::map<int, NAME_TREE_ITEM<D>*>::iterator iteFind;
    
    iteFind = m_mapId.find(iId);
    if (iteFind == m_mapId.end())
    {
        return NULL;
    }
    return iteFind->second;
}


//アイテムデータ取得
template <class D>
D* NAME_TREE<D>::GetItemData(StdString strName, int iNo)
{
    NAME_TREE_ITEM<D>* pItem = GetItem(strName, iNo);
    
    if (pItem == NULL)
    {
        return NULL;
    }

    return pItem->pData;
}

//アイテムデータ取得(ID)
template <class D>
D* NAME_TREE<D>::GetItemData(int iId)
{
    NAME_TREE_ITEM<D>* pItem = GetItem(iId);
    
    if (pItem == NULL)
    {
        return NULL;
    }

    return pItem->pData;
}


//アイテム削除(名称指定)
template <class D>
bool NAME_TREE<D>::DeleteItem(StdString strName, int iNo)
{
    STD_ASSERT(iNo < NAME_MAX);

    std::map<StdString, NAME_TREE_ITEM<D>*>::iterator iteFind;
    
    iteFind = m_mapName[iNo].find(strName);
    if (iteFind == m_mapName[iNo].end())
    {
        return false;
    }
    DeleteItem(iteFind->second);

    return true;
}

//全アイテム削除
template <class D>
void NAME_TREE<D>::Clear()
{
    m_IdMax = 0;

    /*
    std::map<int,       NAME_TREE_ITEM<D>*>::iterator itMap;
    NAME_TREE_ITEM<D>* pTreeItem;
    for(itMap  = m_mapId.begin();
        itMap != m_mapId.end();
        itMap++)
    {
        pTreeItem = itMap->second;
        pTreeItem->Clear();
    }
    */

    m_Root.ClearAll();
    m_mapId.clear();
    m_mapName[0].clear();
    m_mapName[1].clear();
    m_str[0]=_T("");
    m_str[1]=_T("");
}


//アイテム削除(アイテム指定)
template <class D>
bool NAME_TREE<D>::DeleteItem(NAME_TREE_ITEM<D>* pItem)
{
    bool bRet;
    NAME_TREE_ITEM<D>* pChild;
    NAME_TREE_ITEM<D>* pBefore;
    NAME_TREE_ITEM<D>* pParent;

    if (pItem == NULL)
    {
        return false;
    }

    pChild = pItem->spChild;
    if (pChild != NULL)
    {
        bRet = DeleteChild(pChild);
        STD_ASSERT(bRet);
    }


    std::map<StdString, NAME_TREE_ITEM<D>*>::iterator iteFind;

    StdString strItemName;
    for (int iNo = 0; iNo < NAME_MAX; iNo++)
    {
        strItemName = GetName(pItem, iNo);
        iteFind = m_mapName[iNo].find(strItemName);
        if (iteFind != m_mapName[iNo].end())
        {
            m_mapName[iNo].erase(iteFind);
        }
    }

    pParent = pItem->pParent;
    pBefore = pItem->pBefore;

    NAME_TREE_ITEM<D>* pDelete;
    // 実データは、親か兄弟アイテムが保持しているので
    // それを削除する
    if (pBefore != NULL)
    {
        if (pItem->spNext != NULL)
        {
            //自分に兄弟アイテムがある場合は付け替える
            pDelete = pBefore->spNext;

            pItem->spNext->pBefore = pItem->pBefore;
            pBefore->spNext = pItem->spNext;
            delete  pDelete;
            pItem = NULL;
        }
        else
        {
            delete  pBefore->spNext;
            pBefore->spNext = NULL;
            pItem = NULL;
        }
    }

    if (pParent != NULL)
    {
        if(pParent->spChild == pItem)
        {
            delete pParent->spChild;
            pParent->spChild = NULL;
        }
    }
    return true;
}

//子アイテムの削除
template <class D>
bool NAME_TREE<D>::DeleteChild(NAME_TREE_ITEM<D>* pItem)
{
    NAME_TREE_ITEM<D>* pChild;
    NAME_TREE_ITEM<D>* pLast;
    NAME_TREE_ITEM<D>* pNext;
    NAME_TREE_ITEM<D>* pDelete;

    pChild = pItem->spChild;
    if (pChild == NULL)
    {
        return true;
    }

    pLast = pChild;
    pNext = pChild->spNext;

    //リストの最後を探す
    while(pNext != NULL)
    {
        pLast = pNext;
        pNext = pNext->spNext;
    }

    while(pLast != NULL)
    {
        if (pLast->spChild != NULL)
        {
            DeleteChild(pLast->spChild);
        }
        pDelete = pLast;
        pLast = pLast->pBefore;
        DeleteItem(pDelete);
    }
    return true;
}


//ツリー構造表示
template <class D>
void NAME_TREE<D>::DspTree()
{
    DspTree(&m_Root, 0);
}

template <class D>
void NAME_TREE<D>::DspTree(NAME_TREE_ITEM<D>* pItem, int iLevel)
{
    NAME_TREE_ITEM<D>*  pDsp;
    NAME_TREE_ITEM<D>*  pChild;


    pDsp = pItem;
    while(pDsp != NULL)
    {
        DB_PRINT(_T("[%d]:%d  %s\n"), iLevel, pDsp->iId, GetName(pDsp, 0).c_str());
        pChild = pDsp->spChild;
        if (pChild != NULL)
        {
            DspTree(pChild, iLevel + 1);
        }
        pDsp = pDsp->spNext;
    }
}


#endif //__NAME_TREE_DATA_H__