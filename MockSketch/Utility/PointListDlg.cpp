/**
 * @brief			CPointListDlg実装ファイル
 * @file			PointListDlg.cpp
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./PointListDlg.h"
#include "MockSketch.h"


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
IMPLEMENT_DYNAMIC(CPointListDlg, CDialog)

BEGIN_MESSAGE_MAP(CPointListDlg, CDialog)
    ON_NOTIFY(LVN_ENDLABELEDIT, ID_GRD_POINT, &CPointListDlg::OnLvnEndlabeleditGrdPoint)
END_MESSAGE_MAP()


/**
 *  @brief  コンストラクター
 *  @param  なし
 *  @retval なし
 *  @note   
 */
CPointListDlg::CPointListDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPointListDlg::IDD, pParent)
{

}

/**
 *  @brief  デストラクター
 *  @param  なし
 *  @retval なし
 *  @note   
 */
CPointListDlg::~CPointListDlg()
{
}

/**
 *  @brief  フレームワークの自動的なデータ交換
 *  @param  pDX     CDataExchange オブジェクトへのポインタ
 *  @retval なし     
 *  @note   UpdateData メンバ関数から呼び出されます。
 */
void CPointListDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, ID_GRD_POINT, m_grdPoint);
}


/**
 *  @brief  ウインドウの初期化処理
 *  @param  なし
 *  @retval 常にTRUE     
 *  @note   
 */
BOOL CPointListDlg::OnInitDialog()
{
    //スタイル設定
    ListView_SetExtendedListViewStyleEx(m_grdPoint.m_hWnd, LVS_EX_FULLROWSELECT, LVS_EX_FULLROWSELECT);
    ListView_SetExtendedListViewStyleEx(m_grdPoint.m_hWnd, LVS_EX_GRIDLINES, LVS_EX_GRIDLINES);

    CRect rcWin;
    int   iNoWidth  = 30;
    int   iWidth;
    CDialog::OnInitDialog();

    
    m_grdPoint.GetWindowRect(rcWin);
    iWidth = (rcWin.Width() - 30) / 2;

    m_grdPoint.InsertColumn ( 0,  _T("No."), LVCFMT_RIGHT, iNoWidth);
    m_grdPoint.InsertColumn ( 1,  _T("X"), LVCFMT_RIGHT, iWidth);
    m_grdPoint.InsertColumn ( 2,  _T("Y"), LVCFMT_RIGHT, iWidth);

    //-----------
    //項目設定
    //-----------
    CVirtualCheckList::COL_SET ColSet;

    //No
    ColSet.iStyle = 0;
    ColSet.bDraw = false;            //ボタン描画
    ColSet.bColor  = false;           //背景色使用
    ColSet.eEditType = CVirtualCheckList::NONE;     //編集不可
    m_grdPoint.SetCol(0, ColSet);

    //X
    ColSet.eEditType = CVirtualCheckList::ALLOW;       //編集可
    m_grdPoint.SetCol(1, ColSet);

    //Y
    ColSet.eEditType = CVirtualCheckList::ALLOW;       //編集可
    m_grdPoint.SetCol(2, ColSet);

    //コールバック関数設定
    m_grdPoint.SetFunc(Callbackfunc, CallbackChek, this);
    m_grdPoint.SetChgFunc(CallbackChg);

    
    //仮想リストの行数を設定する
    m_grdPoint.SetItemCount( (int)m_lstData.size());

    return TRUE;
}

/**
 *  @brief  OKボタン押下
 *  @param  なし
 *  @retval なし     
 *  @note   
 */
void CPointListDlg::OnOK()
{
    // TODO: ここに特定なコードを追加するか、もしくは基本クラスを呼び出してください。

    CDialog::OnOK();
}


/**
 * @brief   描画コールバック
 * @param   [in]  pParent 呼び出し元         
 * @param   [in]  nRow    データを取得する行         
 * @param   [in]  nCol    データを取得する列        
 * @param   [out] pData   文字列        
 * @param   [out] pbCheck チェックボックス         
 * @retval  なし
 * @note	仮想リスト用表示関数
 */
void __cdecl CPointListDlg::Callbackfunc(LPVOID pParent, int nRow, int nCol, _TCHAR* pData, bool* pbCheck)
{
    CPointListDlg* pDlg = reinterpret_cast<CPointListDlg*>(pParent);

    //描画処理
    pDlg->SetText( nRow, nCol, pData,  pbCheck);
}


/**
 * @brief   チェックボックスコールバック
 * @param   [in]  nRow    選択時の行         
 * @param   [in]  nCol    選択時の列        
 * @retval  なし
 * @note	仮想リストボタンチェック時に呼び出し
 */
void __cdecl CPointListDlg::CallbackChek(LPVOID pParent, int nRow, int nCol)
{
    //今回は処理なし
}

/**
 * @brief   描画処理
 * @param   [in]  nRow    データを取得する行         
 * @param   [in]  nCol    データを取得する列        
 * @param   [out] pData   文字列        
 * @param   [out] pbCheck チェックボックス         
 * @retval  なし
 * @note	
 */
void CPointListDlg::SetText( int nRow, int nCol, _TCHAR* pData, bool* pbCheck)
{
    CString                 sItem;
    if ( nRow >= (int)m_lstData.size())
    {
        return;
    }

    if (nCol == 0)
    {
       sItem.Format(_T("%03d"), nRow);
    }
    else if (nCol == 1)
    {
        sItem.Format(_T("%.06f"), m_lstData[nRow].dX);
    }
    else if (nCol == 2)
    {
        sItem.Format(_T("%.06f"), m_lstData[nRow].dY);
    }
    lstrcpy(pData, sItem);
}

/**
 * @brief    変更前コールバック
 * @param   [in]  pParent 呼び出し元         
 * @param   [in]  nRow    データを取得する行         
 * @param   [in]  nCol    データを取得する列        
 * @param   [in]  pData   変更データ        
 * @retval   なし
 * @note	
 */
void __cdecl CPointListDlg::CallbackChg(LPVOID pParent, 
                                        CVirtualCheckList* pCheckList,
                                        int nRow, int nCol, const _TCHAR* pData)
{
    CPointListDlg* pDlg = reinterpret_cast<CPointListDlg*>(pParent);
    pDlg->SetChg( nRow, nCol, pData);
}

/**
 * @brief    変更前処理
 * @param    [in] nRow  行
 * @param    [in] nCol  列
 * @param    [in] pData   変更文字列
 * @retval   なし
 * @note	
 */
void CPointListDlg::SetChg( int nRow, int nCol, const _TCHAR* pData)
{
    //今回はなし
}

/**
 * @brief    ラベル編集終了
 * @param    [in]  pNMHDR 
 * @param    [out] pResult
 * @retval   なし
 * @note	
 */
void CPointListDlg::OnLvnEndlabeleditGrdPoint(NMHDR *pNMHDR, LRESULT *pResult)
{
    NMLVDISPINFO *pDispInfo = reinterpret_cast<NMLVDISPINFO*>(pNMHDR);

    *pResult = 0;
    if(pDispInfo->item.pszText == NULL)
    {
        return;
    }

    int nRow = pDispInfo->item.iItem;
    int nCol = pDispInfo->item.iSubItem;

    if(nRow >= (int)m_lstData.size())
    {
        return;
    }

    double dVal;
    dVal = _tstof (pDispInfo->item.pszText);

    if (nCol == 1)
    {
        m_lstData[nRow].dX = dVal;
    }
    else if(nCol == 2)
    {
        m_lstData[nRow].dY = dVal;
    }
}
