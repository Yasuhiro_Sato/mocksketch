/**
 * @brief			CPointListDlgインクルードファイル
 * @file			PointListDlg.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

#pragma once

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "afxcmn.h"
#include "Utility/VirtualTreeCtrl/VirtualCheckList.h"
#include "DrawingObject/Primitive/POINT2D.h"
#include "Resource.h"
//#include "Resource2.h"


/**
 * @class   CPointListDlg
 * @brief                        
 */
class CPointListDlg : public CDialog
{
	DECLARE_DYNAMIC(CPointListDlg)

public:
	CPointListDlg(CWnd* pParent = NULL);   // 標準コンストラクタ
	virtual ~CPointListDlg();

// ダイアログ データ
	enum { IDD = IDD_DLG_POINTLIST };

protected:
friend CPointListDlg;

    //!< 描画コールバック
    static void __cdecl Callbackfunc(LPVOID pParent, int nRow, int nCol, _TCHAR* pData, bool* pbCheck);

    //!< チェックコールバック
    static void __cdecl CallbackChek(LPVOID pParent, int nRow, int nCol);

    //!< 描画処理
    virtual void SetText( int nRow, int nCol, _TCHAR* pData, bool* pbCheck);

    //!< 編集前コールバック
    static void __cdecl CallbackChg(LPVOID pParent, CVirtualCheckList* pCheckList,
                                       int nRow, int nCol, const _TCHAR* pData);

    //!< 編集前処理
    void SetChg( int nRow, int nCol, const _TCHAR* pData);

    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート

	DECLARE_MESSAGE_MAP()

public:
    CVirtualCheckList     m_grdPoint;
    std::vector<POINT2D>  m_lstData;

    virtual BOOL OnInitDialog();
protected:
    virtual void OnOK();
public:
    afx_msg void OnLvnEndlabeleditGrdPoint(NMHDR *pNMHDR, LRESULT *pResult);
};
