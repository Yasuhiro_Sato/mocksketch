/**
 * @brief			ScriptAny�����t�@�C��
 * @file			ScriptAny.cpp
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:36
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include <assert.h>
#include <sstream>
#include "ScriptBase.h"
#include "ScriptAny.h"
#include "Utility/CUtility.h"
#include "DrawingObject/Primitive/SPLINE2D.H"
#include "Utility/CStdPropertyTree.h"

/*---------------------------------------------------*/
/*  MACROS                                           */
/*---------------------------------------------------*/
using namespace std;
#ifdef _DEBUG
#undef new 
#define new DEBUG_NEW
#endif 

bool CALLBACK ConvAnyType( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("Any");
    const CAny* pAny = reinterpret_cast<const CAny*>(pValAddress);

    StdStringStream strmVal; 
    strmVal << _T("{");

    VAL_DATA valData;
    valData.strName     = _T("Data");

    valData.strTypeName = pAny->GetAsTypeName();

    valData.strVal = CUtil::StrFormat(_T("%s"), pAny->ToString().c_str());
    pValData->lstVal.push_back(valData);
    strmVal << valData.strVal;

    strmVal << _T("}");

    pValData->strVal = strmVal.str();
    return true;
}


//PropertyItem�^�o�^
int RegisterAnyType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
    int iRet = engine->RegisterObjectType( "Any", 0, asOBJ_REF); assert( iRet >= 0 );  

    //�E�H�b�`���X�g�\���p�o�^
    mapIdFunc[iRet] = ConvAnyType;
    return iRet;
}

//PropertyItem�o�^
int RegisterAny(asIScriptEngine *engine)
{
    RegsterRef<CAny>(engine, "Any");

    int r;
    //Any& operator = (const Any & m);
    r = engine->RegisterObjectMethod("Any",
        "Any &opAssign(const Any &in)"   ,
        asMETHODPR(CAny, operator=, (const CAny &), 
        CAny&), asCALL_THISCALL); assert( r >= 0 );
     
    r = engine->RegisterObjectMethod("Any", 
        "bool ToBool()", 
        asMETHOD(CAny,ToBool), 
        asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("Any", 
        "int ToInt()", 
        asMETHOD(CAny,ToInt), 
        asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("Any", 
        "double ToDouble()", 
        asMETHOD(CAny,ToDouble), 
        asCALL_THISCALL); assert( r >= 0 );

    /* color�̈����Ɋւ��ĕۗ�
    r = engine->RegisterObjectMethod("Any", 
        "uint ToColor()", 
        asMETHOD(CAny,ToColor), 
        asCALL_THISCALL); assert( r >= 0 );
    */

    r = engine->RegisterObjectMethod("Any", 
        "POINT2D& ToPOINT2D()", 
        asMETHOD(CAny,ToPOINT2D), 
        asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("Any", 
        "RECT2D& ToRECT2D()", 
        asMETHOD(CAny,ToRECT2D), 
        asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("Any", 
        "string ToString()", 
        asMETHOD(CAny,ToString), 
        asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("Any", 
        "POINT2D_VECTOR ToPointList()", 
        asMETHOD(CAny,ToPointList), 
        asCALL_THISCALL); assert( r >= 0 );

    //SetVal
    r = engine->RegisterObjectMethod("Any", 
        "void SetVal(bool)",
        asMETHODPR(CAny, SetVal,(bool), void),
        asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("Any", 
        "void SetVal(int)",
        asMETHODPR(CAny, SetVal,(int), void),
        asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("Any", 
        "void SetVal(double)",
        asMETHODPR(CAny, SetVal,(double), void),
        asCALL_THISCALL); assert( r >= 0 );

    /*
    r = engine->RegisterObjectMethod("Any", 
        "void SetVal(uint)",
        asMETHODPR(CAny, SetVal,(uint), SetVal),
        asCALL_THISCALL); assert( r >= 0 );
    */

    r = engine->RegisterObjectMethod("Any", 
        "void SetVal(string)",
        asMETHODPR(CAny, SetVal,(StdString), void),
        asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("Any", 
        "void SetVal(POINT2D& in)",
        asMETHODPR(CAny, SetVal,(POINT2D), void),
        asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("Any", 
        "void SetVal(RECT2D& in)",
        asMETHODPR(CAny, SetVal,(RECT2D), void),
        asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("Any", 
        "void SetVal(POINT2D_VECTOR)",
        asMETHODPR(CAny, SetVal,(std::vector<POINT2D>), void),
        asCALL_THISCALL); assert( r >= 0 );


    return 0;
}

END_AS_NAMESPACE
