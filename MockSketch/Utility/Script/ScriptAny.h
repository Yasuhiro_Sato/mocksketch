/**
 * @brief			ScriptAny
 * @file			ScriptAny.h
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:37
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#ifndef SCRIPT_ANY_H
#define SCRIPT_ANY_H

BEGIN_AS_NAMESPACE

#include <angelscript.h>
#include <string>
#include "VAL_DATA.h"

int RegisterAnyType (asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc);
int RegisterAny (asIScriptEngine *engine);


END_AS_NAMESPACE

#endif
