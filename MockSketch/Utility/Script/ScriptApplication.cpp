/**
 * @brief			ScriptApplication実装ファイル
 * @file			ScriptApplication.cpp
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:36
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include <assert.h>
#include <sstream>
#include "ScriptBase.h"
#include "ScriptApplication.h"
#include "Utility/CUtility.h"
#include "DrawingObject/Primitive/SPLINE2D.H"

/*---------------------------------------------------*/
/*  MACROS                                           */
/*---------------------------------------------------*/
using namespace std;
#ifdef _DEBUG
#undef new 
#define new DEBUG_NEW
#endif
BEGIN_AS_NAMESPACE
extern bool CALLBACK ConvPOINT2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);

// Applicationクラス表示
bool CALLBACK ConvApplication( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("Application");
    //CPartsDef* pObj = reinterpret_cast<CPartsDef*>(pValAddress);
    pValData->strVal = _T("---");

    return true;
}


//Application型登録
int RegisterApplicationType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
    int iRet = engine->RegisterObjectType( "Application", 0, asOBJ_REF); assert( iRet >= 0 );  

    //ウォッチリスト表示用登録
    mapIdFunc[iRet] = ConvApplication;
    return iRet;
}

// Applicationクラス登録
int RegisterApplication(asIScriptEngine *engine)
{
	//int r;
    return 0;
}


/*
    C側
        class Obj
        {
        public:
             Obj();
            ~Obj();

            void SetVal(int val){iVal = val:}
            int  GetVal(){return iVal:}

        protected:
            int iVal;
        };


       map<string, Obj*>  mapObj;

       Obj   Obj1;
       Obj   Obj2;
       Obj   Obj3;
 
       mapObj["Obj1"]  = &Obj1;
       mapObj["Obj2"]  = &Obj2;
       mapObj["Obj3"]  = &Obj3;



    //方法１



    AS側

    
    
    //スクリプト
    {
        class 
        {
            


        }


        M

        MATRIX
        LINE2D

        Line2D ;

        Module.Get("OBJ1")

        Module.Io.Get("OBJ1")

        
        //型
        Line2D
        Circle2D
        Point2D
            ・
            ・
            
            ・

        GetPartsCtrl();
        GetPartsCtrl();


        DrawingObject
            DrawingLine 
            DrawingCircle
                ・
                ・
                ・

        Module.GetLine("OBJ1")
        Module.GetObject("OBJ1")


        Line.Get("OBJ1").Move(10,10);




    }












*/
