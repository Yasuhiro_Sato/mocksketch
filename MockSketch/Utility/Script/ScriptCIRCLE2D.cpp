/**
 * @brief			ScriptCIRCLE2D実装ファイル
 * @file			ScriptBase.cpp
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:36
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include <assert.h>
#include <sstream>
#include "ScriptBase.h"
#include "ScriptCIRCLE2D.h"
#include "Utility/CUtility.h"
#include "DrawingObject/Primitive/SPLINE2D.H"
#include "DrawingObject/Primitive/CIRCLE2D.h"

/*---------------------------------------------------*/
/*  MACROS                                           */
/*---------------------------------------------------*/
using namespace std;
#ifdef _DEBUG
#undef new 
#define new DEBUG_NEW
#endif
extern bool CALLBACK ConvPOINT2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);

bool CALLBACK  ConvCIRCLE2D( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("CIRCLE2D");
    const CIRCLE2D* pCircle2D = reinterpret_cast<const CIRCLE2D*>(pValAddress);
    StdStringStream strmVal; 

    strmVal << _T("{");
    
    VAL_DATA valCenter;
    valCenter.strName     = _T("Center");
    ConvPOINT2D( &valCenter, &pCircle2D->ptCenter,pContext);
    pValData->lstVal.push_back(valCenter);
    strmVal << valCenter.strVal;

    strmVal << valCenter.strVal, _T(",");
 
    VAL_DATA valRad;
    valRad.strName     = _T("Radius");
    valRad.strTypeName = _T("double");
    valRad.strVal = CUtil::StrFormat(_T("%f"), pCircle2D->dRad);
    pValData->lstVal.push_back(valRad);
    strmVal << valRad.strVal, _T(",");

    VAL_DATA valStart;
    valStart.strName     = _T("StartAngle");
    valStart.strTypeName = _T("double");
    valStart.strVal = CUtil::StrFormat(_T("%f"), pCircle2D->GetStart());
    pValData->lstVal.push_back(valStart);
    strmVal << valStart.strVal, _T(",");

    VAL_DATA valEnd;
    valEnd.strName     = _T("EndAngle");
    valEnd.strTypeName = _T("double");
    valEnd.strVal = CUtil::StrFormat(_T("%f"), pCircle2D->GetEnd());
    pValData->lstVal.push_back(valEnd);
    strmVal << valStart.strVal;

    strmVal << _T("}");
    pValData->strVal = strmVal.str();
    return true;
}

//!< コンストラクタ (中心と半径)
static CIRCLE2D* FactoryCIRCLE2D_Center_Rad(const POINT2D& pt, 
                                            double dRad)
{                                                                
    CIRCLE2D *pRet;                                        
    pRet = new CIRCLE2D( pt, dRad);
    CAsRefCtrl::GetInstance()->AddAsObj(pRet);
	return pRet;
}

//!< コンストラクタ (3点)
static CIRCLE2D* FactoryCIRCLE2D_3Point(const POINT2D& pt1,
                                        const POINT2D& pt2,
                                        const POINT2D& pt3)
{                                                                
    CIRCLE2D *pRet;                                        
    pRet = new CIRCLE2D( pt1, pt2, pt3);
    CAsRefCtrl::GetInstance()->AddAsObj(pRet);
	return pRet;
}

//!< コンストラクタ (中心と接する直線)
static CIRCLE2D* FactoryCIRCLE2D_Point_Line (const POINT2D& pt,
                                             const LINE2D& line)
{                                                                
    CIRCLE2D *pRet;                                        
    pRet = new CIRCLE2D( pt, line);
    CAsRefCtrl::GetInstance()->AddAsObj(pRet);
	return pRet;
}

//!< コンストラクタ (中心と接する円)
static CIRCLE2D* FactoryCIRCLE2D_Point_Circle (const POINT2D& pt,
                                               const CIRCLE2D& circle)
{                                                                
    CIRCLE2D *pRet;                                        
    pRet = new CIRCLE2D( pt, circle);
    CAsRefCtrl::GetInstance()->AddAsObj(pRet);
	return pRet;
}

//!< コンストラクタ (3直線)
static CIRCLE2D* FactoryCIRCLE2D_3Line (const LINE2D& line1,
                                        const LINE2D& line2,
                                        const LINE2D& line3)
{                                                                
    CIRCLE2D *pRet;                                        
    pRet = new CIRCLE2D( line1, line2, line3);
    CAsRefCtrl::GetInstance()->AddAsObj(pRet);
	return pRet;
}


//!< 円との交点
static bool CIRCLE2D_IntersectLine( std::vector<POINT2D>& list,   
                                const LINE2D& line,  bool bOnLine, CIRCLE2D& obj)
{
    //TODO:テスト
    return obj.Intersect(line, &list, bOnLine);
}

//!< 円との交点
static bool CIRCLE2D_IntersectCircle( std::vector<POINT2D>& list,   
                                const CIRCLE2D& circle,  bool bOnLine, CIRCLE2D& obj)
{
    //TODO:テスト
    return obj.Intersect(circle, &list, bOnLine);
}


//CIRCLE2D型登録
int RegisterCIRCLE2DType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
    int iRet = engine->RegisterObjectType( "CIRCLE2D", 0, asOBJ_REF); assert( iRet >= 0 );  

    //ウォッチリスト表示用登録
    mapIdFunc[iRet] = ConvCIRCLE2D;
    return iRet;
}

//CIRCLE2D登録
int RegisterCIRCLE2D(asIScriptEngine *engine)
{
    RegsterRef<CIRCLE2D>(engine, "CIRCLE2D");
    
    int r;
    //!< 中心と半径による生成
    r = engine->RegisterObjectBehaviour("CIRCLE2D", asBEHAVE_FACTORY, 
        "CIRCLE2D @f(const POINT2D& in, double)",
        asFUNCTION( FactoryCIRCLE2D_Center_Rad), asCALL_CDECL); 
        STD_ASSERT( r >= 0 );

    //!< ３点による生成
    r = engine->RegisterObjectBehaviour("CIRCLE2D", asBEHAVE_FACTORY, 
        "CIRCLE2D @f(const POINT2D& in, const POINT2D& in, const POINT2D& in)",
        asFUNCTION( FactoryCIRCLE2D_Center_Rad), asCALL_CDECL); 
        STD_ASSERT( r >= 0 );

    //!< 中心と接する直線による生成
    r = engine->RegisterObjectBehaviour("CIRCLE2D", asBEHAVE_FACTORY, 
        "CIRCLE2D @f(const POINT2D& in, const LINE2D& in)",
        asFUNCTION( FactoryCIRCLE2D_Point_Line), asCALL_CDECL); 
        STD_ASSERT( r >= 0 );

    //!< 中心と接する円による生成
    r = engine->RegisterObjectBehaviour("CIRCLE2D", asBEHAVE_FACTORY, 
        "CIRCLE2D @f(const POINT2D& in, const CIRCLE2D& in)",
        asFUNCTION( FactoryCIRCLE2D_Point_Circle), asCALL_CDECL); 
        STD_ASSERT( r >= 0 );

    //!< 3直線による生成
    r = engine->RegisterObjectBehaviour("CIRCLE2D", asBEHAVE_FACTORY, 
        "CIRCLE2D @f(const LINE2D& in, const LINE2D& in, const LINE2D& in)",
        asFUNCTION( FactoryCIRCLE2D_3Line), asCALL_CDECL); 
        STD_ASSERT( r >= 0 );

    //!< 近接点
    r = engine->RegisterObjectMethod("CIRCLE2D", 
        "void NearPoint(const POINT2D& in, bool)",
        asMETHOD(CIRCLE2D, NearPoint), asCALL_THISCALL);
        STD_ASSERT( r >= 0 );

    r = engine->RegisterObjectMethod("CIRCLE2D", 
        "bool Intersect(const LINE2D& in, bool)",
        asMETHOD(CIRCLE2D, NearPoint), asCALL_THISCALL);
        STD_ASSERT( r >= 0 );

    r = engine->RegisterObjectMethod("CIRCLE2D",
        "bool CalcIntersection(POINT2D_VECTOR& out,const LINE2D& in, bool)",
        asFUNCTION(CIRCLE2D_IntersectLine), asCALL_CDECL_OBJLAST);
        COMMENT_ASSERT( r >= 0, _T("CalcIntersection") );

    r = engine->RegisterObjectMethod("CIRCLE2D",
        "bool CalcIntersection(POINT2D_VECTOR& out,const CIRCLE2D& in, bool)",
        asFUNCTION(CIRCLE2D_IntersectCircle), asCALL_CDECL_OBJLAST);
        COMMENT_ASSERT( r >= 0, _T("CalcIntersection") );
    
    //中心点取得
    r = engine->RegisterObjectMethod("CIRCLE2D", 
        "POINT2D& GetCenter()",
        asMETHOD(CIRCLE2D, GetCenter), asCALL_THISCALL);
        STD_ASSERT( r >= 0 );

    //中心点設定
    r = engine->RegisterObjectMethod("CIRCLE2D", 
        "void SetCenter(const POINT2D& in)",
        asMETHOD(CIRCLE2D, GetCenter), asCALL_THISCALL);
        STD_ASSERT( r >= 0 );

    //半径設定
    r = engine->RegisterObjectMethod("CIRCLE2D", 
        "POINT2D& GetRadius()",
        asMETHOD(CIRCLE2D, GetRadius), asCALL_THISCALL);
        STD_ASSERT( r >= 0 );

    //半径取得
    r = engine->RegisterObjectMethod("CIRCLE2D", 
        "void SetRadius(double)",
        asMETHOD(CIRCLE2D, SetRadius), asCALL_THISCALL);
        STD_ASSERT( r >= 0 );
#ifdef POINT2D_REF
    //開始点取得
    r = engine->RegisterObjectMethod("CIRCLE2D", 
        "void GetStartPoint(POINT2D@)",
        asMETHOD(CIRCLE2D, GetStartPoint), asCALL_THISCALL);
        STD_ASSERT( r >= 0 );

    //終了点取得
    r = engine->RegisterObjectMethod("CIRCLE2D", 
        "void GetEndPoint(POINT2D@)",
        asMETHOD(CIRCLE2D, GetEndPoint), asCALL_THISCALL);
        STD_ASSERT( r >= 0 );
#else
    //開始点取得
    r = engine->RegisterObjectMethod("CIRCLE2D", 
        "POINT2D GetStartPoint()",
        asMETHOD(CIRCLE2D, GetStartPointVal), asCALL_THISCALL);
        STD_ASSERT( r >= 0 );

    //終了点取得
    r = engine->RegisterObjectMethod("CIRCLE2D", 
        "POINT2D GetEndPoint()",
        asMETHOD(CIRCLE2D, GetEndPointVal), asCALL_THISCALL);
        STD_ASSERT( r >= 0 );

#endif
    //CIRCLE2D& operator = (const CIRCLE2D & m);
	r = engine->RegisterObjectMethod("CIRCLE2D",
        "CIRCLE2D &opAssign(const CIRCLE2D  &in)"   ,
        asMETHODPR(CIRCLE2D, operator=, (const CIRCLE2D &), 
        CIRCLE2D&), asCALL_THISCALL); assert( r >= 0 );

    //bool operator == (const CIRCLE2D & m); (!= は定義不要？）
    r = engine->RegisterObjectMethod("CIRCLE2D", 
        "bool opEquals(const CIRCLE2D &in) const"    , 
        asMETHODPR(CIRCLE2D, operator==, 
        (const CIRCLE2D &) const, bool), 
        asCALL_THISCALL); assert( r >= 0 );

    //距離取得
    r = engine->RegisterObjectMethod("CIRCLE2D", 
        "double Distance(const POINT2D& in)",
        asMETHOD(CIRCLE2D, Distance), asCALL_THISCALL);
        STD_ASSERT( r >= 0 );

    //開始角取得 
    r = engine->RegisterObjectMethod("CIRCLE2D", 
        "double GetStartAngle()",
        asMETHOD(CIRCLE2D, GetStart), asCALL_THISCALL);
        STD_ASSERT( r >= 0 );

    //終了角取得 
    r = engine->RegisterObjectMethod("CIRCLE2D", 
        "double GetEndAngle()",
        asMETHOD(CIRCLE2D, GetEnd), asCALL_THISCALL);
        STD_ASSERT( r >= 0 );

    //開始角設定
    r = engine->RegisterObjectMethod("CIRCLE2D", 
        "void SetStartAngle(double)",
        asMETHOD(CIRCLE2D, SetStart), asCALL_THISCALL);
        STD_ASSERT( r >= 0 );

    //終了角設定 
    r = engine->RegisterObjectMethod("CIRCLE2D", 
        "void SetEndAngle(double)",
        asMETHOD(CIRCLE2D, SetEnd), asCALL_THISCALL);
        STD_ASSERT( r >= 0 );

    //REGSTER_BASIC_METHOD(CIRCLE2D);
    RegsterBasicMethod<CIRCLE2D>(engine,"CIRCLE2D");


    return 0;
}
