/**
 * @brief			ScriptCIRCLE2D
 * @file			ScriptCIRCLE2D.h
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:37
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#ifndef SCRIPT_CIRCLE2D_H
#define SCRIPT_CIRCLE2D_H

#include <angelscript.h>
#include <string>
#include "VAL_DATA.h"

int RegisterCIRCLE2DType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc);
int RegisterCIRCLE2D(asIScriptEngine *engine);

#endif
