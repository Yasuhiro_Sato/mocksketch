/**
 * @brief			ScriptDrawingCircle実装ファイル
 * @file			ScriptDrawingCircle.cpp
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:36
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include <assert.h>
#include <sstream>
#include "ScriptBase.h"
#include "ScriptDrawingCircle.h"
#include "Utility/CUtility.h"
#include "DrawingObject/Primitive/SPLINE2D.H"
#include "DrawingObject/CDrawingCircle.h"


/*---------------------------------------------------*/
/*  MACROS                                           */
/*---------------------------------------------------*/
using namespace std;
#ifdef _DEBUG
#undef new 
#endif
extern bool CALLBACK ConvCIRCLE2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);


static void ConstructDrawingCircle (CDrawingCircle *thisPointer)
{
	new(thisPointer) CDrawingCircle();
}

#ifdef _DEBUG
#undef new 
#define new DEBUG_NEW
#endif
static void DestructDrawingParts(CDrawingCircle *thisPointer)
{
	thisPointer->~CDrawingCircle();
}

CDrawingCircle* CreateDrawingCircle()
{
    return new CDrawingCircle();
}

bool CALLBACK ConvDrawingCircle( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("DrawingCircle");
    const CDrawingCircle* pObj = reinterpret_cast<const CDrawingCircle*>(pValAddress);

    pValData->strVal = _T("{");


    VAL_DATA valData;

    ConvDrawingObject( &valData, pObj, pContext);
    pValData->lstVal.push_back(valData);
    pValData->strVal += valData.strVal;
    pValData->strVal += _T(",");


    //===========================
    //Circle
    const CIRCLE2D* pCircle;
    pCircle = pObj->GetCircleInstance();
    ConvCIRCLE2D( &valData, pCircle, pContext);
    pValData->lstVal.push_back(valData);
    pValData->strVal += valData.strVal;
    pValData->strVal += _T(",");

    valData.strName     = _T("LineWidth");
    valData.strTypeName = _T("int");
    valData.strVal      =  CUtil::StrFormat(_T("%d"), pObj->GetLineWidth());
    pValData->lstVal.push_back(valData);
    pValData->strVal += valData.strVal;
    pValData->strVal += _T(",");

    valData.strName     = _T("LineType");
    valData.strTypeName = _T("int");
    valData.strVal      =  CUtil::StrFormat(_T("%d"), pObj->GetLineType());
    pValData->lstVal.push_back(valData);
    pValData->strVal += valData.strVal;
    pValData->strVal += _T("}");

    return true;
}


//DrawingCircle型登録
int RegisterDrawingCircleType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
    int iRet = engine->RegisterObjectType( "DrawingCircle", 0, asOBJ_REF); assert( iRet >= 0 );  

    //ウォッチリスト表示用登録
    mapIdFunc[iRet] = ConvDrawingCircle;
    return iRet;
}

//DrawingCircle登録
int RegisterDrawingCircle(asIScriptEngine *engine)
{
    RegsterRef<CDrawingCircle>(engine, "DrawingCircle");
    RegsterBasicMethod<CDrawingCircle>(engine, "DrawingCircle");
    RegsterDrawingObject<CDrawingCircle>(engine, "DrawingCircle");
    RegisterDrawingLineCommon<CDrawingCircle>(engine, "DrawingCircle");

    //-------------------------------------------------
    int r;

    //!< 中心設定
	r = engine->RegisterObjectMethod("DrawingCircle", 
        "void SetCenter(POINT2D &in)", 
        asMETHOD(CDrawingCircle, SetCenter),
        asCALL_THISCALL); assert( r >= 0 );

    //!< 中心取得
	r = engine->RegisterObjectMethod("DrawingCircle", 
        "POINT2D& GetCenter()", 
        asMETHOD(CDrawingCircle, GetCenter),
        asCALL_THISCALL); assert( r >= 0 );

   //!< 半径設定
	r = engine->RegisterObjectMethod("DrawingCircle", 
        "void SetRadius(double)", 
        asMETHOD(CDrawingCircle, SetRadius),
        asCALL_THISCALL); assert( r >= 0 );

    //!< 半径取得
	r = engine->RegisterObjectMethod("DrawingCircle", 
        "double GetRadius()", 
        asMETHOD(CDrawingCircle, GetRadius),
        asCALL_THISCALL); assert( r >= 0 );

   //!< 開始角設定
	r = engine->RegisterObjectMethod("DrawingCircle", 
        "void SetStart(double)", 
        asMETHOD(CDrawingCircle, SetStart),
        asCALL_THISCALL); assert( r >= 0 );

    //!< 開始角取得
	r = engine->RegisterObjectMethod("DrawingCircle", 
        "double GetStart()", 
        asMETHOD(CDrawingCircle, GetStart),
        asCALL_THISCALL); assert( r >= 0 );

    //!< 終了角設定
	r = engine->RegisterObjectMethod("DrawingCircle", 
        "void SetEnd(double)", 
        asMETHOD(CDrawingCircle, SetEnd),
        asCALL_THISCALL); assert( r >= 0 );

    //!< 終了角取得
	r = engine->RegisterObjectMethod("DrawingCircle", 
        "double GetEnd()", 
        asMETHOD(CDrawingCircle, GetEnd),
        asCALL_THISCALL); assert( r >= 0 );


    //!< 円取得
	r = engine->RegisterObjectMethod("DrawingCircle", 
        "CIRCLE2D@ GetCircleInstance()", 
        asMETHODPR(CDrawingCircle, GetCircleInstance,(), CIRCLE2D*),
        asCALL_THISCALL); assert( r >= 0 );


    // キャスト
    //DrawingLine -> DrawingObject へのキャストは 自動
    r = engine->RegisterObjectBehaviour("DrawingCircle", asBEHAVE_IMPLICIT_REF_CAST, 
        "DrawingObject@ f()", 
        asFUNCTION((ASRefCast<CDrawingObject, CDrawingCircle>)), 
        asCALL_CDECL_OBJLAST);
    assert( r>=0 );

    //DrawingObject->  DrawingLine へのキャストは キャスト演算子が必要
    r = engine->RegisterObjectBehaviour("DrawingObject", asBEHAVE_REF_CAST , 
        "DrawingCircle@ f()", 
        asFUNCTION((ASRefCast<CDrawingCircle, CDrawingObject>)), 
        asCALL_CDECL_OBJLAST);
    assert( r>=0 );



    return 0;
}
