/**
 * @brief			ScriptDrawingCompositionLine実装ファイル
 * @file			ScriptDrawingCompositionLine.cpp
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:36
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include <assert.h>
#include <sstream>
#include "ScriptBase.h"
#include "ScriptDrawingCompositionLine.h"
#include "Utility/CUtility.h"
#include "DrawingObject/Primitive/SPLINE2D.H"
#include "DrawingObject/CDrawingCompositionLine.h"


/*---------------------------------------------------*/
/*  MACROS                                           */
/*---------------------------------------------------*/
using namespace std;
#ifdef _DEBUG
#undef new 
#endif
extern bool CALLBACK ConvPOINT2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);
extern bool CALLBACK ConvLINE2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);


static void ConstructDrawingCompositionLine (CDrawingCompositionLine *thisPointer)
{
	new(thisPointer) CDrawingCompositionLine();
}

#ifdef _DEBUG
#undef new 
#define new DEBUG_NEW
#endif
static void DestructDrawingParts(CDrawingCompositionLine *thisPointer)
{
	thisPointer->~CDrawingCompositionLine();
}

CDrawingCompositionLine* CreateDrawingCompositionLine()
{
    return new CDrawingCompositionLine();
}

bool CALLBACK ConvDrawingCompositionLine( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("DrawingCompositionLine");
    const CDrawingCompositionLine* pObj = reinterpret_cast<const CDrawingCompositionLine*>(pValAddress);

    pValData->strVal = _T("{");

    VAL_DATA valData;
    ConvDrawingObject( &valData, pObj, pContext);
    pValData->lstVal.push_back(valData);
    pValData->strVal += valData.strVal;
    pValData->strVal += _T(",");

    //===========================
    //Text



    pValData->strVal += _T("}");

    return true;
}


//DrawingCompositionLine型登録
int RegisterDrawingCompositionLineType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
    int iRet = engine->RegisterObjectType( "DrawingCompositionLine", 0, asOBJ_REF); assert( iRet >= 0 );  

    //ウォッチリスト表示用登録
    mapIdFunc[iRet] = ConvDrawingCompositionLine;
    return iRet;
}

//DrawingCompositionLine登録
int RegisterDrawingCompositionLine(asIScriptEngine *engine)
{
    RegsterRef<CDrawingCompositionLine>(engine, "DrawingCompositionLine");
    RegsterBasicMethod<CDrawingCompositionLine>(engine, "DrawingCompositionLine");
    RegsterDrawingObject<CDrawingCompositionLine>(engine, "DrawingCompositionLine");
    RegisterDrawingLineCommon<CDrawingCompositionLine>(engine, "DrawingCompositionLine");


    //-------------------------------------------------
    int r;

	r = engine->RegisterObjectMethod("DrawingCompositionLine", 
        "DrawingCompositionLine &opAssign(const DrawingCompositionLine &in)"   ,
        asMETHODPR(CDrawingCompositionLine, operator=, (const CDrawingCompositionLine &),
        CDrawingCompositionLine&), asCALL_THISCALL); assert( r >= 0 );


    //!< 内部データ移動
	r = engine->RegisterObjectMethod("DrawingCompositionLine", 
        "void MoveInnerPosition(const POINT2D& in)", 
        asMETHOD(CDrawingCompositionLine, MoveInnerPosition),
        asCALL_THISCALL); assert( r >= 0 );

    //!< 閉じているか
	r = engine->RegisterObjectMethod("DrawingCompositionLine", 
        "bool IsClose() const", 
        asMETHOD(CDrawingCompositionLine, IsClose),
        asCALL_THISCALL); assert( r >= 0 );

    //!< オブジェクト数取得
	r = engine->RegisterObjectMethod("DrawingCompositionLine", 
        "int GetNumberOfObjects() const", 
        asMETHOD(CDrawingCompositionLine, GetNumberOfObjects),
        asCALL_THISCALL); assert( r >= 0 );

    //!< オブジェクト取得
	r = engine->RegisterObjectMethod("DrawingCompositionLine", 
        "const DrawingObject@ GetObject(int) const", 
        asMETHOD(CDrawingCompositionLine, GetObject),
        asCALL_THISCALL); assert( r >= 0 );

    //!<  ポリゴン生成
	r = engine->RegisterObjectMethod("DrawingCompositionLine", 
        "void CreatePolygon()", 
        asMETHOD(CDrawingCompositionLine, CreatePolygon),
        asCALL_THISCALL); assert( r >= 0 );

    //!<  ポリゴン数取得
	r = engine->RegisterObjectMethod("DrawingCompositionLine", 
        "int GetNumberOfPolygon() const", 
        asMETHOD(CDrawingCompositionLine, GetNumberOfPolygon),
        asCALL_THISCALL); assert( r >= 0 );

    //!<  ポリゴン取得
	r = engine->RegisterObjectMethod("DrawingCompositionLine", 
        "bool GetPolygonVertex(int, POINT2D& out, POINT2D& out, POINT2D& out) const", 
        asMETHOD(CDrawingCompositionLine, GetPolygonVertex),
        asCALL_THISCALL); assert( r >= 0 );

    //!< 描画用（画面座標変換用）マトリクス取得
	r = engine->RegisterObjectMethod("DrawingCompositionLine", 
        "MAT2D@ GetDrawingMatrix() const", 
         asMETHOD(CDrawingCompositionLine,GetDrawingMatrix), 
        asCALL_THISCALL); assert( r >= 0 );

    // キャスト
    //DrawingLine -> DrawingObject へのキャストは 自動
    r = engine->RegisterObjectBehaviour("DrawingCompositionLine", asBEHAVE_IMPLICIT_REF_CAST, 
        "DrawingObject@ f()", 
        asFUNCTION((ASRefCast<CDrawingObject, CDrawingCompositionLine>)), 
        asCALL_CDECL_OBJLAST);
    assert( r>=0 );

    //DrawingObject->  DrawingLine へのキャストは キャスト演算子が必要
    r = engine->RegisterObjectBehaviour("DrawingObject", asBEHAVE_REF_CAST , 
        "DrawingCompositionLine@ f()", 
        asFUNCTION((ASRefCast<CDrawingCompositionLine, CDrawingObject>)), 
        asCALL_CDECL_OBJLAST);
    assert( r>=0 );



    return 0;
}
