/**
 * @brief			ScriptDrawingConnectionLine実装ファイル
 * @file			ScriptDrawingConnectionLine.cpp
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:36
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include <assert.h>
#include <sstream>
#include "ScriptBase.h"
#include "ScriptDrawingConnectionLine.h"
#include "Utility/CUtility.h"
#include "DrawingObject/Primitive/SPLINE2D.H"
#include "DrawingObject/CDrawingConnectionLine.h"


/*---------------------------------------------------*/
/*  MACROS                                           */
/*---------------------------------------------------*/
using namespace std;
#ifdef _DEBUG
#undef new 
#endif
extern bool CALLBACK ConvPOINT2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);
extern bool CALLBACK ConvLINE2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);


static void ConstructDrawingConnectionLine (CDrawingConnectionLine *thisPointer)
{
	new(thisPointer) CDrawingConnectionLine();
}

#ifdef _DEBUG
#undef new 
#define new DEBUG_NEW
#endif
static void DestructDrawingConnectionLine(CDrawingConnectionLine *thisPointer)
{
	thisPointer->~CDrawingConnectionLine();
}

CDrawingConnectionLine* CreateDrawingConnectionLine()
{
    return new CDrawingConnectionLine();
}

bool CALLBACK ConvDrawingConnectionLine( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("DrawingConnectionLine");
    const CDrawingConnectionLine* pObj = reinterpret_cast<const CDrawingConnectionLine*>(pValAddress);

    pValData->strVal = _T("{");

    //===========================
    // DrawingObject共通
    //===========================
    VAL_DATA valData;
    ConvDrawingObject( &valData, pObj, pContext);
    pValData->lstVal.push_back(valData);
    pValData->strVal += valData.strVal;
    pValData->strVal += _T(",");
    //===========================
    //Text



    pValData->strVal += _T("}");

    return true;
}


//DrawingConnectionLine型登録
int RegisterDrawingConnectionLineType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
    int iRet = engine->RegisterObjectType( "DrawingConnectionLine", 0, asOBJ_REF); assert( iRet >= 0 );  

    //ウォッチリスト表示用登録
    mapIdFunc[iRet] = ConvDrawingConnectionLine;
    return iRet;
}

//DrawingConnectionLine登録
int RegisterDrawingConnectionLine(asIScriptEngine *engine)
{
    RegsterRef<CDrawingConnectionLine>(engine, "DrawingConnectionLine");
    RegsterBasicMethod<CDrawingConnectionLine>(engine, "DrawingConnectionLine");
    RegsterDrawingObject<CDrawingConnectionLine>(engine, "DrawingConnectionLine");


    //-------------------------------------------------
    int r;


    // キャスト
    //DrawingLine -> DrawingObject へのキャストは 自動
    r = engine->RegisterObjectBehaviour("DrawingConnectionLine", asBEHAVE_IMPLICIT_REF_CAST, 
        "DrawingObject@ f()", 
        asFUNCTION((ASRefCast<CDrawingObject, CDrawingConnectionLine>)), 
        asCALL_CDECL_OBJLAST);
    assert( r>=0 );

    //DrawingObject->  DrawingLine へのキャストは キャスト演算子が必要
    r = engine->RegisterObjectBehaviour("DrawingObject", asBEHAVE_REF_CAST , 
        "DrawingConnectionLine@ f()", 
        asFUNCTION((ASRefCast<CDrawingConnectionLine, CDrawingObject>)), 
        asCALL_CDECL_OBJLAST);
    assert( r>=0 );



    return 0;
}
