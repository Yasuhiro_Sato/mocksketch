/**
 * @brief			ScriptDrawingDimA実装ファイル
 * @file			ScriptDrawingDimA.cpp
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:36
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include <assert.h>
#include <sstream>
#include "ScriptBase.h"
#include "ScriptDrawingDimA.h"
#include "Utility/CUtility.h"
#include "DrawingObject/Primitive/SPLINE2D.H"
#include "DrawingObject/CDrawingDimA.h"


/*---------------------------------------------------*/
/*  MACROS                                           */
/*---------------------------------------------------*/
using namespace std;
#ifdef _DEBUG
#undef new 
#endif
extern bool CALLBACK ConvPOINT2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);
extern bool CALLBACK ConvLINE2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);


static void ConstructDrawingDimA (CDrawingDimA *thisPointer)
{
	new(thisPointer) CDrawingDimA();
}

#ifdef _DEBUG
#undef new 
#define new DEBUG_NEW
#endif
static void DestructDrawingDimA(CDrawingDimA *thisPointer)
{
	thisPointer->~CDrawingDimA();
}

CDrawingDimA* CreateDrawingDimA()
{
    return new CDrawingDimA();
}

bool CALLBACK ConvDrawingDimA( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("DrawingDimA");
    const CDrawingDimA* pObj = reinterpret_cast<const CDrawingDimA*>(pValAddress);

    pValData->strVal = _T("{");

    VAL_DATA valData;
    ConvDrawingObject( &valData, pObj, pContext);
    pValData->lstVal.push_back(valData);
    pValData->strVal += valData.strVal;
    pValData->strVal += _T(",");

    //===========================
    //Text



    pValData->strVal += _T("}");

    return true;
}


//DrawingDimA型登録
int RegisterDrawingDimAType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
    int iRet = engine->RegisterObjectType( "DrawingDimA", 0, asOBJ_REF); assert( iRet >= 0 );  

    //ウォッチリスト表示用登録
    mapIdFunc[iRet] = ConvDrawingDimA;
    return iRet;
}

//DrawingDimA登録
int RegisterDrawingDimA(asIScriptEngine *engine)
{
    RegsterRef<CDrawingDimA>(engine, "DrawingDimA");
    RegsterBasicMethod<CDrawingDimA>(engine, "DrawingDimA");
    RegsterDrawingObject<CDrawingDimA>(engine, "DrawingDimA");


    //-------------------------------------------------
    int r;


    // キャスト
    //DrawingLine -> DrawingObject へのキャストは 自動
    r = engine->RegisterObjectBehaviour("DrawingDimA", asBEHAVE_IMPLICIT_REF_CAST, 
        "DrawingObject@ f()", 
        asFUNCTION((ASRefCast<CDrawingObject, CDrawingDimA>)), 
        asCALL_CDECL_OBJLAST);
    assert( r>=0 );

    //DrawingObject->  DrawingLine へのキャストは キャスト演算子が必要
    r = engine->RegisterObjectBehaviour("DrawingObject", asBEHAVE_REF_CAST , 
        "DrawingDimA@ f()", 
        asFUNCTION((ASRefCast<CDrawingDimA, CDrawingObject>)), 
        asCALL_CDECL_OBJLAST);
    assert( r>=0 );



    return 0;
}
