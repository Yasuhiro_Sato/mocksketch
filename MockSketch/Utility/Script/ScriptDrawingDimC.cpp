/**
 * @brief			ScriptDrawingDimC実装ファイル
 * @file			ScriptDrawingDimC.cpp
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:36
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include <assert.h>
#include <sstream>
#include "ScriptBase.h"
#include "ScriptDrawingDimC.h"
#include "Utility/CUtility.h"
#include "DrawingObject/Primitive/SPLINE2D.H"
#include "DrawingObject/CDrawingDimC.h"


/*---------------------------------------------------*/
/*  MACROS                                           */
/*---------------------------------------------------*/
using namespace std;
#ifdef _DEBUG
#undef new 
#endif
extern bool CALLBACK ConvPOINT2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);
extern bool CALLBACK ConvLINE2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);


static void ConstructDrawingDimC (CDrawingDimC *thisPointer)
{
	new(thisPointer) CDrawingDimC();
}

#ifdef _DEBUG
#undef new 
#define new DEBUG_NEW
#endif
static void DestructDrawingDimC(CDrawingDimC *thisPointer)
{
	thisPointer->~CDrawingDimC();
}

CDrawingDimC* CreateDrawingDimC()
{
    return new CDrawingDimC();
}

bool CALLBACK ConvDrawingDimC( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("DrawingDimC");
    const CDrawingDimC* pObj = reinterpret_cast<const CDrawingDimC*>(pValAddress);

    pValData->strVal = _T("{");

    VAL_DATA valData;
    ConvDrawingObject( &valData, pObj, pContext);
    pValData->lstVal.push_back(valData);
    pValData->strVal += valData.strVal;
    pValData->strVal += _T(",");

    //===========================
    //Text



    pValData->strVal += _T("}");

    return true;
}


//DrawingDimC型登録
int RegisterDrawingDimCType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
    int iRet = engine->RegisterObjectType( "DrawingDimC", 0, asOBJ_REF); assert( iRet >= 0 );  

    //ウォッチリスト表示用登録
    mapIdFunc[iRet] = ConvDrawingDimC;
    return iRet;
}

//DrawingDimC登録
int RegisterDrawingDimC(asIScriptEngine *engine)
{
    RegsterRef<CDrawingDimC>(engine, "DrawingDimC");
    RegsterBasicMethod<CDrawingDimC>(engine, "DrawingDimC");
    RegsterDrawingObject<CDrawingDimC>(engine, "DrawingDimC");


    //-------------------------------------------------
    int r;


    // キャスト
    //DrawingLine -> DrawingObject へのキャストは 自動
    r = engine->RegisterObjectBehaviour("DrawingDimC", asBEHAVE_IMPLICIT_REF_CAST, 
        "DrawingObject@ f()", 
        asFUNCTION((ASRefCast<CDrawingObject, CDrawingDimC>)), 
        asCALL_CDECL_OBJLAST);
    assert( r>=0 );

    //DrawingObject->  DrawingLine へのキャストは キャスト演算子が必要
    r = engine->RegisterObjectBehaviour("DrawingObject", asBEHAVE_REF_CAST , 
        "DrawingDimC@ f()", 
        asFUNCTION((ASRefCast<CDrawingDimC, CDrawingObject>)), 
        asCALL_CDECL_OBJLAST);
    assert( r>=0 );



    return 0;
}
