/**
 * @brief			ScriptDrawingDimR実装ファイル
 * @file			ScriptDrawingDimR.cpp
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:36
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include <assert.h>
#include <sstream>
#include "ScriptBase.h"
#include "ScriptDrawingDimR.h"
#include "Utility/CUtility.h"
#include "DrawingObject/Primitive/SPLINE2D.H"
#include "DrawingObject/CDrawingDimR.h"


/*---------------------------------------------------*/
/*  MACROS                                           */
/*---------------------------------------------------*/
using namespace std;
#ifdef _DEBUG
#undef new 
#endif
extern bool CALLBACK ConvPOINT2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);
extern bool CALLBACK ConvLINE2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);


static void ConstructDrawingDimR (CDrawingDimR *thisPointer)
{
	new(thisPointer) CDrawingDimR();
}

#ifdef _DEBUG
#undef new 
#define new DEBUG_NEW
#endif
static void DestructDrawingDimR(CDrawingDimR *thisPointer)
{
	thisPointer->~CDrawingDimR();
}

CDrawingDimR* CreateDrawingDimR()
{
    return new CDrawingDimR();
}

bool CALLBACK ConvDrawingDimR( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("DrawingDimR");
    const CDrawingDimR* pObj = reinterpret_cast<const CDrawingDimR*>(pValAddress);

    pValData->strVal = _T("{");

    VAL_DATA valData;
    ConvDrawingObject( &valData, pObj, pContext);
    pValData->lstVal.push_back(valData);
    pValData->strVal += valData.strVal;
    pValData->strVal += _T(",");

    //===========================
    //Text



    pValData->strVal += _T("}");

    return true;
}


//DrawingDimR型登録
int RegisterDrawingDimRType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
    int iRet = engine->RegisterObjectType( "DrawingDimR", 0, asOBJ_REF); assert( iRet >= 0 );  

    //ウォッチリスト表示用登録
    mapIdFunc[iRet] = ConvDrawingDimR;
    return iRet;
}

//DrawingDimR登録
int RegisterDrawingDimR(asIScriptEngine *engine)
{
    RegsterRef<CDrawingDimR>(engine, "DrawingDimR");
    RegsterBasicMethod<CDrawingDimR>(engine, "DrawingDimR");
    RegsterDrawingObject<CDrawingDimR>(engine, "DrawingDimR");


    //-------------------------------------------------
    int r;


    // キャスト
    //DrawingLine -> DrawingObject へのキャストは 自動
    r = engine->RegisterObjectBehaviour("DrawingDimR", asBEHAVE_IMPLICIT_REF_CAST, 
        "DrawingObject@ f()", 
        asFUNCTION((ASRefCast<CDrawingObject, CDrawingDimR>)), 
        asCALL_CDECL_OBJLAST);
    assert( r>=0 );

    //DrawingObject->  DrawingLine へのキャストは キャスト演算子が必要
    r = engine->RegisterObjectBehaviour("DrawingObject", asBEHAVE_REF_CAST , 
        "DrawingDimR@ f()", 
        asFUNCTION((ASRefCast<CDrawingDimR, CDrawingObject>)), 
        asCALL_CDECL_OBJLAST);
    assert( r>=0 );



    return 0;
}
