/**
 * @brief			ScriptDrawingElement実装ファイル
 * @file			ScriptDrawingElement.cpp
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:36
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include <assert.h>
#include <sstream>
#include "ScriptBase.h"
#include "ScriptDrawingElement.h"
#include "Utility/CUtility.h"
#include "DrawingObject/CDrawingElement.h"
#include "DefinitionObject/CElementDef.h"



/*---------------------------------------------------*/
/*  MACROS                                           */
/*---------------------------------------------------*/
using namespace std;
#ifdef _DEBUG
#undef new 
#endif
extern bool CALLBACK ConvPOINT2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);
extern bool CALLBACK ConvLINE2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);


static void ConstructDrawingElement (CDrawingElement *thisPointer)
{
	new(thisPointer) CDrawingElement();
}

#ifdef _DEBUG
#undef new 
#define new DEBUG_NEW
#endif
static void DestructDrawingElement(CDrawingElement *thisPointer)
{
	thisPointer->~CDrawingElement();
}

CDrawingElement* CreateDrawingElement()
{
    return new CDrawingElement();
}

bool CALLBACK ConvDrawingElement( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("DrawingElement");
    const CDrawingElement* pObj = reinterpret_cast<const CDrawingElement*>(pValAddress);

    pValData->strVal = _T("{");

    VAL_DATA valData;
    ConvDrawingObject( &valData, pObj, pContext);
    pValData->lstVal.push_back(valData);
    pValData->strVal += valData.strVal;
    pValData->strVal += _T(",");

    //===========================
    //Text



    pValData->strVal += _T("}");

    return true;
}


//DrawingElement型登録
int RegisterDrawingElementType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
    int iRet = engine->RegisterObjectType( "DrawingElement", 0, asOBJ_REF); assert( iRet >= 0 );  

    //ウォッチリスト表示用登録
    mapIdFunc[iRet] = ConvDrawingElement;
    return iRet;
}

//DrawingElement登録
int RegisterDrawingElement(asIScriptEngine *engine)
{
    RegsterRef<CDrawingElement>(engine, "DrawingElement");
    RegsterBasicMethod<CDrawingElement>(engine, "DrawingElement");
    RegsterDrawingObject<CDrawingElement>(engine, "DrawingElement");
    RegisterDrawingScriptBaseCommon<CDrawingElement>(engine,"DrawingElement");


    //-------------------------------------------------
    int r;


    // キャスト
    //DrawingLine -> DrawingObject へのキャストは 自動
    r = engine->RegisterObjectBehaviour("DrawingElement", asBEHAVE_IMPLICIT_REF_CAST, 
        "DrawingObject@ f()", 
        asFUNCTION((ASRefCast<CDrawingObject, CDrawingElement>)), 
        asCALL_CDECL_OBJLAST);
    assert( r>=0 );

    //DrawingObject->  DrawingLine へのキャストは キャスト演算子が必要
    r = engine->RegisterObjectBehaviour("DrawingObject", asBEHAVE_REF_CAST , 
        "DrawingElement@ f()", 
        asFUNCTION((ASRefCast<CDrawingElement, CDrawingObject>)), 
        asCALL_CDECL_OBJLAST);
    assert( r>=0 );



    return 0;
}
