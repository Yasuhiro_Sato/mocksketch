/**
 * @brief			ScriptDrawingEllipse実装ファイル
 * @file			ScriptDrawingEllipse.cpp
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:36
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include <assert.h>
#include <sstream>
#include "ScriptBase.h"
#include "ScriptDrawingEllipse.h"
#include "Utility/CUtility.h"
#include "DrawingObject/Primitive/ELLIPSE2D.H"
#include "DrawingObject/CDrawingEllipse.h"


/*---------------------------------------------------*/
/*  MACROS                                           */
/*---------------------------------------------------*/
using namespace std;
#ifdef _DEBUG
#undef new 
#endif
extern bool CALLBACK ConvELLIPSE2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);


static void ConstructDrawingEllipse (CDrawingEllipse *thisPointer)
{
	new(thisPointer) CDrawingEllipse();
}

#ifdef _DEBUG
#undef new 
#define new DEBUG_NEW
#endif
static void DestructDrawingParts(CDrawingEllipse *thisPointer)
{
	thisPointer->~CDrawingEllipse();
}

CDrawingEllipse* CreateDrawingEllipse()
{
    return new CDrawingEllipse();
}

bool CALLBACK ConvDrawingEllipse( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("DrawingEllipse");
    const CDrawingEllipse* pObj = reinterpret_cast<const CDrawingEllipse*>(pValAddress);

    pValData->strVal = _T("{");

    VAL_DATA valData;
    ConvDrawingObject( &valData, pObj, pContext);
    pValData->lstVal.push_back(valData);
    pValData->strVal += valData.strVal;
    pValData->strVal += _T(",");
    //===========================
    const ELLIPSE2D* pEllipse;
    pEllipse = pObj->GetEllipseInstance();
    ConvELLIPSE2D( &valData, pEllipse, pContext);
    pValData->lstVal.push_back(valData);
    pValData->strVal += valData.strVal;
    pValData->strVal += _T(",");

    valData.strName     = _T("LineWidth");
    valData.strTypeName = _T("int");
    valData.strVal      =  CUtil::StrFormat(_T("%d"), pObj->GetLineWidth());
    pValData->lstVal.push_back(valData);
    pValData->strVal += valData.strVal;
    pValData->strVal += _T(",");

    valData.strName     = _T("LineType");
    valData.strTypeName = _T("int");
    valData.strVal      =  CUtil::StrFormat(_T("%d"), pObj->GetLineType());
    pValData->lstVal.push_back(valData);
    pValData->strVal += valData.strVal;
    pValData->strVal += _T("}");



    pValData->strVal += _T("}");

    return true;
}


//DrawingEllipse型登録
int RegisterDrawingEllipseType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
    int iRet = engine->RegisterObjectType( "DrawingEllipse", 0, asOBJ_REF); assert( iRet >= 0 );  

    //ウォッチリスト表示用登録
    mapIdFunc[iRet] = ConvDrawingEllipse;
    return iRet;
}

//DrawingEllipse登録
int RegisterDrawingEllipse(asIScriptEngine *engine)
{
    RegsterRef<CDrawingEllipse>(engine, "DrawingEllipse");
    RegsterBasicMethod<CDrawingEllipse>(engine, "DrawingEllipse");
    RegsterDrawingObject<CDrawingEllipse>(engine, "DrawingEllipse");
    RegisterDrawingLineCommon<CDrawingEllipse>(engine, "DrawingEllipse");


    //-------------------------------------------------
    int r;
   //!< 中心設定
	r = engine->RegisterObjectMethod("DrawingEllipse", 
        "void SetCenter(POINT2D &in)", 
        asMETHOD(CDrawingEllipse, SetCenter),
        asCALL_THISCALL); assert( r >= 0 );

    //!< 中心取得
	r = engine->RegisterObjectMethod("DrawingEllipse", 
        "POINT2D& GetCenter()", 
        asMETHOD(CDrawingEllipse, GetCenter),
        asCALL_THISCALL); assert( r >= 0 );

    //!< 長径設定
	r = engine->RegisterObjectMethod("DrawingEllipse", 
        "void SetRadius1(double)", 
        asMETHOD(CDrawingEllipse, SetRadius1),
        asCALL_THISCALL); assert( r >= 0 );

    //!< 長径取得
	r = engine->RegisterObjectMethod("DrawingEllipse", 
        "double GetRadius1()", 
        asMETHOD(CDrawingEllipse, GetRadius1),
        asCALL_THISCALL); assert( r >= 0 );

    //!< 短径設定
	r = engine->RegisterObjectMethod("DrawingEllipse", 
        "void SetRadius2(double)", 
        asMETHOD(CDrawingEllipse, SetRadius2),
        asCALL_THISCALL); assert( r >= 0 );

    //!< 短径取得
	r = engine->RegisterObjectMethod("DrawingEllipse", 
        "double GetRadius2()", 
        asMETHOD(CDrawingEllipse, GetRadius2),
        asCALL_THISCALL); assert( r >= 0 );

    //!< 角度設定
	r = engine->RegisterObjectMethod("DrawingEllipse", 
        "void SetAngle(double)", 
        asMETHOD(CDrawingEllipse, SetAngle),
        asCALL_THISCALL); assert( r >= 0 );

    //!< 角度取得
	r = engine->RegisterObjectMethod("DrawingEllipse", 
        "double GetAngle()", 
        asMETHOD(CDrawingEllipse, GetAngle),
        asCALL_THISCALL); assert( r >= 0 );

   //!< 開始角設定
	r = engine->RegisterObjectMethod("DrawingEllipse", 
        "void SetStart(double)", 
        asMETHOD(CDrawingEllipse, SetStart),
        asCALL_THISCALL); assert( r >= 0 );

    //!< 開始角取得
	r = engine->RegisterObjectMethod("DrawingEllipse", 
        "double GetStart()", 
        asMETHOD(CDrawingEllipse, GetStart),
        asCALL_THISCALL); assert( r >= 0 );

    //!< 終了角設定
	r = engine->RegisterObjectMethod("DrawingEllipse", 
        "void SetEnd(double &in)", 
        asMETHOD(CDrawingEllipse, SetEnd),
        asCALL_THISCALL); assert( r >= 0 );

    //!< 終了角取得
	r = engine->RegisterObjectMethod("DrawingEllipse", 
        "double GetEnd()", 
        asMETHOD(CDrawingEllipse, GetEnd),
        asCALL_THISCALL); assert( r >= 0 );

    //!< 楕円取得
	r = engine->RegisterObjectMethod("DrawingEllipse", 
        "ELLIPSE2D@ GetEllipseInstance()", 
        asMETHODPR(CDrawingEllipse, GetEllipseInstance,(), ELLIPSE2D*),
        asCALL_THISCALL); assert( r >= 0 );


    // キャスト
    //DrawingLine -> DrawingObject へのキャストは 自動
    r = engine->RegisterObjectBehaviour("DrawingEllipse", asBEHAVE_IMPLICIT_REF_CAST, 
        "DrawingObject@ f()", 
        asFUNCTION((ASRefCast<CDrawingObject, CDrawingEllipse>)), 
        asCALL_CDECL_OBJLAST);
    assert( r>=0 );

    //DrawingObject->  DrawingLine へのキャストは キャスト演算子が必要
    r = engine->RegisterObjectBehaviour("DrawingObject", asBEHAVE_REF_CAST , 
        "DrawingEllipse@ f()", 
        asFUNCTION((ASRefCast<CDrawingEllipse, CDrawingObject>)), 
        asCALL_CDECL_OBJLAST);
    assert( r>=0 );



    return 0;
}
