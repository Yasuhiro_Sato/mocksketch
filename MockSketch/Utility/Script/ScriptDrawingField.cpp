/**
 * @brief			ScriptDrawingField実装ファイル
 * @file			ScriptDrawingField.cpp
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:36
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include <assert.h>
#include <sstream>
#include "ScriptBase.h"
#include "ScriptDrawingField.h"
#include "Utility/CUtility.h"
#include "DrawingObject/Primitive/SPLINE2D.H"
#include "DrawingObject/CDrawingField.h"


/*---------------------------------------------------*/
/*  MACROS                                           */
/*---------------------------------------------------*/
using namespace std;
#ifdef _DEBUG
#undef new 
#endif
extern bool CALLBACK ConvPOINT2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);
extern bool CALLBACK ConvLINE2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);


static void ConstructDrawingField (CDrawingField *thisPointer)
{
	new(thisPointer) CDrawingField();
}

#ifdef _DEBUG
#undef new 
#define new DEBUG_NEW
#endif
static void DestructDrawingField(CDrawingField *thisPointer)
{
	thisPointer->~CDrawingField();
}

CDrawingField* CreateDrawingField()
{
    return new CDrawingField();
}

bool CALLBACK ConvDrawingField( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("DrawingField");
    const CDrawingField* pObj = reinterpret_cast<const CDrawingField*>(pValAddress);

    pValData->strVal = _T("{");

    VAL_DATA valData;
    ConvDrawingObject( &valData, pObj, pContext);
    pValData->lstVal.push_back(valData);
    pValData->strVal += valData.strVal;
    pValData->strVal += _T(",");

    //===========================
    //Text



    pValData->strVal += _T("}");

    return true;
}


//DrawingField型登録
int RegisterDrawingFieldType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
    int iRet = engine->RegisterObjectType( "DrawingField", 0, asOBJ_REF); assert( iRet >= 0 );  

    //ウォッチリスト表示用登録
    mapIdFunc[iRet] = ConvDrawingField;
    return iRet;
}




//DrawingField登録
int RegisterDrawingField(asIScriptEngine *engine)
{
    RegsterRef<CDrawingField>(engine, "DrawingField");
    RegsterBasicMethod<CDrawingField>(engine, "DrawingField");
    RegsterDrawingObject<CDrawingField>(engine, "DrawingField");
    RegisterDrawingScriptBaseCommon<CDrawingField>(engine,"DrawingField");
    RegisterDrawingPartsCommon<CDrawingField>(engine,"DrawingField");


    //-------------------------------------------------
    int r;

    r = engine->RegisterObjectMethod("DrawingField", 
        "bool GetObjectList(DrawingObjectPtr_VECTOR& out, string& in, string& in, DRAWING_TYPE)", 
        asMETHOD(CDrawingField,GetObjectList), 
        //asMETHODPR(CDrawingField,GetObjectList,(std::vector<CDrawingObject*>&, 
        //            StdString, StdString, DRAWING_TYPE), bool), 
        asCALL_THISCALL); assert( r >= 0 );
#if 0
    r = engine->RegisterObjectMethod("DrawingField", 
        "bool GetObjectList(DrawingObjectPtr_VECTOR& out, string& in, string& in, DRAWING_TYPE)", 
        asMETHOD(CDrawingField,GetObjectListS), 
        //asMETHODPR(CDrawingField,GetObjectList,(std::vector<CDrawingObject*>&, 
        //            StdString, StdString, DRAWING_TYPE), bool), 
        asCALL_THISCALL); assert( r >= 0 );
#endif

    r = engine->RegisterObjectMethod("DrawingField", 
        "void SetPointList(POINT2D_VECTOR& in)", 
        asMETHOD(CDrawingField,SetPointList), 
        asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("DrawingField", 
        "void GetPointList(POINT2D_VECTOR& out)", 
        asMETHOD(CDrawingField,GetPointList), 
        asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("DrawingField", 
        "void SetDrawingObjectList(DrawingObjectPtr_VECTOR& in)", 
        asMETHOD(CDrawingField,SetDrawingObjectList), 
        asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("DrawingField", 
        "void GetDrawingObjectList(DrawingObjectPtr_VECTOR& out, string& in)", 
        asMETHOD(CDrawingField,GetDrawingObjectList), 
        asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("DrawingField", 
        "void GetDrawingType(DRAWING_TYPE)", 
        asMETHOD(CDrawingField,GetDrawingType), 
        asCALL_THISCALL); assert( r >= 0 );

    //!< オブジェクト取得(名称)
	r = engine->RegisterObjectMethod("DrawingField", 
        "DrawingObject@ GetFieldObject(string)", 
         asMETHOD(CDrawingField,GetFieldObject), 
        asCALL_THISCALL); assert( r >= 0 );

#if 0
    //!< オブジェクト取得(名称)
	r = engine->RegisterObjectMethod("DrawingField", 
        "DrawingObject@ GetFieldObject(string)", 
         asMETHOD(CDrawingField,GetFieldObjectS), 
        asCALL_THISCALL); assert( r >= 0 );
#endif

    r = engine->RegisterObjectMethod("DrawingField", 
        "void SetObjectListTest(DrawingObjectPtr_VECTOR& in)", 
        asMETHOD(CDrawingField,SetObjectListTest), 
        asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("DrawingField", 
        "void GetObjectListTest(DrawingObjectPtr_VECTOR& out)", 
        asMETHOD(CDrawingField,GetObjectListTest), 
        asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("DrawingField", 
        "void CreateObjectListTest(DrawingObjectPtr_VECTOR& out)", 
        asMETHOD(CDrawingField,CreateObjectListTest), 
        asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("DrawingField", 
        "void DispObjectListTest(DrawingObjectPtr_VECTOR& in)", 
        asMETHOD(CDrawingField,DispObjectListTest), 
        asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("DrawingField", 
        "void AnalysisData(?& in)", 
        asMETHOD(CDrawingField,AnalysisData), 
        asCALL_THISCALL); assert( r >= 0 );


    r = engine->RegisterObjectMethod("DrawingField", 
        "void PrintObject(const DrawingObject &inout)", 
        asMETHOD(CDrawingField,PrintObject), 
        asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("DrawingField", 
        "void PrintObject_in(const DrawingObject &in)", 
        asMETHOD(CDrawingField,PrintObject), 
        asCALL_THISCALL); assert( r >= 0 );

       r = engine->RegisterObjectMethod("DrawingField", 
        "void PrintObject2(const DrawingCompositionLine &inout)", 
        asMETHOD(CDrawingField,PrintObject2), 
        asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("DrawingField", 
        "void SetData(?& in)", 
        asMETHOD(CDrawingField,Store), 
        asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("DrawingField", 
        "void PrintPoint2D(POINT2D& in)", 
        asMETHOD(CDrawingField,PrintPoint2D), 
        asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("DrawingField", 
        "void GetData(?& out)", 
        asMETHOD(CDrawingField,Retrieve), 
        asCALL_THISCALL); assert( r >= 0 );

    /*
    r = engine->RegisterObjectMethod("DrawingField", 
        "?@ GetDataAddress()", 
        asMETHOD(CDrawingField,GetDataAddress), 
        asCALL_THISCALL); assert( r >= 0 );
    */

    r = engine->RegisterObjectMethod("DrawingField", 
        "DrawingObject@ CreateObject(DRAWING_TYPE, string)", 
        asMETHOD(CDrawingField,CreateObject), 
        asCALL_THISCALL); assert( r >= 0 );
#if 0
    r = engine->RegisterObjectMethod("DrawingField", 
        "DrawingObject@ CreateObject(DRAWING_TYPE, string)", 
        asMETHOD(CDrawingField,CreateObjectS), 
        asCALL_THISCALL); assert( r >= 0 );
#endif

    // キャスト
    //DrawingLine -> DrawingObject へのキャストは 自動
    r = engine->RegisterObjectBehaviour("DrawingField", asBEHAVE_IMPLICIT_REF_CAST, 
        "DrawingObject@ f()", 
        asFUNCTION((ASRefCast<CDrawingObject, CDrawingField>)), 
        asCALL_CDECL_OBJLAST);
    assert( r>=0 );

    //DrawingObject->  DrawingLine へのキャストは キャスト演算子が必要
    r = engine->RegisterObjectBehaviour("DrawingObject", asBEHAVE_REF_CAST , 
        "DrawingField@ f()", 
        asFUNCTION((ASRefCast<CDrawingField, CDrawingObject>)), 
        asCALL_CDECL_OBJLAST);
    assert( r>=0 );



    return 0;
}


//*******************************
//             TEST
//*******************************
#ifdef _DEBUG

//コンストラクタがある参照型の実装がないためここで
//テスト用のクラスを設定する


CRefClass *Factory()
{
	return new CRefClass;
}


//DrawingField登録
int RegisterRefClass(asIScriptEngine *engine)
{
    int r;
	r = engine->RegisterObjectBehaviour("RefClass", asBEHAVE_FACTORY, "RefClass@ f()", asFUNCTION(Factory), asCALL_CDECL); assert(r >= 0);
	r = engine->RegisterObjectBehaviour("RefClass", asBEHAVE_ADDREF, "void f()", asMETHOD(CRefClass, AddRef), asCALL_THISCALL); assert(r >= 0);
	r = engine->RegisterObjectBehaviour("RefClass", asBEHAVE_RELEASE, "void f()", asMETHOD(CRefClass, Release), asCALL_THISCALL); assert(r >= 0);
	r = engine->RegisterObjectMethod("RefClass", "RefClass &opAssign(RefClass &in)", asMETHOD(CRefClass, operator=), asCALL_THISCALL); assert(r >= 0);
	r = engine->RegisterObjectMethod("RefClass", "RefClass &Do()", asMETHOD(CRefClass,Do), asCALL_THISCALL); assert(r >= 0);
	r = engine->RegisterObjectMethod("RefClass", "RefClass &opAdd(RefClass &in)", asFUNCTION(CRefClass::Add), asCALL_CDECL_OBJFIRST); assert(r >= 0);
    return 0;

}

bool CALLBACK ConvRefClass( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("RefClass");
    const CDrawingField* pObj = reinterpret_cast<const CDrawingField*>(pValAddress);

    pValData->strVal = _T("{");

    VAL_DATA valData;
    ConvDrawingObject( &valData, pObj, pContext);
    pValData->lstVal.push_back(valData);
    pValData->strVal += valData.strVal;
    pValData->strVal += _T(",");

    //===========================
    //Text



    pValData->strVal += _T("}");

    return true;
}



int RegisterRefClassType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
	int iRet = engine->RegisterObjectType("RefClass", sizeof(CRefClass), asOBJ_REF); assert(iRet >= 0);

    //ウォッチリスト表示用登録
    mapIdFunc[iRet] = ConvRefClass;
    return iRet;
}


#endif