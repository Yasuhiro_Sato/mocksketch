/**
 * @brief			ScriptDrawingField
 * @file			ScriptDrawingField.h
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:37
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#ifndef SCRIPT_DRAWING_FIELD_H
#define SCRIPT_DRAWING_FIELD_H

#include <angelscript.h>
#include <string>
#include "VAL_DATA.h"

int RegisterDrawingFieldType (asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc);
int RegisterDrawingField (asIScriptEngine *engine);


//*******************************
//             TEST
//*******************************
#ifdef _DEBUG

//コンストラクタがある参照型の実装がないためここで
//テスト用のクラスを設定する
class CRefClass
{
public:
	CRefClass()
	{
		refCount = 1;
	}
	~CRefClass()
	{
	}
	CRefClass &operator=(const CRefClass & /*o*/)
	{
		return *this;
	}
	int AddRef()
	{
		return ++refCount;
	}
	int Release()
	{
		int r = --refCount;
		if( refCount == 0 ) delete this;
		return r;
	}
	static CRefClass &Add(CRefClass &self, CRefClass & /*other*/)
	{
		return self;
	}
	CRefClass &Do()
	{
		return *this;
	}
	int refCount;
};

int RegisterRefClass(asIScriptEngine *engine);
int RegisterRefClassType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc);
#endif



#endif
