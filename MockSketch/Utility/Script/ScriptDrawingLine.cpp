/**
 * @brief			ScriptDrawingLine実装ファイル
 * @file			ScriptDrawingLine.cpp
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:36
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include <assert.h>
#include <sstream>
#include "ScriptBase.h"
#include "ScriptDrawingLine.h"
#include "Utility/CUtility.h"
#include "DrawingObject/Primitive/SPLINE2D.H"
#include "DrawingObject/CDrawingLine.h"


/*---------------------------------------------------*/
/*  MACROS                                           */
/*---------------------------------------------------*/
using namespace std;
#ifdef _DEBUG
#undef new 
#endif
extern bool CALLBACK ConvPOINT2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);
extern bool CALLBACK ConvLINE2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);


static void ConstructDrawingLine (CDrawingLine *thisPointer)
{
	new(thisPointer) CDrawingLine();
}
#ifdef _DEBUG
#undef new 
#define new DEBUG_NEW
#endif

static void DestructDrawingParts(CDrawingLine *thisPointer)
{
	thisPointer->~CDrawingLine();
}

CDrawingLine* CreateDrawingLine()
{
    return new CDrawingLine();
}

bool CALLBACK ConvDrawingLine( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("DrawingLine");
    const CDrawingLine* pObj = reinterpret_cast<const CDrawingLine*>(pValAddress);

    pValData->strVal = _T("{");

    //===========================
    // DrawingObject共通
    //===========================
    VAL_DATA valData;
    ConvDrawingObject( &valData, pObj, pContext);
    pValData->lstVal.push_back(valData);
    pValData->strVal += valData.strVal;
    pValData->strVal += _T(",");

    //===========================
    //Line
    const LINE2D* pLine;
    pLine = pObj->GetLineInstance();
    ConvLINE2D( &valData, pLine, pContext);
    pValData->lstVal.push_back(valData);
    pValData->strVal += valData.strVal;
    pValData->strVal += _T(",");

    valData.strName     = _T("LineWidth");
    valData.strTypeName = _T("int");
    valData.strVal      =  CUtil::StrFormat(_T("%d"), pObj->GetLineWidth());
    pValData->lstVal.push_back(valData);
    pValData->strVal += valData.strVal;
    pValData->strVal += _T(",");

    valData.strName     = _T("LineType");
    valData.strTypeName = _T("int");
    valData.strVal      =  CUtil::StrFormat(_T("%d"), pObj->GetLineType());
    pValData->lstVal.push_back(valData);
    pValData->strVal += valData.strVal;
    pValData->strVal += _T("}");

    return true;
}

//DrawingLine型登録
int RegisterDrawingLineType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
    int iRet = engine->RegisterObjectType( "DrawingLine", 0, asOBJ_REF); assert( iRet >= 0 );  

    //ウォッチリスト表示用登録
    mapIdFunc[iRet] = ConvDrawingLine;
    return iRet;
}


static void GetPoint1(POINT2D& pt1, CDrawingLine &Line)
{
    pt1 = Line.GetPoint1();
}


//DrawingLine登録
int RegisterDrawingLine(asIScriptEngine *engine)
{
    RegsterRef<CDrawingLine>(engine, "DrawingLine");
    RegsterBasicMethod<CDrawingLine>(engine, "DrawingLine");
    RegsterDrawingObject<CDrawingLine>(engine, "DrawingLine");
    //-------------------------------------------------
    int r;
    //!< 位置設定
	r = engine->RegisterObjectMethod("DrawingLine", 
        "void SetPoint(double, double, double, double)", 
        asMETHODPR(CDrawingLine,SetPoint,(double, double, double, double), void),
        asCALL_THISCALL); assert( r >= 0 );

    //!< 位置設定
    r = engine->RegisterObjectMethod("DrawingLine",
        "void SetPoint(POINT2D &in, POINT2D &in)",
        asMETHODPR(CDrawingLine, SetPoint, (const POINT2D&, const POINT2D&), void),
        asCALL_THISCALL); assert(r >= 0);

    //!< 位置設定
    r = engine->RegisterObjectMethod("DrawingLine",
        "void SetLine(LINE2D &in)",
        asMETHODPR(CDrawingLine, SetLine, (const LINE2D&), void),
        asCALL_THISCALL); assert(r >= 0);

    //!< 位置設定1
    /*
	r = engine->RegisterObjectMethod("DrawingLine", 
        "void SetPoint1(POINT2D@)", 
        asMETHODPR(CDrawingLine, SetPoint1Ref,(const POINT2D*), void), 
        asCALL_THISCALL); assert( r >= 0 );
    */

    //!< 位置設定1
	r = engine->RegisterObjectMethod("DrawingLine", 
        "void SetPoint1(POINT2D &in)", 
        asMETHODPR(CDrawingLine, SetPoint1,(const POINT2D&), void), 
        asCALL_THISCALL); assert( r >= 0 );

#ifdef POINT2D_REF
    //!< 位置取得1 ASは参照型を戻り値にできない
	r = engine->RegisterObjectMethod("DrawingLine", 
        "POINT2D@ GetPoint1()", 
        asMETHOD(CDrawingLine,GetPoint1As), 
        asCALL_THISCALL); assert( r >= 0 );
#else
    r = engine->RegisterObjectMethod("DrawingLine", 
        "POINT2D GetPoint1() const", 
        asMETHODPR(CDrawingLine, GetPoint1,(void)  const, POINT2D), 
        asCALL_THISCALL); assert( r >= 0 );
#endif

    //!< 位置設定2
	r = engine->RegisterObjectMethod("DrawingLine", 
        "void SetPoint2(POINT2D &in)", 
        asMETHODPR(CDrawingLine, SetPoint2,(const POINT2D&), void), 
        asCALL_THISCALL); assert( r >= 0 );

#ifdef POINT2D_REF
    //!< 位置取得2
	r = engine->RegisterObjectMethod("DrawingLine", 
        "POINT2D@ GetPoint2()", 
        asMETHOD(CDrawingLine,GetPoint2As), 
        asCALL_THISCALL); assert( r >= 0 );
#else
    r = engine->RegisterObjectMethod("DrawingLine", 
        "POINT2D GetPoint2() const", 
        asMETHODPR(CDrawingLine, GetPoint2,(void) const, POINT2D), 
        asCALL_THISCALL); assert( r >= 0 );
#endif

    // キャスト
    //DrawingLine -> DrawingObject へのキャストは 自動
    r = engine->RegisterObjectBehaviour("DrawingLine", asBEHAVE_IMPLICIT_REF_CAST, 
        "DrawingObject@ f()", 
        asFUNCTION((ASRefCast<CDrawingObject, CDrawingLine>)), 
        asCALL_CDECL_OBJLAST);
    assert( r>=0 );

    //DrawingObject->  DrawingLine へのキャストは キャスト演算子が必要
    r = engine->RegisterObjectBehaviour("DrawingObject", asBEHAVE_REF_CAST , 
        "DrawingLine@ f()", 
        asFUNCTION((ASRefCast<CDrawingLine, CDrawingObject>)), 
        asCALL_CDECL_OBJLAST);
    assert( r>=0 );


    return 0;
}
