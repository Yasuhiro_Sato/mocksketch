/**
 * @brief			ScriptDrawingNode実装ファイル
 * @file			ScriptDrawingNode.cpp
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:36
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include <assert.h>
#include <sstream>
#include "ScriptBase.h"
#include "ScriptDrawingNode.h"
#include "Utility/CUtility.h"
#include "DrawingObject/Primitive/SPLINE2D.H"
#include "DrawingObject/CDrawingNode.h"


/*---------------------------------------------------*/
/*  MACROS                                           */
/*---------------------------------------------------*/
using namespace std;
#ifdef _DEBUG
#undef new 
#endif
extern bool CALLBACK ConvPOINT2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);
extern bool CALLBACK ConvLINE2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);


static void ConstructDrawingNode (CDrawingNode *thisPointer)
{
	new(thisPointer) CDrawingNode();
}

#ifdef _DEBUG
#undef new 
#define new DEBUG_NEW
#endif
static void DestructDrawingParts(CDrawingNode *thisPointer)
{
	thisPointer->~CDrawingNode();
}

CDrawingNode* CreateDrawingNode()
{
    return new CDrawingNode();
}

bool CALLBACK ConvDrawingNode( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("DrawingNode");
    const CDrawingNode* pObj = reinterpret_cast<const CDrawingNode*>(pValAddress);

    pValData->strVal = _T("{");

    //===========================
    // DrawingObject共通
    //===========================
    VAL_DATA valData;
    ConvDrawingObject( &valData, pObj, pContext);
    pValData->lstVal.push_back(valData);
    pValData->strVal += valData.strVal;
    pValData->strVal += _T(",");
    //===========================



    pValData->strVal += _T("}");

    return true;
}


//DrawingNode型登録
int RegisterDrawingNodeType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
    int iRet = engine->RegisterObjectType( "DrawingNode", 0, asOBJ_REF); assert( iRet >= 0 );  

    //ウォッチリスト表示用登録
    mapIdFunc[iRet] = ConvDrawingNode;
    return iRet;
}

//DrawingNode登録
int RegisterDrawingNode(asIScriptEngine *engine)
{
    RegsterRef<CDrawingNode>(engine, "DrawingNode");
    RegsterBasicMethod<CDrawingNode>(engine, "DrawingNode");
    RegsterDrawingObject<CDrawingNode>(engine, "DrawingNode");


    //-------------------------------------------------
    int r;

    //!< 位置設定
	r = engine->RegisterObjectMethod("DrawingNode", 
        "void SetPoint(POINT2D &in)", 
        asMETHODPR(CDrawingNode, SetPoint,(const POINT2D&), void),
        asCALL_THISCALL); assert( r >= 0 );

    //!< 位置取得
	r = engine->RegisterObjectMethod("DrawingNode", 
        "POINT2D& GetPoint()", 
        asMETHODPR(CDrawingNode, GetPoint,(), POINT2D&),
        asCALL_THISCALL); assert( r >= 0 );

    //!< マーカ色取得
	r = engine->RegisterObjectMethod("DrawingNode", 
        "uint GetMarkerColor()", 
        asMETHOD(CDrawingNode, GetMarkerColor),
        asCALL_THISCALL); assert( r >= 0 );

    //!< マーカ色取得
	r = engine->RegisterObjectMethod("DrawingNode", 
        "void SetMarkerColor(uint)", 
        asMETHOD(CDrawingNode, SetMarkerColor),
        asCALL_THISCALL); assert( r >= 0 );

    //!< 接続可能オブジェクトリスト
	r = engine->RegisterObjectMethod("DrawingNode", 
        "string GetConnectabaleObjectList()", 
        asMETHOD(CDrawingNode, GetConnectabaleObjectList),
        asCALL_THISCALL); assert( r >= 0 );


    // キャスト
    //DrawingLine -> DrawingObject へのキャストは 自動
    r = engine->RegisterObjectBehaviour("DrawingNode", asBEHAVE_IMPLICIT_REF_CAST, 
        "DrawingObject@ f()", 
        asFUNCTION((ASRefCast<CDrawingObject, CDrawingNode>)), 
        asCALL_CDECL_OBJLAST);
    assert( r>=0 );

    //DrawingObject->  DrawingLine へのキャストは キャスト演算子が必要
    r = engine->RegisterObjectBehaviour("DrawingObject", asBEHAVE_REF_CAST , 
        "DrawingNode@ f()", 
        asFUNCTION((ASRefCast<CDrawingNode, CDrawingObject>)), 
        asCALL_CDECL_OBJLAST);
    assert( r>=0 );



    return 0;
}
