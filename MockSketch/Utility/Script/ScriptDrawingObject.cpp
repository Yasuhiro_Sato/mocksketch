/**
 * @brief			ScriptDrawingObject�����t�@�C��
 * @file			ScriptDrawingObject.cpp
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:36
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include <assert.h>
#include <sstream>
#include "ScriptBase.h"
#include "ScriptDrawingObject.h"
#include "Utility/CUtility.h"
#include "DrawingObject/Primitive/SPLINE2D.H"
#include "DrawingObject/CDrawingObject.h"


/*---------------------------------------------------*/
/*  MACROS                                           */
/*---------------------------------------------------*/
using namespace std;
#ifdef _DEBUG
#undef new 
#define new DEBUG_NEW
#endif
extern bool CALLBACK ConvPOINT2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);


//---------------------------
// SLT���g�p�����^�́A
//
//!< ��_�v�Z 
 void CalcIntersection ( std::vector<POINT2D>& list,   
                                const CDrawingObject* pObj, bool bOnLine,  CDrawingObject& obj)
{
    //TODO:�e�X�g
    obj.CalcIntersection(&list, pObj, bOnLine);
}

//!< ��_�v�Z(�_)
 void CalcIntersectionPoint ( std::vector<POINT2D>& list,   
                                const CDrawingObject* pObj, POINT2D ptPos, bool bOnLine, CDrawingObject& obj)
{
    //TODO:�e�X�g
    obj.CalcIntersectionPoint(&list, pObj, ptPos, bOnLine);
}


//=======================================================================
//=======================================================================
//=======================================================================
bool CALLBACK ConvDrawingObject( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("DrawingObject");

    VAL_DATA valData;
    const CDrawingObject* pObj = reinterpret_cast<const CDrawingObject*>(pValAddress);

    pValData->strVal = _T("{");

    valData.strName     = _T("ObjectType");
    valData.strTypeName = _T("DRAWING_TYPE");
    valData.strVal      =  VIEW_COMMON::GetObjctTypeName( pObj->GetType()).c_str();
    pValData->lstVal.push_back(valData);
    pValData->strVal += valData.strVal;
    pValData->strVal += _T(",");

    valData.strName     = _T("ID");
    valData.strTypeName = _T("int");
    valData.strVal      =  CUtil::StrFormat(_T("%d"), pObj->GetId());
    pValData->lstVal.push_back(valData);
    pValData->strVal += valData.strVal;
    pValData->strVal += _T(",");

    valData.strName     = _T("Color");
    valData.strTypeName = _T("uint32");
    valData.strVal      =  CUtil::StrFormat(_T("%08x"), pObj->GetColor());
    pValData->lstVal.push_back(valData);
    pValData->strVal += valData.strVal;
    pValData->strVal += _T(",");

    valData.strName     = _T("Layer");
    valData.strTypeName = _T("int");
    valData.strVal      =  CUtil::StrFormat(_T("%d"), pObj->GetLayer());
    pValData->lstVal.push_back(valData);
    pValData->strVal += valData.strVal;
    pValData->strVal += _T(",");

    valData.strName     = _T("Name");
    valData.strTypeName = _T("string");
    valData.strVal      =  pObj->GetName();
    pValData->lstVal.push_back(valData);
    pValData->strVal += valData.strVal;
    pValData->strVal += _T(",");

    valData.strName     = _T("Tag");
    valData.strTypeName = _T("string");
    valData.strVal      =  pObj->GetTag();
    pValData->lstVal.push_back(valData);
    pValData->strVal += valData.strVal;
    pValData->strVal += _T(",");

    valData.strName     = _T("Visible");
    valData.strTypeName = _T("bool");
    valData.strVal      =  pObj->IsVisible() ? _T("true"): _T("false");
    pValData->lstVal.push_back(valData);
    pValData->strVal += valData.strVal;
    //pValData->strVal += _T(",");

    pValData->strVal += _T("}");

    return true;
}

//DrawingObject�^�o�^
int RegisterDrawingObjectType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
    int iRet = engine->RegisterObjectType( "DrawingObject", 0, asOBJ_REF); assert( iRet >= 0 );  

    //�E�H�b�`���X�g�\���p�o�^
    mapIdFunc[iRet] = ConvDrawingObject;
   return iRet;
}

//DrawingObject�o�^
int RegisterDrawingObject(asIScriptEngine *engine)
{
    RegsterRef<CDrawingObject>(engine, "DrawingObject");
    RegsterBasicMethod<CDrawingObject>(engine, "DrawingObject");
    RegsterDrawingObject<CDrawingObject>(engine, "DrawingObject");

    int r;
    r = engine->RegisterObjectMethod("DrawingObject",
        "DrawingObject &opAssign(const DrawingObject &in)"   ,
        asMETHODPR(CDrawingObject, operator=, (const CDrawingObject &), 
        CDrawingObject&), asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("DrawingObject", 
        "DrawingLine@ AsLine()", 
        asMETHOD(CDrawingObject,AsLine), asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("DrawingObject", 
        "DrawingText@ AsText()", 
        asMETHOD(CDrawingObject,AsText), asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("DrawingObject", 
        "DrawingNode@ AsNode()", 
        asMETHOD(CDrawingObject,AsNode), asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("DrawingObject", 
        "DrawingPoint@ AsPoint()", 
        asMETHOD(CDrawingObject,AsPoint), asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("DrawingObject", 
        "DrawingCircle@ AsCircle()", 
        asMETHOD(CDrawingObject,AsCircle), asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("DrawingObject", 
        "DrawingEllipse@ AsEllipse()", 
        asMETHOD(CDrawingObject,AsEllipse), asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("DrawingObject", 
        "DrawingParts@ AsParts()", 
        asMETHOD(CDrawingObject,AsParts), asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("DrawingObject", 
        "DrawingSpline@ AsSpline()", 
        asMETHOD(CDrawingObject,AsSpline), asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("DrawingObject", 
        "DrawingDimL@ AsDimL()", 
        asMETHOD(CDrawingObject,AsDimL), asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("DrawingObject", 
        "DrawingDimH@ AsDimH()", 
        asMETHOD(CDrawingObject,AsDimH), asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("DrawingObject", 
        "DrawingDimV@ AsDimV()", 
        asMETHOD(CDrawingObject,AsDimV), asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("DrawingObject", 
        "DrawingDimA@ AsDimA()", 
        asMETHOD(CDrawingObject,AsDimA), asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("DrawingObject", 
        "DrawingDimD@ AsDimD()", 
        asMETHOD(CDrawingObject,AsDimD), asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("DrawingObject", 
        "DrawingDimR@ AsDimR()", 
        asMETHOD(CDrawingObject,AsDimR), asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("DrawingObject", 
        "DrawingDimC@ AsDimC()", 
        asMETHOD(CDrawingObject,AsDimC), asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("DrawingObject", 
        "DrawingImage@ AsImage()", 
        asMETHOD(CDrawingObject,AsImage), asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("DrawingObject", 
        "DrawingCompositionLine@ AsCompositionLine()", 
        asMETHOD(CDrawingObject,AsCompositionLine), asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("DrawingObject", 
        "DrawingReference@ AsReference()", 
        asMETHOD(CDrawingObject,AsReference), asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("DrawingObject", 
        "DrawingGroup@ AsGroup()", 
        asMETHOD(CDrawingObject,AsGroup), asCALL_THISCALL); assert( r >= 0 );

    return 0;
}
