/**
 * @brief			ScriptDrawingParts実装ファイル
 * @file			ScriptDrawingParts.cpp
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:36
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include <assert.h>
#include <sstream>
#include "ScriptBase.h"
#include "ScriptDrawingParts.h"
#include "Utility/CUtility.h"
#include "DrawingObject/Primitive/SPLINE2D.H"
#include "DrawingObject/CDrawingParts.h"

/*---------------------------------------------------*/
/*  MACROS                                           */
/*---------------------------------------------------*/
using namespace std;
#ifdef _DEBUG
#undef new 
#endif
extern bool CALLBACK  ConvPOINT2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);


static void ConstructDrawingParts(CDrawingParts *thisPointer)
{
	new(thisPointer) CDrawingParts();
}

#ifdef _DEBUG
#undef new 
#define new DEBUG_NEW
#endif
static void DestructDrawingParts(CDrawingParts *thisPointer)
{
	thisPointer->~CDrawingParts();
}

CDrawingParts* CreateDrawingParts()
{
    return new CDrawingParts();
}

bool CALLBACK ConvCDrawingParts( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("DrawingParts");
    const CDrawingParts* pObj = reinterpret_cast<const CDrawingParts*>(pValAddress);

    //===========================
    // DrawingObject共通
    //===========================
    VAL_DATA valData;
    ConvDrawingObject( &valData, pObj, pContext);
    pValData->lstVal.push_back(valData);
    pValData->strVal += valData.strVal;
    pValData->strVal += _T(",");

    /*
    VAL_DATA valX;
    valX.strName     = _T("x");
    valX.strTypeName = _T("double");
    valX.strVal = CUtil::StrFormat(_T("%f"), pPt2D->dX);
    pValData->lstVal.push_back(valX);

    VAL_DATA valY;
    valY.strName     = _T("y");
    valY.strTypeName = _T("double");
    valY.strVal = CUtil::StrFormat(_T("%f"), pPt2D->dY);
    pValData->lstVal.push_back(valY);
    */

    return true;
}

//---------------------------------------------------
static CDrawingParts *DefaultFactoryDrawingParts()
{
    CDrawingParts *pRet;
    pRet = new CDrawingParts();
    CAsRefCtrl::GetInstance()->AddAsObj(pRet);
    return pRet;
}

static CDrawingParts *CopyFactoryDrawingParts(const CDrawingParts &other)
{
    CDrawingParts *pRet;
    pRet = new CDrawingParts(other);
    CAsRefCtrl::GetInstance()->AddAsObj(pRet);
    return pRet;
}


//DrawingParts型登録
int RegisterDrawingPartsType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
    int iRet = engine->RegisterObjectType( "DrawingParts", 0, asOBJ_REF); assert( iRet >= 0 );  

    //ウォッチリスト表示用登録
    mapIdFunc[iRet] = ConvCDrawingParts;
    return iRet;
}

//DrawingParts登録
int RegisterDrawingParts(asIScriptEngine *engine)
{
    RegsterRef<CDrawingParts>(engine, "DrawingParts");
    RegsterBasicMethod<CDrawingParts>(engine, "DrawingParts");
    RegsterDrawingObject<CDrawingParts>(engine, "DrawingParts");
    RegisterDrawingScriptBaseCommon<CDrawingParts>(engine,"DrawingParts");
    RegisterDrawingPartsCommon<CDrawingParts>(engine,"DrawingParts");


    ///////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////
    // FOR TEST  現在 CDrawingScriptBaseに実装
    // この関数は自分以外のオブジェクトを出力するので注意が必要
    // →FIELD専用としたい
    int r;


    // キャスト
    //DrawingParts -> DrawingObject へのキャストは 自動
    r = engine->RegisterObjectBehaviour("DrawingParts", asBEHAVE_IMPLICIT_REF_CAST, 
        "DrawingObject@ f()", 
        asFUNCTION((ASRefCast<CDrawingObject, CDrawingParts>)), 
        asCALL_CDECL_OBJLAST);
    assert( r>=0 );


    //DrawingObject->  DrawingLine へのキャストは キャスト演算子が必要
    r = engine->RegisterObjectBehaviour("DrawingObject", asBEHAVE_REF_CAST , 
        "DrawingParts@ f()", 
        asFUNCTION((ASRefCast<CDrawingParts, CDrawingObject>)), 
        asCALL_CDECL_OBJLAST);
    assert( r>=0 );

    return 0;
}


END_AS_NAMESPACE
