/**
 * @brief			ScriptDrawingPoint実装ファイル
 * @file			ScriptDrawingPoint.cpp
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:36
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include <assert.h>
#include <sstream>
#include "ScriptBase.h"
#include "ScriptDrawingPoint.h"
#include "Utility/CUtility.h"
#include "DrawingObject/Primitive/SPLINE2D.H"
#include "DrawingObject/CDrawingPoint.h"


/*---------------------------------------------------*/
/*  MACROS                                           */
/*---------------------------------------------------*/
using namespace std;
#ifdef _DEBUG
#undef new 
#endif
extern bool CALLBACK ConvPOINT2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);
extern bool CALLBACK ConvLINE2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);


static void ConstructDrawingPoint (CDrawingPoint *thisPointer)
{
	new(thisPointer) CDrawingPoint();
}

#ifdef _DEBUG
#undef new 
#define new DEBUG_NEW
#endif

static void DestructDrawingParts(CDrawingPoint *thisPointer)
{
	thisPointer->~CDrawingPoint();
}

CDrawingPoint* CreateDrawingPoint()
{
    return new CDrawingPoint();
}

bool CALLBACK ConvDrawingPoint( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("DrawingPoint");
    const CDrawingPoint* pObj = reinterpret_cast<const CDrawingPoint*>(pValAddress);

    pValData->strVal = _T("{");

    //===========================
    // DrawingObject共通
    //===========================
    VAL_DATA valData;
    ConvDrawingObject( &valData, pObj, pContext);
    pValData->lstVal.push_back(valData);
    pValData->strVal += valData.strVal;
    pValData->strVal += _T(",");

    //===========================
    pValData->strVal += _T("Pos");
    ConvPOINT2D( &valData, pObj->GetPointInstance(), pContext);
    pValData->lstVal.push_back(valData);
    pValData->strVal +=  valData.strVal;



    pValData->strVal += _T("}");

    return true;
}

//DrawingPoint型登録
int RegisterDrawingPointType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
    int iRet = engine->RegisterObjectType( "DrawingPoint", 0, asOBJ_REF); assert( iRet >= 0 );  

    //ウォッチリスト表示用登録
    mapIdFunc[iRet] = ConvDrawingPoint;
    return iRet;
}

//DrawingPoint登録
int RegisterDrawingPoint(asIScriptEngine *engine)
{
    RegsterRef<CDrawingPoint>(engine, "DrawingPoint");
    RegsterBasicMethod<CDrawingPoint>(engine, "DrawingPoint");
    RegsterDrawingObject<CDrawingPoint>(engine, "DrawingPoint");

    //-------------------------------------------------
    //  DRAWING OBJECT共通
    int r;
    

    //-------------------------------------------------

    //!< 位置設定
	r = engine->RegisterObjectMethod("DrawingPoint", 
        "void SetPoint(POINT2D &in)", 
        asMETHODPR(CDrawingPoint, SetPoint,(const POINT2D&), void),
        asCALL_THISCALL); assert( r >= 0 );

    //!< 位置取得
	r = engine->RegisterObjectMethod("DrawingPoint", 
        "POINT2D& GetPoint()", 
        asMETHODPR(CDrawingPoint, GetPoint,(), POINT2D&),
        asCALL_THISCALL); assert( r >= 0 );


    // キャスト
    //DrawingPoint -> DrawingObject へのキャストは 自動
    r = engine->RegisterObjectBehaviour("DrawingPoint", asBEHAVE_IMPLICIT_REF_CAST, 
        "DrawingObject@ f()", 
        asFUNCTION((ASRefCast<CDrawingObject, CDrawingPoint>)), 
        asCALL_CDECL_OBJLAST);
    assert( r>=0 );

    //DrawingObject->  DrawingPoint へのキャストは キャスト演算子が必要
    r = engine->RegisterObjectBehaviour("DrawingObject", asBEHAVE_REF_CAST , 
        "DrawingPoint@ f()", 
        asFUNCTION((ASRefCast<CDrawingPoint, CDrawingObject>)), 
        asCALL_CDECL_OBJLAST);
    assert( r>=0 );



    return 0;
}
