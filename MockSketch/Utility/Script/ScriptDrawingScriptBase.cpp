/**
 * @brief			ScriptDrawingScriptBase実装ファイル
 * @file			ScriptDrawingScriptBase.cpp
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:36
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include <assert.h>
#include <sstream>
#include "ScriptBase.h"
#include "ScriptDrawingScriptBase.h"
#include "Utility/CUtility.h"
#include "DrawingObject/Primitive/SPLINE2D.H"
#include "DrawingObject/CDrawingScriptBase.h"

/*---------------------------------------------------*/
/*  MACROS                                           */
/*---------------------------------------------------*/
using namespace std;
#ifdef _DEBUG
#undef new 
#endif
extern bool CALLBACK  ConvPOINT2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);

extern  void CalcIntersection ( std::vector<POINT2D>& list,   
                                const CDrawingObject* pObj, bool bOnLine,  CDrawingObject& obj);

//!< 交点計算(点)
extern  void CalcIntersectionPoint ( std::vector<POINT2D>& list,   
                                const CDrawingObject* pObj, POINT2D ptPos, bool bOnLine, CDrawingObject& obj);

static void ConstructDrawingScriptBase(CDrawingScriptBase *thisPointer)
{
	new(thisPointer) CDrawingScriptBase();
}

#ifdef _DEBUG
#undef new 
#define new DEBUG_NEW
#endif
static void DestructDrawingScriptBase(CDrawingScriptBase *thisPointer)
{
	thisPointer->~CDrawingScriptBase();
}


CDrawingScriptBase* CreateDrawingScriptBase()
{
    return new CDrawingScriptBase();
}

bool CALLBACK  ConvDrawingScriptBase( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("DrawingScriptBase");
    const CDrawingScriptBase* pObj = reinterpret_cast<const CDrawingScriptBase*>(pValAddress);
    //===========================
    // DrawingObject共通
    //===========================
    VAL_DATA valData;
    ConvDrawingObject( &valData, pObj, pContext);
    pValData->lstVal.push_back(valData);
    pValData->strVal += valData.strVal;
    pValData->strVal += _T(",");

    return true;
}

//DrawingScriptBase型登録
int RegisterDrawingScriptBaseType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
    int iRet = engine->RegisterObjectType( "DrawingScriptBase", 0, asOBJ_REF); assert( iRet >= 0 );  

    //ウォッチリスト表示用登録
    mapIdFunc[iRet] = ConvDrawingScriptBase;
    return iRet;
}



//DrawingScriptBase登録
int RegisterDrawingScriptBase(asIScriptEngine *engine)
{

	int r;

	// Register the object operator overloads
    // スクリプト側で生成する必要がないのでファクトリ関数は不要
    r = engine->RegisterObjectBehaviour("DrawingScriptBase", asBEHAVE_ADDREF, "void f()", asMETHOD(CDrawingScriptBase,AddRef), asCALL_THISCALL);
    r = engine->RegisterObjectBehaviour("DrawingScriptBase", asBEHAVE_RELEASE, "void f()", asMETHOD(CDrawingScriptBase,Release), asCALL_THISCALL);

    //REGSTER_BASIC_METHOD2(CDrawingScriptBase, DrawingScriptBase);
    RegsterBasicMethod<CDrawingScriptBase>(engine,"DrawingScriptBase");

    //REGSTER_DRAWING_OBJECT(CDrawingScriptBase, DrawingScriptBase);
    RegsterDrawingObject<CDrawingScriptBase>(engine,"DrawingScriptBase");

    //REGSTER_DRAWING_SCRIPT_BASE(CDrawingScriptBase, DrawingScriptBase);
    RegisterDrawingScriptBaseCommon<CDrawingScriptBase>(engine,"DrawingScriptBase");






    // キャスト
    //DrawingScriptBase -> DrawingObject へのキャストは 自動
    r = engine->RegisterObjectBehaviour("DrawingScriptBase", asBEHAVE_IMPLICIT_REF_CAST, 
        "DrawingObject@ f()", 
        asFUNCTION((ASRefCast<CDrawingObject, CDrawingScriptBase>)), 
        asCALL_CDECL_OBJLAST);
    assert( r>=0 );

    //DrawingObject->  DrawingScriptBase へのキャストは キャスト演算子が必要
    r = engine->RegisterObjectBehaviour("DrawingObject", asBEHAVE_REF_CAST , 
        "DrawingScriptBase@ f()", 
        asFUNCTION((ASRefCast<CDrawingScriptBase, CDrawingObject>)), 
        asCALL_CDECL_OBJLAST);
    assert( r>=0 );
    
    
    /*

    //コントロール名取得
	r = engine->RegisterObjectMethod("DrawingScriptBase", 
        "StdString GetName() const", 
        asMETHOD(CPartsDef,GetName), 
        asCALL_THISCALL); assert( r >= 0 );

    //!< オブジェクト取得(名称)
	r = engine->RegisterObjectMethod("DrawingScriptBase", 
        "DrawingObject@ GetObject(StdString)", 
         asMETHOD(CPartsDef,GetObjectByName), 
        asCALL_THISCALL); assert( r >= 0 );

    //!< 描画
	r = engine->RegisterObjectMethod("DrawingScriptBase", 
        "void Draw() const", 
        asMETHOD(CPartsDef,Draw), 
        asCALL_THISCALL); assert( r >= 0 );

    //!< データ追加
	r = engine->RegisterObjectMethod("DrawingScriptBase", 
        "void AddObject(DrawingObject &in)", 
        asMETHOD(CPartsDef,AddObject2), 
        asCALL_THISCALL); assert( r >= 0 );

    //!< オブジェクト選択(名称)
	r = engine->RegisterObjectMethod("DrawingScriptBase", 
        "void SelectObject(StdString)", 
         asMETHOD(CPartsDef,SelectObjectByName), 
        asCALL_THISCALL); assert( r >= 0 );

    //!< 選択オブジェクト削除
	r = engine->RegisterObjectMethod("DrawingScriptBase", 
        "void DeleteSelectObject()", 
         asMETHOD(CPartsDef,DeleteObject), 
        asCALL_THISCALL); assert( r >= 0 );

    //!< 選択解除
    void RelSelect();
	r = engine->RegisterObjectMethod("DrawingScriptBase", 
        "void ReleaseSelect()", 
         asMETHOD(CPartsDef,RelSelect), 
        asCALL_THISCALL); assert( r >= 0 );
    */
    return 0;

}


END_AS_NAMESPACE
