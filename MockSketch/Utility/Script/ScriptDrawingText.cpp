/**
 * @brief			ScriptDrawingText実装ファイル
 * @file			ScriptDrawingText.cpp
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:36
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include <assert.h>
#include <sstream>
#include "ScriptBase.h"
#include "ScriptDrawingText.h"
#include "Utility/CUtility.h"
#include "DrawingObject/Primitive/SPLINE2D.H"
#include "DrawingObject/CDrawingText.h"


/*---------------------------------------------------*/
/*  MACROS                                           */
/*---------------------------------------------------*/
using namespace std;
#ifdef _DEBUG
#undef new 
#endif
extern bool CALLBACK ConvPOINT2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);
extern bool CALLBACK ConvLINE2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);


static void ConstructDrawingText (CDrawingText *thisPointer)
{
	new(thisPointer) CDrawingText();
}

#ifdef _DEBUG
#undef new 
#endif

static void DestructDrawingParts(CDrawingText *thisPointer)
{
	thisPointer->~CDrawingText();
}

CDrawingText* CreateDrawingText()
{
    return new CDrawingText();
}

bool CALLBACK ConvDrawingText( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("DrawingText");
    const CDrawingText* pObj = reinterpret_cast<const CDrawingText*>(pValAddress);

    pValData->strVal = _T("{");

    //===========================
    // DrawingObject共通
    //===========================
    VAL_DATA valData;
    ConvDrawingObject( &valData, pObj, pContext);
    pValData->lstVal.push_back(valData);
    pValData->strVal += valData.strVal;
    pValData->strVal += _T(",");

    //===========================
    //Text



    pValData->strVal += _T("}");

    return true;
}

//DrawingText型登録
int RegisterDrawingTextType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
    int iRet = engine->RegisterObjectType( "DrawingText", 0, asOBJ_REF); assert( iRet >= 0 );  

    //ウォッチリスト表示用登録
    mapIdFunc[iRet] = ConvDrawingText;
    return iRet;
}

//DrawingText登録
int RegisterDrawingText(asIScriptEngine *engine)
{
    RegsterRef<CDrawingText>(engine, "DrawingText");
    RegsterBasicMethod<CDrawingText>(engine, "DrawingText");

    //-------------------------------------------------
    //  DRAWING OBJECT共通
    int r;

    //!< オブジェクト種別
	r = engine->RegisterObjectMethod("DrawingText", 
        "DRAWING_TYPE GetType() const", 
        asMETHOD(CDrawingText,GetType), 
        asCALL_THISCALL); assert( r >= 0 );

    //!< ID取得
	r = engine->RegisterObjectMethod("DrawingText", 
        "int GetId() const", 
        asMETHOD(CDrawingText,GetId), 
        asCALL_THISCALL); assert( r >= 0 );


    //!< 色設定
    //virtual void SetColor(COLORREF color){m_crObj = color;}

    //!< 色取得
    //COLORREF GetColor() const         {return m_crObj;}

    //!< レイヤーID設定
	r = engine->RegisterObjectMethod("DrawingText", 
        "void SetLayer(int)", 
        asMETHOD(CDrawingText,SetLayer), 
        asCALL_THISCALL); assert( r >= 0 );


    //!< レイヤーID取得
	r = engine->RegisterObjectMethod("DrawingText", 
        "int GetLayer() const", 
        asMETHOD(CDrawingText,GetLayer), 
        asCALL_THISCALL); assert( r >= 0 );


    //!< データ名設定

    //!< データ名設定
	r = engine->RegisterObjectMethod("DrawingText", 
        "void SetName(string)", 
        asMETHOD(CDrawingText,SetNameStr), 
        asCALL_THISCALL); assert( r >= 0 );


    //!< データ名取得
	r = engine->RegisterObjectMethod("DrawingText", 
        "string GetName() const", 
        asMETHOD(CDrawingText,GetName), 
        asCALL_THISCALL); assert( r >= 0 );

    //!< ダウンキャスト
	r = engine->RegisterObjectMethod("DrawingText", 
        "DrawingObject@ GetDrawingObject()",
        asMETHODPR(CDrawingText, GetDrawingObject,(void), CDrawingObject*),
        asCALL_THISCALL); assert( r >= 0 );

    //-------------------------------------------------

    //!< 位置設定
	r = engine->RegisterObjectMethod("DrawingText", 
        "void SetPoint(double, double)", 
        asMETHOD(CDrawingText,SetPointDbl), 
        asCALL_THISCALL); assert( r >= 0 );

    //!< 位置設定
	r = engine->RegisterObjectMethod("DrawingText", 
        "void SetPoint(POINT2D &in)", 
        asMETHOD(CDrawingText,SetPoint), 
        asCALL_THISCALL); assert( r >= 0 );

    //!< 位置取得
	r = engine->RegisterObjectMethod("DrawingText", 
        "POINT2D& GetPoint()", 
        asMETHOD(CDrawingText,GetPoint), 
        asCALL_THISCALL); assert( r >= 0 );

    //!< テキスト取得
	r = engine->RegisterObjectMethod("DrawingText", 
        "string& GetText()", 
        asMETHOD(CDrawingText,GetText), 
        asCALL_THISCALL); assert( r >= 0 );

    //!< テキスト設定
	r = engine->RegisterObjectMethod("DrawingText", 
        "void SetText(string &in )", 
        asMETHOD(CDrawingText,SetText), 
        asCALL_THISCALL); assert( r >= 0 );
#if 0
    //!< テキスト設定
	r = engine->RegisterObjectMethod("DrawingText", 
        "void SetText(string &in)", 
        asMETHOD(CDrawingText,SetTextS), 
        asCALL_THISCALL); assert( r >= 0 );
#endif

    // キャスト
    //DrawingLine -> DrawingObject へのキャストは 自動
    r = engine->RegisterObjectBehaviour("DrawingText", asBEHAVE_IMPLICIT_REF_CAST, 
        "DrawingObject@ f()", 
        asFUNCTION((ASRefCast<CDrawingObject, CDrawingText>)), 
        asCALL_CDECL_OBJLAST);
    assert( r>=0 );

    //DrawingObject->  DrawingLine へのキャストは キャスト演算子が必要
    r = engine->RegisterObjectBehaviour("DrawingObject", asBEHAVE_REF_CAST , 
        "DrawingText@ f()", 
        asFUNCTION((ASRefCast<CDrawingText, CDrawingObject>)), 
        asCALL_CDECL_OBJLAST);
    assert( r>=0 );



    return 0;
}
