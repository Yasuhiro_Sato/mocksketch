/**
 * @brief			ScriptDrawingView実装ファイル
 * @file			ScriptDrawingView.cpp
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:36
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include <assert.h>
#include <sstream>
#include "ScriptBase.h"
#include "ScriptDrawingView.h"
#include "Utility/CUtility.h"
#include "DrawingObject/Primitive/SPLINE2D.H"
#include "DefinitionObject/View/CDrawingView.h"

/*---------------------------------------------------*/
/*  MACROS                                           */
/*---------------------------------------------------*/
using namespace std;
#ifdef _DEBUG
#undef new 
#define new DEBUG_NEW
#endif
extern bool CALLBACK  ConvPOINT2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);

int RegisterDrawingViewType         (asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc);


bool CALLBACK ConvDrawingViewType( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("DrawingView");
    const CDrawingView* pObj = reinterpret_cast<const CDrawingView*>(pValAddress);

    pValData->strVal = _T("{");
    //TODO:実装

    pValData->strVal = _T("}");

    return true;
}

//DrawingView型登録
int RegisterDrawingViewType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
    int iRet = engine->RegisterObjectType( "DrawingView", 0, asOBJ_REF); assert( iRet >= 0 );  

    //ウォッチリスト表示用登録
    mapIdFunc[iRet] = ConvDrawingViewType;
    return iRet;
}

//DrawingView登録
int RegisterDrawingView(asIScriptEngine *engine)
{
	int r;
	// Register the object operator overloads
    // スクリプト側で生成する必要がないのでファクトリ関数は不要
    r = engine->RegisterObjectBehaviour("DrawingView", asBEHAVE_ADDREF, "void f()", asMETHOD(CDrawingView,AddRef), asCALL_THISCALL);
    r = engine->RegisterObjectBehaviour("DrawingView", asBEHAVE_RELEASE, "void f()", asMETHOD(CDrawingView,Release), asCALL_THISCALL);


    //点描画
	r = engine->RegisterObjectMethod("DrawingView", 
        "void Point(POINT& in)", 
        asMETHOD(CDrawingView,Point), 
        asCALL_THISCALL); assert( r >= 0 );

    //線描画
	r = engine->RegisterObjectMethod("DrawingView", 
        "void Line(POINT& in, POINT& in)", 
        asMETHOD(CDrawingView,Line), 
        asCALL_THISCALL); assert( r >= 0 );

    //楕円描画
	r = engine->RegisterObjectMethod("DrawingView", 
        "void Ellipse(RECT& in)", 
        asMETHOD(CDrawingView,Ellipse), 
        asCALL_THISCALL); assert( r >= 0 );

    //円弧描画
	r = engine->RegisterObjectMethod("DrawingView", 
        "void Arc(RECT& in, POINT& in, POINT& in)", 
        asMETHOD(CDrawingView,Arc), 
        asCALL_THISCALL); assert( r >= 0 );


    //ドラッグ用マーカ取得
	r = engine->RegisterObjectMethod("DrawingView", 
        "NodeMarker@ GetNodeMarker()", 
        asMETHODPR(CDrawingView,GetNodeMarker,(), CNodeMarker*), 
        asCALL_THISCALL); assert( r >= 0 );


    
    return 0;
}

END_AS_NAMESPACE
