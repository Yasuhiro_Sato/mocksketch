/**
 * @brief			ScriptDrawingView
 * @file			ScriptDrawingView.h
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:37
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#ifndef SCRIPT_DRAWING_VIEW_H
#define SCRIPT_DRAWING_VIEW_H

#include <angelscript.h>
#include <string>
#include "VAL_DATA.h"

int RegisterDrawingViewType (asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc);
int RegisterDrawingView (asIScriptEngine *engine);



END_AS_NAMESPACE

#endif
