/**
 * @brief			ScriptELLIPSE2D実装ファイル
 * @file			ScriptELLIPSE2D.cpp
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:36
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include <assert.h>
#include <sstream>
#include "ScriptBase.h"
#include "ScriptELLIPSE2D.h"
#include "Utility/CUtility.h"
#include "DrawingObject/Primitive/SPLINE2D.H"
#include "DrawingObject/Primitive/ELLIPSE2D.h"

/*---------------------------------------------------*/
/*  MACROS                                           */
/*---------------------------------------------------*/
using namespace std;
#ifdef _DEBUG
#undef new 
#define new DEBUG_NEW
#endif
extern bool CALLBACK ConvPOINT2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);

bool CALLBACK ConvELLIPSE2D( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("ELLIPSE2D");
    const ELLIPSE2D* pEllipse2D = reinterpret_cast<const ELLIPSE2D*>(pValAddress);
    StdStringStream strmVal; 

    strmVal << _T("{");
    
    VAL_DATA valCenter;
    valCenter.strName     = _T("Center");
    ConvPOINT2D( &valCenter, &pEllipse2D->GetCenter(), pContext);
    pValData->lstVal.push_back(valCenter);
    strmVal << valCenter.strVal;

    strmVal << valCenter.strVal, _T(",");
 
    VAL_DATA valRad1;
    valRad1.strName     = _T("Radius1");
    valRad1.strTypeName = _T("double");
    valRad1.strVal = CUtil::StrFormat(_T("%f"), pEllipse2D->GetRadius1());
    pValData->lstVal.push_back(valRad1);
    strmVal << valRad1.strVal, _T(",");

    VAL_DATA valRad2;
    valRad2.strName     = _T("Radius2");
    valRad2.strTypeName = _T("double");
    valRad2.strVal = CUtil::StrFormat(_T("%f"), pEllipse2D->GetRadius2());
    pValData->lstVal.push_back(valRad2);
    strmVal << valRad2.strVal, _T(",");
 
    VAL_DATA valAngle;
    valAngle.strName     = _T("Angle");
    valAngle.strTypeName = _T("double");
    valAngle.strVal = CUtil::StrFormat(_T("%f"), pEllipse2D->GetAngle());
    pValData->lstVal.push_back(valAngle);
    strmVal << valAngle.strVal, _T(",");

    VAL_DATA valStartAngle;
    valStartAngle.strName     = _T("StartAngle");
    valStartAngle.strTypeName = _T("double");
    valStartAngle.strVal = CUtil::StrFormat(_T("%f"), pEllipse2D->GetStart());
    pValData->lstVal.push_back(valStartAngle);
    strmVal << valStartAngle.strVal, _T(",");

    VAL_DATA valEndAnle;
    valEndAnle.strName     = _T("EndAnle");
    valEndAnle.strTypeName = _T("double");
    valEndAnle.strVal = CUtil::StrFormat(_T("%f"), pEllipse2D->GetEnd());
    pValData->lstVal.push_back(valEndAnle);
    strmVal << valEndAnle.strVal;
    strmVal << _T("}");
 
    pValData->strVal = strmVal.str();
    return true;
}

//!< コンストラクタ (中心と半径)
static ELLIPSE2D* FactoryELLIPSE2D_Center_Rad(const POINT2D& pt, 
                                            double dRad1,
                                            double dRad2,
                                            double dAngle)
{                                                                
    ELLIPSE2D *pRet;                                        
    pRet = new ELLIPSE2D( pt, dRad1, dRad2, dAngle);
    CAsRefCtrl::GetInstance()->AddAsObj(pRet);
	return pRet;
}

//!< 楕円と直線との交点
static bool ELLIPSE2D_IntersectLine( std::vector<POINT2D>& list,   
                                   const LINE2D& line,  bool bOnLine, ELLIPSE2D& obj)
{
    //TODO:テスト
    return obj.Intersect(line, &list, bOnLine);
}

//!< 楕円と円との交点
static bool ELLIPSE2D_IntersectCircle( std::vector<POINT2D>& list,   
                                   const CIRCLE2D& circle,  bool bOnLine, ELLIPSE2D& obj)
{
    //TODO:テスト
    return obj.Intersect(circle, &list, bOnLine);
}

//!< 楕円とスプラインとの交点
static bool ELLIPSE2D_IntersectSpline( std::vector<POINT2D>& list,   
                                   const SPLINE2D& spline,  bool bOnLine, ELLIPSE2D& obj)
{
    //TODO:テスト
    return obj.Intersect(spline, &list, bOnLine);
}

//!< 楕円と楕円ととの交点
static bool ELLIPSE2D_IntersectEllipse( std::vector<POINT2D>& list,   
                                   const ELLIPSE2D& ellipse,  bool bOnLine, ELLIPSE2D& obj)
{
    //TODO:テスト
    return obj.Intersect(ellipse, &list, bOnLine);
}

// 一点を通る接線上の点
static bool ELLIPSE2D_TangentPoint( std::vector<POINT2D>& list,   
                                   const POINT2D& pt, ELLIPSE2D& obj)
{
    //TODO:テスト
    return obj.TangentPoint( &list, pt);
}


//ELLIPSE2D型登録
int RegisterELLIPSE2DType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
    int iRet = engine->RegisterObjectType( "ELLIPSE2D", 0, asOBJ_REF); assert( iRet >= 0 );  

    //ウォッチリスト表示用登録
    mapIdFunc[iRet] = ConvELLIPSE2D;
    return iRet;
}

//ELLIPSE2D登録
int RegisterELLIPSE2D(asIScriptEngine *engine)
{
    RegsterRef<ELLIPSE2D>(engine, "ELLIPSE2D");

    int r;
    //!< 中心と半径による生成
    r = engine->RegisterObjectBehaviour("ELLIPSE2D", asBEHAVE_FACTORY, 
        "ELLIPSE2D @f(const POINT2D& in, double, double, double)",
        asFUNCTION( FactoryELLIPSE2D_Center_Rad), asCALL_CDECL); 
        STD_ASSERT( r >= 0 );

    //!< 近接点
    r = engine->RegisterObjectMethod("ELLIPSE2D", 
        "POINT2D NearPoint(const POINT2D& in, bool)",
        asMETHODPR(ELLIPSE2D, NearPoint, (const POINT2D& , bool)const, 
        POINT2D), asCALL_THISCALL); assert( r >= 0 );

    //!< 交点
    r = engine->RegisterObjectMethod("ELLIPSE2D",
        "bool CalcIntersection(POINT2D_VECTOR& out,const LINE2D& in, bool)",
        asFUNCTION(ELLIPSE2D_IntersectLine), asCALL_CDECL_OBJLAST);
        COMMENT_ASSERT( r >= 0, _T("CalcIntersection") );

    r = engine->RegisterObjectMethod("ELLIPSE2D",
        "bool CalcIntersection(POINT2D_VECTOR& out,const CIRCLE2D& in, bool)",
        asFUNCTION(ELLIPSE2D_IntersectCircle), asCALL_CDECL_OBJLAST);
        COMMENT_ASSERT( r >= 0, _T("CalcIntersection") );

    r = engine->RegisterObjectMethod("ELLIPSE2D",
        "bool CalcIntersection(POINT2D_VECTOR& out,const SPLINE2D& in, bool)",
        asFUNCTION(ELLIPSE2D_IntersectSpline), asCALL_CDECL_OBJLAST);
        COMMENT_ASSERT( r >= 0, _T("CalcIntersection") );

    r = engine->RegisterObjectMethod("ELLIPSE2D",
        "bool CalcIntersection(POINT2D_VECTOR& out,const ELLIPSE2D& in, bool)",
        asFUNCTION(ELLIPSE2D_IntersectEllipse), asCALL_CDECL_OBJLAST);
        COMMENT_ASSERT( r >= 0, _T("CalcIntersection") );

    // 一点を通る接線上の点
    r = engine->RegisterObjectMethod("ELLIPSE2D",
        "bool TangentPoint(POINT2D_VECTOR& out,const POINT2D& in)",
        asFUNCTION(ELLIPSE2D_TangentPoint), asCALL_CDECL_OBJLAST);
        COMMENT_ASSERT( r >= 0, _T("TangentPoint") );

    // 中心点取得
    r = engine->RegisterObjectMethod("ELLIPSE2D", 
        "POINT2D& GetCenter()",
        asMETHOD(ELLIPSE2D, GetCenter), asCALL_THISCALL);
        STD_ASSERT( r >= 0 );

    // 中心点設定
    r = engine->RegisterObjectMethod("ELLIPSE2D", 
        "void SetCenter(const POINT2D& in)",
        asMETHOD(ELLIPSE2D, SetCenter), asCALL_THISCALL);
        STD_ASSERT( r >= 0 );

    // 長径取得
    r = engine->RegisterObjectMethod("ELLIPSE2D", 
        "double GetRadius1()",
        asMETHOD(ELLIPSE2D, GetRadius1), asCALL_THISCALL);
        STD_ASSERT( r >= 0 );

    // 長径設定
    r = engine->RegisterObjectMethod("ELLIPSE2D", 
        "void SetRadius1(double)",
        asMETHOD(ELLIPSE2D, SetRadius1), asCALL_THISCALL);
        STD_ASSERT( r >= 0 );

    // 短径取得
    r = engine->RegisterObjectMethod("ELLIPSE2D", 
        "double GetRadius2()",
        asMETHOD(ELLIPSE2D, GetRadius2), asCALL_THISCALL);
        STD_ASSERT( r >= 0 );

    // 短径設定
    r = engine->RegisterObjectMethod("ELLIPSE2D", 
        "void SetRadius2(double)",
        asMETHOD(ELLIPSE2D, SetRadius2), asCALL_THISCALL);
        STD_ASSERT( r >= 0 );

    // 角度取得
    r = engine->RegisterObjectMethod("ELLIPSE2D", 
        "double GetAngle()",
        asMETHOD(ELLIPSE2D, GetAngle), asCALL_THISCALL);
        STD_ASSERT( r >= 0 );

    // 角度設定
    r = engine->RegisterObjectMethod("ELLIPSE2D", 
        "void SetAngle(double)",
        asMETHOD(ELLIPSE2D, SetAngle), asCALL_THISCALL);
        STD_ASSERT( r >= 0 );

    // 開始角取得
    r = engine->RegisterObjectMethod("ELLIPSE2D", 
        "double GetStartAngle()",
        asMETHOD(ELLIPSE2D, GetStart), asCALL_THISCALL);
        STD_ASSERT( r >= 0 );

    // 開始角設定
    r = engine->RegisterObjectMethod("ELLIPSE2D", 
        "void SetStartAngle(double)",
        asMETHOD(ELLIPSE2D, SetStart), asCALL_THISCALL);
        STD_ASSERT( r >= 0 );

    // 終了角取得
    r = engine->RegisterObjectMethod("ELLIPSE2D", 
        "double GetEndAngle()",
        asMETHOD(ELLIPSE2D, GetEnd), asCALL_THISCALL);
        STD_ASSERT( r >= 0 );

    // 終了角設定
    r = engine->RegisterObjectMethod("ELLIPSE2D", 
        "void SetEndAngle(double)",
        asMETHOD(ELLIPSE2D, SetEnd), asCALL_THISCALL);
        STD_ASSERT( r >= 0 );

    // 円弧上の点
    r = engine->RegisterObjectMethod("ELLIPSE2D", 
        "POINT2D& GetPoint(double)",
        asMETHOD(ELLIPSE2D, GetPoint), asCALL_THISCALL);
        STD_ASSERT( r >= 0 );

    //ELLIPSE2D& operator = (const double & m);
    r = engine->RegisterObjectMethod("ELLIPSE2D",
        "ELLIPSE2D &opAssign(const ELLIPSE2D &in)"   ,
        asMETHODPR(ELLIPSE2D, operator=, (const ELLIPSE2D &), 
        ELLIPSE2D&), asCALL_THISCALL); assert( r >= 0 );

    //bool operator == (const ELLIPSE2D & m); 
    r = engine->RegisterObjectMethod("ELLIPSE2D", 
        "bool opEquals(const ELLIPSE2D &in) const"    , 
        asMETHODPR(ELLIPSE2D, operator==, 
        (const ELLIPSE2D &) const, bool), 
        asCALL_THISCALL); assert( r >= 0 );
    
    return 0;
}

END_AS_NAMESPACE
