/**
 * @brief			ScriptMAT2Dΐt@C
 * @file			ScriptBase.cpp
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:36
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include <assert.h>
#include <sstream>
#include "ScriptBase.h"
#include "ScriptMAT2D.h"
#include "Utility/CUtility.h"
#include "DrawingObject/Primitive/POINT2D.h"
#include "IO/CIoRef.h"

/*---------------------------------------------------*/
/*  MACROS                                           */
/*---------------------------------------------------*/
using namespace std;
#ifdef _DEBUG
#undef new 
#define new DEBUG_NEW
#endif
extern bool CALLBACK ConvPOINT2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);

bool CALLBACK ConvIoRef( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("IoRef");
    const CIoRef* pIoRef = reinterpret_cast<const CIoRef*>(pValAddress);
    StdStringStream strmVal; 

    VAL_DATA val;
    val.strTypeName = _T("double");

    strmVal << _T("{");
    strmVal << _T("}");
    pValData->strVal = strmVal.str();
    return true;
}

//IoRef^o^
int RegisterIoRefType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
    int iRet = engine->RegisterObjectType( "IoRef", 0, asOBJ_REF); assert( iRet >= 0 );  

    //EHb`Xg\¦po^
    mapIdFunc[iRet] = ConvIoRef;
    return iRet;
}

//MAT2Do^
int RegisterIoRef(asIScriptEngine *engine)
{
    //int r;
    //REGSTER_REF(CIoRef);
/*
    //bool operator == (const IMat2D & m); (!= Νθ`svHj
    r = engine->RegisterObjectMethod("MAT2D", 
        "bool opEquals(const MAT2D &in) const"    , 
        asMETHODPR(MAT2D, operator==, 
        (const IMat2D &) const, bool), 
        asCALL_THISCALL); assert( r >= 0 );

    //MAT2D  operator * (const double & m);
	r = engine->RegisterObjectMethod("MAT2D", 
        "MAT2D& opMul(const double  &in)"   , 
        asMETHODPR(MAT2D, operator*, 
        (const double &)const, MAT2D), 
        asCALL_THISCALL); assert( r >= 0 );

    //MAT2D  operator * (const MAT2D & m);
	r = engine->RegisterObjectMethod("MAT2D", 
        "MAT2D& opMul(const MAT2D  &in)"   , 
        asMETHODPR(MAT2D, operator*, 
        (const MAT2D &)const, MAT2D), 
        asCALL_THISCALL); assert( r >= 0 );

    //MAT2D& operator = (const MAT2D & m);
	r = engine->RegisterObjectMethod("MAT2D", 
        "MAT2D &opAssign(const MAT2D &in)"   ,
        asMETHODPR(MAT2D, operator=, (const MAT2D &),
        MAT2D&), asCALL_THISCALL); assert( r >= 0 );

    //MAT2D& operator = (const double & m);
	r = engine->RegisterObjectMethod("MAT2D",
        "MAT2D &opAssign(const double  &in)"   ,
        asMETHODPR(MAT2D, operator=, (const double &), 
        MAT2D&), asCALL_THISCALL); assert( r >= 0 );

    //MAT2D  operator + (const MAT2D & m);
	r = engine->RegisterObjectMethod("MAT2D", 
        "MAT2D& opAdd(const MAT2D &in)"   , 
        asMETHODPR(MAT2D, operator+, 
        (const MAT2D &) const, MAT2D), 
        asCALL_THISCALL); assert( r >= 0 );

    //MAT2D  operator + (const double & m);
	r = engine->RegisterObjectMethod("MAT2D", 
        "MAT2D& opAdd(const double  &in)"   , 
        asMETHODPR(MAT2D, operator+, 
        (const double &) const, MAT2D), 
        asCALL_THISCALL); assert( r >= 0 );

    //MAT2D  operator - (const MAT2D & m);
	r = engine->RegisterObjectMethod("MAT2D", 
        "MAT2D& opSub(const MAT2D &in)"   , 
        asMETHODPR(MAT2D, operator-, 
        (const MAT2D &) const, MAT2D), 
        asCALL_THISCALL); assert( r >= 0 );

    //MAT2D  operator - (const double & m);
	r = engine->RegisterObjectMethod("MAT2D", 
        "MAT2D& opSub(const double  &in)"   , 
        asMETHODPR(MAT2D , operator-, 
        (const double &) const, MAT2D), 
        asCALL_THISCALL); assert( r >= 0 );

    //MAT2D  operator - ();
	r = engine->RegisterObjectMethod("MAT2D", 
        "MAT2D& opSub()"   , 
        asMETHODPR(MAT2D, operator-, 
        ()const, MAT2D), 
        asCALL_THISCALL); assert( r >= 0 );

    //l;
	r = engine->RegisterObjectMethod("MAT2D", 
        "double& Get(const int &in, const int &in)"   , 
        asMETHODPR(MAT2D, Val, 
        (size_t , size_t), double&), 
        asCALL_THISCALL); assert( r >= 0 );

    //Move
	r = engine->RegisterObjectMethod("MAT2D", 
        "void Move(const POINT2D &in)"   , 
        asMETHODPR(MAT2D, Move, 
        (const IPoint2D&), void), 
        asCALL_THISCALL); assert( r >= 0 );

    //Rotate
	r = engine->RegisterObjectMethod("MAT2D", 
        "void Rotate(const double &in)"   , 
        asMETHODPR(MAT2D, Rotate, 
        (double), void), 
        asCALL_THISCALL); assert( r >= 0 );

    //Rotate
	r = engine->RegisterObjectMethod("MAT2D", 
        "void Rotate(const POINT2D &in, const double &in)"   , 
        asMETHODPR(MAT2D, Rotate, 
        (const IPoint2D&, double), void), 
        asCALL_THISCALL); assert( r >= 0 );

    //Mirror
	r = engine->RegisterObjectMethod("MAT2D", 
        "void Mirror(const LINE2D &in)"   , 
        asMETHODPR(MAT2D, Mirror, 
        (const ILine2D&), void), 
        asCALL_THISCALL); assert( r >= 0 );

    //Scl
	r = engine->RegisterObjectMethod("MAT2D", 
        "void Scale(const double &in, const double &in)"   , 
        asMETHODPR(MAT2D, Scl, 
        (double, double), void), 
        asCALL_THISCALL); assert( r >= 0 );

    //Scl
	r = engine->RegisterObjectMethod("MAT2D", 
        "void Scale(const POINT2D &in, const double &in, const double &in)"   , 
        asMETHODPR(MAT2D, Scl, 
        (const IPoint2D&, double, double), void), 
        asCALL_THISCALL); assert( r >= 0 );
*/
    return 0;

}


END_AS_NAMESPACE
