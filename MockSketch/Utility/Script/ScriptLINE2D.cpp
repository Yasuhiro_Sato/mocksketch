/**
 * @brief			ScriptLINE2D実装ファイル
 * @file			ScriptLINE2D.cpp
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:36
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include <assert.h>
#include <sstream>
#include "ScriptBase.h"
#include "ScriptLINE2D.h"
#include "Utility/CUtility.h"
#include "DrawingObject/Primitive/SPLINE2D.H"
#include "DrawingObject/Primitive/LINE2D.h"

/*---------------------------------------------------*/
/*  MACROS                                           */
/*---------------------------------------------------*/
using namespace std;
#ifdef _DEBUG
#undef new 
#endif
extern bool CALLBACK ConvPOINT2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);

static void ConstructLINE2D(LINE2D *thisPointer)
{
	new(thisPointer) LINE2D();
}

#ifdef _DEBUG
#undef new 
#define new DEBUG_NEW
#endif

static void DestructLINE2D(LINE2D *thisPointer)
{
	thisPointer->~LINE2D();
}

bool CALLBACK ConvLINE2D( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("LINE2D");
    const LINE2D* pObj = reinterpret_cast<const LINE2D*>(pValAddress);
    StdStringStream strmVal; 

    strmVal << _T("{");

    VAL_DATA valPt1;
    valPt1.strName     = _T("pt1");
    ConvPOINT2D( &valPt1, &pObj->Pt(0), pContext);
    pValData->lstVal.push_back(valPt1);
    strmVal << valPt1.strVal << _T(",");

    VAL_DATA valPt2;
    valPt2.strName     = _T("pt2");
    ConvPOINT2D( &valPt2, &pObj->Pt(1), pContext);
    pValData->lstVal.push_back(valPt2);
    strmVal << valPt2.strVal;

    strmVal << _T("}");

    pValData->strVal = strmVal.str();
    return true;
}


static LINE2D* Factory4ParamLINE2D(double dX1, double dY1, double dX2, double dY2)
{                                                                
    LINE2D *pRet;                                        
    pRet = new LINE2D( dX1, dY1, dX2, dY2);
    CAsRefCtrl::GetInstance()->AddAsObj(pRet);
	return pRet;
}

static LINE2D* Factory3ParamLINE2D(double dA, double dB, double dC)
{                                                                
    LINE2D *pRet;                                        
    pRet = new LINE2D( dA, dB, dC);
    CAsRefCtrl::GetInstance()->AddAsObj(pRet);
	return pRet;
}

static LINE2D* Factory2PointLINE2D(const POINT2D &p1, const POINT2D &p2)
{                                                                
    LINE2D *pRet;                                        
    pRet = new LINE2D( p1, p2);
    CAsRefCtrl::GetInstance()->AddAsObj(pRet);
	return pRet;
}

static LINE2D* FactoryPointAngleLINE2D(const POINT2D &pt, double dAngle)
{                                                                
    LINE2D *pRet;                                        
    pRet = new LINE2D( pt, dAngle);
    CAsRefCtrl::GetInstance()->AddAsObj(pRet);
	return pRet;
}


//LINE2D型登録
int RegisterLINE2DType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
    int iRet = engine->RegisterObjectType( "LINE2D", 0, asOBJ_REF); assert( iRet >= 0 );  

    //ウォッチリスト表示用登録
    mapIdFunc[iRet] = ConvLINE2D;
    return iRet;
}

//LINE2D登録
int RegisterLINE2D(asIScriptEngine *engine)
{
    RegsterRef<LINE2D>(engine, "LINE2D");
    RegsterBasicMethod<LINE2D>(engine, "LINE2D");

    int r;
    //!< 座標指定による生成
    r = engine->RegisterObjectBehaviour("LINE2D", asBEHAVE_FACTORY, 
        "LINE2D @f(double, double, double, double)",
        asFUNCTION( Factory4ParamLINE2D), asCALL_CDECL); 
        STD_ASSERT( r >= 0 );

    //!< 直線パラメータによる生成
    r = engine->RegisterObjectBehaviour("LINE2D", asBEHAVE_FACTORY, 
        "LINE2D @f(double, double, double)", 
        asFUNCTION( Factory3ParamLINE2D), asCALL_CDECL); 
        STD_ASSERT( r >= 0 );

    //!< ２点による生成
    r = engine->RegisterObjectBehaviour("LINE2D", asBEHAVE_FACTORY, 
        "LINE2D @f(const POINT2D &in, const POINT2D &in)", 
        asFUNCTION( Factory2PointLINE2D), asCALL_CDECL); 
        STD_ASSERT( r >= 0 );

    //!< 通過点と角度による生成
    r = engine->RegisterObjectBehaviour("LINE2D", asBEHAVE_FACTORY, 
        "LINE2D @f(const POINT2D &in, double)", 
        asFUNCTION( FactoryPointAngleLINE2D), asCALL_CDECL); 
        STD_ASSERT( r >= 0 );

    //LINE2D& operator = (const LINE2D & m);
    r = engine->RegisterObjectMethod("LINE2D", 
        "LINE2D &opAssign(const LINE2D &in)"   , 
        asMETHODPR(LINE2D, operator=, 
        (const LINE2D &), LINE2D&), asCALL_THISCALL); 
        STD_ASSERT( r >= 0 );

    //bool operator == (const LINE2D & m); (!= は定義不要？）
    r = engine->RegisterObjectMethod("LINE2D", 
        "bool opEquals(const LINE2D &in) const", 
        asMETHODPR(LINE2D, operator==, 
        (const ILine2D &) const, bool), asCALL_THISCALL); 
        STD_ASSERT( r >= 0 );

    //  交点
    r = engine->RegisterObjectMethod("LINE2D", 
        "POINT2D& Intersect(const LINE2D&in)",
        asMETHODPR(LINE2D, Intersect, 
        (const ILine2D &) const, POINT2D), asCALL_THISCALL); 
        STD_ASSERT( r >= 0 );

    //!<  法線ベクトル(正規化）
    r = engine->RegisterObjectMethod("LINE2D", 
        "POINT2D& Normal()",
        asMETHODPR(LINE2D, Normal, 
        (void) const, POINT2D), asCALL_THISCALL); 
        STD_ASSERT( r >= 0 );

    //!<   線分上の点か
    r = engine->RegisterObjectMethod("LINE2D", 
        "bool IsOnLine(const POINT2D &in) const",
        asMETHODPR(LINE2D, IsOnLine, 
        (const IPoint2D &) const, bool), asCALL_THISCALL);
        STD_ASSERT( r >= 0 );

    //!< 線分長取得
    r = engine->RegisterObjectMethod("LINE2D", 
        "double Length() const",
        asMETHODPR(LINE2D, Length, 
        (void) const, double), asCALL_THISCALL);
        STD_ASSERT( r >= 0 );


    //!< 距離取得取得
    r = engine->RegisterObjectMethod("LINE2D", 
        "double Distance(const POINT2D &in) const",
        asMETHODPR(LINE2D, Distance, 
        (const POINT2D &) const, double), asCALL_THISCALL); 
        STD_ASSERT( r >= 0 );

    //!< 端点設定
    r = engine->RegisterObjectMethod("LINE2D", 
        "void SetPt(double, double, double, double)",
        asMETHODPR(LINE2D, SetPt,
        (double,double,double,double), void), asCALL_THISCALL);
        STD_ASSERT( r >= 0 );

    //!< 端点設定
    r = engine->RegisterObjectMethod("LINE2D", 
        "void SetPt(const POINT2D& in, const POINT2D& in)",
        asMETHODPR(LINE2D, SetPt,
        (const POINT2D&,const POINT2D&), void), asCALL_THISCALL);
        STD_ASSERT( r >= 0 );

    //!< 端点1設定
    r = engine->RegisterObjectMethod("LINE2D", 
        "void SetPt1(const POINT2D& in)",
        asMETHOD(LINE2D, SetPt1), asCALL_THISCALL);
        STD_ASSERT( r >= 0 );

    //!< 端点1取得
    r = engine->RegisterObjectMethod("LINE2D", 
        "POINT2D& GetPt1()",
        asMETHOD(LINE2D, GetPt1), asCALL_THISCALL);
        STD_ASSERT( r >= 0 );

    //!< 端点2設定
    r = engine->RegisterObjectMethod("LINE2D", 
        "void SetPt2(const POINT2D& in)",
        asMETHOD(LINE2D, SetPt2), asCALL_THISCALL);
        STD_ASSERT( r >= 0 );

    //!< 端点2取得
    r = engine->RegisterObjectMethod("LINE2D", 
        "POINT2D& GetPt2()",
        asMETHOD(LINE2D, GetPt2), asCALL_THISCALL);
        STD_ASSERT( r >= 0 );

    //!< 近接点取得
    r = engine->RegisterObjectMethod("LINE2D",
        "POINT2D& NearPoint(const POINT2D &in, bool)",
        asMETHODPR(LINE2D, NearPoint,
        (const IPoint2D&, bool) const, POINT2D), asCALL_THISCALL);
        COMMENT_ASSERT( r >= 0, _T("NearPoint") );

    return 0;

}

END_AS_NAMESPACE
