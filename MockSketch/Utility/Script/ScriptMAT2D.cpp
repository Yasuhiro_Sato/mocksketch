/**
 * @brief			ScriptMAT2D実装ファイル
 * @file			ScriptBase.cpp
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:36
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include <assert.h>
#include <sstream>
#include "ScriptBase.h"
#include "ScriptMAT2D.h"
#include "Utility/CUtility.h"
#include "DrawingObject/Primitive/POINT2D.h"
#include "DrawingObject/Primitive/MAT2D.h"

/*---------------------------------------------------*/
/*  MACROS                                           */
/*---------------------------------------------------*/
using namespace std;
#ifdef _DEBUG
#undef new 
#define new DEBUG_NEW
#endif
extern bool CALLBACK ConvPOINT2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);


bool CALLBACK ConvMAT2D( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("MAT2D");
    const MAT2D* pObj = reinterpret_cast<const MAT2D*>(pValAddress);
    StdStringStream strmVal; 

    VAL_DATA val;
    val.strTypeName = _T("double");

    strmVal << _T("{");
    for(int iCol = 0; iCol < 3; iCol++)
    {
        strmVal << _T("{");
        for(int iRow = 0; iRow < 3; iRow++)
        {
            val.strName   = CUtil::StrFormat(_T("%d,%d"), iRow, iCol);
            val.strVal    = CUtil::StrFormat(_T("%f"), pObj->Val(iRow, iCol));
            strmVal << val.strVal;
            pValData->lstVal.push_back(val);

            if(iRow != 2)
            {
                strmVal << ",";
            }
        }
        strmVal << "}";
        if(iCol != 2)
        {
            strmVal << ",";
        }
    }
    strmVal << _T("}");
    pValData->strVal = strmVal.str();
    return true;
}

//MAT2D型登録
int RegisterMAT2DType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
    int iRet = engine->RegisterObjectType( "MAT2D", 0, asOBJ_REF); assert( iRet >= 0 );  

    //ウォッチリスト表示用登録
    mapIdFunc[iRet] = ConvMAT2D;
    return iRet;
}

//MAT2D登録
int RegisterMAT2D(asIScriptEngine *engine)
{
    RegsterRef<MAT2D>(engine, "MAT2D");

    int r;
    //bool operator == (const IMat2D & m); (!= は定義不要？）
    r = engine->RegisterObjectMethod("MAT2D", 
        "bool opEquals(const MAT2D &in) const"    , 
        asMETHODPR(MAT2D, operator==, 
        (const IMat2D &) const, bool), 
        asCALL_THISCALL); assert( r >= 0 );

    //MAT2D  operator * (const double & m);
	r = engine->RegisterObjectMethod("MAT2D", 
        "MAT2D& opMul(const double  &in)"   , 
        asMETHODPR(MAT2D, operator*, 
        (const double &)const, MAT2D), 
        asCALL_THISCALL); assert( r >= 0 );

    //MAT2D  operator * (const MAT2D & m);
	r = engine->RegisterObjectMethod("MAT2D", 
        "MAT2D& opMul(const MAT2D  &in)"   , 
        asMETHODPR(MAT2D, operator*, 
        (const MAT2D &)const, MAT2D), 
        asCALL_THISCALL); assert( r >= 0 );

    //MAT2D& operator = (const MAT2D & m);
	r = engine->RegisterObjectMethod("MAT2D", 
        "MAT2D &opAssign(const MAT2D &in)"   ,
        asMETHODPR(MAT2D, operator=, (const MAT2D &),
        MAT2D&), asCALL_THISCALL); assert( r >= 0 );

    //MAT2D& operator = (const double & m);
	r = engine->RegisterObjectMethod("MAT2D",
        "MAT2D &opAssign(const double  &in)"   ,
        asMETHODPR(MAT2D, operator=, (const double &), 
        MAT2D&), asCALL_THISCALL); assert( r >= 0 );

    //MAT2D  operator + (const MAT2D & m);
	r = engine->RegisterObjectMethod("MAT2D", 
        "MAT2D& opAdd(const MAT2D &in)"   , 
        asMETHODPR(MAT2D, operator+, 
        (const MAT2D &) const, MAT2D), 
        asCALL_THISCALL); assert( r >= 0 );

    //MAT2D  operator + (const double & m);
	r = engine->RegisterObjectMethod("MAT2D", 
        "MAT2D& opAdd(const double  &in)"   , 
        asMETHODPR(MAT2D, operator+, 
        (const double &) const, MAT2D), 
        asCALL_THISCALL); assert( r >= 0 );

    //MAT2D  operator - (const MAT2D & m);
	r = engine->RegisterObjectMethod("MAT2D", 
        "MAT2D& opSub(const MAT2D &in)"   , 
        asMETHODPR(MAT2D, operator-, 
        (const MAT2D &) const, MAT2D), 
        asCALL_THISCALL); assert( r >= 0 );

    //MAT2D  operator - (const double & m);
	r = engine->RegisterObjectMethod("MAT2D", 
        "MAT2D& opSub(const double  &in)"   , 
        asMETHODPR(MAT2D , operator-, 
        (const double &) const, MAT2D), 
        asCALL_THISCALL); assert( r >= 0 );

    //MAT2D  operator - ();
	r = engine->RegisterObjectMethod("MAT2D", 
        "MAT2D& opSub()"   , 
        asMETHODPR(MAT2D, operator-, 
        ()const, MAT2D), 
        asCALL_THISCALL); assert( r >= 0 );

    //値;
	r = engine->RegisterObjectMethod("MAT2D", 
        "double& Get(int, int)"   , 
        asMETHODPR(MAT2D, Val, 
        (size_t , size_t), double&), 
        asCALL_THISCALL); assert( r >= 0 );

    //Move
	r = engine->RegisterObjectMethod("MAT2D", 
        "void Move(const POINT2D &in)"   , 
        asMETHODPR(MAT2D, Move, 
        (const IPoint2D&), void), 
        asCALL_THISCALL); assert( r >= 0 );

    //Rotate
	r = engine->RegisterObjectMethod("MAT2D", 
        "void Rotate(double)"   , 
        asMETHODPR(MAT2D, Rotate, 
        (double), void), 
        asCALL_THISCALL); assert( r >= 0 );

    //Rotate
	r = engine->RegisterObjectMethod("MAT2D", 
        "void Rotate(const POINT2D &in, double)"   , 
        asMETHODPR(MAT2D, Rotate, 
        (const IPoint2D&, double), void), 
        asCALL_THISCALL); assert( r >= 0 );

    //Mirror
	r = engine->RegisterObjectMethod("MAT2D", 
        "void Mirror(const LINE2D &in)"   , 
        asMETHODPR(MAT2D, Mirror, 
        (const ILine2D&), void), 
        asCALL_THISCALL); assert( r >= 0 );

    //Scl
	r = engine->RegisterObjectMethod("MAT2D", 
        "void Scale(double, double)"   , 
        asMETHODPR(MAT2D, Scl, 
        (double, double), void), 
        asCALL_THISCALL); assert( r >= 0 );

    //Scl
	r = engine->RegisterObjectMethod("MAT2D", 
        "void Scale(const POINT2D &in, double, double)"   , 
        asMETHODPR(MAT2D, Scl, 
        (const IPoint2D&, double, double), void), 
        asCALL_THISCALL); assert( r >= 0 );

    return 0;

}


END_AS_NAMESPACE
