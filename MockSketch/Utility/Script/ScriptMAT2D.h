/**
 * @brief			ScriptMAT2D
 * @file			ScriptMAT2D.h
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:37
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#ifndef SCRIPT_MAT2D_H
#define SCRIPT_MAT2D_H

#include <angelscript.h>
#include <string>
#include "VAL_DATA.h"

int RegisterMAT2DType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc);
int RegisterMAT2D(asIScriptEngine *engine);

END_AS_NAMESPACE

#endif
