/**
 * @brief			ScriptNodeMarker実装ファイル
 * @file			ScriptNodeMarker.cpp
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:36
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include <assert.h>
#include <sstream>
#include "ScriptBase.h"
#include "ScriptNodeMarker.h"
#include "Utility/CUtility.h"
#include "DrawingObject/Primitive/SPLINE2D.H"
#include "DrawingObject/CNodeMarker.h"

/*---------------------------------------------------*/
/*  MACROS                                           */
/*---------------------------------------------------*/
using namespace std;
#ifdef _DEBUG
#undef new 
#define new DEBUG_NEW
#endif
extern bool CALLBACK  ConvPOINT2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);

bool CALLBACK ConvNodeMarkerType( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("NodeMarker");
    const CNodeMarker* pObj = reinterpret_cast<const CNodeMarker*>(pValAddress);

    pValData->strVal = _T("{");



    return true;
}

//DrawingLine型登録
int RegisterNodeMarkerType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
    int iRet = engine->RegisterObjectType( "NodeMarker", 0, asOBJ_REF); assert( iRet >= 0 );  

    //ウォッチリスト表示用登録
    mapIdFunc[iRet] = ConvNodeMarkerType;
    return iRet;
}





//NodeMarker登録
int RegisterNodeMarker(asIScriptEngine *engine)
{
	int r;
	// Register the object operator overloads
    // スクリプト側で生成する必要がないのでファクトリ関数は不要
    r = engine->RegisterObjectBehaviour("NodeMarker", asBEHAVE_ADDREF, "void f()", asMETHOD(CNodeMarker,AddRef), asCALL_THISCALL);
    r = engine->RegisterObjectBehaviour("NodeMarker", asBEHAVE_RELEASE, "void f()", asMETHOD(CNodeMarker,Release), asCALL_THISCALL);

	r = engine->RegisterObjectMethod("NodeMarker", 
        "NodeMarker& opAssign(const NodeMarker &in)", 
        asMETHODPR(CNodeMarker, operator=,(const CNodeMarker &), CNodeMarker&),
        asCALL_THISCALL); assert( r >= 0 );


    //マーカー生成
	r = engine->RegisterObjectMethod("NodeMarker", 
        "bool Create(string& in, POINT2D& in)", 
        asMETHODPR(CNodeMarker, Create,(StdString, POINT2D), bool),
        asCALL_THISCALL); assert( r >= 0 );


    //削除
	r = engine->RegisterObjectMethod("NodeMarker", 
        "void Clear()", 
        asMETHOD(CNodeMarker,Clear), 
        asCALL_THISCALL); assert( r >= 0 );


    //選択マーカID取得
	r = engine->RegisterObjectMethod("NodeMarker", 
        "string GetSelectId()", 
        asMETHOD(CNodeMarker,GetSelectId), 
        asCALL_THISCALL); assert( r >= 0 );

    //マーカ位置設定
	r = engine->RegisterObjectMethod("NodeMarker", 
        "void SetMarkerPos(string, POINT2D& in)", 
        asMETHOD(CNodeMarker,SetMarkerPos), 
        asCALL_THISCALL); assert( r >= 0 );

    //マーカ位置取得
	r = engine->RegisterObjectMethod("NodeMarker", 
        "POINT2D& GetMarkerPos(string)", 
        asMETHOD(CNodeMarker,GetMarkerPos), 
        asCALL_THISCALL); assert( r >= 0 );

    //マーカ表示設定
	r = engine->RegisterObjectMethod("NodeMarker", 
        "void  SetVisible(string, bool)", 
        asMETHOD(CNodeMarker,SetVisible), 
        asCALL_THISCALL); assert( r >= 0 );

    return 0;
}


END_AS_NAMESPACE
