/**
 * @brief			ScriptNodeMarker
 * @file			ScriptNodeMarker.h
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:37
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#ifndef SCRIPT_NODE_MARKER_H
#define SCRIPT_NODE_MARKER_H

#include <angelscript.h>
#include <string>
#include "VAL_DATA.h"

int RegisterNodeMarkerType (asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc);
int RegisterNodeMarker (asIScriptEngine *engine);


#endif
