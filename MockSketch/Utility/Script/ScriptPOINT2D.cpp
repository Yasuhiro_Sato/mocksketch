/**
 * @brief			ScriptPOINT2D実装ファイル
 * @file			ScriptBase.cpp
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:36
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include <assert.h>
#include <sstream>
#include "ScriptBase.h"
#include "ScriptPOINT2D.h"
#include "Utility/CUtility.h"
#include "DrawingObject/Primitive/SPLINE2D.H"

/*---------------------------------------------------*/
/*  MACROS                                           */
/*---------------------------------------------------*/
using namespace std;
#ifdef _DEBUG
#undef new 
#endif


static void InitConstructor(double x, double y, POINT2D *self)
{
	new(self) POINT2D(x,y);
}


#ifdef _DEBUG
#undef new 
#define new DEBUG_NEW
#endif

//FOR DEBUG
static int ms_id = 0;

bool CALLBACK ConvPOINT2D( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("POINT2D");
    const POINT2D* pPt2D = reinterpret_cast<const POINT2D*>(pValAddress);
    StdStringStream strmVal; 

    strmVal << _T("{");
    
    VAL_DATA valX;
    valX.strName     = _T("x");
    valX.strTypeName = _T("double");
    valX.strVal = CUtil::StrFormat(_T("%f"), pPt2D->dX);
    pValData->lstVal.push_back(valX);
    strmVal << valX.strVal, _T(",");

    VAL_DATA valY;
    valY.strName     = _T("y");
    valY.strTypeName = _T("double");
    valY.strVal = CUtil::StrFormat(_T("%f"), pPt2D->dY);
    pValData->lstVal.push_back(valY);
    strmVal << valY.strVal, _T("}");

    pValData->strVal = strmVal.str();
    return true;
}

// OBJ_REFで定義したオブジェクトを戻り値で渡す場合は。
// 参照渡しか、アドレスで渡すことになる
static POINT2D& Add(const POINT2D &a, const POINT2D &b)
{
	// Return a new object as a script handle
	POINT2D* pRet = new POINT2D(a.dX + b.dX, a.dY + b.dY);
    pRet->SetRefId(++ms_id);
DB_PRINT(_T("POINT2D Add %d \n"), ms_id);
    pRet->SetScriptCtrl();
	return *pRet;
}

static POINT2D& AddDuble(const POINT2D &a, const double &s)
{
	// Return a new object as a script handle
	POINT2D* pRet = new POINT2D(a.dX + s, a.dY + s);
    pRet->SetRefId(++ms_id);
DB_PRINT(_T("POINT2D AddDuble %d \n"), ms_id);
    pRet->SetScriptCtrl();
    return *pRet;
}

static POINT2D& Sub(const POINT2D &a, const POINT2D &b)
{
	// Return a new object as a script handle
	POINT2D* pRet = new POINT2D(a.dX - b.dX, a.dY - b.dY);
    pRet->SetRefId(++ms_id);
DB_PRINT(_T("POINT2D Sub %d \n"), ms_id);
    pRet->SetScriptCtrl();
	return *pRet;
}

static POINT2D& SubDouble(const POINT2D &a, const double &s)
{
	// Return a new object as a script handle
	POINT2D* pRet = new POINT2D(a.dX - s, a.dY - s);
    pRet->SetRefId(++ms_id);
DB_PRINT(_T("POINT2D SubDouble %d \n"), ms_id);
    pRet->SetScriptCtrl();
	return *pRet;
}

static POINT2D& SubSelf(const POINT2D &a)
{
	// Return a new object as a script handle
	POINT2D* pRet = new POINT2D(-a.dX, -a.dY);
    pRet->SetRefId(++ms_id);
DB_PRINT(_T("POINT2D SubSelf %d \n"), ms_id);
    pRet->SetScriptCtrl();
	return *pRet;
}

#ifdef POINT2D_REF
static POINT2D& Mul(const POINT2D &a, const double &s)
{
	// Return a new object as a script handle
	POINT2D* pRet = new POINT2D(a.dX * s, a.dY * s);
    pRet->SetRefId(++ms_id);
DB_PRINT(_T("POINT2D Mul %d \n"), ms_id);
    pRet->SetScriptCtrl();
	return *pRet;
}
#else
static POINT2D Mul(const POINT2D &a, const double &s)
{
	POINT2D ptRet;
    ptRet.Set(a.dX * s, a.dY * s);
	return ptRet;
}


#endif
static POINT2D& Div(const POINT2D &v, const double &s)
{
	// Return a new object as a script handle
	POINT2D* pRet = new POINT2D(v.dX / s, v.dY / s);
    pRet->SetRefId(++ms_id);
    DB_PRINT(_T("POINT2D Div %d \n"), ms_id);
    pRet->SetScriptCtrl();
	return *pRet;
}

static StdString ToString(const POINT2D &v)
{
    StdStringStream strmRet;
    strmRet << StdFormat(_T("(%f,%f)")) % v.dX % v.dY;
	return strmRet.str();
}


int RegisterOeratorRefPOINT2D(asIScriptEngine *engine)
{
    int r;
    //POINT2D& operator = (const POINT2D & m);
	r = engine->RegisterObjectMethod("POINT2D", 
        "POINT2D &opAssign(const POINT2D &in)"   ,
        asMETHODPR(POINT2D, operator=, (const POINT2D &),
        POINT2D&), asCALL_THISCALL); assert( r >= 0 );

    //POINT2D& operator = (const double & m);
	r = engine->RegisterObjectMethod("POINT2D",
        "POINT2D &opAssign(const double  &in)"   ,
        asMETHODPR(POINT2D, operator=, (const double &), 
        IPoint2D&), asCALL_THISCALL); assert( r >= 0 );

    //bool operator == (const POINT2D & m); (!= は定義不要？）
    r = engine->RegisterObjectMethod("POINT2D", 
        "bool opEquals(const POINT2D &in) const"    , 
        asMETHODPR(POINT2D, operator==, 
        (const POINT2D &) const, bool), 
        asCALL_THISCALL); assert( r >= 0 );
    
	// Reference types cannot be returned by value from system functions
    //POINT2D  operator + (const POINT2D & m);
    r = engine->RegisterObjectMethod("POINT2D", 
        "POINT2D& opAdd(const POINT2D &in) const",
        asFUNCTION(Add), asCALL_CDECL_OBJFIRST);
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("POINT2D", 
        "POINT2D& Add(const POINT2D &in) const",
        asFUNCTION(Add), asCALL_CDECL_OBJFIRST);
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("POINT2D", 
        "POINT2D& opAdd(const double  &in) const",
        asFUNCTION(AddDuble), asCALL_CDECL_OBJFIRST);
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("POINT2D", 
        "POINT2D& Add(const double  &in) const",
        asFUNCTION(AddDuble), asCALL_CDECL_OBJFIRST);
        assert( r >= 0 );

    //POINT2D  operator - (const POINT2D & m);
	r = engine->RegisterObjectMethod("POINT2D", 
        "POINT2D& opSub(const POINT2D  &in) const",
        asFUNCTION(Sub), asCALL_CDECL_OBJFIRST);
        assert( r >= 0 );

    //POINT2D  operator - (const POINT2D & m);
	r = engine->RegisterObjectMethod("POINT2D", 
        "POINT2D& Sub(const POINT2D  &in) const",
        asFUNCTION(Sub), asCALL_CDECL_OBJFIRST);
        assert( r >= 0 );

        //POINT2D  operator - (const double & m);
	r = engine->RegisterObjectMethod("POINT2D", 
        "POINT2D& opSub(const double  &in) const"   , 
        asFUNCTION(SubDouble), asCALL_CDECL_OBJFIRST);
        assert( r >= 0 );

    //POINT2D  operator - (const double & m);
	r = engine->RegisterObjectMethod("POINT2D", 
        "POINT2D& Sub(const double  &in) const"   , 
        asFUNCTION(SubDouble), asCALL_CDECL_OBJFIRST);
        assert( r >= 0 );


    //POINT2D  operator - ();
	r = engine->RegisterObjectMethod("POINT2D", 
        "POINT2D& opNeg()"   , 
        asFUNCTION(SubSelf), asCALL_CDECL_OBJFIRST);
        assert( r >= 0 );

    //POINT2D  operator * (const double & m);
	r = engine->RegisterObjectMethod("POINT2D", 
        "POINT2D& opMul(const double  &in)"   , 
        asFUNCTION(Mul), asCALL_CDECL_OBJFIRST);
        assert( r >= 0 );

    //POINT2D  Multi(const double & m);
	r = engine->RegisterObjectMethod("POINT2D", 
        "POINT2D& Multi(const double  &in)"   , 
        asFUNCTION(Mul), asCALL_CDECL_OBJFIRST);
        assert( r >= 0 );


    //	friend POINT2D operator *(const double val, const POINT2D& m);
    r = engine->RegisterObjectMethod("POINT2D",
        "POINT2D& opMul_r(const double  &in) const",
        asFUNCTION(Mul), asCALL_CDECL_OBJFIRST);
        assert( r >= 0 );

    //POINT2D  operator / (const double & m);
	r = engine->RegisterObjectMethod("POINT2D", 
	    "POINT2D& opDiv(const double  &in) const",
        asFUNCTION(Div), asCALL_CDECL_OBJFIRST);
        assert( r >= 0 );

    //POINT2D  Div (const double & m);
	r = engine->RegisterObjectMethod("POINT2D", 
	    "POINT2D& Div(const double  &in) const",
        asFUNCTION(Div), asCALL_CDECL_OBJFIRST);
        assert( r >= 0 );

	r = engine->RegisterObjectBehaviour( "POINT2D", asBEHAVE_IMPLICIT_VALUE_CAST, 
        "string f() const", 
        asFUNCTION( ToString ), asCALL_CDECL_OBJLAST );
        assert( r >= 0 );


    return 0;
}




int RegisterOeratorValPOINT2D(asIScriptEngine *engine)
{
    int r;
    //POINT2D& operator = (const POINT2D & m);
	r = engine->RegisterObjectMethod("POINT2D", 
        "POINT2D &opAssign(const POINT2D &in)"   ,
        asMETHODPR(POINT2D, operator=, (const POINT2D &),
        POINT2D&), asCALL_THISCALL); assert( r >= 0 );

    //POINT2D& operator = (const double & m);
	r = engine->RegisterObjectMethod("POINT2D",
        "POINT2D &opAssign(const double  &in)"   ,
        asMETHODPR(POINT2D, operator=, (const double &), 
        IPoint2D&), asCALL_THISCALL); assert( r >= 0 );

    //bool operator == (const POINT2D & m); (!= は定義不要？）
    r = engine->RegisterObjectMethod("POINT2D", 
        "bool opEquals(const POINT2D &in) const"    , 
        asMETHODPR(POINT2D, operator==, 
        (const POINT2D &) const, bool), 
        asCALL_THISCALL); assert( r >= 0 );
    
	// Reference types cannot be returned by value from system functions
    //POINT2D  operator + (const POINT2D & m);
    r = engine->RegisterObjectMethod("POINT2D", 
        "POINT2D opAdd(const POINT2D &in) const",
        asMETHODPR(POINT2D, operator+, 
        (const POINT2D &) const, POINT2D), 
        asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("POINT2D", 
        "POINT2D opAdd(const double  &in) const",
        asMETHODPR(POINT2D, operator+, 
        (const double &) const, POINT2D), 
        asCALL_THISCALL); assert( r >= 0 );

    //POINT2D  operator - (const POINT2D & m);
	r = engine->RegisterObjectMethod("POINT2D", 
        "POINT2D opSub(const POINT2D  &in) const",
        asMETHODPR(POINT2D, operator-, 
        (const POINT2D &) const, POINT2D), 
        asCALL_THISCALL); assert( r >= 0 );

    //POINT2D  operator - (const double & m);
	r = engine->RegisterObjectMethod("POINT2D", 
        "POINT2D opSub(const double  &in) const",
        asMETHODPR(POINT2D, operator-, 
        (const double &) const,  POINT2D),
        asCALL_THISCALL); assert( r >= 0 );


    //POINT2D  operator - ();
	r = engine->RegisterObjectMethod("POINT2D", 
        "POINT2D opNeg()"   , 
        asMETHODPR(POINT2D, operator-, () const,
        POINT2D), asCALL_THISCALL); assert( r >= 0 );

    //POINT2D  operator * (const double & m);
	r = engine->RegisterObjectMethod("POINT2D", 
        "POINT2D opMul(const double  &in)"   , 
        asMETHODPR(POINT2D, operator*, (const double &) const,
        POINT2D), asCALL_THISCALL); assert( r >= 0 );

    //	friend POINT2D operator *(const double val, const POINT2D& m);
    r = engine->RegisterObjectMethod("POINT2D",
        "POINT2D opMul_r(const double  &in) const",
        asFUNCTION(Mul), asCALL_CDECL_OBJFIRST);
        assert( r >= 0 );

    //POINT2D  operator / (const double & m);
	r = engine->RegisterObjectMethod("POINT2D", 
	    "POINT2D opDiv(const double  &in) const",
        asMETHODPR(POINT2D, operator/, (const double &) const,
        POINT2D), asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectBehaviour( "POINT2D", asBEHAVE_IMPLICIT_VALUE_CAST, 
        "string f() const", 
        asFUNCTION( ToString ), asCALL_CDECL_OBJLAST );
        assert( r >= 0 );
    return 0;
}

int RegisterPOINT2DType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
#ifdef POINT2D_REF
    int iRet = engine->RegisterObjectType( "POINT2D", sizeof(POINT2D), asOBJ_REF); 
#else
    int iRet = engine->RegisterObjectType( "POINT2D", sizeof(POINT2D), asOBJ_VALUE | asOBJ_APP_CLASS_CDAK); 
#endif

    assert( iRet >= 0 );  

    //ウォッチリスト表示用登録
    mapIdFunc[iRet] = ConvPOINT2D;
    return iRet;
}


int RegisterPOINT2D(asIScriptEngine *engine)
{
    int r; 
    //RegsterRef<POINT2D>(engine, "POINT2D");
    RegsterVal<POINT2D>(engine, "POINT2D");

	// Register the constructors
	r = engine->RegisterObjectBehaviour("POINT2D", asBEHAVE_CONSTRUCT,     
        "void f(double, double)",  
        asFUNCTION(InitConstructor), 
        asCALL_CDECL_OBJLAST);
        assert( r >= 0 );


    r = engine->RegisterObjectMethod("POINT2D", "double Length()"                   , asMETHOD(POINT2D,Length_A), asCALL_THISCALL)                ; COMMENT_ASSERT( r >= 0, _T("Length")   );\
    r = engine->RegisterObjectMethod("POINT2D", "double Distance(const POINT2D &in)", asMETHOD(POINT2D,Distance_P), asCALL_THISCALL)              ; COMMENT_ASSERT( r >= 0, _T("Length")   );\
    r = engine->RegisterObjectMethod("POINT2D", "void Normalize()"                  , asMETHOD(POINT2D,Normalize), asCALL_THISCALL)             ; COMMENT_ASSERT( r >= 0, _T("Normalize")   );\
    r = engine->RegisterObjectMethod("POINT2D", "double Dot(const POINT2D &in)"     , asMETHOD(POINT2D,Dot_P), asCALL_THISCALL)                 ; COMMENT_ASSERT( r >= 0, _T("Normalize")   );\
    r = engine->RegisterObjectMethod("POINT2D", "double Cross(const POINT2D &in)"   , asMETHOD(POINT2D,Cross_P), asCALL_THISCALL)               ; COMMENT_ASSERT( r >= 0, _T("Normalize")   );\


    r = engine->RegisterObjectMethod("POINT2D", "void AbsMove(const POINT2D &in)"   , asMETHOD(POINT2D,AbsMove_P), asCALL_THISCALL)             ; COMMENT_ASSERT( r >= 0, _T("Normalize")   );\
    r = engine->RegisterObjectMethod("POINT2D", "void Move  (const POINT2D &in)"                , asMETHOD(POINT2D,Move_P), asCALL_THISCALL)    ; COMMENT_ASSERT( r >= 0, _T("Move")   );\
    r = engine->RegisterObjectMethod("POINT2D", "void Rotate(const POINT2D &in, double)"        , asMETHOD(POINT2D,Rotate_P), asCALL_THISCALL)  ; COMMENT_ASSERT( r >= 0, _T("Rotate") );\
    r = engine->RegisterObjectMethod("POINT2D", "void Scl(const POINT2D &in, double, double)"   , asMETHOD(POINT2D,Scl_P), asCALL_THISCALL)     ; COMMENT_ASSERT( r >= 0, _T("Scl")    );\
    r = engine->RegisterObjectMethod("POINT2D", "void Mirror(const LINE2D &in)"                 , asMETHOD(POINT2D,Mirror), asCALL_THISCALL)    ; COMMENT_ASSERT( r >= 0, _T("Mirror") );\
    r = engine->RegisterObjectMethod("POINT2D", "void Matrix(const MAT2D &in)"                  , asMETHOD(POINT2D,Matrix), asCALL_THISCALL)    ; COMMENT_ASSERT( r >= 0, _T("Matrix") );
    r = engine->RegisterObjectMethod("POINT2D", "bool IsInside(const POINT2D &in)"              , asMETHOD(POINT2D,IsInside_P), asCALL_THISCALL)    ; COMMENT_ASSERT( r >= 0, _T("Matrix") );
    r = engine->RegisterObjectMethod("POINT2D", "double Angle(const POINT2D &in)"               , asMETHOD(POINT2D,Angle_P), asCALL_THISCALL)    ; COMMENT_ASSERT( r >= 0, _T("Matrix") );


    // Register the object properties
	r = engine->RegisterObjectProperty("POINT2D", "double x", offsetof(POINT2D, dX)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("POINT2D", "double y", offsetof(POINT2D, dY)); assert( r >= 0 );


	// Register the operator overloads
    RegisterOeratorValPOINT2D(engine);





return 0;
}

END_AS_NAMESPACE



