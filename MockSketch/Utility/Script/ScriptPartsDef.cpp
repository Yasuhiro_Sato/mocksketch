/**
 * @brief			ScriptPartsDef実装ファイル
 * @file			ScriptPartsDef.cpp
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:36
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include <assert.h>
#include <sstream>
#include "ScriptBase.h"
#include "ScriptPartsDef.h"
#include "Utility/CUtility.h"
#include "DrawingObject/Primitive/SPLINE2D.H"
#include "DefinitionObject/CPartsDef.h"

/*---------------------------------------------------*/
/*  MACROS                                           */
/*---------------------------------------------------*/
using namespace std;
#ifdef _DEBUG
#undef new 
#endif
extern bool CALLBACK ConvPOINT2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);

static void ConstructPartsDef(CPartsDef *thisPointer)
{
	new(thisPointer) CPartsDef();
}

#ifdef _DEBUG
#undef new 
#define new DEBUG_NEW
#endif

static void DestructPartsDef(CPartsDef *thisPointer)
{
	thisPointer->~CPartsDef();
}


CPartsDef* CreatePartsDef()
{
    return new CPartsDef();
}

bool CALLBACK ConvPartsDef( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("PartsDef");
    const CPartsDef* pObj = reinterpret_cast<const CPartsDef*>(pValAddress);
    pValData->strVal = _T("---");

    /*
    VAL_DATA valX;
    valX.strName     = _T("x");
    valX.strTypeName = _T("double");
    valX.strVal = CUtil::StrFormat(_T("%f"), pPt2D->dX);
    pValData->lstVal.push_back(valX);

    VAL_DATA valY;
    valY.strName     = _T("y");
    valY.strTypeName = _T("double");
    valY.strVal = CUtil::StrFormat(_T("%f"), pPt2D->dY);
    pValData->lstVal.push_back(valY);
    */

    return true;
}

//PartsDef型登録
int RegisterPartsDefType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
    int iRet = engine->RegisterObjectType( "PartsDef", 0, asOBJ_REF); assert( iRet >= 0 );  

    //ウォッチリスト表示用登録
    mapIdFunc[iRet] = ConvPartsDef;
    return iRet;
}

//PartsDef登録
int RegisterPartsDef(asIScriptEngine *engine)
{

	int r;

	// Register the object operator overloads
    // スクリプト側で生成する必要がないのでファクトリ関数は不要
	//r = engine->RegisterObjectBehaviour("PartsDef", asBEHAVE_FACTORY,    "PartsDef@ f()",     asFUNCTION(CreatePartsDef), asCALL_CDECL); assert( r >= 0 );
    r = engine->RegisterObjectBehaviour("PartsDef", asBEHAVE_ADDREF, "void f()", asMETHOD(CPartsDef,AddRef), asCALL_THISCALL);
    r = engine->RegisterObjectBehaviour("PartsDef", asBEHAVE_RELEASE, "void f()", asMETHOD(CPartsDef,Release), asCALL_THISCALL);
    //r = engine->RegisterObjectBehaviour("PartsDef", asBEHAVE_ASSIGNMENT, "PartsDef &f(IWindow &in)", asMETHODPR(iWindow, operator=, (const CPartsDef&), CPartsDef&), asCALL_THISCALL); assert( r &gt;= 0 );
       

    //コントロール名取得
	r = engine->RegisterObjectMethod("PartsDef", 
        "string GetName() const", 
        asMETHOD(CPartsDef,GetName), 
        asCALL_THISCALL); assert( r >= 0 );

    //!< オブジェクト取得(名称)
	r = engine->RegisterObjectMethod("PartsDef", 
        "DrawingObject@ GetObject(string)", 
         asMETHOD(CPartsDef,GetObjectByName), 
        asCALL_THISCALL); assert( r >= 0 );

#if 0
    //!< オブジェクト取得(名称)
	r = engine->RegisterObjectMethod("PartsDef", 
        "DrawingObject@ GetObject(string)", 
         asMETHOD(CPartsDef,GetObjectByNameS), 
        asCALL_THISCALL); assert( r >= 0 );
#endif

    //!< 描画
	r = engine->RegisterObjectMethod("PartsDef", 
        "void Draw() const", 
        asMETHOD(CPartsDef,Draw), 
        asCALL_THISCALL); assert( r >= 0 );
/*
    //!< データ追加
	r = engine->RegisterObjectMethod("PartsDef", 
        "void AddObject(DrawingObject &in)", 
        asMETHOD(CPartsDef,AddObject2), 
        asCALL_THISCALL); assert( r >= 0 );
*/
    //!< オブジェクト選択(名称)
	r = engine->RegisterObjectMethod("PartsDef", 
        "void SelectObject(string)", 
         asMETHOD(CPartsDef,SelectObjectByName), 
        asCALL_THISCALL); assert( r >= 0 );


    //!< 選択オブジェクト削除
	r = engine->RegisterObjectMethod("PartsDef", 
        "void DeleteSelectObject()", 
         asMETHOD(CPartsDef,DeleteDrawingObject), 
        asCALL_THISCALL); assert( r >= 0 );

    //!< 選択解除
	r = engine->RegisterObjectMethod("PartsDef", 
        "void ReleaseSelect()", 
         asMETHOD(CPartsDef,RelSelect), 
        asCALL_THISCALL); assert( r >= 0 );

    //色による検索
    /*
    r = engine->RegisterObjectMethod("PartsDef", 
        "void SearchObjectByColor(DrawingObjectPtr_VECTOR& out, COLORREF& in)", 
        asMETHOD(CPartsDef,SearchObjectByColorAs), 
        asCALL_THISCALL); assert( r >= 0 );
    */

    //種別による検索
    r = engine->RegisterObjectMethod("PartsDef", 
        "void SearchObjectByType(DrawingObjectPtr_VECTOR& out, DRAWING_TYPE)", 
        asMETHOD(CPartsDef,SearchObjectByTypeAs), 
        asCALL_THISCALL); assert( r >= 0 );

    //名前による検索
    r = engine->RegisterObjectMethod("PartsDef", 
        "void SearchObjectByNmae(DrawingObjectPtr_VECTOR& out, string& in)", 
        asMETHOD(CPartsDef,SearchObjectByNmaeAs), 
        asCALL_THISCALL); assert( r >= 0 );

    //プロパティセットによる検索
    r = engine->RegisterObjectMethod("PartsDef", 
        "void SearchObjectByPropertyset(DrawingObjectPtr_VECTOR& out, string& in)", 
        asMETHOD(CPartsDef,SearchObjectByPropertysetAs), 
        asCALL_THISCALL); assert( r >= 0 );

    //タグによる検索
    r = engine->RegisterObjectMethod("PartsDef", 
        "void SearchObjectByTag(DrawingObjectPtr_VECTOR& out, string& in)", 
        asMETHOD(CPartsDef,SearchObjectByTagAs), 
        asCALL_THISCALL); assert( r >= 0 );


    return 0;

}


END_AS_NAMESPACE
