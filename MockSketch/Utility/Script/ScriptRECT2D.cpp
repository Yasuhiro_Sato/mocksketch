/**
 * @brief			ScriptRECT2D実装ファイル
 * @file			ScriptRECT2D.cpp
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:36
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include <assert.h>
#include <sstream>
#include "ScriptBase.h"
#include "ScriptRECT2D.h"
#include "Utility/CUtility.h"
#include "DrawingObject/Primitive/SPLINE2D.H"
#include "DrawingObject/Primitive/RECT2D.h"

/*---------------------------------------------------*/
/*  MACROS                                           */
/*---------------------------------------------------*/
using namespace std;
#ifdef _DEBUG
#undef new 
#define new DEBUG_NEW
#endif
extern bool CALLBACK ConvPOINT2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);

bool CALLBACK ConvRECT2D( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("RECT2D");
    const RECT2D* pRECT2D = reinterpret_cast<const RECT2D*>(pValAddress);
    StdStringStream strmVal; 


    strmVal << _T("{");
    
    VAL_DATA valLeft;
    valLeft.strName     = _T("Left");
    valLeft.strTypeName = _T("double");
    valLeft.strVal = CUtil::StrFormat(_T("%f"), pRECT2D->dLeft);
    pValData->lstVal.push_back(valLeft);
    strmVal << valLeft.strVal, _T(",");

    VAL_DATA valTop;
    valTop.strName     = _T("Top");
    valTop.strTypeName = _T("double");
    valTop.strVal = CUtil::StrFormat(_T("%f"), pRECT2D->dTop);
    pValData->lstVal.push_back(valTop);
    strmVal << valTop.strVal, _T(",");

    VAL_DATA valRight;
    valRight.strName     = _T("Right");
    valRight.strTypeName = _T("double");
    valRight.strVal = CUtil::StrFormat(_T("%f"), pRECT2D->dRight);
    pValData->lstVal.push_back(valRight);
    strmVal << valRight.strVal, _T(",");

    VAL_DATA valBottom;
    valBottom.strName     = _T("Bottom");
    valBottom.strTypeName = _T("double");
    valBottom.strVal = CUtil::StrFormat(_T("%f"), pRECT2D->dBottom);
    pValData->lstVal.push_back(valBottom);
    strmVal << _T("}");

    pValData->strVal = strmVal.str();
    return true;
}

//!< コンストラクタ (幅,高さ指定)
static RECT2D* FactoryCIRCLE2D_Width_Height(double dWidth, double dHeight)
{                                                                
    RECT2D *pRet;                                        
    pRet = new RECT2D( dWidth, dHeight);
    CAsRefCtrl::GetInstance()->AddAsObj(pRet);
	return pRet;
}

//!< コンストラクタ (点指定)
static RECT2D* FactoryRECT2D_2Point(const POINT2D& pt1,
                                    const POINT2D& pt2)
{                                                                
    RECT2D *pRet;                                        
    pRet = new RECT2D( pt1, pt2);
    CAsRefCtrl::GetInstance()->AddAsObj(pRet);
	return pRet;
}

//RECT2D型登録
int RegisterRECT2DType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
    int iRet = engine->RegisterObjectType( "RECT2D", 0, asOBJ_REF); assert( iRet >= 0 );  

    //ウォッチリスト表示用登録
    mapIdFunc[iRet] = ConvRECT2D;
   return iRet;
}

//RECT2D登録
int RegisterRECT2D(asIScriptEngine *engine)
{
    RegsterRef<RECT2D>(engine, "RECT2D");

    int r;
    //!< 幅,高さ指定による生成
    r = engine->RegisterObjectBehaviour("RECT2D", asBEHAVE_FACTORY, 
        "RECT2D @f(double, double)",
        asFUNCTION( FactoryCIRCLE2D_Width_Height), asCALL_CDECL); 
        STD_ASSERT( r >= 0 );

    //!< 2点による生成
    r = engine->RegisterObjectBehaviour("RECT2D", asBEHAVE_FACTORY, 
        "RECT2D @f(const POINT2D& in, const POINT2D& in)",
        asFUNCTION( FactoryRECT2D_2Point), asCALL_CDECL); 
        STD_ASSERT( r >= 0 );


    //RECT2D& operator = (const RECT2D & m);
    r = engine->RegisterObjectMethod("RECT2D",
        "RECT2D &opAssign(const RECT2D  &in)"   ,
        asMETHODPR(RECT2D, operator=, (const RECT2D &), 
        RECT2D&), asCALL_THISCALL); assert( r >= 0 );

    //RECT2D& operator = (const POINT2D & m);
    r = engine->RegisterObjectMethod("RECT2D",
        "RECT2D &opAssign(const POINT2D  &in)"   ,
        asMETHODPR(RECT2D, operator=, (const POINT2D &), 
        RECT2D&), asCALL_THISCALL); assert( r >= 0 );

    //bool operator == (const CIRCLE2D & m); (!= は定義不要？）
    r = engine->RegisterObjectMethod("RECT2D", 
        "bool opEquals(const RECT2D &in) const"    , 
        asMETHODPR(RECT2D, operator==, 
        (const RECT2D &) const, bool), 
        asCALL_THISCALL); assert( r >= 0 );

    //点含む領域に拡張する
    r = engine->RegisterObjectMethod("RECT2D", 
        "void ExpandArea(const POINT2D &in)"    , 
        asMETHODPR(RECT2D, ExpandArea, 
        (const POINT2D &), void), 
        asCALL_THISCALL); assert( r >= 0 );

    //!<  領域を拡張する
    r = engine->RegisterObjectMethod("RECT2D", 
        "void ExpandArea(const RECT2D &in)"    , 
        asMETHODPR(RECT2D, ExpandArea, 
        (const RECT2D &), void), 
        asCALL_THISCALL); assert( r >= 0 );

    //!<  指定した点が内部かどうか
    r = engine->RegisterObjectMethod("RECT2D", 
        "bool IsInside(const POINT2D &in)"    , 
        asMETHODPR(RECT2D, IsInside, 
        (const POINT2D &) const, bool), 
        asCALL_THISCALL); assert( r >= 0 );

    //!<  指定した矩形が内部かどうか
    r = engine->RegisterObjectMethod("RECT2D", 
        "bool IsInside(const RECT2D &in)"    , 
        asMETHODPR(RECT2D, IsInside, 
        (const RECT2D &) const, bool), 
        asCALL_THISCALL); assert( r >= 0 );

    //!< 幅 
	r = engine->RegisterObjectMethod("RECT2D", 
        "double Width() const", 
        asMETHOD(RECT2D,Width), 
        asCALL_THISCALL); assert( r >= 0 );

    //!< 高さ
	r = engine->RegisterObjectMethod("RECT2D", 
        "double Height() const", 
        asMETHOD(RECT2D,Height), 
        asCALL_THISCALL); assert( r >= 0 );

    // Register the object properties
	r = engine->RegisterObjectProperty("RECT2D", "double dLeft"  , offsetof(RECT2D, dLeft)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("RECT2D", "double dTop"   , offsetof(RECT2D, dTop)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("RECT2D", "double dRight" , offsetof(RECT2D, dRight)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("RECT2D", "double dBottom", offsetof(RECT2D, dBottom)); assert( r >= 0 );


    return 0;
}

END_AS_NAMESPACE
