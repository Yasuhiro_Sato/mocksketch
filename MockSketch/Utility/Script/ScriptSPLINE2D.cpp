/**
 * @brief			ScriptSPLINE2D�����t�@�C��
 * @file			ScriptSPLINE2D.cpp
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:36
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include <assert.h>
#include <sstream>
#include "ScriptBase.h"
#include "ScriptSPLINE2D.h"
#include "Utility/CUtility.h"
#include "DrawingObject/Primitive/SPLINE2D.H"
#include "DrawingObject/Primitive/SPLINE2D.h"

/*---------------------------------------------------*/
/*  MACROS                                           */
/*---------------------------------------------------*/
using namespace std;
#ifdef _DEBUG
#undef new 
#define new DEBUG_NEW
#endif
extern bool CALLBACK ConvPOINT2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);

bool CALLBACK ConvSPLINE2D( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("SPLINE2D");
    const SPLINE2D* pSpline2D = reinterpret_cast<const SPLINE2D*>(pValAddress);
    StdStringStream strmVal; 
 

    pValData->strVal = strmVal.str();
    return true;
}

//SPLINE2D�^�o�^
int RegisterSPLINE2DType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
    int iRet = engine->RegisterObjectType( "SPLINE2D", 0, asOBJ_REF); assert( iRet >= 0 );  

    //�E�H�b�`���X�g�\���p�o�^
    mapIdFunc[iRet] = ConvSPLINE2D;
   return iRet;
}

//SPLINE2D�o�^
int RegisterSPLINE2D(asIScriptEngine *engine)
{
    //REGSTER_BEGIN;
    RegsterRef<SPLINE2D>(engine, "SPLINE2D");

    return 0;
}

END_AS_NAMESPACE
