/**
 * @brief			ScriptSPLINE2D
 * @file			ScriptSPLINE2D.h
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:37
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#ifndef SCRIPT_SPLINE2D_H
#define SCRIPT_SPLINE2D_H

#include <angelscript.h>
#include <string>
#include "VAL_DATA.h"

int RegisterSPLINE2DType (asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc);
int RegisterSPLINE2D (asIScriptEngine *engine);

END_AS_NAMESPACE

#endif
