/**
 * @brief			ScriptEbd実装ファイル
 * @file			ScriptEbd.cpp
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:36
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include <assert.h>
#include <sstream>
#include "ScriptBase.h"
#include "ScriptTcb.h"
#include "Script/TCB.h"
#include "Utility/CUtility.h"
#include "DrawingObject/Primitive/SPLINE2D.H"
#include "Utility/CStdPropertyTree.h"

/*---------------------------------------------------*/
/*  MACROS                                           */
/*---------------------------------------------------*/
using namespace std;
#ifdef _DEBUG
#undef new 
#define new DEBUG_NEW
#endif 

bool CALLBACK ConvTcbType( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("Tcb");
    const TCB* pEbd = reinterpret_cast<const TCB*>(pValAddress);

    StdStringStream strmVal; 
    strmVal << _T("{");

    VAL_DATA valData;
    valData.strName     = _T("Data");

    /*
    valData.strTypeName = pEbd->GetAsTypeName();

    valData.strVal = CUtil::StrFormat(_T("%s"), pEbd->ToString().c_str());
    pValData->lstVal.push_back(valData);
    strmVal << valData.strVal;
    */

    strmVal << _T("}");

    pValData->strVal = strmVal.str();
    return true;
}


//PropertyItem型登録
int RegisterTcbType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
    int iRet = engine->RegisterObjectType( "TCB", 0, asOBJ_REF); assert( iRet >= 0 );  

    //ウォッチリスト表示用登録
    mapIdFunc[iRet] = ConvTcbType;
    return iRet;
}

//PropertyItem登録
int RegisterTcb(asIScriptEngine *engine)
{
   int r;

   // スクリプト側で生成する必要がないのでファクトリ関数は不要
    r = engine->RegisterObjectBehaviour("TCB", asBEHAVE_ADDREF, "void f()", asMETHOD(TCB,AddRef), asCALL_THISCALL);
    r = engine->RegisterObjectBehaviour("TCB", asBEHAVE_RELEASE, "void f()", asMETHOD(TCB,Release), asCALL_THISCALL);

    r = engine->RegisterObjectMethod("TCB", "int  GetId() const", asMETHOD(TCB, GetId), asCALL_THISCALL); assert( r >= 0 );
    r = engine->RegisterObjectMethod("TCB", "int  GetStep() const", asMETHOD(TCB, GetStep), asCALL_THISCALL); assert( r >= 0 );
    r = engine->RegisterObjectMethod("TCB", "void  SetStep(int)", asMETHOD(TCB, SetStep), asCALL_THISCALL); assert( r >= 0 );
    r = engine->RegisterObjectMethod("TCB", "int  GetPriority() const", asMETHOD(TCB, GetPriority), asCALL_THISCALL); assert( r >= 0 );
    r = engine->RegisterObjectMethod("TCB", "string  GetTaskName() const", asMETHOD(TCB, GetTaskName), asCALL_THISCALL); assert( r >= 0 );
    r = engine->RegisterObjectMethod("TCB", "TASK_STS  GetStatus() const", asMETHOD(TCB, GetStatus), asCALL_THISCALL); assert( r >= 0 );
    r = engine->RegisterObjectMethod("TCB", "int  GetThreadType() const", asMETHOD(TCB, GetThreadType), asCALL_THISCALL); assert( r >= 0 );

    /*
	r = engine->RegisterObjectProperty("TCB", "int  iId", offsetof(TCB, iId)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("TCB", "int  iPri", offsetof(TCB, iPri)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("TCB", "int  eSts", offsetof(TCB, eSts)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("TCB", "int  eThread", offsetof(TCB, eThread)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("TCB", "uint  dwTime", offsetof(TCB, dwTime)); assert( r >= 0 );
    */
    return 0;
}

END_AS_NAMESPACE
