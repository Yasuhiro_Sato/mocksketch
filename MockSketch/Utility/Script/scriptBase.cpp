/**
 * @brief			ScriptBase�����t�@�C��
 * @file			ScriptBase.cpp
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:36
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include <assert.h>
#include <sstream>
#include "scriptBase.h"
#include "Utility/CUtility.h"
#include "Utility/ExtText/CExtText.h"
#include "View/DRAWING_TYPE.h"
#include "DrawingObject/CDrawingObject.h"
#include "Script/TCB.h"


/*---------------------------------------------------*/
/*  MACROS                                           */
/*---------------------------------------------------*/
using namespace std;
#ifdef _DEBUG
#undef new 
#define new DEBUG_NEW
#endif






//==================================
//     ENUM
//==================================
int RegisterMockSketchEnum(asIScriptEngine *engine)
{
    int r;
    r = engine->RegisterEnum("DRAWING_TYPE"); assert( r >= 0 );
    r = engine->RegisterEnumValue("DRAWING_TYPE", "DT_NONE"     , DT_NONE); assert( r >= 0 );
    r = engine->RegisterEnumValue("DRAWING_TYPE", "DT_POINT"    , DT_POINT); assert( r >= 0 );
    r = engine->RegisterEnumValue("DRAWING_TYPE", "DT_CIRCLE"   , DT_CIRCLE); assert( r >= 0 );
    r = engine->RegisterEnumValue("DRAWING_TYPE", "DT_LINE"     , DT_LINE); assert( r >= 0 );
    r = engine->RegisterEnumValue("DRAWING_TYPE", "DT_TEXT"     , DT_TEXT); assert( r >= 0 );
    r = engine->RegisterEnumValue("DRAWING_TYPE", "DT_SPLINE"   , DT_SPLINE); assert( r >= 0 );
    r = engine->RegisterEnumValue("DRAWING_TYPE", "DT_ELLIPSE"  , DT_ELLIPSE); assert( r >= 0 );
    r = engine->RegisterEnumValue("DRAWING_TYPE", "DT_COMPOSITION_LINE"     , DT_COMPOSITION_LINE); assert( r >= 0 );

    r = engine->RegisterEnumValue("DRAWING_TYPE", "DT_DIM_L"      , DT_DIM_L); assert( r >= 0 );
    r = engine->RegisterEnumValue("DRAWING_TYPE", "DT_DIM_H"      , DT_DIM_H); assert( r >= 0 );
    r = engine->RegisterEnumValue("DRAWING_TYPE", "DT_DIM_V"      , DT_DIM_V); assert( r >= 0 );
    r = engine->RegisterEnumValue("DRAWING_TYPE", "DT_DIM_A"      , DT_DIM_A); assert( r >= 0 );
    r = engine->RegisterEnumValue("DRAWING_TYPE", "DT_DIM_D"      , DT_DIM_D); assert( r >= 0 );
    r = engine->RegisterEnumValue("DRAWING_TYPE", "DT_DIM_R"      , DT_DIM_R); assert( r >= 0 );
    r = engine->RegisterEnumValue("DRAWING_TYPE", "DT_DIM_C"      , DT_DIM_C); assert( r >= 0 );

    r = engine->RegisterEnumValue("DRAWING_TYPE", "DT_NODE"     , DT_NODE); assert( r >= 0 );
    r = engine->RegisterEnumValue("DRAWING_TYPE", "DT_PARTS"    , DT_PARTS); assert( r >= 0 );
    r = engine->RegisterEnumValue("DRAWING_TYPE", "DT_FIELD"    , DT_FIELD); assert( r >= 0 );
    r = engine->RegisterEnumValue("DRAWING_TYPE", "DT_CONNECTION_LINE", DT_CONNECTION_LINE); assert( r >= 0 );


    r = engine->RegisterEnum("POS_TYPE"); assert( r >= 0 );
    r = engine->RegisterEnumValue("POS_TYPE", "POS_LEFT"      , LEFT ); assert( r >= 0 );
    r = engine->RegisterEnumValue("POS_TYPE", "POS_RIGHT"     , RIGHT ); assert( r >= 0 );
    r = engine->RegisterEnumValue("POS_TYPE", "POS_MID"       , MID ); assert( r >= 0 );
    r = engine->RegisterEnumValue("POS_TYPE", "POS_TOP"       , TOP ); assert( r >= 0 );
    r = engine->RegisterEnumValue("POS_TYPE", "POS_BOTTOM"    , BOTTOM ); assert( r >= 0 );
    r = engine->RegisterEnumValue("POS_TYPE", "POS_CENTER"    , CENTER ); assert( r >= 0 );
    r = engine->RegisterEnumValue("POS_TYPE", "POS_TOP_LEFT"       , TOP_LEFT ); assert( r >= 0 );
    r = engine->RegisterEnumValue("POS_TYPE", "POS_TOP_MID"        , TOP_MID ); assert( r >= 0 );
    r = engine->RegisterEnumValue("POS_TYPE", "POS_BOTTTOM_LEFT"   , BOTTTOM_LEFT ); assert( r >= 0 );
    r = engine->RegisterEnumValue("POS_TYPE", "POS_BOTTTOM_RIGHT"  , BOTTTOM_RIGHT ); assert( r >= 0 );
    r = engine->RegisterEnumValue("POS_TYPE", "POS_BOTTTOM_MID"    , BOTTTOM_MID ); assert( r >= 0 );
    r = engine->RegisterEnumValue("POS_TYPE", "POS_CENTER_LEFT"    , CENTER_LEFT ); assert( r >= 0 );
    r = engine->RegisterEnumValue("POS_TYPE", "POS_CENTER_RIGHT"   , CENTER_RIGHT ); assert( r >= 0 );
    r = engine->RegisterEnumValue("POS_TYPE", "POS_CENTER_MID"     , CENTER_MID ); assert( r >= 0 );


    r = engine->RegisterEnum("MK_FLAG"); assert( r >= 0 );
    r = engine->RegisterEnumValue("MK_FLAG", "MK_LBUTTON"     , MK_LBUTTON); assert( r >= 0 );
    r = engine->RegisterEnumValue("MK_FLAG", "MK_RBUTTON"     , MK_RBUTTON); assert( r >= 0 );
    r = engine->RegisterEnumValue("MK_FLAG", "MK_SHIFT"       , MK_SHIFT); assert( r >= 0 );
    r = engine->RegisterEnumValue("MK_FLAG", "MK_CONTROL"     , MK_CONTROL); assert( r >= 0 );
    r = engine->RegisterEnumValue("MK_FLAG", "MK_MBUTTON"     , MK_MBUTTON); assert( r >= 0 );
    r = engine->RegisterEnumValue("MK_FLAG", "MK_XBUTTON1"    , MK_XBUTTON1); assert( r >= 0 );
    r = engine->RegisterEnumValue("MK_FLAG", "MK_XBUTTON2"    , MK_XBUTTON2); assert( r >= 0 );

    r = engine->RegisterEnum("NODE_CHANGE_TYPE"); assert( r >= 0 );
    r = engine->RegisterEnumValue("NODE_CHANGE_TYPE", "NCT_NONE"       , NCT_NONE); assert( r >= 0 );
    r = engine->RegisterEnumValue("NODE_CHANGE_TYPE", "NCT_POSITION"   , NCT_POSITION); assert( r >= 0 );
    r = engine->RegisterEnumValue("NODE_CHANGE_TYPE", "NCT_CONNECTION" , NCT_CONNECTION); assert( r >= 0 );

    r = engine->RegisterEnum("TASK_STS"); assert( r >= 0 );
    r = engine->RegisterEnumValue("TASK_STS", "TS_WAIT"       , TS_WAIT ); assert( r >= 0 );
    r = engine->RegisterEnumValue("TASK_STS", "TS_RUN"        , TS_RUN ); assert( r >= 0 );
    r = engine->RegisterEnumValue("TASK_STS", "TS_INTRRUPT"   , TS_INTRRUPT ); assert( r >= 0 );
    r = engine->RegisterEnumValue("TASK_STS", "TS_SUSPEND"    , TS_SUSPEND ); assert( r >= 0 );
    r = engine->RegisterEnumValue("TASK_STS", "TS_EXT"        , TS_EXT ); assert( r >= 0 );
    return 0;
}




//-------------------------------------------------------------------
//-------------------------------------------------------------------
//                        APPLICATION
//-------------------------------------------------------------------
//-------------------------------------------------------------------







//================================================================
//================================================================
//================================================================





//================================================================
//================================================================
//================================================================



//================================================================
//================================================================
//================================================================

/*
int RegisterRawPointer         (asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc);


bool CALLBACK ConvRawPointerType( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("POINT");
    const POINT* pPoint = reinterpret_cast<const POINT*>(pValAddress);
}

//DrawingView�^�o�^
int RegisterRawPointerType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
    int iRet = engine->RegisterObjectType("RawPointer", sizeof(POINT), asOBJ_VALUE | asOBJ_POD); assert( iRet >= 0 );

    //�E�H�b�`���X�g�\���p�o�^
    mapIdFunc[iRet] = ConvPOINTType;
    return iRet;
}


//POINT�o�^
int RegisterRawPointer(asIScriptEngine *engine)
{
	int r;
	// Register the object properties
	r = engine->RegisterObjectProperty("POINT", "int x", offsetof(POINT, x)); assert( r >= 0 );
    return 0;
}
*/

int RegisterPOINTType         (asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc);

bool CALLBACK ConvPOINTType( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("POINT");
    const POINT* pPoint = reinterpret_cast<const POINT*>(pValAddress);

    StdStringStream strmVal; 
    strmVal << _T("{");

    VAL_DATA valX;
    valX.strName     = _T("x");
    valX.strTypeName = _T("int");
    valX.strVal = CUtil::StrFormat(_T("%d"), pPoint->x);
    pValData->lstVal.push_back(valX);
    strmVal << valX.strVal, _T(",");

    VAL_DATA valY;
    valY.strName     = _T("y");
    valY.strTypeName = _T("int");
    valY.strVal = CUtil::StrFormat(_T("%d"), pPoint->y);
    pValData->lstVal.push_back(valY);
    strmVal << valY.strVal;
    strmVal << _T("}");

    pValData->strVal = strmVal.str();
    return true;
}

//DrawingView�^�o�^
int RegisterPOINTType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
    int iRet = engine->RegisterObjectType("POINT", sizeof(POINT), asOBJ_VALUE | asOBJ_POD); assert( iRet >= 0 );

    //�E�H�b�`���X�g�\���p�o�^
    mapIdFunc[iRet] = ConvPOINTType;
    return iRet;
}

//POINT�o�^
int RegisterPOINT(asIScriptEngine *engine)
{
	int r;
	// Register the object properties
	r = engine->RegisterObjectProperty("POINT", "int x", offsetof(POINT, x)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("POINT", "int y", offsetof(POINT, y)); assert( r >= 0 );
    return 0;
}

//================================================================
//================================================================
//================================================================



bool CALLBACK ConvMOUSE_MOVE_POSType( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("MOUSE_MOVE_POS");
    const MOUSE_MOVE_POS* pMouseMove = reinterpret_cast<const MOUSE_MOVE_POS*>(pValAddress);

    StdStringStream strmVal; 
    strmVal << _T("{");

   
    VAL_DATA valPos;
    valPos.strName     = _T("ptSel");
    ConvPOINTType( &valPos, &pMouseMove->ptSel, pContext);
    pValData->lstVal.push_back(valPos);
    strmVal << valPos.strVal;

    strmVal << valPos.strVal, _T(",");
 
    VAL_DATA valFlag;
    valFlag.strName     = _T("Flags");
    valFlag.strTypeName = _T("uint");
    valFlag.strVal = CUtil::StrFormat(_T("%x"), pMouseMove->nFlags);
    pValData->lstVal.push_back(valFlag);

    strmVal << valFlag.strVal;
    strmVal << _T("}");

    pValData->strVal = strmVal.str();
    return true;
}

//DrawingView�^�o�^
int RegisterMOUSE_MOVE_POSType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
    int iRet = engine->RegisterObjectType("MOUSE_MOVE_POS", sizeof(MOUSE_MOVE_POS), asOBJ_VALUE | asOBJ_POD); assert( iRet >= 0 );

    //�E�H�b�`���X�g�\���p�o�^
    mapIdFunc[iRet] = ConvMOUSE_MOVE_POSType;
    return iRet;
}

//MOUSE_MOVE_POS�o�^
int RegisterMOUSE_MOVE_POS(asIScriptEngine *engine)
{
	int r;
	// Register the object properties
	r = engine->RegisterObjectProperty("MOUSE_MOVE_POS", "POINT ptSel", offsetof(MOUSE_MOVE_POS, ptSel)); assert( r >= 0 );
	r = engine->RegisterObjectProperty("MOUSE_MOVE_POS", "int nFlags", offsetof(MOUSE_MOVE_POS, nFlags)); assert( r >= 0 );
    return 0;
}

//================================================================
//================================================================
//================================================================

int RegisterRECTType         (asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc);


bool CALLBACK ConvRECTType( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("RECT");
    const RECT* pRect = reinterpret_cast<const RECT*>(pValAddress);

    StdStringStream strmVal; 
    strmVal << _T("{");
   

    VAL_DATA valLeft;
    valLeft.strName     = _T("left");
    valLeft.strTypeName = _T("int");
    valLeft.strVal = CUtil::StrFormat(_T("%d"), pRect->left);
    pValData->lstVal.push_back(valLeft);
    strmVal << valLeft.strVal, _T(",");

    VAL_DATA valTop;
    valTop.strName     = _T("top");
    valTop.strTypeName = _T("int");
    valTop.strVal = CUtil::StrFormat(_T("%d"), pRect->top);
    pValData->lstVal.push_back(valTop);
    strmVal << valTop.strVal, _T(",");

    VAL_DATA valRight;
    valRight.strName     = _T("right");
    valRight.strTypeName = _T("int");
    valRight.strVal = CUtil::StrFormat(_T("%d"), pRect->right);
    pValData->lstVal.push_back(valRight);
    strmVal << valRight.strVal, _T(",");

    VAL_DATA valBottom;
    valBottom.strName     = _T("bottom");
    valBottom.strTypeName = _T("int");
    valBottom.strVal = CUtil::StrFormat(_T("%d"), pRect->bottom);
    pValData->lstVal.push_back(valBottom);

    strmVal << valBottom.strVal;
    strmVal << _T("}");

    pValData->strVal = strmVal.str();
    return true;
}

//RECT�^�o�^
int RegisterRECTType(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
    int iRet = engine->RegisterObjectType("RECT", sizeof(RECT), asOBJ_VALUE | asOBJ_POD); assert( iRet >= 0 );

    //�E�H�b�`���X�g�\���p�o�^
    mapIdFunc[iRet] = ConvRECTType;
    return iRet;
}

//RECT�o�^
int RegisterRECT(asIScriptEngine *engine)
{
	int r;
	// Register the object properties
	r = engine->RegisterObjectProperty("RECT", "int left"   , offsetof(RECT, left));    assert( r >= 0 );
	r = engine->RegisterObjectProperty("RECT", "int top"    , offsetof(RECT, top));     assert( r >= 0 );
	r = engine->RegisterObjectProperty("RECT", "int right"  , offsetof(RECT, right));   assert( r >= 0 );
	r = engine->RegisterObjectProperty("RECT", "int bottom" , offsetof(RECT, bottom));  assert( r >= 0 );
    return 0;
}



//================================================================
//================================================================
//================================================================
//CStdPropertyItem
//int RegisterAnyType         (asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc);

