/**
 * @brief			scriptBase
 * @file			CDrawingObject.cpp
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:37
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/

#ifndef SCRIPTBASE_H
#define SCRIPTBASE_H

#include <angelscript.h>
#include <string>
#include "VAL_DATA.h"

BEGIN_AS_NAMESPACE

#ifdef _DEBUG
#undef new 
#endif
/*
class RawPointer
{
public:
    RawPointer():Pointer(NULL){;}
    ~RawPointer(){;}

    virtual RawPointer& operator = (const void* pVal);

    virtual RawPointer& operator = (const void* pVal);


    void* Pointer;
};
*/


template <class T>
class AsConstructor
{
public:
    static void Default(T *self)
    {
	    new(self) T();
    }

    static void Copy(const T &other, T *self)
    {
	    new(self) T(other);
    }

    static void Destruct(T *obj)
    {
	    obj->~T();
    }
};

#ifdef _DEBUG
#undef new 
#define new DEBUG_NEW
#endif

template<class A, class B> B* ASRefCast(A* a)
{
	if (a == NULL)
    {
        return NULL;
    }

	B* b = dynamic_cast<B*>(a);

	if (b!=NULL) 
    {
        b->AddRef();
	}
	return b;
}


template <class T>
class AsFactory
{
public:
    static T *Default()
    {
        T *pRet;
        pRet = new T();
        CAsRefCtrl::GetInstance()->AddAsObj(pRet);
        return pRet;
    }

    static T *Copy(const T &other)
    {
        T *pRet;
        pRet = new T(other); 
        CAsRefCtrl::GetInstance()->AddAsObj(pRet);
	    return pRet;
    }  
};

#ifdef _WIN64
template <class T>
unsigned __int64 GetRawAddress(T* pVal)
{
    return reinterpret_cast<unsigned __int64>(pVal);
}
#else
template <class T>
unsigned __int32 GetRawAddress(T* pVal)
{
    return reinterpret_cast<unsigned __int32>(pVal);
}
#endif

template <class T>
void RegsterRef(asIScriptEngine *engine, const char * scriptName)
{
    int r;
    std::stringstream strmFuncRef;
    strmFuncRef << scriptName  << " @f()";
    r = engine->RegisterObjectBehaviour(scriptName, 
                                asBEHAVE_FACTORY, 
                                strmFuncRef.str().c_str(), 
                                asFUNCTION( AsFactory<T>::Default), 
                                asCALL_CDECL); assert( r >= 0 );

    strmFuncRef.str("");
    strmFuncRef << scriptName  << " @f(const " << scriptName <<  "&in)";

    r = engine->RegisterObjectBehaviour(scriptName, asBEHAVE_FACTORY,
        strmFuncRef.str().c_str(), 
        asFUNCTION(AsFactory<T>::Copy), 
        asCALL_CDECL); 
    assert( r >= 0 );

    r = engine->RegisterObjectBehaviour(scriptName, asBEHAVE_ADDREF,
            "void f()",   asMETHOD(T ,AddRef) ,asCALL_THISCALL);
    assert( r >= 0 );

    r = engine->RegisterObjectBehaviour(scriptName, asBEHAVE_RELEASE,
            "void f()",   asMETHOD(T ,Release), asCALL_THISCALL);
    assert( r >= 0 );
}


template <class T>
void RegsterVal(asIScriptEngine *engine, const char * scriptName)
{
    int r;
    r = engine->RegisterObjectBehaviour(scriptName, 
                                asBEHAVE_CONSTRUCT, 
                                "void f()", 
                                asFUNCTION( AsConstructor<T>::Default), 
                                asCALL_CDECL_OBJLAST); assert( r >= 0 );

    std::stringstream strmFuncRef;
    strmFuncRef.str("");
    strmFuncRef << "void f(const " << scriptName <<  "&in)";

    r = engine->RegisterObjectBehaviour(scriptName, asBEHAVE_CONSTRUCT,
        strmFuncRef.str().c_str(), 
        asFUNCTION(AsConstructor<T>::Copy), 
        asCALL_CDECL_OBJLAST); 

    r = engine->RegisterObjectBehaviour(scriptName, 
        asBEHAVE_DESTRUCT, "void f()", 
        asFUNCTION(AsConstructor<T>::Destruct), 
        asCALL_CDECL_OBJLAST); assert( r >= 0 );

}


template <class T>
void RegsterBasicMethod(asIScriptEngine *engine, const char * scriptName)
{
    int r;
    r = engine->RegisterObjectMethod(scriptName,       
        "void Move  (const POINT2D &in)",               
        asMETHOD(T,Move), asCALL_THISCALL);      
        COMMENT_ASSERT( r >= 0, _T("Move")   );         

    r = engine->RegisterObjectMethod(scriptName,       
        "void Rotate(const POINT2D &in, double)",       
        asMETHOD(T,Rotate), asCALL_THISCALL);    
        COMMENT_ASSERT( r >= 0, _T("Rotate") );         

    r = engine->RegisterObjectMethod(scriptName,       
        "void Scl(const POINT2D &in, double, double)",  
        asMETHOD(T,Scl), asCALL_THISCALL);       
        COMMENT_ASSERT( r >= 0, _T("Scl")    );         

    r = engine->RegisterObjectMethod(scriptName,       
        "void Mirror(const LINE2D &in)",                
        asMETHOD(T,Mirror), asCALL_THISCALL);    
        COMMENT_ASSERT( r >= 0, _T("Mirror") );         

    r = engine->RegisterObjectMethod(scriptName,       
        "void Matrix(const MAT2D &in)",                 
        asMETHOD(T,Matrix), asCALL_THISCALL);    
        COMMENT_ASSERT( r >= 0, _T("Matrix") );         

    r = engine->RegisterObjectMethod(scriptName,                
        "void Trim(const POINT2D &in,const POINT2D &in, bool)",  
        asMETHOD(T,Trim), asCALL_THISCALL);               
        COMMENT_ASSERT( r >= 0, _T("Trim") );                    

    r = engine->RegisterObjectMethod(scriptName,                
        "RECT2D& GetBounds() const",                             
        asMETHOD(T,GetBounds), asCALL_THISCALL);          
        COMMENT_ASSERT( r >= 0, _T("GetBounds") );              
}


template <class T>
void RegsterDrawingObject(asIScriptEngine *engine, const char * scriptName)
{
    int r;
    r = engine->RegisterObjectMethod(scriptName,
        "DrawingObject@ Break(const POINT2D &in)",
        asMETHOD(T,Break), asCALL_THISCALL);
        COMMENT_ASSERT( r >= 0, _T("Break") );

    r = engine->RegisterObjectMethod(scriptName,
        "bool IsInner(const RECT2D &in, bool)",
        asMETHOD(T,IsInner), asCALL_THISCALL);
        COMMENT_ASSERT( r >= 0, _T("IsInner") );

    r = engine->RegisterObjectMethod(scriptName,
        "POINT& GetScreenCoordinate(POINT2D &in, DrawingView& in)",
        asMETHOD(T,GetScreenCoordinate), asCALL_THISCALL);
        COMMENT_ASSERT( r >= 0, _T("GetScreenCoordinate") );

    r = engine->RegisterObjectMethod(scriptName,
        "POINT2D& GetWorldCoordinate(POINT &in, DrawingView& in)",
        asMETHOD(T,GetWorldCoordinate), asCALL_THISCALL);
        COMMENT_ASSERT( r >= 0, _T("GetWorldCoordinate") );

    r = engine->RegisterObjectMethod(scriptName,
        "string get_Name() const",
        asMETHOD(T,GetName), asCALL_THISCALL);
        COMMENT_ASSERT( r >= 0, _T("get_Name") );

    r = engine->RegisterObjectMethod(scriptName,
        "DRAWING_TYPE GetType() const",
        asMETHOD(T,GetType),asCALL_THISCALL);
        COMMENT_ASSERT( r >= 0, _T("GetType") );

    r = engine->RegisterObjectMethod(scriptName,
        "int GetId() const",
        asMETHOD(T,GetId),asCALL_THISCALL);
        COMMENT_ASSERT( r >= 0, _T("GetId") );

    r = engine->RegisterObjectMethod(scriptName,
        "uint GetColor() const",
        asMETHOD(T,GetColor),asCALL_THISCALL);
        COMMENT_ASSERT( r >= 0, _T("GetColor") );

    r = engine->RegisterObjectMethod(scriptName,
        "void SetColor(uint)",
        asMETHOD(T,SetColor),asCALL_THISCALL);
        COMMENT_ASSERT( r >= 0, _T("SetColor") );

    r = engine->RegisterObjectMethod(scriptName,
        "int GetLayer() const",
        asMETHOD(T,GetLayer),asCALL_THISCALL);
        COMMENT_ASSERT( r >= 0, _T("GetLayer") );

    r = engine->RegisterObjectMethod(scriptName,
        "void SetLayer(int)",
        asMETHOD(T,SetLayer),asCALL_THISCALL);
        COMMENT_ASSERT( r >= 0, _T("SetLayer") );

    r = engine->RegisterObjectMethod(scriptName,
        "void CalcIntersection(POINT2D_VECTOR& out,const DrawingObject& in, bool)",
        asMETHOD(T,CalcIntersection), asCALL_THISCALL);
        COMMENT_ASSERT( r >= 0, _T("CalcIntersection") );

/*
    r = engine->RegisterObjectMethod(scriptName,
        "void CalcIntersectionPoint(POINT2D_VECTOR& out,const DrawingObject@, POINT2D &in, bool)",
        asFUNCTION(CalcIntersectionPoint), asCALL_CDECL_OBJLAST);
        COMMENT_ASSERT( r >= 0, _T("CalcIntersectionPoint") );
*/

    r = engine->RegisterObjectMethod(scriptName,
        "string GetName() const",
        asMETHOD(T,GetName),asCALL_THISCALL);
        COMMENT_ASSERT( r >= 0, _T("GetName") );

    r = engine->RegisterObjectMethod(scriptName,
        "void SetName(string)",
        asMETHOD(T,SetName),asCALL_THISCALL);
        COMMENT_ASSERT( r >= 0, _T("SetName") );

    r = engine->RegisterObjectMethod(scriptName,
        "bool IsVisible() const",
        asMETHOD(T,IsVisible),asCALL_THISCALL);
        COMMENT_ASSERT( r >= 0, _T("IsVisible") );

    r = engine->RegisterObjectMethod(scriptName,
        "void SetVisible(bool)",
        asMETHOD(T,SetVisible),asCALL_THISCALL);
        COMMENT_ASSERT( r >= 0, _T("SetVisible") );

    r = engine->RegisterObjectMethod(scriptName,
        "double Distance(const POINT2D& in) const",
        asMETHOD(T,Distance),asCALL_THISCALL);
        COMMENT_ASSERT( r >= 0, _T("SetVisible") );

    r = engine->RegisterObjectMethod(scriptName,                
        "Any@ GetPropertySetVal(string &in)",                   
        asMETHOD(T,GetPropertySetVal),asCALL_THISCALL);     
        COMMENT_ASSERT( r >= 0, _T("GetUserProperty") );

#if 0
        r = engine->RegisterObjectMethod(scriptName,                
        "Any@ GetPropertySetVal(string &in)",                      
        asMETHOD(T,GetPropertySetValS),asCALL_THISCALL);    
        COMMENT_ASSERT( r >= 0, _T("GetUserPropertyS") );        
#endif

    r = engine->RegisterObjectMethod(scriptName,                
        "bool SetPropertySetDefMin(string &in, string &in)",                      
        asMETHOD(T,SetPropertySetDefMin),asCALL_THISCALL);    
        COMMENT_ASSERT( r >= 0, _T("SetPropertySetDefMin") );        

    r = engine->RegisterObjectMethod(scriptName,                
        "bool SetPropertySetDefMax(string &in, string &in)",                      
        asMETHOD(T,SetPropertySetDefMax),asCALL_THISCALL);    
        COMMENT_ASSERT( r >= 0, _T("SetPropertySetDefMax") );        

    r = engine->RegisterObjectMethod(scriptName,                
        "bool SetPropertySetDefNote(string &in, string &in)",                      
        asMETHOD(T,SetPropertySetDefNote),asCALL_THISCALL);    
        COMMENT_ASSERT( r >= 0, _T("SetPropertySetDefNote") );        

    r = engine->RegisterObjectMethod(scriptName,                
        "bool SetPropertySetVal(string &in, const ?& in)",                      
        asMETHOD(T,SetPropertySetValAny),asCALL_THISCALL);    
        COMMENT_ASSERT( r >= 0, _T("SetPropertySetVal") );        

    r = engine->RegisterObjectMethod(scriptName,                
        "POINT2D ConvWorldToLocal(POINT2D &in)",                      
        asMETHOD(T,ConvWorldToLocal),asCALL_THISCALL);    
        COMMENT_ASSERT( r >= 0, _T("ConvWorldToLocal") );        

    r = engine->RegisterObjectMethod(scriptName,                
        "POINT2D ConvLocalToWorld(POINT2D &in)",                      
        asMETHOD(T,ConvLocalToWorld),asCALL_THISCALL);    
        COMMENT_ASSERT( r >= 0, _T("ConvLocalToWorld") );        

#ifdef _WIN64
    r = engine->RegisterObjectMethod(scriptName,
        "uint64 GetRawAddress()",
        asFUNCTION((GetRawAddress<T>)), 
        asCALL_CDECL_OBJLAST);
        COMMENT_ASSERT( r >= 0, _T("GetAddress") );
#else
    r = engine->RegisterObjectMethod(scriptName,
        "uint32 GetRawAddress()",
        asFUNCTION((GetRawAddress<T>)), 
        asCALL_CDECL_OBJLAST);
        COMMENT_ASSERT( r >= 0, _T("GetAddress") );
#endif
        //!< ダウンキャスト
	r = engine->RegisterObjectMethod(scriptName, 
        "DrawingObject@ GetDrawingObject()",
        asMETHODPR(T, GetDrawingObject,(void), CDrawingObject*),
        asCALL_THISCALL);
        COMMENT_ASSERT( r >= 0, _T("GetDrawingObject") );

        //!< ユーザデータ取得
    r = engine->RegisterObjectMethod(scriptName, 
	    "bool GetUserData(?&out) const",
        asMETHODPR(T,GetUserData,(void*,int),bool), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod(scriptName, 
	    "bool GetUserData(int64&out) const",
        asMETHODPR(T,GetUserData,(__int64&),bool), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod(scriptName, 
	    "bool GetUserData(double&out) const",
        asMETHODPR(T,GetUserData,(double&),bool), asCALL_THISCALL); 
        assert( r >= 0 );

        //!< ユーザデータ設定
    r = engine->RegisterObjectMethod(scriptName, 
        "void SetUserData(?&in)" , 
        asMETHODPR(T,SetUserData,(void*,int),void), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod(scriptName, 
	    "void SetUserData(int64&in) const",
        asMETHODPR(T,SetUserData,(__int64&),void), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod(scriptName, 
	    "void GetUserData(double&in) const",
        asMETHODPR(T,SetUserData,(double&),void), asCALL_THISCALL); 
        assert( r >= 0 );
        
}

template <class T>
void RegisterDrawingScriptBaseCommon(asIScriptEngine *engine, const char * scriptName)
{
    int r;
    r = engine->RegisterObjectMethod(scriptName,                
        "Any@ GetUserProperty(string &in)",                   
        asMETHOD(T,GetUserProperty),asCALL_THISCALL);     
        COMMENT_ASSERT( r >= 0, _T("GetUserProperty") );
#if 0
    r = engine->RegisterObjectMethod(scriptName,                
        "Any@ GetUserProperty(string &in)",                      
        asMETHOD(T,GetUserPropertyS),asCALL_THISCALL);    
        COMMENT_ASSERT( r >= 0, _T("GetUserPropertyS") );        
#endif
}


template <class T>
void RegisterDrawingPartsCommon(asIScriptEngine *engine, const char * scriptName)
{
    int r;
   //!< 角度取得
	r = engine->RegisterObjectMethod(scriptName, 
        "double GetAngle() const", 
        asMETHOD(T,GetAngle), 
        asCALL_THISCALL); assert( r >= 0 );

    //!< 角度設定
	r = engine->RegisterObjectMethod(scriptName, 
        "void SetAngle(double)", 
        asMETHOD(T,SetAngle), 
        asCALL_THISCALL); assert( r >= 0 );

    //!< 位置取得
	r = engine->RegisterObjectMethod(scriptName, 
        "const POINT2D& GetPoint() const", 
        asMETHOD(T,GetPoint), 
        asCALL_THISCALL); assert( r >= 0 );

    //!< 位置取得
	r = engine->RegisterObjectMethod(scriptName, 
        "void GetPoint2(POINT2D &out) const", 
        asMETHOD(T,GetPoint2), 
        asCALL_THISCALL); assert( r >= 0 );

    //!< 位置設定
	r = engine->RegisterObjectMethod(scriptName, 
        "void SetPoint(POINT2D &in)", 
        asMETHOD(T,SetPoint), 
        asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod(scriptName, 
        "void MoveInnerPosition  (const POINT2D &in)",
        asMETHOD(T,MoveInnerPosition), asCALL_THISCALL);
        COMMENT_ASSERT( r >= 0, _T("Move")   );

    r = engine->RegisterObjectMethod(scriptName, 
        "void RotateAbs  (double)",
        asMETHOD(T,RotateAbs), asCALL_THISCALL);
        COMMENT_ASSERT( r >= 0, _T("Move")   );

    //オブジェクト追加
	r = engine->RegisterObjectMethod(scriptName, 
        "bool AddData(DrawingObject@)", 
        asMETHOD(T,AddData), 
        asCALL_THISCALL); assert( r >= 0 );

	r = engine->RegisterObjectMethod(scriptName, 
        "bool AddData(DrawingLine@)", 
        asMETHOD(T,AddData), 
        asCALL_THISCALL); assert( r >= 0 );

    //!< オブジェクト取得(名称)
	r = engine->RegisterObjectMethod(scriptName, 
        "DrawingObject@ GetObject(string &in)", 
         asMETHOD(T,GetObjectByName), 
        asCALL_THISCALL); assert( r >= 0 );
#if 0
    //!< オブジェクト取得(名称)
	r = engine->RegisterObjectMethod(scriptName, 
        "DrawingObject@ GetObject(string &in)", 
         asMETHOD(T,GetObjectByNameS), 
        asCALL_THISCALL); assert( r >= 0 );
#endif
    //!< エリア選択
    r = engine->RegisterObjectMethod(scriptName, 
        "bool GetAreaObjects(DrawingObjectPtr_VECTOR& out, POINT2D& in, POINT2D& in, DRAWING_TYPE& in)", 
        //asMETHOD(T,GetAreaObjects), 
        asMETHODPR(T,GetAreaObjects,(std::vector<CDrawingObject*>*, 
                    POINT2D, POINT2D, DRAWING_TYPE), bool), 
        asCALL_THISCALL); assert( r >= 0 );

    //!< マトリクス取得取得
	r = engine->RegisterObjectMethod(scriptName, 
        "MAT2D@ GetMatrix() const", 
         asMETHOD(T,GetMatrix2), 
        asCALL_THISCALL); assert( r >= 0 );

    //!< 描画用（画面座標変換用）マトリクス取得
	r = engine->RegisterObjectMethod(scriptName, 
        "MAT2D@ GetDrawingMatrix() const", 
         asMETHOD(T,GetDrawingMatrix), 
        asCALL_THISCALL); assert( r >= 0 );
}

template <class T>
void RegisterDrawingLineCommon(asIScriptEngine *engine, const char * scriptName)
{
    int r;
    //!< 線幅設定
	r = engine->RegisterObjectMethod(scriptName, 
        "void SetLineWidth(int)", 
        asMETHOD(T, SetLineWidth),
        asCALL_THISCALL); assert( r >= 0 );

    //!< 線幅取得
	r = engine->RegisterObjectMethod(scriptName, 
        "int GetLineWidth()", 
        asMETHOD(T, GetLineWidth),
        asCALL_THISCALL); assert( r >= 0 );

    //!< 線種設定
	r = engine->RegisterObjectMethod(scriptName, 
        "void SetLineType(int)", 
        asMETHOD(T, SetLineType),
        asCALL_THISCALL); assert( r >= 0 );

    //!< 線種取得
	r = engine->RegisterObjectMethod(scriptName, 
        "int GetLineType()", 
        asMETHOD(T, GetLineType),
        asCALL_THISCALL); assert( r >= 0 );
}


bool CALLBACK ConvDrawingObject(VAL_DATA* pValData, const void* pValAddress, void* pContext);


int RegisterMockSketchEnum(asIScriptEngine *engine);


int RegisterPOINTType               (asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc);
int RegisterRECTType                (asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc);
int RegisterMOUSE_MOVE_POSType      (asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc);

int RegisterPOINT               (asIScriptEngine *engine);
int RegisterRECT                (asIScriptEngine *engine);
int RegisterMOUSE_MOVE_POS      (asIScriptEngine *engine);






END_AS_NAMESPACE

#endif
