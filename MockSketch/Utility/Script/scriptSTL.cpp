#include "stdafx.h"
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <new>
#include "Utility//CUtility.h"
#include "MockSketch.h"
#include "Script/CScriptEngine.h"
#include "RegisterVector.h"
#include "VAL_DATA.h"

#include "scriptSTL.h"

/*---------------------------------------------------*/
/*  MACROS                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#undef new 
//#define new DEBUG_NEW
#endif

BEGIN_AS_NAMESPACE
extern bool CALLBACK ConvPOINT2D( VAL_DATA* pValData, const void* pValAddress, void* pContext);
REGISTER_VECTOR(POINT2D, POINT2D_VECTOR);
REGISTER_VECTOR2(CDrawingObject*, DrawingObject, DrawingObjectPtr_VECTOR);


#if 0

typedef std::vector<POINT2D> POINT2D_VECTOR;

static void ConstructPOINT2D_VECTOR( POINT2D_VECTOR* thisPointer)
{
    //placement new
    new( thisPointer ) POINT2D_VECTOR();
}
 
static void DestructPOINT2D_VECTOR(POINT2D_VECTOR *thisPointer)
{
    thisPointer->~POINT2D_VECTOR();
}


POINT2D& AtPOINT2D_VECTOR(unsigned int iIndex, POINT2D_VECTOR* pVec)
{
    return pVec->at(iIndex);
}

void InsertPOINT2D_VECTOR(unsigned int iIndex, const POINT2D& val, POINT2D_VECTOR* pVec)
{
    POINT2D_VECTOR::iterator it = pVec->begin();
    it += iIndex;
    pVec->insert(it, val);
}


void ErasePOINT2D_VECTOR(unsigned int iIndex, POINT2D_VECTOR* pVec)
{
    POINT2D_VECTOR::iterator it = pVec->begin();
    it += iIndex;
    pVec->erase(it);
}

bool CALLBACK ConvPOINT2D_VECTOR( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    return true;
}

void RegsterPOINT2D_VECTOR(asIScriptEngine *engine)
{
    int r;
	r = engine->RegisterObjectBehaviour("POINT2D_VECTOR", 
        asBEHAVE_CONSTRUCT,  "void f()",  
        asFUNCTION(ConstructPOINT2D_VECTOR), 
        asCALL_CDECL_OBJLAST);
        assert( r >= 0 );

	r = engine->RegisterObjectBehaviour("POINT2D_VECTOR", 
        asBEHAVE_DESTRUCT,  "void f()",  
        asFUNCTION(DestructPOINT2D_VECTOR), 
        asCALL_CDECL_OBJLAST); 
        assert( r >= 0 );

	r = engine->RegisterObjectMethod("POINT2D_VECTOR", 
        "POINT2D_VECTOR &opAssign(const POINT2D_VECTOR &in)", 
        asMETHODPR(POINT2D_VECTOR, operator=, (const POINT2D_VECTOR &),
        POINT2D_VECTOR&), asCALL_THISCALL); assert( r >= 0 );

    r = engine->RegisterObjectMethod("POINT2D_VECTOR", 
        "POINT2D& at(uint)", 
        asFUNCTION(AtPOINT2D_VECTOR), 
        asCALL_CDECL_OBJLAST); 
        assert( r >= 0 );

	r = engine->RegisterObjectMethod("POINT2D_VECTOR", 
        "POINT2D &opIndex(uint)", 
        asFUNCTION(AtPOINT2D_VECTOR), 
        asCALL_CDECL_OBJLAST); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("POINT2D_VECTOR", 
        "void clear()", 
        asMETHOD(POINT2D_VECTOR, 
        clear), asCALL_THISCALL); 
        assert( r >= 0 );

        r = engine->RegisterObjectMethod("POINT2D_VECTOR", 
        "void resize(uint)", 
        asMETHODPR(POINT2D_VECTOR, 
        resize, (asUINT), void), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("POINT2D_VECTOR", 
        "uint size()", 
        asMETHOD(POINT2D_VECTOR, 
        size), asCALL_THISCALL); 
        assert( r >= 0 );

	r = engine->RegisterObjectMethod("POINT2D_VECTOR", 
        "bool empty() const", 
        asMETHOD(POINT2D_VECTOR, empty), 
        asCALL_THISCALL); 
        assert( r >= 0 );
//TODO: ����
/*
	r = engine->RegisterObjectMethod("POINT2D_VECTOR", 
        "void pop_back(const POINT2D&in)", 
        asMETHOD(POINT2D_VECTOR, pop_back), 
        asCALL_THISCALL); 
        assert( r >= 0 );

	r = engine->RegisterObjectMethod("POINT2D_VECTOR",
			"void push_back(const POINT2D&in)",
			asMETHOD(POINT2D_VECTOR, push_back),
			asCALL_THISCALL);
		assert(r >= 0);
		*/
	r = engine->RegisterObjectMethod("POINT2D_VECTOR",
        "void insert(uint, const POINT2D&in)", 
        asFUNCTION(InsertPOINT2D_VECTOR), 
        asCALL_CDECL_OBJLAST); 
        assert( r >= 0 );

	r = engine->RegisterObjectMethod("POINT2D_VECTOR", 
        "void erase(uint)", 
        asFUNCTION(ErasePOINT2D_VECTOR), 
        asCALL_CDECL_OBJLAST); 
        assert( r >= 0 );

}

int RegisterTypePOINT2D_VECTOR(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
    int iRet = engine->RegisterObjectType("POINT2D_VECTOR", 
               sizeof(POINT2D_VECTOR), 
               asOBJ_VALUE | asOBJ_APP_CLASS_CD);
               assert( iRet >= 0 );

    mapIdFunc[iRet] = ConvPOINT2D_VECTOR;
    return iRet;
}
#else


//==========================================================
/*
RegisterVector<POINT2D> g_POINT2D_VEC;
typedef  std::vector<POINT2D> POINT2D_VECTOR;

static void Constructer_POINT2D_VEC(POINT2D_VECTOR* thisPointer)
{
    new( thisPointer ) POINT2D_VECTOR();
}

static void Destructor_POINT2D_VEC(POINT2D_VECTOR* thisPointer)
{
        thisPointer->~POINT2D_VECTOR();
}

bool CALLBACK ConvPOINT2D_VEC( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    return g_POINT2D_VEC.Conv( pValData, pValAddress, pContext);
}

int RegisterTypePOINT2D_VEC(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
    g_POINT2D_VEC.SetName("POINT2D_VECTOR");
    int iRet = g_POINT2D_VEC.RegisterObjectType(engine);
    mapIdFunc[iRet] = ConvPOINT2D_VEC;
    return iRet;
}

void RegisterPOINT2D_VEC(asIScriptEngine *engine)
{
    int r;
    g_POINT2D_VEC.SetConvFunc(ConvPOINT2D_VEC);
    r = engine->RegisterObjectBehaviour("POINT2D_VECTOR", asBEHAVE_CONSTRUCT,  "void f()", asFUNCTION(Constructer_POINT2D_VEC),   asCALL_CDECL_OBJLAST);  assert( r >= 0 );
    r = engine->RegisterObjectBehaviour("POINT2D_VECTOR",  asBEHAVE_DESTRUCT,  "void f()", asFUNCTION(Destructor_POINT2D_VEC),    asCALL_CDECL_OBJLAST);  assert( r >= 0 );

    g_POINT2D_VEC.Register(engine);
}
*/

#endif

#if 0

typedef std::vector<CDrawingObject*> DrawingObjectPtr_VECTOR;

static void ConstructDrawingObjectPtr_VECTOR( DrawingObjectPtr_VECTOR* thisPointer)
{
    //placement new
    new( thisPointer ) DrawingObjectPtr_VECTOR();
}
 
static void DestructDrawingObjectPtr_VECTOR(DrawingObjectPtr_VECTOR *thisPointer)
{
    thisPointer->~DrawingObjectPtr_VECTOR();
}


CDrawingObject* AtDrawingObjectPtr_VECTOR(unsigned int iIndex, DrawingObjectPtr_VECTOR* pVec)
{
    return pVec->at(iIndex);
}

void InsertDrawingObjectPtr_VECTOR(unsigned int iIndex, CDrawingObject* val, DrawingObjectPtr_VECTOR* pVec)
{
    DrawingObjectPtr_VECTOR::iterator it = pVec->begin();
    it += iIndex;
    pVec->insert(it, val);
}


void EraseDrawingObjectPtr_VECTOR(unsigned int iIndex, DrawingObjectPtr_VECTOR* pVec)
{
    DrawingObjectPtr_VECTOR::iterator it = pVec->begin();
    it += iIndex;
    pVec->erase(it);
}

bool CALLBACK ConvDrawingObjectPtr_VECTOR( VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    return true;
}

void RegisterDrawingObjectPtr_VECTOR(asIScriptEngine *engine)
{
    int r;
	r = engine->RegisterObjectBehaviour("DrawingObjectPtr_VECTOR", 
        asBEHAVE_CONSTRUCT,  "void f()",  
        asFUNCTION(ConstructDrawingObjectPtr_VECTOR), 
        asCALL_CDECL_OBJLAST);
        assert( r >= 0 );

	r = engine->RegisterObjectBehaviour("DrawingObjectPtr_VECTOR", 
        asBEHAVE_DESTRUCT,  "void f()",  
        asFUNCTION(DestructDrawingObjectPtr_VECTOR), 
        asCALL_CDECL_OBJLAST); 
        assert( r >= 0 );

	r = engine->RegisterObjectMethod("DrawingObjectPtr_VECTOR", 
        "DrawingObjectPtr_VECTOR &opAssign(const DrawingObjectPtr_VECTOR &in)", 
        asMETHODPR(DrawingObjectPtr_VECTOR, operator=, (const DrawingObjectPtr_VECTOR &),
        DrawingObjectPtr_VECTOR&), asCALL_THISCALL); assert( r >= 0 );

	r = engine->RegisterObjectMethod("DrawingObjectPtr_VECTOR", 
        "DrawingObject& at(uint)", 
        asFUNCTION(AtDrawingObjectPtr_VECTOR), 
        asCALL_CDECL_OBJLAST); 
        assert( r >= 0 );

	r = engine->RegisterObjectMethod("DrawingObjectPtr_VECTOR", 
        "DrawingObject &opIndex(uint)", 
        asFUNCTION(AtDrawingObjectPtr_VECTOR), 
        asCALL_CDECL_OBJLAST); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("DrawingObjectPtr_VECTOR", 
        "void clear()", 
        asMETHOD(DrawingObjectPtr_VECTOR, 
        clear), asCALL_THISCALL); 
        assert( r >= 0 );

        r = engine->RegisterObjectMethod("DrawingObjectPtr_VECTOR", 
        "void resize(uint)", 
        asMETHODPR(DrawingObjectPtr_VECTOR, 
        resize, (asUINT), void), asCALL_THISCALL); 
        assert( r >= 0 );

    r = engine->RegisterObjectMethod("DrawingObjectPtr_VECTOR", 
        "uint size()", 
        asMETHOD(DrawingObjectPtr_VECTOR, 
        size), asCALL_THISCALL); 
        assert( r >= 0 );

	r = engine->RegisterObjectMethod("DrawingObjectPtr_VECTOR", 
        "bool empty() const", 
        asMETHOD(DrawingObjectPtr_VECTOR, empty), 
        asCALL_THISCALL); 
        assert( r >= 0 );
//TODO: ����
/*
	r = engine->RegisterObjectMethod("DrawingObjectPtr_VECTOR", 
        "void pop_back(const DrawingObject&in)", 
        asMETHOD(DrawingObjectPtr_VECTOR, pop_back), 
        asCALL_THISCALL); 
        assert( r >= 0 );

	r = engine->RegisterObjectMethod("DrawingObjectPtr_VECTOR",
			"void push_back(const DrawingObject&in)",
			asMETHOD(DrawingObjectPtr_VECTOR, push_back),
			asCALL_THISCALL);
		assert(r >= 0);
		*/
	r = engine->RegisterObjectMethod("DrawingObjectPtr_VECTOR",
        "void insert(uint, const DrawingObject&in)", 
        asFUNCTION(InsertDrawingObjectPtr_VECTOR), 
        asCALL_CDECL_OBJLAST); 
        assert( r >= 0 );

	r = engine->RegisterObjectMethod("DrawingObjectPtr_VECTOR", 
        "void erase(uint)", 
        asFUNCTION(EraseDrawingObjectPtr_VECTOR), 
        asCALL_CDECL_OBJLAST); 
        assert( r >= 0 );

}

int RegisterTypeDrawingObjectPtr_VECTOR(asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc)
{
    int iRet = engine->RegisterObjectType("DrawingObjectPtr_VECTOR", 
               sizeof(DrawingObjectPtr_VECTOR), 
               asOBJ_VALUE | asOBJ_APP_CLASS_CD);
               assert( iRet >= 0 );

    mapIdFunc[iRet] = ConvDrawingObjectPtr_VECTOR;
    return iRet;
}


#endif

END_AS_NAMESPACE
