#ifndef SCRIPT_STL_H
#define SCRIPT_STL_H

#ifndef ANGELSCRIPT_H 
// Avoid having to inform include path if header is already include before
#include <angelscript.h>
#include "VAL_DATA.h"

#endif


BEGIN_AS_NAMESPACE



int     RegisterTypePOINT2D_VECTOR           (asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc);
void    RegisterPOINT2D_VECTOR               (asIScriptEngine *engine);

int     RegisterTypeDrawingObjectPtr_VECTOR    (asIScriptEngine *engine, ID_VALFUNC_MAP& mapIdFunc);
void    RegisterDrawingObjectPtr_VECTOR        (asIScriptEngine *engine);

END_AS_NAMESPACE

#endif
