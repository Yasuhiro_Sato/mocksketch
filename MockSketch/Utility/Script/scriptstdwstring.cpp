/**
 * @brief			SccriptStdWstring実装ファイル
 * @file		    SccriptStdWstring.cpp
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include <assert.h>
#include <sstream>
#include "scriptstdwstring.h"
#include "Utility/CUtility.h"

#include <string.h> // strstr
#include <stdio.h>	// sprintf()
#include <stdlib.h> // strtod()
#include <locale.h> // setlocale()
#include <map>      // std::map


/*---------------------------------------------------*/
/*  class                                            */
/*---------------------------------------------------*/

/*---------------------------------------------------*/
/*  namespace                                        */
/*---------------------------------------------------*/
using namespace std;

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
//---------------------------
// Compilation settings
//

// The use of the string pool can improve performance quite drastically
// for scripts that work with a lot of literal string constants. 
//
//  1 = on
//  0 = off

#ifndef AS_USE_STRINGPOOL
#define AS_USE_STRINGPOOL 1
#endif

// Sometimes it may be desired to use the same method names as used by C++ STL.
// This may for example reduce time when converting code from script to C++ or
// back.
//
//  0 = off
//  1 = on

#ifndef AS_USE_STLNAMES
#define AS_USE_STLNAMES 0
#endif


BEGIN_AS_NAMESPACE
#ifdef _DEBUG
#undef new 
//#define new DEBUG_NEW
#endif
using namespace std;



#if AS_USE_STRINGPOOL == 1

// By keeping the literal strings in a pool the application
// performance is improved as there are less string copies created.

// The string pool will be kept as user data in the engine. We'll
// need a specific type to identify the string pool user data.
// We just define a number here that we assume nobody else is using for 
// object type user data. The add-ons have reserved the numbers 1000 
// through 1999 for this purpose, so we should be fine.
const asPWORD STRING_POOL = 1001;

// This global static variable is placed here rather than locally within the
// StringFactory, due to memory leak detectors that don't see the deallocation
// of global variables. By placing the variable globally it will be initialized
// before the memory leak detector starts, thus it won't report the missing 
// deallocation. An example of this the Marmalade leak detector initialized with
// IwGxInit() and finished with IwGxTerminate().
static const wstring emptyString;

static const wstring &WstringFactory(asUINT length, const char *s)
{


	// Each engine instance has its own string pool
	asIScriptContext *ctx = asGetActiveContext();
	if( ctx == 0 )
	{
		// The string factory can only be called from a script
		assert( ctx );
		return emptyString;
	}


	asIScriptEngine *engine = ctx->GetEngine();

	// TODO: runtime optimize: Use unordered_map if C++11 is supported, i.e. MSVC10+, gcc 4.?+
	map<const char *, wstring> *pool = 
        reinterpret_cast< map<const char *, wstring>* >(engine->GetUserData(STRING_POOL));

	if( !pool )
	{
		// The string pool hasn't been created yet, so we'll create it now
		asAcquireExclusiveLock();

		// Make sure the string pool wasn't created while we were waiting for the lock
		pool = reinterpret_cast< map<const char *, wstring>* >(engine->GetUserData(STRING_POOL));
		if( !pool )
		{
			#if defined(__S3E__)
			pool = new map<const char *, string>;
			#else
			pool = new (nothrow) map<const char *, wstring>;
			#endif
			if( pool == 0 )
			{
				ctx->SetException("Out of memory");
				asReleaseExclusiveLock();
				return emptyString;
			}
			engine->SetUserData(pool, STRING_POOL);
		}

		asReleaseExclusiveLock();
	}

	// We can't let other threads modify the pool while we query it
	asAcquireSharedLock();

	// First check if a string object hasn't been created already
	map<const char *, wstring>::iterator it;
	it = pool->find(s);
	if( it != pool->end() )
	{
		asReleaseSharedLock();
		return it->second;
	}

	asReleaseSharedLock();

	// Acquire an exclusive lock so we can add the new string to the pool
	asAcquireExclusiveLock();

	// Make sure the string wasn't created while we were waiting for the exclusive lock
	it = pool->find(s);
	if( it == pool->end() )
	{
		// Create a new string object
        //wstring wstr = CharToString(s);

		it = pool->insert(map<const char *, wstring>::value_type(s, wstring(CUtil::CharToString(s)))).first;
	}

	asReleaseExclusiveLock();
	return it->second;
}

static void CleanupEngineWstringPool(asIScriptEngine *engine)
{
	map<const char *, wstring> *pool = reinterpret_cast< map<const char *, wstring>* >(engine->GetUserData(STRING_POOL));
	if( pool )
		delete pool;
}

#else
static wstring WstringFactory(asUINT length, const char *s)
{
    //UTF-8 -> UTF-16変換

    /*
    MultiByteToWideChar()
    wstring wstr(s,length / 2);

     MultiByteToWideChar(CP_ACP, 0, cstring, cstringlen, wstr, out_size);
     */


    wstring* pWstr = new wstring(CharToString(s));

    //string tmp(s, length);

	return *pWstr;
}
#endif

static void ConstructWstring(wstring *thisPointer)
{
	new(thisPointer) wstring();
}

static void CopyConstructWstring(const wstring &other, wstring *thisPointer)
{
	new(thisPointer) wstring(other);
}

static void DestructWstring(wstring *thisPointer)
{
#ifdef UNICODE
	thisPointer->~wstring();
#else
	thisPointer->~string();
#endif
}

static wstring &AddAssignWstringToWstring(const wstring &str, wstring &dest)
{
	// We don't register the method directly because some compilers 
	// and standard libraries inline the definition, resulting in the 
	// linker being unable to find the declaration.
	// Example: CLang/LLVM with XCode 4.3 on OSX 10.7
	dest += str;
	return dest;
}

// bool wstring::isEmpty()
// bool wstring::empty() // if AS_USE_STLNAMES == 1
static bool WstringIsEmpty(const wstring &str)
{
	// We don't register the method directly because some compilers 
	// and standard libraries inline the definition, resulting in the 
	// linker being unable to find the declaration
	// Example: CLang/LLVM with XCode 4.3 on OSX 10.7
	return str.empty();
}

static wstring &AssignUIntToWstring(unsigned int i, wstring &dest)
{
	wostringstream stream;
	stream << i;
	dest = stream.str();
	return dest;
}

static wstring &AddAssignUIntToWstring(unsigned int i, wstring &dest)
{
	wostringstream stream;
	stream << i;
	dest += stream.str();
	return dest;
}

static wstring AddSWsringUInt(const wstring &str, unsigned int i)
{
	wostringstream stream;
	stream << i;
	return str + stream.str();
}

static wstring AddIntWstring(int i, const wstring &str)
{
	wostringstream stream;
	stream << i;
	return stream.str() + str;
}

static wstring &AssignIntToWstring(int i, wstring &dest)
{
	wostringstream stream;
	stream << i;
	dest = stream.str();
	return dest;
}

static wstring &AddAssignIntToWstring(int i, wstring &dest)
{
	wostringstream stream;
	stream << i;
	dest += stream.str();
	return dest;
}

static wstring AddWstringInt(const wstring &str, int i)
{
	wostringstream stream;
	stream << i;
	return str + stream.str();
}

static wstring AddUIntWstring(unsigned int i, const wstring &str)
{
	wostringstream stream;
	stream << i;
	return stream.str() + str;
}

static wstring &AssignDoubleToWstring(double f, wstring &dest)
{
	wostringstream stream;
	stream << f;
	dest = stream.str();
	return dest;
}

static wstring &AddAssignDoubleToWstring(double f, wstring &dest)
{
	wostringstream stream;
	stream << f;
	dest += stream.str();
	return dest;
}

static wstring &AssignBoolToSWsring(bool b, wstring &dest)
{
	wostringstream stream;
	stream << (b ? "true" : "false");
	dest = stream.str();
	return dest;
}

static wstring &AddAssignBoolToSWsring(bool b, wstring &dest)
{
	wostringstream stream;
	stream << (b ? "true" : "false");
	dest += stream.str();
	return dest;
}

static wstring AddWstringDouble(const wstring &str, double f)
{
	wostringstream stream;
	stream << f;
	return str + stream.str();
}

static wstring AddDoubleWstring(double f, const wstring &str)
{
	wostringstream stream;
	stream << f;
	return stream.str() + str;
}

static wstring AddWstringBool(const wstring &str, bool b)
{
	wostringstream stream;
	stream << (b ? "true" : "false");
	return str + stream.str();
}

static wstring AddBoolWstring(bool b, const wstring &str)
{
	wostringstream stream;
	stream << (b ? "true" : "false");
	return stream.str() + str;
}

static wchar_t *WstringCharAt(unsigned int i, wstring &str)
{
	if( i >= str.size() )
	{
		// Set a script exception
		asIScriptContext *ctx = asGetActiveContext();
		ctx->SetException("Out of range");

		// Return a null pointer
		return 0;
	}

	return &str[i];
}

// AngelScript signature:
// int string::opCmp(const wstring &in) const
static int WstringCmp(const wstring &a, const wstring &b)
{
	int cmp = 0;
	if( a < b ) cmp = -1;
	else if( a > b ) cmp = 1;
	return cmp;
}

// This function returns the index of the first position where the substring
// exists in the input string. If the substring doesn't exist in the input
// string -1 is returned.
//
// AngelScript signature:
// int string::findFirst(const wstring &in sub, uint start = 0) const
static int WstringFindFirst(const wstring &sub, asUINT start, const wstring &str)
{
	// We don't register the method directly because the argument types change between 32bit and 64bit platforms
	return (int)str.find(sub, start);
}

// This function returns the index of the last position where the substring
// exists in the input string. If the substring doesn't exist in the input
// string -1 is returned.
//
// AngelScript signature:
// int string::findLast(const wstring &in sub, int start = -1) const
static int WstringFindLast(const wstring &sub, int start, const wstring &str)
{
	// We don't register the method directly because the argument types change between 32bit and 64bit platforms
	return (int)str.rfind(sub, (size_t)start);
}

// AngelScript signature:
// uint string::length() const
static asUINT WstringLength(const wstring &str)
{
	// We don't register the method directly because the return type changes between 32bit and 64bit platforms
	return (asUINT)str.length();
}


// AngelScript signature:
// void string::resize(uint l) 
static void WstringResize(asUINT l, wstring &str)
{
	// We don't register the method directly because the argument types change between 32bit and 64bit platforms
	str.resize(l);
}

// AngelScript signature:
// string WformatInt(int64 val, const wstring &in options, uint width)
static wstring WformatInt(asINT64 value, const wstring &options, asUINT width)
{
	bool leftJustify = options.find(_T("l")) != string::npos;
	bool padWithZero = options.find(_T("0")) != string::npos;
	bool alwaysSign  = options.find(_T("+")) != string::npos;
	bool spaceOnSign = options.find(_T(" ")) != string::npos;
	bool hexSmall    = options.find(_T("h")) != string::npos;
	bool hexLarge    = options.find(_T("H")) != string::npos;

	wstring fmt = _T("%");
	if( leftJustify ) fmt += _T("-");
	if( alwaysSign ) fmt += _T("+");
	if( spaceOnSign ) fmt += _T(" ");
	if( padWithZero ) fmt += _T("0");

#ifdef __GNUC__
#ifdef _LP64
	fmt += "*l";
#else
	fmt += "*ll";
#endif
#else
	fmt += _T("*I64");
#endif

	if( hexSmall ) fmt += _T("x");
	else if( hexLarge ) fmt += _T("X");
	else fmt += _T("d");

	wstring buf;
	buf.resize(width+20);
#if _MSC_VER >= 1400 && !defined(__S3E__)
	// MSVC 8.0 / 2005 or newer
	_stprintf_s(&buf[0], buf.size(), fmt.c_str(), width, value);
#else
	sprintf(&buf[0], fmt.c_str(), width, value);
#endif
	buf.resize(_tcslen(&buf[0]));
	
	return buf;
}

// AngelScript signature:
// string WformatFloat(double val, const wstring &in options, uint width, uint precision)
static wstring WformatFloat(double value, const wstring &options, asUINT width, asUINT precision)
{
	bool leftJustify = options.find(_T("l")) != string::npos;
	bool padWithZero = options.find(_T("0")) != string::npos;
	bool alwaysSign  = options.find(_T("+")) != string::npos;
	bool spaceOnSign = options.find(_T(" ")) != string::npos;
	bool expSmall    = options.find(_T("e")) != string::npos;
	bool expLarge    = options.find(_T("E")) != string::npos;

	wstring fmt = _T("%");
	if( leftJustify ) fmt += _T("-");
	if( alwaysSign ) fmt += _T("+");
	if( spaceOnSign ) fmt += _T(" ");
	if( padWithZero ) fmt += _T("0");

	fmt += _T("*.*");

	if( expSmall ) fmt += _T("e");
	else if( expLarge ) fmt += _T("E");
	else fmt += _T("f");

	wstring buf;
	buf.resize(width+precision+50);
#if _MSC_VER >= 1400 && !defined(__S3E__)
	// MSVC 8.0 / 2005 or newer
	_stprintf_s(&buf[0], buf.size(), fmt.c_str(), width, precision, value);
#else
	sprintf(&buf[0], fmt.c_str(), width, precision, value);
#endif
	buf.resize(_tcslen(&buf[0]));
	
	return buf;
}

// AngelScript signature:
// int64 WparseInt(const wstring &in val, uint base = 10, uint &out byteCount = 0)
static asINT64 WparseInt(const wstring &val, asUINT base, asUINT *byteCount)
{
	// Only accept base 10 and 16
	if( base != 10 && base != 16 )
	{
		if( byteCount ) *byteCount = 0;
		return 0;
	}

	const wchar_t *end = &val[0];

	// Determine the sign
	bool sign = false;
	if( *end == _T('-') )
	{
		sign = true;
		*end++;
	}
	else if( *end == _T('+') )
		*end++;

	asINT64 res = 0;
	if( base == 10 )
	{
		while( *end >= _T('0') && *end <= _T('9') )
		{
			res *= 10;
			res += *end++ - _T('0');
		}
	}
	else if( base == 16 )
	{
		while( (*end >= _T('0') && *end <= _T('9') ) ||
		       (*end >= _T('a') && *end <= _T('f') ) ||
		       (*end >= _T('A') && *end <= _T('F') ) )
		{
			res *= 16;
			if( *end >= _T('0') && *end <= _T('9') )
				res += *end++ - _T('0');
			else if( *end >= _T('a') && *end <= _T('f') )
				res += *end++ - _T('a') + 10;
			else if( *end >= _T('A') && *end <= _T('F') )
				res += *end++ - _T('A') + 10;
		}
	}

	if( byteCount )
		*byteCount = asUINT(size_t(end - val.c_str()));

	if( sign )
		res = -res;

	return res;
}

// AngelScript signature:
// double WparseFloat(const wstring &in val, uint &out byteCount = 0)
double WparseFloat(const wstring &val, asUINT *byteCount)
{
	wchar_t *end;

    // WinCE doesn't have setlocale. Some quick testing on my current platform
    // still manages to parse the numbers such as "3.14" even if the decimal for the
    // locale is ",".
#if !defined(_WIN32_WCE) && !defined(ANDROID)
	// Set the locale to C so that we are guaranteed to parse the float value correctly
	char *orig = setlocale(LC_NUMERIC, 0);
	setlocale(LC_NUMERIC, "C");
#endif

	double res = _tcstod(val.c_str(), &end);

#if !defined(_WIN32_WCE) && !defined(ANDROID)
	// Restore the locale
	setlocale(LC_NUMERIC, orig);
#endif

	if( byteCount )
		*byteCount = asUINT(size_t(end - val.c_str()));

	return res;
}

// This function returns a string containing the substring of the input string
// determined by the starting index and count of characters.
//
// AngelScript signature:
// string string::substr(uint start = 0, int count = -1) const
static wstring WstringSubString(asUINT start, int count, const wstring &str)
{
	// Check for out-of-bounds
	wstring ret;
	if( start < str.length() && count != 0 )
		ret = str.substr(start, count);

	return ret;
}

//-------------------------------
//
//-------------------------------
// a = b  //
/*
static wstring &AssignString(const std::string &s, wstring &dest)
{
    dest = CUtil::CharToString(s.c_str());
	return dest;
}

static wstring &AddAssignString(const std::string &s, wstring &dest)
{
    dest += CUtil::CharToString(s.c_str());
	return dest;
}

static bool EqualString(const wstring &a, const std::string &b)
{
    return (CUtil::CharToString(b.c_str()) == a);
}

static int StringCmp(const wstring &a, const std::string &b)
{
	int cmp = 0;
    wstring s;
    s = CUtil::CharToString(b.c_str());
	if( a < s ) cmp = -1;
	else if( a > s ) cmp = 1;
	return cmp;
}
*/


void AsFormat0(const std::wstring &str)
{
    CUtil::SPrintfWString(str);
}

void AsFormat1(std::wstring &strClass, const std::wstring &fmt, void *ref, int refTypeId)
{
    asIScriptContext *ctx = asGetActiveContext();
    asIScriptEngine *engine = ctx->GetEngine();

    UniVal64 Val[1] = {0};
    CUtil::ConvUni(engine, &Val[0], ref, refTypeId);
    CUtil::FormatV(strClass, fmt, (char*)Val);
}

void AsFormat2(std::wstring &strClass, const std::wstring &fmt, 
                 void *ref0, int refTypeId0,
                 void *ref1, int refTypeId1)
{
    asIScriptContext *ctx = asGetActiveContext();
    asIScriptEngine *engine = ctx->GetEngine();

    UniVal64 Val[2] = {0};
    CUtil::ConvUni(engine, &Val[0], ref0, refTypeId0);
    CUtil::ConvUni(engine, &Val[1], ref1, refTypeId1);
    CUtil::FormatV(strClass, fmt, (char*)Val);
}


void AsFormat3(std::wstring &strClass, const std::wstring &fmt, 
                 void *ref0, int refTypeId0,
                 void *ref1, int refTypeId1,
                 void *ref2, int refTypeId2
                 )
{
    asIScriptContext *ctx = asGetActiveContext();
    asIScriptEngine *engine = ctx->GetEngine();

    UniVal64 Val[3] = {0};
    CUtil::ConvUni(engine, &Val[0], ref0, refTypeId0);
    CUtil::ConvUni(engine, &Val[1], ref1, refTypeId1);
    CUtil::ConvUni(engine, &Val[2], ref2, refTypeId2);
    CUtil::FormatV(strClass, fmt, (char*)Val);
}


void AsFormat4(std::wstring &strClass, const std::wstring &fmt, 
                 void *ref0, int refTypeId0,
                 void *ref1, int refTypeId1,
                 void *ref2, int refTypeId2,
                 void *ref3, int refTypeId3
                 )
{
    asIScriptContext *ctx = asGetActiveContext();
    asIScriptEngine *engine = ctx->GetEngine();

    UniVal64 Val[3] = {0};
    CUtil::ConvUni(engine, &Val[0], ref0, refTypeId0);
    CUtil::ConvUni(engine, &Val[1], ref1, refTypeId1);
    CUtil::ConvUni(engine, &Val[2], ref2, refTypeId2);
    CUtil::ConvUni(engine, &Val[2], ref2, refTypeId2);
    CUtil::FormatV(strClass, fmt, (char*)Val);


}


void AsFormat5(std::wstring &strClass, const std::wstring &fmt, 
                 void *ref0, int refTypeId0,
                 void *ref1, int refTypeId1,
                 void *ref2, int refTypeId2,
                 void *ref3, int refTypeId3,
                 void *ref4, int refTypeId4
                 )
{
    asIScriptContext *ctx = asGetActiveContext();
    asIScriptEngine *engine = ctx->GetEngine();

    UniVal64 Val[5] = {0};
    CUtil::ConvUni(engine, &Val[0], ref0, refTypeId0);
    CUtil::ConvUni(engine, &Val[1], ref1, refTypeId1);
    CUtil::ConvUni(engine, &Val[2], ref2, refTypeId2);
    CUtil::ConvUni(engine, &Val[3], ref3, refTypeId3);
    CUtil::ConvUni(engine, &Val[4], ref4, refTypeId4);
    CUtil::FormatV(strClass, fmt, (char*)Val);
}


void AsFormat6(std::wstring &strClass, const std::wstring &fmt, 
                 void *ref0, int refTypeId0,
                 void *ref1, int refTypeId1,
                 void *ref2, int refTypeId2,
                 void *ref3, int refTypeId3,
                 void *ref4, int refTypeId4,
                 void *ref5, int refTypeId5
                 )
{
    asIScriptContext *ctx = asGetActiveContext();
    asIScriptEngine *engine = ctx->GetEngine();

    UniVal64 Val[6] = {0};
    CUtil::ConvUni(engine, &Val[0], ref0, refTypeId0);
    CUtil::ConvUni(engine, &Val[1], ref1, refTypeId1);
    CUtil::ConvUni(engine, &Val[2], ref2, refTypeId2);
    CUtil::ConvUni(engine, &Val[3], ref3, refTypeId3);
    CUtil::ConvUni(engine, &Val[4], ref4, refTypeId4);
    CUtil::ConvUni(engine, &Val[5], ref5, refTypeId5);
    CUtil::FormatV(strClass, fmt, (char*)Val);
}

void AsFormat7(std::wstring &strClass, const std::wstring &fmt, 
                 void *ref0, int refTypeId0,
                 void *ref1, int refTypeId1,
                 void *ref2, int refTypeId2,
                 void *ref3, int refTypeId3,
                 void *ref4, int refTypeId4,
                 void *ref5, int refTypeId5,
                 void *ref6, int refTypeId6
                 )
{
    asIScriptContext *ctx = asGetActiveContext();
    asIScriptEngine *engine = ctx->GetEngine();

    UniVal64 Val[7] = {0};
    CUtil::ConvUni(engine, &Val[0], ref0, refTypeId0);
    CUtil::ConvUni(engine, &Val[1], ref1, refTypeId1);
    CUtil::ConvUni(engine, &Val[2], ref2, refTypeId2);
    CUtil::ConvUni(engine, &Val[3], ref3, refTypeId3);
    CUtil::ConvUni(engine, &Val[4], ref4, refTypeId4);
    CUtil::ConvUni(engine, &Val[5], ref5, refTypeId5);
    CUtil::ConvUni(engine, &Val[6], ref6, refTypeId6);
    CUtil::FormatV(strClass, fmt, (char*)Val);


}

void AsFormat8(std::wstring &strClass, const std::wstring &fmt, 
                 void *ref0, int refTypeId0,
                 void *ref1, int refTypeId1,
                 void *ref2, int refTypeId2,
                 void *ref3, int refTypeId3,
                 void *ref4, int refTypeId4,
                 void *ref5, int refTypeId5,
                 void *ref6, int refTypeId6,
                 void *ref7, int refTypeId7
                 )
{
    asIScriptContext *ctx = asGetActiveContext();
    asIScriptEngine *engine = ctx->GetEngine();

    UniVal64 Val[8] = {0};
    CUtil::ConvUni(engine, &Val[0], ref0, refTypeId0);
    CUtil::ConvUni(engine, &Val[1], ref1, refTypeId1);
    CUtil::ConvUni(engine, &Val[2], ref2, refTypeId2);
    CUtil::ConvUni(engine, &Val[3], ref3, refTypeId3);
    CUtil::ConvUni(engine, &Val[4], ref4, refTypeId4);
    CUtil::ConvUni(engine, &Val[5], ref5, refTypeId5);
    CUtil::ConvUni(engine, &Val[6], ref6, refTypeId6);
    CUtil::ConvUni(engine, &Val[7], ref7, refTypeId7);
    CUtil::FormatV(strClass, fmt, (char*)Val);


}

void AsFormat9(std::wstring &strClass, const std::wstring &fmt, 
                 void *ref0, int refTypeId0,
                 void *ref1, int refTypeId1,
                 void *ref2, int refTypeId2,
                 void *ref3, int refTypeId3,
                 void *ref4, int refTypeId4,
                 void *ref5, int refTypeId5,
                 void *ref6, int refTypeId6,
                 void *ref7, int refTypeId7,
                 void *ref8, int refTypeId8
                 )
{
    asIScriptContext *ctx = asGetActiveContext();
    asIScriptEngine *engine = ctx->GetEngine();

    UniVal64 Val[9] = {0};
    CUtil::ConvUni(engine, &Val[0], ref0, refTypeId0);
    CUtil::ConvUni(engine, &Val[1], ref1, refTypeId1);
    CUtil::ConvUni(engine, &Val[2], ref2, refTypeId2);
    CUtil::ConvUni(engine, &Val[3], ref3, refTypeId3);
    CUtil::ConvUni(engine, &Val[4], ref4, refTypeId4);
    CUtil::ConvUni(engine, &Val[5], ref5, refTypeId5);
    CUtil::ConvUni(engine, &Val[6], ref6, refTypeId6);
    CUtil::ConvUni(engine, &Val[7], ref7, refTypeId7);
    CUtil::ConvUni(engine, &Val[8], ref8, refTypeId8);
    CUtil::FormatV(strClass, fmt, (char*)Val);
}

// String equality comparison.
// Returns true iff lhs is equal to rhs.
//
// For some reason gcc 4.7 has difficulties resolving the 
// asFUNCTIONPR(operator==, (const wstring &, const wstring &) 
// makro, so this wrapper was introduced as work around.
static bool WstringEquals(const wstring& lhs, const wstring& rhs)
{
    return lhs == rhs;
}

int RegisterStdWstring_Native(asIScriptEngine *engine)
{
	int r;
    int iRet;

	// Register the string type
	iRet = engine->RegisterObjectType("string", sizeof(wstring), asOBJ_VALUE | asOBJ_APP_CLASS_CDAK); assert( iRet >= 0 );

#if AS_USE_STRINGPOOL == 1
	// Register the string factory
	r = engine->RegisterStringFactory("const string &", asFUNCTION(WstringFactory), asCALL_CDECL); assert( r >= 0 );

	// Register the cleanup callback for the string pool
	engine->SetEngineUserDataCleanupCallback(CleanupEngineWstringPool, STRING_POOL);
#else
	// Register the string factory
	r = engine->RegisterStringFactory("string", asFUNCTION(WstringFactory), asCALL_CDECL); assert( r >= 0 );
#endif

	// Register the object operator overloads
	r = engine->RegisterObjectBehaviour("string", asBEHAVE_CONSTRUCT,  "void f()",                    asFUNCTION(ConstructWstring), asCALL_CDECL_OBJLAST); assert( r >= 0 );
	r = engine->RegisterObjectBehaviour("string", asBEHAVE_CONSTRUCT,  "void f(const string &in)",    asFUNCTION(CopyConstructWstring), asCALL_CDECL_OBJLAST); assert( r >= 0 );
	r = engine->RegisterObjectBehaviour("string", asBEHAVE_DESTRUCT,   "void f()",                    asFUNCTION(DestructWstring),  asCALL_CDECL_OBJLAST); assert( r >= 0 );
	r = engine->RegisterObjectMethod("string", "string &opAssign(const string &in)", asMETHODPR(wstring, operator =, (const wstring&), wstring&), asCALL_THISCALL); assert( r >= 0 );
    //r = engine->RegisterObjectMethod("string", "wstring &opAssign(const string &in)", asFUNCTION(AssignString), asCALL_CDECL_OBJLAST); assert( r >= 0 );


    // Need to use a wrapper on Mac OS X 10.7/XCode 4.3 and CLang/LLVM, otherwise the linker fails
	//r = engine->RegisterObjectMethod("string", "string &opAddAssign(const string &in)", asFUNCTION(AddAssignWstringToWstring), asCALL_CDECL_OBJLAST); assert( r >= 0 );
	//r = engine->RegisterObjectMethod("string", "string &opAddAssign(const string &in)",asFUNCTION(AddAssignString), asCALL_CDECL_OBJLAST); assert( r >= 0 );
	r = engine->RegisterObjectMethod("string", "string &opAddAssign(const string &in)", asMETHODPR(wstring, operator+=, (const wstring&), wstring&), asCALL_THISCALL); assert( r >= 0 );

	// Need to use a wrapper for operator== otherwise gcc 4.7 fails to compile
	r = engine->RegisterObjectMethod("string", "bool opEquals(const string &in) const", asFUNCTIONPR(WstringEquals, (const wstring &, const wstring &), bool), asCALL_CDECL_OBJFIRST); assert( r >= 0 );
    //r = engine->RegisterObjectMethod("string", "bool opEquals(const string &in) const", asFUNCTION(EqualString), asCALL_CDECL_OBJFIRST); assert( r >= 0 );
	r = engine->RegisterObjectMethod("string", "int opCmp(const string &in) const", asFUNCTION(WstringCmp), asCALL_CDECL_OBJFIRST); assert( r >= 0 );
    //r = engine->RegisterObjectMethod("string", "int opCmp(const string &in) const", asFUNCTION(StringCmp), asCALL_CDECL_OBJFIRST); assert( r >= 0 );
	r = engine->RegisterObjectMethod("string", "string opAdd(const string &in) const", asFUNCTIONPR(operator +, (const wstring &, const wstring &), wstring), asCALL_CDECL_OBJFIRST); assert( r >= 0 );

	// The wstring length can be accessed through methods or through virtual property
	r = engine->RegisterObjectMethod("string", "uint length() const", asFUNCTION(WstringLength), asCALL_CDECL_OBJLAST); assert( r >= 0 );
	r = engine->RegisterObjectMethod("string", "void resize(uint)", asFUNCTION(WstringResize), asCALL_CDECL_OBJLAST); assert( r >= 0 );
	r = engine->RegisterObjectMethod("string", "uint get_length() const", asFUNCTION(WstringLength), asCALL_CDECL_OBJLAST); assert( r >= 0 );
	r = engine->RegisterObjectMethod("string", "void set_length(uint)", asFUNCTION(WstringResize), asCALL_CDECL_OBJLAST); assert( r >= 0 );
	// Need to use a wrapper on Mac OS X 10.7/XCode 4.3 and CLang/LLVM, otherwise the linker fails
//	r = engine->RegisterObjectMethod("string", "bool isEmpty() const", asMETHOD(wstring, empty), asCALL_THISCALL); assert( r >= 0 );
	r = engine->RegisterObjectMethod("string", "bool isEmpty() const", asFUNCTION(WstringIsEmpty), asCALL_CDECL_OBJLAST); assert( r >= 0 );

	// Register the index operator, both as a mutator and as an inspector
	// Note that we don't register the operator[] directly, as it doesn't do bounds checking
	r = engine->RegisterObjectMethod("string", "uint8 &opIndex(uint)", asFUNCTION(WstringCharAt), asCALL_CDECL_OBJLAST); assert( r >= 0 );
	r = engine->RegisterObjectMethod("string", "const uint8 &opIndex(uint) const", asFUNCTION(WstringCharAt), asCALL_CDECL_OBJLAST); assert( r >= 0 );

	// Automatic conversion from values
	r = engine->RegisterObjectMethod("string", "string &opAssign(double)", asFUNCTION(AssignDoubleToWstring), asCALL_CDECL_OBJLAST); assert( r >= 0 );
	r = engine->RegisterObjectMethod("string", "string &opAddAssign(double)", asFUNCTION(AddAssignDoubleToWstring), asCALL_CDECL_OBJLAST); assert( r >= 0 );
	r = engine->RegisterObjectMethod("string", "string opAdd(double) const", asFUNCTION(AddWstringDouble), asCALL_CDECL_OBJFIRST); assert( r >= 0 );
	r = engine->RegisterObjectMethod("string", "string opAdd_r(double) const", asFUNCTION(AddDoubleWstring), asCALL_CDECL_OBJLAST); assert( r >= 0 );

	r = engine->RegisterObjectMethod("string", "string &opAssign(int)", asFUNCTION(AssignIntToWstring), asCALL_CDECL_OBJLAST); assert( r >= 0 );
	r = engine->RegisterObjectMethod("string", "string &opAddAssign(int)", asFUNCTION(AddAssignIntToWstring), asCALL_CDECL_OBJLAST); assert( r >= 0 );
	r = engine->RegisterObjectMethod("string", "string opAdd(int) const", asFUNCTION(AddWstringInt), asCALL_CDECL_OBJFIRST); assert( r >= 0 );
	r = engine->RegisterObjectMethod("string", "string opAdd_r(int) const", asFUNCTION(AddIntWstring), asCALL_CDECL_OBJLAST); assert( r >= 0 );

	r = engine->RegisterObjectMethod("string", "string &opAssign(uint)", asFUNCTION(AssignUIntToWstring), asCALL_CDECL_OBJLAST); assert( r >= 0 );
	r = engine->RegisterObjectMethod("string", "string &opAddAssign(uint)", asFUNCTION(AddAssignUIntToWstring), asCALL_CDECL_OBJLAST); assert( r >= 0 );
	r = engine->RegisterObjectMethod("string", "string opAdd(uint) const", asFUNCTION(AddSWsringUInt), asCALL_CDECL_OBJFIRST); assert( r >= 0 );
	r = engine->RegisterObjectMethod("string", "string opAdd_r(uint) const", asFUNCTION(AddUIntWstring), asCALL_CDECL_OBJLAST); assert( r >= 0 );

	r = engine->RegisterObjectMethod("string", "string &opAssign(bool)", asFUNCTION(AssignBoolToSWsring), asCALL_CDECL_OBJLAST); assert( r >= 0 );
	r = engine->RegisterObjectMethod("string", "string &opAddAssign(bool)", asFUNCTION(AddAssignBoolToSWsring), asCALL_CDECL_OBJLAST); assert( r >= 0 );
	r = engine->RegisterObjectMethod("string", "string opAdd(bool) const", asFUNCTION(AddWstringBool), asCALL_CDECL_OBJFIRST); assert( r >= 0 );
	r = engine->RegisterObjectMethod("string", "string opAdd_r(bool) const", asFUNCTION(AddBoolWstring), asCALL_CDECL_OBJLAST); assert( r >= 0 );
    
	// Utilities
    /*
	r = engine->RegisterObjectMethod("string", "wstring substr(uint start = 0, int count = -1) const", asFUNCTION(WstringSubString), asCALL_CDECL_OBJLAST); assert( r >= 0 );
	r = engine->RegisterObjectMethod("string", "int findFirst(const wstring &in, uint start = 0) const", asFUNCTION(WstringFindFirst), asCALL_CDECL_OBJLAST); assert( r >= 0 );
	r = engine->RegisterObjectMethod("string", "int findLast(const wstring &in, int start = -1) const", asFUNCTION(WstringFindLast), asCALL_CDECL_OBJLAST); assert( r >= 0 );

	r = engine->RegisterGlobalFunction("wstring WformatInt(int64 val, const wstring &in options, uint width = 0)", asFUNCTION(WformatInt), asCALL_CDECL); assert(r >= 0);
	r = engine->RegisterGlobalFunction("wstring WformatFloat(double val, const wstring &in options, uint width = 0, uint precision = 0)", asFUNCTION(WformatFloat), asCALL_CDECL); assert(r >= 0);
	r = engine->RegisterGlobalFunction("int64 WparseInt(const wstring &in, uint base = 10, uint &out byteCount = 0)", asFUNCTION(WparseInt), asCALL_CDECL); assert(r >= 0);
	r = engine->RegisterGlobalFunction("double WparseFloat(const wstring &in, uint &out byteCount = 0)", asFUNCTION(WparseFloat), asCALL_CDECL); assert(r >= 0);
    */

    r = engine->RegisterObjectMethod("string","void Format(const string &in)", 
                                            asFUNCTION(AsFormat0), asCALL_CDECL_OBJFIRST); assert( r >= 0 );
        
    r = engine->RegisterObjectMethod("string","void Format(const string &in, ?& in)", 
                                            asFUNCTION(AsFormat1), asCALL_CDECL_OBJFIRST); assert( r >= 0 );

    r = engine->RegisterObjectMethod("string","void Format(const string &in, ?& in, ?& in)", 
                                            asFUNCTION(AsFormat2), asCALL_CDECL_OBJFIRST); assert( r >= 0 );

    r = engine->RegisterObjectMethod("string","void Format(const string &in, ?& in, ?& in, ?& in)", 
                                            asFUNCTION(AsFormat3), asCALL_CDECL_OBJFIRST); assert( r >= 0 );

    r = engine->RegisterObjectMethod("string","void Format(const string &in, ?& in, ?& in, ?& in, ?& in)", 
                                            asFUNCTION(AsFormat4), asCALL_CDECL_OBJFIRST); assert( r >= 0 );

    r = engine->RegisterObjectMethod("string","void Format(const string &in, ?& in, ?& in, ?& in, ?& in, ?& in)", 
                                            asFUNCTION(AsFormat5), asCALL_CDECL_OBJFIRST); assert( r >= 0 );

    r = engine->RegisterObjectMethod("string","void Format(const string &in, ?& in, ?& in, ?& in, ?& in, ?& in, ?& in)", 
                                            asFUNCTION(AsFormat6), asCALL_CDECL_OBJFIRST); assert( r >= 0 );

    r = engine->RegisterObjectMethod("string","void Format(const string &in, ?& in, ?& in, ?& in, ?& in, ?& in, ?& in, ?& in)", 
                                            asFUNCTION(AsFormat7), asCALL_CDECL_OBJFIRST); assert( r >= 0 );

    r = engine->RegisterObjectMethod("string","void Format(const string &in, ?& in, ?& in, ?& in, ?& in, ?& in, ?& in, ?& in, ?& in)", 
                                            asFUNCTION(AsFormat8), asCALL_CDECL_OBJFIRST); assert( r >= 0 );

    r = engine->RegisterObjectMethod("string","void Format(const string &in, ?& in, ?& in, ?& in, ?& in, ?& in, ?& in, ?& in, ?& in, ?& in)", 
                                            asFUNCTION(AsFormat9), asCALL_CDECL_OBJFIRST); assert( r >= 0 );


#if AS_USE_STLNAMES == 1
	// Same as length
	r = engine->RegisterObjectMethod("wstring", "uint size() const", asFUNCTION(WstringLength), asCALL_CDECL_OBJLAST); assert( r >= 0 );
	// Same as isEmpty
	r = engine->RegisterObjectMethod("wstring", "bool empty() const", asFUNCTION(WstringIsEmpty), asCALL_CDECL_OBJLAST); assert( r >= 0 );
	// Same as findFirst
	r = engine->RegisterObjectMethod("wstring", "int find(const string &in, uint start = 0) const", asFUNCTION(WstringFindFirst), asCALL_CDECL_OBJLAST); assert( r >= 0 );
	// Same as findLast
	r = engine->RegisterObjectMethod("wstring", "int rfind(const string &in, int start = -1) const", asFUNCTION(WstringFindLast), asCALL_CDECL_OBJLAST); assert( r >= 0 );
#endif

	// TODO: Implement the following
	// findFirstOf
	// findLastOf
	// findFirstNotOf
	// findLastNotOf
	// findAndReplace - replaces a text found in the wstring
	// replaceRange - replaces a range of bytes in the wstring
	// trim/trimLeft/trimRight
	// multiply/times/opMul/opMul_r - takes the wstring and multiplies it n times, e.g. "-".multiply(5) returns "-----"

    return iRet;
}


bool CALLBACK ConvWstring(VAL_DATA* pValData, const void* pValAddress, void* pContext)
{
    pValData->strTypeName = _T("string");
    const StdString* pStr = reinterpret_cast<const StdString*>(pValAddress);
    pValData->strVal = CUtil::StrFormat(_T("%s"), pStr->c_str());
    return true;
}


int RegisterStdWstring(asIScriptEngine * engine, ID_VALFUNC_MAP& mapIdFunc)
{
    int iRet;
    iRet = RegisterStdWstring_Native(engine);
    //int iTypeId = engine->GetTypeIdByDecl("StdString");
    mapIdFunc[iRet] = ConvWstring;
	return iRet;
}


END_AS_NAMESPACE




