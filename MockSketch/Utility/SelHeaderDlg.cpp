/**
 * @brief			SelHeaderDlg実装ファイル
 * @file			SelHeaderDlg.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        リストコントロール用ヘッダ設定ダイアログ
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MockSketch.h"
#include "./SelHeaderDlg.h"


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
BEGIN_MESSAGE_MAP(CSelHeaderDlg, CDialog)
END_MESSAGE_MAP()

IMPLEMENT_DYNAMIC(CSelHeaderDlg, CDialog)

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
CSelHeaderDlg::CSelHeaderDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSelHeaderDlg::IDD, pParent)
{

}

/**
 *  @brief  デストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
CSelHeaderDlg::~CSelHeaderDlg()
{
}

/**
 *  @brief  フレームワークの自動的なデータ交換
 *  @param  pDX     CDataExchange オブジェクトへのポインタ
 *  @retval なし     
 *  @note   UpdateData メンバ関数から呼び出されます。
 */
void CSelHeaderDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_LB_HEADER, m_lbHeaderItem);
}

/**
 *  @brief  ヘッダ項目名登録
 *  @param  [in] lstHeader ヘッダ項目
 *  @retval なし
 *  @note   
 */
void CSelHeaderDlg::SetHeaderNames(const std::vector< 
                    boost::tuple<StdString, bool> >& lstHeader)
{
    m_lstHeader = lstHeader;
}

/**
 *  @brief  ヘッダ項目名取得
 *  @param  [out] pLstHeader ヘッダ項目
 *  @retval なし
 *  @note   
 */
void CSelHeaderDlg::GetHeaderNames(std::vector< 
                    boost::tuple<StdString, bool> >* pLstHeader)
{

}

/**
 *  @brief  ウインドウの初期化処理
 *  @param  なし
 *  @retval 常にTRUE     
 *  @note   
 */
BOOL CSelHeaderDlg::OnInitDialog()
{
    CDialog::OnInitDialog();

    StdString strItem;
    bool      bEnable;

    int iIndex;
    foreach(boost::tie(strItem, bEnable), m_lstHeader)
    {
        iIndex = m_lbHeaderItem.InsertString(-1, strItem.c_str());
        m_lbHeaderItem.Enable(iIndex, bEnable);
    }

    return TRUE; 
}
