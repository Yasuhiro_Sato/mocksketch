/**
 * @brief			CSelHeaderDlgヘッダーファイル
 * @file		    SelHeaderDlg.h
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

#pragma once

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "../resource.h"
#include "../resource2.h"
#include "afxwin.h"



/**
 * @class   CSelHeaderDlg
 * @brief   
 */
class CSelHeaderDlg : public CDialog
{
	DECLARE_DYNAMIC(CSelHeaderDlg)

    //ヘッダ項目
    std::vector< boost::tuple<StdString, bool> > m_lstHeader;

    CCheckListBox m_lbHeaderItem;
public:

    //!< コンストラクター
	CSelHeaderDlg(CWnd* pParent = NULL);   // 標準コンストラクタ

    //!< デストラクター
    virtual ~CSelHeaderDlg();

    // ダイアログ データ
    enum { IDD = IDD_DLG_SEL_HEADER };

public:

    //!< ヘッダ項目名登録
    void SetHeaderNames(const std::vector< boost::tuple<StdString, bool> >& lstHeader);

    //!< ヘッダ項目名取得
    void GetHeaderNames(std::vector< boost::tuple<StdString, bool> >* pLstHeader);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート

	DECLARE_MESSAGE_MAP()
public:

    //!< ウインドウの初期化処理 
    virtual BOOL OnInitDialog();
};
