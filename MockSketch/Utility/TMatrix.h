#ifndef __TMATRIX_H__
#define __TMATRIX_H__

#include <exception>
#include <string>
#include <vector>
#ifdef _WIN32
#include  <windows.h>
#endif


//マトリックステンプレートライブラリ

enum E_MAT_ERROR
{
    MAT_ALLOC,
    MAT_RANGE,
    MAT_SIZE,
    MAT_ZERO_DIV,
    MAT_DET_ZERO,
    MAT_NO_ERRROR,
};


class NUM_MATRIX_ERROR :public std::exception
{
public:

    //* コンストラクター 
    NUM_MATRIX_ERROR(void) : exception()
    {
        m_eError = MAT_NO_ERRROR;
    }

    NUM_MATRIX_ERROR(E_MAT_ERROR eError) : exception()
    {
        m_eError = eError;
        switch (eError)
        {
        case MAT_ALLOC:{m_ErrMessage = "Allocation Error"; break; }
        case MAT_RANGE:{m_ErrMessage = "Range Error"; break; }
        case MAT_SIZE:{m_ErrMessage = "Size Not Equal"; break; }
        case MAT_ZERO_DIV:{m_ErrMessage = "Zero Division"; break; }
        case MAT_DET_ZERO:{m_ErrMessage = "Determinant Zero"; break; }
        default:           {m_ErrMessage = "Unknown Error"; break; }
        }
    }

    E_MAT_ERROR  GetErr(){ return m_eError; }

    ~NUM_MATRIX_ERROR(void){}

protected:
    std::string m_ErrMessage;
    E_MAT_ERROR m_eError;

};

template <class D>
class NUM_MATRIX
{
public:
    //* コンストラクター
    NUM_MATRIX();

    //* コンストラクター
    NUM_MATRIX(size_t nRows, size_t nCols);

    //* 初期化付きコンストラクター
    NUM_MATRIX(size_t nRows, size_t nCols, D def);

    //* コピーコンストラクター
    NUM_MATRIX(const NUM_MATRIX<D> &m);

    //* デストラクター
    ~NUM_MATRIX();

    //* 行数取得
    size_t GetRows() const;

    //* 列数取得
    size_t GetCols() const;

    //* 比較演算子
    bool IsEql(const NUM_MATRIX<D> & mat) const;
    bool operator == (const NUM_MATRIX<D> & mat) const;


    //* 代入演算子
    void Eql(const D & m);
    NUM_MATRIX<D>& operator = (const D & m);

    //* 代入演算子
    void Eql(const NUM_MATRIX<D> & mat);
    NUM_MATRIX<D>& operator =   (const NUM_MATRIX<D> & mat);

    //NUM_MATRIX<D>& operator = (const NUM_MATRIX<D> & mat);

    //* 加法演算子
    NUM_MATRIX<D> Add(const D & m) const;
    NUM_MATRIX<D> operator + (const D & m) const;

    //* 加法演算子
    NUM_MATRIX<D> Add(const NUM_MATRIX<D> & mat) const;
    NUM_MATRIX<D> operator + (const NUM_MATRIX<D> & mat) const;

    //* 減法演算子
    NUM_MATRIX<D> Sub(const D & m) const;
    NUM_MATRIX<D> operator - (const D & m) const;

    //* 減法演算子
    NUM_MATRIX<D> Sub(const NUM_MATRIX<D> & mat) const;
    NUM_MATRIX<D> operator - (const NUM_MATRIX<D> & mat) const;

    //* 乗法演算子
    NUM_MATRIX<D> Mul(const D & m) const;
    NUM_MATRIX<D> operator * (const D & m)  const;

    //* 乗法演算子
    NUM_MATRIX<D> Mul(const NUM_MATRIX<D> & mat) const;
    NUM_MATRIX<D> operator * (const NUM_MATRIX<D> & mat) const;

    //* 乗法演算子
    std::vector<D> Mul(const std::vector<D> & vec) const;
    std::vector<D> operator * (const std::vector<D> & vec) const;

    //* 転置行列
    NUM_MATRIX<D> Transpose();

    //行列式
    D Det() const;

    //* ベクター直接アクセス
    NUM_MATRIX<D>* GetVector() const{ return &m_Data; }

    //* 要素アクセス
    D& Val(size_t nRow, size_t nCol);
    const D& Val(size_t nRow, size_t nCol) const;

    //* 要素アクセス
    D& operator () (size_t nRow, size_t nCol);
    const D& operator () (size_t nRow, size_t nCol) const;

    //* 要素アクセス(チェック無)
    D& Elem(size_t nRow, size_t nCol);
    const D& Elem(size_t nRow, size_t nCol) const;

    D* ElemDirect(size_t nRow, size_t nCol);

    //* 疑似逆行列生成
    NUM_MATRIX<D> PsedoInvert();

    //* 逆行列生成
    NUM_MATRIX<D> Invert() const;
    NUM_MATRIX<D> Invert();

    //* LUP分解
    std::vector<size_t> LUPDecompose();

    //* LUP分解による解
    NUM_MATRIX<D> LUPSolve(const std::vector<size_t> & lstRow,
        NUM_MATRIX<D> & mat);

    //* LUP分解による逆行列の計算
    NUM_MATRIX<D> LUPInvert(const std::vector<size_t> & lstRow);

    //* LU分解(2)
    D Lu(std::vector<size_t>& ip);

    //* 逆行列計算(2)
    NUM_MATRIX<D> Inv();

    //* 逆行列計算(3)
    NUM_MATRIX<D> Inv2();

    //* 単位行列設定
    NUM_MATRIX<D> SetIdentity();


    //* マトリックス表示
    void Print() const;

    StdString Str() const;

protected:
    std::vector<D>  m_Data;
    size_t m_nRowSize;
    size_t m_nColSize;
    size_t m_nTotal;

    //std::ostream m_Outputter;
    //std::cout  m_Outputter;
};


///**
// @brief   コンストラクター
// @param	なし
// @return  なし
// @note    なし
// */
template <class D>
inline NUM_MATRIX<D>::NUM_MATRIX()
{
    m_nRowSize = 1;
    m_nColSize = 1;
    m_nTotal = m_nRowSize * m_nColSize;

    m_Data.resize(m_nTotal);
}

///**
// @brief   コンストラクター
// @param	nRows (i) 行数
// @param   nCols (i) 列数
// @return  なし
// @note    なし
// */
template <class D>
inline NUM_MATRIX<D>::NUM_MATRIX(size_t nRows, size_t nCols)
{
    m_nRowSize = nRows;
    m_nColSize = nCols;
    m_nTotal = m_nRowSize * m_nColSize;

    m_Data.resize(m_nTotal);
}

///**
// @brief   初期化付きコンストラクター
// @param	nRows (i) 行数
// @param   nCols (i) 列数
// @param   def  (i) 初期値
// @return  なし
// @note    なし
// */
template <class D>
inline NUM_MATRIX<D>::NUM_MATRIX(size_t nRows, size_t nCols, D def)
{
    m_nRowSize = nRows;
    m_nColSize = nCols;
    m_nTotal = m_nRowSize * m_nColSize;

    m_Data.resize(m_nTotal);
    std::fill(m_Data.begin(), m_Data.end(), def);
}

///**
// @brief   コピーコンストラクター
// @param	m (i) コピー元
// @return  なし
// @note    なし
// */
template <class D>
inline NUM_MATRIX<D>::NUM_MATRIX(const NUM_MATRIX<D> &m)
{
    *this = m;
}

///**
// @brief   デストラクター
// @return  なし
// @note    なし
// */
template <class D>
inline NUM_MATRIX<D>::~NUM_MATRIX()
{
}

///**
// @brief   行数取得
// @param	なし
// @return  行数
// @note    
// */
template <class D> inline
size_t NUM_MATRIX<D>::GetRows() const
{
    return m_nRowSize;
}

///**
// @brief   列数取得
// @param	なし
// @return  列数
// @note    
// */
template <class D> inline
size_t NUM_MATRIX<D>::GetCols() const
{
    return m_nColSize;
}

//* 比較演算子
///**
// @brief   比較演算子
// @param	mat (i) 値
// @return  なし
// @note    全ての項目に同じ値を設定する
// */
template <class D> inline
bool NUM_MATRIX<D>::IsEql(const NUM_MATRIX<D> & mat) const
{
    if (m_nRowSize != mat.m_nRowSize){ return false; }
    if (m_nColSize != mat.m_nColSize){ return false; }

    size_t size = mat.m_Data.size();


    for (size_t iCnt = 0; iCnt < size; iCnt++)
    {
        if (mat.m_Data[iCnt] != m_Data[iCnt]){ return false; }
    }

    return true;
}

template <class D> inline
bool NUM_MATRIX<D>::operator == (const NUM_MATRIX<D> & mat) const
{
    return IsEql(mat);

}

/**
 *  @brief   代入演算子
 *  @param   [in] m 代入元
 *  @retval  代入値
 *  @note    全ての項目に同じ値を設定する
 */
template <class D> inline
void NUM_MATRIX<D>::Eql(const D & m)
{
    std::fill(m_Data.begin(), m_Data.end(), m);
}

template <class D> inline
NUM_MATRIX<D>& NUM_MATRIX<D>::operator = (const D & m)
{
    Eql(m);
    return *this;
}


/**
 *  @brief   代入演算子
 *  @param   [in] mat 代入元
 *  @retval  代入値
 *  @note
 */
template <class D>
void NUM_MATRIX<D>::Eql(const NUM_MATRIX<D> & mat)
{
    m_nRowSize = mat.m_nRowSize;
    m_nColSize = mat.m_nColSize;
    m_nTotal = m_nRowSize * m_nColSize;

    m_Data = mat.m_Data;
}

template <class D>
NUM_MATRIX<D>&  NUM_MATRIX<D>::operator = (const NUM_MATRIX<D> & mat)
{
    Eql(mat);
    return*this;
}
///**
// @brief   加法演算子
// @param	m (i) 値
// @return  なし
// @note    全ての項目に同じ値を加算する
// */
template <class D>
NUM_MATRIX<D> NUM_MATRIX<D>::Add(const D & m) const
{
    NUM_MATRIX<D> ret(*this);

    std::vector<D>::iterator ite;

    for (ite = ret.m_Data.begin();
        ite != ret.m_Data.end();
        ++ite)
    {
        (*ite) += m;
    }
    return ret;
}

template <class D>
NUM_MATRIX<D> NUM_MATRIX<D>::operator + (const D & m) const
{
    return Add(m);
}
///**
// @brief   加法演算子
// @param	m (i) 値
// @return  なし
// @note    全ての項目に同じ値を加算する
// */
template <class D>
NUM_MATRIX<D> NUM_MATRIX<D>::Add(const NUM_MATRIX<D> & mat) const
{
    if (m_nColSize != mat.m_nColSize) { throw NUM_MATRIX_ERROR(MAT_SIZE); }
    if (m_nRowSize != mat.m_nRowSize) { throw NUM_MATRIX_ERROR(MAT_SIZE); }

    std::vector<D>::const_iterator ite;
    std::vector<D>::const_iterator iteMat;
    std::vector<D>::iterator iteRet;

    NUM_MATRIX<D> ret(*this);

    iteMat = mat.m_Data.begin();
    iteRet = ret.m_Data.begin();

    for (ite = ret.m_Data.begin();
        ite != ret.m_Data.end();
        ++ite)
    {
        (*iteRet) = (*ite) + (*iteMat);
        ++iteMat;
        ++iteRet;
    }
    return ret;
}

template <class D>
NUM_MATRIX<D> NUM_MATRIX<D>::operator + (const NUM_MATRIX<D> & mat) const
{
    return Add(mat);
}
///**
// @brief   減法演算子
// @param	m (i) 値
// @return  減算結果
// @note    全ての項目に同じ値を減算する
// */
template <class D>
NUM_MATRIX<D> NUM_MATRIX<D>::Sub(const D & m) const
{
    NUM_MATRIX<D> ret(*this);

    std::vector<D>::iterator ite;

    for (ite = ret.m_Data.begin();
        ite != ret.m_Data.end();
        ++ite)
    {
        (*ite) -= m;
    }
    return ret;
}

template <class D>
NUM_MATRIX<D> NUM_MATRIX<D>::operator - (const D & m) const
{
    return Sub(m);
}

///**
// @brief   減法演算子
// @param	mat (i) 行列
// @return  減算結果
// @note    
// */
template <class D>
NUM_MATRIX<D> NUM_MATRIX<D>::Sub(const NUM_MATRIX<D> & mat) const
{
    if (m_nColSize != mat.m_nColSize) { throw NUM_MATRIX_ERROR(MAT_SIZE); }
    if (m_nRowSize != mat.m_nRowSize) { throw NUM_MATRIX_ERROR(MAT_SIZE); }

    std::vector<D>::const_iterator ite;
    std::vector<D>::const_iterator iteMat;
    std::vector<D>::iterator iteRet;

    NUM_MATRIX<D> ret(*this);

    iteMat = mat.m_Data.begin();
    iteRet = ret.m_Data.begin();

    for (ite = ret.m_Data.begin();
        ite != ret.m_Data.end();
        ++ite)
    {
        (*iteRet) = (*ite) - (*iteMat);
        ++iteMat;
        ++iteRet;
    }
    return ret;
}

template <class D>
NUM_MATRIX<D> NUM_MATRIX<D>::operator - (const NUM_MATRIX<D> & mat) const
{
    return Sub(mat);
}
///**
// @brief   乗法演算子
// @param	m (i) 値
// @return  乗算結果
// @note    全ての項目に同じ値を乗算する
// */
template <class D>
NUM_MATRIX<D> NUM_MATRIX<D>::Mul(const D& m)  const
{
    NUM_MATRIX<D> ret(*this);

    std::vector<D>::iterator ite;

    for (ite = ret.m_Data.begin();
        ite != ret.m_Data.end();
        ite++)
    {
        (*ite) *= m;
    }
    return ret;
}

template <class D>
NUM_MATRIX<D> NUM_MATRIX<D>::operator * (const D& m)  const
{
    return Mul(m);
}

///**
// @brief   乗法演算子
// @param	m (i) 値
// @return  乗算結果
// @note    全ての項目に同じ値を乗算する
// */
template <class D>
NUM_MATRIX<D> NUM_MATRIX<D>::Mul(const NUM_MATRIX<D> & mat) const
{
    if (m_nColSize != mat.m_nRowSize) { throw NUM_MATRIX_ERROR(MAT_SIZE); }

    NUM_MATRIX<D> ret(m_nRowSize, mat.m_nColSize);

    D* pVal;
    for (size_t nRow = 0; nRow < m_nRowSize; nRow++)
    {
        for (size_t nCol = 0; nCol < mat.m_nColSize; nCol++)
        {
            pVal = &ret.m_Data[nRow * mat.m_nColSize + nCol];
            *pVal = 0.0;
            for (size_t mul = 0; mul < m_nColSize; mul++)
            {
                *pVal += m_Data.at(nRow * m_nColSize + mul)
                    * mat.m_Data.at(mul * mat.m_nColSize + nCol);
            }
        }
    }
    return ret;
}

template <class D>
NUM_MATRIX<D> NUM_MATRIX<D>::operator * (const NUM_MATRIX<D> & mat) const
{
    return Mul(mat);
}

///**
// @brief   乗法演算子
// @param	vec (i) ベクトル
// @return  乗算結果
// @note    
// */
template <class D>
std::vector<D> NUM_MATRIX<D>::Mul(const std::vector<D> & vec) const
{
    if (m_nColSize != vec.size()) { throw NUM_MATRIX_ERROR(MAT_SIZE); }
    std::vector<D> ret;
    ret.resize(m_nRowSize);

    for (size_t nRow = 0; nRow < m_nRowSize; nRow++)
    {
        D* pVal = &ret[nRow];
        *pVal = 0.0;
        for (size_t mul = 0; mul < m_nColSize; mul++)
        {
            *pVal += Elem(nRow, mul)
                * vec.at(mul);
        }
    }
    return ret;
}

template <class D>
std::vector<D> NUM_MATRIX<D>::operator * (const std::vector<D> & vec) const
{
    return Mul(vec);
}

//* 
///**
// @brief   転置行列
// @param	なし
// @return  転置行列
// @note    
// */
template <class D>
NUM_MATRIX<D> NUM_MATRIX<D>::Transpose()
{
    NUM_MATRIX<D> ret(m_nColSize, m_nRowSize);
    for (size_t nRow = 0; nRow < m_nRowSize; nRow++)
    {
        for (size_t nCol = 0; nCol < m_nColSize; nCol++)
        {
            ret.m_Data[nCol * m_nRowSize + nRow] = m_Data[nRow * m_nColSize + nCol];
        }
    }
    return ret;
}

///**
// @brief   要素アクセス
// @param	nRow (i) 行
// @param	nCol (i) 列
// @return  指定 行，列の値
// @note    
// */
template <class D>
D& NUM_MATRIX<D>::operator () (size_t nRow, size_t nCol)
{
    if (m_nColSize <= nCol) { throw NUM_MATRIX_ERROR(MAT_RANGE); }
    if (m_nRowSize <= nRow) { throw NUM_MATRIX_ERROR(MAT_RANGE); }
    return m_Data.at(nRow * m_nColSize + nCol);
}


template <class D>
const D& NUM_MATRIX<D>::operator () (size_t nRow, size_t nCol) const
{
    if (m_nColSize <= nCol) { throw NUM_MATRIX_ERROR(MAT_RANGE); }
    if (m_nRowSize <= nRow) { throw NUM_MATRIX_ERROR(MAT_RANGE); }
    return m_Data.at(nRow * m_nColSize + nCol);
}
///**
// @brief   要素アクセス
// @param	nRow (i) 行
// @param	nCol (i) 列
// @return  指定 行，列の値
// @note    チェック無し
// */
template <class D> inline
D& NUM_MATRIX<D>::Elem(size_t nRow, size_t nCol)
{
    return m_Data.at(nRow * m_nColSize + nCol);
}

template <class D> inline
const D& NUM_MATRIX<D>::Elem(size_t nRow, size_t nCol) const
{
    return m_Data.at(nRow * m_nColSize + nCol);
}

template <class D> inline
D* NUM_MATRIX<D>::ElemDirect(size_t nRow, size_t nCol)
{
    return &m_Data.at(nRow * m_nColSize + nCol);
}


///**
// @brief   要素アクセス
// @param	nRow (i) 行
// @param	nCol (i) 列
// @return  指定 行，列の値
// @note    
// */
template <class D>
D& NUM_MATRIX<D>::Val(size_t nRow, size_t nCol)
{
    if (m_nColSize <= nCol) { throw NUM_MATRIX_ERROR(MAT_RANGE); }
    if (m_nRowSize <= nRow) { throw NUM_MATRIX_ERROR(MAT_RANGE); }
    return Elem(nRow, nCol);
}

template <class D>
const D& NUM_MATRIX<D>::Val(size_t nRow, size_t nCol) const
{
    if (m_nColSize <= nCol) { throw NUM_MATRIX_ERROR(MAT_RANGE); }
    if (m_nRowSize <= nRow) { throw NUM_MATRIX_ERROR(MAT_RANGE); }
    return Elem(nRow, nCol);
}

///**
// @brief   疑似逆行列生成
// @param	なし
// @return  疑似逆行列
// @note    
// */
template <class D>
NUM_MATRIX<D> NUM_MATRIX<D>::PsedoInvert()
{
    NUM_MATRIX<D> Trn;
    NUM_MATRIX<D> Solv;

    Trn = Transpose();
    Solv = Trn * (*this);
    return (Solv.Inv() * Trn);
}

///**
// @brief   逆行列計算
// @param	なし
// @return  指定 行，列の値
// @note    逆行列
// */
template <class D>
NUM_MATRIX<D> NUM_MATRIX<D>::Invert()
{
    return LUPInvert(LUPDecompose());
}

template <class D>
NUM_MATRIX<D> NUM_MATRIX<D>::Invert() const
{
    NUM_MATRIX<D> tmp = *this;
    tmp.LUPInvert(tmp.LUPDecompose());
    return tmp;
}

///**
// @brief   LUP分解
// @param	なし
// @return  行順の配列
// @note    
// */
template <class D>
std::vector<size_t> NUM_MATRIX<D>::LUPDecompose()
{
    // 正方行列のみ
    if (m_nRowSize != m_nColSize){ throw NUM_MATRIX_ERROR(MAT_SIZE); }
    if (m_nColSize < 2)          { throw NUM_MATRIX_ERROR(MAT_SIZE); }

    // LU分解(クラウトのアルゴリズム)
    size_t nCnt;
    std::vector<size_t> vChg;
    vChg.resize(m_nRowSize);

    // 
    for (nCnt = 0; nCnt < m_nRowSize; nCnt++)
    {
        vChg.at(nCnt) = nCnt;
    }

    size_t nRow, nCol, nPivot;
    D vVal, vTemp;

    for (nCol = 0; nCol < (m_nColSize - 1); ++nCol)
    {
        vVal = D(0);
        for (nRow = nCol; nRow < m_nRowSize; ++nRow)
        {
            vTemp = abs(Elem(nRow, nCol));
            if (vTemp > vVal)
            {
                vVal = vTemp;
                nPivot = nRow;
            }
        }

        if (vVal == D(0))
        {
            throw NUM_MATRIX_ERROR(MAT_ZERO_DIV);
        }

        //----------
        // 行の交換
        //----------
        std::swap(vChg[nCol], vChg[nPivot]);
        for (nCnt = 0; nCnt < m_nColSize; ++nCnt)
        {
            std::swap(Elem(nCol, nCnt), Elem(nPivot, nCnt));
        }
        //----------

        for (nRow = nCol + 1; nRow < m_nRowSize; ++nRow)
        {
            Elem(nRow, nCol) /= Elem(nCol, nCol);
            for (nCnt = nCol + 1; nCnt < m_nRowSize; ++nCnt)
            {
                Elem(nRow, nCnt) -= Elem(nRow, nCol) * Elem(nCol, nCnt);
            }
        }
    }
    return vChg;
}

///**
// @brief   LUP分解による連立方程式の解
// @param	lstRow (i) 交換行
// @param	mat    (i) 解を求める行列
// @return  解
// @note    LUPDecompose呼び出し後
// */
template <class D>
NUM_MATRIX<D> NUM_MATRIX<D>::LUPSolve(const std::vector<size_t> & lstRow,
    NUM_MATRIX<D> & mat)
{
    if (m_nRowSize != mat.m_nRowSize){ throw NUM_MATRIX_ERROR(MAT_SIZE); }
    if (m_nRowSize != lstRow.size()) { throw NUM_MATRIX_ERROR(MAT_SIZE); }


    size_t nCnt, j, j2;
    D     vSum, vU;
    NUM_MATRIX<D> matY(m_nRowSize, 1);
    NUM_MATRIX<D> matX(m_nRowSize, 1);
    for (nCnt = 0; nCnt < m_nRowSize; nCnt++)
    {
        vSum = D(0);
        j2 = 0;

        for (j = 1; j <= nCnt; ++j)
        {
            vSum += Elem(nCnt, j2) * matY.Elem(j2, 0);
            ++j2;
        }
        matY.Elem(nCnt, 0) = mat.Elem(lstRow[nCnt], 0) - vSum;
    }

    nCnt = m_nRowSize - 1;

    while (1)
    {
        vSum = D(0);
        vU = Elem(nCnt, nCnt);

        for (j = nCnt + 1; j < m_nRowSize; ++j)
        {
            vSum += Elem(nCnt, j) * matX.Elem(j, 0);
        }

        matX.Elem(nCnt, 0) = (matY.Elem(nCnt, 0) - vSum) / vU;

        if (nCnt == 0)
        {
            break;
        }
        nCnt--;
    }
    return matX;
}


///**
// @brief   LUP分解による逆行列の計算
// @param	lstRow (i) 交換行
// @return  解
// @note    LUPDecompose呼び出し後
// */
template <class D>
NUM_MATRIX<D> NUM_MATRIX<D>::LUPInvert(const std::vector<size_t> & lstRow)
{
    size_t i, j;
    NUM_MATRIX<D> matP(m_nRowSize, 1);
    NUM_MATRIX<D> matRes(m_nRowSize, m_nRowSize);

    for (j = 0; j < m_nRowSize; ++j)
    {
        for (i = 0; i < m_nRowSize; ++i)
        {
            matP.Elem(i, 0) = D(0);
        }

        matP.Elem(j, 0) = D(1);

        matP = LUPSolve(lstRow, matP);

        for (i = 0; i < m_nRowSize; ++i)
        {
            matRes.Elem(i, j) = matP.Elem(i, 0);
        }
    }
    return matRes;
}



template <class D>
D NUM_MATRIX<D>::Lu(std::vector<size_t>& ip)  // LU分解
{
    size_t i, j, k, ii, ik;
    D t, u, det;
    std::vector<D> weight;


    weight.resize(m_nRowSize); // weight[0..n-1] の記憶領域確保
    det = 0;                   // 行列式 
    for (k = 0; k < m_nRowSize; k++)    // 各行について 
    {
        ip[k] = k;             // 行交換情報の初期値
        u = 0;                 // その行の絶対値最大の要素を求める
        for (j = 0; j < m_nRowSize; j++)
        {
            t = abs(Elem(k, j));
            if (t > u)
            {
                u = t;
            }
        }
        if (abs(u) == D(0)) { goto EXIT; } // 0 なら行列はLU分解できない
        weight[k] = 1 / u;     // 最大絶対値の逆数
    }

    det = 1;                   // 行列式の初期値
    for (k = 0; k < m_nRowSize; k++)    // 各行について
    {
        u = -1;
        for (i = k; i < m_nRowSize; i++)    // より下の各行について 
        {
            ii = ip[i];            // 重み×絶対値 が最大の行を見つける
            t = abs(Elem(ii, k)) * weight[ii];
            if (t > u) { u = t;  j = i; }
        }
        ik = ip[j];
        if (j != k)
        {
            ip[j] = ip[k];  ip[k] = ik;  // 行番号を交換
            det = -det;  // 行を交換すれば行列式の符号が変わる
        }
        u = Elem(ik, k);  det *= u;    // 対角成分 */
        if (u == 0) { goto EXIT; }       // 0 なら行列はLU分解できない */
        for (i = k + 1; i < m_nRowSize; i++)    // Gauss消去法 */
        {
            ii = ip[i];
            t = (Elem(ii, k) /= u);
            for (j = k + 1; j < m_nRowSize; j++)
            {
                Elem(ii, j) -= t * Elem(ik, j);
            }
        }
    }
EXIT:
    return det;           /* 戻り値は行列式 */
}

template <class D>
NUM_MATRIX<D> NUM_MATRIX<D>::Inv()
{
    long i;
    size_t j, k, ii;
    D t, det;

    // 正方行列のみ
    if (m_nRowSize != m_nColSize){ throw NUM_MATRIX_ERROR(MAT_SIZE); }
    if (m_nColSize < 2)          { throw NUM_MATRIX_ERROR(MAT_SIZE); }

    NUM_MATRIX<D> a_inv(m_nRowSize, m_nColSize);
    std::vector<size_t> ip;  /* 行交換の情報 */
    ip.resize(m_nRowSize);

    det = Lu(ip);
    if (det != D(0))
    {
        for (k = 0; k < m_nRowSize; k++)
        {
            for (i = 0; i < (long)m_nRowSize; i++)
            {
                ii = ip[i];
                t = (ii == k);
                for (j = 0; j < (size_t)i; j++)
                {
                    t -= Elem(ii, j) * a_inv.Elem(j, k);
                }
                a_inv.Elem(i, k) = t;
            }

            for (i = (long)m_nRowSize - 1; i >= 0; i--)
            {
                t = a_inv.Elem(i, k);
                ii = ip[i];
                for (j = i + 1; j < m_nRowSize; j++)
                {
                    t -= Elem(ii, j) * a_inv.Elem(j, k);
                }
                a_inv.Elem(i, k) = t / Elem(ii, i);
            }
        }
    }
    else
    {
        throw NUM_MATRIX_ERROR(MAT_DET_ZERO);
    }
    return a_inv;
}

template <class D>
NUM_MATRIX<D> NUM_MATRIX<D>::Inv2()
{
    //掃き出し法

    size_t i, j, k;
    D buf;

    // 正方行列のみ
    if (m_nRowSize != m_nColSize){ throw NUM_MATRIX_ERROR(MAT_SIZE); }
    if (m_nColSize < 2)          { throw NUM_MATRIX_ERROR(MAT_SIZE); }


    NUM_MATRIX<D> a_inv(m_nRowSize, m_nColSize);
    a_inv.SetIdentity();

    for (i = 0; i < m_nRowSize; i++)
    {
        buf = 1 / Elem(i ,i);

       
        for (j = 0; j < m_nRowSize; j++)
        {
            *ElemDirect(i ,j) = Elem(i ,j) * buf;
            *a_inv.ElemDirect(i ,j) = a_inv.Elem(i ,j) *buf;
        }
        
        for (j = 0; j < m_nRowSize; j++)
        {
            if (i != j)
            {
                buf = Elem(j,i) / Elem(i,i);
                for (k = 0; k < m_nRowSize; k++)
                {
                    *ElemDirect(j ,k) = Elem(j ,k) - (Elem(i ,k) * buf);
                    *a_inv.ElemDirect(j ,k) = a_inv.Elem(j ,k) - (a_inv.Elem(i ,k ) * buf);
                }
            }
        }
    }
    return a_inv;
}

///**
// @brief   単位行列設定
// @param	なし
// @return  単位行列
// @note    
// */
template <class D>
NUM_MATRIX<D> NUM_MATRIX<D>::SetIdentity()
{
    for (size_t nRow = 0; nRow < m_nRowSize; nRow++)
    {
        for (size_t nCol = 0; nCol < m_nColSize; nCol++)
        {
            if (nRow == nCol)
            {
                Elem(nRow, nCol) = D(1);

            }
            else
            {
                Elem(nRow, nCol) = D(0);
            }
        }
    }
    return *this;
}

template <class D>
D NUM_MATRIX<D>::Det() const
{
    // 正方行列のみ
    if (m_nRowSize != m_nColSize){ throw NUM_MATRIX_ERROR(MAT_SIZE); }
    if (m_nColSize < 2)          { throw NUM_MATRIX_ERROR(MAT_SIZE); }


    D dRet;
    if (m_nColSize == 2)
    {
        dRet = Elem(0, 0)*Elem(1, 1) - Elem(0, 1)*Elem(0, 1);
    }
    else if (m_nColSize == 3)
    {
        dRet = Elem(0,0)*(Elem(1,1)*Elem(2,2)-Elem(1,2)*Elem(2,1))
              -Elem(1,0)*(Elem(0,1)*Elem(2,2)-Elem(0,2)*Elem(2,1))
              +Elem(2,0)*(Elem(0,1)*Elem(1,2)-Elem(0,2)*Elem(1,1));
    }
    else
    {
        NUM_MATRIX<D> tmp(*this);
        for (int i = 0; i < m_nColSize; i++) 
        {  
            if (fabs(tmp.Elem(i,i)) > NEAR_ZERO)  
            {
                continue;  
            }

            int k;  
            for (k = 0; k < m_nColSize; k++)  
            {
                if (fabs(tmp.Elem(k,i)) > NEAR_ZERO)  
                {
                    break;  
                }
            }
  
            if (k == m_nColSize)          // all numbers in a column is zero  
            {
                return 0;  
            }
  
            for (int j = 0; j < m_nColSize; j++)  
            {
                tmp.Elem(i,j) += tmp.Elem(k,j);  
            }
        }  
  
        // make a triangular matrix  
        for (int i = 0; i < m_nColSize; i++) 
        {  
            for (int j = 0; j < m_nColSize; j++) 
            {  
                if (i == j)
                {
                    continue;  
                }
                D c = tmp.Elem(j,i) / tmp.Elem(i,i);  
                for (int k = 0; k < m_nColSize; k++)
                {
                    tmp.Elem(j,k) -=  c * tmp.Elem(i,k);  
                }
            }  
        }  
  
        dRet = 1;  
        for (int i = 0; i < m_nColSize; i++)  
        {
            dRet *= tmp.Elem(i,i);  
        }
    }
    return dRet;
}


///**
// @brief   データ表示
// @param	なし
// @return  なし
// @note    
// */
template <class D>
void NUM_MATRIX<D>::Print() const
{
	StdChar szBuf[1024] = {0};
    for (size_t nRow = 0; nRow < m_nRowSize; nRow++)
    {
        for (size_t nCol = 0; nCol < m_nColSize; nCol++)
        {
            _sntprintf(szBuf, 1024, _T("%.3f\t"), m_Data.at(m_nColSize * nRow + nCol));
            OutputDebugString(szBuf);
        }
        OutputDebugString(_T("\r\n"));
    }
    OutputDebugString(_T("\r\n"));
}

template <class D>
StdString NUM_MATRIX<D>::Str() const
{
    StdString strOut;
    StdChar szBuf[1024];
    for (size_t nRow = 0; nRow < m_nRowSize; nRow++)
    {
        for (size_t nCol = 0; nCol < m_nColSize; nCol++)
        {
            _sntprintf(szBuf, 1024, _T("%.3f\t"), m_Data.at(m_nColSize * nRow + nCol));
            strOut = szBuf;
        }
        strOut += _T("\r\n");
    }
    strOut += _T("\r\n");
    return strOut;
}


/*

    //* 除法演算子
    void operator / (const D & m);

    //* 除法演算子
    void operator / (const NUM_MATRIX<D> & m);

    //* 加法代入演算子
    void operator += (const D & m);

    //* 加法代入演算子
    void operator += (const NUM_MATRIX<D> & m);

    //* 減法代入演算子
    void operator -= (const D & m);

    //* 減法代入演算子
    void operator -= (const NUM_MATRIX<D> & m);

    //* 乗法代入演算子
    void operator *= (const D * m);

    //* 乗法代入演算子
    void operator *= (const NUM_MATRIX<D> & m);

    //* 除法代入演算子
    void operator /= (const D * m);

    //* 除法代入演算子
    void operator /= (const NUM_MATRIX<D> & m);

    //* 内積
    D innetProduct (const NUM_MATRIX<D> & m);

    //* 内積
    D get (size_t nRow, size_t nCol);

    //* 空行列確認
    bool isZero(); const;

    //* 単位行列確認
    bool isIdentity(); const;

    //* 転置行列作成
    bool (); const;
    */





#endif //__TMATRIX_H__
