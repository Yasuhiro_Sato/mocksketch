/**
 * @brief			CTestDlg実装ファイル
 * @file		    CTestDlg.cpp
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MockSketch.h"
#include "./TestDlg.h"


/*---------------------------------------------------*/
/* Macros                                            */
/*---------------------------------------------------*/
IMPLEMENT_DYNAMIC(CTestDlg, CDialog)

BEGIN_MESSAGE_MAP(CTestDlg, CDialog)
    ON_WM_SIZE()
    ON_WM_PAINT()
    ON_CBN_SELCHANGE(IDC_CB_TEST, &CTestDlg::OnCbnSelchangeCbTest)
    ON_CBN_SELCHANGE(IDC_CB_TEST2, &CTestDlg::OnCbnSelchangeCbTest2)
    ON_WM_SHOWWINDOW()
    ON_BN_CLICKED(ID_PB_TEST, &CTestDlg::OnBnClickedPbTest)
END_MESSAGE_MAP()


/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CTestDlg::CTestDlg(CWnd* pParent /*=NULL*/): 
    m_pDrawData (0), 
    m_pOnSize   (0),
    m_pTestObj  (0),
    m_pOnTest   (0),
    m_pUserData (0),
    m_pSelChgCb (0),
    m_pSelChgCb2(0),
    m_bInitShow (false),
    m_bAutoFit  (false),
    CDialog(CTestDlg::IDD, pParent)
{

}

/**
 *  @brief  デストラクタ.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CTestDlg::~CTestDlg()
{
}

void CTestDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_CB_TEST, m_cbTest);
    DDX_Control(pDX, IDC_CB_TEST2, m_cbTest2);
    DDX_Control(pDX, ID_PB_TEST, m_pbTest);
}


void CTestDlg::OnPaint()
{
    CPaintDC dc(this); 

    if (m_pDrawFunc)
    {
        m_pDrawFunc(dc.GetSafeHdc(), m_pDrawData);
    }
}

//!< サイズ変更
void CTestDlg::UpdateTestObjct()
{
    CRect rectClient;
    CRect rectButton;
    CRect rectTestButton;
    CRect rectCombo;

	if (GetSafeHwnd() != NULL)
	{
        GetClientRect(rectClient);

        CWnd* pPbOk     = GetDlgItem(IDOK);
        CWnd* pPbCancel = GetDlgItem(IDCANCEL);
        int iButtonSpace  = 10;

        if (pPbOk)
        {
            pPbOk->GetClientRect(rectButton);


            pPbOk->MoveWindow (rectClient.Width() - rectButton.Width() * 2 - iButtonSpace,
                                rectClient.Height() - rectButton.Height(),
                                rectButton.Width(),
                                rectButton.Height());
            if (pPbCancel)
            {
                pPbCancel->MoveWindow (rectClient.Width() - rectButton.Width(),
                                    rectClient.Height() - rectButton.Height(),
                                    rectButton.Width(),
                                    rectButton.Height());

            }

            m_pbTest.GetClientRect(rectTestButton);
            m_pbTest.MoveWindow (rectClient.Width() 
                                - rectButton.Width() * 2 
                                - rectTestButton.Width()
                                - (iButtonSpace * 2),
                                rectClient.Height() - rectButton.Height(),
                                rectButton.Width(),
                                rectButton.Height());


            m_cbTest.GetClientRect(rectCombo);

             
            m_cbTest.MoveWindow ( 10,
                                    rectClient.Height() - rectCombo.Height(),
                                    rectCombo.Width(),
                                    rectCombo.Height());

            m_cbTest2.MoveWindow ( 20 + rectCombo.Width(),
                                    rectClient.Height() - rectCombo.Height(),
                                    rectCombo.Width(),
                                    rectCombo.Height());



        }


        if (m_pTestObj)
        {
            if (m_bAutoFit)
            {
                if (m_pTestObj->GetSafeHwnd() != NULL)
                {
                    m_pTestObj->SetWindowPos(NULL,  rectClient.left, 
                                                    rectClient.top, 
                                                    rectClient.Width(), 
                                                    rectClient.Height() - rectButton.Height() - 5, 
                                                    SWP_NOACTIVATE | SWP_NOZORDER);
                }
            }
        }
	}

}


//!< サイズ変更
void CTestDlg::OnSize(UINT nType, int cx, int cy)
{

    UpdateTestObjct();

    if (m_pOnSize)
    {
        m_pOnSize(m_pTestObj, nType, cx, cy);
    }
}

//!< ダイアログ初期化
BOOL CTestDlg::OnInitDialog()
{
    CDialog::OnInitDialog();

    if (m_pOnInit)
    {
        m_pOnInit(m_pTestObj);
    }

    foreach( StdString& str, m_lstCombo)
    {
        m_cbTest.AddString(str.c_str());
    }

    foreach( StdString& str, m_lstCombo2)
    {
        m_cbTest2.AddString(str.c_str());
    }

    UpdateTestObjct();
    return TRUE;
}

//!<  ウインドウプロシジャ
LRESULT CTestDlg::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
    if (m_pWindowProc)
    {
        m_pWindowProc(m_pTestObj, message, wParam, lParam);
    }
    return CDialog::WindowProc(message, wParam, lParam);
}

BOOL CTestDlg::PreTranslateMessage(MSG* pMsg) 
{
    if (m_pPreTransProc)
    {
        m_pPreTransProc(m_pTestObj, pMsg);
    }
    return CDialog::PreTranslateMessage(pMsg);
}

//!<  コンボボックス変更処理
void CTestDlg::OnCbnSelchangeCbTest()
{
    if (m_pSelChgCb)
    {
        int iIndex;
        StdChar szTxt[1024];
        iIndex = m_cbTest.GetCurSel();
        m_cbTest.GetLBText(iIndex, szTxt);
        m_pSelChgCb(szTxt);
    }
}

void CTestDlg::OnCbnSelchangeCbTest2()
{
    if (m_pSelChgCb2)
    {
        int iIndex;
        StdChar szTxt[1024];
        iIndex = m_cbTest2.GetCurSel();
        m_cbTest2.GetLBText(iIndex, szTxt);
        m_pSelChgCb2(szTxt);
    }
}

void CTestDlg::OnShowWindow(BOOL bShow, UINT nStatus)
{
    CDialog::OnShowWindow(bShow, nStatus);

    if (!m_bInitShow)
    {
        UpdateTestObjct();
        m_bInitShow = true;
    }
}

void CTestDlg::OnBnClickedPbTest()
{
    if (m_pOnTest)
    {
        m_pOnTest(m_pTestObj);
    }
}
