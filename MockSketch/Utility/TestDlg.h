/**
 * @brief			CTestDlgヘッダーファイル
 * @file		    CTestDlg.h
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

#pragma once


/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "../resource.h"
#include "../resource2.h"

#include "afxwin.h"

// CTestDlg ダイアログ
// 描画テスト用に作成


class CTestDlg : public CDialog
{
	DECLARE_DYNAMIC(CTestDlg)

    //描画関数
    boost::function<void (HDC, void*) >      m_pDrawFunc;
    boost::function<void (CWnd*, UINT , int, int) > m_pOnSize;
    boost::function<void (CWnd*) >                m_pOnInit;
    boost::function<void (CWnd*) >                m_pOnTest;
    boost::function< LRESULT (CWnd*, UINT , WPARAM , LPARAM)>   m_pWindowProc;
    boost::function< BOOL (CWnd*, MSG*)>    m_pPreTransProc;
    boost::function<void (StdString) >      m_pSelChgCb;
    boost::function<void (StdString) >      m_pSelChgCb2;

    void*                               m_pDrawData;
    void*                               m_pUserData;
    CWnd*                               m_pTestObj;
    bool                                m_bAutoFit;

    std::vector<StdString>              m_lstCombo;
    std::vector<StdString>              m_lstCombo2;

    CComboBox m_cbTest;
    CComboBox m_cbTest2;
    CButton   m_pbTest;

    bool                                m_bInitShow;

public:
	CTestDlg(CWnd* pParent = NULL);   // 標準コンストラクタ
	virtual ~CTestDlg();

    //!< TestObj取得
    CWnd* GetTestObj()
    {
        return m_pTestObj;
    }

    //!< OnDraw関数設定
    void SetDrawFunc( boost::function<void (HDC, void*) > func)
    {
        m_pDrawFunc = func;
    }

    //!< OnSize関数設定
    void SetOnSize( boost::function<void (CWnd*, UINT , int, int) > func)
    {
        m_pOnSize = func;
    }

    //!< OnInit関数設定
    void SetOnInit( boost::function<void (CWnd*) > func)
    {
        m_pOnInit = func;
    }

    //!< OnPbTest関数設定
    void SetOnPbTest( boost::function<void (CWnd*) > func)
    {
        m_pOnTest = func;
    }

    //!< WindwProc関数設定
    void SetWindowProc( boost::function<LRESULT (CWnd*, UINT , WPARAM , LPARAM)>  func)
    {
        m_pWindowProc = func;
    }

    //!< WindwProc関数設定
    void SetPreTransProc( boost::function<BOOL (CWnd*, MSG*)>  func)
    {
        m_pPreTransProc = func;
    }

    //!< コンボボックス変更処理
    void SetSelChgCb( boost::function<void ( StdString )>  func)
    {
        m_pSelChgCb = func;
    }

    //!< コンボボックスリスト設定
    void SetCombo(const std::vector<StdString>& lstCombo )
    {
        m_lstCombo = lstCombo;
    }

    //!< コンボボックス2変更処理
    void SetSelChgCb2( boost::function<void ( StdString )>  func)
    {
        m_pSelChgCb2 = func;
    }

    //!< コンボボックスリスト2設定
    void SetCombo2(const std::vector<StdString>& lstCombo )
    {
        m_lstCombo2 = lstCombo;
    }


    //描画データの設定
    void SetDrawData( void* pData)
    {
        m_pDrawData = pData;
    }

    //ユーザーデータの設定
    void SetUserData( void* pData)
    {
        m_pUserData = pData;
    }

    //ユーザーデータの取得
    void* GetUserData()
    {
        return m_pUserData;
    }

    //テストオブジェクトの設定
    void SetObj( CWnd* pObj)
    {
        m_pTestObj = pObj;
    }

    //テストオブジェクトの取得
    CWnd* GetObj()
    {
        return m_pTestObj;
    }

    //サイズ自動調整
    void EnableAutoFit( bool bEnable = true)
    {
        m_bAutoFit = bEnable;
    }

    //終了
    void Exit()
    {
        OnOK();
    }

// ダイアログ データ
	enum { IDD = IDD_DLG_TEST };
protected:


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート


    void UpdateTestObjct();

	DECLARE_MESSAGE_MAP()
public:
    afx_msg void OnPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);
    virtual BOOL OnInitDialog();
protected:
    virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
public:
    afx_msg void OnCbnSelchangeCbTest();
    afx_msg void OnCbnSelchangeCbTest2();
    afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
    afx_msg void OnBnClickedPbTest();
    virtual BOOL PreTranslateMessage(MSG* pMsg);

};
