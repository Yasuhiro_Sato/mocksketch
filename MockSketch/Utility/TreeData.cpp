/**
 * @brief			TreeData実装ファイル
 * @file		    TreeData.cpp
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./TreeData.h"
#include "CUtility.h"


/*---------------------------------------------------*/
/* Macros                                            */
/*---------------------------------------------------*/



//*******************************
//             TEST
//*******************************
#ifdef _DEBUG

#include "System/CSystem.h"


void funcCallback(E_TREE_ACTION eAct, int iId )
{
    StdString strAct;
    switch(eAct)
    {
    case E_ADD:{strAct = _T("ADD"); break;}
    case E_DEL:{strAct = _T("DEL"); break;}
    case E_CLR:{strAct = _T("CLR"); break;}
    case E_LOAD:{strAct = _T("LOAD"); break;}
    case E_SAVE:{strAct = _T("SAVE"); break;}
    default:{strAct = _T("Unknown"); break;}

    }
    DB_PRINT(_T("Callback %s id=%d\n"), strAct.c_str(), iId);
}

void TEST_TREE_DATA()
{
    STD_DBG(_T("%s Start\n"), DB_FUNC_NAME.c_str());
    
    TREE_DATA<StdString> treeInt1;
    TREE_DATA<StdString> treeInt2;



    namespace fs = boost::filesystem;
    StdPath  pathTest = CSystem::GetSystemPath();
    pathTest /= _T("TEST_TREE_DATA.xml");

    int iId[8];
    int iId_Child1[3];
    int iId_Child2[3];
    int iId_Child3[3];

    StdString strData[8];
    StdString strSub1[3];
    StdString strSub2[3];
    StdString strSub3[2];


    strData[0] = _T("No1");
    strData[1] = _T("No2");
    strData[2] = _T("No3");
    strData[3] = _T("No4");
    strData[4] = _T("No5");
    strData[5] = _T("No6");
    strData[6] = _T("No7");

    strData[7] = _T("No0");

    strSub1[0] = _T("No3-1");
    strSub1[1] = _T("No3-2");
    strSub1[2] = _T("No3-3");

    strSub2[0] = _T("No6-1");
    strSub2[1] = _T("No6-2");
    strSub2[2] = _T("No6-3");

    strSub3[0] = _T("No6-3-1");
    strSub3[1] = _T("No6-3-2");

    treeInt1.SetNotifyCallback(funcCallback);

DB_PRINT(_T("ADD_1\n"));

    iId[0] = treeInt1.AddData( -1, strData[0]);
    iId[1] = treeInt1.AddData(iId[0], strData[1]);
    iId[2] = treeInt1.AddData(iId[1], strData[2]);
    iId[3] = treeInt1.AddData(iId[2], strData[3]);

    STD_ASSERT(*treeInt1.GetItem(iId[0]) == strData[0]);
    STD_ASSERT(*treeInt1.GetItem(iId[1]) == strData[1]);
    STD_ASSERT(*treeInt1.GetItem(iId[2]) == strData[2]);
    STD_ASSERT(*treeInt1.GetItem(iId[3]) == strData[3]);


    iId_Child1[0] = treeInt1.AddChild(iId[2], strSub1[0]);
    iId_Child1[1] = treeInt1.AddData(iId_Child1[0], strSub1[1]);
    iId_Child1[2] = treeInt1.AddDataLast(iId_Child1[0], strSub1[2]);

    iId[4] = treeInt1.AddDataLast(iId[2], strData[4]);
    iId[5] = treeInt1.AddDataLast(iId[0], strData[5]);
    iId[6] = treeInt1.AddDataLast(iId[4], strData[6]);

    STD_ASSERT(*treeInt1.GetItem(iId[4]) == strData[4]);
    STD_ASSERT(*treeInt1.GetItem(iId[5]) == strData[5]);
    STD_ASSERT(*treeInt1.GetItem(iId[6]) == strData[6]);

    STD_ASSERT(-1 == treeInt1.GetChildId(iId[0]));
    STD_ASSERT(-1 == treeInt1.GetChildId(iId[1]));
    STD_ASSERT(iId_Child1[0] == treeInt1.GetChildId(iId[2]));
    STD_ASSERT(-1 == treeInt1.GetChildId(iId[3]));


    STD_ASSERT(iId_Child1[1] == treeInt1.GetNextId(iId_Child1[0]));
    STD_ASSERT(iId_Child1[2] == treeInt1.GetNextId(iId_Child1[1]));
    STD_ASSERT(           -1 == treeInt1.GetNextId(iId_Child1[2]));
    

    iId_Child2[0] = treeInt1.AddChild(iId[5], strSub2[0]);
    iId_Child2[1] = treeInt1.AddDataLast(iId_Child2[0], strSub2[1]);
    iId_Child2[2] = treeInt1.AddDataLast(iId_Child2[0], strSub2[2]);

    STD_ASSERT(iId_Child2[0] == treeInt1.GetChildId(iId[5]));
    STD_ASSERT(iId_Child2[1] == treeInt1.GetNextId(iId_Child2[0]));
    STD_ASSERT(iId_Child2[2] == treeInt1.GetNextId(iId_Child2[1]));
    STD_ASSERT(           -1 == treeInt1.GetNextId(iId_Child2[2]));

    iId_Child3[0] = treeInt1.AddChild(iId_Child2[2], strSub3[0]);
    iId_Child3[1] = treeInt1.AddDataLast(iId_Child3[0], strSub3[1]);

    STD_ASSERT(-1 == treeInt1.GetChildId(iId_Child2[0]));
    STD_ASSERT(-1 == treeInt1.GetChildId(iId_Child2[1]));
    STD_ASSERT(iId_Child3[0] == treeInt1.GetChildId(iId_Child2[2]));
    STD_ASSERT(iId_Child3[1] == treeInt1.GetNextId(iId_Child3[0]));
    STD_ASSERT(-1 == treeInt1.GetNextId(iId_Child3[1]));

DB_PRINT(_T("Insert Root \n"));

    //Rootに挿入
    iId[7] = treeInt1.AddData( -1, strData[7]);
    STD_ASSERT(iId[0] == treeInt1.GetNextId(iId[7]));

    //GetRootテスト
    STD_ASSERT(iId[7] == treeInt1.GetRootId());


#if 1
DB_PRINT(_T("Save \n"));
    //保存
    {
        StdStreamOut outFs(pathTest);
        {
            StdXmlArchiveOut outXml(outFs);
            outXml << boost::serialization::make_nvp("Root", treeInt1);
        }
        outFs.close();
    }


    //読込
    {
        StdStreamIn inFs(pathTest);
        {
            StdXmlArchiveIn inXml(inFs);
            inXml >> boost::serialization::make_nvp("Root", treeInt2);
        }
        inFs.close();
    }

    //読み込み直後は以下の状態になっている
    // 01  No.0
    // 02  No.1
    // 03  No.2
    // 04  No.3
    // 05      No.3-1
    // 06      No.3-2
    // 07      No.3-3
    // 08  No.4
    // 09  No.5
    // 10  No.6
    // 11      No.6-1
    // 12      No.6-2
    // 13      No.6-3
    // 14          No.6-3-1
    // 15          No.6-3-2
    // 16  No.7



    bool bEquql;
    bEquql = treeInt1.IsEqual( &treeInt2);

    STD_ASSERT(bEquql);

    //削除1
    treeInt2.Delete(10);


    STD_ASSERT(16 == treeInt2.GetNextId(9));

    STD_ASSERT(NULL == treeInt2.GetItem(11));
    STD_ASSERT(NULL == treeInt2.GetItem(12));
    STD_ASSERT(NULL == treeInt2.GetItem(15));

    STD_ASSERT(9 == treeInt2.GetBeforeId(16));

    //削除2
    treeInt2.Delete(5);
    STD_ASSERT( 6 == treeInt2.GetChildId(4));
    STD_ASSERT(-1 == treeInt2.GetBeforeId(6));
    STD_ASSERT( 8 == treeInt2.GetNextId(4));


    //読込
    StdStreamIn inFs2(pathTest);
    {
        StdXmlArchiveIn inXml2(inFs2);
        inXml2 >> boost::serialization::make_nvp("Root", treeInt2);
    }
    inFs2.close();

    bEquql = treeInt1.IsEqual( &treeInt2);
    STD_ASSERT(bEquql);

#endif

#if 1
    //子アイテム削除
    treeInt2.DeleteChild(4);
    STD_ASSERT(-1 == treeInt2.GetChildId(4));
    STD_ASSERT(NULL == treeInt2.GetItem(5));
    STD_ASSERT(NULL == treeInt2.GetItem(6));
    STD_ASSERT(NULL == treeInt2.GetItem(7));

    // 01  No.0
    // 02  No.1
    // 03  No.2
    // 04  No.3
    // 08  No.4
    // 09  No.5
    // 10  No.6 -----------------
    // 11      No.6-1
    // 12      No.6-2
    // 13      No.6-3
    // 14          No.6-3-1
    // 15          No.6-3-2
    // 16  No.7

    //移動
    treeInt2.Move(10, 3);

    // 01  No.0
    // 02  No.1
    // 03  No.2
    // 10  No.6  ++++++++++++++
    // 11      No.6-1
    // 12      No.6-2
    // 13      No.6-3  ---------------
    // 14          No.6-3-1
    // 15          No.6-3-2
    // 04  No.3
    // 08  No.4
    // 09  No.5
    // 16  No.7

    STD_ASSERT( 10 == treeInt2.GetNextId(3));
    STD_ASSERT(  4 == treeInt2.GetNextId(10));



    //子アイテムに移動1
    treeInt2.MoveChild(13, 8);

    // 01  No.0
    // 02  No.1
    // 03  No.2
    // 10  No.6
    // 11      No.6-1
    // 12      No.6-2
    // 04  No.3
    // 08  No.4 ++++++++++++++++
    // 13      No.6-3
    // 14          No.6-3-1 -------------
    // 15          No.6-3-2
    // 09  No.5
    // 16  No.7

    STD_ASSERT( -1 == treeInt2.GetNextId(12));
    STD_ASSERT( 13 == treeInt2.GetChildId(8));
    STD_ASSERT(  8 == treeInt2.GetParentId(13));
    STD_ASSERT( -1 == treeInt2.GetBeforeId(13));
    STD_ASSERT( 14 == treeInt2.GetChildId(13));
    STD_ASSERT( 10 == treeInt2.GetParentId(12));



    //子アイテムに移動2
    treeInt2.MoveChild(14, 1);

    // 01  No.0 ++++++++++++++++
    // 14     No.6-3-1
    // 02  No.1
    // 03  No.2
    // 10  No.6
    // 11      No.6-1
    // 12      No.6-2
    // 04  No.3
    // 08  No.4
    // 13      No.6-3
    // 15         No.6-3-2 --------------
    // 09  No.5
    // 16  No.7

    treeInt2.MoveChild(14, 2);

    STD_ASSERT(  2 == treeInt2.GetNextId(1));
    STD_ASSERT( 14 == treeInt2.GetChildId(1));
    STD_ASSERT(  1 == treeInt2.GetParentId(14));
    STD_ASSERT( -1 == treeInt2.GetNextId(14));

    STD_ASSERT( 15 == treeInt2.GetChildId(13));
    STD_ASSERT( 13 == treeInt2.GetParentId(15));
    STD_ASSERT( -1 == treeInt2.GetBeforeId(15));
    STD_ASSERT( -1 == treeInt2.GetNextId(15));


    //Rootに移動
    treeInt2.Move(15, -1);
    // 15  No.6-3-2 ++++++++++++
    // 01  No.0
    // 14     No.6-3-1
    // 02  No.1
    // 03  No.2
    // 10  No.6
    // 11      No.6-1 -------
    // 12      No.6-2
    // 04  No.3
    // 08  No.4
    // 13      No.6-3
    // 09  No.5
    // 16  No.7

    STD_ASSERT( -1 == treeInt2.GetParentId(15));
    STD_ASSERT(  1 == treeInt2.GetNextId(15));
    STD_ASSERT( 15 == treeInt2.GetBeforeId(1));
    STD_ASSERT( -1 == treeInt2.GetNextId(13));

    //子アイテムの移動
    treeInt2.Move(11, 4);
    // 15  No.6-3-2 --------------
    // 01  No.0
    // 14     No.6-3-1
    // 02  No.1
    // 03  No.2
    // 10  No.6
    // 12      No.6-2
    // 04  No.3
    // 11  No.6-1 ++++++++++
    // 08  No.4
    // 13      No.6-3
    // 09  No.5
    // 16  No.7

    STD_ASSERT( 12 == treeInt2.GetChildId(10));
    STD_ASSERT( 10 == treeInt2.GetParentId(12));
    STD_ASSERT( -1 == treeInt2.GetBeforeId(12));
    STD_ASSERT( -1 == treeInt2.GetNextId(12));


    STD_ASSERT( 11 == treeInt2.GetNextId(4));
    STD_ASSERT(  8 == treeInt2.GetNextId(11));
    STD_ASSERT(  4 == treeInt2.GetBeforeId(11));
    STD_ASSERT( 11 == treeInt2.GetBeforeId(8));


    // すでに子アイテムがある所に
    // 子アイテムの移動を行う
    treeInt2.MoveChild(15, 10);

    // 01  No.0
    // 14     No.6-3-1
    // 02  No.1
    // 03  No.2
    // 10  No.6
    // 15      No.6-3-2 +++++++++++++ 
    // 12      No.6-2
    // 04  No.3
    // 11  No.6-1
    // 08  No.4
    // 13      No.6-3
    // 09  No.5
    // 16  No.7
    STD_ASSERT(  1 == treeInt2.GetRootId());
    STD_ASSERT( -1 == treeInt2.GetBeforeId(1));
    STD_ASSERT( 15 == treeInt2.GetChildId(10));
    STD_ASSERT( 15 == treeInt2.GetBeforeId(12));
    STD_ASSERT( 10 == treeInt2.GetParentId(15));



    //-------------------------------
    // GetItemWithChildテスト
    //-------------------------------
#endif
#if 1
    //読込
    StdStreamIn inFs3(pathTest);
    {
        StdXmlArchiveIn inXml3(inFs3);
        inXml3 >> boost::serialization::make_nvp("Root", treeInt2);
    }
    inFs3.close();


    //読み込み直後は以下の状態になっている
    // 01  No.0
    // 02  No.1
    // 03  No.2
    // 04  No.3
    // 05      No.3-1
    // 06      No.3-2
    // 07      No.3-3
    // 08  No.4
    // 09  No.5
    // 10  No.6
    // 11      No.6-1
    // 12      No.6-2
    // 13      No.6-3
    // 14          No.6-3-1
    // 15          No.6-3-2
    // 16  No.7


    TREE_DATA<StdString>::TREE_ITEM item;

    std::vector< TREE_DATA<StdString>::TREE_ITEM>  lstTree;
    treeInt2.GetItemWithChild(10, &lstTree);


    treeInt2.Delete(10);

    //一度切り取って
    // 01  No.0
    // 02  No.1
    // 03  No.2
    // 04  No.3
    // 05      No.3-1
    // 06      No.3-2
    // 07      No.3-3
    // 08  No.4
    // 09  No.5
    // 16  No.7



    bEquql = treeInt1.IsEqual( &treeInt2);
    STD_ASSERT(!bEquql);


    treeInt2.InsertItemWithChild(9, lstTree, false);

    // 再度追加
    // 01  No.0
    // 02  No.1
    // 03  No.2
    // 04  No.3
    // 05      No.3-1
    // 06      No.3-2
    // 07      No.3-3
    // 08  No.4
    // 09  No.5
    // 10  No.6
    // 11      No.6-1
    // 12      No.6-2
    // 13      No.6-3
    // 14          No.6-3-1
    // 15          No.6-3-2
    // 16  No.7

    bEquql = treeInt1.IsEqual( &treeInt2);
    STD_ASSERT(bEquql);

#endif


    STD_DBG(_T("%s End\n"), DB_FUNC_NAME.c_str());
    STD_DBG(_T("\n"));
}
#endif //_DEBUG