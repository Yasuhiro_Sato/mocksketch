/**
 * @brief			FolderDataヘッダーファイル
 * @file		    FolderData.h
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#if !defined (__TREE_DATA_H__)
#define __TREE_DATA_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "SYSTEM/MOCK_ERROR.h"

//  TREE状のデータを保存、再現するためのクラス
//  NODEがフォルダー、Dataがファイルのような感じ
//
//  取りあえずフォルダーデータの保存と読み込みを行うだけなので
//  複雑な操作は考えない。

    enum E_TREE_ACTION
    {
        E_ADD,
        E_DEL,
        E_CLR,
        E_LOAD,
        E_SAVE,
    };

template <class D>
class TREE_DATA
{
public:
    struct TREE_ITEM
    {
        int                         iId;
        int                         iType;
        StdString                   strName;
        D                           data;
        int                         iChild;
        int                         iNext;

        //非保存
        int                         iParent;
        int                         iBefore;
        bool                        bExpand;

        TREE_ITEM():
            iId(-1),
            iType(0),
            iChild(-1),
            iNext(-1),
            iParent(-1),
            iBefore(-1),
            bExpand(false)
            {}

    private:
        friend class boost::serialization::access;  
        template<class Archive>
        void serialize(Archive& ar, const unsigned int version)
        {
            SERIALIZATION_INIT
                SERIALIZATION_BOTH("Id"     , iId);
                SERIALIZATION_BOTH("iType"  , iType);
                SERIALIZATION_BOTH("Name"   , strName);
                SERIALIZATION_BOTH("Child"  , iChild);
                SERIALIZATION_BOTH("Next"   , iNext);
                SERIALIZATION_BOTH("Data"  , data);
            MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
        }
    };


public:

    //* コンストラクター
    TREE_DATA():m_iId(1), m_pFunc(NULL)
    {
    }

    //* デストラクター
    ~TREE_DATA()
    {
    }



    //アイテム数取得
    int GetItemCount(int iId) const
    {
        return static_cast<int>(mapTree.size());
    }

    //兄弟アイテムID取得
    int GetNextId(int iId) const
    {
        std::map<int, TREE_ITEM>::const_iterator iteMap;
        iteMap = mapTree.find(iId);
        if ( iteMap == mapTree.end())
        {
            return -1;
        }

       return iteMap->second.iNext;
    }

    //兄弟アイテムID取得
    int GetBeforeId(int iId) const
    {
        std::map<int, TREE_ITEM>::const_iterator iteMap;
        iteMap = mapTree.find(iId);
        if ( iteMap == mapTree.end())
        {
            return -1;
        }

       return iteMap->second.iBefore;
    }

    //子アイテムID取得
    int GetChildId(int iId) const
    {
        std::map<int, TREE_ITEM>::const_iterator iteMap;
        iteMap = mapTree.find(iId);
        if ( iteMap == mapTree.end())
        {
            return -1;
        }
        return iteMap->second.iChild;
    }

    //親アイテムID取得
    int GetParentId(int iId) const
    {
        std::map<int, TREE_ITEM>::const_iterator iteMap;
        iteMap = mapTree.find(iId);
        if ( iteMap == mapTree.end())
        {
            return -1;
        }
        return iteMap->second.iParent;
    }

    //ルートアイテム取得
    int GetRootId() const
    {
        std::map<int, TREE_ITEM>::const_iterator iteMap;

        if (mapTree.size() == 0)
        {
            return -1;
        }

        int iId;
        iId = mapTree.begin()->second.iId;


        bool bContinue = false;
        int iCnt = 0;
        do{
            iteMap = mapTree.find(iId);
 
            iId = iteMap->second.iParent;
            if (iId == -1)
            {
                iId = iteMap->second.iBefore;
            }


            iCnt++;
            if (iCnt > 1000)
            {
                STD_ASSERT(_T("iCnt > 1000\n"));
                return -1;
            }

            bContinue = (iteMap->second.iParent != -1) ||
                        (iteMap->second.iBefore != -1);

        } while(bContinue);


        iId = iteMap->second.iId;
        return iId;
    }

    // コールバック設定
    void SetNotifyCallback(boost::function<void (E_TREE_ACTION, int)> func)
    {
        m_pFunc = func;
    }

    // コールバック削除
    void RemoveNotifyCallback()
    {
        m_pFunc = NULL;
    }

    //兄弟アイテム取得
    D* GetNext(int iId) const
    {
        int iNext;
        iNext = GetNextId(iId);
        if (iNext != -1)
        {
            return GetItem(iNext)l
        }
        return NULL;
    }

    //子アイテム取得
    D* GetChild(int iId) const
    {
        int iChild;
        iChild = GetChildId(iId);
        if (iChild != -1)
        {
            return GetItem(iNext);
        }
        return NULL;
    }

    //ルートアイテム取得
    D* GetRoot() const
    {
        return GetItem(GetRootId());
    }

    //ツリーデータ取得
    bool GetTreeValues(StdString* pName,
                       int* pType,
                       bool* pExpand,
                       int iId) const
    {
        const TREE_ITEM* pItem = _GetItem(iId);
        if (pItem)
        {
            if (pName)
            {
                *pName = pItem->strName;
            }
            
            if (pType)
            {
                *pType = pItem->iType;
            }

            if (pExpand)
            {
                *pExpand = pItem->bExpand;
            }
            return true;
        }
        return false;
    }

    //ツリーデータ取得
    bool GetTreeValues(StdString* pName,
                       int* pType,
                       bool* pExpand,
                       D*    pData,
                       int iId) const
    {
        const TREE_ITEM* pItem = _GetItem(iId);
        if (pItem)
        {
            if (pName)
            {
                *pName = pItem->strName;
            }
            
            if (pType)
            {
                *pType = pItem->iType;
            }

            if (pExpand)
            {
                *pExpand = pItem->bExpand;
            }

            if (pData)
            {
                *pData = pItem->data;
            }
            return true;
        }
        return false;
    }

    //ツリーデータ取得
    bool SetTreeValues(const StdString* pName,
                       const int* pType,
                       const bool* pExpand,
                       int iId)
    {
        TREE_ITEM* pItem = _GetItem(iId);
        if (pItem)
        {
            if (pName)
            {
                pItem->strName = *pName;
            }
            
            if (pType)
            {
                pItem->iType = *pType;
            }

            if (pExpand)
            {
                pItem->bExpand = *pExpand;
            }
            return true;
        }
        return false;
    }

    //ツリーデータ設定
    bool SetTreeValues(const StdString& strName,
                       const int iType,
                       const bool bExpand,
                       int iId)
    {
        TREE_ITEM* pItem = _GetItem(iId);
        if (pItem)
        {
            pItem->strName = strName;
            pItem->iType =  iType;
            pItem->bExpand = bExpand;
            return true;
        }
        return false;
    }


   //ツリーデータ設定
    bool SetTreeValues(const StdString& strName,
                       const int iType,
                       const bool bExpand,
                       const D& data,
                       int iId)
    {
        TREE_ITEM* pItem = _GetItem(iId);
        if (pItem)
        {
            pItem->strName = strName;
            pItem->iType =  iType;
            pItem->bExpand = bExpand;
            pItem->data    = data;
            return true;
        }
        return false;
    }

    //ツリーデータ設定
    bool SetTreeValues(const StdString& strName,
                       const int iType,
                       int iId)
    {
        TREE_ITEM* pItem = _GetItem(iId);
        if (pItem)
        {
            pItem->strName = strName;
            pItem->iType =  iType;
            return true;
        }
        return false;
    }

    //アイテム名取得
    StdString GetName(int iId) const
    {
        const TREE_ITEM* pItem = _GetItem(iId);
  
        if (pItem)
        {
            return pItem->strName;
        }

        return _T("");
    }

    //アイテムタイプ取得
    int GetType(int iId) const
    {
        const TREE_ITEM* pItem = _GetItem(iId);
  
        if (pItem)
        {
            return pItem->iType;
        }

        return -1;
    }

    //展開有無取得
    bool IsExpand(int iId) const
    {
        const TREE_ITEM* pItem = _GetItem(iId);
        if (pItem)
        {
            return pItem->bExpand;
        }
        return false;
    }

    //アイテム名設定
    bool SetName(int iId, const StdString& strName)
    {
        TREE_ITEM* pItem = _GetItem(iId);
        if (pItem)
        {
            pItem->strName = strName;
            return true;
        }
        return false;
    }

    //アイテム種別設定
    bool SetType(int iId, int iType)
    {
        TREE_ITEM* pItem = _GetItem(iId);
        if (pItem)
        {
            pItem->iType = iType;
            return true;
        }
        return false;
    }

    //展開有無設定
    bool SetExpand(int iId, bool bExpand)
    {
        TREE_ITEM* pItem = _GetItem(iId);
        if (pItem)
        {
            pItem->bExpand = bExpand;
            return true;
        }
        return false;
    }


    //アイテム取得
    D* GetItem(int iId)
    {
        std::map<int, TREE_ITEM>::iterator iteMap;
        iteMap = mapTree.find(iId);
        if ( iteMap == mapTree.end())
        {
            return NULL;
        }

       return &iteMap->second.data;
    }

    /**
     * @brief   アイテム取得(const).
     * @param   [in] iId    データID
     * @return  アイテム
     * @Note    
     */
    const D* GetItem(int iId) const
    {
        std::map<int, TREE_ITEM>::const_iterator iteMap;
        iteMap = mapTree.find(iId);
        if ( iteMap == mapTree.end())
        {
            return NULL;
        }

       return &iteMap->second.data;
    }


    /**
     * @brief   アイテム取得
     * @param   [in]  iId           データID
     * @param   [out] pListTree     データID
     * @return  アイテム
     * @Note    
     */
    bool GetItemWithChild(int iId, std::vector<TREE_ITEM>* pListTree) const
    {
        if (!pListTree)
        {
            return false;
        }

        pListTree->clear();

        std::map<int, TREE_ITEM>::const_iterator iteMap;
        iteMap = mapTree.find(iId);
        if ( iteMap == mapTree.end())
        {
            return false;
        }

        const TREE_ITEM* pItem;
        pItem = &iteMap->second;

        pListTree->push_back(*pItem);


        int iNext;
        iNext = pItem->iChild;

        std::deque<int> stackDataItem;

        while(iNext != -1)
        {
            pItem = _GetItem(iNext);

            if (pItem)
            {
                pListTree->push_back(*pItem);
				iNext = pItem->iChild;
			}
			else
			{
				break;
			}

            if (iNext != -1)
            {
                stackDataItem.push_front(pItem->iId);
                continue;
            }

            iNext = pItem->iNext;
            if (iNext != -1)
            {
                continue;
            }

            do 
            {
                if (stackDataItem.empty())
                {
                    break;
                }

                iNext = stackDataItem[0];
                stackDataItem.pop_front();

                iNext = GetNextId(iNext);

            } while (iNext == -1);
        }

        TREE_ITEM* pItemHead;
        pItemHead = &pListTree->at(0);
        pItemHead->iParent = -1;
        pItemHead->iNext   = -1;
        pItemHead->iBefore = -1;
        pItemHead->iParent = -1;

        _Renumber( pListTree );

        return true;
    }


    /**
     * @brief   番号振り直し
     * @param   [in out]  pListTree
     * @return  true 成功
     * @Note    
     */
    bool _Renumber( std::vector<TREE_ITEM>* pListTree )const
    {
        std::map<int, int> mapRenumber;

        int iCnt = 1;

        mapRenumber[-1] = -1;
        foreach(TREE_ITEM& item,  *pListTree)
        {
            mapRenumber[item.iId] = iCnt++;
        }


        foreach(TREE_ITEM& item,  *pListTree)
        {
            item.iId = mapRenumber[item.iId];
            item.iChild = mapRenumber[item.iChild];
            item.iNext = mapRenumber[item.iNext];
            item.iParent = mapRenumber[item.iParent];
            item.iBefore = mapRenumber[item.iBefore];

        }
        return true;
    }

    /**
     * @brief   データリスト追加.
     * @param   [in] iId         データID
     * @param   [in] pListTree   追加データ
     * @return  true 成功
     * @Note    iId == -1 の時はRoot直下に挿入
     *          それ以外は指定したIDの次に挿入する
     */
    bool InsertItemWithChild(int iId, 
                             std::vector< TREE_ITEM >& listTree, 
                             bool bChild)
    {

        TREE_ITEM* pItemBefore;

        if (iId != -1)
        {
            pItemBefore = _GetItem(iId);
        }
        else
        {
            pItemBefore = _GetItem(GetRootId());
        }

        if (!pItemBefore)
        {
            return false;
        }

        if(listTree.size() == 0)
        {
            return false;
        }

        int iMaxId = -1;

        //std::map<int, TREE_ITEM>::_Pairib pib;


        foreach(TREE_ITEM& item,  listTree)
        {
            item.iId    += m_iId;

            if (item.iChild != -1)
            {
                item.iChild += m_iId;
            }

            if (item.iNext != -1)
            {
                item.iNext += m_iId;
            }
            
            if (item.iParent != -1)
            {
                item.iParent += m_iId;
            }

            if (item.iBefore != -1)
            {
                item.iBefore += m_iId;
            }

            auto pib = mapTree.insert(std::pair<int, TREE_ITEM>(item.iId ,item));
            if (!pib.second) 
            {
                //TODO:例外にするべきか
                m_iId++;
                return false;
            }

            if (item.iId > iMaxId)
            {
                iMaxId = item.iId;
            }
        }
        m_iId = iMaxId + 1;


        TREE_ITEM* pItemHead =  _GetItem(listTree[0].iId);

        if (bChild)
        {
            if (pItemBefore->iChild != -1)
            {
                TREE_ITEM* pChildNext = _GetItem(pItemBefore->iChild);

                STD_ASSERT(pChildNext);

                STD_ASSERT(pChildNext->iBefore == -1);
                pChildNext->iBefore = pItemHead->iId;
                
                STD_ASSERT(pItemHead->iNext == -1);
                pItemHead->iNext = pChildNext->iId;
            }
            pItemBefore->iChild = pItemHead->iId;
            pItemHead->iParent = pItemBefore->iId;
        }
        else
        {
            if (pItemBefore->iNext != -1)
            {
                TREE_ITEM* pOldNext = _GetItem(pItemBefore->iNext);
                STD_ASSERT(pOldNext);

                pOldNext->iBefore = pItemHead->iId;
                pItemHead->iNext  = pOldNext->iId;
            }

            pItemBefore->iNext = pItemHead->iId;
            pItemHead->iBefore = pItemBefore->iId;
        }
        return true;
    }


    /**
     * @brief   データ追加.
     * @param   [in] iId    データID
     * @param   [in] Data   追加データ
     * @return  追加データのID
     * @Note    iId == -1 の時はRoot直下に挿入
     *          それ以外は指定したIDの次に挿入する
     */
    int AddData(int iId, D& Data)
    {
        int iCurId;
        int iParentId = -1;
        TREE_ITEM item;
        TREE_ITEM* pItem = NULL;
        TREE_ITEM* pItemNext = NULL;

        item.data = Data;

        if (iId != -1)
        {
            pItem = _GetItem(iId);
            if (!pItem)
            {
                return -1;
            }
            iParentId = pItem->iParent;
        }

        iCurId = m_iId++;

        int iNext = -1;

        if (iId != -1)
        {
            if (pItem->iNext != -1)
            {
                pItemNext = _GetItem(pItem->iNext);
            }
        }
        else
        {
            int iRootId = GetRootId();
            if (iRootId != -1)
            {
                pItemNext = _GetItem(iRootId);
            }
        }

        if (pItemNext)
        {
            iNext = pItemNext->iId;
            if (pItem)
            {
                pItem->iNext = iCurId;
            }
            pItemNext->iBefore = iCurId;
        }



        item.iId      = iCurId;
        item.iChild   = -1;
        item.iNext    = iNext;
        item.iParent  = iParentId;
        item.iBefore  = iId;

        //std::map<int, TREE_ITEM>::_Pairib pib;
        auto pib = mapTree.insert(std::pair<int, TREE_ITEM>(iCurId ,item));

        if (!pib.second) 
        {
            return -1;
        }

        if (pItem)
        {
            pItem->iNext = iCurId;
        }

        if (m_pFunc)
        {
            m_pFunc(E_ADD, iCurId);
        }
        return iCurId;
    }

    /**
     * @brief   最終データ追加.
     * @param   [in] iId    データID
     * @param   [in] Data   追加データ
     * @return  追加データのID
     * @Note    iId == -1 の時はRoot直下に挿入
     *          それ以外は指定したIDの次に挿入する
     */
    int AddDataLast(int iId, D& Data)
    {
        if (iId == -1)
        {
            iId = GetRootId();
            if (iId == -1)
            {
                return AddData( iId, Data);
            }
        }

        int iIdNext = iId;
        std::map<int, TREE_ITEM>::iterator iteNext;
        do {
            iteNext = mapTree.find(iIdNext);
            if (iteNext != mapTree.end())
            {
                iId = iteNext->second.iId;
                iIdNext = iteNext->second.iNext;
            }
        }while(iteNext != mapTree.end());

        return AddData(iId, Data);
    }

    /**
     * @brief   子データ追加
     * @param   [in] iId    データID
     * @param   [in] Data   追加データ
     * @return  追加データのID
     * @Note   
     */
    int AddChild(int iId, D& Data)
    {
        TREE_ITEM* pItem =  _GetItem(iId);
        if (!pItem)
        {
            return -1;
        }

        int iCurId;
        TREE_ITEM item;
        item.data = Data;

        iCurId = m_iId++;
        int iNext = -1;
        if (pItem->iChild != -1)
        {
            TREE_ITEM* pChild =  _GetItem(pItem->iChild);
            pChild->iBefore = iCurId;
            iNext = pChild->iId;
        }

        item.iId      = iCurId;
        item.iChild   = -1;
        item.iNext    = iNext;
        item.iParent  = iId;
        item.iBefore  = -1;

        //std::map<int, TREE_ITEM>::_Pairib pib;
        auto pib = mapTree.insert(std::pair<int, TREE_ITEM>(iCurId ,item));

        if (!pib.second) 
        {
            return -1;
        }

        if (m_pFunc)
        {
            m_pFunc(E_ADD, iCurId);
        }

        pItem->iChild     = iCurId;

        return iCurId;
    }

    /**
     * @brief   移動
     * @param   [in] iSrcId    移動元ID
     * @param   [in] iDstId    移動先ID  -1の時はroot
     * @return  true 移動成功
     * @Note    移動元を移動先の次に移動する
     */
    bool Move(int iSrcId, int iDstId)
    {
        TREE_ITEM* pSrc;  
        pSrc =_GetItem(iSrcId);
        if (!pSrc)
        {
            return false;
        }

        if (iDstId == -1)
        {
            TREE_ITEM* pNext;  
            //root 
            pSrc->iBefore = -1;
            pSrc->iParent = -1;                
            pNext = _GetItem(GetRootId());

            if (pNext)
            {
                pNext->iBefore = iSrcId;
                pSrc->iNext = pNext->iId;
            }
            else
            {
                // 移動しているのにRootが存在しない
                // ことはないはず
                STD_ASSERT(pNext);
                pSrc->iNext = -1;
            }
            return true;
        }

        TREE_ITEM* pDst =  _GetItem(iDstId);
        if (!pDst)
        {
            return false;
        }


        //--------------------------------------------------
        TREE_ITEM* pBeforeOld;  
        TREE_ITEM* pNextOld;  
        TREE_ITEM* pParentOld;  

        pBeforeOld  = _GetItem(pSrc->iBefore);
        pNextOld    = _GetItem(pSrc->iNext);

        if (pBeforeOld)
        {
            if (pNextOld)
            {
                pBeforeOld->iNext = pNextOld->iId;
                pNextOld->iBefore = pBeforeOld->iId;
            }
            else
            {
                pBeforeOld->iNext = -1;
            }
        }
        else
        {
            if (pNextOld)
            {
                pNextOld->iBefore = -1;
            }
            else
            {
                return false;
            }
        }


        pParentOld = _GetItem(pSrc->iParent);
        if( pParentOld )
        {
            if (pParentOld->iChild == pSrc->iId)
            {
                if (pNextOld)
                {
                    pParentOld->iChild = pNextOld->iId;
                }
                else
                {
                    pParentOld->iChild = -1;
                }
            }
        }
        //-------------------------------------------------------------
 

        TREE_ITEM* pNext;  
        pNext    = _GetItem(pDst->iNext);

        if (pNext)
        {
            pNext->iBefore = iSrcId;
        }

        pSrc->iNext   = pDst->iNext;
        pSrc->iParent = pDst->iParent;
        pDst->iNext   = iSrcId;
        pSrc->iBefore = iDstId;


        return true;
    }

    /**
     * @brief   子アイテムへ移動
     * @param   [in] iSrcId    移動元ID
     * @param   [in] iDstId    移動先ID
     * @return  true 移動成功
     * @Note    移動先の子アイテムとして登録する
     *          すでに子アイテムがある場合は
     *          子アイテムの先頭に登録する
     */
    bool MoveChild(int iSrcId, int iDstId)
    {
        TREE_ITEM* pSrc;  
        pSrc =_GetItem(iSrcId);
        if (!pSrc)
        {
            return false;
        }

        if (iDstId == -1)
        {
            return false;
        }

        TREE_ITEM* pDst =  _GetItem(iDstId);
        if (!pDst)
        {
            return false;
        }

        TREE_ITEM* pBeforeOld;  
        TREE_ITEM* pNextOld;  
        TREE_ITEM* pParentOld;  


        pBeforeOld  = _GetItem(pSrc->iBefore);
        pNextOld    = _GetItem(pSrc->iNext);


        if (pBeforeOld)
        {
            if (pNextOld)
            {
                pBeforeOld->iNext = pNextOld->iId;
                pNextOld->iBefore = pBeforeOld->iId;
            }
            else
            {
                pBeforeOld->iNext = -1;
            }
        }
        else
        {
            if (pNextOld)
            {
                pNextOld->iBefore = -1;
            }
            else
            {
                return false;
            }
        }

        pParentOld = _GetItem(pSrc->iParent);
        if( pParentOld )
        {
            if (pParentOld->iChild == pSrc->iId)
            {
                if (pNextOld)
                {
                    pParentOld->iChild = pNextOld->iId;
                }
                else
                {
                    pParentOld->iChild = -1;
                }
            }
        }


        pSrc->iBefore = -1;
        pSrc->iNext   = pDst->iChild;
        pSrc->iParent = pDst->iId;
        pDst->iChild  = pSrc->iId;

        TREE_ITEM* pChildDst;
        pChildDst = _GetItem(pSrc->iNext);
        if (pChildDst)
        {
            pChildDst->iBefore = pSrc->iId;
        }

        return true;
    }

    /**
     * @brief   削除
     * @param   [in] iId    データID
     * @return  なし
     * @Note   
     */
    void Delete(int iId)
    {
        std::vector<int> lstDel; 
        std::map<int, TREE_ITEM>::iterator iteMap;
        iteMap = mapTree.find(iId);
        if ( iteMap == mapTree.end())
        {
            return;
        }

        DeleteChild(iId);

        std::map<int, TREE_ITEM>::iterator iteBefore;
        std::map<int, TREE_ITEM>::iterator iteParent;
        std::map<int, TREE_ITEM>::iterator iteNext;

        iteBefore = mapTree.find(iteMap->second.iBefore);
        if ( iteBefore != mapTree.end())
        {
            iteBefore->second.iNext = iteMap->second.iNext;
        }

        iteNext = mapTree.find(iteMap->second.iNext);
        if ( iteNext != mapTree.end())
        {
            iteNext->second.iBefore = iteMap->second.iBefore;
        }

        iteParent = mapTree.find(iteMap->second.iParent);
        if ( iteParent != mapTree.end())
        {
            if (iteParent->second.iChild == iId)
            {
                iteParent->second.iChild = iteMap->second.iNext;
            }
        }


        int iEraseId = iteMap->second.iId;
        mapTree.erase(iteMap);

        if (m_pFunc)
        {
            m_pFunc(E_DEL, iEraseId);
        }

    }

    /**
     * @brief   子アイテム削除
     * @param   [in] iDataId    データID
     * @return  なし
     * @Note    自分自身は残す
     */
    void DeleteChild(int iDataId)
    {
        TREE_ITEM* pItem = _GetItem(iDataId);

        if (!pItem)
        {
            return;
        }

        std::map<int, TREE_ITEM>::const_iterator iteChild;

        iteChild = mapTree.find(pItem->iChild);
        if ( iteChild == mapTree.end())
        {
            return;
        }

        DeleteNextAll(pItem->iChild);


        int iEraseId = iteChild->second.iId;
        mapTree.erase(iteChild);
        pItem->iChild = -1;
        if (m_pFunc)
        {
            m_pFunc(E_DEL, iEraseId);
        }
    }

    /**
     * @brief   自アイテム以下削除
     * @param   [in] iId    データID
     * @return  なし
     * @Note    自分自身は残す
     */
    void DeleteNextAll(int iDataId)
    {
        TREE_ITEM* pItem = _GetItem(iDataId);

        if (!pItem)
        {
            return;
        }

        std::map<int, TREE_ITEM>::const_iterator iteNext;

        iteNext = mapTree.find(pItem->iNext);
        if ( iteNext == mapTree.end())
        {
            return;
        }

        DeleteChild(pItem->iNext);
        DeleteNextAll(pItem->iNext);

        int iEraseId = iteNext->second.iId;
        mapTree.erase(iteNext);
        if (m_pFunc)
        {
            m_pFunc(E_DEL, iEraseId);
        }
    }


    //クリア
    void Clear()
    {
        m_iId = 1;
        mapTree.clear();

        if (m_pFunc)
        {
            m_pFunc(E_CLR, -1);
        }

    }

    // 同一判定（テスト用）
    // 
    bool IsEqual(TREE_DATA* pTreeB)
    {
        if(!pTreeB)
        {
            return false;
        }

        int iRootA = GetRootId();
        int iRootB = pTreeB->GetRootId();

        return _IsEqual(iRootA, iRootB, pTreeB);
    }



protected:
    
    // 同一判定（テスト用）
    // 
    bool _IsEqual(int iA, int iB, TREE_DATA* pTreeB)
    {
        D* pA = GetItem(iA);
        D* pB = pTreeB->GetItem(iB);

        if(pA == NULL)
        {
            if (pB == NULL)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        if (pB == NULL)
        {
            return false;
        }

        int iChildA = GetChildId(iA);
        int iChildB = pTreeB->GetChildId(iB);
        if (iChildA != -1)
        { 
            if (!_IsEqual(iChildA, iChildB, pTreeB))
            {
                return false;
            }
        }

        int iNextA = GetNextId(iA);
        int iNextB = pTreeB->GetNextId(iB);
        if (iNextA != -1)
        {
            if (!_IsEqual(iNextA, iNextB, pTreeB))
            {
                return false;
            }
        }

        if(*(pA) != *(pB))
        {
            return false;
        }
        DB_PRINT(_T("ID[%d] == iD[%d] \n"), iA, iB);

        return true;
    }

    TREE_ITEM* _GetItem(int iId)
     {
        std::map<int, TREE_ITEM>::iterator ite;

        ite = mapTree.find(iId);
        if ( ite == mapTree.end())
        {
            return NULL;
        }
        return &ite->second;
    }

    const TREE_ITEM* _GetItem(int iId) const
     {
        std::map<int, TREE_ITEM>::const_iterator ite;

        ite = mapTree.find(iId);
        if ( ite == mapTree.end())
        {
            return NULL;
        }
        return &ite->second;
    }




    void _SetList(std::vector <TREE_ITEM>& lstItem)
    {
        //保存用にvectorに変換すると同時にIdを振り直す

        TREE_ITEM* pItem = NULL;
        TREE_ITEM* pItemNext = NULL;
        int iId;
        int iSetId = 1;
                
        iId =  GetRootId();
        
        std::deque<size_t> stackChild;
        std::vector<TREE_ITEM>::iterator ite;
        lstItem.clear();

        TREE_ITEM Item;
        do {
            pItem = _GetItem(iId);
    
            if (!pItem)
            {
                break;
            }

            Item = *pItem;
            Item.iId = iSetId++;

            lstItem.push_back(Item);
            ite = lstItem.end();
            ite--;

            if(Item.iChild != -1)
            {
                pItemNext = _GetItem(Item.iChild);
                if (pItemNext)
                {
                    stackChild.push_front(lstItem.size() - 1);
                    ite->iChild = iSetId;
                    iId = pItemNext->iId ;
                    continue;
                }
            }

            if(Item.iNext != -1)
            {
                pItemNext = _GetItem(Item.iNext);
                if (pItemNext)
                {
                    ite->iNext = iSetId;
                    iId = pItemNext->iId ;
                    continue;
                }
            }
 
            if ((pItem->iChild == -1) &&
                (pItem->iNext == -1))
            {
                iId = -1;
                while(!stackChild.empty())
                {
                    iId = static_cast<int>(stackChild[0]);
                    stackChild.pop_front();
                    if (lstItem[iId].iNext != -1)
                    {
                        pItemNext = _GetItem(lstItem[iId].iNext);
                        lstItem[iId].iNext = iSetId;
                        iId = pItemNext->iId ;
                        break;
                    }
                }
            }
        }while (iId != -1);
    }

    void _ExpansionList(std::vector <TREE_ITEM>& lstItem)
    {
        Clear();
        foreach(TREE_ITEM& item, lstItem)
        {
            mapTree.insert(std::pair<int, TREE_ITEM>(item.iId ,item));
            if ( item.iId > m_iId )
            {
                m_iId = item.iId;
            }
        }

        if (lstItem.size() != 0)
        {
            m_iId++;
        }


        TREE_ITEM* pItem;
        std::map<int, TREE_ITEM>::iterator iteMap;
        for(iteMap  = mapTree.begin();
            iteMap != mapTree.end();
            iteMap++)
        {
            if(iteMap->second.iChild != -1)
            {

                 pItem = _GetItem(iteMap->second.iChild);
                 if (pItem)
                 {
                     pItem->iParent = iteMap->second.iId;
                 }
            }

            if(iteMap->second.iNext != -1)
            {
                 pItem = _GetItem(iteMap->second.iNext);
                 if (pItem)
                 {
                     pItem->iBefore = iteMap->second.iId;
                     pItem->iParent = iteMap->second.iParent;
                 }
            }
        }
    }

public:
    std::map<int, TREE_ITEM>  mapTree;

protected:
    int m_iId;
    boost::function<void (E_TREE_ACTION, int) > m_pFunc;


private:
    //==============================
    // ファイル保存
    //==============================
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        std::vector <TREE_ITEM> lstItem;

        if (!Archive::is_loading::value)
        {
            _SetList(lstItem);

            if (m_pFunc)
            {
                m_pFunc(E_SAVE, -1);
            }

        }

        SERIALIZATION_INIT
            SERIALIZATION_BOTH("Tree"  , lstItem);
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )

        if (Archive::is_loading::value)
        {
            if (m_pFunc)
            {
                m_pFunc(E_LOAD, -1);
            }
            _ExpansionList(lstItem);
        }
    }
};

//----------------------------------------------
// TreeCtrl関連
//----------------------------------------------

/**
 * @brief   CTreeCtrlにTREE_DATAを設定する
 * @param   [out] pTreeCtrl
 * @param   [in]  pTreeData
 * @return  なし
 * @Note    
 */
template<class D>
void TREE_DATA_SetData(CTreeCtrl* pTreeCtrl, const TREE_DATA<D>* pTreeData)
{
    HTREEITEM hItemNext = NULL;
    HTREEITEM hItemCur  = NULL;

    int iTreeCur;

    std::deque<HTREEITEM>   stackWindowItem;
    std::deque<int>         stackDataItem;

    int iImage;
    //int iImageSel;
    StdString strItem;
    unsigned int uiState;
    pTreeCtrl->DeleteAllItems();

    hItemCur = pTreeCtrl->GetRootItem();
    iTreeCur = pTreeData->GetRootId();

    unsigned int uiMask = TVIF_TEXT | TVIF_IMAGE| 
                          TVIF_SELECTEDIMAGE |  TVIF_PARAM | 
                          TVIF_STATE | TVIF_HANDLE;

    unsigned int uiStateMask = TVIS_EXPANDED;


    int iTreeNext    = -1;
    while(iTreeCur != -1)
    {
        strItem   = pTreeData->GetName(iTreeCur);
        iImage    = pTreeData->GetType(iTreeCur);
        uiState = (pTreeData->IsExpand(iTreeCur)? TVIS_EXPANDED : 0);

        //hItemNext = pTreeCtrl->InsertItem(strItem.c_str(),
        //                                 iImage, iImage,
        //                                 hItemCur,
        //                                 TVI_LAST);

        hItemNext = pTreeCtrl->InsertItem(
                                           uiMask,                 //UINT nMask,
                                           strItem.c_str(),        //LPCTSTR lpszItem,
                                           iImage,                 //int nImage,
                                           iImage,                 //int nSelectedImage,
                                           uiState,                //UINT nState,
                                           uiStateMask,            //UINT nStateMask,
                                           iTreeCur,               //LPARAM lParam,
                                           hItemCur,               //HTREEITEM hParent,
                                           TVI_LAST                //HTREEITEM hInsertAfter 
      );

        iTreeNext = pTreeData->GetChildId(iTreeCur);

        if (iTreeNext != -1)
        {
            stackWindowItem.push_front(hItemCur);
            stackDataItem.push_front(iTreeCur);
            hItemCur = hItemNext;
            iTreeCur  = iTreeNext;
            continue;
        }   

        iTreeNext = pTreeData->GetNextId(iTreeCur);
        if (iTreeNext != -1)
        {
            iTreeCur  = iTreeNext;
            continue;
        }

        if (stackDataItem.empty())
        {
            iTreeCur = -1;
            continue;
        }

        do
        {
            if (stackDataItem.empty())
            {
                break;
            }

            hItemCur = stackWindowItem[0];
            stackWindowItem.pop_front();

            iTreeCur = stackDataItem[0];
            stackDataItem.pop_front();
            iTreeNext = pTreeData->GetNextId(iTreeCur);
            iTreeCur = iTreeNext;
        } while (iTreeCur == -1);
    }
}

/**
 * @brief   CTreeCtrlにTREE_DATAを設定する
 * @param   [out] pTreeData
 * @param   [in]  pTreeCtrl
 * @return  なし
 * @Note    アイテムデータの設定ができないのでテスト以外の
 *          用途はない
 */
template<class D>
void TREE_DATA_GetData(TREE_DATA<D>* pTreeData, CTreeCtrl* pTreeCtrl)
{
    HTREEITEM hItemNext = NULL;
    HTREEITEM hItemCur  = NULL;

    int iTreeNext    = -1;
    int iTreeCur;

    std::deque<HTREEITEM>   stackWindowItem;
    std::deque<int>         stackDataItem;

    bool bChild = false;
    bool bExpand = false;
    StdChar strItem[1024];

    pTreeData->Clear();

    D objData;

    hItemCur = pTreeCtrl->GetRootItem();
    iTreeCur = pTreeData->GetRootId();


    TVITEM tvItem;

    memset(&tvItem, 0, sizeof(tvItem));
    tvItem.mask = TVIF_TEXT | TVIF_IMAGE| 
                  TVIF_SELECTEDIMAGE |  TVIF_CHILDREN | 
                  TVIF_STATE | TVIF_HANDLE;
    tvItem.cchTextMax = 1024;
    tvItem.pszText = &strItem[0];
    tvItem.stateMask = TVIS_EXPANDED;


    while(hItemCur != 0)
    {
        tvItem.hItem = hItemCur;

        //pWndTree->GetItemImage(hItemCur, iImage, iImageSel);
        pTreeCtrl->GetItem(&tvItem);

        //bChild  = (tvItem.cChildren != 0);
        bExpand = ((tvItem.state & TVIS_EXPANDED) != 0);
        

        if (bChild)
        {
            iTreeNext = pTreeData->AddChild(iTreeCur, objData);

        }
        else
        {
            iTreeNext = pTreeData->AddData(iTreeCur, objData);
        }
        pTreeData->SetName(iTreeNext, tvItem.pszText);
        pTreeData->SetType(iTreeNext, tvItem.iImage);
        pTreeData->SetExpand(iTreeNext, bExpand);

        pTreeCtrl->SetItemData(hItemCur, iTreeNext);

        hItemNext = pTreeCtrl->GetChildItem(hItemCur);

        if (hItemNext)
        {
            stackWindowItem.push_front(hItemCur);
            stackDataItem.push_front(iTreeNext);
            hItemCur = hItemNext;
            iTreeCur  = iTreeNext;
            bChild = true;
            continue;
        }   

        bChild = false;
        hItemNext = pTreeCtrl->GetNextItem(hItemCur, TVGN_NEXT);
        if (hItemNext)
        {
            hItemCur = hItemNext;
            iTreeCur  = iTreeNext;
            continue;
        }

        if (stackWindowItem.empty())
        {
            hItemCur = 0;
            continue;
        }

        do
        {
            if (stackWindowItem.empty())
            {
                break;
            }
            hItemCur = stackWindowItem[0];
            stackWindowItem.pop_front();
            hItemNext = pTreeCtrl->GetNextItem(hItemCur, TVGN_NEXT);
            hItemCur  = hItemNext;

            iTreeNext = stackDataItem[0];
            stackDataItem.pop_front();
            iTreeCur = iTreeNext;

        }while(hItemNext == NULL);
    }
}

template<class D>
bool TREE_DATA_Valid(TREE_DATA<D>* pTreeData, 
                     CTreeCtrl* pTreeCtrl,
                     bool bChcekExpand = false)
{
    HTREEITEM hItemNext = NULL;
    HTREEITEM hItemCur  = NULL;

    int iTreeNext    = -1;
    int iTreeCur;

    std::deque<HTREEITEM>   stackWindowItem;
    std::deque<int>         stackDataItem;

    int iImage;
    int iImageSel;
    bool bChild = false;
    bool bExpand = false;
    StdChar strItem[1024];

    pTreeData->Clear();

    D objData;

    hItemCur = pTreeCtrl->GetRootItem();
    iTreeCur = pTreeData->GetRootId();


    TVITEM tvItem;

    memset(&tvItem, 0, sizeof(tvItem));
    tvItem.mask = TVIF_TEXT | TVIF_IMAGE| 
                  TVIF_SELECTEDIMAGE |  TVIF_CHILDREN | 
                  TVIF_PARAM |
                  TVIF_STATE | TVIF_HANDLE;
    tvItem.cchTextMax = 1024;
    tvItem.pszText = &strItem[0];
    tvItem.stateMask = TVIS_EXPANDED;


    int iItemCnt = 0;

    while(hItemCur != 0)
    {
        tvItem.hItem = hItemCur;
        pTreeCtrl->GetItem(&tvItem);
        bExpand = (tvItem.state & TVIS_EXPANDED);
        
        iTreeCur = tvItem.lParam;
        iItemCnt++;

        if (pTreeData->GetName(iTreeCur) != tvItem.pszText){ return false;}
        if (pTreeData->GetType(iTreeCur) != tvItem.iImage){ return false;}


        if (bChcekExpand)
        {
            if (pTreeData->IsExpand(iTreeCur) != bExpand)
            {
                return false;
            }
        }

        hItemNext = pTreeCtrl->GetChildItem(hItemCur);
        if (hItemNext)
        {
            stackWindowItem.push_front(hItemCur);
            stackDataItem.push_front(iTreeNext);
            hItemCur = hItemNext;
            iTreeCur  = iTreeNext;
            bChild = true;

            continue;
        }   
 
        bChild = false;
        hItemNext = pTreeCtrl->GetNextItem(hItemCur, TVGN_NEXT);
        if (hItemNext)
        {
            hItemCur = hItemNext;
            iTreeCur  = iTreeNext;
            continue;

        }

        if (stackWindowItem.empty())
        {
            hItemCur = 0;
            continue;
        }

        do
        {
            if (stackWindowItem.empty())
            {
                break;
            }

            hItemCur = stackWindowItem[0];
            stackWindowItem.pop_front();
            hItemNext = pTreeCtrl->GetNextItem(hItemCur, TVGN_NEXT);
            hItemCur  = hItemNext;

            iTreeNext = stackDataItem[0];
            stackDataItem.pop_front();
            iTreeCur = iTreeNext;
        }while (hItemNext == NULL)
    }

    if (iItemCnt != GetItemCount())
    {
        return false;
    }
    return true;
}


template<class D>
void TREE_DATA_GetExpand(TREE_DATA<D>* pTreeData, CTreeCtrl* pTreeCtrl)
{
    HTREEITEM hItemNext = NULL;
    HTREEITEM hItemCur  = NULL;

    int iTreeCur     = -1;

    std::deque<HTREEITEM>   stackWindowItem;

    bool bChild = false;
    bool bExpand = false;
    StdChar strItem[1024];


    hItemCur = pTreeCtrl->GetRootItem();


    TVITEM tvItem;

    memset(&tvItem, 0, sizeof(tvItem));
    tvItem.mask = TVIF_TEXT | TVIF_IMAGE| 
                  TVIF_SELECTEDIMAGE | 
                  TVIF_CHILDREN | 
                  TVIF_PARAM |
                  TVIF_STATE | TVIF_HANDLE;
    tvItem.cchTextMax = 1024;
    tvItem.pszText = &strItem[0];
    tvItem.stateMask = TVIS_EXPANDED;


    while(hItemCur != 0)
    {
        tvItem.hItem = hItemCur;

        pTreeCtrl->GetItem(&tvItem);
        iTreeCur = static_cast<int>(tvItem.lParam);
        bExpand = ((tvItem.state & TVIS_EXPANDED) != 0);
        pTreeData->SetExpand(iTreeCur, bExpand);


        hItemNext = pTreeCtrl->GetChildItem(hItemCur);

        if (hItemNext)
        {
            stackWindowItem.push_front(hItemCur);
            hItemCur = hItemNext;
            bChild = true;
            continue;
        }   

        bChild = false;
        hItemNext = pTreeCtrl->GetNextItem(hItemCur, TVGN_NEXT);
        if (hItemNext)
        {
            hItemCur = hItemNext;
            continue;
        }

        if (stackWindowItem.empty())
        {
            hItemCur = 0;
            continue;
        }

        do
        {
            if (stackWindowItem.empty())
            {
                break;
            }
            hItemCur = stackWindowItem[0];
            stackWindowItem.pop_front();
            hItemNext = pTreeCtrl->GetNextItem(hItemCur, TVGN_NEXT);
            hItemCur  = hItemNext;
        }while(hItemNext == NULL);
    }
}


/**
 * @brief   子アイテム追加
 * @param   [in]  strName     追加データ表示名
 * @param   [in]  iType       追加データ種別
 * @param   [in]  bExpand     追加データ展開状態
 * @param   [in]  data        追加データ
 * @param   [in]  hItemParent 追加データの親
 * @param   [in out]  pTreeData
 * @param   [in out]  pTreeCtrl
 * @return  
 * @Note    
 */
template<class D>
HTREEITEM TREE_DATA_AddChild( StdString& strName,
               int        iType,
               bool       bExpand,
               D&         data,
               HTREEITEM  hItemParent,
               TREE_DATA<D>* pTreeData, 
               CTreeCtrl* pTreeCtrl)
{
    HTREEITEM  hItem = NULL;

    if (!hItemParent)
    {
        return hItem;
    }

    int iItemId = -1;
    int iFolderId;
    UINT nMask = TVIF_TEXT | TVIF_IMAGE| 
                    TVIF_SELECTEDIMAGE| 
                    TVIF_PARAM |
                    TVIF_STATE | TVIF_HANDLE;
    UINT nStateMask = TVIS_EXPANDED;
    UINT nState = (bExpand ? TVIS_EXPANDED : 0);

    iItemId = pTreeCtrl->GetItemData(hItemParent);
    iFolderId = pTreeData->AddChild(iItemId, data);
    pTreeData->SetTreeValues(strName, iType,  iFolderId);

    hItem = pTreeCtrl->InsertItem(
        nMask, 
        strName.c_str(), 
        iType, 
        iType, 
        nState,
        nStateMask, 
        iFolderId,
        hItemParent, 
        TVI_FIRST); 


    return hItem;
}

/**
 * @brief   アイテム追加
 * @param   [in]  strName     追加データ表示名
 * @param   [in]  iType       追加データ種別
 * @param   [in]  data        追加データ
 * @param   [in]  hItem       追加データ位置のハンドル
 * @param   [in out]  pTreeData
 * @param   [in out]  pTreeCtrl
 * @return  追加データのハンドル
 * @Note    
 */
template<class D>
HTREEITEM TREE_DATA_AddItem( StdString& strName,
               int        iType,
               D&         data,
               HTREEITEM  hItem,
               TREE_DATA<D>* pTreeData, 
               CTreeCtrl* pTreeCtrl)
{
    HTREEITEM  hItemRet = NULL;

    if (!hItem)
    {
        return hItemRet;
    }

    int iItemId;
    int iFolderId;
    UINT nMask = TVIF_TEXT | TVIF_IMAGE| 
                    TVIF_SELECTEDIMAGE| 
                    TVIF_PARAM |
                    TVIF_STATE | TVIF_HANDLE;
    UINT nStateMask = 0;//TVIS_EXPANDED;
    UINT nState = 0;//(bExpand ? TVIS_EXPANDED : 0);

    iItemId = pTreeCtrl->GetItemData(hItem);
    iFolderId = pTreeData->AddData(iItemId, data);
    pTreeData->SetTreeValues(strName, iType,  iFolderId);

    HTREEITEM hParent = pTreeCtrl->GetParentItem(hItem);

    hItemRet = pTreeCtrl->InsertItem(
        nMask, 
        strName.c_str(), 
        iType, 
        iType, 
        nState,
        nStateMask, 
        iFolderId,
        hParent, 
        hItem); 

    return hItemRet;
}

/**
 * @brief   アイテム削除
 * @param   [in]  strName     追加データ表示名
 * @param   [in out]  pTreeData
 * @param   [in out]  pTreeCtrl
 * @return  追加データのハンドル
 * @Note    
 */
template<class D>
HTREEITEM TREE_DATA_DeleteItem (HTREEITEM  hItem,
                                bool bDeleteChild,
                                TREE_DATA<D>* pTreeData, 
                                CTreeCtrl* pTreeCtrl)
{
    return 0;
}

#endif //__TREE_DATA_H__

