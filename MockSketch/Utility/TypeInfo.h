/**
 * @brief			TypeInfoインクルードファイル
 * @file			TypeInfo.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	LOKIを参照   
 *			
 *
 * $
 * $
 * 
 */


#ifndef  _TYPE_INFO_H__
#define  _TYPE_INFO_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/

/*---------------------------------------------------*/
/*  classes                                          */
/*---------------------------------------------------*/


/**
 * @class   CTypeInfo
 * @brief
 */
class CTypeInfo
{
public:

    CTypeInfo();
    CTypeInfo(const std::type_info&);
    CTypeInfo& operator = (const CTypeInfo&);

    const std::type_info& Get() const;
    bool Before(const CTypeInfo&) const;
    const char* Name() const;

private:
    const std::type_info* m_pInfo;
};


inline CTypeInfo::CTypeInfo()
{
    class Nil {};
    m_pInfo = &typeid(Nil);
    STD_ASSERT(m_pInfo);
}
    
inline CTypeInfo::CTypeInfo(const std::type_info& ti)
: m_pInfo(&ti)
{ 
    STD_ASSERT(m_pInfo);
}
    
inline bool CTypeInfo::Before(const CTypeInfo& rhs) const
{
    STD_ASSERT(m_pInfo);
    return m_pInfo->before(*rhs.m_pInfo) != 0;
}

inline const std::type_info& CTypeInfo::Get() const
{
    STD_ASSERT(m_pInfo);
    return *m_pInfo;
}
    
inline const char* CTypeInfo::Name() const
{
    STD_ASSERT(m_pInfo);
    return m_pInfo->name();
}

// Comparison operators
    
inline bool operator==(const CTypeInfo& lhs, const CTypeInfo& rhs)
// type_info::operator== return type is int in some VC libraries
{ 
    return (lhs.Get() == rhs.Get()) != 0; 
}

inline bool operator<(const CTypeInfo& lhs, const CTypeInfo& rhs)
{ 
    return lhs.Before(rhs); 
}

inline bool operator!=(const CTypeInfo& lhs, const CTypeInfo& rhs)
{ 
    return !(lhs == rhs); 
}    
    
inline bool operator>(const CTypeInfo& lhs, const CTypeInfo& rhs)
{ 
    return rhs < lhs; 
}
    
inline bool operator<=(const CTypeInfo& lhs, const CTypeInfo& rhs)
{ 
    return !(lhs > rhs); 
}
     
inline bool operator>=(const CTypeInfo& lhs, const CTypeInfo& rhs)
{ 
    return !(lhs < rhs); 
}



#endif  _TYPE_INFO_H__
