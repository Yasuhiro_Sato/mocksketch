/**
 * @brief			CViewTreeヘッダーファイル
 * @file			CViewTree.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./ViewTree.h"
#include "Utility/CUtility.h"
#include "Utility/DropUtil.h"
#include "Utility/DropTarget.h"
#include "System/CSystem.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

BEGIN_MESSAGE_MAP(CViewTree, CTreeCtrl)
    ON_WM_LBUTTONUP()
    ON_WM_MOUSEMOVE()
    ON_WM_PAINT()
    //ドロップ処理
    ON_MESSAGE(WM_DROP_ENTER,  OnDropEnter)
    ON_MESSAGE(WM_DROP_END,    OnDropEnd)
    ON_MESSAGE(WM_DROP_OVER,   OnDropOver)
    ON_MESSAGE(WM_DROP_LEAVE,  OnDropLeave)


END_MESSAGE_MAP()


/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CViewTree::CViewTree():
m_pFunc (NULL),
m_pVoid (NULL),
m_hInsert    (0),
m_eInsertPos (E_NONE)

{
}

/**
 *  @brief  デストラクター.
 *  @param  なし
 *  @retval なし     
 *  @note
 */
CViewTree::~CViewTree()
{
}


/**
 *  @brief  イベント発生通知.
 *  @param  [in]  wParam 
 *  @param  [in]  lParam 
 *  @param  [out] pResult 
 *  @retval なし     
 *  @note
 */
BOOL CViewTree::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult)
{
	BOOL bRes = CTreeCtrl::OnNotify(wParam, lParam, pResult);

	NMHDR* pNMHDR = (NMHDR*)lParam;
	ASSERT(pNMHDR != NULL);

	if (pNMHDR && pNMHDR->code == TTN_SHOW && GetToolTips() != NULL)
	{
		GetToolTips()->SetWindowPos(&wndTop, -1, -1, -1, -1, SWP_NOMOVE | SWP_NOACTIVATE | SWP_NOSIZE);
	}

    CWnd* pWnd = GetParent();
    if (pWnd != NULL)
    {
        pWnd->SendMessage(WM_NOTIFY, wParam, lParam);
    }
	return bRes;
}

/**
 *  @brief  規定ウインドウプロシジャ.
 *  @param  [in] message メッセージの識別子
 *  @param  [in] wParam  メッセージ最初のパラメータ
 *  @param  [in] lParam  メッセージ２番目のパラメータ
 *  @retval メッセージ処理結果
 *  @note
 */
LRESULT CViewTree::DefWindowProc( UINT message, WPARAM wParam,  LPARAM lParam )
{
    return CTreeCtrl::DefWindowProc( message, wParam,  lParam );
}

/**
 *  @brief  描画コールバック登録.
 *  @param  [in] func  登録関数
 *  @param  [in] pVoid 登録インスタンス
 *  @retval true 登録成功
 *  @note
 */
bool CViewTree::SetDrawCallback(boost::function<void (CDC* , HTREEITEM, void*) > func, 
                void* pVoid)
{
    m_pFunc = func;
    m_pVoid = pVoid;
    return true;
}

/**
 * マウス移動
 * @param   [in]    nFlags   仮想キー         
 * @param   [in]    point    マウス座標         
 * @note	 フレームワークより呼び出し
 * @note	
 */
void CViewTree::OnMouseMove(UINT nFlags, CPoint point) 
{
    //CWn::OnMouseMove( nFlags, point);
}

/**
 * マウス左ボタン解放
 * @param   [in]    nFlags   仮想キー         
 * @param   [in]    point    マウス座標         
 * @note	 フレームワークより呼び出し
 * @note	
 */
void CViewTree::OnLButtonUp(UINT nFlags, CPoint point) 
{
    //CTreeCtrl::OnLButtpnUp( nFlags, point);
}

/**
 * @brief    描画処理
 * @param    なし
 * @retval   なし
 * @note	 フレームワークより
 */
void CViewTree::OnPaint()
{
    //http://www.alpha-net.ne.jp/users2/uk413/vc/VCT_TVcolor.html
    CPaintDC dc(this); // 描画のデバイス コンテキスト

    // デフォルトの描画処理
    // WM_PAINTメッセージでは、wParamに描画をおこなうデバイスコンテキストのハンドルを指定する
    CWnd::DefWindowProc( WM_PAINT, (WPARAM)dc.m_hDC, 0 );

    // ツリーコントロールのフォントを取得し、これを
    // デバイスコンテキストのフォントとして設定する
    LOGFONT lf;
    CFont f;
    GetFont()->GetLogFont(&lf);
    f.CreateFontIndirect(&lf);
    CFont* pOldFont = dc.SelectObject(&f);

    // すべてのアイテムについてテキストを描画しなおす
    HTREEITEM hItem = GetFirstVisibleItem();
    do
    {
        if ((m_eInsertPos ==  E_SEL) &&
             (hItem == m_hInsert))

        {
            //現状使用予定なし
            SetInsertMark( 0, TRUE);
            dc.SetBkColor((RGB(0,0,0)));
            dc.SetTextColor(RGB(255,255,255));
        }
        else if( hItem == GetDropHilightItem())
	    {
            dc.SetBkColor(DRAW_CONFIG->crDrop);
            dc.SetTextColor(DRAW_CONFIG->crText);
        }
        else  if (hItem == GetSelectedItem() && 
                    GetDropHilightItem() == NULL) 
        {
            dc.SetBkColor(DRAW_CONFIG->crSelcetList);
            dc.SetTextColor(DRAW_CONFIG->crText);
        }
        else
        {
            dc.SetBkColor(DRAW_CONFIG->crBack);
            dc.SetTextColor(DRAW_CONFIG->crText );
        }

       
        if (m_pFunc)
        {
            m_pFunc(&dc, hItem, m_pVoid);
        }
        else
        {
            // アイテムのテキストを取得し、描画する
            CString strText = GetItemText(hItem);
            CRect rect;
            GetItemRect(hItem, &rect, TRUE);
            dc.TextOut(rect.left + 2, rect.top + 1, strText);
        }


    } while(hItem = GetNextVisibleItem(hItem));

    switch (m_eInsertPos)
    {
    case E_NONE:
        SetInsertMark( 0, FALSE);
        break;

    case E_UP:
        SetInsertMark( m_hInsert, TRUE);
        break;

    case E_DOWN:
        SetInsertMark( m_hInsert, FALSE);
        break;

    case E_SEL:
        break;

    default:
        break;
    }

    // デバイスコンテキストのフォントを元に戻す
    dc.SelectObject(pOldFont);

}

/**
 * @brief   ドロップ完了通知
 * @param   [in] type   ドロップ種別 
 * @param   [in] lParam ドロップデータ (DROP_PARTS_DATA) 
 * @retval  
 * @note	
 */
LRESULT CViewTree::OnDropEnd(WPARAM type, LPARAM lParam)
{

    DROP_TARGET_DATA* pTargetData;
    pTargetData = reinterpret_cast<DROP_TARGET_DATA*>(lParam);

    if (CF_MOCK_PARTS == type)
    {
DB_PRINT(_T("CViewTree::OnDropEnd\n"));

    }

    return 0;
}

/**
 * @brief   ドロップ通過開始通知
 * @param   [in] type    ドロップ種別 
 * @param   [in] lParam  使用しない
 * @retval  
 * @note	
 */
LRESULT CViewTree::OnDropEnter(WPARAM type, LPARAM lParam)
{
    DROP_TARGET_DATA* pTargetData;
    pTargetData = reinterpret_cast<DROP_TARGET_DATA*>(lParam);

    if (CF_MOCK_PARTS == type)
    {
DB_PRINT(_T("CViewTree::OnDropEnter\n"));
    }
    return 0;
}


/**
 * @brief   ドロップ通過通知
 * @param   [in] type    ドロップ種別 
 * @param   [in] lParam  マウス位置
 * @retval  
 * @note	
 */
LRESULT CViewTree::OnDropOver(WPARAM type, LPARAM lParam)
{
    DROP_TARGET_DATA* pTargetData;
    pTargetData = reinterpret_cast<DROP_TARGET_DATA*>(lParam);


    if (CF_MOCK_PARTS == type)
    {
        CursorProc();
        SetInsertMarkProc(pTargetData->ptDrop);
        ScrollProc(pTargetData->ptDrop);
        return DROPEFFECT_COPY;
    }
    return DROPEFFECT_NONE;
}

/**
 * @brief   ドロップ通過完了
 * @param   [in] type    ドロップ種別 
 * @param   [in] lParam  使用しない
 * @retval  
 * @note	
 */
LRESULT CViewTree::OnDropLeave(WPARAM type, LPARAM lParam)
{
    if (CF_MOCK_PARTS == type)
    {
DB_PRINT(_T("CViewTree::OnDropLeave\n"));
    }

    SelectDropTarget(NULL);

    return 0;
}

//!< インサートマーク処理
/**
 * @brief   インサートマーク設定
 * @param   [in] hitem   インサートマーク設定アイテム
 * @param   [in] ePos    インサートマーク位置
 * @retval  
 * @note	
 */
void CViewTree::SetInsert(HTREEITEM hitem, CViewTree::E_INSERT_POS ePos)
{
    m_hInsert = hitem;
    m_eInsertPos = ePos;
}


/**
 * @brief   インサートマーク処理
 * @param   [in] point   カーソルの座標
 * @retval  
 * @note	
 */
void CViewTree::SetInsertMarkProc(CPoint point)
{

}

/**
 * @brief   スクロール処理
 * @param   [in] point   カーソルの座標
 * @retval  
 * @note	
 */
void CViewTree::ScrollProc(CPoint point)
{
	UINT uFlags;

	HitTest(point, &uFlags);

	//if (ms_lScroll == m_lScroll)
	{
		if(uFlags & TVHT_ABOVE)
		{
			//上にスクロール
			ScrollTree(TRUE);
		}
		else if(uFlags & TVHT_BELOW)
		{
			//下にスクロール
			ScrollTree(FALSE);
		}
	}
}

/**
 * @brief   マウスカーソル処理
 * @param   [in] point   カーソルの座標
 * @retval  
 * @note	
 */
void CViewTree::CursorProc()
{
    POINT  ptWinPos;
    POINT  ptCurPos;
    CRect  rcWin;

    if (!IsWindowVisible())
    {
        return;
    }

	//各ウィンドウの座標系に変換
	GetCursorPos(&ptCurPos);
    ptWinPos = ptCurPos;
    ::ScreenToClient(m_hWnd , &ptWinPos);
    ::GetClientRect(m_hWnd , rcWin);


    /*
	if((ms_hWnd == m_hWnd) || rcWin.PtInRect(ptWinPos))
	{
        
		//ウィンドウ内
		if(rcWin.PtInRect(ptWinPos) && m_bEnableDrop)
		{
			SetShowCursor(IDC_POINTER);
			ms_lScroll = m_lScroll;
			ms_bIsDrop = true;
		}
		//ウィンドウ外
		else
		{
			SetShowCursor(IDC_NODROP);
			SetInsertMark(0, false);
			ms_bIsDrop = false;
		}
	}
    */
}

/**
 * @brief   スクロール時の移動処理
 * @param   [in] bUp  true/false 上移動/下移動
 * @retval  
 * @note	
 */
void CViewTree::ScrollTree(BOOL bUp)
{
	HTREEITEM hFirst;
	HTREEITEM hNext;

	hFirst = GetFirstVisibleItem();
	if(hFirst == NULL)
	{
		return;
	}

	if(bUp)
	{
		hNext = GetPrevVisibleItem(hFirst);
	}
	else
	{
		hNext = GetNextVisibleItem(hFirst);
	}

	if(hNext)
	{
		//指定されたアイテムを先頭に表示
		SelectSetFirstVisible(hNext);
		UpdateWindow();
	}
}

//!< インデント数の取得
int CViewTree::GetIndentNum(HTREEITEM hItem)
{
    int iRet = 0;

    HTREEITEM hParent;

    do
    {
        hParent = GetParentItem(hItem);
        if (hParent)
        {
            iRet++;
            hItem = hParent;
        }
    }while(hParent);

    return iRet;
}

LRESULT CViewTree::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
    if (message == WM_NOTIFY)
    {
        CWnd* pWnd = GetParent();
        if (pWnd != NULL)
        {
            pWnd->SendMessage(WM_NOTIFY, wParam, lParam);
        }
    }

    else if ((message == WM_DROP_ENTER)||
             (message == WM_DROP_END)||
             (message == WM_DROP_OVER)||
             (message == WM_DROP_LEAVE))
    {
        CWnd* pWnd = GetParent();
        if (pWnd != NULL)
        {
            LRESULT lRet;

            lRet = pWnd->SendMessage(message, wParam, lParam);
            if( lRet != 0)
            {
                return lRet;
            }
        }
    }

    return CTreeCtrl::WindowProc(message, wParam, lParam);
}
