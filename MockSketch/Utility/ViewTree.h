
#pragma once

class CViewTree : public CTreeCtrl
{
public:
    enum E_INSERT_POS
    {
        E_NONE,
        E_UP,
        E_DOWN,
        E_SEL
    };
    
    // コンストラクション
public:
	CViewTree();

// オーバーライド
protected:
	virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);

// 実装
public:
	virtual ~CViewTree();
    //afx_msg void OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult);
    //virtual BOOL PreTranslateMessage(  MSG* pMsg );
    virtual LRESULT DefWindowProc( UINT message,  WPARAM wParam,  LPARAM lParam );

    bool SetDrawCallback(boost::function<void (CDC* , HTREEITEM, void*) > func, void* pVoid);

    void SetDrawCallback();

    //!< マウス左ボタン解放
    afx_msg void OnLButtonUp(UINT nFlags, CPoint point);

    //!< マウス移動
    afx_msg void OnMouseMove(UINT nFlags, CPoint point);

    //!<ドロップ開始
    afx_msg  LRESULT OnDropEnter(WPARAM type, LPARAM lParam);

    //!<ドロップ完了
    afx_msg LRESULT OnDropEnd(WPARAM type, LPARAM lParam);

    //!< ドロップ通過
    afx_msg LRESULT OnDropOver(WPARAM pMessage, LPARAM lParam);

    //!< ドロップ通過完了
    afx_msg LRESULT OnDropLeave(WPARAM pMessage, LPARAM lParam);

    //!< インサートマーク処理
    void SetInsert(HTREEITEM hitem, CViewTree::E_INSERT_POS ePos);

    //!< インサートマーク処理
    void SetInsertMarkProc(CPoint point);

    //!< スクロール処理
    void ScrollProc(CPoint point);

    //!< マウスカーソル処理
    void CursorProc();

    //!< スクロール時の移動処理
    void ScrollTree(BOOL bUp);

    //!< 描画
    afx_msg void OnPaint();

    //!< インデント数の取得
    int GetIndentNum(HTREEITEM hItem);

protected:
	DECLARE_MESSAGE_MAP()

    boost::function<void (CDC* , HTREEITEM, void*) > m_pFunc;

    void* m_pVoid;

    //!< 挿入位置アイテム
    HTREEITEM       m_hInsert;

    //!< 挿入位置
    E_INSERT_POS    m_eInsertPos;
    virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
};
