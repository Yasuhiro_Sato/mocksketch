/**
 * @brief			VirtualCheckCombo実装ファイル
 * @file			VirtualCheckCombo.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        チェックボックス付き仮想リストコントロール用エディットボックス
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "VirtualCheckCombo.h"
#include "VirtualCheckList.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

BEGIN_MESSAGE_MAP(CVirtualCheckCombo, CEdit)
    //{{AFX_MSG_MAP(CVirtualCheckCombo)
    ON_WM_KILLFOCUS()
    ON_WM_CHAR()
    ON_WM_CREATE()
    ON_WM_GETDLGCODE()
	ON_WM_KEYUP()
	ON_WM_KEYDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


///////////////////////////////////////////////////////////
//	Method    : CVirtualCheckCombo::CVirtualCheckCombo
//	Summary   : コンストラクター
//	Arguments : pCtrl     (i) 親リストコントロール
//              nRow      (i) 行
//              nCol      (i) 列
//              sInitText (i) 初期文字列
//	Return    : なし
//	Exception : なし
//	Note      :
///////////////////////////////////////////////////////////
CVirtualCheckCombo::CVirtualCheckCombo (CVirtualCheckList* pCtrl, int nRow, int nCol, 
                                        CString sInitText)
{
    pListCtrl = pCtrl;
    m_nRow  = nRow;
    m_nCol  = nCol;
    m_InitText = sInitText;
	m_isClosing = false;
}

///////////////////////////////////////////////////////////
//	Method    : CVirtualCheckCombo::~CVirtualCheckCombo
//	Summary   : デストラクター
//	Arguments : なし
//	Return    : なし
//	Exception : なし
//	Note      :
///////////////////////////////////////////////////////////
CVirtualCheckCombo::~CVirtualCheckCombo()
{
}



///////////////////////////////////////////////////////////
//	Method    : CVirtualCheckCombo::PreTranslateMessage
//	Summary   : メッセージフィルタ処理
//	Arguments : pMsg (i) ウインドウメッセージ
//	Return    : true/false
//	Exception : なし
//	Note      : 
///////////////////////////////////////////////////////////
BOOL CVirtualCheckCombo::PreTranslateMessage (MSG* pMsg) 
{
    if (pMsg->message == WM_KEYDOWN)
    {
	    if (pMsg->wParam == VK_RETURN )
        {
            Stop(false);
            return TRUE;
        }
    }
    return CComboBox::PreTranslateMessage (pMsg);
}

///////////////////////////////////////////////////////////
//	Method    : CVirtualCheckCombo::OnKillFocus
//	Summary   : フォーカス消失時処理
//	Arguments : pNewWnd (i) 新規フォーカスウインドウ
//	Return    : なし
//	Exception : なし
//	Note      : 
///////////////////////////////////////////////////////////
void CVirtualCheckCombo::OnKillFocus (CWnd* pNewWnd) 
{
    CComboBox::OnKillFocus(pNewWnd);

	//End edit?
	if(!m_isClosing)
	{
		Stop(false);
	}
}

/**
 * 生成時処理要求
 * @param [in]    lpCreateStruct	初期化パラメータへのポインタ	 	  
 * @return		
 * @note	 
 * @exception   なし
 */
int CVirtualCheckCombo::OnCreate (LPCREATESTRUCT lpCreateStruct) 
{
    if (CComboBox::OnCreate (lpCreateStruct) == -1)
		return -1;

    //親のフォントを引き継ぐ
    CFont* Font = GetParent()->GetFont();
    SetFont (Font);

    SetWindowText (m_InitText);
    SetFocus();

    std::vector<StdString> m_lstText; //コンボボックス

    //SetSel (0, -1);
    return 0;
}

///////////////////////////////////////////////////////////
//	Method    : CVirtualCheckCombo::OnGetDlgCode
//	Summary   : 方向キーおよび Tab キーによる入力
//	Arguments : なし
//	Return    : UINT
//	Exception : なし
//	Note      : 
///////////////////////////////////////////////////////////
UINT CVirtualCheckCombo::OnGetDlgCode() 
{
    return CComboBox::OnGetDlgCode() | DLGC_WANTARROWS;// | DLGC_WANTTAB;
}

/**
 *  @brief  編集中止
 *  @param  [in] cancel 中止の有無
 *  @retval なし 
 *  @note   
 */
void CVirtualCheckCombo::Stop(bool cancel)
{
    m_isClosing = true;

	CString Text;
	GetWindowText (Text);
    
	if(pListCtrl != NULL)
    {
        pListCtrl->m_pCombo = NULL;
    }
	DestroyWindow();
	pListCtrl->OnEndEdit(m_nRow, m_nCol, Text, cancel, 0);
	delete this;
}

/**
 *  @brief  リスト設定
 *  @param  [in] pLstText コンボボックスに設定するリスト
 *  @param  [in] curText  選択中のテキスト
 *  @retval なし 
 *  @note   
 */
void CVirtualCheckCombo::SetList(std::vector<StdString> lstText,
                                 StdString curText)
{
    ResetContent();
    std::vector<StdString>::iterator ite; 
    for (ite  = lstText.begin();
         ite != lstText.end(); ++ite )
    {
        AddString((*ite).c_str() );
    }
    SelText(curText);
}

/**
 *  @brief  テキスト設定
 *  @param  [in] strSel  選択中するテキスト
 *  @retval なし 
 *  @note   
 */
void CVirtualCheckCombo::SelText( StdString strSel)
{
    int iIndex =  FindStringExact(-1, strSel.c_str());

    if (iIndex == CB_ERR)
    {
        SetCurSel(-1);
    }
    else
    {
        SetCurSel(iIndex);
    }
}