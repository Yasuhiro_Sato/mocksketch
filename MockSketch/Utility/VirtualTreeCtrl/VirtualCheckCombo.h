/**
 * @brief			VirtualCheckComboヘッダーファイル
 * @file			VirtualCheckCombo.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        チェックボックス付き仮想リストコントロール用エディットボックス
 *			
 *
 * $
 * $
 * 
 */
#ifndef _VERTUAL_CHECK_COMBO_H_
#define _VERTUAL_CHECK_COMBO_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/

/*---------------------------------------------------*/
/*  class                                            */
/*---------------------------------------------------*/
class CVirtualCheckList;

/**
 * @class   CVirtualCheckEdit
 * @brief   チェックボックス付き仮想リストコントロール用エディットボックス
 */

class CVirtualCheckCombo : public CComboBox
{
public:
    //コンストラクター
    CVirtualCheckCombo (CVirtualCheckList* pCtrl, int iItem, int iSubItem, CString sInitText);

    //ディストラクター
    virtual ~CVirtualCheckCombo();

    //編集中止
    void Stop(bool cancel);

    //!< リスト設定
    void SetList(std::vector<StdString> lstText, StdString curText);


    //テキスト設定
    void SelText( StdString strSel);

    //{{AFX_VIRTUAL(CVirtualCheckEdit)
    public:
    virtual BOOL PreTranslateMessage(MSG* pMsg);
    //}}AFX_VIRTUAL

protected:
    //親リストコントロール
    CVirtualCheckList* pListCtrl;

    //編集行
    int			m_nRow;

    //編集列
    int			m_nCol;

    //入力開始時文字列
    CString		m_InitText;

     //終了中判定フラグ
    bool        m_isClosing;

    //{{AFX_MSG(CVirtualCheckEdit)
    //フォーカス消失時処理
    afx_msg void OnKillFocus(CWnd* pNewWnd);

    
    //作成時処理
    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);

    //方向キーおよび Tab キーによる入力
	afx_msg UINT OnGetDlgCode();
	//}}AFX_MSG

    DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
#endif //_VERTUAL_CHECK_COMBO_H_

