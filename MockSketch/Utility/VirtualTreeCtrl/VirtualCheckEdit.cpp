/**
 * @brief			VirtualCheckEdit実装ファイル
 * @file			VirtualCheckEdit.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        チェックボックス付き仮想リストコントロール用エディットボックス
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "VirtualCheckList.h"
#include "VirtualCheckEdit.h"

/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


///////////////////////////////////////////////////////////
//	Method    : CVirtualCheckEdit::CVirtualCheckEdit
//	Summary   : コンストラクター
//	Arguments : pCtrl     (i) 親リストコントロール
//              nRow      (i) 行
//              nCol      (i) 列
//              sInitText (i) 初期文字列
//	Return    : なし
//	Exception : なし
//	Note      :
///////////////////////////////////////////////////////////
CVirtualCheckEdit::CVirtualCheckEdit (CVirtualCheckList* pCtrl, int nRow, int nCol, CString sInitText)
{
    pListCtrl = pCtrl;
    m_nRow  = nRow;
    m_nCol  = nCol;
    m_InitText = sInitText;
	m_isClosing = false;
}

///////////////////////////////////////////////////////////
//	Method    : CVirtualCheckEdit::~CVirtualCheckEdit
//	Summary   : デストラクター
//	Arguments : なし
//	Return    : なし
//	Exception : なし
//	Note      :
///////////////////////////////////////////////////////////
CVirtualCheckEdit::~CVirtualCheckEdit()
{
}

BEGIN_MESSAGE_MAP(CVirtualCheckEdit, CEdit)
    //{{AFX_MSG_MAP(CVirtualCheckEdit)
    ON_WM_KILLFOCUS()
    ON_WM_CHAR()
    ON_WM_CREATE()
    ON_WM_GETDLGCODE()
	ON_WM_KEYUP()
	ON_WM_KEYDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CVirtualCheckEdit message handlers

///////////////////////////////////////////////////////////
//	Method    : CVirtualCheckEdit::PreTranslateMessage
//	Summary   : メッセージフィルタ処理
//	Arguments : pMsg (i) ウインドウメッセージ
//	Return    : true/false
//	Exception : なし
//	Note      : 
///////////////////////////////////////////////////////////
BOOL CVirtualCheckEdit::PreTranslateMessage (MSG* pMsg) 
{
    if (pMsg->message == WM_KEYDOWN)
    {
	    if (pMsg->wParam == VK_RETURN || pMsg->wParam == VK_DELETE || 
			pMsg->wParam == VK_ESCAPE)// || pMsg->wParam == VK_TAB || 
			//pMsg->wParam == VK_UP || pMsg->wParam == VK_DOWN || GetKeyState (VK_CONTROL))
			{
				::TranslateMessage (pMsg);
				::DispatchMessage (pMsg);
				return TRUE;
			}
    }
    return CEdit::PreTranslateMessage (pMsg);
}

///////////////////////////////////////////////////////////
//	Method    : CVirtualCheckEdit::OnKillFocus
//	Summary   : フォーカス消失時処理
//	Arguments : pNewWnd (i) 新規フォーカスウインドウ
//	Return    : なし
//	Exception : なし
//	Note      : 
///////////////////////////////////////////////////////////
void CVirtualCheckEdit::OnKillFocus (CWnd* pNewWnd) 
{
    CEdit::OnKillFocus(pNewWnd);

	//End edit?
	if(!m_isClosing)
	{
		StopEdit(false);
	}
}

///////////////////////////////////////////////////////////
//	Method    : CVirtualCheckEdit::OnChar
//	Summary   : 文字入力
//	Arguments : nChar   (i)
//              nRepCnt (i)
//              nFlags  (i)
//	Return    : なし
//	Exception : なし
//	Note      :   
///////////////////////////////////////////////////////////
void CVirtualCheckEdit::OnChar (UINT nChar, UINT nRepCnt, UINT nFlags) 
{
    BOOL Shift = GetKeyState (VK_SHIFT) < 0;
    switch (nChar)
    {
		case VK_ESCAPE :
		{	
			//GetParent()->SetFocus();
			StopEdit(TRUE, VK_ESCAPE);
			return;
		}
		case VK_RETURN :
		{
			StopEdit(FALSE, VK_RETURN);
			return;
		}
    }
    CEdit::OnChar (nChar, nRepCnt, nFlags);
}

/**
 * 生成時処理要求
 * @param [in]    lpCreateStruct	初期化パラメータへのポインタ	 	  
 * @return		
 * @note	 
 * @exception   なし
 */
int CVirtualCheckEdit::OnCreate (LPCREATESTRUCT lpCreateStruct) 
{
    if (CEdit::OnCreate (lpCreateStruct) == -1)
		return -1;

    //親のフォントを引き継ぐ
    CFont* Font = GetParent()->GetFont();
    SetFont (Font);

    SetWindowText (m_InitText);
    SetFocus();
    SetSel (0, -1);
    return 0;
}

///////////////////////////////////////////////////////////
//	Method    : CVirtualCheckEdit::OnGetDlgCode
//	Summary   : 方向キーおよび Tab キーによる入力
//	Arguments : なし
//	Return    : UINT
//	Exception : なし
//	Note      : 
///////////////////////////////////////////////////////////
UINT CVirtualCheckEdit::OnGetDlgCode() 
{
    return CEdit::OnGetDlgCode() | DLGC_WANTARROWS;// | DLGC_WANTTAB;
}

///////////////////////////////////////////////////////////
//	Method    : CVirtualCheckEdit::StopEdit
//	Summary   : 編集中止
//	Arguments : cancel      (i) 中止
//              endkey      (i) 終了文字
//	Return    : なし
//	Exception : なし
//	Note      :
///////////////////////////////////////////////////////////
void CVirtualCheckEdit::StopEdit(bool cancel, UINT endkey)
{
    m_isClosing = true;

	CString Text;
	GetWindowText (Text);
    
	if(pListCtrl != NULL)
    {
		pListCtrl->m_pEdit = NULL;
    }
	DestroyWindow();

	pListCtrl->OnEndEdit(m_nRow, m_nCol, Text, cancel, endkey);
	delete this;
}
