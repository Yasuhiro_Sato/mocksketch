/**
 * @brief			VirtualCheckEditヘッダーファイル
 * @file			VirtualCheckEdit.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        チェックボックス付き仮想リストコントロール用エディットボックス
 *			
 *
 * $
 * $
 * 
 */
#ifndef _VERTUAL_CHECK_EDIT_H_
#define _VERTUAL_CHECK_EDIT_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/*---------------------------------------------------------------------------*/
/*      Header files                                                         */
/*---------------------------------------------------------------------------*/

/*--------------------------*/
/*      Class               */
/*--------------------------*/
class CVirtualCheckList;

/////////////////////////////////////////////////////////////////////////////
//	Class     : CVirtualCheckEdit
//	Summary   : チェックボックス付き仮想リストコントロール用エディットボックス
//	Note      :
/////////////////////////////////////////////////////////////////////////////
class CVirtualCheckEdit : public CEdit
{
public:
    //コンストラクター
    CVirtualCheckEdit (CVirtualCheckList* pCtrl, int iItem, int iSubItem, CString sInitText);

    //ディストラクター
    virtual ~CVirtualCheckEdit();

    //編集中止
	void StopEdit(bool cancel, UINT endkey=0);

    //{{AFX_VIRTUAL(CVirtualCheckEdit)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL

protected:
    //親リストコントロール
    CVirtualCheckList* pListCtrl;

    //編集行
    int			m_nRow;

    //編集列
    int			m_nCol;

    //入力開始時文字列
    CString		m_InitText;
	
    //終了中判定フラグ
    bool        m_isClosing;

    //{{AFX_MSG(CVirtualCheckEdit)
    //フォーカス消失時処理
    afx_msg void OnKillFocus(CWnd* pNewWnd);

    //文字入力
    afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
    
    //作成時処理
    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);

    //方向キーおよび Tab キーによる入力
	afx_msg UINT OnGetDlgCode();
	//}}AFX_MSG

    DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
#endif //_VERTUAL_CHECK_EDIT_H_

