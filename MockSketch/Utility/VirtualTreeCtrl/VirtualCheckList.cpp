/**
 * @brief			VirtualCheckList実装ファイル
 * @file			VirtualCheckList.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        チェックボックス付き仮想リストコントロール
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "VirtualCheckList.h"
#include "VirtualCheckEdit.h"
#include "VirtualCheckCombo.h"
#include "Utility/SelHeaderDlg.h"

/*---------------------------------------------------*/
/*  Macro                                            */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifdef _DEBUG
#include "Utility/CUtility.h"
#define DRAW_DEBUG_VCHECKLST 0
#endif

BEGIN_MESSAGE_MAP(CVirtualCheckList, CListCtrl)
    //{{AFX_MSG_MAP(CVirtualCheckList)
    ON_WM_PAINT()
    ON_WM_KEYDOWN()
    ON_WM_MOUSEWHEEL()
    ON_NOTIFY_REFLECT(LVN_GETDISPINFO, OnGetdispinfo)
    ON_NOTIFY_REFLECT(LVN_GETINFOTIP , OnGetInfoTip)
    ON_WM_VSCROLL()
    ON_WM_HSCROLL()
    ON_WM_MOUSEMOVE()
    ON_NOTIFY_REFLECT(NM_CUSTOMDRAW, OnCustomDraw)
    ON_WM_CHAR()
	ON_WM_SETFOCUS()
    ON_WM_KILLFOCUS()
    //}}AFX_MSG_MAP

    //ON_NOTIFY_REFLECT(LVN_HOTTRACK, OnLvnHotTrack) 
    ON_NOTIFY_REFLECT(LVN_ITEMCHANGED, OnItemChanged) 
    ON_NOTIFY_REFLECT_EX(NM_CLICK, OnClickEx)
    ON_NOTIFY_REFLECT_EX(NM_DBLCLK, OnDblClickEx)
    ON_NOTIFY_REFLECT_EX(NM_RCLICK, OnRightClickEx)
    
    ON_WM_LBUTTONDOWN()
    ON_WM_LBUTTONUP()

    //ON_NOTIFY_REFLECT(NM_KILLFOCUS, &CVirtualCheckList::OnNMKillfocus)
    //ON_NOTIFY_REFLECT(NM_SETFOCUS, &CVirtualCheckList::OnNMSetfocus)
END_MESSAGE_MAP()

void DB_PRINT_TIME()
{
        SYSTEMTIME      vTime;
        
        GetLocalTime( &vTime );

        StdStringStream strmOut;
        strmOut << StdFormat(_T("%02d:%02d:%02d.%03d:  "))  
            //% vTime.wMonth % vTime.wDay
            % vTime.wHour % vTime.wMinute % vTime.wSecond 
            % vTime.wMilliseconds;
        DB_PRINT(strmOut.str().c_str());
}

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
CVirtualCheckList::CVirtualCheckList()
{

    m_pEdit = NULL;
    m_editOnEnter = false;
    m_editOnWriting = false;
    m_editLastEndKey = 0;

    m_pCombo = NULL;

    m_bSetCallBack   = false;
    m_pParent        = NULL;
    m_CallbackChek   = NULL;
    m_CallbackChg    = NULL;
    m_CallbackFocus  = NULL;
    m_CallbackContextMenu = NULL;
    m_CallbackOnCommand   = NULL;
    m_CallOwnerDraw       = NULL; 
    m_CallOwnerMouse      = NULL;

    m_iRow = 0;
    m_iCol = 0;
    m_iOldRow = 0;
    m_iOldCol = 0;

    m_iFocusRowT = 0;
    m_iFocusColT = 0;
    m_iFocusRowB = 0;
    m_iFocusColB = 0;

    m_iFocusRowOldT = -1;
    m_iFocusColOldT = -1;
    m_iFocusRowOldB = -1;
    m_iFocusColOldB = -1;

     m_eSelMode = SEL_CELL;
    m_bEnableFocus = false;
    m_bMouseDown   = false;
    m_iMouseDownRow = -1;
    m_iMouseDownCol = -1;

    // クリップ領域
    m_iClipRowT = 0;
    m_iClipColT = 0;
    m_iClipRowB = 0;
    m_iClipColB = 0;

    // クリップ領域
    m_bEnableClip = false;

    m_bDrag = false;
    m_bDisableDrag = true;

    // ユーザによる列の表示変更
    m_bUserVisibleCol = false;
    m_bHeaderRbuttonDown = false;
    pHeaderMneu          = NULL;

}

/**
*  @brief  デストラクター.
*  @param  なし
*  @retval なし 
*  @note
*/
CVirtualCheckList::~CVirtualCheckList()
{
    //delete m_lastget;
}


/**
 *  @brief  入力編集設定
 *  @param  [in] bEdit 文字入力による編集開始
 *  @retval なし 
 *  @note   文字入力による編集開始設定
 */
void CVirtualCheckList::SetEditOnWriting(const bool bEdit)
{
    m_editOnWriting = bEdit;
}

/**
 *  @brief  Enterキーによる編集開始設定
 *  @param  [in] edit 編集フラグ
 *  @retval なし 
 *  @note   
 */
void CVirtualCheckList::SetEditOnEnter(const bool edit)
{
    m_editOnEnter = edit;
}

/**
 *  @brief  編集終了時押下キー取得
 *  @param  なし
 *  @retval キー種別 
 *  @note   通常 0, VK_RETURN or VK_ESACPE
 */
UINT CVirtualCheckList::GetLastEndEditKey() const
{
    return m_editLastEndKey;
}

/**
 *  @brief  コールバック設定
 *  @param  [in]  Callbackfunc  コールバック関数
 *  @param  [in]  CallbackChek  コールバック関数(チェックボックス)
 *  @param  [in]  pParent       親ウインドウ
 *  @retval なし 
 *  @note   
 */
void CVirtualCheckList::SetFunc(void (__cdecl *Callbackfunc)(LPVOID pParent, int nRow, int nCol, _TCHAR* pData, bool* bCheck), 
                                void (__cdecl *CallbackChek)(LPVOID pParent, int nRow, int nCol)                          
                                ,  LPVOID pParent)
{
    m_bSetCallBack   = true;
    m_Callbackfunc = Callbackfunc;
    m_CallbackChek = CallbackChek;
    m_pParent = pParent;
}

/**
 *  @brief  編集前コールバック
 *  @param  [in]  CallbackChg  コールバック関数
 *  @retval なし 
 *  @note   
 */
void CVirtualCheckList::SetChgFunc(void (__cdecl *CallbackChg)(LPVOID pParent, 
                                   CVirtualCheckList* pCheckList,
                                   int nRow, int nCol, const _TCHAR* pData))
{
    m_CallbackChg = CallbackChg;
}

/**
 *  @brief  編集前コールバック
 *  @param  [in]  CallFocusChg  コールバック関数
 *  @retval なし 
 *  @note   
 */
void CVirtualCheckList::SetFocusFunc(void (__cdecl *CallFocusChg)(LPVOID pParent, 
                                               CVirtualCheckList* pCheckList, 
                                               int nRowT, int nColT, 
                                               int nRowB, int nColB))
{
    m_CallbackFocus = CallFocusChg;
}

/**
 *  @brief  コンテキストメニューコールバック
 *  @param  [in]  CallContextMenu  コールバック関数
 *  @retval なし 
 *  @note   
 */
void CVirtualCheckList::SetContextMenuFunc(void (__cdecl * CallContextMenu)
                               (LPVOID pParent, 
                               CVirtualCheckList* pCheckList, 
                               POINT ptClick,
                               int nRow, int nCol))
{
    m_CallbackContextMenu = CallContextMenu;
}



/**
 *  @brief  コマンドコールバック
 *  @param  [in]  CallbackOnCommand  コールバック関数
 *  @retval なし 
 *  @note   
 */
void CVirtualCheckList::SetOnCommandFunc(bool (__cdecl *CallbackOnCommand)  
                                     (LPVOID pParent,
                                      CVirtualCheckList* pCheckList,
                                      UINT nCode, UINT nID))
{
    m_CallbackOnCommand = CallbackOnCommand;
}

/**
 *  @brief  オナードローコールバック
 *  @param  [in]  CallOwnerDraw  コールバック関数
 *  @retval なし 
 *  @note   
 */
void CVirtualCheckList::SetOwnerDrawFunc(void (__cdecl *CallOwnerDraw)
    (LPVOID pParent, 
    CVirtualCheckList* pCheckList,
    int nRow, int nCol, CDC* pDC, 
    CString sData, bool bCheck))
{
    m_CallOwnerDraw = CallOwnerDraw;
}


/**
 *  @brief  オナーマウスボタンコールバック
 *  @param  [in]  CallOwnerDraw  コールバック関数
 *  @retval なし 
 *  @note   
 */
void CVirtualCheckList::SetOwnerMouseFunc(void (__cdecl *CallOwnerMouse)
                                           (LPVOID pParent, 
                                            CVirtualCheckList* pCheckList,
                                            int nRow, int nCol, 
                                            POINT ptPos, bool bMouseDown))
{
    m_CallOwnerMouse = CallOwnerMouse;
}


/**
 *  @brief  現在のエディットボックス取得
 *  @param  なし
 *  @retval エディットボックスへのポインタ
 *  @note  
 */
CEdit* CVirtualCheckList::GetEditBox() const
{
    return m_pEdit;
}

/**
 *  @brief  列挿入
 *  @param  [in] nCol              新しい列のインデックス
 *  @param  [in] lpszColumnHeading 列ヘッダーを持つ文字列のアドレス 
 *  @param  [in] nFormat           列の配置を指定する整数(LVCFMT_LEFT、LVCFMT_RIGHT、 LVCFMT_CENTER)
 *  @param  [in] nWidth            ピクセル単位の列の幅。このパラメータに -1 を指定すると、列の幅は設定されません。 
 *  @param  [in] nSubItem          列に関連付けられたサブアイテムのインデックス。このパラメータに -1 を指定すると、列に関連付けられるサブアイテムはありません。 
 *  @retval 正常終了した場合は新しい列のインデックスを返します。
 *  @retval     それ以外の場合は -1 を返します。タ
 *  @note  
 */
int CVirtualCheckList::InsertColumn(int nCol, LPCTSTR lpszColumnHeading, int nFormat,
                                    int nWidth, int nSubItem)
{

    int nRet;   
    nRet = CListCtrl::InsertColumn( nCol, lpszColumnHeading, nFormat,
        nWidth, nSubItem);    

    std::vector<COL_SET>::iterator      iteColSet;
    std::vector<COL_STATE>::iterator    iteColState;

    if (nRet != -1)
    {
        //ディフォルトデータを挿入
        COL_SET     ColSet;
        COL_STATE   ColState;

        iteColSet = m_lstColSet.begin();
        iteColSet += nCol;

        iteColState = m_lstColState.begin();
        iteColState += nCol;

        ColState.iWidth   = nWidth;
        ColState.bVisible = ColSet.bVisible;

        m_lstColSet.insert(iteColSet, ColSet);
        m_lstColState.insert(iteColState, ColState);
    }
    return nRet;
}

/**
 *  @brief  列設定
 *  @param  [in]  nCol 列位置
 *  @param  [in]  Set  設定データ
 *  @retval ture:成功
 *  @note   
 */
BOOL CVirtualCheckList::SetCol(int nCol, const COL_SET Set)
{
    if ((nCol < 0) ||
        (nCol >= (int)m_lstColSet.size()))
    {
        return FALSE;
    }

    m_lstColSet[nCol] = Set;
    return TRUE;
}

/**
 *  @brief  列取得
 *  @param  [in]  nCol 列位置
 *  @param  [out] pGet 取得データ
 *  @retval ture:成功
 *  @note   
 */
BOOL CVirtualCheckList::GetCol (int nCol, COL_SET* pGet)
{
    if ((nCol < 0) ||
        (nCol >= (int)m_lstColSet.size()))
    {
        return FALSE;
    }

    *pGet = m_lstColSet[nCol];
    return TRUE;
}

/**
 *  @brief カスタム描画処理
 *  @param [in]     pNMHDR
 *  @param [out]    pResult
 *  @retval なし
 *  @note  
 */
void CVirtualCheckList::OnCustomDraw(NMHDR* pNMHDR, LRESULT* pResult)
{	
    NMLVCUSTOMDRAW* pLVCD = reinterpret_cast<NMLVCUSTOMDRAW*>( pNMHDR );
    if ( CDDS_PREPAINT == pLVCD->nmcd.dwDrawStage )
    {
        //描画サイクルが始まる前
        *pResult = CDRF_NOTIFYITEMDRAW;
        //コントロールはNM_CUSTOMDRAW通知メッセージをアイテム描画の前後で送ってきます。
    }
    else if ( CDDS_ITEMPREPAINT == pLVCD->nmcd.dwDrawStage )
    {
        //アイテムが描画される前
        *pResult = CDRF_NOTIFYSUBITEMDRAW; 
        //コントロールは描画サイクルが完了したらWM_CUSTOMDRAW を送ってきます。
    }
    else if ( (CDDS_ITEMPREPAINT| CDDS_SUBITEM) == pLVCD->nmcd.dwDrawStage )
    {
        //サブアイテムが描画されようとしているとき
        CDC* pDC = CDC::FromHandle(pLVCD->nmcd.hdc);

        //描画処理
        DrawItem(static_cast<int> (pLVCD->nmcd.dwItemSpec),
            pLVCD->iSubItem, pDC);

        *pResult = CDRF_SKIPDEFAULT;	// コントロールは全く描画をしません
    }

    /*
    このイベントは発生しない
    else if ( CDDS_POSTPAINT == pLVCD->nmcd.dwDrawStage )
    {
        //描画サイクル完了後
        int nRow = static_cast<int> (pLVCD->nmcd.dwItemSpec);
        int nCol = pLVCD->iSubItem;

        CDC* pDC = CDC::FromHandle(pLVCD->nmcd.hdc);
        DrawPostPaint(pDC);
    }
    */
/*
DB_PRINT(_T("OnCustomDraw %x, %d %d\n"), pLVCD->nmcd.dwDrawStage
                                    , static_cast<int> (pLVCD->nmcd.dwItemSpec)
                                    , pLVCD->iSubItem);
*/
/*

   NMCUSTOMDRAW nmcd;
    COLORREF clrText;
    COLORREF clrTextBk;
#if (_WIN32_IE >= 0x0400)
    int iSubItem;
#endif
#if (_WIN32_WINNT >= 0x0501)
    DWORD dwItemType;

    // Item custom draw
    COLORREF clrFace;
    int iIconEffect;
    int iIconPhase;
    int iPartId;
    int iStateId;

    // Group Custom Draw
    RECT rcText;
    }
*/
}


#ifndef COLOR_HOTLIGHT
#define COLOR_HOTLIGHT 26
#endif

/**
 *  @brief  文字色取得
 *  @param  [in]    id (i)
 *  @retval 文字色
 *  @note  
 */
COLORREF CVirtualCheckList::GetTextColor(int nRow, int nCol, 
                                         const bool forceNoSelection, 
                                         const bool forceSelection)
{
    bool navigated = false;
    if(nCol == m_iCol )
    {
        navigated = true;
    }

    if(	(!forceNoSelection && IsSelected(nRow, nCol)) 
        || forceSelection)
    {

        if(GetFocus() == this)
        {
            return ::GetSysColor(COLOR_HIGHLIGHTTEXT);
        }
        else
        {
            return ::GetSysColor(COLOR_WINDOWTEXT);
        }

    }

    if( GetHotItem() == nRow )
    {
        return ::GetSysColor(COLOR_HOTLIGHT);
    }
    else
    {
        return ::GetSysColor(COLOR_WINDOWTEXT);
    }
}

/**
 *  @brief  背景色取得
 *  @param  [in] nRow
 *  @param  [in] nCol
 *  @param  [in] forceNoSelection
 *  @retval 背景色
 *  @note  
 */
COLORREF CVirtualCheckList::GetBackColor(int nRow, int nCol, const bool forceNoSelection)
{	
    bool navigated = false;
    if( nCol == m_iCol )
    {
        navigated = true;
    }

    bool hasFocus = (GetFocus() == this);

    if(!forceNoSelection && IsSelected(nRow, nCol)) 
    {
        if(navigated && GetFocus() == this)
        {
            return ::GetSysColor(COLOR_HOTLIGHT);
        }
        else if(GetHotItem() == nRow)
        {
            return ::GetSysColor(COLOR_HOTLIGHT);
        }
        else
        {
            if(GetFocus() == this)
            {
                return ::GetSysColor(COLOR_HIGHLIGHT);
            }
            else
            {
                return ::GetSysColor(COLOR_BTNFACE);
            }
        }
    }
    return ::GetSysColor(COLOR_WINDOW);
}

/**
 *  @brief  選択有無判定
 *  @param  [in] nRow     (i) 行
 *  @param  [in] nCol     (i) 列
 *  @retval 選択有無
 *  @note  
 */
bool CVirtualCheckList::IsSelected(const int nRow, const int nCol)
{
    UINT iSel = GetItemState(nRow, LVIS_SELECTED);
    UINT iFocus = GetItemState(nRow, LVIS_FOCUSED);
    if (iSel /*|| iFocus*/)
    {
        if ( nCol == 0 ||		 
            FullRowSelect()	
            )			
        {
            //先頭項目もしくは全行選択時
            if (m_hWnd != ::GetFocus())
            {
                // フォーカスあり
                if (GetStyle() & LVS_SHOWSELALWAYS)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return true;
        }
    }
    return false;
}

/**
 *  @brief  描画処理
 *  @param  なし
 *  @retval なし
 *  @note  
 */
void CVirtualCheckList::OnPaint() 
{
    Default();
}

/**
 *  @brief  セル領域取得
 *  @param  [in]  nRow  行
 *  @param  [in]  nCol  列
 *  @param  [out] rect  領域
 *  @retval 成功・失敗
 *  @note  
 */
BOOL CVirtualCheckList::GetCellRect(int nRow,int nCol,	CRect& rect) 
{
    if ((nRow < 0) || nRow >= GetItemCount())
    {
        return FALSE;
    }

    if ((nCol < 0) || nCol >= GetHeaderCtrl()->GetItemCount())
    {
        return FALSE;
    }

    BOOL bRC = CListCtrl::GetSubItemRect(nRow, nCol,  LVIR_BOUNDS, rect);

    if (nCol == 0)
    {
        //水平スクロール使用時に要オフセット
        int offset = rect.left;

        CRect firstColumnRect;
        GetHeaderCtrl()->GetItemRect(0, &firstColumnRect);
        rect.left = firstColumnRect.left + offset;
        rect.right = firstColumnRect.right + offset;
    }
    return bRC;
}

/**
 *  @brief  テキスト領域取得
 *  @param  [in]  nRow  行
 *  @param  [in]  nCol  列
 *  @param  [out] rect  領域
 *  @retval 成功・失敗
 *  @note  
 */
bool CVirtualCheckList::GetTextRect(const int nRow, const int nCol, CRect& rect)
{
    //CVirtualCheckList::CListItemData& data = GetItemData(nRow, nCol);
    if(!GetCellRect( nRow, nCol, rect))
    {
        return false;
    }
    CRect temp;

    if( GetCheckboxRect(nRow, nCol, temp, false))
    {
        rect.left = temp.right;
    }
    return true;
}

/**
 *  @brief チェックボックス領域取得
 *  @param  [in]  nRow  行
 *  @param  [in]  nCol  列
 *  @param  [out] rect  領域
 *  @retval 成功・失敗
 *  @note  
 */
bool CVirtualCheckList::GetCheckboxRect(const int nRow, const int nCol, CRect& rect, 
                                        bool checkOnly, bool* pCheck)
{
    if (nCol < 0)
    {
        return false;
    }

    if (nCol >= (int)m_lstColSet.size())
    {
        return false;
    }

    if (!m_lstColSet[nCol].bDraw)
    {
        return false;
    }

    bool    bCheck;
    CString sData;

    GetItemData( nRow, nCol, sData,  &bCheck);

    CRect full;
    GetCellRect( nRow, nCol, full);

    rect = full;

    int checkWidth = full.Height();
    rect.right = rect.left + checkWidth;

    if(!checkOnly)
    {
        rect.left = full.left;
    }
    if (pCheck)
    {
        *pCheck = bCheck;
    }
    return true;
}

/**
 *  @brief アイテム描画
 *  @param  [in]  nRow  行
 *  @param  [in]  nCol  列
 *  @param  [in]  pDC   
 *  @retval なし
 *  @note  
 */
void CVirtualCheckList::DrawItem(int nRow, int nCol, CDC* pDC)
{	
    CString sData;
    bool    bCheck;

    GetItemData( nRow, nCol, sData,  &bCheck);


    if(m_lstColSet[nCol].bOwnerDraw)
    {
        if(m_CallOwnerDraw)
        {
            m_CallOwnerDraw(m_pParent, this, 
                           nRow, nCol, pDC, sData, bCheck);
        }
    }
    else
    {
        //ボタン描画
        if(m_lstColSet[nCol].bDraw)
        {
            DrawButton( nRow, nCol, pDC);
        }
        //テキスト描画
        DrawText(nRow, nCol, sData, pDC);
    }

    //選択枠描画
    DrawSel(nRow, nCol, pDC);
}

/**
 *  @brief  選択描画
 *  @param  [in]  nRow  行
 *  @param  [in]  nCol  列
 *  @param  [in]  pDC   
 *  @retval なし
 *  @note  
 */
void CVirtualCheckList::DrawSel(int nRow, int nCol, CDC* pDC)
{
    if (m_eSelMode != SEL_CELL)
    {
        return;
    }

    CBrush brEnable (GetSysColor(COLOR_WINDOWTEXT));
    CRect rcSel;
    GetTextRect( nRow, nCol, rcSel);

    int iColS, iColE;
    int iRowS, iRowE;

    if (m_iFocusColT > m_iFocusColB)
    {
        iColE = m_iFocusColT;
        iColS = m_iFocusColB;
    }
    else
    {
        iColS = m_iFocusColT;
        iColE = m_iFocusColB;
    }

    if (m_iFocusRowT > m_iFocusRowB)
    {
        iRowE = m_iFocusRowT;
        iRowS = m_iFocusRowB;
    }
    else
    {
        iRowS = m_iFocusRowT;
        iRowE = m_iFocusRowB;
    }
    if (m_bEnableFocus)
    {
        if ((nCol >= iColS) &&
            (nCol <= iColE) &&
            (nRow >= iRowS) &&
            (nRow <= iRowE))
        {
            if ((nRow != m_iFocusRowT) ||
                (nCol != m_iFocusColT))
            {
                AlphaBlendFill(pDC->m_hDC, rcSel, GetSysColor(COLOR_HIGHLIGHT), 50);
            }
            /*
            CBrush brSel (GetSysColor(COLOR_HIGHLIGHT));
            int iOldRop = pDC->SetROP2(R2_XORPEN);
            pDC->FillRect( rcSel, &brSel);
            pDC->SetROP2(iOldRop);
            */
        }
    }

    if (m_bEnableClip)
    {
        //先頭列選択時は１行すべてを枠で囲む
        POINT ptPos[4];
        CPen penEnable(PS_SOLID, 1, GetSysColor(COLOR_HIGHLIGHT));
        CPen* pOldPen = (CPen*)pDC->SelectObject(&penEnable);
        for (int nCnt = 0; nCnt < 2; nCnt++)
        {
            ptPos[0].x = rcSel.left   + nCnt;
            ptPos[0].y = rcSel.top    + nCnt;
            ptPos[1].x = rcSel.right  - nCnt;
            ptPos[1].y = rcSel.top    + nCnt;
            ptPos[2].x = rcSel.right  - nCnt;
            ptPos[2].y = rcSel.bottom - nCnt - 1;
            ptPos[3].x = rcSel.left   + nCnt;
            ptPos[3].y = rcSel.bottom - nCnt - 1;

            if ((nRow == m_iClipRowT)&&
                (nCol >= m_iClipColT) &&
                (nCol <= m_iClipColB))
            {
                pDC->MoveTo(ptPos[0]);
                pDC->LineTo(ptPos[1]);
            }

            if ((nRow == m_iClipRowB) &&
                (nCol >= m_iClipColT) &&
                (nCol <= m_iClipColB))
            {
                pDC->MoveTo(ptPos[3]);
                pDC->LineTo(ptPos[2]);
            }


            if ((nCol == m_iClipColT) && 
                (nRow >= m_iClipRowT) &&
                (nRow <= m_iClipRowB))
            {
                pDC->MoveTo(ptPos[0]);
                pDC->LineTo(ptPos[3]);
            }

            if ((nCol == m_iClipColB) && 
                (nRow >= m_iClipRowT) &&
                (nRow <= m_iClipRowB))
            {
                pDC->MoveTo(ptPos[1]);
                pDC->LineTo(ptPos[2]);
            }
        }
        pDC->SelectObject(pOldPen);
    }

    // 選択行
    if ((m_iRow == nRow) &&
        (m_iCol == nCol))
    {
        rcSel.bottom--;
        pDC->FrameRect( rcSel, &brEnable);
        rcSel.DeflateRect( 1, 1 );
        pDC->FrameRect( rcSel, &brEnable);
    }
}

/**
 *  @brief テキスト描画
 *  @param  [in]  nRow  行
 *  @param  [in]  nCol  列
 *  @param  [in]  sData 表示テキスト
 *  @param  [in]  pDC    
 *  @retval なし
 *  @note  
 */
void CVirtualCheckList::DrawText( int nRow, int nCol, CString sData,
                                 CDC* pDC)
{
    ASSERT(pDC);

    CRect rect, fulltextrect;
    CRect rcColor;

    GetTextRect( nRow, nCol, rect);

    fulltextrect = rect;
    rcColor      = fulltextrect; 
    rcColor.DeflateRect( 3, 3 );

    bool bColor = m_lstColSet[nCol].bColor;

    if(sData.IsEmpty())
    {
        //Just clean if no progress bar
        FillSolidRect(	pDC,
            fulltextrect,
            GetBackColor(nRow, nCol, !FullRowSelect() ));

        if (bColor)
        {
            //テキストエリアより一回り小さいエリアに色を表示する

            COLORREF crFill = m_lstColSet[nCol].crDefault;

            
            CBrush brFrame(::GetSysColor(COLOR_WINDOWFRAME));
            FillSolidRect(	pDC, rcColor, crFill);
            pDC->FrameRect( rcColor, &brFrame);
        }

    }
    else
    {		
        UINT textFormat=0;

        textFormat = DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS | GetTextJustify(nCol);

        //If we don't do this, character after & will be underlined.
        sData.Replace(_T("&"), _T("&&"));

        {
            rect.left += 2;

            pDC->SetTextColor( GetTextColor(nRow, nCol, false) );

            COLORREF backColor = GetBackColor(nRow, nCol, false);

            FillSolidRect(	pDC,
                fulltextrect,
                GetBackColor(nRow, nCol, !FullRowSelect())
                );
            

            if( backColor == TRANSPARENTCOLOR)
            {
                pDC->SetBkMode(TRANSPARENT);
            }
            else
            {
                pDC->SetBkMode(OPAQUE);
                pDC->SetBkColor( backColor );
            }			

            if (bColor)
            {
                //テキストエリアより一回り小さいエリアに色を表示する
                COLORREF crFill = (COLORREF)_tstoi(sData);
                CBrush brFrame(::GetSysColor(COLOR_WINDOWFRAME));

                FillSolidRect(	pDC, rcColor, crFill);
                pDC->FrameRect( rcColor, &brFrame);
            }
            else
            {
                pDC->DrawText(sData, &rect, textFormat);
            }
        }
    }

    /*
    if(	GetNavigationColumn() == nCol &&
        GetFocus() == this &&
        GetItemInFocus() == nRow
        )
    {
        //fulltextrect.DeflateRect(1,0,0,1);
        pDC->DrawFocusRect(&fulltextrect);
    }
    */
}

/**
 *  @brief  ボタン描画
 *  @param  [in]  nRow  行
 *  @param  [in]  nCol  列
 *  @param  [in]  pDC   (i) 
 *  @retval なし
 *  @note  
 */
void CVirtualCheckList::DrawButton( int nRow, int nCol, CDC* pDC)
{
    ASSERT(pDC);

    CRect chkboxrect;
    bool bCheck;

    GetCheckboxRect( nRow, nCol, chkboxrect, false);

    //Clear background
    FillSolidRect(	pDC,
        chkboxrect,
        GetBackColor(nRow, nCol, true )
        );

    GetCheckboxRect( nRow, nCol, chkboxrect, true, &bCheck);

    int iStyle;
    iStyle = m_lstColSet[nCol].iStyle;


    if ((DFCS_BUTTONPUSH & iStyle) ||
        (DFCS_BUTTON3STATE & iStyle))
    {
        POINT ptPos;
        ptPos.x = 0;
        ptPos.y = 0;


        if ((m_bMouseDown) &&
            (!(DFCS_INACTIVE & iStyle)))
        {
            ::GetCursorPos(&ptPos);
            ScreenToClient(&ptPos);

            int iClickRow;
            int iClickCol;
            bool bClickButton;

            HitTest(ptPos, &iClickRow, &iClickCol, &bClickButton);
            if ( (iClickRow == nRow) &&
                 (iClickCol == nCol) &&
                  bClickButton)
            {
                iStyle |= DFCS_PUSHED;
                m_iMouseDownRow = nRow;
                m_iMouseDownCol = nCol;
            }
        }
    }
    else
    {
        if (bCheck) 
        {
            iStyle |= DFCS_CHECKED;
        }
    }
    pDC->DrawFrameControl(&chkboxrect, DFC_BUTTON, iStyle );
}

/**
 *  @brief  領域塗りつぶし
 *  @param  [in] pDC   デバイスコンテキスト
 *  @param  [in] rect  塗りつぶし領域
 *  @param  [in] color 塗りつぶし色
 *  @retval なし
 *  @note  
 */
void CVirtualCheckList::FillSolidRect(CDC *pDC, const RECT& rect, const COLORREF color)
{
    if(color != TRANSPARENTCOLOR)
    {
        pDC->FillSolidRect(&rect, color);
    }
}

/**
 *  @brief オーナーデータ要求
 *  @param  [in] pNMHDR
 *  @param  [in] pResult
 *  @retval なし
 *  @note  
 */
void CVirtualCheckList::OnGetdispinfo(NMHDR* pNMHDR, LRESULT* pResult) 
{
    LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pNMHDR;

    LV_ITEM* pItem= &(pDispInfo)->item;

    int nRow = pItem->iItem;
    int nCol = pItem->iSubItem;

    if( (pItem->mask & LVIF_IMAGE) || (pItem->mask & LVIF_TEXT) ) 
    {
        CString sData;
        bool    bCheck;
        GetItemData( nRow, nCol, sData, &bCheck);

        if( (pItem->mask & LVIF_TEXT) )
        {
            lstrcpyn(pItem->pszText, sData, pItem->cchTextMax);
        }

        if( pItem->mask & LVIF_IMAGE) 
        {
            //ボタン描画
            if(m_lstColSet[nCol].bDraw)
            {
                pItem->mask |= LVIF_STATE;
                pItem->stateMask = LVIS_STATEIMAGEMASK;

                //if( (m_lstColSet[nCol].iStyle & DFCS_CHECKED) != 0)
                if (bCheck)
                {
                    //チェックON
                    ;//pItem->state = INDEXTOSTATEIMAGEMASK(2);
                    //チェックOFF
                    pItem->state = INDEXTOSTATEIMAGEMASK(1);
                }
                else
                {
                    //チェックOFF
                    pItem->state = INDEXTOSTATEIMAGEMASK(1);
                }
            }
        }
    }
    *pResult = 0;
}

/**
 *  @brief  ツールチップ要求
 *  @param  [in] pNMHDR
 *  @param  [in] pResult
 *  @retval なし
 *  @note  
 */
void CVirtualCheckList::OnGetInfoTip(NMHDR* pNMHDR, LRESULT* pResult) 
{
    //TODO:実装
    
}

/**
 *  @brief  項目データ取得
 *  @param  [in]  nRow    行
 *  @param  [in]  nCol    列
 *  @param  [out] sData   テキスト
 *  @param  [out] pbCheck チェックの有無
 *  @retval なし
 *  @note  
 */
void CVirtualCheckList::GetItemData(const int nRow, const int nCol, CString& sData, bool* pbCheck)
{

    ASSERT(nRow >= 0);
    ASSERT(nRow < GetItemCount());
    ASSERT(nCol >= 0);

    TCHAR szBuf[4098];
    memset(szBuf, 0, sizeof(szBuf));

    if (m_Callbackfunc)
    {
        ASSERT(m_pParent != NULL);
        m_Callbackfunc( m_pParent, nRow, nCol,  szBuf, pbCheck);
    }
    sData = szBuf;
}

/**
 *  @brief  テキスト表示位置
 *  @param  [in]  nCol    列
 *  @retval テキスト表示位置
 *  @note  
 */
UINT CVirtualCheckList::GetTextJustify(const int nCol)
{
    HDITEM hditem;
    hditem.mask = HDI_FORMAT;
    GetHeaderCtrl()->GetItem(nCol, &hditem);

    int nFmt = hditem.fmt & HDF_JUSTIFYMASK;

    if (nFmt == HDF_CENTER)
    {
        return  DT_CENTER;
    }
    else if (nFmt == HDF_LEFT)
    {
        return  DT_LEFT;
    }
    else
    {
        return  DT_RIGHT;
    }
}

/**
 *  @brief 全行選択確認
 *  @param なし
 *  @retval true/全行選択
 *  @note  
 */
bool CVirtualCheckList::FullRowSelect()
{
    return (GetExtendedStyle() & LVS_EX_FULLROWSELECT) != 0;
}

/**
 *  @brief ヒットテスト
 *  @param  [in]  point   確認位置
 *  @param  [out] pRow    行位置
 *  @param  [out] pCol    列位置
 *  @param  [out] pbCheck チェックボックスクリックの有無
 *  @retval true/項目を選択した
 *  @note  
 */
bool CVirtualCheckList::HitTest(const POINT& point, int* pRow, int* pCol, bool* pbCheck)
{
    LVHITTESTINFO test;
    test.pt = point;
    test.flags=0;
    test.iItem = test.iSubItem = -1;

    if(ListView_SubItemHitTest(m_hWnd, &test) == -1)
    {
        //SetItemState( GetItemInFocus(),  LVIS_SELECTED | LVIS_FOCUSED, 
        //    LVIS_SELECTED | LVIS_FOCUSED);
        return false;
    }

    if (pRow)
    {
        *pRow    = test.iItem;
    }

    if (pCol)
    {
        *pCol = test.iSubItem;
    }   

    if ((test.iItem < 0) ||
        (test.iItem >= GetItemCount()))
    {
        return false;
    }

    if ((test.iSubItem < 0) || 
        (test.iSubItem >= GetHeaderCtrl()->GetItemCount()))
    {
        return false;
    }

    //Make check box hit test?
    if(pbCheck != NULL)
    {
        CRect checkrect;

        //Has check box?
        if(GetCheckboxRect(test.iItem, test.iSubItem, checkrect, true))
        {
            if( checkrect.PtInRect(point) )
            {
                *pbCheck = true;
            }
            else
            {
                *pbCheck = false;
            }
        }
        else
        {
            *pbCheck = false;
        }

    }
    return true;
}

/**
 *  @brief 列再描画
 *  @param  [in] nBottom    最終行位置
 *  @param  [in] eType      再描画種別
 *  @retval なし
 *  @note  
 */
void CVirtualCheckList::RedrawCol(int nCol, REDRAW_TYPE eType)
{
    int nTop = GetTopIndex(), nBottom = nTop + GetCountPerPage();
    RedrawCol( nTop,  nBottom, nCol, eType, true);
}

/**
 *  @brief  列再描画
 *  @param  [in] nTop       先頭行位置
 *  @param  [in] nBottom    最終行位置
 *  @param  [in] nCol       列位置
 *  @param  [in] eType      再描画種別
 *  @param  [in] erase      背景描画の有無
 *  @retval なし
 *  @note  
 */
void CVirtualCheckList::RedrawCol(int nTop, int nBottom, int nCol, REDRAW_TYPE eType, BOOL erase)
{
#if DRAW_DEBUG_VCHECKLST
    DB_PRINT_TIME();
    DB_PRINT(_T("RedrawCol %d, %d, %d, %d %c,\n"), nTop, nBottom, nCol, eType, erase?_T('T'):_T('F'));
#endif
    if(GetItemCount() <= 0)
        return;

    if(!MakeInside(nTop, nBottom))
    {
        //再描画するものがない
        return;
    }

    //最終行を選択状態にする
    if( nCol < 0 )
    {
        return;
    }

    CRect t, b;

    if( eType  == RD_COL) 
    {
        GetCellRect(nTop, nCol, t);
        GetCellRect(nBottom, nCol, b);

    }
    else if(eType == RD_CHECK)
    {
        GetCheckboxRect(nTop, nCol, t, true);
        GetCheckboxRect(nBottom, nCol, b, true);
    }
    else if(eType == RD_TEXT)
    {
        GetTextRect(nTop, nCol, t);
        GetTextRect(nBottom, nCol, b);
    }
    //更新
    t.UnionRect(&t, &b);
    InvalidateRect( &t, erase );
}

/**
 *  @brief  列再描画
 *  @param  [in] nRow  行位置
 *  @param  [in] erase 更新領域背景消去有無
 *  @retval なし
 *  @note  
 */
void CVirtualCheckList::RedrawRow(int nRow, BOOL erase)
{
    CRect t, b;

    if(!MakeInside(nRow, nRow))
    {
        //再描画するものがない
        return;
    }


    int iColMax = GetHeaderCtrl()->GetItemCount() - 1;

    if (iColMax < 0)
    {
        return;
    }

    GetCellRect(nRow, 0, t);
    GetCellRect(nRow, iColMax, b);

    t.UnionRect(&t, &b);
    InvalidateRect( &t, erase );
}

/**
 *  @brief  表示領域取得
 *  @param  [out] nTop     先頭行位置
 *  @param  [out] nBottom  最終行位置
 *  @retval false/該当可視表示領域なし
 *  @note   
 */
bool CVirtualCheckList::MakeInside(int& nTop, int &nBottom)
{
    int min = GetTopIndex(), max = min+GetCountPerPage();

    if(max >= GetItemCount())
        max = GetItemCount()-1;

    if(nTop < min && nBottom < min)
    {
        return false;
    }

    if(nTop > max && nBottom > max)
    {
        return false;
    }

    if(nTop < min)
    {
        nTop = min;
    }

    if(nBottom > max)
    {
        nBottom = max;
    }
    return true;
}

/**
 *  @brief 項目編集開始
 *  @param  [in] nRow 行
 *  @param  [in] nCol 列
 *  @retval なし
 *  @note  
 */
CWnd* CVirtualCheckList::EditSubItem (int nRow, int nCol)
{
    //表示確認
    if (!EnsureVisible (nRow, TRUE)) 
    {
        return NULL;
    }

    CHeaderCtrl* pHeader = (CHeaderCtrl*) GetDlgItem(0);
    int nColumnCount = pHeader->GetItemCount();
    if (nCol >= nColumnCount || GetColumnWidth (nCol) < 5)
    {
        return NULL;
    }

    CString text;
    bool bCheck;

    GetItemData(nRow, nCol, text, &bCheck);

    if (m_CallbackChg != NULL)
    {
        m_CallbackChg(m_pParent, this, nRow, nCol, (LPCTSTR)text);
    }

    // 編集許可
    if(m_lstColSet[nCol].eEditType == NONE)
    {
        return NULL;
    }

    //-------------------
    //-------------------
    LV_DISPINFO dispinfo;
    dispinfo.hdr.hwndFrom = m_hWnd;
    dispinfo.hdr.idFrom = GetDlgCtrlID();
    dispinfo.hdr.code = LVN_BEGINLABELEDIT;

    dispinfo.item.mask = LVIF_TEXT;
    dispinfo.item.iItem = nRow;
    dispinfo.item.iSubItem = nCol;

    //Copy the text we get from a simple WM_USER_GETLISTITEMDATA message
    //and add some extra space to allow changing the item
    dispinfo.item.cchTextMax = text.GetLength()*4;

    if(dispinfo.item.cchTextMax < 2048)
    {
        dispinfo.item.cchTextMax = 2048;
    }

    dispinfo.item.pszText = new TCHAR[dispinfo.item.cchTextMax];
    _tcscpy_s(dispinfo.item.pszText, dispinfo.item.cchTextMax, text);

    //Send to parent
    BOOL ret =	(BOOL)::SendMessage (	GetParent()->GetSafeHwnd(),
        WM_NOTIFY, 
        (WPARAM) GetDlgCtrlID(), 
        (LPARAM) &dispinfo);

    //Copy editing text
    text = dispinfo.item.pszText;
    delete [] dispinfo.item.pszText;

    //Editing is not allowed, skip this
    if(ret == TRUE)
    {
        return NULL;
    }
    //------------------------				

    CRect Rect;
    //GetItemRect (Item, &Rect, LVIR_BOUNDS);
    GetTextRect(nRow, nCol, Rect);

    // Now scroll if we need to expose the column
    CRect ClientRect;
    GetClientRect (&ClientRect);

    //    Rect.right = Rect.left + GetColumnWidth (Column);
    if (Rect.right > ClientRect.right)
    {
        Rect.right = ClientRect.right;
    }

    //Make a little bit larger, looks better on some fonts
    Rect.top-=1;
    Rect.bottom+=2;

    // Get Column alignment
    DWORD style = 0;

    {
        UINT justify=GetTextJustify(nCol);
        if(justify==DT_CENTER) style = ES_CENTER;
        else if(justify==DT_LEFT) style = ES_LEFT;
        else style = DT_RIGHT;
    }	

    if(m_lstColSet[nCol].eEditType == ALLOW)
    {

#define IDC_EDITCELL 1001
        if(m_pEdit != NULL)
        {
            STD_DBG(_T("Warning: Edit box was visible when an item was started to edit.\n"));
            StopEdit();
        }

        style |= WS_CHILD | WS_VISIBLE | ES_AUTOHSCROLL | WS_BORDER;
        m_pEdit = new CVirtualCheckEdit (this, nRow, nCol, text);
        if(m_pEdit)
        {
            m_pEdit->Create (style, Rect, this, IDC_EDITCELL);
            m_pEdit->SetSel( 0, -1);
        }
        return m_pEdit;
    }
    else if (m_lstColSet[nCol].eEditType == COMBO)
    {
#define IDC_COMBOCELL 1002
        if(m_pCombo != NULL)
        {
            STD_DBG(_T("Warning: Conbot box was visible when an item was started to edit.\n"));
            StopEdit();
        }

    	style |= WS_VISIBLE | WS_CHILD | WS_VSCROLL | CBS_DROPDOWNLIST |CBS_NOINTEGRALHEIGHT;

        CRect rcCombo;
        rcCombo = Rect;
        rcCombo.bottom = Rect.bottom + Rect.Height() * 4;
        m_pCombo = new CVirtualCheckCombo (this, nRow, nCol, text);
        m_pCombo->Create (style, rcCombo, this, IDC_COMBOCELL);

        StdString curText = text;
        m_pCombo->SetList(m_lstColSet[nCol].lstCombo, curText);
        m_pCombo->SetItemHeight(  0, Rect.Height());
        //m_pCombo->SetItemHeight( -1, Rect.Height());
        return m_pCombo;
    }
    return NULL;
}
    //
    void PushSubItem (int Item, int Column );

/**
 *  @brief  項目ボタン押下
 *  @param  [in] nRow 
 *  @retval なし 
 *  @note   文字入力による編集開始設定
 */
void CVirtualCheckList::PushSubItem (int nRow, int nCol)
{
    bool  bButton = false;
    CRect crCheck;
    //Has check box?
    if(GetCheckboxRect(nRow, nCol, crCheck, true))
    {
        bButton = true;
    }

    if (m_CallbackChek && bButton)
    {
        m_CallbackChek(m_pParent, nRow, nCol);
    }
}


/**
 *  @brief  項目編集
 *  @param  [in] nRow      行
 *  @param  [in] nCol      列
 *  @param  [in] sData     表示文字列列
 *  @param  [in] bEscape   終了
 *  @param  [in] endkey    最終入力キー  
 *  @retval なし
 *  @note  
 */
void CVirtualCheckList::OnEndEdit(const int nRow, 
                                  const int nCol, 
                                  const CString sData, 
                                  bool bEscape, 
                                  UINT endkey)
{
    m_pEdit = NULL;
    m_editLastEndKey = endkey;

    SetFocus();

    LV_DISPINFO dispinfo;
    dispinfo.hdr.hwndFrom = m_hWnd;
    dispinfo.hdr.idFrom = GetDlgCtrlID();
    dispinfo.hdr.code = LVN_ENDLABELEDIT;

    dispinfo.item.mask = LVIF_TEXT;
    dispinfo.item.iItem = nRow;
    dispinfo.item.iSubItem = nCol;
    dispinfo.item.pszText = bEscape ? NULL : LPTSTR ((LPCTSTR) sData);
    dispinfo.item.cchTextMax = sData.GetLength();

    GetParent()->SendMessage (WM_NOTIFY, GetDlgCtrlID(), (LPARAM) &dispinfo);

    RedrawCol(nRow, nRow, nCol, RD_TEXT);
}

/**
 *  @brief  マウスホイール回転
 *  @param  [in] nFlags  
 *  @param  [in] zDelta  
 *  @param  [in] pt      
 *  @retval なし
 *  @note  
 */
BOOL CVirtualCheckList::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt) 
{
    StopEdit();	
    return CListCtrl::OnMouseWheel(nFlags, zDelta, pt);
}

/**
 *  @brief  垂直スクロール
 *  @param  [in] nSBCode    
 *  @param  [in] nPos       
 *  @param  [in] pScrollBar
 *  @retval なし
 *  @note  
 */
void CVirtualCheckList::OnVScroll(UINT nSBCode, 
                                  UINT nPos, 
                                  CScrollBar* pScrollBar) 
{
    StopEdit();	
    CListCtrl::OnVScroll(nSBCode, nPos, pScrollBar);
}

/**
 *  @brief 水平スクロール
 *  @param  [in] nSBCode    
 *  @param  [in] nPos       
 *  @param  [in] pScrollBar
 *  @retval なし
 *  @note  
 */
void CVirtualCheckList::OnHScroll(UINT nSBCode, 
                                  UINT nPos, 
                                  CScrollBar* pScrollBar) 
{
    StopEdit();

    CListCtrl::OnHScroll(nSBCode, nPos, pScrollBar);
}

/**
 *  @brief  編集中止
 *  @param  [in] cancel 中止の有無
 *  @retval なし 
 *  @note   文字入力による編集開始設定
 */
void CVirtualCheckList::StopEdit(bool bCancel)
{
    if(m_pEdit != NULL)
    {
        m_pEdit->StopEdit(bCancel);
    }
}

/**
 *  @brief  編集中止
 *  @param  [in] cancel 中止の有無
 *  @retval なし 
 *  @note   
 */
void CVirtualCheckList::StopCombo(bool bCancel)
{
    if(m_pCombo != NULL)
    {
        m_pCombo->Stop(bCancel);
    }
}

/**
 *  @brief キー押下
 *  @param  [in] nChar    
 *  @param  [in] nRepCnt       
 *  @param  [in] nFlags
 *  @retval なし
 *  @note  
 */
void CVirtualCheckList::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{

    bool ret = false;
    int nRowS;
    int nColS;
    int nRowE;
    int nColE;

    bool bShift = (::GetKeyState(VK_SHIFT) < 0);

    bool bFoucs = GetFocusCell( &nRowS, &nColS, &nRowE, &nColE);

    if ((!bShift) &&  bFoucs && (nChar != VK_SHIFT))
    {
        RelFocusCell();
    }

    if (bShift)
    {
        if (!bFoucs)
        {
            nRowS = m_iRow;
            nColS = m_iCol;
            nRowE = m_iRow;
            nColE = m_iCol;
            SetFocusCell(m_iRow, m_iCol, m_iRow, m_iCol);
        }
    }
    else
    {
        nRowE = m_iRow;
        nColE = m_iCol;
    }

    int   nCount = GetHeaderCtrl()->GetItemCount();

    if (nCount == 0)
    {
        return;
    }

    std::vector<int> lstOrder;
    lstOrder.resize(nCount);

    GetHeaderCtrl()->GetOrderArray( &lstOrder[0], nCount );
 
    int iColPos = 0;
    bool bFind = false;
    foreach(int iPos, lstOrder)
    {
        if (iPos == nColE)
        {
            bFind = true;
            break;
        }
        iColPos++;
    }

    if (!bFind)
    {
        return;
    }

    int nCol;

    switch (nChar)
    {		
    case VK_LEFT :
    {
        ret = true;
        
        do
        {
            iColPos--;
            if (iColPos < 0)
            {
                iColPos = 0;
            }

            nCol = lstOrder[iColPos];
            if (m_lstColState[nCol].bVisible)
            {
                nColE = nCol;
                break;
            }
        }while(iColPos != 0);
        break;
    }
    case VK_RIGHT :
    {
        ret = true;

        do
        {
            iColPos++;
            if (iColPos >= nCount)
            {
                iColPos = nCount - 1;
            }

            nCol = lstOrder[iColPos];
            if (m_lstColState[nCol].bVisible)
            {
                nColE = nCol;
                break;
            }
        }while(iColPos != (nCount - 1));
        break;
    }

    case VK_UP:
        {
            nRowE--;
            ret = true;
            break;
        }

    case VK_DOWN:
        {
            nRowE++;
            ret = true;
            break;
        }

    case VK_RETURN:
        {
            if(m_editOnEnter)
            {
                EditSubItem(m_iRow /*GetItemInFocus()*/, m_iCol);
                return;
            }
            PushSubItem (m_iRow, m_iCol);
            break;
        }

    case VK_F2:
        {
            EditSubItem(m_iRow /*GetItemInFocus()*/, m_iCol);
            return;
        }
    }

    if ((ret) && (!bShift))
    {
        Select(nRowE, nColE);
    }


    if (bShift)
    {
        if (nRowE < 0){nRowE = 0;}
        if (nRowE >= GetItemCount())
        {
            nRowE = GetItemCount() - 1;
        }

        if (nColE < 0) {nColE = 0;}
        if (nColE >= GetHeaderCtrl()->GetItemCount())
        {
            nColE = GetHeaderCtrl()->GetItemCount() -1;
        }

        MakeColumnVisible(nColE);
        EnsureVisible(nRowE, FALSE);
        SetFocusCell(nRowS, nColS, nRowE, nColE);
    }

    CListCtrl::OnKeyDown(nChar, nRepCnt, nFlags);
}

/**
 *  @brief キー押下
 *  @param  [in] nChar    
 *  @param  [in] nRepCnt       
 *  @param  [in] nFlags
 *  @retval なし
 *  @note  
 */
void CVirtualCheckList::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
    if(	m_editOnWriting && 
        nChar != VK_RETURN && 
        nChar != VK_SPACE && 
        nChar != VK_BACK &&
        nRepCnt >= 0 &&
        nChar >= 32 //Ignore ctrl+a, b and similar
        )
    {
        //DB_PRINT(_T("Edit by writing\n"));
        CString text( (TCHAR) nChar, nRepCnt==0?1:nRepCnt );

        //		EditSubItem(GetItemInFocus(), m_navigationColumn, text);
        EditSubItem(GetItemInFocus(), m_iCol);
        return;
    }
    CListCtrl::OnChar(nChar, nRepCnt, nFlags);
}

/**
 *  @brief  メッセージフィルタ処理
 *  @param  [in] pMsg ウインドウメッセージ
 *  @retval true/false
 *  @note   Returnキー処理
 */
BOOL CVirtualCheckList::PreTranslateMessage(MSG* pMsg) 
{

    if (pMsg->message == WM_KEYDOWN)
    {
        if (pMsg->wParam == VK_RETURN)
        {
            ::TranslateMessage (pMsg);
            ::DispatchMessage (pMsg);
            return TRUE;		    	
        }
    }


     if ((pMsg->message == WM_RBUTTONUP) ||
         (pMsg->message == WM_RBUTTONDOWN))
     {
        if ( pMsg->hwnd == CListCtrl::GetHeaderCtrl()->m_hWnd)
        {
            UINT nFlags = pMsg->wParam;
            CPoint ptMouse(pMsg->lParam); 

            if (pMsg->message == WM_RBUTTONUP)
            {
                OnHeaderRButtonUp(nFlags, ptMouse);
            }
            else
            {
                OnHeaderRButtonDown(nFlags, ptMouse);
            }



            //TODO:ヘッダー幅制限を書く
        }
        else
        {
            m_bHeaderRbuttonDown = false;
        }
     }

#if DRAW_DEBUG_VCHECKLST
    if ( pMsg->hwnd != m_hWnd)
    {
        DB_PRINT_TIME();
         static UINT uiOldMsg2 = 0;
        if (uiOldMsg2 != pMsg->message)
        {
            uiOldMsg2 = pMsg->message;
            DB_PRINT(_T("  %s,%x\n"), CUtil::ConvWinMessage(pMsg->message),
                                                               pMsg->hwnd);
        }
    }
#endif
    

    if ( pMsg->hwnd == /*CListCtrl::GetHeaderCtrl()->*/m_hWnd)
    {
         if (pMsg->message == WM_COMMAND)
         {
            UINT nCode = HIWORD(pMsg->wParam);  //通知コード STN_CLICKED
            UINT nID   = LOWORD(pMsg->wParam);  // コントロールＩＤ
            OnCommand(nCode, nID);
         }

#if DRAW_DEBUG_VCHECKLST
    static UINT uiOldMsg = 0;

    if (uiOldMsg != pMsg->message)
    {
        uiOldMsg = pMsg->message;
//#if 0
        DB_PRINT_TIME();
        if (pMsg->message == WM_MOUSEMOVE)
        {
            UINT x = GET_X_LPARAM(pMsg->lParam);  //通知コード STN_CLICKED
            UINT y = GET_Y_LPARAM(pMsg->lParam);  // コントロールＩＤ
           DB_PRINT(_T("  %s,%x, (X,Y)= (%d, %d)\n"), CUtil::ConvWinMessage(pMsg->message),
                                                           pMsg->wParam, 
                                                           x, y);


        }
        else
        {
        DB_PRINT(_T("  %s,%x,%x\n"), CUtil::ConvWinMessage(pMsg->message),
                                                           pMsg->wParam, 
                                                           pMsg->lParam);
        }
//#endif
    }
#endif

    }

    return CListCtrl::PreTranslateMessage(pMsg);
}

/**
 *  @brief  イベント発生通知.
 *  @param  [in]  wParam 
 *  @param  [in]  lParam 
 *  @param  [out] pResult 
 *  @retval なし     
 *  @note
 */
BOOL CVirtualCheckList::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult) 
{
    HD_NOTIFY * pHdr = reinterpret_cast<HD_NOTIFY *>(lParam);
    switch( pHdr->hdr.code ) 
    {
        case HDN_BEGINTRACKA:
        case HDN_BEGINTRACKW:
        case HDN_DIVIDERDBLCLICKA:
        case HDN_DIVIDERDBLCLICKW:
        {
            //非表示項目の幅変更禁止
            if ((pHdr->iItem > 0) &&
                (pHdr->iItem <= static_cast<int>(m_lstColState.size())))
            {
                *pResult = FALSE;
                if (!m_lstColState[pHdr->iItem].bVisible)
                {
                    *pResult = TRUE;
                }
            }
            return TRUE;
        }
    }
    return CListCtrl::OnNotify(wParam, lParam, pResult);
}


/**
 *  @brief  列表示
 *  @param  [in] nCol 列
 *  @retval なし
 *  @note   列を可視状態にする
 */
void CVirtualCheckList::MakeColumnVisible(int nCol)
{
    if(nCol < 0)
    {
        return;
    }

    CHeaderCtrl* pHeader = GetHeaderCtrl();

    int nColCount = pHeader->GetItemCount();
    ASSERT( nCol < nColCount);

    int *pOrderarray = new int[nColCount];
    Header_GetOrderArray(pHeader->m_hWnd, nColCount, pOrderarray);

    // Get the column offset
    int offset = 0;
    for(int i = 0; pOrderarray[i] != nCol; i++)
    {
        offset += GetColumnWidth(pOrderarray[i]);
    }

    int colwidth = GetColumnWidth(nCol);
    delete[] pOrderarray;

    CRect rect;
    GetItemRect(0, &rect, LVIR_BOUNDS);

    CRect rcClient;
    GetClientRect(&rcClient);
    if(offset + rect.left < 0 || offset + colwidth + rect.left > rcClient.right)
    {
        CSize size(offset + rect.left,0);
        Scroll(size);
        Invalidate(FALSE);
        UpdateWindow();
    }
}

/**
 *  @brief  クリック処理
 *  @param  [in]  pNMHDR 
 *  @param  [out] pResult
 *  @retval なし
 *  @note  
 */
BOOL CVirtualCheckList::OnClickEx(NMHDR* pNMHDR, LRESULT* pResult) 
{
    //ここから調べる
    //m_bDrag = false;
    //ReleaseCapture();
#if DRAW_DEBUG_VCHECKLST
    DB_PRINT_TIME();
    DB_PRINT(_T("OnClickEx\n"));
#endif
    NMLISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;

    m_bDrag = false;

    if (m_bMouseDown)
    {
        m_bMouseDown = false;
#if DRAW_DEBUG_VCHECKLST
    DB_PRINT_TIME();
    DB_PRINT(_T("OnClickEx m_bMouseDown = FALSE\n"));
#endif
        if (m_iMouseDownRow != -1)
        {
            RedrawCol( m_iMouseDownRow, 
                       m_iMouseDownRow, 
                       m_iMouseDownCol, RD_CHECK);
            m_iMouseDownRow = -1;
            m_iMouseDownCol = -1;
        }
    }

    ReleaseCapture();

    return TRUE;
}

/**
 *  @brief  ダブルクリック
 *  @param  [in]  pNMHDR 
 *  @param  [out] pResult
 *  @retval なし
 *  @note  
 */
BOOL CVirtualCheckList::OnDblClickEx(NMHDR* pNMHDR, LRESULT* )
{
    NMLISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;

    int     nRow, nCol;
    bool    bChk;

    if(HitTest(pNMListView->ptAction, &nRow, &nCol, &bChk))
    {
        if(!bChk)
        {
            EditSubItem(nRow,nCol);
        }
    }
    return FALSE;
}

/**
 *  @brief  右クリック
 *  @param  [in]  pNMHDR 
 *  @param  [out] pResult
 *  @retval なし
 *  @note  
 */
BOOL CVirtualCheckList::OnRightClickEx(NMHDR* pNMHDR, LRESULT* )
{
    int iRow;
    int iCol;
    bool bChk;
    NMLISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;

    CPoint point = pNMListView->ptAction;

    if(HitTest(point, &iRow, &iCol, &bChk))
    {
        ClientToScreen(&point);
        if(m_CallbackContextMenu)
        {
            Select(iRow, iCol);
            m_CallbackContextMenu(m_pParent, this, 
                            point, iRow, iCol);
        }
    }
    return TRUE;
}


/**
 * @brief  ヘッダーコントロール上 マウス左ボタン押下
 * @param  [in]    nFlags   仮想キー         
 * @param  [in]    point    マウス座標         
 * @retval true:動作有り false:なし
 * @note	
 */
void CVirtualCheckList::OnHeaderRButtonDown(UINT nFlags, CPoint point)
{
    if (!m_bUserVisibleCol){ return;}

    m_bHeaderRbuttonDown = true;
}


/**
 * @brief  ヘッダーコントロール上  マウス左ボタン押下
 * @param  [in]    nFlags   仮想キー         
 * @param  [in]    point    マウス座標         
 * @retval true:動作有り false:なし
 * @note	
 */
void CVirtualCheckList::OnHeaderRButtonUp(UINT nFlags, CPoint point)
{
    if (!m_bUserVisibleCol){ return;}


    if (m_bHeaderRbuttonDown == true )
    {
        CMenu menu;
        //menu.LoadMenu(IDR_POPUP_DUMMY);
        //CMenu* pSubMenu = menu.GetSubMenu(0);


        menu.CreatePopupMenu();
        StdString strTest;
        int         iMenuId;
        HDITEM      hdrItem;
        enum   { sizeOfBuffer = 256 };
        TCHAR  lpBuffer[sizeOfBuffer];



        memset(&hdrItem, 0, sizeof(hdrItem));
        hdrItem.mask = HDI_WIDTH | HDI_TEXT;
        hdrItem.pszText = lpBuffer;
        hdrItem.cchTextMax = sizeOfBuffer;

        int iColSize = GetHeaderCtrl()->GetItemCount();

        STD_ASSERT(iColSize == static_cast<int>(m_lstColState.size()));

        for (int iCnt = 0; iCnt < iColSize; iCnt++)
        {
            if(!GetHeaderCtrl()->GetItem(iCnt, &hdrItem))
            {
                continue;
            }

            strTest = hdrItem.pszText;
            iMenuId = menu.AppendMenu(MF_STRING, iCnt, strTest.c_str());

            
            if (m_lstColState[iCnt].bVisible)
            {
                 menu.CheckMenuItem(iCnt, MF_BYPOSITION| MF_CHECKED );
            }   
            else
            {
                 menu.CheckMenuItem(iCnt , MF_BYPOSITION| MF_UNCHECKED );
            }
        }

        ClientToScreen(&point);

        //スクロール位置調整
        int iScrollX =  GetScrollPos(SB_HORZ);
        point.x = point.x - iScrollX;

        pHeaderMneu = &menu;

        menu.TrackPopupMenu(
            TPM_LEFTALIGN  |	//クリック時のX座標をメニューの左辺にする
            TPM_RIGHTBUTTON,	//右クリックでメニュー選択可能とする
            point.x,point.y,	//メニューの表示位置
                this            	//このメニューを所有するウィンドウ
        );

        pHeaderMneu = NULL;
        menu.DestroyMenu();
    }
}

/**
 *  @brief  コマンドメッセージ受信
 *  @param  [in] nCode
 *  @param  [in] nCode
 *  @retval なし
 *  @note  
 */
void  CVirtualCheckList::OnCommand(UINT nCode, UINT nID)
{

    HDITEM      hdrItem;
    memset(&hdrItem, 0, sizeof(hdrItem));
    hdrItem.mask = HDI_WIDTH;

    int iColSize = GetHeaderCtrl()->GetItemCount();

    if (m_CallbackOnCommand)
    {
        if (m_CallbackOnCommand(m_pParent, this, nCode, nID))
        {
            return;
        }
    }

    if (iColSize < static_cast<int>(nID))
    {
        return;
    }

    STD_ASSERT(iColSize >= static_cast<int>(nID));

    if (m_lstColState[nID].bVisible)
    {
        m_lstColState[nID].bVisible = false;
        hdrItem.cxy = 0;
        GetHeaderCtrl()->SetItem( nID, &hdrItem);
        //pHeaderMneu->CheckMenuItem(nID, MF_BYPOSITION| MF_UNCHECKED );
    }
    else
    {
        m_lstColState[nID].bVisible = true;
        hdrItem.cxy = m_lstColState[nID].iWidth;
        GetHeaderCtrl()->SetItem( nID, &hdrItem);
        //pHeaderMneu->CheckMenuItem(nID, MF_BYPOSITION| MF_CHECKED );
    }
}

/**
 *  @brief  ヘッダー更新
 *  @param  [in] nCode
 *  @param  [in] nCode
 *  @retval なし
 *  @note   列表示・非表示を
 */
void  CVirtualCheckList::UpdateHeaderVisible()
{
    HDITEM      hdrItem;
    memset(&hdrItem, 0, sizeof(hdrItem));
    hdrItem.mask = HDI_WIDTH;

    int iColSize = GetHeaderCtrl()->GetItemCount();

    STD_ASSERT(iColSize == static_cast<int>(m_lstColSet.size()));

    for (int iCnt = 0; iCnt < iColSize; iCnt++)
    {
        GetHeaderCtrl()->GetItem( iCnt, &hdrItem);

        if (m_lstColSet[iCnt].bVisible)
        {
            if (hdrItem.cxy == 0)
            {
                hdrItem.cxy = m_lstColState[iCnt].iWidth;
                GetHeaderCtrl()->SetItem( iCnt, &hdrItem);
                m_lstColState[iCnt].bVisible = true;
            }
        }
        else
        {
            if (hdrItem.cxy != 0)
            {
                hdrItem.cxy = 0;
                GetHeaderCtrl()->SetItem( iCnt, &hdrItem);
                m_lstColState[iCnt].bVisible = false;
            }
        }
    }
}


/**
 *  @brief フォーカス行取得
 *  @param なし
 *  @retval フォーカスのある行
 *  @note  
 */
int CVirtualCheckList::GetItemInFocus()
{
    return GetNextItem(-1, LVNI_FOCUSED);
}

/**
 *  @brief  マウス移動
 *  @param  [in]  nFlags 
 *  @param  [in]  point
 *  @retval なし
 *  @note   行わない
 */
void CVirtualCheckList::OnMouseMove(UINT nFlags, CPoint point) 
{
    CListCtrl::OnMouseMove(nFlags, point);
    if (!m_bDrag)
    {
        return;
    }
        


    //ドラッグ中
    if (!(nFlags & MK_LBUTTON))
    {
        m_bDrag = false;
        ReleaseCapture();
        return;
    }

    int nRow, nCol;
    nRow = nCol = -1;
    bool bButton = false;
    if(HitTest(point, &nRow, &nCol, &bButton))
    {
       SetFocusCell(m_iFocusRowT, m_iFocusColT, nRow, nCol);
    }	
}

/**
 * @brief  マウス左ボタン押下
 * @param  [in]    nFlags   仮想キー         
 * @param  [in]    point    マウス座標         
 * @retval true:動作有り false:なし
 * @note	
 */
void CVirtualCheckList::OnLButtonDown(UINT nFlags, CPoint point)
{

    m_bMouseDown = true;
    if (!m_bDisableDrag)
    {
        m_bDrag = true;
    }
    //SetCapture();

    if (m_bEnableFocus)
    {
        RelFocusCell();
    }

    int nRow, nCol;
    nRow = nCol = -1;
    bool bButton = false;
    if(HitTest(point, &nRow, &nCol, &bButton))
    {
        Select(nRow, nCol);
        SetFocusCell(nRow, nCol, nRow, nCol, false);

        if(m_lstColSet[nCol].bOwnerDraw)
        {
            if(m_CallOwnerMouse)
            {
                m_CallOwnerMouse(m_pParent, this, nRow, nCol, point, true);
            }
        }


        int iStyle = 0;
        if (nCol > 0)
        {
            iStyle = m_lstColSet[nCol].iStyle;
        }

        if (!(DFCS_INACTIVE & iStyle))
        {
            if (m_CallbackChek && bButton)
            {
                m_CallbackChek(m_pParent, nRow, nCol);
                RedrawCol( nRow, nRow, nCol, RD_CHECK);
            }
        }
    }	
#if DRAW_DEBUG_VCHECKLST
    DB_PRINT_TIME();
    DB_PRINT(_T("OnLButtonDown %d, %d\n"), point.x, point.y);
#endif
    CListCtrl::OnLButtonDown(nFlags, point);
}

/**
 * @brief  マウス左ボタン解放
 * @param  [in]    nFlags   仮想キー         
 * @param  [in]    point    マウス座標         
 * @retval true:動作有り false: 動作なし
 * @note   機能しない 
 */
void CVirtualCheckList::OnLButtonUp(UINT nFlags, CPoint point)
{
    m_bDrag = false;

    if (m_bMouseDown)
    {
        m_bMouseDown = false;

        if(m_CallOwnerMouse)
        {
            int nRow, nCol;
            nRow = nCol = -1;
            bool bButton = false;
            if(HitTest(point, &nRow, &nCol, &bButton))
            {
                if(m_lstColSet[nCol].bOwnerDraw)
                {
                    m_CallOwnerMouse(m_pParent, this, nRow, nCol, point, false);
                }
            }
        }

#if DRAW_DEBUG_VCHECKLST
    DB_PRINT_TIME();
    DB_PRINT(_T("OnLButtonUp m_bMouseDown = FALSE\n"));
#endif
        if (m_iMouseDownRow != -1)
        {
            RedrawCol( m_iMouseDownRow, 
                       m_iMouseDownRow, 
                       m_iMouseDownCol, RD_CHECK);
            m_iMouseDownRow = -1;
            m_iMouseDownCol = -1;
        }
    }

    ReleaseCapture();

#if DRAW_DEBUG_VCHECKLST
    DB_PRINT_TIME();
    DB_PRINT(_T("OnLButtonUp\n"));
#endif

    CListCtrl::OnLButtonUp(nFlags, point);
}

/**
 *  @brief アイテム変更
 *  @param [in]     pNMHDR
 *  @param [out]    pResult
 *  @retval なし
 *  @note   行わない
 */
void CVirtualCheckList::OnItemChanged(NMHDR * pNMHDR, LRESULT *pResult)
{

    *pResult = 0;
    if (m_eSelMode == SEL_LINE)
    {
        return;
    }

    NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
    int iFocus = GetNextItem(-1, LVNI_ALL | LVNI_SELECTED);

#if DRAW_DEBUG_VCHECKLST
    DB_PRINT_TIME();
    DB_PRINT(_T("OnItemChanged %d,\n"), iFocus);
#endif

    if( iFocus == -1 )
    {
        iFocus = GetNextItem(-1, LVNI_ALL | LVNI_FOCUSED);
    }
   
    if( iFocus != -1 ) 
    {
        SetItemState(iFocus,
            ~(LVIS_SELECTED | LVIS_FOCUSED), LVIS_SELECTED | LVIS_FOCUSED);
    }

}

/**
 *  @brief コントロールの動的サブクラス化
 *  @param なし
 *  @retval なし
 *  @note   
 */
void CVirtualCheckList::PreSubclassWindow() 
{
#ifdef _DEBUG
    //Doesn't have LVS_OWNERDATA?
    if(!(GetStyle() & LVS_OWNERDATA))
    {
        ::MessageBox(NULL, _T("ERROR : You must set the LVS_OWNERDATA style for your CVirtualCheckList in the resources"), _T("Error"), MB_OK);
    }

    //Have owner draw fixed?
    if(GetStyle() & LVS_OWNERDRAWFIXED)
    {
        ::MessageBox(NULL, _T("ERROR : You should NOT set the LVS_OWNERDRAWFIXED style for your CVirtualCheckList in the resources"), _T("Error"), MB_OK);
    }

    //Not in report view?
    DWORD style = GetStyle()&LVS_TYPEMASK;

    if(style != LVS_REPORT)
    {
        ::MessageBox(NULL, _T("You should only use CVirtualCheckList in report view. Please change!"), _T("Why not report view?"), MB_OK);
    }
#endif
    CListCtrl::PreSubclassWindow();
}


/**
 *  @brief 強制単一選択
 *  @param  [in] nRow  選択行
 *  @retval なし
 *  @note   
 */
void  CVirtualCheckList::ForceSingleSelect(int nRow ) 
{
#if DRAW_DEBUG_VCHECKLST
    DB_PRINT_TIME();
    DB_PRINT(_T("ForceSingleSelect %d,\n"), nRow);
#endif
    if ((nRow < 0) || nRow >= GetItemCount())
    {
        return;
    }

    int iIndex;
    POSITION pos = GetFirstSelectedItemPosition();
    while( pos != NULL)
    {
        iIndex = GetNextSelectedItem( pos);
        SetItemState( iIndex, 0, LVIS_SELECTED | LVIS_FOCUSED);
    }
    SetItemState( nRow,  LVIS_SELECTED | LVIS_FOCUSED, 
        LVIS_SELECTED | LVIS_FOCUSED);
}

/**
 *  @brief  選択モード設定
 *  @param  [in] eMode   選択モード
 *  @retval なし 
 *  @note   
 */
void CVirtualCheckList::SetSelMode(E_SEL_MODE eMode)
{
    m_eSelMode = eMode;
}


/**
 *  @brief  選択
 *  @param  [in] nRow  選択行
 *  @param  [in] nCol  選択列
 *  @retval なし
 *  @note   
 */
void CVirtualCheckList::Select(int nRow, int nCol)
{
#if DRAW_DEBUG_VCHECKLST
    DB_PRINT_TIME();
    DB_PRINT(_T("Select %d, %d,\n"), nRow, nCol);
#endif
    if ((nRow < 0) ||
        (nRow >= GetItemCount()))
    {
        return;
    }

    if ((nCol < 0) || 
        (nCol >= GetHeaderCtrl()->GetItemCount()))
    {
        return;
    }


    MakeColumnVisible(nCol);
    EnsureVisible(nRow, FALSE);

    m_iRow = nRow;
    m_iCol = nCol;

    if ((m_iOldRow >= 0) && (m_iOldCol>= 0))
    {
       RedrawCol(m_iOldRow, m_iOldRow, m_iOldCol, RD_COL);
    }
    RedrawCol(nRow, nRow, nCol, RD_COL);

    if ((m_iOldRow >= 0))
    {
       RedrawRow(m_iOldRow, true);
    }

    m_iOldRow = nRow;
    m_iOldCol = nCol;

}

/**
 *  @brief  フォーカス設定
 *  @param  [in] nRowT  選択行頭
 *  @param  [in] nColT  選択列頭
 *  @param  [in] nRowB  選択行末
 *  @param  [in] nColB  選択列末
 *  @param  [in] bInvalid  更新の有無
 *  @retval なし
 *  @note   
 */
void CVirtualCheckList::SetFocusCell(int nRowT, int nColT, int nRowB, int nColB, bool bInvalid)
{
#if DRAW_DEBUG_VCHECKLST
    DB_PRINT_TIME();
    DB_PRINT(_T("SetFocusCell %d, %d, %d, %d %c,\n"), nRowT, nColT, nRowB, nColB, bInvalid?_T('T'):_T('F'));
#endif
    m_iFocusRowT = nRowT;
    m_iFocusColT = nColT;
    m_iFocusRowB = nRowB;
    m_iFocusColB = nColB;

    m_bEnableFocus = true;

    if (m_CallbackFocus)
    {
        m_CallbackFocus(m_pParent, this, nRowT, nColT, nRowB, nColB);
    }

    if (m_iFocusRowOldT == -1)
    {
        m_iFocusRowOldT = m_iFocusRowT;
        m_iFocusColOldT = m_iFocusColT;
        m_iFocusRowOldB = m_iFocusRowB;
        m_iFocusColOldB = m_iFocusColB;
    }

    if( (m_iFocusRowOldT == m_iFocusRowT) &&
        (m_iFocusColOldT == m_iFocusColT) &&
        (m_iFocusRowOldB == m_iFocusRowB) &&
        (m_iFocusColOldB == m_iFocusColB))
    {
        return;
    }

    if (bInvalid)
    {
        CRect rcFocus;
        GetFocusRectl(&rcFocus);
        InvalidateRect( &rcFocus, false );
    }

    m_iFocusRowOldT = m_iFocusRowT;
    m_iFocusColOldT = m_iFocusColT;
    m_iFocusRowOldB = m_iFocusRowB;
    m_iFocusColOldB = m_iFocusColB;
}

/**
 *  @brief  フォーカス解除
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CVirtualCheckList::RelFocusCell()
{
#if DRAW_DEBUG_VCHECKLST
    DB_PRINT_TIME();
    DB_PRINT(_T("RelFocusCell,\n"));
#endif

    if (!m_bEnableFocus)
    {
        return;
    }
    m_bEnableFocus = false;

    CRect rcFocus;
    GetFocusRectl(&rcFocus);
    InvalidateRect( &rcFocus, false );

    m_iFocusRowOldT = -1;

}

/**
 *  @brief  フォーカス領域取得
 *  @param  [out] pRect フォーカス領域
 *  @retval なし
 *  @note   
 */
void CVirtualCheckList::GetFocusRectl(RECT* pRect)
{
    RECT  rc1, rc2;
    CRect rcTop;
    CRect rcBottom;
    CRect rcOldTop;
    CRect rcOldBottom;

    GetTextRect(m_iFocusRowT, m_iFocusColT, rcTop);
    GetTextRect(m_iFocusRowB, m_iFocusColB, rcBottom);

    GetTextRect(m_iFocusRowOldT, m_iFocusColOldT, rcOldTop);
    GetTextRect(m_iFocusRowOldB, m_iFocusColOldB, rcOldBottom);

    ::UnionRect(&rc1 , rcTop  , rcBottom);
    ::UnionRect(&rc2 , rcOldTop, rcOldBottom);
    ::UnionRect(pRect, &rc1, &rc2);
}

/**
 *  @brief  フォーカス取得
 *  @param  [out] pRowS フォーカス領域行頭
 *  @param  [out] pColS フォーカス領域列頭
 *  @param  [out] pRowE フォーカス領域行末
 *  @param  [out] pColE フォーカス領域列末
 *  @retval true: フォーカスあり
 *  @note   
 */
bool CVirtualCheckList::GetFocusCell(int* pRowS, int* pColS, int* pRowE, int* pColE)
{
    *pRowS  = m_iFocusRowT;
    *pColS  = m_iFocusColT;
    *pRowE  = m_iFocusRowB;
    *pColE  = m_iFocusColB;

    return m_bEnableFocus;
}

/**
 *  @brief  カレントセル取得
 *  @param  [out] pRow 行
 *  @param  [out] pCol 列
 *  @retval true: フォーカスあり
 *  @note   
 */
void CVirtualCheckList::GetCurCell(int* pRow, int* pCol)
{
    *pRow  = m_iRow;
    *pCol  = m_iCol;
}

/**
 *  @brief  フォーカス判定
 *  @param  なし
 *  @retval true: フォーカスあり
 *  @note   
 */
bool CVirtualCheckList::IsFocus()
{
    return m_bEnableFocus;
}

/**
 *  @brief  クリップ領域設定
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CVirtualCheckList::SetClipCell()
{
    if(!m_bEnableFocus)
    {
        return;   
    }
    
    m_iClipRowT  = m_iFocusRowT;
    m_iClipColT  = m_iFocusColT;
    m_iClipRowB  = m_iFocusRowB;
    m_iClipColB  = m_iFocusColB;
    m_bEnableClip = true;

    CRect rcFocus;
    GetFocusRectl(&rcFocus);
    InvalidateRect( &rcFocus, false );
}

/**
 *  @brief  クリップ領域取得
 *  @param  [out] pRowS クリップ領域行頭
 *  @param  [out] pColS クリップ領域列頭
 *  @param  [out] pRowE クリップ領域行末
 *  @param  [out] pColE クリップ領域列末
 *  @retval true: クリップあり
 *  @note   
 */
bool CVirtualCheckList::GetClipCell(int* pRowS, 
                                    int* pColS, 
                                    int* pRowE, 
                                    int* pColE)
{
    *pRowS = m_iClipRowT;
    *pColS = m_iClipColT;
    *pRowE = m_iClipRowB;
    *pColE = m_iClipColB;
    
    return m_bEnableClip;
}

/**
 *  @brief  クリップ領域解除
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CVirtualCheckList::RelClipCell()
{
    CRect rcFocus;
    CRect rcTop;
    CRect rcBottom;

    m_bEnableClip = false;

    GetTextRect(m_iClipRowT, m_iClipColT, rcTop);
    GetTextRect(m_iClipRowB, m_iClipColB, rcBottom);

    ::UnionRect(&rcFocus , rcTop  , rcBottom);
    InvalidateRect( &rcFocus, false );
}

/**
 *  @brief  アルファブレンド塗りつぶし
 *  @param  [in] hDc
 *  @param  [in] rcFill
 *  @param  [in] crFill
 *  @param  [in] iAlpha
 *  @retval なし
 *  @note   
 */
void CVirtualCheckList::AlphaBlendFill(HDC hDc,
                                       RECT rcFill, 
                                       COLORREF crFill, 
                                       int iAlpha)
{
    BLENDFUNCTION bf;
    bf.BlendOp = AC_SRC_OVER;
    bf.BlendFlags = 0;
    bf.SourceConstantAlpha = BYTE(255.0 * (double(iAlpha) / 100.0));
    bf.AlphaFormat = AC_SRC_ALPHA;

    int iWidth = rcFill.right  - rcFill.left;
    int iHeight= rcFill.bottom - rcFill.top;

    HBITMAP hBmp = CreateCompatibleBitmap(hDc, iWidth, iHeight);
    HDC     hBuf = CreateCompatibleDC(hDc);

    CRect  rcFillBase(0,0,iWidth, iHeight);
    CBrush brFill(crFill);

    SelectObject(hBuf, hBmp);
    FillRect(hBuf, rcFillBase, brFill);

    /*
    AlphaBlend(hDc, rcFill.left,
                    rcFill.top,
                    iWidth,
                    iHeight,
                    hBuf, 0, 0, iWidth, iHeight, bf);
    */
    BitBlt(hDc, rcFill.left,
                rcFill.top,
                iWidth,
                iHeight,
                hBuf, 0, 0, PATINVERT);
    DeleteDC(hBuf);
    DeleteObject(hBmp);
}


/**
 *  @brief  ウインドウフォーカス設定
 *  @param  [in] pOldWnd  今までフォーカスがあったウインドウ
 *  @retval なし
 *  @note   
 */
void CVirtualCheckList::OnSetFocus(CWnd* pOldWnd)
{
	CListCtrl::OnSetFocus(pOldWnd);
}

/**
 *  @brief  ウインドウフォーカス開放
 *  @param  [in] pNewWnd  これからフォーカスを設定するウインドウ
 *  @retval なし
 *  @note   
 */
void CVirtualCheckList::OnKillFocus(CWnd* pNewWnd)
{
    if (m_bMouseDown)
    {
        m_bMouseDown = false;
        if (m_iMouseDownRow != -1)
        {
            RedrawCol( m_iMouseDownRow, 
                       m_iMouseDownRow, 
                       m_iMouseDownCol, RD_CHECK);
            m_iMouseDownRow = -1;
            m_iMouseDownCol = -1;
        }
    }
	CListCtrl::OnSetFocus(pNewWnd);
}



//!<  ウインドウプロシジャ
LRESULT CVirtualCheckList::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
    return CListCtrl::WindowProc(message, wParam, lParam);
}


//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
#include "Utility/CUtility.h"
#include "Utility/TestDlg.h"

namespace TEST_VIRTUAL_CHECK
{

using namespace boost::assign;

//!< ダイアログ
CTestDlg* g_TestDlg;

enum TEST_COL
{
    COL_1 = 0,
    COL_2 ,
    COL_3 ,
    COL_4 ,
    COL_5 ,
    COL_6 ,
    COL_7 ,
};

enum DUMMY_ENUM
{
    IO_01,
    IO_02,
    IO_03,
    IO_04,
    IO_05,
    IO_06,
};


std::map<DUMMY_ENUM, StdString> g_mapCol2Str = 
  map_list_of(IO_01,_T("IO_01"))
             (IO_02,_T("IO_02"))
             (IO_03,_T("IO_03"))
             (IO_04,_T("IO_04"))
             (IO_05,_T("IO_05"))
             (IO_06,_T("IO_06"));


//テスト用データ構造体
struct DUMMY_DATA
{
    int             iId;            // COL_1
    StdString       strName;        // COL_2
    StdString       strSub;         // COL_3
    double          dVal;           // COL_4
    COLORREF        crColor;        // COL_5
    DUMMY_ENUM      colData;        // COL_6
    bool            bCheck;         // COL_7
};

std::vector<DUMMY_DATA> g_lstDymmy;
int                     g_iCurRow;

//-------------------------
// データ初期化
//-------------------------
void InitData(std::vector<DUMMY_DATA>* pData)
{
    g_iCurRow = 0;
    pData->clear();

    DUMMY_DATA datDummy;

    for (int iCnt = 0; iCnt < 200; iCnt++)
    {
        datDummy.iId        = iCnt + 1;
        datDummy.strName    = CUtil::StrFormat(_T("Name%03d"), iCnt);
        datDummy.strSub     = CUtil::StrFormat(_T("Sub%03d"), iCnt);
        datDummy.dVal       = iCnt * 0.02;
        datDummy.crColor    = ::GetSysColor(COLOR_WINDOWFRAME);
        datDummy.colData    = IO_01;
        datDummy.bCheck     = false;
         
        g_lstDymmy.push_back(datDummy);
    }
}

// ダイアログコールバック
//-------------------
// ダイアログ初期化
//-------------------
void TestOnInitDialog(CWnd* pTestObj)
{
    CVirtualCheckList* pList;
    pList = dynamic_cast<CVirtualCheckList*>(pTestObj);

    pList->Create( LVS_REPORT | LVS_ALIGNLEFT | LVS_OWNERDATA | WS_CHILD | 
                       WS_BORDER | WS_VISIBLE |WS_TABSTOP, CRect(), g_TestDlg, 10); 

    ListView_SetExtendedListViewStyleEx(pList->m_hWnd, LVS_EX_FULLROWSELECT, LVS_EX_FULLROWSELECT);
    ListView_SetExtendedListViewStyleEx(pList->m_hWnd, LVS_EX_GRIDLINES, LVS_EX_GRIDLINES);
    ListView_SetExtendedListViewStyleEx(pList->m_hWnd, LVS_EX_HEADERDRAGDROP, LVS_EX_HEADERDRAGDROP);


    
    pList->InsertColumn ( COL_1,   _T("001"),  LVCFMT_LEFT, 50);
    pList->InsertColumn ( COL_2,   _T("002"),  LVCFMT_RIGHT, 50);
    pList->InsertColumn ( COL_3,   _T("003"),  LVCFMT_CENTER, 100);
    pList->InsertColumn ( COL_4,   _T("004"),  LVCFMT_LEFT, 100);
    pList->InsertColumn ( COL_5,   _T("005"),  LVCFMT_LEFT, 100);
    pList->InsertColumn ( COL_6,   _T("006"),  LVCFMT_LEFT, 100);
    pList->InsertColumn ( COL_7,   _T("007"),  LVCFMT_LEFT, 100);


    //-----------
    //項目設定
    //-----------
    CVirtualCheckList::COL_SET ColSet;

    //COL_1
    ColSet.iStyle = DFCS_BUTTONRADIO;
    ColSet.bColor   = false;                      //背景色不使用
    ColSet.bDraw    = true;                       //ボタン描画
    ColSet.eEditType = CVirtualCheckList::NONE;  //編集不可
    pList->SetCol(COL_1, ColSet);


    //COL_2
    ColSet.iStyle   = 0;
    ColSet.bColor   = false;                    //背景色不使用
    ColSet.bDraw    = false;                    //ボタンなし
    ColSet.eEditType = CVirtualCheckList::ALLOW; //編集可
    pList->SetCol(COL_2, ColSet);

    //COL_3
    ColSet.iStyle   = 0;
    ColSet.bColor   = false;                    //背景色不使用
    ColSet.bDraw    = false;                    //ボタンなし
    ColSet.eEditType = CVirtualCheckList::ALLOW; //編集可
    pList->SetCol(COL_3, ColSet);

    //COL_4
    ColSet.iStyle   = 0;
    ColSet.bColor   = false;                    //背景色不使用
    ColSet.bDraw    = false;                    //ボタンなし
    ColSet.eEditType = CVirtualCheckList::ALLOW; //編集可
    pList->SetCol(COL_4, ColSet);

    //COL_5
    ColSet.iStyle   = DFCS_BUTTONPUSH;
    ColSet.bColor   = true;                     //背景色不使用
    ColSet.bDraw    = true;                     //ボタン描画
    ColSet.eEditType = CVirtualCheckList::NONE; //編集不可
    pList->SetCol(COL_5, ColSet);

    //COL_6
    ColSet.iStyle   = 0;
    ColSet.bColor   = false;                        //背景色不使用
    ColSet.bDraw    = false;                        //ボタンなし
    ColSet.eEditType = CVirtualCheckList::COMBO;    //コンボボックス
    ColSet.lstCombo  += g_mapCol2Str[IO_01],g_mapCol2Str[IO_02],
                        g_mapCol2Str[IO_03],g_mapCol2Str[IO_04],
                        g_mapCol2Str[IO_05],g_mapCol2Str[IO_06];

    pList->SetCol(COL_6, ColSet);

    //COL_7
    ColSet.iStyle   = DFCS_BUTTONCHECK;
    ColSet.bColor   = false;                    //背景色不使用
    ColSet.bDraw    = true;                     //ボタン描画
    ColSet.eEditType = CVirtualCheckList::NONE; //編集不可
    pList->SetCol(COL_7, ColSet);

    pList->SetItemCount( SizeToInt(g_lstDymmy.size()));
}


//-------------------------
// ラベル編集終了
//-------------------------
void OnLvnEndlabeleditVlst(NMHDR *pNMHDR, LRESULT *pResult)
{
    NMLVDISPINFO *pDispInfo = reinterpret_cast<NMLVDISPINFO*>(pNMHDR);

    *pResult = 0;
	if(pDispInfo->item.pszText == NULL)
	{
        return;
    }

    int nRow = pDispInfo->item.iItem;
    int nCol = pDispInfo->item.iSubItem;

    StdString strData;
    strData = pDispInfo->item.pszText;

    DUMMY_DATA* pDummy;
    pDummy = &g_lstDymmy[nRow];

    switch(nCol)
    {
        case COL_1:
            //pDummy->iId;
            break;

        case COL_2:
            pDummy->strName = strData;
            break;

        case COL_3:
            pDummy->strSub = strData;
            break;

        case COL_4:
            pDummy->dVal = _tstof(strData.c_str());
            break;

        case COL_5:
            //pDummy->crColor;
            break;

        case COL_6:
        {
            std::map<DUMMY_ENUM, StdString>::iterator ite;
            ite = g_mapCol2Str.find(pDummy->colData);
            for(ite  = g_mapCol2Str.begin();
                ite != g_mapCol2Str.end();
                ite++)
            {
                if (ite->second == strData)
                {
                    pDummy->colData = ite->first;
                }
            }
            break;
        }


        case COL_7:
            //pDummy->bCheck;
            break;

    }
}


//-------------------------
// 通知処理
//-------------------------
void OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResul)
{
    NMHDR* pNMHDR = reinterpret_cast<NMHDR*>(lParam);

    if (pNMHDR->code == LVN_ENDLABELEDIT)
    {
        OnLvnEndlabeleditVlst(pNMHDR, pResul);
        return;
    }

    return;
}


// ダイアログコールバック
//-------------------------
// ウインドウサイズ変更
//-------------------------
void TestOnSize(CWnd* pTestObj, UINT nType, int cx, int cy)
{
    CVirtualCheckList* pList;
    pList = dynamic_cast<CVirtualCheckList*>(pTestObj);
}

// ダイアログコールバック
//-------------------------
//  ウインドウプロシジャ
//-------------------------
LRESULT TestWindowProc(CWnd* pTestObj, UINT message, WPARAM wParam, LPARAM lParam)
{
    CVirtualCheckList* pList;
    pList = dynamic_cast<CVirtualCheckList*>(pTestObj);


    LRESULT lRes = 0;
    if (message == WM_NOTIFY)
    {
        OnNotify( wParam, lParam, &lRes);
        return lRes;
    }
    return 0;
}


//-------------------------
// 描画コールバック
//-------------------------
void __cdecl Callbackfunc(LPVOID pParent, int nRow, int nCol, _TCHAR* pData, bool* pbCheck)
{
    DUMMY_DATA* pDummy;
    bool                    bCheck = false;
    CString                 sItem;

    if (g_lstDymmy.size() <= static_cast<size_t>(nRow))
    {
        return;
    }
    pDummy = &g_lstDymmy[nRow];
    switch(nCol)
    {
    case COL_1:
        bCheck = (g_iCurRow == nRow);

        sItem.Format(_T("%03d"), pDummy->iId);
        break;

    case COL_2:
        sItem = pDummy->strName.c_str();
        break;

    case COL_3:
        sItem = pDummy->strSub.c_str();
        break;

    case COL_4:
        sItem.Format(_T("%02.2f"), pDummy->dVal);
        break;

    case COL_5:
         sItem.Format(_T("%d"),pDummy->crColor);
        break;

    case COL_6:
    {
        std::map<DUMMY_ENUM, StdString>::iterator ite;
        ite = g_mapCol2Str.find(pDummy->colData);

        if (ite != g_mapCol2Str.end())
        {
            sItem = ite->second.c_str();
        }
    }

    case COL_7:
        bCheck = g_lstDymmy[nRow].bCheck;
        break;

    default:
        break;
    }
    
    *pbCheck = bCheck;
    lstrcpy (pData, sItem);
}

//-------------------------
// チェックコールバック
//-------------------------
void __cdecl CallbackChek(LPVOID pParent, int nRow, int nCol)
{ 
    CTestDlg* pDlg = reinterpret_cast<CTestDlg*>(pParent);
    CVirtualCheckList* pList = 
        reinterpret_cast<CVirtualCheckList*>(pDlg->GetObj());

    if (nCol == COL_1)
    {
        int iOldRow = g_iCurRow;
        g_iCurRow   = nRow;

        pList->RedrawCol(iOldRow, iOldRow, nCol, CVirtualCheckList::RD_CHECK);
        pList->RedrawCol(nRow, nRow, nCol, CVirtualCheckList::RD_CHECK);
    }
    else if (nCol == COL_5)
    {
        //現在の位置にカラーピックを表示する
         CColorDialog dlgColor(g_lstDymmy[nRow].crColor,        //デフォルトカラー
                               0,                                //カスタマイズフラグ
                               pDlg);                            //親ウィンドウ

        if (dlgColor.DoModal() == IDOK )
        {
            g_lstDymmy[nRow].crColor = dlgColor.GetColor();
        }
        pList->RedrawCol(nRow, nRow, nCol, CVirtualCheckList::RD_TEXT);

    }
    else if (nCol == COL_7)
    {
        //On/Off反転
        g_lstDymmy[nRow].bCheck = (!g_lstDymmy[nRow].bCheck);
    }
}


}//namedpace TEST_VIRTUAL_CHECK


void TEST_CVirtualCheckList()
{
    using namespace TEST_VIRTUAL_CHECK;
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());
    CTestDlg dlgTest;
    g_TestDlg = &dlgTest;

    CVirtualCheckList  list;

    dlgTest.SetObj(&list);
    list.UseVisibleCol();

    // データ初期化
    InitData(&g_lstDymmy);

 
    //コールバック関数設定
    list.SetFunc(Callbackfunc, CallbackChek, g_TestDlg);

    dlgTest.SetOnSize       (TestOnSize);
    dlgTest.SetOnInit       (TestOnInitDialog);
    dlgTest.SetWindowProc   (TestWindowProc);
    dlgTest.EnableAutoFit();

    dlgTest.DoModal();

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));

}
#endif
