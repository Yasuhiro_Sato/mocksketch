/**
 * @brief			VirtualCheckListヘッダーファイル
 * @file		    VirtualCheckList.h
 * @author			Yasuhiro Sato
 * @date			02-2-2009 03:00:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef _VIRTUAL_CHECK_LIST_H_
#define _VIRTUAL_CHECK_LIST_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/*---------------------------------------------------*/
/*      Header files                                 */
/*---------------------------------------------------*/
#include <vector>
//#include "VirtualCheckEdit.h"
//#include "VirtualCheckCombo.h"

/*---------------------------------------------------*/
/*  class                                            */
/*---------------------------------------------------*/
class CVirtualCheckEdit;
class CVirtualCheckCombo;
/*---------------------------------------------------*/
/*      Macros                                       */
/*---------------------------------------------------*/
#define DEFAULTCOLOR		CLR_DEFAULT
#define TRANSPARENTCOLOR	CLR_NONE


/**
 * @class   CVirtualCheckList
 * @brief   チェックボックス付き仮想リストコントロール
 */
class CVirtualCheckList : public CListCtrl
{
public:
    //===================
    // 再描画用設定
    //===================
	enum REDRAW_TYPE
    {
        RD_CHECK,
        RD_COL,
        RD_TEXT,
        RD_COL_ALL,
    };

    //===================
	enum EDIT_TYPE
    {
        NONE,       //!< 編集不可
        ALLOW,      //!< 編集可
        COMBO,      //!< コンボボックス
    };

    //===================
    enum E_SEL_MODE
    {
        SEL_NONE,  //選択なし
        SEL_CELL,  //単一項目
        SEL_LINE,  //行選択
    };


    //===================
    // 列設定
    //===================
    struct COL_SET
    {
        COL_SET()
        {
            iStyle      = DFCS_BUTTONCHECK;
            bDraw       = false;
            bOwnerDraw  = false;
            eEditType   = NONE;
            bColor      = false;
            crDefault   = ::GetSysColor(COLOR_WINDOW);
            bVisible    = true;
        };

        // DFCS_BUTTONCHECK   チャックボックス
        // DFCS_BUTTONRADIO   ラジオボタン
        // DFCS_BUTTONPUSH    プッシュボタン
        int         bOwnerDraw; // オナードロー
        int         iStyle;
        bool        bDraw;      // ボタン表示の有無
        EDIT_TYPE   eEditType;  // 編集種別
        bool        bColor;     // 色変更有無
        COLORREF    crDefault;  // 色変更時のディフォルト色 
        bool        bVisible;   // 表示の有無
        std::vector<StdString> lstCombo; //コンボボックス

    };

    //===================
    // 列状態
    //===================
    struct COL_STATE
    {
        COL_STATE()
        {
            iWidth      = 100;
            bVisible    = true;
        }

        int  iWidth;
        bool bVisible;
    };


public:
    // Construction
	CVirtualCheckList();
	virtual ~CVirtualCheckList();

// Operations
public:
	
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CVirtualCheckList)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

// Implementation
public:

    // チェックボックス領域取得
	bool GetCheckboxRect(const int item, const int subitem, CRect& rect, bool checkOnly, bool* pCheck = NULL);

    // テキスト領域取得
	bool GetTextRect(const int item, const int subitem, CRect& rect);

    // 列挿入
    int InsertColumn(int nCol, LPCTSTR lpszColumnHeading, int nFormat = LVCFMT_LEFT, int nWidth = -1, int nSubItem = -1);

    // 列設定
    BOOL SetCol (int nCol, const COL_SET Set);

    // 列取得
    BOOL GetCol (int nCol, COL_SET* pGet);

    //!< ヘッダー更新
    void  UpdateHeaderVisible();

    // 描画コールバック
    void SetFunc(void (__cdecl *Callbackfunc)(LPVOID pParent, int nRow, int nCol, _TCHAR* pData,  bool* pbChec),  
                 void (__cdecl *CallbackChek)(LPVOID pParent, int nRow, int nCol),
                 LPVOID pParent);

    // 編集前コールバック
    void SetChgFunc(void (__cdecl *CallbackChg)(LPVOID pParent, CVirtualCheckList* pCheckList, 
                                               int nRow, int nCol, const _TCHAR* pData));

    // フォーカスコールバック
    void SetFocusFunc(void (__cdecl *CallFocusChg)(LPVOID pParent, CVirtualCheckList* pCheckList, 
                                               int nRowT, int nColT, 
                                               int nRowB, int nColB));
 
    // コンテキストメニューコールバック
    void SetContextMenuFunc(void (__cdecl *CallContextMenu)
                               (LPVOID pParent, 
                               CVirtualCheckList* pCheckList, 
                               POINT ptClick,
                               int nRow, int nCol));

    // コマンドコールバック
    void SetOnCommandFunc(bool (__cdecl *CallbackOnCommand)  (LPVOID pParent,
                                      CVirtualCheckList* pCheckList,
                                      UINT nCode, UINT nID));
    // オナードローコールバック
    void SetOwnerDrawFunc(void (__cdecl *CallOwnerDraw)
                               (LPVOID pParent, 
                                CVirtualCheckList* pCheckList,
                                int nRow, int nCol, CDC* pDC, 
                                CString sData, bool bCheck));

    // オナーマウスボタンコールバック
    void SetOwnerMouseFunc(void (__cdecl *CallOwnerMouseDraw)
                               (LPVOID pParent, 
                                CVirtualCheckList* pCheckList,
                                int nRow, int nCol, 
                                POINT ptPos, bool bMouseDown));



	// 列再描画
    void RedrawCol(int nCol, REDRAW_TYPE eType);

	// 列再描画
    void RedrawCol(const int nTop, const int nBottom, const int nCol, REDRAW_TYPE eType, BOOL erase=FALSE);

	// 行再描画
    void RedrawRow(int nRow, BOOL erase=FALSE);

    //Enterキーによる編集開始設定
	void SetEditOnEnter(const bool edit);

    //入力編集設定
	void SetEditOnWriting(const bool edit);

    //ヒットテスト
	bool HitTest(const POINT& point, int* pRow, int* pCol, bool* onCheck=NULL);
	
    //項目編集開始
    CWnd* EditSubItem (int Item, int Column );
	
    //編集中止
    void StopEdit(bool bCancel = false);
	
    //編集中止
    void StopCombo(bool bCancel = false);

    //項目ボタン押下
    void PushSubItem (int Item, int Column );

    //編集終了時押下キー取得
	UINT GetLastEndEditKey() const;

    //現在のエディットボックス取得
	CEdit* GetEditBox() const;

    //選択有無判定
	bool IsSelected(const int item, const int subitem);

    //フォーカス行取得
	int GetItemInFocus();

    //強制単一選択
    void ForceSingleSelect(int nRow );

    //選択モード設定
    void SetSelMode(E_SEL_MODE eMode);

    //フォーカス設定
    void SetFocusCell(int nRowS, int nColS, int nRowE, int nColE, bool bInvalid = true);

    //フォーカス取得
    bool GetFocusCell(int* pRowS, int* pColS, int* pRowE, int* pColE);

    //フォーカス設定
    bool IsFocus();

    //フォーカス判定
    void RelFocusCell();

    //カレントセル取得
    void GetCurCell(int* pRow, int* pCol);

    // クリップ領域設定
    void SetClipCell();

    // クリップ領域取得
    bool GetClipCell(int* pRowS, int* pColS, int* pRowE, int* pColE);

    // クリップ領域解除
    void RelClipCell();

    //選択
    void Select(int nRow, int nCol);

    //ユーザーによる列項目設定
    void UseVisibleCol(bool bUse = true ){m_bUserVisibleCol = bUse;}
	// Generated message map functions
protected:
	
	//{{AFX_MSG(CVirtualCheckList)
	afx_msg void OnPaint();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnGetdispinfo(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnGetInfoTip(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
    afx_msg void OnKillFocus(CWnd* pNewWnd);
	//}}AFX_MSG

    afx_msg void OnItemChanged(NMHDR *pNMHDR, LRESULT *pResult);
    //afx_msg void OnLvnHotTrack(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnCustomDraw(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg BOOL OnClickEx(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg BOOL OnDblClickEx(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg BOOL OnRightClickEx(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
    afx_msg void OnLButtonUp(UINT nFlags, CPoint point);

	DECLARE_MESSAGE_MAP()

    virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);

    virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);

    //項目編集
	void OnEndEdit(const int item, const int subitem, const CString text, bool bCancel, UINT endkey);

    //ヘッダーコントロール 右ボタン押下
    void OnHeaderRButtonDown(UINT nFlags, CPoint point);

    //ヘッダーコントロール 右ボタン開放
    void OnHeaderRButtonUp(UINT nFlags, CPoint point);

    //!< コマンドメッセージ受信
    void  OnCommand(UINT nCode, UINT nID);


protected:
	friend CVirtualCheckEdit;

	CVirtualCheckEdit  *m_pEdit;
	bool m_editOnEnter;
	bool m_editOnWriting;
	bool m_editEndOnLostFocus;

    //!< ユーザによる列の表示変更
	bool m_bUserVisibleCol;

    //!< ヘッダー上のボタン押下状態記憶
	bool m_bHeaderRbuttonDown;


	friend CVirtualCheckCombo;
	CVirtualCheckCombo *m_pCombo;


    int  m_navigationColumn;
	UINT m_editLastEndKey;

    //列設定
    std::vector<COL_SET> m_lstColSet;

    //列状態
    std::vector<COL_STATE> m_lstColState;
    

    // コールバック設定有無
    bool m_bSetCallBack;

    // 親ウインドウ
    LPVOID  m_pParent;

    //透過色
    COLORREF m_ctTransparent;

    // 選択行
    int m_iRow;

    // 選択列
    int m_iCol;
    
    // 選択行
    int m_iOldRow;

    // 選択列
    int m_iOldCol;

    // 選択モード
    E_SEL_MODE m_eSelMode;

    // フォーカス
    int m_iFocusRowT;
    int m_iFocusColT;
    int m_iFocusRowB;
    int m_iFocusColB;

    int m_iFocusRowOldT;
    int m_iFocusColOldT;
    int m_iFocusRowOldB;
    int m_iFocusColOldB;

    // フォーカス
    bool m_bEnableFocus;


    // クリップ領域
    int m_iClipRowT;
    int m_iClipColT;
    int m_iClipRowB;
    int m_iClipColB;

    // クリップ領域
    bool m_bEnableClip;

    // ドラッグ
    bool m_bDrag;

    // マウス左ボタン押下
    bool m_bMouseDown;
    int  m_iMouseDownRow;
    int  m_iMouseDownCol;

    // ドラッグ禁止
    bool m_bDisableDrag;

    // ヘッダーメニュー
    CMenu*  pHeaderMneu;

protected:

    //領域塗りつぶし
	inline void FillSolidRect(CDC* pDC, const RECT& rect, const COLORREF color);

	//Try to navigato to a column. Ask parent first
	void TryNavigate(const int newcol);

	//列表示
	void MakeColumnVisible(int nCol);

	//Draw functions
	void DrawItem(int nRow, int nCol, CDC* pDC);

    //テキスト描画
    void DrawText( int nRow, int nCol, CString sData, CDC* pDC);

    //ボタン描画
	void DrawButton( int nRow, int nCol, CDC* pDC);

    //選択枠描画
    void DrawSel( int nRow, int nCol, CDC* pDC);

	//全行選択確認
	bool FullRowSelect();
 
    //文字色取得
	COLORREF GetTextColor(int nRow, int nCol, const bool forceNoSelection=false, const bool forceSelection=false);

    //背景色取得
    COLORREF GetBackColor(int nRow, int nCol, const bool forceNoSelection=false);

    //セル領域取得
	BOOL GetCellRect(int nRow, int nCol, CRect& rect);
	
    // 描画コールバック
    void (__cdecl *m_Callbackfunc)    (LPVOID pParent, int nRow, int nCol, _TCHAR* pData, bool* pbCheck);

    // チェックコールバック
    void (__cdecl *m_CallbackChek)    (LPVOID pParent, int nRow, int nCol);

    // 描画コールバック
    void (__cdecl *m_CallbackChg)    (LPVOID pParent, CVirtualCheckList* pCheckList,int nRow, int nCol, const _TCHAR* pData);

    // フォーカス変更コールバック
    void (__cdecl *m_CallbackFocus)  (LPVOID pParent, CVirtualCheckList* pCheckList,
                                      int nRowT, int nColT, 
                                      int nRowB, int nColB);

    // コンテキストメニューコールバック
    void (__cdecl *m_CallbackContextMenu)  (LPVOID pParent, 
                                      CVirtualCheckList* pCheckList,
                                      POINT ptClick,
                                      int nRow, int nCol);

    // コマンドコールバック
    bool (__cdecl *m_CallbackOnCommand)  (LPVOID pParent,
                                      CVirtualCheckList* pCheckList,
                                      UINT nCode, UINT nID);

    // オーナードロー
    void (__cdecl *m_CallOwnerDraw)   (LPVOID pParent, 
                                       CVirtualCheckList* pCheckList,
                                       int nRow, int nCol, 
                                       CDC* pDC,  CString sData, 
                                       bool bCheck);
    // オーナーマウス
    void (__cdecl *m_CallOwnerMouse)
                                       (LPVOID pParent, 
                                        CVirtualCheckList* pCheckList,
                                        int nRow, int nCol, 
                                        POINT ptPos, bool bMouseDown);

//テキスト表示位置
    UINT GetTextJustify(const int nCol);

    //項目データ取得
    virtual void GetItemData(const int nRow, const int nCol, CString& sData, bool* pbCheck);
	
	//Used by GetItemData. Don't use this directly
	//CListItemData* m_lastget;

    //表示領域取得
	bool MakeInside(int& nTop, int &nBottom);	

    //フォーカス領域取得
    void GetFocusRectl(RECT* pRect);

    void AlphaBlendFill(HDC hDc,RECT rcFill, COLORREF crFill, int iAlpha);

public:
    //afx_msg void OnNMKillfocus(NMHDR *pNMHDR, LRESULT *pResult);
    //afx_msg void OnNMSetfocus(NMHDR *pNMHDR, LRESULT *pResult);
};

#endif //_VIRTUAL_CHECK_EDIT_H_