/**
 * @brief			VirtualTreeCtrl実装ファイル
 * @file			VirtualTreeCtrl.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "VirtualTreeCtrl.h"
#include "VirtualCheckList.h"
#include <math.h>


CVirtualTreeCtrl::CVirtualTreeCtrl():
m_psRoot(NULL)
{
    m_psRoot = new CVirtualTreeCtrlItem;
#ifdef _DEBUG
    Test();
#endif 
}

CVirtualTreeCtrl::~CVirtualTreeCtrl()
{
    delete m_psRoot;
}

/*
 * @brief   子イテム追加
 * @param   [in] pRoot 兄弟アイテム
 * @param   [in] pData 追加データ
 * @param   [in] 展開の有無
 * @retval  追加したアイテム
 * @note    
 */
void CVirtualTreeCtrl::UpdateVisibleItemMap()
{
    CVirtualTreeCtrlItem* pItem;
    int iCnt = -1;
    std::deque<CVirtualTreeCtrlItem*> stack;

    m_mapPos2Item.clear();
    pItem = m_psRoot;

    while(pItem != NULL)
    {
        if (pItem != m_psRoot)
        {
            m_mapPos2Item[iCnt] = pItem;
        }

        if (pItem->psChild &&
            pItem->bExpand)
        {
            iCnt++;
            stack.push_front(pItem);
            pItem = pItem->psChild;
        }
        else if(pItem->psNext)
        {
            iCnt++;
            pItem = pItem->psNext;
        }
        else
        {
            while (stack.size() != 0)
            {
                pItem = stack[0]->psNext;
                stack.pop_front();
                if (pItem != NULL)
                {
                    iCnt++;
                    break;
                }

            }

            if (stack.size() == 0)
            {
                pItem = NULL;
            }

        }
    }
}

/*
 * @brief   子イテム追加
 * @param   [in] pRoot 兄弟アイテム
 * @param   [in] pData 追加データ
 * @param   [in] 展開の有無
 * @retval  追加したアイテム
 * @note    
 */
CVirtualTreeCtrlItem* CVirtualTreeCtrl::AddItem(
                                 CVirtualTreeCtrlItem* pRoot, 
                                 void* pData,
                                 bool bExpand,
                                 bool bUpdate )
{
    boost::recursive_mutex::scoped_lock lock(m_mtxGuird);

    if (pRoot == NULL)
    {
        pRoot = m_psRoot;
    }

    assert (pRoot->psNext == NULL);

    CVirtualTreeCtrlItem* pItem = new CVirtualTreeCtrlItem;


    pItem->pBefore  = pRoot;
    pItem->pData    = pData;
    pItem->iRank    = pRoot->iRank;
    pRoot->psNext   = pItem;

    if (bUpdate)
    {
        UpdateVisibleItemMap();
    }

    return pRoot->psNext;
}

/*
 * @brief   子アイテム追加
 * @param   [in] pRoot 親となるアイテム
 * @param   [in] pData 追加データ
 * @param   [in] 展開の有無
 * @retval  追加したアイテム
 * @note    
 */
CVirtualTreeCtrlItem* CVirtualTreeCtrl::AddChild(
                                 CVirtualTreeCtrlItem* pRoot, 
                                 void* pData,
                                 bool bExpand,
                                 bool bUpdate)
{
    boost::recursive_mutex::scoped_lock lock(m_mtxGuird);

    if (pRoot == NULL)
    {
        pRoot = m_psRoot;
    }

    assert (pRoot->psChild == NULL);

    CVirtualTreeCtrlItem* pItem = new CVirtualTreeCtrlItem;


    pItem->pParent  = pRoot;
    pItem->bExpand  = bExpand;
    pItem->iRank    = pRoot->iRank + 1;
    pItem->pData   = pData;
    pRoot->psChild  = pItem;
    if (bUpdate)
    {
        UpdateVisibleItemMap();
    }
    return pRoot->psChild;
}

/*
 * @brief   表示アイテム数取得
 * @param   なし
 * @retval  表示アイテム数
 * @note    VirtualCheckリストで表示するアイテム数
 *          (ツリーを畳んで表示していない部分は数えない)
 */
int CVirtualTreeCtrl::GetVisibleItemNum()
{
    boost::recursive_mutex::scoped_lock lock(m_mtxGuird);
    return static_cast<int>(m_mapPos2Item.size());
}

/*
 * @brief   ツリー展開
 * @param   [in] pItem    表示アイテム番号
 * @param   [in] bExpand  true:展開
 * @retval  なし
 * @note
 */
bool  CVirtualTreeCtrl::Expand
        (CVirtualTreeCtrlItem* pItem,
         bool bExpand)
{
    boost::recursive_mutex::scoped_lock lock(m_mtxGuird);

    if (!pItem)
    {
        return false;
    }
    pItem->bExpand  = bExpand;
    UpdateVisibleItemMap();
    return true;
}

/*
 * @brief   アイテム取得
 * @param   [in] iNo 表示アイテム番号
 * @retval  なし
 * @note    GetVisibleItemNumで取得した分のアイテムを
 *          取得するのに使う
 */
CVirtualTreeCtrlItem* CVirtualTreeCtrl::GetItem(int iNo)
{
    boost::recursive_mutex::scoped_lock lock(m_mtxGuird);
    std::map<int, CVirtualTreeCtrlItem*>::iterator ite;
    ite = m_mapPos2Item.find(iNo);

    if (ite == m_mapPos2Item.end())
    {
        return NULL;
    }

    return ite->second;
}

/*
 * @brief   全消去
 * @param   なし
 * @retval  なし
 * @note
 */
void CVirtualTreeCtrl::AllClear()
{
    boost::recursive_mutex::scoped_lock lock(m_mtxGuird);
    if(m_psRoot)
    {
        m_psRoot->Delete();
        UpdateVisibleItemMap();
    }
}   


bool CVirtualTreeCtrl::GetItemTreeButtonRect( CRect* pRect ,
                                              CVirtualCheckList* pVList,
                                                  int nRow,
                                                  int iRank)
{
    CRect rcTree;

    if(!pVList->GetTextRect(nRow, 0, rcTree))
    {
        return false;
    }

    if (rcTree.Height() < 4)
    {
        return false;
    }

    int iButtonSize = rcTree.Height() / 3;
    int iRet;

	CPoint ptCenter;
	ptCenter.x = rcTree.left + (iButtonSize * 2) * (iRank );
	ptCenter.y = rcTree.top	 + (iButtonSize * 2);
    iRet = ptCenter.x + iButtonSize;

	CRect rcButton;
	pRect->SetRect( -iButtonSize, -iButtonSize, iButtonSize+1, iButtonSize+1 );
	pRect->OffsetRect( ptCenter );

    return true;
}

int CVirtualTreeCtrl::DrawItemTreeButton( CDC* pDC, 
                                           CVirtualCheckList* pVList,
                                           int nCol,
                                           int nRow, 
                                           int iRank, 
                                           E_TREE_BUTTON_TYPE eType )
{

    int iRet = 0;
    CRect rcButton;
    if(! GetItemTreeButtonRect( &rcButton , pVList, nRow, iRank))
    {
        return iRet;
    }



	CBrush brButton;
    COLORREF crButton(0x808080);	
    COLORREF crFill  (0x000000);	

    brButton.CreateSolidBrush( crButton );

    if ( !(pVList->GetStyle()&WS_DISABLED) )
    {
	    crFill = 0x00FFFFFF ;
    }

    CPoint ptCenter( rcButton.left + int(::floor(rcButton.Width() / 2.0)),
                     rcButton.top  + int(::floor(rcButton.Height() / 2.0)));

	rcButton.DeflateRect( 2, 2, 2, 2 );

	// draw plus button [+]
	if( eType == TBT_PLUS )
	{
	    pDC->FillSolidRect( rcButton, crFill );
        pDC->FrameRect( rcButton, &brButton );
		CPen Pen, *pOldPen;
		Pen.CreatePen( PS_SOLID, 1, (COLORREF)0x000000 );
		pOldPen = pDC->SelectObject( &Pen );
        pDC->MoveTo( rcButton.left, ptCenter.y  );
		pDC->LineTo( rcButton.right, ptCenter.y );
		pDC->MoveTo( ptCenter.x, rcButton.top );
		pDC->LineTo( ptCenter.x, rcButton.bottom );
		pDC->SelectObject( pOldPen );
	}
	// draw minus button [-]
	else if( eType == TBT_MINUS)
	{
	    pDC->FillSolidRect( rcButton, crFill );
        pDC->FrameRect( rcButton, &brButton );
		CPen Pen, *pOldPen;
		Pen.CreatePen( PS_SOLID, 1, (COLORREF)0x000000 );
		pOldPen = pDC->SelectObject( &Pen );
		pDC->MoveTo( rcButton.left, ptCenter.y );
		pDC->LineTo( rcButton.right, ptCenter.y );
		pDC->SelectObject( pOldPen );
	}

	::ExtSelectClipRgn( pDC->m_hDC, NULL, RGN_COPY );
    iRet = rcButton.right + 2;
    return iRet;
}

void CVirtualTreeCtrl::DrawItemProgressBar( CDC* pDC, 
                                 const CRect& rcBar,
                                 int iParcent, 
                                 CString strText)
{

    CBrush brListback( GetSysColor(COLOR_WINDOW));
    CBrush brFrame( GetSysColor(COLOR_WINDOWTEXT));
    CBrush brIndcate( RGB(100,255,100));


    pDC->FillRect(rcBar, &brListback);

    CRect rcIndcate;
    
    if(iParcent > 100)
    {
        iParcent = 100;
    }
    else if(iParcent < 0)
    {
        iParcent = 0;
    }

    if(rcBar.Width() < 3)
    {
        return;
    }

    int iWidth;
    iWidth = (rcBar.Width() * iParcent) / 100;

    rcIndcate = rcBar;
    rcIndcate.left = rcBar.right - iWidth;

    pDC->FillRect(rcBar, &brListback);


    UINT textFormat=0;
    textFormat = DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS | DT_LEFT;

    RECT rcTmp = rcBar;
    rcTmp.left += 5;

    //-----------------------
    //int iOldRop    = pDC->GetROP2();
    //pDC->SetROP2(R2_NOTMASKPEN);
    //CBrush* pOldBlush = pDC->SelectObject(&brIndcate);
    //pDC->Rectangle(rcIndcate);
    //pDC->SelectObject(pOldBlush);
    //pDC->SetROP2(iOldRop);
    //--------------------
    
    pDC->FillRect(rcIndcate, &brIndcate);
    pDC->FrameRect(rcBar, &brFrame);

    int iOldBkMode    = pDC->GetBkMode();
    pDC->SetBkMode(TRANSPARENT);
    pDC->DrawText(strText, &rcTmp, textFormat);
    pDC->SetBkMode(iOldBkMode);

}


#ifdef _DEBUG
void CVirtualTreeCtrl::Test()
{
    std::vector<std::wstring> lstStr;

    lstStr.push_back(std::wstring(_T("1")));         // 00
    lstStr.push_back(std::wstring(_T("1-1")));       // 01
    lstStr.push_back(std::wstring(_T("1-2")));       // 02
    lstStr.push_back(std::wstring(_T("1-3")));       // 03
    lstStr.push_back(std::wstring(_T("1-2-1")));     // 04
    lstStr.push_back(std::wstring(_T("1-2-2")));     // 05
    lstStr.push_back(std::wstring(_T("2")));         // 06
    lstStr.push_back(std::wstring(_T("3")));         // 07
    lstStr.push_back(std::wstring(_T("3-1")));       // 08
    lstStr.push_back(std::wstring(_T("3-2")));       // 09

    CVirtualTreeCtrlItem* pItem1;
    CVirtualTreeCtrlItem* pItem1_2;
    CVirtualTreeCtrlItem* pItem2;
    CVirtualTreeCtrlItem* pItem3;
    CVirtualTreeCtrlItem* pItem;

    pItem1   = AddChild  (NULL     , &lstStr[0], true);// 00_T("1")));         
    pItem    = AddChild  (pItem1   , &lstStr[1], true);// 01_T("1-1")));       
    pItem1_2 = AddItem   (pItem    , &lstStr[2], true);// 02_T("1-2")));       
    pItem    = AddItem   (pItem1_2 , &lstStr[3], true);// 03_T("1-3")));       
    pItem    = AddChild  (pItem1_2 , &lstStr[4], true);// 04_T("1-2-1")));     
    pItem    = AddItem   (pItem    , &lstStr[5], true);// 05_T("1-2-2")));     
    pItem2   = AddItem   (pItem1   , &lstStr[6], true);// 06_T("2")));         
    pItem3   = AddItem   (pItem2   , &lstStr[7], true);// 07_T("3")));         
    pItem    = AddChild  (pItem3   , &lstStr[8], true);// 08_T("3-1")));       
    pItem    = AddItem   (pItem    , &lstStr[9], true);// 09_T("3-2")));       
    
    int iVisibleCnt = GetVisibleItemNum();
    //assert (iVisible == 10);

    CVirtualTreeCtrlItem* pCtrlItem;
    std::wstring* pStr;
    for (int iCnt = 0; iCnt <  iVisibleCnt; iCnt++)
    {
        pCtrlItem = GetItem(iCnt);
        pStr = reinterpret_cast<std::wstring*>(pCtrlItem->pData);

        if (pStr)
        {
            DB_PRINT(_T("%03d %02d %s\n"), iCnt, pCtrlItem->iRank, pStr->c_str());
        }
        else
        {
            DB_PRINT(_T("%03d %02d ---\n"), iCnt, pCtrlItem->iRank);
        }
    }

    Expand(pItem1_2, false);
    iVisibleCnt = GetVisibleItemNum();
    for (int iCnt = 0; iCnt <  iVisibleCnt; iCnt++)
    {
        pCtrlItem = GetItem(iCnt);
        pStr = reinterpret_cast<std::wstring*>(pCtrlItem->pData);

        if (pStr)
        {
            DB_PRINT(_T("%03d %02d %s\n"), iCnt, pCtrlItem->iRank, pStr->c_str());
        }
        else
        {
            DB_PRINT(_T("%03d %02d ---\n"), iCnt, pCtrlItem->iRank);
        }
    }

    AllClear();
    return;
}
#endif
