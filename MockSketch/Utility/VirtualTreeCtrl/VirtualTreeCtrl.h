/**
 * @brief			VirtualTreeCtrlヘッダーファイル
 * @file			VirtualTreeCtrl.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *	VirtualCheckリストにツリー表示を追加する時に使用
 *  
 *
 * $
 * $
 * 
 */


/*---------------------------------------------------*/
/*  class                                            */
/*---------------------------------------------------*/
class CVirtualTreeCtrl;
class CVirtualCheckList;


enum E_TREE_BUTTON_TYPE
{
    TBT_NONE,
    TBT_PLUS,
    TBT_MINUS,
};

/**
 * @class   CVirtualTreeCtrlItem
 * @brief   CVirtualTreeCtrlの1項目
 */
class CVirtualTreeCtrlItem
{
friend CVirtualTreeCtrl;
public:
    CVirtualTreeCtrlItem* pParent;
    CVirtualTreeCtrlItem* psChild;
    CVirtualTreeCtrlItem* pBefore;
    CVirtualTreeCtrlItem* psNext;
    void*         pData;
    int           iRank;
protected:
    bool          bExpand;

public:
    CVirtualTreeCtrlItem():
    pParent (NULL),
    psChild (NULL),
    pBefore (NULL),
    psNext  (NULL),
    pData   (NULL),
    iRank   (0)
    {;}

    ~CVirtualTreeCtrlItem()
    {
        Delete();
    }

    bool IsExpand(){return bExpand;}

    void Delete()
    {
        if (psChild)
        {
            delete psChild;
            psChild = NULL;
        }

        if (psNext)
        {
            delete psNext;
            psNext = NULL;
        }
    }
};

/**
 * @class   CVirtualTreeCtrl
 * @brief   
 *
 *
 *
 */
class CVirtualTreeCtrl
{
public:

    //!< コンストラクタ
    CVirtualTreeCtrl();

    //!< デストラクタ
    virtual ~CVirtualTreeCtrl();

    //!< アイテム追加
    CVirtualTreeCtrlItem* AddItem
        (CVirtualTreeCtrlItem* pRoot, 
        void* pData, 
        bool bExpand,
        bool bUpdate = true);
    
    //!< 子アイテム追加
    CVirtualTreeCtrlItem* AddChild
        (CVirtualTreeCtrlItem* pRoot, 
        void* pData, 
        bool bExpand,
        bool bUpdate = true);
    
    //!< 表示アイテム数取得
    int  GetVisibleItemNum();

    //!< ツリー展開
    bool Expand(CVirtualTreeCtrlItem* pItem, 
        bool bExpand);

    //!< アイテム取得
    CVirtualTreeCtrlItem* GetItem(int iNo);
    
    //!< 全データクリア
    void AllClear();

    //---------------
    // ツリー描画補助
    //---------------

    static bool GetItemTreeButtonRect( CRect* pRect , 
                                CVirtualCheckList* pVList,
                                int iRank,
                                int nRow);

    static int DrawItemTreeButton( CDC* pDC,
                            CVirtualCheckList* pVList,
                            int nCol,
                            int nRow, 
                            int iRank, 
                            E_TREE_BUTTON_TYPE eType );

    static void DrawItemProgressBar( CDC* pDC, 
                                     const CRect& rcBar,
                                     int iParcent, 
                                     CString strText);


    void UpdateVisibleItemMap();
protected:

#ifdef _DEBUG
    void Test();
#endif

protected:
    CVirtualTreeCtrlItem* m_psRoot;
    std::map<int, CVirtualTreeCtrlItem*> m_mapPos2Item;
    mutable boost::recursive_mutex   m_mtxGuird;

};