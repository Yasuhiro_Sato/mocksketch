/**
 * @brief			CCanvas実装ファイル
 * @file			CCanvas.cpp
 * @author			Yasuhiro Sato
 * @date			29-1-2009 23:18:35
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CCanvas.h"
#include "Utility/CUtility.h"
#include "System/CSystem.h"


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CDotLine::CDotLine():
m_dLestLen(0.0),
m_iDotPos(0),
m_bLestLen(false),
m_iPenStyle(PS_SOLID)
{

    m_lstDotMargin =
                           { {12.0, 3.0},                        //破線     PS_DASH
                             { 3.0, 3.0},                        //点線     PS_DOT 
                             {24.0, 3.0, 6.0, 3.0},              //一点鎖線 PS_DASHDOT
                             {24.0, 3.0, 6.0, 3.0, 6.0, 3.0}};   //２点鎖線 PS_DASHDOTDOT

    m_iPenStyle = PS_SOLID;
    m_iPenWidth = 1;
    m_dDpu = 96.0 /25.4;
    m_bLineAlternate = false;
    m_crBack = DRAW_CONFIG->crFront;
}

CDotLine::~CDotLine()
{

}

void CDotLine::SetLineAlternateMode(bool bSet)
{
    m_bLineAlternate = bSet;
}

void CDotLine::SetBackColor(COLORREF cr)
{
    m_crBack = cr;
}

void CDotLine::Line(HDC hDc, POINT p1, POINT p2, RECT rc)
{
    MoveTo(hDc, p1);
    LineTo(hDc, p2, rc);
}

void CDotLine::SetLineStyle(int iPenStyle, int iWidth, double dDpu)
{
    std::lock_guard<std::mutex> lock(m_Mtx);
    m_dDpu = dDpu;
    if (iWidth < 1)
    {
        iWidth = 1;
    }
    m_iPenWidth = iWidth;


    if (m_iPenStyle != iPenStyle)
    {
        m_dLestLen = 0.0;
        m_bLestLen = false;
        m_iDotPos = 0;
        m_iPenStyle = iPenStyle;
    }
}

void CDotLine::MoveTo(HDC hDc, POINT p)
{
    std::lock_guard<std::mutex> lock(m_Mtx);
    ::MoveToEx(hDc, p.x, p.y, NULL);
    m_ptMoveTo = p; 
    m_dLestLen = 0.0;
    m_iDotPos = 0;
    m_bLestLen = false;
}


void CDotLine::LineTo(HDC hDc, POINT p, RECT rc)
{
    std::lock_guard<std::mutex> lock(m_Mtx);
    if (m_iPenStyle == PS_SOLID)
    {
        ::LineTo(hDc, p.x, p.y);
        m_ptMoveTo = p; 
        return;
    }


    POINT p1 = m_ptMoveTo;
    POINT p2 = p;

    if(!CUtil::Clip(&p1, &p2, rc))
    {
        //表示なし
        ::MoveToEx(hDc, p.x, p.y, NULL);
        m_ptMoveTo = p; 
        return;
    }

    

    double diffx = p2.x - p1.x;
    double diffy = p2.y - p1.y;

    double dL =  sqrt( double ( diffx * diffx +
                                diffy * diffy ));

    if (dL < 1e-10)
    {
        return;
    }


    const std::vector<double>* pLstDot;
    pLstDot = &m_lstDotMargin[m_iPenStyle - 1];

    double dx = diffx / dL;
    double dy = diffy / dL;
    double ptx = p1.x;
    double pty = p1.y;

    double ptNextX;
    double ptNextY;
    int iX = CUtil::Round(ptx);
    int iY = CUtil::Round(pty);

    //::MoveToEx(hDc, iX, iY, NULL);
    //m_ptMoveTo.x = iX; 
    //m_ptMoveTo.y = iY; 


    HPEN   m_penBack;
    HPEN   m_penOld;

    if (m_bLineAlternate)
    {
        m_penBack = ::CreatePen(PS_SOLID, m_iPenWidth, m_crBack);
    }

    double dSpanOfDot = m_iPenWidth;

    double dCurLen = 0.0;
    bool bLoop = true;
    while(bLoop)
    {
        bool bLine = true;
        double dDotLen;
        size_t size = pLstDot->size();
        for(size_t iCnt = m_iDotPos; iCnt < size; iCnt++)
        {
            double dDotFactor = pLstDot->at(iCnt);

            if (m_bLestLen)
            {
                //残りを引き継ぐとき
                dDotLen = m_dLestLen;
                m_bLestLen = false;
            }
            else
            {
                dDotLen =  dDotFactor * dSpanOfDot ;
            }

            ptNextX = ptx + dx * dDotLen;
            ptNextY = pty + dy * dDotLen;

            dCurLen += dDotLen;
            if (dCurLen > dL)
            {
                bLoop = false;
                ptNextX = p2.x;
                ptNextY = p2.y;
                m_dLestLen = dCurLen - dL;
                m_bLestLen = true;
                m_iDotPos = SizeToInt(iCnt);
            }

            iX = CUtil::Round(ptNextX);
            iY = CUtil::Round(ptNextY);

            if ((iCnt % 2) == 0)
            {
                ::LineTo(hDc, iX, iY);
            }
            else
            {
                if(!m_bLineAlternate)
                {
                    ::MoveToEx(hDc, iX, iY, NULL);
                }
                else
                {
                    m_penOld = (HPEN)SelectObject(hDc, m_penBack);
                    ::LineTo(hDc, iX, iY);
                    (HPEN)SelectObject(hDc, m_penOld);
                }
            }
            p1.x = iX; 
            p1.y = iY; 
            ptx = ptNextX;
            pty = ptNextY;

            if (!bLoop)
            {
                break;
            }
        }

        if (!m_bLestLen)
        {
            m_iDotPos = 0;
        }
    }

    if(m_bLineAlternate)
    {
        ::DeleteObject(m_penBack);
    }
    m_ptMoveTo.x = p.x; 
    m_ptMoveTo.y = p.y; 

}


/**
 * コンストラクタ
 */
CCanvas::CCanvas(LPCTSTR name, bool bDirect):
m_hBmp      (0),
h_bmpOld    (0),
m_hDc       (0),
m_iPenStyle (-1),
m_iPenWidth (1),
m_dDivPrecision(0.1),
m_hPen      (0),
m_hOldPen   (0),
m_hRgn      (0),
m_strName   (name),
m_crPen     (DRAW_CONFIG->crFront),
m_crBrush   (DRAW_CONFIG->crFront),
m_bPrint    (false)
{
    COLORREF crBack = DRAW_CONFIG->crBack;
    m_brBack = CreateSolidBrush(crBack);
    m_brFront = CreateSolidBrush(m_crBrush);
    memset( &m_bmpInfo, 0, sizeof( m_bmpInfo ) );
    m_pPen = std::make_unique<Gdiplus::Pen>(Gdiplus::Color(0,0,0),1.0f);
    m_dDpu = 96.0 / 25.4;
    m_dWideToPrintWidth = 1.0;
    m_bDirectMode = bDirect;
    m_dotLine.SetBackColor(crBack);
}


/**
 * デストラクタ
 */
CCanvas::~CCanvas()
{
    Release();
}

/**
 * @brief  解放
 * @param        なし
 * @retval		 なし
 * @note	  
 */
void  CCanvas::Release()
{
    DeleteBitmap();

    if (m_hPen)
    {
        ::DeleteObject(m_hPen);
        m_hPen = 0;
    }

    if (m_brBack)
    {
        ::DeleteObject(m_brBack);
        m_brBack = 0;
    }

    if (m_brFront)
    {
        ::DeleteObject(m_brFront);
        m_brFront = 0;
    }

    if (m_hRgn)
    {
        ::DeleteObject(m_hRgn);
        m_hRgn = 0;
    }

}


/**
 * @brief   背景色変更
 * @param  [in]  crBack  背景色
 * @retval		 なし
 * @note	  
 */
void  CCanvas::SetBack(COLORREF crBack)
{
    if (m_brBack)
    {
        ::DeleteObject(m_brBack);
    }
    m_brBack = CreateSolidBrush(crBack);
    m_dotLine.SetBackColor(crBack);

}

/**
 * @brief   消去
 * @param       なし
 * @retval		なし
 * @note	  
 */
void CCanvas::Clear()
{
    RECT rc;
    rc.top = 0;
    rc.left = 0;
    rc.right = GetWidth();
    rc.bottom = GetHeight();
    ::FillRect(m_hDc, &rc, m_brBack);
    ::SetBkMode(m_hDc , TRANSPARENT);
}

/**
 * @brief   デバイスコンテキスト取得
 * @param       なし
 * @retval		デバイスコンテキスト
 * @note	  
 */
HDC CCanvas::GetDc() const
{
    return m_hDc;
}

HRGN CCanvas::GetRgn() const
{
    return m_hRgn;
}

void CCanvas::SetRgn(HRGN hRgn)
{
    if (m_hRgn)
    {
        ::DeleteObject(m_hRgn);
    }
    m_hRgn = hRgn;
}

/**
 * @brief   画面更新
 * @param       なし
 * @retval		なし
 * @note	  
 */
void CCanvas::Flip(HDC hDc)
{
    if (!m_hDc){return;}
    ::BitBlt(hDc, 0, 0, GetWidth(), GetHeight(),
             m_hDc, 0, 0, SRCCOPY);
}


/**
 * @brief  初期化
 * @param         なし 
 * @retval		  なし
 * @note	
 */
void CCanvas::OnInit()
{
    //m_pImg = std::make_shared<CImage>();


}


//!< 幅取得
long  CCanvas::GetWidth () const
{
    if (m_bPrint)
    {
        return m_szPaper.cx;
    }
    else
    {
        return m_bmpInfo.bmiHeader.biWidth;
    }
}

//!< 高さ取得
long  CCanvas::GetHeight() const
{
    if (m_bPrint)
    {
        return m_szPaper.cy;
    }
    else
    {
        return m_bmpInfo.bmiHeader.biHeight;
    }
}

/**
 * @brief デバイスコンテキスト変更
 * @param [in]    hDc デバイスコンテキスト	 	  
 * @retval		  なし
 * @note	
 */
void CCanvas::ChangeDc(CDC* pDC, bool bPrint)
{
    m_bPrint = bPrint;

    if (m_hDc == pDC->GetSafeHdc())
    {
        return;
    }
    m_hDc = pDC->GetSafeHdc();

    if (m_bPrint)
    {
        //ピクセル単位の画面の幅。
        m_szPaper.cx = pDC->GetDeviceCaps( HORZRES);
        m_szPaper.cy = pDC->GetDeviceCaps( VERTRES);
    }

    m_pGrap = std::make_unique<Gdiplus::Graphics>(m_hDc); 
    int cxInch = pDC->GetDeviceCaps( LOGPIXELSX);

    double dConvFactor = DRAW_CONFIG->pDispUnit->ToBase(1.0);
    m_dDpu = cxInch * (1000.0  * dConvFactor) / 25.4;

     m_dWideToPrintWidth = DRAW_CONFIG->dLineWidthParDot * m_dDpu;
    if (m_dWideToPrintWidth < 1.0)
    {
        m_dWideToPrintWidth = 1.0;
    }

    //ペン再設定
    ::DeleteObject(m_hPen);
    m_hOldPen = (HPEN)::SelectObject(m_hDc, (HGDIOBJ)m_hPen);
    ::SetBkMode(m_hDc , TRANSPARENT);
    SelectObject(m_hDc,GetStockObject(NULL_BRUSH));
}

/**
 * @brief   デバイスコンテキスト変更
 * @param   なし
 * @retval  true 成功
 * @note	
 */
bool CCanvas::DeleteBitmap()
{
    if (m_hBmp)
    {
        if(h_bmpOld)
        {
            ::SelectObject(m_hDc, h_bmpOld);
        }
        ::DeleteObject(m_hBmp);
        m_hBmp = 0;
        h_bmpOld = 0;
    }
    memset( &m_bmpInfo, 0, sizeof( m_bmpInfo ) );
    return true;
}

RECT  CCanvas::GetRect() const
{
    RECT rc;

    rc.top = GetHeight();
    rc.bottom = 0;
    rc.left = 0;
    rc.right = GetWidth();

    return rc;
}

/**
 * @brief   ビットマップ生成
 * @param   [in] iWidth  幅
 * @param   [in] iHeight 使用しない
 * @param   [in] iBitCnt 使用しない
 * @return  なし
 * @note    
 */
bool CCanvas::CreateBmp(int iWidth, int iHeight, int iBitCnt)
{
    if ((iBitCnt != 32)&&
        (iBitCnt != 24))
    {
        return false;
    }

    DeleteBitmap();


    m_bmpInfo.bmiHeader.biSize  = sizeof (BITMAPINFOHEADER);
    m_bmpInfo.bmiHeader.biWidth     = iWidth;
    m_bmpInfo.bmiHeader.biHeight    = iHeight;
    m_bmpInfo.bmiHeader.biPlanes    = 1;
    m_bmpInfo.bmiHeader.biBitCount  = iBitCnt; 
    m_bmpInfo.bmiHeader.biCompression	= BI_RGB;

    int iPitch = ( (((iWidth * iBitCnt)+31)/32)*4 );
    int iSize = iHeight * abs(iPitch);
 
    m_hBmp = ::CreateDIBSection(
              NULL,                     // デバイスコンテキストのハンドル
              &m_bmpInfo,               // ビットマップデータ
              DIB_RGB_COLORS,           // データ種類のインジケータ
              (void**) &m_pPixel,       // ビット値
              NULL,                     // ファイルマッピングオブジェクトのハンドル
              0);                       // ビットマップのビット値へのオフセット


    if (m_hBmp)
    {
        if (!m_hDc)
        {
            m_hDc = ::CreateCompatibleDC(NULL);
            int cxInch = ::GetDeviceCaps(m_hDc, LOGPIXELSX);

            double dConvFactor = DRAW_CONFIG->pDispUnit->ToBase(1.0);
            m_dDpu = cxInch * (1000.0  * dConvFactor) / 25.4;

            m_dWideToPrintWidth = DRAW_CONFIG->dLineWidthParDot * m_dDpu;
            if (m_dWideToPrintWidth < 1.0)
            {
                m_dWideToPrintWidth = 1.0;
            }
            SelectObject(m_hDc,GetStockObject(NULL_BRUSH));
        }
        h_bmpOld = (HBITMAP)::SelectObject(m_hDc, m_hBmp);
        m_pGrap = std::make_unique<Gdiplus::Graphics>(m_hDc);
        return true;
    }
    return false;
}

/**
 * サイズ変更
 * @param [in]    uiType	 	  
 * @param [in]    iX    X位置 (dot)	 	  
 * @param [in]    iY	Y位置 (dot) 	  
 * @retval		  なし
 * @note	
 */
void CCanvas::OnSize( int iX, int iY)
{
    if (iX == 0) 
    {
        iX = 1;
    }

    if (iY == 0)
    {
        iY = 1;
    }

    if (!m_bDirectMode)
    {
        if (CreateBmp(iX, iY, 32 ))
        {
           Clear();
        }
    }
    m_iWidthMax = max(iX, iY);
}


/**
 * ペン設定
 * @param   [in]             
 * @retval      なし
 * @note	
 */
void CCanvas::SetPen(int iPenStyle, int iWidth, COLORREF crPen)
{
    if (m_crPen == crPen){
        if (m_iPenWidth == iWidth){
            if (m_iPenStyle == iPenStyle){
                //ペン変更必要なし
                return ;
            }
        }
    }
#ifdef _CANVAS_DGB_
TRACE(_T("CCanvas::SetPen(%s) %x \n"), m_strName.c_str(), crPen);
#endif

    if (iWidth < 1)
    {
        iWidth = 1;
    }

    if (DRAW_CONFIG->bUsePrintLineWidthOnDisp)
    {
        iWidth = CUtil::Round(m_dWideToPrintWidth * iWidth);
    }

    if (m_hPen != NULL)
    {
        ::DeleteObject(m_hPen);
    }


    if ((iWidth !=1 ) && (iPenStyle != PS_SOLID))
    {
        m_hPen = ::CreatePen(PS_SOLID, iWidth, crPen);
    }
    else
    {
        m_hPen = ::CreatePen(iPenStyle, iWidth, crPen);
    }

    m_dotLine.SetLineStyle(iPenStyle, iWidth, m_dDpu);


    /*
    m_pPen->SetColor(Gdiplus::Color(GetRValue(crPen), 
                                    GetGValue(crPen),
                                    GetBValue(crPen)));

    m_pPen->SetWidth(static_cast<Gdiplus::REAL>(iWidth));

    Gdiplus::DashStyle ds;
    ds = static_cast<Gdiplus::DashStyle>(iPenStyle);
    m_pPen->SetDashStyle(ds);
    */

    if (m_hPen != NULL)
    {
        m_hOldPen = (HPEN)::SelectObject(m_hDc, (HGDIOBJ)m_hPen);
    }
    else
    {
        STD_DBG(_T("m_hPen != NULL \n"));
    }

    //STD_ASSERT(m_hPen != 0);

    m_iPenStyle = iPenStyle;
    m_iPenWidth = iWidth;
    m_crPen     = crPen;
}



/**
 * ペン設定
 * @param   [in]             
 * @retval      なし
 * @note	
 */
void CCanvas::SetPenColor(COLORREF crPen)
{
    if (m_crPen == crPen){
        //ペン変更必要なし
        return ;
    }

    if (m_hPen != NULL)
    {
        ::DeleteObject(m_hPen);
    }


    m_hPen = ::CreatePen(m_iPenStyle, m_iPenWidth, crPen);

    m_pPen->SetColor(Gdiplus::Color(GetRValue(crPen), 
                                    GetGValue(crPen),
                                    GetBValue(crPen)));


    m_hOldPen = (HPEN)::SelectObject(m_hDc, (HGDIOBJ)m_hPen);

    STD_ASSERT(m_hPen != 0);
    m_crPen     = crPen;
}


/**
 * ブラシ設定
 * @param   [in]             
 * @retval      なし
 * @note	
 */
void CCanvas::SetBrush(COLORREF crBrush)
{
    if (m_crBrush == crBrush)
    {
        //ブラシ変更必要なし
        return ;
    }

    if (m_brFront != NULL)
    {
        ::DeleteObject(m_brFront);
    }

    m_brFront = CreateSolidBrush(crBrush);

    STD_ASSERT(m_brFront != 0);
    m_crBrush     = crBrush;
}


void CCanvas::Line(POINT pt1, POINT pt2)
{
    if ( (DRAW_CONFIG->bUseSystemLineType) &&
         (m_iPenWidth == 1))
    {
        ::MoveToEx(m_hDc, pt1.x, pt1.y, NULL);
        ::LineTo(m_hDc, pt2.x, pt2.y);
    }
    else
    {
        m_dotLine.Line(m_hDc, pt1, pt2, GetRect());
    }
}


void CCanvas::MoveTo(POINT pt)
{
    if ( (DRAW_CONFIG->bUseSystemLineType) &&
        (m_iPenWidth == 1))
    {
        ::LineTo(m_hDc, pt.x, pt.y);
    }
    else
    {
        m_dotLine.MoveTo(m_hDc, pt);
    }
}

void CCanvas::LineTo(POINT pt)
{
    if ( (DRAW_CONFIG->bUseSystemLineType) &&
         (m_iPenWidth == 1))
    {
        ::LineTo(m_hDc, pt.x, pt.y);
    }
    else
    {
        m_dotLine.LineTo(m_hDc, pt, GetRect());
    }
}

void CCanvas::Ellipse(RECT rc)
{
    POINT pt;
    pt.x = 0;
    pt.y = 0;
    Arc(rc, pt, pt, true );
}


void CCanvas::FillEllipse(RECT rc)
{
    //int iOldBkColor = ::SetBkColor(m_hDc, m_crBrush);
    int iOldBkMode  = ::SetBkMode(m_hDc, OPAQUE);
    m_brOld  = (HBRUSH)::SelectObject(m_hDc, m_brFront);
    ::Ellipse(m_hDc, rc.left, rc.top, rc.right, rc.bottom);
    SelectObject(m_hDc, m_brOld);
    ::SetBkMode(m_hDc, iOldBkMode);
    //::SetBkColor(m_hDc, iOldBkColor);
}

void CCanvas::Rect(RECT rc)
{
	::Rectangle(m_hDc, rc.left, rc.top, rc.right, rc.bottom);
}


void CCanvas::FillRect(RECT rc)
{
	//int iOldBkColor = ::SetBkColor(m_hDc, m_crBrush);
	int iOldBkMode = ::SetBkMode(m_hDc, OPAQUE);
	m_brOld = (HBRUSH)::SelectObject(m_hDc, m_brFront);
	::Rectangle(m_hDc, rc.left, rc.top, rc.right, rc.bottom);
	SelectObject(m_hDc, m_brOld);
	::SetBkMode(m_hDc, iOldBkMode);
	//::SetBkColor(m_hDc, iOldBkColor);
}


bool CCanvas::_ArcCalcInterSection(POINT2D* pRet, double dW, double dH, POINT2D ptCenter, POINT pt)
{
    /*
    中心ptCenterを通り pt を通る直線と
    dW dHで表される楕円との交点を求める


    (x/w)^2 + (y/h)^2 = 1
    y = ax

    x^2 = w^2* h^2 /(h^2 + w^2* a^2)


    */

    POINT2D ptRet;
    double dW2 = dW * dW;
    double dH2 = dH * dH;

    double dDeltaY1 = double(pt.y) - ptCenter.dY;
    double dDeltaX1 = double(pt.x) - ptCenter.dX;

    

    if (fabs(dDeltaY1) > fabs(dDeltaX1))
    {
        if(fabs(dDeltaY1) < NEAR_ZERO)
        {
            return false;
        }
        /*
           x = b * y
           (x/w)^2 + (y/h)^2 - 1 = 0


        */

        double b =  dDeltaX1 / dDeltaY1;
        double y2 = sqrt(dH2*dW2 / (dW2 + b * b * dH2));

        if (dDeltaY1 < 0)
        {
                y2 = -y2;
        }

        ptRet.dY = y2 + ptCenter.dY;
        ptRet.dX = y2 * b + ptCenter.dX;

    }
    else
    {
        if(fabs(dDeltaX1) < NEAR_ZERO)
        {
            return false;
        }

        double a =  dDeltaY1 / dDeltaX1;
        double x2 = sqrt(dH2*dW2 / (dW2 * a * a + dH2));

        if (dDeltaX1 < 0)
        {
            x2 = -x2;
        }

        ptRet.dY = a * x2  + ptCenter.dY;
        ptRet.dX = x2 + ptCenter.dX;
    }
    *pRet = ptRet;
    return true;
}
void CCanvas::SetDivPrecision(double dDev)
{
    m_dDivPrecision = dDev;
}

void CCanvas::SetLineAlternateMode(bool bSet)
{
    m_dotLine.SetLineAlternateMode(bSet);
}

void CCanvas::Arc(RECT rc, POINT pt1, POINT pt2, bool bClockWise, bool bArcTo)
{
    bool bEllipse = false;

    if ( (pt1. x == pt2.x) &&
         (pt1. y == pt2.y))
    {
        if(!bArcTo)
        {
            bEllipse = true;
        }
    }


    if (((DRAW_CONFIG->bUseSystemLineType) &&
          (m_iPenWidth == 1)) || 
          (m_iPenStyle == PS_SOLID))
    {
        if (bClockWise)
        {
            ::SetArcDirection(m_hDc, AD_CLOCKWISE);
        }
        else
        {
            ::SetArcDirection(m_hDc, AD_COUNTERCLOCKWISE);
        }

        if (bEllipse)
        {
            ::Ellipse(m_hDc, rc.left, rc.top, rc.right, rc.bottom);
        }
        else
        {

           if(bArcTo)
           {
                ::ArcTo(m_hDc, rc.left, rc.top, rc.right, rc.bottom,
                        pt1.x, pt1.y, pt2.x, pt2.y);
           }
           else
           {
                ::Arc(m_hDc, rc.left, rc.top, rc.right, rc.bottom,
                        pt1.x, pt1.y, pt2.x, pt2.y);
           }
        }

        if (bClockWise)
        {
            ::SetArcDirection(m_hDc, AD_COUNTERCLOCKWISE);
        }
    }
    else
    {
        POINT2D ptCenter;
        ptCenter.dX = (rc.right +  rc.left) / 2.0;
        ptCenter.dY = (rc.bottom +  rc.top) / 2.0;
        double dW = (rc.right  -  rc.left) / 2.0;
        double dH = (rc.bottom -  rc.top) / 2.0;;


        //表示許容差(寸法単位系) 
        //単位がmmの場合は、画面上で0.1mmのズレを許容する
        double dPrecDsp = m_dDivPrecision; 

        //許容表示精度(dot数)
        double dPrec = ceil(m_dDpu * dPrecDsp);
        if (dPrec < 1.0)
        {
            dPrec = 1;
        }

        double dR = max(dW, dH);
        if (dR < 10.0){dR = 10.0;}
        if (dR > m_iWidthMax){dR = double(m_iWidthMax);}

        double dStep;

        dStep = acos(1 - dPrec / dR);
        POINT2D pt2D1;
        POINT2D pt2D2;
        double dTeta1;
        double dTeta2;

        if ( bEllipse )
        {
            bClockWise = true;
            pt2D1.dX = rc.right;
            pt2D1.dY = ptCenter.dY;
            pt2D2 = pt2D1;
            if (!bClockWise)
            {
                dTeta1 = 0.0;
                dTeta2 = 2 * M_PI;

            }
            else
            {
                dTeta1 = 2 * M_PI;
                dTeta2 = 0.0;
            }
        }
        else
        {
            bool bRet1 = _ArcCalcInterSection(&pt2D1, dW, dH, ptCenter, pt1);
            bool bRet2 = _ArcCalcInterSection(&pt2D2, dW, dH, ptCenter, pt2);

            if(!bRet1 ||  !bRet2)
            {
                POINT pt;
                pt.x = CUtil::Round( ptCenter.dX);
                pt.y = CUtil::Round( ptCenter.dY);
                m_dotLine.MoveTo(m_hDc, pt);
                return;
            }

            dTeta1 = atan2 ( (pt2D1.dY - ptCenter.dY) / dH, (pt2D1.dX -  ptCenter.dX)/ dW);
            dTeta2 = atan2 ( (pt2D2.dY - ptCenter.dY) / dH, (pt2D2.dX -  ptCenter.dX)/ dW);

            if (dTeta1 < 0)
            {
                dTeta1 += 2 * M_PI;
            }

            if (dTeta2 < 0)
            {
                dTeta2 += 2 * M_PI;
            }
        }

        POINT pt;
        CPen penN;

        if(bClockWise)
        {
            if (dTeta2 < dTeta1)
            {
                dTeta2 += 2 * M_PI;
            }
        }
        else
        {
            if (dTeta2 > dTeta1)
            {
                dTeta1 += 2 * M_PI;
            }
        }

        double dDelte = 2.0 * M_PI / 100.0;

        pt.x = CUtil::Round(dW * cos(dTeta1) + ptCenter.dX);
        pt.y = CUtil::Round(dH * sin(dTeta1) + ptCenter.dY);

        m_dotLine.MoveTo(m_hDc, pt);

        if(bClockWise)
        {
            for (double dTeta = dTeta1; dTeta <= dTeta2; dTeta += dStep)
            {
                pt.x = CUtil::Round(dW * cos(dTeta) + ptCenter.dX);
                pt.y = CUtil::Round(dH * sin(dTeta) + ptCenter.dY);
                m_dotLine.LineTo(m_hDc, pt, GetRect());
            }
        }
        else
        {
            for (double dTeta = dTeta1; dTeta >= dTeta2; dTeta -= dStep)
            {
                pt.x = CUtil::Round(dW * cos(dTeta) + ptCenter.dX);
                pt.y = CUtil::Round(dH * sin(dTeta) + ptCenter.dY);
                m_dotLine.LineTo(m_hDc, pt, GetRect());
            }
        }

        POINT ptLine;
        ptLine.x = CUtil::Round(pt2D2.dX);
        ptLine.y = CUtil::Round(pt2D2.dY);
        m_dotLine.LineTo(m_hDc, ptLine, GetRect());
    }
}

void CCanvas::Polyline(const std::vector<POINT>& lstPoint)
{
    bool bFirst = true;
    for (const POINT& pt: lstPoint)
    {
        if (bFirst)
        {
            MoveTo(pt);
            bFirst = false;
        }
        else
        {
            LineTo(pt);
        }
    }
}

void CCanvas::FillPolyline(const std::vector<POINT>& lstPoint)
{
    int iOldBkMode  = ::SetBkMode(m_hDc, OPAQUE);
    HBRUSH brOld = (HBRUSH)SelectObject(m_hDc, m_brFront);

COLORREF cr = GetDCBrushColor(m_hDc);
    int iR = GetRValue(cr);
    int iG = GetGValue(cr);
    int iB = GetBValue(cr);

    ::Polygon( m_hDc, &lstPoint[0], SizeToInt(lstPoint.size()));
    SelectObject(m_hDc, brOld);
    ::SetBkMode(m_hDc, iOldBkMode);
}

bool CCanvas::Clip(POINT* pPt1, POINT* pPt2)
{
    RECT rc;
    rc.top = GetHeight();
    rc.bottom = 0;
    rc.left   = 0;
    rc.right  = GetWidth();

    return CUtil::Clip(pPt1,  pPt2, rc);
}
