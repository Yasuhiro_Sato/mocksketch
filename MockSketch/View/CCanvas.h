/**
 * @brief			CCanvasヘッダーファイル
 * @file			CCanvas.h
 * @author			Yasuhiro Sato
 * @date			29-1-2009 23:18:34
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#if !defined(EA_A326C4E4_04B6_427c_90FA_B337016D7FD6__INCLUDED_)
#define EA_A326C4E4_04B6_427c_90FA_B337016D7FD6__INCLUDED_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include <mutex>
#include "DrawingObject/Primitive/POINT2D.h"


class CDotLine
{
public:
    CDotLine();
    virtual ~CDotLine();
    void SetLineStyle(int iPenStyle, int iWidth, double dDpu);
    //void SetSpanOfDot(double dDpu);
    void Line(HDC hDc, POINT p1, POINT p2, RECT rc);
    void MoveTo(HDC hDc, POINT p);
    void LineTo(HDC hDc, POINT p, RECT rc);
    void SetLineAlternateMode(bool bSet);
    void SetBackColor(COLORREF cr);

protected:
    COLORREF m_crBack;

    int    m_iPenStyle;
    double m_dLestLen;
    int    m_iDotPos;
    //double m_dSpanOfDot;          //1点線単位当たりのたりのドット数
    bool   m_bLestLen;
    bool       m_bLineAlternate;

    int m_iPenWidth;
    double m_dDpu;

    std::vector< std::vector<double> > m_lstDotMargin; 
    POINT                         m_ptMoveTo;
    std::mutex m_Mtx;
};


/**
 * @class   CCanvas
 * @brief                        
 * @note    
 */
class CCanvas
{
public:

protected:
    friend CCanvas;
    //ビットマップハンドル
    HBITMAP         m_hBmp;
    HBITMAP         h_bmpOld;

    //!< デバイスコンテキスト
    HDC m_hDc;

    std::unique_ptr<Gdiplus::Graphics>    m_pGrap;
    std::unique_ptr<Gdiplus::Pen>         m_pPen;  
    mutable POINT                         m_ptMoveTo;

    //!< ミューテックス
    boost::mutex m_Cs;

    //!< 背景用ブラシ
    HBRUSH  m_brBack; 


    //!< 線種
	int m_iPenStyle;

	//!< 線幅
    int m_iPenWidth;

	//!< 線色
    COLORREF m_crPen;

    //!< ペン
    HPEN    m_hPen;

    //!< ペン
    HPEN        m_hOldPen;

    //!< リージョン
    HRGN        m_hRgn;

    //!< ブラシ色
    COLORREF m_crBrush;

    //!< 塗りつぶし用ブラシ
    HBRUSH  m_brFront; 
    HBRUSH  m_brOld; 

    //!< ビットマップ情報
    BITMAPINFO  m_bmpInfo;

    //!< 点線描画
    CDotLine    m_dotLine;

    //!< ビットフィールド
    BYTE*       m_pPixel;

    double      m_dDpu;
    double      m_dWideToPrintWidth;


    bool       m_bDirectMode;
    int        m_iWidthMax;
    double     m_dDivPrecision;
    StdString  m_strName;
    bool       m_bPrint;
    SIZE       m_szPaper;

public:
	//!< コンストラクタ
	CCanvas(LPCTSTR name, bool bDirect = false);

	//!< デストラクタ
	virtual ~CCanvas();

	//!< 削除
	void  Release();

	//!< 消去
	void Clear();

    //!< デバイスコンテキスト取得
    HDC GetDc() const;

    //!< リージョン取得
    HRGN GetRgn() const;

    void SetRgn(HRGN hRgn);

    //!< 画面更新
	void Flip(HDC hDc);

	//!< サイズ変更
	void OnSize( int iX, int iY);

	//!< ペン設定
	void SetPen(int iPenStyle, int iWidth, COLORREF crPen);

	//!< ブラシ設定
	void SetBrush(COLORREF crBrush);

	//!< ペン色設定
    void SetPenColor(COLORREF crPen);

    //!< 背景色変更
	void  SetBack(COLORREF crBack);

    //!< 幅取得
    long  GetWidth() const ;

    //!< 高さ取得
    long  GetHeight()const;

    RECT  GetRect() const;

    //!< 描画色取得
    COLORREF  GetColor(){return m_crPen;}

    //!< ビットマップ生成
    bool CreateBmp(int iWidth, int iHeight, int iBitCnt);

    //!< ビットマップ生成
    bool DeleteBitmap();

    void SetLineAlternateMode(bool bSet);
    void Line(POINT pt1, POINT pt2);
    void MoveTo(POINT pt);
    void LineTo(POINT pt);
    void Ellipse(RECT rc);
    void FillEllipse(RECT rc);
	void Rect(RECT rc);
	void FillRect(RECT rc);
    void Arc(RECT rc, POINT pt1, POINT pt2, bool bClockWise, bool bArcTo = false);
    void Polyline(const std::vector<POINT>& lstPoint);
    void FillPolyline(const std::vector<POINT>& lstPoint);

    //!< デバイスコンテキスト変更
    void ChangeDc(CDC* pDC, bool bPrint = false);

    StdString  GetName(){return m_strName;}

    void SetDivPrecision(double dDev);
    bool Clip(POINT* pPt1, POINT* pPt2);

        

protected:
    bool _ArcCalcInterSection(POINT2D* ptRet, double dW, double dH, POINT2D ptCenter, POINT pt);

    //!< 初期化処理
    void OnInit();


};
#endif // !defined(EA_A326C4E4_04B6_427c_90FA_B337016D7FD6__INCLUDED_)
