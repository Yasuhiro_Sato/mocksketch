/**
 * @brief			CDrawMarker実装ファイル
 * @file			CDrawMarker.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CDrawMarker.h"
#include "DefinitionObject/View/CDrawingView.h"
#include "DrawingObject/CDrawingObject.h"

#include "View/CLayer.h"
#include "resource.h"
#include "resource2.h"

#include "System/CSystem.h"
/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CDrawMarker::MARKER_DATA::MARKER_DATA()
{
    crDraw = DRAW_CONFIG->crFront;
    ptPos.x = 0;
    ptPos.y = 0;
    ptOffset.x = 0;
    ptOffset.y = 0;
    m_cursor.nCursor = 0;
    bStandard = true;
}



void CDrawMarker::MARKER_DATA::SetCursor(UINT iId)
{
    m_cursor.nCursor = iId;
    bStandard = false;
}

void CDrawMarker::MARKER_DATA::SetCursor(LPCTSTR pId)
{
    m_cursor.pCursor = pId;
    bStandard = true;
}

bool CDrawMarker::MARKER_DATA::IsCursorStr()
{
    return bStandard;
}

UINT CDrawMarker::MARKER_DATA::GetCursor()
{
    return m_cursor.nCursor;
}

LPCTSTR CDrawMarker::MARKER_DATA::SetCursorStr()
{
    return   m_cursor.pCursor;
}

StdString CDrawMarker::MARKER_DATA::Str() const
{
	StdStringStream sOut;
	sOut << StdFormat(_T("CursorId(%d:%s) iID%d pt(%d,%d)"))
		% m_cursor.nCursor
		% m_cursor.pCursor
		% iID
		% ptPos.x % ptPos.y;
	return sOut.str();
}


//!< コンストラクター
CDrawMarker::CDrawMarker()
{
    Init();
}

CDrawMarker::CDrawMarker(CDrawingView* pView)
{
    Init();
    m_pView = pView;
}

//!< デストラクタ
CDrawMarker::~CDrawMarker()
{

}

//!< 初期化
void CDrawMarker::Init()
{
    m_lstBmp[MARKER_POINT].Load(IDB_POINT_MARK);
    m_lstBmp[MARKER_IND].Load(IDB_IND_MARK);
    m_lstBmp[MARKER_TANGENT].Load(IDB_TANGENT_MARK);
    m_lstBmp[MARKER_CENTER].Load(IDB_CENTER_MARK);
    m_lstBmp[MARKER_NORM].Load(IDB_NORM_MARK);
    m_lstBmp[MARKER_SNAP].Load(IDB_POINT_RECT);
    m_lstBmp[MARKER_CROSS].Load(IDB_CROSS_MARK);

    m_pView = NULL;
    m_Dc.m_hDC = NULL;
    m_bHideObject = false;
    m_pSelData = NULL;
     
}

void CDrawMarker::SetView(CDrawingView* pView)
{
    STD_ASSERT(pView != 0);
    m_pView = pView;
}

/**
 * 画面初期化
 * @param   なし         
 * @retval  なし
 * @note	画面のデバイスコンテキストが必要なものは
 * @note	ここで初期化する
 */
void  CDrawMarker::InitDisp(HDC hDc)
{
    STD_ASSERT(m_pView != 0);
    if (m_Dc.m_hDC == NULL )
    {
        m_Dc.Attach(hDc);
    }
    else
    {
        m_Dc.Detach();
        m_Dc.Attach(hDc);
    }
}


//!< データ設定
/**
 * マーカ追加
 * @param   MARKER_DATA [in] マーカデータ         
 * @param   ptObject [in] オブジェクト位置       
 * @retval  マーカID
 * @note	ラジオボタン動作は未設定
 */
int CDrawMarker::AddMarker(MARKER_DATA data, POINT2D ptObject)
{
    //int iLayerId   = m_pView->GetCurrentLayerId();
    //POINT pt;
    //m_pView->ConvWorld2Scr(&pt, ptObject, iLayerId);
    //data.ptPos = pt;
    data.ptObject  =  ptObject;

    int iX;
    int iY;

    if (data.eType < MARKER_BMP_MAX)
    {
        iX = m_lstBmp[data.eType].GetSize().cy;
        iY = m_lstBmp[data.eType].GetSize().cy;
    }
    else
    {
        //iX = iY = DRAW_CONFIG->GetMarkerSize();
        double dLen = DRAW_CONFIG->DimSettingToDispUnit(DRAW_CONFIG->GetMarkerSize());  
        iX = iY =  int(dLen * m_pView->GetDpu());

    }

    data.rcArea.top     = - iY;
    data.rcArea.bottom  =   iY;
    data.rcArea.left    = - iX;
    data.rcArea.right   =   iX;

    data.iID            = static_cast<int>(m_lstMarker.size());

    boost::mutex::scoped_lock look(mtxGuard);
    m_lstMarker.push_back(data);

    return data.iID;
}

/**
 * マーカ追加
 * @param   eType [in] マーカ種別         
 * @param   ptOffset [in] マーカ位置オフセット         
 * @param   ptObject [in] オブジェクト位置       
 * @param   bRadio   [in] ラジオブタン動作         
 * @param   iGroupNo [in] ラジオボタン用グルーブ番号   
 * @param   iGroupNo [in] ラジオボタン用グルーブ番号   
  * @retval  マーカID
 * @note	ラジオボタン動作は未設定
 * @note	
 */
int CDrawMarker::AddMarker(E_MARKER_TYPE eType, POINT   ptOffset, 
                                               POINT2D ptObject, bool bRadio, int iGroupNo,
                                               int iObjectId, int iVal)
{
    MARKER_DATA Data;
    Data.eType = eType;
    Data.ptOffset  =  ptOffset;
    Data.ptObject  =  ptObject;
    Data.iSelect = 0;
    Data.bRadio  = bRadio;
    Data.iRadioGroup = iGroupNo;
    Data.iObjct = iObjectId;
	Data.eSnap = VIEW_COMMON::SNP_NONE;
	Data.iVal = iVal;
    return AddMarker(Data, ptObject);
}

//!< 追加可能オブジェクト設定
void CDrawMarker::AddObject(std::weak_ptr<CDrawingObject> pObj)
{
    boost::mutex::scoped_lock look(mtxGuardListObj);

    m_lstObj.push_back(pObj);
}

//!< 追加可能オブジェクト設定
void CDrawMarker::AddObjectLast(std::weak_ptr<CDrawingObject> pObj)
{
    boost::mutex::scoped_lock look(mtxGuardListObj);
    m_lstObj.push_front(pObj);
}

//!< 追加可能オブジェクト削除
void CDrawMarker::ClearObject()
{
    boost::mutex::scoped_lock look(mtxGuardListObj);
    m_lstObj.clear();
}

//!< 追加可能オブジェクト非表示
void CDrawMarker::HideObject(bool bHide)
{
    m_bHideObject = bHide;
}

//!< 描画
void CDrawMarker::Draw()
{
    HDC hDc = 0;
    DRAWING_MODE eMode = m_pView->GetDrawMode();
    if(eMode & DRAW_FIG)
    {
        hDc = m_pView->GetBackDc();
        InitDisp(hDc);
        _DrawMarker();
    }

    if(eMode & DRAW_SEL)
    {
        hDc = m_pView->GetSelDc();
        InitDisp(hDc);
        _DrawMarker();
    }

    if((eMode & DRAW_FRONT) || 
       (eMode & DRAW_PRINT))
    {
        hDc = m_pView->GetViewDc();
        InitDisp(hDc);
        _DrawMarker();
    }
    _DrawObject();
}

void CDrawMarker::_DrawMarker()
{
    POINT pt;
    int iLayerId   = m_pView->GetCurrentLayerId();

    boost::mutex::scoped_lock look(mtxGuard);
    for( auto& Data: m_lstMarker)
    {

        m_pView->ConvWorld2Scr(&pt, Data.ptObject, iLayerId);
        Data.ptPos = pt;
        pt.x = pt.x + Data.ptOffset.x;
        pt.y = pt.y - Data.ptOffset.y;

        /*
        SIZE szBmp = m_lstBmp[Data.eType].GetSize();
        Data.rcArea.top     = pt.y - szBmp.cy / 2;
        Data.rcArea.bottom  = Data.rcArea.top  + szBmp.cy;
        Data.rcArea.left    = pt.x - szBmp.cy / 2;
        Data.rcArea.right   = Data.rcArea.left + szBmp.cy;
        */

        Draw(Data);
    }
}

void CDrawMarker::_DrawObject()
{
    if (!m_bHideObject)
    {
        //boost::mutex::scoped_lock look(mtxGuardListObj);
        for( auto wObj: m_lstObj)
        {
            auto pObj = wObj.lock();
            if(!pObj)
            {
                continue;
            }

            if (pObj->IsVisible())
            {
                pObj->Draw(m_pView, NULL);

#ifdef _DEBUG
    if (m_pView->GetDebugMode() != CDrawingView::DM_DISABLE)
    {
        DB_PRINT(_T("CDrawMarker::_DrawObject %d %I64x\n"), pObj->GetId(), pObj.get());
    }
#endif
            }
        }
    }
}

//!< 消去
void CDrawMarker::Clear()
{
    boost::mutex::scoped_lock look(mtxGuard);
    m_lstMarker.clear();
}

//!< 
/**
 *  @brief   マウス移動動作
 *  @param   [in]    ptMouse   マウス位置
 *  @retval  マーカのID
 *  @note
 */
int CDrawMarker::MouseMove(POINT ptMouse, bool bUseKeep)
{
    int iRet = -1;
    POINT pt;
    int iLayerId   = m_pView->GetCurrentLayerId();
    CRect rcDraw;
    
    int iSel  = -1;
    int iKeep = -1;
    foreach(MARKER_DATA& Data, m_lstMarker)
    {
        m_pView->ConvWorld2Scr(&pt, Data.ptObject, iLayerId);
        Data.ptPos = pt;
        if (Data.GetDrawRect().PtInRect(ptMouse))
        {
          iSel = Data.iID;
        }

        if (Data.bKeepSelect && bUseKeep)
        {
            iKeep = Data.iID;
        }
    }

    if ((iSel == -1) && 
        (iKeep != -1))
    {
        iSel = iKeep;
    }

    
    foreach(MARKER_DATA& Data, m_lstMarker)
    {

        if (iSel == Data.iID)
        {
			if (Data.iID != -1)
			{
				//マーカのIDが存在するものを優先する
				iRet = Data.iID;
			}
            Data.iSelect = 1;
            Draw(Data);
        }
        else
        {
            if (Data.iSelect == 1)
            {
                //領域からはずれたものを解除
                if (!Data.bKeepSelect)
                {
                    Data.iSelect = 0;
                    Draw(Data);
                }
            }
        }
    }

    return iRet;
}

//!< マウス左ボタン押下
void CDrawMarker::LButtonDown(UINT nFlags, POINT point)
{
    POINT pt;
    int iLayerId   = m_pView->GetCurrentLayerId();

    //boost::mutex::scoped_lock look(mtxGuard);
    foreach(MARKER_DATA& Data, m_lstMarker)
    {
        m_pView->ConvWorld2Scr(&pt, Data.ptObject, iLayerId);
        Data.ptPos = pt;

        if (!Data.GetDrawRect().PtInRect(point))
        {
            if (Data.iSelect == 1)
            {
                //領域からはずれたものを解除
                Data.iSelect = 0;
                Draw(Data);
            }
            continue;
        }

        Data.iSelect = 2;
        Draw(Data);
    }
}

//!< マウス左ボタン解放
void CDrawMarker::LButtonUp(UINT nFlags, POINT point)
{
    POINT pt;
    int iLayerId   = m_pView->GetCurrentLayerId();

    //boost::mutex::scoped_lock look(mtxGuard);
    foreach(MARKER_DATA& Data, m_lstMarker)
    {
        m_pView->ConvWorld2Scr(&pt, Data.ptObject, iLayerId);
        Data.ptPos = pt;

        if (!Data.GetDrawRect().PtInRect(point))
        {
            if (Data.iSelect != 0)
            {
                //領域からはずれたものを解除
                Data.iSelect = 0;
                Draw(Data);
            }
            continue;
        }

        Data.iSelect = 1;
        Draw(Data);
    }
}

//!< クリック確認
int CDrawMarker::CheckCkick( POINT point, E_MARKER_TYPE ePriority)
{
    return CheckCkickAndObject( point, -2, ePriority);
}   


int CDrawMarker::CheckCkickAndObject( POINT point, int iObjctId, E_MARKER_TYPE ePriority)
{
    //boost::mutex::scoped_lock look(mtxGuard);
    int iKeep = -1;
	int iRetId = -1;
    CRect rcDraw;
    foreach(MARKER_DATA& Data, m_lstMarker)
    {
        if( Data.bKeepSelect)
        {
            if (iObjctId == -2)
            {
                iKeep = Data.iID;
            }
            else
            {
                if(Data.iObjct == iObjctId)
                {
                    iKeep = Data.iID;
                }
            }
        }

        if (Data.GetDrawRect().PtInRect(point))
        {
			if (ePriority == MARKER_NONE)
			{
				return Data.iID;
			}
			else
			{
				//ePriorityで設定したマーカを優先する
				if (Data.eType == ePriority)
				{
					return Data.iID;
				}
				else
				{
					iRetId = Data.iID;
				}
			}
        }
    }

	if (iRetId != -1)
	{
		return iRetId;
	}

    return iKeep;
}   

//!< マーカ取得
CDrawMarker::MARKER_DATA* CDrawMarker::GetMarker(int iId )
{
    //マーカリスト
    boost::mutex::scoped_lock look(mtxGuard);
    std::vector< MARKER_DATA >::iterator ite;
    ite = std::find_if(m_lstMarker.begin(),
                       m_lstMarker.end(),
                       GET_FIRST(MARKER_DATA)::iID == iId);


    if (ite == m_lstMarker.end())
    {
        return NULL;
    }
        
    MARKER_DATA* pData = &(*ite);
    return pData;
}


void CDrawMarker::Draw(MARKER_DATA& Data)
{
    if (Data.eType < MARKER_BMP_MAX)
    {
        m_lstBmp[Data.eType].Draw(&m_Dc, Data.iSelect, Data.ptPos);
    }
    else
    {
        COLORREF crPen;
        if      (Data.iSelect == 0){crPen = Data.crDraw;}
        else if (Data.iSelect == 1){crPen = DRAW_CONFIG->GetSelColor();}
        else                       {crPen = Data.crDraw;}


        CRect rcDraw = Data.GetDrawRect();

        if (Data.eType == MARKER_RECT)
        {
            CBrush brRect(crPen);
            m_Dc.FrameRect(rcDraw, &brRect);
        }
        else if (Data.eType == MARKER_CIRCLE)
        {
            _DrawCircle(Data, false, false, crPen);
        }
        else if (Data.eType == MARKER_CIRCLE_FILL)
        {
            _DrawCircle(Data, true, false, crPen);
        }
        else if (Data.eType == MARKER_DBL_CIRCLE)
        {
            _DrawCircle(Data, false, true, crPen);
        }
        else if (Data.eType == MARKER_RECT_FILL)
        {
            _DrawRect(Data, true, false, crPen);
        }
        else if (Data.eType == MARKER_RECT)
        {
            _DrawRect(Data, false, false, crPen);
        }
        else if (Data.eType == MARKER_DBL_RECT)
        {
            _DrawRect(Data, false, true, crPen);
        }
        else if (Data.eType == MARKER_X)
        {
            _DrawX(Data, crPen);
        }
        else if (Data.eType == MARKER_PLUS)
        {
            _DrawCross(Data, crPen);
        }
         else
        {
            STD_DBG(_T("Data.eType:  %d \n"), Data.eType);
        }
    }
}

void CDrawMarker::_DrawCircle(MARKER_DATA& Data, 
        bool bFill,
        bool bDouble,
        COLORREF cr
        )
{
    CRect rcDraw = Data.GetDrawRect();
    int iRad = rcDraw.Width() / 2;

    if (bFill)
    {
        DrawCircle(m_Dc,  Data.ptPos,   m_pView->GetCurrentLayer()->crDefault, cr, 1, iRad, bFill, bDouble);
    }
    else
    {
        DrawCircle(m_Dc,  Data.ptPos,   cr, cr, 1, iRad, bFill, bDouble);
    }
}


void CDrawMarker::DrawCircle(HDC hDc, 
        POINT ptScr,
        COLORREF cr,
        COLORREF crFill,
        int iLineWidth,
        long lRad,
        bool bFill,
        bool bDouble
        )
{
    HPEN hPen;
    HPEN hOldPen;
    HBRUSH hBr;

    int iLoop = 1;
    if (bDouble)
    {
        iLoop = 2;   
    }

    HGDIOBJ hBrFillOld;
    int iOldBkMode;
    hPen = ::CreatePen(PS_SOLID, iLineWidth, cr);
    hOldPen = (HPEN)::SelectObject(hDc, (HGDIOBJ)hPen);

    if (bFill)
    {
        hBr = ::CreateSolidBrush(crFill);
        iOldBkMode = ::SetBkMode(hDc, OPAQUE);
        hBrFillOld = ::SelectObject(hDc, hBr);
    }
    else
    {
        hBr = ::CreateSolidBrush(cr);
        iOldBkMode = ::SetBkMode(hDc, TRANSPARENT);
    }

    for( int iCnt = 0; iCnt < iLoop;    ++iCnt)
    {
        if (iCnt == 1)
        {
            lRad = lRad / 2;
        }

        RECT rc;
        rc.left = ptScr.x - lRad;
        rc.top  = ptScr.y - lRad;
        rc.right  = ptScr.x + lRad;
        rc.bottom = ptScr.y + lRad;

        ::Ellipse(hDc, rc.left, rc.top, 
                                    rc.right, rc.bottom);
                  
    }

    ::SetBkMode(hDc, iOldBkMode);
    ::SelectObject(hDc, hOldPen);
    ::DeleteObject(hPen);
        
    if (bFill)
    {
        ::SetBkMode(hDc, iOldBkMode);
        ::SelectObject(hDc, hBrFillOld);
    }
    ::DeleteObject(hBr);
}

void CDrawMarker::_DrawRect(MARKER_DATA& Data, 
        bool bFill,
        bool bDouble,
        COLORREF cr
        )
{
    CRect rcDraw = Data.GetDrawRect();
    int iRad = rcDraw.Width() / 2;
    DrawRect(m_Dc,  Data.ptPos,   m_pView->GetCurrentLayer()->crDefault, cr, 1, iRad, bFill, bDouble); 
}

void CDrawMarker::_DrawCross(MARKER_DATA& Data, 
        COLORREF cr
        )
{
    CRect rcDraw = Data.GetDrawRect();
    int iRad = rcDraw.Width() / 2;
    DrawCross(m_Dc,  Data.ptPos,    cr, 1, iRad); 
}

void CDrawMarker::DrawCross(HDC hDc, 
    POINT ptScr,
    COLORREF cr,
    int iLineWidth,
    long lRad
    )
{
    HPEN hPen;
    HPEN hOldPen;
    hPen = ::CreatePen(PS_SOLID, iLineWidth, cr);
    hOldPen = (HPEN)::SelectObject(hDc, (HGDIOBJ)hPen);

    lRad = lRad / 2;

    RECT rc;
    rc.left = ptScr.x - lRad;
    rc.top  = ptScr.y - lRad;
    rc.right  = ptScr.x + lRad;
    rc.bottom = ptScr.y + lRad;

    POINT pt;
    ::MoveToEx(hDc, ptScr.x - lRad, ptScr.y - lRad, &pt);
    ::LineTo  (hDc, ptScr.x + lRad, ptScr.y + lRad);

    ::MoveToEx(hDc, ptScr.x - lRad, ptScr.y + lRad, &pt);
    ::LineTo  (hDc, ptScr.x + lRad, ptScr.y - lRad);

    ::SelectObject(hDc, hOldPen);
    ::DeleteObject(hPen);
}

void CDrawMarker::_DrawX(MARKER_DATA& Data, 
        COLORREF cr
        )
{
    CRect rcDraw = Data.GetDrawRect();
    int iRad = rcDraw.Width() / 2;
    DrawX(m_Dc,  Data.ptPos,    cr, 1, iRad); 
}

void CDrawMarker::DrawX(HDC hDc, 
    POINT ptScr,
    COLORREF cr,
    int iLineWidth,
    long lRad
    )
{
    HPEN hPen;
    HPEN hOldPen;
    hPen = ::CreatePen(PS_SOLID, iLineWidth, cr);
    hOldPen = (HPEN)::SelectObject(hDc, (HGDIOBJ)hPen);

    lRad = lRad / 2;

    RECT rc;
    rc.left = ptScr.x - lRad;
    rc.top  = ptScr.y - lRad;
    rc.right  = ptScr.x + lRad;
    rc.bottom = ptScr.y + lRad;

    POINT pt;
    ::MoveToEx(hDc, ptScr.x - lRad, ptScr.y, &pt);
    ::LineTo  (hDc, ptScr.x + lRad, ptScr.y);

    ::MoveToEx(hDc, ptScr.x, ptScr.y - lRad, &pt);
    ::LineTo  (hDc, ptScr.x, ptScr.y + lRad);

    ::SelectObject(hDc, hOldPen);
    ::DeleteObject(hPen);
}

void CDrawMarker::DrawRect(HDC hDc, 
        POINT ptScr,
        COLORREF cr,
        COLORREF crFill,
        int iLineWidth,
        long lRad,
        bool bFill,
        bool bDouble
        )
{
    HPEN hPen;
    HPEN hOldPen;
    HBRUSH hBr;
    HGDIOBJ hBrFillOld;
    int iOldBkMode;
    hPen = ::CreatePen(PS_SOLID, iLineWidth, cr);
    hOldPen = (HPEN)::SelectObject(hDc, (HGDIOBJ)hPen);

    if (bFill)
    {
        hBr = ::CreateSolidBrush(crFill);
        iOldBkMode = ::SetBkMode(hDc, OPAQUE);
        hBrFillOld = ::SelectObject(hDc, hBr);

    }
    else
    {
        hBr = ::CreateSolidBrush(cr);
    }

    int iLoop = 1;
    if (bDouble)
    {
        iLoop = 2;   
    }

    for( int iCnt = 0; iCnt < iLoop;    ++iCnt)
    {
        if (iCnt == 1)
        {
            lRad = lRad / 2;
        }

        RECT rc;
        rc.left = ptScr.x - lRad;
        rc.top  = ptScr.y - lRad;
        rc.right  = ptScr.x + lRad;
        rc.bottom = ptScr.y + lRad;


        if (bFill)
        {
            ::Rectangle(hDc, rc.left, rc.top, 
                                        rc.right, rc.bottom);
                  
        }
        else
        {
            ::FrameRect(hDc, &rc, hBr);
        }
    }

    if (bFill)
    {
        ::SetBkMode(hDc, iOldBkMode);
        ::SelectObject(hDc, hBrFillOld);
    }
    ::SelectObject(hDc, hOldPen);
    ::DeleteObject(hPen);
   ::DeleteObject(hBr);
}

//!< カーソル設定
bool CDrawMarker::OnSetCursor()
{
    if(m_lstMarker.empty())
    {
        return false;
    }

    if(!m_pView)
    {
        return false;
    }

    CURSORINFO cursor;
    cursor.cbSize = { sizeof(CURSORINFO) };

    ::GetCursorInfo(&cursor);

    POINT pt = cursor.ptScreenPos; 

    m_pView->GetWindow()->ScreenToClient(&pt);
     
    foreach(MARKER_DATA& Data, m_lstMarker)
    {
        if (Data.GetCursor() == 0)
        {
            continue;
        }

        if (Data.GetDrawRect().PtInRect(pt))
        {
            if (Data.IsCursorStr())
            {
                HCURSOR hCursor;
		        hCursor =(HCURSOR)LoadImage(NULL, Data.SetCursorStr(),IMAGE_CURSOR,
                                            NULL,NULL,LR_DEFAULTCOLOR | LR_SHARED);
		        ::SetCursor(hCursor);
            }
            else
            {
                ::SetCursor(AfxGetApp()->LoadCursor(Data.GetCursor()));
            }
            return true;
        }
    }
	return false;
}

void CDrawMarker::Print() const
{
	DB_PRINT(_T("---CDrawMarker---\n"));

	if (!m_lstMarker.empty())
	{
		DB_PRINT(_T("  ---m_lstMarker---\n"));
		for (auto marker : m_lstMarker)
		{
			DB_PRINT(_T("  %s\n"), marker.Str().c_str());
		}
	}

	if (!m_lstObj.empty())
	{
		DB_PRINT(_T("  ---m_lstObj---\n"));
		for (auto wObj : m_lstObj)
		{
			auto pObj = wObj.lock();
			DB_PRINT(_T("  %s\n"), pObj->Text().c_str());
		}
	}
	DB_PRINT(_T("---CDrawMarker-End-\n"));
}
