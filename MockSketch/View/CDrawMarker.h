/**
 * @brief			CDrawMarkerヘッダーファイル
 * @file			CDrawMarker.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#if !defined(_DRAW_MARKER_H_)
#define _DRAW_MARKER_H_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DrawingObject/Primitive/POINT2D.h"
#include "View/ViewCommon.h"

class CDrawingObject;

class CDrawingView;
/**
 * @class   CDrawMarker
 * @brief                        
   
 */
class CDrawMarker
{
public:
    enum E_MARKER_TYPE
    {
		MARKER_NONE,         //
		MARKER_POINT,        // 点
        MARKER_IND,          // 方向
        MARKER_TANGENT,      // 接線
        MARKER_CENTER,       // 中心点
        MARKER_NORM,         // 垂線
        MARKER_SNAP,         // スナップ
        MARKER_CROSS,        // 交点


        MARKER_BMP_MAX,      //ビットマップマーカ      
        //-----------------------
        MARKER_CIRCLE_FILL  = 0x1000,   //!< 円塗りつぶし       
        MARKER_CIRCLE       ,   //!< 円                 
        MARKER_RECT_FILL    ,   //!< 四角塗りつぶし     
        MARKER_RECT         ,   //!< 四角               
        MARKER_DBL_CIRCLE   ,   //!< 二重丸             
        MARKER_DBL_RECT     ,   //!< 二重角  
        MARKER_X            ,   //!< ×印
        MARKER_PLUS         ,   //!< ＋印
        MARKER_MAX
    };


    union  CursorId
    {
        UINT            nCursor;
        LPCTSTR         pCursor;
    };


    //**  マーカデータ
    class MARKER_DATA
    {
    protected:
        CursorId    m_cursor;
        bool        bStandard;

    public:
        //!< 自分自身のID
        int           iID = -1;

        //!< マーカータイプ
        E_MARKER_TYPE  eType = MARKER_POINT;
		VIEW_COMMON::E_SNAP_TYPE eSnap = VIEW_COMMON::SNP_NONE;

        //!< 表示位置
        POINT         ptPos;

        //!< オフセット位置
        POINT         ptOffset;

        //!< 
        POINT2D       ptObject;

        //!< 選択 0:none 1:on 2:push 4:優先選択
        int            iSelect = 0;

        bool           bKeepSelect = false;

        //!< ラジオボタン
        bool            bRadio = false;

        //!< ラジオボタングループ
        int            iRadioGroup = 0;

        //!<領域
        //!<
        CRect           rcArea;

        //!< オブジェクトID
        int             iObjct = -1;

        //!< 値(通常スナップ)
        int             iVal = -1;

        COLORREF        crDraw;


    public:
        MARKER_DATA();

        void SetCursor(UINT iId);
        void SetCursor(LPCTSTR pId);

        bool IsCursorStr();
        UINT GetCursor();
        LPCTSTR SetCursorStr();
		//デバッグ用
		StdString Str() const;

        CRect GetDrawRect()
        {
            CRect   rcDraw = rcArea;
            rcDraw.OffsetRect(ptPos.x + ptOffset.x,
                              ptPos.y - ptOffset.y);
            return rcDraw;
        }

    };

    //**  マーカビットマップ
    class BMP_DATA
    {
    protected:

        CBitmap     Bmp;
        CImageList  ImageList;
        int         iItemCnt;
        int         iWidthOffset;
        int         iHeightOffset;
        bool        bLoad;
        CSize       szBmp;

    public:
        BMP_DATA()
        {
            iItemCnt = 0;
            iWidthOffset = 0;
            iHeightOffset = 0;
            bLoad = false;
            CSize    szBmp;
        }

        virtual ~BMP_DATA()
        {
        }

        void Load(int iId)
        {
            COLORREF crKey = RGB(0, 128, 128);
            BITMAP BitMap; 

            STD_ASSERT(Bmp.LoadBitmap(iId));
            Bmp.GetBitmap(&BitMap);
            szBmp.cx = BitMap.bmWidth;
            szBmp.cy = BitMap.bmHeight;
            iItemCnt = szBmp.cx / szBmp.cy;
            
            ImageList.Create(szBmp.cy, szBmp.cy, ILC_COLOR24 | ILC_MASK, iItemCnt, 4);
            //ImageList.SetBkColor(crKey);	// or CLR_NONE
            ImageList.SetBkColor(CLR_NONE);	// or CLR_NONE
            ImageList.Add(&Bmp, crKey);

            iWidthOffset    = szBmp.cy / 2; //個々のイメージは正方形;
            iHeightOffset   = szBmp.cy / 2;
            bLoad = true;
        }

        void Draw(CDC* pDC, int iIndex, POINT pos)
        {
            STD_ASSERT(bLoad);
            STD_ASSERT(iIndex >= 0);
            STD_ASSERT(pDC != 0);

            if(iIndex >=  iItemCnt)
            {
                iIndex = iItemCnt - 1;
            }

            pos.x -= iWidthOffset;
            pos.y -= iHeightOffset;
            
            ImageList.Draw(pDC, iIndex, pos, ILD_NORMAL);	// CLR_NONEならばILD_NORMALでもよい
        }

        int GetItemNum()
        {
            return iItemCnt;
        }

        SIZE GetSize()
        {
            return (SIZE)szBmp;
        }

    };

protected:

    //ビットマップデータ
    BMP_DATA    m_lstBmp[MARKER_BMP_MAX];

    //マーカリスト
    std::vector< MARKER_DATA > m_lstMarker;

    //オブジェクトリスト
    std::deque< std::weak_ptr<CDrawingObject> > m_lstObj;

    CDrawingView* m_pView;

    CDC           m_Dc;

    bool          m_bHideObject;

    bool          m_bDrawIndex;

    //現在選択中のマーカ
    MARKER_DATA*     m_pSelData;

    mutable boost::mutex   mtxGuardListObj;

    mutable boost::mutex   mtxGuard;

public:
	//!< コンストラクタ
    CDrawMarker();

	//!< コンストラクタ
    CDrawMarker(CDrawingView* pView);

	//!< デストラクタ
    virtual ~CDrawMarker();

	//!< 初期化
    void Init();

	//!< 画面初期化
    void InitDisp(HDC hDc);

	//!< データ設定
    int AddMarker(MARKER_DATA data, POINT2D ptObject);

    int AddMarker(CDrawMarker::E_MARKER_TYPE eType, POINT ptMarker ,
                  POINT2D       ptObject,
                  bool bRadio, int iGroupNo, int iObjectId = -1,
                                             int iVal = -1);
  
    int GetMarkerNum() { return SizeToInt(m_lstMarker.size()) ; }


	//!< 追加可能オブジェクト設定
    void AddObject(std::weak_ptr<CDrawingObject> pObj);

	//!< 追加可能オブジェクト設定
    void AddObjectLast(std::weak_ptr<CDrawingObject> pObj);

    //!< 追加可能オブジェクト削除
    void ClearObject();

	//!< 追加可能オブジェクト非表示
    void HideObject(bool bHide = true);

    //!< ビュー設定
    void SetView(CDrawingView* pView);

    //!< 描画
    void Draw();

	//!< 
	void Clear();

	//!< マウス移動
	int MouseMove(POINT ptMouse, bool bUseKeep = true);

	//!< マウス左ボタン押下
    void LButtonDown(UINT nFlags, POINT point);

	//!< マウス左ボタン解放
    void LButtonUp(UINT nFlags, POINT point);

	//!< クリック確認
    int CheckCkick( POINT point, E_MARKER_TYPE ePriority = MARKER_NONE);

	//!< クリック確認
    int CheckCkickAndObject( POINT point ,
		                     int iObjctId,
                       		 E_MARKER_TYPE ePriority = MARKER_NONE);

	//!< カーソル設定
	bool OnSetCursor();

    //!< マーカ取得
    CDrawMarker::MARKER_DATA* GetMarker( int iId );

    //!< 描画処理
    void Draw(MARKER_DATA& Data);

	//!< デバッグ用
	void Print() const;

    static void DrawRect(HDC hDc, 
        POINT ptScr,
        COLORREF cr,
        COLORREF crFill,
        int iLineWidth,
        long lRad,
        bool bFill,
        bool bDouble
        );

    static void DrawCircle(HDC hDc, 
        POINT ptScr,
        COLORREF cr,
        COLORREF crFill,
        int iLineWidth,
        long lRad,
        bool bFill,
        bool bDouble
        );

    static void DrawCross(HDC hDc, 
        POINT ptScr,
        COLORREF cr,
        int iLineWidth,
        long lRad
        );

    static void DrawX(HDC hDc, 
        POINT ptScr,
        COLORREF cr,
        int iLineWidth,
        long lRad
        );

private:

    void _DrawCircle(MARKER_DATA& Data, 
        bool bFill,
        bool bDouble,
        COLORREF cr);

    void _DrawRect(MARKER_DATA& Data, 
        bool bFill,
        bool bDouble,
        COLORREF cr);

    void _DrawCross(MARKER_DATA& Data, 
        COLORREF cr);

    void _DrawX(MARKER_DATA& Data, 
        COLORREF cr);


    void _DrawMarker();
    void _DrawObject();

};
#endif // !defined(_DRAW_MARKER_H_)
