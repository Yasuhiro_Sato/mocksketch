/**
 * @brief			CLayerヘッダーファイル
 * @file			CLayer.h
 * @author			Yasuhiro Sato
 * @date			03-2-2009 22:31:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#if !defined(EA_6EFCDFAC_0DBC_446b_8324_27A6FF32D47E__INCLUDED_)
#define EA_6EFCDFAC_0DBC_446b_8324_27A6FF32D47E__INCLUDED_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DrawingObject/Primitive/Rect2D.h"
#include "DrawingObject/Primitive/POINT2D.h"

/**
 * @class   CLayer
 * @brief   レイヤーデータ                     
 */
class CLayer
{

public:
    
	//!< レイヤ−ID
    int  iLayerId = -1;

	//!< 選択可/不可
	bool bEnable = true;

	//!< 選択可/不可
	bool bEnableForScl = true;

	//!< 表示有無
	bool bVisible = true;

	//!< レイヤー色使用有無
	bool bUseLayerColor = false;

	//!< レイヤー色
	COLORREF crLayer = RGB(0,0,0);

	//!< 
	bool bUseBoundingBox = true;
	
	//!< レイヤー毎のスケール仕様有無
    //bool  bOrignalScl;

    //!< オブジェクトデフォルト色
    COLORREF crDefault = RGB(   0,   0,   0);

    //!< ブラシ色
    COLORREF crBrush = RGB(   0,   0,   0);

    //!< ディフォルトの線種
    int iLineType = PS_SOLID;

    //!< ディフォルトの線幅
    int iLineWidth = 1;

    //!< スケール
	double dScl = 1.0;

	//!< レイヤー名
	StdString strName = _T("Default");

	//!< 表示領域
    //RECT2D   rcViewArea;

    //!< オフセット
    POINT2D  ptOffset;

    CLayer& operator = (const CLayer & m)
    {
        iLayerId    = m.iLayerId;
    	bEnable     = m.bEnable;
        bEnableForScl  = m.bEnableForScl;
        bVisible    = m.bVisible;
		bUseBoundingBox = m.bUseBoundingBox;
        bUseLayerColor  = m.bUseLayerColor;
        crLayer         = m.crLayer;
        crDefault   = m.crDefault;
        crBrush     = m.crBrush;
        iLineType   = m.iLineType;
        iLineWidth  = m.iLineWidth;
        dScl        = m.dScl;
        strName     = m.strName;
        ptOffset    = m.ptOffset;
        return *this;
    }

    //==============================
    // ファイル保存
    //==============================
    friend class boost::serialization::access;  
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        SERIALIZATION_INIT;
            SERIALIZATION_BOTH("LayerId"   , iLayerId);
            SERIALIZATION_BOTH("Enable"    , bEnable);
            SERIALIZATION_BOTH("Visible"   , bVisible);
            SERIALIZATION_BOTH("UseLayerColor" , bUseLayerColor);
			SERIALIZATION_BOTH("UseBoundingBox", bUseBoundingBox);
			SERIALIZATION_BOTH("Color"         , crLayer);
            SERIALIZATION_BOTH("Scl"           , dScl);
            SERIALIZATION_BOTH("Name"          , strName);
            SERIALIZATION_BOTH("Offset"       , ptOffset);
            SERIALIZATION_BOTH("DefaultColor"          , crDefault);
            SERIALIZATION_BOTH("DefaultBrushColor"     , crBrush);
            SERIALIZATION_BOTH("DefaultLineType"       , iLineType);
            SERIALIZATION_BOTH("DefaultLineWidth"      , iLineWidth);
 
        MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
    }

};
BOOST_CLASS_VERSION(CLayer, 0);

#endif // !defined(EA_6EFCDFAC_0DBC_446b_8324_27A6FF32D47E__INCLUDED_)
