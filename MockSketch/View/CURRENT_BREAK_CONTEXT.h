/**
 * @brief			CURRENT_BREAK_CONTEXTヘッダーファイル
 * @file			CURRENT_BREAK_CONTEXT.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 20:48:43
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

#if !defined(_CURRENT_BREAK_CONTEXT_H)
#define      _CURRENT_BREAK_CONTEXT_H

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/

class asIScriptContext;
class CScriptObject;

/**
 * @class   CURRENT_BREAK_CONTEXT
 * @brief   ブレークポイントデータ
 * @note   
 */
class CURRENT_BREAK_CONTEXT
{
public:
    std::string           strSection;   //セクション名（表示タブ名)
    std::string           strTopFunc;   //呼び出し元トップレベルの関数名
    CScriptObject*        pScriptObj = NULL;  
    asIScriptContext*     pAsContext = NULL;
    int                   iLineNo = 0;        //行数 
    int                   iColum = 0;         //列数

};

#endif // _CURRENT_BREAK_CONTEXT_H
