/**
 * @brief			CUndoAction実装ファイル
 * @file			CUndoAction.cpp
 * @author			Yasuhiro Sato
 * @date			03-2-2009 23:20:45
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./CUndoAction.h"
#include "DefinitionObject/CPartsDef.h"
#include "DrawingObject/CDrawingObject.h"

#include "Utility/CUtility.h"


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/**
 *  @brief  コンストラクター.
 *  @param  なし
 *  @retval なし 
 *  @note
 */
CUndoAction::CUndoAction():
m_pItem(NULL),
m_dwGroupCount(0),
m_iUndoMax(20),
m_bBufferOverFlow(false)
{
}


/**
 *  @brief  デストラクタ
 *  @param  なし
 *  @retval なし 
 *  @note
 */
CUndoAction::~CUndoAction()
{
    Clear();
}

/**
 *  @brief  最大アイテム数設定
 *  @param  なし
 *  @retval なし 
 *  @note   最初に設定してください
 */
void CUndoAction::SetMaxItem(int iMax)
{
    m_iUndoMax = iMax;
}

/**
 *  @brief  データ追加
 *  @param  [in] eType  
 *  @param  [in] pCtrl
 *  @param  [in] pOldObj
 *  @param  [in] pNewObj
 *  @param  [in] bEnd           Undoデータ設定終了
 *  @retval なし 
 *  @note   pOldObjは内部でコピーを作ります
 */
void CUndoAction::Add(UNDO_TYPE eType, CPartsDef* pCtrl, 
                              std::shared_ptr<CDrawingObject> pOldObj,
                              std::shared_ptr<CDrawingObject> pNewObj,
                              bool bEnd)
{

    UndoItem item;

    item.eType = eType;
    item.pCtrl = pCtrl;
    item.pNewObj = pNewObj;

    switch(eType)
    {
    case UD_ADD:
        STD_ASSERT(pOldObj == NULL);
        STD_ASSERT(pNewObj != NULL);
        if (pNewObj == NULL)
        {
            AddCancel();
            return;
        }

        item.pOldObj = NULL;
        break;

    case UD_CHG:
        STD_ASSERT(pOldObj != NULL);
        STD_ASSERT(pNewObj != NULL);
        if (pNewObj == NULL ||
            pOldObj == NULL)
        {
            AddCancel();
            return;
        }
        item.pOldObj = CDrawingObject::CloneShared(pOldObj.get());
        break;

    case UD_DEL:
        STD_ASSERT(pNewObj == NULL);
        STD_ASSERT(pOldObj != NULL);
        if (pOldObj == NULL)
        {
            AddCancel();
            return;
        }

        item.pNewObj = NULL;
        item.pOldObj = CDrawingObject::CloneShared(pOldObj.get());
        break;

    default:
        STD_ASSERT(eType);
        break;
    }

    bool bFirst = false;
    size_t size = m_queUndo.size();
    if (size == 0)
    {
        std::vector<UndoItem> lstItem;
        m_queUndo.push_back(lstItem);
        size++;
        bFirst = true;
    }

    m_queUndo[size - 1].push_back(item);

#if 0
       StdString strRet;
       strRet = GetStr(&item);
       DB_PRINT(_T("%s"), strRet.c_str());
#endif

    if (bEnd)
    {
        Push();
    }

    if (bFirst)
    {
        ::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_CHG_OBJECT_STS, 
                            (WPARAM)0, (LPARAM)0);
    }

}

/**
 *  @brief  バッファー先頭削除
 *  @param  なし
 *  @retval なし 
 *  @note
 */
bool CUndoAction::DeleteFront()
{
    if(m_queUndo.size() == 0)
    {
        return false;
    }

    /*
    std::vector<UndoItem>* pItemList;

    pItemList = &m_queUndo[0];

    foreach(UndoItem item, *pItemList)
    {
        if(item.pOldObj)
        {
            delete item.pOldObj;
        }
    }
    */
    m_queUndo.pop_front();
    return true;
}

/**
 *  @brief  バッファー全削除
 *  @param  なし
 *  @retval なし 
 *  @note
 */
void CUndoAction::DeleteAll()
{
    int iCnt = 0;
    while(DeleteFront())
    {
        iCnt++;
        if (iCnt >= (m_iUndoMax +2) )
        {
            STD_ASSERT(iCnt == m_iUndoMax);
            break;
        }
    }
    m_queUndo.clear();

    ::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_CHG_OBJECT_STS, 
                        (WPARAM)0, (LPARAM)0);

}

/**
 *  @brief  リドゥバッファー削除
 *  @param  なし
 *  @retval なし 
 *  @note
 */
void CUndoAction::DeleteRedo()
{
    /*
    foreach(std::vector<UndoItem>& itemList, m_queRedo)
    {
        foreach(UndoItem& item, itemList)
        {
            if(item.pOldObj)
            {
                delete item.pOldObj;
            }
        }
    }
    */
    m_queRedo.clear();
}

/**
 *  @brief  データ追加(分割式）
 *  @param  [in] eType  
 *  @param  [in] pCtrl
 *  @param  [in] pOldObj
 *  @retval なし 
 *  @note   
 */
void CUndoAction::AddStart(UNDO_TYPE eType, CPartsDef* pCtrl,
                               CDrawingObject* pOldObj)
{
    STD_ASSERT(m_pItem == NULL);
    m_pItem = new UndoItem;

    m_pItem->eType = eType;
    m_pItem->pCtrl = pCtrl;
    m_pItem->pNewObj = NULL;
    m_pItem->pOldObj = NULL;
    switch(eType)
    {
    case UD_ADD:
        STD_ASSERT(pOldObj == NULL);
        m_pItem->pOldObj = NULL;
        break;

    case UD_CHG:
        STD_ASSERT(pOldObj != NULL);
        if (pOldObj == NULL)
        {
            AddCancel();
            return;
        }
        m_pItem->pOldObj = CDrawingObject::CloneShared(pOldObj);
        break;

    case UD_DEL:
        STD_ASSERT(pOldObj != NULL);
        if (pOldObj == NULL)
        {
            AddCancel();
            return;
        }
        m_pItem->pOldObj = CDrawingObject::CloneShared(pOldObj);
        break;

    default:
        STD_ASSERT(eType);
        break;
    }
}

/**
 *  @brief  データ追加
 *  @param  [in] pNewObj
 *  @param  [in] bEnd
 *  @retval なし 
 *  @note   pOldObjは内部でコピーを作ります
 */
void CUndoAction::AddEnd  (std::shared_ptr<CDrawingObject> pNewObj, bool bEnd)
{
    STD_ASSERT(m_pItem != NULL);

    m_pItem->pNewObj = CDrawingObject::CloneShared(pNewObj.get());

    switch(m_pItem->eType)
    {
    case UD_ADD:
    case UD_CHG:
        STD_ASSERT(pNewObj != NULL);
        if (pNewObj == NULL)
        {
            AddCancel();
            return;
        }
        break;

    case UD_DEL:
        STD_ASSERT(pNewObj == NULL);
        m_pItem->pNewObj = NULL;
        break;

    default:
        STD_ASSERT(m_pItem->eType);
        break;
    }

    bool bFirst = false;
    size_t size = m_queUndo.size();
    if (size == 0)
    {
        std::vector<UndoItem> lstItem;
        m_queUndo.push_back(lstItem);
        size++;
        bFirst = true;
    }

    m_queUndo[size - 1].push_back(*m_pItem);

#if 0
        //文字列が長すぎる バッファーオーバーフロー
       StdString str = GetStr(m_pItem);
       DB_PRINT(_T("%s"), str.c_str());
#endif

    if (bEnd)
    {
        Push();
    }

    delete m_pItem;
    m_pItem = NULL;

    if (bFirst)
    {
        ::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_CHG_OBJECT_STS, 
                            (WPARAM)0, (LPARAM)0);
    }

}

/**
 *  @brief  データ追加キャンセル
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CUndoAction::AddCancel()
{
    if(m_pItem == NULL)
    {
        return;
    }

    delete m_pItem;
    m_pItem = NULL;
}

/**
 *  @brief  データ登録(複数データ登録時)
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CUndoAction::Push()
{
    std::vector<UndoItem> lstItem;
    m_queUndo.push_back(lstItem);

    if ((int)m_queUndo.size() > m_iUndoMax)
    {
        m_bBufferOverFlow = true;
        DeleteFront();
    }
    //リドゥバッファクリア
#ifdef _DEBUG
       DB_PRINT(_T("CUndoAction::Push\n"));
#endif
    DeleteRedo();
}

/**
 *  @brief  アンドゥ実行
 *  @param  なし
 *  @retval false アンドゥ不可
 *  @note   
 */
bool CUndoAction::Undo()
{
    //後ろから
    size_t iSize = m_queUndo.size();
    if(iSize == 0)
    {
        return false;
    }

    std::vector<UndoItem>* pListItem;

    pListItem = &m_queUndo[iSize-1];
    
    if (pListItem->size() == 0)
    {
        if (iSize == 1)
        {
            return false;
        }
        else
        {
            m_queUndo.pop_back();
            pListItem = &m_queUndo[iSize-2];
        }
    }

    UndoItem itemRedo;
    std::vector<UndoItem> lstRedo;

    CPartsDef* pCtrl = NULL;

    if (pListItem->size() != 0)
    {
        pCtrl = pListItem->at(0).pCtrl;
        pCtrl->RelSelect();
    }


    foreach(UndoItem item, *pListItem)
    {
        itemRedo = item;
        switch(item.eType)
        {
        case UD_ADD:
            //
            itemRedo.eType   =  UD_DEL;
            itemRedo.pOldObj =  CDrawingObject::CloneShared(item.pNewObj.get());
            itemRedo.pNewObj =  NULL;
            itemRedo.pCtrl->DeleteDrawingObject(item.pNewObj.get());
            break;

        case UD_CHG:
            itemRedo.eType   =  UD_CHG;
            itemRedo.pOldObj =  CDrawingObject::CloneShared(item.pNewObj.get());
            itemRedo.pNewObj =  item.pOldObj;
            itemRedo.pCtrl->ReplaceObject(item.pNewObj, item.pOldObj);
            item.pOldObj->SetSelect(false);

            break;

        case UD_DEL:
            itemRedo.eType   =  UD_ADD;
            itemRedo.pOldObj =  NULL;
            itemRedo.pNewObj =  item.pOldObj;
            itemRedo.pNewObj->SetSelect(false);
            itemRedo.pCtrl->RegisterObject(item.pOldObj, true, false);
            break;
        }
        lstRedo.push_back(itemRedo);
    }
    if (pCtrl)
    {
        pCtrl->AddGroupChg();
        pCtrl->Redraw();
    }
    m_queRedo.push_back(lstRedo);
    pListItem->clear();


    if (!m_bBufferOverFlow)
    {
        if (m_queUndo.empty())
        {
            ::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_CHG_OBJECT_STS, 
                                (WPARAM)0, (LPARAM)0);
        }
    }

    return true;
}


/**
 *  @brief  リドゥ実行
 *  @param  なし
 *  @retval false リドゥ実行
 *  @note   
 */
bool CUndoAction::Redo()
{
    //後ろから
    size_t iSize = m_queRedo.size();
    if(iSize == 0)
    {
        return false;
    }

    std::vector<UndoItem>* pListItem;

    pListItem = &m_queRedo[iSize-1];
    
    if (pListItem->size() == 0)
    {
        STD_ASSERT(pListItem->size() == 0);
        m_queRedo.pop_back();
        return false;
    }

    bool bFirst = false;
    if (m_queUndo.empty() && (!m_bBufferOverFlow))
    {
        bFirst = true;
    }

    UndoItem itemUndo;
    std::vector<UndoItem> lstUndo;
    CPartsDef* pCtrl = NULL;

    if (pListItem->size() != 0)
    {
        pCtrl = pListItem->at(0).pCtrl;
        pCtrl->RelSelect();
    }

    foreach(UndoItem item, *pListItem)
    {
        pCtrl = item.pCtrl;
        itemUndo = item;
        switch(item.eType)
        {
        case UD_ADD:
            //
            itemUndo.eType   =  UD_DEL;
            itemUndo.pOldObj =  CDrawingObject::CloneShared(item.pNewObj.get());
            itemUndo.pNewObj =  NULL;
            itemUndo.pCtrl->DeleteDrawingObject(item.pNewObj.get());
            break;

        case UD_CHG:
            itemUndo.eType   =  UD_CHG;
            itemUndo.pOldObj =  CDrawingObject::CloneShared(item.pNewObj.get());
            itemUndo.pNewObj =  item.pOldObj;
            itemUndo.pCtrl->ReplaceObject(item.pNewObj, item.pOldObj);
            item.pOldObj->SetSelect(false);
            break;

        case UD_DEL:
            itemUndo.eType   =  UD_ADD;
            itemUndo.pOldObj =  NULL;
            itemUndo.pNewObj =  item.pOldObj;
            item.pOldObj->SetSelect(false);
            itemUndo.pCtrl->RegisterObject(item.pOldObj, true, false);
            break;
        }
        lstUndo.push_back(itemUndo);
    }

    if (pCtrl)
    {
        pCtrl->Redraw();
    }

    m_queRedo.pop_back();
    m_queUndo.push_back(lstUndo);

    std::vector<UndoItem> lstTmp;
    m_queUndo.push_back(lstTmp);

    if ((int)m_queUndo.size() > m_iUndoMax)
    {
        DeleteFront();
    }


    if (bFirst)
    {
        ::SendMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_CHG_OBJECT_STS, 
                            (WPARAM)0, (LPARAM)0);
    }

    return true;
}

/**
 *  @brief  全データ削除
 *  @param  なし
 *  @retval なし
 *  @note   
 */
void CUndoAction::Clear()
{
    DeleteAll();
    DeleteAll();
}

/**
 *  @brief  アンドゥ有無問い合わせ
 *  @param  なし
 *  @retval true アンドゥなし
 *  @note   
 */
bool CUndoAction::IsUndoEmpty()
{
    if (m_queUndo.empty())
    {
        return true;
    }

    if(m_queUndo[0].empty())
    {
        return true;
    }
    return false;
}

/**
 *  @brief  リドゥ有無問い合わせ
 *  @param  なし
 *  @retval true リドゥなし
 *  @note   
 */
bool CUndoAction::IsRedoEmpty()
{
    if (m_queRedo.empty())
    {
        return true;
    }
    return false;
}

/**
 *  @brief  デバッグ用文字列
 *  @param  pItem
 *  @retval 文字列
 *  @note   
 */
StdString CUndoAction::GetStr(UndoItem* pItem)
{
    StdStringStream strRet;
    switch(pItem->eType)
    {
        case UD_ADD: strRet<< _T("追加 "); break;
        case UD_CHG: strRet<< _T("変更 "); break;
        case UD_DEL: strRet<< _T("削除 "); break;
        default:     strRet<< _T("不明 "); break;
    }
    strRet<< _T("Ctrl:") << pItem->pCtrl->GetName() << _T("\n");

    strRet<< _T("Old:");
    if (pItem->pOldObj == NULL) {strRet<< _T("None \n");}
    else                        {strRet<< pItem->pOldObj->GetStr() <<_T("\n");}

    strRet<< _T("New:");
    if (pItem->pNewObj == NULL) {strRet<< _T("None \n");}
    else                        {strRet<< pItem->pNewObj->GetStr() <<_T("\n");}

    return strRet.str();
}

void CUndoAction::PrintUndoBuffer() const
{
    int iBlock = 1;
    DB_PRINT(_T("----------------\n"));
    for(auto lst: m_queUndo)
    {
        DB_PRINT(_T("   ---BLOCK(%d) START ---\n"), iBlock);
        for(auto item: lst)
        {
            DB_PRINT(_T("       %s\n"), GetStr(&item).c_str());
        }
        DB_PRINT(_T("   ---BLOCK(%d) END ---\n"), iBlock);
        iBlock++;
    }
    DB_PRINT(_T("\n"));
}

//*******************************
//             TEST
//*******************************
#ifdef _DEBUG
void TEST_CUndoAction()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());

    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}
#endif //_DEBUG