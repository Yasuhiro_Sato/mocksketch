/**
 * @brief			CUndoActionヘッダーファイル
 * @file			CUndoAction.h
 * @author			Yasuhiro Sato
 * @date			03-2-2009 23:20:45
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef UNDO_ACTION_H__
#define UNDO_ACTION_H__

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/

/*---------------------------------------------------*/
/*  classes                                          */
/*---------------------------------------------------*/
class CPartsDef;
class CDrawingObject;

/*---------------------------------------------------*/
/*  Enum                                             */
/*---------------------------------------------------*/
enum UNDO_TYPE
{
    UD_ADD,     //オブジェクトを追加
    UD_CHG,     //オブジェクトを変更
    UD_DEL,     //オブジェクトを削除   
    UD_ADD_GROUP,  //グループオブジェクトを追加   
    UD_DEL_GROUP,  //グループオブジェクトを削除   
};

/*---------------------------------------------------*/
/*  Struct                                           */
/*---------------------------------------------------*/
struct UndoItem
{
    UNDO_TYPE           eType;          //変更種別
    int                     iGroup;     //グループ
    CPartsDef*    pCtrl;      //対象コントロール
    std::shared_ptr<CDrawingObject>     pOldObj;    //旧オブジェクト
    std::shared_ptr<CDrawingObject>     pNewObj;    //新オブジェクト
};


/**
 * @class   CUndoAction
 * @brief   
 */
class CUndoAction
{
public:
    CUndoAction();
    virtual ~CUndoAction();

/*
 Undoデータは以下のデータに格納されています。
     std::deque< std::vector<UndoItem> > m_queUndo;

     std::vector<UndoItem>がUndoの単位となります

     Pushを行うか、Add, AddEnd でbEnd引数をtrue設定することで
     Undo１単位をセットします。

     分割式のデータ追加は主に、UD_CHGに使用され以下の場合で使用します
        AddStart 変更操作 AddEnd or AddCancel



	    AddStart(UD_ADD, pDef, NULL);
		AddObject(pNewObj);
		AddEnd(pNewObj, bEnd = false);



*/


    //!< 最大アイテム数設定
    void SetMaxItem(int iMax);

    //!< データ追加
    void Add(UNDO_TYPE eType, CPartsDef* pCtrl, 
                              std::shared_ptr<CDrawingObject> pOldObj,
                              std::shared_ptr<CDrawingObject> pNewObj,
                              bool bEnd = false);

    //!< データ追加(分割式）
    void AddStart(UNDO_TYPE eType, CPartsDef* pCtrl,
                                   CDrawingObject* pOldObj);

    //!< データ追加(分割式）
    void AddEnd  (std::shared_ptr<CDrawingObject> pNewObj, bool bEnd = false);

    //!< データキャンセル
    void AddCancel  ();

    //!< データ登録(複数データ登録時)
    void Push(); 

    //!< 全データ削除
    void Clear();

    //!< アンドゥ
    bool Undo();

    //!< リドゥ
    bool Redo();

    //!< アンドゥ有無問い合わせ
    bool IsUndoEmpty();

    //!< リドゥ有無問い合わせ
    bool IsRedoEmpty();

    //!< デバッグ用文字列
    static StdString GetStr(UndoItem* pItem);


    void PrintUndoBuffer() const;

protected:
    //!< バッファー先頭部削除
    bool DeleteFront();

    //!< 全バッファー削除
    void DeleteAll();

    //!< リドゥバッファ削除
    void DeleteRedo();

protected:
    //!< アンドゥ最大値
    int m_iUndoMax;

    //!< アンドゥバッファ
    std::deque< std::vector<UndoItem> > m_queUndo;

    //!< リドゥバッファ
    std::deque< std::vector<UndoItem> > m_queRedo;

    //!< 分割追加用
    UndoItem* m_pItem;

    //!< バッファ最大値超
    bool m_bBufferOverFlow;

    //!< グループ値
    DWORD m_dwGroupCount;

};

#endif //UNDO_ACTION_H__