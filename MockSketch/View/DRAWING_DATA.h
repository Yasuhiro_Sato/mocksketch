/**
 * @brief			DRAWING_DATAヘッダーファイル
 * @file			DRAWING_DATA.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 20:48:43
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef _DRAWING_DATA_H
#define _DRAWING_DATA_H

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "DrawingObject/CDrawingObject.h"
#include "DrawingObject/Primitive/MAT2D.h"

/**
 * @class   DRAWING_DATA
 * @brief                        
 */
//TODO: DrawMatrix で事足りるので削除予定 
struct DRAWING_DATA
{
    CDrawingObject* pObj;
    MAT2D           mat;
};

struct DISPLAY_LIST
{
    int iChgCnt;
    std::map<int, std::vector<DRAWING_DATA> > mapLstDraw;   //レイヤー
    std::vector<DRAWING_DATA>           lstPoint;
};

#endif //DRAWING_DATA
