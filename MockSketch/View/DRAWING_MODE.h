/**
 * @brief			DRAWING_MODEヘッダーファイル
 * @file			DRAWING_MODE.h
 * @author			Yasuhiro Sato
 * @date			30-1-2009 15:53:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#if !defined(EA_49BE0A3E_FCC5_4141_9AE7_485D3FBDEBCF__INCLUDED_)
#define EA_49BE0A3E_FCC5_4141_9AE7_485D3FBDEBCF__INCLUDED_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
/**
 * @class   DRAWING_MODE
 * @brief   描画モード                     
   
 */
enum DRAWING_MODE
{
	//!< 印刷表示
	DRAW_PRINT = 0x1000,

    //!< 表示
	DRAW_VIEW  = 0x0003,

	//!< 図形描画(インデックスバッファーには描画しない）
	DRAW_FIG   = 0x0001,

	//!< インデックスバッファ描画
	DRAW_INDEX = 0x0002,

    //!< 選択描画
	DRAW_SEL   = 0x0004,

    //!< 選択&インデックスバッファ
	DRAW_SEL_INDEX  = 0x0006,


    //!< フロントバッファ描画
	DRAW_FRONT = 0x0008,

    //!< フロント&インデックスバッファ
	DRAW_FRONT_INDEX  = 0x000A,

    //!< フロント&図形&インデックスバッファ
	DRAW_FRONT_VIEW  = 0x000B,

    //!< 表示
	DRAW_ALL   = 0x0007,

};
#endif // !defined(EA_49BE0A3E_FCC5_4141_9AE7_485D3FBDEBCF__INCLUDED_)
