/**
 * @brief			DRAWING_TYPE実装
 * @file			DRAWING_TYPE.cpp
 * @author			Yasuhiro Sato
 * @date			06-2-2009 15:00:30
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./DRAWING_TYPE.h"
#include "System/CSystem.h"
#include "Utility/CUtility.h"


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


/**
 *  @brief   オブジェクト種別名称取得.
 *  @param   [in] eType オブジェク種別
 *  @retval  オブジェク種別名
 *  @note
 */
StdString VIEW_COMMON::GetObjctTypeName(DRAWING_TYPE eType)
{
    StdString strName;
    switch(eType)
    {
    case DT_POINT:  {strName = GET_STR(STR_PRO_POINT)  ; break;}
    case DT_CIRCLE: {strName = GET_STR(STR_PRO_CIRCLE) ; break;}
    case DT_LINE:   {strName = GET_STR(STR_PRO_LINE)   ; break;}
    case DT_TEXT:   {strName = GET_STR(STR_PRO_TEXT)   ; break;}
    case DT_ELLIPSE:{strName = GET_STR(STR_PRO_ELLIPSE); break;}
    case DT_COMPOSITION_LINE:   {strName = GET_STR(STR_PRO_COMPOSITION_LINE)   ; break;}
    case DT_DIM:    {strName = GET_STR(STR_PRO_DIM)    ; break;}

    case DT_DIM_L:    {strName = GET_STR(STR_MNU_L_DIM)    ; break;}
    case DT_DIM_H:    {strName = GET_STR(STR_MNU_H_DIM)    ; break;}
    case DT_DIM_V:    {strName = GET_STR(STR_MNU_V_DIM)    ; break;}
    case DT_DIM_A:    {strName = GET_STR(STR_MNU_A_DIM)    ; break;}
    case DT_DIM_D:    {strName = GET_STR(STR_MNU_D_DIM)    ; break;}
    case DT_DIM_R:    {strName = GET_STR(STR_MNU_R_DIM)    ; break;}
    case DT_DIM_C:    {strName = GET_STR(STR_MNU_C_DIM)    ; break;}

    case DT_SPLINE: {strName = GET_STR(STR_PRO_SPLINE) ; break;}
    case DT_NODE:   {strName = GET_STR(STR_PRO_NODE)   ; break;}
    case DT_CONNECTION_LINE:{strName = GET_STR(STR_PRO_CONNECTION_LINE)   ; break;}
    
    case DT_GROUP:      {strName = GET_STR(STR_PRO_GROUP)  ; break;}
    case DT_PARTS:      {strName = GET_STR(STR_PRO_PARTS)  ; break;}
    case DT_REFERENCE:  {strName = GET_STR(STR_PRO_REFERENCE)  ; break;}
    case DT_FIELD:  {strName = GET_STR(STR_PRO_FIELD)  ; break;}
    case DT_ELEMENT:{strName = GET_STR(STR_PRO_ELEMENT); break;}

    case DT_BOARD_IO:   {strName = GET_STR(STR_PRO_BOARD_IO)    ; break;}
    case DT_NET_IO:     {strName = GET_STR(STR_PRO_NET_IO)      ; break;}
    case DT_PARTS_DEF:  {strName = GET_STR(STR_PRO_PARTS_DEF)   ; break;}
    case DT_FIELD_DEF:  {strName = GET_STR(STR_PRO_FIELF_DEF)   ; break;}
    case DT_ELEMENT_DEF:{strName = GET_STR(STR_PRO_ELEMENT_DEF) ; break;}

    }
    return strName;
}

StdString VIEW_COMMON::GetObjctTypeVarBaseName(DRAWING_TYPE eType)
{
    StdString strName;
    switch(eType)
    {
    case DT_POINT:  {strName = _T("Point")  ; break;}
    case DT_CIRCLE: {strName = _T("Circle") ; break;}
    case DT_LINE:   {strName = _T("Line")   ; break;}
    case DT_TEXT:   {strName = _T("Text")   ; break;}
    case DT_ELLIPSE:{strName = _T("Ellipse") ; break;}
    case DT_COMPOSITION_LINE:   {strName = _T("CompositionLine")    ; break;}

    case DT_DIM:    
    case DT_DIM_L:   
    case DT_DIM_H:    
    case DT_DIM_V:    
    case DT_DIM_A:    
    case DT_DIM_D:    
    case DT_DIM_R:    
    case DT_DIM_C:    {strName = _T("Dimension")    ; break;}

    case DT_SPLINE: {strName = _T("Spline") ; break;}
    case DT_NODE:   {strName = _T("Node")   ; break;}
    case DT_CONNECTION_LINE:   {strName = _T("ConnectionLine")   ; break;}
    
    case DT_PARTS:  {strName = _T("Parts")  ; break;}
    case DT_FIELD:  {strName = _T("Field")  ; break;}
    case DT_ELEMENT:{strName = _T("Element")  ; break;}
    case DT_GROUP:  {strName = _T("Group")  ; break;}
    case DT_REFERENCE:{strName = _T("Reference")  ; break;}

    case DT_BOARD_IO:   {strName = _T("BoardIo")    ; break;}
    case DT_NET_IO:     {strName = _T("NetIo")      ; break;}
    case DT_PARTS_DEF:  {strName = _T("PartsDef")   ; break;}
    case DT_FIELD_DEF:  {strName = _T("FieldDef")   ; break;}
    case DT_ELEMENT_DEF:{strName = _T("ElementDef") ; break;}

    }
    return strName;
}
//*******************************
//             TEST
//*******************************
#ifdef _DEBUG

void TEST_DRAWING_TYPE()
{
    DB_PRINT(_T("%s Start\n"), DB_FUNC_NAME.c_str());


    DB_PRINT(_T("%s End\n"), DB_FUNC_NAME.c_str());
    DB_PRINT(_T("\n"));
}
#endif //_DEBUG