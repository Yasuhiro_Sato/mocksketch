/**
 * @brief			DRAWING_TYPEヘッダーファイル
 * @file			DRAWING_TYPE.h
 * @author			Yasuhiro Sato
 * @date			06-2-2009 15:00:30
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#if !defined(EA_1657CD0D_429A_4467_A059_C6252EA36F2A__INCLUDED_)
#define EA_1657CD0D_429A_4467_A059_C6252EA36F2A__INCLUDED_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
/**
 * @class   DRAWING_TYPE
 * @brief   描画要素種別                     
 */


enum DRAWING_TYPE
{
	//!< 設定なし
    DT_NONE     = 1, 
	//!< 点
	DT_POINT    = 2,
	//!< 円
	DT_CIRCLE   = 3,
	//!< 線
	DT_LINE     = 4,
	//!< 文字
	DT_TEXT     = 5,
	//!< スプライン
	DT_SPLINE   = 6,
	//!< 楕円
	DT_ELLIPSE  = 7,

	//!< 複合線
	DT_COMPOSITION_LINE     = 8,

    //!< 節点
	DT_NODE           = 10,

	//!< 接続線
    DT_CONNECTION_LINE      = 13,

	//!< グループ
    DT_GROUP          = 14,

	//!< 参照
    DT_REFERENCE      = 15,
       

    //!< 画像
    DT_IMAGE         = 16,
    
    //!< 寸法
	DT_DIM      = 0x100,
	DT_DIM_L    =  (1 | DT_DIM),
	DT_DIM_H    =  (2 | DT_DIM),
	DT_DIM_V    =  (3 | DT_DIM),
	DT_DIM_A    =  (4 | DT_DIM),
	DT_DIM_D    =  (5 | DT_DIM),
	DT_DIM_R    =  (6 | DT_DIM),
	DT_DIM_C    =  (7 | DT_DIM),

    //-------------------
    // スクリプト付き図形
    //-------------------
	//!< グループ
	DT_SCRIPTBASE = 0x1000,

	//!< グループ
	DT_PARTS    =   (10 | DT_SCRIPTBASE),

	//!< フィールド
	DT_FIELD   =    (11  | DT_SCRIPTBASE),

    //!< エレメント
	DT_ELEMENT  =   (12  | DT_SCRIPTBASE),

    //-------------------
    // スクリプト付きIO
    //-------------------
	//!< グループ
	DT_SCRIPTIO = 0x1400,

    //!< ボード
	DT_BOARD_IO =   (10  | DT_SCRIPTIO),

	//!< ボード
	DT_NET_IO =     (11 | DT_SCRIPTIO),
    //-------------------

    //-------------------------
    // スクリプト付き図形定義
    //-------------------------
	//!<
	DT_SCRIPTDEF = 0x2000,
	//!< グループ
	DT_PARTS_DEF    =   (10 | DT_SCRIPTDEF),

	//!< フィールド
	DT_FIELD_DEF   =    (11  | DT_SCRIPTDEF),

    //!< エレメント
	DT_ELEMENT_DEF  =   (12  | DT_SCRIPTDEF),

   	//!< 参照(スクリプトは使用しない)
	DT_REFERENCE_DEF =  (13 | DT_SCRIPTDEF),
       

};

enum E_NODE_CHANGE_TYPE
{
    NCT_NONE        = -1,
    NCT_POSITION    =  0,
    NCT_CONNECTION  =  1,
};

namespace VIEW_COMMON
{
//!< オブジェクト種別名称取得.
StdString GetObjctTypeName(DRAWING_TYPE eType);

//!< オブジェクト種別変数名称.
StdString GetObjctTypeVarBaseName(DRAWING_TYPE eType);

};//namespace VIEW_COMMON
#endif // !defined(EA_1657CD0D_429A_4467_A059_C6252EA36F2A__INCLUDED_)
