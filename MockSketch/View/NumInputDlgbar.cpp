/**
 * @brief			CNumInputDlgbar実装ファイル
 * @file			NumInputDlgbar.cpp
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "MockSketch.h"
#include "SYSTEM/CSystem.h"
#include "./NumInputDlgbar.h"


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNAMIC(CNumInputDlgbar, CDialogBar)

BEGIN_MESSAGE_MAP(CNumInputDlgbar, CDialogBar)
    ON_WM_SIZE()
    ON_BN_CLICKED(IDC_PB_PAD, &CNumInputDlgbar::OnBnClickedPbPad)
    ON_BN_CLICKED(IDC_PB_RET, &CNumInputDlgbar::OnBnClickedPbRet)
    ON_CBN_EDITUPDATE(IDC_CB_INPUT, &CNumInputDlgbar::OnCbnEditupdateCbInput)
    ON_CBN_CLOSEUP(IDC_CB_INPUT, &CNumInputDlgbar::OnCbnEditupdateCbCloseup)
    ON_CBN_DROPDOWN(IDC_CB_INPUT, &CNumInputDlgbar::OnCbnEditupdateCbDropDown)

    ON_MESSAGE(WM_INITDIALOG, OnInitDialog )    // <-- Add this line.
    ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

/**
 * コンストラクタ
 */
CNumInputDlgbar::CNumInputDlgbar():
m_eView(VIEW_NONE),
m_color(RGB(255,160,167)),
m_bInputError(false)
{
    m_hBrError = 0;
    m_lstInpMem.resize(VIEW_MODE_MAX);
}

/**
 * デストラクタ
 */
CNumInputDlgbar::~CNumInputDlgbar()
{
    if (m_hBrError)
    {
        ::DeleteObject(m_hBrError);
    }
}


/**
 *  @brief ダイアログデータ交換
 *  @param [in] pDX CDataExchange オブジェクトへのポインタ。
 *  @retval           なし
 *  @note
 */
void CNumInputDlgbar::DoDataExchange(CDataExchange* pDX)
{
	CDialogBar::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_PB_PAD, m_pbCal);
    DDX_Control(pDX, IDC_PB_RET, m_pbRet);
    DDX_Control(pDX, IDC_CB_INPUT, m_cbInput);
}

/**
 *  @brief ダイアログ初期化
 *  @param [in] wParam   
 *  @param [in] lParam  
 *  @retval           なし
 *  @note
 */
LRESULT CNumInputDlgbar::OnInitDialog(WPARAM wParam, LPARAM lParam)
{
    LRESULT bRet = HandleInitDialog(wParam, lParam);
    if (!UpdateData(FALSE))
    {
       DB_PRINT(_T("Warning: UpdateData failed during dialog init.\n"));
    } 
    m_color = RGB(255,160,167); //TODO: SYSTEM使用
    m_hBrError = ::CreateSolidBrush(m_color);
    return bRet;
}

/**
 *  @brief ウインドウサイズ変更
 *  @param [in] nType   要求されるサイズ変更のタイプ  (SIZE_MAXIMIZED....)
 *  @param [in] cx      クライアント領域の新しい幅 
 *  @param [in] cy      クライアント領域の新しい高さ
 *  @retval           なし
 *  @note
 */
void CNumInputDlgbar::OnSize(UINT nType, int cx, int cy)
{
    CDialogBar::OnSize(nType, cx, cy);

    /*
    CWnd* pMode    = GetDialogItem(IDC_ST_MODE);
    CWnd* pComment = GetDialogItem(IDC_ST_COMMNET);
    CWnd* pInput   = GetDialogItem(IDC_CB_INPUT);

    CWnd* pPad   = GetDialogItem(IDC_PB_PAD);
    CWnd* pRet   = GetDialogItem(IDC_PB_RET);
    CWnd* pInput = GetDialogItem(IDC_CB_INPUT);
    */
}


/**
 *  @brief 数値入力パッド表示ボタン押下
 *  @param    なし
 *  @retval   なし
 *  @note   
 */
void CNumInputDlgbar::OnBnClickedPbPad()
{
    int a=1;
}

void CNumInputDlgbar::OnBnClickedPbRet()
{
    SetReturnKey();
}

/**
 *  @brief  ウインドウプロシジャ
 *  @param    [in]  message  処理される Windows メッセージ
 *  @param    [in]  wParam   メッセージの処理で使う付加情報1
 *  @param    [in]  lParam   メッセージの処理で使う付加情報2
 *  @retval   メッセージに依存する値
 *  @note   
 */
LRESULT CNumInputDlgbar::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
    return CDialogBar::WindowProc(message, wParam, lParam);
}

/**
 *  @brief メッセージ処理前取得
 *  @param    [in]  pMsg  処理するメッセージを保持する MSG 構造体を指します。
 *  @retval   メッセージが変換されていて、ディスパッチできない場合は、0 以外を返します
 *  @note   
 */
BOOL CNumInputDlgbar::PreTranslateMessage(MSG* pMsg)
{
    //マウスホイールの動作があった場合
    if (pMsg->message == WM_MOUSEWHEEL)
    {
        ::SetFocus(m_hWndParent);
    }

    if ( pMsg->message == WM_KEYDOWN)
    {
        if (pMsg->wParam == VK_RETURN)
        {
            SetReturnKey();
        }
    }

    return CDialogBar::PreTranslateMessage(pMsg);

}

/**
 *  @brief コンボボックス更新通知
 *  @param    なし
 *  @retval   なし
 *  @note     コンボ ボックスのエディット コントロール部分に、
 *             変更されたテキストが表示される直前であることを通知します。
 */
void CNumInputDlgbar::OnCbnEditupdateCbInput()
{
    DB_PRINT(_T("CNumInputDlgbar::OnCbnEditupdateCbInput\n"));
}

void CNumInputDlgbar::OnCbnEditupdateCbCloseup()
{

    int iSel = m_cbInput.GetCurSel();

    if (iSel != CB_ERR)
    {

        CString rString;
        m_cbInput. GetLBText(iSel, rString);
        //文字を追加する
        m_cbInput.SetWindowText(rString);


    }

    DB_PRINT(_T("CNumInputDlgbar::OnCbnEditupdateCbCloseup\n"));
}

void CNumInputDlgbar::OnCbnEditupdateCbDropDown()
{
    DB_PRINT(_T("CNumInputDlgbar::OnCbnEditupdateCbDropDown\n"));
}


//!< 親ウインドウ設定
void CNumInputDlgbar::SetParent(HWND hWnd)
{
    m_hWndParent = hWnd;
}

//!< リターンキー押下
void CNumInputDlgbar::SetReturnKey()
{
    //とりあえず数値入力を通知
    ::SendMessage(m_hWndParent, WM_VAL_INPUT, 0, 0);

    //モードによって異なるため
    //入力値の判定その他は Action側で行う
}


//!< 入力更新
void CNumInputDlgbar::UpdateInput(bool bOk)
{
    if (!bOk)
    {
        // エラーダイアログを表示(Actionで行う)
        
        //文字列を選択状態にする
        m_cbInput.SetEditSel(-1, -1 );
        // 色を変える
        m_bInputError = true;
        return;
    }
    else
    {
        m_bInputError = false;
        //入力内容を記憶しておく
        StdString strInput;

        strInput = GetString();

        //同じ内容があるかどうか検索する

        int iIndex;
        iIndex = m_cbInput.FindStringExact(  -1,   strInput.c_str());

        if (iIndex != CB_ERR)
        {
            //同じ内容があれば削除しておく
            m_cbInput.DeleteString(   iIndex );
        }

        //文字を追加する
        m_cbInput.InsertString(  0, strInput.c_str() );
        
        long lSize = m_cbInput.GetCount();
        if (lSize == CB_ERR)
        {
            return;
        }
        
        if (lSize >= DRAW_CONFIG->GetInputStoreCount() )
        {
            lSize--;
            m_cbInput.DeleteString( lSize );
        }


        //表示タイプ毎に記憶しておく
        m_lstInpMem[m_eView].clear();
        CString strListBox;
        for (long lCnt = 0; lCnt < lSize; lCnt++)
        {
            m_cbInput.GetLBText(lCnt, strListBox);
            m_lstInpMem[m_eView].push_back( (const TCHAR*)strListBox);
        }

        iIndex = m_cbInput.FindStringExact(  -1,   strInput.c_str());
        if (iIndex != CB_ERR)
        {
            m_cbInput.SetCurSel(iIndex);
        }

        int iSel = m_cbInput.GetCurSel();

    }
}


//!< 入力文字列取得
StdString CNumInputDlgbar::GetString()
{
    CString stInput;
    CWnd* pInput   = GetDlgItem(IDC_CB_INPUT);
    pInput->GetWindowText(stInput);

    StdString strRet(stInput);
    return strRet;
}

//!< 種別表示
void CNumInputDlgbar::SetMode(VIEW_MODE eView)
{
    CWnd* pMode   = GetDlgItem(IDC_ST_MODE);

    CString strDisp = SYS_STR->StrViewMode( eView ).c_str();
    pMode->SetWindowText(strDisp);

    if (m_eView != eView)
    {
        //記憶している文字を設定する
        m_cbInput.ResetContent();
        foreach( StdString& strData, m_lstInpMem[eView])
        {
                m_cbInput.AddString(strData.c_str());
        }

        //文字が設定され定場合は表示する
        long lCnt = m_cbInput.GetCount();
        if (lCnt >= 1)
        {
            m_cbInput.SetCurSel(0);
            //文字列を選択状態にする
            m_cbInput.SetEditSel(-1, -1 );
        }
    }
    m_eView = eView;
}

//!< コメント表示
void CNumInputDlgbar::SetComment(StdString strType)
{
    CWnd* pMode   = GetDlgItem(IDC_ST_COMMNET);

    pMode->SetWindowText(strType.c_str());
}

void CNumInputDlgbar::ClearInput()
{
    m_cbInput.SetWindowText(_T(""));
}


bool CNumInputDlgbar::HasFocus()
{
    HWND hWnd = ::GetFocus();

    if (hWnd == this->m_hWnd)
    {
        return true;
    }

    if (hWnd == m_cbInput.m_hWnd)
    {
        return true;
    }

    COMBOBOXINFO  comboInfo;
    comboInfo.cbSize = sizeof COMBOBOXINFO;
    if (m_cbInput.GetComboBoxInfo(&comboInfo))
    {
        if (hWnd == comboInfo.hwndItem)
        {
            return true;
        }
    }
    return false;
}

//!< 入力可不可設定
void CNumInputDlgbar::EnableInput(bool bEnable)
{
    m_pbCal.EnableWindow( bEnable );
    m_pbRet.EnableWindow( bEnable);
    m_cbInput.EnableWindow( bEnable);
}


HBRUSH CNumInputDlgbar::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
    HBRUSH hbr = CDialogBar::OnCtlColor(pDC, pWnd, nCtlColor);

    HWND            hwnd = NULL;
    COMBOBOXINFO    comboInfo;    
    bool            bChangeColor = false;
    int             err = 0;
    
    hwnd = pWnd->GetSafeHwnd();
    
    comboInfo.cbSize = sizeof COMBOBOXINFO;
    if (m_cbInput.GetComboBoxInfo(&comboInfo))
    {
        if (hwnd == comboInfo.hwndItem)
        {
            if (m_bInputError)
            {
                // 文字色
                //pDC->SetTextColor(m_color);
                // 背景色
                hbr = m_hBrError;
                pDC->SetBkColor(m_color);
            }
            else
            {
                pDC->SetBkColor(::GetSysColor(COLOR_WINDOW));
            }
        }
    }
   
    return hbr;
}
