/**
 * @brief			CNumInputDlgbarヘッダーファイル
 * @file			CNumInputDlgbar.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#pragma once

#include "View/VIEW_MODE.h"

//  ダイアログ

class CNumInputDlgbar : public CDialogBar
{
	DECLARE_DYNAMIC(CNumInputDlgbar)
protected:
    //!< 数値入力パッド表示ボタン
    CButton m_pbCal;

    //!< リターン入力ボタン
    CButton m_pbRet;

    //!< 入力コンボボックス
    CComboBox m_cbInput;

    //入力値記憶
    std::vector< std::deque<StdString> > m_lstInpMem;
    
    HWND m_hWndParent;

    //!< 表示モード
    VIEW_MODE  m_eView;

    COLORREF m_color;

    HBRUSH m_hBrError;

    bool m_bInputError;
public:
	CNumInputDlgbar();   // 標準コンストラクタ
	virtual ~CNumInputDlgbar();

    //!< 親ウインドウ設定
    void SetParent(HWND hWnd);

    //!< 入力文字列取得
    StdString GetString();

    //!< 入力更新
    void UpdateInput(bool bOk);

    //!< 入力消去
    void ClearInput();

    bool HasFocus();

    //!< 種別表示
    void SetMode(VIEW_MODE eView);

    //!< コメント表示
    void SetComment(StdString strComemnt);

    //!< 入力可不可設定
    void EnableInput(bool bEnable);

	enum { IDD = IDD_DLG_NUM_INPUT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート

	DECLARE_MESSAGE_MAP()

protected:
    afx_msg LRESULT OnInitDialog ( WPARAM wParam, LPARAM lParam );

    //リターンキー入力
    void SetReturnKey();

    //!< ウインドウサイズ変更
    afx_msg void OnSize(UINT nType, int cx, int cy);

    //!<数値入力パッド表示ボタン押下
    afx_msg void OnBnClickedPbPad();
    afx_msg void OnBnClickedPbRet();

    afx_msg void OnCbnEditupdateCbInput();
    afx_msg void OnCbnEditupdateCbCloseup();
    afx_msg void OnCbnEditupdateCbDropDown();
    virtual BOOL PreTranslateMessage(MSG* pMsg);
    virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);

public:
    afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};
