/**
 * @brief			QUERY_REF_OBJECTヘッダーファイル
 * @file			QUERY_REF_OBJECT.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 20:48:43
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#if !defined(__QUERY_REF_OBJECT_H)
#define __QUERY_REF_OBJECT_H

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/

/**
 * @class   QUERY_REF_OBJECTl
 * @brief   参照オブジェクト問い合わせ用データ
 * @note    オブジェクトIDが負の時は部品の主DrawingParts
 */
struct QUERY_REF_OBJECT
{
    boost::uuids::uuid uuidParts;    // 部品ID
    int iObjectId;   // オブジェクトID

private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive& ar, const unsigned int version)
	{
		SERIALIZATION_INIT
			SERIALIZATION_BOTH("Parts", uuidParts);
			SERIALIZATION_BOTH("ObjectId", iObjectId);
		MOCK_EXCEPTION_FILE(Archive::is_loading::value ? e_file_read : e_file_write)
	}

};

#endif // __QUERY_REF_OBJECT_H
