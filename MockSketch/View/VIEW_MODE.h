/**
 * @brief			VIEW_MODEヘッダーファイル
 * @file			VIEW_MODE.h
 * @author			Yasuhiro Sato
 * @date			09-2-2009 20:48:43
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#if !defined(EA_4531AC81_1D4E_4e62_9919_92D9CC07677C__INCLUDED_)
#define EA_4531AC81_1D4E_4e62_9919_92D9CC07677C__INCLUDED_

/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
/**
 * @class   VIEW_MODE
 * @brief                        
   
 */
enum VIEW_MODE
{
    VIEW_NONE   = 0,
    VIEW_SELECT    ,//!< 選択
    VIEW_POINT,     //!< 点描画
    VIEW_LINE,      //!< 線分
    VIEW_CIRCLE,    //!< 円
    VIEW_CIRCLE_3P, //!< ３点を通る円
    VIEW_ARC,       //!< 円弧
    VIEW_NODE,      //!< 節点描画
    VIEW_IMAGE,     //!< 画像


    VIEW_ELLIPSE,    //!< 楕円
    VIEW_TEXT,       //!< 文字
    VIEW_SPLINE,     //!< スプライン
    VIEW_L_DIM,      //!< 寸法
    VIEW_H_DIM,      //!< 水平寸法
    VIEW_V_DIM,      //!< 垂直寸法
    VIEW_A_DIM,      //!< 角度
    VIEW_D_DIM,      //!< 直径
    VIEW_R_DIM,      //!< 半径
    VIEW_C_DIM,      //!< チャンファー
    VIEW_CONNECTION_LINE,  //!< 接続線


    VIEW_DROP,       //!< ドロップ処理

    //選択後操作
    //    動作後選択状態に遷移
    //ActionCommand
    VIEW_GROUP,          //!< グループ化
    VIEW_DELETE,         //!< 削除

    //CActionCreateへ
    VIEW_PARTS,          //!< 部品化
    VIEW_REFERENCE,      //!< 参照化
    VIEW_DISASSEMBLY,    //!< 分解

    //CActionNodeSelectへ
    VIEW_NODE_SELECT,    //!< ノード選択

    VIEW_MOVE,           //!< 移動
    VIEW_COPY,           //!< コピー
    VIEW_ROTATE,         //!< 回転
    VIEW_SCL,            //!< スケール
    VIEW_MIRROR,         //!< 鏡像


    //VIEW_LINESTYLE,      //!< 線種変更  プロパティの変更なので保留
    //VIEW_COLOR,          //!< 色変更

    VIEW_OFFSET,    //!< オフセット
    VIEW_TRIM,      //!< トリム
    VIEW_CORNER,    //!< コーナー
    VIEW_CHAMFER,   //!< 面取り
    VIEW_BREAK,     //!< 分割

    VIEW_COMPOSITION_LINE,  //!< 複合線
    VIEW_FILL,        //!< 塗りつぶし





    VIEW_MODE_MAX,

};




#endif // !defined(EA_4531AC81_1D4E_4e62_9919_92D9CC07677C__INCLUDED_)
