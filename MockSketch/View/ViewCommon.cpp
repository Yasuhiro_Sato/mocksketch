/**
 * @brief			ViewCommon実装ファイル
 * @file			ViewCommon.cpp
 * @author			Yasuhiro Sato
 * @date			09-2-2009 23:59:08
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/
#include "stdafx.h"
#include "./ViewCommon.h"
#include "System/CSystem.h"


/*---------------------------------------------------*/
/*  Macros                                           */
/*---------------------------------------------------*/

namespace VIEW_COMMON
{
    StdString PermissionToStr(EDIT_PERMISSION ePermisson)
    {
        StdString strRet;
        switch(ePermisson)
        {
        case EP_NONE:        // 制限なし
            strRet = GET_STR( STR_EP_NONE);
            break;

        case EP_SOURCE:        // ソース変更禁止
            strRet = GET_STR( STR_EP_SOURCE);
            break;

        case EP_DESIGN:        // デザイン変更禁止
            strRet = GET_STR( STR_EP_DESIGN);
            break;

        case EP_PROPERTY:        // プロパティ変更禁止
            strRet = GET_STR( STR_EP_PROPERTY);
            break;

        case EP_READONRY:        // 変更禁止
            strRet = GET_STR( STR_EP_READONRY);
            break;

        case EP_ENCRYPTION:    // ソースコード暗号化
            strRet = GET_STR( STR_EP_ENCRYPTION);
            break;

        default:
            strRet = _T("Unknown");
            break;
        }

        return strRet;
    }


    E_ITEM_TYPE DefToItem(E_OBJ_DEF_TYPE eDef)
    {
        switch(eDef)
        {
        case E_NONE:    { return OBJ_UNKNOWN;}
        case E_PARTS:{ return OBJ_PARTS;}
        case E_FIELD:   { return OBJ_FIELD;}
        case E_ELEMENT: { return OBJ_ELEMENT;}
        case E_REFERENCE: { return OBJ_REFERENCE;}
        default:{ return OBJ_UNKNOWN;}
        }
    }

    E_OBJ_DEF_TYPE ItemToDef(E_ITEM_TYPE eItem)
    {
        switch(eItem)
        {
        case OBJ_UNKNOWN:   { return E_NONE;}
        case OBJ_PARTS :   { return E_PARTS;}
        case OBJ_FIELD:     { return E_FIELD;}
        case OBJ_ELEMENT:   { return E_ELEMENT;}
        case OBJ_REFERENCE:   { return E_REFERENCE;}

        default:{ return E_NONE;}
        }
    }

    StdString GetProjectPrifx(E_PROJ_TYPE eProj)
    {
        StdString strRet = _T("Unknown");
        switch(eProj)
        {
        case E_PROJ_UNKNOWN:
            break;

        case E_PROJ_MAIN:
            strRet = _T("");
            break;

        case E_PROJ_LIB:
            strRet = _T("LIB:");
            break;

        default:
            break;
        }

        return strRet;
    }


    StdString GetArrowTypeToStr(E_ARROW_TYPE eType)
    {
        StdString strRet = _T("Unknown");
        switch(eType)
        {
        case AT_NONE:       {strRet = GET_STR( STR_AT_NONE); break;}
        case AT_DOT:        {strRet = GET_STR( STR_AT_DOT); break;}
        case AT_DOT_FILL:   {strRet = GET_STR( STR_AT_DOT_FILL); break;}
        case AT_OBLIQUE:    {strRet = GET_STR( STR_AT_OBLIQUE); break;}
        case AT_OPEN:       {strRet = GET_STR( STR_AT_OPEN); break;}
        case AT_FILLED:     {strRet = GET_STR( STR_AT_FILLED); break;}
        case AT_DATUM_BLANK:{strRet = GET_STR( STR_AT_DATUM_BLANK); break;}
        case AT_DATUM_FILL: {strRet = GET_STR( STR_AT_DATUM_FILL); break;}
        default:
            break;
        }
        return strRet;
    }

    StdString GetArrowSideToStr(E_ARROW_SIDE eSide)
    {
        StdString strRet = _T("Unknown");
        switch(eSide)
        {
        case AT_NONE:       {strRet = GET_STR( STR_ARS_NONE); break;}
        case AT_DOT:        {strRet = GET_STR( STR_ARS_START); break;}
        case AT_OBLIQUE:    {strRet = GET_STR( STR_ARS_END); break;}
        case AT_OPEN:       {strRet = GET_STR( STR_ARS_BOTH); break;}
        default:
            break;
        }
        return strRet;
    }

    StdString GetArrowLengthTypeToStr(E_DIM_LENGTH_TYPE eType)
    {
        StdString strRet = _T("Unknown");
        switch(eType)
        {
        case DLT_DECIMAL:       {strRet = GET_STR( STR_DLT_DECIMAL); break;}
        case DLT_FRACTION:      {strRet = GET_STR( STR_DLT_FRACTION); break;}
        case DLT_ENGINEERING:   {strRet = GET_STR( STR_DLT_ENGINEERING); break;}
        case DLT_ARCHTECTUAL:   {strRet = GET_STR( STR_DLT_ARCHTECTUAL); break;}
        default:
            break;
        }
        return strRet;
    }


    /**
     *  @brief   スナップ名称取得
     *  @param   [in]    eSnap    スナップ種別
     *  @retval  
     *  @note
     */
    StdString GetSnapName(VIEW_COMMON::E_SNAP_TYPE eSnap, bool bEnableNone)
    {
        StdString strString;

        if (bEnableNone && 
            (eSnap == SNP_NONE))
        {
            return GET_STR(STR_SNAP_NONE);
        }
        switch(eSnap)
        {
        case SNP_END_POINT       :{strString = GET_STR(STR_END_POINT    );break;}
        case SNP_MID_POINT       :{strString = GET_STR(STR_MID_POINT    );break;}
        case SNP_CROSS_POINT     :{strString = GET_STR(STR_CROSS_POINT  );break;}
        case SNP_ON_LINE         :{strString = GET_STR(STR_ON_LINE      );break;}
        case SNP_NEAR_POINT      :{strString = GET_STR(STR_NEAR_POINT   );break;}
        case SNP_CENTER          :{strString = GET_STR(STR_CENTER       );break;}
        case SNP_TANGENT         :{strString = GET_STR(STR_TANGENT      );break;}
        case SNP_FEATURE_POINT   :{strString = GET_STR(STR_FEATURE_POINT);break;}
        case SNP_DATUME_POINT    :{strString = GET_STR(STR_DATUM_POINT  );break;}
        case SNP_GRID            :{strString = GET_STR(STR_GRID         );break;}
        case SNP_POINT           :{strString = GET_STR(STR_POINT        );break;}
        case SNP_NODE            :{strString = GET_STR(STR_SNAP_NODE  );break;}   

        case SNP_ALL_POINT       :{strString = GET_STR(STR_PRO_POINT    );break;}

        case SNP_DISTANCE        :{strString = GET_STR(STR_DISTANCE  );break;}


        case SNP_ANGLE           :{strString = GET_STR(STR_ANGLE        );break;}
        case SNP_BISECTRIX       :{strString = GET_STR(STR_BISECTRIX    );break;}
        case SNP_NORMAL          :{strString = GET_STR(STR_NORMAL       );break;}

        case SNP_INPUT_VAL       :{strString = GET_STR(STR_INPUT_VAL    );break;}

        case SNP_CIRCLE       :{strString = GET_STR(STR_PRO_CIRCLE    );break;}
        case SNP_LINE         :{strString = GET_STR(STR_PRO_LINE      );break;}
        case SNP_ELLIPSE      :{strString = GET_STR(STR_PRO_ELLIPSE   );break;}
        case SNP_PARTS        :{strString = GET_STR(STR_PRO_PARTS     );break;}
        case SNP_TEXT         :{strString = GET_STR(STR_PRO_TEXT      );break;}


        case SNP_OBJECT       :{strString = GET_STR(STR_OBJECT    );break;}

 

        default:
            break;
        }
        return strString;
    }

}
