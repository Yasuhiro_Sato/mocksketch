/**
 * @brief			CViewCommon ヘッダーファイル
 * @file			CViewCommon.h
 * @author			Yasuhiro Sato
 * @date			07-2-2009 20:03:09
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */

#ifndef  _VIEW_COMMON_H__
#define  _VIEW_COMMON_H__
/*---------------------------------------------------*/
/*  Header files                                     */
/*---------------------------------------------------*/

/*---------------------------------------------------*/
/*  Class                                            */
/*---------------------------------------------------*/

/*---------------------------------------------------*/
/*  Enums                                            */
/*---------------------------------------------------*/

namespace VIEW_COMMON
{
    enum E_DIM_DISC_TYPE
    {


    };

    enum E_OBJ_DEF_TYPE
    {
        E_NONE = -1,
        E_PARTS = 0,     //CDrawingPartsDef
        E_FIELD    = 1,     //CFieldDef
        E_ELEMENT  = 2,     //CElementDef
        E_REFERENCE = 3,

    };

    enum E_DOCUMENT_TYPE
    {
        E_DOC_NONE      =  0x0000,
        E_DOC_TEXT      =  0x0100,
        E_DOC_SCRIPT    =  0x0101,
        E_DOC_DEF       =  0x0200,
        //E_DOC_ASSY      =  0x0201,
        //E_DOC_SYMBOL    =  0x0202,
        //E_DOC_FIELD     =  0x0203,
        //E_DOC_CONNECTER =  0x0204,
        //E_DOC_JOINT     =  0x0205,
        //E_DOC_INDICATOR =  0x0206,
        E_DOC_DRAWING   =  0x0400,
    };

    enum E_PROJ_TYPE
    {
        E_PROJ_UNKNOWN = -1,    //不明
        E_PROJ_MAIN,            //プロジェクト
        E_PROJ_LIB              //ライブラリ
    };

    //Tree表示種別(アイコン)
    enum E_ITEM_TYPE
    {
        OBJ_UNKNOWN = -1,  //!< 不明
        OBJ_FOLDER  = 0,  //!< フォルダー   
        OBJ_PARTS   = 1,  //!< 図形   
        OBJ_SCRIPT  = 2,  //!< スクリプト  
        OBJ_REFERENCE   = 3,  //!< 参照 
        OBJ_ELEMENT = 4,  //!< エレメント
        OBJ_FIELD   = 5,  //!< フィールド
        OBJ_ROOT        = 6,  //!< ルート
    };

    //スナップ種別
    enum E_SNAP_TYPE
    {
        SNP_NONE            = 0x00000000,
        SNP_END_POINT       = 0x00000001,   // 端点
        SNP_MID_POINT       = 0x00000002,   // 中点
        SNP_CROSS_POINT     = 0x00000004,   // 交点
        SNP_ON_LINE         = 0x00000008,   // 線上
        SNP_NEAR_POINT      = 0x00000010,   // 近接点
        SNP_CENTER          = 0x00000020,   // 中心点
        SNP_TANGENT         = 0x00000040,   // 接線
        SNP_FEATURE_POINT   = 0x00000080,   // 特徴点
        SNP_DATUME_POINT    = 0x00000100,   // 基準点
        SNP_GRID            = 0x00000200,   // グリッド
        SNP_POINT           = 0x00000400,   // 点
        SNP_NORM            = 0x00000800,   // 近接点(垂線)
        SNP_NODE            = 0x00001000,   // ノード


        SNP_ALL_POINT       = 0x0000FFFF,   // 点全般
        //------------------------------------------------------
        SNP_DISTANCE          = 0x00010000,   // 距離
        //SNP_OFFSET          = 0x00002000,   // オフセット

        SNP_ALL_DISTANCE    = 0x000F0000,   // 距離全般
        //------------------------------------------------------

        SNP_ANGLE           = 0x00100000,   // 角度
        SNP_BISECTRIX       = 0x00200000,   // 角２等分線
        SNP_NORMAL          = 0x00400000,   // 垂線

       
        //------------------------------------------------------
        SNP_INPUT_VAL       = 0x00800000,   // 入力値

        //------------------------------------------------------
        SNP_CIRCLE          = 0x01000000,   // 円
        SNP_LINE            = 0x02000000,   // 線
        SNP_ELLIPSE         = 0x04000000,   // 楕円
        SNP_PARTS           = 0x08000000,   // 部品
        SNP_TEXT            = 0x10000000,   // 文字
        SNP_OBJECT          = 0xFF000000,   // オブジェクト 

        //変更時はCViewAction::GetSnapNameも修正すること
        SNP_ALL             =0xFFFFFFFF,
        
    };

    enum UI_TYPE
    {
        NEMU_BAR,
        TOOL_BAR,
        BUILD_BAR,
        STATUS_BAR,
        USER_IMAGE,
        FORMAT_BAR,
        DRAW_BAR,
        LINE_BAR,
        EDIT_BAR,
        FIND_BAR,
        SNAP_BAR,
        SHOW_BAR,
        OBJ_WINDOW,
        LIB_WINDOW,
        OUT_WINDOW,
        PROP_WINDOW,
        ZOOM_WINDOW,
        LAYER_WINDOW,
        IO_WINDOW,
        IO_REF_WINDOW_1,
        IO_REF_WINDOW_2,
        IO_REF_WINDOW_3,
        IO_REF_WINDOW_4,
        IO_REF_WINDOW_5,
        STACK_WINDOW,
        WATCH_WINDOW,
        MONITOR_WINDOW,
        DEF_WINDOW,
    };

    enum EXPAND_TYPE
    {
        EXP_NONE,
        EXP_PLUS,
        EXP_MINUS
    };

    //
    enum EDIT_PERMISSION
    {
        EP_NONE     = 0x0000,        // 制限なし
        EP_SOURCE   = 0x0010,        // ソース変更禁止
        EP_DESIGN   = 0x0020,        // デザイン変更禁止
        EP_PROPERTY = 0x0040,        // プロパティ変更禁止
        EP_READONRY = 0x0070,        // 変更禁止
        EP_ENCRYPTION = 0x0100,     // ソースコード暗号化
    };


    enum E_ARROW_TYPE
    {
        AT_NONE,        
        AT_DOT,
        AT_DOT_FILL,
        AT_OBLIQUE,
        AT_OPEN,
        AT_FILLED,
        AT_DATUM_BLANK,
        AT_DATUM_FILL
    };

    enum E_ARROW_SIDE
    {
        ARS_NONE  = 0x00,
        ARS_START = 0x01,
        ARS_END   = 0x02,
        ARS_BOTH  = 0x03,
    };

    enum E_DIM_LENGTH_TYPE
    {
        DLT_DECIMAL,            //10進表記
        DLT_FRACTION,           //分数表記
        DLT_ENGINEERING,        //工業図表記   インチのみ
        DLT_ARCHTECTUAL,        //建築図面表記 インチのみ
    };

    enum E_DIM_ANGLE_TYPE
    {
        DIA_DECIMAL,            //10進表記
        DIA_DEGMINSEC,          //度。分。秒
        DIA_RADIANS,            //ラジアン
    };

    enum E_DISP_ORDER
    {
        E_TOPMOST,
        E_TOPBACK,
        E_FRONT,
        E_BACK,
    };


    StdString PermissionToStr(EDIT_PERMISSION ePermission);

    E_ITEM_TYPE DefToItem(E_OBJ_DEF_TYPE eDef);

    E_OBJ_DEF_TYPE ItemToDef(E_ITEM_TYPE eItem);

    StdString GetProjectPrifx(E_PROJ_TYPE eProj);

    StdString GetArrowTypeToStr(E_ARROW_TYPE eType);

    StdString GetArrowSideToStr(E_ARROW_SIDE eSide);

    StdString GetArrowLengthTypeToStr(E_DIM_LENGTH_TYPE eType);

    StdString GetSnapName(E_SNAP_TYPE eSnap, bool bEnableNone = false);


    struct OBJ_TREE_DATA
    {
        boost::uuids::uuid    uuidDef;    //オブジェクトUUID
        int                   iScriptNo;  //スクリプト番号


    private:
        friend class boost::serialization::access;  

        template<class Archive>
        void serialize(Archive& ar, const unsigned int version)
        {
            SERIALIZATION_INIT
                SERIALIZATION_BOTH("Uuid"         ,  uuidDef);      
                SERIALIZATION_BOTH("ScriptNo"     ,  iScriptNo);      
            MOCK_EXCEPTION_FILE(Archive::is_loading::value? e_file_read:e_file_write )
        }
    };

    struct VIEW_VALUE
    {
        //ウインドウ幅
        int      iWidth;

        //ウインドウ高さ
        int      iHeight;

        //!< 表示スケール
        double   dDspScl;

        //!< 注視点
        POINT    ptLook;

        VIEW_VALUE()
        {
            Clear();
        }

        void Clear()
        {
            iWidth = 0;
            iHeight = 0;
            dDspScl = 0.0;
            ptLook.x = 0;
            ptLook.y = 0;
        }

        VIEW_VALUE& operator = (const VIEW_VALUE& m)
        {
            iWidth = m.iWidth;
            iHeight = m.iHeight;
            dDspScl = m.dDspScl;
            ptLook.x = m.ptLook.x;
            ptLook.y = m.ptLook.x;
            return *this;
        }

        //!< 等価演算子 (AS)
        bool operator == (const VIEW_VALUE& m) const
        {
            if (iWidth != m.iWidth) { return false; }
            if (iHeight != m.iHeight) { return false; }
            if (fabs(dDspScl - m.dDspScl)> NEAR_ZERO) { return false; }
            if (ptLook.x != m.ptLook.x) { return false; }
            if (ptLook.y != m.ptLook.x) { return false; }
            return true;
        }

        //!< 不等価演算子
        bool operator != (const VIEW_VALUE& m) const
        {
            return !(*this == m);
        }
    };


};//namespace VIEW_COMMON

#endif  //VIEW_COMMON
