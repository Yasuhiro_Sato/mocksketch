/**
 * @brief			ViewTypeヘッダーファイル
 * @file			ViewType.h
 * @author			Yasuhiro Sato
 * @date			01-2-2009 10:36:24
 * 
 * @note	
 * - Copyright:     Copyright (C) 2010 MockTools Ltd.
 * - Compiler:      Visual Studio 2008 (Visual C++8.0 + MFC)
 * - Target system:	Windows XP Professional(SP3)
 * 
 * @remarks	        
 *			
 *
 * $
 * $
 * 
 */
#ifndef __VIEW_TYPE_
#define __VIEW_TYPE_

enum VIEW_TYPE
{
    VT_EDIT,
    VT_CAD,
};

#endif //__VIEW_TYPE_
