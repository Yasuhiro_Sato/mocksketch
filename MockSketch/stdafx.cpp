
// stdafx.cpp : 標準インクルード MockSketch.pch のみを
// 含むソース ファイルは、プリコンパイル済みヘッダーになります。
// stdafx.obj にはプリコンパイルされた型情報が含まれます。

#include "stdafx.h"
#include <imagehlp.h>


enum E_RING_BUF
{
    BUF_SET,
    BUF_GET,
    BUF_POP
};

const TCHAR* OperateOutputBuffer(E_OUTPUT_WINDOW eWin, E_RING_BUF eMode, TCHAR* pChar)
{
    static std::deque<StdString> queStr[WIN_OUTPUT_MAX];
    const TCHAR* pRet;

    if (eMode == BUF_SET)
    {
        StdString strTmp = pChar;
        queStr[eWin].push_back(strTmp);
        pRet = queStr[eWin].at(queStr[eWin].size() - 1).c_str();
        return pRet;
    }
    else if (eMode == BUF_GET)
    {
        if (queStr[eWin].size() == 0)
        {
            return NULL;
        }
        pRet = queStr[eWin].at(0).c_str();
        return pRet;
    }
    else if (eMode == BUF_POP)
    {
        if (queStr[eWin].size() == 0)
        {
            return NULL;
        }
        queStr[eWin].pop_front();
    }
    return NULL;
}

const TCHAR* GetOutputBuffer(E_OUTPUT_WINDOW eWin)
{
    return OperateOutputBuffer( eWin, BUF_GET, NULL);
}

void PopOutputBuffer(E_OUTPUT_WINDOW eWin)
{
    OperateOutputBuffer( eWin, BUF_POP, NULL);
}


/**
 * @brief   デバッグウインドウ表示.
 * @param   [in] eWin         出力ウインドウ種別
 * @param   [in] pOutputStr   出力文字(printfと同一形式)
 * @param   [in] argptr       可変引数
 * 
 * @return  なし
 * @note    通常は PrtintOutを使用します
 */
void PrtintOutV  (E_OUTPUT_WINDOW eWin, const TCHAR* pOutputStr, va_list argptr)
{
    static const int iBufMax  =  1024;
    TCHAR szBuf[iBufMax];

    int iRet;

    iRet = _vsntprintf(szBuf, iBufMax, pOutputStr, argptr);
    if (iRet < 0)
    {
        //書き込みエラー
        return;
    }

    const TCHAR* pChar;
    pChar = OperateOutputBuffer(eWin, BUF_SET, szBuf);

    //メインフレームに送信
    if(AfxGetApp()->m_pMainWnd)
    {
        PostMessage(AfxGetApp()->m_pMainWnd->m_hWnd, WM_OUTPUT_PRINT, (WPARAM)eWin, (LPARAM)pChar);
    }
}

/**
 * @brief   デバッグウインドウ表示.
 * @param   [in] eWin         出力ウインドウ種別
 * @param   [in] pOutputStr   出力文字(printfと同一形式)
 * 
 * @return  
 */
void PrtintOut(E_OUTPUT_WINDOW eWin, const TCHAR* pOutputStr, ...)
{
    va_list arglist; 
    va_start(arglist, pOutputStr); 
    PrtintOutV  (eWin, pOutputStr, arglist);
    va_end(arglist); 
}


StdString CharToString(const char* str)
{
    StdString strRet = _T("");
#ifdef  UNICODE             
  	wchar_t*	psToSemStringBuf;
	int			iToSemStringLen;
    std::string sFromString;
    sFromString = str;
    size_t iConvCnt;

    size_t iLen     =  sFromString.length() + 1 ;
    size_t iBufSize =  iLen * sizeof(wchar_t);
	psToSemStringBuf = (wchar_t*)malloc( iBufSize );
	iToSemStringLen = (int)mbstowcs_s( &iConvCnt, psToSemStringBuf , iLen,
								sFromString.c_str(),
								iLen );
	if ( iToSemStringLen >= 0 )
	{
		strRet = psToSemStringBuf;
	}
	free( psToSemStringBuf );

	return strRet;

#else
     strRet = str;
     return strRet;
#endif
}


/**
 * @brief   デバッグウインドウ表示.
 * @param   [in] eWin         出力ウインドウ種別
 * @return  なし
 * @note    通常は PrtintOutを使用します
 */
void ChangeOutputWindow  (E_OUTPUT_WINDOW eWin)
{
    CWnd* pWnd = AfxGetApp()->m_pMainWnd;
    //メインフレームに送信
    if (pWnd)
    {
        SendMessage(pWnd->m_hWnd, WM_OUTPUT_SEL, (WPARAM)eWin, 0);
    }
}

/**
 * @brief   デバッグウインドウ表示.
 * @param   [in] eWin         出力ウインドウ種別
 * @return  なし
 * @note    通常は PrtintOutを使用します
 */
void ClearOutputWindow  (E_OUTPUT_WINDOW eWin)
{
    //メインフレームに送信
    CWnd* pWnd = AfxGetApp()->m_pMainWnd;
    //メインフレームに送信
    if (pWnd)
    {
        SendMessage(pWnd->m_hWnd, WM_OUTPUT_CLEAR, (WPARAM)eWin, 0);
    }
}

//構造化例外
void se_translator_function(unsigned int code, struct _EXCEPTION_POINTERS* ExInfo)
{
#ifdef _WIN64
    STACKFRAME64 sf;
#else 
    STACKFRAME sf;
#endif

    BOOL bResult;
    FILE *stream;
    StdChar  filename[256];

    size_t iSize = sizeof(filename)/ sizeof(StdChar);
    _stprintf_s(filename, iSize, _T("app_problem.log"));
    stream = _tfopen( filename, _T("w") );


    /* スタックフレームの初期化 */
    ZeroMemory(&sf, sizeof(sf));
#ifdef _M_X64
    sf.AddrPC.Offset    = ExInfo->ContextRecord->Rip;
    sf.AddrStack.Offset = ExInfo->ContextRecord->Rsp;
    sf.AddrFrame.Offset = ExInfo->ContextRecord->Rbp;
#endif

#ifdef _M_IX86
    sf.AddrPC.Offset    = ExInfo->ContextRecord->Eip;
    sf.AddrStack.Offset = ExInfo->ContextRecord->Esp;
    sf.AddrFrame.Offset = ExInfo->ContextRecord->Ebp;
#endif

    sf.AddrPC.Mode      = AddrModeFlat;
    sf.AddrStack.Mode   = AddrModeFlat;
    sf.AddrFrame.Mode   = AddrModeFlat;

    /* シンボルハンドラの初期化 */
    SymInitialize(GetCurrentProcess(), NULL, TRUE);

    switch(ExInfo->ExceptionRecord->ExceptionCode)
    {
      case EXCEPTION_ACCESS_VIOLATION        :     _ftprintf( stream, _T("ACCESS_VIOLATION \n")); break;
      case EXCEPTION_ARRAY_BOUNDS_EXCEEDED   :     _ftprintf( stream, _T("ARRAY_BOUNDS_EXCEEDED \n")); break;
      case EXCEPTION_BREAKPOINT              :     _ftprintf( stream, _T("BREAKPOINT \n")); break;
      case EXCEPTION_DATATYPE_MISALIGNMENT   :     _ftprintf( stream, _T("DATATYPE_MISALIGNMENT \n")); break;   
      case EXCEPTION_FLT_DENORMAL_OPERAND    :     _ftprintf( stream, _T("FLT_DENORMAL_OPERAND \n")); break;    
      case EXCEPTION_FLT_DIVIDE_BY_ZERO      :     _ftprintf( stream, _T("FLT_DIVIDE_BY_ZERO \n")); break;     
      case EXCEPTION_FLT_INEXACT_RESULT      :     _ftprintf( stream, _T("FLT_INEXACT_RESULT \n")); break;     
      case EXCEPTION_FLT_INVALID_OPERATION   :     _ftprintf( stream, _T("FLT_INVALID_OPERATION \n")); break;  
      case EXCEPTION_FLT_OVERFLOW            :     _ftprintf( stream, _T("FLT_OVERFLOW \n")); break;           
      case EXCEPTION_FLT_STACK_CHECK         :     _ftprintf( stream, _T("FLT_STACK_CHECK \n")); break;        
      case EXCEPTION_FLT_UNDERFLOW           :     _ftprintf( stream, _T("FLT_UNDERFLOW \n")); break;          
      case EXCEPTION_ILLEGAL_INSTRUCTION     :     _ftprintf( stream, _T("ILLEGAL_INSTRUCTION \n")); break;    
      case EXCEPTION_IN_PAGE_ERROR           :     _ftprintf( stream, _T("IN_PAGE_ERROR  \n")); break;         
      case EXCEPTION_INT_DIVIDE_BY_ZERO      :     _ftprintf( stream, _T("INT_DIVIDE_BY_ZERO \n")); break;     
      case EXCEPTION_INT_OVERFLOW            :     _ftprintf( stream, _T("INT_OVERFLOW \n")); break;           
      case EXCEPTION_INVALID_DISPOSITION     :     _ftprintf( stream, _T("INVALID_DISPOSITION \n")); break;    
      case EXCEPTION_NONCONTINUABLE_EXCEPTION:     _ftprintf( stream, _T("NONCONTINUABLE_EXCEPTION \n")); break;
      case EXCEPTION_PRIV_INSTRUCTION        :     _ftprintf( stream, _T("PRIV_INSTRUCTION \n")); break;       
      case EXCEPTION_SINGLE_STEP             :     _ftprintf( stream, _T("SINGLE_STEP \n")); break;            
      case EXCEPTION_STACK_OVERFLOW          :     _ftprintf( stream, _T("STACK_OVERFLOW  \n")); break;        
      default :       break;
    }

    DWORD dwMachineType;

#ifdef _M_IX86
    dwMachineType = IMAGE_FILE_MACHINE_I386;
#endif
#ifdef _M_X64
    dwMachineType = IMAGE_FILE_MACHINE_AMD64;
#endif
#ifdef _M_IA64
    dwMachineType = IMAGE_FILE_MACHINE_IA64;
#endif

    //TODO:
    // C6387	無効なパラメーター値	'_Param_(5)' は '0' 
    // である可能性があります: この動作は、関数 'StackWalk' の指定に従っていません。	
    // MockSketch	stdafx.cpp	224

	CONTEXT             context;

	bool first = true;
	DWORD64 pFrame = 0;
	DWORD64 pPrevFrame = 0;

	HANDLE hProcess =  GetCurrentProcess();
	HANDLE hThread   = GetCurrentThread();
	
	/* スタックフレームを順に表示していく */
    for(;;) 
	{
        /* 次のスタックフレームの取得 */
        bResult = StackWalk64(
            dwMachineType,
			hProcess,
			hThread,
            &sf,
			&context,
            NULL,
            SymFunctionTableAccess,
            SymGetModuleBase,
            NULL);

        /* 失敗ならば、ループを抜ける */
		pFrame = sf.AddrFrame.Offset;
        if(!bResult || pFrame == 0) break;

		if (!first) 
		{
			if (pFrame <= pPrevFrame) 
			{
				// Sanity check
				break;
			}
			
			if ((pFrame - pPrevFrame) > 10000000) 
			{
				// Sanity check
				break;
			}
		}
		if ((pFrame % sizeof(void*)) != 0) {
			// Sanity check
			break;
		}

		pPrevFrame = pFrame;
		first = false;

		//------------------------
		enum { emMaxNameLength = 512 };
		// Use union to ensure proper alignment
		union {
			SYMBOL_INFO symb;
			BYTE symbolBuffer[sizeof(SYMBOL_INFO) + emMaxNameLength];
		} u;
		PSYMBOL_INFO pSymbol = &u.symb;
		pSymbol->SizeOfStruct = sizeof(SYMBOL_INFO);
		pSymbol->MaxNameLen = emMaxNameLength;

		PDWORD64 symDisplacement = 0;  // Displacement of the input address,
									// relative to the start of the symbol

		DWORD lineDisplacement = 0;
		IMAGEHLP_LINE64  line;
		line.SizeOfStruct = sizeof(line);
		line.LineNumber = 0;
		BOOL bLine = FALSE;


        /* プログラムカウンタから関数名を取得 */
		bLine = SymGetLineFromAddr64(hProcess, sf.AddrPC.Offset, &lineDisplacement, &line);
		if (SymFromAddr(hProcess, sf.AddrPC.Offset, symDisplacement, pSymbol))
		{
			if (SymFromAddr(hProcess, sf.AddrPC.Offset,
				symDisplacement, pSymbol))
			{
				StdString strName = CharToString(pSymbol->Name);

				if (bLine) {
					_ftprintf(stream, _T("0x%016I64x + 0x%I64x  %s() line %d \n"),
						sf.AddrPC.Offset, pFrame,
						strName.c_str(), line.LineNumber);
				}
				else {
					_ftprintf(stream, _T("0x%016I64x + 0x%I64x  %s() \n"),
						sf.AddrPC.Offset, pFrame,
						strName.c_str());
				}
			}
			else    // No symbol found.  Print out the logical address instead.
			{
				DWORD err = GetLastError();
				_ftprintf(stream, _T("0x%016I64x + 0x%I64x (err = %d)\n"),
					sf.AddrPC.Offset, pFrame, err);
			}
		}

	} // while


    /* 後処理 */
    SymCleanup(GetCurrentProcess());
    fclose( stream );

    MessageBox(  NULL, _T("file \nto the person in charge."),_T("Application Error"),MB_OK ); 
}




