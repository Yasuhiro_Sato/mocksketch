// DlgSetting.cpp : 実装ファイル
//

#include "stdafx.h"
#include "MtEditor.h"
#include "DlgSetting.h"
#include <memory>


// CDlgSetting ダイアログ

IMPLEMENT_DYNAMIC(CDlgSetting, CDialog)



CEditorPropertyGrid::CEditorPropertyGrid()
{
    m_strGroupEditor        = _T("エディタ");
        m_strGroupDisplay   = _T("表示設定");
            m_strWordwrap   = _T("折り返し");
            m_strWordwrapExplanation = _T("画面幅で折り返す時は TRUE");

            m_strLineNo     = _T("行番号表示");
            m_strLineNoExplanation  = _T("行番号を表示する時は TRUE");

            //空白表示
            m_strDispSpace = _T("空白表示");
            m_strDispSpaceExplanation = _T("空白、タブを可視化する時は TRUE");

            //オートインデント
            m_strAutoIndent = _T("オートインデント");
            m_strAutoIndentExplanation = _T("インデント位置を前の行に合わせる時は TRUE");

            //自動メンバー表示
            m_strAutoMember = _T("自動メンバー表示");
            m_strAutoMemberExplanation = _T("自動でクラスメンバーを表示する時は TRUE");

            //タブサイズ
            m_strTabSize = _T("タブサイズ");
            m_strTabSizeExplanation = _T("タブの幅を空白の数で入力");


        m_strGroupFont  = _T("フォント");
            m_strFont   = _T("フォント");
            m_strFontExplanation = _T("エディタで使用するフォントを設定します");

        m_strGroupColor = _T("色");

            m_strColorText            = _T("文字色");
            m_strColorTextExplanation = _T("テキストの色");

            m_strBackText             = _T("背景色");
            m_strBackTextExplanation  = _T("文字の背景色");

            m_strColorCommnet               = _T("コメント");
            m_strColorCommnetExplanation    = _T("コメントの色");  

            m_strColorSel                   = _T("選択");
            m_strColorSelExplanation        = _T("選択色");

            m_strColorBack                  = _T("選択背景");
            m_strColorBackExplanation       = _T("選択背景色");

            m_strColorReserv1               = _T("予約語1");
            m_strColorReserv1Explanation    = _T("予約語1の色");

            m_strColorReserv2               = _T("予約語2");
            m_strColorReserv2Explanation    = _T("予約語2の色");

            m_strColorReserv3               = _T("予約語3");
            m_strColorReserv3Explanation    = _T("予約語3の色");

            m_strColorString                = _T("文字列色");
            m_strColorStringExplanation     = _T("リテラル文字列の色");

            m_strColorLineNo                = _T("行番号");
            m_strColorLineNoExplanation     = _T("行番号の色");
}


void CEditorPropertyGrid::Setup()
{
    EnableHeaderCtrl();
    EnableDescriptionArea();
    SetVSDotNetLook(true/*m_bDotNetLook*/);
    MarkModifiedProperties(true/*m_bMarkChanged*/);
    SetAlphabeticMode(false/*!m_bPropListCategorized*/);
    SetShowDragContext(true/*m_bShowDragContext*/);

    std::auto_ptr<CMFCPropertyGridProperty> apGroupEditor(new CMFCPropertyGridProperty(m_strGroupEditor.c_str()));

    CMFCPropertyGridProperty* pGroupDisp = new CMFCPropertyGridProperty(m_strGroupDisplay.c_str());
	apGroupEditor->AddSubItem(pGroupDisp);

    //===============
    // BOOL値
    //折り返し
    COleVariant varWardwrap((short)(m_es.bWordwrap ? VARIANT_TRUE: VARIANT_FALSE), VT_BOOL);
    CMFCPropertyGridProperty* pPropWordwrap = 
        new CMFCPropertyGridProperty(m_strWordwrap.c_str(),
        varWardwrap, 
        m_strWordwrapExplanation.c_str());
    pPropWordwrap->SetData(e_Wordwrap);
	pGroupDisp->AddSubItem(pPropWordwrap);

    //行番号表示
	COleVariant varLienNo((short)(m_es.bEnableLineNumber ? VARIANT_TRUE: VARIANT_FALSE), VT_BOOL);
    CMFCPropertyGridProperty* pPropLineNo = 
        new CMFCPropertyGridProperty(m_strLineNo.c_str(),
        varLienNo, 
        m_strLineNoExplanation.c_str());
    pPropLineNo->SetData(e_LineNo);
	pGroupDisp->AddSubItem(pPropLineNo);


    //空白表示
	COleVariant varDispSpace((short)(m_es.bDispSpace ? VARIANT_TRUE: VARIANT_FALSE), VT_BOOL);
    CMFCPropertyGridProperty* pDispSpace = 
        new CMFCPropertyGridProperty(m_strDispSpace.c_str(),
        varDispSpace, 
        m_strDispSpaceExplanation.c_str());
    pDispSpace->SetData(e_DispSpace);
	pGroupDisp->AddSubItem(pDispSpace);


    //オートインデント
	COleVariant varAutoIndent((short)(m_es.bAutoIndent ? VARIANT_TRUE: VARIANT_FALSE), VT_BOOL);
    CMFCPropertyGridProperty* pAutoIndent = 
        new CMFCPropertyGridProperty(m_strAutoIndent.c_str(),
        varAutoIndent, 
        m_strAutoIndentExplanation.c_str());
    pPropLineNo->SetData(e_AutoIndent);
	pGroupDisp->AddSubItem(pAutoIndent);


   //自動メンバー表示
	COleVariant varAutoMember((short)(m_es.bAutoMember ? VARIANT_TRUE: VARIANT_FALSE), VT_BOOL);
    CMFCPropertyGridProperty* pAutoMember = 
        new CMFCPropertyGridProperty(m_strAutoMember.c_str(),
        varAutoMember, 
        m_strAutoMemberExplanation.c_str());
    pAutoMember->SetData(e_AutoMember);
	pGroupDisp->AddSubItem(pAutoMember);

    //タブサイズ
    COleVariant varTabSize((long)m_es.iTabSize, VT_I4);
    CMFCPropertyGridProperty* pTabSize = 
        new CMFCPropertyGridProperty(m_strTabSize.c_str(),
        varTabSize, 
        m_strTabSizeExplanation.c_str());
    pTabSize->SetData(e_TabSize);
	pGroupDisp->AddSubItem(pTabSize);


    //===============
    // フォント
    CMFCPropertyGridProperty* pGroupFont = new CMFCPropertyGridProperty(m_strGroupFont.c_str());
	apGroupEditor->AddSubItem(pGroupFont);


    CMFCPropertyGridFontProperty* pFontProp = new CMFCPropertyGridFontProperty(m_strFont.c_str(),
        m_es.lf, /*CF_EFFECTS |*/ CF_NOSTYLESEL|CF_SCREENFONTS,
        m_strFontExplanation.c_str());
    pFontProp->SetData(e_Font);
	pGroupFont->AddSubItem(pFontProp);
/*
	m_pGroupFont->AddSubItem(new CMFCPropertyGridProperty(_T("Use System Font"), 
        (COleVariant)VARIANT_TRUE, _T("Specifies that the dialog uses MS Shell Dlg font")));
*/

    //===============
    // 色
	//CMFCPropertyGridProperty* pGroupColor = new CMFCPropertyGridProperty(_T("色"));
	std::auto_ptr<CMFCPropertyGridProperty> apGroupColor(new CMFCPropertyGridProperty(m_strGroupColor.c_str()));



	CMFCPropertyGridColorProperty* pColorText = new 
        CMFCPropertyGridColorProperty(m_strColorText.c_str(), 
        m_es.crText, NULL, m_strColorTextExplanation.c_str());
	pColorText->EnableOtherButton(_T("Other..."));
	pColorText->EnableAutomaticButton(_T("Default"), ::GetSysColor(COLOR_WINDOWTEXT));
    pColorText->SetData(e_ColorText);
    apGroupColor->AddSubItem(pColorText);

	CMFCPropertyGridColorProperty* pColorBack = new 
        CMFCPropertyGridColorProperty(m_strBackText.c_str(), 
        m_es.crBack, NULL, m_strBackTextExplanation.c_str());
	pColorBack->EnableOtherButton(_T("Other..."));
	pColorBack->EnableAutomaticButton(_T("Default"), ::GetSysColor(COLOR_WINDOW));
    pColorBack->SetData(e_BackText);
    apGroupColor->AddSubItem(pColorBack);

	CMFCPropertyGridColorProperty* pColorCommnet = new 
        CMFCPropertyGridColorProperty(m_strColorCommnet.c_str(), 
        m_es.crCommnet, NULL, m_strColorCommnetExplanation.c_str());
	pColorCommnet->EnableOtherButton(_T("Other..."));
	pColorCommnet->EnableAutomaticButton(_T("Default"), RGB(0,128,128));
    pColorCommnet->SetData(e_ColorCommnet);
    apGroupColor->AddSubItem(pColorCommnet);

	CMFCPropertyGridColorProperty* pColorSel = new 
        CMFCPropertyGridColorProperty(m_strColorSel.c_str(), 
        m_es.crSel, NULL, m_strColorSelExplanation.c_str());
	pColorSel->EnableOtherButton(_T("Other..."));
	pColorSel->EnableAutomaticButton(_T("Default"), ::GetSysColor(COLOR_HIGHLIGHTTEXT));
    pColorSel->SetData(e_ColorSel);
    apGroupColor->AddSubItem(pColorSel);

	CMFCPropertyGridColorProperty* pColorSelBack = new 
        CMFCPropertyGridColorProperty(m_strColorBack.c_str(), 
        m_es.crSelBack, NULL, m_strColorBackExplanation.c_str());
	pColorSelBack->EnableOtherButton(_T("Other..."));
	pColorSelBack->EnableAutomaticButton(_T("Default"), ::GetSysColor(COLOR_HIGHLIGHT));
    pColorSelBack->SetData(e_ColorBack);
    apGroupColor->AddSubItem(pColorSelBack);

	CMFCPropertyGridColorProperty* pColorReserv1 = new 
        CMFCPropertyGridColorProperty(m_strColorReserv1.c_str(), 
        m_es.crReserv1, NULL, m_strColorReserv1Explanation.c_str());
	pColorReserv1->EnableOtherButton(_T("Other..."));
	pColorReserv1->EnableAutomaticButton(_T("Default"), RGB(0,255,255));
    pColorReserv1->SetData(e_ColorReserv1);
    apGroupColor->AddSubItem(pColorReserv1);

	CMFCPropertyGridColorProperty* pColorReserv2 = new 
        CMFCPropertyGridColorProperty(m_strColorReserv2.c_str(), 
        m_es.crReserv2, NULL, m_strColorReserv1Explanation.c_str());
	pColorReserv2->EnableOtherButton(_T("Other..."));
	pColorReserv2->EnableAutomaticButton(_T("Default"), RGB(0,255,0));
    pColorReserv2->SetData(e_ColorReserv2);
    apGroupColor->AddSubItem(pColorReserv2);

	CMFCPropertyGridColorProperty* pColorReserv3 = new 
        CMFCPropertyGridColorProperty(m_strColorReserv3.c_str(), 
        m_es.crReserv3, NULL, m_strColorReserv3Explanation.c_str());
	pColorReserv3->EnableOtherButton(_T("Other..."));
	pColorReserv3->EnableAutomaticButton(_T("Default"), RGB(255,0,255));
    pColorReserv3->SetData(e_ColorReserv3);
    apGroupColor->AddSubItem(pColorReserv3);

	CMFCPropertyGridColorProperty* pColorString = new 
        CMFCPropertyGridColorProperty(m_strColorString.c_str(), 
        m_es.crString, NULL, m_strColorStringExplanation.c_str());
	pColorString->EnableOtherButton(_T("Other..."));
	pColorString->EnableAutomaticButton(_T("Default"), RGB(0,0,255));
    pColorString->SetData(e_ColorString);
    apGroupColor->AddSubItem(pColorString);

	CMFCPropertyGridColorProperty* pColorLineNo = new 
        CMFCPropertyGridColorProperty(m_strColorLineNo.c_str(), 
        m_es.crLineNo, NULL, m_strColorLineNoExplanation.c_str());
	pColorLineNo->EnableOtherButton(_T("Other..."));
	pColorLineNo->EnableAutomaticButton(_T("Default"), RGB(128,128,128));
    pColorLineNo->SetData(e_ColorLineNo);
    apGroupColor->AddSubItem(pColorLineNo);

    apGroupEditor->AddSubItem(apGroupColor.release());

	AddProperty(apGroupEditor.release());
}

void CEditorPropertyGrid::OnPropertyChanged(CMFCPropertyGridProperty* pProp) const
{
    DWORD_PTR dwData;
    dwData = pProp->GetData();

    switch(dwData)
    {
    case e_Wordwrap:     //折り返し
        m_es.bWordwrap = (_variant_t)pProp->GetValue();
        break;

    case e_LineNo:       //行番号表示
        m_es.bEnableLineNumber = (_variant_t)pProp->GetValue();
        break;

    case e_DispSpace:       //空白表示
        m_es.bDispSpace = (_variant_t)pProp->GetValue();
        break;

    case e_AutoIndent:      //オートインデント
        m_es.bAutoIndent = (_variant_t)pProp->GetValue();
        break;

    case e_AutoMember:      //自動メンバー
        m_es.bAutoMember = (_variant_t)pProp->GetValue();
        break;

    case e_TabSize:         //タブサイズ
        m_es.iTabSize = (_variant_t)pProp->GetValue();
        break;

    case e_Font:        //フォント
        {
            CMFCPropertyGridFontProperty* pFntProp;
            pFntProp = dynamic_cast<CMFCPropertyGridFontProperty*>(pProp);
            m_es.lf = *pFntProp->GetLogFont();
        }
        break;

    case e_ColorText:       //文字色;
        m_es.crText = (_variant_t)pProp->GetValue();
        break;

    case e_BackText:        //背景色;
        m_es.crBack = (_variant_t)pProp->GetValue();
        break;

    case e_ColorCommnet:    //コメント;
        m_es.crCommnet = (_variant_t)pProp->GetValue();
        break;

    case e_ColorSel:        //選択;
        m_es.crSel = (_variant_t)pProp->GetValue();
        break;

    case e_ColorBack:       //選択背景;
        m_es.crSelBack = (_variant_t)pProp->GetValue();
        break;

    case e_ColorReserv1:    //予約語;
        m_es.crReserv1 = (_variant_t)pProp->GetValue();
        break;

    case e_ColorReserv2:    //予約語;
        m_es.crReserv2 = (_variant_t)pProp->GetValue();
        break;

    case e_ColorReserv3:    //予約語;
        m_es.crReserv3 = (_variant_t)pProp->GetValue();
        break;

    case e_ColorString:    //文字列色;
        m_es.crString = (_variant_t)pProp->GetValue();
        break;

    case e_ColorLineNo:    //行番号;
        m_es.crLineNo = (_variant_t)pProp->GetValue();
        break;

    default:
        break;
    }
    return;
}

void CEditorPropertyGrid::SetData(const MT_EDITOR::EDITOR_SETTING& setting)
{
    m_es = setting;
}

void CEditorPropertyGrid::GetData(MT_EDITOR::EDITOR_SETTING* pSetting)
{
    /*
    CMFCPropertyGridProperty* pProp = FindItemByData(e_Font);

    CMFCPropertyGridFontProperty* pFntProp;
    pFntProp = dynamic_cast<CMFCPropertyGridFontProperty*>(pProp);
    m_es.lf = *pFntProp->GetLogFont();
    */
    *pSetting = m_es;
}





CDlgSetting::CDlgSetting(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgSetting::IDD, pParent)
{
	m_pGroupFont = NULL;

}

CDlgSetting::~CDlgSetting()
{
}

void CDlgSetting::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_ST_PROPERTY, m_wndPropListLocation);
    DDX_Control(pDX, IDC_TREE_SEL, m_TreeSel);
}


BEGIN_MESSAGE_MAP(CDlgSetting, CDialog)
    ON_BN_CLICKED(IDOK, &CDlgSetting::OnBnClickedOk)
    ON_NOTIFY(TVN_SELCHANGED, IDC_TREE_SEL, &CDlgSetting::OnTvnSelchangedTreeSel)
END_MESSAGE_MAP()


// CDlgSetting メッセージ ハンドラ

BOOL CDlgSetting::OnInitDialog()
{
    CDialog::OnInitDialog();

	CRect rectPropList;
	m_wndPropListLocation.GetClientRect(&rectPropList);
	m_wndPropListLocation.MapWindowPoints(this, &rectPropList);

	m_wndPropList.Create(WS_CHILD | WS_VISIBLE | WS_TABSTOP | WS_BORDER, rectPropList, this, (UINT)-1);

    m_wndPropList.Setup();


    return TRUE;  // return TRUE unless you set the focus to a control
    // 例外 : OCX プロパティ ページは必ず FALSE を返します。
}


void CDlgSetting::SetData(const MT_EDITOR::EDITOR_SETTING& setting)
{
    m_wndPropList.SetData(setting);
}

void CDlgSetting::GetData(MT_EDITOR::EDITOR_SETTING* pSetting)
{
    m_wndPropList.GetData(pSetting);
}

void CDlgSetting::OnBnClickedOk()
{
    OnOK();
}

void CDlgSetting::OnTvnSelchangedTreeSel(NMHDR *pNMHDR, LRESULT *pResult)
{
    LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
    // TODO: ここにコントロール通知ハンドラ コードを追加します。
    *pResult = 0;
}
