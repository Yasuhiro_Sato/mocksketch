#pragma once
#include "MtEditorBuffer.h"
#include "afxcmn.h"



class CEditorPropertyGrid : public CMFCPropertyGridCtrl
{
public:

    enum E_PROP
    {
        e_Wordwrap,         //折り返し
        e_LineNo,           //行番号表示
        e_DispSpace,        //空白表示
        e_AutoIndent,       //オートインデント
        e_AutoMember,       //自動メンバー
        e_TabSize,          //タブサイズ

         e_Font,            //フォント
         e_ColorText,       //文字色;
         e_BackText,        //背景色;
         e_ColorCommnet,    //コメント;
         e_ColorSel,        //選択;
         e_ColorBack,       //選択背景;
         e_ColorReserv1,    //予約語;
         e_ColorReserv2,    //予約語;
         e_ColorReserv3,    //予約語;
         e_ColorString ,    //文字列色;
         e_ColorLineNo ,    //行番号;
    };

    CEditorPropertyGrid();

    virtual ~CEditorPropertyGrid(){;}

    void Setup();

    void SetData(const MT_EDITOR::EDITOR_SETTING& setting);
    void GetData(MT_EDITOR::EDITOR_SETTING* pSetting);


protected:
    //virtual BOOL ValidateItemData(CMFCPropertyGridProperty* pProp);
    virtual void OnPropertyChanged(CMFCPropertyGridProperty* pProp) const;
public:

    mutable MT_EDITOR::EDITOR_SETTING m_es;

    StdString m_strGroupEditor;

    StdString m_strGroupDisplay;
 
    ////ワードラップ
    StdString m_strWordwrap;
    StdString m_strWordwrapExplanation;

    //行番号表示
    StdString m_strLineNo;
    StdString m_strLineNoExplanation;

    //空白表示
    StdString m_strDispSpace;
    StdString m_strDispSpaceExplanation;

    //オートインデント
    StdString m_strAutoIndent;
    StdString m_strAutoIndentExplanation;

    //自動メンバー
    StdString m_strAutoMember;
    StdString m_strAutoMemberExplanation;

    //タブサイズ
    StdString m_strTabSize;
    StdString m_strTabSizeExplanation;

    //フォント
    StdString m_strGroupFont;
    StdString m_strFont;
    StdString m_strFontExplanation;

    StdString m_strGroupColor;
    StdString m_strColorText;
    StdString m_strColorTextExplanation;
    COLORREF m_crText;      //文字色


    StdString m_strBackText;
    StdString m_strBackTextExplanation;
    COLORREF m_crBack;      //背景色

    StdString m_strColorCommnet;
    StdString m_strColorCommnetExplanation;
    COLORREF m_crCommnet;   //コメント

    StdString m_strColorSel;
    StdString m_strColorSelExplanation;
    COLORREF m_crSel;       //選択色

    StdString m_strColorBack;
    StdString m_strColorBackExplanation;
    COLORREF m_crSelBack;   //選択背景色

    StdString m_strColorReserv1;
    StdString m_strColorReserv1Explanation;
    COLORREF m_crReserv1;   //予約語1色

    StdString m_strColorReserv2;
    StdString m_strColorReserv2Explanation;
    COLORREF m_crReserv2;   //予約語2色

    StdString m_strColorReserv3;
    StdString m_strColorReserv3Explanation;
    COLORREF m_crReserv3;   //予約語3色

    StdString m_strColorString;
    StdString m_strColorStringExplanation;
    COLORREF m_crString;    //文字列色

    StdString m_strColorLineNo;
    StdString m_strColorLineNoExplanation;
    COLORREF m_crLineNo;    //行番号
};


// CDlgSetting ダイアログ

class CDlgSetting : public CDialog
{
	DECLARE_DYNAMIC(CDlgSetting)

public:
	CDlgSetting(CWnd* pParent = NULL);   // 標準コンストラクタ
	virtual ~CDlgSetting();

    void SetData(const MT_EDITOR::EDITOR_SETTING& setting);
    void GetData(MT_EDITOR::EDITOR_SETTING* pSetting);

// ダイアログ データ
	enum { IDD = IDD_DLG_SETTING };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート

	DECLARE_MESSAGE_MAP()


protected:
	CStatic m_wndPropListLocation;
    CEditorPropertyGrid m_wndPropList;

    //CMFCPropertyGridColorProperty* m_pColorText;

    CMFCPropertyGridProperty* m_pGroupFont;


public:
    virtual BOOL OnInitDialog();



public:
    /*
    bool m_bWordwrap;   //ワードラップ
    bool m_bLineNo;     //行番号表示
    LOGFONT m_lf;       //フォント

    COLORREF m_crText;      //文字色
    COLORREF m_crBack;      //背景色
    COLORREF m_crCommnet;   //コメント
    COLORREF m_crSel;       //選択色
    COLORREF m_crSelBack;   //選択背景色
    COLORREF m_crReserv1;   //予約語1色
    COLORREF m_crReserv2;   //予約語2色
    COLORREF m_crReserv3;   //予約語3色
    COLORREF m_crString;    //文字列色
    COLORREF m_crLineNo;    //行番号
    */

    afx_msg void OnBnClickedOk();
    CTreeCtrl m_TreeSel;
    afx_msg void OnTvnSelchangedTreeSel(NMHDR *pNMHDR, LRESULT *pResult);
};
