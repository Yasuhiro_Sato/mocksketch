﻿#include "stdafx.h"
#include "MTEditorBuffer.h"
#include "MTParse.h"

namespace MT_EDITOR
{

CMtEditorBuffer::CMtEditorBuffer():
m_eFeedType (FT_CRLF),
m_hWnd      (NULL),
m_pPerser   (NULL),
m_pThreadParse (NULL),
m_bParseThreadRun           (false),
m_bParseThreadStopRequest   (false),
m_bChangedDocument          (false),
m_eObsChgDocument           (EO_STOP),
m_pChgDocument              (NULL)

{
    SetPerser(LT_AS);
#ifdef _TEST
    Test();
#endif 
     
}

CMtEditorBuffer::~CMtEditorBuffer()
{
    if (m_pPerser)
    {
        delete m_pPerser;
    }

    if(m_pThreadParse)
    {
        m_pThreadParse->join();
        delete m_pThreadParse;
        m_pThreadParse = NULL;
    }

}

bool CMtEditorBuffer::Clear()
{
    //TODO:実装
    return true;
}

/**
 * @brief   パーサーの設定
 * @param   [in]  eLang   言語
 * @retval  true:成功
 * @note    現状はAngelScriptのみ
 */
bool CMtEditorBuffer::SetPerser(E_LANG eLang)
{
    if (m_pPerser)
    {
        delete m_pPerser;
    }
    if (eLang == LT_AS)
    {
        m_pPerser = new MtParserAS;
        return true;
    }

    return false;
}


/**
 * @brief   文字列分割
 * @param   [out] pList    分割後文字列
 * @param   [in]  str      分割する文字列
 * @retval  なし
 * @note    改行コードで文字列を分割する
 */
void CMtEditorBuffer::DevideSeparator(std::vector<StdString>* pList, 
                                      const TCHAR *str) const
{
    const TCHAR* pChar;
    bool  bCr = false;
    pChar = str;
    StdString strLine;

    while(1)
    {
        if (*pChar == 0)
        {
            pList->push_back(strLine);
            if(bCr)
            {
                strLine = _T("");
                pList->push_back(strLine);
                bCr = false;
            }
            break;
        }
        
        if ((*pChar == _T('\n')) ||
            (*pChar == _T('\r')))
        {
            bCr = true;
        }
        else
        {
            if(bCr)
            {
                pList->push_back(strLine);
                strLine = _T("");
                bCr = false;
            }
            strLine += *pChar;
        }
        pChar++;
    }
    return;
}


bool CMtEditorBuffer::SetText(const TCHAR *str)
{
    m_lstLine.clear();
    const TCHAR* pChar;
    bool  bCr = false;
    EditorLine  line;
    line.eBreakPoint = BP_NONE;
    E_TOKEN   eTokenNext = TK_NONE;
    TCHAR cOld = 0;
    pChar = str;

    while(1)
    {
        if (*pChar == 0)
        {
            eTokenNext = m_pPerser->ParseLine(&line.lstToken, line.m_bufLine, eTokenNext);
            m_lstLine.push_back(line);
            break;
        }
        
        if ((*pChar == _T('\n')) ||
            (*pChar == _T('\r')))
        {

            if ((cOld   == _T('\r'))&&
                (*pChar == _T('\n')))
            {
                pChar++;
                continue;
            }
            cOld = *pChar;
            pChar++;
            bCr = true;
        }

        if(bCr)
        {
            eTokenNext = m_pPerser->ParseLine(&line.lstToken, line.m_bufLine, eTokenNext);
            m_lstLine.push_back(line);

            line.m_bufLine = _T("");
            bCr = false;
        }
        else
        {
            line.m_bufLine += *pChar;
            pChar++;
        }

    }

    _ObservChange();

    return true;
}


bool CMtEditorBuffer::GetText(StdString* pStr, 
                              LINE_POS posStart, 
                              LINE_POS posEnd) const
{
    *pStr = _T("");
    StdString strLf;

    switch(m_eFeedType)
    {
    case FT_CR:
        strLf = _T("\r");
        break;

    case FT_LF:
        strLf = _T("\n");
        break;

    case FT_CRLF:
        strLf = _T("\r\n");
        break;

    default:
        break;

    }

    if (posStart > posEnd)
    {
        std::swap(posStart, posEnd);
    }


    int iLength;
    if (posStart.iLine == posEnd.iLine)
    {
        iLength = posEnd.iCol - posStart.iCol;
        *pStr = m_lstLine[posStart.iLine].
               m_bufLine.substr(posStart.iCol, iLength);
        return true;
    }

    StdString strStart;
    StdString strEnd;
    strStart = m_lstLine[posStart.iLine].m_bufLine;
    strEnd   = m_lstLine[posEnd.iLine].m_bufLine;

    if (posStart.iCol > 0)
    {
        strStart.erase(0, posStart.iCol);
    }

    strEnd.erase(posEnd.iCol);

    *pStr = strStart;
    *pStr += strLf;

    std::vector<EditorLine>::const_iterator iteLine;
    for(iteLine =  m_lstLine.begin() + posStart.iLine + 1;
        iteLine <= m_lstLine.begin() + posEnd.iLine - 1;
        iteLine++)
    {
        *pStr += (*iteLine).m_bufLine;
        *pStr += strLf;
    }

    *pStr += strEnd;
    return true;
}



bool CMtEditorBuffer::GetText(StdString* pStr) const
{
    LINE_POS posStart;
    LINE_POS posEnd;

    posEnd.iLine = SizeToInt(m_lstLine.size()) - 1;
    posEnd.iCol = SizeToInt(m_lstLine[posEnd.iLine].m_bufLine.size());

    return GetText(pStr, posStart, posEnd);
}


/**
 * @brief   文字列挿入
 * @param   [in]   posInsert    挿入位置
 * @param   [in]   str          挿入文字
 * @param   [in]   bInsertode   挿入モード
 * @param   [out]  p_posEnd     挿入終端位置     
 * @retval  挿入行数: 
 * @note    
 */
int CMtEditorBuffer::InsertString(LINE_POS posInsert,
                  const StdString& str, 
                  const bool bInsertode,
                  LINE_POS* p_posEnd)
{
    std::vector<StdString> strList;
    DevideSeparator(&strList, str.c_str());

    int iInsertNum =  static_cast<int>(strList.size());
    if (iInsertNum == 0)
    {
        //何もないとき
        if(p_posEnd)
        {
            *p_posEnd = posInsert;
        }
        return iInsertNum;
    }

    StdString strLast;
    StdString* pStr;  //挿入行
    int iLength;
    int iEndLine;
    int iEndCol;
    E_TOKEN eTokenHead = TK_NONE;


    pStr = &m_lstLine[posInsert.iLine].m_bufLine;
    iLength = static_cast<int>(pStr->length());

    if (posInsert.iLine != 0)
    {
        eTokenHead = m_pPerser->GetLastToken(m_lstLine[posInsert.iLine - 1].lstToken);
    }

    //挿入位置が行末を超える
    assert(posInsert.iCol <= iLength);

    ////////////////////////////////////
    if (!bInsertode && 
        iInsertNum == 1 && 
        strList.size() == 1)
    {
        //一行以内の上書き
        if (posInsert.iCol < static_cast<int>(pStr->length()))
        {
            //一文字上書き
            pStr->at(posInsert.iCol) = strList[0].at(0);
        }
        else
        {
            //後端に追加
            pStr->append(strList[0]);
        }

        if(p_posEnd)
        {
            iEndCol  = posInsert.iCol + 1;
            p_posEnd->Set(posInsert.iLine, iEndCol);
        }

        //TODO:最終項目のみパースするようにする
        eTokenHead = m_pPerser->ParseLine
                    (&m_lstLine[posInsert.iLine].lstToken, 
                     m_lstLine[posInsert.iLine].m_bufLine,
                     eTokenHead);


        _ObservChange();

        return iInsertNum;
    }

    ////////////////////////////////////
    //
    //   aabbccddeeffgghhii
    //           ^ <- ここに挿入
    //
    //   pstr    <- aabbccdd
    //   strLast <- eeffgghhii
    //   となるように分割する
    //
    strLast = pStr->substr(posInsert.iCol, iLength - posInsert.iCol);
    pStr->erase(posInsert.iCol, iLength - posInsert.iCol);

    ////////////////////////////////////

    int iInsertLine = 0;

    pStr->append(strList[iInsertLine]);

    EditorLine  line;
    line.eBreakPoint = BP_NONE;

    //---------------------------------

    if (iInsertNum > 1)
    {
        iEndCol = SizeToInt(strList[iInsertNum - 1].size());
        strList[iInsertNum - 1].append(strLast);
        std::vector<EditorLine>::iterator ite;

        eTokenHead = m_pPerser->ParseLine
                    (&m_lstLine[posInsert.iLine].lstToken, 
                     m_lstLine[posInsert.iLine].m_bufLine, 
                     eTokenHead);


        for (int iCnt = 1; iCnt < iInsertNum; iCnt++)
        {
            ite = m_lstLine.begin();
            ite += (posInsert.iLine + iCnt);

            line.m_bufLine = strList[iCnt];
            m_lstLine.insert(ite,line);

            /* 
            ここでパースすると大量の行を挿入した場合遅くなるので
            スレッドを起動るようにする
            eTokenHead = m_pPerser->ParseLine
                        (&m_lstLine[posInsert.iLine + iCnt].lstToken, 
                         m_lstLine[posInsert.iLine + iCnt].m_bufLine, 
                         eTokenHead);
            */
        }

        ParseThread(posInsert.iLine + 1);

        if(p_posEnd)
        {
            iEndLine = posInsert.iLine + iInsertNum - 1;
            p_posEnd->Set(iEndLine, iEndCol);
        }

    }
    else
    {
        pStr->append(strLast);

        eTokenHead = m_pPerser->ParseLine
                (&m_lstLine[posInsert.iLine].lstToken, 
                 m_lstLine[posInsert.iLine].m_bufLine, 
                 eTokenHead);

        if(p_posEnd)
        {
            iEndCol  = SizeToInt(posInsert.iCol + str.size());
            p_posEnd->Set(posInsert.iLine, iEndCol);
        }
    }
   

    _ObservChange();
    return iInsertNum;
}

/**
 * @brief   データ削除
 * @param   [in]  posStart 削除開始位置
 * @param   [in]  posStart 削除終了位置
 * @retval  削除した行数
 * @note	
 */
int CMtEditorBuffer::DeleteString(LINE_POS posStart,
                                   LINE_POS posEnd)
{
    if (posEnd < posStart)
    {
        std::swap(posStart, posEnd);
    }

    size_t iLength = m_lstLine[posStart.iLine].m_bufLine.length();

    if (posStart.iLine == posEnd.iLine)
    {
        //改行コードのみ削除
        if (posStart.iCol == iLength)
        {
            if (posStart.iCol + 1 ==  posEnd.iCol)
            {
                posEnd.iLine++;
                posEnd.iCol = 0;
            }
            else
            {
                assert(posStart.iCol + 1 ==  posEnd.iCol);
                return 0;
            }
        }
        else
        {
            m_lstLine[posStart.iLine].m_bufLine.erase
                (posStart.iCol, posEnd.iCol - posStart.iCol);
        }
    }

    if (static_cast<size_t>(posEnd.iLine) >= m_lstLine.size())
    {
        posEnd.iLine--;
    }

    if (posStart.iLine == posEnd.iLine)
    {
        _ObservChange();
        return 0;
    }


    m_lstLine[posStart.iLine].m_bufLine.erase
        (posStart.iCol, iLength - posStart.iCol);

    m_lstLine[posEnd.iLine].m_bufLine.erase
        (0, posEnd.iCol);

    m_lstLine[posStart.iLine].m_bufLine.append
        (m_lstLine[posEnd.iLine].m_bufLine);


    std::vector<EditorLine>::iterator ite;
    int iDeleteLine = 0;
    //中間行の削除
    for (int iLine = posStart.iLine + 1;
             iLine <= posEnd.iLine; iLine++)
    {

        ite = m_lstLine.begin() + posStart.iLine + 1;
        m_lstLine.erase(ite);
        iDeleteLine++;
    }
    _ObservChange();
    return iDeleteLine;
}

/**
 *  @brief   ブレークポイント全解除
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CMtEditorBuffer::ClearBreakPoint()
{
    std::vector<EditorLine>::iterator ite;
    for(ite  = m_lstLine.begin();
        ite != m_lstLine.end();
        ite++)
    {
        (*ite).eBreakPoint = BP_NONE;
    }
}

/**
 *  @brief   ブレークポイント取得
 *  @param   [in] iLine     設定行
 *  @retval  ブレークポイントステータス
 *  @note
 */
BRAKE_POINT_STS CMtEditorBuffer::GetBreakPoint(int iLine) const
{
   return m_lstLine[iLine].eBreakPoint;
}

/**
 *  @brief   ブレークポイント設定
 *  @param   [in] iLine     設定行
 *  @param   [in] eBreakSts ステータス
 *  @retval  なし
 *  @note
 */
void CMtEditorBuffer::SetBreakPoint(int iLine, BRAKE_POINT_STS eBreakSts)
{
    m_lstLine[iLine].eBreakPoint = eBreakSts;
}

/**
 *  @brief   ブレークポイントリスト取得
 *  @param   [out] pBreakPointList ブレークポイントリスト
 *  @retval  なし
 *  @note
 */
void CMtEditorBuffer::GetBreakPointList
    (std::vector<BREAK_POINT_DATA>* pBreakPointList) const
{
    BREAK_POINT_DATA bpData;
    pBreakPointList->clear();

    int iLine = 0;
    std::vector<EditorLine>::const_iterator ite;
    for(ite  = m_lstLine.begin();
        ite != m_lstLine.end();
        ite++)
    {
        if((*ite).eBreakPoint != BP_NONE)
        {
            bpData.iLine = iLine;
            if ((*ite).eBreakPoint == BP_ENABLE)
            {
                bpData.bEnable = true;
            }
            else
            {
                bpData.bEnable = false;
            }
            pBreakPointList->push_back(bpData);
        }
        iLine++;
    }
}

/**
 *  @brief   ブレークポイントリスト設定
 *  @param   [in] pBreakPointList ブレークポイントリスト
 *  @retval  なし
 *  @note
 */
void  CMtEditorBuffer::SetBreakPointList
      (const std::vector<BREAK_POINT_DATA>& lstBreakPoint)
{
    ClearBreakPoint();
   
    int iLine;
    std::vector<BREAK_POINT_DATA>::const_iterator ite;
    for(ite  = lstBreakPoint.begin();
        ite != lstBreakPoint.end();
        ite++)
    {
        iLine = (*ite).iLine;
        if((*ite).bEnable)
        {
            m_lstLine.at(iLine).eBreakPoint = BP_ENABLE;
        }
        else
        {
            m_lstLine.at(iLine).eBreakPoint = BP_DISABLE;
        }
    }
}

/**
 *  @brief   ブレークポイント追加
 *  @param   [in] iLine   追加行(0org)
 *  @param   [in] bEnable 有効・追加行
 *  @retval  false 失敗
 *  @note
 */
bool  CMtEditorBuffer::AddBreakPoint(const int iLine, bool bEnable)
{
    if (static_cast<int>(m_lstLine.size()) <= iLine)
    {
        return false;
    }

    if(bEnable)
    {
        m_lstLine.at(iLine).eBreakPoint = BP_ENABLE;
    }
    else
    {
        m_lstLine.at(iLine).eBreakPoint = BP_DISABLE;
    }
    return true;
}

/**
 *  @brief   ブレークポイント削除
 *  @param   [in] iLine   削除行(0org)
 *  @retval  false 失敗
 *  @note
 */
bool  CMtEditorBuffer::DelBreakPoint(const int iLine)
{
    if (static_cast<int>(m_lstLine.size()) <= iLine)
    {
        return false;
    }

    m_lstLine.at(iLine).eBreakPoint = BP_NONE;
    return true;
}

int  CMtEditorBuffer::GetLineNum()
{
    return static_cast<int>(m_lstLine.size());
}

const StdString*  CMtEditorBuffer::GetLineString(int iLine) const
{
    return &m_lstLine[iLine].m_bufLine;
}

const std::vector<PARSE_TOKEN>*  CMtEditorBuffer::GetTokenList(int iLine) const
{
    return &m_lstLine[iLine].lstToken;
}


bool CMtEditorBuffer::IsLast(const LINE_POS &pos)
{
    if ((pos.iLine + 1) == m_lstLine.size())
    {
        if (m_lstLine[pos.iLine].m_bufLine.length() == pos.iCol)
        {
            return true;
        }
    }
    return false;
}


void CMtEditorBuffer::ParseThread(int iStartLine)
{
    if(m_pThreadParse)
    {
        m_pThreadParse->join();
        delete m_pThreadParse;
        m_pThreadParse = NULL;
    }


    m_pThreadParse = new std::thread( 
        &CMtEditorBuffer::ParseThreadLoop, this, iStartLine);

}

void CMtEditorBuffer::ParseThreadLoop(CMtEditorBuffer* pBuffer,
                                      int iStartLine)
{
    E_TOKEN eTokenHead = TK_NONE;

    if (iStartLine != 0)
    {
        std::vector<PARSE_TOKEN>* pFirstListToken;

        pFirstListToken = &pBuffer->m_lstLine[iStartLine - 1].lstToken;
        if (pFirstListToken->size() > 0)
        {
            eTokenHead = pFirstListToken->at(pFirstListToken->size() - 1).eToken;
        }
    }

    size_t iSize = pBuffer->m_lstLine.size(); 

    for (size_t iLine = iStartLine; iLine < iSize; iLine++)
    {
        eTokenHead = pBuffer->m_pPerser->ParseLine
                    (&pBuffer->m_lstLine[iLine].lstToken, 
                      pBuffer->m_lstLine[iLine].m_bufLine, 
                     eTokenHead);


        //
        if (pBuffer->m_hWnd)
        {
            if ((iLine % 1000) == 0)
            {
                ::SendMessage(pBuffer->m_hWnd, WM_PAINT, 0, 0);
            }
        }
    }

    if (pBuffer->m_hWnd)
    {
        ::SendMessage(pBuffer->m_hWnd, WM_PAINT, 0, 0);
    }
}

//!> ウインドウの設定
bool CMtEditorBuffer::SetWindow(HWND hWnd)
{
    m_hWnd = hWnd;
    return true;
}


/**
 * @brief   ドキュメント変更監視設定
 * @param   [in]  eExec
 * @retval  true 成功
 * @note	
 */
bool CMtEditorBuffer::ObservChangeDocument(E_EXEC_OBSERVR eExec)
{
    switch(eExec)
    {
    case EO_STOP:
        m_bChangedDocument = false;
        break;
    case EO_START:
        break;
    case EO_PAUSE:
        break;
    default:
        return false;
    }

    m_eObsChgDocument = eExec;
    return true;
}


/**
 * @brief   ドキュメント変更監視
 * @param   なし   
 * @retval  true 保存成功
 * @note	
 */
/*
bool CMtEditorBuffer::ClearObservChangeDocument()
{
    m_bChangedDocument = false;
    return false;
}
*/

/**
 * @brief   ドキュメント変更コールバック関数登録
 * @param   [in] pChgDocument
 * @param   [in] pData
 * @retval  true 登録成功
 * @note	
 */
bool CMtEditorBuffer::SetObservCallback(void* pData,
                    void (CALLBACK* pChgDocument)(void* , bool ))
{
    m_pData = pData;
    m_pChgDocument = pChgDocument;
    return true;
}

/**
 * @brief   ドキュメント変更監視
 * @param   [in] なし
 * @retval  なし
 * @note	
 */
void CMtEditorBuffer::_ObservChange()
{
    if (m_eObsChgDocument != EO_START) 
    {
        return;
    }

    if (!m_bChangedDocument)
    {
        if(m_pChgDocument)
        {
            m_pChgDocument(m_pData, true);
        }
        m_bChangedDocument = true;
    }
}



bool IsIdentifier(StdChar c)
{
    if ( (c >= 0x30) && (c <= 0x39)){return true;}
    if ( (c >= 0x41) && (c <= 0x5a)){return true;}
    if (c == 0x5f){return true;}
    if ( (c >= 0x61) && (c <= 0x7a)){return true;}
    return false;
}

bool IsHankakuKana(StdChar c)
{
    if ( (c >= 0xa1) && (c <= 0xdf)){return true;}
    return false;
}

bool IsBlank(StdChar c)
{
    if ( (c == 0x20) || (c == 0x09)){return true;}
    return false;
}

bool IsCntrl(StdChar c)
{
    if (((c >= 0x00) && (c <= 0x1f)) ||
        (c == 0x7f))
    {
        return true;
    }
    return false;
}

/*
文字コード	文字	文字コード	文字	文字コード	文字
0x21	!	0x2c	,	0x5b	[
0x22	"	0x2d	-	0x5c	\
0x23	#	0x2e	.	0x5d	]
0x24	$	0x2f	/	0x5e	^
0x25	%	0x3a	:	0x5f	_
0x26	&	0x3b	;	0x60	`
0x27	'	0x3c	<	0x7b	{
0x28	(	0x3d	=	0x7c	|
0x29	)	0x3e	>	0x7d	}
0x2a	*	0x3f	?		
0x2b	+	0x40	@		
*/
bool IsPunct(StdChar c)
{
    if (((c >= 0x21) && (c <= 0x2f)) ||
        ((c >= 0x3a) && (c <= 0x40)) ||
        ((c >= 0x5b) && (c <= 0x60)) ||
        ((c >= 0x7b) && (c <= 0x7d))
        )
    {
        return true;
    }
    return false;
}




CHAR_SET_TYPE GetCharSet(StdChar c)
{
    if (IsBlank(c)){return CST_BLANK;}
    else if (IsCntrl(c)) {return CST_CTRL;}
    else if (IsIdentifier(c)) {return CST_IDENTFIRE;}
    else if (IsPunct(c)) {return CST_PUNCT;}
    else if (IsHankakuKana(c)){return CST_KANA;}
    else if (c == _T('　')){return CST_ZEN_SPC;}

    return CST_OTHER;
}


/**
 * @brief   単語先頭位置取得
 * @param   [in] なし
 * @retval  なし
 * @note	
 */
LINE_POS CMtEditorBuffer::GetWordHead(const LINE_POS &pos)
{
    LINE_POS ret = pos;
    if (m_lstLine.size() <= pos.iLine)
    {
        return ret;
    }

    if (m_lstLine[pos.iLine].m_bufLine.length() <= pos.iCol)
    {
        return ret;
    }

    CHAR_SET_TYPE ct;
    ct =  GetCharSet(m_lstLine[pos.iLine].m_bufLine[pos.iCol]);

    CHAR_SET_TYPE ct_cur;
    for (int iPos = pos.iCol; iPos >= 0; iPos--)
    {
        ct_cur = GetCharSet(m_lstLine[pos.iLine].m_bufLine[iPos]);
        if (ct_cur != ct)
        {
            break;
        }
        ret.iCol = iPos;
    }
    return ret;
}

LINE_POS CMtEditorBuffer::GetWordEnd(const LINE_POS &pos)
{
    LINE_POS ret = pos;
    if (m_lstLine.size() <= pos.iLine)
    {
        return ret;
    }

    size_t iLen = m_lstLine[pos.iLine].m_bufLine.length();
    if ( iLen <= pos.iCol)
    {
        return ret;
    }

    CHAR_SET_TYPE ct;
    ct =  GetCharSet(m_lstLine[pos.iLine].m_bufLine[pos.iCol]);

    CHAR_SET_TYPE ct_cur;
    for (int iPos = pos.iCol; iPos < iLen; iPos++)
    {
        ct_cur = GetCharSet(m_lstLine[pos.iLine].m_bufLine[iPos]);
        if (ct_cur != ct)
        {
            break;
        }
        ret.iCol = iPos;
    }

    return ret;
}




/**
 *  @brief   テスト
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CMtEditorBuffer::Test()
{

    StdString str;
    str = _T("0123\r\n012345\r\n012345678\r\n012345678\r\n\r\n01234567890");
    SetText(str.c_str());


/*
    assert(GetLineNum() == 6);

    assert(*GetLineString(0) == _T("0123"));
    assert(*GetLineString(1) == _T("012345"));
    assert(*GetLineString(2) == _T("012345678"));
    assert(*GetLineString(3) == _T("012345678"));
    assert(*GetLineString(4) == _T(""));
    assert(*GetLineString(5) == _T("01234567890"));

    StdString str1;

    GetText(&str1);
    assert(str == str1);

    //-----------------
    //  行末を削除
    //-----------------
    LINE_POS posStart(0,4);
    LINE_POS posEnd(1,0);

    int iDelete;
    iDelete =  DeleteString( posStart, posEnd);

    assert(iDelete == 1);
    assert(GetLineNum() == 5);

    assert(*GetLineString(0) == _T("0123012345"));
    assert(*GetLineString(1) == _T("012345678"));
    assert(*GetLineString(2) == _T("012345678"));
    assert(*GetLineString(3) == _T(""));
    assert(*GetLineString(4) == _T("01234567890"));

    //-----------------
    //  複数行を削除
    //-----------------

    posStart.Set(0,4);
    posEnd.Set(2,3);

    iDelete =  DeleteString( posStart, posEnd);
    assert(iDelete == 2);
    assert(GetLineNum() == 3);

    assert(*GetLineString(0) == _T("0123345678"));
    assert(*GetLineString(1) == _T(""));
    assert(*GetLineString(2) == _T("01234567890"));

    //-----------------
    //  1文字削除
    //-----------------

    posStart.Set(0,0);
    posEnd.Set(0,1);

    iDelete =  DeleteString( posStart, posEnd);
    assert(iDelete == 0);
    assert(GetLineNum() == 3);

    assert(*GetLineString(0) == _T("123345678"));
    assert(*GetLineString(1) == _T(""));
    assert(*GetLineString(2) == _T("01234567890"));

    //-----------------

    posStart.Set(0,0);
    posEnd.Set(1,0);
    iDelete =  DeleteString( posStart, posEnd);

    assert(iDelete == 1);
    assert(GetLineNum() == 2);
    assert(*GetLineString(0) == _T(""));
    assert(*GetLineString(1) == _T("01234567890"));

    //-----------------
    // このように直接行を削除できないので注意
    str = _T("012345678\r\n\r\n01234567890");
    SetText(str.c_str());

    posStart.Set(0,0);
    posEnd.Set(1,1);
    iDelete =  DeleteString( posStart, posEnd);

    assert(iDelete == 1);
    assert(GetLineNum() == 2);
    assert(*GetLineString(0) == _T(""));
    assert(*GetLineString(1) == _T("01234567890"));


    //-----------------
    str = _T("012345678\r\n\r\n01234567890");
    SetText(str.c_str());

    posStart.Set(0,0);
    posEnd.Set(2,0);
    iDelete =  DeleteString( posStart, posEnd);

    assert(iDelete == 2);
    assert(GetLineNum() == 1);
    assert(*GetLineString(0) == _T("01234567890"));



    str = _T("今日はいい天気ですねー\r\n it is very impatanthings.\r\n lastLine");
    SetText(str.c_str());
    
    StdString strRet;
    GetText(&strRet);

    assert(GetLineNum() == 3);

    //-----------------

    str = _T("今日はいい天気ですねー\r\n\r\n\r\nHello\r\n lastLine");
    SetText(str.c_str());
    
    GetText(&strRet);

    assert(GetLineNum() == 5);

    ///////////////////

    //--------------------
    // GetCharSet
    //--------------------
    assert(GetCharSet('A') == CST_IDENTFIRE);
    assert(GetCharSet('#') == CST_PUNCT);
    assert(GetCharSet(' ') == CST_BLANK);
    //        0         1         2               3               4
    //        0123456789012345678901234 5 6 7 8 9 0 1 2 3 45678 9 0 
    str = _T("1234 abcd as4dfcef+1234 今日はいい天気ですね1234ははは");
    SetText(str.c_str());

    LINE_POS pos;
    LINE_POS posSel;
    posSel.iLine = 0;
    posSel.iCol  = 1;

    pos = GetWordHead(posSel);
    assert(pos.iCol == 0);

    pos = GetWordEnd(posSel);
    assert(pos.iCol == 3);


    /////////////////////////
    posSel.iLine = 0;
    posSel.iCol  = 7;

    pos = GetWordHead(posSel);
    assert(pos.iCol == 5);

    pos = GetWordEnd(posSel);
    assert(pos.iCol == 8);

    /////////////////////////
    posSel.iLine = 0;
    posSel.iCol  = 19;

    pos = GetWordHead(posSel);
    assert(pos.iCol == 19);

    pos = GetWordEnd(posSel);
    assert(pos.iCol == 22);


    /////////////////////////
    posSel.iLine = 0;
    posSel.iCol  = 21;

    pos = GetWordHead(posSel);
    assert(pos.iCol == 19);

    pos = GetWordEnd(posSel);
    assert(pos.iCol == 22);

    /////////////////////////
    posSel.iLine = 0;
    posSel.iCol  = 27;

    pos = GetWordHead(posSel);
    assert(pos.iCol == 24);

    pos = GetWordEnd(posSel);
    assert(pos.iCol == 33);
*/
}




}//namespace MT_EDITOR
