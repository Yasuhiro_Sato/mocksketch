#ifndef _MT_EDITOR_BUFFER_H
#define _MT_EDITOR_BUFFER_H

#ifndef MOCK_TOOLS
#include <windows.h>
#include <tchar.h>
#include <vector>
#include <map>
#include <string>
#include <sstream>
#include <thread>
//#include <boost/thread.hpp>
#else
#include <thread>
#endif



enum E_EXEC_OBSERVR
{
    EO_STOP   = 0,
    EO_START,
    EO_PAUSE,
};


//
namespace MT_EDITOR
{
class MtParserBase;

enum E_LANG
{
    LT_NONE,
    LT_C,
    LT_AS
};


enum CHAR_SET_TYPE
{
    CST_BLANK,     //空白文字
    CST_CTRL,      //制御文字
    CST_IDENTFIRE, //識別子
    CST_PUNCT,     //パンクチュエーション文字
    CST_KANA,      //半角カタカナ
    CST_ZEN_SPC ,  //全角スペース
    CST_ZEN,       //
    CST_OTHER,     //その他
};

struct BREAK_POINT_DATA
{
    int  iLine;    //論理行
    bool bEnable;  //有効・無効
};

struct EDITOR_SETTING
{
    EDITOR_SETTING():
    bWordwrap           (true),
    bEnableLineNumber   (true),
    bDispSpace          (false),
    bAutoIndent         (true),
    bAutoMember         (true),  
    iTabSize            (4),

    crText        (GetSysColor(COLOR_WINDOWTEXT)),
    crBack        (GetSysColor(COLOR_WINDOW)),
    crSel         (GetSysColor(COLOR_HIGHLIGHTTEXT)),
    crSelBack     (GetSysColor(COLOR_HIGHLIGHT)),
    crCommnet     (RGB(0,128,128)),
    crReserv1     (RGB(0,255,255)),
    crReserv2     (RGB(0,255,0)),
    crReserv3     (RGB(255,0,255)),
    crString      (RGB(0,0,255)),
    crLineNo      (RGB(128,128,128))


    {
	    CFont* font = CFont::FromHandle((HFONT) GetStockObject(DEFAULT_GUI_FONT));
	    font->GetLogFont(&lf);
	    lstrcpy(lf.lfFaceName, _T("Arial"));
    }

    bool bWordwrap;

    bool bEnableLineNumber;

    // 空白表示
    bool bDispSpace;

    // オートインデント
    bool bAutoIndent;   

    //自動メンバー
    bool bAutoMember;       

    //タブサイズ
    int iTabSize;      

    //文字色
    COLORREF crText;

    //背景色
    COLORREF crBack;

    //フォント
    LOGFONT lf;       

    //コメント
    COLORREF crCommnet;

    //選択色
    COLORREF crSel;

    //選択背景色
    COLORREF crSelBack;

    //予約語1色
    COLORREF crReserv1;

    //予約語2色
    COLORREF crReserv2;

    //予約語3色
    COLORREF crReserv3;

    //文字列色
    COLORREF crString;

    //行番号
    COLORREF crLineNo;


};


enum BRAKE_POINT_STS
{
    BP_NONE,
    BP_DISABLE,
    BP_ENABLE,
};

enum RETURN_MODE
{
    RM_NONE,
    RN_RETURN,
    RN_WORD,        //ワードラップ
};

enum  LINE_FEED_TYPE
{
    FT_LF,
    FT_CRLF,
    FT_CR,
};

enum E_TOKEN
{
    TK_NONE             ,
    TK_WHITE_SPACE      ,
    TK_IDENTIFIER       ,
    TK_RESERVED_WORD    ,
    TK_COMMENT_ML       ,
    TK_COMMENT_SL       ,
    TK_OPERATOR         ,
    TK_STRING           ,
    TK_TRI_STRING       ,
    TK_NUMBER           ,
};

struct PARSE_TOKEN
{
    size_t  iCol;
    E_TOKEN eToken;
};

struct EditorLine
{
    BRAKE_POINT_STS          eBreakPoint;
    std::vector<PARSE_TOKEN> lstToken;
    StdString                m_bufLine;
};

//折り返し時の表示行
struct LINE_POS
{
    int iLine;  //行
    int iCol;   

    LINE_POS(): iLine(0), iCol(0){;}
    LINE_POS(int iSetLine, int iSetCol ): 
        iLine(iSetLine), 
        iCol(iSetCol){;}

    void Set(int iSetLine, int iSetCol){iLine = iSetLine; iCol = iSetCol;}

    bool operator == (const LINE_POS & pos) const
    {
        if (iLine != pos.iLine){return false;}
        if (iCol  != pos.iCol) {return false;}
        return true;
    }

    bool operator != (const LINE_POS & pos) const
    {
        return (! (*this == pos)) ;
    }


    LINE_POS& operator = (const LINE_POS & pos)
    {
        iLine = pos.iLine;
        iCol  = pos.iCol;
        return *this;
    }

    bool operator > (const LINE_POS & pos) const
    {
        if(iLine > pos.iLine){return true;}
        else if(iLine < pos.iLine){return false;}

        if(iCol > pos.iCol){return true;}
        return false;
    }

    bool operator >= (const LINE_POS & pos) const
    {
        if (*this > pos){return true;}
        if (*this ==pos){return true;}
        return false;
    }


    bool operator < (const LINE_POS & pos) const
    {
        if(iLine < pos.iLine){return true;}
        else if(iLine > pos.iLine){return false;}

        if(iCol < pos.iCol){return true;}
        return false;
    }

    bool operator <= (const LINE_POS & pos) const
    {
        if (*this < pos){return true;}
        if (*this ==pos){return true;}
        return false;
    }
};


class CMtEditorBuffer
{
protected:
    //行データ
    std::vector<EditorLine> m_lstLine;

    LINE_FEED_TYPE m_eFeedType;

    MtParserBase*   m_pPerser;

    HWND            m_hWnd;
    
    bool            m_bParseThreadRun;

    bool            m_bParseThreadStopRequest;

    std::thread*   m_pThreadParse;

    // 文章変更監視フラグ
    bool m_bChangedDocument;

    // 文章変更監視フラグ
    E_EXEC_OBSERVR m_eObsChgDocument;

    // 文章変更コールバック用データ
    void* m_pData;

    // 文章変更コールバック
    void (CALLBACK* m_pChgDocument)(void*, bool);


public:
    CMtEditorBuffer();

    ~CMtEditorBuffer();

    bool ObservChangeDocument(E_EXEC_OBSERVR eExec);

    //bool ClearObservChangeDocument();

    bool SetObservCallback(void* pData,
          void (CALLBACK* pChgDocument)(void* , bool ));

    bool Clear();

    //!< パーサーの設定
    bool SetPerser(E_LANG eLang);

    //!> ウインドウの設定
    bool SetWindow(HWND hWnd);

    int  GetLineNum();

    const StdString*  GetLineString(int iLine) const;
    const std::vector<PARSE_TOKEN>*  GetTokenList(int iLine) const;

    bool SetText(const TCHAR *str);

    bool GetText(StdString* pStr) const;

    bool GetText(StdString* pStr, LINE_POS posStart, 
                                  LINE_POS posEnd) const;

    bool IsLast(const LINE_POS &pos);

    int InsertString(LINE_POS posInsert,
                      const StdString& str, 
                      const bool bInsertode,
                      LINE_POS* p_posInsertEnd = NULL);

    int DeleteString(LINE_POS posStart,
                      LINE_POS posEnd);
    
    void ClearBreakPoint();

    BRAKE_POINT_STS GetBreakPoint(int iLine) const;

    void SetBreakPoint(int iLine, BRAKE_POINT_STS eBreakSts);

    void GetBreakPointList
         (std::vector<BREAK_POINT_DATA>* pBreakPointList) const;

    void  SetBreakPointList
      (const std::vector<BREAK_POINT_DATA>& lstBreakPoint);


    bool  AddBreakPoint(const int iLine, bool bEnable);

    bool  DelBreakPoint(const int iLine);

    void DevideSeparator(std::vector<StdString>* pList, 
                         const TCHAR *str) const;

    LINE_POS GetWordHead(const LINE_POS &pos);

    LINE_POS GetWordEnd(const LINE_POS &pos);

protected:
    void _ObservChange();

    void Test();

    void ParseThread(int iStartLine);

    static void ParseThreadLoop(CMtEditorBuffer* pBuffer, 
                                int iStartLine);
    

};




}//namespace MT_EDITOR

#endif
