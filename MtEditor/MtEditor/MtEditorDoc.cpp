
// MtEditorDoc.cpp : CMtEditorDoc クラスの実装
//

#include "stdafx.h"
#include "MtEditor.h"

#include "MtEditorDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMtEditorDoc

IMPLEMENT_DYNCREATE(CMtEditorDoc, CDocument)

BEGIN_MESSAGE_MAP(CMtEditorDoc, CDocument)
END_MESSAGE_MAP()


// CMtEditorDoc コンストラクション/デストラクション

CMtEditorDoc::CMtEditorDoc()
{
	// TODO: この位置に 1 度だけ呼ばれる構築用のコードを追加してください。

}

CMtEditorDoc::~CMtEditorDoc()
{
}

BOOL CMtEditorDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: この位置に再初期化処理を追加してください。
	// (SDI ドキュメントはこのドキュメントを再利用します。)

	return TRUE;
}




// CMtEditorDoc シリアル化

void CMtEditorDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: 格納するコードをここに追加してください。
	}
	else
	{
		// TODO: 読み込むコードをここに追加してください。
	}
}


// CMtEditorDoc 診断

#ifdef _DEBUG
void CMtEditorDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMtEditorDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CMtEditorDoc コマンド
