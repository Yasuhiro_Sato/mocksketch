#include "StdAfx.h"
#include "MtEditorEngine.h"
#include <math.h>

/*
* nEdit
*
* nEdit.c
*
* Copyright (C) 1996-2005 by Nakashima Tomoaki. All rights reserved.
*		http://www.nakka.com/
*		nakka@nakka.com
*/
/* Define */
#define RESERVE_BUF						1024
#define RESERVE_INPUT					256
#define RESERVE_LINE					256
#define RESERVE_UNDO					256

#define DRAW_LEN						256
#define LINE_MAX						32768
#define TAB_STOP						8

// ホイールメッセージ
#ifndef WM_MOUSEWHEEL
#define WM_MOUSEWHEEL					0x020A
#endif
// XPテーマ変更通知
#ifndef WM_THEMECHANGED
#define WM_THEMECHANGED					0x031A
#endif

#define SWAP(a, b)						{a = b - a; b -= a; a += b;}

#define _TEST_EDITOR

CMtEditorEngine::CMtEditorEngine(void):
m_pBuf          (NULL),
m_dwBufSize     (0),
m_dwBufLen      (0),
m_pInputBuf     (NULL),
m_dwInputSize   (0),
m_dwInputLen    (0),
m_iLineLen      (0),
m_iLineAddIndex (0),
m_iLineAddLen   (0),
m_iLogicalLineAdd   (0),
m_pUndo         (NULL),
m_iUndoSize     (0),
m_iUndoLen      (0),
m_iUndoPos      (0),
m_pInputPos     (NULL),
m_dwInputPosLen (0),
m_pDeletePos    (NULL),
m_dwDeleteLen   (0),
m_dwCaretPos    (0),
m_dwSelectPos   (0),
m_iCaretPosX    (0),
m_iLineMax      (0),
m_iLineWidth    (0),
m_iWidth        (0),
m_iHeight       (0),
m_iScrollX      (0),
m_iScrollMaxX   (0),
m_iScrollY      (0),
m_iScrollMaxY   (0),
m_iTabStop      (0),
m_iLeftMargin   (0),
m_iTopMargin    (0),
m_iRightMargin  (0),
m_iBottomMargin (0),
m_iSpacing      (0),
m_iIndicatorWidth   (20),
m_crIndicator   (GetSysColor(COLOR_3DFACE)),
m_hDc           (NULL),
m_hRetBmp       (NULL),
m_hRgn          (NULL),
m_hFont         (NULL),
m_hRetFont      (NULL),
m_iFontHeight   (0),
m_iCharWidth    (0),
m_bWordwrap     (FALSE),
m_bNoHideSel    (FALSE),
m_bLock         (FALSE),
m_bModified     (FALSE),
m_bInsertMode   (FALSE),
m_bSel          (FALSE),
m_bMousedown    (FALSE),
m_iExecLine     (0),
m_iId           (0),
m_hImc          (NULL),
m_pSetBreakPointModified(NULL),
m_pParent       (NULL),
#ifdef OP_XP_STYLE
F_CloseThemeData(NULL),
F_OpenThemeData (NULL),
F_DrawThemeBackground(NULL),
#endif	// OP_XP_STYLE

m_crText        (GetSysColor(COLOR_WINDOWTEXT)),
m_crBack        (GetSysColor(COLOR_WINDOW)),
m_crSel         (GetSysColor(COLOR_HIGHLIGHTTEXT)),
m_crSelBack     (GetSysColor(COLOR_HIGHLIGHT)),
m_crCommnet     (RGB(0,128,128)),
m_crReserv1     (RGB(0,255,255)),
m_crReserv2     (RGB(0,255,0)),
m_crReserv3     (RGB(255,0,255)),
m_crString      (RGB(0,0,255)),
m_crLineNo      (RGB(128,128,128)),

m_hWnd          (NULL)


{
    memset(&m_cWidth[0], 0, sizeof(m_cWidth));

#ifdef _TEST_EDITOR
    Test();
#endif 
}

CMtEditorEngine::~CMtEditorEngine(void)
{
    /*
    std::vector<LINE_INFO*>::iterator ite;
    for (ite  = m_lstLine.begin();
         ite != m_lstLine.end();
         ite++)
    {
        delete (*ite);
    }
    */

    MemFree(reinterpret_cast<void**>(&m_pBuf));
    MemFree(reinterpret_cast<void**>(&m_pInputBuf));
    MemFree(reinterpret_cast<void**>(&m_pUndo));
    m_lstLine.clear();

}

void CMtEditorEngine::Create(HWND hWnd)
{
    m_hWnd = hWnd;
}

//!< バッファを解放
void CMtEditorEngine::MemFree(void **mem)
{
    if (*mem != NULL) 
    {
        LocalFree(*mem);
        *mem = NULL;
    }
}


/*
* string_to_clipboard - 文字列をクリップボードに設定
*/
BOOL CMtEditorEngine::StringToClipboard( const TCHAR *st, const TCHAR *en)
{
    HANDLE hMem;
    TCHAR *buf;

    if (OpenClipboard(m_hWnd) == FALSE) {
        return FALSE;
    }

    if (EmptyClipboard() == FALSE) 
    {
        CloseClipboard();
        return FALSE;
    }

    if ((hMem = GlobalAlloc(GHND, sizeof(TCHAR) * (en - st + 1))) == NULL) {
        CloseClipboard();
        return FALSE;
    }
    if ((buf = reinterpret_cast<TCHAR *>(GlobalLock(hMem))) == NULL) {
        GlobalFree(hMem);
        CloseClipboard();
        return FALSE;
    }
    CopyMemory(buf, st, sizeof(TCHAR) * (en - st));
    *(buf + (en - st)) = TEXT('\0');
    GlobalUnlock(hMem);
#ifdef UNICODE
    SetClipboardData(CF_UNICODETEXT, hMem);
#else
    SetClipboardData(CF_TEXT, hMem);
#endif

    CloseClipboard();
    return TRUE;
}

/**
* @brief   親ウィンドウに通知
* @param   なし
* @return	なし
* @note	 
*/
void CMtEditorEngine::NotifyMessage(const int code) const
{
    SendMessage(GetParent(m_hWnd), 
        WM_COMMAND, MAKEWPARAM(m_iId, code), 
        (LPARAM)m_hWnd);
}

/*
* get_edit_rect - 描画領域の取得
*/
BOOL CMtEditorEngine::GetEditRect( RECT *rect) const
{
    BOOL ret;

    ret = GetClientRect(m_hWnd, rect);

    rect->left += m_iLeftMargin;
    rect->top += m_iTopMargin;
    rect->right -= m_iRightMargin;
    rect->bottom -= m_iBottomMargin;
    return ret;
}

//!<  スクロールバーの設定
void CMtEditorEngine::SetScrollbar()
{
    SCROLLINFO si;
    RECT rect;

    GetEditRect(&rect);

    // 横スクロールバー
    if (m_bWordwrap == FALSE && (rect.right - rect.left) < m_iLineWidth) {
        int width = m_iLineWidth / m_iCharWidth + 1;

        EnableScrollBar(m_hWnd, SB_HORZ, ESB_ENABLE_BOTH);

        m_iScrollMaxX = width - ((rect.right - rect.left) / m_iCharWidth);
        m_iScrollX = (m_iScrollX < m_iScrollMaxX) ? m_iScrollX : m_iScrollMaxX;

        ZeroMemory(&si, sizeof(SCROLLINFO));
        si.cbSize = sizeof(SCROLLINFO);
        si.fMask = SIF_POS | SIF_RANGE | SIF_PAGE;
        si.nPage = (rect.right - rect.left) / m_iCharWidth;
        si.nMax = width - 1;
        si.nPos = m_iScrollX;
        SetScrollInfo(m_hWnd, SB_HORZ, &si, TRUE);
    } 
    else 
    {
        EnableScrollBar(m_hWnd, SB_HORZ, ESB_DISABLE_BOTH);

        m_iScrollX = m_iScrollMaxX = 0;

        ZeroMemory(&si, sizeof(SCROLLINFO));
        si.cbSize = sizeof(SCROLLINFO);
        si.fMask = SIF_POS | SIF_PAGE | SIF_RANGE;
        si.nMax = (m_bWordwrap == TRUE) ? 0 : 1;
        SetScrollInfo(m_hWnd, SB_HORZ, &si, TRUE);
    }

    // 縦スクロールバー
    if (((rect.bottom - rect.top) / m_iFontHeight) < m_iLineLen)
    {
        EnableScrollBar(m_hWnd, SB_VERT, ESB_ENABLE_BOTH);

        m_iScrollMaxY = m_iLineLen - ((rect.bottom - rect.top) / m_iFontHeight);
        m_iScrollY = (m_iScrollY < m_iScrollMaxY) ? m_iScrollY : m_iScrollMaxY;

        ZeroMemory(&si, sizeof(SCROLLINFO));
        si.cbSize = sizeof(SCROLLINFO);
        si.fMask = SIF_POS | SIF_RANGE | SIF_PAGE;
        si.nPage = (rect.bottom - rect.top) / m_iFontHeight;
        si.nMax = m_iLineLen - 1;
        si.nPos = m_iScrollY;
        SetScrollInfo(m_hWnd, SB_VERT, &si, TRUE);
    } 
    else 
    {
        EnableScrollBar(m_hWnd, SB_VERT, ESB_DISABLE_BOTH);

        m_iScrollY = m_iScrollMaxY = 0;

        ZeroMemory(&si, sizeof(SCROLLINFO));
        si.cbSize = sizeof(SCROLLINFO);
        si.fMask = SIF_POS | SIF_PAGE | SIF_RANGE;
        si.nMax = 1;
        SetScrollInfo(m_hWnd, SB_VERT, &si, TRUE);
    }
}

//!> キャレット位置を表示
void CMtEditorEngine::EnsureVisible()
{
    RECT rect;
    int i;
    int x, y;

    GetEditRect(&rect);

    i = IndexToLine(m_dwCaretPos);
    x = CaretCharToCaret(m_hDc, i, m_dwCaretPos) + (m_iScrollX * m_iCharWidth);
    y = i;

    // x
    i = m_iScrollX;
    if (x < (m_iScrollX + 5) * m_iCharWidth) 
    {
        m_iScrollX = x / m_iCharWidth - 5;
        if (m_iScrollX < 0) 
        {
            m_iScrollX = 0;
        }
    }

    if (x > ((m_iScrollX - 5) * m_iCharWidth) + (rect.right - rect.left)) 
    {
        m_iScrollX = x / m_iCharWidth - (rect.right - rect.left) / m_iCharWidth + 5;
        if (m_iScrollX > m_iScrollMaxX) 
        {
            m_iScrollX = m_iScrollMaxX;
        }
    }

    if (i != m_iScrollX)
    {
        SetScrollPos(m_hWnd, SB_HORZ, m_iScrollX, TRUE);
        ScrollWindowEx(m_hWnd, (i - m_iScrollX) * m_iCharWidth, 0, NULL, &rect, NULL, NULL, SW_INVALIDATE | SW_ERASE);
    }

    // y
    i = m_iScrollY;
    if (y < m_iScrollY) 
    {
        m_iScrollY = y;
    }

    if (y > m_iScrollY + (rect.bottom - rect.top) / m_iFontHeight - 1) 
    {
        m_iScrollY = y - (rect.bottom - rect.top) / m_iFontHeight + 1;
        if (m_iScrollY > m_iScrollMaxY) 
        {
            m_iScrollY = m_iScrollMaxY;
        }
    }
    if (i != m_iScrollY) 
    {
        SetScrollPos(m_hWnd, SB_VERT, m_iScrollY, TRUE);
        ScrollWindowEx(m_hWnd, 0, (i - m_iScrollY) * m_iFontHeight, NULL, &rect, NULL, NULL, SW_INVALIDATE | SW_ERASE);
    }
}

//!< マルチバイトの先頭バイトかチェック
BOOL CMtEditorEngine::IsLeadByte(TCHAR *p) const
{
#ifdef UNICODE
    return FALSE;
#else
    if (IsDBCSLeadByte((BYTE)*p) == TRUE && CharToIndex(CharNext(p)) < GetBufferLength()) {
        return TRUE;
    }
    return FALSE;
#endif
}


/**
 * @brief    文字の描画幅の取得
 * @param   [in]  iLineIndex  行数
 * @param   [out] ret_len 文字数
 * @retval  文字の描画幅
 * @note
 */
int CMtEditorEngine::GetCharExtent(TCHAR *str, int *ret_len)
{
    SIZE sz;
    int ret = 0;

#ifdef UNICODE
    *ret_len = 1;
    if (HIBYTE(*str) == 0 && 
        *(m_cWidth + LOBYTE(*str)) != 0) 
    {
        ret = *(m_cWidth + LOBYTE(*str));
    }
    else
    {
        GetTextExtentPoint32(m_hDc,     // デバイスコンテキストのハンドル
                             str,       // 文字列
                             *ret_len,  // 文字列内の文字数
                             &sz);      // 文字列のサイズ
        ret = sz.cx;
        if (HIBYTE(*str) == 0 && sz.cx < 256) 
        {
            *(m_cWidth + LOBYTE(*str)) = (BYTE)sz.cx;
        }
    }
#else
    *ret_len = (IsLeadByte(str) == TRUE) ? 2 : 1;
    if (*ret_len == 1 && *(m_cWidth + (unsigned char)*str) != 0) {
        ret = *(m_cWidth + (unsigned char)*str);
    } else {
        GetTextExtentPoint32(m_hDc, str, *ret_len, &sz);
        ret = sz.cx;
        if (*ret_len == 1 && sz.cx < 256) {
            *(m_cWidth + (unsigned char)*str) = (BYTE)sz.cx;
        }
    }
#endif
    return ret;
}

//!< 次の文字を取得
TCHAR* CMtEditorEngine::CharNext( TCHAR *p) const
{
    if (m_pDeletePos != NULL && 
        p == m_pDeletePos - 1) 
    {
        return (m_pDeletePos + m_dwDeleteLen);
    }

    if (m_pInputPos != NULL) 
    {
        if (p == (m_pInputPos - 1)) 
        {
            return m_pInputBuf;
        }
        else if (p == (m_pInputBuf + m_dwInputLen - 1)) 
        {
            return (m_pInputPos + m_dwInputPosLen);
        }
    }
    return (p + 1);
}

//!< 前の文字を取得
TCHAR* CMtEditorEngine::CharPrev( TCHAR *p) const
{
    if (m_pDeletePos != NULL && 
        p == m_pDeletePos + m_dwDeleteLen) 
    {
        return (m_pDeletePos - 1);
    }

    if (m_pInputPos != NULL) 
    {
        if (p == (m_pInputPos + m_dwInputPosLen)) 
        {
            return (m_pInputBuf + m_dwInputLen - 1);
        } 
        else if (p == m_pInputBuf) 
        {
            return (m_pInputPos - 1);
        }
    }
    return (p - 1);
}

//!< 文字インデックスから文字位置を取得
TCHAR* CMtEditorEngine::IndexToChar(const DWORD index) const
{
    TCHAR *p;

    if ((m_pDeletePos != NULL) && 
        index >= (DWORD)(m_pDeletePos - m_pBuf)) 
    {
        p = m_pBuf + index + m_dwDeleteLen;
    } 
    else if ((m_pInputPos != NULL) && 
        index >= (DWORD)(m_pInputPos - m_pBuf)) 
    {
        if (index < (DWORD)(m_pInputPos - m_pBuf + m_dwInputLen)) 
        {
            p = m_pInputBuf + (index - (m_pInputPos - m_pBuf));
        }
        else 
        {
            p = m_pBuf + index - m_dwInputLen + m_dwInputPosLen;
        }
    } 
    else 
    {
        p = m_pBuf + index;
    }
    return p;
}

//!< 文字位置から文字インデックスを取得
DWORD CMtEditorEngine::CharToIndex(const TCHAR *p) const
{
    DWORD i;

    if (m_pDeletePos != NULL && 
        p >= m_pDeletePos) 
    {
        i = p - m_pBuf - m_dwDeleteLen;
    }
    else if ((m_pInputPos != NULL) &&
        !(p >= m_pBuf && p < m_pInputPos))
    {
        if (p >= m_pInputBuf && p <= m_pInputBuf + m_dwInputLen) 
        {
            i = (m_pInputPos - m_pBuf) + (p - m_pInputBuf);
        }
        else 
        {
            i = (p - m_pBuf) + m_dwInputLen - m_dwInputPosLen;
        }
    } 
    else 
    {
        i = p - m_pBuf;
    }
    return i;
}

//!< 文字インデックスから行位置を取得
int CMtEditorEngine::IndexToLine( const DWORD index) const
{
    int low = 0;
    int high = m_iLineLen - 1;
    int i;

    while (low <= high) 
    {
        i = (low + high) / 2;

        if (LineGet(i) > index) 
        {
            if (i > 0 && 
                LineGet(i - 1) <= index) 
            {
                return (i - 1);
            }
            high  = i - 1;
        } 
        else if (LineGet(i) < index) 
        {
            if (i < m_iLineLen - 1 && 
                LineGet(i + 1) > index) 
            {
                return i;
            }
            low = i + 1;
        }
        else 
        {
            return i;
        }
    }



    return (m_iLineLen - 1);
}


/**
 * @brief   行情報の確保
 * @param   [in] iLineNum    行数
 * @retval  TRUE 成功
 * @note
 */
BOOL CMtEditorEngine::LineAlloc(const int iLineNum)
{
    //size_t nSize = m_lstLine.size();
    m_lstLine.resize(iLineNum + 1);

    //memset( &m_lstLine[nSize - 1], 0, sizeof(LINE_INFO) * iLineNum);

    return TRUE;
}

/**
 * @brief   行情報の移動
 * @param   [in] iLineIndex  行数
 * @param   [in] iLineSize   追加する行数
 * @retval  TRUE 成功
 * @note
 */
BOOL CMtEditorEngine::LineMove(const int iLineIndex, const int iLen)
{
    std::vector<LINE_INFO>::iterator ite;

    // Insert および delete
    ite = m_lstLine.begin() + iLineIndex;
    if (iLen < 0)
    {
        std::vector<LINE_INFO>::iterator iteStart;
        iteStart = ite + iLen;
        m_lstLine.erase(iteStart, ite);
    }
    else
    {
        LINE_INFO info;
        memset(&info, 0, sizeof(info));
        info.eEndSts = STS_NORMAL;
        m_lstLine.insert( ite, iLen, info );
    }
    m_iLineLen += iLen;
    return TRUE;

}

//!< 行頭インデックスの取得
DWORD CMtEditorEngine::LineGet(const int lindex) const
{
    assert(lindex >= 0);
    assert(lindex < m_lstLine.size());

    if (m_iLineAddLen != 0 && 
        m_iLineAddIndex <= lindex) 
    {
        return m_lstLine[lindex].dwLineHead + m_iLineAddLen;
    }
    return m_lstLine[lindex].dwLineHead;
}

/**
 * @brief   行頭インデックスを設定
 * @param   [in] iLine          行
 * @param   [in] dwCharPos      文字位置
 * @param   [in] iLogicalLine   論理行
 * @retval  なし
 * @note	
 */
void CMtEditorEngine::LineSet(const int   iLine, 
                              const DWORD dwCharPos,
                              const int   iLogicalLine)
{
    assert(iLine >= 0);
    assert(iLine < m_lstLine.size());

    if (m_iLineAddLen != 0 && 
        m_iLineAddIndex <= iLine) 
    {
        m_lstLine[iLine].dwLineHead = dwCharPos - m_iLineAddLen;
    }
    else 
    {
        m_lstLine[iLine].dwLineHead = dwCharPos;
    }
    m_lstLine[iLine].iLogicalLine = iLogicalLine;
}

//!< 行情報の反映
/**
 * @brief   行頭インデックスを設定
 * @param   [in] iLine          行
 * @param   [in] dwCharPos      文字位置
 * @param   [in] iLogicalLine   論理行
 * @retval  なし
 * @note	
 */
void CMtEditorEngine::LineFlush()
{
    int i;
    assert(m_iLineAddIndex >= 0);
    assert(m_iLineLen < m_lstLine.size());

    for (i = m_iLineAddIndex; i <= m_iLineLen; i++) 
    {
        m_lstLine[i].dwLineHead += m_iLineAddLen;
        m_lstLine[i].iLogicalLine += m_iLogicalLineAdd;
    }
    m_iLineAddIndex = 0;
    m_iLineAddLen = 0;
    m_iLogicalLineAdd = 0;
}

//!< 行情報に文字数を追加
void CMtEditorEngine::LineAddLength(const int index, const int len)
{
    int i;

    if (m_iLineAddIndex != 0 && 
        m_iLineAddIndex < index) 
    {
        for (i = m_iLineAddIndex; i < index; i++) 
        {
            m_lstLine[i].dwLineHead += m_iLineAddLen;
            m_lstLine[i].iLogicalLine += m_iLogicalLineAdd;
        }
    }
    m_iLineAddIndex     = index;
    m_iLineAddLen      += len;
}

//!< 行の長さを取得
int CMtEditorEngine::LineGetLength(const DWORD index)
{
    int i, j;

    i = LineGet(IndexToLine(index) + 1) - 1;
    j = LineGet(IndexToLine(index));
    for (; i >= j &&
        (*IndexToChar(i) == TEXT('\r') ||
        *IndexToChar(i) == TEXT('\n')); i--);
    return (i - j + 1);
}

/**
 * @brief   次の行頭の文字位置
 * @param   [out] bContinue  次の行とつながっているか
 * @param   [in] dwCharPos  文字イン
 * @retval  文字インデックス
 * @note
 */
DWORD CMtEditorEngine::LineGetNextIndex(bool* bContinue,  
                                        const DWORD dwCharStartPos)
{
    TCHAR *p;
    DWORD word = -1;
    DWORD dwCharPos;
    int line_size;
    int cnt = 0;
    int offset = m_iLeftMargin;
    int word_offset;
    int width;
    int tab;
    int clen;

    

    if (m_bWordwrap == TRUE && m_iWidth <= 0) 
    {
        return GetBufferLength();
    }
    
    line_size = m_iWidth + m_iLeftMargin;
    
    DWORD dwLen = GetBufferLength();
    for (dwCharPos = dwCharStartPos/*iLineStart*/; dwCharPos < dwLen; dwCharPos++) 
    {
        p = IndexToChar(dwCharPos);
        
        switch (*p) 
        {
        case TEXT('\r'):
        case TEXT('\n'):
            // new line
            if (*p == TEXT('\r') && 
                (dwCharPos + 1) < GetBufferLength() && 
                *(CharNext(p)) == TEXT('\n')) 
            {
                dwCharPos++;
            }

            if (m_bWordwrap == FALSE && 
                m_iLineWidth < offset + m_iRightMargin) 
            {
                m_iLineWidth = offset + m_iRightMargin;
            }
            return dwCharPos;

        case TEXT('\t'):
            // tab
            tab = m_iTabStop * m_iCharWidth - 
                ((offset - m_iLeftMargin) % (m_iTabStop * m_iCharWidth));
            if (tab < m_iCharWidth) 
            {
                tab += m_iTabStop * m_iCharWidth;
            }
            if ((m_bWordwrap == TRUE && offset + tab > line_size) ||
                (m_iLineMax > 0 && 
                cnt + m_iTabStop - (cnt % m_iTabStop) > m_iLineMax))
            {
                // 折り返し
                if (word != -1 && 
                    word != dwCharStartPos && 
                    clen == 1) 
                {
                    dwCharPos = word;
                    offset = word_offset;
                }

                if (m_bWordwrap == FALSE && 
                    m_iLineWidth < offset + m_iRightMargin) 
                {
                    m_iLineWidth = offset + m_iRightMargin;
                }
                return (dwCharPos + ((cnt == 0) ? 1 : 0));
            } 
            else 
            {
                cnt += m_iTabStop - (cnt % m_iTabStop);
                offset += tab;
            }
            word = -1;
            break;

        default:
            // char
            width = GetCharExtent(p, &clen);
            if ((m_bWordwrap == TRUE &&          //折り返し設定
                offset + width > line_size) ||   //折り返し判定
                (m_iLineMax > 0 && 
                cnt + 1 > m_iLineMax))           //文字数が m_iLineMax(256)より大きい
            {
                // 折り返し
                if (word != -1 && 
                    word != dwCharStartPos && 
                    clen == 1) 
                {
                    dwCharPos = word;
                    offset = word_offset;
                }

                if (m_bWordwrap == FALSE && 
                    m_iLineWidth < offset + m_iRightMargin) 
                {
                    m_iLineWidth = offset + m_iRightMargin;
                }

                return (dwCharPos + ((cnt == 0) ? clen : 0));
            } 
            else 
            {
                cnt += clen;
                offset += width;
            }

            if (clen == 2) 
            {
                dwCharPos++;
                word = -1;
            } 
            else if (*p != TEXT(' ')) 
            {
                if (word == (DWORD)-1) 
                {
                    // 単語の開始位置
                    word = dwCharPos;
                    word_offset = offset;
                }
            } 
            else 
            {
                word = -1;
            }
            break;
        }
    }
    if (m_bWordwrap == FALSE && 
        m_iLineWidth < offset + m_iRightMargin) 
    {
        // 1行の長さ (スクロールバー用)
        m_iLineWidth = offset + m_iRightMargin;
    }
    return dwCharPos;
}

/**
 * @brief   行情報の設定
 * @param   なし
 * @retval  TRUE 成功
 * @note
 */
BOOL CMtEditorEngine::LineSetInfo()
{
    DWORD dwCharPos;
    int iLine = 1;
    int iLogicalLine = 0;
    bool bContinue;

    m_lstLine.clear();
    LineAlloc(iLine);

    dwCharPos = LineGetNextIndex(&bContinue, 0);
    while (dwCharPos < GetBufferLength()) 
    {
        switch (*IndexToChar(dwCharPos)) 
        {
        case TEXT('\r'):
        case TEXT('\n'):
            // 行頭のインデックスを設定
            iLogicalLine++;
            LineSet(iLine, dwCharPos + 1, iLogicalLine);
            dwCharPos++;
            break;

        default:
            // 行頭のインデックスを設定 (折り返し)
            LineSet(iLine, dwCharPos, iLogicalLine);
            break;
        }
        ++iLine;
        LineAlloc(iLine);
        dwCharPos = LineGetNextIndex(&bContinue, dwCharPos);
    }

    LineSet(iLine, dwCharPos, iLogicalLine);
    m_iLineLen = iLine;
    //DrawBuffer();
    return TRUE;
}

/**
 * @brief   論理行数の取得
 * @param   [in] iLine 行
 * @retval  指定行以後で、折り返しを行なっている行数
 * @note
 */
int CMtEditorEngine::LineGetCount(const int iLine)
{
    TCHAR *p;
    int i;

    for (i = iLine + 1; i < m_iLineLen; i++) 
    {
        p = IndexToChar(LineGet(i) - 1);
        if (*p == TEXT('\r') || 
            *p == TEXT('\n')) 
        {
            // 論理行の終端
            break;
        }
    }
    return (i - iLine);
}

/**
 * @brief   行情報の設定
 * @param   [in] iLineStart 開始行
 * @param   [in] iOldCnt   
 * @param   [in] iLineCnt   追加行数
 * @retval  TRUE 成功
 * @note
 */
int CMtEditorEngine::LineSetCount(const int iLineStart, 
                                  const int iOldCnt, 
                                  const int iLineCnt)
{
    TCHAR *p;
    DWORD dwCharPos;
    int iLine = iLineStart - 1;
    int rcnt = 0;
    int iLogicalLine = 0;
    bool bContinue;


/*
   AAAAAAAAAAAAAAAA
   AAAAAAAAAAAAAAAA
   BBBBBBBBBBB[CR]















      +----------------------------------+
00:00 |                                  |
      +----------------------------------+
      +----------------------------------+
01:00 |                                  |      LineStaart
      +----------------------------------+
      +-----------------+
02:00 |         [CR][LF]|                       Line
      +-----------------+
      +----------------------------------+
03:01 |                                  |
      +----------------------------------+
      +-----------------+
04:01 |         [CR][LF]|
      +-----------------+
      +-----------------+
05:02 |         [CR][LF]|
      +-----------------+

*/
    int iAddLogical = 0;
    int iChgLine = 0;
    if ( iLine < m_lstLine.size() &&
         iLine > 0 )
    {
        iLogicalLine = m_lstLine[iLine].iLogicalLine;
    }

    dwCharPos = LineGet(iLine);
    iLine++;

    while (1) 
    {
        dwCharPos = LineGetNextIndex(&bContinue, dwCharPos);
        if ((iLine - iLineStart) >= iOldCnt) 
        {
            //
            // 挿入行を移動
            LineMove(iLine, 1);
        }

        p = IndexToChar(dwCharPos);

        if (*p == TEXT('\r') ||
            *p == TEXT('\n')) 
        {
            // 行頭のインデックスを設定
            dwCharPos++;
            iLogicalLine++;

            LineSet(iLine++, dwCharPos, iLogicalLine);
            if (++rcnt >= iLineCnt) 
            {
                break;
            }
        } 
        else 
        {
            // 行頭のインデックスを設定 (折り返し)
            LineSet(iLine++, dwCharPos, iLogicalLine);
            if (dwCharPos >= GetBufferLength()) 
            {
                break;
            }
        }
    }

    if ((iLine - iLineStart) < iOldCnt) 
    {
        // 行情報の削除
        LineMove(iLine + iOldCnt - (iLine - iLineStart), 
                       -(iOldCnt - (iLine - iLineStart)));
    }
/*
    // 論理行の整合性をとる
    if (m_iLineLen > iLine + 1)
    {
        DWORD dwCharPos = m_lstLine[iLine].dwLineHead - 1;
        TCHAR* pChar;
        
        iLogicalLine = m_lstLine[iLine - 1].iLogicalLine;

        pChar = IndexToChar(dwCharPos);
        if (*pChar == TEXT('\r') ||
            *pChar == TEXT('\n')) 
        {
            iLogicalLine++;
        }
        
        int iAddLogical = iLogicalLine - m_lstLine[iLine].iLogicalLine;
        if ( iAddLogical != 0)
        {
            for (int i = iLine; i < m_iLineLen; i++) 
            {
                m_lstLine[i].iLogicalLine += iAddLogical;
            }
        }
    }
*/    
    return (iLine - iLineStart);
}

//!< 指定範囲文字列のある行を再描画対象にする
void CMtEditorEngine::LineRefresh( const DWORD p, const DWORD r)
{
    RECT rect;
    int i, j;

    // 文字位置から行のインデックスを取得
    i = IndexToLine(p);
    if (p == r) 
    {
        j = i;
    } 
    else 
    {
        j = IndexToLine(r);
    }

    if (i > j) 
    {
        SWAP(i, j);
    }
    i -= m_iScrollY;
    j -= m_iScrollY;

    // 再描画
    GetEditRect(&rect);
    if (i * m_iFontHeight > (rect.bottom - rect.top) || 
        j * m_iFontHeight + m_iFontHeight < 0) 
    {
        return;
    }

    if (j * m_iFontHeight + m_iFontHeight < (rect.bottom - rect.top)) 
    {
        rect.bottom = j * m_iFontHeight + m_iFontHeight + m_iTopMargin;
    }

    if (i * m_iFontHeight > 0) 
    {
        rect.top = i * m_iFontHeight + m_iTopMargin;
    }
    rect.left = 0;
    ::InvalidateRect(m_hWnd, &rect, FALSE);
}

// UNDOの確保
BOOL CMtEditorEngine::UndoAlloc()
{
    UNDO *ud;

    m_iUndoSize += RESERVE_UNDO;
    ud = reinterpret_cast<UNDO*>(MemCalloc(sizeof(UNDO) * m_iUndoSize));
    if (ud == NULL) 
    {
        return FALSE;
    }

    if (m_pUndo != NULL) 
    {
        CopyMemory(ud, m_pUndo, sizeof(UNDO) * m_iUndoLen);
        MemFree(reinterpret_cast<void**>(&m_pUndo));
    }
    m_pUndo = ud;
    return TRUE;
}

//!< UNDOの解放
void CMtEditorEngine::UndoFree(const int index)
{
    int i;

    for (i = index; i < m_iUndoSize; i++) 
    {
        (m_pUndo + i)->type = 0;
        MemFree(reinterpret_cast<void**>(&(m_pUndo + i)->buf));
    }
}

//!< UNDOのセット
BOOL CMtEditorEngine::UndoSet(const int type, const DWORD index, const DWORD len)
{
    UndoFree(m_iUndoLen);
    if (m_iUndoLen + 1 >= m_iUndoSize &&
        UndoAlloc() == FALSE) 
    {
        return FALSE;
    }

    (m_pUndo + m_iUndoLen)->type = type;
    (m_pUndo + m_iUndoLen)->st = index;
    (m_pUndo + m_iUndoLen)->len = len;

    switch (type) {
    case UNDO_TYPE_INPUT:
        // 入力
        break;

    case UNDO_TYPE_DELETE:
        // 削除
        if (((m_pUndo + m_iUndoLen)->buf = reinterpret_cast<TCHAR*>(MemAlloc(sizeof(TCHAR) * len))) == NULL) {
            return FALSE;
        }
        CopyMemory((m_pUndo + m_iUndoLen)->buf, m_pBuf + index, sizeof(TCHAR) * len);
        break;
    }
    m_iUndoLen++;
    return TRUE;
}

/**
 *  @brief  UNDOの実行
 *  @param  なし
 *  @retval TRUE: 成功
 *  @note
 */
BOOL CMtEditorEngine::Undo()
{
    int i;

    FlushString(TRUE);

    i = m_iUndoLen - 1;
    if (i < 0) 
    {
        return TRUE;
    }
    m_dwSelectPos = m_dwCaretPos = (m_pUndo + i)->st;
    switch ((m_pUndo + i)->type) {
    case UNDO_TYPE_INPUT:
        if ((m_pUndo + i)->buf == NULL) {
            if (((m_pUndo + i)->buf = reinterpret_cast<TCHAR*>(MemAlloc(sizeof(TCHAR) * (m_pUndo + i)->len))) == NULL) {
                return FALSE;
            }
            CopyMemory((m_pUndo + i)->buf, m_pBuf + (m_pUndo + i)->st, sizeof(TCHAR) * (m_pUndo + i)->len);
        }
        DeleteString((m_pUndo + i)->st, (m_pUndo + i)->st + (m_pUndo + i)->len);
        break;

    case UNDO_TYPE_DELETE:
        InsertString((m_pUndo + i)->buf, (m_pUndo + i)->len, TRUE);
        break;
    }
    FlushString(FALSE);
    m_iUndoLen--;

    SendMessage(m_hWnd, EM_SETMODIFY, (m_iUndoLen == m_iUndoPos) ? FALSE : TRUE, 0);

    SetScrollbar();
    EnsureVisible();
    InvalidateRect(m_hWnd, NULL, FALSE);
    return TRUE;

}

/**
 *  @brief  REDOの実行
 *  @param  なし
 *  @retval TRUE: 成功
 *  @note
 */
BOOL CMtEditorEngine::Redo()
{
    int i;

    FlushString(TRUE);

    i = m_iUndoLen;
    if ((m_pUndo + i)->type == 0) {
        return TRUE;
    }
    m_dwSelectPos = m_dwCaretPos = (m_pUndo + i)->st;
    switch ((m_pUndo + i)->type) {
    case UNDO_TYPE_INPUT:
        InsertString((m_pUndo + i)->buf, (m_pUndo + i)->len, TRUE);
        break;

    case UNDO_TYPE_DELETE:
        DeleteString((m_pUndo + i)->st, (m_pUndo + i)->st + (m_pUndo + i)->len);
        break;
    }
    FlushString(FALSE);
    m_iUndoLen++;

    SendMessage(m_hWnd, EM_SETMODIFY, (m_iUndoLen == m_iUndoPos) ? FALSE : TRUE, 0);

    SetScrollbar();
    EnsureVisible();
    InvalidateRect(m_hWnd, NULL, FALSE);
    return TRUE;
}

//!< 初期化
BOOL CMtEditorEngine::InitString()
{
    // free
    MemFree(reinterpret_cast<void**>(&m_pInputBuf));
    UndoFree( 0);
    MemFree(reinterpret_cast<void**>(&m_pUndo));

    // input init
    m_dwInputSize = RESERVE_INPUT;
    m_pInputBuf = reinterpret_cast<TCHAR*>(MemAlloc(sizeof(TCHAR) * m_dwInputSize));
    if (m_pInputBuf == NULL) 
    {
        return FALSE;
    }
    *m_pInputBuf = TEXT('\0');
    m_dwInputLen = 0;
    m_pInputPos = NULL;
    m_dwInputPosLen = 0;

    // line init
   
    m_iLineLen = 0;
    m_iLineWidth = 0;
    
    // undo init
    m_iUndoSize = RESERVE_UNDO;
    m_pUndo = reinterpret_cast<UNDO*>(MemCalloc(sizeof(UNDO) * m_iUndoSize));

    if (m_pUndo == NULL) 
    {
        return FALSE;
    }

    m_iUndoLen = 0;

    m_iScrollX = m_iScrollMaxX = 0;
    m_iScrollY = m_iScrollMaxY = 0;
    m_dwSelectPos = m_dwCaretPos = 0;
    m_iCaretPosX = 0;
    m_pDeletePos = NULL;
    m_dwDeleteLen = 0;
    m_bModified = FALSE;

    return TRUE;
}

/**
 *  @brief   文字列の設定
 *  @param   [in] str 設定文字
 *  @retval  [in] len 設定文字列長
 *  @note    バッファーを消去し単一の文字列を設定する
 *           OnSetMem, WM_SETTEXTで使用
 */
BOOL CMtEditorEngine::SetString(const TCHAR *str, const DWORD len)
{
    HCURSOR old_cursor;

    old_cursor = SetCursor(LoadCursor(NULL, IDC_WAIT));

    if (InitString() == FALSE) 
    {
        SetCursor(old_cursor);
        return FALSE;
    }

    // 文字列設定
    MemFree(reinterpret_cast<void**>(&m_pBuf));
    m_dwBufLen = len;
    m_dwBufSize = m_dwBufLen + 1 + RESERVE_BUF;
    m_pBuf = reinterpret_cast<TCHAR*>(MemAlloc(sizeof(TCHAR) * m_dwBufSize));
    if (m_pBuf == NULL) 
    {
        SetCursor(old_cursor);
        return FALSE;
    }

    CopyMemory(m_pBuf, str, sizeof(TCHAR) * (m_dwBufLen + 1));

    // 行情報の設定
    LineSetInfo();
    SetScrollbar();
    InvalidateRect(m_hWnd, NULL, FALSE);

    SetCursor(old_cursor);
    return TRUE;
}

/**
 *  @brief   選択位置の削除
 *  @param   なし
 *  @retval  TRUE 選択あり
 *  @note
 */
BOOL CMtEditorEngine::DeleteSelect()
{
    if (m_dwCaretPos != m_dwSelectPos) 
    {
        // 選択文字の削除
        DeleteString( m_dwCaretPos, m_dwSelectPos);
        if (m_dwCaretPos > m_dwSelectPos) 
        {
            SWAP(m_dwCaretPos, m_dwSelectPos);
        }

        m_dwSelectPos = m_dwCaretPos;
        return TRUE;
    }
    return FALSE;
}

/**
 *  @brief   改行数の取得
 *  @param   [in] pStr   設定文字
 *  @param   [in] iLen   文字長
 *  @retval  改行数
 *  @note
 */
int CMtEditorEngine::GetReturnCount(TCHAR *pStr, 
                                   const int iLen)
{
    int iReturnCount = 0; //改行数
    TCHAR *p;

    // 改行数の取得
    for (p = pStr; (p - pStr) < iLen; p++) 
    {
        if (*p == TEXT('\r') || 
            *p == TEXT('\n')) 
        {
            if (*p == TEXT('\r') && 
                *(p + 1) == TEXT('\n')) 
            {
                p++;
            }
            iReturnCount++;
        }
    }
    return iReturnCount;
}


/**
 *  @brief   入力バッファへの追加
 *  @param   [in] pStr   設定文字
 *  @param   [in] iLen   文字長
 *  @retval  改行数
 *  @note    
 */
void CMtEditorEngine::AddInputBuf(TCHAR *pStr, 
                                 const int iLen)
{
     TCHAR *p;
    //------------------------------------
    //TODO: vectorに変更する
    // 入力バッファの設定
    if (m_dwInputLen + iLen + 1 > m_dwInputSize) 
    {
        //入力バッファの拡張
        m_dwInputSize = m_dwInputLen + iLen + 1 + RESERVE_INPUT;
        p = reinterpret_cast<TCHAR*>(MemAlloc(sizeof(TCHAR) * m_dwInputSize));
        if (p == NULL) 
        {
            return;
        }
        CopyMemory(p, m_pInputBuf, sizeof(TCHAR) * m_dwInputLen);
        MemFree(reinterpret_cast<void**>(&m_pInputBuf));
        m_pInputBuf = p;
    }

    CopyMemory(m_pInputBuf + m_dwInputLen, pStr, sizeof(TCHAR) * iLen);
    m_dwInputLen += iLen;
}


/**
 *  @brief   文字列の追加
 *  @param   [in] pStr    追加文字
 *  @param   [in] iLen    文字長
 *  @param   [in] bInsert 挿入モード
 *  @retval  改行数
 *  @note    主バッファへを頻繁に移動することを避けるため
 *           基本は入力バッファへ書き込む、行リストについては
 *           先に反映させておく
 *           
 */
BOOL CMtEditorEngine::InsertString(TCHAR *str, 
                                   const int iLen, 
                                   const BOOL bInsert)
{
    TCHAR *p;
    int iReturnCount = 0; //改行数
    BOOL bSel = FALSE;    // 選択文字


    //選択文字の削除
    bSel = DeleteSelect();

    //削除データがある場合は反映する
    if (m_pDeletePos != NULL) 
    {
        FlushString(TRUE);
    }
    
    //入力値に含まれる改行数の取得
    iReturnCount = GetReturnCount(str, iLen);

    DWORD dwInserPos;
    if (m_pInputPos == NULL)
    {
        dwInserPos = m_dwCaretPos;
    }
    else
    {
        dwInserPos = (m_pInputPos - m_pBuf) + m_dwInputLen;
    }

    // 追加前の表示行数
    int iInsertLine = IndexToLine(dwInserPos);

    int iInsertLineCount;
    iInsertLineCount = LineGetCount(iInsertLine);

    //入力バッファへ文字を追加 
    AddInputBuf(str, iLen);

    if (m_pInputPos == NULL) 
    {
        //入力位置の設定
        m_pInputPos = IndexToChar(m_dwCaretPos);
    }

    m_dwCaretPos += iLen;
    m_dwSelectPos = m_dwCaretPos;

    TCHAR *pInsert;
    DWORD dwInputLen = 0;
    if ((bInsert == FALSE) &&
        (bSel == FALSE))
    {
        // 上書きモード、選択文字なしの時
        // 新たに追加したデータの改行までの文字長を取得し
        // (改行がない場合は iLen）dwInputLenに設定

        for (p = str; 
            (p - str) < iLen;
              p++, dwInputLen++) 
        {
            pInsert = m_pInputPos + 
                      m_dwInputPosLen + 
                      dwInputLen;

            if (CharToIndex(pInsert) >= GetBufferLength() ||
                *(pInsert) == TEXT('\r') ||
                *(pInsert) == TEXT('\n')) 
            {
                break;
            }

#ifndef UNICODE
            if (IsLeadByte(pInsert) == TRUE) {ip_len++; }

            if (IsDBCSLeadByte((BYTE)*p) == TRUE && 
                (p - str) < ilen) { p++;}
#endif
        }
    }
/*


    □ dwSelectPos;
     | dwCaretPos;

    +---------------------------------+
00  |                                 |
    +---------------------------------+
    +--------------------------+
01  |                  [CR][LF]|
    +--------------------------+
    +---------------------------------+
02  | □        |                     |
    +---------------------------------+
    +---------------------------------+
03  |                                 |
    +---------------------------------+
    +-----------------------+
04  |               [CR][LF]|
    +-----------------------+


入力データ  m_pInputBuf, m_dwInputPosLen
    +---------------------------------+
00  |                                 |
    +---------------------------------+
    +--------------------------+
01  |                  [CR][LF]|    既に入力バッファへ登録済み
    +--------------------------+
    +---------------------------------+
02  |                                 | 今回入力したデータ (iLen)
    +---------------------------------+
    +---------------------+
03  |             [CR][LF]| 
    +---------------------+
    +---------------------------------+
04  |                        [CR][LF] |
    +---------------------------------+

*/
    m_dwInputPosLen += dwInputLen;

    // 行情報の設定
    // 
    int st;  //追加開始位置
    st = (m_pInputPos - m_pBuf) + // 挿入位置
         (m_dwInputLen - iLen);   // 


    TRACE(_T("iLen %d: m_dwInputLen%d\n"), iLen, m_dwInputLen);
    TRACE(_T("st %d: insertPos %d\n"), st, m_pInputPos - m_pBuf);

    int iNextLine; //追加開始位置の次の行
    iNextLine = IndexToLine(st) + 1;

    int lcnt2;
    lcnt2 = 0;

    if(iReturnCount > 0) 
    {
        // 入力内容に改行が含まれている場合
        // 改行までの行を反映させる
        lcnt2 =  LineSetCount(iNextLine, 0, iReturnCount);
    }
  
    //追加前の
    lcnt2 += LineSetCount(iNextLine + lcnt2, iInsertLineCount, 0);
    
    LineAddLength(iNextLine + lcnt2, iLen - dwInputLen);

    //論理行を反映させる
    TRACE(_T("lcnt2 %d: insertPos %d\n"), lcnt2);
    if (lcnt2 != 1)
    {
        //TODO:ちょっとやっつけすぎ
        LineSetInfo();
    }

    // 親ウィンドウに通知
    NotifyMessage(EN_UPDATE);

    if (iNextLine > lcnt2) 
    {
        // 行数減少
        InvalidateRect(m_hWnd, NULL, FALSE);
    } 
    else if (iReturnCount > 0 || 
             iNextLine != lcnt2) 
    {
        // 複数行
        LineRefresh(LineGet(iNextLine - 1), m_dwBufLen + m_dwInputLen);
    }
    else if (lcnt2 > 1) 
    {
        // 折り返しを含めて反映
        LineRefresh(LineGet(iNextLine - 1), LineGet(iNextLine + lcnt2 - 2));
    }
    else 
    {
        // 単一行
        LineRefresh(st, st);
    }
    SetScrollbar();
    EnsureVisible();

    if (m_bModified == FALSE) 
    {
        m_iUndoPos = m_iUndoLen;
    }
    m_bModified = TRUE;
    // 親ウィンドウに通知
    NotifyMessage(EN_CHANGE);
    return TRUE;
}

/**
 *  @brief   文字列の削除
 *  @param   [in] st 開始位置
 *  @param   [in] en 終了位置   
 *  @retval  なし
 *  @note
 */
void CMtEditorEngine::DeleteString(DWORD st, DWORD en)
{
    int lcnt1, lcnt2;
    int iDeleteLineNum;
    int i;

    if (st > en) 
    {
        SWAP(st, en);
    }

    if (m_pInputPos != NULL || 
            (m_pDeletePos != NULL && 
             m_pDeletePos != m_pBuf + st && 
             m_pDeletePos != m_pBuf + en))
    {
        FlushString(TRUE);
    }

    //
    DWORD dwBufSize = m_dwBufLen - m_dwDeleteLen;
    if (en - st > dwBufSize || 
        st >= dwBufSize) 
    {
        //TODO:本当にこれで良いのか？
        return;
    }

    // 行数取得
    iDeleteLineNum = IndexToLine(en) - IndexToLine(st);
    lcnt1 = LineGetCount(IndexToLine(en));


  //   +---------------------------------+
  // 0 |                        [CR]{LF] |
  //   +---------------------------------+
  //   +---------------------------------+
  // 1 | ABC-- St                        |
  //   +---------------------------------+
  //   +-------------------+
  // 2 |           [CR]{LF]|                
  //   +-------------------+
  //   +------------------------------+
  // 3 |                              |    
  //   +------------------------------+
  //   +------------------------------+
  // 4 |              en--EFG         |    
  //   +------------------------------+
  //   +------------------------------+
  // 5 |                              |    
  //   +------------------------------+
  //   +------------------------------+
  // 6 |                              |    
  //   +------------------------------+
  //   +-------------------+
  // 7 |           [CR]{LF]|               i=6
  //   +-------------------+
  //   +------------------------------+
  // 8 |                      [CR]{LF]|
  //   +------------------------------+

	


    // 削除位置設定
    m_pDeletePos = m_pBuf + st;
    m_dwDeleteLen += en - st;

    // 行情報の設定
    i = IndexToLine(st) + 1;
    if (iDeleteLineNum > 0) 
    {
        LineMove(i + iDeleteLineNum, -iDeleteLineNum);
    }
    lcnt2 = LineSetCount(i, lcnt1, 0);
    LineAddLength(i + lcnt2, st - en);

    //論理行を反映させる
    if (iDeleteLineNum != 1)
    {
        //TODO:ちょっとやっつけすぎ
        LineSetInfo();
    }

    // 親ウィンドウに通知
    NotifyMessage(EN_UPDATE);

    if (iDeleteLineNum > 0 || lcnt1 != lcnt2) 
    {
        // 複数行
        InvalidateRect(m_hWnd, NULL, FALSE);
    } 
    else if (lcnt2 > 1) 
    {
        // 折り返しを含めて反映
        LineRefresh( LineGet(i - 1), LineGet(i + lcnt2 - 2));
    }
    else 
    {
        // 単一行
        LineRefresh( st, en);
    }
    if (m_bModified == FALSE) 
    {
        m_iUndoPos = m_iUndoLen;
    }
    m_bModified = TRUE;
    // 親ウィンドウに通知
    NotifyMessage(EN_CHANGE);
}

/**
 * @brief   文字の削除
 * @param   [in] dwCharPos 文字位置
 * @retval  なし
 * @note	
 */
void CMtEditorEngine::DeleteStringChar(DWORD dwCharPos)
{
    int i = 1;

    if (m_dwCaretPos != m_dwSelectPos) 
    {
        // 選択文字の削除
        DeleteString(m_dwCaretPos, m_dwSelectPos);
        if (m_dwCaretPos > m_dwSelectPos) 
        {
            SWAP(m_dwCaretPos, m_dwSelectPos);
        }
        m_dwSelectPos = m_dwCaretPos;
    } 
    else 
    {
        // 一文字削除
#ifndef UNICODE
        if (IsLeadByte(IndexToChar(st)) == TRUE) {
            i++;
        }
#endif
        if (*(IndexToChar(dwCharPos)) == TEXT('\r') && 
            *(IndexToChar(dwCharPos + 1)) == TEXT('\n')) 
        {
            i++;
        }
        DeleteString(dwCharPos, dwCharPos + i);
    }
    SetScrollbar();
    EnsureVisible();
}

//!< 削除と入力バッファの反映
BOOL CMtEditorEngine::FlushString(const BOOL bUndo)
{
    TCHAR *p;

    if (m_pDeletePos != NULL) 
    {
        if (bUndo == TRUE) 
        {
            // undoに追加
            UndoSet(UNDO_TYPE_DELETE, m_pDeletePos - m_pBuf, m_dwDeleteLen);
        }

        // 削除文字列の反映
        MoveMemory(m_pDeletePos, 
                   m_pDeletePos + m_dwDeleteLen, 
                   sizeof(TCHAR) * (m_dwBufLen - (m_pDeletePos - m_pBuf + m_dwDeleteLen) + 1));

        m_dwBufLen -= m_dwDeleteLen;
        m_pDeletePos = NULL;
        m_dwDeleteLen = 0;

        if (m_dwBufLen + 1 < m_dwBufSize - RESERVE_BUF) 
        {
            m_dwBufSize = m_dwBufLen + 1 + RESERVE_BUF;
            p = reinterpret_cast<TCHAR*>(MemAlloc(sizeof(TCHAR) * m_dwBufSize));
            if (p == NULL) 
            {
                return FALSE;
            }
            CopyMemory(p, m_pBuf, sizeof(TCHAR) * (m_dwBufLen + 1));
            MemFree(reinterpret_cast<void**>(&m_pBuf));
            m_pBuf = p;
        }
        // 行情報の反映
        LineFlush();
        return TRUE;
    }

    if (m_pInputPos == NULL) 
    {
        return TRUE;
    }

    if (bUndo == TRUE) 
    {
        // undoに追加
        if (m_dwInputPosLen > 0) 
        {
            UndoSet(UNDO_TYPE_DELETE, m_pInputPos - m_pBuf, m_dwInputPosLen);
        }
        UndoSet(UNDO_TYPE_INPUT, m_pInputPos - m_pBuf, m_dwInputLen);
    }

    // 入力バッファの反映
    if (m_dwBufLen + m_dwInputLen - m_dwInputPosLen + 1 > m_dwBufSize) 
    {
        m_dwBufSize = m_dwBufLen + m_dwInputLen - m_dwInputPosLen + 1 + RESERVE_BUF;
        p = reinterpret_cast<TCHAR*>(MemAlloc(sizeof(TCHAR) * m_dwBufSize));
        if ( p == NULL) 
        {
            return FALSE;
        }

        if (m_pInputPos != m_pBuf) 
        {
            CopyMemory(p, m_pBuf, sizeof(TCHAR) * (m_pInputPos - m_pBuf));
        }

        CopyMemory(p + (m_pInputPos - m_pBuf), m_pInputBuf, sizeof(TCHAR) * m_dwInputLen);
        CopyMemory(p + (m_pInputPos - m_pBuf) + m_dwInputLen, m_pInputPos + m_dwInputPosLen, sizeof(TCHAR) * (m_dwBufLen - (m_pInputPos + m_dwInputPosLen - m_pBuf) + 1));
        MemFree(reinterpret_cast<void**>(&m_pBuf));
        m_pBuf = p;
    } 
    else 
    {
        MoveMemory(m_pInputPos + m_dwInputLen, m_pInputPos + m_dwInputPosLen, sizeof(TCHAR) * (m_dwBufLen - (m_pInputPos + m_dwInputPosLen - m_pBuf) + 1));
        CopyMemory(m_pInputPos, m_pInputBuf, sizeof(TCHAR) * m_dwInputLen);
    }

    m_dwBufLen += m_dwInputLen - m_dwInputPosLen;

    // 入力バッファの解放
    if (m_dwInputSize > RESERVE_INPUT) 
    {
        MemFree(reinterpret_cast<void**>(&m_pInputBuf));
        m_dwInputSize = RESERVE_INPUT;
        m_pInputBuf = reinterpret_cast<TCHAR*>(MemAlloc(sizeof(TCHAR) * m_dwInputSize));
        if ( m_pInputBuf == NULL) 
        {
            return FALSE;
        }
    }
    *m_pInputBuf = TEXT('\0');
    m_dwInputLen = 0;
    m_pInputPos = NULL;
    m_dwInputPosLen = 0;

    // 行情報の反映
    LineFlush();
    return TRUE;
}

//!< 描画情報の初期化
BOOL CMtEditorEngine::DrawInit()
{
    HDC hdc;
    HBITMAP hBmp;
    HRGN hrgn[2];
    RECT rect;
    TEXTMETRIC tm;

    GetClientRect(m_hWnd, &rect);
    GetTextMetrics(m_hDc, &tm);

    hdc = GetDC(m_hWnd);
    hBmp = CreateCompatibleBitmap(hdc, rect.right, tm.tmHeight + m_iSpacing);
    m_hRetBmp = static_cast<HBITMAP>(SelectObject(m_hDc, hBmp));
    ReleaseDC(m_hWnd, hdc);

    // リージョンの作成
    hrgn[0] = CreateRectRgnIndirect(&rect);
    m_hRgn = CreateRectRgnIndirect(&rect);
    // 除去するリージョンの作成
    GetEditRect(&rect);
    hrgn[1] = CreateRectRgnIndirect(&rect);
    // リージョンの結合
    CombineRgn(m_hRgn, hrgn[0], hrgn[1], RGN_DIFF);
    DeleteObject(hrgn[0]);
    DeleteObject(hrgn[1]);
    return TRUE;

}


//!< 描画情報の解放
void CMtEditorEngine::DrawFree()
{
    HBITMAP hBmp;

    hBmp = static_cast<HBITMAP>(SelectObject(m_hDc, m_hRetBmp));
    DeleteObject(hBmp);
    DeleteObject(m_hRgn);
    m_hRgn = NULL;
}

/**
 * @brief   矩形描画
 * @param   [in] hDc        デバイスコンテキスト
 * @param   [in] left
 * @param   [in] top
 * @param   [in] right
 * @param   [in] bottom
 * @retval  なし
 * @note
 */
void CMtEditorEngine::DrawRect(const HDC hDc, 
                               const int left, 
                               const int top, 
                               const int right, 
                               const int bottom)
{
    RECT trect;
    HBRUSH hBrush;

    if (right < 0) 
    {
        return;
    }
    if (m_bNoHideSel == FALSE && 
        GetFocus() != m_hWnd) 
    {
        return;
    }

    ::SetRect(&trect, left, top, right, bottom);

    hBrush = ::CreateSolidBrush(GetSysColor(COLOR_HIGHLIGHT));
    ::FillRect(hDc, &trect, hBrush);
    ::DeleteObject(hBrush);
}

/**
 * @brief   文字描画
 * @param   [in] hDc        デバイスコンテキスト
 * @param   [in] pRect      描画領域
 * @param   [in] iLeftX     開始位置（基準点）の x 座標
 * @param   [in] iTopY      開始位置（基準点）の y 座標
 * @param   [in] pStr       文字列
 * @param   [in] iLength    文字数
 * @param   [in] bSel       選択状態
 * @retval  なし
 * @note
 */
int CMtEditorEngine::DrawString(const HDC       hDc, 
                                const RECT*     pRect, 
                                const int       iLeftX,
                                const int       iTopY, 
                                const TCHAR*    pStr, 
                                const int       iLength, 
                                const BOOL      bSel,
                                const COLORREF  crText)
{
    RECT rect;
    SIZE sz;

    StdString strIn(pStr, iLength);

//TRACE(_T("DrawString %s :%d (%08x)\n"), strIn.c_str(), iLength, crText);

    GetTextExtentPoint32(m_hDc, pStr, iLength, &sz);
    if (iLeftX + sz.cx >= pRect->left) 
    {
        if (bSel == TRUE && 
           (m_bNoHideSel == TRUE || 
            ::GetFocus() == m_hWnd)) 
        {
            //選択表示
            ::SetTextColor(hDc, m_crSel);
            ::SetBkColor(hDc, m_crSelBack);
            DrawRect(hDc, iLeftX, pRect->top, iLeftX + sz.cx, pRect->bottom);
TRACE(_T("DrawRect [%s] %d-%d \n"), strIn.c_str(), iLeftX, iLeftX + sz.cx);
        }

        if (m_crCurrent != crText)
        {
            ::SetTextColor(hDc, crText);
            m_crCurrent = crText;
        }

        ::SetRect(&rect, iLeftX, pRect->top, iLeftX + sz.cx, pRect->bottom);
        ::ExtTextOut(hDc, iLeftX, iTopY, 0, &rect, pStr, iLength, NULL);


        if (bSel == TRUE) 
        {
            ::SetTextColor(hDc, m_crText);
            ::SetBkColor(hDc, m_crBack);
        }
    }
    return sz.cx;
}

/**
 * @brief   1行描画
 * @param   [in] hDc        デバイスコンテキスト
 * @param   [in] iLinePos   行位置
 * @param   [in] iLeft      左座標
 * @param   [in] iRight     右座標
 * @retval  なし
 * @note
 */
void CMtEditorEngine::AnalysisLine(const HDC hDc, 
                               const int iLinePos, 
                               const int iLeft,
                               const int iRight,
                               std::map<int,STRING_ANALYSYS_STS>* pMap)

{

    RECT rcDraw;
    int offset;
    int top;
    int width;
    int tab;

    offset = m_iLeftMargin - (m_iScrollX * m_iCharWidth);
    width = (m_iScrollX * m_iCharWidth) + iRight;
    top = m_iSpacing / 2;
    rcDraw.left = iLeft;
    rcDraw.top = 0;
    rcDraw.right = iRight;
    rcDraw.bottom = rcDraw.top + m_iFontHeight;


    STRING_ANALYSYS_STS  stsLine = STS_NORMAL;
    if (iLinePos > 0)
    {
        stsLine = m_lstLine[iLinePos - 1].eEndSts;
    }

    TCHAR  r;
    TCHAR  s;
    TCHAR* p;
    DWORD dwLineHead     = LineGet(iLinePos);       //先頭文字位置
    DWORD dwLineNextHead = LineGet(iLinePos + 1);  

    StdString strText;
    DWORD dwDrawPos;
    p = IndexToChar(dwLineHead);
    s = r = _T('');

    COLORREF crText;
    switch(stsLine)
    {
        case STS_BLOCK_COMMNET: {crText = m_crCommnet; break;}
        case STS_LINE_COMMNET:  {crText = m_crCommnet; break;}
        case STS_SINGLE_QUOTATION:  {crText = m_crString; break;}
        case STS_DOUBLE_QUOTATION:  {crText = m_crString; break;}
        case STS_TRIPLE_QUOTATION:  {crText = m_crString; break;}
        case STS_NORMAL          :  {crText = m_crText; break;}
        case STS_RESERV1         :  {crText = m_crReserv1; break;}
        case STS_RESERV2         :  {crText = m_crReserv2; break;}
        case STS_RESERV3         :  {crText = m_crReserv3; break;}
        default:    {crText = m_crText; break;}
    }




    DWORD dwSelStart = -1;
    DWORD dwSelEnd = -1;
    if (m_dwSelectPos != m_dwCaretPos)
    {
        dwSelStart = m_dwSelectPos;
        dwSelEnd = m_dwCaretPos;

        if (m_dwSelectPos > m_dwCaretPos)
        {
            std::swap(dwSelStart, dwSelEnd);
        }
    }





    int iStartCnt = 0;
    DWORD         dwDrawStart;
    DWORD         dwDrawEnd;

    if (pMap)
    {
        pMap->clear();
        pMap->insert(std::pair<int,STRING_ANALYSYS_STS>(0, stsLine));

        for (dwDrawPos = dwLineHead,
             dwDrawStart = dwLineHead ;
             dwDrawPos < dwLineNextHead;
             dwDrawPos++,
             iStartCnt++,
             p = CharNext(p)) 
        {

            if ((dwDrawPos >= dwSelStart) && 
                (dwDrawPos <= dwSelEnd))
            {
                m_bSel = TRUE;
                offset += DrawString(hDc, &rcDraw, offset, top, strText.c_str(), strText.length(), TRUE, crText);
                strText = _T("");
            }
            else
            {
                m_bSel = FALSE;
                offset += DrawString(hDc, &rcDraw, offset, top, strText.c_str(), strText.length(), FALSE, crText);
                strText = _T("");
            }

            if( *p != _T('\r') &&
                *p != _T('\n'))
            {
                strText += *p;
            }


            switch(stsLine)
            {
            case STS_BLOCK_COMMNET:
                if ( ( s == _T('*')) &&
                     (*p == _T('/')))
                {
                    pMap->insert(std::pair<int,STRING_ANALYSYS_STS>(iStartCnt, STS_NORMAL));
                    stsLine = STS_NORMAL;
                    offset += DrawString(hDc, &rcDraw, offset, top, strText.c_str(), strText.length(), m_bSel, m_crCommnet);
                    dwDrawStart += strText.length();
                    strText = _T("");
                    crText = m_crText;
                }
                break;
            case STS_LINE_COMMNET:

                if(  s == _T('\r') &&
                    *p == _T('\n'))
                {
                    pMap->insert(std::pair<int,STRING_ANALYSYS_STS>(iStartCnt, STS_NORMAL));
                    stsLine = STS_NORMAL;
                    offset += DrawString(hDc, &rcDraw, offset, top, strText.c_str(), strText.length(), m_bSel, m_crCommnet);
                    dwDrawStart += strText.length();
                    strText = _T("");
                    crText = m_crText;
                }
                break;
            case STS_DOUBLE_QUOTATION:
                if ( ( s != _T('\'')) &&
                     (*p == _T('\"')))
                {
                    pMap->insert(std::pair<int,STRING_ANALYSYS_STS>(iStartCnt, STS_NORMAL));
                    stsLine = STS_NORMAL;
                    offset += DrawString(hDc, &rcDraw, offset, top, strText.c_str(), strText.length(), m_bSel, m_crString);
                    dwDrawStart += strText.length();
                    strText = _T("");
                    crText = m_crText;
                }

                if(  s == _T('\r') &&
                    *p == _T('\n'))
                {
                    pMap->insert(std::pair<int,STRING_ANALYSYS_STS>(iStartCnt, STS_NORMAL));
                    stsLine = STS_NORMAL;
                    offset += DrawString(hDc, &rcDraw, offset, top, strText.c_str(), strText.length(), m_bSel, m_crCommnet);
                    dwDrawStart += strText.length();
                    strText = _T("");
                    crText = m_crText;
                }
                break;

            case STS_SINGLE_QUOTATION:
                if ( ( s != _T('\\')) &&
                     (*p == _T('\'')))
                {
                    pMap->insert(std::pair<int,STRING_ANALYSYS_STS>(iStartCnt, STS_NORMAL));
                    stsLine = STS_NORMAL;
                    offset += DrawString(hDc, &rcDraw, offset, top, strText.c_str(), strText.length(), m_bSel, m_crString);
                    dwDrawStart += strText.length();
                    strText = _T("");
                    crText = m_crText;
                }

                if(  s == _T('\r') &&
                    *p == _T('\n'))
                {
                    pMap->insert(std::pair<int,STRING_ANALYSYS_STS>(iStartCnt, STS_NORMAL));
                    stsLine = STS_NORMAL;
                    offset += DrawString(hDc, &rcDraw, offset, top, strText.c_str(), strText.length(), m_bSel, m_crString);
                    dwDrawStart += strText.length();
                    strText = _T("");
                    crText = m_crText;
                }
                break;

            case STS_TRIPLE_QUOTATION:
                if ( ( r == _T('\"')) && 
                     ( s == _T('\"')) &&
                     (*p == _T('\"')))
                {
                    pMap->insert(std::pair<int,STRING_ANALYSYS_STS>(iStartCnt, STS_NORMAL));
                    stsLine = STS_NORMAL;
                    offset += DrawString(hDc, &rcDraw, offset, top, strText.c_str(), strText.length(), m_bSel, m_crString);
                    dwDrawStart += strText.length();
                    strText = _T("");
                    crText = m_crText;
                }

                break;
            case STS_NORMAL:
                if (s == _T('/'))
                {
                    if(*p == _T('*'))
                    {
                        pMap->insert(std::pair<int,STRING_ANALYSYS_STS>(iStartCnt - 1, STS_BLOCK_COMMNET));
                        stsLine = STS_BLOCK_COMMNET;

                        offset += DrawString(hDc, &rcDraw, offset, top, strText.c_str(), strText.length() - 2, m_bSel, crText);
                        dwDrawStart += strText.length() - 2;
                        strText = _T("/*");
                        crText = m_crCommnet;
                    }
                    else if (*p == _T('/'))
                    {
                        pMap->insert(std::pair<int,STRING_ANALYSYS_STS>(iStartCnt - 1, STS_LINE_COMMNET));
                        stsLine = STS_LINE_COMMNET;

                        offset += DrawString(hDc, &rcDraw, offset, top, strText.c_str(), strText.length() - 2, m_bSel, crText);
                        dwDrawStart += strText.length() - 2;
                        strText = _T("//");
                        crText = m_crCommnet;
                    }
                }

                if (*p == _T('\"'))
                {
                   //Tirple """
                   if ((s == _T('\"')) && 
                       (r == _T('\"')))
                   {
                        pMap->insert(std::pair<int,STRING_ANALYSYS_STS>(iStartCnt, STS_TRIPLE_QUOTATION));
                        stsLine = STS_TRIPLE_QUOTATION;

                        offset += DrawString(hDc, &rcDraw, offset, top, strText.c_str(), strText.length() - 2, m_bSel, crText);
                        dwDrawStart += strText.length() - 2;
                        strText = _T("\"\"\"");
                        crText = m_crString;
                   }
                   else  if (s != _T('\\'))
                   {
                        pMap->insert(std::pair<int,STRING_ANALYSYS_STS>(iStartCnt, STS_DOUBLE_QUOTATION));
                        stsLine = STS_DOUBLE_QUOTATION;

                        offset += DrawString(hDc, &rcDraw, offset, top, strText.c_str(), strText.length() - 1, m_bSel, crText);
                        dwDrawStart += strText.length() - 1;
                        strText = _T("\"");
                        crText = m_crString;
                   }
                }
                else if (*p == _T('\''))
                {
                   if (s != _T('\\'))
                    {
                        pMap->insert(std::pair<int,STRING_ANALYSYS_STS>(iStartCnt, STS_DOUBLE_QUOTATION));
                        stsLine = STS_SINGLE_QUOTATION;

                        offset += DrawString(hDc, &rcDraw, offset, top, strText.c_str(), strText.length() - 1, m_bSel, crText);
                        dwDrawStart += (strText.length() - 1);
                        strText = _T("\'");
                        crText = m_crString;
                   }
                }
                break;

            default:
                break;
            }
            r = s;
            s = *p;
        }
    }

    DrawString(hDc, &rcDraw, offset, top, strText.c_str(), strText.length(), m_bSel, crText);

    m_lstLine[iLinePos].eEndSts = stsLine;
}

/**
 * @brief   1行描画
 * @param   [in] hDc        デバイスコンテキスト
 * @param   [in] iLinePos   行位置
 * @param   [in] iLeft      左座標
 * @param   [in] iRight     右座標
 * @retval  なし
 * @note
 */
void CMtEditorEngine::DrawLine(const HDC hDc, 
                               const int iLinePos, 
                               const int iLeft,
                               const int iRight)
{
    RECT rcDraw;
    RECT rcLineNo;
    TCHAR *p, *r, *s;
    int offset;
    int top;
    int width;
    int tab;

    offset = m_iLeftMargin - (m_iScrollX * m_iCharWidth);
    width = (m_iScrollX * m_iCharWidth) + iRight;
    top = m_iSpacing / 2;
    rcDraw.left = iLeft;
    rcDraw.top = 0;
    rcDraw.right = iRight;
    rcDraw.bottom = rcDraw.top + m_iFontHeight;

    DWORD dwLineHead     = LineGet(iLinePos);       //先頭文字位置
    DWORD dwLineNextHead = LineGet(iLinePos + 1);   
    DWORD dwDrawPos;


    COLORREF crText;

    s = p = IndexToChar(dwLineHead);

    //とりあえず行番号を書く
    rcLineNo.left  = m_iIndicatorWidth;
    rcLineNo.right = rcLineNo.left + 160;
    rcLineNo.top = rcDraw.top ;
    rcLineNo.bottom = rcDraw.bottom;

    m_crCurrent = m_crLineNo;
    ::SetTextColor(hDc, m_crCurrent);
    TCHAR szBuf[256];
    _stprintf_s(szBuf, _T("%02d:%02d"), iLinePos, m_lstLine[iLinePos].iLogicalLine);
    ::ExtTextOut(
    hDc,              // デバイスコンテキストのハンドル
    m_iIndicatorWidth,            // 開始位置（基準点）の x 座標
    0,                // 開始位置（基準点）の y 座標
    ETO_CLIPPED,      // 長方形領域の使い方のオプション
    &rcLineNo,        // 長方形領域の入った構造体へのポインタ
    &szBuf[0],        // 文字列
    5,                // 文字数
    NULL              // 文字間隔の入った配列
    );

    if (m_lstLine[iLinePos].iBreakPoint != UNSET)
    {
        
        
    }

    //-------------------
    //  色変更点の計算
    //-------------------
    std::map<int,STRING_ANALYSYS_STS> mapAnalysis;
    AnalysisLine(hDc, iLinePos, iLeft, iRight, &mapAnalysis);
    std::map<int,STRING_ANALYSYS_STS>::iterator itMap;

    itMap = mapAnalysis.begin();


    /*
    //-----------------------------------------

    s = p = IndexToChar(dwLineHead);
    crText = m_crText;


    int iPos = 0;
    for (dwDrawPos = dwLineHead;
         dwDrawPos < dwLineNextHead;
         dwDrawPos++, p = CharNext(p), iPos++) 
    {
        if  (offset > width)
        {
            break;
        }

        //  色変更
        if ((itMap != mapAnalysis.end()) &&
            (itMap->first == iPos))
        {
TRACE(_T("COLOR %d, %d\n"), iPos, itMap->second);

            switch(itMap->second)
            {
            case STS_BLOCK_COMMNET: {crText = m_crCommnet; break;}
            case STS_LINE_COMMNET:  {crText = m_crCommnet; break;}
            case STS_SINGLE_QUOTATION:  {crText = m_crString; break;}
            case STS_DOUBLE_QUOTATION:  {crText = m_crString; break;}
            case STS_NORMAL          :  {crText = m_crText; break;}
            case STS_RESERV1         :  {crText = m_crReserv1; break;}
            case STS_RESERV2         :  {crText = m_crReserv2; break;}
            case STS_RESERV3         :  {crText = m_crReserv3; break;}
            default: break;
            }

            if (itMap != mapAnalysis.end())
            {
                itMap++;
            }
        }
    //-----------------------------------------


        if (s != p) 
        {
            if (m_pInputPos != NULL && 
                (p == m_pInputPos + m_dwInputPosLen || 
                 p == m_pInputBuf)) 
            {
                // 入力バッファの出力
                if(p == m_pInputBuf)
                {
                    r = m_pInputPos;
                }
                else
                {
                    r =  m_pInputBuf + m_dwInputLen;
                }

TRACE(_T("Draw 1\n"));
                offset += DrawString(hDc, &rcDraw, offset, top, s, r - s, m_bSel, crText);
                s = p;
            } 
            else if (m_pDeletePos != NULL && 
                    p == m_pDeletePos + m_dwDeleteLen) 
            {
                // 削除文字列
TRACE(_T("Draw 2\n"));
                offset += DrawString(hDc, &rcDraw, offset, top, s, m_pDeletePos - s, m_bSel, crText);
                s = p;
            } 
            else if ((dwDrawPos % DRAW_LEN) == 0) 
            {
TRACE(_T("Draw 3\n"));
                offset += DrawString(hDc, 
                    &rcDraw, 
                    offset,     //開始位置（基準点）の x 座標
                    top,        //開始位置（基準点）の y 座標
                    s,          //文字列
                    p - s,      //文字数
                    m_bSel, crText);
                s = p;
            }
        }

        if ((dwDrawPos >= m_dwSelectPos && dwDrawPos < m_dwCaretPos) || 
            (dwDrawPos >= m_dwCaretPos && dwDrawPos < m_dwSelectPos)) 
        {
            if (m_bSel == FALSE) 
            {
TRACE(_T("Draw 4\n"));
                // 選択開始
                offset += DrawString(hDc, &rcDraw, offset, top, s, p - s, m_bSel, crText);
                s = p;
                m_bSel = TRUE;
            }
        } 
        else if (m_bSel == TRUE) 
        {
            // 選択終了
TRACE(_T("Draw 5\n"));
            offset += DrawString(hDc, &rcDraw, offset, top, s, p - s, m_bSel, crText);
            s = p;
            m_bSel = FALSE;
        }

        if (*p == TEXT('\r') || *p == TEXT('\n')) 
        {
            // return
TRACE(_T("Draw 6\n"));
            offset += DrawString(hDc, &rcDraw, offset, top, s, p - s, m_bSel, crText);
           s = p;
            if (m_bSel == TRUE && offset + m_iCharWidth >= 0) 
            {
                DrawRect(hDc,offset, top - (m_iSpacing / 2),
                    offset + m_iCharWidth, top - (m_iSpacing / 2) + m_iFontHeight);
            }
            break;
        } 
        else if (*p == TEXT('\t')) 
        {
            // Tab
TRACE(_T("Draw 7\n"));
            offset += DrawString(hDc, &rcDraw, offset, top, s, p - s, m_bSel, crText);

            tab = m_iTabStop * m_iCharWidth -
                ((offset - m_iLeftMargin + 
                (m_iScrollX * m_iCharWidth)) % (m_iTabStop * m_iCharWidth));

            if (tab < m_iCharWidth) 
            {
                tab += m_iTabStop * m_iCharWidth;
            }

            if (m_bSel == TRUE && 
                offset + tab >= 0) 
            {
                DrawRect(hDc,offset, top - (m_iSpacing / 2),
                    offset + tab, top - (m_iSpacing / 2) + m_iFontHeight);
            }
            offset += tab;
            s = CharNext(p);
            continue;
        }
#ifndef UNICODE
        if (IsLeadByte(p) == TRUE && CharNext(p) == (p + 1)) {
            p = CharNext(p);
            dwDrawPos++;
        }
#endif
    }


    if (s != p && 
        m_pInputPos != NULL && 
        (p == m_pInputPos + m_dwInputPosLen || p == m_pInputBuf)) 
    {
        r = (p == m_pInputBuf) ? m_pInputPos : (m_pInputBuf + m_dwInputLen);
    } 
    else if (s != p && 
        m_pDeletePos != NULL && 
        p == m_pDeletePos + m_dwDeleteLen) 
    {
        r = m_pDeletePos;
    }
    else 
    {
        r = p;
    }
TRACE(_T("Draw 8\n"));
    DrawString(hDc, &rcDraw, offset, top, s, r - s, m_bSel, crText);
    */
    DrawBreakPoint(iLinePos, FALSE);
    DrawExecIndicator(iLinePos, FALSE);
}

//!< キャレットのサイズ設定
void CMtEditorEngine::CaretSetSize()
{
    TCHAR *p;
    int csize;
    int len;

    p = IndexToChar(m_dwCaretPos);
    csize = GetCharExtent( p, &len);

    if (csize <= 0) 
    {
        csize = m_iCharWidth;
    }
    DestroyCaret();
    ::CreateCaret(m_hWnd, NULL, csize, m_iFontHeight);
}

//!< 文字位置からキャレットの位置取得
int CMtEditorEngine::CaretCharToCaret(const HDC mdc, const int i, const DWORD cp)
{
    SIZE sz;
    TCHAR *p, *r, *s;
    DWORD j;
    int offset;
    int tab;

    offset = m_iLeftMargin - (m_iScrollX * m_iCharWidth);
    for (j = LineGet(i), s = p = IndexToChar(LineGet(i)); 
        j < LineGet(i + 1); 
        j++, p = CharNext(p)) 
    {
        r = NULL;
        if (s != p && 
            m_pInputPos != NULL && 
            (p == m_pInputPos + m_dwInputPosLen || p == m_pInputBuf)) 
        {
            // 入力バッファ
            r = (p == m_pInputBuf) ? m_pInputPos : (m_pInputBuf + m_dwInputLen);
        }
        else if (s != p && 
            m_pDeletePos != NULL && 
            p == m_pDeletePos + m_dwDeleteLen) 
        {
            // 削除文字列
            r = m_pDeletePos;
        }

        if (r != NULL) 
        {
            GetTextExtentPoint32(mdc, s, r - s, &sz);
            offset += sz.cx;
            s = p;
        }

        if (j >= cp) 
        {
            break;
        }

        if (*p == TEXT('\r') || *p == TEXT('\n')) 
        {
            break;
        } 
        else if (*p == TEXT('\t')) 
        {
            // tab
            GetTextExtentPoint32(mdc, s, p - s, &sz);
            offset += sz.cx;
            tab = m_iTabStop * m_iCharWidth - 
                ((offset - m_iLeftMargin + (m_iScrollX * m_iCharWidth)) % (m_iTabStop * m_iCharWidth));
            if (tab < m_iCharWidth) 
            {
                tab += m_iTabStop * m_iCharWidth;
            }
            offset += tab;
            s = CharNext(p);
            continue;
        }
#ifndef UNICODE
        if (IsLeadByte(p) == TRUE) {
            p = CharNext(p);
            j++;
        }
#endif
    }

    if (s != p && 
        m_pInputPos != NULL && 
        (p == m_pInputPos + m_dwInputPosLen || p == m_pInputBuf)) 
    {
        r = (p == m_pInputBuf) ? m_pInputPos : (m_pInputBuf + m_dwInputLen);
    }

    else if (s != p && 
        m_pDeletePos != NULL && 
        p == m_pDeletePos + m_dwDeleteLen) 
    {
        r = m_pDeletePos;
    }
    else 
    {
        r = p;
    }
    GetTextExtentPoint32(mdc, s, r - s, &sz);
    return (offset + sz.cx);
}

//!<  座標からキャレットの位置取得
DWORD CMtEditorEngine::CaretPointToCaret(const int x, const int y)
{
    TCHAR *p;
    DWORD j;
    int offset, old;
    int tab;
    int clen = 1;
    int i;

    i = m_iScrollY + ((y - m_iTopMargin) / m_iFontHeight);
    if (i < 0) 
    {
        return 0;
    }
    else if (i >= m_iLineLen) 
    {
        i = m_iLineLen - 1;
    }

    if (i < 0) 
    {
        return 0;
    }


    old = offset = m_iLeftMargin - (m_iScrollX * m_iCharWidth);
    for (j = LineGet(i), p = IndexToChar(LineGet(i)); 
        j < LineGet(i + 1); 
        j++, p = CharNext(p)) 
    {
        if (*p == TEXT('\r') || *p == TEXT('\n')) 
        {
            return j;
        } 
        else if (*p == TEXT('\t'))
        {
            // tab
            clen = 1;
            tab = m_iTabStop * m_iCharWidth - ((offset - m_iLeftMargin + (m_iScrollX * m_iCharWidth)) % (m_iTabStop * m_iCharWidth));
            if (tab < m_iCharWidth) 
            {
                tab += m_iTabStop * m_iCharWidth;
            }
            offset += tab;
            if (offset > x) 
            {
                break;
            }
            old = offset;
        } 
        else 
        {
            offset += GetCharExtent( p, &clen);
            if (offset > x) 
            {
                if ((offset - old) / 2 < x - old) 
                {
                    j += clen;
                }
                break;
            }
            old = offset;
            if (clen == 2) 
            {
                p = CharNext(p);
                j++;
            }
        }
    }
    if (j == LineGet(i + 1) && (i + 1) < m_iLineLen) 
    {
        j -= clen;
    }
    return j;
}

//!< キャレット位置のトークンを取得
void CMtEditorEngine::CaretGetToken()
{
    TCHAR *p, *r;
    int i;

    i = IndexToLine(m_dwCaretPos);
    for (; i > 0; i--) 
    {
        // 論理行の先頭に移動
        p = IndexToChar(LineGet(i) - 1);
        if (*p == TEXT('\r') || *p == TEXT('\n')) {
            break;
        }
    }

    p = IndexToChar(LineGet(i));
    while (CharToIndex(p) < GetBufferLength() && 
        *p != TEXT('\r') && 
        *p != TEXT('\n')) 
    {
        if (IsLeadByte(p) == TRUE) {
            r = p;
            while (IsLeadByte(p) == TRUE) 
            {
                p = CharNext(p);
                p = CharNext(p);
            }

        } 
        else if (*p != TEXT(' ') && 
            *p != TEXT('\t') && 
            *p != TEXT('\r') && 
            *p != TEXT('\n')) 
        {
            r = p;
            while (CharToIndex(p) < GetBufferLength() && 
                IsLeadByte(p) == FALSE &&
                *p != TEXT(' ') && 
                *p != TEXT(' ') && 
                *p != TEXT('\t') && 
                *p != TEXT('\r') && 
                *p != TEXT('\n')) 
            {
                p = CharNext(p);
            }

        } 
        else 
        {
            r = p;
            p = CharNext(p);
        }
        if (p > IndexToChar(m_dwCaretPos)) 
        {
            m_dwCaretPos = CharToIndex(p);
            m_dwSelectPos = CharToIndex(r);
            break;
        }
    }
}

//!< キャレットの移動
void CMtEditorEngine::CaretMove(const int key)
{
    RECT rect;
    POINT pt;
    TCHAR *p;
    DWORD oldcp, oldsp;
    DWORD j;
    int i, t;

    GetEditRect(&rect);

    oldcp = m_dwCaretPos;
    oldsp = m_dwSelectPos;
    i = IndexToLine(m_dwCaretPos);

    switch (key) {
    case VK_HOME:
        if (GetKeyState(VK_CONTROL) < 0) 
        {
            // 全体の先頭
            m_dwCaretPos = 0;
        }
        else 
        {
            // 論理行頭
            for (; *(IndexToChar(m_dwCaretPos)) == TEXT('\r') || 
                *(IndexToChar(m_dwCaretPos)) == TEXT('\n'); 
                m_dwCaretPos--)
            {;}

            for (; m_dwCaretPos > 0 && 
                *(IndexToChar(m_dwCaretPos)) != TEXT('\r') && 
                *(IndexToChar(m_dwCaretPos)) != TEXT('\n'); 
            m_dwCaretPos--)
            {;}

            if (m_dwCaretPos > 0) 
            {
                m_dwCaretPos++;
            }
        }
        InvalidateRect(m_hWnd, NULL, FALSE);
        break;

    case VK_END:
        if (GetKeyState(VK_CONTROL) < 0) 
        {
            // 全体の末尾
            m_dwCaretPos = GetBufferLength();
        }
        else 
        {
            // 論理行末
            for (; m_dwCaretPos < GetBufferLength() && 
                *(IndexToChar(m_dwCaretPos)) != TEXT('\r') &&
                *(IndexToChar(m_dwCaretPos)) != TEXT('\n'); 
            m_dwCaretPos++)
            {;}
        }
        InvalidateRect(m_hWnd, NULL, FALSE);
        break;

    case VK_PRIOR:
        // Page UP
        if (GetCaretPos(&pt) == FALSE) {
            break;
        }

        if (m_iCaretPosX == 0) 
        {
            m_iCaretPosX = pt.x;
        }

        m_dwCaretPos = CaretPointToCaret(m_iCaretPosX, pt.y - (rect.bottom - rect.top));

        if (GetKeyState(VK_SHIFT) >= 0) 
        {
            m_dwSelectPos = m_dwCaretPos;
        }
        ::SendMessage(m_hWnd, WM_VSCROLL, SB_PAGEUP, 0);
        break;

    case VK_NEXT:
        // Page Down
        if (GetCaretPos(&pt) == FALSE) {
            break;
        }

        if (m_iCaretPosX == 0) 
        {
            m_iCaretPosX = pt.x;
        }
        m_dwCaretPos = CaretPointToCaret(m_iCaretPosX, pt.y + (rect.bottom - rect.top));
        if (GetKeyState(VK_SHIFT) >= 0) 
        {
            m_dwSelectPos = m_dwCaretPos;
        }
        SendMessage(m_hWnd, WM_VSCROLL, SB_PAGEDOWN, 0);
        break;

    case VK_UP:
        if (--i < 0) 
        {
            if (GetKeyState(VK_SHIFT) >= 0) 
            {
                m_dwSelectPos = m_dwCaretPos;
            }
            break;
        }

        if (GetCaretPos(&pt) == FALSE) {
            break;
        }

        if (m_iCaretPosX == 0) 
        {
            m_iCaretPosX = pt.x;
        }
        m_dwCaretPos = CaretPointToCaret(m_iCaretPosX, pt.y - m_iFontHeight);
        break;

    case VK_DOWN:
        if (++i > m_iLineLen - 1) 
        {
            if (GetKeyState(VK_SHIFT) >= 0) 
            {
                m_dwSelectPos = m_dwCaretPos;
            }
            break;
        }

        if (GetCaretPos(&pt) == FALSE) 
        {
            break;
        }

        if (m_iCaretPosX == 0) 
        {
            m_iCaretPosX = pt.x;
        }
        m_dwCaretPos = CaretPointToCaret(m_iCaretPosX, pt.y + m_iFontHeight);
        break;

    case VK_LEFT:
        m_iCaretPosX = 0;
        if (m_dwCaretPos != m_dwSelectPos && GetKeyState(VK_SHIFT) >= 0) 
        {
            // 選択解除
            if (m_dwCaretPos > m_dwSelectPos)
            {
                SWAP(m_dwCaretPos, m_dwSelectPos);
            }
            m_dwSelectPos = m_dwCaretPos;
            break;
        }

        if (m_dwCaretPos == LineGet(i) && 
            --i < 0) 
        {
            break;
        }
        for (j = LineGet(i); 
            j < LineGet(i + 1) && 
            j < m_dwCaretPos;
        j++) 
        {
            t = j;
            p = IndexToChar(j);
            if (*p == TEXT('\r') || *p == TEXT('\n')) {
                break;
            }
#ifndef UNICODE
            if (IsLeadByte(p) == TRUE) {
                p = CharNext(p);
                j++;
            }
#endif
        }
        m_dwCaretPos = t;
        break;

    case VK_RIGHT:
        m_iCaretPosX = 0;
        if (m_dwCaretPos != m_dwSelectPos && 
            GetKeyState(VK_SHIFT) >= 0) 
        {
            // 選択解除
            if (m_dwCaretPos < m_dwSelectPos) 
            {
                SWAP(m_dwCaretPos, m_dwSelectPos);
            }
            m_dwSelectPos = m_dwCaretPos;
            break;
        }

        if (m_dwCaretPos >= GetBufferLength()) 
        {
            break;
        }

        if ((m_dwCaretPos + 1) < GetBufferLength() &&
            *(IndexToChar(m_dwCaretPos)) == TEXT('\r') &&
            *(IndexToChar(m_dwCaretPos + 1)) == TEXT('\n')) 
        {
            m_dwCaretPos += 2;
        }
        else if (*(IndexToChar(m_dwCaretPos)) == TEXT('\r') || 
            *(IndexToChar(m_dwCaretPos)) == TEXT('\n')) 
        {
            m_dwCaretPos++;
        } else {
#ifdef UNICODE
            m_dwCaretPos++;
#else
            if (IsLeadByte(IndexToChar(m_dwCaretPos)) == TRUE) {
                m_dwCaretPos += 2;
            } else {
                m_dwCaretPos++;
            }
#endif
        }
        break;
    }

    if (GetKeyState(VK_SHIFT) >= 0) 
    {
        m_dwSelectPos = m_dwCaretPos;
    }

    if (oldsp != m_dwSelectPos) 
    {
        LineRefresh( oldcp, oldsp);
    }
    LineRefresh( oldcp, m_dwCaretPos);
    EnsureVisible();
}


LRESULT CMtEditorEngine::OnCreate(const WPARAM wParam, const LPARAM lParam)
{
    HDC hdc;
    int i;

#ifdef OP_XP_STYLE
    // XP
    if ((m_hModThemes = LoadLibrary(TEXT("uxtheme.dll"))) != NULL) 
    {
        if (F_OpenThemeData == NULL) 
        {
            F_OpenThemeData = reinterpret_cast<OPENTHEMEDATA>(::GetProcAddress(m_hModThemes, "OpenThemeData"));
        }

        if (F_OpenThemeData != NULL) 
        {
            m_hTheme = F_OpenThemeData(m_hWnd, L"Edit");
        }
    }
#endif	// OP_XP_STYLE

    m_iId = (int)((LPCREATESTRUCT)lParam)->hMenu;

    i = GetWindowLong(m_hWnd, GWL_STYLE);
    m_iTabStop = TAB_STOP;

    m_iLeftMargin = 70;
    m_iTopMargin = m_iRightMargin = m_iBottomMargin = 1;
    m_iSpacing = 0;
    m_iLineMax = LINE_MAX;
    m_bWordwrap = (i & WS_HSCROLL) ? FALSE : TRUE;
    m_bInsertMode = TRUE;
    m_bLock = (i & ES_READONLY) ? TRUE : FALSE;
    m_bNoHideSel = (i & ES_NOHIDESEL) ? TRUE : FALSE;

    if (InitString() == FALSE) {
        return -1;
    }

    // バッファの初期化
    m_dwBufSize = RESERVE_BUF;
    m_pBuf = reinterpret_cast<TCHAR*>(MemAlloc(sizeof(TCHAR) * m_dwBufSize));

    if (m_pBuf  == NULL) 
    {
        return -1;
    }
    *m_pBuf = TEXT('\0');

    // 描画情報の初期化
    hdc = GetDC(m_hWnd);
    m_hDc = CreateCompatibleDC(hdc);
    ReleaseDC(m_hWnd, hdc);
    DrawInit();

    SetMapMode(m_hDc, MM_TEXT);
    SetTextCharacterExtra(m_hDc, 0);
    SetTextJustification(m_hDc, 0, 0);
    SetTextAlign(m_hDc, TA_TOP | TA_LEFT);
    SetBkMode(m_hDc, TRANSPARENT);

    // buffer info to window long
    //SetWindowLong(m_hWnd, GWL_USERDATA, (LPARAM)bf);
    SendMessage(m_hWnd, WM_REFLECT, 0, 0);

    // 初期文字列の設定
    if (((LPCREATESTRUCT)lParam)->lpszName != NULL) 
    {
        SendMessage(m_hWnd, WM_SETTEXT, 0, (LPARAM)((LPCREATESTRUCT)lParam)->lpszName);
    }
    return 0;
}

LRESULT CMtEditorEngine::OnDestroy(const WPARAM wParam, const LPARAM lParam)
{
    //SetWindowLong(m_hWnd, GWL_USERDATA, (LPARAM)0);

#ifdef OP_XP_STYLE
    // XP
    if (m_hTheme != NULL) {
        if (F_CloseThemeData == NULL) {
            F_CloseThemeData = reinterpret_cast<CLOSETHEMEDATA>(::GetProcAddress(m_hModThemes, "CloseThemeData"));
        }
        if (F_CloseThemeData != NULL) {
            F_CloseThemeData(m_hTheme);
        }
    }
    if (m_hModThemes != NULL) 
    {
        FreeLibrary(m_hModThemes);
    }
#endif	// OP_XP_STYLE
    DrawFree();
    if (m_hFont != NULL) {
        ::SelectObject(m_hDc, m_hRetFont);
        ::DeleteObject(m_hFont);
    }
    DeleteDC(m_hDc);
    MemFree(reinterpret_cast<void**>(&m_pBuf));
    MemFree(reinterpret_cast<void**>(&m_pInputBuf));
    UndoFree(0);
    MemFree(reinterpret_cast<void**>(&m_pUndo));

    if (GetFocus() == m_hWnd) 
    {
        DestroyCaret();
    }
    return DefWindowProc(m_hWnd, WM_DESTROY, wParam, lParam);
}

LRESULT CMtEditorEngine::OnSetFocus(const WPARAM wParam, const LPARAM lParam)
{
    CreateCaret(m_hWnd, NULL, 0, m_iFontHeight);
    ShowCaret(m_hWnd);
    LineRefresh( m_dwCaretPos, m_dwSelectPos);
    // 親ウィンドウに通知
    NotifyMessage(EN_SETFOCUS);
    return 0;
}

LRESULT CMtEditorEngine::OnKillFocus(const WPARAM wParam, const LPARAM lParam)
{
    HideCaret(m_hWnd);
    DestroyCaret();
    LineRefresh( m_dwCaretPos, m_dwSelectPos);
    // 親ウィンドウに通知
    NotifyMessage(EN_KILLFOCUS);
    return 0;
}

LRESULT CMtEditorEngine::OnSize(const WPARAM wParam, const LPARAM lParam)
{
    RECT rect;
    HIMC hIMC;
    POINT pt;
    COMPOSITIONFORM cf;

    if (wParam == SIZE_MINIMIZED) 
    {
        return 0;
    }

    int iWidth  = LOWORD(lParam);
    int iHeight = HIWORD(lParam);

    GetEditRect(&rect);
    if (m_bWordwrap == TRUE && 
        (rect.right - rect.left) > 0) 
    {
        // 行情報の再設定
        m_iWidth = (rect.right - rect.left);
        m_iLineLen = 0;
        LineSetInfo();
    }

    m_iHeight = rect.bottom - rect.top;
    SetScrollbar();

    DrawFree();
    DrawInit();

    InvalidateRect(m_hWnd, NULL, FALSE);

    // IMEの設定
    hIMC = ImmGetContext(m_hWnd);
    GetCaretPos(&pt);
    cf.dwStyle = CFS_POINT | CFS_RECT;
    cf.ptCurrentPos.x = pt.x;
    cf.ptCurrentPos.y = pt.y + (m_iSpacing / 2);
    GetEditRect(&cf.rcArea);
    ImmSetCompositionWindow(hIMC, &cf);
    ImmReleaseContext(m_hWnd, hIMC);
    return 0;
}

LRESULT CMtEditorEngine::OnHScroll(const WPARAM wParam, const LPARAM lParam)
{
    RECT rect;
    int i;

    GetEditRect(&rect);
    i = m_iScrollX;
    switch ((int)LOWORD(wParam)) 
    {
    case SB_TOP:
        m_iScrollX = 0;
        break;

    case SB_BOTTOM:
        m_iScrollX = m_iScrollMaxX;
        break;

    case SB_LINELEFT:
        m_iScrollX = (m_iScrollX > 0) ? m_iScrollX - 1 : 0;
        break;

    case SB_LINERIGHT:
        m_iScrollX = (m_iScrollX < m_iScrollMaxX) ? m_iScrollX + 1 : m_iScrollMaxX;
        break;

    case SB_PAGELEFT:
        m_iScrollX = (m_iScrollX - ((rect.right - rect.left) / m_iCharWidth) > 0) ?
            m_iScrollX - ((rect.right - rect.left) / m_iCharWidth) : 0;
        break;

    case SB_PAGERIGHT:
        m_iScrollX = (m_iScrollX + ((rect.right - rect.left) / m_iCharWidth) < m_iScrollMaxX) ?
            m_iScrollX + ((rect.right - rect.left) / m_iCharWidth) : m_iScrollMaxX;
        break;

    case SB_THUMBPOSITION:
    case SB_THUMBTRACK:
        {
            SCROLLINFO si;

            ZeroMemory(&si, sizeof(SCROLLINFO));
            si.cbSize = sizeof(SCROLLINFO);
            si.fMask = SIF_ALL;
            GetScrollInfo(m_hWnd, SB_HORZ, &si);
            m_iScrollX = si.nTrackPos;
        }
        break;
    }
    switch ((int)LOWORD(wParam)) {
    case SB_TOP:
    case SB_BOTTOM:
    case SB_LINELEFT:
    case SB_LINERIGHT:
    case SB_PAGELEFT:
    case SB_PAGERIGHT:
        if (i - m_iScrollX != 0) {
            // 親ウィンドウに通知
            NotifyMessage(EN_HSCROLL);
        }
        break;
    }

    SetScrollPos(m_hWnd, SB_HORZ, m_iScrollX, TRUE);
    ScrollWindowEx(m_hWnd, (i - m_iScrollX) * m_iCharWidth, 0, NULL, &rect, NULL, NULL, SW_INVALIDATE | SW_ERASE);
    return 0;
}

LRESULT CMtEditorEngine::OnVScroll(const WPARAM wParam, const LPARAM lParam)
{
    RECT rect;
    int i;

    GetEditRect(&rect);
    i = m_iScrollY;
    switch ((int)LOWORD(wParam)) {
    case SB_TOP:
        m_iScrollY = 0;
        break;

    case SB_BOTTOM:
        m_iScrollY = m_iScrollMaxY;
        break;

    case SB_LINEUP:
        m_iScrollY = (m_iScrollY > 0) ? m_iScrollY - 1 : 0;
        break;

    case SB_LINEDOWN:
        m_iScrollY = (m_iScrollY < m_iScrollMaxY) ? m_iScrollY + 1 : m_iScrollMaxY;
        break;

    case SB_PAGEUP:
        m_iScrollY = (m_iScrollY - (((rect.bottom - rect.top) / m_iFontHeight) - 1) > 0) ?
            m_iScrollY - (((rect.bottom - rect.top) / m_iFontHeight) - 1) : 0;
        break;

    case SB_PAGEDOWN:
        m_iScrollY = (m_iScrollY + (((rect.bottom - rect.top) / m_iFontHeight) - 1) < m_iScrollMaxY) ?
            m_iScrollY + (((rect.bottom - rect.top) / m_iFontHeight) - 1) : m_iScrollMaxY;
        break;

    case SB_THUMBPOSITION:
    case SB_THUMBTRACK:
        {
            SCROLLINFO si;

            ZeroMemory(&si, sizeof(SCROLLINFO));
            si.cbSize = sizeof(SCROLLINFO);
            si.fMask = SIF_ALL;
            GetScrollInfo(m_hWnd, SB_VERT, &si);
            m_iScrollY = si.nTrackPos;
        }
        break;
    }
    switch ((int)LOWORD(wParam)) {
    case SB_TOP:
    case SB_BOTTOM:
    case SB_LINEUP:
    case SB_LINEDOWN:
    case SB_PAGEUP:
    case SB_PAGEDOWN:
        if (i - m_iScrollY != 0) 
        {
            // 親ウィンドウに通知
            NotifyMessage(EN_VSCROLL);
        }
        break;
    }

    rect.left = 0; 
    SetScrollPos(m_hWnd, SB_VERT, m_iScrollY, TRUE);
    ScrollWindowEx(m_hWnd, 0, (i - m_iScrollY) * m_iFontHeight, 
        NULL, &rect, NULL, NULL, SW_INVALIDATE | SW_ERASE);
    return 0;
}

LRESULT CMtEditorEngine::OnKeyDown(const WPARAM wParam, const LPARAM lParam)
{
    switch (wParam) {
    case VK_APPS:
        SendMessage(m_hWnd, WM_CONTEXTMENU, 0, 0);
        break;

    case VK_INSERT:
        if (GetKeyState(VK_CONTROL) < 0) {
            // コピー
            SendMessage(m_hWnd, WM_COPY, 0, 0);
        } else if (GetKeyState(VK_SHIFT) < 0) {
            // 貼り付け
            SendMessage(m_hWnd, WM_PASTE, 0, 0);
        } else {
            // 入力モード切替
            m_bInsertMode = !m_bInsertMode;

            DestroyCaret();
            ::CreateCaret(m_hWnd, NULL, 0, m_iFontHeight);
            ShowCaret(m_hWnd);
            LineRefresh( m_dwCaretPos, m_dwCaretPos);
        }
        break;

    case VK_DELETE:
        if (m_bLock == TRUE) {
            break;
        }
        if (GetKeyState(VK_SHIFT) < 0) {
            // 切り取り
            SendMessage(m_hWnd, WM_CUT, 0, 0);
        } else {
            // 削除
            DeleteStringChar(m_dwCaretPos);
        }
        break;

    case VK_BACK:
        if (m_bLock == TRUE) {
            break;
        }
        if (m_dwCaretPos == m_dwSelectPos) {
            if (m_dwCaretPos <= 0) {
                break;
            }
            CaretMove(VK_LEFT);
        }
        DeleteStringChar(m_dwCaretPos);
        break;

    case 'C':
        if (GetKeyState(VK_CONTROL) < 0) {
            SendMessage(m_hWnd, WM_COPY, 0, 0);
        }
        break;

    case 'X':
        if (GetKeyState(VK_CONTROL) < 0) {
            SendMessage(m_hWnd, WM_CUT, 0, 0);
        }
        break;

    case 'V':
        if (GetKeyState(VK_CONTROL) < 0) {
            SendMessage(m_hWnd, WM_PASTE, 0, 0);
        }
        break;

    case 'Z':
        if (GetKeyState(VK_CONTROL) < 0 && GetKeyState(VK_SHIFT) < 0) {
            // やり直し
            SendMessage(m_hWnd, EM_REDO, 0, 0);
        } else if (GetKeyState(VK_CONTROL) < 0) {
            // 元に戻す
            SendMessage(m_hWnd, EM_UNDO, 0, 0);
        }
        break;

    case 'Y':
        if (GetKeyState(VK_CONTROL) < 0) {
            SendMessage(m_hWnd, EM_REDO, 0, 0);
        }
        break;

    case VK_HOME:
    case VK_END:
    case VK_PRIOR:
    case VK_NEXT:
    case VK_UP:
    case VK_DOWN:
    case VK_LEFT:
    case VK_RIGHT:
        FlushString(TRUE);
        CaretMove(wParam);
        break;
    }
    return 0;
}

LRESULT CMtEditorEngine::OnChar(const WPARAM wParam, const LPARAM lParam)
{
    TCHAR in[3];

    if (GetKeyState(VK_CONTROL) < 0) 
    {
        return 0;
    }
    switch (wParam) {
    case VK_RETURN:
        // 改行
        InsertString(TEXT("\r\n"), 2, TRUE);
        FlushString(TRUE);
        break;

    case VK_BACK:
        break;

    case VK_ESCAPE:
        break;

    default:
        in[0] = wParam;
        InsertString(in, 1, m_bInsertMode);
        break;
    }
    return 0;
}

LRESULT CMtEditorEngine::OnImeChar(const WPARAM wParam, const LPARAM lParam)
{
    TCHAR in[3];
    if (GetKeyState(VK_CONTROL) < 0) 
    {
        return 0;
    }
#ifdef UNICODE
    in[0] = wParam;
    InsertString(in, 1, m_bInsertMode);
#else
    in[0] = wParam >> 8;
    in[1] = wParam & 0xFF;
    InsertString(in, 2, m_bInsertMode);
#endif
    return 0;
}

LRESULT CMtEditorEngine::OnImeStartComposition (const WPARAM wParam, const LPARAM lParam)
{
    HIMC hIMC;
    POINT pt;			
    LOGFONT lf;
    COMPOSITIONFORM cf;

    EnsureVisible();

    hIMC = ImmGetContext(m_hWnd);
    if (m_hFont != NULL) 
    {
        // フォントの設定
        GetObject(m_hFont, sizeof(LOGFONT), &lf);
        ImmSetCompositionFont(hIMC, &lf);
    }
    // 位置の設定
    GetCaretPos(&pt);
    cf.dwStyle = CFS_POINT | CFS_RECT;
    cf.ptCurrentPos.x = pt.x;
    cf.ptCurrentPos.y = pt.y + (m_iSpacing / 2);
    GetEditRect(&cf.rcArea);
    ImmSetCompositionWindow(hIMC, &cf);
    ImmReleaseContext(m_hWnd, hIMC);

    HideCaret(m_hWnd);
    return DefWindowProc(m_hWnd, WM_IME_STARTCOMPOSITION, wParam, lParam);
}

LRESULT CMtEditorEngine::OnImeComposition(const WPARAM wParam, const LPARAM lParam)
{
    POINT pt;
    int len;
    HIMC hIMC;
    COMPOSITIONFORM cf;

    if (m_bLock == FALSE && 
        (lParam & GCS_RESULTSTR)) 
    {
        TCHAR *buf;

        // 確定文字列をバッファに追加
        hIMC = ImmGetContext(m_hWnd);
        len = ImmGetCompositionString(hIMC, GCS_RESULTSTR, NULL, 0);
        buf = reinterpret_cast<TCHAR*>(MemCalloc(len + sizeof(TCHAR)));
        if (buf != NULL) {
            ImmGetCompositionString(hIMC, GCS_RESULTSTR, buf, len);
            InsertString(buf, len / sizeof(TCHAR), m_bInsertMode);
            UpdateWindow(m_hWnd);
            MemFree(reinterpret_cast<void**>(&buf));
        }
        // 位置の設定
        GetCaretPos(&pt);
        cf.dwStyle = CFS_POINT | CFS_RECT;
        cf.ptCurrentPos.x = pt.x;
        cf.ptCurrentPos.y = pt.y + (m_iSpacing / 2);
        GetEditRect(&cf.rcArea);
        ImmSetCompositionWindow(hIMC, &cf);
        ImmReleaseContext(m_hWnd, hIMC);
DB_PRINT(_T("Fix"));
        return DefWindowProc( m_hWnd, WM_IME_COMPOSITION, wParam, lParam );;
    }
    return DefWindowProc(m_hWnd, WM_IME_COMPOSITION, wParam, lParam);
}

LRESULT CMtEditorEngine::OnMouseMove(const WPARAM wParam, const LPARAM lParam)
{
    DWORD cp, sp;

    int iX = LOWORD(lParam);
    int iY = HIWORD(lParam);

    cp = m_dwCaretPos;
    sp = m_dwSelectPos;


    if (iX < m_iIndicatorWidth)
    {
        SetCursor(LoadCursor(0, IDC_ARROW));
    }
    else
    {
        SetCursor(LoadCursor(0, IDC_IBEAM));
    }


    if (!(wParam & MK_LBUTTON) 
        || m_bMousedown == FALSE) 
    {
        return 0;
    }

    SetFocus(m_hWnd);
    m_dwCaretPos = CaretPointToCaret(iX, iY);
    if (sp != m_dwSelectPos) 
    {
        LineRefresh( cp, sp);
    }
    LineRefresh( cp, m_dwCaretPos);
    return 0;
}

LRESULT CMtEditorEngine::OnLButtonDown(const WPARAM wParam, const LPARAM lParam)
{
    DWORD cp, sp;
    cp = m_dwCaretPos;
    sp = m_dwSelectPos;
    SetCursor(LoadCursor(0, IDC_IBEAM));

    int iX = LOWORD(lParam);
    int iY = HIWORD(lParam);


    FlushString(TRUE);
    SetCapture(m_hWnd);
    m_bMousedown = TRUE;
    m_iCaretPosX = 0;

    SetFocus(m_hWnd);

    m_dwCaretPos = CaretPointToCaret((short)LOWORD(lParam), (short)HIWORD(lParam));

    m_iClickBreakPoint = -1;
    if (iX < m_iIndicatorWidth)
    {  
        int iLine;
        iLine = IndexToLine( m_dwCaretPos);
        m_iClickBreakPoint = iLine;
    }

    EnsureVisible();
    if (GetKeyState(VK_SHIFT) >= 0) 
    {
        m_dwSelectPos = m_dwCaretPos;
    }

    if (sp != m_dwSelectPos) 
    {
        LineRefresh( cp, sp);
    }

    LineRefresh( cp, m_dwCaretPos);
    return 0;
}

LRESULT CMtEditorEngine::OnLButtonUp(const WPARAM wParam, const LPARAM lParam)
{
    DWORD cp, sp;

    cp = m_dwCaretPos;
    sp = m_dwSelectPos;
    SetCursor(LoadCursor(0, IDC_IBEAM));
    if (m_bMousedown == FALSE) 
    {
        return 0;
    }
    ReleaseCapture();
    m_bMousedown = FALSE;
    m_iCaretPosX = 0;

    SetFocus(m_hWnd);

    int iX = LOWORD(lParam);
    int iY = HIWORD(lParam);

    DWORD dwCaretPos;
    dwCaretPos = CaretPointToCaret((short)LOWORD(lParam), (short)HIWORD(lParam));

    if ((iX < m_iIndicatorWidth)&& 
        (m_iClickBreakPoint >= 0))
    {  
        int iLine;
        int iLogcalLineHead;
        bool bSet;
        iLine = IndexToLine(dwCaretPos);
        iLogcalLineHead = GetLogicalLineHead(iLine);

        if(m_lstLine[iLogcalLineHead].iBreakPoint >= SET)
        {
            bSet = false;
            m_lstLine[iLogcalLineHead].iBreakPoint = UNSET;
        }
        else
        {
            bSet = true;
            m_lstLine[iLogcalLineHead].iBreakPoint = SET;
        }
        m_iClickBreakPoint = -1;

        if(m_pSetBreakPointModified)
        {
            m_pSetBreakPointModified(m_pParent, iLogcalLineHead, bSet);
        }
        DrawBreakPoint(iLine, TRUE);
    }
    else
    {
        m_dwCaretPos = dwCaretPos;
        EnsureVisible();
    }


    if (sp != m_dwSelectPos) 
    {
        LineRefresh( cp, sp);
    }
    LineRefresh( cp, m_dwCaretPos);

    return 0;
}

LRESULT CMtEditorEngine::OnRButtonDown(const WPARAM wParam, const LPARAM lParam)
{
    DWORD cp;
    FlushString(TRUE);
    SetFocus(m_hWnd);
    m_iCaretPosX = 0;

    cp = CaretPointToCaret((short)LOWORD(lParam), (short)HIWORD(lParam));
    if (!(m_dwCaretPos >= cp && 
        m_dwSelectPos <= cp || 
        m_dwSelectPos >= cp && 
        m_dwCaretPos <= cp)) 
    {
        m_dwCaretPos = m_dwSelectPos = cp;
        EnsureVisible();
        InvalidateRect(m_hWnd, NULL, FALSE);
    }
    return 0;
}

LRESULT CMtEditorEngine::OnLButtonDblClick(const WPARAM wParam, const LPARAM lParam)
{
    DWORD cp;
    DWORD sp;

    if (!(wParam & MK_LBUTTON)) 
    {
        return 0;
    }

    FlushString(TRUE);
    SetFocus(m_hWnd);
    m_iCaretPosX = 0;
    cp = m_dwCaretPos;
    m_dwSelectPos = m_dwCaretPos;
    sp = CaretPointToCaret((short)LOWORD(lParam), (short)HIWORD(lParam));
    // 文字列選択
    CaretGetToken();
    EnsureVisible();
    LineRefresh( cp, sp);
    LineRefresh( m_dwCaretPos, m_dwSelectPos);

    return 0;
}

LRESULT CMtEditorEngine::OnMouseWheel(const WPARAM wParam, const LPARAM lParam)
{
    for (int i = 0; i < 3; i++) {
        SendMessage(m_hWnd, WM_VSCROLL, ((short)HIWORD(wParam) > 0) ? SB_LINEUP : SB_LINEDOWN, 0);
    }
    return 0;
}

LRESULT CMtEditorEngine::OnPaint(const WPARAM wParam, const LPARAM lParam)
{
    PAINTSTRUCT ps;
    HBRUSH hBrush;
    HBRUSH brIndicator;
    RECT rect;
    RECT rcIndicator;
    HDC hdc;

    hdc = ::BeginPaint(m_hWnd, &ps);
   
    ::GetClientRect(m_hWnd, &rect);
    ::SetRect(&rect, ps.rcPaint.left, 0, ps.rcPaint.right, m_iFontHeight);
    ::SetRect(&rcIndicator, 0, 0, m_iIndicatorWidth, m_iFontHeight);

    LONG lStyle = GetWindowLong(m_hWnd, GWL_STYLE);
    m_bNoHideSel = (lStyle & ES_NOHIDESEL) ? TRUE : FALSE;

    // caret pos

    int iCaretLine = -1;    //キャレットが存在する行
    if (GetFocus() == m_hWnd) 
    {
        iCaretLine = IndexToLine(m_dwCaretPos);
    }

    m_bSel = FALSE;
    ::SetTextColor(m_hDc, GetSysColor(COLOR_WINDOWTEXT));
    ::SetBkColor(m_hDc, GetSysColor(COLOR_WINDOW));

    hBrush = ::CreateSolidBrush(GetSysColor(COLOR_WINDOW));
    brIndicator = ::CreateSolidBrush(m_crIndicator);

    int iTopLine     = m_iScrollY + (ps.rcPaint.top / m_iFontHeight) - 1;
    int iBottomLine  = m_iScrollY + (ps.rcPaint.bottom / m_iFontHeight) + 1;

    if (iTopLine < 0)    {iTopLine = 0;}
    if (iBottomLine < 0) {iBottomLine = 0;}

    int iLinePos = iTopLine;

    int iCaretY;    //キャレットY座標
    int iCaretX;    //キャレットX座標

    int iLineY = 0;

    for (;iLinePos < iBottomLine; iLinePos++) 
    {
        if (iLinePos >= m_iLineLen)
        {
            break;
        }

        if (iCaretLine >= 0)
        {
            if (iLinePos == iCaretLine) 
            {
                
                // set caret
                if (m_bInsertMode == FALSE) 
                {
                    CaretSetSize();
                }

                iCaretX = CaretCharToCaret(m_hDc, iCaretLine, m_dwCaretPos);
                iCaretY = (iCaretLine - m_iScrollY) * m_iFontHeight + m_iTopMargin;
                ::SetCaretPos(iCaretX, iCaretY);
                iCaretLine = -1;
            }
        }

        // 1行描画
        
        ::FillRect(m_hDc, &rect, hBrush);
        ::FillRect(m_hDc, &rcIndicator, brIndicator);
        iLineY = (iLinePos - m_iScrollY) * m_iFontHeight + m_iTopMargin;

        DrawLine(m_hDc, iLinePos, ps.rcPaint.left, ps.rcPaint.right);
        ::BitBlt(hdc,
            ps.rcPaint.left, iLineY,
            ps.rcPaint.right, m_iFontHeight,
            m_hDc, ps.rcPaint.left, 0, SRCCOPY);
    }

    iLineY = (iLinePos - m_iScrollY) * m_iFontHeight + m_iTopMargin;
    if (iLineY < ps.rcPaint.bottom) 
    {
        // 文末の余白描画
        ::SetRect(&rect,
            ps.rcPaint.left, iLineY,
            ps.rcPaint.right, ps.rcPaint.bottom);
        ::FillRect(hdc, &rect, hBrush);
        ::SetRect(&rcIndicator, 0, iLineY, 
            m_iIndicatorWidth, ps.rcPaint.bottom);
        ::FillRect(hdc, &rcIndicator, brIndicator);
    }
    // 矩形描画
    //::FillRgn(hdc, m_hRgn, hBrush);
    ::DeleteObject(hBrush);
    ::DeleteObject(brIndicator);

    if (iCaretLine != -1 && 
        GetFocus() == m_hWnd) 
    {
        iCaretX = CaretCharToCaret(m_hDc, iCaretLine, m_dwCaretPos);
        iCaretY = (iCaretLine - m_iScrollY) * m_iFontHeight + m_iTopMargin;
        ::SetCaretPos(iCaretX, iCaretY);
    }

    ::EndPaint(m_hWnd, &ps);
    return 0;
}

LRESULT CMtEditorEngine::OnNcPaint(const WPARAM wParam, const LPARAM lParam)
{
#ifdef OP_XP_STYLE
    HRGN hrgn;
    DWORD stats;
    RECT clip_rect;
    HDC hdc;
    RECT rect;

    if (m_hTheme == NULL) 
    {
        return DefWindowProc(m_hWnd, WM_NCPAINT, wParam, lParam);
    }

    // XP用の背景描画
    if (F_DrawThemeBackground == NULL) 
    {
        F_DrawThemeBackground = reinterpret_cast<DRAWTHEMEBACKGROUND>(GetProcAddress(m_hModThemes, "DrawThemeBackground"));
    }

    if (F_DrawThemeBackground == NULL) 
    {
        return DefWindowProc(m_hWnd, WM_NCPAINT, wParam, lParam);
    }

    // 状態の設定
    if (IsWindowEnabled(m_hWnd) == 0) 
    {
        stats = ETS_DISABLED;
    }
    else if (GetFocus() == m_hWnd) 
    {
        stats = ETS_FOCUSED;
    } 
    else 
    {
        stats = ETS_NORMAL;
    }

    // ウィンドウ枠の描画
    hdc = GetDCEx(m_hWnd, (HRGN)wParam, DCX_WINDOW | DCX_INTERSECTRGN);
    if (hdc == NULL) 
    {
        hdc = GetWindowDC(m_hWnd);
    }
    GetWindowRect(m_hWnd, &rect);
    OffsetRect(&rect, -rect.left, -rect.top);
    ExcludeClipRect(hdc, rect.left + GetSystemMetrics(SM_CXEDGE), rect.top + GetSystemMetrics(SM_CYEDGE),
        rect.right - GetSystemMetrics(SM_CXEDGE), rect.bottom - GetSystemMetrics(SM_CYEDGE));
    clip_rect = rect;
    F_DrawThemeBackground(m_hTheme, hdc, EP_EDITTEXT, stats, &rect, &clip_rect);
    ReleaseDC(m_hWnd, hdc);

    // スクロールバーの描画
    GetWindowRect(m_hWnd, (LPRECT)&rect);
    hrgn = CreateRectRgn(rect.left + GetSystemMetrics(SM_CXEDGE), rect.top + GetSystemMetrics(SM_CYEDGE),
        rect.right - GetSystemMetrics(SM_CXEDGE), rect.bottom - GetSystemMetrics(SM_CYEDGE));
    CombineRgn(hrgn, hrgn, (HRGN)wParam, RGN_AND);
    DefWindowProc(m_hWnd, WM_NCPAINT, (WPARAM)hrgn, 0);
    DeleteObject(hrgn);
#endif	// OP_XP_STYLE
    return 0;
}

LRESULT CMtEditorEngine::OnTheeChanged(const WPARAM wParam, const LPARAM lParam)
{
#ifdef OP_XP_STYLE
    if (m_hModThemes == NULL) 
    {
        return 0;
    }
    // XPテーマの変更
    if (m_hTheme != NULL) 
    {
        if (F_CloseThemeData == NULL)
        {
            F_CloseThemeData = reinterpret_cast<CLOSETHEMEDATA>(::GetProcAddress(m_hModThemes, "CloseThemeData"));
        }

        if (F_CloseThemeData != NULL) 
        {
            F_CloseThemeData(m_hTheme);
        }
        m_hTheme = NULL;
    }

    if (F_OpenThemeData == NULL) 
    {
        F_OpenThemeData = reinterpret_cast<OPENTHEMEDATA>(::GetProcAddress(m_hModThemes, "OpenThemeData"));
    }

    if (F_OpenThemeData != NULL)
    {
        m_hTheme = F_OpenThemeData(m_hWnd, L"Edit");
    }
#endif	// OP_XP_STYLE
    return 0;
}
/*
LRESULT CMtEditorEngine::OnGetText(const WPARAM wParam, const LPARAM lParam)
{
    // テキストを取得
    return GetString((TCHAR *)lParam, wParam);
}
*/

//!< 文字列の取得
int CMtEditorEngine::GetString(TCHAR *str, const DWORD len)
{
    FlushString(TRUE);
    lstrcpyn(str, m_pBuf, len);
    return lstrlen(str);
}
/*
LRESULT CMtEditorEngine::OnSetFont(const WPARAM hFont, const LPARAM fRedraw)
{
    SetFont(static_cast<HFONT>(hFont));
    return 0;
}
*/

//!< フォント設定
void CMtEditorEngine::SetFont(const HFONT hFont)
{
   // フォントを設定
    LOGFONT lf;

    if (m_hFont != NULL) 
    {
        SelectObject(m_hDc, m_hRetFont);
        DeleteObject(m_hFont);
    }

    ZeroMemory(m_cWidth, sizeof(BYTE) * 256);

    if (GetObject((HGDIOBJ)hFont, sizeof(LOGFONT), &lf) == 0) 
    {
        return;
    }

    m_hFont = CreateFontIndirect((CONST LOGFONT *)&lf);
    m_hRetFont = static_cast<HFONT>(SelectObject(m_hDc, m_hFont));

    SendMessage(m_hWnd, WM_REFLECT, 0, 0);
    DrawFree();
    DrawInit();
    return;
}

/**
 *  @brief   選択中テキスト消去
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CMtEditorEngine::Clear()
{
    if (m_dwCaretPos != m_dwSelectPos) 
    {
        DeleteStringChar(m_dwCaretPos);
    }
}

/**
 *  @brief   選択中テキストコピー
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CMtEditorEngine::Copy()
{
    // コピー
    FlushString(TRUE);
    if (m_dwCaretPos == m_dwSelectPos) 
    {
        return;
    }

    if (m_dwSelectPos > m_dwCaretPos) 
    {
        StringToClipboard(IndexToChar(m_dwCaretPos), 
                          IndexToChar(m_dwSelectPos));
    }
    else 
    {
        StringToClipboard(IndexToChar(m_dwSelectPos), 
                          IndexToChar(m_dwCaretPos));
    }
}

/**
 *  @brief   選択中テキストカット
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CMtEditorEngine::Cut()
{
    Copy();
    Clear();
}

/**
 *  @brief   貼り付け
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CMtEditorEngine::Paste()
{

#ifdef UNICODE
    if (IsClipboardFormatAvailable(CF_UNICODETEXT) == 0) 
    {
#else
    if (IsClipboardFormatAvailable(CF_TEXT) == 0) 
    {
#endif
        return;
    }

    if (::OpenClipboard(m_hWnd) != 0) 
    {
        HANDLE hclip;
        TCHAR *p;
        HCURSOR old_cursor;

        old_cursor = ::SetCursor(LoadCursor(NULL, IDC_WAIT));
#ifdef UNICODE
        hclip = GetClipboardData(CF_UNICODETEXT);
#else
        hclip = GetClipboardData(CF_TEXT);
#endif
        if ((p = reinterpret_cast<TCHAR*>(GlobalLock(hclip))) != NULL) {
            InsertString(p, lstrlen(p), TRUE);
            GlobalUnlock(hclip);
        }
        CloseClipboard();
        SetCursor(old_cursor);
    }
}

LRESULT CMtEditorEngine::OnCanUndo(const WPARAM wParam, const LPARAM lParam)
{
    return ((m_iUndoLen > 0 || m_pInputPos != NULL || m_pDeletePos != NULL) ? TRUE : FALSE);
}

LRESULT CMtEditorEngine::OnCanRedo(const WPARAM wParam, const LPARAM lParam)
{
    // REDO可能か取得
    return (((m_pUndo + m_iUndoLen)->type != 0) ? TRUE : FALSE);
}

LRESULT CMtEditorEngine::OnEmptyUndoBuffer(const WPARAM wParam, const LPARAM lParam)
{
    UndoFree(0);
    MemFree(reinterpret_cast<void**>(&m_pUndo));

    m_iUndoSize = RESERVE_UNDO;
    m_pUndo = reinterpret_cast<UNDO*>(MemCalloc(sizeof(UNDO) * m_iUndoSize));

    if (m_pUndo == NULL)
    {
        return FALSE;
    }
    m_iUndoLen = 0;
    return 0;
}

LRESULT CMtEditorEngine::OnGetFirstVisibleLine(const WPARAM wParam, const LPARAM lParam)
{
    // 一番上に表示されている行番号を取得
    return m_iScrollY;
}


LRESULT CMtEditorEngine::OnGetLine(const WPARAM wParam, const LPARAM lParam)
{
    // 1行の文字列を取得
    if ((wParam < 0) || 
        (wParam >= (WPARAM)m_iLineLen)) 
    {
        return 0;
    }

    CopyMemory((TCHAR *)lParam,
        IndexToChar(LineGet(wParam)),
        sizeof(TCHAR) * LineGetLength(LineGet(wParam)));
    return LineGetLength(LineGet(wParam));
}

/**
 *  @brief   行数取得
 *  @param   なし
 *  @retval  行数
 *  @note    論理行数ではない
 */
int CMtEditorEngine::GetDispLineCount()
{
    return m_iLineLen;
}

LRESULT CMtEditorEngine::OnGetModify(const WPARAM wParam, const LPARAM lParam)
{
    // 変更フラグを取得
    return m_bModified;
}

LRESULT CMtEditorEngine::OnGetRect(const WPARAM wParam, const LPARAM lParam)
{
    // 描画領域の取得
    if (lParam == 0) 
    {
        return 0;
    }
    GetEditRect((RECT *)lParam);
    return 0;
}

LRESULT CMtEditorEngine::OnGetSel(const WPARAM wParam, const LPARAM lParam)
{
    int i, j;
    // 選択されているインデックスを取得
    i = ((m_dwSelectPos < m_dwCaretPos) ? m_dwSelectPos : m_dwCaretPos);
    j = ((m_dwSelectPos < m_dwCaretPos) ? m_dwCaretPos : m_dwSelectPos);
    if (wParam != 0) 
    {
        *((LPDWORD)wParam) = i;
    }
    if (lParam != 0) 
    {
        *((LPDWORD)lParam) = j;
    }
    return MAKELPARAM(i, j);
}


LRESULT CMtEditorEngine::OnLimitText(const WPARAM wParam, const LPARAM lParam)
{
    //入力文字数制限は不要
    // 入力文字数制限
    //m_dwLimitLen = wParam;
    //if (m_dwLimitLen < 0) 
    //{
        m_dwLimitLen = 0;
    //}
    return 0;
}

LRESULT CMtEditorEngine::OnLineFromChar          (const WPARAM wParam, const LPARAM lParam)
{
    // 特定文字インデックスが含まれる行番号を取得
    if (wParam == -1) 
    {
        if (m_dwSelectPos < m_dwCaretPos) 
        {
            return IndexToLine(m_dwSelectPos);
        }
        else 
        {
            return IndexToLine(m_dwCaretPos);
        }
    }
    return IndexToLine(wParam);
}

LRESULT CMtEditorEngine::OnLineIndex             (const WPARAM wParam, const LPARAM lParam)
{
    // 行番号の文字インデックスを取得
    if (wParam == -1) 
    {
        return LineGet(IndexToLine(m_dwCaretPos));
    }

    if (wParam < 0 || wParam >= (WPARAM)m_iLineLen) 
    {
        return -1;
    }
    return LineGet(wParam);
}

LRESULT CMtEditorEngine::OnLineLength            (const WPARAM wParam, const LPARAM lParam)
{
    int i, j;
    if (wParam == -1) 
    {
        if (m_dwSelectPos == m_dwCaretPos) 
        {
            return LineGetLength(m_dwCaretPos);
        }
        i = ((m_dwSelectPos < m_dwCaretPos) ? m_dwSelectPos : m_dwCaretPos);
        i = i - LineGet(IndexToLine(i));
        j = ((m_dwSelectPos < m_dwCaretPos) ? m_dwCaretPos : m_dwSelectPos);
        j = LineGetLength(j) - (j - LineGet(IndexToLine(j)));
        return (i + j);
    }
    return LineGetLength(wParam);
}

LRESULT CMtEditorEngine::OnLineScroll            (const WPARAM wParam, const LPARAM lParam)
{
    RECT rect;
    int i;

    GetEditRect(&rect);
    if (wParam != 0) 
    {
        i = wParam;
        
        if (m_iScrollX + i < 0) 
        {
            i = m_iScrollX * -1;
        }
        
        if (m_iScrollX + i > m_iScrollMaxX) 
        {
            i = m_iScrollMaxX - m_iScrollX;
        }
        ::SetScrollPos(m_hWnd, SB_HORZ, m_iScrollX + i, TRUE);
        ::ScrollWindowEx(m_hWnd, -((int)i * m_iCharWidth), 
            0, NULL, &rect, NULL, NULL, SW_INVALIDATE | SW_ERASE);
        m_iScrollX += i;
    }

    if (lParam != 0) 
    {
        i = lParam;
        if (m_iScrollY + i < 0) 
        {
            i = m_iScrollY * -1;
        }

        if (m_iScrollY + i > m_iScrollMaxY) 
        {
            i = m_iScrollMaxY - m_iScrollY;
        }

        ::SetScrollPos(m_hWnd, SB_VERT, m_iScrollY + i, TRUE);
        ::ScrollWindowEx(m_hWnd, 0, 
            -((int)i * m_iFontHeight),
            NULL, &rect, NULL, NULL, SW_INVALIDATE | SW_ERASE);
        m_iScrollY += i;
    }
    return 0;
}

LRESULT CMtEditorEngine::OnReplaceSel            (const WPARAM wParam, const LPARAM lParam)
{
    // 選択範囲のテキストを置き換える
    InsertString((TCHAR *)lParam, lstrlen((TCHAR *)lParam), TRUE);
    return 0;
}

LRESULT CMtEditorEngine::OnScroll                (const WPARAM wParam, const LPARAM lParam)
{
    RECT rect;
    int i;
    // テキストを垂直にスクロール
    GetEditRect(&rect);
    switch (wParam) 
    {
    case SB_LINEUP:
        i = (m_iScrollY > 0) ? -1 : 0;
        break;

    case SB_LINEDOWN:
        i = (m_iScrollY < m_iScrollMaxY) ? 1 : 0;
        break;

    case SB_PAGEUP:
        i = (m_iScrollY - (((rect.bottom - rect.top) / m_iFontHeight) - 1) > 0) ?
            -(((rect.bottom - rect.top) / m_iFontHeight) - 1) : m_iScrollY;
        break;

    case SB_PAGEDOWN:
        i = (m_iScrollY + (((rect.bottom - rect.top) / m_iFontHeight) - 1) < m_iScrollMaxY) ?
            (((rect.bottom - rect.top) / m_iFontHeight) - 1) : m_iScrollMaxY - m_iScrollY;
        break;

    default:
        return FALSE;
    }

    if (i == 0) 
    {
        return 0;
    }

    SendMessage(m_hWnd, WM_VSCROLL, wParam, 0);
    return MAKELRESULT(i, TRUE);
}


LRESULT CMtEditorEngine::OnScrollCaret(const WPARAM wParam, const LPARAM lParam)
{
    // キャレット位置にスクロール
    EnsureVisible();
    InvalidateRect(m_hWnd, NULL, FALSE);
    return !0;
}

LRESULT CMtEditorEngine::OnSetModify(const WPARAM wParam, const LPARAM lParam)
{
    // 変更フラグをセット
    m_bModified = wParam;
    return 0;
}

LRESULT CMtEditorEngine::OnGetReadOnly(const WPARAM wParam, const LPARAM lParam)
{
    // 読み取り専用フラグの取得
    return m_bLock;
}

LRESULT CMtEditorEngine::OnSetReadOnly(const WPARAM wParam, const LPARAM lParam)
{
    // 読み取り専用の設定
    m_bLock = wParam;
    if (m_bLock == TRUE) 
    {
        ::SetWindowLong(m_hWnd, GWL_STYLE, GetWindowLong(m_hWnd, GWL_STYLE) | ES_READONLY);
        if (m_hImc == (HIMC)NULL) 
        {
            m_hImc = ImmAssociateContext(m_hWnd, (HIMC)NULL);
        }
    } 
    else if (m_bLock == FALSE) 
    {
        SetWindowLong(m_hWnd, GWL_STYLE, GetWindowLong(m_hWnd, GWL_STYLE) & ~ES_READONLY);
        if (m_hImc != (HIMC)NULL) 
        {
            ImmAssociateContext(m_hWnd, m_hImc);
            m_hImc = (HIMC)NULL;
        }
    }
    return !0;
}

LRESULT CMtEditorEngine::OnSetRect(const WPARAM wParam, const LPARAM lParam)
{
    // 描画領域の設定 (再描画あり)
    ::SendMessage(m_hWnd, EM_SETRECTNP, wParam, lParam);
    ::InvalidateRect(m_hWnd, NULL, FALSE);
    return 0;
}

LRESULT CMtEditorEngine::OnSetRectNp(const WPARAM wParam, const LPARAM lParam)
{
    RECT rect;
    // 描画領域の設定 (再描画なし)
    GetClientRect(m_hWnd, &rect);
    m_iLeftMargin = ((RECT *)lParam)->left + 1;
    m_iTopMargin = ((RECT *)lParam)->top + 1;
    m_iRightMargin = rect.right - ((RECT *)lParam)->right + 1;
    m_iBottomMargin = rect.bottom - ((RECT *)lParam)->bottom + 1;
    DrawFree();
    DrawInit();
    ::SendMessage(m_hWnd, WM_REFLECT, 1, 0);
    return 0;
}

LRESULT CMtEditorEngine::OnSetSel(const WPARAM wParam, const LPARAM lParam)
{
    int i, j;
    DWORD cp, sp;

    // 選択インデックスの設定
    FlushString(TRUE);

    i = m_dwSelectPos;
    j = m_dwCaretPos;

    sp = ((DWORD)wParam < (DWORD)lParam) ? wParam : lParam;
    cp = ((DWORD)wParam < (DWORD)lParam) ? lParam : wParam;
    if (sp < 0) {
        sp = 0;
    }

    if (sp > m_dwBufLen) 
    {
        sp = m_dwBufLen;
    }
    
    if (cp < 0 || cp > m_dwBufLen) 
    {
        cp = m_dwBufLen;
    }
    m_dwSelectPos = sp; m_dwCaretPos = cp;
    m_iCaretPosX = 0;
    LineRefresh( i, j);
    LineRefresh( m_dwCaretPos, m_dwSelectPos);
    return 0;
}

LRESULT CMtEditorEngine::OnSetAbsTops(const WPARAM wParam, const LPARAM lParam)
{
    // タブストップの設定
    if (wParam < 0 || lParam == 0) {
        return FALSE;
    } else if (wParam == 0) {
        m_iTabStop = TAB_STOP;
    } else {
        m_iTabStop = *((LPDWORD)lParam) / 4;
    }
    ::SendMessage(m_hWnd, WM_REFLECT, 0, 0);
    return TRUE;
}


LRESULT CMtEditorEngine::OnGetBufferInfo         (const WPARAM wParam, const LPARAM lParam)
{
    //ユーザー定義メッセージ
    //TODO:使用しない
    return 0;
}

LRESULT CMtEditorEngine::OnReflect(const WPARAM wParam, const LPARAM lParam)
{
    //ユーザー定義メッセージ
    // 描画情報の更新
    TEXTMETRIC tm;
    HCURSOR old_cursor;
    RECT rect;

    old_cursor = SetCursor(LoadCursor(NULL, IDC_WAIT));

    // フォント情報の取得
    ::GetTextMetrics(m_hDc, &tm);
    m_iFontHeight = tm.tmHeight + m_iSpacing;
    m_iCharWidth = tm.tmAveCharWidth;
    if (::GetFocus() == m_hWnd) 
    {
        ::DestroyCaret();
        ::CreateCaret(m_hWnd, NULL, 0, m_iFontHeight);
        ::ShowCaret(m_hWnd);
    }
    GetEditRect(&rect);
    m_iWidth  = rect.right - rect.left;
    m_iHeight = rect.bottom - rect.top;
    m_iLineWidth = 0;
    m_iLineLen = 0;
    LineSetInfo();
    SetScrollbar();
    if (wParam == 0) 
    {
        ::InvalidateRect(m_hWnd, NULL, FALSE);
    }
    ::SetCursor(old_cursor);
    return 0;
}


LRESULT CMtEditorEngine::OnGetWordwrap(const WPARAM wParam, const LPARAM lParam)
{
    //ユーザー定義メッセージ
    int i;
    // 折り返しフラグの取得
    i = GetWindowLong(m_hWnd, GWL_STYLE);
    return ((i & WS_HSCROLL) ? FALSE : TRUE);
}

LRESULT CMtEditorEngine::OnSetWordwrap(const WPARAM wParam, const LPARAM lParam)
{
    //ユーザー定義メッセージ
    // 折り返し設定
    int i;
    m_bWordwrap = wParam;
    i = m_iScrollY;
    SetScrollbar();
    m_iScrollY = i;
    ::SendMessage(m_hWnd, WM_REFLECT, 0, 0);
    return 0;
}

LRESULT CMtEditorEngine::OnGetMemSize(const WPARAM wParam, const LPARAM lParam)
{
    //ユーザー定義メッセージ
    // 内部メモリのサイズ取得
    return (GetBufferLength() * sizeof(TCHAR));
}

LRESULT CMtEditorEngine::OnSetMem(const WPARAM wParam, const LPARAM lParam)
{
    //ユーザー定義メッセージ
    // メモリを内部メモリに設定
    return SetString((TCHAR *)lParam, wParam / sizeof(TCHAR));
}

LRESULT CMtEditorEngine::OnGetMem(const WPARAM wParam, const LPARAM lParam)
{
    //ユーザー定義メッセージ
    // 内部メモリの取得
    FlushString(TRUE);
    CopyMemory((TCHAR *)lParam, m_pBuf, (GetBufferLength() * sizeof(TCHAR)));
    return (GetBufferLength() * sizeof(TCHAR));
}

LRESULT CMtEditorEngine::OnContextMenu(const WPARAM wParam, const LPARAM lParam)
{
    HMENU hMenu;
    POINT apos;
    DWORD st, en;
    WORD lang;
    int  i;

    ::SendMessage(m_hWnd, EM_GETSEL, (WPARAM)&st, (LPARAM)&en);
    lang = PRIMARYLANGID(LANGIDFROMLCID(GetThreadLocale()));

    // メニューの作成
    hMenu = CreatePopupMenu();
    AppendMenu(hMenu, MF_STRING | (::SendMessage(m_hWnd, EM_CANUNDO, 0, 0) == TRUE) ? 0 : MF_GRAYED, EM_UNDO,
        (lang != LANG_JAPANESE) ? TEXT("&Undo") : TEXT("元に戻す(&U)"));
    AppendMenu(hMenu, MF_STRING | (::SendMessage(m_hWnd, EM_CANREDO, 0, 0) == TRUE) ? 0 : MF_GRAYED, EM_REDO,
        (lang != LANG_JAPANESE) ? TEXT("&Redo") : TEXT("やり直し(&R)"));
    AppendMenu(hMenu, MF_SEPARATOR, 0, NULL);
    AppendMenu(hMenu, MF_STRING | (st != en) ? 0 : MF_GRAYED, WM_CUT,
        (lang != LANG_JAPANESE) ? TEXT("Cu&t") : TEXT("切り取り(&T)"));
    AppendMenu(hMenu, MF_STRING | (st != en) ? 0 : MF_GRAYED, WM_COPY,
        (lang != LANG_JAPANESE) ? TEXT("&Copy") : TEXT("コピー(&C)"));
    AppendMenu(hMenu, MF_STRING, WM_PASTE,
        (lang != LANG_JAPANESE) ? TEXT("&Paste") : TEXT("貼り付け(&P)"));
    AppendMenu(hMenu, MF_STRING | (st != en) ? 0 : MF_GRAYED, WM_CLEAR,
        (lang != LANG_JAPANESE) ? TEXT("&Delete") : TEXT("削除(&D)"));
    AppendMenu(hMenu, MF_SEPARATOR, 0, NULL);
    AppendMenu(hMenu, MF_STRING, 1,
        (lang != LANG_JAPANESE) ? TEXT("Select &All") : TEXT("すべて選択(&A)"));

    // メニューの表示
    GetCursorPos((LPPOINT)&apos);
    i = TrackPopupMenu(hMenu, TPM_TOPALIGN | TPM_RETURNCMD, apos.x, apos.y, 0, m_hWnd, NULL);
    DestroyMenu(hMenu);
   
    switch (i) 
    {
    case 0:
        break;
    case 1:
        ::SendMessage(m_hWnd, EM_SETSEL, 0, -1);
        break;
    default:
        ::SendMessage(m_hWnd, i, 0, 0);
        break;
    }

    return 0;
}



//!< ウィンドウプロシージャ
LRESULT CMtEditorEngine::WindowProc(const UINT msg, const WPARAM wParam, const LPARAM lParam, BOOL* pOverRide)
{
    bool        bOverRide = FALSE;
    LRESULT     lRet = 0;
    switch (msg) 
    {
    case WM_CREATE:     {lRet = OnCreate   (wParam, lParam); bOverRide = FALSE; break;}
    case WM_DESTROY:    {lRet = OnDestroy  (wParam, lParam); bOverRide = FALSE; break;}
    case WM_SETFOCUS:   {lRet = OnSetFocus (wParam, lParam); bOverRide = FALSE; break;}
    case WM_KILLFOCUS:  {lRet = OnKillFocus(wParam, lParam); bOverRide = FALSE; break;}
    case WM_SIZE:       {lRet = OnSize     (wParam, lParam); bOverRide = FALSE; break;}

    case WM_GETDLGCODE: {lRet = DLGC_WANTALLKEYS; break;}
    case WM_HSCROLL:    {lRet = OnHScroll  (wParam, lParam); break;}
    case WM_VSCROLL:    {lRet = OnVScroll  (wParam, lParam); break;}
    case WM_KEYDOWN:    {lRet = OnKeyDown  (wParam, lParam); break;}
    case WM_CHAR:       {lRet =  OnChar     (wParam, lParam); break;}
    case WM_IME_CHAR:   {lRet =  OnImeChar  (wParam, lParam);  bOverRide = TRUE; break;}

    /*
    case WM_IME_NOTIFY: { 
		if( wParam == IMN_SETCONVERSIONMODE || wParam == IMN_SETOPENSTATUS){
			ShowCaret(m_hWnd); 
		}
        break;}
        */
    case WM_IME_STARTCOMPOSITION:   {lRet =  OnImeStartComposition  (wParam, lParam); bOverRide = TRUE;break;}
    case WM_IME_COMPOSITION:        {lRet =  OnImeComposition       (wParam, lParam); bOverRide = TRUE;break;}
    case WM_IME_ENDCOMPOSITION:     {ShowCaret(m_hWnd); bOverRide = TRUE;break;}

    case WM_MOUSEMOVE:  {lRet =  OnMouseMove  (wParam, lParam); break;}
    case WM_LBUTTONDOWN:{lRet =  OnLButtonDown(wParam, lParam); break;}
    case WM_LBUTTONUP:  {lRet =  OnLButtonUp  (wParam, lParam); break;}
    case WM_RBUTTONDOWN:{lRet =  OnRButtonDown(wParam, lParam); break;}
    case WM_RBUTTONUP:  {SendMessage(m_hWnd, WM_CONTEXTMENU, (WPARAM)m_hWnd, lParam);break;}
    case WM_LBUTTONDBLCLK:  
    {
        lRet =  OnLButtonDblClick  (wParam, lParam);
        break;
    }
    case WM_MOUSEWHEEL:     {lRet =  OnMouseWheel       (wParam, lParam); break;}
    case WM_PAINT:          {lRet =  OnPaint            (wParam, lParam); break;}

#ifdef OP_XP_STYLE
    case WM_NCPAINT:        {lRet =  OnNcPaint          (wParam, lParam); break;}
    case WM_THEMECHANGED:   {lRet =  OnTheeChanged      (wParam, lParam); break;}
#endif	// OP_XP_STYLE

    case WM_SETTEXT:		{lRet =  SetString((TCHAR *)lParam, lstrlen((TCHAR *)lParam)); break;}
    case WM_GETTEXT:        {lRet =  GetString          ((TCHAR *)wParam, lParam); break;}
    case WM_GETTEXTLENGTH:  {lRet =  GetBufferLength(); break;}
    case WM_SETFONT:        {SetFont(reinterpret_cast<const HFONT>(wParam)); break;}
    case WM_GETFONT:		{lRet = (LRESULT)m_hFont; break;}
    case WM_CLEAR:		    {Clear(); break;}
    case WM_COPY:           {Copy(); break;}
    case WM_CUT:            {Cut(); break;}
    case WM_PASTE:          {Paste(); break;}
    case EM_CANUNDO:                {lRet = OnCanUndo       (wParam, lParam); break;}
    case EM_CANREDO:                {lRet = OnCanRedo       (wParam, lParam); break;}
    case EM_EMPTYUNDOBUFFER:        {lRet = OnEmptyUndoBuffer(wParam, lParam); break;}
    case EM_GETFIRSTVISIBLELINE:    {lRet = OnGetFirstVisibleLine(wParam, lParam); break;}
    case EM_GETLINE:                {lRet = OnGetLine       (wParam, lParam); break;}
    case EM_GETLINECOUNT:           {lRet = GetLineCount(); break;}
    case EM_GETMODIFY:              {lRet = OnGetModify     (wParam, lParam); break;}
    case EM_GETRECT:                {lRet = OnGetRect       (wParam, lParam); break;}
    case EM_GETSEL:                 {lRet = OnGetSel        (wParam, lParam); break;}
    case EM_LIMITTEXT:              {lRet = OnLimitText     (wParam, lParam); break;}
    case EM_LINEFROMCHAR:           {lRet = OnLineFromChar  (wParam, lParam); break;}
    case EM_LINEINDEX:              {lRet = OnLineIndex     (wParam, lParam); break;}
    case EM_LINELENGTH:             {lRet = OnLineLength    (wParam, lParam); break;}
    case EM_LINESCROLL:             {lRet = OnLineScroll    (wParam, lParam); break;}
    case EM_REPLACESEL:             {lRet = OnReplaceSel    (wParam, lParam); break;}
    case EM_SCROLL:                 {lRet = OnScroll        (wParam, lParam); break;}
    case EM_SCROLLCARET:            {lRet = OnScrollCaret   (wParam, lParam); break;}
    case EM_SETMODIFY:              {lRet = OnSetModify     (wParam, lParam); break;}
    case EM_GETREADONLY:            {lRet = OnGetReadOnly   (wParam, lParam); break;}
    case EM_SETREADONLY:            {lRet = OnSetReadOnly   (wParam, lParam); break;}
    case EM_SETRECT:                {lRet = OnSetRect       (wParam, lParam); break;}
    case EM_SETRECTNP:              {lRet = OnSetRectNp     (wParam, lParam); break;}
    case EM_SETSEL:                 {lRet = OnSetSel        (wParam, lParam); break;}
    case EM_SETTABSTOPS:            {lRet = OnSetAbsTops    (wParam, lParam); break;}
    case WM_UNDO:
    case EM_UNDO:                   {Undo(); break;}
    case EM_REDO:                   {Redo(); break;}
    case WM_GETBUFFERINFO:          {lRet = OnGetBufferInfo (wParam, lParam); break;}
    case WM_REFLECT:                {lRet = OnReflect       (wParam, lParam); break;}
    case WM_GETWORDWRAP:            {lRet = OnGetWordwrap   (wParam, lParam); break;}
    case WM_SETWORDWRAP:            {lRet = OnSetWordwrap   (wParam, lParam); break;}
    case WM_GETMEMSIZE:             {lRet = OnGetMemSize    (wParam, lParam); break;}
    case WM_GETMEM:                 {lRet = OnSetMem        (wParam, lParam); break;}
    case WM_SETMEM:                 {lRet = OnGetMem        (wParam, lParam); break;}
    case WM_CONTEXTMENU:            {lRet = OnContextMenu   (wParam, lParam); break;}

    default:
        break;
    }


    if(pOverRide)
    {
        *pOverRide = bOverRide;
    }
    return 0;
}


//!< バッファー表示
void CMtEditorEngine::DrawBuffer()
{
    DWORD dwCharIndex;
    TCHAR* pChar;
    TCHAR szBuf[1024];
    int iCnt;
    int iLen;
    
    for(int iLine = 0; iLine < m_iLineLen; iLine++)
    {
        dwCharIndex = LineGet(iLine);
        pChar = IndexToChar(dwCharIndex);

        iCnt = LineGetCount(iLine);
        iLen = LineGetLength(iLine);
        memset(&szBuf[0], 0, sizeof(szBuf));
        memcpy(&szBuf[0], pChar, iLen);
        TRACE(_T("%04d:%04d:%s\n"), iLine, iCnt, szBuf);
    }
}


//!< 論理行頭取得
int CMtEditorEngine::GetLogicalLineHead(int iLine) const
{
    int iRet = 0;
    int iLogcalLine = m_lstLine[iLine].iLogicalLine;
    for (int i = iLine; i >= 0; i--)
    {
        if (iLogcalLine != m_lstLine[i].iLogicalLine)
        {
            iRet = i + 1;
            break;
        }
    }
    return iRet;
}

/**
 *  @brief   ブレークポイント描画
 *  @param   [in] iLine       描画行
 *  @param   [in] bDrawDirect true:直接描画 
 *                            false:バックバッファー
 *  @retval  なし
 *  @note
 */
void CMtEditorEngine::DrawBreakPoint(int iLine, BOOL bDrawDirect) const
{
    if(!IsVisivleLine(iLine))
    {
        return;
    }

    HDC hDrawDC;

    if (!bDrawDirect)
    {
        hDrawDC = m_hDc;
    }
    else
    {
        /*
        if(m_lstLine[iLine].iBreakPoint == UNSET)
        {
            return;
        }
        */
        hDrawDC = GetDC(m_hWnd);
    }

    //TODO: 後で、複数回計算を行わないように、くくりだす
    int iBreakPointSize;
    if (m_iFontHeight > m_iIndicatorWidth)
    {
        iBreakPointSize = (m_iIndicatorWidth - 2) / 2;
    }
    else
    {
        iBreakPointSize = (m_iFontHeight - 2) / 2;
    }

    if (iBreakPointSize < 2 )
    {
        iBreakPointSize = 2;
    }

    int iLineYTop   = (iLine - m_iScrollY) * m_iFontHeight + m_iTopMargin;
    RECT rcBreak;

    int iOffset = m_iFontHeight / 2 - iBreakPointSize;
    
    if (bDrawDirect)
    {
        rcBreak.top    = iLineYTop + iOffset;
    }
    else
    {
        rcBreak.top    = iOffset;
    }
    rcBreak.bottom = rcBreak.top + 2 * iBreakPointSize;
    rcBreak.left   = iOffset;
    rcBreak.right  = iOffset + 2 * iBreakPointSize;

    //TODO: ブラシのキャッシュを作成
    HBRUSH hBr;
    if(m_lstLine[iLine].iBreakPoint == UNSET)
    {
        if (bDrawDirect)
        {
            hBr = ::CreateSolidBrush(m_crIndicator);
            ::FillRect(hDrawDC, &rcBreak, hBr);
        }
        return;
    }
    else if(m_lstLine[iLine].iBreakPoint == SET)
    {
        hBr = ::CreateSolidBrush(RGB(255,0,0));
    }
    else
    {
        hBr = ::CreateSolidBrush(RGB(0,0,0));
    }

    HPEN hPen;
    HBRUSH hBrOld;

    hPen = static_cast<HPEN>(::GetStockObject(BLACK_PEN));
    hBrOld = static_cast<HBRUSH>(::SelectObject(hDrawDC, hBr));
    SelectObject(hDrawDC , hPen);

    ::Ellipse(hDrawDC,  rcBreak.left, rcBreak.top,
                       rcBreak.right, rcBreak.bottom);

    ::SelectObject(hDrawDC, hBrOld);
    ::DeleteObject(hBr);

}

/**
 *  @brief   ブレークポイント全解除
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CMtEditorEngine::ClearBreakPoint()
{
    std::vector<LINE_INFO>::iterator ite;
    for(ite  = m_lstLine.begin();
        ite != m_lstLine.end();
        ite++)
    {
        (*ite).iBreakPoint = UNSET;
    }
}

/**
 *  @brief   ブレークポイント取得
 *  @param   [out] pBreakPointList ブレークポイントリスト
 *  @retval  なし
 *  @note
 */
void CMtEditorEngine::GetBreakPoint
    (std::vector<BREAK_POINT_DATA>* pBreakPointList)
{
    BREAK_POINT_DATA breakData;
    pBreakPointList->clear();
    std::vector<LINE_INFO>::iterator ite;

    for(ite  = m_lstLine.begin();
        ite != m_lstLine.end();
        ite++)
    {
        if ((*ite).iBreakPoint == UNSET)
        {
            continue;
        }

        breakData.iLine = (*ite).iLogicalLine;
        if ((*ite).iBreakPoint == SET)
        {
TRACE(_T("GetBreakPoint %d\n"), breakData.iLine);
            breakData.bEnable = true;
        }
        else
        {
            breakData.bEnable = false;
        }
        pBreakPointList->push_back(breakData);
    }
}

/**
 *  @brief   ブレークポイント設定
 *  @param   [in] pBreakPointList ブレークポイントリスト
 *  @retval  なし
 *  @note
 */
void CMtEditorEngine::SetBreakPoint
                (const std::vector<BREAK_POINT_DATA>& lstBreakPoint)
{
    std::vector<BREAK_POINT_DATA>::const_iterator ite;

    int iLine;
    ClearBreakPoint();

    for(ite  = lstBreakPoint.begin();
        ite != lstBreakPoint.end();
        ite++)
    {
        iLine = LogicaToLine((*ite).iLine);
        if (iLine > 0)
        {
            if ((*ite).bEnable)
            {
TRACE(_T("SetBreakPoint %d\n"), iLine);
                m_lstLine[iLine].iBreakPoint = SET;
            }
            else
            {
                m_lstLine[iLine].iBreakPoint = UNUSE;
            }
        }
    }
}

/**
 *  @brief   ブレークポイント追加
 *  @param   [in] iLine 追加行
 *  @param   [in] iLine 追加行
 *  @retval  なし
 *  @note
 */
bool CMtEditorEngine::AddBreakPoint(const int iLine, bool bEnable)
{
    int iDispLine;
    iDispLine = LogicaToLine(iLine);
TRACE(_T("AddBreakPoint %d\n"), iLine);

    if (iDispLine < 0)
    {
        return false;
    }
    if (bEnable)
    {
        m_lstLine[iDispLine].iBreakPoint = SET;
    }
    else
    {
        m_lstLine[iDispLine].iBreakPoint = UNUSE;
    }
    return true;
}

/**
 *  @brief   ブレークポイント削除
 *  @param   [in] pBreakPointList ブレークポイントリスト
 *  @retval  なし
 *  @note
 */
bool CMtEditorEngine::DelBreakPoint(const int iLine)
{
    int iDispLine;
    iDispLine = LogicaToLine(iLine);

    if (iDispLine < 0)
    {
        return false;
    }

    m_lstLine[iDispLine].iBreakPoint = UNSET;
    return true;
}

/**
 *  @brief   行表示の有無
 *  @param   なし
 *  @retval  なし
 *  @note
 */
BOOL CMtEditorEngine::IsVisivleLine(int iLine) const
{
    if (m_iScrollY > iLine)
    {
        return FALSE;
    }

    int iVisibleMax;
    iVisibleMax = m_iScrollY + (m_iHeight / m_iFontHeight);

    if (iLine > iVisibleMax)
    {
        return FALSE;
    }

    return TRUE;
}

/**
 *  @brief   論理行から表示行を取得
 *  @param   [in]  iLogicalLine
 *  @retval  表示行
 *  @note
 */
int CMtEditorEngine::LogicaToLine( const int iLogicalLine)
{
    int low = 0;
    int high = m_lstLine.size() - 1;
    int i;
    int iRet = -1;
    bool bRet = false;

    while (low <= high) 
    {
        i = (low + high) / 2;

        if (m_lstLine[i].iLogicalLine > iLogicalLine) 
        {
            if (i > 0 && 
                m_lstLine[i-1].iLogicalLine <= iLogicalLine) 
            {
                bRet = true;
                iRet = i - 1;
            }
            high  = i - 1;
        } 
        else if (m_lstLine[i].iLogicalLine < iLogicalLine) 
        {
            if (i < m_lstLine.size() - 1 && 
                m_lstLine[i + 1].iLogicalLine > iLogicalLine) 
            {
                bRet = true;
                iRet = i;
            }
            low = i + 1;
        }
        else 
        {
            bRet = true;
            iRet = i;
        }

        if (bRet)
        {
            return GetLogicalLineHead(iRet);
        }
    }
    return ( -1);
}

/**
 *  @brief   実行行矢印描画
 *  @param   [in] iLine       描画行
 *  @param   [in] bDrawDirect true:直接描画 
 *                            false:バックバッファー
 *  @retval  なし
 *  @note
 */
void CMtEditorEngine::DrawExecIndicator(int iLine, BOOL bDrawDirect)
{
    if (m_iExecLine != m_lstLine[iLine].iLogicalLine)
    {
        return;
    }

    if(!IsVisivleLine(iLine))
    {
        return;
    }

    HDC hDrawDC;

    if (!bDrawDirect)
    {
        hDrawDC = m_hDc;
    }
    else
    {
        hDrawDC = GetDC(m_hWnd);
    }

    //TODO: 後で、複数回計算を行わないように、くくりだす
    int iBreakPointSize;
    if (m_iFontHeight > m_iIndicatorWidth)
    {
        iBreakPointSize = (m_iIndicatorWidth - 2) / 2;
    }
    else
    {
        iBreakPointSize = (m_iFontHeight - 2) / 2;
    }

    if (iBreakPointSize < 2 )
    {
        iBreakPointSize = 2;
    }

    int iLineYTop   = (iLine - m_iScrollY) * m_iFontHeight + m_iTopMargin;
    RECT rcBreak;

    int iOffset = m_iFontHeight / 2 - iBreakPointSize;
    
    if (bDrawDirect)
    {
        rcBreak.top    = iLineYTop + iOffset;
    }
    else
    {
        rcBreak.top    = iOffset;
    }
    rcBreak.bottom = rcBreak.top + 2 * iBreakPointSize;
    rcBreak.left   = iOffset;
    rcBreak.right  = iOffset + 2 * iBreakPointSize;


    HPEN hPen;
    HBRUSH hBr;
    HBRUSH hBrOld;
    POINT   lstPt[7];

    hBr = ::CreateSolidBrush(RGB(255,255,0));


    hPen = static_cast<HPEN>(::GetStockObject(BLACK_PEN));
    hBrOld = static_cast<HBRUSH>(::SelectObject(hDrawDC, hBr));
    SelectObject(hDrawDC , hPen);

    int iUnit = static_cast<int>(::floor(iBreakPointSize / 2.0 ));

    lstPt[0].x = rcBreak.right;
    lstPt[0].y = rcBreak.top + iBreakPointSize;

    lstPt[1].x = lstPt[0].x - iBreakPointSize;
    lstPt[1].y = rcBreak.top + 1;

    lstPt[2].x = lstPt[1].x;
    lstPt[2].y = rcBreak.top + iUnit;

    lstPt[3].x = rcBreak.left + 2;
    lstPt[3].y = lstPt[2].y;

    lstPt[4].x = lstPt[3].x;
    lstPt[4].y = lstPt[3].y + iBreakPointSize;

    lstPt[5].x = lstPt[1].x;
    lstPt[5].y = lstPt[4].y;

    lstPt[6].x = lstPt[1].x;
    lstPt[6].y = rcBreak.bottom - 1;

    ::Polygon(hDrawDC, &lstPt[0], sizeof(lstPt) / sizeof(lstPt[0]));

    ::SelectObject(hDrawDC, hBrOld);
    ::DeleteObject(hBr);
}

/**
 *  @brief   実行行の設定
 *  @param   [in] iLine   論理行
 *  @param   [in] bEnsure true:画面に表示
 *  @retval  なし
 *  @note    
 *
 */
void CMtEditorEngine::SetExecLine(int iLine, bool bEnsure)
{
    //!< 指定行が表示領域にあるか
TRACE(_T("SetExecLine %d\n"), iLine);
    int iOldDispLine = LogicaToLine(m_iExecLine);
    int iDispLine = LogicaToLine(iLine);
    if (!bEnsure)
    {
        return;
    }

    if (!IsVisivleLine(iDispLine))
    {
        EnsureLine(iDispLine);
    }

    m_iExecLine = iLine;
    DrawBreakPoint(iOldDispLine, TRUE);
    DrawBreakPoint(iDispLine, TRUE);
    DrawExecIndicator(iDispLine, TRUE);
}


//!> 行を表示
void CMtEditorEngine::EnsureLine(int iLine)
{
    RECT rect;
    int i;
    int x, y;

    GetEditRect(&rect);

    i = IndexToLine(m_dwCaretPos);
    x = 0;
    y = iLine;

    // x
    i = m_iScrollX;
    m_iScrollX = 0;

    if (i != m_iScrollX)
    {
        SetScrollPos(m_hWnd, SB_HORZ, m_iScrollX, TRUE);
        ScrollWindowEx(m_hWnd, (i - m_iScrollX) * m_iCharWidth, 0, NULL, &rect, NULL, NULL, SW_INVALIDATE | SW_ERASE);
    }

    double dLineNum = (rect.bottom - rect.top) / m_iFontHeight;
    int iDispLine = static_cast<int>(::floor(dLineNum));
    int iCenterLine = static_cast<int>(::ceil(dLineNum / 2.0));

    TRACE(_T("LINE %f:%d:%d\n"), dLineNum, iDispLine, iCenterLine);
    // y
    i = m_iScrollY;
    if (y < m_iScrollY) 
    {
        m_iScrollY = y - iCenterLine;
        if (m_iScrollY < 0)
        {
            m_iScrollY = 0;
        }
    }
    else if (y > m_iScrollY + iDispLine) 
    {
        m_iScrollY = y - iCenterLine;
        if (m_iScrollY > m_iScrollMaxY) 
        {
            m_iScrollY = m_iScrollMaxY;
        }
    }

    if (i != m_iScrollY) 
    {
        rect.left = 0;
        SetScrollPos(m_hWnd, SB_VERT, m_iScrollY, TRUE);
        ScrollWindowEx(m_hWnd, 0, (i - m_iScrollY) * m_iFontHeight, NULL, &rect, NULL, NULL, SW_INVALIDATE | SW_ERASE);
    }
}

/**
 *  @brief   行数取得
 *  @param   なし
 *  @retval  行数
 *  @note    論理行数を返す
 */
int CMtEditorEngine::GetLineCount()
{
    int iSize;
    iSize = m_lstLine.size();
    if (iSize == 0)
    {
        return 0;
    }
    return m_lstLine[iSize - 1].iLogicalLine + 1;
}

/**
 *  @brief   キャレット位置取得
 *  @param   [out] pLine  論理行
 *  @param   [out] pCol   文字位置
 *  @retval  なし
 *  @note    
 */
void CMtEditorEngine::GetCaretPosition(int* pLine, int* pCol) const
{
    int iLine;
    int iLineHead;
    iLine = IndexToLine(m_dwCaretPos);
    iLineHead = GetLogicalLineHead(iLine);
    *pLine = m_lstLine[iLine].iLogicalLine;
    *pCol = m_dwCaretPos - m_lstLine[iLineHead].dwLineHead;
}

/**
 *  @brief   キャレット位置設定
 *  @param   [in] iLine  論理行
 *  @param   [in] iCol   文字位置
 *  @retval  なし
 *  @note    
 */
void CMtEditorEngine::SetCaretPosition(int iLine, int iCol)
{
    int iLineHead;
    iLineHead = LogicaToLine(iLine);

    if (iLineHead < 0)
    {
        return;
    }
    m_dwCaretPos = m_lstLine[iLineHead].dwLineHead + iCol;

    int iSelLine;
    iSelLine = IndexToLine(m_dwCaretPos);

    EnsureLine(iSelLine);

}

/**
 *  @brief   文字列の取得
 *  @param   [out] pString  文字列
 *  @retval  なし
 *  @note    文字数
 */
int CMtEditorEngine::GetString(StdString* pString)
{
    FlushString(TRUE);
    *pString = m_pBuf;
    return pString->length();
}

/**
 *  @brief   文字列の設定
 *  @param   [out] pString  文字列
 *  @retval  なし
 *  @note    文字数
 */
BOOL CMtEditorEngine::SetString(const StdString& str)
{
    return SetString(&str[0], str.length());
}

/**
 *  @brief   再描画
 *  @param   なし
 *  @retval  なし
 *  @note    
 */
void CMtEditorEngine::Redraw()
{

    /*
    ◆OnDraw( ) を直接呼び出す場合
    OnDraw( ) での内容が描かれます。( 上書きされます。)
    背景はそのままです。

    ◆RedrawWindow( ) を呼び出す場合
    まず OnEraseBkgnd( ) が呼び出され、背景が描かれます。
    次に OnDraw( ) が呼び出されます。

    ◆InvalidateRect( ) を呼び出す場合 ( 引数に 領域と FALSE を指定 )
    呼出しを書いた関数の終了後に、OnDraw( ) が呼び出されます。
    背景はそのままです。
    引数に指定した描画領域だけが、OnDraw( ) で描画されます。

    ◆InvalidateRect( ) を呼び出す場合 ( 引数に 領域と TRUE を指定 )
    呼出しを書いた関数の終了後に、OnEraseBkgnd( ) が呼び出されます。
    次に OnDraw( ) が呼び出されます。
    OnEraseBkgnd( ) が背景を塗り潰すのは、引数で指定した領域だけです。
    */

    InvalidateRect(m_hWnd, NULL, FALSE);
}

bool CMtEditorEngine::SetBreakPointCallback(void* pParent, void (CALLBACK* pSetBreakPointModified)(void* pData, int iLine, bool bSet))
{
    m_pParent = pParent;
    m_pSetBreakPointModified = pSetBreakPointModified;

    return true;
}

/**
 * @brief   1行解析
 * @param   [in] iLinePos   行位置
 * @retval  なし
 * @note
 */
void CMtEditorEngine::ParseLine(const int iLinePos)
{
    DWORD dwLineHead     = LineGet(iLinePos);       //先頭文字位置
    DWORD dwLineNextHead = LineGet(iLinePos + 1);  
    DWORD dwDrawPos;  
    TCHAR* p;


    STRING_ANALYSYS_STS eSts;

    p = IndexToChar(dwLineHead);

    for (dwDrawPos = dwLineHead ;
         dwDrawPos < dwLineNextHead;
         dwDrawPos++,
         p = CharNext(p)) 
    {
        





    }
}


/**
 * @brief   文字列取得
 * @param   [out] pLine      行
 * @param   [in]  iLinePos   行位置
 * @retval  なし
 * @note
 */
void CMtEditorEngine::GetLine(std::vector<TCHAR>* pLine, int iLinePos) const
{
}

/**
 *  @brief   テスト
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CMtEditorEngine::Test()
{
    m_lstLine.clear();


    LINE_INFO info;

    //-------------------------------------
    //Line 0
    info.dwLineHead   = 0;
    info.iLogicalLine = 0;
    info.iBreakPoint  = UNSET;
    m_lstLine.push_back(info);

    //Line 1
    info.dwLineHead   = 2;
    info.iLogicalLine = 1;
    info.iBreakPoint  = UNSET;
    m_lstLine.push_back(info);

    //Line 2
    info.dwLineHead   += 4;
    info.iLogicalLine = 1;
    info.iBreakPoint  = SET;
    m_lstLine.push_back(info);

    //Line 3
    info.dwLineHead   += 4;
    info.iLogicalLine = 1;
    info.iBreakPoint  = UNSET;
    m_lstLine.push_back(info);

    //Line 4
    info.dwLineHead   += 4;
    info.iLogicalLine = 2;
    info.iBreakPoint  = UNSET;
    m_lstLine.push_back(info);

    //Line 5
    info.dwLineHead   += 4;
    info.iLogicalLine = 3;
    info.iBreakPoint  = UNSET;
    m_lstLine.push_back(info);

    //Line 6
    info.dwLineHead   += 4;
    info.iLogicalLine = 4;
    info.iBreakPoint  = UNSET;
    m_lstLine.push_back(info);

    //Line 7
    info.dwLineHead   += 4;
    info.iLogicalLine = 5;
    info.iBreakPoint  = UNSET;
    m_lstLine.push_back(info);

    //Line 8
    info.dwLineHead   += 4;
    info.iLogicalLine = 6;
    info.iBreakPoint  = UNUSE;
    m_lstLine.push_back(info);

    //Line 9
    info.dwLineHead   += 4;
    info.iLogicalLine = 6;
    info.iBreakPoint  = UNSET;
    m_lstLine.push_back(info);

    //Line 10
    info.dwLineHead   += 4;
    info.iLogicalLine = 6;
    info.iBreakPoint  = UNSET;
    m_lstLine.push_back(info);

    //Line 11
    info.dwLineHead   += 4;
    info.iLogicalLine = 7;
    info.iBreakPoint  = UNSET;
    m_lstLine.push_back(info);
    //-------------------------------------
    //============
    // GetLogicalLineHeadテスト
    //============
    assert ( GetLogicalLineHead(0) == 0);
    assert ( GetLogicalLineHead(1) == 1);
    assert ( GetLogicalLineHead(2) == 1);
    assert ( GetLogicalLineHead(3) == 1);
    
    assert ( GetLogicalLineHead(4) == 4);
    assert ( GetLogicalLineHead(5) == 5);
    assert ( GetLogicalLineHead(6) == 6);
    
    assert ( GetLogicalLineHead(7) == 7);
    assert ( GetLogicalLineHead(8) == 8);
    assert ( GetLogicalLineHead(9) == 8);
    assert ( GetLogicalLineHead(10) == 8);
    assert ( GetLogicalLineHead(11) == 11);

    //============
    // LogicaToLineテスト
    //============
    assert ( LogicaToLine(6) == 8);
    assert ( LogicaToLine(7) == 11);
    assert ( LogicaToLine(10) == -1);

}

