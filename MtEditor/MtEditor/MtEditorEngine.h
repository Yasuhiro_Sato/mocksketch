#pragma once


#ifndef _INC_NEDIT_H
#define _INC_NEDIT_H

/* Include Files */
#include <windows.h>
#include <tchar.h>
#include <imm.h>
#include <vector>
#include <map>
#ifdef OP_XP_STYLE
//#include <uxtheme.h>
#include <vssym32.h>
//#include <tmschema.h>
#endif	// OP_XP_STYLE

#ifndef StdString
#ifdef  UNICODE
#define StdString           std::wstring
#else
#define StdString           std::string
#endif  // UNICODE
#endif	// StdString

/* Define */

#define WM_GETBUFFERINFO				(WM_APP + 1)
#define WM_REFLECT						(WM_APP + 2)
#define WM_GETWORDWRAP					(WM_APP + 3)
#define WM_SETWORDWRAP					(WM_APP + 4)
#define WM_GETMEMSIZE					(WM_APP + 5)
#define WM_GETMEM						(WM_APP + 6)
#define WM_SETMEM						(WM_APP + 7)

#define EM_GETREADONLY					(WM_APP + 100)

#ifndef EM_REDO
#define EM_REDO							(WM_USER + 84)
#endif
#ifndef EM_CANREDO
#define EM_CANREDO						(WM_USER + 85)
#endif

#define UNDO_TYPE_INPUT					1
#define UNDO_TYPE_DELETE				2

/* Struct */


class CMtEditorEngine
{
public:
    struct BREAK_POINT_DATA
    {
        int  iLine;    //論理行
        bool bEnable;  //有効・無効
    };


protected:
    enum STRING_ANALYSYS_STS
    {
        STS_NORMAL = 0,
        STS_BLOCK_COMMNET   ,
        STS_LINE_COMMNET    ,
        STS_SINGLE_QUOTATION,
        STS_DOUBLE_QUOTATION,
        STS_TRIPLE_QUOTATION,
        STS_OPERATOR,
        STS_SPACE,
        STS_RESERV1,
        STS_RESERV2,
        STS_RESERV3,

    };

    enum BREAK_POINT
    {
        UNSET = 0,
        SET   = 1,
        UNUSE = 2,
    };

    struct UNDO 
    {
        BYTE type;

        DWORD st;
        DWORD len;
        TCHAR *buf;
    };

    struct LINE_INFO
    {
        DWORD  dwLineHead;
        int    iLogicalLine;
        int    iBreakPoint;
        //STRING_ANALYSYS_STS  eStartSts;
        STRING_ANALYSYS_STS  eEndSts;
        //std::map<int, STRING_ANALYSYS_STS> mapSts;
    };

protected:
	// 保持している内容
    TCHAR *m_pBuf;
	DWORD m_dwBufSize;
	DWORD m_dwBufLen;

	// 入力バッファ
	TCHAR *m_pInputBuf;
	DWORD m_dwInputSize;
	DWORD m_dwInputLen;

    // 表示行頭のオフセット
	int m_iLineLen;     //行数
	int m_iLineAddIndex;
	int m_iLineAddLen;
    int m_iLogicalLineAdd;
    std::vector<LINE_INFO>  m_lstLine;

    // UNDOバッファ
	UNDO *m_pUndo;
	int m_iUndoSize;
	int m_iUndoLen;
	int m_iUndoPos;

    
    // 入力開始位置
	TCHAR* m_pInputPos;
	DWORD  m_dwInputPosLen;

	// 削除開始位置
    TCHAR *m_pDeletePos;
	DWORD m_dwDeleteLen;

	// キャレットの位置
	DWORD m_dwCaretPos;

	// 選択位置
	DWORD m_dwSelectPos;

	// 上下移動時のキャレットのX座標
	DWORD m_iCaretPosX;

	// 1行の文字数
	int m_iLineMax;

    // 行の最大幅
	int m_iLineWidth;

    // 描画領域の幅
	int m_iWidth;

    // 描画領域の高さ
	int m_iHeight;

	// スクロール バー
	int m_iScrollX;
	int m_iScrollMaxX;
	int m_iScrollY;
	int m_iScrollMaxY;

    
    // タブストップ
	int m_iTabStop;
	// 左マージン
	int m_iLeftMargin;
	// 上マージン
	int m_iTopMargin;
	// 右マージン
	int m_iRightMargin;
	// 下マージン
	int m_iBottomMargin;
	// 行間
	int m_iSpacing;

    //インジケータ幅
    int m_iIndicatorWidth;

    //インジケータ行
    int m_iExecLine;


	// 描画用情報
	HDC     m_hDc;
	HBITMAP m_hRetBmp;
	HRGN    m_hRgn;
	HFONT   m_hFont;
	HFONT   m_hRetFont;

    // インジケータ色
    COLORREF m_crIndicator;


	// フォント
	int m_iFontHeight;
	int m_iCharWidth;


	// 折り返しフラグ
	BOOL m_bWordwrap;

    // フォーカスが無くても選択表示
	BOOL m_bNoHideSel;

	// ロックフラグ
	BOOL m_bLock;

    // 修正フラグ
	BOOL m_bModified;

	// 入力モード
	BOOL m_bInsertMode;

    // 選択フラグ
	BOOL m_bSel;

	// マウス情報フラグ
	BOOL m_bMousedown;

    // 入力長制限
	DWORD m_dwLimitLen;

	// 文字の幅
	BYTE m_cWidth[256];

	// コントロール識別子
	int m_iId;

	// IME
	HIMC m_hImc;

    //ブレークポイント選択行
    int m_iClickBreakPoint;

    //------------------
    // 色
    //------------------

    //文字色
    COLORREF m_crText;

    //背景色
    COLORREF m_crBack;

    //コメント
    COLORREF m_crCommnet;

    //選択色
    COLORREF m_crSel;

    //選択背景色
    COLORREF m_crSelBack;

    //予約語1色
    COLORREF m_crReserv1;

    //予約語2色
    COLORREF m_crReserv2;

    //予約語3色
    COLORREF m_crReserv3;

    //文字列色
    COLORREF m_crString;

    //行番号
    COLORREF m_crLineNo;

    //現在の文字色
    COLORREF m_crCurrent;


#ifdef OP_XP_STYLE
	// XP
	HTHEME  m_hTheme;
	HMODULE m_hModThemes;

    typedef HRESULT ( STDAPICALLTYPE* CLOSETHEMEDATA )( HTHEME hTheme );
    typedef HTHEME  ( STDAPICALLTYPE* OPENTHEMEDATA  )( HWND hwnd, LPCWSTR pszClassList );
    typedef HRESULT ( STDAPICALLTYPE* DRAWTHEMEBACKGROUND )( HTHEME hTheme, HDC hdc, int iPartId, int iStateId, const RECT *pRect, OPTIONAL const RECT *pClipRect );

    CLOSETHEMEDATA        F_CloseThemeData;
    OPENTHEMEDATA         F_OpenThemeData;
    DRAWTHEMEBACKGROUND   F_DrawThemeBackground;


#endif	// OP_XP_STYLE

    HWND m_hWnd;

    void (CALLBACK* m_pSetBreakPointModified)(void* pData, int iLine, bool bSet);

    void* m_pParent;
public:
    CMtEditorEngine(void);

    ~CMtEditorEngine(void);

    void Create(HWND hWnd);

    //!< ウィンドウプロシージャ
    LRESULT  WindowProc(const UINT msg, const WPARAM wParam, const LPARAM lParam, BOOL* pOverRide);

    //--------------------------------
    // publicで使用する行は全て論理行
    //--------------------------------

    //!< ブレークポイント全解除
    void ClearBreakPoint();

    //!< ブレークポイント取得
    void GetBreakPoint
        (std::vector<BREAK_POINT_DATA>* pBreakPointList);

    //!< ブレークポイント設定
    void SetBreakPoint
         (const std::vector<BREAK_POINT_DATA>& lstBreakPoint);

    //!< ブレークポイント追加
    bool AddBreakPoint(const int iLine, bool bEnable);

    //!< ブレークポイント削除
    bool DelBreakPoint(const int iLine);

    //!< ブレークポイントコールバック
    bool SetBreakPointCallback(void* pParent, void (CALLBACK* pSetBreakPointModified)(void* pData, int iLine, bool bSet));

    //!< 実行行の設定
    void SetExecLine( int iLine, bool bEnsure);

    //!< 文字列の設定
    BOOL SetString(const TCHAR *str, const DWORD len);

    //!< 文字列の設定
    BOOL SetString(const StdString& str);

    //!< 文字列の取得
    int GetString(TCHAR *str, const DWORD len);

    //!< 文字列の取得
    int GetString(StdString* pString);

    //!< フォント設定
    void SetFont(const HFONT hFont);
    
    //!< 論理行数取得
    int GetLineCount();

    //!< 論理行から表示行を取得
    int LogicaToLine( const int iLogicalLine);

    inline
    DWORD GetBufferLength() {return (m_dwBufLen + m_dwInputLen - m_dwDeleteLen - m_dwInputPosLen);}

    //!< UnDoの実行
	BOOL Undo();

    //!< ReDoの実行
	BOOL Redo();

    //-----------------------
	// Clipboard operations
    //-----------------------
    //!< 選択中テキスト消去
    void Clear();

    //!< 選択中テキストコピー
    void Copy();

    //!< 選択中テキスト切り取り
    void Cut();

    //!< テキスト貼り付け
    void Paste();
    //-----------------------

    //!< キャレット位置取得
    void GetCaretPosition(int* pLine, int* pCol) const;

    //!< キャレット位置設定
    void SetCaretPosition(int iLine, int iCol);

    //!< 再描画
    void Redraw();





protected:


    void *MemAlloc(const DWORD size){return LocalAlloc(LMEM_FIXED, size);}

    //!< 初期化してバッファを確保
    void *MemCalloc(const DWORD size){return LocalAlloc(LPTR, size);}

    //!< バッファを解放
    void MemFree(void **mem);

    //!< 文字列をクリップボードに設定
    BOOL StringToClipboard(const TCHAR *st, const TCHAR *en);

    //!< 親ウィンドウに通知
    void NotifyMessage(const int code) const;

    //!< 描画領域の取得
    BOOL GetEditRect(RECT *rect) const;

    //!<  スクロールバーの設定
    void SetScrollbar();

    //!< キャレット位置を表示
    void EnsureVisible();

    //!< マルチバイトの先頭バイトかチェック
    BOOL IsLeadByte( TCHAR *p) const;

    //!< 文字の描画幅の取得
    int GetCharExtent(TCHAR *str, int *ret_len);

    //!< 次の文字を取得
    TCHAR* CharNext( TCHAR *p) const;

    //!< 前の文字を取得
    TCHAR* CharPrev( TCHAR *p) const;

    //!< 文字インデックスから文字位置を取得
    TCHAR* IndexToChar(const DWORD index) const;

    //!< 文字位置から文字インデックスを取得
    DWORD CharToIndex(const TCHAR *p) const;

    //!< 文字インデックスから行位置を取得
    int IndexToLine(const DWORD index) const;

    //!< 行情報の確保
    BOOL LineAlloc(const int i);

    //!< 行情報の移動
    BOOL LineMove(const int i, const int len);

    //!< 行頭インデックスの取得
    DWORD LineGet(const int lindex) const;

    //!< 行頭インデックスを設定
    void LineSet(const int iLine, 
                 const DWORD dwCharPos, 
                 const int iLogcalLine);

    //!< 行情報の反映
    void LineFlush();

    //!< 行情報に文字数を追加
    void LineAddLength(const int index, const int len);

    //!< 行の長さを取得
    int LineGetLength(const DWORD index);

    //!< 次の行頭の文字インデックスを取得
    DWORD LineGetNextIndex(bool* bContinue, const DWORD index);

    //!< 行情報の設定
    BOOL LineSetInfo();
    
    //!< 論理行数の取得
    int LineGetCount(const int lindex);

    //!< 行情報の設定
    int LineSetCount(const int lindex, 
        const int old_cnt, 
        const int line_cnt);

    //!< 指定範囲文字列のある行を再描画対象にする
    void LineRefresh(const DWORD p, const DWORD r);

    //!< 行数取得
    int GetDispLineCount();

    //!< UNDOの確保
    BOOL UndoAlloc();

    //!< UNDOの解放
    void UndoFree(const int index);

    //!< UNDOのセット
    BOOL UndoSet(const int type, const DWORD index, const DWORD len);

    //!< 初期化
    BOOL InitString();

    //!< 文字列の追加
    BOOL InsertString(TCHAR *str, const int len, const BOOL insert_mode);

    //!< 改行数の取得
    int GetReturnCount(TCHAR *pStr, const int iLen);

    //!< 入力バッファへの追加
    void AddInputBuf(TCHAR *pStr, const int iLen);

    //!< 選択位置の削除
    BOOL DeleteSelect();

    //!< 文字列の削除
    void DeleteString(DWORD st, DWORD en);

    //!< 文字の削除
    void DeleteStringChar(DWORD dwCharPos);

    //!< 削除と入力バッファの反映
    BOOL FlushString(const BOOL undo_flag);

    //!< 描画情報の初期化
    BOOL DrawInit();

    //!< 描画情報の解放
    void DrawFree();

    //!< 矩形描画
    void DrawRect(const HDC mdc, const int left, const int top, const int right, const int bottom);

    //!< 文字列描画
    int DrawString(const HDC mdc, const RECT *drect, 
        const int left, 
        const int top, 
        const TCHAR *str, 
        const int len, 
        const BOOL sel,
        const COLORREF  crText
        );

    //!< 1行描画
    void DrawLine(const HDC mdc, const int i, const int left, const int right);

    //!< キャレットのサイズ設定
    void CaretSetSize();

    //!< 文字位置からキャレットの位置取得
    int CaretCharToCaret(const HDC mdc, const int i, const DWORD cp);

    //!<  座標からキャレットの位置取得
    DWORD CaretPointToCaret(const int x, const int y);

    //!< キャレット位置のトークンを取得
    void CaretGetToken();

    //!< キャレットの移動
    void CaretMove(const int key);

    //!< 論理行頭取得
    int GetLogicalLineHead(int iLine) const;

    //!< バッファー表示(デバッグ用)
    void DrawBuffer();

    //!< ブレークポイント描画
    void DrawBreakPoint(int iLine, BOOL bDrawDirect) const;

    //!< 指定行が表示領域にあるか
    BOOL IsVisivleLine(int iLine) const;


    //!< 実行行矢印描画
    void DrawExecIndicator(int iLine, BOOL bDrawDirect);

    //!< 行の表示
    void EnsureLine(int iLine);

    //!<1行解析
    void AnalysisLine(const HDC hDc, 
                      const int iLinePos, 
                      const int iLeft,
                      const int iRight,
                      std::map<int,STRING_ANALYSYS_STS>* pMap);

    void ParseLine(const int iLinePos);

    void GetLine(std::vector<TCHAR>* pLine, int iLinePos) const;

    //!< テスト
    void Test();


    LRESULT OnCreate    (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnDestroy   (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnSetFocus  (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnKillFocus (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnSize      (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnHScroll   (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnVScroll   (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnKeyDown   (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnChar      (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnImeChar   (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnImeStartComposition (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnImeComposition      (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnMouseMove           (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnLButtonDown         (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnLButtonUp           (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnRButtonDown         (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnLButtonDblClick     (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnMouseWheel          (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnPaint               (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnNcPaint             (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnTheeChanged         (const WPARAM wParam, const LPARAM lParam);
    //LRESULT OnGetText             (const WPARAM wParam, const LPARAM lParam);
    //LRESULT OnSetFont             (const WPARAM wParam, const LPARAM lParam);
    //LRESULT OnClear               (const WPARAM wParam, const LPARAM lParam);
    //LRESULT OnCopy                (const WPARAM wParam, const LPARAM lParam);
    //LRESULT OnCut                 (const WPARAM wParam, const LPARAM lParam);
    //LRESULT OnPaste               (const WPARAM wParam, const LPARAM lParam);

    LRESULT OnCanUndo               (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnCanRedo               (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnEmptyUndoBuffer       (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnGetFirstVisibleLine   (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnGetLine               (const WPARAM wParam, const LPARAM lParam);
    //LRESULT OnGetLineCount          (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnGetModify             (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnGetRect               (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnGetSel                (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnLimitText             (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnLineFromChar          (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnLineIndex             (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnLineLength            (const WPARAM wParam, const LPARAM lParam);

    LRESULT OnLineScroll            (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnReplaceSel            (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnScroll                (const WPARAM wParam, const LPARAM lParam);

    LRESULT OnScrollCaret           (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnSetModify             (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnGetReadOnly           (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnSetReadOnly           (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnSetRect               (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnSetRectNp             (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnSetSel                (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnSetAbsTops            (const WPARAM wParam, const LPARAM lParam);

    LRESULT OnGetBufferInfo         (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnReflect               (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnGetWordwrap           (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnSetWordwrap           (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnGetMemSize            (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnSetMem                (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnGetMem                (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnContextMenu           (const WPARAM wParam, const LPARAM lParam);
};

#endif
