
// MtEditorView.cpp : CMtEditorView クラスの実装
//

#include "stdafx.h"
#include "MtEditor.h"

#include "MtEditorDoc.h"
#include "MtEditorView.h"
//#include "MtEditorEngine.h"
#include "MtEditorWindow.h"
#include "DlgSetting.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif
using namespace MT_EDITOR;


// CMtEditorView


/*
IMPLEMENT_DYNCREATE(CCompelitionBox, CComboBoxEx)

BEGIN_MESSAGE_MAP(CCompelitionBox, CComboBoxEx)

END_MESSAGE_MAP()

*/


CCompelitionBox::CCompelitionBox()
{
}


CCompelitionBox:: ~CCompelitionBox()
{
}

void CCompelitionBox:: Create(CWnd * pParent, UINT iId)
{
    //CComboBox::Create(CBS_SIMPLE ,CRect(0,0,100,200), pParent ,iId);
    CListBox::Create(WS_VISIBLE|LBS_NOTIFY ,CRect(0,0,100,200), pParent ,iId);
    ShowWindow(SW_HIDE);
}

BOOL CCompelitionBox::PreTranslateMessage(MSG* pMsg)
{
    CWnd* pParent;
    if(pMsg->message == WM_KEYDOWN)
    {
        pParent = GetParent();
        if(pMsg->hwnd == pParent->m_hWnd)
        {

            switch(pMsg->wParam)
            {
            case VK_DOWN:
               break;
            case VK_UP:
                break;
            }

        }
    }
    return true;
}

void CCompelitionBox::Open(POINT pt)
{
    //デバッグ用

    //Clear();
    ResetContent();
    /*
    COMBOBOXEXITEM item;
    memset(&item, 0, sizeof(item));
    
    item.mask = CBEIF_TEXT;
    item.iItem = 0;
    item.pszText = _T("hello1");
    InsertItem( &item);

    item.pszText = _T("hello2");
    InsertItem( &item);

    item.pszText = _T("hello3");
    InsertItem( &item);
    */
    AddString(_T("Hello1"));
    AddString(_T("Hello2"));
    AddString(_T("Hello3"));
    SetWindowPos(NULL,pt.x,pt.y,0,100,SWP_SHOWWINDOW);


}





IMPLEMENT_DYNCREATE(CMtEditorView, CView)

BEGIN_MESSAGE_MAP(CMtEditorView, CView)
	// 標準印刷コマンド
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CMtEditorView::OnFilePrintPreview)
    ON_COMMAND(ID_MNU_EXEC_LINE, &CMtEditorView::OnMnuExecLine)
    ON_WM_TIMER()
    ON_UPDATE_COMMAND_UI(ID_MNU_EXEC_LINE, &CMtEditorView::OnUpdateMnuExecLine)
    ON_COMMAND(ID_NMU_SETTING, &CMtEditorView::OnMnuSetting)
    ON_COMMAND(ID_EDIT_COPY, &CMtEditorView::OnEditCopy)
    ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, &CMtEditorView::OnUpdateEditCopy)
    ON_COMMAND(ID_EDIT_CUT, &CMtEditorView::OnEditCut)
    ON_UPDATE_COMMAND_UI(ID_EDIT_CUT, &CMtEditorView::OnUpdateEditCut)
    ON_COMMAND(ID_EDIT_PASTE, &CMtEditorView::OnEditPaste)
    ON_UPDATE_COMMAND_UI(ID_EDIT_PASTE, &CMtEditorView::OnUpdateEditPaste)
    ON_COMMAND(ID_EDIT_UNDO, &CMtEditorView::OnEditUndo)
    ON_UPDATE_COMMAND_UI(ID_EDIT_UNDO, &CMtEditorView::OnUpdateEditUndo)
    ON_WM_CLOSE()
    ON_COMMAND(ID_COMPLICATION, &CMtEditorView::OnComplication)
    ON_WM_CREATE()
END_MESSAGE_MAP()

// CMtEditorView コンストラクション/デストラクション

CMtEditorView::CMtEditorView():
m_pEditor(NULL)
{

    m_pEditor = new CMtEditorWindow;
    bExecTimer = false;
    //m_Compelition = std::make_unique<CCompelitionBox>();
    //m_Compelition = new CCompelitionBox;
    m_Compelition = new CListBox;
    
}

CMtEditorView::~CMtEditorView()
{
    if (m_pEditor)
    {
        delete m_pEditor;
    }
}

BOOL CMtEditorView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: この位置で CREATESTRUCT cs を修正して Window クラスまたはスタイルを
	//  修正してください。

	return CView::PreCreateWindow(cs);
}

// CMtEditorView 描画

void CMtEditorView::OnDraw(CDC* /*pDC*/)
{
	CMtEditorDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: この場所にネイティブ データ用の描画コードを追加します。
}


// CMtEditorView 印刷


void CMtEditorView::OnFilePrintPreview()
{
	AFXPrintPreview(this);
}

BOOL CMtEditorView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 既定の印刷準備
	return DoPreparePrinting(pInfo);
}

void CMtEditorView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 印刷前の特別な初期化処理を追加してください。
}

void CMtEditorView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 印刷後の後処理を追加してください。
}

void CMtEditorView::OnRButtonUp(UINT nFlags, CPoint point)
{
	ClientToScreen(&point);
	OnContextMenu(this, point);
}

void CMtEditorView::OnContextMenu(CWnd* pWnd, CPoint point)
{
	theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDIT, point.x, point.y, this, TRUE);
}


// CMtEditorView 診断

#ifdef _DEBUG
void CMtEditorView::AssertValid() const
{
	CView::AssertValid();
}

void CMtEditorView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMtEditorDoc* CMtEditorView::GetDocument() const // デバッグ以外のバージョンはインラインです。
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMtEditorDoc)));
	return (CMtEditorDoc*)m_pDocument;
}
#endif //_DEBUG


// CMtEditorView メッセージ ハンドラ

LRESULT CMtEditorView::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
    // TODO: ここに特定なコードを追加するか、もしくは基本クラスを呼び出してください。
    LRESULT ret;
    BOOL bOver;
    if (m_pEditor)
    {
        ret = m_pEditor->WindowProc(message, wParam, lParam, &bOver);
        if (message == WM_DESTROY)
        {
            delete m_pEditor;
            m_pEditor = NULL;
        }

        if (!bOver)
        {
            return CView::WindowProc(message, wParam, lParam);
        }
        return ret;
    }
    return CView::WindowProc(message, wParam, lParam);
}

BOOL CMtEditorView::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext)
{
    // TODO: ここに特定なコードを追加するか、もしくは基本クラスを呼び出してください。
    //m_Compelition = std::make_unique<CCompelitionBox>();
    return CView::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext);
}

void CMtEditorView::PreSubclassWindow()
{
    m_pEditor->Create(m_hWnd);

    CView::PreSubclassWindow();
}

void CMtEditorView::OnMnuExecLine()
{    
    if (bExecTimer)
    {
        KillTimer(500);
        bExecTimer = false;
    }
    else
    {
        iExecLine = 0;
        SetTimer(500, 1000, NULL);
        bExecTimer = true;
    }
}

void CMtEditorView::OnTimer(UINT_PTR nIDEvent)
{
    if (nIDEvent == 500)
    {
        iExecLine++;
        if (iExecLine >=  m_pEditor->GetLineCount())
        {
            iExecLine = 0;
        }
        m_pEditor->SetExecLine(iExecLine, TRUE);
    }

    CView::OnTimer(nIDEvent);
}

void CMtEditorView::OnUpdateMnuExecLine(CCmdUI *pCmdUI)
{
    pCmdUI->SetCheck(bExecTimer?TRUE:FALSE);
}

void CMtEditorView::OnMnuSetting()
{
    CDlgSetting dlgSetting;

    MT_EDITOR::EDITOR_SETTING es;

    m_pEditor->GetData(&es);
    
    dlgSetting.SetData(es);
    INT_PTR iRet = dlgSetting.DoModal();
    if (iRet == IDOK)
    {
        dlgSetting.GetData(&es);
        m_pEditor->SetData(es);
    }
}

void CMtEditorView::OnEditCopy()
{
    m_pEditor->Copy();
}

void CMtEditorView::OnUpdateEditCopy(CCmdUI *pCmdUI)
{
    // TODO: ここにコマンド更新 UI ハンドラ コードを追加します。
}

void CMtEditorView::OnEditCut()
{
    m_pEditor->Cut();
}

void CMtEditorView::OnUpdateEditCut(CCmdUI *pCmdUI)
{
    // TODO: ここにコマンド更新 UI ハンドラ コードを追加します。
}

void CMtEditorView::OnEditPaste()
{
    m_pEditor->Paste();
}

void CMtEditorView::OnUpdateEditPaste(CCmdUI *pCmdUI)
{
}

void CMtEditorView::OnEditUndo()
{
    m_pEditor->Undo();
}

void CMtEditorView::OnUpdateEditUndo(CCmdUI *pCmdUI)
{
    // TODO: ここにコマンド更新 UI ハンドラ コードを追加します。
}


void CMtEditorView::OnClose()
{
    KillTimer(500);

    CView::OnClose();
}


void CMtEditorView::OnComplication()
{
    CPoint pt = GetCaretPos();
    //m_Compelition->Open(pt);
    //m_Compelition->ShowWindow(SW_SHOW);
    m_Compelition->AddString(_T("Hello1"));
    m_Compelition->AddString(_T("Hello2"));
    m_Compelition->AddString(_T("Hello3"));
    m_Compelition->SetWindowPos(NULL,pt.x,pt.y,0,100,SWP_SHOWWINDOW);
    m_Compelition->ShowWindow(SW_SHOW);

}


void CMtEditorView::OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView)
{
    // TODO: ここに特定なコードを追加するか、もしくは基底クラスを呼び出してください。

    CView::OnActivateView(bActivate, pActivateView, pDeactiveView);
}


int CMtEditorView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
    if (CView::OnCreate(lpCreateStruct) == -1)
        return -1;

    //m_Compelition->Create(NULL, ID_COMPLICATION);    

    m_Compelition->Create(WS_VISIBLE|LBS_NOTIFY,CRect(0,0,100,200), this ,ID_COMPLICATION);
    m_Compelition->ShowWindow(SW_HIDE);

    return 0;
}
