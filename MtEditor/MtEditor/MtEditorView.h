
// MtEditorView.h : CMtEditorView クラスのインターフェイス
//
#include <memory>

//#include "MtEditorWindow.h"
#pragma once
class CMtEditorEngine;

namespace MT_EDITOR{
    class CMtEditorWindow;
}


class CCompelitionBox : public CListBox
{
public:

    CCompelitionBox();
    virtual ~CCompelitionBox();
    void Create(CWnd * pParent, UINT iId);


    virtual BOOL PreTranslateMessage(MSG* pMsg);

    void Open(POINT pt);

protected:
 
};

class CMtEditorView : public CView
{
protected: // シリアル化からのみ作成します。
	CMtEditorView();
	DECLARE_DYNCREATE(CMtEditorView)

    //CMtEditorEngine*  m_pEditor;
    MT_EDITOR::CMtEditorWindow*  m_pEditor;

    int iExecLine;

    bool bExecTimer;

    //std::unique_ptr<CCompelitionBox> m_Compelition;
    //CCompelitionBox* m_Compelition;
    CListBox* m_Compelition;

// 属性
public:
	CMtEditorDoc* GetDocument() const;

// 操作
public:

// オーバーライド
public:
	virtual void OnDraw(CDC* pDC);  // このビューを描画するためにオーバーライドされます。
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// 実装
public:
	virtual ~CMtEditorView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 生成された、メッセージ割り当て関数
protected:
	afx_msg void OnFilePrintPreview();
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	DECLARE_MESSAGE_MAP()
    virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
public:
    virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
protected:
    virtual void PreSubclassWindow();
public:
    afx_msg void OnMnuExecLine();
    afx_msg void OnTimer(UINT_PTR nIDEvent);
    afx_msg void OnUpdateMnuExecLine(CCmdUI *pCmdUI);
    afx_msg void OnMnuSetting();
    afx_msg void OnEditCopy();
    afx_msg void OnUpdateEditCopy(CCmdUI *pCmdUI);
    afx_msg void OnEditCut();
    afx_msg void OnUpdateEditCut(CCmdUI *pCmdUI);
    afx_msg void OnEditPaste();
    afx_msg void OnUpdateEditPaste(CCmdUI *pCmdUI);
    afx_msg void OnEditUndo();
    afx_msg void OnUpdateEditUndo(CCmdUI *pCmdUI);
    afx_msg void OnClose();
    afx_msg void OnComplication();
    virtual void OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView);
    afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
};

#ifndef _DEBUG  // MtEditorView.cpp のデバッグ バージョン
inline CMtEditorDoc* CMtEditorView::GetDocument() const
   { return reinterpret_cast<CMtEditorDoc*>(m_pDocument); }
#endif

