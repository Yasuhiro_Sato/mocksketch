#include "stdafx.h"
#include "MtEditorWindow.h"
#include <math.h>
#include <algorithm>

namespace MT_EDITOR
{
 

CMtEditorWindow::CMtEditorWindow(void):
m_iId           (0),
m_hWnd          (NULL),
m_hDc           (NULL),
m_hRetBmp       (NULL),
//m_hRgn          (NULL),
m_hFont         (NULL),
m_hOldFont      (NULL),
m_iFontHeight   (0),
m_iCharWidth    (0),
m_iScrollX      (0),
m_iScrollMaxX   (0),
m_iScrollY      (0),
m_iScrollMaxY   (0),
m_iTabStop      (4),
m_iLeftMargin   (0),
m_iTopMargin    (0),
m_iRightMargin  (0),
m_iBottomMargin (0),
m_iSpacing      (0),
m_iLineCharMax  (0),
m_iLinePixlWidthMax (0),
m_iWidth        (0),
m_iHeight       (0),
m_bMousedown    (false),
m_iClickBreakPoint    (0),
m_bInsertMode   (true),
m_iCaretXPixel  (0),
m_bReadOnly     (false),
m_iMargin       (0),
m_iLineNum      (0),
m_iExecLine     (-1),
m_bVisibleCaret (false),
m_pSetBreakPointModified    (NULL),
m_pParent       (NULL),

#ifdef OP_XP_STYLE
F_CloseThemeData(NULL),
F_OpenThemeData (NULL),
F_DrawThemeBackground(NULL),
#endif	// OP_XP_STYLE

m_iIndicatorWidth   (20),
m_iLineNumberWidth  (0),
m_iLineNumberDigit  (5),
m_crIndicator   (GetSysColor(COLOR_3DFACE))
{
    memset(m_cWidth, 0, sizeof(m_cWidth));
    RecalcMargin();
#ifdef _TEST
    Test();
#endif 
}

CMtEditorWindow::~CMtEditorWindow(void)
{

}


void CMtEditorWindow::SetData(const EDITOR_SETTING& setting)
{
    bool bSetFont = false;
    
    if  ((m_es.lf.lfHeight != setting.lf.lfHeight) ||
         (m_es.lf.lfWidth != setting.lf.lfWidth) ||
         (m_es.lf.lfEscapement != setting.lf.lfEscapement) ||
         (m_es.lf.lfOrientation != setting.lf.lfOrientation) ||
         (m_es.lf.lfWeight != setting.lf.lfWeight) ||
         (m_es.lf.lfItalic != setting.lf.lfItalic) ||
         (m_es.lf.lfUnderline != setting.lf.lfUnderline) ||
         (m_es.lf.lfStrikeOut != setting.lf.lfStrikeOut) ||
         (m_es.lf.lfCharSet != setting.lf.lfCharSet) ||
         (m_es.lf.lfOutPrecision != setting.lf.lfOutPrecision) ||
         (m_es.lf.lfClipPrecision != setting.lf.lfClipPrecision) ||
         (m_es.lf.lfQuality != setting.lf.lfQuality) ||
         (m_es.lf.lfPitchAndFamily != setting.lf.lfPitchAndFamily) ||
         (_tcscmp(m_es.lf.lfFaceName, setting.lf.lfFaceName) != 0))
    {
        bSetFont = true;
    }

    m_es = setting;

    RecalcMargin();


    if (bSetFont)
    {
        SetFont(m_es.lf);
    }
    else
    {
        Redraw();
    }
}

void CMtEditorWindow::GetData(EDITOR_SETTING* pSetting)
{
    *pSetting = m_es;
}

//!<  スクロールバーの設定
void CMtEditorWindow::SetScrollbar()
{
    SCROLLINFO si;
    RECT rect;

    GetEditRect(&rect);

    // 横スクロールバー
    if (m_es.bWordwrap == false && (
        rect.right - rect.left) < m_iLinePixlWidthMax) {
        int width = m_iLinePixlWidthMax / m_iCharWidth + 1;

        ::EnableScrollBar(m_hWnd, SB_HORZ, ESB_ENABLE_BOTH);

        m_iScrollMaxX = width - ((rect.right - rect.left) / m_iCharWidth);
        m_iScrollX = (m_iScrollX < m_iScrollMaxX) ? m_iScrollX : m_iScrollMaxX;

        ZeroMemory(&si, sizeof(SCROLLINFO));
        si.cbSize = sizeof(SCROLLINFO);
        si.fMask = SIF_POS | SIF_RANGE | SIF_PAGE;
        si.nPage = (rect.right - rect.left) / m_iCharWidth;
        si.nMax = width - 1;
        si.nPos = m_iScrollX;
        SetScrollInfo(m_hWnd, SB_HORZ, &si, TRUE);
    } 
    else 
    {
        EnableScrollBar(m_hWnd, SB_HORZ, ESB_DISABLE_BOTH);

        m_iScrollX = m_iScrollMaxX = 0;

        ZeroMemory(&si, sizeof(SCROLLINFO));
        si.cbSize = sizeof(SCROLLINFO);
        si.fMask = SIF_POS | SIF_PAGE | SIF_RANGE;
        si.nMax = (m_es.bWordwrap == true) ? 0 : 1;
        SetScrollInfo(m_hWnd, SB_HORZ, &si, TRUE);
    }

    // 縦スクロールバー
    if (m_iLineNum < static_cast<int>(m_lstLineHead.size()))
    {
        EnableScrollBar(m_hWnd, SB_VERT, ESB_ENABLE_BOTH);

        m_iScrollMaxY = GetLineCount() - m_iLineNum;
        m_iScrollY = (m_iScrollY < m_iScrollMaxY) ? m_iScrollY : m_iScrollMaxY;

        ZeroMemory(&si, sizeof(SCROLLINFO));
        si.cbSize = sizeof(SCROLLINFO);
        si.fMask = SIF_POS | SIF_RANGE | SIF_PAGE;
        si.nPage = m_iLineNum;
        si.nMax = SizeToInt(m_lstLineHead.size()) - 1;
        si.nPos = m_iScrollY;
        SetScrollInfo(m_hWnd, SB_VERT, &si, TRUE);
    } 
    else 
    {
        EnableScrollBar(m_hWnd, SB_VERT, ESB_DISABLE_BOTH);

        m_iScrollY = m_iScrollMaxY = 0;

        ZeroMemory(&si, sizeof(SCROLLINFO));
        si.cbSize = sizeof(SCROLLINFO);
        si.fMask = SIF_POS | SIF_PAGE | SIF_RANGE;
        si.nMax = 1;
        SetScrollInfo(m_hWnd, SB_VERT, &si, TRUE);
    }
}


//!< フォント設定
void CMtEditorWindow::SetFont(LOGFONT lf)
{
    if (m_hFont != NULL) 
    {
        SelectObject(m_hDc, m_hOldFont);
        DeleteObject(m_hFont);
    }
    
    ZeroMemory(m_cWidth, sizeof(BYTE) * 256);

    m_hFont = CreateFontIndirect((CONST LOGFONT *)&lf);
    m_hOldFont = static_cast<HFONT>(SelectObject(m_hDc, m_hFont));

    SetDrawInfo(true);
    DrawFree();
    DrawInit();
    
    return;

}


//!< フォント設定
void CMtEditorWindow::SetFont(const HFONT hFont)
{
   // フォントを設定
    LOGFONT lf;

    if (m_hFont != NULL) 
    {
        SelectObject(m_hDc, m_hOldFont);
        DeleteObject(m_hFont);
    }

    ZeroMemory(m_cWidth, sizeof(BYTE) * 256);

    if (GetObject((HGDIOBJ)hFont, sizeof(LOGFONT), &lf) == 0) 
    {
        return;
    }

    m_hFont = CreateFontIndirect((CONST LOGFONT *)&lf);
    m_hOldFont = static_cast<HFONT>(SelectObject(m_hDc, m_hFont));

    SetDrawInfo(true);
    DrawFree();
    DrawInit();
    
    return;
}

LRESULT CMtEditorWindow::OnSize(const WPARAM wParam, const LPARAM lParam)
{
    RECT rect;
    HIMC hIMC;
    POINT pt;
    COMPOSITIONFORM cf;

    if (wParam == SIZE_MINIMIZED) 
    {
        return 0;
    }

    int iWidth  = LOWORD(lParam);
    int iHeight = HIWORD(lParam);

    LINE_POS posTopLeft;
    LINE_POS posTopLeftData;
    if (m_lstLineHead.size() > 0)
    {
        posTopLeft.Set(m_iScrollY, m_iScrollX);
        posTopLeftData = CaretToDataPos(posTopLeft);
    }

    GetEditRect(&rect);

    RecalcMargin();

    m_iWidth = (rect.right - rect.left);
    m_iHeight = rect.bottom - rect.top;

    if (m_iWidth < 0) {m_iWidth = 0;}
    if (m_iHeight < 0){m_iHeight = 0;}

    m_iLineNum = m_iHeight / m_iFontHeight;

    CalcAllLine();

    if (m_lstLineHead.size() > 0)
    {
        posTopLeft = DataPosToCaret(posTopLeftData);
        m_iScrollY = posTopLeft.iLine;
        m_iScrollX = posTopLeft.iCol;
    }

    m_posPresetStart.Set(-1,0);
    m_posPresetEnd.Set(-1,0);

    SetScrollbar();

    DrawFree();
    DrawInit();

    Redraw();

    // IMEの設定
    hIMC = ImmGetContext(m_hWnd);
    GetCaretPos(&pt);
    cf.dwStyle = CFS_POINT | CFS_RECT;
    cf.ptCurrentPos.x = pt.x;
    cf.ptCurrentPos.y = pt.y + (m_iSpacing / 2);
    GetEditRect(&cf.rcArea);
    ImmSetCompositionWindow(hIMC, &cf);
    ImmReleaseContext(m_hWnd, hIMC);
    return 0;
}

LRESULT CMtEditorWindow::OnImeChar(const WPARAM wParam, const LPARAM lParam)
{
    StdString strIn;
    if (GetKeyState(VK_CONTROL) < 0) 
    {
        return 0;
    }
#ifdef UNICODE
    strIn = static_cast<const TCHAR>(wParam);
    Insert(strIn);
#else
    in[0] = wParam >> 8;
    in[1] = wParam & 0xFF;
    InsertString(in, 2, m_bInsertMode);
#endif

DB_PRINT(_T("OnImeChar\n"));

    return 0;
}

LRESULT CMtEditorWindow::OnImeStartComposition (const WPARAM wParam, const LPARAM lParam)
{
    HIMC hIMC;
    POINT pt;			
    LOGFONT lf;
    COMPOSITIONFORM cf;

    EnsureVisible();

    hIMC = ImmGetContext(m_hWnd);
    if (m_hFont != NULL) 
    {
        // フォントの設定
        GetObject(m_hFont, sizeof(LOGFONT), &lf);
        ImmSetCompositionFont(hIMC, &lf);
    }
    // 位置の設定
    GetCaretPos(&pt);
    cf.dwStyle = CFS_POINT | CFS_RECT;
    cf.ptCurrentPos.x = pt.x;
    cf.ptCurrentPos.y = pt.y + (m_iSpacing / 2);
    GetEditRect(&cf.rcArea);
    ImmSetCompositionWindow(hIMC, &cf);
    ImmReleaseContext(m_hWnd, hIMC);

    ::HideCaret(m_hWnd);
    m_bVisibleCaret = false;
DB_PRINT(_T("OnImeStartComposition\n"));
    return DefWindowProc(m_hWnd, WM_IME_STARTCOMPOSITION, wParam, lParam);
}

LRESULT CMtEditorWindow::OnImeComposition(const WPARAM wParam, const LPARAM lParam)
{
    POINT pt;
    int len;
    HIMC hIMC;
    COMPOSITIONFORM cf;

    if (!m_bReadOnly && 
        (lParam & GCS_RESULTSTR)) 
    {

        // 確定文字列をバッファに追加
        hIMC = ::ImmGetContext(m_hWnd);
        len = ::ImmGetCompositionString(hIMC, GCS_RESULTSTR, NULL, 0);

        StdString strIn;
        strIn.resize(len / 2);
        ImmGetCompositionString(hIMC, GCS_RESULTSTR, &strIn[0], len);
        Insert(strIn);

        // 位置の設定
        GetCaretPos(&pt);
        cf.dwStyle = CFS_POINT | CFS_RECT;
        cf.ptCurrentPos.x = pt.x;
        cf.ptCurrentPos.y = pt.y + (m_iSpacing / 2);
        GetEditRect(&cf.rcArea);
        ImmSetCompositionWindow(hIMC, &cf);
        ImmReleaseContext(m_hWnd, hIMC);
DB_PRINT(_T("OnImeComposition 1\n"));
        return 0;
    }
DB_PRINT(_T("OnImeComposition 2\n"));

    return DefWindowProc(m_hWnd, WM_IME_COMPOSITION, wParam, lParam);
}

LRESULT CMtEditorWindow::OnImeEndComposition(const WPARAM wParam, const LPARAM lParam)
{
DB_PRINT(_T("OnImeEndComposition 2\n"));

/*
    COMPOSITIONFORM cf;
    HIMC hIMC = ImmGetContext(m_hWnd);

    cf.dwStyle = CFS_POINT | CFS_RECT;
    cf.ptCurrentPos.x = 0;
    cf.ptCurrentPos.y = 0;

    ImmSetCompositionWindow(hIMC, &cf);
    ImmReleaseContext(m_hWnd, hIMC);
*/
    ::ShowCaret(m_hWnd);
    return DefWindowProc( m_hWnd, WM_IME_ENDCOMPOSITION, wParam, lParam );
;
}

LRESULT CMtEditorWindow::OnLButtonDblClick(const WPARAM wParam, const LPARAM lParam)
{
    int iX = LOWORD(lParam);
    int iY = HIWORD(lParam);
    SetFocus(m_hWnd);

    LINE_POS pos = CaretPointToCaret( iX, iY);;

    SelectWord(pos);

    m_posLastDblClick.x = iX;
    m_posLastDblClick.y = iY;
    m_dwLastDblClickTime = ::GetTickCount();

    return 0;
}


LRESULT CMtEditorWindow::OnMouseWheel(const WPARAM wParam, const LPARAM lParam)
{
    for (int i = 0; i < 3; i++) {
        SendMessage(m_hWnd, WM_VSCROLL, ((short)HIWORD(wParam) > 0) ? SB_LINEUP : SB_LINEDOWN, 0);
    }
    return 0;
}


/*
* string_to_clipboard - 文字列をクリップボードに設定
*/
bool CMtEditorWindow::StringToClipboard( LINE_POS posDispStart, 
                                         LINE_POS posDispEnd)
{
    HANDLE hMem;
    TCHAR *buf;

    if (::OpenClipboard(m_hWnd) == FALSE) 
    {
        return false;
    }

    if (::EmptyClipboard() == FALSE) 
    {
        ::CloseClipboard();
        return false;
    }

    StdString strIn;
    m_Buf.GetText(&strIn, posDispStart, posDispEnd); 

    hMem = ::GlobalAlloc(GHND, sizeof(TCHAR) * (strIn.size() + 1) );
    if (hMem == NULL)
    {
        ::CloseClipboard();
        return false;
    }

    buf = reinterpret_cast<TCHAR *>(::GlobalLock(hMem));
    if (buf == NULL) 
    {
        ::GlobalFree(hMem);
        ::CloseClipboard();
        return false;
    }

    ::CopyMemory(buf, strIn.c_str(), sizeof(TCHAR) * (strIn.size() + 1));
    *(buf + strIn.size()) = TEXT('\0');
    ::GlobalUnlock(hMem);

#ifdef UNICODE
    ::SetClipboardData(CF_UNICODETEXT, hMem);
#else
    ::SetClipboardData(CF_TEXT, hMem);
#endif

    ::CloseClipboard();
    return true;
}

//!< 描画情報の解放
void CMtEditorWindow::DrawFree()
{
    HBITMAP hBmp;

    hBmp = static_cast<HBITMAP>(SelectObject(m_hDc, m_hRetBmp));
    DeleteObject(hBmp);
    //DeleteObject(m_hRgn);
    //m_hRgn = NULL;
}

//!< 描画情報の初期化
bool CMtEditorWindow::DrawInit()
{
    HDC hdc;
    HBITMAP hBmp;
    //HRGN hrgn[2];
    RECT rect;
    TEXTMETRIC tm;

    GetClientRect(m_hWnd, &rect);
    GetTextMetrics(m_hDc, &tm);

    hdc = GetDC(m_hWnd);
    hBmp = CreateCompatibleBitmap(hdc, rect.right, tm.tmHeight + m_iSpacing);
    m_hRetBmp = static_cast<HBITMAP>(SelectObject(m_hDc, hBmp));
    ReleaseDC(m_hWnd, hdc);

    /*
    // リージョンの作成
    hrgn[0] = CreateRectRgnIndirect(&rect);
    m_hRgn = CreateRectRgnIndirect(&rect);
    // 除去するリージョンの作成
    GetEditRect(&rect);
    hrgn[1] = CreateRectRgnIndirect(&rect);
    // リージョンの結合
    CombineRgn(m_hRgn, hrgn[0], hrgn[1], RGN_DIFF);
    DeleteObject(hrgn[0]);
    DeleteObject(hrgn[1]);
    return TRUE;
    */
    return true;
}

void CMtEditorWindow::Create(HWND hWnd)
{
    m_hWnd = hWnd;
    m_Buf.SetWindow(hWnd);
}


LRESULT CMtEditorWindow::OnCreate(const WPARAM wParam, const LPARAM lParam)
{
    HDC hdc;

#ifdef OP_XP_STYLE
    // XP
    if ((m_hModThemes = LoadLibrary(TEXT("uxtheme.dll"))) != NULL) 
    {
        if (F_OpenThemeData == NULL) 
        {
            F_OpenThemeData = reinterpret_cast<OPENTHEMEDATA>(::GetProcAddress(m_hModThemes, "OpenThemeData"));
        }

        if (F_OpenThemeData != NULL) 
        {
            m_hTheme = F_OpenThemeData(m_hWnd, L"Edit");
        }
    }
#endif	// OP_XP_STYLE

    m_iId = (int)((LPCREATESTRUCT)lParam)->hMenu;


    SetLeftMargin(3);
    m_iTopMargin = m_iRightMargin = m_iBottomMargin = 1;
    m_iSpacing = 0;

    // 描画情報の初期化
    hdc = GetDC(m_hWnd);
    m_hDc = CreateCompatibleDC(hdc);
    ReleaseDC(m_hWnd, hdc);
    DrawInit();

    SetMapMode(m_hDc, MM_TEXT);
    SetTextCharacterExtra(m_hDc, 0);
    SetTextJustification(m_hDc, 0, 0);
    SetTextAlign(m_hDc, TA_TOP | TA_LEFT);
    SetBkMode(m_hDc, TRANSPARENT);

    // buffer info to window long
    //SetWindowLong(m_hWnd, GWL_USERDATA, (LPARAM)bf);
    SetDrawInfo(true);


    /*
    // 初期文字列の設定
    if (((LPCREATESTRUCT)lParam)->lpszName != NULL) 
    {
        SendMessage(m_hWnd, WM_SETTEXT, 0, (LPARAM)((LPCREATESTRUCT)lParam)->lpszName);
    }
    */
    return 0;
}

/**
 * @brief   描画領域の取得
 * @param   [out] rect 描画領域
 * @return  なし
 * @note     
 */
bool CMtEditorWindow::GetEditRect(RECT *rect) const
{
    bool ret;

    ret = (GetClientRect(m_hWnd, rect) == TRUE) ? true: false;

    rect->left += m_iMargin;
    rect->top += m_iTopMargin;
    rect->right -= m_iRightMargin;
    rect->bottom -= m_iBottomMargin;
    return ret;
}

/**
 * @brief    文字の描画幅の取得
 * @param   [in] str
 * @param   [in] ret_len 文字数
 * @retval  文字の描画幅
 * @note
 */
int CMtEditorWindow::GetCharExtent(const TCHAR *str, int iLen)
{
    SIZE sz;
    int ret = 0;

#ifdef UNICODE
    //*ret_len = 1;
    if (HIBYTE(*str) == 0 && 
        *(m_cWidth + LOBYTE(*str)) != 0) 
    {
        ret = *(m_cWidth + LOBYTE(*str));
    }
    else
    {
        GetTextExtentPoint32(m_hDc,     // デバイスコンテキストのハンドル
                             str,       // 文字列
                             iLen,  // 文字列内の文字数
                             &sz);      // 文字列のサイズ
        ret = sz.cx;
        if (HIBYTE(*str) == 0 && sz.cx < 256) 
        {
            *(m_cWidth + LOBYTE(*str)) = (BYTE)sz.cx;
        }
    }
#else
    *ret_len = (IsLeadByte(str) == TRUE) ? 2 : 1;
    if (iLen == 1 && *(m_cWidth + (unsigned char)*str) != 0) {
        ret = *(m_cWidth + (unsigned char)*str);
    } else {
        GetTextExtentPoint32(m_hDc, str, iLen, &sz);
        ret = sz.cx;
        if (iLen == 1 && sz.cx < 256) {
            *(m_cWidth + (unsigned char)*str) = (BYTE)sz.cx;
        }
    }
#endif
    return ret;
}

LRESULT CMtEditorWindow::OnPaint(const WPARAM wParam, const LPARAM lParam)
{
    PAINTSTRUCT ps;
    HBRUSH hBrush;
    HBRUSH brIndicator;
    RECT rect;
    RECT rcIndicator;
    HDC hdc;

    hdc = ::BeginPaint(m_hWnd, &ps);
   
    ::GetClientRect(m_hWnd, &rect);
    ::SetRect(&rect, ps.rcPaint.left, 0, ps.rcPaint.right, m_iFontHeight);
    ::SetRect(&rcIndicator, 0, 0, m_iIndicatorWidth, m_iFontHeight);

#ifdef _WIN64
    LONG_PTR lStyle = GetWindowLongPtr(m_hWnd, GWL_STYLE);
#else
    LONG lStyle = GetWindowLong(m_hWnd, GWL_STYLE);
#endif

    //m_bNoHideSel = (lStyle & ES_NOHIDESEL) ? TRUE : FALSE;
    // caret pos

    int iCaretLine = -1;    //キャレットが存在する行
    if (GetFocus() == m_hWnd) 
    {
        //iCaretLine = IndexToLine(m_dwCaretPos);
    }

    ::SetTextColor(m_hDc, GetSysColor(COLOR_WINDOWTEXT));
    ::SetBkColor(m_hDc, GetSysColor(COLOR_WINDOW));

    hBrush = ::CreateSolidBrush(GetSysColor(COLOR_WINDOW));
    brIndicator = ::CreateSolidBrush(m_crIndicator);

    int iTopLine     = m_iScrollY + (ps.rcPaint.top / m_iFontHeight) - 1;
    int iBottomLine  = m_iScrollY + (ps.rcPaint.bottom / m_iFontHeight) + 1;

    if (iTopLine < 0)    {iTopLine = 0;}
    if (iBottomLine < 0) {iBottomLine = 0;}

    int iLinePos = iTopLine;

    int iCaretY = -1;    //キャレットY座標
    int iCaretX = -1;    //キャレットX座標

    int iLineY = 0;

    for (;iLinePos < iBottomLine; iLinePos++) 
    {
        if (iLinePos >= static_cast<int>(m_lstLineHead.size()))
        {
            break;
        }

        if (iLinePos == m_posCaret.iLine) 
        {
            // キャレットの設定
            if (!m_bInsertMode) 
            {
                SetCaretSize(m_posCaret);
            }

            iCaretX = CalcCaretXPos(m_hDc, m_posCaret);
            iCaretY = (iLinePos - m_iScrollY) * m_iFontHeight + m_iTopMargin;
        }

        // 1行描画
        ::FillRect(m_hDc, &rect, hBrush);
        ::FillRect(m_hDc, &rcIndicator, brIndicator);
        iLineY = (iLinePos - m_iScrollY) * m_iFontHeight + m_iTopMargin;
        DrawLine(m_hDc, iLinePos, ps.rcPaint.left, ps.rcPaint.right);
        ::BitBlt(hdc,
            ps.rcPaint.left, iLineY,
            ps.rcPaint.right, m_iFontHeight,
            m_hDc, ps.rcPaint.left, 0, SRCCOPY);
    }

    iLineY = (iLinePos - m_iScrollY) * m_iFontHeight + m_iTopMargin;
    if (iLineY < ps.rcPaint.bottom) 
    {
        // 文末の余白描画
        ::SetRect(&rect,
            ps.rcPaint.left, iLineY,
            ps.rcPaint.right, ps.rcPaint.bottom);
        ::FillRect(hdc, &rect, hBrush);
        ::SetRect(&rcIndicator, 0, iLineY, 
            m_iIndicatorWidth, ps.rcPaint.bottom);
        ::FillRect(hdc, &rcIndicator, brIndicator);
    }
    // 矩形描画

    ::SetRect(&rect,
        m_iMargin - m_iLeftMargin, 
        ps.rcPaint.top,
        m_iMargin, 
        ps.rcPaint.bottom);
    ::FillRect(hdc, &rect, hBrush);

    ::DeleteObject(hBrush);
    ::DeleteObject(brIndicator);

    if (iCaretX != -1 && 
        GetFocus() == m_hWnd) 
    {
        //////////////////////////////////
        if ( (iCaretX < m_iMargin))
        {
            if(m_bVisibleCaret)
            {
                ::HideCaret(m_hWnd);
                m_bVisibleCaret = false;
            }
        }
        else
        {
            if(!m_bVisibleCaret)
            {
                ::ShowCaret(m_hWnd);
                m_bVisibleCaret = true;
            }
        }
        //////////////////////////////////
        ::SetCaretPos(iCaretX, iCaretY);
    }
    ::EndPaint(m_hWnd, &ps);
    return 0;
}



//!< ウィンドウプロシージャ
LRESULT  CMtEditorWindow::WindowProc(const UINT msg, const WPARAM wParam, const LPARAM lParam, BOOL* pOverRide)
{
    bool        bOverRide = FALSE;
    LRESULT     lRet = 0;
    switch (msg) 
    {
    case WM_CREATE:     {lRet = OnCreate   (wParam, lParam); bOverRide = FALSE; break;}
    case WM_SETFOCUS:   {lRet = OnSetFocus (wParam, lParam); bOverRide = FALSE; break;}
    case WM_SIZE:       {lRet = OnSize     (wParam, lParam); bOverRide = FALSE; break;}
    case WM_PAINT:      {lRet = OnPaint    (wParam, lParam); break;}
    case WM_SETFONT:    {SetFont(reinterpret_cast<const HFONT>(wParam)); break;}
    case WM_KEYDOWN:    {lRet = OnKeyDown  (wParam, lParam); break;}
    case WM_CHAR:       {lRet =  OnChar     (wParam, lParam); break;}
    case WM_MOUSEMOVE:  {lRet =  OnMouseMove  (wParam, lParam); break;}
    case WM_LBUTTONDOWN:{lRet =  OnLButtonDown(wParam, lParam); break;}
    case WM_LBUTTONUP:  {lRet =  OnLButtonUp  (wParam, lParam); break;}
    case WM_LBUTTONDBLCLK:  {lRet =  OnLButtonDblClick  (wParam, lParam);break;}

    case WM_HSCROLL:    {lRet = OnHScroll  (wParam, lParam); break;}
    case WM_VSCROLL:    {lRet = OnVScroll  (wParam, lParam); break;}
    case WM_IME_CHAR:   {lRet =  OnImeChar  (wParam, lParam);  bOverRide = TRUE; break;}
    case WM_IME_STARTCOMPOSITION:   {lRet =  OnImeStartComposition  (wParam, lParam); bOverRide = TRUE;break;}
    case WM_IME_COMPOSITION:        {lRet =  OnImeComposition       (wParam, lParam); bOverRide = TRUE;break;}
    case WM_IME_ENDCOMPOSITION:     {lRet =  OnImeEndComposition       (wParam, lParam);; bOverRide = TRUE;break;}
    case WM_MOUSEWHEEL:             {lRet =  OnMouseWheel       (wParam, lParam); break;}

    case WM_GETFONT:        {lRet = (LRESULT)m_hFont; break;}
    case WM_CLEAR:          {Clear(); break;}
    case WM_COPY:           {Copy(); break;}
    case WM_CUT:            {Cut(); break;}
    case WM_PASTE:          {Paste(); break;}


    

    default:
        break;
    }
    if(pOverRide)
    {
        *pOverRide = bOverRide;
    }
    return 0;

}

/**
 *  @brief   ブレークポイント全解除
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void CMtEditorWindow::ClearBreakPoint()
{
    m_Buf.ClearBreakPoint();
}

/**
 *  @brief   ブレークポイントリスト取得
 *  @param   [out] pBreakPointList ブレークポイントリスト
 *  @retval  なし
 *  @note
 */
void CMtEditorWindow::GetBreakPoint 
    (std::vector<BREAK_POINT_DATA>* pBreakPointList) const
{
    m_Buf.GetBreakPointList(pBreakPointList);
}

/**
 *  @brief   ブレークポイント設定
 *  @param   [in] pBreakPointList ブレークポイントリスト
 *  @retval  なし
 *  @note
 */
void CMtEditorWindow::SetBreakPoint
                (const std::vector<BREAK_POINT_DATA>& lstBreakPoint)
{
    m_Buf.SetBreakPointList(lstBreakPoint);
}

/**
 *  @brief   ブレークポイント追加
 *  @param   [in] iLine   追加行
 *  @param   [in] bEnable 有効・追加行
 *  @retval  なし
 *  @note
 */
bool CMtEditorWindow::AddBreakPoint(const int iLine, bool bEnable)
{
   return m_Buf.AddBreakPoint(iLine, bEnable);
}

/**
 *  @brief   ブレークポイント削除
 *  @param   [in] pBreakPointList ブレークポイントリスト
 *  @retval  なし
 *  @note
 */
bool CMtEditorWindow::DelBreakPoint(const int iLine)
{
    return m_Buf.DelBreakPoint(iLine);
}

/**
 *  @brief   ブレークポイント問合せ
 *  @param   [in] iLine   問合行
 *  @retval  true ブレークポイント設定
 *  @note
 */
bool CMtEditorWindow::IsBreakPoint(const int iLine)
{
    BRAKE_POINT_STS eSet;
    eSet =m_Buf.GetBreakPoint(iLine);

    if (eSet == BP_ENABLE)
    {
        return true;
    }
    return false;
}

/**
 * @brief   ブレークポイントコールバック
 * @param   [in] pParent   行位置
 * @param   [in] pSetBreakPointModified
 * @retval  true 成功
 * @note
 */
bool CMtEditorWindow::SetBreakPointCallback(void* pParent, void (CALLBACK* pSetBreakPointModified)(void* pData, int iLine, bool bSet))
{
    m_pParent = pParent;
    m_pSetBreakPointModified = pSetBreakPointModified;
    return true;
}

/**
 *  @brief   実行行の設定
 *  @param   [in] iLine   論理行(0 org)
 *  @param   [in] bEnsure true:画面に表示
 *  @retval  なし
 *  @note    
 */
void CMtEditorWindow::SetExecLine(int iLine, bool bEnsure)
{
    int iOldDispLine = -1;
    int iDispLine =  -1;

    if (m_iExecLine >= 0)
    {
        iOldDispLine = LineToDispLine(m_iExecLine);
    }

    if (iLine >= 0)
    {
        iDispLine = LineToDispLine(iLine);
    }

    if (bEnsure)
    {
        if (iDispLine >= 0)
        {
            if (!IsVisivleLine(iDispLine))
            {
                VisibleLine(iDispLine);
            }
        }
    }

    if (iOldDispLine >= 0)
    {
        DrawBreakPoint(iOldDispLine, true);
    }

    m_iExecLine = iLine;
    if (iDispLine >= 0)
    {
        DrawBreakPoint(iDispLine, true);
        DrawExecIndicator(iDispLine, true);
    }
}


/**
 *  @brief   文字列の設定
 *  @param   [in] pString  文字列
 *  @retval  なし
 *  @note    文字数
 */
bool CMtEditorWindow::SetString(const StdString& str)
{
    bool bRet;
    bRet = m_Buf.SetText(str.c_str());
    return true;
}

/**
 *  @brief   文字列の取得
 *  @param   [out] pString  文字列
 *  @retval  なし
 *  @note    文字数
 */
int CMtEditorWindow::GetString(StdString* pString)
{
    if(!m_Buf.GetText(pString))
    {   
        return -1;
    }
    return SizeToInt(pString->length());
}

/**
 *  @brief   行数取得
 *  @param   なし
 *  @retval  行数
 *  @note    論理行数を返す
 */
int CMtEditorWindow::GetLineCount()
{
    return SizeToInt(m_lstLineHead.size());
}



//!< UnDoの実行
bool CMtEditorWindow::Undo()
{
    return false;
}

//!< ReDoの実行
bool CMtEditorWindow::Redo()
{
    return false;
}

//!< 選択中テキスト消去
void CMtEditorWindow::Clear()
{
    Delete(true);
}

//!< 選択中テキストコピー
void CMtEditorWindow::Copy()
{
    // コピー
    if (m_posCaret == m_posSelectCaret) 
    {
        return;
    }
    StringToClipboard(m_posCaret, 
                      m_posSelectCaret);

    PresetRedrawArea(m_posCaret, m_posSelectCaret);
    m_posSelectCaret = m_posCaret;
    RedrawPreset();

}

//!< 選択中テキスト切り取り
void CMtEditorWindow::Cut()
{
    // コピー
    if (m_posCaret == m_posSelectCaret) 
    {
        return;
    }
    StringToClipboard(m_posCaret, 
                      m_posSelectCaret);
    Delete(true);
}

//!< テキスト貼り付け
void CMtEditorWindow::Paste()
{

#ifdef UNICODE
    if (::IsClipboardFormatAvailable(CF_UNICODETEXT) == 0) 
    {
#else
    if (IsClipboardFormatAvailable(CF_TEXT) == 0) 
    {
#endif
        return;
    }

    if (::OpenClipboard(m_hWnd) != 0) 
    {
        HANDLE hclip;
        TCHAR *p;
        HCURSOR old_cursor;

        old_cursor = ::SetCursor(LoadCursor(NULL, IDC_WAIT));
#ifdef UNICODE
        hclip = ::GetClipboardData(CF_UNICODETEXT);
#else
        hclip = GetClipboardData(CF_TEXT);
#endif
        p = reinterpret_cast<TCHAR*>(GlobalLock(hclip));
        if (p != NULL) 
        {
            //TODO: stringに変換しなくても良いようにする
            StdString str(p);
            Insert(str);
            GlobalUnlock(hclip);
        }
        ::CloseClipboard();
        ::SetCursor(old_cursor);
    }
}

//!< 再描画
void CMtEditorWindow::Redraw()
{
    /*
    ◆OnDraw( ) を直接呼び出す場合
    OnDraw( ) での内容が描かれます。( 上書きされます。)
    背景はそのままです。

    ◆RedrawWindow( ) を呼び出す場合
    まず OnEraseBkgnd( ) が呼び出され、背景が描かれます。
    次に OnDraw( ) が呼び出されます。

    ◆InvalidateRect( ) を呼び出す場合 ( 引数に 領域と FALSE を指定 )
    呼出しを書いた関数の終了後に、OnDraw( ) が呼び出されます。
    背景はそのままです。
    引数に指定した描画領域だけが、OnDraw( ) で描画されます。

    ◆InvalidateRect( ) を呼び出す場合 ( 引数に 領域と TRUE を指定 )
    呼出しを書いた関数の終了後に、OnEraseBkgnd( ) が呼び出されます。
    次に OnDraw( ) が呼び出されます。
    OnEraseBkgnd( ) が背景を塗り潰すのは、引数で指定した領域だけです。
    */

     ::InvalidateRect(m_hWnd, NULL, FALSE);
}

/**
 * @brief   矩形描画
 * @param   [in] hDc        デバイスコンテキスト
 * @param   [in] left
 * @param   [in] top
 * @param   [in] right
 * @param   [in] bottom
 * @retval  なし
 * @note
 */
void CMtEditorWindow::DrawRect(const HDC hDc, 
                               const int left, 
                               const int top, 
                               const int right, 
                               const int bottom)
{
    RECT trect;
    HBRUSH hBrush;

    if (right < 0) 
    {
        return;
    }

    ::SetRect(&trect, left, top, right, bottom);

    hBrush = ::CreateSolidBrush(GetSysColor(COLOR_HIGHLIGHT));
    ::FillRect(hDc, &trect, hBrush);
    ::DeleteObject(hBrush);
}

/**
 * @brief   文字列描画
 * @param   [in] hDc        デバイスコンテキスト
 * @param   [in] pRect
 * @param   [in] iLeftX
 * @param   [in] iTopY
 * @param   [in] pStr
 * @param   [in] iLength
 * @param   [in] bSel
 * @param   [in] crText
 * @retval  なし
 * @note
 */
int CMtEditorWindow::DrawString(const HDC       hDc, 
                                const RECT*     pRect, 
                                const int       iLeftX,
                                const int       iTopY, 
                                const TCHAR*    pStr, 
                                const int       iLength, 
                                const bool      bSel,
                                const COLORREF  crText)
{
    RECT rect;
    SIZE sz;

    ::GetTextExtentPoint32(m_hDc, pStr, iLength, &sz);

    if (iLeftX + sz.cx >= pRect->left) 
    {
        if (bSel) //&&  (m_bNoHideSel == TRUE ||::GetFocus() == m_hWnd)) 
        {
            //選択表示
            ::SetTextColor(hDc, m_es.crSel);
            ::SetBkColor(hDc, m_es.crSelBack);
            DrawRect(hDc, iLeftX, pRect->top, iLeftX + sz.cx, pRect->bottom);
        }
        else
        {
            COLORREF crCurrent = ::GetTextColor(hDc);
            if (crCurrent != crText)
            {
                ::SetTextColor(hDc, crText);
                m_crCurrent = crText;
            }
        }

        ::SetRect(&rect, m_iMargin, pRect->top, iLeftX + sz.cx, pRect->bottom);
        ::ExtTextOut(hDc, iLeftX, iTopY, 0, &rect, pStr, iLength, NULL);


        if (bSel) 
        {
            ::SetTextColor(hDc, crText);
            ::SetBkColor(hDc, m_es.crBack);
        }
    }
    return sz.cx;
}


/**
 * @brief   TOKEN文字色変換
 * @param   [in]   eToken    挿入位置
 * @retval  文字色 
 * @note    
 */
COLORREF CMtEditorWindow::Token2Coloer(E_TOKEN eToken)
{
    COLORREF crRet;
    switch (eToken)
    {
    case TK_NONE             :{crRet = m_es.crText;break;}
    case TK_WHITE_SPACE      :{crRet = m_es.crLineNo;break;}
    case TK_RESERVED_WORD    :{crRet = m_es.crReserv1;break;}
    case TK_IDENTIFIER       :{crRet = m_es.crText;break;}
    case TK_COMMENT_ML       :
    case TK_COMMENT_SL       :{crRet = m_es.crCommnet;break;}
    case TK_OPERATOR         :{crRet = m_es.crText;break;}
    case TK_STRING           :
    case TK_TRI_STRING       :{crRet = m_es.crString;break;}
    case TK_NUMBER           :{crRet = m_es.crText;break;}
    default                  :{crRet = m_es.crText;break;} 
    }
    return  crRet;
}


/**
 * @brief   1行描画
 * @param   [in] hDc        デバイスコンテキスト
 * @param   [in] iLinePos   行位置
 * @param   [in] iLeft      左座標
 * @param   [in] iRight     右座標
 * @retval  なし
 * @note
 */
void CMtEditorWindow::DrawLine(const HDC hDc, 
                               const int iLineDisp, 
                               const int iLeft,
                               const int iRight)
{
    RECT rcDraw;
    RECT rcLineNo;
    int offset;
    int top;
    int width;

    offset = m_iMargin 
             - (m_iScrollX * m_iCharWidth);
    
    width = (m_iScrollX * m_iCharWidth) + iRight;
    top = m_iSpacing / 2;
    //rcDraw.left = iLeft;
    rcDraw.left = m_iMargin;
    rcDraw.top = 0;
    rcDraw.right = iRight;
    rcDraw.bottom = rcDraw.top + m_iFontHeight;


    if (m_es.bEnableLineNumber)
    {
        SetTextAlign(m_hDc, TA_TOP | TA_RIGHT);
        //行番号を書く
        rcLineNo.left   = m_iIndicatorWidth;
        rcLineNo.right  = rcLineNo.left + m_iLineNumberWidth;
        rcLineNo.top    = rcDraw.top ;
        rcLineNo.bottom = rcDraw.bottom;
        SetTextAlign(m_hDc, TA_TOP | TA_RIGHT);

        m_crCurrent = m_es.crLineNo;

        ::SetTextColor(hDc, m_crCurrent);
        StdStringStream strm;
        if (m_lstLineHead[iLineDisp].iCol == 0)
        {
            strm << m_lstLineHead[iLineDisp].iLine;
//DB_PRINT("DRAWLINE %d(%d)\n", m_lstLineHead[iLineDisp].iLine, iLineDisp);
        }
        else
        {
            strm << _T("        ");
        }
        ::ExtTextOut(
            hDc,              // デバイスコンテキストのハンドル
            m_iIndicatorWidth + m_iLineNumberWidth,            // 開始位置（基準点）の x 座標
            0,                // 開始位置（基準点）の y 座標
            ETO_CLIPPED,      // 長方形領域の使い方のオプション
            &rcLineNo,        // 長方形領域の入った構造体へのポインタ
            strm.str().c_str(),  // 文字列
            SizeToUInt(strm.str().length()), // 文字数
            NULL                // 文字間隔の入った配列
        );
        SetTextAlign(m_hDc, TA_TOP | TA_LEFT);

        //
        HBRUSH hBrush = ::CreateSolidBrush(RGB(0,0,0));
        ::FrameRect(hDc, &rcLineNo, hBrush);
        ::DeleteObject(hBrush);
    }


    int iLine = m_lstLineHead.at(iLineDisp).iLine;
    const StdString* pStr = m_Buf.GetLineString(iLine);

    int iLen;
    int iTopCol;
    int iColNext;


    //------------------------
    // 折り返しを考慮
    //------------------------
    iTopCol = m_lstLineHead.at(iLineDisp).iCol;
    int iStrLen = SizeToInt(pStr->length());
    iLen = iStrLen;
    if ((iLineDisp + 1) < static_cast<int>(m_lstLineHead.size()))
    {
        iColNext = m_lstLineHead.at(iLineDisp + 1).iCol;
        if (iColNext > iTopCol)
        {
            iLen =  iColNext - iTopCol;
        }
    }

    STD_ASSERT(iTopCol <= iStrLen );
    StdString strSub;
    if (iTopCol < iStrLen)
    {
        strSub = pStr->substr(iTopCol, iLen);
    }

    //------------------------


    LINE_POS posCaret = m_posCaret;
    LINE_POS posSel   = m_posSelectCaret;
    
    if (posSel > posCaret)
    {
        std::swap(posSel, posCaret);
    }

    int iLength = static_cast<int>(strSub.length());
    TCHAR* pChar;


    
    //-------------------------------------
    // 最初のトークン位置を取得する
    int iNextTokenPos = 0;
    const std::vector<PARSE_TOKEN>* pLstToken;
    pLstToken = m_Buf.GetTokenList(iLine) ;
    COLORREF crText = m_es.crText;
    std::vector<PARSE_TOKEN>::const_iterator iteToken;

    for(iteToken  = pLstToken->begin();
        iteToken != pLstToken->end();
        iteToken++)
    {
        if ((*iteToken).iCol > iTopCol)
        {
            break;
        }
    }

    if (iteToken != pLstToken->begin())
    {
        iteToken--;
    }

    if (pLstToken->size() != 0)
    {
        crText = Token2Coloer((*iteToken).eToken);
    }


    //-------------------------------------


    for(int iCharPos = 0; iCharPos < iLength; iCharPos++)
    {
        if ((pLstToken->size() != 0) &&
           ((*iteToken).iCol == (iTopCol + iCharPos)))
        {
            crText = Token2Coloer((*iteToken).eToken);

            iteToken++;
            if (iteToken == pLstToken->end())
            {
                iteToken--;
            }
        }

        bool bSel = false;
        if (m_posCaret != m_posSelectCaret)
        {
            LINE_POS posCur;
            posCur.iLine = iLineDisp;
            posCur.iCol  = iCharPos;


            if ((posSel <= posCur) && 
                (posCur < posCaret))
            {
                bSel = true;
            }
#if 0
            DB_PRINT(_T("Sel(%d:%d) Car(%d:%d) Cur(%d:%d) (%s)\n"),  
                posSel.iLine,
                posSel.iCol,
                posCaret.iLine,
                posCaret.iCol,
                posCur.iLine,
                posCur.iCol,
                bSel?_T("true"):_T("false"));
#endif
        }


        pChar = &strSub.at(iCharPos);
        offset += DrawString(hDc, &rcDraw, 
                                offset, 
                                top,
                                pChar, 
                                1, bSel, crText);

        if (*pChar == '\t')
        {
            offset = CalcTabPixl(offset);
        }
    }

    DrawBreakPoint(iLineDisp, false);
    DrawExecIndicator(iLineDisp, false);
}

void CMtEditorWindow::SetIndicatorWidth(int iWidth)
{
    m_iIndicatorWidth = iWidth;
    RecalcMargin();
}

void CMtEditorWindow::SetLeftMargin(int iMargin)
{
    m_iLeftMargin = iMargin;
    RecalcMargin();
}

void CMtEditorWindow::RecalcMargin()
{
    m_iMargin = m_iIndicatorWidth + m_iLeftMargin;
    if(m_es.bEnableLineNumber)
    {
        m_iMargin += m_iLineNumberWidth;
    }
}

int CMtEditorWindow::CalcTabPixl(int iOffset) const
{
    int iMargin   = m_iIndicatorWidth + m_iLeftMargin; 
    int iTabWidth = m_iCharWidth * m_iTabStop;

    if(m_es.bEnableLineNumber)
    {
        iMargin += m_iLineNumberWidth;
    }
    
    int iTabPixel;
    int iTapPos = ((iOffset - iMargin) / iTabWidth) + 1  ;
    iTabPixel = iTapPos * iTabWidth + iMargin;

    return iTabPixel;
}

bool CMtEditorWindow::IsIdentifier(const TCHAR& chr) const
{
#ifdef UNICODE
    if(::iswalnum(chr) != 0)
    {
        return true;
    }
#else
    if(::isalnum(chr) != 0)
    {
        return true;
    }
#endif

    if(chr == _T('_'))
    {
        return true;
    }
    return false;
}

/**
 * @brief   行情報の設定
 * @param   [out] pList 開始行
 * @param   [in] iLine   
 * @retval  行幅(マージン含む)
 * @note
 */
int CMtEditorWindow::CalcLineWidth(std::vector<LINE_POS>* pList,
                                   int iLine, int iMargin)
{
    int iPixelOffset = iMargin;
    int iPixelOffsetOld;
    int iOffsetMax = 0;
    int iCharPixlWidth;
        
    const StdString* pLine;
    LINE_POS head;

    pLine = m_Buf.GetLineString(iLine);
    head.iLine = iLine;
    head.iCol  = 0;
    pList->push_back(head);

    bool bId = false;

    const TCHAR*  p;
    int iWordStartCol    = 0;
    int iWordStartXPixl  = 0;

    int iLength = static_cast<int>(pLine->length());
    for(int iCol = 0; iCol < iLength; iCol++)
    {
        p = &pLine->at(iCol);
        if (*p == _T('\t'))
        {
            iPixelOffset = CalcTabPixl(iPixelOffset);
            if (m_es.bWordwrap)
            {
                if (iPixelOffset > iMargin + m_iWidth)
                {
                    head.iCol  = iCol;
                    pList->push_back(head);
                    iPixelOffset = iMargin;
                }
            }
            bId = false;
        }
        else if(IsIdentifier(*p))
        {
            iCharPixlWidth = GetCharExtent(p, 1);
            iPixelOffsetOld = iPixelOffset;
            iPixelOffset += iCharPixlWidth;

            if (m_es.bWordwrap)
            {
                if (iPixelOffset > iMargin + m_iWidth)
                {
                    if (head.iCol  != iWordStartCol)
                    {
                        //1単語の長さが表示幅を超える時は、
                        //単語の途中で改行する
                        head.iCol  = iWordStartCol;
                        iPixelOffset = iPixelOffset - iWordStartXPixl + iMargin;
                    }
                    else
                    {
                        head.iCol  = iCol;
                        iWordStartCol = iCol;
                        iPixelOffset = iMargin + iCharPixlWidth;
                    }
                    pList->push_back(head);
                }

                if (!bId)
                {
                    iWordStartCol    = iCol;
                    iWordStartXPixl  = iPixelOffsetOld;
                    bId = true;
                }
            }
        }
        else
        {
            iCharPixlWidth = GetCharExtent(p, 1);
            iPixelOffset += iCharPixlWidth;
            if (m_es.bWordwrap)
            {
                if (iPixelOffset > iMargin + m_iWidth)
                {
                    head.iCol  = iCol;
                    iWordStartCol = iCol;
                    pList->push_back(head);
                    iPixelOffset = iMargin + iCharPixlWidth;
                }
                bId = false;
            }
        }

        if (iOffsetMax < iPixelOffset)
        {
            iOffsetMax = iPixelOffset;
        }
    }

    if (m_es.bWordwrap)
    {
        if (iOffsetMax > iMargin + m_iWidth)
        {
            iOffsetMax = iMargin + m_iWidth;
        }
    }

    return iOffsetMax;
}

/**
 * @brief   全行計算
 * @param   なし
 * @retval  行幅
 * @note
 */
int CMtEditorWindow::CalcAllLine()
{

    int iWidth = 0;
    int iWidthMax = 0;
    m_lstLineHead.clear();



    
    int iLineMax = m_Buf.GetLineNum();

    for (int iLine = 0; iLine < iLineMax; iLine++)
    {
        iWidth = CalcLineWidth(&m_lstLineHead, iLine, m_iMargin);
        if (iWidth > iWidthMax)
        {
            iWidthMax = iWidth;
        }
    }

    if (!m_es.bWordwrap)
    {
        // 1行の長さ (スクロールバー用)
        m_iLinePixlWidthMax = iWidthMax - m_iMargin;
    }

    return iWidthMax;
}


void CMtEditorWindow::SetDrawInfo(bool bRedraw)
{
    //OnReflect
    // 描画情報の更新
    TEXTMETRIC tm;
    HCURSOR old_cursor;
    RECT rect;

    old_cursor = SetCursor(LoadCursor(NULL, IDC_WAIT));

    // フォント情報の取得
    ::GetTextMetrics(m_hDc, &tm);
    m_iFontHeight = tm.tmHeight + m_iSpacing;
    m_iCharWidth = tm.tmAveCharWidth;
    if (::GetFocus() == m_hWnd) 
    {
        ::DestroyCaret();
        ::CreateCaret(m_hWnd, NULL, 0, m_iFontHeight);
        ::ShowCaret(m_hWnd);
    }
    GetEditRect(&rect);
    m_iWidth  = rect.right - rect.left;
    m_iHeight = rect.bottom - rect.top;

    if (m_iWidth < 0) {m_iWidth = 0;}
    if (m_iHeight < 0){m_iHeight = 0;}

    m_iLineNum = m_iHeight / m_iFontHeight;

    SetScrollbar();

    SIZE sz;
    StdString strDigit;
    for(int iCnt = 0; iCnt < m_iLineNumberDigit; iCnt++)
    {
        strDigit += _T("0");
    }

    ::GetTextExtentPoint32(m_hDc, strDigit.c_str(), m_iLineNumberDigit + 1, &sz);
    m_iLineNumberWidth = sz.cx;

    if (bRedraw) 
    {
        Redraw();
    }
    ::SetCursor(old_cursor);
    return;
}

//!> 行を表示
void CMtEditorWindow::VisibleLine(int iLine)
{
    RECT rect;
    int i;
    int x, y;

    GetEditRect(&rect);

    x = 0;
    y = iLine;

    // x
    i = m_iScrollX;
    m_iScrollX = 0;

    if (i != m_iScrollX)
    {
        SetScrollPos(m_hWnd, SB_HORZ, m_iScrollX, TRUE);
        ScrollWindowEx(m_hWnd, (i - m_iScrollX) * m_iCharWidth, 
            0, NULL, &rect, NULL, NULL, SW_INVALIDATE | SW_ERASE);
    }

    double dLineNum = (rect.bottom - rect.top) / m_iFontHeight;
    int iDispLine = static_cast<int>(::floor(dLineNum));
    int iCenterLine = static_cast<int>(::ceil(dLineNum / 2.0));

    //DB_PRINT(_T("LINE %f:%d:%d\n"), dLineNum, iDispLine, iCenterLine);
    // y
    i = m_iScrollY;
    if (y < m_iScrollY) 
    {
        m_iScrollY = y - iCenterLine;
        if (m_iScrollY < 0)
        {
            m_iScrollY = 0;
        }
    }
    else if (y > m_iScrollY + iDispLine) 
    {
        m_iScrollY = y - iCenterLine;
        if (m_iScrollY > m_iScrollMaxY) 
        {
            m_iScrollY = m_iScrollMaxY;
        }
    }

    if (i != m_iScrollY) 
    {
        rect.left = 0;
        SetScrollPos(m_hWnd, SB_VERT, m_iScrollY, TRUE);
        ScrollWindowEx(m_hWnd, 0, (i - m_iScrollY) * m_iFontHeight, NULL, &rect, NULL, NULL, SW_INVALIDATE | SW_ERASE);
    }
}

const TCHAR* CMtEditorWindow::GetChar(int iDispLine, int iDispCol) const
{
    int iLine = m_lstLineHead[iDispLine].iLine;
    int iCol  = m_lstLineHead[iDispLine].iCol + iDispCol;
    return &m_Buf.GetLineString(iLine)->at(iCol);
}

//!< キャレットのサイズ設定
bool CMtEditorWindow::SetCaretSize(const LINE_POS& posCaret)
{
    int iDispLine = posCaret.iLine;
    int iDispCol  = posCaret.iCol;
    const TCHAR *p;
    int csize;

    try
    {
        p = GetChar( iDispLine, iDispCol);
        csize = GetCharExtent( p, 1);
        if (csize <= 0) 
        {
            csize = m_iCharWidth;
        }

        ::DestroyCaret();
        ::CreateCaret(m_hWnd, NULL, csize, m_iFontHeight);
        return true;
    }
    catch(...)
    {
        return false;
    }

}

//!< キャレットX座標取得
int CMtEditorWindow::CalcCaretXPos(const HDC hDc, 
                                   const LINE_POS& posCaret)
{
    int iDspLine   = posCaret.iLine;
    int iColOffset = posCaret.iCol;
    int iCol;
    int iLine = m_lstLineHead[iDspLine].iLine;
    const StdString* pStr;
    pStr = m_Buf.GetLineString(iLine);
    size_t iLength = pStr->length();
    int iOffset;
    const TCHAR *p;


    iOffset = m_iMargin - (m_iScrollX * m_iCharWidth);

    size_t iCnt = 0;
    for (iCol  = m_lstLineHead[iDspLine].iCol;
         iCol  < iLength;  iCol++)
    {
        if (iCnt >= iColOffset)
        {
            break;
        }

        p = &(pStr->at(iCol));
        if (*p == TEXT('\t'))
        {
            iOffset = CalcTabPixl(iOffset);
        } 
        else 
        {
            iOffset += GetCharExtent( p, 1);
        }
        iCnt++;
    }
//DB_PRINT(_T("Margin %d, Scroll%d, Offset%d \n"), m_iMargin, m_iScrollX * m_iCharWidth, iOffset);
    return iOffset;
}

/**
 *  @brief   キャレット位置取得
 *  @param   [out] pLine  論理行
 *  @param   [out] pCol   文字位置
 *  @retval  なし
 *  @note    
 */
void CMtEditorWindow::GetCaretPosition(int* pLine, int* pCol) const
{
    *pCol  = m_posCaret.iCol;
    *pLine = m_posCaret.iLine;
}

/**
 *  @brief   キャレット位置設定
 *  @param   [in] iLine  論理行
 *  @param   [in] iCol   文字位置
 *  @retval  なし
 *  @note    選択は解除
 */
void CMtEditorWindow::SetCaretPosition(int iLine, int iCol)
{
    m_posCaret.Set(iLine, iCol);
    VisibleLine(iLine);
}


//!< キャレットのサイズ設定
void CMtEditorWindow::CaretSetSize()
{
    /*
    TCHAR *p;
    int csize;
    int len;

    p = IndexToChar(m_dwCaretPos);
    csize = GetCharExtent( p, &len);

    if (csize <= 0) 
    {
        csize = m_iCharWidth;
    }
    ::DestroyCaret();
    ::CreateCaret(m_hWnd, NULL, csize, m_iFontHeight);
    */
}


//!<  座標からキャレットの標示位置取得
LINE_POS CMtEditorWindow::CaretPointToCaret(const int x, const int y)
{

    int clen = 1;
    int iDispLine;

    LINE_POS posLine;
    posLine.iCol  = 0;
    posLine.iLine = 0;

    if (GetLineCount() <= 0)
    {
        return posLine;
    }

    iDispLine = m_iScrollY + ((y - m_iTopMargin) / m_iFontHeight);
    if (iDispLine < 0) 
    {
        return posLine;
    }
    else if (iDispLine >= GetLineCount()) 
    {
        iDispLine = GetLineCount() - 1;
    }

    int iOffset;
    int iOffsetOld;
    iOffset = m_iMargin - (m_iScrollX * m_iCharWidth);
    iOffsetOld = iOffset;


    int iLine;
    size_t iCol;
    const StdString* pStr;
    const TCHAR *p;

    iLine = m_lstLineHead[iDispLine].iLine;

    pStr = m_Buf.GetLineString(iLine);
    size_t iLength = pStr->length();
    
    int iColOffset = 0;
    for (iCol  = m_lstLineHead[iDispLine].iCol;
         iCol  < iLength;  iCol++)
    {
        p = &(pStr->at(iCol));
        if (*p == TEXT('\t'))
        {
            iOffset = CalcTabPixl(iOffset);
        } 
        else 
        {
            iOffset += GetCharExtent( p, 1);
        }

        if (iOffset > x) 
        {
            if ((iOffset - iOffsetOld) / 2 < x - iOffsetOld) 
            {
                iColOffset++;
            }
            break;
        }
        iOffsetOld = iOffset;
        iColOffset++;
    }

    posLine.iLine = iDispLine;
    posLine.iCol  = iColOffset;
    return posLine;
}


LRESULT CMtEditorWindow::OnKeyDown(const WPARAM wParam, const LPARAM lParam)
{
    switch (wParam) 
    {
    case VK_APPS:
        SendMessage(m_hWnd, WM_CONTEXTMENU, 0, 0);
        break;

    case VK_INSERT:
        if (GetKeyState(VK_CONTROL) < 0) 
        {
            // コピー
            SendMessage(m_hWnd, WM_COPY, 0, 0);
        }
        else if (GetKeyState(VK_SHIFT) < 0) 
        {
            // 貼り付け
            SendMessage(m_hWnd, WM_PASTE, 0, 0);
        } 
        else 
        {
            // 入力モード切替
            m_bInsertMode = !m_bInsertMode;

            ::DestroyCaret();
            ::CreateCaret(m_hWnd, NULL, 0, m_iFontHeight);
            ::ShowCaret(m_hWnd);
            LineRefresh( m_posCaret, m_posSelectCaret);
        }
        break;

    case VK_DELETE:
    case VK_BACK:
        if (m_bReadOnly) 
        {
            break;
        }
        
        if (::GetKeyState(VK_SHIFT) < 0)
        {
            // 切り取り
            SendMessage(m_hWnd, WM_CUT, 0, 0);
        }
        else 
        {
            // 削除
           LINE_POS posDelStart = CaretToDataPos (m_posCaret);

            if (wParam == VK_BACK)
            {
                if (m_posCaret == m_posSelectCaret) 
                {
//DB_PRINT(_T("m_posSelectCaret1 = %d,%d\n"),m_posSelectCaret.iLine, m_posSelectCaret.iCol);
                    MoveCaret(VK_LEFT);
                    m_posSelectCaret = MovCaretRight(m_posSelectCaret);
//DB_PRINT(_T("m_posSelectCaret2 = %d,%d\n"),m_posSelectCaret.iLine, m_posSelectCaret.iCol);
                }
            }
            else
            {
                if (m_posCaret == m_posSelectCaret)
                {
//DB_PRINT(_T("m_posSelectCaret3 = %d,%d\n"),m_posSelectCaret.iLine, m_posSelectCaret.iCol);
                    m_posSelectCaret = MovCaretRight(m_posSelectCaret);
//DB_PRINT(_T("m_posSelectCaret4 = %d,%d\n"),m_posSelectCaret.iLine, m_posSelectCaret.iCol);
                }
            }
            Delete(true);

        }
        break;
    
    case _T('C'):
        if (GetKeyState(VK_CONTROL) < 0) 
        {
            SendMessage(m_hWnd, WM_COPY, 0, 0);
        }
        break;

    case 'X':
        if (GetKeyState(VK_CONTROL) < 0) 
        {
            SendMessage(m_hWnd, WM_CUT, 0, 0);
        }
        break;

    case 'V':
        if (GetKeyState(VK_CONTROL) < 0) 
        {
            SendMessage(m_hWnd, WM_PASTE, 0, 0);
        }
        break;

    case 'Z':
        if (GetKeyState(VK_CONTROL) < 0 && GetKeyState(VK_SHIFT) < 0) 
        {
            // やり直し
            SendMessage(m_hWnd, EM_REDO, 0, 0);
        }
        else if (GetKeyState(VK_CONTROL) < 0) 
        {
            // 元に戻す
            SendMessage(m_hWnd, EM_UNDO, 0, 0);
        }
        break;

    case 'Y':
        if (GetKeyState(VK_CONTROL) < 0)
        {
            SendMessage(m_hWnd, EM_REDO, 0, 0);
        }
        break;

    case VK_HOME:
    case VK_END:
    case VK_PRIOR:
    case VK_NEXT:
    case VK_UP:
    case VK_DOWN:
    case VK_LEFT:
    case VK_RIGHT:
    {
        auto iVal = static_cast<const int>(wParam);
        MoveCaret(iVal);
    }
        break;
    default:
//DB_PRINT(_T("Key %c(%x)\n"), wParam, wParam);
        break;

    }
    return 0;
}


LRESULT CMtEditorWindow::OnChar(const WPARAM wParam, const LPARAM lParam)
{
    TCHAR in[3] = {};
    StdString str;
    LINE_POS posStart;

    if (GetKeyState(VK_CONTROL) < 0) 
    {
        return 0;
    }
    switch (wParam) {
    case VK_RETURN:
        // 改行
        str = _T("\r\n");
        Insert(str);
        break;

    case VK_BACK:
        break;

    case VK_ESCAPE:
        break;

    default:

        auto iVal = static_cast<const int>(wParam);

        in[0] = wParam;
        str = &in[0];
        Insert(str);
        break;
    }
    return 0;
}

LINE_POS CMtEditorWindow::MiveCaretLeft(const LINE_POS& posCaret)
{
    LINE_POS posRet = posCaret;

    if ((posCaret.iCol == 0) &&
        (posCaret.iLine == 0))
    {
        return posRet;
    }
    
    if (posRet.iCol == 0)
    {
        posRet.iLine--;
        posRet.iCol = CalcDispLineLength(posRet.iLine);
    }
    else
    {
        posRet.iCol--;
    }
    return posRet;
}

LINE_POS CMtEditorWindow::MovCaretRight(const LINE_POS& posCaret)
{
    LINE_POS posRet = posCaret;

    LINE_POS posData = CaretToDataPos(posCaret);

    if (m_Buf.IsLast(posData))
    {
        return posRet;
    }
    
    int iLength = CalcDispLineLength(posRet.iLine);
    if (posRet.iCol == iLength)
    {
        posRet.iLine++;
        posRet.iCol = 0;
    }
    else
    {
        posRet.iCol++;
    }

    return posRet;
}


//!< キャレットの移動
void CMtEditorWindow::MoveCaret(const int key)
{
    RECT rect;
    POINT pt;

    GetEditRect(&rect);

    LINE_POS posCaretOld = m_posCaret;
    LINE_POS posSelectCaretOld = m_posSelectCaret;

    switch (key) {
    case VK_HOME:
        if (GetKeyState(VK_CONTROL) < 0) 
        {
            // 全体の先頭
            m_posCaret.iCol  = 0;
            m_posCaret.iLine = 0;
        }
        else 
        {
            // 論理行頭
            m_posCaret.iCol  = 0;
        }
        Redraw();
        break;

    case VK_END:
        if (GetKeyState(VK_CONTROL) < 0) 
        {
            // 全体の末尾
            m_posCaret.iLine = SizeToInt(m_lstLineHead.size()) - 1;
            if (m_posCaret.iLine < 0 )
            {
                m_posCaret.iLine = 0;
                m_posCaret.iCol = 0;
            }
            else
            {
                m_posCaret.iCol = CalcDispLineLength(m_posCaret.iLine);
            }
        }
        else 
        {
            m_posCaret.iCol = CalcDispLineLength(m_posCaret.iLine);
        }
        Redraw();
        break;

    case VK_PRIOR:
        // Page UP
        if (::GetCaretPos(&pt) == FALSE) 
        {
            break;
        }

        /*
        if (m_iCaretPosX == 0) 
        {
            m_iCaretPosX = pt.x;
        }

        m_dwCaretPos = CaretPointToCaret(m_iCaretPosX, pt.y - (rect.bottom - rect.top));

        if (GetKeyState(VK_SHIFT) >= 0) 
        {
            m_dwSelectPos = m_dwCaretPos;
        }
        */

        ::SendMessage(m_hWnd, WM_VSCROLL, SB_PAGEUP, 0);
        break;

    case VK_NEXT:
        // Page Down
        if (::GetCaretPos(&pt) == FALSE) 
        {
            break;
        }

        /*
        if (m_iCaretPosX == 0) 
        {
            m_iCaretPosX = pt.x;
        }
        m_dwCaretPos = CaretPointToCaret(m_iCaretPosX, pt.y + (rect.bottom - rect.top));
        if (GetKeyState(VK_SHIFT) >= 0) 
        {
            m_dwSelectPos = m_dwCaretPos;
        }
        */
        SendMessage(m_hWnd, WM_VSCROLL, SB_PAGEDOWN, 0);
        break;

    case VK_UP:
        if (m_posCaret.iLine == 0)
        {
            break;
        }

        if (::GetKeyState(VK_SHIFT) >= 0) 
        {
            m_posSelectCaret = m_posCaret;
        }

        if (::GetCaretPos(&pt) == FALSE) 
        {
            break;
        }

        if (m_iCaretXPixel == 0) 
        {
            m_iCaretXPixel = pt.x;
        }

        m_posCaret = CaretPointToCaret(m_iCaretXPixel, pt.y - m_iFontHeight);

        break;

    case VK_DOWN:
        if((m_posCaret.iLine + 1) == m_lstLineHead.size())
        {
            break;
        }

        if (::GetKeyState(VK_SHIFT) >= 0) 
        {
            m_posSelectCaret = m_posCaret;
        }

        if (::GetCaretPos(&pt) == FALSE) 
        {
            break;
        }

        if (m_iCaretXPixel == 0) 
        {
            m_iCaretXPixel = pt.x;
        }

        m_posCaret = CaretPointToCaret(m_iCaretXPixel, pt.y + m_iFontHeight);

        break;

    case VK_LEFT:
        m_iCaretXPixel = 0;

        if ((m_posCaret != m_posSelectCaret) && 
            ::GetKeyState(VK_SHIFT) >= 0) 
        {
            // 選択解除
            if (m_posCaret > m_posSelectCaret)
            {
                std::swap(m_posCaret, m_posSelectCaret);
            }
            m_posSelectCaret = m_posCaret;
            break;
        }

        m_posCaret =  MiveCaretLeft(m_posCaret);
        break;

    case VK_RIGHT:
        m_iCaretXPixel = 0;

        if ((m_posCaret != m_posSelectCaret) && 
            GetKeyState(VK_SHIFT) >= 0) 
        {
            // 選択解除
            if (m_posCaret < m_posSelectCaret)
            {
                std::swap(m_posCaret, m_posSelectCaret);
            }
            m_posSelectCaret = m_posCaret;
            break;
        }
        
        m_posCaret = MovCaretRight(m_posCaret);
        break;
    }

    if (::GetKeyState(VK_SHIFT) >= 0) 
    {
        m_posSelectCaret = m_posCaret;
    }

    LINE_POS posMax = m_posCaret;
    LINE_POS posMin = posCaretOld;

    if (posCaretOld > m_posCaret)
    {
       posMax = posCaretOld;
       posMin = m_posCaret;
    }

    if (m_posSelectCaret > posMax)
    {
        posMax = m_posSelectCaret;
    }

    if (posSelectCaretOld > posMax)
    {
        posMax = posSelectCaretOld;
    }
    else if (posSelectCaretOld < posMin)
    {
        posMin = posSelectCaretOld;
    }

    if (m_posSelectCaret > posMax)
    {
        posMax = m_posSelectCaret;
    }
    else if (m_posSelectCaret < posMin)
    {
        posMin = m_posSelectCaret;
    }

    EnsureVisible();
    LineRefresh( posMax, posMin);
}


LRESULT CMtEditorWindow::OnMouseMove(const WPARAM wParam, const LPARAM lParam)
{
    LINE_POS posCaret = m_posCaret;
    LINE_POS posSelectCaret = m_posSelectCaret;
    PresetRedrawArea(m_posCaret, m_posSelectCaret);

    int iX = LOWORD(lParam);
    int iY = HIWORD(lParam);

    if (iX < m_iIndicatorWidth)
    {
        SetCursor(LoadCursor(0, IDC_ARROW));
    }
    else
    {
        SetCursor(LoadCursor(0, IDC_IBEAM));
    }


    if (!(wParam & MK_LBUTTON) 
        || m_bMousedown == FALSE) 
    {
        return 0;
    }

    SetFocus(m_hWnd);

    m_posCaret = CaretPointToCaret(iX, iY);


    if (posSelectCaret != m_posCaret) 
    {
        PresetRedrawArea(m_posCaret, m_posSelectCaret);
        RedrawPreset();
    }

    return 0;
}



LRESULT CMtEditorWindow::OnLButtonDown(const WPARAM wParam, const LPARAM lParam)
{
    LINE_POS posCaret = m_posCaret;
    LINE_POS posSelectCaret = m_posSelectCaret;

    PresetRedrawArea(m_posCaret, m_posSelectCaret);
    SetCursor(LoadCursor(0, IDC_IBEAM));

    int iX = LOWORD(lParam);
    int iY = HIWORD(lParam);


    //FlushString(TRUE);
    SetCapture(m_hWnd);
    m_bMousedown = true;
    m_iCaretXPixel = 0;

    SetFocus(m_hWnd);

    m_posCaret = CaretPointToCaret( iX, iY);

    m_iClickBreakPoint = -1;
    if (iX < m_iIndicatorWidth)
    {  
        m_iClickBreakPoint = m_posCaret.iLine;
    }

    EnsureVisible();
    if (GetKeyState(VK_SHIFT) >= 0) 
    {
        m_posSelectCaret = m_posCaret;
    }

    PresetRedrawArea(m_posCaret, m_posSelectCaret);
    RedrawPreset();
    return 0;
}

LRESULT CMtEditorWindow::OnLButtonUp(const WPARAM wParam, const LPARAM lParam)
{

    SetCursor(LoadCursor(0, IDC_IBEAM));
    if (m_bMousedown == FALSE) 
    {
        return 0;
    }
    ReleaseCapture();

    m_bMousedown = FALSE;
    m_iCaretXPixel = 0;

    SetFocus(m_hWnd);

    int iX = LOWORD(lParam);
    int iY = HIWORD(lParam);


    if (_IsTripleClick( iX, iY ))
    {
        LINE_POS pos = CaretPointToCaret( iX, iY);
        SelectLine(pos);
        return 0;
    }

    m_posCaret = CaretPointToCaret( iX, iY);

    if ((iX < m_iIndicatorWidth)&& 
        (m_iClickBreakPoint >= 0))
    {  
        int iLine;
        bool bSet;

        iLine = m_lstLineHead[m_posCaret.iLine].iLine;

        if(m_Buf.GetBreakPoint(iLine) == BP_ENABLE)
        {
            bSet = false;
            m_Buf.SetBreakPoint(iLine, BP_NONE);
        }
        else
        {
            bSet = true;
            m_Buf.SetBreakPoint(iLine, BP_ENABLE);
        }
        m_iClickBreakPoint = -1;

        if(m_pSetBreakPointModified)
        {
            m_pSetBreakPointModified(m_pParent, iLine, bSet);
        }
        DrawBreakPoint(m_posCaret.iLine, true);


        if (m_iExecLine == iLine)
        {
            DrawExecIndicator(m_iExecLine, true);
        }
    }
    else
    {
        EnsureVisible();
    }


    /*
    if (sp != m_dwSelectPos) 
    {
        LineRefresh( cp, sp);
    }
    LineRefresh( cp, m_dwCaretPos);
    */

    return 0;
}

LRESULT CMtEditorWindow::OnHScroll(const WPARAM wParam, const LPARAM lParam)
{
    RECT rect;
    int i;

    GetEditRect(&rect);
    i = m_iScrollX;
    switch ((int)LOWORD(wParam)) 
    {
    case SB_TOP:
        m_iScrollX = 0;
        break;

    case SB_BOTTOM:
        m_iScrollX = m_iScrollMaxX;
        break;

    case SB_LINELEFT:
        m_iScrollX = (m_iScrollX > 0) ? m_iScrollX - 1 : 0;
        break;

    case SB_LINERIGHT:
        m_iScrollX = (m_iScrollX < m_iScrollMaxX) ? m_iScrollX + 1 : m_iScrollMaxX;
        break;

    case SB_PAGELEFT:
        m_iScrollX = (m_iScrollX - ((rect.right - rect.left) / m_iCharWidth) > 0) ?
            m_iScrollX - ((rect.right - rect.left) / m_iCharWidth) : 0;
        break;

    case SB_PAGERIGHT:
        m_iScrollX = (m_iScrollX + ((rect.right - rect.left) / m_iCharWidth) < m_iScrollMaxX) ?
            m_iScrollX + ((rect.right - rect.left) / m_iCharWidth) : m_iScrollMaxX;
        break;

    case SB_THUMBPOSITION:
    case SB_THUMBTRACK:
        {
            SCROLLINFO si;

            ZeroMemory(&si, sizeof(SCROLLINFO));
            si.cbSize = sizeof(SCROLLINFO);
            si.fMask = SIF_ALL;
            GetScrollInfo(m_hWnd, SB_HORZ, &si);
            m_iScrollX = si.nTrackPos;
        }
        break;
    }
    switch ((int)LOWORD(wParam)) {
    case SB_TOP:
    case SB_BOTTOM:
    case SB_LINELEFT:
    case SB_LINERIGHT:
    case SB_PAGELEFT:
    case SB_PAGERIGHT:
        if (i - m_iScrollX != 0) {
            // 親ウィンドウに通知
            //NotifyMessage(EN_HSCROLL);
        }
        break;
    }

    SetScrollPos(m_hWnd, SB_HORZ, m_iScrollX, TRUE);
    ScrollWindowEx(m_hWnd, (i - m_iScrollX) * m_iCharWidth, 
        0, NULL, &rect, NULL, NULL, SW_INVALIDATE | SW_ERASE);
    return 0;
}

LRESULT CMtEditorWindow::OnVScroll(const WPARAM wParam, const LPARAM lParam)
{
    RECT rect;
    int i;

    GetEditRect(&rect);
    i = m_iScrollY;
    switch ((int)LOWORD(wParam)) {
    case SB_TOP:
        m_iScrollY = 0;
        break;

    case SB_BOTTOM:
        m_iScrollY = m_iScrollMaxY;
        break;

    case SB_LINEUP:
        m_iScrollY = (m_iScrollY > 0) ? m_iScrollY - 1 : 0;
        break;

    case SB_LINEDOWN:
        m_iScrollY = (m_iScrollY < m_iScrollMaxY) ? m_iScrollY + 1 : m_iScrollMaxY;
        break;

    case SB_PAGEUP:
        m_iScrollY = (m_iScrollY - (m_iLineNum - 1) > 0) ?
            m_iScrollY - (m_iLineNum - 1) : 0;
        break;

    case SB_PAGEDOWN:
        m_iScrollY = (m_iScrollY + (m_iLineNum - 1) < m_iScrollMaxY) ?
            m_iScrollY + (m_iLineNum - 1) : m_iScrollMaxY;
        break;

    case SB_THUMBPOSITION:
    case SB_THUMBTRACK:
        {
            SCROLLINFO si;

            ZeroMemory(&si, sizeof(SCROLLINFO));
            si.cbSize = sizeof(SCROLLINFO);
            si.fMask = SIF_ALL;
            GetScrollInfo(m_hWnd, SB_VERT, &si);
            m_iScrollY = si.nTrackPos;
        }
        break;
    }
    switch ((int)LOWORD(wParam)) {
    case SB_TOP:
    case SB_BOTTOM:
    case SB_LINEUP:
    case SB_LINEDOWN:
    case SB_PAGEUP:
    case SB_PAGEDOWN:
        if (i - m_iScrollY != 0) 
        {
            // 親ウィンドウに通知
            //NotifyMessage(EN_VSCROLL);
        }
        break;
    }

    rect.left = 0; 
    ::SetScrollPos(m_hWnd, SB_VERT, m_iScrollY, TRUE);
    ::ScrollWindowEx(m_hWnd, 0, (i - m_iScrollY) * m_iFontHeight, 
        NULL, &rect, NULL, NULL, SW_INVALIDATE | SW_ERASE);
    return 0;
}

/**
 * @brief   表示行取得
 * @param   [in] iLine データ行
 * @retval  表示行
 * @note    
 */

int CMtEditorWindow::LineToDispLine( const int iLine) const
{
    int iSize = SizeToInt(m_lstLineHead.size()) - 1;
    int iLow = 0;
    int iHigh = iSize;
    int iDspLine = 0;

    while (iLow <= iHigh) 
    {
        iDspLine = (iLow + iHigh) / 2;

        if (m_lstLineHead[iDspLine].iLine > iLine) 
        {
            if (iDspLine > 0 && 
                m_lstLineHead[iDspLine - 1].iLine <= iLine) 
            {
                iDspLine--;
                break;
            }

            iHigh  = iDspLine - 1;
        } 
        else if (m_lstLineHead[iDspLine].iLine < iLine) 
        {
            if (iDspLine < iSize  && 
                m_lstLineHead[iDspLine + 1].iLine >= iLine) 
            {
                iDspLine++;
                break;
            }
            iLow = iDspLine + 1;
        }
        else 
        {
            break;
        }
    }

    iDspLine =  GetTopDispLine(iDspLine);
    
    return iDspLine;
}

/**
 * @brief   再描画設定
 * @param   [in] cposStart 開始位置
 * @param   [in] cposEnd   終了位置
 * @retval  なし
 * @note    
 */
void CMtEditorWindow::LineRefresh( LINE_POS cposStart, LINE_POS cposEnd)
{
    RECT rect;

//DB_PRINT(_T("-----LineRefresh  pos[%d %d]\n"), 
//      cposStart.iLine,
//      cposEnd.iLine);

    if (cposStart > cposEnd) 
    {
        std::swap(cposStart, cposEnd);
    }
    cposStart.iLine -= m_iScrollY;
    cposEnd  .iLine -= m_iScrollY;

    // 再描画
    GetEditRect(&rect);
    if (cposStart.iLine * m_iFontHeight > (rect.bottom - rect.top) || 
        cposEnd.iLine * m_iFontHeight + m_iFontHeight < 0) 
    {
//DB_PRINT(_T("LineRefresh1  pos(%d, %d)\n"));
        return;
    }

    if (cposEnd.iLine * m_iFontHeight + m_iFontHeight < 
        (rect.bottom - rect.top)) 
    {
        rect.bottom = cposEnd.iLine * m_iFontHeight + 
                      m_iFontHeight + m_iTopMargin;
    }

    if (cposStart.iLine * m_iFontHeight > 0) 
    {
        rect.top = cposStart.iLine * m_iFontHeight + m_iTopMargin;
    }
    rect.left = 0;
//DB_PRINT(_T("LineRefresh2 %d,%d,%d,%d \n"),
//      rect.top,
//      rect.left,
//      rect.bottom,
//      rect.right);
    ::InvalidateRect(m_hWnd, &rect, FALSE);
}

//!> キャレット位置を表示
void CMtEditorWindow::EnsureVisible()
{
    RECT rect;

    GetEditRect(&rect);

    int iXPos;

    assert(m_posCaret.iLine >= 0);

    if (m_posCaret.iLine < 0)
    {
        return;
    }


    int iOldScrollX = m_iScrollX;

    if (m_es.bWordwrap)
    {
        m_iScrollX = 0;
    }
    else
    {
        iXPos = CalcCaretXPos(m_hDc, m_posCaret);
        int iWidth = rect.right - rect.left;

        int iLeft  = m_iMargin ;
        int iRight = m_iMargin  + iWidth;

        if (iXPos < iLeft) 
        {
            m_iScrollX = ((iXPos - iLeft)/ m_iCharWidth) + m_iScrollX - 5;
            if (m_iScrollX < 0) 
            {
                m_iScrollX = 0;
            }
        }
        else if (iXPos > iRight)
        {
            m_iScrollX = 5 + m_iScrollX - ( iRight - iXPos) / m_iCharWidth;
            if (m_iScrollX > m_iScrollMaxX) 
            {
                m_iScrollX = m_iScrollMaxX;
            }
        }

int iXPosNew = CalcCaretXPos(m_hDc, m_posCaret);
//DB_PRINT(_T("Old:%d New%d  %d < %d < %d : %d \n"), 
//    iOldScrollX, m_iScrollX, iLeft, iXPos, iRight, iXPosNew);        
    }

    if (iOldScrollX != m_iScrollX)
    {
        ::SetScrollPos(m_hWnd, SB_HORZ, m_iScrollX, TRUE);
        ::ScrollWindowEx(m_hWnd, (iOldScrollX - m_iScrollX) * m_iCharWidth,
            0, NULL, &rect, NULL, NULL, SW_INVALIDATE | SW_ERASE);
    }

    // y
    int iOldScrollY = m_iScrollY;
    if (m_posCaret.iLine < m_iScrollY) 
    {
        m_iScrollY = m_posCaret.iLine;
    }

    int iYLineNum = m_iLineNum;

    if (m_posCaret.iLine > m_iScrollY + iYLineNum - 1) 
    {
        m_iScrollY = m_posCaret.iLine - iYLineNum + 1;
        if (m_iScrollY > m_iScrollMaxY) 
        {
            m_iScrollY = m_iScrollMaxY;
        }
    }

    if (iOldScrollY != m_iScrollY) 
    {
        rect.left = 0;
        ::SetScrollPos(m_hWnd, SB_VERT, m_iScrollY, TRUE);
        ::ScrollWindowEx(m_hWnd, 0, (iOldScrollY - m_iScrollY) * m_iFontHeight,
            NULL, &rect, NULL, NULL, SW_INVALIDATE | SW_ERASE);
    }
}

LRESULT CMtEditorWindow::OnSetFocus(const WPARAM wParam, const LPARAM lParam)
{
    ::DestroyCaret();
    ::CreateCaret(m_hWnd, NULL, 0, m_iFontHeight);
    ::ShowCaret(m_hWnd);
    LineRefresh( m_posCaret, m_posSelectCaret);
    // 親ウィンドウに通知
    //NotifyMessage(EN_SETFOCUS);
    return 0;
}

/**
 *  @brief   表示行長の取得
 *  @param   [in] iDspLine  表示行
 *  @retval  行の長さ（文字数)
 *  @note    
 */
int CMtEditorWindow::CalcDispLineLength( const int iDspLine) const
{
    if(iDspLine < 0)
    {
        return -1;
    }

    if(static_cast<int>(m_lstLineHead.size()) <= iDspLine)
    {
        return -1;
    }

    int iLine    = m_lstLineHead[iDspLine].iLine;
    int iColHead = m_lstLineHead[iDspLine].iCol;
    size_t iLength  = m_Buf.GetLineString(iLine)->length();

    int iDspLineLength = -1;

    if (iDspLine + 1 != m_lstLineHead.size())
    {
        int iLineNext    = m_lstLineHead[iDspLine + 1].iLine;
        int iColNext     = m_lstLineHead[iDspLine + 1].iCol;

        if (iLineNext == iLine)
        {
            iDspLineLength = iColNext - iColHead;
            return iDspLineLength;
        }
    }

    iDspLineLength = SizeToInt(iLength) - iColHead;
    return iDspLineLength;
}


/*
int CMtEditorWindow::GetDispLineEndCol(int iDispLine)  const
{
    int iLine;
    int iCol;
    int iRetCol = 0;

    if(GetLineCount() >= iDispLine)
    {
        return -1;
    }

    int iLength;
    iLine = m_lstLineHead[iDispLine].iLine;
    iCol  = m_lstLineHead[iDispLine].iCol;
    iLength = m_Buf.GetLineString(iLine)->length();

    if (GetLineCount() != (iDispLine + 1))
    {
        if (iLine == m_lstLineHead[iDispLine + 1].iLine)
        {
            iRetCol = m_lstLineHead[iDispLine + 1].iCol - iCol;
            return iRetCol;
        }
    }
    iRetCol = iLength - iCol;
    return iRetCol;
}
*/


/**
 *  @brief   キャレット位置からデータ位置への変換
 *  @param   [in] posCaret キャレット位置
 *  @retval  データ位置
 *  @note    キャレット位置は、
 *             折り返し時の表示行数,各
 *            表示行先頭からの桁位置で示す。
 *            
 *            データ位置は、１改行、１行とした時の行数、
 *            行の先頭のからの桁数を示す
 */
LINE_POS CMtEditorWindow::CaretToDataPos(const LINE_POS& posCaret) const
{
    LINE_POS posData;

    posData.iLine = m_lstLineHead[posCaret.iLine].iLine;
    posData.iCol  = m_lstLineHead[posCaret.iLine].iCol + posCaret.iCol;
    return posData;
}


/**
 *  @brief   データ位置からキャレット位置への変換
 *  @param   [in] posCaret 位置
 *  @retval  キャレット位置
 *  @note    キャレット位置は、
 *             折り返し時の表示行数,各
 *            表示行先頭からの桁位置で示す。
 *            
 *            データ位置は、１改行、１行とした時の行数、
 *            行の先頭のからの桁数を示す
 */
LINE_POS CMtEditorWindow::DataPosToCaret(const LINE_POS& posData) const
{
    LINE_POS posDisp;
    int iLine;
    int iColStart;
    int iColEnd;
    int iSize;

    iSize =  static_cast<int>(m_lstLineHead.size());
    for (iLine =  LineToDispLine(posData.iLine);
         iLine < iSize; 
         iLine++)
    {
        iColStart = m_lstLineHead[iLine].iCol;
        iColEnd   = iColStart + CalcDispLineLength(iLine);
        if ((posData.iCol >= iColStart) &&
            (posData.iCol <= iColEnd))
        {
            posDisp.iLine = iLine;
            posDisp.iCol = posData.iCol - iColStart;
            return posDisp;
        }
    }

    posDisp.iLine = -1;
    posDisp.iCol  = -1;
    return posDisp;
}
 
/**
 * @brief   表示行の先頭位置取得
 * @param   [in] iDispLine
 * @retval  先頭行の位置
 * @note	
 */
int CMtEditorWindow::GetTopDispLine(int iDispLine) const
{
    if(!m_es.bWordwrap){return iDispLine;}

    std::vector<LINE_POS>::const_iterator ite;
    int iLine;
    ite = m_lstLineHead.begin() + iDispLine;
    iLine = (*ite).iLine;
    while(ite != m_lstLineHead.begin())
    {
        ite--;
        if((*ite).iLine != iLine)
        {   
            return iDispLine;
        }
        iDispLine--;
    }
    return 0;
}

/**
 * @brief   再描画領域設定
 * @param   [in] posDispStart
 * @param   [in] posDispEnd
 * @retval  なし
 * @note	
 */
void CMtEditorWindow::PresetRedrawArea
    (LINE_POS posDispStart, 
     LINE_POS posDispEnd)
{

    if (posDispStart > posDispEnd)
    {
        std::swap(posDispStart, posDispEnd);
    }

    if(m_posPresetStart.iLine < 0)
    {
        m_posPresetStart = posDispStart;
    }
    else
    {
        if(m_posPresetStart > posDispStart)
        {
            m_posPresetStart = posDispStart;
        }
    }

    if(posDispEnd > m_posPresetEnd)
    {
        m_posPresetEnd = posDispEnd;
    }
}

/**
 * @brief   再描画領域設定
 * @param   [in] posDispStart
 * @retval  なし
 * @note	
 */
void CMtEditorWindow::PresetRedrawAreaTop
    (LINE_POS posDispStart)
{
    if(m_posPresetStart.iLine < 0)
    {
        m_posPresetStart = posDispStart;
    }
    else
    {
        if(m_posPresetStart > posDispStart)
        {
            m_posPresetStart = posDispStart;
        }
    }
    m_posPresetEnd.iLine = m_posPresetStart.iLine
                           + m_iLineNum; 
}

/**
 * @brief   再描画領域
 * @param   なし
 * @retval  なし
 * @note	
 */
void CMtEditorWindow::RedrawPreset()
{
    //TODO:実装
    LineRefresh( m_posPresetStart, m_posPresetEnd);
    m_posPresetStart.Set(-1,0);
    m_posPresetEnd.Set(-1,0);
}


/**
 * @brief   表示行の先頭位置補正
 * @param   [in] iDispLine  変更位置
 * @param   [in] iOffset    変更量
 * @retval  なし
 * @note    追加、削除による先頭行のズレを補正する
 */
void CMtEditorWindow::OffsetDispLine(int iDispLine, int iOffset)
{
    if (iOffset == 0)
    {
        return;
    }

    std::vector<LINE_POS>::iterator ite;

    ite = m_lstLineHead.begin() + iDispLine;
    for(;ite != m_lstLineHead.end()
        ;ite++)
    {
        (*ite).iLine += iOffset;
    }
}


/**
 * @brief   文字の削除文字
 * @param   [in]  bRedraw 再描画の有無
 * @retval  削除行数(データ行)
 * @note	
 */
int CMtEditorWindow::Delete(bool bRedraw)
{
    if (m_posCaret == m_posSelectCaret)
    {
        return 0;
    }

    //削除処理
    int iDeleteLine = 0;
    LINE_POS posDataSelect;
    LINE_POS posDataInsert;
    LINE_POS posStart;

    posDataSelect = CaretToDataPos(m_posSelectCaret);
    posDataInsert = CaretToDataPos(m_posCaret);

    if (m_posSelectCaret < m_posCaret)
    {
        posStart = m_posSelectCaret;
    }
    else
    {
        posStart = m_posCaret;
    }

    //削除前の折り返し有無
    bool bMultLine = false;
    if (m_es.bWordwrap)
    {
        if (static_cast<int>(m_lstLineHead.size()) > posStart.iLine + 1)
        {
            if( m_lstLineHead[posStart.iLine].iLine ==
                m_lstLineHead[posStart.iLine + 1].iLine)
            {
                bMultLine = true;
            }
        }
    }

    int iDeleteNum;
    iDeleteNum = m_Buf.DeleteString(posDataSelect, posDataInsert);

    std::vector<LINE_POS> lstPos;
    CalcLineWidth(&lstPos, m_lstLineHead[posStart.iLine].iLine, m_iMargin);
    int iDspTop = GetTopDispLine(posStart.iLine);
    ReplaceHeadList(iDspTop, lstPos, -iDeleteNum);

    if (iDeleteNum == 0 && !bMultLine)
    {
        if (lstPos.size() == 1)
        {
            PresetRedrawArea(posStart, posStart);
        }
        else
        {
            PresetRedrawAreaTop(posStart);
        }
    }
    else
    {
        PresetRedrawAreaTop(posStart);
    }
    
    m_posCaret = posStart;
    m_posSelectCaret = posStart;

    if (bRedraw)
    {
        RedrawPreset();
    }
    return iDeleteNum;
}


/**
 * @brief   文字の挿入
 * @param   [in] str 挿入文字
 * @retval  なし
 * @note	
 */
void CMtEditorWindow::Insert(const StdString& str)
{
    int iDeleteNum;
    PresetRedrawArea(m_posSelectCaret, m_posCaret);
    iDeleteNum = Delete(false);


    std::vector<LINE_POS> lstPos;

    LINE_POS posInsert;
    LINE_POS posEnd;
    posInsert = CaretToDataPos(m_posSelectCaret);


    int iInsertedLineNum = m_Buf.InsertString(posInsert, str, m_bInsertMode, &posEnd);

    //DB_PRINT(_T("InsertString1  [%d,%d] %s [%d,%d]\n"), posInsert.iLine,posInsert.iCol,
    //    str.c_str(), posEnd.iLine, posEnd.iCol);
   
    int  iDspLine = GetTopDispLine(m_posCaret.iLine);


    if (iInsertedLineNum <= 1)
    {
        //行数が増加しない場合
        CalcLineWidth(&lstPos, posInsert.iLine, m_iMargin);
        ReplaceHeadList(iDspLine, lstPos, iInsertedLineNum);

    }
    else
    {
        for(int iLine = posInsert.iLine;
                iLine < posInsert.iLine + iInsertedLineNum;
                iLine++)
        {
            CalcLineWidth(&lstPos, iLine, m_iMargin);
        }
        ReplaceHeadList(iDspLine, lstPos, iInsertedLineNum);
    }
    
//DB_PRINT(_T("InsertString2  [%d,%d]\n"), posEnd.iLine,posEnd.iCol);
    m_posSelectCaret = DataPosToCaret(posEnd);
    m_posCaret = m_posSelectCaret;

    if (iInsertedLineNum != iDeleteNum)
    {
        posInsert.iLine += m_iLineNum;
        PresetRedrawArea(m_posCaret, posInsert);
    }

//DB_PRINT(_T("InsertString3  [%d,%d]\n"), m_posCaret.iLine,m_posCaret.iCol);

    PresetRedrawArea(m_posCaret, m_posCaret);
    SetScrollbar();
    EnsureVisible();
    RedrawPreset();
}

/**
 * @brief   折り返しリストの置き換え
 * @param   [in] iDspLine 表示行
 * @param   [in] listPos  置き換えリスト
 * @param   [in] iType    0:置き換え
 *                        1:削除
 *                        2:挿入
 * @retval  なし
 * @note    
 */
void CMtEditorWindow::ReplaceHeadList(int iDspLine, 
                                     const std::vector<LINE_POS>& listPos,
                                     int iModifyLineNum)
{

    if(listPos.size() == 0)
    {
        return;
    }
#if 0
DB_PRINT(_T("--------ReplaceHeadList(%d,listPos,%d)\n"),iDspLine, iModifyLineNum);
for(int i = 0; i< static_cast<int>(listPos.size()); i++)
{
    DB_PRINT(_T("  listPos %d,%d\n"),listPos[i].iLine, listPos[i].iCol);
}

DB_PRINT(_T("  -----------\n"));
for(int i = 0; i< static_cast<int>(m_lstLineHead.size()); i++)
{
    DB_PRINT(_T("  m_lstLineHead %d,%d\n"),m_lstLineHead[i].iLine, m_lstLineHead[i].iCol);
}
#endif

    int iInsertType;
    int iStartLine;
    std::vector<LINE_POS>::const_iterator ite;
    std::vector<LINE_POS>::iterator iteHead;
    
    int iOldLine = -1;
    int iChangeLine = -1;
    bool bStartLine;

    iStartLine = (*listPos.begin()).iLine;

    for (ite  = listPos.begin();
         ite != listPos.end();
         ite++)
    {
        iteHead = m_lstLineHead.begin() + iDspLine;
        
        if (iteHead == m_lstLineHead.end())
        {
            //挿入する
            m_lstLineHead.insert(iteHead, *ite);
            iDspLine++;
            continue;
        }

        bStartLine = false;
        if( ((*iteHead).iLine == iStartLine) ||
            ((*ite).iLine == iStartLine))
        {
            bStartLine = true;
        }


        if((*iteHead).iLine == (*ite).iLine)
        { 
            if(bStartLine)  {iInsertType = 0;}
            else            {iInsertType = 1;}
        }
        else if((*iteHead).iLine > (*ite).iLine)
        {
            iInsertType = 1;
        }
        else
        {
            if(bStartLine)  {iInsertType = 2;}
            else            {iInsertType = 1;}
        }


        if(iInsertType == 0)
        { 
            (*iteHead).iCol = (*ite).iCol;
            iDspLine++;
        }
        else if(iInsertType == 1)
        {
            m_lstLineHead.insert(iteHead, *ite);
            iDspLine++;
        }
        else
        {
            while((*iteHead).iLine < (*ite).iLine)
            {
                if (iteHead == m_lstLineHead.end())
                {
                    iteHead = m_lstLineHead.end();
                    break;
                }
                
                m_lstLineHead.erase(iteHead);

                if(iDspLine >= static_cast<int>(m_lstLineHead.size()))
                {
                    iteHead = m_lstLineHead.end();
                    break;
                }

                iteHead = m_lstLineHead.begin() + iDspLine;

                if ((*iteHead).iLine == (*ite).iLine)
                {
                    break;
                }
            }
            m_lstLineHead.insert(iteHead, *ite);
            iDspLine++;
        }
    }

    iteHead = m_lstLineHead.begin() + iDspLine;
    if (iteHead != m_lstLineHead.end())
    {
        if((*iteHead).iLine == iStartLine)
        {
            while((*iteHead).iLine == iStartLine)
            {
                //if (iteHead == m_lstLineHead.end())
                //{
                //    break;
                //}
                m_lstLineHead.erase(iteHead);
                iteHead = m_lstLineHead.begin() + iDspLine;

                if (iteHead == m_lstLineHead.end())
                {
                    break;
                }
            }
        }
    }

    iteHead = m_lstLineHead.begin() + iDspLine;
    if (iteHead == m_lstLineHead.end())
    {
        return;
    }

    int iOffset;
    if(iModifyLineNum < 0)
    {
        int iDeleteLine;
        iOffset = (iModifyLineNum);

        iDeleteLine = iStartLine - iOffset;
        if((*iteHead).iLine <= iDeleteLine)
        {
            while((*iteHead).iLine <= iDeleteLine)
            {
                m_lstLineHead.erase(iteHead);
                iteHead = m_lstLineHead.begin() + iDspLine;
                if (iteHead == m_lstLineHead.end())
                {
                    break;
                }
            }
        }
        OffsetDispLine(iDspLine, iOffset);
    }
    else if(iModifyLineNum > 0)
    {
        iOffset = iModifyLineNum - 1;
        OffsetDispLine(iDspLine, iOffset);
    }
#if 0
DB_PRINT(_T("  -----------\n"));
for(int i = 0; i< static_cast<int>(m_lstLineHead.size()); i++)
{
    DB_PRINT(_T("  m_lstLineHead2 %d,%d\n"),m_lstLineHead[i].iLine, m_lstLineHead[i].iCol);
}

DB_PRINT(_T("  End -----------\n"));
#endif
}

/**
 *  @brief   行表示の有無
 *  @param   [in] iDspLine 表示行
 *  @retval  なし
 *  @note
 */
bool CMtEditorWindow::IsVisivleLine(int iDspLine) const
{
    if (m_iScrollY > iDspLine)
    {
        return FALSE;
    }

    int iVisibleMax;
    iVisibleMax = m_iScrollY + (m_iHeight / m_iFontHeight);

    if (iDspLine > iVisibleMax)
    {
        return false;
    }

    return true;
}

/**
 *  @brief   ブレークポイント描画
 *  @param   [in] iDspLine       描画行
 *  @param   [in] bDrawDirect true:直接描画 
 *                            false:バックバッファー
 *  @retval  なし
 *  @note
 */
void CMtEditorWindow::DrawBreakPoint(int iDspLine, bool bDrawDirect) const
{
    if(!IsVisivleLine(iDspLine))
    {
        return;
    }

    HDC hDrawDC;

    if (!bDrawDirect)
    {
        hDrawDC = m_hDc;
    }
    else
    {
        hDrawDC = GetDC(m_hWnd);
    }

    //TODO: 後で、複数回計算を行わないように、くくりだす
    int iBreakPointSize;
    if (m_iFontHeight > m_iIndicatorWidth)
    {
        iBreakPointSize = (m_iIndicatorWidth - 2) / 2;
    }
    else
    {
        iBreakPointSize = (m_iFontHeight - 2) / 2;
    }

    if (iBreakPointSize < 2 )
    {
        iBreakPointSize = 2;
    }

    int iLineYTop   = (iDspLine - m_iScrollY) * m_iFontHeight + m_iTopMargin;
    RECT rcBreak;

    int iOffset = m_iFontHeight / 2 - iBreakPointSize;
    
    if (bDrawDirect)
    {
        rcBreak.top    = iLineYTop + iOffset;
    }
    else
    {
        rcBreak.top    = iOffset;
    }
    rcBreak.bottom = rcBreak.top + 2 * iBreakPointSize;
    rcBreak.left   = iOffset;
    rcBreak.right  = iOffset + 2 * iBreakPointSize;

    //TODO: ブラシのキャッシュを作成
    HBRUSH hBr;

    int iLine;
    iLine = m_lstLineHead[iDspLine].iLine;
    BRAKE_POINT_STS eBreak = m_Buf.GetBreakPoint(iLine);
    if( eBreak == BP_NONE)
    {
        if (bDrawDirect)
        {
            hBr = ::CreateSolidBrush(m_crIndicator);
            ::FillRect(hDrawDC, &rcBreak, hBr);
        }
        return;
    }
    else if(eBreak == BP_ENABLE)
    {
        hBr = ::CreateSolidBrush(RGB(255,0,0));
    }
    else
    {
        hBr = ::CreateSolidBrush(RGB(0,0,0));
    }

    HPEN hPen;
    HBRUSH hBrOld;

    hPen = static_cast<HPEN>(::GetStockObject(BLACK_PEN));
    hBrOld = static_cast<HBRUSH>(::SelectObject(hDrawDC, hBr));
    SelectObject(hDrawDC , hPen);

    ::Ellipse(hDrawDC,  rcBreak.left, rcBreak.top,
                       rcBreak.right, rcBreak.bottom);

    ::SelectObject(hDrawDC, hBrOld);
    ::DeleteObject(hBr);

}

/**
 *  @brief   実行行矢印描画
 *  @param   [in] iLine       描画行
 *  @param   [in] bDrawDirect true:直接描画 
 *                            false:バックバッファー
 *  @retval  なし
 *  @note
 */
void CMtEditorWindow::DrawExecIndicator(int iDspLine, bool bDrawDirect)
{
    int iLine;
    iLine = m_lstLineHead[iDspLine].iLine;

    if (m_iExecLine != iLine)
    {
        return;
    }

    if(!IsVisivleLine(iDspLine))
    {
        return;
    }

    HDC hDrawDC;

    if (!bDrawDirect)
    {
        hDrawDC = m_hDc;
    }
    else
    {
        hDrawDC = GetDC(m_hWnd);
    }

    //TODO: 後で、複数回計算を行わないように、くくりだす
    int iBreakPointSize;
    if (m_iFontHeight > m_iIndicatorWidth)
    {
        iBreakPointSize = (m_iIndicatorWidth - 2) / 2;
    }
    else
    {
        iBreakPointSize = (m_iFontHeight - 2) / 2;
    }

    if (iBreakPointSize < 2 )
    {
        iBreakPointSize = 2;
    }

    int iLineYTop   = (iDspLine - m_iScrollY) * m_iFontHeight + m_iTopMargin;
    RECT rcBreak;

    int iOffset = m_iFontHeight / 2 - iBreakPointSize;
    
    if (bDrawDirect)
    {
        rcBreak.top    = iLineYTop + iOffset;
    }
    else
    {
        rcBreak.top    = iOffset;
    }
    rcBreak.bottom = rcBreak.top + 2 * iBreakPointSize;
    rcBreak.left   = iOffset;
    rcBreak.right  = iOffset + 2 * iBreakPointSize;


    HPEN hPen;
    HBRUSH hBr;
    HBRUSH hBrOld;
    POINT   lstPt[7];

    hBr = ::CreateSolidBrush(RGB(255,255,0));


    hPen = static_cast<HPEN>(::GetStockObject(BLACK_PEN));
    hBrOld = static_cast<HBRUSH>(::SelectObject(hDrawDC, hBr));
    SelectObject(hDrawDC , hPen);

    int iUnit = static_cast<int>(::floor(iBreakPointSize / 2.0 ));

    lstPt[0].x = rcBreak.right;
    lstPt[0].y = rcBreak.top + iBreakPointSize;

    lstPt[1].x = lstPt[0].x - iBreakPointSize;
    lstPt[1].y = rcBreak.top + 1;

    lstPt[2].x = lstPt[1].x;
    lstPt[2].y = rcBreak.top + iUnit;

    lstPt[3].x = rcBreak.left + 2;
    lstPt[3].y = lstPt[2].y;

    lstPt[4].x = lstPt[3].x;
    lstPt[4].y = lstPt[3].y + iBreakPointSize;

    lstPt[5].x = lstPt[1].x;
    lstPt[5].y = lstPt[4].y;

    lstPt[6].x = lstPt[1].x;
    lstPt[6].y = rcBreak.bottom - 1;

    ::Polygon(hDrawDC, &lstPt[0], sizeof(lstPt) / sizeof(lstPt[0]));

    ::SelectObject(hDrawDC, hBrOld);
    ::DeleteObject(hBr);
}


/**
 * @brief   ドキュメント変更監視設定
 * @param   [in]  eExec
 * @retval  true 成功
 * @note	
 */
bool CMtEditorWindow::ObservChangeDocument(E_EXEC_OBSERVR eExec)
{
    return m_Buf.ObservChangeDocument(eExec);
}

/*
//!< ドキュメント変更監視クリア
bool CMtEditorWindow::ClearObservChangeDocument()
{

}
*/

/**
 * @brief   ドキュメント変更コールバック関数登録
 * @param   [in] pChgDocument
 * @param   [in] pData
 * @retval  true 登録成功
 * @note	
 */
bool CMtEditorWindow::SetObservCallback(void* pData,
      void (CALLBACK* pChgDocument)(void* , bool ))
{
    return m_Buf.SetObservCallback(pData, pChgDocument);
}


bool CMtEditorWindow::_IsTripleClick( int iX, int iY )
{
    DWORD dwCur =  ::GetTickCount();
    DWORD dwDiff;

    if (dwCur > m_dwLastDblClickTime)
    {
        dwDiff = dwCur - m_dwLastDblClickTime; 
    }
    else
    {
        dwDiff = 0xffffffff - m_dwLastDblClickTime + dwCur;
    }

    if (dwDiff > GetDoubleClickTime())
    {
        return false;
    }

    int iDiffX = m_posLastDblClick.x - iX;
    int iDiffY = m_posLastDblClick.y - iY;

    if (iDiffX > GetSystemMetrics(SM_CXDOUBLECLK))
    {
        return false;
    }

    if (iDiffY > GetSystemMetrics(SM_CYDOUBLECLK))
    {
        return false;
    }
    	
	return true;
}


//単語選択
void CMtEditorWindow::SelectWord(LINE_POS pos)
{
    if (pos.iLine >= m_Buf.GetLineNum())
    {
        return;
    }

    LINE_POS posStart = m_Buf.GetWordHead(pos);
    LINE_POS posEnd   = m_Buf.GetWordEnd(pos);

    int iLineSize = static_cast<int>(m_Buf.GetLineString(pos.iLine)->size());
    if (posEnd.iCol < iLineSize)
    {
        posEnd.iCol++;
    }
    m_posCaret = posEnd;
    m_posSelectCaret = posStart;
    PresetRedrawArea(m_posCaret, m_posSelectCaret);
    RedrawPreset();
    return;
}

//単語選択
void CMtEditorWindow::SelectLine(LINE_POS pos)
{
    int iLineSize = static_cast<int>(m_Buf.GetLineString(pos.iLine)->size());
    m_posSelectCaret.iLine = pos.iLine;
    m_posSelectCaret.iCol = 0;
    m_posCaret.iLine = pos.iLine;
    m_posCaret.iCol = iLineSize;
    PresetRedrawArea(m_posCaret, m_posSelectCaret);
    RedrawPreset();
}

/**
 *  @brief   テスト
 *  @param   なし
 *  @retval  なし
 *  @note
 */
#ifdef _TEST
void CMtEditorWindow::Test()
{

    //===================
    //LINE_POSテスト
    //===================
    LINE_POS pos1;
    LINE_POS pos2;

    assert(pos1.iCol  == 0);
    assert(pos1.iLine == 0);

    pos1.iCol  = 20;
    pos1.iLine = 10;

    pos2 = pos1;

    assert(pos2.iCol  == 20);
    assert(pos2.iLine == 10);

    assert(pos1 == pos2);


    pos2.iCol = 21;

    assert(pos1 < pos2);

    pos2.iLine = 9;

    assert(pos1 > pos2);
    //===================

    std::vector<LINE_POS> lstBkup;
    std::vector<LINE_POS> lstInsert;

    lstBkup = m_lstLineHead;

    LINE_POS posLine;

    posLine.Set(2,2000);   lstInsert.push_back(posLine);
    posLine.Set(2,2001);   lstInsert.push_back(posLine);
    posLine.Set(3,3000);   lstInsert.push_back(posLine);
    posLine.Set(3,3001);   lstInsert.push_back(posLine);
    posLine.Set(4,4000);   lstInsert.push_back(posLine);
    posLine.Set(4,4001);   lstInsert.push_back(posLine);
    posLine.Set(4,4002);   lstInsert.push_back(posLine);
    posLine.Set(4,4003);  lstInsert.push_back(posLine);

    m_lstLineHead.clear();
    posLine.Set(0,  0);   m_lstLineHead.push_back(posLine);  //00
    posLine.Set(0,  1);   m_lstLineHead.push_back(posLine);  //01
    posLine.Set(1,100);   m_lstLineHead.push_back(posLine);  //02
    posLine.Set(1,101);   m_lstLineHead.push_back(posLine);  //03
    posLine.Set(1,102);   m_lstLineHead.push_back(posLine);  //04
    posLine.Set(2,200);   m_lstLineHead.push_back(posLine);  //05
    posLine.Set(3,300);   m_lstLineHead.push_back(posLine);  //06
    posLine.Set(3,301);   m_lstLineHead.push_back(posLine);  //07
    posLine.Set(3,302);   m_lstLineHead.push_back(posLine);  //08
    posLine.Set(4,400);   m_lstLineHead.push_back(posLine);  //09
    posLine.Set(4,401);   m_lstLineHead.push_back(posLine);  //10
    posLine.Set(5,500);   m_lstLineHead.push_back(posLine);  //11

    ReplaceHeadList(5, lstInsert, 3);

    //m_lstLineHead[ 0]  (0,  0)     m_lstLineHead[ 0]  (0,  0)
    //m_lstLineHead[ 1]  (0,  1)     m_lstLineHead[ 1]  (0,  1)
    //m_lstLineHead[ 2]  (1,100)     m_lstLineHead[ 2]  (1,100)
    //m_lstLineHead[ 3]  (1,101)     m_lstLineHead[ 3]  (1,101)
    //m_lstLineHead[ 4]  (1,102)     m_lstLineHead[ 4]  (1,102)
    //m_lstLineHead[ 5]  (2,200)   ->m_lstLineHead[ 5]  (2,200)   (2,2000)
    //m_lstLineHead[ 6]  (3,300)     m_lstLineHead[ 6]            (2,2001)
    //m_lstLineHead[ 7]  (3,301)     m_lstLineHead[ 7]            (3,3000)
    //m_lstLineHead[ 8]  (3,302)     m_lstLineHead[ 8]            (3,3001)
    //m_lstLineHead[ 9]  (4,400)     m_lstLineHead[ 9]            (4,4000)
    //m_lstLineHead[10]  (4,401)     m_lstLineHead[10]            (4,4001)
    //m_lstLineHead[11]  (5,500)     m_lstLineHead[11]            (4,4002)
    //                               m_lstLineHead[12]            (4,4003)
    //                               m_lstLineHead[13]  (5,300)
    //                               m_lstLineHead[14]  (5,301)
    //                               m_lstLineHead[15]  (5,302)
    //                               m_lstLineHead[16]  (6,400)
    //                               m_lstLineHead[17]  (6,401)
    //                               m_lstLineHead[18]  (7,500)
                                                        
    assert(m_lstLineHead[ 5] == lstInsert[0]);
    assert(m_lstLineHead[ 6] == lstInsert[1]);
    assert(m_lstLineHead[ 7] == lstInsert[2]);
    assert(m_lstLineHead[ 8] == lstInsert[3]);
    assert(m_lstLineHead[ 9] == lstInsert[4]);
    assert(m_lstLineHead[10] == lstInsert[5]);
    assert(m_lstLineHead[11] == lstInsert[6]);
    assert(m_lstLineHead[12] == lstInsert[7]);
    assert(m_lstLineHead[13] == LINE_POS(5,300));
    assert(m_lstLineHead[14] == LINE_POS(5,301));
    assert(m_lstLineHead[15] == LINE_POS(5,302));
    assert(m_lstLineHead[16] == LINE_POS(6,400));
    assert(m_lstLineHead[17] == LINE_POS(6,401));
    assert(m_lstLineHead[18] == LINE_POS(7,500));


    //=======================================================
    lstInsert.clear();

    posLine.Set(2,0);   lstInsert.push_back(posLine);
    posLine.Set(2,26);   lstInsert.push_back(posLine);
    posLine.Set(3,0);   lstInsert.push_back(posLine);
    posLine.Set(3,26);   lstInsert.push_back(posLine);

    m_lstLineHead.clear();
    posLine.Set(0,  0);   m_lstLineHead.push_back(posLine);  //00
    posLine.Set(1,  0);   m_lstLineHead.push_back(posLine);  //01
    posLine.Set(2,  0);   m_lstLineHead.push_back(posLine);  //02
    posLine.Set(2,  26);  m_lstLineHead.push_back(posLine);  //03
    posLine.Set(2,  52);  m_lstLineHead.push_back(posLine);  //04
    posLine.Set(3,  0);   m_lstLineHead.push_back(posLine);  //05
    posLine.Set(4,  0);   m_lstLineHead.push_back(posLine);  //06
    posLine.Set(5,  0);   m_lstLineHead.push_back(posLine);  //07

    ReplaceHeadList(2, lstInsert, 2);

    assert(m_lstLineHead.size() == 9);
    assert(m_lstLineHead[ 0] == LINE_POS(   0,  0));
    assert(m_lstLineHead[ 1] == LINE_POS(   1,  0));
    assert(m_lstLineHead[ 2] == LINE_POS(   2,  0));
    assert(m_lstLineHead[ 3] == LINE_POS(   2,  26));
    assert(m_lstLineHead[ 4] == LINE_POS(   3,  0));
    assert(m_lstLineHead[ 5] == LINE_POS(   3,  26));
    assert(m_lstLineHead[ 6] == LINE_POS(   4,  0));
    assert(m_lstLineHead[ 7] == LINE_POS(   5,  0));
    assert(m_lstLineHead[ 8] == LINE_POS(   6,  0));


    //=======================================================
    lstInsert.clear();

    posLine.Set(5, 0);   lstInsert.push_back(posLine);
    posLine.Set(5,56);   lstInsert.push_back(posLine);
    posLine.Set(6, 0);   lstInsert.push_back(posLine);
    posLine.Set(6,41);   lstInsert.push_back(posLine);
    posLine.Set(6,82);   lstInsert.push_back(posLine);

    m_lstLineHead.clear();
    posLine.Set(0,  0);   m_lstLineHead.push_back(posLine);  //00
    posLine.Set(1,  0);   m_lstLineHead.push_back(posLine);  //01
    posLine.Set(2,  0);   m_lstLineHead.push_back(posLine);  //02
    posLine.Set(3,  0);   m_lstLineHead.push_back(posLine);  //03
    posLine.Set(4,  0);   m_lstLineHead.push_back(posLine);  //04
    posLine.Set(5,  0);   m_lstLineHead.push_back(posLine);  //05
    posLine.Set(5, 56);   m_lstLineHead.push_back(posLine);  //06
    posLine.Set(5, 97);   m_lstLineHead.push_back(posLine);  //07
    posLine.Set(5,137);   m_lstLineHead.push_back(posLine);  //08

    ReplaceHeadList(5, lstInsert, 2);

    assert(m_lstLineHead.size() == 10);
    assert(m_lstLineHead[ 0] == LINE_POS(   0,   0));
    assert(m_lstLineHead[ 1] == LINE_POS(   1,   0));
    assert(m_lstLineHead[ 2] == LINE_POS(   2,   0));
    assert(m_lstLineHead[ 3] == LINE_POS(   3,   0));
    assert(m_lstLineHead[ 4] == LINE_POS(   4,   0));
    assert(m_lstLineHead[ 5] == LINE_POS(   5,   0));
    assert(m_lstLineHead[ 6] == LINE_POS(   5,  56));
    assert(m_lstLineHead[ 7] == LINE_POS(   6,   0));
    assert(m_lstLineHead[ 8] == LINE_POS(   6,  41));
    assert(m_lstLineHead[ 9] == LINE_POS(   6,  82));

    //=======================================================
    lstInsert.clear();

    posLine.Set(0,1000);   lstInsert.push_back(posLine);
    //posLine.Set(3,3000);   lstInsert.push_back(posLine);
    //posLine.Set(3,3001);   lstInsert.push_back(posLine);
    //posLine.Set(4,4000);   lstInsert.push_back(posLine);
    //posLine.Set(4,4001);   lstInsert.push_back(posLine);
    //posLine.Set(4,4002);   lstInsert.push_back(posLine);
    //posLine.Set(4,4003);  lstInsert.push_back(posLine);

    m_lstLineHead.clear();
    posLine.Set(0,  0);   m_lstLineHead.push_back(posLine);  //00
    posLine.Set(1,  1);   m_lstLineHead.push_back(posLine);  //01
    posLine.Set(2,  2);   m_lstLineHead.push_back(posLine);  //02

    ReplaceHeadList(0, lstInsert, 0);

    assert(m_lstLineHead.size() == 3);
    assert(m_lstLineHead[ 0] == LINE_POS(0,1000));
    assert(m_lstLineHead[ 1] == LINE_POS(1,1));
    assert(m_lstLineHead[ 2] == LINE_POS(2,2));



    //=======================================================
    lstInsert.clear();

    posLine.Set(0,1000);   lstInsert.push_back(posLine);

    m_lstLineHead.clear();
    posLine.Set(0,  0);   m_lstLineHead.push_back(posLine);  //00
    posLine.Set(1,  1);   m_lstLineHead.push_back(posLine);  //01
    posLine.Set(2,  2);   m_lstLineHead.push_back(posLine);  //02

    ReplaceHeadList(0, lstInsert, -1);

    assert(m_lstLineHead.size() == 2);
    assert(m_lstLineHead[ 0] == LINE_POS(0,1000));
    assert(m_lstLineHead[ 1] == LINE_POS(1,2));


    //=====================================================

    lstInsert.clear();

    posLine.Set(2,2000);   lstInsert.push_back(posLine);
    posLine.Set(2,2001);   lstInsert.push_back(posLine);
    //posLine.Set(3,3000);   lstInsert.push_back(posLine);
    //posLine.Set(3,3001);   lstInsert.push_back(posLine);
    //posLine.Set(4,4000);   lstInsert.push_back(posLine);
    //posLine.Set(4,4001);   lstInsert.push_back(posLine);
    //posLine.Set(4,4002);   lstInsert.push_back(posLine);
    //posLine.Set(4,4003);  lstInsert.push_back(posLine);

    m_lstLineHead.clear();
    posLine.Set(0,  0);   m_lstLineHead.push_back(posLine);  //00
    posLine.Set(0,  1);   m_lstLineHead.push_back(posLine);  //01
    posLine.Set(1,100);   m_lstLineHead.push_back(posLine);  //02
    posLine.Set(1,101);   m_lstLineHead.push_back(posLine);  //03
    posLine.Set(1,102);   m_lstLineHead.push_back(posLine);  //04
    posLine.Set(2,200);   m_lstLineHead.push_back(posLine);  //05
    posLine.Set(2,201);   m_lstLineHead.push_back(posLine);  //06
    posLine.Set(2,202);   m_lstLineHead.push_back(posLine);  //07
    posLine.Set(2,203);   m_lstLineHead.push_back(posLine);  //08
    posLine.Set(2,204);   m_lstLineHead.push_back(posLine);  //09
    posLine.Set(3,300);   m_lstLineHead.push_back(posLine);  //10
    posLine.Set(3,301);   m_lstLineHead.push_back(posLine);  //11
    posLine.Set(3,302);   m_lstLineHead.push_back(posLine);  //12
    posLine.Set(4,400);   m_lstLineHead.push_back(posLine);  //13
    posLine.Set(4,401);   m_lstLineHead.push_back(posLine);  //14
    posLine.Set(5,500);   m_lstLineHead.push_back(posLine);  //14

    ReplaceHeadList(5, lstInsert, -2);

    //m_lstLineHead[ 0]  (0,  0)     m_lstLineHead[ 0]  (0,  0)
    //m_lstLineHead[ 1]  (0,  1)     m_lstLineHead[ 1]  (0,  1)
    //m_lstLineHead[ 2]  (1,100)     m_lstLineHead[ 2]  (1,100)
    //m_lstLineHead[ 3]  (1,101)     m_lstLineHead[ 3]  (1,101)
    //m_lstLineHead[ 4]  (1,102)     m_lstLineHead[ 4]  (1,102)
    //m_lstLineHead[ 5]  (2,200)   ->m_lstLineHead[ 5]  (2,200)   (2,2000)
    //m_lstLineHead[ 6]  (2,201)   ->m_lstLineHead[ 6]  (2,201)   (2,2001)
    //m_lstLineHead[ 7]  (2,202)   ->m_lstLineHead[  ]  (2,202)   
    //m_lstLineHead[ 8]  (2,203)   ->m_lstLineHead[  ]  (2,203)   
    //m_lstLineHead[ 9]  (2,204)   ->m_lstLineHead[  ]  (2,204)   
    //m_lstLineHead[10]  (3,300)     m_lstLineHead[  ]  
    //m_lstLineHead[11]  (3,301)     m_lstLineHead[  ]  
    //m_lstLineHead[12]  (3,302)     m_lstLineHead[  ]  
    //m_lstLineHead[13]  (4,400)     m_lstLineHead[  ]  
    //m_lstLineHead[14]  (4,401)     m_lstLineHead[  ]  
    //m_lstLineHead[15]  (5,500)     m_lstLineHead[ 7]  (3,500)
                                                        
    assert(m_lstLineHead[ 5] == lstInsert[0]);
    assert(m_lstLineHead[ 6] == lstInsert[1]);
    assert(m_lstLineHead[ 7] == LINE_POS(3,500));


    //=======================
    // GetTopDispLineテスト
    //=======================
    assert(GetTopDispLine(0) == 0);
    assert(GetTopDispLine(1) == 0);
    assert(GetTopDispLine(2) == 2);
    assert(GetTopDispLine(3) == 2);
    assert(GetTopDispLine(4) == 2);
    assert(GetTopDispLine(5) == 5);
    assert(GetTopDispLine(6) == 5);
    assert(GetTopDispLine(7) == 7);



    //=======================
    // CalcDispLineLengthテスト
    //======================
    
       m_Buf.Clear();

    StdString str;
    str = _T("0123\r\n012345\r\n012345678\r\n012345678\r\n01234567890");
    m_Buf.SetText(str.c_str());
    
    m_lstLineHead.clear();

    
    m_lstLineHead.clear();

    posLine.Set(0,  0);   m_lstLineHead.push_back(posLine); //0123          0
    posLine.Set(1,  0);   m_lstLineHead.push_back(posLine); //012           1
    posLine.Set(1,  3);   m_lstLineHead.push_back(posLine); //345           2
    posLine.Set(2,  0);   m_lstLineHead.push_back(posLine); //01234         3
    posLine.Set(2,  5);   m_lstLineHead.push_back(posLine); //5678          4
    posLine.Set(3,  0);   m_lstLineHead.push_back(posLine); //01234567      5
    posLine.Set(3,  8);   m_lstLineHead.push_back(posLine); //8             6
    posLine.Set(4,  0);   m_lstLineHead.push_back(posLine); //012           7
    posLine.Set(4,  3);   m_lstLineHead.push_back(posLine); //345           8
    posLine.Set(4,  6);   m_lstLineHead.push_back(posLine); //67890         9


    assert(CalcDispLineLength( 0) == 4);
    assert(CalcDispLineLength( 1) == 3);
    assert(CalcDispLineLength( 2) == 3);
    assert(CalcDispLineLength( 3) == 5);
    assert(CalcDispLineLength( 4) == 4);
    assert(CalcDispLineLength( 5) == 8);
    assert(CalcDispLineLength( 6) == 1);
    assert(CalcDispLineLength( 7) == 3);
    assert(CalcDispLineLength( 8) == 3);
    assert(CalcDispLineLength( 9) == 5);


    //=======================
    // LineToDispLineテスト
    //=======================
    //int CMtEditorWindow::LineToDispLine( const int iLine) const
    assert(LineToDispLine( 0) == 0);
    assert(LineToDispLine( 1) == 1);
    assert(LineToDispLine( 2) == 3);
    assert(LineToDispLine( 3) == 5);
    assert(LineToDispLine( 4) == 7);
    //エラー処理


    //=======================
    // DataPosToCaretテスト
    //=======================
    //LINE_POS CMtEditorWindow::DataPosToCaret(const LINE_POS& posData) const
    //                 Line
    //0123          0   0
    //012           1   1
    //345           2   1
    //01234         3   2
    //5678          4   2
    //01234567      5   3
    //8             6   3
    //012           7   4
    //345           8   4
    //67890         9   4


    assert(DataPosToCaret(LINE_POS(0,0)) == LINE_POS(0,0));
    assert(DataPosToCaret(LINE_POS(1,5)) == LINE_POS(2,2));
    assert(DataPosToCaret(LINE_POS(2,8)) == LINE_POS(4,3));
    assert(DataPosToCaret(LINE_POS(3,1)) == LINE_POS(5,1));
    assert(DataPosToCaret(LINE_POS(4,9)) == LINE_POS(9,3));

    //エラー処理



    //=======================
    // CalcTabPixlテスト
    //=======================

    //前提条件
    assert(m_es.bEnableLineNumber);

    m_iIndicatorWidth  = 20;
    m_iLineNumberWidth =  3;
    m_iLeftMargin = 5;
    m_iCharWidth  = 10;
    m_iTabStop = 4;


    assert(CalcTabPixl(28 + 0)  == (4 * 10) * 1 + 28);
    assert(CalcTabPixl(28 +10)  == (4 * 10) * 1 + 28);
    assert(CalcTabPixl(28 +39)  == (4 * 10) * 1 + 28);
    assert(CalcTabPixl(28 +40)  == (4 * 10) * 2 + 28);



/*
int CMtEditorWindow::CalcTabPixl(int iOffset) const
{
    int iMargin   = m_iIndicatorWidth + m_iLeftMargin; 
    int iTabWidth = m_iCharWidth * m_iTabStop;

    if(m_es.bEnableLineNumber)
    {
        iMargin += m_iLineNumberWidth;
    }
    
    int iTabPixel;
    iTabPixel = (((iOffset - iMargin) % m_iTabStop) + 1) 
                 * iTabWidth + iMargin;

    return iTabPixel;
*/

    //str = _T("0123\r\n012345\r\n012345678\r\n012345678\r\n01234567890");
    m_lstLineHead.clear();

    
    m_lstLineHead.clear();

   // m_lstLineHead = lstBkup;
}
#endif _TEST




}//namespace MT_EDITOR
