#ifndef _MT_EDITOR_WINDOW_H
#define _MT_EDITOR_WINDOW_H

#include "MtEditorBuffer.h"
#include <windows.h>
#include <imm.h>
#include <vector>
#include <map>

#ifdef OP_XP_STYLE
#include <vssym32.h>
#endif	// OP_XP_STYLE

namespace MT_EDITOR
{

class CMtEditorWindow
{
public:

protected:
    enum E_WORDWRAP
    {
        WW_NONW,        //ワードラップなし
        WW_WORDWRAP,    //ワードラップ
        WW_FIX          //固定長
    };



    CMtEditorBuffer m_Buf;

    //先頭位置リスト
    std::vector<LINE_POS> m_lstLineHead;

    HWND m_hWnd;

    int    m_iId;

	// 描画用情報
	HDC     m_hDc;
	HBITMAP m_hRetBmp;
	HFONT   m_hFont;
    HFONT   m_hOldFont;

	// フォント
	int m_iFontHeight;
	int m_iCharWidth;

	// スクロール バー
	int m_iScrollX;
	int m_iScrollMaxX;
	int m_iScrollY;
	int m_iScrollMaxY;


    // タブストップ
	int m_iTabStop;
	// 左マージン
	int m_iLeftMargin;
	// 上マージン
	int m_iTopMargin;
	// 右マージン
	int m_iRightMargin;
	// 下マージン
	int m_iBottomMargin;
	// 行間
	int m_iSpacing;

    //インジケータ幅
    int m_iIndicatorWidth;

    //インジケータ行
    int m_iExecLine;

    //行番号幅
    int m_iLineNumberWidth;

    //マージン
    int m_iMargin;

    //行番号桁数
    int m_iLineNumberDigit;

    EDITOR_SETTING m_es;

	// 文字の幅
	BYTE m_cWidth[256];


    //------------------
    // 色
    //------------------
    // インジケータ色
    COLORREF m_crIndicator;

    // 現在の色
    COLORREF m_crCurrent;

    //折り返し
    E_WORDWRAP m_eWordwrap;

	// 1行の文字数
	int m_iLineCharMax;

    // 行の最大幅(マージンは含まない)
	int m_iLinePixlWidthMax;

    // 描画領域の幅
	int m_iWidth;

    // 描画領域の高さ
	int m_iHeight;

    // 描画領域の行数
	int m_iLineNum;

    //キャレット位置
    LINE_POS m_posCaret;
    LINE_POS m_posSelectCaret;
    int      m_iCaretXPixel;

    // マウス押下
    bool m_bMousedown;
    
    // ブレークポイント
    int m_iClickBreakPoint;

    // 挿入モード
    bool m_bInsertMode;

    // 読み取り専用
    bool m_bReadOnly;

    // 再描画位置記憶
    LINE_POS m_posPresetStart;
    LINE_POS m_posPresetEnd;

    //キャレット表示フラグ
    bool m_bVisibleCaret;

    //トリプルクリック判定フラグ
    POINT m_posLastDblClick;
    DWORD m_dwLastDblClickTime;
    bool  m_bDblCkick;
    void (CALLBACK* m_pSetBreakPointModified)(void* pData, int iLine, bool bSet);

    void* m_pParent;



#ifdef OP_XP_STYLE
	// XP
	HTHEME  m_hTheme;
	HMODULE m_hModThemes;

    typedef HRESULT ( STDAPICALLTYPE* CLOSETHEMEDATA )( HTHEME hTheme );
    typedef HTHEME  ( STDAPICALLTYPE* OPENTHEMEDATA  )( HWND hwnd, LPCWSTR pszClassList );
    typedef HRESULT ( STDAPICALLTYPE* DRAWTHEMEBACKGROUND )( HTHEME hTheme, HDC hdc, int iPartId, int iStateId, const RECT *pRect, OPTIONAL const RECT *pClipRect );

    CLOSETHEMEDATA        F_CloseThemeData;
    OPENTHEMEDATA         F_OpenThemeData;
    DRAWTHEMEBACKGROUND   F_DrawThemeBackground;


#endif	// OP_XP_STYLE


public:
    CMtEditorWindow(void);

    ~CMtEditorWindow(void);

    void Create(HWND hWnd);

    void SetData(const EDITOR_SETTING& setting);

    void GetData(EDITOR_SETTING* pSetting);

    void SetIndicatorWidth(int iWidth);

    void SetLeftMargin(int iMargin);

    //!< ウィンドウプロシージャ
    LRESULT  WindowProc(const UINT msg, const WPARAM wParam, const LPARAM lParam, BOOL* pOverRide);


    //!< ブレークポイント全解除
    void ClearBreakPoint();

    //!< ブレークポイント取得
    void GetBreakPoint
        (std::vector<BREAK_POINT_DATA>* pBreakPointList) const;

    //!< ブレークポイント設定
    void SetBreakPoint
         (const std::vector<BREAK_POINT_DATA>& lstBreakPoint);

    //!< ブレークポイント追加
    bool AddBreakPoint(const int iLine, bool bEnable);

    //!< ブレークポイント削除
    bool DelBreakPoint(const int iLine);

    //!< ブレークポイント問合せ
    bool IsBreakPoint(const int iLine);

    //!< ブレークポイントコールバック
    bool SetBreakPointCallback(void* pParent, void (CALLBACK* pSetBreakPointModified)(void* pData, int iLine, bool bSet));

    //!< 実行行の設定
    void SetExecLine( int iLine, bool bEnsure);

    //!< 文字列の設定
    //BOOL SetString(const TCHAR *str, const DWORD len);

    //!< 文字列の設定
    bool SetString(const StdString& str);

    //!< 文字列の取得
    //int GetString(TCHAR *str, const DWORD len);

    //!< 文字列の取得
    int GetString(StdString* pString);

    //!< フォント設定
    void SetFont(LOGFONT lf);
    void SetFont(const HFONT hFont);
    
    //!< 論理行数取得
    int GetLineCount();

    //!< UnDoの実行
	bool Undo();

    //!< ReDoの実行
	bool Redo();

    //-----------------------
	// Clipboard operations
    //-----------------------
    //!< 選択中テキスト消去
    void Clear();

    //!< 選択中テキストコピー
    void Copy();

    //!< 選択中テキスト切り取り
    void Cut();

    //!< テキスト貼り付け
    void Paste();
    //-----------------------
    //!> 行を表示
    void VisibleLine(int iLine);

    //!< キャレット位置取得
    void GetCaretPosition(int* pLine, int* pCol) const;

    //!< キャレット位置設定
    void SetCaretPosition(int iLine, int iCol);

    //!< 再描画
    void Redraw();

    //!< キャレット位置を表示
    void EnsureVisible();

    COLORREF Token2Coloer(E_TOKEN eToken);

    //!< ドキュメント変更監視
    bool ObservChangeDocument(E_EXEC_OBSERVR eExec);

    //!< ドキュメント変更監視クリア
    //bool ClearObservChangeDocument();

    //!< ドキュメント変更コールバック関数登録
    bool SetObservCallback(void* pData,
          void (CALLBACK* pChgDocument)(void*, bool));


protected:
    //!< 文字の描画幅の取得
    int GetCharExtent(const TCHAR *str, int iLen);

    //!< 描画領域の取得
    bool GetEditRect(RECT *rect) const;


    //!< 文字列描画
    int DrawString(const HDC mdc, const RECT *drect, 
        const int left, 
        const int top, 
        const TCHAR*    pStr, 
        const int       iLength, 
        const bool sel,
        const COLORREF  crText
        );

    void DrawRect(const HDC hDc, 
       const int left, 
       const int top, 
       const int right, 
       const int bottom);


    //!< １行描画
    void DrawLine(const HDC hDc, 
                               const int iLinePos, 
                               const int iLeft,
                               const int iRight);


    LRESULT OnCreate    (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnSize      (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnPaint     (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnMouseMove (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnLButtonDown(const WPARAM wParam, const LPARAM lParam);
    LRESULT OnLButtonUp (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnKeyDown   (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnChar      (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnSetFocus  (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnHScroll   (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnVScroll   (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnImeChar   (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnImeStartComposition (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnImeComposition      (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnImeEndComposition   (const WPARAM wParam, const LPARAM lParam);
    LRESULT OnMouseWheel(const WPARAM wParam, const LPARAM lParam);
    LRESULT OnLButtonDblClick(const WPARAM wParam, const LPARAM lParam);


    //!< 描画情報の解放
    void DrawFree();
    
    //!< 描画情報の初期化
    bool DrawInit();

    void SetScrollbar();

    //表示文字の取得
    const TCHAR* GetChar(int iDispLine, int iCol) const;

    //!< キャレットのサイズ設定
    bool SetCaretSize(const LINE_POS& posCaret);

    //!< キャレットX座標取得
    int CalcCaretXPos(const HDC mdc,  const LINE_POS& posCaret);

    //!行情報の設定
    //bool LineSetInfo();

    //!< 指定範囲文字列のある行を再描画対象にする
    void LineRefresh( LINE_POS cposStart, LINE_POS cposEnd);

    int CalcTabPixl(int iOffset) const;

    bool IsIdentifier(const TCHAR& chr) const;


    int CalcLineWidth(std::vector<LINE_POS>* pList,
                      int iLine, int iMargin);

    int CalcAllLine();

    void SetDrawInfo(bool bRedraw);

    //!< キャレットのサイズ設定
    void CaretSetSize();

    //!<  座標からキャレットの位置取得
    LINE_POS CaretPointToCaret(const int x, const int y);

    //!< キャレットの移動
    void MoveCaret(const int key);

    LINE_POS MiveCaretLeft(const LINE_POS& posCaret);
    LINE_POS MovCaretRight(const LINE_POS& posCaret);


    //!< データ位置からキャレット位置への変換
    LINE_POS DataPosToCaret(const LINE_POS& posBuf) const;

    //!< キャレット位置からデータ位置への変換
    LINE_POS CaretToDataPos(const LINE_POS& posCaret) const;

    int LineToDispLine( const int iLine) const;

    //!< 表示行の長さを取得
    int CalcDispLineLength( const int iDspLine) const;

    //void DeleteStringChar(const LINE_POS& posCaret);

    int Delete(bool bRedraw);

    void Insert(const StdString& str);

    void ReplaceHeadList(int iDspLine, 
                         const std::vector<LINE_POS>& listPos,
                         int iModifyLineNum);

    //int GetDispLineEndCol(int iLine) const;

    int GetTopDispLine(int iDispLine) const;

    void OffsetDispLine(int iDispLine, int iOffset);

    void PresetRedrawArea(LINE_POS posDispStart, LINE_POS posDispEnd);

    void PresetRedrawAreaTop(LINE_POS posDispStart);

    void RedrawPreset();

    //void DeleteHead(int iDispLine, int iDeleteNum);

    //マージン再計算
    void RecalcMargin();

    bool StringToClipboard( LINE_POS posDispStart, LINE_POS posDispEnd);

    //!< ブレークポイント描画
    void DrawBreakPoint(int iLine, bool bDrawDirect) const;

    //!< 指定行が表示領域にあるか
    bool IsVisivleLine(int iDspLine) const;

    //!< 実行行矢印描画
    void DrawExecIndicator(int iLine, bool bDrawDirect);

    //!< 読み込み完了設定
    void LoadEnd();

    //!< 文章初回変更コールバック設定

    //単語選択
    void SelectWord(LINE_POS pos);

    //行選択
    void SelectLine(LINE_POS pos);

protected:
    //トリプルクリックチェック
    bool _IsTripleClick( int iX, int iY );


#ifdef _TEST
    //!< テスト
    void Test();
#endif

};

}//namespace MT_EDITOR
#endif
