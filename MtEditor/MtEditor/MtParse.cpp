#include "stdafx.h"
#include "MTParse.h"

namespace MT_EDITOR
{

std::set<StdChar>   MtParserBase::m_setSpace;
std::set<StdChar>   MtParserBase::m_setOperator;
std::set<StdString>   MtParserBase::m_setReserv;


MtParserAS::MtParserAS()
{
    m_eLang = LT_AS;
    m_setSpace.insert(_T(' '));
    m_setSpace.insert(_T('\t'));
    m_setSpace.insert(_T('　'));

    //-------------------------------
    m_setOperator.insert(_T('+'));
    m_setOperator.insert(_T('-'));
    m_setOperator.insert(_T('*'));
    m_setOperator.insert(_T('/'));

    m_setOperator.insert(_T('='));
    m_setOperator.insert(_T('<'));
    m_setOperator.insert(_T('>'));

    m_setOperator.insert(_T('?'));

    m_setOperator.insert(_T('~'));
    m_setOperator.insert(_T('!'));
    m_setOperator.insert(_T('|'));
    m_setOperator.insert(_T('&'));

    m_setOperator.insert(_T('@'));  //OnlyAS

    m_setOperator.insert(_T('.'));
    m_setOperator.insert(_T(','));

    m_setOperator.insert(_T(';'));
    m_setOperator.insert(_T('('));
    m_setOperator.insert(_T(')'));
    m_setOperator.insert(_T('['));
    m_setOperator.insert(_T(']'));
    m_setOperator.insert(_T('{'));
    m_setOperator.insert(_T('}'));


    m_setReserv.insert(_T("and"));
    m_setReserv.insert(_T("bool"));
    m_setReserv.insert(_T("break"));
    m_setReserv.insert(_T("cast"));
    m_setReserv.insert(_T("const"));
    m_setReserv.insert(_T("continue"));
    m_setReserv.insert(_T("do"));
    m_setReserv.insert(_T("double"));
    m_setReserv.insert(_T("else"));
    m_setReserv.insert(_T("false"));
    m_setReserv.insert(_T("float"));
    m_setReserv.insert(_T("for"));
    m_setReserv.insert(_T("funcdef"));
    m_setReserv.insert(_T("if"));
    m_setReserv.insert(_T("in"));
    m_setReserv.insert(_T("inout"));
    m_setReserv.insert(_T("import"));
    m_setReserv.insert(_T("int"));
    m_setReserv.insert(_T("int8"));
    m_setReserv.insert(_T("int16"));
    m_setReserv.insert(_T("int32"));
    m_setReserv.insert(_T("int64"));
    m_setReserv.insert(_T("interface"));
    m_setReserv.insert(_T("is"));
    m_setReserv.insert(_T("!is"));
    m_setReserv.insert(_T("not"));
    m_setReserv.insert(_T("null"));
    m_setReserv.insert(_T("or"));
    m_setReserv.insert(_T("out"));
    m_setReserv.insert(_T("return"));
    m_setReserv.insert(_T("true"));
    m_setReserv.insert(_T("void"));
    m_setReserv.insert(_T("while"));
    m_setReserv.insert(_T("uint"));
    m_setReserv.insert(_T("uint8"));
    m_setReserv.insert(_T("uint16"));
    m_setReserv.insert(_T("uint32"));
    m_setReserv.insert(_T("uint64"));
    m_setReserv.insert(_T("switch"));
    m_setReserv.insert(_T("class"));
    m_setReserv.insert(_T("case"));
    m_setReserv.insert(_T("default"));
    m_setReserv.insert(_T("xor"));
    m_setReserv.insert(_T("typedef"));
    m_setReserv.insert(_T("enum"));
    m_setReserv.insert(_T("private"));


#ifdef _TEST
    Test();
#endif 


}

/**
 * @brief   空白文字判別
 * @param   [in] chr  判定する文字
 * @retval  true: 空白文字
 * @note
 */
bool MtParserAS::IsWiteSpace(StdChar chr)
{
    return (m_setSpace.find(chr) != m_setSpace.end());
}

/**
 * @brief   演算子判別
 * @param   [in] chr  判定する文字
 * @retval  true: 演算子
 * @note
 */
bool MtParserAS::IsOperator(StdChar chr)
{
    return (m_setOperator.find(chr) != m_setOperator.end());
}

/**
 * @brief   予約語判別
 * @param   [in] str  判定する文字列
 * @retval  true: 予約語
 * @note
 */
bool MtParserAS::IsReserve(const StdString& str)
{
    return (m_setReserv.find(str) != m_setReserv.end());
}


/**
 * @brief   識別子先頭文字判別
 * @param   [in] chr  判定する文字
 * @retval  true: 識別子先頭文字
 * @note    _|[a-z][A-Z]
 */
bool MtParserAS::IsFirstIdentfire(StdChar chr)
{
    if (chr > 0xff)
    {
        return false;
    }

    if (::_istalpha (chr))
    {
        return true;
    }
    else if (chr == _T('_'))
    {
        return true;
    }
    return false;
}

/**
 * @brief   識別子文字判別
 * @param   [in] chr  判定する文字
 * @retval  true: 識別子先頭文字
 * @note    _|[a-z][A-Z]
 */
bool MtParserAS::IsIdentfire(StdChar chr)
{
    if (chr > 0xff)
    {
        return false;
    }

    if (::_istalpha (chr))
    {
        return true;
    }
    if (::_istdigit  (chr))
    {
        return true;
    }
    else if (chr == _T('_'))
    {
        return true;
    }
    return false;
}

/**
 * @brief   数値先頭文字判別
 * @param   [in] chr  判定する文字
 * @retval  true: 数値先頭文字文字
 * @note    .|[0-9]
 */
bool MtParserAS::IsFirstNumber(StdChar chr)
{
    if (::_istdigit(chr))
    {
        return true;
    }
    else if (chr == _T('.'))
    {
        return true;
    }
    return false;
}

/**
 * @brief   一行解析
 * @param   [out] pLstToken    解析結果
 * @param   [in]  Line         判定する行
 * @param   [in]  eBeforeToken 前回の最終文字種別
 * @retval  true: 解析成功
 * @note    
 */
/*
E_TOKEN MtParserAS::ParseLine( std::vector<PARSE_TOKEN>* pLstToken, 
                       const StdString &Line,
                       E_TOKEN eBeforeToken)
{
    size_t  nLen = Line.size();
    E_TOKEN token = eBeforeToken;

    for (size_t nPos = 0; nPos < nLen; nPos++)
    {
        switch (token)
        {
        case TK_NONE             :{token = FuncNone (pLstToken, Line, nPos);break;}
        case TK_WHITE_SPACE      :{token = FuncSpace(pLstToken, Line, nPos);break;}
        case TK_RESERVED_WORD    :
        case TK_IDENTIFIER       :{token = FuncIdentifier(pLstToken, Line, nPos);break;}
        case TK_COMMENT_ML       :{token = FuncComment(pLstToken, Line, nPos);break;}
        case TK_COMMENT_SL       :{nPos = nLen;  token = TK_NONE; break;}
        case TK_OPERATOR         :{token = FuncOperator(pLstToken, Line, nPos);break;}
        case TK_STRING           :{token = FuncString(pLstToken, Line, nPos);break;}
        case TK_TRI_STRING       :{token = FuncTriString(pLstToken, Line, nPos);break;}
        case TK_NUMBER           :{token = FuncNumber(pLstToken, Line, nPos);break;}
        }
    }

    if ((token == TK_COMMENT_ML) ||
        (token == TK_TRI_STRING))
    {
        return token;
    }
    return TK_NONE;
}
+/


/**
 * @brief   一行解析
 * @param   [out] pLstToken    解析結果
 * @param   [in]  Line         判定する行
 * @param   [in]  eBeforeToken 前回の最終文字種別
 * @retval  true: 解析成功
 * @note    
 */
E_TOKEN MtParserAS::ParseLine( std::vector<PARSE_TOKEN>* pLstToken, 
                       const StdString &Line,
                       E_TOKEN eBeforeToken)
{
    size_t  nLen = Line.size();
    pLstToken->clear();

    E_TOKEN token = TK_NONE;


    if ((eBeforeToken == TK_COMMENT_ML) ||
        (eBeforeToken == TK_TRI_STRING))
    {
        PARSE_TOKEN parseToken;
        parseToken.eToken = eBeforeToken;
        parseToken.iCol = 0;
        pLstToken->push_back(parseToken);
    }

    for (size_t nPos = 0; nPos < nLen; nPos++)
    {
        switch (token)
        {
        case TK_NONE             :{token = FuncNone (pLstToken, Line, nPos);break;}
        case TK_WHITE_SPACE      :{token = FuncSpace(pLstToken, Line, nPos);break;}
        case TK_RESERVED_WORD    :
        case TK_IDENTIFIER       :{token = FuncIdentifier(pLstToken, Line, nPos);break;}
        case TK_COMMENT_ML       :{token = FuncComment(pLstToken, Line, nPos);break;}
        case TK_COMMENT_SL       :{nPos = nLen;  token = TK_NONE; break;}
        case TK_OPERATOR         :{token = FuncOperator(pLstToken, Line, nPos);break;}
        case TK_STRING           :{token = FuncString(pLstToken, Line, nPos);break;}
        case TK_TRI_STRING       :{token = FuncTriString(pLstToken, Line, nPos);break;}
        case TK_NUMBER           :{token = FuncNumber(pLstToken, Line, nPos);break;}
        }
    }

    if ((token == TK_COMMENT_ML) ||
        (token == TK_TRI_STRING))
    {
        return token;
    }
    return TK_NONE;
}



/**
 * @brief   一行解析
 * @param   [out] pLstToken    解析結果
 * @param   [in]  Line         判定する行
 * @param   [in]  eBeforeToken 前回の最終文字種別
 * @retval  true: 解析成功
 * @note    
 */
E_TOKEN MtParserAS::PushToken(std::vector<PARSE_TOKEN>* pLstToken, 
                              size_t iCol,
                              E_TOKEN eToken)
{
    PARSE_TOKEN  token;
    token.iCol = iCol;
    token.eToken = eToken;
    pLstToken->push_back(token);
    return eToken;
}

/**
 * @brief   
 * @param   [out] pLstToken    解析結果
 * @param   [in]  strLine      判定する行
 * @param   [in]  nPos         文字位置
 * @retval  true: 
 * @note    
 */
E_TOKEN MtParserAS::FuncNone(std::vector<PARSE_TOKEN>* pLstToken, 
                             const StdString strLine,
                             size_t nPos)
{
 
    StdChar chr = strLine[nPos];

    if      (IsWiteSpace(chr))      {return PushToken(pLstToken, nPos, TK_WHITE_SPACE);} 
    else if (IsOperator(chr))       {return PushToken(pLstToken, nPos, TK_OPERATOR);} 
    else if (IsFirstIdentfire(chr)) {return PushToken(pLstToken, nPos, TK_IDENTIFIER);} 
    else if (IsFirstNumber(chr))    {return PushToken(pLstToken, nPos, TK_NUMBER);} 
    else if (chr == _T('\"'))       {return PushToken(pLstToken, nPos, TK_STRING);} 
    
    return TK_NONE;
}

/**
 * @brief   
 * @param   [out] pLstToken    解析結果
 * @param   [in]  strLine      判定する行
 * @param   [in]  nPos         文字位置
 * @retval  true: 
 * @note    
 */
E_TOKEN MtParserAS::FuncSpace(std::vector<PARSE_TOKEN>* pLstToken, 
                             const StdString strLine,
                             size_t nPos)
{
 
    StdChar chr = strLine[nPos];

    if      (IsWiteSpace(chr))      {return TK_WHITE_SPACE;} 
    else if (IsOperator(chr))       {return PushToken(pLstToken, nPos, TK_OPERATOR);} 
    else if (IsFirstIdentfire(chr)) {return PushToken(pLstToken, nPos, TK_IDENTIFIER);} 
    else if (IsFirstNumber(chr))    {return PushToken(pLstToken, nPos, TK_NUMBER);} 
    else if (chr == _T('\"'))       
    {
        if (nPos > 2)
        {
            if ((strLine[nPos - 2] == _T('\"')) &&
                (strLine[nPos - 1] == _T('\"')))
            {
                pLstToken->at(pLstToken->size() -1).eToken =TK_TRI_STRING;
                return TK_TRI_STRING;
            }
        }
        return PushToken(pLstToken, nPos, TK_STRING);
    } 
    
    return TK_NONE;
}

/**
 * @brief   
 * @param   [out] pLstToken    解析結果
 * @param   [in]  strLine      判定する行
 * @param   [in]  nPos         文字位置
 * @retval  true: 
 * @note    
 */
E_TOKEN MtParserAS::FuncIdentifier(std::vector<PARSE_TOKEN>* pLstToken, 
                             const StdString strLine,
                             size_t nPos)
{
     StdChar chr = strLine[nPos];
    StdString str = strLine.substr(nPos);

    if(IsReserve(str))      
    {
        pLstToken->at(pLstToken->size() - 1).eToken = TK_RESERVED_WORD;
        return TK_RESERVED_WORD;
    }
    else if (IsIdentfire(chr)) 
    {
        pLstToken->at(pLstToken->size() - 1).eToken = TK_IDENTIFIER;
        return TK_IDENTIFIER;
    } 
    else if (IsWiteSpace(chr))      {return PushToken(pLstToken, nPos, TK_WHITE_SPACE);} 
    else if (IsOperator(chr))       {return PushToken(pLstToken, nPos, TK_OPERATOR);} 
    else if (chr == _T('\"'))       {return PushToken(pLstToken, nPos, TK_STRING);} 
    return TK_NONE;
}


/**
 * @brief   
 * @param   [out] pLstToken    解析結果
 * @param   [in]  strLine      判定する行
 * @param   [in]  nPos         文字位置
 * @retval  true: 
 * @note    
 */
E_TOKEN MtParserAS::FuncNumber(std::vector<PARSE_TOKEN>* pLstToken, 
                             const StdString strLine,
                             size_t nPos)
{
    StdChar chr = strLine[nPos];
    
    int iTop = pLstToken->at(SizeToInt(pLstToken->size()) - 1).iCol;
    
    if (::_istdigit(chr))
    {
        return TK_NUMBER;
    }
    else if (chr == _T('.'))
    {
        if (strLine.find_first_of(_T("."), iTop) == nPos)
        {
            return TK_NUMBER;
        }
        
        //TODO 12.34e2.3 これはどうする？

        pLstToken->at(pLstToken->size() - 1).eToken = TK_NONE;
        return TK_NONE;
    }
    else if (chr == _T('e'))
    {
        if (strLine.find_first_of(_T("e"), iTop) == nPos)
        {
            return TK_NUMBER;
        }
        
        pLstToken->at(pLstToken->size() - 1).eToken = TK_NONE;
        return TK_NONE;
    }
    else if (chr == _T('+'))
    {
        if (strLine[nPos -1] == _T('e'))
        {
            return TK_NUMBER;
        }
        
        pLstToken->at(pLstToken->size() - 1).eToken = TK_NONE;
        return TK_NONE;
    }
    else if (chr == _T('-'))
    {
        if (strLine[nPos -1] == _T('e'))
        {
            return TK_NUMBER;
        }
        
        pLstToken->at(pLstToken->size() - 1).eToken = TK_NONE;
        return TK_NONE;
    }
    else if (IsWiteSpace(chr))      {return PushToken(pLstToken, nPos, TK_WHITE_SPACE);} 
    else if (IsOperator(chr))       {return PushToken(pLstToken, nPos, TK_OPERATOR);} 
    else if (IsFirstIdentfire(chr)) {return PushToken(pLstToken, nPos, TK_IDENTIFIER);} 
    else if (chr == _T('\"'))       {return PushToken(pLstToken, nPos, TK_STRING);} 
    else {return TK_NONE;}

    return TK_NONE;
}


/**
 * @brief   
 * @param   [out] pLstToken    解析結果
 * @param   [in]  strLine      判定する行
 * @param   [in]  nPos         文字位置
 * @retval  true: 
 * @note    
 */
E_TOKEN MtParserAS::FuncString(std::vector<PARSE_TOKEN>* pLstToken, 
                             const StdString strLine,
                             size_t nPos)
{
    StdChar chr = strLine[nPos];
    if (chr == _T('\"'))
    {
        return TK_NONE;
    }
    
    return TK_STRING;
}


/**
 * @brief   
 * @param   [out] pLstToken    解析結果
 * @param   [in]  strLine      判定する行
 * @param   [in]  nPos         文字位置
 * @retval  true: 
 * @note    
 */
E_TOKEN MtParserAS::FuncTriString(std::vector<PARSE_TOKEN>* pLstToken, 
                             const StdString strLine,
                             size_t nPos)
{
    if (nPos < 2)
    {
        return TK_TRI_STRING;
    }

    if ((strLine[nPos - 2] == _T('\"')) &&
        (strLine[nPos - 1] == _T('\"')) &&
        (strLine[nPos    ] == _T('\"'))) 
    {
        return TK_NONE;
    }
    return TK_TRI_STRING;
}

/**
 * @brief   
 * @param   [out] pLstToken    解析結果
 * @param   [in]  strLine      判定する行
 * @param   [in]  nPos         文字位置
 * @retval  true: 
 * @note    
 */
E_TOKEN MtParserAS::FuncOperator(std::vector<PARSE_TOKEN>* pLstToken, 
                             const StdString strLine,
                             size_t nPos)
{
    StdChar chr = strLine[nPos];
    if (IsOperator(chr))  
    {
        if(nPos == 0)
        {
            return TK_OPERATOR;
        }

        if ( strLine[nPos - 1] == _T('/'))
        {
            if( strLine[nPos] == _T('/'))
            {
                pLstToken->at(pLstToken->size() - 1).eToken = TK_COMMENT_SL;
                return TK_COMMENT_SL;
            }
            else if ( strLine[nPos] == _T('*'))
            {
                pLstToken->at(pLstToken->size() - 1).eToken = TK_COMMENT_ML;
                return TK_COMMENT_ML;
            }
        }
        return TK_OPERATOR;
    } 
    else if (IsWiteSpace(chr))      {return PushToken(pLstToken, nPos, TK_WHITE_SPACE);} 
    else if (IsFirstIdentfire(chr)) {return PushToken(pLstToken, nPos, TK_IDENTIFIER);} 
    else if (IsFirstNumber(chr))    {return PushToken(pLstToken, nPos, TK_NUMBER);} 
    else if (chr == _T('\"'))       {return PushToken(pLstToken, nPos, TK_STRING);} 

    return TK_NONE;
}

/**
 * @brief   
 * @param   [out] pLstToken    解析結果
 * @param   [in]  strLine      判定する行
 * @param   [in]  nPos         文字位置
 * @retval  true: 
 * @note    
 */
E_TOKEN MtParserAS::FuncComment(std::vector<PARSE_TOKEN>* pLstToken, 
                             const StdString strLine,
                             size_t nPos)
{
    if(nPos == 0)
    {
        return TK_COMMENT_ML;
    }

    if (( strLine[nPos - 1] == _T('*')) &&
        ( strLine[nPos] == _T('/')))
    {
        return TK_NONE;
    }
    return TK_COMMENT_ML;
}

/**
 * @brief   識別子判定
 * @param   [in]  strId      判定する文字列
 * @retval  true: 識別子として使用可
 * @note    
 */
bool MtParserAS::IsIdentfire(const StdString strId)
{
    if (m_setReserv.size() == 0)
    {
        MtParserAS::MtParserAS();
    }

    using namespace MT_EDITOR;
    bool bRet = false;
    size_t iLen =strId.length();

    if (iLen < 1)
    {
        return false;
    }

    if(!MtParserAS::IsFirstIdentfire(strId.at(0)))
    {
        return false;
    }


    for (int iPos = 1; iPos < iLen; iPos++)
    {
        if(!MtParserAS::IsIdentfire(strId.at(iPos)))
        {
            return false;
        }
    }

    if (MtParserAS::IsReserve(strId))
    {
        return false;
    }

    return true;
}

/**
 *  @brief   テスト
 *  @param   なし
 *  @retval  なし
 *  @note
 */
void MtParserAS::Test()
{
    assert( IsWiteSpace(_T(' '))); 
    assert( IsWiteSpace(_T('\t'))); 
    assert( IsWiteSpace(_T('　'))); 
    assert(!IsWiteSpace(_T('a'))); 



    StdString strLine;
    //                      1           2
    //            012345678901234 56789 0
    strLine = _T("Hello /*good*/ \"eeee\"");
    std::vector<PARSE_TOKEN> lstToken;


    E_TOKEN eToken;
    eToken = ParseLine( &lstToken, 
                       strLine,
                       TK_NONE);


    assert( eToken ==  TK_NONE); 
    assert( lstToken.size() == 5); 
    assert( lstToken[0].iCol == 0); 
    assert( lstToken[0].eToken == TK_IDENTIFIER); 

    assert( lstToken[1].iCol == 5); 
    assert( lstToken[1].eToken == TK_WHITE_SPACE); 

    assert( lstToken[2].iCol == 6); 
    assert( lstToken[2].eToken == TK_COMMENT_ML); 

    assert( lstToken[3].iCol == 14); 
    assert( lstToken[3].eToken == TK_WHITE_SPACE); 

    assert( lstToken[4].iCol == 15); 
    assert( lstToken[4].eToken == TK_STRING); 
    return;

}

}//namespace MT_EDITOR
