#ifndef _MT_PARSE_H
#define _MT_PARSE_H

#include "MTEditorBuffer.h"
#include <set>

namespace MT_EDITOR
{
    //-----------------------------------------------
    class MtParserBase
    {
    protected:
        E_LANG  m_eLang;
        static std::set<StdChar>  m_setSpace;
        static std::set<StdChar>  m_setOperator;
        static std::set<StdString>  m_setReserv;


    public:
        MtParserBase():m_eLang(LT_NONE){;}
        virtual ~MtParserBase(){;}

        E_LANG  GetLangType(){return m_eLang;}
        virtual E_TOKEN ParseLine( std::vector<PARSE_TOKEN>* pLstToken, 
                               const StdString &Line,
                               E_TOKEN eBeforeToken){return TK_NONE;}

        E_TOKEN GetLastToken(const std::vector<PARSE_TOKEN>& lstToken)
        {
            size_t nSize = lstToken.size();
            if(nSize == 0)
            {
                return TK_NONE;
            }
            return lstToken[nSize - 1].eToken;
        }
        /*
        virtual PARSE_TOKEN ParseLast( PARSE_TOKEN tokenLast, 
            const StdString &Line)
        {
            PARSE_TOKEN token
            ;
        }
        */
    protected:
        virtual void Test(){;}
    };

    //-----------------------------------------------
    class MtParserAS: public MtParserBase
    {

    public:
        MtParserAS();
        virtual ~MtParserAS(){;}

        virtual E_TOKEN ParseLine( std::vector<PARSE_TOKEN>* pLstToken, 
                               const StdString &Line,
                               E_TOKEN eBeforeToken);

        /*
        virtual PARSE_TOKEN ParseLast( PARSE_TOKEN tokenLast, 
                               const StdString &Line);
        */

        static bool IsIdentfire(const StdString strId);

    protected:
        E_TOKEN FuncNone(std::vector<PARSE_TOKEN>* pListToken, 
                    const StdString strLine,
                    size_t nPos);

        E_TOKEN FuncSpace(std::vector<PARSE_TOKEN>* pListToken, 
                    const StdString strLine,
                    size_t nPos);

        E_TOKEN FuncIdentifier(std::vector<PARSE_TOKEN>* pListToken, 
                    const StdString strLine,
                    size_t nPos);

        E_TOKEN FuncNumber(std::vector<PARSE_TOKEN>* pLstToken, 
                                     const StdString strLine,
                                     size_t nPos);

        E_TOKEN FuncString(std::vector<PARSE_TOKEN>* pLstToken, 
                                     const StdString strLine,
                                     size_t nPos);

        E_TOKEN FuncTriString(std::vector<PARSE_TOKEN>* pLstToken, 
                                     const StdString strLine,
                                     size_t nPos);

        E_TOKEN FuncOperator(std::vector<PARSE_TOKEN>* pLstToken, 
                                     const StdString strLine,
                                     size_t nPos);

        E_TOKEN FuncComment(std::vector<PARSE_TOKEN>* pLstToken, 
                                     const StdString strLine,
                                     size_t nPos);

        E_TOKEN PushToken(std::vector<PARSE_TOKEN>* pLstToken, 
                           size_t iCol,
                           E_TOKEN eToken);




        //!< 空白文字判別
        static bool IsWiteSpace(StdChar chr);

        //!< 演算子判別
        static bool IsOperator(StdChar chr);

        //!< 識別子先頭文字判別
        static bool IsFirstIdentfire(StdChar chr);

        //!< 識別子文字判別
        static bool IsIdentfire(StdChar chr);

        //!< 数値先頭文字判別
        static bool IsFirstNumber(StdChar chr);

        //!< 予約語判別
        static bool IsReserve(const StdString& str);

        virtual void Test();
    };






}//namespace MT_EDITOR

#endif  //_MT_PARSE_H
