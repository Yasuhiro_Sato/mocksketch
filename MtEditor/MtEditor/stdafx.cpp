
// stdafx.cpp : 標準インクルード MtEditor.pch のみを
// 含むソース ファイルは、プリコンパイル済みヘッダーになります。
// stdafx.obj にはプリコンパイルされた型情報が含まれます。

#include "stdafx.h"
#include <stdexcept>
#include <mutex>

//TRACEの代わりに使用
void DB_PRINT(const TCHAR* pStr, ...)
{
    TCHAR szBuf[4096]={0};

    va_list arglist; 
    va_start(arglist, pStr); 
    _vstprintf_s(szBuf, 4096, pStr, arglist);
    va_end(arglist); 

    OutputDebugString(szBuf);

}


//!< エラー表示
void DispErrorV(bool bDlg, const StdChar * lpszFormat, va_list argptr)
{
    const static int iSize = 4096;
    StdChar    szMsg[iSize];

    _vstprintf_s(szMsg, iSize, lpszFormat, argptr);
    if (bDlg)
    {
        AfxMessageBox(szMsg);
    }

    OutputDebugString(szMsg);
}
/**
 * @brief   条件付きメッセージ.
 * @param   [in] bFail      フラグ
 * @param   [in] pMessage   表示メッセージ
 * @param   [in] pCond      演算式
 * @param   [in] pLine      行に関する表示
 */
void FailIf (long bFail, const TCHAR* pMessage, const TCHAR* pCond, const TCHAR* pLine)
{
    TCHAR szBuf[4098];

    if(bFail){
        _sntprintf_s(szBuf, sizeof(szBuf), _T("%s :assert %s (%s)\n"), pLine, pMessage, pCond);
        OutputDebugString(szBuf);
        abort();
    }
}



/**
 * @brief   ソース行表示.
 * @param   [in] pFile   ファイル名
 * @param   [in] lLine   行数
 * 
 * @return  メッセージ処理の結果
 */
TCHAR* SourceLine( const TCHAR* pFile, long lLine )
{
    // スレッドセーフではない。
    static TCHAR szLine[1024];
    _sntprintf_s(szLine, sizeof(szLine), _T("%s(%d)"),   pFile, lLine);
    return szLine;
}



//!< Size_t->int変換
int SizeToInt( size_t what )
{
    if( what > INT_MAX ) 
    {
       throw std::out_of_range("size_t > INT_MAX");
    }
    return static_cast<int>( what );
}

UINT SizeToUInt( size_t what )
{
    if( what > UINT_MAX ) 
    {
       throw std::out_of_range("size_t > UINT_MAX");
    }
    return static_cast<UINT>( what );
}

int IntptrToInt( INT_PTR what )
{
    if( what > INT_MAX ) 
    {
       throw std::out_of_range("size_t > INT_MAX");
    }
    return static_cast<int>( what );
}

int DwordptrToInt( DWORD_PTR what )
{
    if( what > INT_MAX ) 
    {
       throw std::out_of_range("size_t > INT_MAX");
    }
    return static_cast<int>( what );
}
