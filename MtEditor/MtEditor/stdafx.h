
// stdafx.h : 標準のシステム インクルード ファイルのインクルード ファイル、または
// 参照回数が多く、かつあまり変更されない、プロジェクト専用のインクルード ファイル
// を記述します。

#pragma once

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN            // Windows ヘッダーから使用されていない部分を除外します。
#endif

#include "targetver.h"

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // 一部の CString コンストラクタは明示的です。

// 一般的で無視しても安全な MFC の警告メッセージの一部の非表示を解除します。
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC のコアおよび標準コンポーネント
#include <afxext.h>         // MFC の拡張部分


#include <afxdisp.h>        // MFC オートメーション クラス
#include <assert.h>


#ifdef  UNICODE
#define StdChar             wchar_t
#define StdString           std::wstring
#define StdStringStream     std::wstringstream
#else
#define StdChar             char
#define StdString           std::string
#define StdStringStream     std::stringstream
#endif  // UNICODE


 
void DB_PRINT(const TCHAR* pStr, ...);

/**
 * アサーション
 */
void FailIf (long bFail, const TCHAR* pMessage, const TCHAR* pCond, const TCHAR* pLine);
TCHAR* SourceLine( const TCHAR* pFile, long lLine );
#define TEST_SOURCELINE() SourceLine( _T( __FILE__ ), __LINE__ )

// DBG_ASSERT は リリースモードではコンパイルされない
#ifndef _DEBUG
#define DBG_ASSERT(condition)
#else
#define DBG_ASSERT(condition)                        \
  ( FailIf( !(condition),                            \
              _T(""),       _T( #condition ),        \
                                 TEST_SOURCELINE() ) )
#endif //_DEBUG

// 以下のASSERT は リリースモードでもログを残す
#define COMMENT_ASSERT(condition, str)               \
  ( FailIf( !(condition),                            \
              str,       _T( #condition ),           \
                                 TEST_SOURCELINE() ) )

#define STD_ASSERT(condition)                        \
  ( FailIf( !(condition),                            \
              _T(""),       _T( #condition ),        \
                                 TEST_SOURCELINE() ) )
#define STD_ASSERT_DBL_EQ(condition1, condition2)            \
  ( FailIf( !(fabs (condition1 - condition2) < NEAR_ZERO),   \
  _T(""),       _T( #condition1)##_T("==")##_T(#condition2), \
                                 TEST_SOURCELINE() ) )

//標準デバッグ出力
#define STD_DBG(fmt, ...)                                \
    DbgLog2 (_T( __FILE__ ), __LINE__ , fmt, __VA_ARGS__)\


//!< Size_t->int変換
int SizeToInt( size_t what );

UINT SizeToUInt( size_t what );

int IntptrToInt( INT_PTR what );

int DwordptrToInt( DWORD_PTR what );


#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>           // MFC の Internet Explorer 4 コモン コントロール サポート
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>             // MFC の Windows コモン コントロール サポート
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxcontrolbars.h>     // MFC におけるリボンとコントロール バーのサポート









#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif


